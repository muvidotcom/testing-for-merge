<?php
class StudioAppMenu extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_apps_menu';
    }
    public function getAppSettings($studio_id) {
        $sql = "SELECT * FROM studio_apps_menu WHERE studio_id = ".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function updateAppSettings($studio_id,$menu_sum) {
        $sql = "INSERT INTO studio_apps_menu (studio_id, apps_menu) VALUES(".$studio_id.", ".$menu_sum.") ON DUPLICATE KEY UPDATE studio_id=".$studio_id.", apps_menu=".$menu_sum;
        $data = Yii::app()->db->createCommand($sql)->execute();
        return $data;
    }
}