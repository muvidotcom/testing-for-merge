<?php

class VdStudioConfig extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'vd_studio_config';
    }
}
?>