<?php
class Menu extends CActiveRecord {
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'menus';
    }
    public function set_menuid_for_reseller_customer($data = array()){
        if(count($data) > 0){
            foreach($data as $key =>$val){
                $this->$key = $val;
}
            $this->save();
        }
    }
}
?>