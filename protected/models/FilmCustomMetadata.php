<?php
class FilmCustomMetadata extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'film_custom_metadata';
    }
}
