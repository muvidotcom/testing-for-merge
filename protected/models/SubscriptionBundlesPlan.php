<?php
class SubscriptionBundlesPlan extends CActiveRecord{
    public $price;
    public $currency_id;
    
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'subscriptionbundles_plans';
    }
   
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'subscriptionbundlespricing'=>array(self::HAS_MANY, 'SubscriptionBundlesPricing','subscription_plan_id',
                'condition'=>'subscriptionbundlespricing.status=1'
            ),
            'subscriptionbundlescontent'=>array(self::HAS_MANY, 'SubscriptionBundlesContent','subscriptionbundles_plan_id')
        );
    }
    
      public function checkSubscriptionBundles($studio_id = 0){
         if($studio_id == 0){
             $studio_id = Yii::app()->common->getStudiosId();
         }
          $data = Yii::app()->db->createCommand()
                       ->select('count(*) as no_of_subscriptionsbundles')
                       ->from(" subscriptionbundles_plans ")
                       ->where(' studio_id =:studio_id AND status=1',array(':studio_id' => $studio_id))
                       ->queryAll();
         if($data[0]['no_of_subscriptionsbundles']>0){
             return $data[0]['no_of_subscriptionsbundles'];
         }else{
             return 0;
         }
     }
    public function getPlanDetails($plan_id,$studio_id, $default_currency_id = NULL, $country = NULL)
    {
         $data = Yii::app()->db->createCommand()
                       ->select(' sp.*,spr.*,sp.id as id ')
                       ->from("subscriptionbundles_plans  sp")
                       ->Join('subscriptionbundles_pricing spr' , 'spr.subscription_plan_id = sp.id')
                       ->where('sp.id =:id AND sp.studio_id =:studio_id AND sp.status=1 AND spr.status = 1',array(':id'=>$plan_id,':studio_id' => $studio_id))
                       ->queryRow();
        if (!isset($default_currency_id)) {
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;
        }
        $price_list = Yii::app()->common->getUserSubscriptionBundlesPrice($data['id'], $default_currency_id, $country);
        $data['price'] = $price_list['price'];
        $data['currency_id'] = $price_list['currency_id'];
        return $data;
    }
    public function IsSubscriptionBundlesExists($studioId,$contentId){
         $dataSubscriptionBundles = Yii::app()->db->createCommand()
                       ->select('sp.id as id')
                       ->from("subscriptionbundles_plans  sp")
                       ->Join('subscriptionbundles_content sbc' , 'sbc.subscriptionbundles_plan_id = sp.id')
                       ->where('sp.studio_id =:studio_id  AND sbc.content_id=:content_id AND sp.status=1',array(':studio_id' => $studioId,':content_id'=>$contentId))
                       ->queryRow();
       $data['id'] = $dataSubscriptionBundles['id']; 
        return $data;
    }
}
