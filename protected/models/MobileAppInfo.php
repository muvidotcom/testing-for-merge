<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MobileAppInfo
 *
 * @author Sanjeev<sanjeev@muvi.com>
 */
class MobileAppInfo extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'mobile_app_info';
    }

    public function relations() {
        return array();
    }

    public function getAll($studio_id, $type) {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:studio_id AND app_type=:type', array(':studio_id' => $studio_id, ':type' => $type))
                ->queryRow();
        return $data;
    }

    public function checkImage($studio_id, $app_type) {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:studio_id AND app_type=:type', array(':studio_id' => $studio_id, ':type' => $app_type))
                ->queryAll();
        return $data;
    }

    public function addImage($studio_id, $app_type, $image, $path, $path2) {
        switch ($image) {
            case 'appiconandroid':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('app_icon' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            
            case 'appicon':
                $data = Yii::app()->db->createCommand()
                        ->insert($this->tableName(), array('studio_id' => $studio_id, 'app_type' => $app_type, 'app_icon' => $path));
                break;
            case 'splashscreen':
                $data = Yii::app()->db->createCommand()
                        ->insert($this->tableName(), array('studio_id' => $studio_id, 'app_type' => $app_type, 'splash_screen_portrait' => $path, 'splash_screen_landscape' => $path2));
                break;
            case 'featuregraphic':
                $data = Yii::app()->db->createCommand()
                        ->insert($this->tableName(), array('studio_id' => $studio_id, 'app_type' => $app_type, 'feature_graphic' => $path));
                break;
        }
        return $data;
    }

    public function updateImage($studio_id, $app_type, $image, $path) {
        switch ($image) {
            case 'bannerscreen':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('banner_screen' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            case 'appicon':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('app_icon' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            case 'splashscreenportrait':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('splash_screen_portrait' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            case 'splashscreenlandscape':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('splash_screen_landscape' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            case 'featuregraphic':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('feature_graphic' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
            case 'transparenticon':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(), array('transparent_app_icon' => $path), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
                break;
        }
        return $data;
    }

    public function addInfo($appdata) {
        $data = Yii::app()->db->createCommand()
                ->insert($this->tableName(), $appdata);
        return $data;
    }

    public function updateInfo($studio_id, $app_type, $appdata) {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(), $appdata, 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
        return $data;
    }

    public function updateApp($studio_id, $app_type) {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(), array('status' => 1), 'studio_id=:studio_id AND app_type=:app_type', array(':studio_id' => $studio_id, ':app_type' => $app_type));
        return $data;
    }

    

}
