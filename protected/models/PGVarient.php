<?php
class PGVarient extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'pg_varient';
    }
    
    public function insertFieldData($studio_id,$value){
        $this->studio_id = $studio_id;
        $this->product_id = $value['productid'];
		$this->varient_sku = $value['sku'];
		$this->sale_price = $value['sale_price'];
		$this->created_date = gmdate('Y-m-d H:i:s');
		$this->ip = $_SERVER['REMOTE_ADDR'];
        $this->isNewRecord = TRUE;
        $this->primaryKey = NULL;
        $this->save();
        return $this->id;
    }
}
