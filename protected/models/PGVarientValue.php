<?php
class PGVarientValue extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'pg_varient_value';
    }
    
    public function insertFieldData($variantid,$value){
        foreach ($value as $key1 => $value1) {
			$this->pg_varient_id = $variantid;
            $this->pg_variable_id = $key1;
			$this->value = $value1;
			$this->isNewRecord = TRUE;
			$this->primaryKey = NULL;
			$this->save();
        }
	}
}
