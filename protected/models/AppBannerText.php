<?php
class AppBannerText extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'app_banner_text';
    }
    public function getBannerText($studio_id, $language_id = 20, $select = ""){
        $banner_text = "";
        $cond        = "";
        $criteria    = new CDbCriteria();
        $parent      = $this->checkBannerText($studio_id, $language_id);
        if(!empty($parent)){
            $parent_id  = $parent->parent_id;
            $cond       = ' AND t.language_id='. $language_id .' OR t.parent_id=0 AND t.id NOT IN ('.$parent_id.')';
        }
        $criteria->condition = 't.studio_id = ' . $studio_id.  $cond;
        if($select !=""){
           $criteria->select = $select; 
        }
        $bnr_txt = $this->find($criteria);
        if(!empty($bnr_txt)){
            $banner_text = $bnr_txt;
        }
        return $banner_text;
    }
    public function AddBannerText($studio_id, $language_id = 20, $bnr_txt = ""){
        $banner_text    = $this->checkBannerText($studio_id, $language_id);
        if(!empty($banner_text)){
            $banner_text->banner_text  = $bnr_txt;
            $banner_text->date_updated = new CDbExpression("NOW()");
        }else{
            $parent_id = 0;
            $parent    = $this->findByAttributes(array('studio_id' => $studio_id, 'parent_id' => 0), array('select' => 'id'));
            if(!empty($parent)){
                $parent_id = $parent->id;
            }
            $banner_text   = new AppBannerText;
            $banner_text->studio_id    = $studio_id;
            $banner_text->language_id  = $language_id;
            $banner_text->parent_id    = $parent_id;
            $banner_text->banner_text  = $bnr_txt;
            $banner_text->date_added   = new CDbExpression("NOW()");
            $banner_text->date_updated = new CDbExpression("NOW()");
        }
        return $banner_text->save();
    }
    public function checkBannerText($studio_id, $language_id = 20){
        $bnr_text  = $this->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id));
        return $bnr_text;
    }
}
