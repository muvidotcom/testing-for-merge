<?php

/**
 * TicketForm class.
 * TicketForm is the post structure for keeping
 * contact form post. It is used by the 'contact' action of 'SiteController'.
 */
//class TicketForm extends CFormModel
class TicketForm extends CActiveRecord {
    public static function model($className=__CLASS__){
    return parent::model($className);
    }

	public $type;
	//public $assigned_to;
	public $priority;
	public $description;
	public $id_ticket;
	//public $verifyCode;
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}
        public function tableName() {
            return 'ticket';
        } 
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('priority, description', 'required')
			// email has to be a valid email address
		);
	}
    public function getAnotherDbconnection()
    {
		 if(HOST_IP == '127.0.0.1' || HOST_IP=='52.0.64.95'){
            return Yii::app()->db;
        }else{
			$dbname='studio';
			$dsn='mysql:host=studiomuvi.cagqnk2yhltv.us-east-1.rds.amazonaws.com;dbname='. $dbname;
			$username='studio';
			$password='#%!!studio%&!%##';
			$dbcon=new CDbConnection($dsn,$username,$password);
			$dbcon->active=true;
			return $dbcon;
		}
    }
   public function insertTicket($post) {
        $tickettingdb=$this->getAnotherDbconnection();
		$post['type'] = isset($post['type']) ? $post['type'] : '';
		$post['assigned_to']=isset($post['assigned_to']) ? $post['assigned_to'] : '';
		$creation_date=date('Y-m-d H:i:s');
		$last_updated_date=date('Y-m-d H:i:s');
		$timezone = date('T');
                $post['title'] = addslashes ($post['title']);//echo $post['title'];exit;
                $post['app']=addslashes($post['app']);
                $post['description'] = addslashes ($post['description']);
		$studio_id =$post['studio_id'];//Yii::app()->session['username'];
                $attachment=addslashes($post['attachment']);
                $post['attachment_url']=isset($post['attachment_url']) ? $post['attachment_url'] : '';
                $post['app']=($post['app'])?$post['app']:0;
                $post['portal_user_id']=($post['portal_user_id'])?$post['portal_user_id']:0;
                $post['ticket_master_id'] = addslashes($post['ticket_master_id']);   
                $post['status'] = 'Open';
           $sql = "insert into ticket set ticket_master_id = '".$post['ticket_master_id']."', portal_user_id='".$post['portal_user_id']."', title='".$post['title']."', description='".$post['description']."', priority='".$post['priority']."', status='".$post['status']."', type='".$post['type']."', app='".$post['app']."', studio_id='".$studio_id."', assigned_to='".$post['assigned_to']."', creater_id='".$post['creater_id']."', creation_date='".$creation_date."', last_updated_date='".$last_updated_date."', timezone='".$timezone."', ticket_email_cc='".$post['ticket_email_cc']."',  attachment='".$attachment."',attachment_url='".$post['attachment_url']."'"; 
           $rid = $tickettingdb->createCommand($sql)->execute();
           $last_insert_id = $tickettingdb->getLastInsertID();
        $tickettingdb->active=false;
        return $last_insert_id;
    }

    public function insertNote($note, $ticket_id, $note_id = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        $note_details = addslashes($note);
        $ticket_id = $ticket_id;
        $return_flag = 0;
        if (isset($note_id) && $note_id != '') {
            $sql = "UPDATE ticket_notes SET note='" . $note_details . "' WHERE id=" . $note_id;
        } else {
            $sql = "insert into ticket_notes (id_ticket,note) values ($ticket_id,'{$note_details}')";

            $return_flag = 1;
        }

            $tickettingdb->createCommand($sql)->execute();

            $last_insert_id = $tickettingdb->getLastInsertID();
        
            $tickettingdb->active=false;
        $insertid = ($return_flag == 1) ? $last_insert_id : 0;
        return $insertid;
    }

    public function ticketList($cond, $orderby = 'last_updated_date DESC', $limit = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        $sql = "SELECT * FROM `ticket` WHERE $cond ORDER BY $orderby $limit";
        $post = $tickettingdb->createCommand($sql)->queryAll();
        $tickettingdb->active=false;
        return $post;
    }

    public function noteList($cond, $orderby = 'last_updated_date DESC', $limit = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        $sql = "SELECT * FROM `ticket_notes` WHERE $cond ORDER BY $orderby $limit";

       
            $post = $tickettingdb->createCommand($sql)->queryAll();
        $tickettingdb->active=false;
        return $post;
    }

    public function ticketNotes($id_ticket, $limit = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        $sql = "SELECT * FROM `ticket_notes` WHERE id_ticket=" . $id_ticket . " and Internal=0 ORDER BY updated_date DESC";
       
        $post = $tickettingdb->createCommand($sql)->queryAll();
        $tickettingdb->active=false;
        return $post;
    }

    public function addNote($note, $id_ticket = '', $id_note = '') {
        $tickettingdb=$this->getAnotherDbconnection();
        //echo $note."::::::::;".$id_ticket;
        $arr['note'] = addslashes($note);
        $arr['id_ticket'] = $id_ticket;
        $arr['added_by'] = Yii::app()->common->getAgent(); //Yii::app()->user->name;
        $arr['id_user'] = Yii::app()->common->getStudioId();
        $arr['add_date'] = date('Y-m-d H:i:s');
        $arr['updated_date'] = date('Y-m-d H:i:s');
        if (isset($id_note) && $id_note != '')
            $sql = "UPDATE ticket_notes SET note='" . $arr['note'] . "',added_by='" . $arr['added_by'] . "',id_user=" . $arr['id_user'] . ",updated_date='" . $arr['updated_date'] . "' WHERE id=" . $id_note;
        else
            $sql = "Insert into ticket_notes SET id_ticket=" . $id_ticket . ", note='" . $arr['note'] . "',added_by='" . $arr['added_by'] . "',id_user=" . $arr['id_user'] . ",updated_date='" . $arr['updated_date'] . "'";

        //  $sql = "insert into ticket_notes (id_ticket,note,added_by,id_user,add_date,updated_date) values ($id_ticket,'{$arr['note']}','{$arr['added_by']}',{$arr['id_user']},'{$arr['add_date']}','{$arr['updated_date']}')";

        
            $tickettingdb->createCommand($sql)->execute();
            $tickettingdb->active=false;
        if ($id_note) {
            $date = $arr['updated_date'];
            $text = "<p>" . stripslashes(nl2br(htmlentities($arr['note']))) . "</p><p><span class='grey'>By : " . ucfirst($arr['added_by']) . "
   	 					   At : " . $date . "</span></p>";
            echo $text;
        }
    }

    public function updateTicket($post) {
        $tickettingdb=$this->getAnotherDbconnection();
        $post['last_updated_date'] = date('Y-m-d H:i:s');
       // $post['last_updated_by'] = Yii::app()->common->getStudioId();
        if (!empty($post['description'])) {
            $post['description'] = addslashes($post['description']);
        }
        $update_qry = "UPDATE ticket SET ";
        $options = '';
        foreach ($post as $key => $val) {
            if ($key != 'id_ticket') {
                if ($key == 'description')
                    $options.= "$key='" . addslashes($val) . "',";
                else if ($key == 'attachment')
                    $options.= "$key='" . addslashes($val) . "',";
                else
                    $options.= "$key='" . $val . "',";
            } else
                $cond = " WHERE id=$val";
        }
        $update_qry.=trim($options, ',') . " $cond";
            $rid = $tickettingdb->createCommand($update_qry)->execute(); 
            $tickettingdb->active=false;;
        return $rid;
    }
    
    public function updateTicketNote($post) {
        $tickettingdb=$this->getAnotherDbconnection();
        $post['last_updated_date'] = date('Y-m-d H:i:s');
       // $post['last_updated_by'] = Yii::app()->common->getStudioId();
        if (!empty($post['note'])) {
            $post['note'] = addslashes($post['note']);
        }
        $update_qry = "UPDATE ticket_notes SET ";
        $cond='';
        $options = '';
        foreach ($post as $key => $val) {
            if ($key != 'id_note' ) {
                $update_qry = "insert into ticket_notes SET ";
                if ($key == 'note')
                    $options.= "$key='" . addslashes($val) . "',";
                else if ($key == 'attachment')
                    $options.= "$key='" . addslashes($val) . "',";
                else
                    $options.= "$key='" . $val . "',";
            } else
                
                $cond = " WHERE id=$val";
        }
        
        $update_qry.=trim($options, ',') . " $cond";

       $rid = $tickettingdb->createCommand($update_qry)->execute();
       $tickettingdba->active=false;
        return $rid;
    }
    
     public function updateNote($post) {
         $id=$post['id'];
        $tickettingdb=$this->getAnotherDbconnection();
        $post['last_updated_date'] = date('Y-m-d H:i:s');
       // $post['last_updated_by'] = Yii::app()->common->getStudioId();
        if (!empty($post['note'])) {
            $post['note'] = addslashes($post['note']);
        }
        $update_qry = "UPDATE ticket_notes SET ";
        $cond='';
        $options = '';
        foreach ($post as $key => $val) {
            
                if ($key == 'note')
                {
                    $options.= "$key='" . addslashes($val) . "',";
                }
                else if ($key == 'attachment'){
                    $options.= "$key='" . addslashes($val) . "',";
                }
                else
                {
                    $options.= "$key='" . $val . "',";
                }
              
        }
         $cond = " WHERE id=$id";
        $update_qry.=trim($options, ',') . " $cond";
     
        $rid = $tickettingdb->createCommand($update_qry)->execute();
        $tickettingdb->active=false;
        return $rid;
    }

    public function deleteRecord($id,$table){
        $tickettingdb=$this->getAnotherDbconnection();
		$delsql = "DELETE FROM $table WHERE studio_id=".Yii::app()->common->getStudiosId() ." AND id=$id";
        $rid = $tickettingdb->createCommand()->delete($table, 'id=:id', array(':id' => $id));
        $tickettingdb->active=false;
		echo $rid;//exit;
	}
	public function getUserDetail($id_ticket,$studio_id){
		$sql = "SELECT * FROM `users` WHERE id=".$studio_id;
		$user = Yii::app()->db->createCommand($sql)->queryAll();
		$tktsql = "SELECT * FROM `ticket` WHERE id=".$id_ticket;
        $tickettingdb=$this->getAnotherDbconnection();
        $ticket = $tickettingdb->createCommand($tktsql)->queryAll();
        $tickettingdb->active=false;
		$arr['email'] = $user[0]['email'];
		$arr['id_ticket'] = $id_ticket;
		$arr['subject'] = "Reply from Ticketing System - $id_ticket";
		$arr['desc'] = "Hello ".$user[0]['name']."<br /><br />
						<b>Ticket Number</b>: $id_ticket<br />
						<b>Ticket Detail:</b> ".$ticket[0]['description'];
		echo json_encode($arr);exit;
	}
	public function replyToClient($post){
		$mandrill=new Mandrill();	
		$mandrill->sendmailViaMandrill($post['content'], $post['subject'],$post['to']);
	}
        public  function getOwner(){
            $sql = "SELECT id,first_name FROM `owner`";
            $owner = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($owner as $key=>$val){
                    $arr[0] ='-Select-';
                    $arr[$val['id']]=  ucfirst($val['first_name']);
            }
            return $arr;
        }
        public  function getStudio(){
            $sql = "SELECT id,name FROM `studios` where `is_subscribed`='1' AND `is_deleted`='0' AND `is_default`='0' AND `status`='1' order by name ASC";
            $arr =array();
            $owner = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($owner as $key=>$val){
                   // $arr[1]= '-Select-';
                    $arr[$val['id']]=  ucfirst($val['name']);
            }
            return $arr;
        }
       
      
}