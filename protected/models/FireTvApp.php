<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FireTvApp extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'app_fire_tv';
    }
    public function getAll($studio_id,$type)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:studio_id AND app_type=:type',array(':studio_id' => $studio_id,':type'=>$type))
                ->queryRow();
        return $data;
    }
    
    public function updateInfo($studio_id,$app_type,$appdata)
    {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(),$appdata,'studio_id=:studio_id AND app_type=:app_type',array(':studio_id' => $studio_id,':app_type'=>$app_type));
        return $data;
    }
     public function updateApp($studio_id,$app_type)
    {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(),array('status' => 1),'studio_id=:studio_id AND app_type=:app_type',array(':studio_id' => $studio_id,':app_type'=>$app_type));
        return $data;
    }
    public function addInfo($appdata)
    {
        $data = Yii::app()->db->createCommand()
                ->insert($this->tableName(),$appdata);
        return $data;
    }
    public function checkImage($studio_id,$app_type)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:studio_id AND app_type=:type',array(':studio_id' => $studio_id,':type'=>$app_type))
                ->queryAll();
        return $data;
    }
    
    public function addImage($studio_id,$app_type,$image,$path,$path2)
    {
        switch ($image){
            case 'appicon':
                $data = Yii::app()->db->createCommand()
                        ->insert($this->tableName(),array('studio_id' => $studio_id,'app_type' => $app_type,'app_icon' => $path));      
                break;
            case 'splashscreen':
                $data = Yii::app()->db->createCommand()
                        ->insert($this->tableName(),array('studio_id' => $studio_id,'app_type' => $app_type,'splash_screen' => $path));      
                break;
            
        }
        return $data;
        
    }
    
    public function updateImage($studio_id,$app_type,$image,$path,$path2)
    {
        switch ($image){
            case 'appicon':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(),array('app_icon' => $path),'studio_id=:studio_id AND app_type=:app_type',array(':studio_id' => $studio_id,':app_type'=>$app_type));
                break;
            case 'splashscreen':
                $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(),array('splash_screen' => $path),'studio_id=:studio_id AND app_type=:app_type',array(':studio_id' => $studio_id,':app_type'=>$app_type));
                break;
            
        }
        return $data;
    }
    public function relations(){
        return array();
    }
}