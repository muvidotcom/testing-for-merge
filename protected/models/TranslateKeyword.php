<?php

class TranslateKeyword extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'translate_keyword';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function getMessages($trans_type,$studio_id=false){
        $condition = 'trans_type=:trans_type';
        $params[':trans_type'] = $trans_type;
        if($studio_id){
            $condition .=' AND(studio_id=:studio_id OR studio_id=0)';
            $params[':studio_id'] = $studio_id;
        }
        $msgs = TranslateKeyword::model()->findAll(array(
            'condition'=>$condition,
            'params'=>$params
        ));
        return $this->makeTransKeyValue($msgs);
    }
    public function getAllMessages($studio_id=false,$mobile_app=false){
        $where = '';
        $condition = '';
        if($mobile_app){
          $condition .='(device_type=:device_type OR device_type=:all_device) ';  
          $params[':device_type'] = $mobile_app;
          $params[':all_device']  = '2';
        if($studio_id){
              $condition .=" AND ";
          }
        }
        if($studio_id){
            $condition .=' (studio_id=0 OR (store_keys=:store_keys AND studio_id=:studio_id AND language_id=20)) ';
            $params[':studio_id'] = $studio_id;
            $params[':store_keys'] = '1';
            $where = array('condition'=>$condition,'params'=>$params);
        }else{
            $condition .='studio_id=0';
        }
        $msgs = TranslateKeyword::model()->findAll($where);
        return $this->makeTransKeyValue($msgs);
    }
    public function makeTransKeyValue($msgs){
        $message = array();
        if(!empty($msgs)){
            foreach($msgs as $msg){
              $message[$msg->trans_key]  =  $msg->trans_value;
            }
        }
        return $message;
    }
    public function GetKeysofValue($trans_array=array()){
        $duplicate_keys = array();
        if(!empty($trans_array)){
            foreach($trans_array as $key=>$val){
                $trans = Yii::app()->db->createCommand()
                    ->select('trans_key,trans_type')
                    ->from('translate_keyword')
                    ->where('trans_value=:trans_val AND trans_key !=:trans_key', array(':trans_val'=>$val,':trans_key'=>$key))
                    ->queryRow();
                $duplicate_keys[$key] =  $trans['trans_key'];
            }
        }
        return $duplicate_keys;
    }
    public function getLanguageType($trans_key,$studio_id){
        $data = Yii::app()->db->createCommand()
            ->select('trans_type')
            ->from('translate_keyword')
            ->where("trans_key =:trans_key AND (studio_id=:studio_id OR studio_id=0)", array(':studio_id'=>$studio_id,':trans_key'=>$trans_key))
            ->setFetchMode(PDO::FETCH_OBJ)->queryROW();
        if(!empty($data)){
            return $data->trans_type;
        }else{
            return false;
        }
    }
    public function checkUniqueKey($studio_id, $trans_key, $trans_val){
        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from('translate_keyword')
            ->where("trans_key =:trans_key AND (studio_id=:studio_id OR studio_id=0)", array(':studio_id'=>$studio_id,':trans_key'=>$trans_key))
            ->queryAll();
        return $data;
}
    public function insertNewKey($studio_id, $trans_key, $trans_val,$device_type = '0'){
        $trans = new TranslateKeyword;
        $trans->studio_id   = $studio_id;
        $trans->trans_key   = $trans_key;
        $trans->trans_value = $trans_val;
        $trans->trans_type  = '0';
        $trans->device_type = $device_type;
        $trans->store_keys = '1';
        $trans->setIsNewRecord(true);
        $trans->setPrimaryKey(NULL);
        return $trans->save();
    }
    public function UpdateKey($studio_id, $trans_key, $trans_val = ''){
        $trans = TranslateKeyword::model()->findByAttributes(array('studio_id'=>$studio_id,'trans_key'=>$trans_key,'language_id' => 20));
        if(!empty($trans)){
            $trans->trans_value = $trans_val;
            return $trans->save();
        }else{
            $this->insertNewKey($studio_id, $trans_key, $trans_val,'2');
        }
    }
    public function DeleteKey($studio_id, $trans_key){
        $params = array(':studio_id'=>$studio_id, ':trans_key'=>$trans_key);
        return TranslateKeyword::model()->deleteAll('studio_id=:studio_id AND trans_key=:trans_key', $params);
    }
	
	public function getKeyValue($lang_code, $trans_key, $studio_id){
        $lang      = array();
        $theme     = Studio::model()->findByPk($studio_id, array('select' => 'theme'))->theme;
        if(file_exists(ROOT_DIR."languages/".$theme."/".$lang_code.".php")){
            $lang  = include( ROOT_DIR."languages/".$theme."/".$lang_code.".php");
            $lang  = @$lang["server_messages"] ? @$lang["server_messages"] : $lang;
        }
        if(!isset($lang[$trans_key])){
            $lang = Yii::app()->db->createCommand()->select('trans_value')->from('translate_keyword')->where("(studio_id = :studio_id OR studio_id = 0) AND trans_key = :trans_key",array(':studio_id'=>$this->studio_id, ':trans_key' => $trans_key))->queryColumn();
            return @$lang[0];
        } else {
            return @$lang[$trans_key];
        }
    }
    public function languageCategory($studio_id){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $controller = Yii::app()->controller;
        $studio_details = $controller->studio;
        $active_features = array(1);
        $active_sub_features =  array();
        $srest = StudioCountryRestriction::model()->countByAttributes(array('studio_id' => $studio_id));
        $grest = GeoBlockCategory::model()->countByAttributes(array('studio_id' => $studio_id)); 
        if(($srest) || ($grest)):
            $active_features[] = 2;
        endif;
        if($template->content_type != 1):
            $active_features[] = 3;
        endif;
        if($controller->is_audio_enable):
            $active_features[] = 5;
        endif;
        if(Yii::app()->general->getStoreLink()):
            $active_features[] = 6;
        endif;  
        if (Yii::app()->custom->restrictDevices($studio_id, 'restrict_no_devices')):
            $active_features[] = 7;
        endif;
        $ugcconfig = StudioConfig::model()->getconfigvalue('user_generated_content');
        if(!empty($ugcconfig) && $ugcconfig['config_value'] == 1):
            $active_features[] = 8;
        endif;
        if($studio_details->rating_activated){ $active_sub_features[] = "1,1";}
        $config = StudioConfig::model()->getconfigvalue('newsletter');
        if(!empty($config) && $config['config_value'] == 1){ $active_sub_features[] = "1,2";}
        if($controller->has_blog){ $active_sub_features[] = "1,3";}
        if($controller->has_faqs){ $active_sub_features[] = "1,4";}
        if($studio_details->need_login){ $active_sub_features[] = "1,5";}
        $gateway       = Yii::app()->common->isPaymentGatwayExists($studio_id);
        $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
        $gateway_activated = $voucher_activated = $free_content_activated = $subscription = $payperview = $payperview_bundle = $coupon = $preorder = $commonkeys = 0;
        if(isset($monetizations['menu']) && !empty($monetizations['menu'])):
            $active_features[] = 4;
            if($monetizations['menu'] & 128):
                $vdata = Yii::app()->db->createCommand()->select('COUNT(*) as total')->from('voucher')->where('studio_id='.$studio_id.' AND status=1 AND valid_until >="'.date('Y-m-d').'"')->queryRow();
                if(!empty($vdata) && $vdata['total'] > 0):
                    $active_sub_features[] = "4,5";
                endif;
            endif;
            if($monetizations['menu'] & 8):
                $active_sub_features[] = "4,6";
            endif;
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])):
                if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1)): 
                    $sdata = Yii::app()->db->createCommand()->select('COUNT(*) as total')->from('subscription_plans')->where('studio_id='.$studio_id.' AND status=1 AND parent_id = 0')->queryRow();
                    if(!empty($sdata) && $sdata['total'] > 0):
                        $active_sub_features[] = "4,1";
                    endif;
                endif;
                if($monetizations['menu'] & 2):
                    $active_sub_features[] = "4,2";
                endif;
                if($monetizations['menu'] & 64):
                    $active_sub_features[] = "4,3";
                endif;
                if($monetizations['menu'] & 16):
                    $active_sub_features[] = "4,4";
                endif;
                if($monetizations['menu'] & 32):
                    $active_sub_features[] = "4,7";
                endif;
            endif;            
        endif; 
        $res = array(
            'active_features' => $active_features,
            'active_sub_features' => $active_sub_features
        );
        return $res ;
    }
    public function getAllTranslatedMessages($studio_id=false,$language_code='en',$language_id=20,$theme=''){
        if(!$studio_id):
            $studio_id = Yii::app()->common->getStudiosId();
        endif;
        if(!$theme):
            $theme = Yii::app()->controller->studio->theme;
        endif;
        $lang_config = StudioLanguageConfig::model()->getconfig($studio_id, 'is_new');
        $lang = array();
        if((!empty($lang_config)) && ($lang_config->config_value == 1)):
            $enable_lang_msg = $this->findAll(array('condition'=>'studio_id=:studio_id AND language_id=:language_id','params'=>array(':studio_id'=>$studio_id,':language_id'=>$language_id)),array('select'=>'trans_key,trans_value,trans_type'));
            foreach($enable_lang_msg as $lang_msg):
                if($lang_msg->trans_type == '0'):
                    $lang['static_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
                elseif($lang_msg->trans_type == '1'):
                    $lang['js_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
                else:
                    $lang['server_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
                endif;
            endforeach;
        endif;
        if(empty($lang)):
            if(file_exists(ROOT_DIR."languages/".$theme."/".$language_code.".php")):
                $lang = include( ROOT_DIR."languages/".$theme."/".$language_code.".php");
            else:
                $lang = $this->getOriginalMessages($studio_id);
            endif;
        endif;
        return $lang;
    }
    public function getOriginalMessages($studio_id=false){
        if(!$studio_id):
            $studio_id = Yii::app()->common->getStudiosId();
        endif;
        $trans_msg = $this->findAll(array('condition'=>'language_id=20 AND (studio_id=0 OR (store_keys=:store_keys AND studio_id=:studio_id))','params'=>array(':store_keys'=>'1',':studio_id'=>$studio_id)),array('select'=>'trans_key,trans_value,trans_type'));
        foreach($trans_msg as $lang_msg):
            if($lang_msg->trans_type == '0'):
                $lang['static_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
            elseif($lang_msg->trans_type == '1'):
                $lang['js_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
            else:
                $lang['server_messages'][$lang_msg->trans_key] = $lang_msg->trans_value; 
            endif;
        endforeach;
        return $lang;
    }
    public function getLatestKey($mail_date =""){  
        if(!$mail_date){
            $mail_date = date('Y-m-d H:i:s');
        }
        $latest_keys = $this->findAll('studio_id = 0 AND date_added >"'.$mail_date.'"');
        return $latest_keys;
    }
}
