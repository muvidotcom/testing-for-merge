<?php
class TranslationCategory extends CActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'translation_message_category';
    }
    public function relations() {
        return array(
            'sub_cat'=>array(self::HAS_MANY, 'TranslationSubCategory', 'category_id'),
        );
    } 
    public function getAllSubCategories(){
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'sub_cat' => array(// this is for fetching data
                'together' => false,
            ),
        );
        $trans_cat = $this->findAll($criteria);
        $cat_subcat = array();
        if(!empty($trans_cat)){
            foreach($trans_cat as $cat){
                $cat_subcat[$cat->id][0] = "Common";
                if(!empty($cat['sub_cat'])){
                    foreach($cat['sub_cat'] as $subcat){
                        $cat_subcat[$cat->id][$subcat->sub_id] = $subcat['subcategory_name'];
                    }
                }
            }
        }
        return $cat_subcat;
    }
}