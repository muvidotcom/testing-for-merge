<?php

class LivestreamCameraLog extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'livestream_camera_log';
	}

	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function getDataByStudioId($studio_id) {
		$fields = LivestreamCameraLog::model()->find(array(
			'condition' => 'studio_id=:studio_id',
			'params' => array(':studio_id' => $studio_id)
		));
		return($fields);
	}

	public function insertLogData($value) {
		foreach ($value as $key1 => $value1) {
			$this->$key1 = $value1;
		}
		$this->isNewRecord = TRUE;
		$this->primaryKey = NULL;
		$this->save();
		return $this->id;
	}

}
