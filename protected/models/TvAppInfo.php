<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MobileAppInfo
 *
 * @author Sanjeev<sanjeev@muvi.com>
 */
class TvAppInfo extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'tv_app_info';
    }
    
    public function relations(){
        return array();
    }
}
