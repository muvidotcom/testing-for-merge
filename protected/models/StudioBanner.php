<?php
class StudioBanner extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_banner';
    }
    /**
    * @return array relational rules.
    */
   
    public function getMaxOrd(){
        $studio_id = Yii::app()->common->getStudiosId();
        $bnr = StudioBanner::model()->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($bnr) && count($bnr)){
            return (int) $bnr->id_seq;
        }
        else{
            return 0;
        }      
    }
    
    public function getBannerObj($id){
        $studio_id = Yii::app()->common->getStudiosId();
        $bnr = StudioBanner::model()->find(array(
            'select' => '*',
            'condition' => 'id=:id AND studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id, ':id' => $id),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));  
        return $bnr;
    }  
    
    public function getBannerObjByPos($banner_section, $pos){
        $studio_id = Yii::app()->common->getStudiosId();
        $bnr = StudioBanner::model()->find(array(
            'select' => '*',
            'condition' => 'banner_section=:banner_section AND studio_id=:studio_id AND id_seq=:id_seq',
            'params' => array(':banner_section' => $banner_section, ':studio_id' => $studio_id, ':id_seq' => $pos),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));  
        return $bnr;
    }      
}
