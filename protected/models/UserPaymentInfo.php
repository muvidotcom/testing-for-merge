<?php
class UserPaymentInfo extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'user_payment_infos';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studios','studio_id'),
        );
    }
}
