<?php
class StudioLoginHistory extends CActiveRecord{
    public static function model($className=__CLASS__) {
            return parent::model($className);
      }
    protected function afterValidate(){
            //$this->password = $this->encrypt($this->password);
            return parent::afterValidate();
    }
    public function tableName() {
        return 'studio_login_hostory';
    }                
    /**
    * @return array relational rules.
    */
   public function relations()
   {
        return array('studiorel'=>array(self::BELONGS_TO, 'Studio','studio_id'));
   }
}
