<?php

/**
 * This is the model class for table "studio_content_restriction".
 *
 * The followings are the available columns in table 'studio_content_restriction':
 * @property integer $id
 * @property integer $studio_id
 * @property integer $category_id
 * @property string $country_code
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 */
class StudioContentRestriction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'studio_content_restriction';
	}
	/**
	 * @return array relational rules.
	 */
	 public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
        }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudioCountryRestriction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
