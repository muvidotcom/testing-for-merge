<?php
class AppMenuItems extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'app_menu_items';
    }
    public function getAllMenus($studio_id, $menu_id, $language_id = 20){
        $cond     = " AND t.language_parent_id = 0";
        $criteria = new CDbCriteria();
        if($language_id != 20){
            $parent_ids = array(); 
            $parent    = $this->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id, 'menu_id' => $menu_id));
            if(!empty($parent)){
                foreach($parent as $parent){
                    $parent_ids[] = $parent->language_parent_id;
                }
                $parent_ids = implode(',', $parent_ids);
                $cond       = ' AND t.language_id=' .$language_id.' OR (t.language_parent_id=0 AND t.studio_id= ' . $studio_id. ') AND t.id NOT IN ('.$parent_ids.')';
            }
        }
        $criteria->condition = 't.studio_id = ' . $studio_id. ' AND t.menu_id =' . $menu_id .  $cond;
        $criteria->order     = 't.id_seq ASC';
        $menus               = $this->findAll($criteria);
        $amenu = $childs = array();
        if(!empty($menus)){
            foreach($menus as $menu){
                if($menu->language_parent_id > 0){
                    $menu->id = $menu->language_parent_id;
                }
                if($menu->parent_id > 0){
                    $childs[$menu->id] = array(
                        'id'                  => (int)$menu->id,
                        'title'               => $menu->title,
                        'parent_id'           => (int)$menu->parent_id,
                        'link_type'           => $menu->link_type,
                        'permalink'           => $menu->permalink,
                        'value'               => $menu->value,
                        'id_seq'              => (int)$menu->id_seq,
                        'language_id'         => (int)$menu->language_id,
                        'language_parent_id'  => (int)$menu->language_parent_id,
                    );
                }else{
                    $parents[$menu->id] = array(
                        'id'                  => (int)$menu->id,
                        'title'               => $menu->title,
                        'parent_id'           => (int)$menu->parent_id,
                        'link_type'           => $menu->link_type,
                        'permalink'           => $menu->permalink,
                        'value'               => $menu->value,
                        'id_seq'              => (int)$menu->id_seq,
                        'language_id'         => (int)$menu->language_id,
                        'language_parent_id'  => (int)$menu->language_parent_id,
                        'child'               => array()
                    );
                }
            }
            $amenu = $parents;  
            if(!empty($childs)){
                foreach($childs as $child){
                    if(array_key_exists($child['parent_id'], $parents)){
                       $amenu[$child['parent_id']]['child'][] = (array)$child;
                    }
                }
            }
            foreach($amenu as $appmenu){
                $appMenuItems[] = $appmenu;
            }
        }
        return $appMenuItems;
    }
}