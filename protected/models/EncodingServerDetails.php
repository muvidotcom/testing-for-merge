<?php

class EncodingServerDetails extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'encoding_server_details';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    public function getFolderPathForSpecificServer($serverIp,$fieldName)
    {
        $data = Yii::app()->db->createCommand()
                ->select($fieldName.',threads')
                ->from($this->tableName())
                ->where('code_server_ip ="'.$serverIp.'" and '.$fieldName.'!= ""'  )
                ->queryRow();
        return $data;
    }
    
    public function getFolderPath($fieldName)
    {
        $data = Yii::app()->db->createCommand()
                ->select($fieldName.',threads')
                ->from($this->tableName())
                ->where($fieldName.'!= "" and active =1 and ip != "52.220.157.212" and code_server_ip =""')
                ->queryRow();
        return $data;
    }
    
    public function getFolderPathForSingapore($fieldName)
    {
        $data = Yii::app()->db->createCommand()
                ->select($fieldName.',threads')
                ->from($this->tableName())
                ->where('ip ="52.220.157.212" and '.$fieldName.'!= ""'  )
                ->queryRow();
        return $data;
    }
}
