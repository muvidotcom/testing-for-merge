<?php
class AnnouncementLog extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    public function tableName() {
        return 'notification_email_log';
    }
    public function CountAnnouncementSent($studio_id,$weekrange){
        $NextWeekStartDate = date('jS F Y', strtotime($weekrange[1] . ' +1 day'));
        $config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'announcement_mail_per_week');
        if(!empty($config)){
            $announcement_per_week = $config['config_value'];
        }else{
            $config = StudioConfig::model()->getconfigvalueForStudio(0,'announcement_mail_per_week');  
            $announcement_per_week = $config['config_value'];
        }
        $announcement_log = $this->findAll(array('condition'=>'studio_id='.$studio_id.' AND notification_send_time BETWEEN "'.$weekrange[0].'" AND "'.$weekrange[1].'"'));
        $announcement_sent_this_week = count($announcement_log);
        if($announcement_sent_this_week >= $announcement_per_week){
            $res['success'] = false;
            $res['msg'] = "You have exhausted your allowed limit of ".$announcement_per_week." announcements in a week. You can send an announcement on ".$NextWeekStartDate;
        }else{
            $res['success'] = true;
            $res['msg'] = "You can send announcement.";
        }
        echo json_encode($res);exit;
    }   
}
