<?php
class movies extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'movies';
    }
 /**
  * @method getFilms() Returns the list the Movie with there details.
  * @return array
  */   
    
    //Updated for getting ppv details
    public function getFilms($search_text='',$limit='',$offset='',$cond=" 1 "){
		$pgconnection = Yii::app()->db2;
		$sql_data = "SELECT M.movie_id,M.id AS movie_stream_id,F.name,F.content_type,full_movie, F.movie_payment_type, F.price_for_paid, F.price_for_unpaid, F.available_for_hours FROM movie_streams M,films F WHERE M.movie_id = F.id AND M.studio_user_id=".Yii::app()->common->getStudioId()."  ".$cond." AND is_episode=0 ORDER BY M.updated_at DESC LIMIT ".$limit." OFFSET ".$offset;
		$list = $pgconnection->createCommand($sql_data)->queryAll();
		//echo "<pre>";print_r($list);exit;
		$studio_user_id = Yii::app()->common->getStudioId();//=29; 
		$cond ='';
		$movieids='';
		if($list){
			foreach($list as $key=>$val){
				/*if($_SERVER['HTTP_HOST']!='localhost'){
					$url = "http://www.muvi.com/get_movie_details?movie_id=".$val['movie_id'];
				}else{
					$url = "http://172.29.11.5:5050/get_movie_details?movie_id=".$val['movie_id'];
				}
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				$cdata = curl_exec($ch);
				curl_close($ch); 
				$result = json_decode($cdata,true);*/
				$cdata = Yii::app()->Controller->getMovieDetails($val['movie_id']);
				$result = json_decode($cdata,true);
				$detailsData['data'] = $cdata;
					
				$result[0]['id'] = $val['movie_id'];
				$result[0]['name'] = $val['name'];
				$result[0]['full_movie'] = $val['full_movie'];
				$result[0]['movie_stream_id'] = $val['movie_stream_id'];
				$result[0]['content_type'] = $val['content_type'];
				$result[0]['movie_payment_type'] = trim($val['movie_payment_type']);
				$result[0]['price_for_paid'] = $val['price_for_paid'];
				$result[0]['price_for_unpaid'] = $val['price_for_unpaid'];
				$result[0]['available_for_hours'] = $val['available_for_hours'];

				$movies[] = $result;
				//$movieids .= "'".$val['movie_id']."',";
			}
			return $movies;
		}else{
			return array();
		}
		
        //echo "<pre>";print_r($list1);exit;
        //return $view;
    }
 /**
  * @method getdMovie() Returns the list the Movie with there details.
  * @return array
  */   
    public function getMovie($search_text='',$limit='',$offset=''){
		$pgconnection = Yii::app()->db;
		$sql_data = "SELECT id,name FROM films WHERE LOWER(name) LIKE '%".strtolower($search_text)."%' AND studio_id = ".Yii::app()->common->getStudioId()."   ORDER BY created_date DESC LIMIT ".$limit." OFFSET ".$offset;
		$list = $pgconnection->createCommand($sql_data)->queryAll();
		//echo "<pre>";print_r($list);exit;
		$studio_user_id = Yii::app()->common->getStudioId();//=29; 
		$cond ='';
		$movieids='';
		if($list){
			foreach($list as $key=>$val){
				$cdata = Yii::app()->Controller->getMovieDetails($val['id']);
				$result = json_decode($cdata,true);
				$detailsData['data'] = $cdata;
					
				$result[0]['id'] = $val['id'];
				$result[0]['name'] = $val['name'];
				$result[0]['content_type'] = $val['studio_content_types'];
				$movies[] = $result;
			}
			return $movies;
		}else{
			return array();
		}
		
        //echo "<pre>";print_r($list1);exit;
        //return $view;
    }

}
