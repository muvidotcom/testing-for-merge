<?php
class Faq extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'faqs';
    }
    public function findAllPosts($studio_id) {
        //Find All FAQs       
        $posts = Faq::model()->findAll(array(
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id ASC'
        ));          
        return($posts);
    }
}    

