<?php
class ThirdpartyServerAccess extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'thirdparty_server_access';
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}