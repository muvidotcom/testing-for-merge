<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StudioExtension extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'studio_extensions';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
            'extension'=>array(self::BELONGS_TO, 'Extension', 'id'),
        );
    } 
    
    public function findExtensions($studio_id = 0) {
        if(!$studio_id)
            $studio_id = Yii::app()->common->getStudiosId();
		if(isset($_SESSION[$studio_id]['StudioExtensions']) && $_SESSION[$studio_id]['StudioExtensions']){
			return $_SESSION[$studio_id]['StudioExtensions'];
		}else{
        //Find Studio Extensions        
        $extensions = StudioExtension::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND t.status = :status',
            'params' => array(':studio_id' => $studio_id, ':status' => 1),
            'order' => 't.id ASC'
        ));   
			$_SESSION[$studio_id]['StudioExtensions'] = $extensions;
        return($extensions);      
    }
}
}

