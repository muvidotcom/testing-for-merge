<?php

class Package extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        if (@IS_DEMO_PAYMENT == 1) {
            return 'packages_demo';
        } else {
            return 'packages';
        }
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    /*
     * @method getPackages getting the package value
     * @return array
     * @author SNL
     */
    
    public function getPackages($package_id = NULL) {
        $data = array();
        
        $cond = "";
        if (isset($package_id) && intval($package_id)) {
            $cond = " id=$package_id AND ";
        }
        
        if (@IS_DEMO_PAYMENT == 1) {
            $table = 'packages_demo';
        } else {
            $table = 'packages';
        }
        
        $package = Yii::app()->db->createCommand()
        ->select('p.*')
        ->from($table.' p')
        ->where(" $cond parent_id = 0 AND status = 1 order by p.sequence asc")
        ->queryAll();
        
        if (isset($package) && !empty($package)) {
            $data['package'] = $package;
            
            if (isset($package_id) && intval($package_id)) {
                
            } else {
                foreach ($package as $key => $value) {
                    $application = $this->getApplications($value['id']);
                    if (isset($application) && !empty($application)) {
                        $data['appilication'][$value['id']] = $application;
                    }
                }
            }
        }
        
        return $data;
    }
    
    public function getApplications($package_id = NULL, $application_id = NULL) {
        $applications = array();
        
        if (isset($package_id) && intval($package_id)) {
            $cond = '';
            if (isset($application_id) && !empty($application_id)) {
                $cond = ' id IN ('.$application_id.') AND ';
            }
            
            if (@IS_DEMO_PAYMENT == 1) {
                $table = 'packages_demo';
            } else {
                $table = 'packages';
            }
            $applications = Yii::app()->db->createCommand()
            ->select('p.*')
            ->from($table.' p')
            ->where(" $cond parent_id = $package_id AND status = 1 order by p.id asc")
            ->queryAll();
        }
        
        return $applications;
    }

}
