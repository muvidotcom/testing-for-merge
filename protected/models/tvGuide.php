<?php
class tvGuide extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'tvguide_channel_master';
    }
    public function allguides($start_time='',$end_time=''){
        $studio_id = Yii::app()->common->getStudiosId();
        if($start_time == ''){
            $time = date('Y-m-d H').":00:00";
        }else{
            $time = $start_time;
        }
        if($end_time == ''){
            $timeafter4hour = date('Y-m-d H', strtotime("+ 4 hours")).":00:00";
        }else{
            $timeafter4hour = $end_time;
        }
        $time_after1min = date('Y-m-d H:i:s', strtotime($time . ' + 1 minutes'));
        $sql = "SELECT `tcm`.`id`,`tcm`.`channel_name`,`tcm`.`channel_description`,`tcm`.`channel_logo`,`tcm`.`studio_id`,`se`.`id` as `event_id`,`se`.`event_name`,`se`.`event_desc`,`ls`.`movie_id`,`ls`.`stream_type`,`ls`.`stream_id`,`ls`.`start_time`,`ls`.`end_time`,`livestream`.`feed_url`,`livestream`.`id` as `live_id`,`movie_streams`.`full_movie`,`movie_streams`.`id` as `film_id` FROM `tvguide_channel_master` as `tcm` JOIN `stream_events` as `se` ON `tcm`.id = `se`.`channel_id` JOIN `ls_schedule` as `ls` ON `se`.`id`=`ls`.`event_id` LEFT JOIN `livestream` ON `ls`.`movie_id` = `livestream`.`id` LEFT JOIN `movie_streams` ON `ls`.`movie_id` = `movie_streams`.`id` WHERE `tcm`.`studio_id`={$studio_id} AND ((`ls`.`start_time` BETWEEN '".$time."' AND '".$timeafter4hour."') OR (`ls`.`end_time` BETWEEN '".$time_after1min."' AND '".$timeafter4hour."'))  ORDER BY `ls`.`start_time` ";
        //echo $sql;exit;
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        return $res;
    }
    public function allguideschannelwise($channel_id,$start_time='',$end_time=''){
        $studio_id = Yii::app()->common->getStudiosId();
        if($start_time == ''){
            $time = gmdate('Y-m-d H').":00:00";
        }else{
            $time = $start_time;
        }
        if($end_time == ''){
            $timeafter4hour = gmdate('Y-m-d H', strtotime("+ 4 hours")).":00:00";
        }else{
            $timeafter4hour = $end_time;
        }
        $time_after1min = date('Y-m-d H:i:s', strtotime($time . ' + 1 minutes'));
        $sql = "SELECT `tcm`.`id`,`tcm`.`channel_name`,`tcm`.`channel_description`,`tcm`.`channel_logo`,`tcm`.`studio_id`,`se`.`id` as `event_id`,`se`.`event_name`,`se`.`event_desc`,`ls`.`movie_id`,`ls`.`stream_type`,`ls`.`stream_id`,`ls`.`start_time`,`ls`.`end_time`,`livestream`.`feed_url`,`livestream`.`id` as `live_id`,`movie_streams`.`full_movie`,`movie_streams`.`id` as `film_id` FROM `tvguide_channel_master` as `tcm` JOIN `stream_events` as `se` ON `tcm`.id = `se`.`channel_id` JOIN `ls_schedule` as `ls` ON `se`.`id`=`ls`.`event_id` LEFT JOIN `livestream` ON `ls`.`movie_id` = `livestream`.`id` LEFT JOIN `movie_streams` ON `ls`.`movie_id` = `movie_streams`.`id` WHERE `tcm`.`studio_id`={$studio_id} AND `tcm`.`id`={$channel_id} AND ((`ls`.`start_time` BETWEEN '".$time."' AND '".$timeafter4hour."') OR (`ls`.`end_time` BETWEEN '".$time_after1min."' AND '".$timeafter4hour."'))  ORDER BY `ls`.`start_time` ";
       // $sql = "SELECT `tcm`.`id`,`tcm`.`channel_name`,`tcm`.`channel_description`,`tcm`.`channel_logo`,`tcm`.`studio_id`,`se`.`id` as `event_id`,`se`.`event_name`,`se`.`event_desc`,`ls`.`movie_id`,`ls`.`stream_type`,`ls`.`stream_id`,`ls`.`start_time`,`ls`.`end_time`,`livestream`.`feed_url`,`livestream`.`id` as `live_id`,`movie_streams`.`full_movie`,`movie_streams`.`id` as `film_id` FROM `tvguide_channel_master` as `tcm` JOIN `stream_events` as `se` ON `tcm`.id = `se`.`channel_id` JOIN `ls_schedule` as `ls` ON `se`.`id`=`ls`.`event_id` LEFT JOIN `livestream` ON `ls`.`movie_id` = `livestream`.`id` LEFT JOIN `movie_streams` ON `ls`.`movie_id` = `movie_streams`.`id` WHERE `tcm`.`studio_id`={$studio_id} AND `tcm`.`id`={$channel_id} AND ((`ls`.`start_time` BETWEEN '".$time."' AND '".$timeafter4hour."') OR (`ls`.`end_time` BETWEEN '".$time_after1min."' AND '".$timeafter4hour."'))  ORDER BY `ls`.`start_time` ";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        return $res;
    }
    
    public function findguides($channel_id, $start_time, $end_time = ''){
        $studio_id = Yii::app()->common->getStudiosId();
        $end_time = strtotime($start_time) + 3600*4;
        $end_date = date('Y-d-m H:i:s', $end_time);
        $start_date = date('Y-d-m H:i:s', strtotime($start_time));
        
        $sql = "SELECT `tcm`.`id`,`tcm`.`channel_name`,`tcm`.`channel_description`,`tcm`.`channel_logo`,`tcm`.`studio_id`,`se`.`id` as `event_id`,`se`.`event_name`,`se`.`event_desc`,`ls`.`movie_id`,`ls`.`stream_type`,`ls`.`stream_id`,`ls`.`start_time`,`ls`.`end_time`,`livestream`.`feed_url`,`livestream`.`id` as `live_id`,`movie_streams`.`full_movie`,`movie_streams`.`id` as `film_id` FROM `tvguide_channel_master` as `tcm` JOIN `stream_events` as `se` ON `tcm`.id = `se`.`channel_id` JOIN `ls_schedule` as `ls` ON `se`.`id`=`ls`.`event_id` LEFT JOIN `livestream` ON `ls`.`movie_id` = `livestream`.`id` LEFT JOIN `movie_streams` ON `ls`.`movie_id` = `movie_streams`.`id` WHERE `tcm`.`studio_id`={$studio_id} AND `tcm`.`id`={$channel_id} AND ((`ls`.`start_time` BETWEEN '".$start_date."' AND '".$end_date."') OR (`ls`.`end_time` BETWEEN '".$start_date."' AND '".$end_date."')) ORDER BY `ls`.`start_time` ";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        return $res;        
    }    
}    