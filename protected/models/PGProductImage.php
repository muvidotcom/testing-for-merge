<?php

class PGProductImage extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_product_image';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pg_product' => array(self::BELONGS_TO, 'PGProduct','product_id')
        );
    }
}


