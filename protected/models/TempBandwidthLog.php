<?php
class TempBandwidthLog extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'temp_bandwidth_log';
    }
    public function getTempBandwidthLog($bandwidth_log_id){
        $result_bandwidth_content = $this->findAllByAttributes(array('bandwidth_log_id' => $bandwidth_log_id)); 
        return $result_bandwidth_content;
    }  
    public function updateTempBandwidthLog($bandwidth_log_id,$browser_information,$bandwidth_value){
        $sql = 'UPDATE `temp_bandwidth_log` SET `bandwidth_value`="'.@$bandwidth_value.'",`browser_information`="'.@$browser_information.'" WHERE `bandwidth_log_id`='.@$bandwidth_log_id;
        $data['data'] = Yii::app()->db->createCommand($sql)->execute();
        return $data;
    }  
}
