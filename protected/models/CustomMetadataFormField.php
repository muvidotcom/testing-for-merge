<?php
class CustomMetadataFormField extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'custom_metadata_form_field';
    }
    function insertFormFieldData($custom_form_id,$custom_field_id,$id_seq){
        $this->custom_form_id = $custom_form_id;
        $this->custom_field_id = $custom_field_id;              
        $this->id_seq = $id_seq;
        $this->isNewRecord = TRUE;
        $this->primaryKey = NULL;
        $this->save(); 
}
    /**
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @param object object of film_custom_metadata or movie_stream_custom_metadata or pg_custom_metadata
     * @param index autoincrement value
     * @param custom_field_id custom_field_id
     * @param studio_id studio_id
     * @param custom_metadata_form_id custom_metadata_form_id
     * @Description insert into corresponding table
     * @return null
     */
    function addContentTocustommeta($fcm,$customindex,$custom_field_id,$studio_id,$custom_metadata_form_id) {
        $fcm->field_name = 'custom'.$customindex;
        $fcm->custom_field_id = $custom_field_id;
        $fcm->studio_id = $studio_id;
        $fcm->custom_metadata_form_id = $custom_metadata_form_id;
        $fcm->created_date = gmdate('Y-m-d H:i:s');
        $fcm->isNewRecord = TRUE;
        $fcm->primaryKey = NULL;
        $fcm->save();
    }
}
