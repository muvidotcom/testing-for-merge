<?php
class StudioAds extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'studio_ads';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studiorel'=>array(self::BELONGS_TO, 'Studio', 'studio_id'),
            'adnetwork'=>array(self::HAS_MANY, 'AdNetworks', 'id'),
        );
    }
	 public function getStudioAdsDetails($studio_id) {
		$data = $this->findByAttributes(array('studio_id' => $studio_id))->attributes;
		return $data;
    }
}
