<?php
class PGCart extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'pg_cart';
    }
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function insertCart($studio_id,$userid,$cart_item,$ip){
        $this->studio_id = $studio_id;
        $this->user_id = $userid;
        $this->session_id = session_id();
        $this->created_date = gmdate('Y-m-d H:i:s');//new CDbExpression("NOW()");
        $this->cart_item = json_encode($cart_item);
        $this->ip = $ip;
        $this->save();
    }
    public function getCart($studio_id,$userid){
        $cart_arr = Yii::app()->db->createCommand("SELECT * FROM pg_cart WHERE studio_id = '{$studio_id}' AND user_id = '{$userid}'")->queryROW();
        return $cart_arr;
    }
}
