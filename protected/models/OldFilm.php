<?php

/**
 * This is the model class for table "films".
 *
 * The followings are the available columns in table 'films':
 * @property string $id
 * @property string $name
 * @property string $language
 * @property string $rating
 * @property string $country
 * @property string $genre
 * @property string $permalink
 * @property string $alias_name
 * @property string $story
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $content_type_id
 * @property string $censor_rating
 * @property string $tags
 * @property string $movie_payment_type
 * @property integer $is_verified
 * @property integer $is_merged
 * @property integer $studio_id
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 * @property integer $last_updated_by
 * @property string $last_updated_date
 */
class OldFilm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'old_films';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content_type_id, is_verified, is_merged, studio_id, created_by, last_updated_by', 'numerical', 'integerOnly'=>true),
			array('name, country, censor_rating', 'length', 'max'=>100),
			array('language, rating', 'length', 'max'=>200),
			array('genre', 'length', 'max'=>200),
			array('permalink', 'length', 'max'=>150),
			array('alias_name', 'length', 'max'=>255),
			array('movie_payment_type', 'length', 'max'=>10),
			array('ip', 'length', 'max'=>30),
			array('story, meta_title, meta_description, meta_keywords, tags, created_date, last_updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, language, rating, country, genre, permalink, alias_name, story, meta_title, meta_description, meta_keywords, content_type_id, censor_rating, tags, movie_payment_type, is_verified, is_merged, studio_id, ip, created_by, created_date, last_updated_by, last_updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'casts'=>array(self::MANY_MANY, 'Celebrities','movie_casts(movie_id,celebrity_id)'),
                    'movie_streams' => array(self::HAS_MANY, 'movieStreams','movie_id'),
                    'poster' => array(self::HAS_ONE, 'Poster','object_id',
                    'condition'=>"object_type='films'"),
                    'trailer' => array(self::HAS_ONE, 'movieTrailer','movie_id'),
                    'studio_content_type' => array(self::BELONGS_TO, 'StudioContentType', 'content_type_id'),                
		);
	}
        public function behaviors() {
            return array(
                'commentable' => array(
                    'class' => 'ext.comment-module.behaviors.CommentableBehavior',
                    // name of the table created in last step
                    'mapTable' => 'movies_comments_nm',
                    // name of column to related model id in mapTable
                    'mapRelatedColumn' => 'movieId'
                ),
			'sluggable' => array(
				'class'=>'ext.behaviors.SluggableBehavior',
				'columns' => array('name'),
				'unique' => true,
				'update' => true
            ),
           );
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'language' => 'Language',
			'rating' => 'Rating',
			'country' => 'Country',
			'genre' => 'Genre',
			'permalink' => 'Permalink',
			'alias_name' => 'Alias Name',
			'story' => 'Story',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'content_type_id' => 'Content Type',
			'censor_rating' => 'Censor Rating',
			'tags' => 'Tags',
			'movie_payment_type' => 'Movie Payment Type',
			'is_verified' => 'Is Verified',
			'is_merged' => 'Is Merged',
			'studio_id' => 'Studio',
			'ip' => 'Ip',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'last_updated_by' => 'Last Updated By',
			'last_updated_date' => 'Last Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('rating',$this->rating,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('genre',$this->genre,true);
		$criteria->compare('permalink',$this->permalink,true);
		$criteria->compare('alias_name',$this->alias_name,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('content_type_id',$this->content_type_id);
		$criteria->compare('censor_rating',$this->censor_rating,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('movie_payment_type',$this->movie_payment_type,true);
		$criteria->compare('is_verified',$this->is_verified);
		$criteria->compare('is_merged',$this->is_merged);
		$criteria->compare('studio_id',$this->studio_id);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_updated_by',$this->last_updated_by);
		$criteria->compare('last_updated_date',$this->last_updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Films the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
