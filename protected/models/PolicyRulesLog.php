<?php

class PolicyRulesLog extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'policy_rules_log';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'policy_rules' => array(self::BELONGS_TO, 'PolicyRules', 'rules_id')
        );
    }

}
