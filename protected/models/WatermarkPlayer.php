<?php
class WatermarkPlayer extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'watermark_player';
    }
    
    public function getStudioId($studio_id) {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id = :studio_id', array(':studio_id' => $studio_id))
                ->queryRow();
        return $data;
    }
    
}    

