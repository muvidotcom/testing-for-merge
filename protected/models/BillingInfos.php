<?php

class BillingInfos extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'billing_infos';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    
    public function getBillingInfos($studio_id,$user_id,$offset,$limit)
    {
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM billing_infos WHERE studio_id = ".$studio_id." ORDER BY id DESC LIMIT ".$offset.",".$limit;
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getBillingDetails($id)
    {
        $sql = "SELECT * FROM billing_details WHERE billing_info_id = ".$id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
      public function get_studio_billing_info($studio_id){
        $lastmondate = date('Y-m', strtotime("first day of last month"));
         $sql = Yii::app()->db->createCommand()
               ->select("t.id,t.billing_info_id,t.studio_id,t.billing_amount,t.paid_amount")
               ->from("transaction_infos t")
               ->join("billing_infos b","t.billing_info_id = b.id")
               ->where("t.is_success =:issuccess and b.transaction_type = :trans_type and b.is_paid=:is_paid and b.is_refund =:isrefaund and DATE_FORMAT(t.created_date,'%Y-%m')= :last_month and t.studio_id =:studio_id",array(':issuccess'=>1,':trans_type'=>1,':is_paid'=>2,':isrefaund'=>0,':last_month'=>$lastmondate,':studio_id'=>$studio_id))
               ->queryAll();
        return $sql;
    }   

}