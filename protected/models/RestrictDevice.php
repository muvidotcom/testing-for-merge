<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RestrictDevice extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'restrict_device';
    }

    
    public function getNoOfUserActive($studio_id, $user_id){
        $countval = $this->countByAttributes(array('studio_id' => $studio_id,'user_id' => $user_id));
        return $countval;
    }
    
    public function dataSave($data){
        if($data){
            if($data['id'] != 0){
                $restrictDeviceLog = RestrictDeviceLog::model()->findByPk($data['id']);
                if($restrictDeviceLog){
                    $restrictDeviceLog->last_update = date("Y-m-d H:i:s");
                    $restrictDeviceLog->save();
                }
                
                $restrictDevice = $this->findByPk($data['id']);
                if($restrictDevice){
                    $restrictDevice->last_update = date("Y-m-d H:i:s");
                    $restrictDevice->save();
                    return $restrictDevice->id;
                }
            } else{
                $restrictDeviceLog = new RestrictDeviceLog();
                $restrictDeviceLog->studio_id = $data['studio_id'];
                $restrictDeviceLog->user_id = $data['user_id'];
                $restrictDeviceLog->movie_stream_id = $data['movie_stream_id'];
                $restrictDeviceLog->last_update = date("Y-m-d H:i:s");
                $restrictDeviceLog->save();
                
                $restrictDevice = new RestrictDevice();
                $restrictDevice->studio_id = $data['studio_id'];
                $restrictDevice->user_id = $data['user_id'];
                $restrictDevice->movie_stream_id = $data['movie_stream_id'];
                $restrictDevice->last_update = date("Y-m-d H:i:s");
                $restrictDevice->save();
                return $restrictDevice->id;
            }
        }
    }
    
    public function deleteDataInCron(){
        $this->deleteAll(array('condition' => 'DATE_ADD(last_update ,INTERVAL 2 MINUTE) < NOW()'));
    }
    
    public function deleteData($id){
        $this->deleteByPk($id);
    }
}
?>