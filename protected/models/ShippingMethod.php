<?php
class ShippingMethod extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'pg_shipping_method';
    }
}