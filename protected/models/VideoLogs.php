<?php
class VideoLogs extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'video_logs';
    }
    
    public function getTotalWatchedHour($studio_id,$movie_id = '',$video_id = 0,$dt,$is_partner = 0,$searchKey,$deviceType)
    {
        $cond = "";
        if(isset($video_id) && $video_id){
            $cond = " AND v.video_id = ".$video_id;
        }
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $qstr = '';
        
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        } else {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            }
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND ((f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%') OR (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%'))";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND v.device_type=".$deviceType;
        }
        //$sql = "SELECT SUM(played_length) AS watched_hour FROM video_logs v,films f,movie_streams ms, sdk_users u WHERE v.studio_id = ".$studio_id." ".$qstr.$cond." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id))) AND v.user_id = u.id ".$searchStr.$deviceStr." GROUP BY v.studio_id";
        $sql = "SELECT SUM(played_length) AS watched_hour FROM video_logs v,films f,movie_streams ms, sdk_users u WHERE v.studio_id = ".$studio_id." ".$qstr.$cond." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND v.movie_id = f.id AND v.video_id=ms.id AND v.user_id = u.id ".$searchStr.$deviceStr." GROUP BY v.studio_id";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    public function getTotalBandwidth($studio_id,$dt,$movie_id = '',$is_partner = 0,$searchKey,$deviceType)
    {
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND bbl.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%' OR bbl.country LIKE '%".$searchKey."%')";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bbl.device_type=".$deviceType;
        }
        //$sql = "SELECT SUM(total_buffered_size) AS buffered_size FROM bandwidth_buffer_log bbl,films f,movie_streams ms WHERE bbl.studio_id = ".$studio_id." AND (DATE_FORMAT(bbl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$qstr." AND (bbl.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (bbl.video_id=ms.id OR (bbl.video_id=0 OR ISNULL(bbl.video_id))) ".$searchStr.$deviceStr." GROUP BY bbl.studio_id";
        $sql = "SELECT SUM(buffer_size) AS buffered_size FROM bandwidth_log bbl,films f,movie_streams ms WHERE bbl.studio_id = ".$studio_id." AND (DATE_FORMAT(bbl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') ".$qstr." AND bbl.movie_id = f.id AND bbl.video_id=ms.id ".$searchStr.$deviceStr." GROUP BY bbl.studio_id";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function getAllBandwidth($studio_id,$movie_id = '',$is_partner = 0)
    {
        $data = array();
        $qstr = '';
        $limit = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND movie_id IN ('.$movie_id.') ';
            } else {
                return $data;
            }
        }
        //$sql = "SELECT DATE_FORMAT(created_date,'%Y-%m') AS created_date, SUM(total_buffered_size) AS total_buffered_size FROM bandwidth_buffer_log WHERE studio_id = ".$studio_id.$qstr." GROUP BY DATE_FORMAT(created_date,'%Y-%m')";
        $sql = "SELECT DATE_FORMAT(created_date,'%Y-%m') AS created_date, SUM(buffer_size) AS total_buffered_size FROM bandwidth_log WHERE studio_id = ".$studio_id.$qstr." GROUP BY 1";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function getAllBandwidthMovie($studio_id,$movie_id,$video_id = 0,$searchKey,$deviceType)
    {
        $cond = "";
        if(isset($video_id) && $video_id){
            $cond = " AND bbl.video_id=".$video_id;
        }
        $mcond = "";
        if(isset($movie_id) && trim($movie_id)){
            $mcond = " AND bbl.movie_id IN (".$movie_id.")";
        }else{
            $mcond = " AND bbl.movie_id IN (-1)";
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND bbl.device_type=".$deviceType;
        }
        //$sql = "SELECT *,bbl.created_date as created_date FROM bandwidth_buffer_log bbl,films f,movie_streams ms WHERE bbl.studio_id = ".$studio_id.$mcond.$cond." AND (bbl.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (bbl.video_id=ms.id OR (bbl.video_id=0 OR ISNULL(bbl.video_id))) ".$searchStr.$deviceStr;
        //$sql = "SELECT SUM(total_buffered_size) as total_buffered_size, bbl.created_date, DATE_FORMAT(bbl.created_date, '%Y-%m'),f.created_date as movie_date FROM bandwidth_buffer_log bbl LEFT JOIN films f ON (f.id = bbl.movie_id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams ms ON (bbl.video_id = ms.id) WHERE bbl.studio_id = ".$studio_id.$mcond.$cond." AND (bbl.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (bbl.video_id=ms.id) ".$searchStr.$deviceStr." GROUP BY (DATE_FORMAT(bbl.created_date, '%Y-%m')) ORDER BY bbl.created_date DESC";
        $sql = "SELECT SUM(buffer_size) as total_buffered_size, bbl.created_date, DATE_FORMAT(bbl.created_date, '%Y-%m'),f.created_date as movie_date FROM bandwidth_log bbl LEFT JOIN films f ON f.id = bbl.movie_id LEFT JOIN movie_streams ms ON (bbl.video_id = ms.id) WHERE bbl.studio_id = ".$studio_id.$mcond.$cond." AND bbl.movie_id = f.id AND (bbl.video_id=ms.id) ".$searchStr.$deviceStr." GROUP BY (DATE_FORMAT(bbl.created_date, '%Y-%m')) ORDER BY bbl.created_date DESC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    public function getAllWatchedHour($studio_id,$movie_id = '',$is_partner = 0)
    {
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        $sql = "SELECT DATE_FORMAT(created_date,'%Y-%m') AS created_date, SUM(played_length) AS played_length FROM video_logs WHERE studio_id = ".$studio_id.$qstr." GROUP BY DATE_FORMAT(created_date,'%Y-%m')";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function getNetWatchedHour($studio_id,$dt,$movie_id = '',$is_partner = 0,$searchKey,$deviceType)
    {
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND v.device_type=".$deviceType;
        }
        $sql = "SELECT SUM(v.played_length) AS watched_hour FROM video_logs v WHERE v.studio_id = ".$studio_id.$qstr.$deviceStr." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "')";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        return $data;
    }
    
    public function getAllWatchedHourMovie($studio_id,$movie_id,$video_id = 0,$searchKey,$deviceType)
    {
        $cond = "";
        $mcond = "";
        if(isset($video_id) && $video_id){
            $cond = " AND v.video_id=".$video_id;
        }
        if(isset($movie_id) && trim($movie_id)){
            $mcond = " AND v.movie_id IN (".$movie_id.")";
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND v.device_type=".$deviceType;
        }
        $sql = "SELECT *,v.created_date as created_date FROM video_logs v,films f,movie_streams ms WHERE v.studio_id = ".$studio_id.$mcond.$cond." AND (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id))) ".$searchStr.$deviceStr;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    
    public function getViewsDetails($dt,$offset,$page_size,$movie_id='',$is_partner = 0,$searchKey = '',$deviceType)
    {
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        if(trim($searchKey)){
            $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND v.device_type=".$deviceType;
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS SUM(v.played_length) AS played_length,count(v.id) as viewcount, count(distinct v.user_id) as u_viewcount,f.id,f.name,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,f.id as movie_id,ms.id as video_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams ms ON (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id)))  WHERE (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.studio_id = ".Yii::app()->common->getStudiosId()." GROUP BY v.movie_id, v.video_id ORDER BY f.name LIMIT ".$offset.",".$page_size;
        //$sql = "SELECT SQL_CALC_FOUND_ROWS SUM(v.played_length) AS played_length,SUM(bl.buffer_size) AS bandwidth,count(v.id) as viewcount,bl.device_type,bl.location,count(v.id) as viewcount, count(distinct v.user_id) as u_viewcount,f.id,f.name,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,f.id as movie_id,ms.id as video_id FROM video_logs v LEFT JOIN bandwidth_log bl ON (v.studio_id = bl.studio_id AND v.movie_id = bl.movie_id AND v.video_id = bl.video_id) LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams ms ON (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id)))  WHERE (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.studio_id = ".Yii::app()->common->getStudiosId()." GROUP BY v.movie_id, v.video_id ORDER BY f.name, bl.location,bl.device_type DESC LIMIT ".$offset.",".$page_size;
        //$sql = "SELECT SQL_CALC_FOUND_ROWS SUM(v.played_length) AS played_length,SUM(bl.buffer_size) AS bandwidth,count(v.id) as viewcount,bl.device_type,bl.location,count(v.id) as viewcount, count(distinct v.user_id) as u_viewcount,f.id,f.name,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,f.id as movie_id,ms.id as video_id FROM video_logs v LEFT JOIN bandwidth_log bl ON (v.studio_id = bl.studio_id AND v.movie_id = bl.movie_id AND v.video_id = bl.video_id AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')) LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams ms ON (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id)))  WHERE (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.studio_id = ".Yii::app()->common->getStudiosId()." GROUP BY v.movie_id, v.video_id ORDER BY f.name, bl.location,bl.device_type DESC LIMIT ".$offset.",".$page_size;
        $results['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $results;
    }
    
    public function getSearchedViewsDetails($key,$movie_id = '',$is_partner = 0)
    {
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        $results['data'] = Yii::app()->db->createCommand()
            ->select('count(v.id) as viewcount,count(distinct v.user_id) as u_viewcount,f.id,f.name,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,f.id as movie_id,ms.id as video_id')
            ->from('films f ,movie_streams ms,video_logs v ')
            ->where('v.movie_id=f.id AND  f.studio_id = ' . Yii::app()->common->getStudiosId() . " AND (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id))) AND f.name LIKE '%".$key."%' ".$qstr)
            ->order('f.name ASC')
            ->group('v.movie_id,v.video_id')
            ->queryAll();
        return $results;
    }
    
    public function getViewsDetailsReport($dt,$movie_id='',$is_partner = 0,$searchKey,$deviceType)
    {
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $dt = stripcslashes($dt);
            $dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        
        $qstr = '';
        if (intval($is_partner)) {
            if(trim($movie_id)){
                $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
            } else {
                return array();
            }
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
        }
        $deviceStr = '';
        if(trim($deviceType)){
            $deviceStr = " AND v.device_type=".$deviceType;
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS SUM(v.played_length) AS played_length,count(v.id) as viewcount, count(distinct v.user_id) as u_viewcount,f.id,f.name,ms.id AS stream_id,f.content_types_id, ms.episode_title,ms.episode_number,f.id as movie_id,ms.id as video_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams ms ON (v.video_id=ms.id OR (v.video_id=0 OR ISNULL(v.video_id))) WHERE (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.studio_id = ".Yii::app()->common->getStudiosId()." GROUP BY v.movie_id, v.video_id ORDER BY f.name";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        return $results;
    }
/**
 * @method protected getviewStatus(String $movieids) It will return the view status for that particular content
 * @author Gayadhar<support@muvi.com>
 * @return array Array of report
 */	
	function getViewStatus($movie_ids,$studio_id){
		if($studio_id && $movie_ids){
			$sql = "SELECT count(id) as viewcount, count(distinct user_id) as u_viewcount,movie_id,episode_id FROM video_logs WHERE studio_id=".$studio_id." AND movie_id IN(".$movie_ids.") GROUP BY movie_id ";
			$results = Yii::app()->db->createCommand($sql)->queryAll();
			return $results;
		}else{
			return FALSE;
		}
	}
/**
 * @method protected getepisodeViewStatus(String $movieids) It will return the view status for that particular content
 * @author Gayadhar<support@muvi.com>
 * @return array Array of report
 */	
	function getEpisodeViewStatus($episode_id,$studio_id){
		if($studio_id && $episode_id){
			$sql = "SELECT count(id) as viewcount, count(distinct user_id) as u_viewcount,movie_id,video_id FROM video_logs WHERE studio_id=".$studio_id." AND movie_id IN(".$episode_id.") GROUP BY video_id ";
			$results = Yii::app()->db->createCommand($sql)->queryAll();
			return $results;
		}else{
			return FALSE;
		}
	}
        
        function getVideoAllViewDetails($dt,$offset,$page_size,$movie_id='',$is_partner = 0,$searchKey = '',$deviceType) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
        
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
            if(trim($searchKey)){
                //$searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
                $searchStr = " AND (f.name LIKE '%".$searchKey."%')";
            }
            $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }
            //$sql = "SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." GROUP BY v.movie_id LIMIT ".$offset.",".$page_size;
            $sql="SELECT SQL_CALC_FOUND_ROWS COUNT(v.id) as viewcount,v.content_type,v.video_id as episod_id,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id,m.episode_title,m.episode_number,m.series_number,v.trailer_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE  v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr."  GROUP BY CASE WHEN v.video_id!=0 THEN v.video_id ELSE v.trailer_id END LIMIT ".$offset.",".$page_size;
           //$sql="SELECT SQL_CALC_FOUND_ROWS COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id,m.episode_title,m.episode_number,m.series_number FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE  v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND  v.video_id!=0  GROUP BY v.video_id LIMIT ".$offset.",".$page_size;
            $results['data'] = Yii::app()->db->createCommand($sql)->queryAll();
           $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            
            return $results;
        }
        function getVideoViewDetails($dt,$offset,$page_size,$movie_id='',$is_partner = 0,$searchKey = '',$deviceType) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
        
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
            if(trim($searchKey)){
                //$searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
                $searchStr = " AND (f.name LIKE '%".$searchKey."%')";
            }
            $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }
            //$sql = "SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.content_type = 1 GROUP BY v.movie_id LIMIT ".$offset.",".$page_size;
           $sql="SELECT SQL_CALC_FOUND_ROWS COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.content_type = 1 GROUP BY v.video_id LIMIT ".$offset.",".$page_size;
            $results['data'] = Yii::app()->db->createCommand($sql)->queryAll();
           $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            return $results;
        }
        
        function getVideoBandwidthDetails($dt,$movie_id,$deviceType='',$contentTypes,$bandwidth_type = 1) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $searchStr = '';
            if(trim($searchKey)){
                $searchStr = " AND (bl.country LIKE '%".$searchKey."%')";
            }
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND bl.movie_id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
              $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND bl.device_type=".$deviceType;
            }
            $sql = "SELECT SUM(bl.buffer_size) AS bandwidth,bl.city, bl.region, bl.country,movie_id FROM bandwidth_log bl where DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND bl.studio_id = ".Yii::app()->common->getStudiosId().$searchStr.$qstr." AND bl.movie_id=".$movie_id. $deviceStr ." AND content_type=".$contentTypes." AND bl.bandwidth_type = ".$bandwidth_type." GROUP BY bl.movie_id";
            $results = Yii::app()->db->createCommand($sql)->queryRow();
            return $results;
        }
        
           function getVideoBandwidthDetailsvideo($dt,$video_id,$deviceType='',$contentTypes,$movie_id,$bandwidth_type) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $searchStr = '';
            if(trim($searchKey)){
                $searchStr = " AND (bl.country LIKE '%".$searchKey."%')";
            }
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND bl.video_id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
             $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND bl.device_type=".$deviceType;
            }
           
             $sql = "SELECT SUM(bl.buffer_size) AS bandwidth,bl.city, bl.region, bl.country,movie_id FROM bandwidth_log bl where DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND bl.studio_id = ".Yii::app()->common->getStudiosId().$searchStr.$qstr." AND (CASE WHEN ".$video_id ."!=0 THEN bl.video_id=".$video_id ." ELSE bl.movie_id=".$movie_id." END )". $deviceStr ." AND content_type=".$contentTypes." AND bl.bandwidth_type = ".$bandwidth_type." GROUP BY bl.video_id";
            $results = Yii::app()->db->createCommand($sql)->queryRow();
            return $results;
        }
        
        
        function getPpvTransactionDetails($dt,$currency_id,$studio_id,$video_id){
          
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
          
            //$sql = "SELECT COUNT(t.id) as transactioncount,SUM(t.amount) as amount,movie_id,currency_id FROM transactions t WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." GROUP BY movie_id LIMIT ".$offset.",".$page_size;
           $sql= " select count(t.id) as transactioncount , SUM(t.amount) as transactionamt  from transactions t left join ppv_subscriptions p on (p.id=t.ppv_subscription_id and p.movie_id=t.movie_id) where DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND  p.studio_id=".$studio_id." AND p.episode_id=".$video_id." AND t.currency_id = ".$currency_id." GROUP BY p.movie_id";
            //$transaction_data= Yii::app()->db->createCommand($sql)->queryRow();
            $results = Yii::app()->db->createCommand($sql)->queryRow();
          return $results;
        }
        
         function getPpvTransactionDetailsmovie($dt,$currency_id,$studio_id,$movie_id){
          
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            //$sql = "SELECT COUNT(t.id) as transactioncount,SUM(t.amount) as amount,movie_id,currency_id FROM transactions t WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." GROUP BY movie_id LIMIT ".$offset.",".$page_size;
            $sql= "select count(t.id) as transactioncount , SUM(t.amount) as transactionamt  from transactions t  where  DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id=".$studio_id." AND t.movie_id=".$movie_id."  AND t.currency_id = ".$currency_id." GROUP BY t.movie_id;";
         //$transaction_data= Yii::app()->db->createCommand($sql)->queryRow();
            $results = Yii::app()->db->createCommand($sql)->queryRow();
          return $results;
        }
        
        
        
    /***** Play video from where paused *****/
    function getvideoDurationPlayed($studio_id, $user_id, $movie_id, $stream_id) {
		$sql = "SELECT resume_time,played_length,played_percent,watch_status FROM video_logs WHERE movie_id = " . $movie_id . " AND studio_id = " . $studio_id . " AND user_id = " . $user_id . " AND video_id = " . $stream_id . " ORDER BY id DESC LIMIT 0,1";
		$record = Yii::app()->db->createCommand($sql)->queryRow();
		if (@$record['watch_status'] != 'complete') {
			@$record['played_length'] = @$record['resume_time'];
			if (@$record['played_length'] > 0 && @$record['played_percent'] == 0) {
				$movieStreamData = Yii::app()->db->createCommand()->select('video_duration')->from('movie_streams')->where('id=' . $stream_id)->queryRow();
				if (@$movieStreamData['video_duration']) {
					$duration = explode(":", $movieStreamData['video_duration']);
					$duration = 3600 * $duration[0] + 60 * $duration[1] + $duration[2];
					$record['played_percent'] = ($record['played_length'] * 100) / $duration;
				}
			}
			return $record;
		} else {
			return array();
		}
	}

	function getVideoViewDetailsReport($dt,$movie_id='',$is_partner = 0,$searchKey = '',$deviceType) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $dt = stripcslashes($dt);
                $dt = json_decode($dt);
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND v.movie_id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
            if(trim($searchKey)){
                //$searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
                $searchStr = " AND (f.name LIKE '%".$searchKey."%')";
            }
            $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }
           // $sql = "SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." GROUP BY v.movie_id";
             //$sql="SELECT SQL_CALC_FOUND_ROWS COUNT(v.id) as viewcount,v.content_type,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id,m.episode_title,m.episode_number,m.series_number FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE  v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr."  GROUP BY CASE WHEN v.video_id!=0 THEN v.video_id ELSE v.trailer_id END LIMIT ".$offset.",".$page_size;

            $sql = "SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.content_type,v.movie_id,v.video_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id,m.episode_title,m.episode_number,m.series_number,v.trailer_id,v.trailer_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." GROUP BY CASE WHEN v.video_id!=0 THEN v.video_id ELSE v.trailer_id END ORDER BY f.name";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            return $results;
        }
        
        function getVideoBandwidthDetailsReport($dt) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $dt = stripcslashes($dt);
                $dt = json_decode($dt);
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
        
            $sql = "SELECT SUM(bl.buffer_size) AS bandwidth,bl.city, bl.region, bl.country,movie_id FROM bandwidth_log bl where DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND bl.studio_id = ".Yii::app()->common->getStudiosId()." GROUP BY bl.movie_id";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            return $results;
        }
        
        function getPpvTransactionDetailsReport($dt,$currency_id){
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $dt = stripcslashes($dt);
                $dt = json_decode($dt);
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $sql = "SELECT COUNT(t.id) as transactioncount,SUM(t.amount) as amount,movie_id,currency_id FROM transactions t WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." GROUP BY movie_id";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            return $results;
        }
        
        function getAllContent($studio_id, $offset, $page_size, $searchKey = '', $movie_id = '', $is_partner = 0)
        {
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND f.id IN ('.$movie_id.') ';
                } else {
                    return array();
                }
            }
            if(trim($searchKey)){
                $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
            }
            //$sql = "SELECT SQL_CALC_FOUND_ROWS f.id as movie_id,f.name,f.content_types_id,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = ".$studio_id." AND f.id = ms.movie_id ".$searchStr.$qstr. " GROUP BY ms.movie_id ORDER BY f.name DESC LIMIT ".$offset.",".$page_size;
            $sql = "SELECT SQL_CALC_FOUND_ROWS f.id as movie_id,f.name,f.content_types_id,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = ".$studio_id." AND f.id = ms.movie_id ".$searchStr.$qstr. " ORDER BY f.name DESC LIMIT ".$offset.",".$page_size;
            $results['data'] = Yii::app()->db->createCommand($sql)->queryAll();
            $results['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            return $results;
        }
        
        function getPpvContentTransactionDetails($dt,$currency_id,$movie_id = '',$season = 0,$episode = 0,$is_partner = 0){
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $movie_str = '';
            if(intval($is_partner)){
                if(trim($movie_id)){
                    $movie_str = " AND ps.movie_id IN (".$movie_id.")";
                }else{
                    return array();
                }
            }else{
                if(trim($movie_id)){
                    $movie_str = " AND ps.movie_id IN (".$movie_id.")";
                }
            }
        $sql= "select count(t.id) as transactioncount , SUM(t.amount) as amount  from transactions t  where  DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id=".Yii::app()->common->getStudiosId()." AND t.movie_id=".$movie_id." AND t.currency_id = ".$currency_id." GROUP BY t.movie_id;";
        //$sql = "SELECT COUNT(t.id) as transactioncount,SUM(t.amount) as amount,ps.movie_id,ps.currency_id FROM transactions t, ppv_subscriptions ps WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." AND t.ppv_subscription_id = ps.id AND ps.season_id =".$season." AND ps.episode_id =".$episode.$movie_str." GROUP BY movie_id";
         $type_sql = "SELECT t.movie_id,t.currency_id,ps.is_advance_purchase FROM transactions t,ppv_subscriptions ps WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." AND ps.id = t.ppv_subscription_id AND ps.season_id =".$season." AND ps.episode_id =".$episode.$movie_str;
           $results['count'] = Yii::app()->db->createCommand($sql)->queryRow();
           $results['type'] = Yii::app()->db->createCommand($type_sql)->queryAll();
            return $results;
        }
        
        
                function getPpvContentTransactionDetailsContent($dt,$currency_id,$movie_id = '',$season = 0,$episode = 0,$is_partner = 0){
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
            $movie_str = '';
            if(intval($is_partner)){
                if(trim($movie_id)){
                    $movie_str = " AND ps.movie_id IN (".$movie_id.")";
                }else{
                    return array();
                }
            }else{
                if(trim($movie_id)){
                    $movie_str = " AND ps.movie_id IN (".$movie_id.")";
                }
            }
       $sql= " select count(t.id) as transactioncount , SUM(t.amount) as amount  from transactions t left join ppv_subscriptions p on (p.id=t.ppv_subscription_id and p.movie_id=t.movie_id) where DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND  p.studio_id=".Yii::app()->common->getStudiosId()." AND p.episode_id=".$episode." AND t.currency_id = ".$currency_id." GROUP BY p.movie_id;";
        //$sql= "select count(t.id) as transactioncount , SUM(t.amount) as amount  from transactions t  where  DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id=".Yii::app()->common->getStudiosId()." AND t.movie_id=".$movie_id." GROUP BY t.movie_id;";
        //$sql = "SELECT COUNT(t.id) as transactioncount,SUM(t.amount) as amount,ps.movie_id,ps.currency_id FROM transactions t, ppv_subscriptions ps WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." AND t.ppv_subscription_id = ps.id AND ps.season_id =".$season." AND ps.episode_id =".$episode.$movie_str." GROUP BY movie_id";
         $type_sql = "SELECT t.movie_id,t.currency_id,ps.is_advance_purchase FROM transactions t,ppv_subscriptions ps WHERE DATE_FORMAT(t.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."' AND t.studio_id = ".Yii::app()->common->getStudiosId()." AND t.currency_id = ".$currency_id." AND ps.id = t.ppv_subscription_id AND ps.season_id =".$season." AND ps.episode_id =".$episode.$movie_str;
           $results['count'] = Yii::app()->db->createCommand($sql)->queryRow();
           $results['type'] = Yii::app()->db->createCommand($type_sql)->queryAll();
            return $results;
        }

        
        
        

        
        function getContentData($studio_id,$searchKey = '',$movie_id = '',$offset = 0,$page_size = 0,$is_partner = 0)
        {
            $searchStr = '';
            if(trim($searchKey)){
                $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
            }
            $movie_str = '';
            if(intval($is_partner)){
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }else{
                    return array();
                }
            }else{
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }
            }
            $limit = '';
            if(intval($page_size)){
                $limit = " LIMIT ".$offset.",".$page_size;
            }
            $sql = "SELECT SQL_CALC_FOUND_ROWS f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = " . $studio_id . $searchStr . $movie_str . " AND f.id = ms.movie_id GROUP BY movie_id ORDER BY f.name DESC".$limit;
           
            $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
            $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            return $data;
        }
        
        function getSeasonData($studio_id,$searchKey = '',$movie_id = '',$offset = 0,$page_size = 0,$is_partner = 0)
        {
            $searchStr = '';
            if(trim($searchKey)){
                $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
            }
            
            $movie_str = '';
            if(intval($is_partner)){
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }else{
                    return array();
                }
            }else{
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }
            }
            $limit = '';
            if(intval($page_size)){
                $limit = " LIMIT ".$offset.",".$page_size;
            }
            $sql = "SELECT SQL_CALC_FOUND_ROWS f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = " . $studio_id . $searchStr . $movie_str . " AND f.id = ms.movie_id AND f.content_types_id = 3 AND (series_number <> 0 OR series_number IS NOT NULL) GROUP BY series_number, movie_id ORDER BY f.name DESC".$limit;
            $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
            $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            return $data;
        }
        
        function getEpisodeData($studio_id,$searchKey = '',$movie_id = '',$offset = 0,$page_size = 0,$is_partner = 0)
        {
            $searchStr = '';
            if(trim($searchKey)){
                $searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
            }
            
            $movie_str = '';
            if(intval($is_partner)){
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }else{
                    return array();
                }
            }else{
                if(trim($movie_id)){
                    $movie_str = " AND f.id IN (".$movie_id.")";
                }
            }
            $limit = '';
            if(intval($page_size)){
                $limit = " LIMIT ".$offset.",".$page_size;
            }
            $sql = "SELECT SQL_CALC_FOUND_ROWS f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = " . $studio_id . $searchStr . $movie_str . " AND f.id = ms.movie_id AND f.content_types_id = 3 AND (series_number <> 0 OR series_number IS NOT NULL) ORDER BY f.name DESC".$limit;
            $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
            $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            return $data;
        }
         public function getAllViewHistory($studio_id) {
        $sql = "SELECT created_date FROM ".$this->tableName()." WHERE studio_id=".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    function videoPriceDetails($movie_id,$ppv_plan_id,$currency_id)
    {
        $sql = "SELECT p.is_advance_purchase,pr.price_for_subscribed,pr.price_for_unsubscribed FROM ppv_plans p LEFT JOIN ppv_pricing pr on p.id=pr.ppv_plan_id  WHERE pr.currency_id=".$currency_id." AND pr.ppv_plan_id in (".$ppv_plan_id.")  AND pr.status=1 AND p.is_deleted=0 AND p.status=1 ORDER BY p.last_updated_date DESC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
     return $data;
    }
    function videoPriceDetailsMultipart($movie_id,$ppv_plan_id,$currency_id)
    {
        $sql = "SELECT p.is_advance_purchase,pr.episode_unsubscribed,pr.episode_subscribed FROM ppv_plans p LEFT JOIN ppv_pricing pr on p.id=pr.ppv_plan_id  WHERE pr.currency_id=".$currency_id." AND pr.ppv_plan_id in(".$ppv_plan_id.") AND pr.status=1 AND p.is_deleted=0 AND p.status=1 ORDER BY p.last_updated_date DESC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
     return $data;
    }
    function videoPriceDetailsadvance($movie_id,$currency_id)
    {
        $sql = "SELECT p.is_advance_purchase,pr.price_for_subscribed,pr.price_for_unsubscribed FROM films f LEFT JOIN ppv_advance_content pdv on f.id=pdv.content_id LEFT JOIN ppv_pricing pr on pdv.ppv_plan_id=pr.ppv_plan_id LEFT JOIN ppv_plans p on p.id=pdv.ppv_plan_id WHERE pr.currency_id=".$currency_id." AND f.id=".$movie_id." AND pr.status=1 AND p.is_deleted=0 AND p.status=1 ORDER BY p.last_updated_date DESC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
     return $data;
    }

	function getTotalViewsForStudio($studio_id) {
        $start_date = Date('Y-m-d', strtotime("-1 Months +1 Days"));
        $end_date = Date('Y-m-d');
        
        $sql = "SELECT count(*) AS total_views FROM video_logs WHERE studio_id = {$studio_id} AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "')";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        
        return $data;
    }
    
    function getTrailerdata($dt,$movie_id,$studio_id,$deviceType='')
    {
         if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
            }
             $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }           
    $sql =  "SELECT  COUNT(v.id) as viewcount FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = '".$studio_id."' AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') AND v.movie_id='". $movie_id."' And v.trailer_id!='' AND v.video_id=0".$deviceStr ;  
     $data = Yii::app()->db->createCommand($sql)->queryRow(); 
        return $data;
     }
     function getOnlytTrailerView($studio_id,$movie_id){
        $sql =  "SELECT  COUNT(v.id) as onlytrailer FROM video_logs v WHERE v.studio_id = '".$studio_id."' AND v.movie_id='".$movie_id."' AND v.video_id=0" ;  
       $data = Yii::app()->db->createCommand($sql)->queryRow(); 
        return $data['onlytrailer'];  
     }
     
    function getFirstWatchedDateAndTimeOfContent($arg = array()) {
        $first_watched_date = '';
        
        if (isset($arg) && !empty($arg)) {
            $studio_id = @$arg['studio_id'];
            $user_id = @$arg['user_id'];
            $movie_id = @$arg['movie_id'];
            $stream_id = @$arg['stream_id'];

            $purchased_date = date("Y-m-d H:i:s", strtotime($arg['created_date']));
            $todayDate = date("Y-m-d H:i:s");
            $connection = Yii::app()->db;
            $cond = "studio_id= {$studio_id} AND user_id = {$user_id} AND movie_id = {$movie_id} AND video_id = {$stream_id} AND DATE_FORMAT(created_date,'%Y-%m-%d %H:%i:%s') >= '".$purchased_date."' AND DATE_FORMAT(created_date,'%Y-%m-%d %H:%i:%s') <= '".$todayDate."'";
            $sql = "SELECT MIN(created_date) AS created_date FROM video_logs WHERE {$cond}";//Get first watch date and time
            $log_obj = $connection->createCommand($sql);
            $log_data = $log_obj->queryRow();
            
            $first_watched_date = strtotime(@$log_data['created_date']);
}
        
        return $first_watched_date;
    }
	 function getWatchHistoryList($user_id, $studio_id = NULL,$page_size=NULL,$offset=NULL,$duration,$language_id){
        $list = array();
        $count = 0;
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudioId();
		}
        $start_date = date('Y-m-d', strtotime('-'.$duration.' days'));
        $end_date = Date('Y-m-d');
        $sql = 'SELECT SQL_CALC_FOUND_ROWS DISTINCT movie_id,video_id FROM video_logs WHERE user_id = '.$user_id.' AND studio_id='.$studio_id.' AND played_length > 0 AND (DATE_FORMAT(created_date,"%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") ORDER BY id desc';
        if($page_size != NULL){
            $sql .= ' LIMIT '.$page_size.' OFFSET '.$offset;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        if(!empty($data)){
            foreach($data as $fav){
                 if(Film::model()->exists('id=:id',array(':id' => $fav['movie_id']))){
                    $is_episode = movieStreams::model()->find(array('select' => 'is_episode', 'condition' => 'id=:id', 'params' => array(':id' => $fav['video_id'])))->is_episode;
					if ($is_episode == '1') {
						$content_id = $fav['video_id'];
					} else {
						$content_id = $fav['movie_id'];
					}
					$customData = array();
					$list[] = Yii::app()->general->getContentData($content_id, $is_episode,$customData,$language_id,$studio_id,$user_id,'',1);
				}
			}
		}
        $favlist = array(
            'list' => $list,
            'total' => $count
        );
        return $favlist;
    }
    
    function dataSave($data){
        $log_id = (isset($data['log_id']) && intval($data['log_id'])) ? $data['log_id'] : 0;
        if ($log_id == 0) {
            $video_log = new VideoLogs();
            $video_log->movie_id = $data['movie_id'];
            $video_log->video_id = $data['stream_id'];
            $video_log->user_id = $data['user_id'];
            $video_log->studio_id = $data['studio_id'];
            if(@$data['device_type'] != ""){
                $video_log->device_type = @$data['device_type'];
            }
            if(@$data['content_type'] != ""){
                $video_log->content_type = @$data['content_type'];
            }
            $video_log->trailer_id = @$data['trailer_id'];
            if(@$data['played_from'] != ''){
                $video_log->played_from = $data['played_from'];
            }
            $video_log->created_date = new CDbExpression("NOW()");
            $video_log->ip = $data['ip_address'];
            $video_log->save();
            $log_id = $video_log->id;
        }
        $log_temp_id = (isset($data['log_temp_id']) && intval($data['log_temp_id'])) ? $data['log_temp_id'] : 0;
        if($log_temp_id > 0){
            $video_log_temp = VideoLogTemp::model()->findByPk($log_temp_id);
        } else{
            $video_log_temp = new VideoLogTemp();
            $video_log_temp->created_date = new CDbExpression("NOW()");
            $video_log_temp->ip=$data['ip_address'];
        }
        $video_log_temp->video_logs_id = $log_id;
        $video_log_temp->resume_time = @$data['resume_time'];
        $video_log_temp->played_length = $data['played_length'];
        $video_log_temp->played_percent = @$data['percent'];
        $video_log_temp->updated_date = new CDbExpression("NOW()");
        $video_log_temp->movie_id = $data['movie_id'];
        $video_log_temp->video_id = $data['stream_id'];
        $video_log_temp->user_id = $data['user_id'];
        $video_log_temp->studio_id = $data['studio_id'];
        $video_log_temp->watch_status = $data['status'];
        if(@$data['device_type'] != ""){
            $video_log_temp->device_type = @$data['device_type'];
        }
        if(@$data['content_type'] != ""){
            $video_log_temp->content_type = @$data['content_type'];
        }
        $video_log_temp->trailer_id = @$data['trailer_id'];
        if(@$data['video_length'] != ''){   
            $video_log_temp->video_length = $data['video_length'];
        }
        $video_log_temp->save();
        $sum = 0;
        $video_log = $this->findByPk($log_id);
        if($data['log_id'] != "" && $data['log_id'] != 0 && $log_temp_id > 0){
            $sum = Yii::app()->db->createCommand()
                    ->select('SUM(t.played_length)')
                    ->from('video_log_temp t')
                    ->where('t.video_logs_id = ' . $data['log_id'])
                    ->queryScalar();
        } else{
            $sum = $video_log->played_length +$data['played_length'];
        }
        $video_log->played_length = $sum;
        $video_log->played_percent = @$data['percent'];
        $video_log->updated_date = new CDbExpression("NOW()");
        $video_log->watch_status = $data['status'];
        $video_log->resume_time = @$data['resume_time'];
        $video_log->is_watch_durationenabled = @$data['enableWatchDuration'];
        if(@$data['video_length'] != ''){
            $video_log->video_length = $data['video_length'];
        }
        $video_log->save();
        return array($video_log_temp->id,$video_log->id);
    }
    public function activePlayLog($data){
        $playCount=Yii::app()->db->createCommand()
                ->select('count(distinct(user_id))')->from('video_logs')
				->where('DATE_ADD(updated_date ,INTERVAL 2 MINUTE) > NOW() and movie_id='.$data['movie_id'].' and video_id='.$data['stream_id'].' and studio_id='.$data['studio_id'])->queryColumn();
        if(@$playCount[0] > 0){
            return $playCount[0];
        } else{
            return "0";
        }
    }
	function getTotalViewsForStudio_with_devicetype($studio_id,$device_type = 0) {
        $device = '';
        if($device_type != 0){
            switch($device_type){
               case 1 :
                  $device = '_web'; 
                break;                
               case 2 :
                  $device = '_android'; 
                break;
                case 3 :
                  $device = '_ios'; 
                break;  
                case 4 :
                  $device = '_Roku'; 
                break;              
			}
        }
        $start_date = Date('Y-m-d', strtotime("-1 Months +1 Days"));
        $end_date = Date('Y-m-d');
        
        $sql = "SELECT count(*) AS total_views".$device." FROM video_logs WHERE studio_id = {$studio_id} AND device_type = {$device_type} AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') AND (trailer_id is null or trailer_id = 0)";
		$data = Yii::app()->db->createCommand($sql)->queryRow();
        
        return $data;
    } 
	public function getEmbededData($dt,$movie_id,$studio_id,$type,$trailer_id='',$deviceType){
         if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
}
            $trailer_sql = '';
           if(trim($trailer_id)){
              $trailer_sql = 'and v.trailer_id='.$trailer_id;
           }
           $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }         
         if($type == 3){
             $sql =  "SELECT  COUNT(v.id) as u_embededcnt FROM video_logs v LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = '".$studio_id."' AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') AND v.video_id='". $movie_id."' and v.played_from = 2 ".$trailer_sql.$deviceStr ;  
             
         }else{
             $sql =  "SELECT  COUNT(v.id) as u_embededcnt FROM video_logs v WHERE v.studio_id = '".$studio_id."' AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') AND v.movie_id='". $movie_id."' and v.played_from = 2 ".$trailer_sql.$deviceStr ;  
         }
        // echo $sql.'<br><br>';
         $data = Yii::app()->db->createCommand($sql)->queryRow(); 
      return $data['u_embededcnt'];        
    }
      function getgetuniqueview($dt,$movie_id,$video_id,$deviceType='',$contentTypes) {
            if($dt == ''){
                $end_date = date('Y-m-d');
                $daysgo = date('d')-1;
                $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }else{
                $start_date = $dt->start;
                $end_date = $dt->end;
}
            $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }
            $number_of_zero_view_sql = "select COUNT(v.user_id) as uid FROM video_logs v WHERE  v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$deviceStr." AND v.movie_id = '".$movie_id."' and v.video_id = '".$video_id."' and user_id = 0";
            $number_of_zero_view = Yii::app()->db->createCommand($number_of_zero_view_sql)->queryRow();
            $sql="select COUNT(distinct v.user_id) as u_viewcount FROM video_logs v WHERE  v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$deviceStr." AND v.movie_id = '".$movie_id."' and v.video_id = '".$video_id."'  GROUP BY CASE WHEN v.video_id!=0 THEN v.video_id ELSE v.trailer_id END ";
            $results = Yii::app()->db->createCommand($sql)->queryRow();
            if($number_of_zero_view['uid'] > 0){
                return $results['u_viewcount'] - 1;
            }else{
            return $results['u_viewcount'];
            }
        }
    public function getMaximumViewd($studio_id, $limit, $cond) {
        if ($studio_id && $limit) {
            $sql = 'SELECT v.movie_id FROM video_logs v, films F  where v.studio_id=' . $studio_id . ' AND v.movie_id=F.id ' . $cond . ' GROUP BY movie_id ORDER BY count(*) DESC LIMIT ' . $limit . '';
            $data = Yii::app()->db->createCommand($sql)->queryAll(); //print_r($data); exit;
            return $data;
        }
    }
    public function getLastSeen($studio_id, $limit, $cond) {
        $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        $sql = 'SELECT v.movie_id,v.video_id, v.episode_id FROM video_logs v, films F WHERE v.user_id = ' . $user_id . ' AND v.studio_id=' . $studio_id . ' AND v.played_length > 0 AND v.movie_id=F.id ' . $cond . '  ORDER BY v.id desc';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
}