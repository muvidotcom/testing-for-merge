<?php
class ContentFilter extends CActiveRecord{
	
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'content_filter';
    }
	
/**
 * @method public addontentFilter(array $request) It will add Content Filters into database
 * @return bool TRUE/FALSE
 * @author GDR<support@muvi.com>
 */	
	function addContentFilter($data,$content_type_id){
		$genre = array_unique($data['movie']['genre']);
		if($data['content_filter_type']){
			$contentFilterTypeCls = new ContentFilterType();
			$contentData = $contentFilterTypeCls->find('type_value=:type_value',array(':type_value'=>  strtolower(trim($data['content_filter_type']))));
			if($contentData){	
				if(isset($genre) && $genre){
					foreach($genre AS $key=>$val){
						if(!($this->exists('LOWER(filter_name) =:name AND studio_id=:studio_id AND content_type_id =:content_type_id', array(":name"=>$val,':studio_id'=>Yii::app()->common->getStudioId(),':content_type_id'=>$content_type_id)))){
							$this->isNewRecord = true;
							$this->primaryKey = NULL;
							$this->studio_id = Yii::app()->common->getStudioId();
							$this->content_type_id = $content_type_id;
							$this->filter_type_id = $contentData->id;
							$this->filter_name = $val;
							$this->filter_value = strtolower($val);
							$this->created_date = gmdate('Y-m-d H:i:s');
							$this->created_by = Yii::app()->user->id;
							$this->save(false);
						}
					}
				}
			}
		}
	}
}
