<?php

class CreditRules extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'credit_rule';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function checkExistingPlan($rule_id){
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('credit_rule')
                ->where('id = :id',array(':id'=>$rule_id))
               ->queryRow();
        return $data;
    } 
    public function checkplanexist($action,$plan_id,$rule_id){
         $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('credit_rule')
                ->where('rule_action = :rule_action and plan_id = :plan_id and id <> :id' ,array(':rule_action'=>$action,':id'=>$rule_id,':plan_id'=>$plan_id))
               ->queryAll();  
         return $data;
    }
    public function checkplanexistForAdd($action,$plan_id){
         $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('credit_rule')
                ->where('rule_action = :rule_action and plan_id = :plan_id',array(':rule_action'=>$action,':plan_id'=>$plan_id))
               ->queryAll();  
         return $data;       
    }


}
