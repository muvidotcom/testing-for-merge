<?php

class PpvPlans extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'ppv_plans';
    }

    public function rules() {
        return array(
            array('content_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ppvpricing'=>array(self::HAS_MANY, 'PpvPricing','ppv_plan_id',
                'condition'=>'ppvpricing.status=1'
            ),
            'ppvadvancecontent'=>array(self::HAS_MANY, 'PpvAdvanceContent','ppv_plan_id')
        );
    }

}
