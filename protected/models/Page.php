<?php
class Page extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'pages';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
        );
    }
    
    public function getPages($id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*,'.$this->tableName().'.id AS pageid,'.$this->tableName().'.title AS pagetitle, seo_info.title AS seotitle')
                ->from($this->tableName())
                ->leftJoin('seo_info',$this->tableName().'.id = seo_info.content_types_id ')
                ->where($this->tableName().'.studio_id=:id AND '.$this->tableName().'.status=:s AND '.$this->tableName().'.parent_id=0',array(':id'=>$id,':s' =>1))
                ->queryAll();
        return $data;
    }
}

