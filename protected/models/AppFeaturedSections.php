<?php
class AppFeaturedSections extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'app_featured_sections';
    }
    public function relations() {
        return array(
            'contents'=>array(self::HAS_MANY, 'AppFeaturedContent', 'section_id'),
        );
    }  
    public function getFeaturedSections($studio_id, $language_id = 20){
        $cond = "";
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = :studio_id',
                'params' => array(
                    ':studio_id' => $studio_id,
                ),
                'order' => 'contents.id_seq ASC'
            ),
        );
        $criteria->condition = 't.studio_id = ' . $studio_id. ' AND t.parent_id = 0';
        $criteria->order = 't.id_seq ASC';
        $featured_sections = $this->findAll($criteria);
        $translated  = array();
        $fsection    = array();
        if($language_id !=20){
            $translated = CHtml::listData($this->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
            if(!empty($featured_sections)){
                foreach($featured_sections as $sections){
                    if (array_key_exists($sections->id, $translated)) {
                       $sections->title = $translated[$sections->id];
                    }
                    $fsection[] = $sections;
                }
            }
        }else{
            $fsection = $featured_sections;
        }
        return $fsection;
    }
    public function getAppFeaturesParentId($feat_id){
        $feats = $this->findByPk($feat_id);
        if($feats->parent_id == 0):
           $feats_id =  $feat_id;               
        else:
           $feats_id = $feats->parent_id;
        endif;
        return $feats_id;
    }
    public function getMaxOrd($studio_id = false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $feat = $this->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($feat) && count($feat)){
            return (int) $feat->id_seq;
        }
        else{
            return 0;
        }      
    }
    public function getAllFeaturedSections($studio_id, $language_id = 20){
        $sections = $this->findAll('studio_id=:studio_id AND parent_id = 0 ORDER BY id_seq', array(':studio_id' => $studio_id));
        if($language_id !=20){
            $translated = CHtml::listData($this->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
            if(!empty($sections)){
                foreach($sections as $section){
                    if (array_key_exists($section->id, $translated)) {
                       $section->title = $translated[$section->id];
                    }
                    $sectionsdetail[] = $section;
                }
            }
        }else{
            $sectionsdetail = $sections;
        }
        if($sectionsdetail){
            foreach ($sectionsdetail as $key => $sec) {
                $afsection[$key]['studio_id'] = $sec->studio_id;
                $afsection[$key]['language_id'] = $sec->language_id;
                $afsection[$key]['title'] = $sec->title;
                $afsection[$key]['section_id'] = $sec->id;
                $afsection[$key]['section_type'] = $sec->content_type;
                $total_content = AppFeaturedContent::model()->countByAttributes(array(
                    'section_id'=> $sec->id,
                    'studio_id' => $studio_id
                ));
                $afsection[$key]['total'] =  $total_content;
            }
            return $afsection;
        }
    }
}
