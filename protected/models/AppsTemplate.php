<?php
class AppsTemplate extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'app_templates';
    }
    public function getTemplate($code)
    {
        $template = $this->find(array('condition' => 'code="'.$code.'"'));
        return $template;
    }    
}
