<?php
class FeaturedContent extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'featured_content';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
            'section'=>array(self::HAS_ONE, 'FeaturedSection', 'section_id'),
            'movie'=>array(self::BELONGS_TO, 'Film', 'movie_id'),
        );
    } 
    public function getMaxOrd($studio_id,$section_id){
        $cnt = FeaturedContent::model()->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id AND section_id=:section_id',
            'params' => array(':studio_id' => $studio_id,':section_id'=>$section_id),
            'order' => 'id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($cnt) && count($cnt)){
            return (int) $cnt->id_seq;
        }
        else{
            return 0;
        }      
    }
    public function getFeatured() {
        $studio_id = Yii::app()->common->getStudiosId();
        $return =  $translatedSections = array();
        $controller = Yii::app()->controller;
        $language_id = $controller->language_id;
        $user_id = Yii::app()->user->id;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.parent_id = 0 AND t.studio_id = ' . $studio_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $sections = FeaturedSection::model()->findAll($criteria);
        if($language_id !=20){
            $translatedSections = FeaturedSection::model()->getTranslatedFeaturedSections($sections, $studio_id, $language_id);
        }
        
        $return = array();
        if ($sections) {
            $addQuery = '';
            $customFormObj = new CustomForms();
            $customData = $customFormObj->getFormCustomMetadat($studio_id);
            if ($customData) {
                foreach ($customData AS $ckey => $cvalue) {
                    $addQuery = ",F." . $cvalue['field_name'];
                    //$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
                    $customArr[] = array(
                        'f_id' => trim($cvalue['f_id']),
                        'field_name' => trim($cvalue['field_name']),
                        'field_display_name' => trim($cvalue['f_display_name'])
                    );
                }
            }
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
            $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
            foreach ($sections as $section) {
                if(array_key_exists($section->id, $translatedSections)){
                    $section->title = $translatedSections[$section->id];
                }
                $final_content = array();
                if ($section->section_type == 1 && count($section->contents)) {
                    $streamIds = '';
                    $movieIds = '';
                    $final_content = $id_seq = array();
                    $final_physical = array();
                    $total_contents = 0;
                    $is_playlist = 0;
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
                    } else {
                        foreach ($section->contents as $featured) {
                            $content_episode = ($featured->is_episode == 3) ? 0 : $featured->is_episode;
                            $id_seq[$featured->movie_id."_".$content_episode] = array(
                                'is_episode' => $featured->is_episode,
                                'movie_id' => $featured->movie_id,
                                'id_seq' => $featured->id_seq
                            ); 
                            if ($featured->is_episode == 1) {
                                $streamIds .="," . $featured->movie_id;
                            } elseif ($featured->is_episode == 4) {
                                $playlistId .= "," . $featured->movie_id;
                                $is_playlist = 1;
                                $is_episode = $featured->is_episode;
                            } else {
                                $movieIds .= "," . $featured->movie_id;
                            }
                        }
                        $cond = '';
                        if (@$movieIds || @$streamIds) {
                            $cond = " AND (";
                            if (@$movieIds) {
                                $movieIds = ltrim($movieIds, ',');
                                $cond .="(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                                $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                            }
                            if ($streamIds) {
                                $streamIds = ltrim($streamIds, ',');
                                $cond .=" (M.id IN(" . $streamIds . "))";
                                $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                            } else {
                                $cond = str_replace(' OR ', ' ', $cond);
                            }
                            $cond .= ")";
                        }
                        $cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                        $sql = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
                                . "( SELECT M.movie_id,M.is_converted,M.video_duration,M.is_episode,M.embed_id,M.is_downloadable,M.id AS movie_stream_id,F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id,M.episode_title,M.episode_date,M.episode_story,M.episode_number,M.series_number, M.last_updated_date " . $addQuery
                                . "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=" . $studio_id . " " . $cond . " ) AS PM "
                                . " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
                        $sql_data = "SELECT P.* FROM (SELECT t.*,g.* FROM (" . $sql . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                        $where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
                        $sql_data .= $where . " " . $orderBy;
                        $list = Yii::app()->db->createCommand($sql_data)->queryAll();
                        $limited_data = 0;
                        if (Yii::app()->custom->LimitedContentHomePageData() == 1) {
                            $limited_data = 1;
                        }
                        $final_content = self::getFeaturedContentList($list, $margs, $customArr, $studio_id, $language_id, $translate, $id_seq, $limited_data);
                        if ($is_playlist == 1) {
                            $movieIds = ltrim($playlistId, ',');
                            $command1 = Yii::app()->db->createCommand()
                                    ->select('id,playlist_name,playlist_permalink')
                                    ->from('user_playlist_name u')
                                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id IN(' . $movieIds . ') ORDER BY FIELD(u.id,' . $movieIds . ')');
                            $data = $command1->QueryAll();
                            $k = 0;
                            if ($data) {
                                foreach ($data as $res) {
                                    $playlist_id = $res['id'];
                                    $playlist_name = $res['playlist_name'];
                                    $permalink = $res['playlist_permalink'];
                                    $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                                    
                                    $playlist_content = array(
                                        'movie_id' => $playlist_id,
                                        'title' => utf8_encode($playlist_name),
                                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                        'poster' => $poster,
                                        'is_episode' => $is_episode,
                                        'seq_feat' => $seqId[$k]
                                    );
                                    if(empty($id_seq)){
                                        $final_content[] = $playlist_content;
                                    }else{
                                        $key = $id_seq[$playlist_content['movie_id']."_".$playlist_content['is_episode']]['id_seq'];
                                        $final_content[$key] = $playlist_content;
                                    }
                                    $k++;
                                }
                            }
                        }
                        ksort($final_content);
                        $final_content = array_values(array_filter($final_content));
                    }
                } else if($section->section_type == 2) {
                    $section_category = json_decode($section->section_category);
                    $content_types = json_decode($section->content_types);
                    $section_type = $section->section_type;
                    $content_limit = $section->content_limit;
                    $viewmorecpermalink = '';
                    if($section->is_viewmore == 1){
                        //$getPermalink = ContentCategories::model()->findByPk($section_category[0], array('select' => 'permalink'));
                        $getPermalink = MenuItem::model()->findByAttributes(array('value' => $section_category[0], 'studio_id'=>$studio_id), array('select' => 'permalink'));
                        $viewmorecpermalink = $getPermalink->permalink;
                    }
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id, $section_type, $section_category, $content_limit);
                    } else {
                        $section_criteria = $section->section_criteria;
                        //$language_id = $section->language_id;
                        $content_type = $section->content_type;
                        //$customData = array();
                        $findContent = Film::model()->getContentID($studio_id, $section_criteria, $section_category, $content_limit, $content_type,$content_types);
                        $customData = Yii::app()->Helper->getRelationalField($studio_id, 1);
                        $i = 0; 
                        if (!empty($findContent)) { 
                            foreach ($findContent as $data) { 
                                if ($section_criteria == 6) {
                                    if ($data[0]['is_episode'] == 1) {
                                        $movie_id = $data[0]['stream_id'];
                                    } else {
                                        $movie_id = $data[0]['movie_id'];
                                    }
                                    $is_episode = $data[0]['is_episode'];
                                }else if ($section_criteria == 2 || $section_criteria == 7) {
                                    $movie_id = $data['movie_id'];
                                    $is_episode = $data['is_episode'];
                                }else {
                                    if ($data['is_episode'] == 1) {
                                        $movie_id = $data['stream_id'];
                                    } else {
                                        $movie_id = $data['movie_id'];
                                    }
                                    $is_episode = $data['is_episode'];
                                }
                                $final_content[$i] = Yii::app()->general->getContentData($movie_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
                                $i++;
                            }
                        } else {
                            $final_content = array();
                        }
                    } 
                }
                $return[] = array(
                    'id' => $section->id,
                    'title' => utf8_encode($section->title),
                    'content_type' => $section['content_type'],
                    'total' => count($final_content),
                    'layout_design' => $section->layout_design,
                    'cpermalink'=>$viewmorecpermalink,
                    'column_count' => $section->column_count,
                    'meta_data_info' => json_decode($section->meta_data_info),
                    'contents' => $final_content
                );
                }
                return $return;
            }
    } 
    /**
 * 
 * @method public getFeaturedContentList() It will return the list of content with all details in a array format
 * @param array $content Content array with all required information
 * @param array $arg Array of reqired basic information 
 * @author Biswajit<support@muvi.com>
 * @return array array of content data
 */		
	function getFeaturedContentList($contentList,$catVlaue,$customArr=array(),$studio_id='', $language_id='', $translate='', $id_seq = array(), $limited_data=0){
		$api_available = $controller->login_with;
		$getStudioApi = ExternalApiKey::model()->checkRegisterApi();

		if (!empty($getStudioApi)) {
			$chk_registeration = 1;
		}
		$getPerimissionApi = ExternalApiKey::model()->chkPermissionApi();
		$controller = Yii::app()->controller;
		if(!$translate){
			$translate = $controller->Language;
		}
		if(!$themes){
			$themes = $controller->studio->parent_theme;
		}
		if($user_id == false){
			$user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
		}
		if(!$studio_id){
			$studio_id = $controller->studio->id;
			
		}
		if(!$language_id){
			$language_id = $controller->language_id;
		}                
		$j=0;
		$isEnableAllPlayAud = 0;
		$allAudioPlay = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'get_all_episode_play');
		 if (!empty($allAudioPlay)) {
			 $isEnableAllPlayAud = $allAudioPlay['config_value'];
		 }
		foreach($contentList AS $cvalues){
			if(@$cvalues['is_episode']==1){
				if($cvalues['mapped_stream_id']){
					$mapped_stream_id .= $cvalues['mapped_stream_id'].",";
				}else{
					$streamIds .=$cvalues['movie_stream_id'].","; 
				}
			}else{
				if($cvalues['mapped_id']){
					$mapped_id .= $cvalues['mapped_id'].",";
				}else{
					$movie_ids .=$cvalues['movie_id'].","; 
				}
			}	
		}
		$postObj = new Poster();
		$pdata = array();
		if(@$movie_ids){
			$movie_ids = trim($movie_ids,',');
			$pdata = $postObj->getPosterByids($movie_ids,'films',$studio_id,'standard');
		}
		if(@$mapped_id){
			$mapped_id = trim($mapped_id,',');
			$pdata1 = $postObj->getPosterByids($mapped_id,'films',$studio_id,'standard',1);
			if($pdata1)
			$pdata = @$pdata + @$pdata1;
			
			$criteria = new CDbCriteria();
			$criteria->select ='id,movie_id , is_converted,video_duration,full_movie';
			$criteria->condition=  'movie_id IN('.$mapped_id.')';
			$streams = movieStreams::model()->findAll($criteria);
			
			foreach ($streams AS $skey=>$sval){
				$streamContent[$sval->movie_id] = $sval->attributes; 
			}
		}
		if($streamIds){
			$streamIds = trim($streamIds,',');
			$pdata2 = $postObj->getPosterByids($streamIds,'moviestream',$studio_id,'episode');
			if($pdata2)
			$pdata = @$pdata + @$pdata2;
		}
		if($mapped_stream_id){
			$mapped_stream_id = trim($mapped_stream_id,',');
			$pdata3 = $postObj->getPosterByids($mapped_stream_id,'moviestream',$studio_id,'episode',1);
			if($pdata3)
			$pdata = @$pdata + $pdata3;
		}
		$DefaultPoster = $postObj->getDefaultPoster($themes, $studio_id);
		
		foreach($contentList AS $ckey=>$contentdetails){
            $season_number  = 0;
            $parent_content_title = $topBanner = "";
            $content_details = array();
			$content = (object)$contentdetails;
			$movie_id = $content_id =  $content->movie_id;
            if($content->is_episode == 1){
                $content_id = $content->movie_stream_id;
            }
			$langcontent = array();
			if($language_id !=20){
				$langcontent = Yii::app()->custom->getTranslatedContent($content_id, @$content->is_episode, $language_id, $studio_id);
			}
			$custom_vals = array();
			$is_live = 0;
			if($content->mapped_id){	
				$map_movieid =  $content->mapped_id;
				$stream = $streamContent[$map_movieid];
				$is_converted = $stream['is_converted'];
				$stream_id = $stream['id'];
				$full_movie = $stream['full_movie'];
				$video_duration = $stream['video_duration'];
			}else{
				$map_movieid =  $movie_id;
				$is_converted = $content->is_converted;
				$stream_id = $content->movie_stream_id;
				$full_movie = $content->full_movie;
				$video_duration = $content->video_duration;
			}            
			if($content->feed_url != '')
				$is_live = 1;
			if (array_key_exists($movie_id, @$langcontent['film'])) {
                //$content = Yii::app()->Helper->getLanuageCustomValue($content,@$langcontent['film'][$movie_id]);
				$content->name = @$langcontent['film'][$movie_id]->name;
				$content->story = @$langcontent['film'][$movie_id]->story;
				$content->genre = @$langcontent['film'][$movie_id]->genre;
				$content->censor_rating = @$langcontent['film'][$movie_id]->censor_rating;
                $content->language = $langcontent['film'][$movie_id]->language;
			}
			$ses = array();$purchaseType='';
			if(@$content->is_episode==1){
				if (array_key_exists($content->movie_stream_id, @$langcontent['episode'])) {
                   //$content = Yii::app()->Helper->getEpisodeLanguageCustomValue($content,@$langcontent['episode'][$stream_id]); 
				   $content->episode_title = $langcontent['episode'][$stream_id]->episode_title;
				   $content->episode_story = $langcontent['episode'][$stream_id]->episode_story;
				}
				$content_title = ($content->episode_title != '') ? $content->episode_title : "SEASON " . $content->series_number . ", EPISODE " . $content->episode_number;
				$content_name = ($content->episode_title != '') ? $content->episode_title : "SEASON " . $content->series_number . ", EPISODE " . $content->episode_number;
				$story = $content->episode_story;
				$release = $content->episode_date;
				if(@$pdata[$stream_id]){
					$posterUrl = $pdata[$stream_id]['poster_url'];
				}else{
					$posterUrl = $DefaultPoster['horizontal_poster'];
				}
                $season_number = @$content->series_number;
                $parent_content_title  = @$content->name;
			}else{
                $topBanner = Yii::app()->controller->getPoster($movie_id, 'topbanner', 'original', $studio_id);
				$content_title = $content_name = $content->name;
				$release = $content->release_date;
				$story = $content->story;
				if (@$content->content_types_id == 3) {
                    $content_details = Yii::app()->controller->getContentSeasonDetails($movie_id, $studio_id);
					$EpDetails = Yii::app()->controller->getEpisodeToPlay($movie_id, $studio_id);
					if ($EpDetails) {
						$is_converted = 1;
					}
					$seasons = Yii::app()->general->getSeasonsToPlay($movie_id);
					foreach ($seasons as $season) {
						if (isset($season->series_number) && intval($season->series_number)) {
							$ses[] = array('series_number' => $season->series_number);
						}
					}
					$purchaseType ='season';
				}
				$audAllPlay = 0;
				if($content->content_types_id == 6 && $isEnableAllPlayAud > 0){
					 $audAllPlay = Yii::app()->controller->getAudioPlayable($movie_id, $studio_id);
				}
				if($content_types_id ==2 || $content_types_id == 4){
					if(@$pdata[$map_movieid]){
						$posterUrl = str_replace('/standard/', '/episode/', $pdata[$map_movieid]['poster_url']);
						$posterOriginal = str_replace('/standard/', '/original/', $pdata[$map_movieid]['poster_url']);
					}else{
						$posterUrl = $posterOriginal = $DefaultPoster['horizontal_poster'];
					}
				}else{					
					if(@$pdata[$map_movieid]){
						$posterUrl = $pdata[$map_movieid]['poster_url'];
						$posterOriginal = str_replace('/standard/', '/original/', $pdata[$map_movieid]['poster_url']);
					}else{
						$posterUrl = $posterOriginal = $DefaultPoster['vertical_poster'];
					}
				}
			}
			$stream_uniq_id = 0;
			$payment_type = $adv_payment = $is_ppv_bundle = 0;
			$content_types_id = $content->content_types_id;
			
			$permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
			$release_date = Yii::app()->general->formatReleaseDate($release);
			$full_release_date = Yii::app()->general->formatFullReleaseDate($release);
			$play_btn = '';
			$fpermalink = $content->permalink;  
			$content_unique_id = $content->uniq_id;
			
			if($content_types_id != 5 && $content_types_id != 6){
				if ($is_converted == 1 || $content_types_id == 4) {	
					if($content->is_episode==1){
						if ($user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playbtn" data-ctype="' . $content_types_id . '">';
						} else {
							$play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-content_title="' . $content_title . '" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-backdrop="static" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
						}
                        if (!empty($getPerimissionApi) && $user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-stream_id="' . $content->embed_id . '" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">';
                        }
					}else{
						if ($user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="'.$purchaseType.'"  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}else{
							$play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-content_title="' . $content_title . '" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-backdrop="static" data-purchase_type="'.$purchaseType.'"  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}
                        if (!empty($getPerimissionApi) && $user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-stream_id="0" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">';
                        }
					}
                    if ($content_types_id == 3) {
                        if ($user_id > 0) {
                            $play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="season" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
                        }  else {
                            $play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-backdrop="static" data-purchase_type="season" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
                        }
                    }
					$play_btn.= $translate['play_now'] . '</a>';
					if($play_btn !=""){
						$play_btn .='<input type="hidden" name="permalink" id="permalink" value="'.$fpermalink.'" />';
						$play_btn .='<input type="hidden" name="content_name" id="content_name" value="'.$content_name.'" />';
					}
				}
			}else{
				if($is_converted==1 && $content_types_id == 5){
					$play_btn = '<a href="javascript:void(0);" class="playbtn" onclick="playAudio(this)" data-content_type="' .$content->is_episode. '" id="'.$content->movie_id.'" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playaudio" data-ctype="' . $content_types_id . '">';
				$play_btn.= $translate['play_now'] . '</a>';
			}	
				if($audAllPlay > 0 && $content_types_id == 6){
					$play_btn = '<a href="javascript:void(0);" class="playbtn" onclick="playAllAudio(this)" data-content_type="' .$content->is_episode. '" id="'.$content->movie_id.'" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playaudio" data-index="0" data-ctype="' . $content_types_id . '">';
                    $play_btn.= $translate['play_all'] . '</a>';
				}
			}	
			$cast_detail = Yii::app()->general->getCasts($movie_id, 1, $language_id, $studio_id, $translate);
            $casts = $cast_detail['casts'];
            $casting = $cast_detail['casting'];
            $short_story = Yii::app()->common->htmlchars_encode_to_html(Yii::app()->general->formattedWords($story, 200));
			$genre = json_decode($content->genre);
			if(!is_array($genre) && $content->genre){
				if(!in_array(trim($content->genre),array('null','NULL','[','""'))){
					$genre = explode(",", $content->genre);
				}
			}

		
			if(isset($customArr) && count($customArr) > 0){
				foreach($customArr as $ar){
					$ar_k = trim($ar['f_id']);
					$ar_k1 = trim($ar['field_name']);
					$k = (int) str_replace('custom', '', $ar_k1);
					if($k > 9){
						if(@$content->custom10){
							$x = json_decode($content->custom10,true);
							foreach ($x as $key => $value) {
								foreach ($value as $key1 => $value1) {
									$custom_vals[$ar_k] = array(
										'field_display_name' => trim($ar['field_display_name']),
										'field_value' => trim($value1)
									);
                                    $custom_vals2[$ar_k] = trim(is_array(json_decode($content->$ar_k1)) ? implode(', ', json_decode($content->$ar_k1)) : $content->$ar_k1);
                                }
							}
						}
					}else{
						$custom_vals[$ar_k] = array(
							'field_display_name' => trim($ar['field_display_name']),
							'field_value' => trim(is_array(json_decode($content->$ar_k1))?implode(', ', json_decode($content->$ar_k1)):$content->$ar_k1)
						);
                        $custom_vals2[$ar_k] = trim(is_array(json_decode($content->$ar_k1)) ? implode(', ', json_decode($content->$ar_k1)) : $content->$ar_k1);
                    }
				} 
			}

			if ($content->is_downloadable && ($content->thirdparty_url=='')) {
				if ($is_converted == 1) {
					if($content->is_episode==1){
						if ($user_id > 0) {
							$download_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playbtn" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '">';
						} else {
							$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' .$content->uniq_id. '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '" class="playbtn allplay" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '">';
						}
					}else{
						if ($user_id > 0) {
							$download_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="'.$purchaseType.'"  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}else{
							$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-purchase_type="'.$purchaseType.'"  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}
					}
					$download_btn .= $translate['download'] . '</a>';
				}
			}
            if($limited_data){
                $final_content = array(
                    'movie_id' => $movie_id,
                    'title' => utf8_encode($content_name),
                    'play_btn' => $play_btn,
                    'permalink' => $permalink,
                    'poster' => $posterUrl,
                    'data_type' => ($content->content_types_id == 3) ? 1 : 0,
                    'release_date' => $release_date,
                    'full_release_date' => $full_release_date,
                    'is_converted' => @$is_converted,
                    'movie_stream_id' => $stream_id,
                    'uniq_id' => $content_unique_id,
                    'content_type_id' => @$featured['content_type_id'],
                    'content_types_id' => $content_types_id,
                    'short_story' => utf8_encode($short_story),
                    'is_episode' => @$content->is_episode,
                    'genres' =>$genre,
                );
            }else{
                $final_content = array(
                    'movie_id' => $movie_id,
                    'content_category_value' => $content->content_category_value,
                    'title' => utf8_encode($content_name),
                    'content_title' => $content_title,
                    'is_episode' => @$content->is_episode,
                    'episode_number' => @$content->episode_number,
                    'season_number' => @$season_number,
                    'parent_content_title' => @$parent_content_title,
                    'content_details' => $content_details,
                    'play_btn' => $play_btn,
                    'buy_btn' => @$buy_btn,
                    'permalink' => $permalink,
                    'poster' => $posterUrl,
                    'top_banner' => $topBanner,
                    'data_type' => ($content->content_types_id == 3) ? 1 : 0,
                    'is_landscape' => ($content->content_types_id == 2) ? 1 : 0,
                    'release_date' => $release_date,
                    'full_release_date' => $full_release_date,
                    'censor_rating' => (!is_array(json_decode($content->censor_rating))) ? @$content->censor_rating : implode(',', json_decode($content->censor_rating)) . '&nbsp;',				
                    'movie_uniq_id' => $content->uniq_id,
                    'stream_uniq_id' => $stream_uniq_id,
                    'video_duration' => $video_duration,
                    'video_duration_text' => Yii::app()->general->videoDurationText($video_duration),
                    'ppv' => @$ppv_id,
                    'payment_type' => @$payment_type,
                    'is_converted' => $is_converted,
                    'movie_stream_id' => $stream_id,
                    'uniq_id' => $content_unique_id,
                    'content_types_id' => $content_types_id,
                    'ppv_plan_id' => $content->ppv_plan_id,
                    'full_movie' => $full_movie,
                    'story' => utf8_encode(Yii::app()->general->showHtmlContents($story)),
                    'short_story' => utf8_encode($short_story),
                    'genres' => $genre,
                    'display_name' => utf8_encode(@$catVlaue['display_name']),
                    'content_permalink' => @$catVlaue['category_permalink'],
                    'casts' => @$casts,
                    'casting' => @$casting,
                    'custom' => @$custom_vals,
                    'custom_meta_data' => @$custom_vals2,
                    'buy_btn' => @$buy_btn,
                    'seq_feat' =>$idSeq[$j],
                    'is_downloadable' => @$content->is_downloadable,
                    'download_btn' => @$download_btn
                );
            }
			if(count($custom_vals) > 0){
				$final_content = array_merge($final_content, @$custom_vals);
			}
            if(empty($id_seq)){
                $data[] = $final_content;
            }else{
                $fcontent_movie_id = ($final_content['is_episode'] == 1) ? $final_content['movie_stream_id'] : $final_content['movie_id'];
                $key = $id_seq[$fcontent_movie_id."_".$final_content['is_episode']]['id_seq'];
                $data[$key] = $final_content;
            }
			$j++; 
		}
        return $data;
	}
}

