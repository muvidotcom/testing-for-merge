<?php

class PGProduct extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_product';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getStudioCurrencyCode($studio) {
        /*$studiocurrency = StudioCurrencySupport::model()->findAll(array('condition' => 'studio_id = ' . $studio->id));
        if (empty($studiocurrency)) {
            $currencyid = $studio->default_currency_id;
        } else {
            foreach ($studiocurrency as $k => $v) {
                $currencyid[] = $v->currency_id;
            }
        }
        $currenycode = Currency::model()->findAllByAttributes(array("id" => $currencyid));
        foreach ($currenycode as $k => $v) {
            $code_curreny[$v->id] = $v->code.'('.$v->symbol.')';
        }*/
        /*
         * below query taken from MenetizationController > actionGetCurrencyList()
         */
        $sql = "SELECT cu.*,scs.id as scs_id,scs.status,scs.is_default FROM currency cu,studio_currency_support scs WHERE  cu.id = scs.currency_id AND scs.studio_id = ".$studio->id." ORDER BY scs.is_default desc";
        $code_curreny = Yii::app()->db->createCommand($sql)->queryAll();
        if (empty($code_curreny)) {
            $currenycode = Currency::model()->findAllByAttributes(array("id" => $studio->default_currency_id));
            foreach ($currenycode as $k => $v) {
                $code_curreny[$k]['id'] = $v['id'];
                $code_curreny[$k]['country_code'] = $v['country_code'];
                $code_curreny[$k]['code'] = $v['code'];
                $code_curreny[$k]['title'] = $v['title'];
                $code_curreny[$k]['symbol'] = $v['symbol'];
                $code_curreny[$k]['scs_id'] = 1;
                $code_curreny[$k]['status'] = 1;
                $code_curreny[$k]['is_default'] = 1;
            }
        }
        return $code_curreny;
    }

    public function getProductList($type, $studio_id, $movie_id = '',$limit = '',$offset = '',$req = '') {
        $dbcon = Yii::app()->db;
        $showproduct = 1;
        $cond = " is_deleted=0 AND status != 2 AND is_free_offer=0 AND studio_id={$studio_id} AND product_type={$type}";
        $cond .= " AND if(is_preorder = 1, 1, ((publish_date IS NULL) OR  (UNIX_TIMESTAMP(publish_date) = 0) OR (publish_date <=NOW())))";
        $condArray = array();
		if($req['cat_id']){
            //$cond .=" AND (content_category_value & :content_category_value)";
			$cond .=" AND FIND_IN_SET(".$req['cat_id'].",content_category_value)";
			//$condArray[':content_category_value'] = $req['cat_id'];
        }
        if($req['format']){
            $cond .=" AND (custom_fields = :custom_fields)";
			$condArray[':custom_fields'] = $req['format'];
        }
        if($req['genre']){
            $cond .=" AND (genre like :genre)";
			$condArray[':genre'] = '%'.$req['genre'].'%';
        }
        
        
          if($req['orderby']==4)
            $orderby=' ORDER BY release_date DESC';
          else if($req['orderby']==2)
            $orderby=' ORDER BY name DESC';
          else if($req['orderby']==3) 
            $orderby=' ORDER BY updated_date ASC';
        else
            $orderby = ' ORDER BY id DESC';
        
        
        if ($movie_id) {
            $productids = $dbcon->createCommand("SELECT GROUP_CONCAT(product_id) as pid FROM pg_product_content WHERE movie_id=" . $movie_id)->queryROW();
            $productids = $productids['pid'];
            if($productids!=''){
                $cond .= " AND id IN($productids)";
            }else{
                $showproduct=0;
            }
        }
        if($showproduct){
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id',array(':studio_id' => $studio_id));
            
            $sql_data1 = "SELECT * FROM pg_product WHERE " . $cond;
            if($geoexist){
                
                $visitor_loc = Yii::app()->common->getVisitorLocation();
                $country = $visitor_loc['country'];
                $sql_geo = "SELECT DISTINCT gc.`pg_product_id` as pg_product_id, gc.`geocategory_id`,sc.`country_code` FROM geoblockpgcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = ".$studio_id." AND sc.country_code='{$country}'";
				$sql_data = $sqldata_wl = "SELECT P.* FROM (SELECT t.*,g.* FROM (".$sql_data1.") AS t LEFT JOIN (".$sql_geo.") AS g ON t.id = g.pg_product_id) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'" . $orderby;	
				if($limit){
                    $sql_data .=" LIMIT " . $limit . " OFFSET " . $offset ;
                }
            }else{
               
				$sql_data = $sqldata_wl = "SELECT * FROM pg_product WHERE " . $cond." ".$orderby;
                if($limit)
                    {
                    $sql_data .= " LIMIT " . $limit . " OFFSET " . $offset ;
				}
            }
            
            //$productlist = $dbcon->createCommand($sql_data)->queryAll();
			$productlist = PGProduct::model()->findAllBySql($sql_data,$condArray);
			$Pgcount = PGProduct::model()->findAllBySql($sqldata_wl,$condArray);
			$productlist['count'] = count($Pgcount);
        }else{
            $productlist = array();
        }
      
        return $productlist;
    }
    public function getPgImageForApp($productid, $type, $studio_id=false){
        if(empty($productid))
        {
            return FALSE;
        }
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
		$local_poster_path = Yii::app()->getBaseUrl(true);
        $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $productid)->queryAll();
        if (!empty($image)) {
            $pgposter = $url . '/system/pgposters/' . $productid . '/' . $type . '/' . $image[0]['name'];
            if(false ===  file_get_contents($pgposter)){
                $pgposter = false;
            }
        } else {
            $pgposter = false;
        }
        return $pgposter;
    }
    public function getpgImage($productid, $type, $variant_id = null) {
        if(!$productid)
            return FALSE;
        
        $studio_id = Yii::app()->common->getStudiosId();
        $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
		$local_poster_path = Yii::app()->getBaseUrl(true);
         if(isset($variant_id)){
           $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $productid . " AND varient_id=" . $variant_id)->queryAll();
         } else {
        $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $productid)->queryAll();
         }
        if (!empty($image)) {
            $pgposter = $url . '/system/pgposters/' . $productid . '/' . $type . '/' . $image[0]['name'];
            if(false ===  file_get_contents($pgposter)){
                $pgposter = $local_poster_path.'/img/no-image.jpg';
            }
        } else {
            $pgposter = $local_poster_path.'/img/no-image.jpg';
        }
        return $pgposter;
    }

    public function getPersionalizeImage($itemId) {
        $studio_id = Yii::app()->common->getStudiosId();
        $image = CustomizerLog::model()->find(array('select'=>'image_url_output','condition' => 'id=:id','params' => array(':id' =>$itemId)));
        if (!empty($image->image_url_output)) {
            $pgposter = $image->image_url_output;
        } else {
            $pgposter = '/img/no-image.jpg';
        }
        return $pgposter;
    }    
    /**
    * @method public addcontent() Add a content to pg_product tables based on the parameters
    * @return int $last_insert_id
    * @author MRS<manas@muvi.com> 
    */
    function  addContent($data,$uniqid,$permalink){
        $this->uniqid = $uniqid;
        $this->permalink = $permalink;
        $this->name = $data['name'];
        $this->description = $data['description'];
        $this->studio_id = Yii::app()->user->studio_id;
        $this->user_id = Yii::app()->user->id;
        $this->category_id = 0;
        $this->sub_cat_id = 0;
        $this->brand_id = 0;
        $this->sale_price = @$data['sale_price'];
        $this->purchase_price = 0.0;
        $this->discount = 0;
        $this->discount_type = 0;
        $this->discount_till_date = '0000-00-00';
        $this->shipping_cost = 0.0;
        $this->product_type = $data['product_type'];
        $this->currency_id = @$data['currency_id'];
        $this->units = '';
        $this->sku = $data['sku'];
        $this->custom_fields = $data['custom_fields'];
        $this->rating = $data['rating'];
        $this->video = '';
        $this->current_stock = 0;
        $this->release_date = $data['release_date'] ? date('Y-m-d', strtotime($data['release_date'])) : NULL;
        $this->status = $data['status'];
        $this->content_category_value = implode(",",@$data['content_category_value']);
        $this->size = @$data['size'];
        $this->barcode = @$data['barcode'];
        $this->genre = (is_array(@$data['genre'])?json_encode(@$data['genre']):@$data['genre']);
        $this->audio = @$data['audio'];
        $this->subtitles = @$data['subtitles'];
        $this->alternate_name = @$data['alternate_name'];
        $this->is_deleted = 0;
        $this->is_preorder = 0;
        $publish_date = NULL;
        if (@$data['content_publish_date']) {
            if (@$data['publish_date']) {
                $pdate = explode('/', $data['publish_date']);
                $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                if (@$data['publish_time']) {
                    $publish_date .= ' ' . $data['publish_time'];
				}
            }
        }
        $this->publish_date = $publish_date;
        $this->created_date = gmdate('Y-m-d H:i:s');
        $this->updated_date = gmdate('Y-m-d H:i:s');
        $this->feature = $data['feature'];
		$this->productize_flag = @$data['productize_flag'];
        $this->ip = CHttpRequest::getUserHostAddress();
        $data['custom6'] = self::createSerializeCustomDate($data);
        $this->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
        $this->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
        $this->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
        $this->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
        $this->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
        $this->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
        if(@$data['custom_metadata_form_id'])
                $this->custom_metadata_form_id = $data['custom_metadata_form_id'];
        $this-> setIsNewRecord(true);
        $this-> setPrimaryKey(NULL);
        if($this->save()){            
            return $this->attributes;
        }else{
            return '';
        }
    }
    function createSerializeCustomDate($data,$flag=5){
        $cus = '';
        foreach ($data as $key => $value) {
            $k = (int) str_replace('custom', '', $key);
            if($k>$flag){
                $cus[] = array($key=>$value);
            }
        }
        return $cus;
    }
    function getSerializeCustomDate($list){
        if(@$list[0]['custom6']){
            $x = json_decode($list[0]['custom6'],true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $list[0][$key1] = $value1;
                }
            }
        }
        return $list;
    }
    
    public function getPersonalizationImageUrl($productid,$personalised_id) {
        $url = CustomizerLog::model()->findByAttributes(array('product_id' => $productid, 'studio_id' => Yii::app()->common->getStudiosId(),'id'=>$personalised_id), array('select' => 'image_url_output'));
        if($url->image_url_output)
        {
            $org_image = $url->image_url_output;
            $thumb_image = str_replace('/original', '/studio_thumb', $url->image_url_output);
            return array('original' => $org_image, 'thumb' => $thumb_image);
        }
        else
            return array();
    }
    /**
     * @method getPgContentData 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return Array Returns data of a physical product 
     * @param  $content_id integer, $studio_id integer
    * */
    public function getPgContentData($content_id, $studio_id){
        $pgContent = $this->findByPk($content_id); 
        $currency_id = Studio::model()->findByPk($studio_id, array('select' => 'default_currency_id'))->default_currency_id;
        if (Yii::app()->common->isGeoBlockPGContent($content_id)){
            $cont_name = $pgContent['name'];
            $story = $pgContent['description'];
            $permalink = $pgContent['permalink'];
            $short_story = substr(Yii::app()->common->htmlchars_encode_to_html($story), 0, 200);
            $poster = $this->getPgImageForApp($content_id, 'standard', $studio_id);
            $formatted_price = Yii::app()->common->formatPrice($pgContent['sale_price'], $pgContent['currency_id']);
            $tempprice = Yii::app()->common->getPGPrices($content_id, $currency_id);
            if(!empty($tempprice)){
                $formatted_price = Yii::app()->common->formatPrice($tempprice['price'], $pgContent['currency_id']);
                $currency_id = $tempprice['currency_id'];
            }
        }
        $final_content = array(
            'movie_id' => $content_id,
            'title' => $cont_name,
            'permalink' => $permalink,
            'poster_url' => $poster,
            'data_type' => 4,
            'uniq_id' => @$pgContent['uniqid'],
            'short_story' => $short_story,
            'price' => @$formatted_price,
            'currency_id' => $currency_id,
            'status' => @$pgContent['status']
        ); 
        return @$final_content;
    }
}
