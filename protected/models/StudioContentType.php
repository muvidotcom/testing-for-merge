<?php
class StudioContentType extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_content_types';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio' => array(self::HAS_ONE, 'Studio', 'studio_id'),                
        );
    }
    
    public function getActiveContent($studio_id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:id AND is_enabled=:active',array(':id' => $studio_id,':active' => '1'))
                ->queryAll();
        return $data;
    }
}
