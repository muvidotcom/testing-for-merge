<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class IP2Location extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'ip2location';
    }
    
    public function getLocation($ip_address = '111.93.166.194'){
        $return = array();
        if(!$ip_address)
            $ip_address = CHttpRequest::getUserHostAddress();
        $res = IP2Location::model()->find(array(
            'select' => 'country_code, country_name, region_name, city_name',
            'condition' => 'INET_ATON(:ip) <= ip_to',
            'params' => array(':ip' => $ip_address),
            'limit' => '1'
        ));
        if(count($res)){
            $return = $res->attributes;
        }
        return ($return);
        exit;
    }
    public function getCountryCode($ip_address = '111.93.166.194'){
        $return = array();
        if(!$ip_address)
            $ip_address = CHttpRequest::getUserHostAddress();
        $res = IP2Location::model()->find(array(
            'select' => 'country_code',
            'condition' => 'INET_ATON(:ip) <= ip_to',
            'params' => array(':ip' => $ip_address),
            'limit' => '1'
        ));
        if(count($res)){
            $return = $res->attributes;
        }
        return ($return);
        exit;
    }
}