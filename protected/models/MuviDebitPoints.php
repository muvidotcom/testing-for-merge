<?php

class MuviDebitPoints extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'muvi_debit_points';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getTotalDebitOfStudio($studio_id, $sDate, $eDate) {
        $sql = "SELECT SUM(debit_value) AS total_credit_used FROM muvi_debit_points mdp, sdk_users u WHERE mdp.user_id = u.id AND u.studio_id = {$studio_id} AND (DATE_FORMAT(mdp.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ";
        $total = Yii::app()->db->createCommand($sql)->queryRow();
        return $total['total_credit_used'];
    }
    
    public function getDebitDetailsOfStudio($studio_id, $search, $sDate, $eDate, $page_size, $offset) {
        $res = array();
        if(isset($search) && trim($search)){
            $search_cond = " AND (u.email LIKE '%{$search}%' OR u.display_name LIKE '%{$search}%' OR f.name LIKE '%{$search}%')";
        }
        $cond = " AND f.id = mdp.movie_id";
        $sql = "SELECT SQL_CALC_FOUND_ROWS mdp.*, u.id AS user_id,u.email,u.display_name, mdp.created_date AS created_date, f.name AS movie_name FROM muvi_debit_points mdp, sdk_users u, films f WHERE mdp.user_id = u.id AND u.studio_id = {$studio_id} AND (DATE_FORMAT(mdp.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') " . $cond . $search_cond . " LIMIT ".$offset.",".$page_size;
        $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $res;
    }
}
