<?php

class ResellerCardInfos extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_card_infos';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function setResellerCardInfo($data=array()){    
        if(count($data)>= 1){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
            return $this->id;
        }
    }
    public function cancel_card_is_primary($user_id){
       $sql = Yii::app()->db->createCommand()
       ->update('reseller_card_infos',array('is_cancelled'=>1),'portal_user_id=:userid',array(':userid'=>$user_id));

}
    public function make_card_is_primary($card_info_id){
        $sql = Yii::app()->db->createCommand()
        ->update('reseller_card_infos',array('is_cancelled'=>0),'id=:cid',array(':cid'=>$card_info_id));
    }
    public function getnumberOfExistingCard($user_id){
        $sql = Yii::app()->db->createCommand()
          ->select('count(id) as numof_data')
          ->from('reseller_card_infos')
          ->where('portal_user_id=:id',array(':id'=>$user_id))
          ->queryAll(); 
        return $sql[0]['numof_data'];
    }
    public function getprimary_card($user_id){
        $sql = Yii::app()->db->createCommand()
             ->select('c.*')
             ->from('reseller_card_infos c')
             ->where('is_cancelled = 0')
             ->queryAll();
        if(count($sql) > 0){
            return $sql[0];
        }else{
            return '';
        }
    }
    public function getExistingCard($reseller_id){
           $sql = Yii::app()->db->createCommand()
                 ->select('*')
                 ->from('reseller_card_infos')
                 ->where('portal_user_id=:pid',array(':pid'=>$reseller_id))
                 ->queryAll();
           return $sql;
    }
}
