<?php 
class MovieTag extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie_tags';
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function addTags($data) {
		if($data){
			if(isset($data['language']) && $data['language']){
				foreach($data['language'] AS $key=>$val){
					if(!($this->exists('LOWER(name) = :name AND taggable_type=:taggable_type', array(":name"=>$val,':taggable_type'=>3)))){
						$this->isNewRecord = true;
						$this->primaryKey = NULL;	
						$this->name = $val;
						$this->taggable_type =3;
                                                $this->studio_id =0;
						$this->save(false);
					}
				}
			}
			if(isset($data['genre']) && $data['genre']){
				foreach($data['genre'] AS $key=>$val){
					if(!($this->exists('name=:name AND taggable_type=:taggable_type AND studio_id=:studio_id', array(":name"=>$val,':taggable_type'=>1,':studio_id'=>Yii::app()->user->studio_id)))){
						$this->isNewRecord = true;
						$this->primaryKey = NULL;
						$this->name = $val;
						$this->taggable_type =1;
                                                $this->studio_id = Yii::app()->user->studio_id;
						$this->save(false);
					}
				}
			}
			if(isset($data['censer_rating']) && $data['censer_rating']){
				foreach($data['censer_rating'] AS $key=>$val){
					if(!($this->exists('LOWER(name) = :name AND taggable_type=:taggable_type', array(":name"=>$val,':taggable_type'=>2)))){
						$this->isNewRecord = true;
						$this->primaryKey = NULL;
						$this->name = $val;
						$this->taggable_type =2;
                                                $this->studio_id =0;
						$this->save(false);
					}
				}
			}
			return TRUE;
		}
		return FALSE;
	}
	public function addTagsFromCustomfield($data) {
        if ($data) {
            $tags = $this->findAll(array('condition'=>'studio_id='.Yii::app()->user->studio_id.' AND taggable_type=1'));
            $tag = CHtml::listData($tags, 'id', 'name');            
            $diff = array_diff($tag, $data['genre']);
            $tagids = array_keys($diff);
            if (!empty($tagids)) {
                $memberIds = implode(', ', $tagids);
                $this->deleteAll('id IN (' . $memberIds . ')');
}
            if (isset($data['genre']) && $data['genre']) {
                foreach ($data['genre'] AS $key => $val) {
                    if (!($this->exists('name=:name AND taggable_type=:taggable_type AND studio_id=:studio_id', array(":name" => trim($val), ':taggable_type' => 1, ':studio_id' => Yii::app()->user->studio_id)))) {
                        $this->isNewRecord = true;
                        $this->primaryKey = NULL;
                        $this->name = trim($val);
                        $this->taggable_type = 1;
                        $this->studio_id = Yii::app()->user->studio_id;
                        $this->save(false);
                    }                    
                }
            }
            return TRUE;
        }
        return FALSE;
    }
}

