<?php
class CustomMetadataForm extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'custom_metadata_form';
    }
    public function getFromData($studio_id = 0, $fields = '*',$orderby = 'id ASC') {
        $command = Yii::app()->db->createCommand()
            ->from('custom_metadata_form')
            ->select($fields)
            ->where('studio_id ='.$studio_id)
            ->order($orderby);
        $data = $command->queryAll();
        foreach ($data as $key => $value) {
            $fromdata[] = $value;
        }
        return(@$fromdata);
    }
    public function getbinaryvaluesofcustomform($formField) {
        $command = Yii::app()->db->createCommand()
            ->from('custom_metadata_form')
            ->select('content_binary,child_content_binary')
            ->where('parent_content_type_id = '.$formField['parent_content_type_id'].' AND content_type = '.$formField['content_type'].' AND is_child = '.$formField['is_child'].' AND studio_id = 0');
        $data = $command->queryRow();
        return(@$data);
    }
    public function insertFormData($studio_id,$formField,$binary,$editid = 0){
        $cmf = CustomMetadataForm::model()->findByAttributes( array('id' => $editid,'studio_id' => $studio_id));
        if(empty($cmf)){
            $cmf = New CustomMetadataForm;
            $cmf->studio_id = $studio_id;
            $cmf->created_by = Yii::app()->user->id;
            $cmf->created_date = gmdate('Y-m-d H:i:s');
            $cmf->ip = CHttpRequest::getUserHostAddress();
            $cmf->updated_date = NULL;
            $cmf->content_types_id = $formField['content_types_id'];
            $cmf->form_type_id = $formField['form_type_id'];
            $cmf->parent_content_type_id = $formField['parent_content_type_id'];
            $cmf->content_type = $formField['content_type'];
            $cmf->is_child = $formField['is_child'];            
            $cmf->content_binary = $binary['content_binary'];
            $cmf->child_content_binary = $binary['child_content_binary'];
        }else{
            $cmf->updated_date = gmdate('Y-m-d H:i:s');
        }
        $cmf->name = $formField['name'];
        $cmf->poster_size = $formField['poster_size'];
        
        $cmf->save();
        return $cmf->id;
    }
    public function showFormBySetting($studio_id,$form){
        $content = Yii::app()->general->content_count($studio_id);
        $content_child = Yii::app()->general->content_count_child($studio_id);
        if(empty($content)){$content['content_count']=1;}
        if(empty($content_child)){$content_child['content_count']=3;}
        $finalform = array();
        if(Yii::app()->general->getStoreLink()){
            $content['content_count'] = $content['content_count']+16;//16 is binary value of muvikart see "parent_content_type" table
        }
        //krsort($form);
        foreach ($form as $key => $value) {
            if((int)$value['content_binary'] & (int)$content['content_count']){
                if($value['child_content_binary']){
                    if((int)$value['child_content_binary'] & (int)$content_child['content_count']){
                        $metadata_form_type_id =  Yii::app()->general->getmetadata_form_type_id($value);
                        $value['metadata_form_type_id'] = $metadata_form_type_id;
                        $finalform[$value['id']] = $value;
                    }
                }else{
                    $metadata_form_type_id =  Yii::app()->general->getmetadata_form_type_id($value);
                    $value['metadata_form_type_id'] = $metadata_form_type_id;
                    $finalform[$value['id']] = $value;
                }                                        
            }
        }
        return $finalform;
    }
}

