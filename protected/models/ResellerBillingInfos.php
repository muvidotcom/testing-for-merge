<?php

class ResellerBillingInfos extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_billing_infos';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    
    public function getResellerBillingInfos($user_id,$offset,$limit)
    {
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM reseller_billing_infos WHERE portal_user_id = ".$user_id." ORDER BY id DESC LIMIT ".$offset.",".$limit;
        $data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $data['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $data;
    }
    
    public function getBillingDetails($id)
    {
        $sql = "SELECT * FROM billing_details WHERE billing_info_id = ".$id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        return $data;
    }
    public function setBillingInfo($data = array()){
       if(count($data) >= 1){
            foreach($data as $key => $val ){
                $this->$key = $val;
            }
             $this->save();
             return $this->id;
         }
    }

}