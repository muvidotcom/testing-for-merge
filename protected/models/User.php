<?php
class User extends CActiveRecord{
    public static function model($className=__CLASS__) {
            return parent::model($className);
      }
    protected function afterValidate(){
            //$this->password = $this->encrypt($this->password);
            return parent::afterValidate();
    }
    public function tableName() {
        return 'user';
    }                
    /**
    * @return array relational rules.
    */
   public function relations()
   {
           // NOTE: you may need to adjust the relation name and the related
           // class name for the relations automatically generated below.
           return array(
               'studio'=>array(self::BELONGS_TO, 'Studio','studio_id')

           );
   }

    public function encrypt($value){
            $enc = new bCrypt();
            return $enc->hash($value);
    }
    public function get_users($user_type){
        $user_info = array();
        /*
        $studio_user_id = Yii::app()->common->getStudioId();
        $db_user = Yii::app()->db;
        $studio_sql = "SELECT subdomain FROM studios WHERE id = '".$studio_user_id."' LIMIT 1";
        $data = $db_user->createCommand($studio_sql)->queryAll();
        if($data){
            $user_reg = $data[0]['subdomain'];
            $users = SdkUser::model()->findAll(array("condition" => "registered_from = '".$user_reg."' AND email IS NOT NULL"));
            foreach($users as $k => $v){
                $user_info[] = array('display_name'=>$v['display_name'] ,'id'=>$v['id'],'email'=>$v['email']);
            }
        }
         * 
         */
        $studio_id = Yii::app()->common->getStudioId();
        $users = SdkUser::model()->findAll(array("condition" => "status = 1 AND is_deleted = '0' AND studio_id = ".$studio_id));
        foreach($users as $k => $v){
            $user_info[] = array('display_name'=>$v['display_name'] ,'id'=>$v['id'],'email'=>$v['email']);
        }                
        return $user_info;
    }
    
    public function updateUser($studio_id,$user_data)
    {
        $data = Yii::app()->db->createCommand()
                    ->update($this->tableName(),$user_data,'studio_id=:studio_id',array(':studio_id' => $studio_id));
        return $data;
    }
    
    public function getPartnersContentViewDetails($dt,$studio_id,$device)
    {  
        $start_date=$dt->start;
        $end_date=$dt->end;
      
        $datasql = "select DISTINCT u.first_name,u.studio_id,p.movie_id,p.percentage_revenue_share,count(v.id) as viewcount,u.id from user u left join partners_contents p on u.id = p.user_id left join video_logs v on v.movie_id=p.movie_id where u.studio_id ='".$studio_id."'and u.role_id = 4 AND u.is_active = 1 AND u.is_sdk = 1 AND u.signup_step = 0 AND (DATE_FORMAT(u.created_at, '%Y-%m-%d') BETWEEN '" . $start_date . "' AND '".$end_date."') group by u.id";
         
        $con = Yii::app()->db;
         (array) $data = $con->createCommand($datasql)->queryAll();

        return $data;
    }
    public function save_reseller_customer_data($data = array()){
        if(count($data)> 0){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
            return $this->id;
        }
    }
    
}
