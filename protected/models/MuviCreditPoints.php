<?php

class MuviCreditPoints extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'muvi_credit_points';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
     public function getTotalCreditOfStudio($studio_id, $sDate, $eDate) {
        $sql = "SELECT SUM(credit_value) AS total_credit_generated FROM muvi_credit_points mcp, sdk_users u WHERE mcp.user_id = u.id AND u.studio_id = {$studio_id} AND (DATE_FORMAT(mcp.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ";
        $total = Yii::app()->db->createCommand($sql)->queryRow();
        return $total['total_credit_generated'];
    }
    
    public function getCreditDetailsOfStudio($studio_id, $search, $sDate, $eDate, $page_size, $offset) {
        $res = array();
        if(isset($search) && trim($search)){
            $search_cond = " AND (u.email LIKE '%{$search}%' OR u.display_name LIKE '%{$search}%' OR sp.name LIKE '%{$search}%')";
        }
        $cond = " AND sp.id = mcp.plan_id";
        $sql = "SELECT SQL_CALC_FOUND_ROWS mcp.*, u.id AS user_id,u.email,u.display_name, mcp.created_date AS created_date,sp.name AS plan_name FROM muvi_credit_points mcp, sdk_users u,subscription_plans sp WHERE mcp.user_id = u.id AND u.studio_id = {$studio_id} AND (DATE_FORMAT(mcp.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') " . $cond . $search_cond . " LIMIT ".$offset.",".$page_size;
        $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
        $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $res;
    }

}
