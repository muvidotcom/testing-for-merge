<?php
class StudioPaymentGateways extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'studio_payment_gateways';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studiorel'=>array(self::BELONGS_TO, 'Studio','studio_id'),
            'paymentgt'=>array(self::BELONGS_TO, 'PaymentGateways','gateway_id'),
        );
    }    
}
