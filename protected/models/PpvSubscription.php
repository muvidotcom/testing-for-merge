<?php
class PpvSubscription extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'ppv_subscriptions';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sdk_user'=>array(self::HAS_ONE, 'SdkUser', 'user_id'),
            'ppv_plan'=>array(self::HAS_ONE, 'PpvPlans', 'ppv_plan_id'),
        );
    }
    public function update_status_of_ppv_subscription($user_id,$studio_id,$movie_id,$session = 0,$episod = 0){
        $data = Yii::app()->db->createCommand()
        ->update('ppv_subscriptions',array('status'=>0),'user_id=:user_id and studio_id=:studio_id and movie_id=:movie_id and season_id=:season_id and episode_id=:episode_id',array(':user_id'=>$user_id,':studio_id'=>$studio_id,':movie_id'=>$movie_id,':season_id'=>$session,':episode_id'=>$episod));      
}
    public function update_status_of_ppv_subscription_multi($id){
        $data = Yii::app()->db->createCommand()
                ->update('ppv_subscriptions',array('status'=>0),'id=:id',array(':id'=>$id));      
    }
    public function checkVoucherUsed($studio_id,$voucher_id){
        $data = Yii::app()->db->createCommand()
              ->select('id')
              ->from('ppv_subscriptions')
              ->where('studio_id=:studio_id and ppv_plan_id=:ppv_plan_id',array(':studio_id'=>$studio_id,':ppv_plan_id'=>$voucher_id))
              ->queryAll();
        return $data;
        
}
}
