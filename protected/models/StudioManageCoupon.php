<?php
class StudioManageCoupon extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'studio_manage_coupon';
    } 
    public function save_reseller_customer_studio_id($studio_id){
        $this->studio_id = $studio_id;
        $this->save();
}
}
