<?php

class ImageManagement extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'image_management';
    }
public function checkavailibility($studio_id,$image_type,$image_name)
    {
     
         $sql ="select * from  image_management where image_type=".$image_type." and image_name like '%".trim($image_name)."%' and flag_deleted=0 and studio_id=".$studio_id;
           // echo $sql;
         $command =Yii::app()->db->createCommand($sql);
         $results = $command->queryAll();
          return $results;
    }
    
    public function get_imagedetails_by_name($name,$studio_id)
    {
    $sql ="select * from  image_management where image_name='".trim($name)."' and studio_id=".$studio_id ;  
    $command =Yii::app()->db->createCommand($sql);
    $results = $command->queryAll();
    return $results;  
    }
    
     public function get_imagedetails_by_id($rec_id)
    {
    $sql ="select * from  image_management where id=".$rec_id ;  
    $command =Yii::app()->db->createCommand($sql);
    $results = $command->queryAll();
    return $results;  
    }
    
     public function get_imagedetails_by_studio_id($studio_id)
    {
    $sql ="select * from  image_management where  studio_id=".$studio_id." and flag_deleted=0 ORDER BY id DESC LIMIT 100" ;  
    $command =Yii::app()->db->createCommand($sql);
    $results = $command->queryAll();
    return $results;  
    }
  
     public function get_imagedetails_by_id_studio($rec_id,$studio_id)
    {
        $sql ="select * from  image_management where id=".$rec_id." and studio_id=".$studio_id ;  
        $command =Yii::app()->db->createCommand($sql);
        $results = $command->queryAll();
        return $results;  
    }
   
    function isExistImageKey($studio_id=null){
        if(!$studio_id){
            $studio_id = isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] ? $_REQUEST['studio_id'] : Yii::app()->user->studio_id;
		}
        $checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
        if(!empty($checkImageKey) && $checkImageKey->config_value==1){
            $image_id = isset($_REQUEST['id']) && $_REQUEST['id']?$_REQUEST['id']:'';
            $image_key = @$_REQUEST['image_key'];
            $chkImage = $this->count('id != :id AND image_key=:image_key AND studio_id=:studio_id', array(':id'=>$image_id, ':image_key'=>trim($image_key), ':studio_id'=>$studio_id));
            if($chkImage == 0){
                $res['success']  = 1;
            } else {
                $res['success'] = 0;
                $res['message']  = 'Image key already exist';
            }
        } else {
            $res['success'] = 0;
            $res['message']  = 'Image key featured disabled';
        }
        /* 0=exist image key, 1=not exist */
        return $res;
    }
}
