<?php
class CelebrityType extends CActiveRecord {
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'celebrity_type';
    }
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
}
?>