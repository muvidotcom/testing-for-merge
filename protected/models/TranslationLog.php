<?php
class TranslationLog extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'translation_log';
    } 
    public function removeOldestBackup(){
        $controller = Yii::app()->controller;
        $studio = $controller->studio;
        $studio_id = $studio->id;
        $total_backups = $this->countByAttributes(array(
            'studio_id' => $studio_id,
            'language_id'=>$controller->language_id
        ));
        if($total_backups >= 5){
            $theme  =  $studio->theme;
            $oldest = $this->find("studio_id=:studio_id AND language_id=:language_id ORDER BY action_date ASC",array(':studio_id'=>$studio_id,':language_id'=>$controller->language_id));
            $backup_lang_path = ROOT_DIR."languages/".$theme."/backups/".$controller->language_code."/".$oldest->backup_file_name;
            chmod($backup_lang_path,0777);
            if(unlink($backup_lang_path)){
                TranslationLog::model()->deleteByPk($oldest->id);
                return true;
            }else{
                return false;
            }
        }
    }
    public function lastUpdateDate($studio_id, $language_id=20){
        $last_update_date = date("Y-m-d H:i:s");
        $last_update = $this->find("studio_id=:studio_id AND language_id=:language_id ORDER BY action_date DESC",array(':studio_id'=>$studio_id,':language_id'=>$language_id),array('select'=>'action_date'));
        if($last_update){
            $last_update_date = $last_update->action_date;
        }
        return $last_update_date;
    }
}