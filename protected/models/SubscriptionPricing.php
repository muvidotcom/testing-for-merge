<?php

class SubscriptionPricing extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'subscription_pricing';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    public function get_pricing_ids($plan_id){
        $dbcon = Yii::app()->db;
        $sql ="select GROUP_CONCAT(id) AS pricing_ids FROM subscription_pricing where subscription_plan_id = ".$plan_id." ";
        $data = $dbcon->createCommand($sql)->queryRow();
        return $data;      
    }
}
