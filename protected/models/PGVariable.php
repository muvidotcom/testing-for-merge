<?php
class PGVariable extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'pg_variable';
    }
    public function getFieldData($studio_id,$customflag=0){
        $cond = 'f.studio_id ='.$studio_id;
        if($customflag){
            $cond.= " AND field_type=1";
        }
        $command = Yii::app()->db->createCommand()
            ->from('pg_variable f')
            ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.field_type')
            ->where($cond);
        $Allcolumn_data = $command->queryAll();
        foreach ($Allcolumn_data as $key1 => $value1) {
            $Allcolumn_data_new[$value1['f_name']] = $value1;
		}
        if($Allcolumn_data_new){
            return $Allcolumn_data_new;
        }else{
            return array();
        }
    }
    public function insertFieldData($studio_id,$custom_form_id,$custom_field_id){
		$this->studio_id = $studio_id;
        $this->custom_form_id = $custom_form_id;
        $this->custom_field_id = $custom_field_id;
		$this->created_date = gmdate('Y-m-d H:i:s');
		$this->ip = $_SERVER['REMOTE_ADDR'];
        $this->isNewRecord = TRUE;
        $this->primaryKey = NULL;
        $this->save(); 
	}
}
