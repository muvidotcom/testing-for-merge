<?php

class TempSdkUser extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'temp_sdk_users';
    }

    function saveTempSdkUser($data, $studio_id = '') {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }

        $pass = '';
        $password = $data['password'];
        $enc = new bCrypt();
        if ($password) {
            $pass = $enc->hash($password);
        }

        $confirm_token = $enc->hash($email);
        if (strpos($confirm_token, "/") !== FALSE) {
            $confirm_token = str_replace('/', '', $confirm_token);
        }


        $ip_address = Yii::app()->request->getUserHostAddress();
        $geo_data = Yii::app()->common->getVisitorLocation($ip_address);
        //Save SDK user detail
        $tempuser = new TempSdkUser;
        $tempuser->email = trim($data['email']);
        $tempuser->name = trim($data['name']);
        $tempuser->unique_id = $data['unique_id'];
        $tempuser->studio_id = $studio_id;
        $tempuser->signup_ip = $ip_address;
        $tempuser->mobile_number = $data['mobile_number'];
        $tempuser->password = $pass;
        $tempuser->confirmation_token = $confirm_token;
        $tempuser->signup_location = serialize($geo_data);
        $tempuser->created_date = date('Y-m-d H:i:s');
        $tempuser->user_language = Yii::app()->controller->language_code;

        $return = $tempuser->save();
        $tempuser_id = $tempuser->id;
        $arr['tempuser_id'] = $user_id;
    }

    function saveToSdkUser($getTempUser) {
        if (!empty($getTempUser)) {
            $data['data']['email'] = $getTempUser['email'];
            $data['data']['name'] = $getTempUser['name'];
            $data['data']['password'] = $getTempUser['password'];
            $data['data']['signup_ip'] = $getTempUser['signup_ip'];
            $data['data']['mobile_number'] = $getTempUser['mobile_number'];
            $data['data']['api_unique_id'] = $getTempUser['unique_id'];
            $data['data']['signup_location'] = $getTempUser['signup_location'];
            $data['data']['is_api'] = 1;
            $ret = SdkUser::model()->saveSdkUser($data, $getTempUser['studio_id']);

            return $ret;
            exit;
        }
    }

}
