<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Countrys3location
 *
 * @author Sanjeev<sanjeev@muvi.com>
 */
class Countrys3location extends CActiveRecord{
    
     public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'country_s3_location';
    }
    
    public function getS3Location($country)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('country_name LIKE :country_name',array(':country_name' => $country))
                ->queryRow();
        return $data;
    }
}
