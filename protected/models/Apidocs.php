<?php
class Apidocs extends CActiveRecord{

/**
 * @return string the associated database table name
 */
	public function tableName(){
		return 'apidocs';
	}
	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
	
	
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'api_request_params' => array(self::HAS_MANY, 'Apirequestparams','apidocs_id')
		);
	}
	public function behaviors() {
		return array(
			'sluggable' => array(
				'class'=>'ext.behaviors.SluggableBehavior',
				'columns' => array('title'),
				'unique' => true
			),
	   );
	}	
}