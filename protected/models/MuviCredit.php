<?php

class MuviCredit extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'muvi_credit';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function getContentCategoryData($studio_id){
        $data = Yii::app()->db->createCommand()
               ->select('*')
               ->from('muvi_credit')
               ->where('studio_id=:studio_id',array(':studio_id'=>$studio_id))
               ->order('status desc')
               ->queryAll();
        return $data;
    }

}
