<?php
class BlogRelatedContent extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'blog_related_content';
    }
    /**
     * @method addRelatedContent
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For inserting related contents for a blog 
     */
    public function addRelatedContent($related_content, $post_id, $studio_id){
        if(!empty($related_content)){
			$i = 0;
            foreach($related_content as $rcontent):
				$i++;
                $content = explode("_",$rcontent);
                $content_id   = $content[0];
                $is_episode   = $content[1];
                $content_type = $content[2];
                $this->blog_id      =  $post_id;
                $this->studio_id    =  $studio_id;
                $this->content_id   =  $content_id;
                $this->is_episode   =  $is_episode;
                $this->content_type =  $content_type;
				$this->id_seq =  $i;
                $this->setIsNewRecord(true);
                $this->setPrimaryKey(NULL);
                $this->save();
            endforeach;
        }
    }
    /**
     * @method getAllRelatedDataOfPost
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related contents of blog post 
     * @return  Array of related contents id
     */
    public function getAllRelatedDataOfPost($studio_id, $post_id){
        if($post_id){
            $post_cond = " AND blog_id=".$post_id ;
            $related_post = Yii::app()->db->createCommand()->select('id,CONCAT(content_id,"_",is_episode,"_",content_type) as rcontents')->from('blog_related_content')->where('studio_id='.$studio_id.$post_cond.' AND content_type=0')->QueryAll();
            $related_post = CHtml::listData($related_post, 'id', 'rcontents');
            $related_content = Yii::app()->db->createCommand()->select('id,CONCAT(content_id,"_",is_episode,"_",content_type) as rcontents')->from('blog_related_content')->where('studio_id='.$studio_id.$post_cond.' AND content_type!=0')->QueryAll();
            $related_content = CHtml::listData($related_content, 'id', 'rcontents');
            $data['related_post'] = array_values(array_filter($related_post));
            $data['related_content'] = array_values(array_filter($related_content));
        }else{
            $data['related_post'] = $data['related_content'] = array();
        }
        return @$data;
    }
    /**
     * @method getAllRelatedDataWithName
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related contents of a blog post
     * @return Array of related content/posts name and content_id
     */
    public function getAllRelatedDataWithName($post_id, $studio_id){
        $selected_contents = $selected_posts = array();
        $rcontent = Yii::app()->db->createCommand()->select('content_id,is_episode,content_type')->from('blog_related_content')->where('studio_id='.$studio_id.' AND blog_id='.$post_id)->QueryAll();
        if($rcontent){
            foreach($rcontent as $content){
                $content_id = $content['content_id'];
                if($content['content_type'] == 0){
                    $name = BlogPost::model()->findByPk($content_id, array('select' => 'post_title'))->post_title;
                    $selected_posts[]= array(
                        'name' => @$name,
                        'content_id'=> @$content_id."_".@$content['is_episode']."_".@$content['content_type']
                    );
                }else{
                    if(in_array($content['content_type'], array(1,2,3)) ){
                        if($content->is_episode == 1){
                            $val  = movieStreams::model()->findByPk($content_id,  array('select'=>'episode_title,movie_id,series_number,episode_number'));
                            $tlt  = ($val['episode_title'] != '') ? $val['episode_title'] : " SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                            $tlt  = 'Episode-' . str_replace("'", "u0027", ($tlt));
                            $movie_name = Film::model()->findByPk($val['movie_id'], array('select'=>'name'))->name;
                            $name = str_replace("'", "u0027", ($movie_name)).' '.$tlt;
                        }else{
                            $name = str_replace("'", "u0027", (Film::model()->findByPk($content_id, array('select'=>'name'))->name));
                        }
                    }elseif($content['content_type'] == 4){
                        $name = str_replace("'", "u0027", (PGProduct::model()->findByPk($content_id, array('select' => 'name'))->name));
                    }elseif($content['content_type'] == 5){
						$name = str_replace("'", "u0027", (UserPlaylistName::model()->findByPk($content_id, array('select' => 'playlist_name'))->playlist_name));
                    }
                    $selected_contents[]= array(
                        'name' => @$name,
                        'content_id'=> @$content_id."_".@$content['is_episode']."_".@$content['content_type']
                    );
                }
            }
        }
        $contents = array(
            'selected_posts' => $selected_posts,
            'selected_contents' => $selected_contents
        );
        return @$contents; 
    }
    /**
     * @method getRelatedContentsOfBlogPost
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related contents of a blog post 
     * @return Array
     */
    public function getRelatedContentsOfBlogPost($post_id, $studio_id, $language_id = 20, $user_id = 0, $translate = array()){
        $list = array();
        $criteria = new CDbCriteria;
        $criteria->select = 'content_id,is_episode,content_type'; 
        $criteria->condition = 'studio_id = '.$studio_id.' AND blog_id = '.$post_id.' AND content_type !=0 ORDER BY id_seq DESC';
        $rcontents = $this->findAll($criteria);
        if($rcontents):
            foreach($rcontents as $content):
                if($content->content_type != 4):
                    $list[] = Yii::app()->general->getContentData($content->content_id, $content->is_episode, array(), $language_id, $studio_id, $user_id, $translate);
                else:
                    $list[] = PGProduct::model()->getPgContentData($content->content_id, $studio_id);
                endif;
            endforeach;
        endif;
        return @$list;
    }
    /**
     * @method getRelatedPostOfBlogPost
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related posts of a blog post 
     * @return Array
     */
    public function getRelatedPostOfBlogPost($post_id, $studio_id, $type = 'blog', $is_episode = 0, $language_id =20){
        $list = $posts = $translatedData = array();
        $criteria = new CDbCriteria;
        if($type == 'blog'):
            $content_id = 'content_id';
            $criteria->select = 'id,'.$content_id; 
            $criteria->condition = 'studio_id = :studio_id AND blog_id = :blog_id  AND content_type = 0';
			$criteria->params = array(':studio_id'=>$studio_id,':blog_id'=>$post_id);
        else:
            $content_id = 'blog_id';
            $criteria->select = 'id,'.$content_id; 
            $criteria->condition = 'studio_id = :studio_id AND content_id =:content_id AND is_episode = :is_episode AND content_type != 0';
			$criteria->params = array(':studio_id'=>$studio_id,':content_id'=>$post_id,':is_episode'=>$is_episode);
        endif;
        $criteria->order='id DESC';
        $rposts = $this->findAll($criteria);
        if($rposts):
            $posts = array_values(array_filter(CHtml::listData($rposts, 'id', $content_id)));
            if($language_id != 20):
                $translatedData = BlogPost::model()->getTranslatedRelatedPost($studio_id, $language_id, $posts);
            endif;
			$posts = implode(',', $posts);
            $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('blog_posts')
                ->where('parent_id = 0 AND studio_id = '.$studio_id.' AND id IN ('.$posts.') ORDER BY FIELD(id,'.$posts.')')
                ->setFetchMode(PDO::FETCH_OBJ)->queryAll();
            foreach ($data as $post):            
                if(array_key_exists($post->id, $translatedData)):
                    $post->post_title = $translatedData[$post->id]['post_title'];
                    $post->post_content = $translatedData[$post->id]['post_content'];
                    $post->post_short_content = $translatedData[$post->id]['post_short_content'];
                endif;
                $short_desc = $post->post_short_content;
                if($short_desc == ""):
                    $html_removed_text = Yii::app()->common->strip_html_tags($post->post_content);
                    $short_desc = Yii::app()->general->formattedWords($html_removed_text, 300);
                endif;
                $author = $post->author;
                if($author == ""):
                    $author = Studio::model()->findByPk($studio_id, array('select' => 'name'))->name;
                endif;
                $featured_image = $post->featured_image;
                if($featured_image != ""):
                    $featimgUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                    $thumb   = $featimgUrl. "/system/featured_image/" . $studio_id . "/original/" .$post->featured_image;
                else:
                    $html = html_entity_decode($post->post_content);
                    preg_match('/<img[^>]+>/i', $html, $thumbnail);
                    $thumb = "";
                    if(!empty($thumbnail)):
                        $thumb = @$thumbnail[0];
                        $doc = new DOMDocument();
                        $doc->loadHTML($thumb);
                        $xpath = new DOMXPath($doc);
                        $thumb = $xpath->evaluate("string(//img/@src)");
                    endif;
                endif;
                $list[] = array(
                    'post_id' => (int)$post->id,
                    'permalink' => trim($post->permalink),
                    'full_permalink' => Yii::app()->getBaseUrl(true).'/blog/'.trim($post->permalink),
                    'post_title' => Yii::app()->common->htmlchars_encode_to_html($post->post_title),
                    'short_desc' => $short_desc,
                    'thumb' => trim($thumb),
                    'full_desc' => Yii::app()->common->htmlchars_encode_to_html($post->post_content),
                    'author' =>$author,
                    'date' => $post->created_date,
                );
            endforeach;
		endif;
        return @$list;
    }
}