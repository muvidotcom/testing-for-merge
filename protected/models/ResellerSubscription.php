<?php

class ResellerSubscription extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'reseller_subscriptions';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    public function check_subscription($user_id){
        $sql = Yii::app()->db->createCommand()
                ->select('id,end_date')
                ->from('reseller_subscriptions')
                ->where('reseller_id=:id', array(':id'=>$user_id))
                ->order('id desc')
                ->limit(1)
                ->queryAll();
       return $sql[0]['end_date'];
}
    public function setSubscription_details($data = array()){
        if(count($data) >= 1){
            foreach($data as $key => $val){
                $this->$key = $val;
            }
            $this->save();
            return $this->id;
        }
    }
    public function getResellerPaymentStatus($user_id){
            $sql = Yii::app()->db->createCommand()
                   ->select('id,payment_status, partial_failed_date, partial_failed_due,start_date,end_date')
                   ->from('reseller_subscriptions')
                   ->where('reseller_id=:id',array(':id'=>$user_id))
                   ->order('id desc')
                   ->limit(1)
                   ->queryAll();
           return $sql;
    }
}
