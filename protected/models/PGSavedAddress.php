<?php

class PGSavedAddress extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_saved_address';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    /* 
     * This function is insert data into order table
     */
    public function saveAddress($studio_id,$buyer_id,$shipping_addr,$ip,$flag){        
        if($flag!='insert'){
            $model = $this->findByPk($flag);
            $model->address_name = $shipping_addr['address_name'];
            $model->first_name = $shipping_addr['first_name'];
            $model->last_name = $shipping_addr['last_name'];
            $model->email = $shipping_addr['email'];
            $model->address = $shipping_addr['address'];
            $model->city = $shipping_addr['city'];
            $model->state = $shipping_addr['state'];
            $model->country = $shipping_addr['country'];
            $model->zip = $shipping_addr['zip'];
            $model->phone_number = $shipping_addr['phone_number'];
            $model->user_id = $buyer_id;
            $model->studio_id = $studio_id;
            $model->created_date = new CDbExpression("NOW()");
            $model->ip = $ip;
            $model->save();
            $save_id = $model->id;
        }else{
            $this->address_name = $shipping_addr['address_name'];
            $this->first_name = $shipping_addr['first_name'];
            $this->last_name = $shipping_addr['last_name'];
            $this->email = $shipping_addr['email'];
            $this->address = $shipping_addr['address'];
            $this->city = $shipping_addr['city'];
            $this->state = $shipping_addr['state'];
            $this->country = $shipping_addr['country'];
            $this->zip = $shipping_addr['zip'];
            $this->phone_number = $shipping_addr['phone_number'];
            $this->user_id = $buyer_id;
            $this->studio_id = $studio_id;
            $this->created_date = new CDbExpression("NOW()");
            $this->ip = $ip;
            $this->save();
            $save_id = $this->id;
        }
        return $save_id;
    }
    /* Delete Address */
    public function deleteAddress($id,$studio_id,$buyer_id){
        $address=$this->findByPk($id);
        $address->delete();
        $model = $this->find( "studio_id = :studio_id AND user_id = :user_id",array('studio_id' => $studio_id,'user_id' => $buyer_id) );
        if(empty($model)){return 1;}else{return 0;}
    }
}