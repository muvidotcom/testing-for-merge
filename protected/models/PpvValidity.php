<?php

class PpvValidity extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'ppv_validity';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getPlaybackAccessForStudio($studio_id, $monetization_code, $key) {
        $data = $this->findByAttributes(array('studio_id' => $studio_id, 'monetization_code' => $monetization_code, 'playbility_access_key' => $key));
        return $data;
}

}
