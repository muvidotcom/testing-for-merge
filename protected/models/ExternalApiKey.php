<?php
class ExternalApiKey extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'external_api_keys';
    }
    public function chkPermissionApi(){
        $studio_id = Yii::app()->common->getStudiosId();
        $getPerimissionApi = Yii::app()->db->createCommand()
                ->select('id,studio_id')
                ->from('studio_api_details')
                ->where('studio_id=:id and module=:module', array(':id' => $studio_id, ':module' => 'playPerimissionChk'))
                ->queryRow();
        
        return $getPerimissionApi;
    }
    public function checkRegisterApi(){
        $studio_id = Yii::app()->common->getStudiosId();
        $regApi = Yii::app()->db->createCommand()
                ->select('e.id,e.studio_id,s.id,s.studio_id')
                ->from('external_api_keys e')
                ->join('studio_api_details s', 'e.id=s.api_type_id')
                ->where('s.studio_id=:id and e.studio_id=:studio_id and s.module="register"', array(':id' => $studio_id, ':studio_id' => $studio_id))
                ->queryRow();
        
        return $regApi;
    }
}