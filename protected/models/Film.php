<?php

/**
 * This is the model class for table "films".
 *
 * The followings are the available columns in table 'films':
 * @property string $id
 * @property string $name
 * @property string $language
 * @property string $rating
 * @property string $country
 * @property string $genre
 * @property string $permalink
 * @property string $alias_name
 * @property string $story
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $content_type_id
 * @property string $censor_rating
 * @property string $tags
 * @property string $movie_payment_type
 * @property integer $is_verified
 * @property integer $is_merged
 * @property integer $studio_id
 * @property string $ip
 * @property integer $created_by
 * @property string $created_date
 * @property integer $last_updated_by
 * @property string $last_updated_date
 */
class Film extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'films';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content_type_id, is_verified, is_merged, studio_id, created_by, last_updated_by', 'numerical', 'integerOnly'=>true),
			array('name, country, censor_rating', 'length', 'max'=>255),
			array('language, rating', 'length', 'max'=>200),
			array('genre', 'length', 'max'=>200),
			array('permalink', 'length', 'max'=>150),
			array('alias_name', 'length', 'max'=>255),
			array('movie_payment_type', 'length', 'max'=>10),
			array('ip', 'length', 'max'=>30),
			array('story, meta_title, meta_description, meta_keywords, tags, created_date, last_updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, language, rating, country, genre, permalink, alias_name, story, meta_title, meta_description, meta_keywords, content_type_id, censor_rating, tags, movie_payment_type, is_verified, is_merged, studio_id, ip, created_by, created_date, last_updated_by, last_updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'casts'=>array(self::MANY_MANY, 'Celebrities','movie_casts(movie_id,celebrity_id)'),
                    'movie_streams' => array(self::HAS_MANY, 'movieStreams','movie_id'),
                    'poster' => array(self::HAS_ONE, 'Poster','object_id',
                    'condition'=>"object_type='films'"),
                    'trailer' => array(self::HAS_ONE, 'movieTrailer','movie_id'),
                    'live_stream' => array(self::HAS_MANY, 'Livestream','movie_id'),
                    'studio_content_type' => array(self::BELONGS_TO, 'StudioContentType', 'content_type_id'),                
		);
	}
        public function behaviors() {
            return array(
                'commentable' => array(
                    'class' => 'ext.comment-module.behaviors.CommentableBehavior',
                    // name of the table created in last step
                    'mapTable' => 'movies_comments_nm',
                    // name of column to related model id in mapTable
                    'mapRelatedColumn' => 'movieId'
                ),
			/*'sluggable' => array(
				'class'=>'ext.behaviors.SluggableBehavior',
				'columns' => array('name'),
				'unique' => true,
				'update' => true
            ),*/
           );
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'language' => 'Language',
			'rating' => 'Rating',
			'country' => 'Country',
			'genre' => 'Genre',
			'permalink' => 'Permalink',
			'alias_name' => 'Alias Name',
			'story' => 'Story',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'content_type_id' => 'Content Type',
			'censor_rating' => 'Censor Rating',
			'tags' => 'Tags',
			'movie_payment_type' => 'Movie Payment Type',
			'is_verified' => 'Is Verified',
			'is_merged' => 'Is Merged',
			'studio_id' => 'Studio',
			'ip' => 'Ip',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'last_updated_by' => 'Last Updated By',
			'last_updated_date' => 'Last Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('rating',$this->rating,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('genre',$this->genre,true);
		$criteria->compare('permalink',$this->permalink,true);
		$criteria->compare('alias_name',$this->alias_name,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('content_type_id',$this->content_type_id);
		$criteria->compare('censor_rating',$this->censor_rating,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('movie_payment_type',$this->movie_payment_type,true);
		$criteria->compare('is_verified',$this->is_verified);
		$criteria->compare('is_merged',$this->is_merged);
		$criteria->compare('studio_id',$this->studio_id);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_updated_by',$this->last_updated_by);
		$criteria->compare('last_updated_date',$this->last_updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Films the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
/**
 * @method public addcontent() Add a content to films tables based on the parameters
 * @return int $last_insert_id
 * @author GDR<support@muvi.com> 
 */
	function  addContent($data,$studio_id=''){
        
		$permalinkins = ($data['permalink']!='')?$data['permalink']:$data['name'];
		$this->studio_id = $studio_id?$studio_id : Yii::app()->user->studio_id;
		$this->name = $data['name'];
		$uniqid = Yii::app()->common->generateUniqNumber();
		$this->uniq_id = $uniqid;
		if(isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
			$this->release_date = date('Y-m-d', strtotime($data['release_date']));
		
		$this->start_time = (@$data['start_time'])?gmdate('Y-m-d H:i:s', strtotime(@$data['start_time'])):NULL;
		$this->duration = (@$data['duration'])?gmdate('Y-m-d H:i:s', strtotime(@$data['duration'])):NULL;
		$this->language = json_encode(@$data['language']);
		$this->genre = json_encode(@$data['genre'],JSON_UNESCAPED_UNICODE);
		$this->censor_rating = is_array(@$data['censer_rating'])?json_encode(array_values(array_unique($data['censer_rating']))):@$data['censer_rating'];
		$this->story = @$data['story'];
		$this->created_date = gmdate('Y-m-d H:i:s');
		$this->last_updated_date = gmdate('Y-m-d H:i:s');
		
		$this->content_types_id = $data['content_types_id'];
		$this->parent_content_type_id = $data['parent_content_type'];
		$this->content_category_value = implode(',',$data['content_category_value']);
		if(@$data['content_subcategory_value'])
			$this->content_subcategory_value = implode(',',$data['content_subcategory_value']);
		$this->permalink = Yii::app()->general->generatePermalink(stripslashes($permalinkins));
		$this->status = 1;
		
		$this->custom1 = @$data['custom1']?(is_array(@$data['custom1'])?json_encode(@$data['custom1']):@$data['custom1']):'';
		$this->custom2 = @$data['custom2']?(is_array(@$data['custom2'])?json_encode(@$data['custom2']):@$data['custom2']):'';
		$this->custom3 = @$data['custom3']?(is_array(@$data['custom3'])?json_encode(@$data['custom3']):@$data['custom3']):'';
		$this->custom4 = @$data['custom4']?(is_array(@$data['custom4'])?json_encode(@$data['custom4']):@$data['custom4']):'';
		$this->custom5 = @$data['custom5']?(is_array(@$data['custom5'])?json_encode(@$data['custom5']):@$data['custom5']):'';
		$this->custom6 = @$data['custom6']?(is_array(@$data['custom6'])?json_encode(@$data['custom6']):@$data['custom6']):'';
		$this->custom7 = @$data['custom7']?(is_array(@$data['custom7'])?json_encode(@$data['custom7']):@$data['custom7']):'';
		$this->custom8 = @$data['custom8']?(is_array(@$data['custom8'])?json_encode(@$data['custom8']):@$data['custom8']):'';
		$this->custom9 = @$data['custom9']?(is_array(@$data['custom9'])?json_encode(@$data['custom9']):@$data['custom9']):'';
		$this->custom10 = @$data['custom10']?(is_array(@$data['custom10'])?json_encode(@$data['custom10']):@$data['custom10']):'';
		if(@$data['custom_metadata_form_id'])
			$this->custom_metadata_form_id = $data['custom_metadata_form_id'];
		$this-> setIsNewRecord(true);
		$this-> setPrimaryKey(NULL);
		if($this->save()){
                    $content_id = $this->id;
                    $film = Film::model()->findByPk($content_id);
                    $film->search_parent_id = $content_id;
                    $film->save();
			return $this->attributes;
		}else{
			return '';
		}
	}
        
        public function getAllContent($id)
        {
            $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from($this->tableName())
                    ->where('studio_id=:id AND parent_id=0',array(':id' => $id))
                    ->queryAll();
            return $data;
        }
/**
 * 
 * @method public getContentList() It will return the list of content with all details in a array format
 * @param array $content Content array with all required information
 * @param array $arg Array of reqired basic information 
 * @author Gayadhar<support@muvi.com>
 * @return array array of content data
 */		
function getContentListData($contentList, $catVlaue, $customArr = array(), $studio_id = '', $language_id = '', $themes = '', $translate = '', $idSeq = '') {
		$api_available = $controller->login_with;
		$getStudioApi = ExternalApiKey::model()->checkRegisterApi();

		if (!empty($getStudioApi)) {
			$chk_registeration = 1;
		}
		$getPerimissionApi = ExternalApiKey::model()->chkPermissionApi();
		$controller = Yii::app()->controller;
		if (!$translate) {
			$translate = $controller->Language;
		}
		if (!$themes) {
			$themes = $controller->studio->parent_theme;
		}
		if ($user_id == false) {
			$user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
		}
		if (!$studio_id) {
			$studio_id = $controller->studio->id;
		}
		if (!$language_id) {
			$language_id = $controller->language_id;
		}
		$j = 0;
		$isEnableAllPlayAud = 0;
		$allAudioPlay = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'get_all_episode_play');
		if (!empty($allAudioPlay)) {
			$isEnableAllPlayAud = $allAudioPlay['config_value'];
		}
		foreach ($contentList AS $cvalues) {
			if (@$cvalues['is_episode'] == 1) {
				if ($cvalues['mapped_stream_id']) {
					$mapped_stream_id .= $cvalues['mapped_stream_id'] . ",";
				} else {
					$streamIds .= $cvalues['movie_stream_id'] . ",";
				}
			} else {
				if ($cvalues['mapped_id']) {
					$mapped_id .= $cvalues['mapped_id'] . ",";
				} else {
					$movie_ids .= $cvalues['movie_id'] . ",";
				}
			}
		}
		$postObj = new Poster();
		$pdata = array();
		if (@$movie_ids) {
			$movie_ids = trim($movie_ids, ',');
			$pdata = $postObj->getPosterByids($movie_ids, 'films', $studio_id, 'standard');
		}
		if (@$mapped_id) {
			$mapped_id = trim($mapped_id, ',');
			$pdata1 = $postObj->getPosterByids($mapped_id, 'films', $studio_id, 'standard', 1);
			if ($pdata1)
				$pdata = @$pdata + @$pdata1;

			$criteria = new CDbCriteria();
			$criteria->select = 'id,movie_id , is_converted,video_duration,full_movie';
			$criteria->condition = 'movie_id IN(' . $mapped_id . ')';
			$streams = movieStreams::model()->findAll($criteria);

			foreach ($streams AS $skey => $sval) {
				$streamContent[$sval->movie_id] = $sval->attributes;
			}
		}
		if ($streamIds) {
			$streamIds = trim($streamIds, ',');
			$pdata2 = $postObj->getPosterByids($streamIds, 'moviestream', $studio_id, 'episode');
			if ($pdata2)
				$pdata = @$pdata + @$pdata2;
		}
		if ($mapped_stream_id) {
			$mapped_stream_id = trim($mapped_stream_id, ',');
			$pdata3 = $postObj->getPosterByids($mapped_stream_id, 'moviestream', $studio_id, 'episode', 1);
			if ($pdata3)
				$pdata = @$pdata + $pdata3;
		}
		$DefaultPoster = $postObj->getDefaultPoster($themes, $studio_id);

		foreach ($contentList AS $ckey => $contentdetails) {
			$season_number = 0;
			$parent_content_title = $topBanner = "";
			$content_details = array();
			$content = (object) $contentdetails;
			$movie_id = $content_id = $content->movie_id;
			if ($content->is_episode == 1) {
				$content_id = $content->movie_stream_id;
			}
			$langcontent = array();
			if ($language_id != 20) {
				$langcontent = Yii::app()->custom->getTranslatedContent($content_id, @$content->is_episode, $language_id, $studio_id);
			}
			$custom_vals = array();
			$is_live = 0;

			if ($content->mapped_id) {
				$map_movieid = $content->mapped_id;
				$stream = $streamContent[$map_movieid];
				$is_converted = $stream['is_converted'];
				$stream_id = $stream['id'];
				$full_movie = $stream['full_movie'];
				$video_duration = $stream['video_duration'];
			} else {
				$map_movieid = $movie_id;
				$is_converted = $content->is_converted;
				$stream_id = $content->movie_stream_id;
				$full_movie = $content->full_movie;
				$video_duration = $content->video_duration;
			}
			if ($content->feed_url != '')
				$is_live = 1;
			if (array_key_exists($movie_id, @$langcontent['film'])) {
				//$content = Yii::app()->Helper->getLanuageCustomValue($content,@$langcontent['film'][$movie_id]);
				$content->name = @$langcontent['film'][$movie_id]->name;
				$content->story = @$langcontent['film'][$movie_id]->story;
				$content->genre = @$langcontent['film'][$movie_id]->genre;
				$content->censor_rating = @$langcontent['film'][$movie_id]->censor_rating;
				$content->language = $langcontent['film'][$movie_id]->language;
			}
			$ses = array();$purchaseType='';
			if (@$content->is_episode == 1) {
				if (array_key_exists($content->movie_stream_id, @$langcontent['episode'])) {
					//$content = Yii::app()->Helper->getEpisodeLanguageCustomValue($content,@$langcontent['episode'][$stream_id]); 
					$content->episode_title = $langcontent['episode'][$stream_id]->episode_title;
					$content->episode_story = $langcontent['episode'][$stream_id]->episode_story;
				}
				$content_title = ($content->episode_title != '') ? $content->episode_title : "SEASON " . $content->series_number . ", EPISODE " . $content->episode_number;
				$content_name = ($content->episode_title != '') ? $content->episode_title : "SEASON " . $content->series_number . ", EPISODE " . $content->episode_number;
				$story = $content->episode_story;
				$release = $content->episode_date;
				if (@$pdata[$stream_id]) {
					$posterUrl = $pdata[$stream_id]['poster_url'];
				} else {
					$posterUrl = $DefaultPoster['horizontal_poster'];
				}
				$season_number = @$content->series_number;
				$parent_content_title = @$content->name;
			} else {
				$topBanner = Yii::app()->controller->getPoster($movie_id, 'topbanner', 'original', $studio_id);
				$content_title = $content_name = $content->name;
				$release = $content->release_date;
				$story = $content->story;
				if (@$content->content_types_id == 3) {
					$content_details = Yii::app()->controller->getContentSeasonDetails($movie_id, $studio_id);
					$EpDetails = Yii::app()->controller->getEpisodeToPlay($movie_id, $studio_id);
					if ($EpDetails) {
						$is_converted = 1;
					}
					$seasons = Yii::app()->general->getSeasonsToPlay($movie_id);
					foreach ($seasons as $season) {
						if (isset($season->series_number) && intval($season->series_number)) {
							$ses[] = array('series_number' => $season->series_number);
						}
					}
					$purchaseType = 'season';
				}
				$audAllPlay = 0;
				if ($content->content_types_id == 6 && $isEnableAllPlayAud > 0) {
					$audAllPlay = Yii::app()->controller->getAudioPlayable($movie_id, $studio_id);
				}
				if ($content_types_id == 2 || $content_types_id == 4) {
					if (@$pdata[$map_movieid]) {
						$posterUrl = str_replace('/standard/', '/episode/', $pdata[$map_movieid]['poster_url']);
						$posterOriginal = str_replace('/standard/', '/original/', $pdata[$map_movieid]['poster_url']);
					} else {
						$posterUrl = $posterOriginal = $DefaultPoster['horizontal_poster'];
					}
				} else {
					if (@$pdata[$map_movieid]) {
						$posterUrl = $pdata[$map_movieid]['poster_url'];
						$posterOriginal = str_replace('/standard/', '/original/', $pdata[$map_movieid]['poster_url']);
					} else {
						$posterUrl = $posterOriginal = $DefaultPoster['vertical_poster'];
					}
				}
			}
			$stream_uniq_id = 0;
			$payment_type = $adv_payment = $is_ppv_bundle = 0;
			$content_types_id = $content->content_types_id;

                        
                        $arg['studio_id'] = $studio_id;
                        $arg['movie_id'] = $movie_id;
                        $arg['season_id'] = $season_number;
                        $arg['episode_id'] = $stream_id;
                        $arg['content_types_id'] = $content_types_id;
                        $arg['user_id'] = $user_id;
                        $arg['default_currency_id'] = $controller->studio->default_currency_id;
                        $arg['is_monetizations_menu'] = 1;
			$isFreeContent = Yii::app()->common->isFreeContent($arg);
                        
			$permalink = Yii::app()->getbaseUrl(true) . '/' . $content->permalink;
			$release_date = Yii::app()->general->formatReleaseDate($release);
			$full_release_date = Yii::app()->general->formatFullReleaseDate($release);
			$play_btn = '';
			$fpermalink = $content->permalink;
			$content_unique_id = $content->uniq_id;
                        $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
			if ($content_types_id != 5 && $content_types_id != 6) {
				if ($is_converted == 1 || $content_types_id == 4) {
					if ($content->is_episode == 1) {
						if ($user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playbtn" data-ctype="' . $content_types_id . '">';
						} else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                                                         $play_btn = '<a href="javascript:void(0);" onclick="episodeplayMovie(this);" data-season_id="' . $season_number . '" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '" data-isppv="0" class="playbtn allplay" data-ctype="' . $content_types_id . '">';                      
                                                }else {
							$play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-content_title="' . $content_title . '" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '" class="playbtn allplay" data-ctype="' . $content_types_id . '">';
						}
						if (!empty($getPerimissionApi) && $user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-stream_id="' . $content->embed_id . '" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">';
						}
					} else if ($content_types_id == 3) {
						if ($user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="season" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
						} else {
							$play_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-content_title="' . $content_title . '" data-backdrop="static" data-purchase_type="season" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="' . $is_ppv_bundle . '" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn" >';
						}
					} else {
						if ($user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="' . $purchaseType . '"  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						} else if($std_config['config_value'] == 0 && $isFreeContent == 1){
                                                        $play_btn = '<a href="javascript:void(0);" onclick="playMovie(this);" data-movie_id="' . $content->uniq_id . '" data-purchase_type=""  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="0" class="playbtn allplay"  data-ctype="' . $content_types_id . '">';
						}
                                                else {
							$play_btn = '<a href="javascript:void(0);" data-season_id="3" data-toggle="modal" data-target="#loginModal" data-content_title="' . $content_title . '" data-chk_register="' . $chk_registeration . '" data-api_available="' . $api_available . '" data-backdrop="static" data-purchase_type="' . $purchaseType . '"  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}
						if (!empty($getPerimissionApi) && $user_id > 0) {
							$play_btn = '<a href="javascript:void(0);" onclick="chkPlayPerimission(this);" data-content-permalink="' . $fpermalink . '" data-content_title="' . $content_title . '" data-stream_id="0" data-purchase_type="season"  data-movie_id="' . $content->uniq_id . '"  data-name="' . $content_name . '" class="playbtn"  data-ctype="' . $content_types_id . '">';
						}
					}
					
					$play_btn .= $translate['play_now'] . '</a>';
					if ($play_btn != "") {
						$play_btn .= '<input type="hidden" name="permalink" id="permalink" value="' . $fpermalink . '" />';
						$play_btn .= '<input type="hidden" name="content_name" id="content_name" value="' . $content_name . '" />';
					}
				}
			} else {
				if ($is_converted == 1 && $content_types_id == 5) {
					$play_btn = '<a href="javascript:void(0);" class="playbtn" onclick="playAudio(this)" data-content_type="' . $content->is_episode . '" id="' . $content->movie_id . '" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playaudio" data-ctype="' . $content_types_id . '">';
					$play_btn .= $translate['play_now'] . '</a>';
				}
				if ($audAllPlay > 0 && $content_types_id == 6) {
					$play_btn = '<a href="javascript:void(0);" class="playbtn" onclick="playAllAudio(this)" data-content_type="' . $content->is_episode . '" id="' . $content->movie_id . '" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playaudio" data-index="0" data-ctype="' . $content_types_id . '">';
					$play_btn .= $translate['play_all'] . '</a>';
				}
			}
			if ($themes == 'classic-byod') {
				//$cast_detail = Yii::app()->general->getCasts($movie_id, 1, $language_id, $studio_id, $translate);
				//$casts = $cast_detail['casts'];
				//$casting = $cast_detail['casting'];
				$short_story = Yii::app()->common->htmlchars_encode_to_html(Yii::app()->general->formattedWords($story, 200));
			}
			$genre = json_decode($content->genre);
			if (!is_array($genre) && $content->genre) {
				if (!in_array(trim($content->genre), array('null', 'NULL', '[', '""'))) {
					$genre = explode(",", $content->genre);
				}
			}


			if (isset($customArr) && count($customArr) > 0) {
				foreach ($customArr as $ar) {
					$ar_k = trim($ar['f_id']);
					$ar_k1 = trim($ar['field_name']);
					$k = (int) str_replace('custom', '', $ar_k1);
					if ($k > 9) {
						if (@$content->custom10) {
							$x = json_decode($content->custom10, true);
							foreach ($x as $key => $value) {
								foreach ($value as $key1 => $value1) {
									$custom_vals[$ar_k] = array(
										'field_display_name' => trim($ar['field_display_name']),
										'field_value' => trim($value1)
									);
								}
							}
						}
					} else {
						$custom_vals[$ar_k] = array(
							'field_display_name' => trim($ar['field_display_name']),
							'field_value' => trim(is_array(json_decode($content->$ar_k1)) ? implode(', ', json_decode($content->$ar_k1)) : $content->$ar_k1)
						);
					}
				}
			}

			if ($content->is_downloadable && ($content->thirdparty_url == '')) {
				if ($is_converted == 1) {
					if ($content->is_episode == 1) {
						if ($user_id > 0) {
							$download_btn = '<a href="javascript:void(0);" onclick="episodegetPpvPlans(this);" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '"  class="playbtn" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '">';
						} else {
							$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="' . $content->embed_id . '" class="playbtn allplay" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '">';
						}
					} else {
						if ($user_id > 0) {
							$download_btn = '<a href="javascript:void(0);" onclick="getPpvPlans(this,1);" data-movie_id="' . $content->uniq_id . '" data-purchase_type="' . $purchaseType . '"  data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						} else {
							$download_btn = '<a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-backdrop="static" data-purchase_type="' . $purchaseType . '"  data-movie_id="' . $content->uniq_id . '" data-content-permalink="' . $fpermalink . '" data-stream_id="0" data-isppv="1" data-is_ppv_bundle="" data-name="' . $content_name . '" data-download="' . $content->is_downloadable . '" data-ctype="' . $content_types_id . '" class="playbtn">';
						}
					}
					$download_btn .= $translate['download'] . '</a>';
				}
			}
			$final_content = array(
				'movie_id' => $movie_id,
				'content_category_value' => $content->content_category_value,
				'title' => $content_name,
				'content_title' => $content_title,
				'is_episode' => @$content->is_episode,
				'episode_number' => @$content->episode_number,
				'season_number' => @$season_number,
				'parent_content_title' => @$parent_content_title,
				'content_details' => $content_details,
				'play_btn' => $play_btn,
				'buy_btn' => @$buy_btn,
				'permalink' => $permalink,
				'poster' => $posterUrl,
				'top_banner' => $topBanner,
				'data_type' => ($content->content_types_id == 3) ? 1 : 0,
				'is_landscape' => ($content->content_types_id == 2) ? 1 : 0,
				'release_date' => $release_date,
				'full_release_date' => $full_release_date,
				'censor_rating' => (!is_array(json_decode($content->censor_rating))) ? @$content->censor_rating : implode(',', json_decode($content->censor_rating)) . '&nbsp;',
				'movie_uniq_id' => $content->uniq_id,
				'stream_uniq_id' => $stream_uniq_id,
				'video_duration' => $video_duration,
				'video_duration_text' => Yii::app()->general->videoDurationText($video_duration),
				'ppv' => @$ppv_id,
				'payment_type' => @$payment_type,
				'is_converted' => $is_converted,
				'movie_stream_id' => $stream_id,
				'uniq_id' => $content_unique_id,
				'content_types_id' => $content_types_id,
				'ppv_plan_id' => $content->ppv_plan_id,
				'full_movie' => $full_movie,
				'story' => utf8_encode(Yii::app()->general->showHtmlContents($story)),
				'short_story' => utf8_encode($short_story),
				'genres' => $genre,
				'display_name' => utf8_encode(@$catVlaue['display_name']),
				'content_permalink' => @$catVlaue['category_permalink'],
				'casts' => @$casts,
				'casting' => @$casting,
				'custom' => @$custom_vals,
				'buy_btn' => @$buy_btn,
				'seq_feat' => $idSeq[$j],
				'is_downloadable' => @$content->is_downloadable,
				'download_btn' => @$download_btn
			);
			if (count($custom_vals) > 0) {
				$final_content = array_merge($final_content, @$custom_vals);
			}
			if (!empty($idSeq)) {
				$data[$idSeq[$j]] = $final_content;
			} else {
				$data[] = $final_content;
			}
			$j++;
		}
		return $data;
	}

	function _generateContentPlayBtn($pgateway,$monetizationMenuSettings=array(),$ppvData=array(),$arg=array()) {
		if($pgateway){
			if(isset($monetizationMenuSettings['menu']) && !empty($monetizationMenuSettings['menu']) && ($monetizationMenuSettings['menu'] & 8)){
				$isFreeContent = Yii::app()->common->isFreeContent($arg);
				if($isFreeContent){
					
				}
			}
			if(isset($monetizationMenuSettings['menu']) && !empty($monetizationMenuSettings['menu']) && ($monetizationMenuSettings['menu'] & 2)){
				if(@$ppvData['ppv_plan_id'] && intval($ppvData['ppv_plan_id'])){
					$pl = new PpvPlans();
					$ppv = $pl->findByAttributes(array('id' => $ppvData['ppv_plan_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
				}elseif($ppvData['content_types_id'] != 3 && $ppvData['single_ppv_plan']){
					$ppv = $ppvData['single_ppv_plan'];
				}elseif($ppvData['content_types_id'] == 3 && @$ppvData['multi_ppv_plan']){
					$ppv = $ppvData['multi_ppv_plan'];
				}
				$payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
			}
		}else{
			return '';
		}
	}
    public function getAvailableContent($cat=''){
        $dbcon = Yii::app()->db;
        $sql ="select GROUP_CONCAT(id) AS content_id FROM films where content_category_value in (".$cat.") ";
        $data = $dbcon->createCommand($sql)->queryRow();
        return $data;        
    }
    public function getcontentNameFromId($id = ''){
        $dbcon = Yii::app()->db;
        $sql ="select GROUP_CONCAT(name) AS content_name FROM films where id in(".trim($id,',').") ";
        $data = $dbcon->createCommand($sql)->queryRow();
        return $data['content_name'];        
    }

    public function getContentID($studio_id, $criteria, $category, $limit, $content_type, $content_types) {
        $cond = '';
        $condforEpisode = '';
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if (in_array(0, $category)) {
            $cond = '';
        } else {
            foreach($category as $cat){
                $cond .= 'FIND_IN_SET (' . $cat . ', F.content_category_value) OR ';
            }
            $cond = rtrim($cond, 'OR ');
			if($cond)
				$cond = " AND ($cond) "; 
        }
        // set conditions for content types
        if(!in_array(0,$content_types) && !in_array(10,$content_types)){
            $condforEpisode = ' AND M.is_episode=0';
        }else if(count($content_types) == 1 && in_array(10,$content_types)){
            $condforEpisode = ' AND M.is_episode=1';
        }
        if ($content_type == 2) {
            if(in_array(0,$content_types) || !$content_types){
                $cond .= ' AND F.content_types_id IN ("5","6","8")';
            }else if(count($content_types) == 1 && in_array(10,$content_types)){
                $cond .= ' AND F.content_types_id IN ("6")';
            }else if(is_array($content_types)){
                if(($key = array_search('10',$content_types)) !== false){
                    unset($content_types[$key]);
                }
                $content_types = implode(',', $content_types);
                $cond .= ' AND F.content_types_id IN (' . $content_types . ')';
            }
//            else{
//                $cond .= ' AND F.content_types_id IN ("5","6","8")';
//            }
        } else if($content_type == '0') {
            if(in_array(0,$content_types) || !$content_types){
                $cond .= ' AND F.content_types_id IN ("1","3","4")';
            }else if(count($content_types) == 1 && in_array(10,$content_types)){
                $cond .= ' AND F.content_types_id IN ("3")';
            }else{
                if(($key = array_search('10',$content_types)) !== false){
                    unset($content_types[$key]);
                }
                $content_types = implode(',', $content_types);
                $cond .= ' AND F.content_types_id IN (' . $content_types . ')';
            }
        }
        if(isset($_REQUEST['user_id']) && $_REQUEST['user_id']){
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        }else{
            $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
        }
        switch ($criteria) {
            case "1": // Most Viewed
                $data = array();
                $maximumViewed = VideoLogs::model()->getMaximumViewd($studio_id, $limit, $cond);
                foreach (array_values($maximumViewed) as $view) {
                    $movie_id[] = $view['movie_id'];
                }
                $maxViewed = implode(',', $movie_id);
                if(!empty($movie_id)){
                    $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.episode_parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' AND M.movie_id IN (' . $maxViewed . ')'.$condforEpisode.' limit ' . $limit . '';
                $data = Yii::app()->db->createCommand($sql_data)->queryAll();
                }
                return $data;
                break;
            case "2": // Top Rated
                $sqlData = $sql = 'SELECT c.content_id as movie_id,c.rating,c.is_episode as is_episode FROM content_ratings c, films F  where c.studio_id=' . $studio_id . ' AND c.content_id=F.id AND F.parent_id = 0 AND F.status = 1 ' . $cond . ' ORDER BY rating DESC LIMIT ' . $limit . '';
                $topRated = Yii::app()->db->createCommand($sqlData)->queryAll();
                return $topRated;
                break;
            case "3": // Latest Uploaded
                $cond .= $condforEpisode;
                $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.episode_parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '  order by M.id desc limit ' . $limit . '';
                $data = Yii::app()->db->createCommand($sql_data)->queryAll(); //print_r($data); exit;
                return $data;
                break;
            case "4": //Content Category
            case "0": //None
                $cond .= $condforEpisode;
                $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.episode_parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '  limit ' . $limit . '';
                $data = Yii::app()->db->createCommand($sql_data)->queryAll(); //print_r($data); exit;
                return $data;
                break;
            case "5":
                return "Latest Movies, TV Series";
                break;
            case "6": // My Library
                if($user_id > 0){
                    $cond .= $condforEpisode;
                    $myLibrary = array();
                    $myLibrary = $this->getLibrary($cond, $limit); 
                    $myLibrary = array_filter($myLibrary);
                    if(!empty($myLibrary)){
                        $myLibrary = array_values($myLibrary);
                    }
                    return $myLibrary;
                }
                break;
            case "7": // My Favourite   
                if($user_id > 0){
                    $sqlData = 'SELECT uf.content_id as movie_id,uf.content_type as is_episode FROM user_favourite_list uf, films F  where uf.studio_id=' . $studio_id . ' AND uf.user_id=' . $user_id . ' AND uf.content_id=F.id AND F.parent_id = 0 AND F.status = 1 ' . $cond . ' LIMIT ' . $limit . '';
                    $getFavorutes = Yii::app()->db->createCommand($sqlData)->queryAll();
                    return $getFavorutes;
                }
                break;
            case "8": // Last Seen
                if($user_id > 0){
                    $data = array();
                    $lastSeen = VideoLogs::model()->getlastSeen($studio_id, $limit, $cond);
                     if(!empty($lastSeen)){
                        foreach (array_values($lastSeen) as $view) {
                            if ($view['episode_id']) {
                                $streamIds .= "," . $view['episode_id'];
                            } else {
                                $movieIds .= "," . $view['movie_id'];
                            }
                            $cond = '';
                            if (@$movieIds || @$streamIds) {
                                $cond = " AND (";
                                if (@$movieIds) {
                                    $movieIds = ltrim($movieIds, ',');
                                    $cond .= "(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                                    $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                                }
                                if ($streamIds) {
                                    $streamIds = ltrim($streamIds, ',');
                                    $cond .= " (M.id IN(" . $streamIds . "))";
                                    $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                                } else {
                                    $cond = str_replace(' OR ', ' ', $cond);
                                }
                                $cond .= ")";
                            }
                        }
                            $cond .= $condforEpisode;
                        $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.episode_parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . ' limit ' . $limit . '';
                        $data = Yii::app()->db->createCommand($sql_data)->queryAll();
                    }
                    return $data;
                }
                break;
        }
    }

    public function getLibrary($cond, $limit) {
        $test999 = array();
        $library = array();
        if ($cond && $limit) {
            $statusppv = 0;
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = (isset(Yii::app()->user->id) && Yii::app()->user->id > 0) ? Yii::app()->user->id : 0;
            $transactions = Yii::app()->db->createCommand()
                    ->select('plan_id,ppv_subscription_id,transaction_type')
                    ->from('transactions')
                    ->where('studio_id = ' . $studio_id . ' AND user_id=' . $user_id . ' AND transaction_type IN (2,3,5,6)')
                    ->order('id DESC')
                    ->limit($limit)
                    ->queryAll();
            if (count($transactions)) {
                foreach ($transactions as $key => $details) {
                    $ppv_subscription = PpvSubscription::model()->find('id=:id', array(':id' => $details['ppv_subscription_id']));
                    $ppv_subscription->end_date = $ppv_subscription->end_date?$ppv_subscription->end_date=='1970-01-01 00:00:00'?'0000-00-00 00:00:00':$ppv_subscription->end_date:'0000-00-00 00:00:00';
                    //$film = Film::model()->find('id=:id', array(':id' => $ppv_subscription->movie_id));
                    $movie_id = $ppv_subscription->movie_id;
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($ppv_subscription->end_date);
                    if ($details['transaction_type'] == 2) {//PPV // Voucher
                        if ($currdatetime <= $expirytime && (strtotime($ppv_subscription->end_date) != '' || $ppv_subscription->end_date != '0000-00-00 00:00:00')) {
                            $statusppv = 1;
                        } else if ($ppv_subscription->end_date == '0000-00-00 00:00:00') {
                            $statusppv = 1;
                        }
                        if ($statusppv) {
                            //$test999[] = $ppv_subscription->movie_id;
                            $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND F.id=' . $ppv_subscription->movie_id . ' AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '';
                            $library[] = Yii::app()->db->createCommand($sql_data)->queryAll();
                        }
                    } else if ($details['transaction_type'] == 3) {
                        //$test999[] = $ppv_subscription->movie_id;
                        $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND F.id=' . $ppv_subscription->movie_id . ' AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '';
                        $library[] = Yii::app()->db->createCommand($sql_data)->queryAll();
                    } else if ($details['transaction_type'] == 5) {//PPV Bundle
                        $ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $details['ppv_subscription_id']))->timeframe_id;
                        $ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid))->validity_days;
                        $ppvbundlesmovieid = Yii::app()->db->createCommand("SELECT content_id FROM ppv_advance_content WHERE ppv_plan_id=" . $details['plan_id'])->queryAll();
                        $expirydate = date('M d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
                        if ($currdatetime <= $expirytime) {
                            $statusbundles = 'Active';
                            foreach ($ppvbundlesmovieid as $keyb => $contentid) {
                                //$test999[] = $contentid['content_id'];
                                $movie_id = $contentid['content_id'];
                                $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND F.id=' . $movie_id . ' AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '';
                                $library[] = Yii::app()->db->createCommand($sql_data)->queryAll();
                            }
                        }
                    }
                    else if($details['transaction_type'] == 6){ //Voucher
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($ppv_subscription->end_date);
                        $statusvoucher = 'N';
                        if ($currdatetime <= $expirytime && $ppv_subscription->end_date != '0000-00-00 00:00:00' && strtotime($ppv_subscription->end_date) != "") {
                            $statusvoucher = 'A';
                        } else if ($ppv_subscription->end_date != '0000-00-00 00:00:00' && strtotime($ppv_subscription->end_date) != "") {
                            $statusvoucher = 'A';
                        } else if ($ppv_subscription->end_date == '0000-00-00 00:00:00') {
                            $statusvoucher = 'A';
                        }
                        if ($statusvoucher == 'A') {
                            $sql_data = 'SELECT M.movie_id,M.is_converted,M.is_episode as is_episode, M.id as stream_id FROM movie_streams M,films F WHERE F.parent_id = 0 AND F.id=' . $ppv_subscription->movie_id . ' AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=' . $studio_id . ' ' . $cond . '';
                            $library[] = Yii::app()->db->createCommand($sql_data)->queryAll();
                        }
                    }
                }
            }
            return $library;
        }
    }

}
