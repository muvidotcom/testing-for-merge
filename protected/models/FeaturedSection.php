<?php
class FeaturedSection extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'featured_section';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
            'template'=>array(self::HAS_ONE, 'Template', 'template_id'),
            'contents'=>array(self::HAS_MANY, 'FeaturedContent', 'section_id'),
        );
    }    
    public function getFeaturedSections($studio_id, $language_id = 20, $limit = ""){
        if($limit){
            $limit_content = " LIMIT ".$limit;
        }
        $sections = $this->findAll('studio_id=:studio_id AND parent_id = 0 ORDER BY id_seq '.$limit_content, array(':studio_id' => $studio_id));
        if($language_id !=20){
            $translated = CHtml::listData($this->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
            if(!empty($sections)){
                foreach($sections as $section){
                    if (array_key_exists($section->id, $translated)) {
                       $section->title = $translated[$section->id];
                    }
                    $sectionsdetail[] = $section;
                }
            }
        }else{
            $sectionsdetail = $sections;
        }
        if($sectionsdetail){
            foreach ($sectionsdetail as $key => $sec) {
                $afsection[$key]['studio_id'] = $sec->studio_id;
                $afsection[$key]['language_id'] = $sec->language_id;
                $afsection[$key]['title'] = $sec->title;
                $afsection[$key]['id'] = $afsection[$key]['section_id'] =  $sec->id;
                $afsection[$key]['parent_id'] =  $sec->parent_id;
                $afsection[$key]['content_type'] = $sec->content_type;
                $afsection[$key]['section_type'] = $sec->section_type;
                $afsection[$key]['section_criteria'] = Yii::app()->custom->getSectionCriteria($sec->section_criteria);
                $afsection[$key]['content_limit'] = $sec->content_limit;
                if ($sec->section_type == 2) {
                    if (!$sec->section_category || $sec->section_category == 'null') {
                        $afsection[$key]['section_category'] = '';
                    } else if (in_array(0, json_decode($sec->section_category, true))) {
                        $afsection[$key]['section_category'] = "All";
                    } else {
                        $catIds = implode(',', json_decode($sec->section_category, true));
                        $afsection[$key]['section_category'] = ContentCategories::model()->getContent_category_name($catIds);
                    }
					$afsection[$key]['section_criteria_value'] = $sec->section_criteria;
					$afsection[$key]['content_types'] = $sec->content_types;
					$afsection[$key]['is_viewmore'] = $sec->is_viewmore;
					$afsection[$key]['section_category_id'] = $sec->section_category;
                }
                $total_content = AppFeaturedContent::model()->countByAttributes(array(
                    'section_id'=> $sec->id,
                    'studio_id' => $studio_id
                ));
                $afsection[$key]['total'] =  $total_content;
            }
            return $afsection;
        }
    }
    public function getSectionData($id, $language_id=20, $studio_id="") {
        if ($id) {
            if(!$studio_id){
                $studio_id = Yii::app()->common->getStudioId();
            }
            $sectionData = $this->findByPK($id);
            if($language_id != 20){
                $tfsections = $this->getSectionTranslated($id, $studio_id, $language_id);
                if(array_key_exists($id,$tfsections)){
                    $sectionData->title = $tfsections[$id];
                }
            }
            return $sectionData;
        }
    }
/**
 * @method public getFeaturedContent() It will return the list of featured contents with all details
 * @author Gayadhar<support@muvi.com>
 * @return array array of data
 */	
	function getFeaturedContents($studio_id=''){
		if($studio_id){
			$studio_id = Yii::app()->common->getStudiosId();
		}
		$offset = isset($_REQUEST['dataoffset']) ? $_REQUEST['dataoffset'] : 0;
        $limit = isset($_REQUEST['viewlimit']) ? $_REQUEST['viewlimit'] : 10;
        $themes = $_REQUEST['themes'] ? $_REQUEST['themes'] : '';
        $controller = Yii::app()->controller;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0 ? $_REQUEST['user_id'] : 0;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id . ' AND language_id=' . $language_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $sections = $this->findAll($criteria);
        $return = array();
        if ($sections) {
            $addQuery = '';
            $customFormObj = new CustomForms();
            $customData = $customFormObj->getFormCustomMetadat($studio_id);
            if ($customData) {
                foreach ($customData AS $ckey => $cvalue) {
                    $addQuery = ",F." . $cvalue['field_name'];
                    //$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
                    $customArr[] = array(
                        'f_id' => trim($cvalue['f_id']),
                        'field_name' => trim($cvalue['field_name']),
                        'field_display_name' => trim($cvalue['f_display_name'])
                    );
                }
            }
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
            $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";

            foreach ($sections as $section) {
                if(count($section->contents)){
                    $streamIds = '';
                    $movieIds = '';
                  $idSeq = '';
                    $idSeqplay = '';
                    $final_content = array();
                    $final_physical = array();
                    $Seq = array();
                    $total_contents = 0;
                    $is_playlist = 0;
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
                    } else {
                        foreach ($section->contents as $featured) {
                            if ($featured->is_episode == 1) {
                                $streamIds .="," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            } elseif ($featured->is_episode == 4) {
                                $playlistId .= "," . $featured->movie_id;
                                $is_playlist = 1;
                                $is_episode = $featured->is_episode;
                                $idSeqplay .= "," . $featured->id_seq;
                            }else {
                                $movieIds .= "," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            }
                        }
                        $cond = '';
                        if (@$movieIds || @$streamIds) {
                            $cond = " AND (";
                            if (@$movieIds) {
                                $movieIds = ltrim($movieIds, ',');
                                $cond .="(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                                $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                            }
                            if ($streamIds) {
                                $streamIds = ltrim($streamIds, ',');
                                $cond .=" (M.id IN(" . $streamIds . "))";
                                $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                            } else {
                                $cond = str_replace(' OR ', ' ', $cond);
                            }
                            $cond .= ")";
                        }
						$cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                        $sql = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
                            . "( SELECT M.movie_id,M.is_converted,M.video_duration,M.is_episode,M.embed_id,M.is_downloadable,M.id AS movie_stream_id,F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id,M.episode_title,M.episode_date,M.episode_story,M.episode_number,M.series_number, M.last_updated_date " . $addQuery
                                . "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=" . $studio_id . " " . $cond . " ) AS PM "
                                . " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
                        $sql_data = "SELECT P.* FROM (SELECT t.*,g.* FROM (" . $sql . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                        $where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
                        $sql_data .= $where . " " . $orderBy;
                        $list = Yii::app()->db->createCommand($sql_data)->queryAll();
                         if ($is_playlist == 1) {
                            $Seqid = ltrim($idSeq, ',');
                            $Seq = explode(',', $Seqid);
                        }
                        $fobject = new Film();
                        $final_content = $fobject->getContentListData($list, $margs, $customArr, $studio_id, $language_id, $themes, $translate, $Seq);
                        if ($is_playlist == 1) {
                            $movieIds = ltrim($playlistId, ',');
                            $idSeqplay = ltrim($idSeqplay, ',');
                            $seqId = explode(',', $idSeqplay);
                            $command1 = Yii::app()->db->createCommand()
                                    ->select('id,playlist_name,playlist_permalink')
                                    ->from('user_playlist_name u')
                                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id IN(' . $movieIds . ') ORDER BY FIELD(u.id,' . $movieIds . ')');
                            $data = $command1->QueryAll();
                            $tot = count($final_content);
                            $k = 0;
                            if ($data) {
                                foreach ($data as $res) {
                                    $playlist_id = $res['id'];
                                    $playlist_name = $res['playlist_name'];
                                    $permalink = $res['playlist_permalink'];
                                    $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                                    $final_content[$seqId[$k]] = array(
                                        'movie_id' => $playlist_id,
                                        'title' => utf8_encode($playlist_name),
                                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                        'poster' => $poster,
                                        'is_episode' => $is_episode,
                                        'seq_feat' => $seqId[$k]
                                    );
                                    $k++;
                                    $tot ++;
                                }
                            }
                        }
                        ksort($final_content);
                        $final_content = array_values(array_filter($final_content));
                    }
                        $return[] = array(
                            'id' => $section->id,
                            'title' => utf8_encode($section->title),
                            'content_type' => $section['content_type'],
                            'total' => count($final_content),
                            'contents' => $final_content
                        );
                    
                }
            }
        }
		return $return;
	}
function getTransData($lang_code='en', $studio_id){
	$studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
	$theme = $studio->theme;
	if(!$lang_code)
		$lang_code = 'en';

        if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
           $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
        }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
            $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
        }else{
            $lang = include(ROOT_DIR . 'languages/studio/en.php');
        }
        $language = Yii::app()->controller->getAllTranslation($lang,$studio_id);
        return $language;
    }
    /**
     * @method getTranslatedFeaturedSection  get the translated name of a featured sections 
     * @param array $sections
     * @param Int $studio_id 
     * @param int $language_id
     * @author Biswajit parida<support@muvi.com>
     * @return array the translated name of a featured sections. 
     */
    public function getTranslatedFeaturedSections($sections = array(), $studio_id, $language_id = 20){
        $tfsections = array();
        if($sections):
            foreach($sections as $section):
                $section_ids[] = $section['id'];
            endforeach;
            $section_ids = implode(',', $section_ids);
            $tfsections = $this->getSectionTranslated($section_ids, $studio_id, $language_id);
        endif;
        return $tfsections;
    }
    public function getSectionTranslated($section_ids, $studio_id, $language_id){
        $tfsections = CHtml::listData($this->findAll(array('condition' => 'studio_id='.$studio_id.' AND language_id='.$language_id.' AND parent_id IN ('.$section_ids.')', 'select' => 'title,parent_id')), 'parent_id','title');
        return $tfsections;
    }
    /**
     * @method updateTranslatedSection   add/update the translated name of a featured sections 
     * @param array $section
     * @param Int $section_id
     * @param int $language_id
     * @author Biswajit parida<support@muvi.com>
     */
    public function updateTranslatedSection($section = array(), $section_id, $section_name, $studio_id, $language_id = 20){
        if($section_id && $section_name):
            $tsection = $this->findByAttributes(array('parent_id' => $section_id, 'studio_id' => $studio_id, 'language_id' => $language_id));
            if($tsection):
                $tsection->title = htmlspecialchars($section_name);
                $tsection->content_type = $section->content_type;
            else:
                $tsection = new FeaturedSection;
                $tsection->studio_id = $studio_id;
                $tsection->content_type = $section->content_type;
                $tsection->title = htmlspecialchars($section_name);
                $tsection->language_id = $language_id;
                $tsection->parent_id = $section_id;
                $tsection->id_seq = $section->id_seq;
                $tsection->is_active = 1;
                $tsection->is_viewmore = $section->is_viewmore;
            endif;
            return $tsection->save();
        endif;
    }
    public function getMaxOrd($studio_id = false){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $feat = $this->find(array(
            'select' => 'id_seq',
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id_seq DESC',
            'limit' => '1'
        ));   
        if(isset($feat) && count($feat)){
            return (int) $feat->id_seq;
        }
        else{
            return 0;
        }      
    }
}
