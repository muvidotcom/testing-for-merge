<?php

class PolicyRules extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'policy_rules';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'policy_rules_mapping' => array(self::HAS_MANY, 'PolicyRulesMapping', 'rules_id')
        );
    }

    public function saveRule($data, $rule) {
        $studio_id = Yii::app()->common->getStudiosId();
        if (!empty($rule) && $rule['id']) {
            $newRule = PolicyRules::model()->with('policy_rules_mapping')->findByPk($rule['id']);
            $newRule->updated_date = gmdate('Y-m-d H:i:s');
        } else {
            $newRule = new PolicyRules();
            $newRule->created_date = gmdate('Y-m-d H:i:s');
            $newRule->uniqid = Yii::app()->Helper->generateUniqNumber();
        }
        $newRule->studio_id = $studio_id;
        $newRule->name = $data['rule']['name'];
        $newRule->short_description = nl2br($data['rule']['short_description']);
        $newRule->created_by = Yii::app()->user->id;
        $newRule->updated_date = gmdate('Y-m-d H:i:s');
        $newRule->ip = CHttpRequest::getUserHostAddress();
        if ($newRule->save()) {
            if (!$newRule->id) {
                $newRule->id = Yii::app()->db->getLastInsertId();
            }
            PolicyRulesMapping::model()->deleteAll("rules_id ='" . $newRule->id . "' AND studio_id='" . $studio_id . "'");
            foreach ($data['policy']['value'] as $key => $policy) {
                if (trim($policy)) {
                    $policyMaping = new PolicyRulesMapping();
                    $policyMaping->policy_rules_master_id = $data['policy']['id'][$key];
                    $policyMaping->rules_id = $newRule->id;
                    $policyMaping->studio_id = $studio_id;
                    $policyMaping->policy_type = $data['policy']['name'][$key];
                    $policyMaping->duration_type = $data['policy']['duration_type'][$key];
                    $policyMaping->policy_value = $policy;
                    $policyMaping->created_date = gmdate('Y-m-d H:i:s');
                    $policyMaping->save();

                    /* save to policy_rules_log table */
                    $policyMapingLog = new PolicyRulesLog();
                    //$policyMapingLog->setAttributes($policyMaping);
                    $policyMapingLog->policy_rules_master_id = $data['policy']['id'][$key];
                    $policyMapingLog->rules_id = $newRule->id;
                    $policyMapingLog->studio_id = $studio_id;
                    $policyMapingLog->policy_type = $data['policy']['name'][$key];
                    $policyMapingLog->duration_type = $data['policy']['duration_type'][$key];
                    $policyMapingLog->policy_value = $policy;
                    $policyMapingLog->created_date = gmdate('Y-m-d H:i:s');
                    $policyMapingLog->user_id = Yii::app()->user->id;
                    $policyMapingLog->save();
                }
            }
            return $newRule->id;
        }
    }

    public function updateStatus($rules_id = null) {
        $response = array();
        $policyRule = PolicyRules::model()->findByPk($rules_id);
        $status = $policyRule['status'] == 1 ? 0 : 1;
        if (self::countAppliedRules($rules_id) > 0 && $status==0) {
            $response['message'] = "This rule already applied with some plan.";
            $response['error'] = true;
        } else {
            $policyRule->status = $status;
            $policyRule->save();
            $response['success'] = true;
            $response['status'] = $status;
        }
        return $response;
    }

    public function deleteRule($rules_id = null) {
        if(!$rules_id){
            $response['message'] = "This rule already applied with some plan.";
            $response['error'] = true;
            return $response;
        }
        $studio_id = Yii::app()->common->getStudiosId();
        $response = array();
        $isApplied = self::countAppliedRules($rules_id);
        if ($isApplied > 0) {
            $response['message'] = "This rule already applied with some plan.";
            $response['error'] = true;
        } else {
            PolicyRulesMapping::model()->deleteAll("rules_id ='" . $rules_id . "' AND studio_id='" . $studio_id . "'");
            if (PolicyRules::model()->deleteAll("id='" . $rules_id . "' AND studio_id='" . $studio_id . "'")) {
                $response['error'] = false;
            }
        }
        return $response;
    }

    function countAppliedRules($rules_id = null) {
        $content['count_rules'] = 0;
        if ($rules_id) {
            $con = Yii::app()->db;
            $sql = "SELECT count(1) as count_rules 
                    FROM (
                        SELECT rules_id as rules_id FROM ppv_plans WHERE is_deleted=0
                        union all
                        SELECT rules_id FROM voucher
                        union all
                        SELECT rules_id FROM subscription_plans
                        union all
                        SELECT rules_id FROM subscriptionbundles_plans
                    ) a
                    WHERE rules_id = '$rules_id'";
            $content = $con->createCommand($sql)->queryRow();
        }
        return $content['count_rules'];
    }

    public function getConfigRule($studio_id = null) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $studioConfig = StudioConfig::model()->find('studio_id=:studio_id AND config_key="enable_policy" AND config_value=1', array(':studio_id' => $studio_id));
        if (!count($studioConfig)) {
            return false;
        }
        return true;
    }

}
