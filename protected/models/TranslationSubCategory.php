<?php
class TranslationSubCategory extends CActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'translation_message_subcategory';
    }
}