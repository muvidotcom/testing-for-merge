<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class BlogPost extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'blog_posts';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::BELONGS_TO, 'Studio', 'studio_id'),
            'comments'=>array(self::HAS_MANY, 'BlogComment', 'post_id'),
        );
    } 
    
    public function findAllPosts($studio_id) {
        //Find All Blog Posts        
        $posts = BlogPost::model()->findAll(array(
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id ASC'
        ));          
        return($posts);
    }
    public function getTranslatedPost($studio_id, $language_id, $posts = array()){
        $post_ids = array();
        if($posts):
            foreach($posts as $post):
                $post_ids [] = $post['id'];
            endforeach;
        endif;
        $post_ids = implode(',', $post_ids);
        $translated_data = $this->getTranslatedBlogData($studio_id, $language_id, $post_ids);
        return $translated_data;
    }
    public function getTranslatedRelatedPost($studio_id, $language_id, $posts = array()){
        $post_ids = implode(',', $posts);
        $translated_data = $this->getTranslatedBlogData($studio_id, $language_id, $post_ids);
        return $translated_data;
    }
    public function getTranslatedBlogData($studio_id, $language_id, $post_ids){
        $translated_posts = array();
        $blog_posts = BlogPost::model()->findAll(array(
            'select' => 'post_title,post_content,post_short_content,parent_id',
            'condition' => 'studio_id=:studio_id AND language_id=:language_id AND parent_id IN ('.$post_ids.')',
            'params' => array(':studio_id' => $studio_id, ':language_id' => $language_id)
        ));
        if($blog_posts):
            foreach($blog_posts as $blog_post):
                $translated_posts[$blog_post->parent_id] = array(
                    'post_title' => $blog_post->post_title,
                    'post_content' => $blog_post->post_content,
                    'post_short_content' => $blog_post->post_short_content
                );
            endforeach;
        endif;
        return $translated_posts;
    }
}

