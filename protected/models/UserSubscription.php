<?php
class UserSubscription extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'user_subscriptions';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sdk_user'=>array(self::HAS_ONE, 'SdkUser', 'user_id'),
            'plan'=>array(self::HAS_ONE, 'SubscriptionPlans', 'plan_id'),
			'subscription_plan'=>array(self::BELONGS_TO, 'SubscriptionPlans', 'plan_id'),
        );
    }
    
    public function getSubscriptionDetails($user_id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('user_id =:user_id AND status =:status',array(':user_id' => $user_id,':status' => 1))
                ->queryRow();
        return $data;
    }
    
    public function deleteUserSubscription($user_id)
    {
        $data = Yii::app()->db->createCommand()
                ->delete($this->tableName(),'user_id=:user_id AND status=:status',array(':user_id' => $user_id,':status' => 1));
        return $data;
    }
    
    public function backupAllData($studio_id)
    {
        $dbcon = Yii::app()->db;
        $sql = "INSERT INTO ".$this->tableName()."_history SELECT * FROM ".$this->tableName()." WHERE studio_id=".$studio_id;
        $data = $dbcon->createCommand($sql)->execute();
        return $data;
    }
    
    public function getSubscriptionDetailsByProfileID($profile_id)
    {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('profile_id =:profile_id',array(':profile_id' => $profile_id))
                ->queryRow();
        return $data;
    }
     public function UserSubscriptionBundlesPlanExist($studio_id,$user_id,$planExists=NULL){
         if($planExists){
           $anyPlanexits='';  
         }else{
           $anyPlanexits="AND usb.status=1";   
         }
         $data = Yii::app()->db->createCommand()
                ->select('usb.user_id as user_id,sp.id as id,usb.plan_id,usb.currency_id,usb.start_date,usb.end_date,sp.is_subscription_bundle,sp.name,sp.short_desc,sp.trial_recurrence,sp.trial_period,sp.recurrence,usb.studio_payment_gateway_id,usb.profile_id,usb.card_id,usb.id as userId,usb.amount,usb.payment_key,usb.coupon_code')
                ->from($this->tableName()." usb")
                ->join('subscription_plans  sp' , 'usb.plan_id=sp.id')
                ->where('sp.studio_id =:studio_id AND usb.user_id=:user_id '.$anyPlanexits,array(':studio_id' => $studio_id,':user_id'=>$user_id))
                ->queryAll();
        return $data;
        
}
    public function IsUserTakenSubscriptionBundles($studio_id,$user_id,$planId){
         $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);  
        if($is_subscribed==1){
         $data = Yii::app()->db->createCommand()
                ->select('usb.plan_id as id,usb.start_date,usb.end_date,sp.recurrence,sp.trial_recurrence,sp.trial_period,sp.is_subscription_bundle,sp.name')
                ->from($this->tableName()." usb")
                ->join('subscription_plans  sp' , 'usb.plan_id=sp.id')
                ->join('sdk_users  sdu' , 'sdu.id=usb.user_id')    
                ->where('sp.studio_id =:studio_id AND usb.user_id=:user_id AND usb.status=0 AND sdu.is_deleted=1 AND usb.is_subscription_bundle=0 AND sp.is_subscription_bundle=0',array(':studio_id' => $studio_id,':user_id'=>$user_id))
                ->order('usb.id DESC')
                ->LIMIT(1)    
                ->queryAll();
        return $data;
    }
        
}
}
