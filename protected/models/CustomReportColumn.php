<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SeoInfo
 *
 * @author SKM<support@muvi.com>
 */
class CustomReportColumn extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return 'custom_report_column';
    }
    
    public function relations(){
        return array();
    }
   
    
}
