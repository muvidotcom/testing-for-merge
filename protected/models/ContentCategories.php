<?php
class ContentCategories extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'content_category';
    }
    
    /*Added by - RKS
     * To get the content category properties from Binary Value
     * @params: $studio_id, $binary_value of category, field names separated by comma to select
     */
    public function CategoryFromBinaryValue($studio_id = 0, $binary_value, $fields = 'category_name') {
        $fields = ContentCategories::model()->findAll(array(
            'select'=> $fields,
            'condition' => 'studio_id=:studio_id AND binary_value = :binary_value',
            'params' => array(':studio_id' => $studio_id, ':binary_value' => $binary_value),
            'order' => 't.id DESC',
            'limit' => '1'
        )); 
        return(@$fields[0]);        
    }    
	/**
	 * @method public categorynameFromCommavalues() It will return the category names based on the comma separated value
	 * @param string comma separated content category value
	 * @param array $contentCategory Content Category array with comma separated value
	 * @param string $returnType array or String
	 * @author Manas<manas@muvi.com>
	 * @return string Category list
	 */
	function categoryFromCommavalues($studio_id = 0, $category_value, $fields = 'category_name') {
		$fields = ContentCategories::model()->findAll(array(
			'select' => $fields,
			'condition' => 'studio_id=:studio_id AND id = :category_value',
			'params' => array(':studio_id' => $studio_id, ':category_value' => $category_value),
			'order' => 't.id DESC',
			'limit' => '1'
		));
		return(@$fields[0]);
	}
        function getCategories($studio_id)
        {
        $allcategories = Yii::app()->db->createCommand()
                ->select('*')
                ->from('content_category')
                ->where('studio_id=:studio_id',array(':studio_id'=>$studio_id))
                ->queryAll();
        $contentCategories = CHtml::listData($allcategories, 'id', 'category_name');  
        return $contentCategories;
        }
    public function getContent_category_name($id){
        $dbcon = Yii::app()->db;
        $sql ="select GROUP_CONCAT(category_name) AS cname FROM content_category where id in(".$id.") ";
        $data = $dbcon->createCommand($sql)->queryRow();
        return $data['cname'];
    }

    /*
     * @params: $studio_id
     * @author: Sanjib<support@muvi.com>
     * @return: array
     */
    public function getAllCategory($studio_id){
        $category = $this->findAll(array('select'=>'id, category_name', 'condition'=>'studio_id=:studio_id AND parent_id = 0', 'params'=>array(':studio_id' => $studio_id)));
        $contentCategories = CHtml::listData($category, 'id', 'category_name');
        return $contentCategories;
    }
}