<?php

class PGOrderDetails extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_order_details';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /*
     * This function is insert data into order details table
     */

    public function insertOrderDetails($orderid, $cart_item) {
        foreach ($cart_item as $item) {
            $this->order_id = $orderid;
            $this->product_id = $item['id'];
            $this->quantity = $item['quantity'];
            $this->price = $item['price'];
            $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $item['id']));
            $this->name = $item['name'];
            $this->discount = $pgproduct['discount'];
            $this->tax = '';
            $this->description = $pgproduct['description'];
            $pgproductimage = PGProductImage::model()->find('product_id=:id AND feature=1', array(':id' => $item['id']));
            $this->image_url = $pgproductimage['name'];
            $this->sub_total = ($item['price'] * $item['quantity']); //(price*qnt)+tax+shipping-discount
            $this->item_status = 1;//pending status
            $this->personalization_id = $item['personalization_id'];       
            $this->personalized_sku =($item['personalization_id']!=0)?($pgproduct['sku']."_".$orderid):NULL;
			$this->pg_varient_id = (isset($item['pg_varient_id']) && $item['pg_varient_id'])?$item['pg_varient_id']:NULL;       
            $this->varient_sku =(isset($item['pg_varient_id']) && $item['pg_varient_id'] && $item['sku'])?($item['sku']):NULL;
            $this->isNewRecord = true;
            $this->primaryKey = NULL;
            $this->save();
        }
    }
    /*
     * This function cancel an order at CDS
     * author ajit@muvi.in
     * date 22/8/2016
    */    
    public function CancelCDSOrder($studio_id,$order_number,$item_id){
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $site_id = $webservice->user_id;
        $password = $webservice->password;      
        $xml_data_cancelOrder = "
            <app:cancelOrder>
               <app:siteId>$site_id</app:siteId>
               <app:password>$password</app:password>
               <app:cancelOrder>
                      &lt;CancelBundle&gt;
                        &lt;Cancel&gt;
                        &lt;CancelLines&gt;
                            &lt;SiteID&gt;$site_id&lt;/SiteID&gt;
                            &lt;OrderNumber&gt;$order_number&lt;/OrderNumber&gt;
                            &lt;LineID&gt;$item_id&lt;/LineID&gt;
                            &lt;ProductID&gt;&lt;/ProductID&gt;
                        &lt;/CancelLines&gt;
                        &lt;/Cancel&gt;
                        &lt;/CancelBundle&gt;
                      </app:cancelOrder>
            </app:cancelOrder>";
        $value =  CDS::CancelOrder($xml_data_cancelOrder);
        return $value;
        }
    }
