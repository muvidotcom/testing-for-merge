<?php
class CouponSubscription extends CActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'coupon_subscription';
    }
    public function getCouponSearchData($studio_id,$coupon_code){
       $db_user = Yii::app()->db;
       $history_data = $db_user->createCommand()
                       ->select('*')->from('coupon_subscription')
                       ->where('status=1 AND studio_id=:studio_id AND coupon_code LIKE :coupon_code', array(':studio_id'=>$studio_id, ':coupon_code'=>'%'.$coupon_code.'%'))
                       ->queryAll();
       return $history_data;
     }
     
     public function delete_coupon($studio_id,$id){
       $db_user = Yii::app()->db;
       if($id!=''){
       $delcoupon_sql = $db_user->createCommand()
                        ->update('coupon_subscription', array('status'=>0), 'studio_id=:studio_id AND id IN('.$id.')', array(':studio_id'=>$studio_id));
       }else{
           $delcoupon_sql = $db_user->createCommand()
                        ->update('coupon_subscription', array('status'=>0), 'studio_id=:studio_id', array(':studio_id'=>$studio_id));
       }
       return $delcoupon_sql;
       }
     
     public function getcoupon_history($coupon_code){
       $db_user = Yii::app()->db;
       $history_data = $db_user->createCommand()
                       ->select('*')->from('coupon_subscription')
                       ->where('status=1 AND coupon_code=:coupon_code', array(':coupon_code'=>$coupon_code))
                       ->queryRow();
       return $history_data;
     }
     
     public function getcoupon_data($studio_id){
        $db_user = Yii::app()->db;
        $data = $db_user->createCommand()
                ->select("*,(IF (UNIX_TIMESTAMP(used_date) = 0,'',DATE(used_date))) AS cused_date")->from('coupon_subscription')
                ->where('status=1 AND studio_id=:studio_id', array(':studio_id'=>$studio_id))
                ->queryAll();
        $item_count = count($data);
        $pages =new CPagination($item_count);
        $pages->setPageSize($page_size);
        $end =($pages->offset+$pages->limit <= $item_count ? $pages->offset+$pages->limit : $item_count);
        $sample =range($pages->offset+1, $end);
        return $data;
     }
}
?>