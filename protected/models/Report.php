<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SeoInfo
 *
 * @author SKM<support@muvi.com>
 */
class Report extends CActiveRecord{
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName() {
        return '';
    }
    
    public function relations(){
        return array();
    }
    
    public function allUsersReport()
    {
        $r_sql = "SELECT u.id,u.email,u.source,u.created_date,u.display_name,u.signup_location FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.id DESC";
        $res['registration'] = Yii::app()->db->createCommand($r_sql)->queryAll();
        
        //$s_sql = "SELECT us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC";
        //$s_sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.is_success=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC";
        $s_sql = "SELECT us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC";
        $res['subscription'] = Yii::app()->db->createCommand($s_sql)->queryAll();
        
        $p_sql = "SELECT ppv_subscriptions.user_id,start_date FROM  ppv_subscriptions WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 0";
        $res['ppv_user'] = Yii::app()->db->createCommand($p_sql)->queryAll();
        
        $ap_sql = "SELECT ppv_subscriptions.user_id,start_date FROM  ppv_subscriptions WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 1";
        $res['adv_ppv_user'] = Yii::app()->db->createCommand($ap_sql)->queryAll();
        
        return $res;
    }
    
    
    public function usersReport($dt = '',$offset,$page_size,$searchKey)
    {
        if($dt == ''){
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $sDate = $dt->start;
            $eDate = $dt->end;
        }
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR u.signup_location LIKE '%".$searchKey."%')";
        }
        $r_sql = "SELECT SQL_CALC_FOUND_ROWS u.id,u.email,u.source,u.created_date,u.display_name,u.signup_location,u.mobile_number FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.id DESC LIMIT ".$offset.",".$page_size;
        //$r_sql = "SELECT SQL_CALC_FOUND_ROWS u.id,u.email,u.source,u.created_date,u.display_name,t.subscription_id,t.ppv_subscription_id FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id LEFT JOIN transactions t ON u.id = t.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND t.ppv_subscription_id IS NULL AND t.subscription_id IS NULL ORDER BY u.id DESC LIMIT ".$offset.",".$page_size;
        $res['registration']['data'] = Yii::app()->db->createCommand($r_sql)->queryAll();
        $res['registration']['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        //$s_sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
        $s_sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
        $res['subscription']['data'] = Yii::app()->db->createCommand($s_sql)->queryAll();
        $res['subscription']['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        $p_sql = "SELECT SQL_CALC_FOUND_ROWS ps.user_id FROM  ppv_subscriptions ps,sdk_users u WHERE ps.studio_id = '" . Yii::app()->user->studio_id . "' AND ps.is_advance_purchase = 0 AND (DATE_FORMAT(ps.start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND ps.user_id = u.id ".$searchStr;
        $res['ppv_user']['data'] = Yii::app()->db->createCommand($p_sql)->queryAll();
        $res['ppv_user']['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        $ap_sql = "SELECT SQL_CALC_FOUND_ROWS ps.user_id FROM  ppv_subscriptions ps,sdk_users u WHERE ps.studio_id = '" . Yii::app()->user->studio_id . "' AND ps.is_advance_purchase = 1 AND (DATE_FORMAT(ps.start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND ps.user_id = u.id ".$searchStr;
        $res['adv_ppv_user']['data'] = Yii::app()->db->createCommand($ap_sql)->queryAll();
        $res['adv_ppv_user']['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        return $res;
    }
    
    public function userDetails($user_id)
    {
        $sql = "SELECT u.email,u.id,u.display_name,u.signup_location, u.status as user_status, u.is_deleted, u.deleted_at FROM sdk_users u where u.id = $user_id ";
        $res['userdata'] = Yii::app()->db->createCommand($sql)->queryRow();
        
        $currsql = "SELECT SUM(transactions.amount) as total_amount,transactions.currency_id FROM sdk_users u, transactions where u.id = $user_id AND u.id = transactions.user_id GROUP BY transactions.user_id, transactions.currency_id";
        $res['currency_info'] = Yii::app()->db->createCommand($currsql)->queryAll();
        $videosql = "SELECT SUM(played_length) AS watched_time FROM video_logs WHERE user_id = $user_id GROUP BY user_id";
        $res['uservideodata'] = Yii::app()->db->createCommand($videosql)->queryRow();
        
        $loginsql = "SELECT SQL_CALC_FOUND_ROWS login_at FROM login_history WHERE user_id = $user_id ORDER by login_at DESC";
        $res['userlogindata'] = Yii::app()->db->createCommand($loginsql)->queryRow();
        $res['userlogindata']['total_login'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        $subscriptionsql = "SELECT us.*, sp.name AS plan_name FROM user_subscriptions us LEFT JOIN subscription_plans sp ON (us.plan_id = sp.id) WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' AND us.user_id={$user_id} ORDER BY us.status DESC, us.created_date DESC";
        $res['subscriptions'] = Yii::app()->db->createCommand($subscriptionsql)->queryAll();
        return $res;
    }
    
    public function usersTypeData($type = '',$sDate,$eDate,$offset,$page_size,$searchKey)
    {
        $res = array();
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR u.signup_location LIKE '%".$searchKey."%')";
        }
        switch ($type){
            case 'registrations':
                $sql = "SELECT SQL_CALC_FOUND_ROWS tot.* FROM (SELECT DISTINCT u.id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,u.is_free,us.status FROM sdk_users u LEFT JOIN user_subscriptions AS us ON u.id = us.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.status DESC) AS tot GROUP BY tot.user_id ORDER BY tot.user_id DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'subscriptions':
                //$sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'ppvusers':
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 0 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY start_date DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            
            case 'advppvusers':
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 1 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY start_date DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'cancelled':
                $searchStr = '';
                if(trim($searchKey)){
                    $searchStr = " AND (user.display_name LIKE '%".$searchKey."%' OR user.email LIKE '%".$searchKey."%' OR user.signup_location LIKE '%".$searchKey."%')";
                }
                //$sql = "SELECT SQL_CALC_FOUND_ROWS tot.* FROM (SELECT DISTINCT us.user_id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,us.status,us.cancel_date,us.cancel_note,cr.reason FROM user_subscriptions AS us LEFT JOIN cancel_reasons cr ON (us.cancel_reason_id = cr.id) LEFT JOIN sdk_users u ON (u.id = us.user_id AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."')) WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' ".$searchStr." ORDER BY us.status DESC) AS tot WHERE tot.status = 0 GROUP BY tot.user_id ORDER BY tot.user_id DESC LIMIT ".$offset.",".$page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS user.* FROM (SELECT tot.*, u.id, u.email,u.studio_id,u.display_name,u.signup_location,u.created_date FROM (SELECT sub.* FROM (SELECT us.user_id, us.status,us.cancel_reason_id, us.cancel_date,us.cancel_note FROM user_subscriptions AS us  WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY us.user_id ORDER BY us.status DESC) AS sub WHERE sub.status=0) AS tot , sdk_users u  WHERE u.id !='' AND u.id = tot.user_id AND u.is_developer != '1' AND u.is_studio_admin != '1' AND (DATE_FORMAT(tot.cancel_date, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."')) AS user LEFT JOIN cancel_reasons cr ON (user.cancel_reason_id = cr.id) WHERE user.studio_id = '" . Yii::app()->user->studio_id . "' ".$searchStr." ORDER BY user.cancel_date DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'deleted':
                $searchStr = '';
                if(trim($searchKey)){
                    $searchStr = " AND (display_name LIKE '%".$searchKey."%' OR email LIKE '%".$searchKey."%' OR signup_location LIKE '%".$searchKey."%')";
                }
                $sql = "SELECT *, status as user_status,id as user_id FROM sdk_users WHERE is_developer != '1' AND is_studio_admin != '1' AND is_deleted=1 AND status=0 AND (DATE_FORMAT(deleted_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." AND studio_id = '".Yii::app()->user->studio_id."'  ORDER BY deleted_at DESC LIMIT ".$offset.",".$page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
        }
        return $res;
    }
    
    public function usersSearchedTypeData($type = '',$key)
    {
        $res = array();
        switch ($type){
            case 'registrations':
                $sql = "SELECT u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE (u.email LIKE '%".$key."%' OR u.display_name LIKE '%".$key."%') AND u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.created_date DESC";
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'subscriptions':
                //$sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id LEFT JOIN transactions AS t ON t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' WHERE (u.email LIKE '%".$key."%' OR u.display_name LIKE '%".$key."%') AND us.status=1 AND us.is_success=1  AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '') ORDER BY us.created_date DESC";
                $sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "'  AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE (u.email LIKE '%".$key."%' OR u.display_name LIKE '%".$key."%') AND us.status=1 AND us.is_success=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.created_date DESC";
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'ppvusers':
                $sql = "SELECT DISTINCT ppv_subscriptions.user_id,su.* FROM  ppv_subscriptions LEFT JOIN sdk_users AS su ON su.id = ppv_subscriptions.user_id WHERE (su.email LIKE '%".$key."%' OR su.display_name LIKE '%".$key."%') AND ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' ORDER BY start_date DESC";
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                break;
        }
        return $res;
    }
    
    public function usersReportData($type = '',$sDate,$eDate,$searchKey = '')
    {
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR u.signup_location LIKE '%".$searchKey."%')";
        }
        switch ($type){
            case 'registrations':
                $sql = "SELECT u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name,us.status FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.id DESC";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'subscriptions':
                //$sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id LEFT JOIN transactions AS t ON t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' WHERE us.status=1 AND us.is_success=1  AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '') ORDER BY us.id DESC";
                $sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.is_success=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'ppvusers':
                $sql = "SELECT DISTINCT ppv_subscriptions.user_id,u.* FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."')".$searchStr;
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
        }
        return $res;
    }
    
     public function usersReportDataTypes($type = '',$mobile_no,$sDate,$eDate,$searchKey = '')
    {
        $searchStr = '';
        if(trim($searchKey)){
            $searchStr = " AND (u.display_name LIKE '%".$searchKey."%' OR u.email LIKE '%".$searchKey."%' OR u.signup_location LIKE '%".$searchKey."%')";
        }
        switch ($type){
            case 'registrations':
             $sql = "SELECT tot.* FROM (SELECT DISTINCT u.id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,u.is_free,u.mobile_number,us.status FROM sdk_users u LEFT JOIN user_subscriptions AS us ON u.id = us.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.status DESC) AS tot GROUP BY tot.user_id ORDER BY tot.email";
                //$sql = "SELECT u.id,u.email,u.source,u.signup_location,u.created_date,u.display_name,us.status FROM sdk_users u LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.id DESC";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'subscriptions':
                //$sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id LEFT JOIN transactions AS t ON t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' WHERE us.status=1 AND us.is_success=1  AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '') ORDER BY us.id DESC";
                $sql = "SELECT  us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,u.mobile_number,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.email ";
                //$sql = "SELECT u.*  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.is_success=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'ppvusers':
               $sql = "SELECT  DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 0 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.email ";
                //$sql = "SELECT DISTINCT ppv_subscriptions.user_id,u.* FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."')".$searchStr;
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
         case 'advppvusers':
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 1 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY u.email ";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'cancelled':
                $searchStr = '';
                if(trim($searchKey)){
                    $searchStr = " AND (user.display_name LIKE '%".$searchKey."%' OR user.email LIKE '%".$searchKey."%' OR user.signup_location LIKE '%".$searchKey."%')";
                }
                //$sql = "SELECT SQL_CALC_FOUND_ROWS tot.* FROM (SELECT DISTINCT us.user_id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,us.status,us.cancel_date,us.cancel_note,cr.reason FROM user_subscriptions AS us LEFT JOIN cancel_reasons cr ON (us.cancel_reason_id = cr.id) LEFT JOIN sdk_users u ON (u.id = us.user_id AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."')) WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' ".$searchStr." ORDER BY us.status DESC) AS tot WHERE tot.status = 0 GROUP BY tot.user_id ORDER BY tot.user_id DESC LIMIT ".$offset.",".$page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS user.* FROM (SELECT tot.*, u.id, u.email,u.studio_id,u.display_name,u.signup_location,u.created_date FROM (SELECT sub.* FROM (SELECT us.user_id, us.status,us.cancel_reason_id, us.cancel_date,us.cancel_note FROM user_subscriptions AS us  WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY us.user_id ORDER BY us.status DESC) AS sub WHERE sub.status=0) AS tot , sdk_users u  WHERE u.id !='' AND u.id = tot.user_id AND u.is_developer != '1' AND u.is_studio_admin != '1' AND (DATE_FORMAT(tot.cancel_date, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."')) AS user LEFT JOIN cancel_reasons cr ON (user.cancel_reason_id = cr.id) WHERE user.studio_id = '" . Yii::app()->user->studio_id . "' ".$searchStr." ORDER BY user.display_name ";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
            case 'deleted':
                $searchStr = '';
                if(trim($searchKey)){
                    $searchStr = " AND (display_name LIKE '%".$searchKey."%' OR email LIKE '%".$searchKey."%' OR signup_location LIKE '%".$searchKey."%')";
                }
                $sql = "SELECT *, status as user_status FROM sdk_users WHERE is_developer != '1' AND is_studio_admin != '1' AND is_deleted=1 AND status=0 AND (DATE_FORMAT(deleted_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." AND studio_id = '".Yii::app()->user->studio_id."'  ORDER BY email ";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                break;
        }
        return $res;
    }
    
    public function endUserList($type = '', $offset = 0, $page_size = 20, $searchKey = '') {
        $res = array();
        $searchStr = '';
        if (trim($searchKey)) {
            $searchStr = " AND (u.display_name LIKE '%" . $searchKey . "%' OR u.email LIKE '%" . $searchKey . "%' OR u.signup_location LIKE '%" . $searchKey . "%')";
        }

        switch ($type) {
            case 'registrations':
                //$sql = "SELECT SQL_CALC_FOUND_ROWS user.* FROM (SELECT tot.* FROM (SELECT DISTINCT u.id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,us.status,u.is_free FROM sdk_users u LEFT JOIN user_subscriptions AS us ON u.id = us.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_deleted!=1 AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') " . $searchStr . " ORDER BY us.status DESC) AS tot GROUP BY tot.user_id ORDER BY tot.user_id) AS user WHERE user.status <> 1 OR user.status IS NULL ORDER BY user.user_id DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS user.*, us.status FROM (SELECT u.id as user_id, u.email, u.source, u.created_date, u.display_name, u.signup_location,u.is_free FROM sdk_users u WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_deleted!=1 AND u.is_studio_admin != '1' " . $searchStr . " ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name) user LEFT JOIN (SELECT id AS user_sub_id, user_id, MAX(status) AS status FROM user_subscriptions WHERE studio_id='" . Yii::app()->user->studio_id . "' GROUP By user_id) AS us ON user.user_id = us.user_id WHERE us.status<>1 OR us.status IS NULL LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'subscriptions':
                //$sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN ppv_subscriptions ppvs on us.user_id=ppvs.user_id WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') " . $searchStr . " ORDER BY us.id DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT us.*,u.email,u.created_date AS reg_dt,u.display_name,u.signup_location,u.id as user_id FROM sdk_users u, user_subscriptions us WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_deleted!=1 AND u.is_studio_admin != '1' " . $searchStr . " AND u.id = us.user_id AND us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'ppvusers':
                //$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND u.status=" . 1 . " AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') " . $searchStr . " ORDER BY start_date DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS ps.user_id, u.*,u.id as user_id FROM sdk_users u, ppv_subscriptions ps WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_deleted!=1 AND u.is_studio_admin != '1' AND u.id = ps.user_id AND u.status=1 AND ps.studio_id='" . Yii::app()->user->studio_id . "' " . $searchStr . " GROUP BY u.id ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'cancelled':
                $searchStr = '';
                if (trim($searchKey)) {
                    $searchStr = " AND (u.display_name LIKE '%" . $searchKey . "%' OR u.email LIKE '%" . $searchKey . "%' OR u.signup_location LIKE '%" . $searchKey . "%')";
                }
                //$sql = "SELECT user.* FROM (SELECT tot.*, u.email,u.studio_id,u.display_name,u.signup_location,u.created_date, u.status AS user_status, u.is_deleted FROM (SELECT cancel_user.* FROM (SELECT all_user.* FROM (SELECT us.user_id, us.status,us.cancel_reason_id, us.cancel_date,us.cancel_note FROM user_subscriptions AS us WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' ORDER BY us.status DESC) AS all_user GROUP BY all_user.user_id) AS cancel_user WHERE cancel_user.status=0) AS tot , sdk_users u  WHERE u.id !='' AND u.id = tot.user_id AND u.is_developer != '1' AND u.is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "')) AS user LEFT JOIN cancel_reasons cr ON (user.cancel_reason_id = cr.id) WHERE user.studio_id = '" . Yii::app()->user->studio_id . "' " . $searchStr . " ORDER BY user.cancel_date DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS us.*, u.email,u.studio_id,u.display_name,u.signup_location,u.created_date, u.status AS user_status, u.is_deleted FROM (SELECT user_id, MAX(status) AS status FROM user_subscriptions WHERE studio_id = '" . Yii::app()->user->studio_id . "' GROUP By 1 HAVING status=0) AS us, sdk_users u WHERE us.user_id=u.id AND u.is_developer != '1' AND u.is_studio_admin != '1' ".$searchStr." ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'deleted':
                $searchStr = '';
                if (trim($searchKey)) {
                    $searchStr = " AND (display_name LIKE '%" . $searchKey . "%' OR email LIKE '%" . $searchKey . "%' OR signup_location LIKE '%" . $searchKey . "%')";
                }
                //$sql = "SELECT *, status as user_status,id as user_id FROM sdk_users WHERE is_developer != '1' AND is_studio_admin != '1' AND is_deleted=1 AND status=0 AND (DATE_FORMAT(deleted_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') " . $searchStr . " AND studio_id = '" . Yii::app()->user->studio_id . "'  ORDER BY deleted_at DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS *, status as user_status,id as user_id FROM sdk_users WHERE is_developer != '1' AND is_studio_admin != '1' AND is_deleted=1 AND status=0 AND studio_id = '" . Yii::app()->user->studio_id . "' " . $searchStr . " ORDER BY DATE_FORMAT(deleted_at, '%Y-%m-%d') DESC, display_name ASC LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                break;
            case 'device_switch':                 
                $sql = "SELECT SQL_CALC_FOUND_ROWS user.*, us.status FROM (SELECT u.id as user_id, u.email, u.source, u.created_date, u.display_name, u.signup_location,u.is_free,d.flag FROM sdk_users u INNER JOIN device_management d ON u.id=d.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "'" . $searchStr . " AND u.is_developer != '1' AND u.is_deleted!=1 AND u.is_studio_admin != '1' AND d.flag='1' " . $searchStr . " GROUP BY u.id ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name) user LEFT JOIN (SELECT id AS user_sub_id, user_id, MAX(status) AS status FROM user_subscriptions WHERE studio_id='" . Yii::app()->user->studio_id . "' GROUP By user_id) AS us ON user.user_id = us.user_id  LIMIT " . $offset . "," . $page_size;                
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();                 
                break;
            default :
                //$sql = "SELECT SQL_CALC_FOUND_ROWS users.* FROM (SELECT tot.*,t.subscription_id,t.ppv_subscription_id FROM (SELECT DISTINCT u.id as user_id,u.studio_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,us.status,u.is_free,u.status as user_status,u.is_deleted FROM sdk_users u LEFT JOIN user_subscriptions AS us ON u.id = us.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '" . $eDate . "') " . $searchStr . " ORDER BY us.status DESC) AS tot LEFT JOIN transactions AS t ON (t.user_id = tot.user_id AND t.studio_id = tot.studio_id) GROUP BY tot.user_id,t.subscription_id,t.ppv_subscription_id ORDER BY tot.user_id) AS users GROUP BY users.user_id ORDER BY users.user_id DESC LIMIT " . $offset . "," . $page_size;
                $sql = "SELECT SQL_CALC_FOUND_ROWS user.*, us.status FROM (SELECT DISTINCT u.id as user_id,u.studio_id,u.email,u.source,u.created_date,u.display_name,u.is_free,u.status as user_status,u.is_deleted, u.signup_location FROM sdk_users u WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND u.is_studio_admin != '1' " . $searchStr . " ORDER BY DATE_FORMAT(u.created_date, '%Y-%m-%d') DESC, u.display_name ASC) AS user LEFT JOIN (SELECT user_id, MAX(status) AS status FROM user_subscriptions WHERE studio_id = '" . Yii::app()->user->studio_id . "' GROUP By 1) AS us ON user.user_id = us.user_id LIMIT " . $offset . "," . $page_size;
                $res['data'] = Yii::app()->db->createCommand($sql)->queryAll();
                $res['count'] = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$res['delete_requested']=Device::model()->getDeleteRequestedUsers(Yii::app()->user->studio_id);							
                break;
        }
        return $res;
    }
   
}
