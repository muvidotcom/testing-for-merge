<?php

class StudioPricingPlan extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'studio_pricing_plans';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function set_Reseller_customer_pricing_plan($data = array()){
        if(count($data) > 0){
            foreach($data as $key =>$val){
              $this->$key = $val;  
            }
            $this->save();
            return $this->id;
        }
    }
    public function get_studio_pricing_plan($studio_id){
        $sql = Yii::app()->db->createCommand()
             ->select('COUNT(sp.application_id) as no_of_app,sp.package_id,sp.plans')
             ->from('studio_pricing_plans sp')
             ->where('sp.studio_id =:studio_id',array(':studio_id'=>$studio_id))
             ->queryAll();
        return $sql;
    }    

}
