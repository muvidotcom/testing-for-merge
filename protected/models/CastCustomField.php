<?php
class CastCustomField extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'cast_custom_field';
    }
    public function relations() {
        return array(
            'customdata'=>array(self::HAS_ONE, 'CastCustomFieldValue', 'field_id'),
        );
    } 
    public function findAllFields($studio_id, $type = 1, $flag) {
        //Find All FAQs       
        $conditions = '';
        if($flag){
            $conditions = ' AND is_default!="1"';
        }
        $fields = CastCustomField::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND form_type = :form_type AND status=:status'. $conditions,
            'params' => array(':studio_id' => $studio_id, ':form_type' => $type, ':status'=>1),
            'order' => 't.id_seq ASC'
        ));          
        return($fields);
    }    
    public function findFieldByName($studio_id, $field_name) {
        $field = CastCustomField::model()->find(array(
            'condition' => 'studio_id=:studio_id AND field_name = :field_name',
            'params' => array(':studio_id' => $studio_id, ':field_name' => $field_name),
            'order' => 't.id_seq ASC'
        ));          
        return($field);
    }
    public function saveCustomField($formData, $studio_id, $id = 0) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $CustomFieldLang = @$formData['CustomFieldLang'];
        $f_value = '';
        $is_require = (isset($formData['is_require'])) ? $formData['is_require'] : 0;
        $status = (isset($formData['status']) && $formData['status'] == 1) ? $formData['status'] : 0;
        $is_multi = ($formData['input_type'] == 3) ? 1 : 0;
        if ($id) {
            $cfield = $this->find('id=:id AND studio_id=:studio_id',array(':id'=>$id,':studio_id'=>$studio_id));
            if ($formData['input_type'] > 1) {
                $cs_value = json_decode($cfield->field_values,true);
            }
            $ret['msg'] = 'Field updated successfully.';
        }else{
            $cfield = New CastCustomField;
            $cfield->field_id = $formData['field_id'];
            $cfield->field_name = $formData['field_id'];
            $cfield->status = $status;
            $ret['msg'] = 'Field added successfully.';
        }
        $cfield->studio_id = $studio_id;
        if(isset($formData['input_type'])){
            $cfield->input_type = $formData['input_type'];
        }
        //$cfield->placeholder = $formData['f_placeholder'];
        $cfield->field_label = $formData['f_label'];
        $cfield->is_required = $is_require;
        $cfield->is_multi = $is_multi;
        $cfield->form_type = 1;
        $cfield->status = $status;
        if($formData['input_type'] > 1){
            if(@$cs_value && @$CustomFieldLang){
                if(!array_key_exists('en', $cs_value)){
                    $tempvalue['en'] = $cs_value;
                }else{
                    $tempvalue = $cs_value;
                }
                $tempvalue[$CustomFieldLang] = array_values($formData['field_values']);
            }else{
                $tempvalue['en'] = array_values($formData['field_values']);
            }
            $f_value = $tempvalue;
        }
        $cfield->field_values = ($formData['input_type']>1)?json_encode($f_value):NULL;
        $cfield->save();
        $placeholder_key =  $formData['field_id'].'_placeholder';
        if($formData['input_type'] != 4 && $formData['input_type'] != 5 && $formData['f_placeholder']){
            $transs = TranslateKeyword::model()->UpdateKey($studio_id, $placeholder_key, $formData['f_placeholder'], 1);
        }else if(!$formData['f_placeholder'] && $formData['input_type'] != 4 && $formData['input_type'] != 5){
            $transs = TranslateKeyword::model()->DeleteKey($studio_id, $placeholder_key, 1);
        }
        $trans = TranslateKeyword::model()->UpdateKey($studio_id, $formData['field_id'], $formData['f_label'], 1);
        $ret['id'] = $cfield->id;
        return $ret;
    }
    public function chkStudio($studio_id){
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getRecord = $this->count(array('select'=>'id', 'condition'=>'studio_id=:studio_id', 'params'=>array(':studio_id' => $studio_id)));
        return $getRecord;
    }
    public function getCustomFields($studio_id){
        $command = Yii::app()->db->createCommand()
            ->from('cast_custom_field f')
            ->select('f.id,f.form_type as f_type,f.field_label as label,f.field_name as f_name,f.field_id as f_id,f.field_values as f_value,f.is_required as f_is_required,f.input_type as field_type')
            ->where('f.studio_id ='.$studio_id.' AND status= "1"')
            ->order('f.id_seq asc');
        $data = $command->queryAll();
        return $data;
    }
    public function getAllCustomData($celeb_id,$studio_id, $language_id = 20, $translate = array()){
        if(!$translate):
           $translate = Yii::app()->controller->Language; 
        endif;
        $cond = "";
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'customdata' => array(// this is for fetching data
                'together' => false,
                'select' => 'customdata.field_id,customdata.value',  
                'condition' => 'customdata.studio_id = :studio_id AND customdata.celeb_id = :celeb_id',
                'params' => array(
                    ':studio_id' => $studio_id,
                    ':celeb_id' => $celeb_id,
                ),
            ),
        );
        $criteria->select = 't.id,t.field_name,t.field_label';
        $criteria->condition = 't.status = 1 AND t.studio_id = ' . $studio_id;
        $criteria->order = 't.id_seq ASC';
        $custom_fields = $this->findAll($criteria);
        $translated = $cfield = $custom_celeb_data = array();
        if($language_id !=20){
            $translated = CHtml::listData(CastCustomFieldValue::model()->findAllByAttributes(array('studio_id' => $studio_id,'celeb_id'=>$celeb_id, 'language_id' => $language_id), array('select'=>'field_id,value')),'field_id','value');
            if(!empty($custom_fields)){
                foreach($custom_fields as $fields){
                    if (array_key_exists($fields->id, $translated)) {
                       $fields['customdata']->value = $translated[$fields->id];
                    }
                    $cfield[] = $fields;
                }
            }
        }else{
            $cfield = $custom_fields;
        }
        if(!empty($cfield)){
            foreach($cfield as $ckey => $cval):
                if(trim($cval['customdata']->value)):
                    $svalue = $cval['customdata']->value;
                    $svalue = is_string($svalue) && is_array(json_decode($svalue, true)) ? json_decode($svalue, true) : $svalue;
                    $custom_celeb_data[] = array(
                        'field_name' => isset($translate[$cval->field_name]) ? $translate[$cval->field_name] :  $cval->field_label,
                        'value' => $svalue
                    );
                endif;
            endforeach;
        }
        return $custom_celeb_data;
    }
}

