<?php
class ContentFilterType extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'content_filter_types';
    }
}
