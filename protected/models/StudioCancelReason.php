<?php
class StudioCancelReason extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studio_cancel_reasons';
    }
    /**
    * @return array relational rules.
    */
    public function relations()
    {
           // NOTE: you may need to adjust the relation name and the related
           // class name for the relations automatically generated below.
           return array(
               'studios'=>array(self::HAS_MANY, 'Studio', 'studio_id'),
           );
    }
}
