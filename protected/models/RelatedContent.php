<?php

class RelatedContent extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'related_content';
	}

	public function insertRelatedData($studio_id, $data) {
		if ($data) {
			$movieids = explode(',', trim($data['movieids'], ','));
			$contenttypes = explode(',', trim($data['contenttypes'], ','));

			$master_cont_stream = $data['mastercontentstreamid'];
			$master_cont_id = $data['mastercontentid'];

			$cond = "studio_id = " . $studio_id . " AND id  = " . $master_cont_stream;
			$movietypes_list = Yii::app()->db->createCommand()
					->select("*")
					->from('movie_streams')
					->where($cond)
					->queryROW();

			$is_episode = $movietypes_list['is_episode'];

			// movie_type for Physical 
			$cond1 = "studio_id = " . $studio_id . " AND id IN (" . $master_cont_stream . ")";
			$physical_cont = Yii::app()->db->createCommand()
					->select("*")
					->from('pg_product')
					->where($cond1)
					->queryROW();

			$physical_id = $physical_cont['id'];

			// movie_type for Live-Streaming 
			$cond2 = "studio_id = " . $studio_id . " AND movie_id IN (" . $master_cont_id . ")";
			$live_cont = Yii::app()->db->createCommand()
					->select("*")
					->from('livestream')
					->where($cond2)
					->queryROW();

			$live_id = $live_cont['movie_id'];


			if ($is_episode == 0 && $master_cont_stream != $physical_id && $master_cont_id != $live_id) {
				$movie_type = 0;
			} else if (isset($is_episode) && $is_episode == 1 && $master_cont_stream != $physical_id && $master_cont_id != $live_id) {
				$movie_type = 1;
			} else if ($master_cont_stream == $physical_id && $master_cont_id != $live_id) {
				$movie_type = 2;
			} else if ($master_cont_id == $live_id) {
				$movie_type = 3;
			}

			foreach ($movieids as $key => $value) {
				$content_type = $contenttypes[$key];
				$movie_id = $data['mastercontentid'];
				$movie_stream_id = $data['mastercontentstreamid'];
				$isNewRecord = true;
				$primaryKey = NULL;

				if ($content_type == 0) {
					$content_id = $value;
					$cond = "studio_id = " . $studio_id . " AND movie_id  = " . $content_id . "";
					$moviestream_list = Yii::app()->db->createCommand()
							->select("id")
							->from('movie_streams')
							->where($cond)
							->queryROW();

					$current_moviestream = $moviestream_list['id'];
					if ($movie_type == 2) {			   // check if movie_id frpm pg_product
						$movie_stream_id = 0;
					}
					$content_stream_id = $current_moviestream;
					$type = @$data['type'];
				} else if ($content_type == 1) {
					$cond = "studio_id = " . $studio_id . " AND id= " . $value . "";
					$moviestream_list = Yii::app()->db->createCommand()
							->select("movie_id")
							->from('movie_streams')
							->where($cond)
							->queryROW();

					$ch_movie_id = $moviestream_list['movie_id'];
					if ($movie_type == 2) {			   // check if movie_id frpm pg_product
						$movie_stream_id = 0;
					}
					$content_stream_id = $value;
					$content_id = $ch_movie_id;
				} else if ($content_type == 2) {
					$cond = "studio_id = " . $studio_id . " AND movie_id  = " . $movie_id . "";
					$moviestreams = Yii::app()->db->createCommand()
							->select("id")
							->from('movie_streams')
							->where($cond)
							->queryROW();

					$rel_movie_stream = $moviestreams['id'];
					if ($movie_type == 1) {
						$movie_stream_id = $data['mastercontentstreamid'];
					} else {
						$movie_stream_id = $rel_movie_stream;
					}
					$content_id = $value;
					$content_stream_id = 0;
				} else if ($content_type == 3) {
					$content_id = $value;
					$cond = "studio_id = " . $studio_id . " AND movie_id  = " . $content_id . "";
					$get_moviestream = Yii::app()->db->createCommand()
							->select("id")
							->from('movie_streams')
							->where($cond)
							->queryROW();

					$current_stream = $get_moviestream['id'];
					$content_stream_id = $current_stream;
					if ($movie_type == 2) {			   // check if movie_id frpm pg_product
						$movie_stream_id = 0;
					}
				}
				$this->studio_id = $studio_id;
				$this->movie_id = $movie_id;
				$this->movie_stream_id = $movie_stream_id;
				$this->content_id = $content_id;
				$this->content_stream_id = $content_stream_id;
				$this->content_type = $content_type;
				$this->movie_type = $movie_type;
				$this->type = $type;
				$this->isNewRecord = $isNewRecord;
				$this->primaryKey = $primaryKey;
				$this->save();

				$rev = new RelatedContent();

				$rev->studio_id = $studio_id;
				$rev->movie_id = $content_id;
				$rev->movie_stream_id = $content_stream_id;
				$rev->content_id = $movie_id;
				$rev->content_stream_id = $movie_stream_id;
				$rev->content_type = $movie_type;
				$rev->movie_type = $content_type;
				$rev->type = $type;
				$rev->isNewRecord = $isNewRecord;
				$rev->primaryKey = $primaryKey;
				$rev->save();
			}

			return 1;
		} else {
			return '';
		}
	}

}
