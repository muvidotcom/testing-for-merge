<?php

class CustomField extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'custom_field';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }
    
    public function findAllFields($studio_id, $type = 1, $flag) {
        //Find All FAQs       
        $conditions = '';
        if($flag){
            $conditions = ' AND is_default!="1"';
        }
        $fields = CustomField::model()->findAll(array(
            'condition' => 'studio_id=:studio_id AND form_type = :form_type AND status=:status'. $conditions,
            'params' => array(':studio_id' => $studio_id, ':form_type' => $type, ':status'=>1),
            'order' => 't.id_seq ASC'
        ));          
        return($fields);
    }    

    public function findFieldByName($studio_id, $field_name) {
        //Find All FAQs       
        $field = CustomField::model()->find(array(
            'condition' => 'studio_id=:studio_id AND field_name = :field_name',
            'params' => array(':studio_id' => $studio_id, ':field_name' => $field_name),
            'order' => 't.id_seq ASC'
        ));          
        return($field);
    }     
    public function saveCustomField($formData, $studio_id, $id = 0) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
		$CustomFieldLang = @$formData['CustomFieldLang'];
        $f_value = '';
        $is_require = (isset($formData['is_require'])) ? $formData['is_require'] : 0;
        $is_multi = ($formData['input_type'] == 3) ? 1 : 0;
        
        //print_r($formData); exit;
        if ($formData['input_type'] > 1 && $formData['input_type'] != 6) {
            $f_value = json_encode(array_values($formData['field_values']));
        }

        if ($id) {
            $cfield = CustomField::model()->find('id=:id AND studio_id=:studio_id',array(':id'=>$id,':studio_id'=>$studio_id));
            if ($formData['input_type'] > 1) {
                $cs_value = json_decode($cfield->field_values,true);
            }
            $ret['msg'] = 'Field updated successfully.';
        }else{
            $cfield = New CustomField;
            $cfield->field_id = $formData['field_id'];
            $cfield->field_name = $formData['field_id'];
            $cfield->status = 0;
            $ret['msg'] = 'Field added successfully.';
            $chkNewStudio = $this->chkStudio($studio_id);
            if($chkNewStudio){
                $cfield->is_new = 1;
            }else{
                $cfield->is_new = 0;
            }
        }
        $cfield->studio_id = $studio_id;
        if(isset($formData['input_type'])){
            $cfield->input_type = $formData['input_type'];
        }
        $cfield->placeholder = $formData['f_placeholder'];
        $cfield->field_label = $formData['f_label'];
        $cfield->is_required = $is_require;
        $cfield->is_multi = $is_multi;
        $cfield->form_type = 1;
        if($cfield->field_id != 'name'){
            $cfield->status = 0;
        }
		if($formData['input_type'] > 1){
		if(@$cs_value && @$CustomFieldLang){
			if(!array_key_exists('en', $cs_value)){
				$tempvalue['en'] = $cs_value;
			}else{
				$tempvalue = $cs_value;
			}
			$tempvalue[$CustomFieldLang] = array_values($formData['field_values']);
		}else{
			$tempvalue['en'] = array_values($formData['field_values']);
		}
		$f_value = $tempvalue;
	}
        $cfield->field_values = ($formData['input_type']>1)?json_encode($f_value):NULL;
        $cfield->save();
        $placeholder_key =  $formData['field_id'].'_placeholder';
        if($formData['input_type'] != 4 && $formData['input_type'] != 5 && $formData['f_placeholder']){
            $transs = TranslateKeyword::model()->UpdateKey($studio_id, $placeholder_key, $formData['f_placeholder']);
        }else if(!$formData['f_placeholder'] && $formData['input_type'] != 4 && $formData['input_type'] != 5){
            $transs = TranslateKeyword::model()->DeleteKey($studio_id, $placeholder_key);
        }
        $trans = TranslateKeyword::model()->UpdateKey($studio_id, $formData['field_id'], $formData['f_label']);
        $ret['id'] = $cfield->id;
        return $ret;
    }
    
    public function chkStudio($studio_id){
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $getRecord = $this->count(array('select'=>'id', 'condition'=>'studio_id=:studio_id AND is_new=:is_new', 'params'=>array(':studio_id' => $studio_id, ':is_new'=>1)));
        return $getRecord;
    }
}
