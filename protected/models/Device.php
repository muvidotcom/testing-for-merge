<?php
class Device extends CActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'device_management';
    }
	
	public function getDeleteRequestedUsers($studio_id=0){		
		$user_list=Yii::app()->db->createCommand()
                ->select('user_id')->from('device_management')
				->where('flag=1 AND studio_id=:studio_id',array(':studio_id'=>$studio_id))->group('user_id')->queryColumn();
		return $user_list;				
	}
}
?>