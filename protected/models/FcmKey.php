<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This model keeps track of all fcm keys for both android and ios devices
 *
 * @author muvi
 */
class FcmKey extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'fcm_keys';
    }
    public function getGpushNotificationAPIkey($studio_id){
        $cond        = "";
        $criteria    = new CDbCriteria();
        $criteria->condition = 't.studio_id = ' . $studio_id. ' OR t.studio_id = 0';
        $criteria->select = 'device_type, fcm_key'; 
        $criteria->order = 'id DESC';
        $criteria->limit = 4;
        $keys = $this->findAll($criteria); 
        $android = $ios = '';
        foreach($keys as $key){
            if($android == '' && $key['device_type'] == 1)
                $android = $key['fcm_key'];
            else if($ios == '' && $key['device_type'] == 2)
                $ios = $key['fcm_key'];            
        }
        $return=array('android' => $android, 'ios' => $ios);
        return $return;
    }
}