<?php

class PpvAdvanceContent extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'ppv_advance_content';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ppvplans'=>array(self::BELONGS_TO, 'PpvPlans','ppv_plan_id')
        );
    }

}
