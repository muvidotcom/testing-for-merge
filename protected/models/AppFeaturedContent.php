<?php
class AppFeaturedContent extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName(){
        return 'app_featured_content';
    }
    public function getMaxOrd($studio_id = false, $section_id){
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $feat = $this->find(array(
            'select'    => 'id_seq',
            'condition' => 'studio_id = :studio_id AND section_id = :section_id',
            'params'    => array(':studio_id' => $studio_id, ':section_id' => $section_id),
            'order'     => 't.id_seq DESC',
            'limit'     => '1'
        ));   
        if(isset($feat) && count($feat)){
            return (int) $feat->id_seq;
        }else{
            return 0;
        }      
    }
    public function getAppFeatured($section_id, $studio_id = false, $language_id = 20, $user_id = 0,$domainName = '') {
        $controller = Yii::app()->controller;
        if(!$studio_id){
            $studio_id = Yii::app()->common->getStudiosId();
        }
        if($section_id){
            $criteria = new CDbCriteria();
            $criteria->with = array(
                'contents' => array(
                    'together' => false,
                    'condition' => 'contents.studio_id = ' . $studio_id,
                    'order' => 'contents.id_seq ASC'
                ),
            );
            $cond = 't.id = '.$section_id.' AND t.studio_id = ' . $studio_id.' AND t.parent_id = 0';
            $criteria->condition = $cond;
            $section = AppFeaturedSections::model()->find($criteria);
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
            $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
            if(count($section->contents)){
                $streamIds = '';
                $movieIds = '';
                $final_content = array();
                $total_contents = 0;
                if ($section->content_type == 1) {
                    $default_currency_id = Studio::model()->findByPk($studio_id, array('select' => 'default_currency_id'))->default_currency_id;
                    $final_content = Yii::app()->custom->AppSectionProducts($section->id, $studio_id, $default_currency_id, true);
                } else {
                    foreach ($section->contents as $featured) {
                        if ($featured->is_episode == 1) {
                            $streamIds .="," . $featured->movie_id;
                        } else {
                            $movieIds .= "," . $featured->movie_id;
                        }
                    }
                    $cond = '';
                    if (@$movieIds || @$streamIds) {
                        $cond = " AND (";
                        if (@$movieIds) {
                            $movieIds = ltrim($movieIds, ',');
                            $cond .="(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                            $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                        }
                        if ($streamIds) {
                            $streamIds = ltrim($streamIds, ',');
                            $cond .=" (M.id IN(" . $streamIds . "))";
                            $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                        } else {
                            $cond = str_replace(' OR ', ' ', $cond);
                        }
                        $cond .= ")";
                    }

                    $sql = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
                        . "( SELECT M.movie_id,M.is_converted,M.video_duration,M.is_episode,M.embed_id,M.id AS movie_stream_id,F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id,M.episode_title,M.episode_story,M.episode_number,M.series_number, M.last_updated_date "
                        . "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) AND  M.studio_id=" . $studio_id . " " . $cond . " ) AS PM "
                        . " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
                    $sql_data = "SELECT P.* FROM (SELECT t.*,g.* FROM (" . $sql . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                    $where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
                    $sql_data .= $where . " " . $orderBy;
                    $content_list = Yii::app()->db->createCommand($sql_data)->queryAll();
                    if(!empty($content_list)){
                        foreach ($content_list AS $key => $val) {
							$val['stream_id'] = $val['movie_stream_id'];
                            $langcontent = array();
                            if($language_id !=20){
                                $langcontent = Yii::app()->custom->getTranslatedContent($val['movie_id'], @$val['is_episode'], $language_id, $studio_id);
                            }
                            $arg['studio_id']  = $studio_id;
                            $arg['movie_id']   = $val['movie_id'];
                            $arg['season_id']  = 0;
                            $arg['episode_id'] = 0;
                            $arg['content_types_id'] = $content_types_id = $val['content_types_id'];
                            $isFreeContent = Yii::app()->common->isFreeContent($arg);
                            $embededurl = $domainName.'/embed/'.$val['embed_id'];
                            if ($val['is_episode'] == 1) {
                                if (array_key_exists($val['stream_id'], @$langcontent['episode'])) {
                                    $val['episode_title'] = @$langcontent['episode'][$val['stream_id']]->episode_title;
                                    $val['episode_story'] = @$langcontent['episode'][$val['stream_id']]->episode_story;
                                }
                                $cont_name = ($val['episode_title'] != '') ? $val['episode_title'] : "SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                                $story = $val['episode_story'];
                                $release = $val['episode_date'];
                                $poster = $controller->getPoster($val['stream_id'], 'moviestream', 'episode',$studio_id);
                            }else{
                                if (array_key_exists($val['movie_id'], @$langcontent['film'])) {
                                    $val['name'] = @$langcontent['film'][$val['movie_id']]->name;
                                    $val['story'] = @$langcontent['film'][$val['movie_id']]->story;
                                    $val['genre'] = @$langcontent['film'][$val['movie_id']]->genre;
                                    $val['censor_rating'] = @$langcontent['film'][$val['movie_id']]->censor_rating;
                                }
                                $cont_name = $val['name'];
                                $story = $val['story'];
                                $release = $val['release_date'];
                                $stream = movieStreams::model()->findByPk($val['stream_id']);
                                if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {                    
                                    $poster = $controller->getPoster($val['movie_id'], 'films', 'episode',$studio_id);                        
                                } else {
                                    $poster = $controller->getPoster($val['movie_id'], 'films', 'standard',$studio_id);                        
                                }                        
                            }
                            $geo = GeoblockContent::model()->find('movie_id=:movie_id', array(':movie_id' => $val['movie_id']));
                            $viewStatus = VideoLogs::model()->getViewStatus($val['movie_id'],$studio_id);
                            if(@$viewStatus){
                                $viewStatusArr = '';
                                foreach ($viewStatus AS $valarr){
                                    $viewStatusArr['viewcount'] = $valarr['viewcount']; 
                                    $viewStatusArr['uniq_view_count'] = $valarr['u_viewcount']; 
                                }
                            }                    
                            if(isset($stream['content_publish_date']) && @$stream['content_publish_date'] && $stream['content_publish_date'] > gmdate("Y-m-d H:i:s")){}else{
                                $final_content[$key]['is_episode'] = $val['is_episode'];
                                $final_content[$key]['movie_stream_uniq_id'] = $val['embed_id'];
                                $final_content[$key]['movie_id'] = $val['movie_id'];
                                $final_content[$key]['movie_stream_id'] = $val['stream_id'];
                                $final_content[$key]['muvi_uniq_id'] = $val['uniq_id'];
                                $final_content[$key]['content_type_id'] = $val['content_type_id'];
                                $final_content[$key]['ppv_plan_id'] = $val['ppv_plan_id'];
                                $final_content[$key]['permalink'] = $val['permalink'];
                                $final_content[$key]['name'] = $cont_name;
                                $final_content[$key]['full_movie'] = $stream->full_movie;
                                $final_content[$key]['story'] = $story;
                                $final_content[$key]['genre'] = json_decode($val['genre']);
                                $final_content[$key]['censor_rating'] = ($val['censor_rating'] != '') ? implode(',', json_decode($val['censor_rating'])) . '&nbsp;' : '';
                                $final_content[$key]['release_date'] = $release;
                                $final_content[$key]['content_types_id'] = $val['content_types_id'];
                                $final_content[$key]['is_converted'] = $val['is_converted'];
                                $final_content[$key]['last_updated_date'] = '';                    
                                if($user_id){
                                    $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id ,$user_id,$val['movie_id'],$val['movie_stream_id'] );
                                    $final_content[$key]['watch_duration'] = $watch_duration['resume_time'];
                                    $final_content[$key]['video_duration'] = $val['video_duration'];
                                }
                                $final_content[$key]['movieid'] = $geo->movie_id;
                                $final_content[$key]['geocategory_id'] = $geo->geocategory_id;                    
                                $final_content[$key]['category_id'] = $restriction->category_id;
                                $final_content[$key]['studio_id'] = $studio_id;
                                $final_content[$key]['country_code'] = $restriction->country_code;
                                $final_content[$key]['ip'] = $restriction->ip;
                                $final_content[$key]['poster_url'] = $poster;
                                $final_content[$key]['isFreeContent'] = $isFreeContent;
                                $final_content[$key]['embeddedUrl'] = $embededurl;
                                $final_content[$key]['viewStatus'] = $viewStatusArr;                    
                            } 
                        }
                    }
                }
            }
        }
        return @$final_content;
    }    
}
