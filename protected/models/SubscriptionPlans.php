<?php
class SubscriptionPlans extends CActiveRecord{
    public $price;
    public $currency_id;
    
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'subscription_plans';
    }
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'studio'=>array(self::HAS_ONE, 'Studio','studio_id'),
             'SubscriptionPricing'=>array(self::HAS_MANY, 'SubscriptionPricing','subscription_plan_id',
                'condition'=>'SubscriptionPricing.status=1'
            ),
            'subscriptionbundlescontent'=>array(self::HAS_MANY, 'SubscriptionBundlesContent','subscriptionbundles_plan_id')
        );
    }
    
    public function getPlanDetails($plan_id,$studio_id, $default_currency_id = NULL, $country = NULL, $is_api = 0)
    {
		if (intval($is_api)) {
			$str_cond = 'sp.unique_id=:unique_id';
			$cond = array(':unique_id'=>$plan_id, ':studio_id'=>$studio_id, ':sp_status'=>1, ':spr_status'=>1);
		} else {
			$str_cond = 'sp.id=:id';
			$cond = array(':id'=>$plan_id, ':studio_id'=>$studio_id, ':sp_status'=>1, ':spr_status'=>1);
		}
        $data = Yii::app()->db->createCommand()
                ->select('sp.*,spr.*,sp.id as id')
                ->from('subscription_plans sp')
                ->join('subscription_pricing spr', 'spr.subscription_plan_id = sp.id')
                ->where($str_cond.' AND studio_id=:studio_id AND sp.status=:sp_status AND spr.status =:spr_status', $cond)
                ->queryRow();
        
        if (!isset($default_currency_id)) {
            $controller = Yii::app()->controller;
            $default_currency_id = $controller->studio->default_currency_id;
        }
        $price_list = Yii::app()->common->getUserSubscriptionPrice($data['id'], $default_currency_id, $country);
        $data['price'] = $price_list['price'];
        $data['currency_id'] = $price_list['currency_id'];
        
        return $data;
    }
    
    
     public function getAllSubscriptionPlanDetails($studio_id, $default_currency_id = NULL, $country = NULL)
    {
        $data = Yii::app()->db->createCommand()
                ->select('count(sp.id) as subscriptionsplancount,sp.*,spr.*,sp.id as id')
                ->from('subscription_plans sp')
                ->join('subscription_pricing spr', 'spr.subscription_plan_id = sp.id')
                ->where('studio_id=:studio_id AND sp.status=:sp_status AND spr.status =:spr_status', array(':studio_id'=>$studio_id, ':sp_status'=>1, ':spr_status'=>1))
                ->queryAll();
         return $data;
}
    
    public function IsSubscriptionBundlesExists($studioId,$contentId){
         $dataSubscriptionBundles = Yii::app()->db->createCommand()
                       ->select('sp.id as id')
                       ->from("subscription_plans  sp")
                       ->Join('subscriptionbundles_content sbc' , 'sbc.subscriptionbundles_plan_id = sp.id')
                       ->where('sp.studio_id =:studio_id  AND sbc.content_id=:content_id AND sp.status=1',array(':studio_id' => $studioId,':content_id'=>$contentId))
                       ->queryRow();
       $data['id'] = $dataSubscriptionBundles['id']; 
        return $data;
    }
    
}
