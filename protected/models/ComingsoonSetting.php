<?php
class ComingsoonSetting extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'comingsoon_settings';
    }
    
    public function relations()
    {
    }
    public function getComingSoonSetting($studio_id){
        $comingsoon = ComingsoonSetting::model()->find(array(
            'condition' => 'studio_id=:studio_id',
            'params' => array(':studio_id' => $studio_id),
            'order' => 't.id ASC'
        ));  
        return $comingsoon;           
    }
}
