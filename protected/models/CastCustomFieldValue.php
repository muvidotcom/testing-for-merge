<?php
class CastCustomFieldValue extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'cast_custom_field_value';
    }
    public function getCustomfieldvalues($studio_id, $celeb_id){
        $custom_celeb_val = array();
        $celeb = $this->findAllByAttributes(array('studio_id' => $studio_id, 'celeb_id' => $celeb_id));
        if(!empty($celeb)){
            $custom_celeb_val = CHtml::listData($celeb, 'field_id', 'value');
        }
        return $custom_celeb_val;
    }
}
?>

