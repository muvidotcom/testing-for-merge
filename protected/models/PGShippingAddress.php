<?php

class PGShippingAddress extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'pg_shipping_address';
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    /* 
     * This function is insert data into order table
     */
    public function insertAddress($studio_id,$buyer_id,$orderid,$shipping_addr,$ip){
        $this->order_id = $orderid;
        $this->first_name = $shipping_addr['first_name'];
        $this->last_name = $shipping_addr['last_name'];
        $this->address = $shipping_addr['address'];
        $this->city = $shipping_addr['city'];
        $this->state = $shipping_addr['state'];
        $this->country = $shipping_addr['country'];
        $this->zip = $shipping_addr['zip'];
        $this->phone_number = $shipping_addr['phone_number'];
        $this->user_id = $buyer_id;
        $this->studio_id = $studio_id;
        $this->created_date = new CDbExpression("NOW()");
        $this->ip = $ip;
        $this->save();
    }
}