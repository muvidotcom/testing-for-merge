<?php
class PciLog extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'pci_log';
    } 
    
    public function updateErrorLogIPPayment($pciId,$req){
        $db_user = Yii::app()->db;
        $update_sql =  $db_user->createCommand()
	->update('pci_log', array('log_data'=>$req), 'id=:id', array(':id'=>$pciId));
}
}
