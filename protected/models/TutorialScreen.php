<?php
class TutorialScreen extends CActiveRecord {
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'mobile_app_tutorial_screen';
    }
    
    public function getAll($studio_id, $type) {
        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from($this->tableName())
                ->where('studio_id=:studio_id AND app_type=:type ORDER BY sequence', array(':studio_id' => $studio_id, ':type' => $type))
                ->queryAll();
        return $data;
    }
    
    public function updateSequence($studio_id, $type, $sequence) {
        $data = self::getAll($studio_id,$type);
        $data_seq = explode(",", $sequence);
        
        for($i=0; $i < count($data_seq); $i++){
            $db_user = Yii::app()->db;
            Yii::app()->db->createCommand()->update($this->tableName(),array('sequence'=>$data_seq[$i]),'id=:id AND studio_id=:studio_id AND app_type=:app_type',array(':id'=>$data[$i]['id'],':studio_id'=>$studio_id,':app_type'=>$type));
        }
    }
    public function deleteScreen($studio_id, $type, $img_id){
        Yii::app()->db->createCommand()->delete($this->tableName(), 'id=:id AND studio_id=:studio_id AND app_type=:app_type', array(':id'=>$img_id,':studio_id'=>$studio_id,':app_type'=>$type));
    }
    public function getMaxSequence($studio_id, $type){
        $data = Yii::app()->db->createCommand()->select('MAX(sequence) as max_seq')->from($this->tableName())->where('studio_id=:studio_id AND app_type=:app_type', array(':studio_id'=>$studio_id,':app_type'=>$type))->queryRow();
        return $data;
    }
    
}
?>