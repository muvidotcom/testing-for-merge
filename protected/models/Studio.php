<?php
class Studio extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'studios';
    }
    /**
    * @return array relational rules.
    */
    public function relations()
    {
           // NOTE: you may need to adjust the relation name and the related
           // class name for the relations automatically generated below.
           return array(
               'users'=>array(self::HAS_MANY, 'User','studio_id'),
               'films'=>array(self::HAS_MANY, 'Film','studio_id'),
               'celebrities'=>array(self::HAS_MANY, 'Celebrity','studio_id'),
               'studiopaymentgateways'=>array(self::MANY_MANY, 'PaymentGateways','studio_payment_gateways(studio_id, gateway_id)'),
               'subscrpln'=>array(self::MANY_MANY, 'SubscriptionPlans','studio_subscription_plans(studio_id, id)'),
               'pages'=> array(self::HAS_MANY, 'Page', 'studio_id'),
               'studio_banners'=> array(self::HAS_MANY, 'StudioBanner', 'studio_id'),
           );
    }
	
	/**
	 * @method public checkSubdomain($subdomain) Check a subdomain and returns a uniq subdomain for the domain
	 * @author GDR <support@muvi.com>
	 * @return string Returns the unique subdomain
	 */
	public function checkSubdomain($subdomain=''){
		if($subdomain){
			for($i=1;$i>=1;$i++){
				$data = $this->findByAttributes(array('subdomain'=>$subdomain));
				if(!$data){
					break;
				}else{
					$subdomain .=$i;
				}
			}
			return $subdomain;
		}else{return FALSE;}
	}
        
        public function updateServerLocation($s3bucket_id,$studio_id)
        {
            $std = Studio::model()->findbyPk($studio_id);
            $std->s3bucket_id = $s3bucket_id;
            return $std->update();
        }
        
        public function updateadvancedsetting($google_analytics,$google_webmaster,$robots_txt,$studio_id)
        {
            $std = Studio::model()->findbyPk($studio_id);
            $std->google_analytics = $google_analytics;
            $std->additional_meta_contents = $google_webmaster;
            $std->robots_txt = $robots_txt;
            return $std->update();
        }
        
        public function updateStudio($studio_id,$studio_data)
        {
            $data = Yii::app()->db->createCommand()
                        ->update($this->tableName(),$studio_data,'id=:studio_id',array(':studio_id' => $studio_id));
            return $data;
        }
        public function getStudioBucketInfo($studio_id){
            $studioData = Yii::app()->db->createCommand()
            ->select('s3bucket_id,studio_s3bucket_id,is_subscribed,new_cdn_users,cdn_status,signed_url,unsigned_url,video_url')
            ->from('studios')
            ->where('id='.$studio_id)
            ->queryAll();
            return $studioData[0];
        }
        public function getStudioBucketId($studio_id) {
//			if($studio_id == Yii::app()->common->getStudiosId()){
//				$controller= new Controller();
//				$studioData['studio_s3bucket_id']= $controller->studio->studio_s3bucket_id;
//				$studioData['theme']= $controller->studio->theme;
//			}else{
				if(@$_SESSION[$studio_id]['StudioBucketId']){
					return $_SESSION[$studio_id]['StudioBucketId'];
				}else{
					$studioData = Yii::app()->db->createCommand()
					->select('studio_s3bucket_id,theme')
					->from('studios')
					->where('id='.$studio_id)
					->queryRow();
					$_SESSION[$studio_id]['StudioBucketId']= $studioData;
				}
			//}
			return $studioData;
        }
        public function getStudioDataForLogo($studio_id) {
            $studioData = Yii::app()->db->createCommand()
            ->select('new_cdn_users,theme,logo_file,favicon_name')
            ->from('studios')
            ->where('id='.$studio_id)
            ->queryAll();
            return $studioData[0];
        }
        public function getStudioBucketData($studio_id) {
            $studioData = Yii::app()->db->createCommand()
            ->select('studio_s3bucket_id,new_cdn_users,s3bucket_id')
            ->from('studios')
            ->where('id='.$studio_id)
            ->queryAll();
            return $studioData[0];
        }
        public function getStudioName($studio_id) {
            $studioData = Yii::app()->db->createCommand()
            ->select('name')
            ->from('studios')
            ->where('id='.$studio_id)
            ->queryAll();
            return $studioData[0]['name'];
        }
        public function getStudioMrssCode($studio_id) {
            $studioData = Yii::app()->db->createCommand()
            ->select('mrss_id')
            ->from('studios')
            ->where('id='.$studio_id)
            ->queryAll();
            return $studioData[0]['mrss_id'];
        }
        public function getStudioDetailsFromMrssCode($mrss_id) {
            $studioData = Yii::app()->db->createCommand()
            ->select('id')
            ->from('studios')
            ->where('mrss_id="'.$mrss_id.'"')
            ->queryAll();
            if(isset($studioData[0])){
                return $studioData[0]['id'];
            }else{
                return '';
            }
        }
        public function save_reseller_customer_data($data = array()){
            if(count($data)>0){
                foreach($data as $key => $val){
                    $this->$key = $val;
}
                $this->save();
                $studio_id = $this->id;
                $this->mrss_id = md5($studio_id);
                $this->save();                
               return $studio_id;
            }
        }
    public function find_reseller_customer($usrs,$offset,$page_size,$search=''){
        if($search != ''){
            $add_where = "s.domain like :domain,array(':domain'=>$search)";
        }else{
            $add_where = '';
        }
        $user_id = explode(',', $usrs);
        $customer = Yii::app()->db->createCommand()
        ->select('s.*,u.id AS user_id,u.*')
        ->from('studios s')
        ->leftjoin('user u', 's.id = u.studio_id')
        ->where('u.is_admin=:is_admin and u.is_sdk=:issdk and u.role_id=:role_id',array(':is_admin'=>0,':issdk'=>1,':role_id'=>1))
        ->andWhere(array('IN','u.id',$user_id))
        ->andWhere("s.domain like :domain or u.email like :email or u.first_name like :fname",array(':domain'=>'%'.$search.'%',':email'=>'%'.$search.'%',':fname'=>'%'.$search.'%'))
        ->order('u.id DESC')
        ->offset($offset)
        ->limit($page_size)
        ->queryAll();
        
        $count = count($customer);
         $arr['customer'] = $customer;
         $count = Yii::app()->db->createCommand()
        ->select('count(*) as count')
        ->from('studios s')
        ->leftjoin('user u', 's.id = u.studio_id')
        ->where('u.is_admin=:is_admin and u.is_sdk=:issdk and u.role_id=:role_id',array(':is_admin'=>0,':issdk'=>1,':role_id'=>1))
        ->andWhere(array('IN','u.id',$user_id))
        ->order('u.created_at DESC')
        ->queryAll(); 
         $arr['count'] = $count[0]['count'];
        return $arr;
    }
    public function delink_reseller_customer($studio_id,$portal_user_id){
        $start_date = date('Y-m-d', strtotime("+15 days"));
        $end_date = date('Y-m-d H:i:s', strtotime($start_date . ' +1 Months -1 Days'));
         $sql = Yii::app()->db->createCommand()
         ->update('studios',array('reseller_id'=>0,'delink_from'=>$portal_user_id),'id=:studioid',array(':studioid'=>$studio_id));
              
    }
}
