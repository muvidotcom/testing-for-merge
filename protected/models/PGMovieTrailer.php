<?php
class PGMovieTrailer extends CActiveRecord{
	public static function model($className=__CLASS__){
        return parent::model($className);
    }
 
    public function tableName() {
        return 'pg_movie_trailer';
    }    
    public function updateStratTime($trailer_id,$upload_start_time)
    {
        $data = Yii::app()->db->createCommand()
                ->update($this->tableName(),array('upload_start_time' => $upload_start_time,'upload_end_time'=>'','upload_cancel_time'=>'','encoding_start_time'=>'','encoding_end_time'=>'','encode_fail_time'=>''),'id=:id',array(':id'=>$trailer_id));
        return $data;
    }
}  