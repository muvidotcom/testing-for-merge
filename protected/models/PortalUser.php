<?php
class PortalUser extends CActiveRecord{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function tableName()
    {
        return 'portal_user';
    }
    public function relations()
               {
                       // NOTE: you may need to adjust the relation name and the related
                       // class name for the relations automatically generated below.
                       return array(
                       );
               }
                public function get_reseller_parent_id($user_id){
        $parentsql = $level = Yii::app()->db->createCommand()
                    ->select('parent_id')
                    ->from('portal_user')
                    ->where('id=:id',array(':id'=>$user_id))
                    ->queryRow();
          return  $parentsql;    
    }
    public function get_portal_username($id){
        $sql = Yii::app()->db->createCommand()
              ->select('name')
              ->from('portal_user')
              ->where('id=:id',array(':id'=>$id))
              ->queryRow();
       return $sql;
    }
}
