<?php

/**
 * Policy class has been written for controller and model only
 * @author Chinmay<chinmay@muvi.com>
 */
class Policy extends AppComponent {

    function getAllPolicyRules($studio_id = null) {
        if (!$studio_id) {
            $studio_id = Yii::app()->user->studio_id;
        }
        return PolicyRules::model()->findAll('studio_id=:studio_id AND status=1', array(':studio_id' => $studio_id));
    }

    public function policy_count($studio_id) { /* Not used this function */
        $data = StudioConfig::model()->getConfig($studio_id, 'enable_policy');
        return $data;
    }

    function chkUesrSubscription($subscriptoin_id = null) {
        /*if ($subscriptoin_id) {
            $subscriptionPlan = SubscriptionPlans::model()->find(array('select' => 'rules_id', 'condition' => 'id=:id', 'params' => array(':id' => $subscriptoin_id)))->rules_id;
            if (!$subscriptionPlan) {
                return false;
            }
            if ($subscriptionPlan) {
                $chkUserSubscriptoin = UserSubscription::model()->count(array('select' => 'id', 'condition' => 'plan_id=:subscription_id', 'params' => array(':subscription_id' => $subscriptoin_id)));
                return $chkUserSubscriptoin > 0 ? true : false;
            } else {
                return false;
            }
        }*/
        return false;
    }

    function chkPPVSubscription($plan_id = null) {
        /*if ($plan_id) {
            $ppvPlan = PpvPlans::model()->find(array('select' => 'rules_id', 'condition' => 'id=:id', 'params' => array(':id' => $plan_id)))->rules_id;
            if ($ppvPlan) {
                $chkPpvSubscriptoin = PpvSubscription::model()->count(array('select' => 'id', 'condition' => 'ppv_plan_id=:plan_id', 'params' => array(':plan_id' => $plan_id)));
                return $chkPpvSubscriptoin > 0 ? true : false;
            } else {
                return false;
            }
        } */
        return false;
    }
    
    function chkVoucherSubscription($voucher_id = null) {
        if ($voucher_id) {
            $chk = CouponOnly::model()->find(array('select' => 'rules_id', 'condition' => 'id=:id', 'params' => array(':id' => $voucher_id)))->rules_id;
            if ($chk) {
                $chkPpvSubscriptoin = VoucherSubscription::model()->count(array('select' => 'id', 'condition' => 'voucher_id=:voucher_id', 'params' => array(':voucher_id' => $voucher_id)));
                return $chkPpvSubscriptoin > 0 ? true : false;
            } else {
                return false;
            }
        }
        return false;
    }

}
