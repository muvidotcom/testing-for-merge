<?php
/**
 * @package CustomForms This class will contain all the Custom form related methods & properties
 * @author Gayadhar<support@muvi.com>
 */
class CustomForms extends AppComponent{
/**
 * @method public getcustomMetadata() This will create a new custom form 
 * @author Gayadhar<support@muvi.com>
 * @param int $studio_id 
 * @param int $form_type 
 * @return HTML It will return the HTML form
*/
	function getCustomMetadata($studio_id,$form_type,$arg=array()){
        $condition = 'studio_id=:studio_id AND parent_content_type_id=:parent_content_type_id AND content_type=:content_type AND is_child=:is_child'; 
        $params = array(':studio_id'=>$studio_id,':parent_content_type_id'=>$form_type,':content_type'=>$arg['content_type'],':is_child'=>$arg['is_child']);
        if(@$arg['editid']){
            $condition.=' AND id=:id';
            $params[':id']=$arg['editid'];
        }
        $customForm = CustomMetadataForm::model()->find($condition,$params);        
		if($customForm){
			$command = Yii::app()->db->createCommand()
				->from('custom_metadata_form_field t,custom_metadata_field f')
				->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value,f.f_is_required,f.field_type')
				->where('t.custom_field_id = f.id AND t.custom_form_id='.$customForm->id.' AND f.studio_id ='.$studio_id)
				->order('t.id_seq ASC');
			$data = $command->queryAll();
			if($data){
				$data['formData'] = $customForm->attributes;
				return $data;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
        /*getTagdata by manas@muvi.com*/
        function getTagData($studio_id){
            $main = array();
            if($studio_id){
                $tagarray = array('genre','language','censor_rating');
                foreach($tagarray as $k=>$fieldname){
                    $genre = "SELECT {$fieldname} FROM films WHERE {$fieldname} LIKE '%[%' AND studio_id = ".$studio_id;
                    $genrename = Yii::app()->db->createCommand($genre)->queryAll();
                    $genarr=array();
                    $gvarr=array();
                    foreach($genrename as $gval)
                    {
                        $gv = json_decode($gval[$fieldname]);

                        foreach($gv as $gvval)
                        {                    
                            $rawval = strtoupper(trim($gvval));
                            $gvarr[] = $rawval;
                        }

                        $genarr = array_unique(array_merge($genarr,$gvarr));
                    }
                    $main[$fieldname] = $genarr;
                }
            }            
            return $main;
        }
/**
 * @method public GetFormCustomMetadat() This will create a new custom form 
 * @author Gayadhar<support@muvi.com>
 * @param int $studio_id 
 * @param int $form_id It will be used a content types id for old implementation, Once phase II is live then its real form id
 * @return HTML It will return the HTML form
*/	
	function getFormCustomMetadat($studio_id,$form_type_id,$form_id=0){
		if(!$studio_id){
			$studio_id = Yii::app()->common->getStudiosId();
		}
		$limit = 9;
		$tbl = 'film_custom_metadata';
		if($form_type_id==4){
			$tbl = 'movie_streams_custom_metadata';
			$limit = 5;
		}elseif($form_type_id==6){
			$limit = 5;
			$tbl = 'pg_custom_metadata';
		}
		if($form_type_id){
			$sql = "SELECT p.*, cm.field_name FROM (SELECT cf.id,cf.f_display_name,cf.f_id FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND f.form_type_id = {$form_type_id} AND f.studio_id ={$studio_id})  AS p, {$tbl} cm WHERE p.id = cm.custom_field_id LIMIT {$limit}";
			$customData = Yii::app()->db->createCommand($sql)->queryAll();
			return $customData;
		}else{
			return '';
		}
	}

	/**
	 * @method getEpisodeCustomData is used for provide custom field values 
	 * @param int custom_metadata_form_id it use "movie_stream" table
	 * @param int studio_id 
	 * @return array customfield value with key 
	 */
	function getEpisodeCustomData($custom_metadata_form_id, $studio_id, $custom_table = 'movie_streams_custom_metadata') {
		if ($custom_metadata_form_id) {
			$sql_cus = "SELECT p.*, cm.field_name FROM (SELECT cf.id,cf.f_display_name,cf.f_id FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND f.id = {$custom_metadata_form_id} AND f.studio_id ={$studio_id})  AS p, {$custom_table} cm WHERE p.id = cm.custom_field_id AND cm.custom_metadata_form_id={$custom_metadata_form_id}";
		} else {
			$sql_cus = "SELECT p.*, cm.field_name FROM (SELECT cf.id,cf.f_display_name,cf.f_id FROM custom_metadata_form f,custom_metadata_field cf, custom_metadata_form_field cff WHERE f.id=cff.custom_form_id AND cf.id = cff.custom_field_id AND f.studio_id ={$studio_id}) AS p, {$custom_table} cm WHERE p.id = cm.custom_field_id GROUP BY p.id";
		}
		$command_custom = Yii::app()->db->createCommand($sql_cus)->QueryAll();
		return $command_custom;
	}
	/**
	 * @method getEpisodeCustomValue is used for assign values to custom field 
	 * @param array $command_custom result from getEpisodeCustomData 
	 * @param array $content data
	 * @return array $custom_vals
	 */
	function getEpisodeCustomValue($command_custom,$content) {
		$custom_vals = array();
		if (!empty($command_custom)) {
			$x = (@$content['custom6']) ? json_decode($content['custom6'], true) : 0;
			foreach ($command_custom as $key => $ar) {
				$ar_k = trim($ar['f_id']);
				$ar_k1 = trim($ar['field_name']);
				$k = (int) str_replace('custom', '', $ar_k1);
				if ($k > 5) {
					if ($x) {
						foreach ($x as $ckey => $value) {
							foreach ($value as $key1 => $value1) {
								if ($key1 == $ar['field_name']) {
									$custom_vals[$ar_k] = array(
										'field_display_name' => trim($ar['f_display_name']),
										'field_value' => trim(is_array($value1) ? implode(',',$value1) : $value1)
									);
								}
							}
						}
					}
				} else {
					$custom_vals[$ar_k] = array(
						'field_display_name' => trim($ar['f_display_name']),
						'field_value' => trim(is_array(json_decode($content[$ar_k1])) ? implode(', ', json_decode($content[$ar_k1])) : $content[$ar_k1])
					);
				}				
			}
			return $custom_vals;
		}else{
			return '';
		}
	}

}