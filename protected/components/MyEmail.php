<?php
use Aws\Ses\SesClient; //for sending email by amason ses sdk
class MyEmail extends CApplicationComponent {

    function sendMandrilEmail($template_name, $template_content, $message) {

        require_once 'mandrill-api-php/src/Mandrill.php';
        $mandrill = new Mandrill('Xb3a0BtRxPQIJTbJwn1Vlg');
        $async = false;
        try {
            $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
            return $result;
        } catch (Exception $e) {
            $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
            fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage() . "\n Template name-" . $template_name . '\n Template Content:-' . print_r($template_content, TRUE));
            fclose($fp);
            return false;
        }
    }
    
    
    // for sending email by amason ses sdk
    /*function sendmailViaAmazonsdk($html, $subject, $to, $from,$cc=array(),$bcc=array(),$reply_to=array()) {
       $from_email=$from;
       $from_explode=explode('@',$from_email);
       $from_domain=$from_explode[1];
       $client = SesClient::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
                    'region' => 'us-east-1',
        ));
   
       
       if(trim($from_domain)!='muvi.com')
       {
         $list_of_verified_email = array();
                   
                    //get the list of verified email address
                    $result = $client->listVerifiedEmailAddresses();

                    $list_of_verified_email = $result['VerifiedEmailAddresses'];
                                        
                    //check whether the user email is verified or not ?
                
         $from=(in_array($from_email,$list_of_verified_email))?$from_email:'noreply@muvi.com';
         
       }
      
            try {
                 $result = $client->sendEmail(array(
                // Source is required
                'Source' => $from,
                // Destination is required
                'Destination' => array(
                    'ToAddresses' =>$to,
                    'CcAddresses' => $cc,
                    'BccAddresses' => $bcc,
                ),
                // Message is required
                'Message' => array(
                    // Subject is required
                    'Subject' => array(
                        // Data is required
                        'Data' => $subject,
                        'Charset' => 'UTF-8',
                    ),
                    // Body is required
                    'Body' => array(
                        'Html' => array(
                            // Data is required
                            'Data' => $html,
                            'Charset' => 'UTF-8',
                        ),
                    ),
                ),
                'ReplyToAddresses' => $reply_to,
            ));
            return $result;
        } Catch (Exception $e) {
            $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
            fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage() . "\n Template name-" . $template_name . '\n Template Content:-' . print_r($template_content, TRUE));
            fclose($fp);
            return false;
        }
    } */
	
    function sendmailViaAmazonsdk($html, $subject, $to, $from,$cc=array(),$bcc=array(),$reply_to=array(),$from_name = 'Muvi') {
		require 'sendgrid-php/vendor/autoload.php';
		$sendgrid = new SendGrid(SENDGRID_APIKEY);
		$email = new SendGrid\Email();
        try{
			$reply_to = $reply_to ?$reply_to:$from; 
			$email->addTo($to)
				->addCc($cc)
				->addBcc($bcc)
				->setReplyTo($reply_to)
				->setFromName(@$from_name)
				->setFrom($from)
				->setSubject($subject)
				->setHtml($html);
			$result=$sendgrid->send($email);
			return $result;  
         } catch (Exception $e) {
             $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
             fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage());
             fclose($fp);
             return false;
         }
    }

    function getCommonParams($arg = 0) {
        $location = '';
        if (intval($arg)) {
            $ip_address = CHttpRequest::getUserHostAddress();
            $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
            $location = Yii::app()->common->getLocationTextFromData('', $visitor_loc);            
        }

        //Sending Email to Sales Team
        $site_url = Yii::app()->getBaseUrl(true);
        $store_details = Yii::app()->common->storeDetails('studio');

        $logo = EMAIL_LOGO;
        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
        $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';


        $params = array(
            array('name' => 'website_name', 'content' => 'Muvi'),
            array('name' => 'logo', 'content' => @$logo),
            array('name' => 'location', 'content' => @$location),
            array('name' => 'fb_link', 'content' => @$fb_link),
            array('name' => 'twitter_link', 'content' => @$twitter_link),
            array('name' => 'gplus_link', 'content' => @$gplus_link)
        );

        return $params;
    }
    
    
    //get common params amazon sdk 
    
    function getCommonParamSes($arg = 0) {
        $location = '';
        if (intval($arg)) {
            $ip_address = CHttpRequest::getUserHostAddress();
            $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
            $location = Yii::app()->common->getLocationTextFromData('', $visitor_loc);
        }

        //Sending Email to Sales Team
        $site_url = Yii::app()->getBaseUrl(true);
        $store_details = Yii::app()->common->storeDetails('studio');

        $logo = EMAIL_LOGO;
        $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="Muvi" /></a>';
        $fb_link = '<a href="' . @$store_details['fb_link'] . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        $twitter_link = '<a href="' . @$store_details['twitter_link'] . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        $gplus_link = '<a href="' . @$store_details['gplus_link'] . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';


        $params = array(
           'website_name' => 'Muvi',
            'logo' =>@$logo,
            'location' => @$location,
            'fb_link' => @$fb_link,
            'twitter_link' => @$twitter_link,
            'gplus_link' => @$gplus_link
            );

        return $params;
    }

    public function emailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        $params = array(
            'name'=> ucfirst(@$req['name']),
            'email'=> @$req['email'],
            'phone'=> @$req['phone'],
            'company'=> @$req['companyname'] );
       
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('autosignup_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('autosignup_from');
        $to=$adminGroupEmail;
        $adminSubject = "New Free Trial";
        $subject=$adminSubject;
        $cc=array();
        $bcc=array('development@muvi.com');
        $from= 'studio@muvi.com';
        //$template_name = 'signup_sales_welcome';
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/signup_sales_welcome',array('params'=>$params),true);
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc,'','Muvi'); 
        return $returnVal;
    }

    public function emailToUser($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');
        $cmnparams = $this->getCommonParamSes();
        $params = array(
            'fname'=> ucfirst($req['name']),
            'email'=> $req['email'],
            'domain' => $req['domain'],
            'trial_period'=> $config[0]['value'],
            'expiry_date'=> date('d F, Y', strtotime("+{$config[0]['value']} days"))
        );
        $params = array_merge($cmnparams, $params);
        
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('autosignup_from');
        $toEmail = array($req['email']);
        $adminSubject = "Getting started with Muvi";
        $subject=$adminSubject;
        $to=$toEmail;
        
        $from=$autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/studio_signup_touser',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi'); 
        
        return $returnVal;
    }

    public function subscriptionPurchaseMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        
        $cloud_hosting ='';
        if (isset($req['is_cloud_hosting']) && intval($req['is_cloud_hosting'])) {
            $cloud_hosting = '<strong>Add Cloud hosting for your application (powered by Amazon Web Services)</strong><br/>';
        }
        
        $params = array(
            'package_name'=> ucwords(@$req['package_name']),
            'applications'=> ucwords(@$req['applications']),
            'plan'=> ucwords(@$req['plan']),
            'amount_charged'=> '$' . @$req['amount_charged'],
            'name'=> ucfirst(@$req['name']),
            'email'=> @$req['email'],
            'phone'=> @$req['phone'],
            'cloud_hosting' => $cloud_hosting,
            'company'=> @$req['companyname']
             );
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('subscription_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "New paying customer!";
        $subject=$adminSubject;
       
        $to=$adminGroupEmail;
        
        $from=$autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/subscription_sales',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from); 
       
        return $returnVal;
    }

    public function subscriptionPurchaseMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $adminSubject = ucwords($req['package_name']) . " subscription activated!";

        if (trim($invoice_file)) {
            $attachments = array(ROOT_DIR . 'docs/' . $invoice_file);
            $attached_file = "Please find the invoice attached here.";
        } else {       
            $attached_file = "";
            $attachments=array();
        }

        $params = array(
            'fname'=> ucfirst($req['name']),
            'email'=> $req['email'],
            'domain'=> $req['domain'],
            'attached_file'=> $attached_file);
        

        $params = array_merge($cmnparams, $params);

        $template_name = 'subscription_activation';
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$autosignup_from[0];
       
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/subscription_activation',array('params'=>$params),true);
        
        $returnVal= $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachments);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }
    /*function sendAttchmentMailViaAmazonsdk($to, $subject, $from, $html,$attachments,$cc='',$bcc='')
    {
      $from_email=$from;
       $from_explode=explode('@',$from_email);
       $from_domain=$from_explode[1];
       $client = SesClient::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
                    'region' => 'us-east-1',
        ));
      
       if(trim($from_domain)!='muvi.com')
       {
         $list_of_verified_email = array();
                   
                    //get the list of verified email address
                    $result = $client->listVerifiedEmailAddresses();

                    $list_of_verified_email = $result['VerifiedEmailAddresses'];
                                        
                    //check whether the user email is verified or not ?
                
         $from=(in_array($from_email,$list_of_verified_email))?$from_email:'noreply@muvi.com';
         
       }
      
      
		//Create a random boundary
		$boundary = md5(rand() . time() . 'Suraja');

		//lets get started, the headers come first. Pay attention to the blank lines, they are important.
		//Following the headers is our first part of the email. The plain text version.
		$rawEmail = <<<EOE
Subject: {$subject}
MIME-Version: 1.0
Content-type: multipart/alternative; boundary="{$boundary}"{$returnPath}
To: {$to}
Cc: {$cc}
Bcc: {$bcc}
From: {$from}

--{$boundary}


EOE;
		//if we have some html set, lets create a new part and add it
		if ($html)
		{
			$rawEmail .= <<<EOE
--{$boundary}
Content-Type: text/html; charset=iso-8859-1

{$html}


EOE;
		}

		//loop through our attachments
		foreach ($attachments as $attachment)
		{
			//ensure we can access the file
			if (file_exists($attachment))
			{
				//get all the meta information we need
				$contentType = $this->mimeContentType($attachment);
				$size = filesize($attachment);
				$attachmentName = basename($attachment);
				//base64 encode our attachment content
				$attachmentContent = base64_encode(file_get_contents($attachment));
				
				$rawEmail .= <<<EOE
--{$boundary}
Content-Type: {$contentType}; name="{$attachmentName}"
Content-Description: "{$attachmentName}"
Content-Disposition: attachment; filename="{$attachmentName}"; size={$size};
Content-Transfer-Encoding: base64

{$attachmentContent}

EOE;
			}
		}

		//finish off our email with the boundary
		$rawEmail .= <<<EOE
--{$boundary}--
EOE;

		//set up the arguments to pass to the client. You can set 'Source' in
		//here, but I encountered errors. So found setting it in the headers worked
		//best.
		
		try
		{
			$response = $client->sendRawEmail(array(
                'RawMessage' => array('Data' => base64_encode($rawEmail))), array('Source' => $from));
                      
            return $response->get('MessageId');
		}
		catch (MessageRejectedException $mrEx)
		{
			$this->log('Unable to send email: Rejected. ' . $mrEx->getMessage());
		}
		catch (\Exception $ex)
		{
			$this->log('Unable to send email: Unknown. ' . $ex->getMessage());
		}

		return false;  
    }*/
    function sendAttchmentMailViaAmazonsdk($to, $subject, $from, $html,$attachments,$cc='',$bcc='',$reply_to='', $from_name = 'Muvi'){
		require 'sendgrid-php/vendor/autoload.php';
        $sendgrid = new SendGrid(SENDGRID_APIKEY);
        $email    = new SendGrid\Email();
        try{
         $email->addTo($to)
             ->addCc($cc)
             ->addBcc($bcc)
             ->setReplyTo($reply_to)
             ->setFromName(@$from_name)
             ->setFrom($from)
             ->setSubject($subject)
             ->setHtml($html);
		 if($attachments){
			$email->setAttachments(@$attachments);
		 } 
         $result=$sendgrid->send($email);
           return $result;  
         } catch (Exception $e) {
             $fp = fopen(__DIR__ . '/../../protected/runtime/email.log', 'a+');
             fwrite($fp, '\n\t Exception occured on dt:-' . date('m-d-Y H:i:s') . "\n\t " . $e->getMessage());
             fclose($fp);
             return false;
         }        
    }
    function mimeContentType($file)
	{
		if (file_exists($file) && $extension = pathinfo($file, PATHINFO_EXTENSION))
		{
			//taken from https://github.com/laravel/laravel/blob/3.0/application/config/mimes.php
			$mimes = array(
				'hqx'   => 'application/mac-binhex40',
				'cpt'   => 'application/mac-compactpro',
				'csv'   => array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream'),
				'bin'   => 'application/macbinary',
				'dms'   => 'application/octet-stream',
				'lha'   => 'application/octet-stream',
				'lzh'   => 'application/octet-stream',
				'exe'   => array('application/octet-stream', 'application/x-msdownload'),
				'class' => 'application/octet-stream',
				'psd'   => 'application/x-photoshop',
				'so'    => 'application/octet-stream',
				'sea'   => 'application/octet-stream',
				'dll'   => 'application/octet-stream',
				'oda'   => 'application/oda',
				'pdf'   => array('application/pdf', 'application/x-download'),
				'ai'    => 'application/postscript',
				'eps'   => 'application/postscript',
				'ps'    => 'application/postscript',
				'smi'   => 'application/smil',
				'smil'  => 'application/smil',
				'mif'   => 'application/vnd.mif',
				'xls'   => array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'),
				'ppt'   => array('application/powerpoint', 'application/vnd.ms-powerpoint'),
				'wbxml' => 'application/wbxml',
				'wmlc'  => 'application/wmlc',
				'dcr'   => 'application/x-director',
				'dir'   => 'application/x-director',
				'dxr'   => 'application/x-director',
				'dvi'   => 'application/x-dvi',
				'gtar'  => 'application/x-gtar',
				'gz'    => 'application/x-gzip',
				'php'   => array('application/x-httpd-php', 'text/x-php'),
				'php4'  => 'application/x-httpd-php',
				'php3'  => 'application/x-httpd-php',
				'phtml' => 'application/x-httpd-php',
				'phps'  => 'application/x-httpd-php-source',
				'js'    => 'application/x-javascript',
				'swf'   => 'application/x-shockwave-flash',
				'sit'   => 'application/x-stuffit',
				'tar'   => 'application/x-tar',
				'tgz'   => array('application/x-tar', 'application/x-gzip-compressed'),
				'xhtml' => 'application/xhtml+xml',
				'xht'   => 'application/xhtml+xml',
				'zip'   => array('application/x-zip', 'application/zip', 'application/x-zip-compressed'),
				'mid'   => 'audio/midi',
				'midi'  => 'audio/midi',
				'mpga'  => 'audio/mpeg',
				'mp2'   => 'audio/mpeg',
				'mp3'   => array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
				'aif'   => 'audio/x-aiff',
				'aiff'  => 'audio/x-aiff',
				'aifc'  => 'audio/x-aiff',
				'ram'   => 'audio/x-pn-realaudio',
				'rm'    => 'audio/x-pn-realaudio',
				'rpm'   => 'audio/x-pn-realaudio-plugin',
				'ra'    => 'audio/x-realaudio',
				'rv'    => 'video/vnd.rn-realvideo',
				'wav'   => 'audio/x-wav',
				'bmp'   => 'image/bmp',
				'gif'   => 'image/gif',
				'jpeg'  => array('image/jpeg', 'image/pjpeg'),
				'jpg'   => array('image/jpeg', 'image/pjpeg'),
				'jpe'   => array('image/jpeg', 'image/pjpeg'),
				'png'   => 'image/png',
				'tiff'  => 'image/tiff',
				'tif'   => 'image/tiff',
				'css'   => 'text/css',
				'html'  => 'text/html',
				'htm'   => 'text/html',
				'shtml' => 'text/html',
				'txt'   => 'text/plain',
				'text'  => 'text/plain',
				'log'   => array('text/plain', 'text/x-log'),
				'rtx'   => 'text/richtext',
				'rtf'   => 'text/rtf',
				'xml'   => 'text/xml',
				'xsl'   => 'text/xml',
				'mpeg'  => 'video/mpeg',
				'mpg'   => 'video/mpeg',
				'mpe'   => 'video/mpeg',
				'qt'    => 'video/quicktime',
				'mov'   => 'video/quicktime',
				'avi'   => 'video/x-msvideo',
				'movie' => 'video/x-sgi-movie',
				'doc'   => 'application/msword',
				'docx'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'xlsx'  => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'word'  => array('application/msword', 'application/octet-stream'),
				'xl'    => 'application/excel',
				'eml'   => 'message/rfc822',
				'json'  => array('application/json', 'text/json'),
			);

			if (array_key_exists($extension, $mimes))
			{
				return (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
			}
		}
        }
        
    function log($message)
	{
		echo '<p class="error">' . strip_tags($message) . '</p>';
	}
    
    public function raiseInvoicePaidMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $params = array(
            'name'=> ucfirst(@$req['name']),
           'customer'=> ucfirst(@$req['companyname']),
            'invoice_title'=> $req['invoice_title'],
            'invoice_amount'=> $req['invoice_amount']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('invoice_paid');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');

        $adminSubject = "Invoice paid!";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/invoice_paid_sales',array('params'=>$params),true);
        
        //$template_name = 'invoice_paid_sales';
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }

    public function raiseInvoicePaidMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();

        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array( $req['email']);
        $adminSubject = "Payment successful";

        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
            //$attachment_encoded = base64_encode($attachment);
           
            $attached_file = "Please find the attached receipt.";
        } else {
            $attachment=array();
            $attached_file = "";
        }

        $params = array(
            'name' => ucfirst($req['name']),
            'email' => $req['email'],
           'invoice_title' => $req['invoice_title'],
           'invoice_amount' => $req['invoice_amount'],
            'attached_file'=> $attached_file
               
             );

        $params = array_merge($cmnparams, $params);

        $template_name = 'invoice_paid';
       
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$emails_from[0];
        
         if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/invoice_paid',array('params'=>$params),true);
        
        //$template_name = 'invoice_paid_sales';
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function errorMessageMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);

        $params = array(
            'name' => ucfirst(@$req['name']),
           'email'=> @$req['email'],
            'company'=> @$req['companyname'],
           'card_last_fourdigit'=> @$req['card_last_fourdigit'],
           'error_message'=> @$req['ErrorMessage']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('credit_card_failure');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');

        $adminSubject = "Credit card failure of " . $req['name'];
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$autosignup_from[0];
        $cc=array();
        //$bcc=array('mohan@muvi.com','anshuman@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/credit_card_failure',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc);
         
        return $returnVal;
    }

    public function mailToStudioForPaymentGatewayAPI($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $params = array(
            'company'=> (@$req['companyname']),
           'payment_gateway'=> @$req['payment_gateway'],
            'api_username' => @$req['api_username'],
           'api_password'=> @$req['api_password'],
            'api_signature'> @$req['api_signature'],
             'api_mode' => @$req['api_mode']
             );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('payment_gateway');
        $autosignup_from =  Yii::app()->common->MuviAdminEmailsNew('billing_from');
		
        $adminSubject = "Integration of '" . $req['payment_gateway'] . "' payment gateway";
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/payment_gateway_api',array('data'=>$params),true);
       
       $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }

    public function monthlyYearlyRenewMailToUser($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();
        $ip_address = CHttpRequest::getUserHostAddress();
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $cc = '';
        $bcc = '';
        if (strpos($_SERVER['HTTP_HOST'], 'muvi.com')) {
            $bcc = 'anshuman@muvi.com,sunil@muvi.com,sanjeev@muvi.com';
        }
        
        $plan = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth and Storage' : @$req['plan'].'ly';
        $adminSubject = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth  and Storage payment receipt' : ucwords($plan)." payment receipt";

        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
           
            
            $attached_file = "Please find the receipt attached here.";
        } else {
           $attachment=array();
            $attached_file = "";
        }

        $params = array(
             'name'=> ucfirst($req['name']),
           'email' => $req['email'],
            'plan'=> $plan,
            'package_name'=> $req['package_name'],
            'billing_period'=> $req['billing_period'],
            'attached_file'=> $attached_file
        );

        $params = array_merge($cmnparams, $params);
       
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$emails_from[0];
         
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/monthly_yearly_renew',array('params'=>$params),true);
        
        //$template_name = 'invoice_paid_sales';
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment,$cc,$bcc);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function monthlyYearlyRenewMailToSales($req = array()) {
        $plan = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'Bandwidth' : @$req['plan'].'ly';
        
        $cmnparams = $this->getCommonParamSes(1);
        $params = array(
            'name' => ucfirst(@$req['name']),
           'customer'=> ucfirst(@$req['companyname']),
            'plan'=> $plan,
            'billing_amount'=> '$' . @$req['billing_amount']
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('invoice_paid');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? "Bandwidth payment charged" : ucwords($plan)." payment charged";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from = $emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/monthly_yearly_renew_sales',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }
    
    public function monthlyYearlyManualRenewMailToSales($req = array(), $invoice_file) {
        $plan = (isset($req['isBandwidthOnly']) && intval($req['isBandwidthOnly'])) ? 'bandwidth' : ucfirst(@$req['plan']).'ly';
        
        $cmnparams = $this->getCommonParamSes(1);
        
        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
        } else {
           $attachment=array();
        }
        
        $params = array(
            'name' => ucfirst(@$req['name']),
            'customer'=> ucfirst(@$req['companyname']),
            'plan'=> $plan,
            'billing_amount'=> '$' . @$req['billing_amount'],
            'billing_period'=> $req['billing_period']
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('wire_transfer');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "Payment alert - Wire transfer";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/monthly_yearly_manual_renew_sales',array('params'=>$params),true);
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function cancelSubscriptionMailToSales($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');

        $cmnparams = $this->getCommonParamSes(1);
        $params = array(
            'name' => ucfirst(@$req['name']),
            'email' => @$req['email'],
            'phone' => @$req['phone'],
            'company'=> @$req['companyname'],
            'cancel_reason'=> @$req['cancel_reason'],
            'custom_notes'=> @$req['custom_notes'],
            'expiry_date'=> date('F d, Y', strtotime("+{$config[0]['value']} days"))
            );

        $params = array_merge($cmnparams, $params);

        if(@$req['cancel_reason'] == "Free Trial Expired"){
            $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('cancel_recipients_free_trial_expiry');
        }else{
            $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('cancel_recipients');
        }

        $adminSubject = "Subscription cancelled";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from='noreply@muvi.com';
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/cancel_subscription_sales',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',@$req['name']);
        
        return $returnVal;
    }

    public function cancelSubscriptionMailToUser($req = array(), $invoice_file) {
        $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');
        $cmnparams = $this->getCommonParamSes();

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        //$toEmail = array('sanjeev@muvi.com','manas@muvi.com',$req['email']);
         $toEmail = array($req['email']);
         $adminSubject = "Muvi subscription cancelled";

        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
            
            $attached_file = "Please find the invoice attached here.";
        } else {
            $attachment=array();
            $attached_file = "";
        }

        $params = array(
           'fname'=> ucfirst($req['name']),
           'email'=> $req['email'],
           'domain'=> $req['domain'],
           'onhold_period'=> $config[0]['value'],
           'attached_file' => $attached_file
        );

        $params = array_merge($cmnparams, $params);
        $to_email=$toEmail;
        $from_email=$autosignup_from[0];
        $template_name = 'cancel_subscription';
       
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/cancel_subscription',array('params'=>$params),true);
        
        //$template_name = 'invoice_paid_sales';
        
         if (is_array($toEmail)) {
            $to = rtrim(implode(',', $toEmail), ',');
        } else {
            $to = $toEmail;
        }
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $adminSubject, $from_email, $thtml,$attachment);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function freeTrialExpiringEmailToUser($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('monthly_charge'), 'value');
        $cmnparams = $this->getCommonParamSes();
        
        $params = 
            array(
            'fname' => ucfirst($req['name']),
            'email'=> $req['email'],
            'domain' => $req['domain'],
            'monthly_charge'=> $config[0]['value']
             );
        
        $params = array_merge($cmnparams,$params);
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('autosignup_from');
        $toEmail = array($req['email']);
        $adminSubject = "Your free trial is expiring in 2 days";
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        //$template_name = 'signup_sales_welcome';
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/free_trial_expire',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        return $returnVal;
    }

    public function freeTrialExpiredEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        $params = array(                        
            'fname' => ucfirst($req['name']),
            'email' => $req['email'],
            'domain' => $req['domain'],
            'expired_date' => $req['expired_date']
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('autosignup_from');

        $toEmail = array($req['email']);
        $adminSubject = "Your free trial has expired";
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/free_trial_expired',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        
        return $returnVal;
    }
    
    public function freeTrialExpiredEmailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);

        $params = array(
            'name' => ucfirst(@$req['name']),
            'email' => @$req['email'],
            'phone' => @$req['phone'],
            'company'=> @$req['companyname'],
            'cancel_reason'=> @$req['cancel_reason'],
            'custom_notes'=> @$req['custom_notes'],
            'expiry_date'=> date('F d, Y', strtotime("+{$config[0]['value']} days"))
            );      
        
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('free_trial_expired');
        $emails_from = $req['from_email'];        
        $adminSubject = "Free Trial expired";
        
        $subject=$adminSubject;
        $to= $adminGroupEmail; 
        $from=$emails_from; 
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/free_trial_expired_sales',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        
        return $returnVal;
    }    
    
    
/* billingCycleMailToSale() Method not in use,commented in cron tab, related to top billing*/
    public function billingCycleMailToSale($req) {
        $cmnparams = $this->getCommonParamSes(1);

        $params = array(
            'name'=>ucfirst(@$req['name']),
            'email'=> @$req['email'],
            'company'=> @$req['companyname'],
            'to_date'=> date('F d, Y')        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = array('support@muvi.com');
        $billing_from = $req['email'];

        $adminSubject = "Billing cycle started!";
        
        $template_name = 'billing_cycle';
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/billing_cycle',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }

    public function yearlyPlanRenewalReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        if ($req['days'] == 30) {
            $adminSubject = "Muvi yearly plan renewal";
            $cntnt_msg = "";
        } else if ($req['days'] == 14) {
            $adminSubject = "Muvi yearly plan renewal - second reminder";
            $cntnt_msg = "second and final";
        }
        
        $params = array(
            'cntnt_msg'=> $cntnt_msg,
            'fname' => ucfirst($req['name']),
            'package_name'=> $req['package_name'],
            'next_billing_date'=> $req['next_billing_date'],
            'total_amount_to_be_charged'=> $req['total_amount_to_be_charged']
        );

        $params = array_merge($cmnparams, $params);
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
       
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/yearly_plan_renewal',array('param'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }
    
    public function cardFailureMailToUser($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $adminSubject = "Payment failed!";

        $params = array(
            'name'=> ucfirst($req['name']),
            'email'=> $req['email'],
           'card_last_fourdigit'=> $req['card_last_fourdigit']
        );

        $params = array_merge($cmnparams, $params);
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/user_payment_failed',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }
    
    public function cardFailureMailToAdmin($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $params = array(
            'name'=> ucfirst(@$req['name']),
            'email'=> ucfirst(@$req['email']),
            'company'=> ucfirst(@$req['companyname']),
            'plan'=> @$req['plan'],
            'card_last_fourdigit'=> @$req['card_last_fourdigit'],
            'error_message'=> @$req['ErrorMessage']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('payment_failed');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');

        $adminSubject = ucfirst(@$req['name']) . " payment failed";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/admin_payment_failed',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }
    
    public function paymentFailedReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        $adminSubject = "Reminder - payment failed!";
        
        $params = array(
            'name'=> ucfirst($req['name']),
           'plan' => strtolower(@$req['plan'].'ly'),
           'card_last_fourdigit' => $req['card_last_fourdigit']
            );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);

        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/payment_failed_reminder',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }
    
    public function paymentFailedFinalReminderEmailToUser($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        $adminSubject = "Final Reminder - payment failed!";
        
        $params = array(
            'name'=> ucfirst($req['name']),
            'plan'=> strtolower(@$req['plan'].'ly'),
            'card_last_fourdigit'=> $req['card_last_fourdigit'],
            'cancelled_date'=> $req['cancelled_date']
        );

        $params = array_merge($cmnparams, $params);

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);

        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/payment_failed_final_reminder',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }

    public function subscriptionUpdatedMailToUser($req = array(), $invoice_file) {
       
        $cmnparams = $this->getCommonParamSes();

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
         
        $adminSubject = "Muvi subscription updated";

        if (trim($invoice_file)) {
            $attachments = array(ROOT_DIR . 'docs/' . $invoice_file);            
        } else {
            $attachments=array();    
        }

        $amount_charged = '';

        if (isset($req['amount_charged']) && trim($req['amount_charged'])) {
            $amount_charged = "Amount charged: <strong>$" . $req['amount_charged'] . "</strong>";
        }
        
       $params = array(
          'name'=> ucfirst($req['name']),
          'email'=> $req['email'],
          'package_name'=> ucwords(@$req['package_name']),
          'applications'=> ucwords(@$req['applications']),
          'plan'=> ucwords(@$req['plan'].'ly'),
          'amount_charged'=> $amount_charged );
       

        $params = array_merge($cmnparams, $params);
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
       
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/subscription_updated',array('params'=>$params),true);
        $returnVal=$this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachments);
        
        return $returnVal;
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }
    
    public function subscriptionUpdatedMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        
        $amount_charged = '';

        if (isset($req['amount_charged']) && trim($req['amount_charged'])) {
            $amount_charged = "Amount charged: $" . $req['amount_charged'];
        }
        
        $params = array(
           'name' => ucfirst(@$req['name']),
           'customer'=> ucfirst(@$req['companyname']),
           'package_name'=> ucwords(@$req['package_name']),
           'applications'=> ucwords(@$req['applications']),
           'plan'=> ucwords(@$req['plan'].'ly'),
           'amount_charged'=> $amount_charged
                );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('invoice_paid');
        
		$emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $subject="Subscription updated";
      
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/subscription_updated_sales',array('params'=>$params),true);
        
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from);
        
        return $returnVal;
    }
    
    public function RaiseNewticket_frommailtoadmin($req = array()){
           $cmnparams = $this->getCommonParamSes();
        $priority = 'medium';
        $params = array(
           'name'=> ucfirst(@$req['name']),
            'title' => (@$req['title']),
            'email'=> (@$req['email']),
            'description'=> (@$req['description']),
            'admin_url' => (@$req['admin_url']),
            'store_url'=> (@$req['store_url']),
            'priority'=> $priority,
            'customer_type' => (@$req['customer_type']),
            'app' => (@$req['app']),
            'highvalue' => (@$req['highvalue']),
            'subject'=> (@$req['subject']));
        $params = array_merge($cmnparams, $params);
       // print_r($params);exit;
        $adminGroupEmail = array('support@muvi.com');
        $test_email_subject="Muvitest ticket";
        if(strpos(@$req['ticket_desc'],$test_email_subject)!== false)
        {
            $adminGroupEmail=array('testmuvi@gmail.com');
        }           
        $emails_from = array($req['email']);
        $cc_to_sales = @$req['cc_to_sales'];

      
        $subject=@$req['subject'];
        $to=$adminGroupEmail;
        $from= 'support@muvi.com';

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/create_ticket_from_email_to_admin',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc_to_sales,'','','Muvi');
        return $returnVal;
    }
    
    public function RaiseNewticket_frommailtouser($req = array()){
            $cmnparams = $this->getCommonParamSes();
        $priority = 'medium';
        $params = array(
           'name'=> ucfirst(@$req['name']),
            'title' => (@$req['title']),
            'email'=> (@$req['email']),
            'description'=> (@$req['description']),
            'admin_url' => (@$req['admin_url']),
            'store_url'=> (@$req['store_url']),
            'priority'=> $priority,
            'subject'=> (@$req['subject']));
        $params = array_merge($cmnparams, $params);
       // print_r($params);exit;
        $adminGroupEmail = @$req['email'];
        $test_email_subject="Muvitest ticket";
         if(strpos(@$req['ticket_desc'],$test_email_subject)!== false)
         {
             $adminGroupEmail=array('testmuvi@gmail.com');
         }           

        $subject=@$req['subject'];
        $to=$adminGroupEmail;
        $from= 'support@muvi.com';
        $cc_email_list = @$req['cc_email_list'];
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/create_ticket_from_email_to_user',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc_email_list,'','','Muvi');
        return $returnVal;
    }
    
    public function addnote_frommailtoadmin($req = array()){
      $cmnparams = $this->getCommonParamSes();
        $priority = 'medium';
        $params = array(
           'name'=> ucfirst(@$req['name']),
            'email'=> (@$req['email']),
            'description'=> (@$req['description']),
            'url1' => (@$req['url1']),
            'url2'=> (@$req['url2']),
            'priority'=> $priority,
            'subject'=> (@$req['subject']));
        $params = array_merge($cmnparams, $params);
       // print_r($params);exit;
        $adminGroupEmail = array('support@muvi.com');
           $test_email_subject="Muvitest ticket";
                 if(strpos(@$req['ticket_desc'],$test_email_subject)!== false)
                 {
                     $adminGroupEmail=array('testmuvi@gmail.com');
                 }           
        $emails_from = array($req['email']);
        $cc = @$req['cc'];

        $subject=@$req['subject'];
        $to=$adminGroupEmail;
        $from= 'support@muvi.com';

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/addnote_mail_admin',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,'','','Muvi');
        
        return $returnVal;
    }
    
    public function addnote_frommailtouser($req = array()){
       $cmnparams = $this->getCommonParamSes();
        $priority = 'medium';
        $params =array('name' => ucfirst(@$req['name']),
            'email'=> (@$req['email']),
            'description'=> (@$req['description']),
            'url1' => (@$req['url1']),
            'url2'=> (@$req['url2']),
            'priority'=> $priority,
            'subject'=> (@$req['subject']));
        
        
        $params = array_merge($cmnparams, $params);
      
        //print_r($params);exit;
        $adminGroupEmail = array($req['email']);
      
        $template_name = 'addnote_mail_user';
        
        $subject=$req['subject'];
        $to=$adminGroupEmail;
        $from='support@muvi.com';
        //$from='bootstrap';
       Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/addnote_mail_user',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        
        return $returnVal;
    }
    
    
        public function UpdateNote_admin($req = array()){
        //echo 1111111111111;exit;
       $cmnparams = $this->getCommonParamSes();
       $priority = 'medium';
       $params = array(
           'name'=> ucfirst(@$req['name']),
           'description' => @$req['description'],
          'url1' => (@$req['url1']),
           'url2' => (@$req['url2']),
            'priority' => $priority,
           'subject'=> (@$req['subject'])
        );
        //print_r($params);exit;
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail =array('ticket@muvi.com');
                    
        // Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/UpdateticketNote_admin',array('data'=>$params),true);
        $emails_from = $req['email'];

        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= 'testmuvi12@gmail.com';

       $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc);
        
        return $returnVal;
    }
    
    public function videoEncoding($mailcontent = '') {
        $cmnparams = $this->getCommonParamSes();
        $host_ip = Yii::app()->params['host_ip'];
     
        if(in_array(HOST_IP,$host_ip)){
            $adminGroupEmail = array('dev-player@muvi.com');  
            $bcc= array('rasmi@muvi.com','infra@muvi.com','gayadhar@muvi.com','arya@muvi.com','suniln@muvi.com');
          $cc=array();
        } else{
            $adminGroupEmail = array('srutikant@muvi.com' );
          $cc=array();
          $bcc=array();
        }
        $adminSubject = "Video/Audio Encoding status";
        $params = array('mailcontent'=> $mailcontent);
            
        $params = array_merge($cmnparams, $params);
        
        $fromMail ='studio@muvi.com';
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $fromMail;

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/video_conversion',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc,'','Muvi');
        
        return $returnVal;
    }
    
    public function weeklyPaymentStatus($str) {
        $cmnparams = $this->getCommonParamSes();
        
        $params = array(
            'payment_data'=> $str);

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = array(
            'sunil@muvi.com',
            'rasmi@muvi.com' );
        
        $emails_from = 'studio@muvi.com';
        $adminSubject = "Weekly studio's payment status!";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $emails_from;

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/weekly_payment_status',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        
        return $returnVal;
    }
    public function customerByReseller($req = array(),$resellername,$studio) {
        //$StudioName = $studio['name'];
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = EMAIL_LOGO;
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p>A New customer from reseller "'.$resellername.'" has registered . Below are the details of the user</p>
                    <p style="display:block;margin:0 0 17px">
                        Name: <strong><span mc:edit="name">' . ucfirst(@$req['customer_name']) . '</span></strong><br/>
                        Email: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
           'website_name'=> $site_url,
           'logo'=> $logo,
            'mailcontent'=> $admincontent
                );
        $host_ip = Yii::app()->params['host_ip'];
        if(!in_array(HOST_IP,$host_ip)){
            $adminGroupEmail =array('manas@muvi.com','testmuvi12@gmail.com');
        }else{
            $adminGroupEmail = 'all@muvi.com';
        }        
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "Reseller purchased new customer!";
        $subject=$adminSubject;
        $to=$adminGroupEmail;

        $from= $autosignup_from[0];

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }
    public function SendApplicationMail($req){
        $applicationtype = $req['application_type'];
        $applicationtype = str_replace(1, "referral", $applicationtype);
        $applicationtype = str_replace(2, "reseller", $applicationtype);
        
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = EMAIL_LOGO;
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p><strong>'.$req['name'].'</strong> has submitted the following application</p>
                    <p style="display:block;margin:0 0 17px">
                        Application Type: <strong><span mc:edit="applicationtype">' . $applicationtype . '</span></strong><br/>
                        Company Name: <strong><span mc:edit="name">' . @$req['name'] . '</span></strong><br/>
                        Industry: <strong><span mc:edit="industry">' . @$req['industry'] . '</span></strong><br/>
                        Primary Contact: <strong><span mc:edit="email">' . @$req['company_name'] . '</span></strong><br/>
                        phone: <strong><span mc:edit="phone">' . @$req['phone'] . '</span></strong><br/>
                        Email: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                        No of Customers: <strong><span mc:edit="email">' . @$req['numberofcustomer'] . '</span></strong><br/>
                        Reason: <strong><span mc:edit="reason">' . @$req['reason'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            'website_name' => $site_url,
            'logo'=> $logo,
           'mailcontent'=> $admincontent
        );
        $host_ip = Yii::app()->params['host_ip'];
        if(!in_array(HOST_IP,$host_ip)){
            $adminGroupEmail =array('manas@muvi.com','testmuvi12@gmail.com');
                
        }else{
            $adminGroupEmail = array('studio@muvi.com','product@muvi.com');
        }
        $autosignup_from = 'studio@muvi.com';
        
        $adminSubject = "New reseller/referral application";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $autosignup_from;
        
        Yii::app()->theme = 'bootstrap';
        
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
       
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }
    public function SendMailtoApplier($req){
       
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = EMAIL_LOGO;
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                    <p>Dear <strong>'.$req['name'].'</strong>,</p>
                    <p style="display:block;margin:0 0 17px">
                        Thank you for your interest in Muvi\'s Partner Program.<br> 
                        A representative will review the same and get back to you in couple of days time.<br>
                        Should you have any questions, please feel free to email us on partners@muvi.com <br>
                        We will get back to you ASAP.
                    </p>
                    <p>&nbsp;</p>
                    <p>Regards,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            'website_name' => $site_url,
            'logo'=> $logo,
           'mailcontent'=> $admincontent
        );
        $adminGroupEmail = array($req['email']);
        
        $autosignup_from = 'partners@muvi.com';
        
        $adminSubject = "Your Muvi Partner Application";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $autosignup_from;

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        return $returnVal;
    }
    public function addReferrermail($req = array(),$resellername,$studio){
        $StudioName = $studio->name;
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = EMAIL_LOGO;
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left"><p>Hi,</p>
                    <p><strong>'.$resellername.'</strong> has added the following referral</p>
                    <p style="display:block;margin:0 0 17px">
                        Company Name: <strong><span mc:edit="name">' . ucfirst(@$req['company_name']) . '</span></strong><br/>
                        Primary Contact: <strong><span mc:edit="email">' . @$req['primary_contact'] . '</span></strong><br/>
                        Email Address: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                        Phone Number: <strong><span mc:edit="email">' . @$req['phone'] . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team Muvi</p></div>';
        $params = array(
            'website_name' => $site_url,
            'logo'=> $logo,
            'mailcontent'=> $admincontent,
             );
        $host_ip = Yii::app()->params['host_ip'];
        if(!in_array(HOST_IP,$host_ip)){
            $adminGroupEmail =array('manas@muvi.com','suraja@muvi.com','testmuvi12@gmail.com');
        }else{
            $adminGroupEmail =array('studio@muvi.com');
        }
        $autosignup_from = 'studio@muvi.com';
        
        $adminSubject = "New referral";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $autosignup_from;

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');
        return $returnVal;
    }    
    public function mailToResellerscustomer($req = array(),$studio=''){
        $site_url = Yii::app()->getBaseUrl(true);
        if($studio){
            if($studio['id']==1){//for sony
                $StudioName = 'Sony';
                $logo = 'https://www.muvi.com/images/sony_DADC_white.png';
            }else{
                $StudioName = $studio['name'];
                $logo = Yii::app()->getBaseUrl(true) . Yii::app()->common->getLogoFavPath($studio['id']);
            }
            $domain = $studio['domain'];
        }else{
            $StudioName = 'Muvi';
            $logo = EMAIL_LOGO;
            $domain = 'muvi.com';
        }
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="'.$StudioName.'" /></a>';
        $admincontent = '<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                    <p>Dear '.@$req['name'].',</p>
                    <p>Congratulations on your first step towards launching your own branded VOD platform!</p>
                    <p style="display:block;margin:0 0 17px">
                        Your VOD platform\'s Admin Panel:: <strong><span mc:edit="website">https://'.$domain.'</span></strong><br/>
                        Login: <strong><span mc:edit="email">' . @$req['email'] . '</span></strong><br/>
                        Password: <strong><span mc:edit="email">' . @$req['password'] . '</span></strong><br/>
                    </p>
                    <p>You can change your password after you login<br>
Contact us anytime you have any questions, we will reply ASAP.
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team '.$StudioName.'</p></div>';
        $params = array(
            'website_name'=> $site_url,
             'logo' => $logo,
           'mailcontent'=> $admincontent,
        );

        $adminGroupEmail = array($req['email'],'testmuvi12@gmail.com');
        $autosignup_from = 'studio@muvi.com';
        
        $adminSubject = "Getting started with $StudioName";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $autosignup_from;

        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        return $returnVal;
    }
    
    public function cancelSSLEmailToSystemAdmin($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $adminSubject = "Cancel SSL!";

        $params = array(
           'studio_name'=> ucfirst($req['studio_name']),
           'domain'=> $req['domain']
        );

        $params = array_merge($cmnparams, $params);
        
        $toEmail = Yii::app()->common->MuviAdminEmailsNew('cancel_ssl');
        $from_email='studio@muvi.com';
       
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/cancel_ssl',array('params'=>$params),true);
        
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($toEmail, $adminSubject, $from_email, $thtml);
       
        return $returnVal;
    }
    public function approveDevhourEmailtoCustomer($req)
    {
      $cmnparams = $this->getCommonParamSes();
        
        $params = array(
            'cname'=> $req['name'],
            'ticket_id'=>$req['ticket_id'],
            'approvedhours'=>$req['dev_hour'],         
            'remaining_hour'=>$req['remaining_devhour']
            );

        $params = array_merge($cmnparams, $params);
        
        $to = array($req['email']);
        
        $emails_from = 'support@muvi.com';
        $adminSubject = "You approved DevHours";
       
        $subject=$adminSubject;
        
        $from= $emails_from;
        $bcc= array('suraja@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/approve_devhours_customer',array('params'=>$params),true);
       
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'',$bcc,'','Muvi');
        
        return $returnVal;
    }
    
    public function approveDevhourEmailtoAdmin($req)
    {
      $cmnparams = $this->getCommonParamSes();
        
        $params = array(
            'cname'=> $req['name'],
            'ticket_id'=>$req['ticket_id'],
            'approvedhours'=>$req['dev_hour'],         
            'remaining_hour'=>$req['remaining_devhour']
            );

        $params = array_merge($cmnparams, $params);
        $host_ip = Yii::app()->params['host_ip'];
      
        if (!in_array(HOST_IP,$host_ip)) {
          $to = array('suraja@muvi.com','rasmi@muvi.com');  
        }else{
        $to = array('support@muvi.com');
        }
        $emails_from = 'support@muvi.com';
        $adminSubject = "DevHours approved for ticket ".$ticket_id;
       
        $subject=$adminSubject;
        
        $from= $emails_from;
        $bcc= array('suraja@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/approve_devhour_admin',array('params'=>$params),true);
       
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'',$bcc,'','Muvi');
        
        return $returnVal;  
    }
    
    public function purchaseDevhourEmailtoCustomer($req,$filename)
    {
      $cmnparams = $this->getCommonParamSes();
        if (trim($filename)) {
            $attachment = array(ROOT_DIR . 'docs/' . $filename);
            //$attachment_encoded = base64_encode($attachment);
           
            $attached_file = "Please find the attached receipt.";
        } else {
            $attachment=array();
            $attached_file = "";
        }

        $params = array(
            'cname'=>@ $req['name'],
           
            'dev_hour_purchased'=>@$req['dev_hour'],         
            'total_devhour'=>@$req['total_devhour'],
            'amount_paid'=>@$req['amount_paid'],
            'attached_file'=>@$filename
            );

        $params = array_merge($cmnparams, $params);
       
        $to = array($req['email']);
        
        $emails_from = 'support@muvi.com';
        $adminSubject = "DevHours purchased successfully";
       
        $subject=$adminSubject;
        
        $from= $emails_from;
        $bcc= array('suraja@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/purchase_devhour_customer',array('params'=>$params),true);
     
        //$returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'',$bcc,'','Muvi');
         $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment,'',$bcc,'');
     
        if (trim($filename)) {
            unlink(ROOT_DIR . 'docs/' . $filename);
        }
        return $returnVal;   
    }
    public function purchaseDevhourEmailtoAdmin($req)
    {
      $cmnparams = $this->getCommonParamSes();
        
        $params = array(
            'cname'=> $req['name'],
            'ticket_id'=>$req['ticket_id'],
            'dev_hour_purchased'=>$req['dev_hour'],         
            'total_devhour'=>$req['total_devhour'],
            'amount_paid'=>$req['amount_paid']
            );

        $params = array_merge($cmnparams, $params);
       $host_ip = Yii::app()->params['host_ip'];
      
	   $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('devhours_purchase');
	   $to = $adminGroupEmail;
	   
        $emails_from = 'support@muvi.com';
        $adminSubject = "Customer purchased DevHours!";
       
        $subject=$adminSubject;
        
        $from= $emails_from;
        $bcc= array('suraja@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/purchase_devhour_admin',array('params'=>$params),true);
       
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'',$bcc,'','Muvi');
        
        return $returnVal;   
    }
    /*
     * @author <ajit@muvi.com>
     * Desc Mail trigger for Physical goods order status
     * @param :
     *  $req['emailtype'] = 'orderconfirm';
     *  $req['emailtype'] = 'ordercancel';
     *  $req['emailtype'] = 'orderrefund';
     *  $req['emailtype'] = 'ordershipped';
     *  $req['emailtype'] = 'orderdelivered';
     *  $req['emailtype'] = 'orderreturned';
     *  $req['emailtype'] = 'orderreturnreceived';
     */
    public function pgEmailTriggers($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        //get ordernumber,buyer_id from pg_order table
        $pgorderval = Yii::app()->db->createCommand("SELECT order_number,buyer_id,shipping_cost,discount,discount_type,coupon_code,grand_total FROM pg_order WHERE id = ".$req['orderid']." AND studio_id=".$req['studio_id'])->queryAll();
        //get studio name
        $studio_name = Yii::app()->db->createCommand("SELECT name FROM studios WHERE id=".$req['studio_id'])->queryAll();
        //get name, email from sdk_users table
        $sdkusersval = Yii::app()->db->createCommand("SELECT email,display_name FROM sdk_users WHERE id = ".$pgorderval[0]['buyer_id']." AND studio_id=".$req['studio_id'])->queryAll();
        //get the items from pg_order_details
        if($req['emailtype'] == 'ordercancel'){
            $sql = "SELECT * FROM pg_order_details WHERE order_id = ".$req['orderid']." AND id=".$req['item_id'];            
        }
        else{
            $sql = "SELECT * FROM pg_order_details WHERE order_id = ".$req['orderid'];
        }
        
        $itemval = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($itemval AS $val)
        {
            $items[] = $val;
}
        //get the shipping details from pg_shipping_address
        $shipval = Yii::app()->db->createCommand("SELECT * FROM pg_shipping_address WHERE order_id = ".$req['orderid'])->queryAll(); 
        $country = Countries::model()->findByAttributes(array('code' => $shipval[0]['country']));
        $admin_email = User::model()->find(array('select'=>'email','condition' => 'studio_id=:studio_id and role_id=:role_id','params' => array(':studio_id' =>$req['studio_id'],':role_id'=>1)));        
        $first_name=$shipval[0]['first_name'];        
        $address=$shipval[0]['address'];
        $address2=$shipval[0]['address2'];
        $city=$shipval[0]['city'];
        $state=$shipval[0]['state'];
        $country = $country->country;
        $zip=$shipval[0]['zip'];
        $phone_number=$shipval[0]['phone_number'];
        $shipping_cost=$pgorderval[0]['shipping_cost'];
        $discount=($pgorderval[0]['discount']!='0.00')?$pgorderval[0]['discount']:0;
        if($pgorderval[0]['discount']!='0.00'){
            $discount_type = $pgorderval[0]['discount_type'];
        }
        $grand_total = $pgorderval[0]['grand_total'];
        $studio_name = $studio_name[0]['name'];
        $to=$sdkusersval[0]['email'];
        $toadmin = $admin_email['email'];
        //$toadmin = 'ajit@muvi.com';
        $from='studio@muvi.com';
        $cc=array();
        $admin_cc = Yii::app()->common->emailNotificationLinks($req['studio_id'],'end_user_action');
        if(!empty($admin_cc)){
            $cc = $admin_cc;
        }
        $bcc=array();
        if($req['emailtype'] == 'orderconfirm')
        {
            $subject = "Your order ".$pgorderval[0]['order_number']." is placed successfully";
            $adminsubject = "New Order : ".$pgorderval[0]['order_number'];
            $firstline = "Thank you for your order! We will update you when your order is being shipped. Order details are below :";
            $firstlineadmin = "An order has been placed on your site by ".ucfirst($sdkusersval[0]['display_name'])." Details are :";
        }        
        if($req['emailtype'] == 'ordercancel')
        {
            $subject = "Cancellation of ".$itemval[0]['name']." of order ".$pgorderval[0]['order_number']." is underway";
            $adminsubject = "Cancelled : ".$itemval[0]['name']." of order ".$pgorderval[0]['order_number'];
            $firstline = "Your ".$itemval[0]['name']." has been successfully cancelled. We are sorry to see you. Details are as below :";
            $firstlineadmin = "An order was cancelled on your site by ".ucfirst($sdkusersval[0]['display_name'])." Details are :";
        }       
        if($req['emailtype'] == 'orderrefund')
        {
            $subject = "Refund for ".$pgorderval[0]['order_number']." is complete";
            $adminsubject = "Refunded : ".$pgorderval[0]['order_number'];
            $firstline = "We have successfully processed the refund and credited $25000 in your account. Details are:";
            $firstlineadmin = "A refund request was successfully processed , your account is debited with $25000. Details are:";
        } 
        if($req['emailtype'] == 'ordershipped')
        {
            $subject = "Your ".$itemval[0]['name']." of order ".$pgorderval[0]['order_number']." is out for shipping";
            $adminsubject = "Shipped : ".$pgorderval[0]['order_number'];
            $firstline = "Your order has successfully shipped. The details are below :";
            $firstlineadmin = "An Order was successfully shipped. The details are below :";
        } 
        if($req['emailtype'] == 'orderdelivered')
        {
            $subject = "Your order ".$pgorderval[0]['order_number']."  has been delivered";
            $adminsubject = "Delivered : ".$pgorderval[0]['order_number'];
            $firstline = "Your order was successfully delivered.  We will be happy to see you again!";
            $firstlineadmin = "An Order was successfully delivered. The details are below :";
        } 
        if($req['emailtype'] == 'orderreturned')
        {
            $subject = "Return label for Order ".$pgorderval[0]['order_number'];
            $adminsubject = "Return Started : ".$pgorderval[0]['order_number'];
            $firstline = "Your return for ".$pgorderval[0]['order_number']." has been initiated . Please find the return label and order information below";
            $firstlineadmin =  "An return was initiated for ".$pgorderval[0]['order_number'].". A return label was mailed to the customer.";
        }         
        if($req['emailtype'] == 'orderreturnreceived')
        {
            //$studio_name = Yii::app()->user->studio_name;
            $subject = "Your return for ".$pgorderval[0]['order_number']." was received";
            $adminsubject = "Return Received : ".$pgorderval[0]['order_number'];
            $firstline = "Your order ".$pgorderval[0]['order_number']." was successfully received by the ".$req['studio_name']." Your account will be credited by ".$req['amount']." shortly";
            $firstlineadmin =  "Return for Order ".$pgorderval[0]['order_number'].". was successfully received. Details of the return are";
        }
        $site_url = Yii::app()->getBaseUrl(true);
        $adminlogo = '<a href="'.$site_url.'"><img src="'.EMAIL_LOGO.'" alt="Muvi" /></a>';
        $logo = Yii::app()->common->getLogoFavPath($req['studio_id']);
        $StudioName = $req['studio_name'];
        $studiologo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="'.$StudioName.'" /></a>';
        $params = array(
           'name' => ucfirst($sdkusersval[0]['display_name']),
           'product_id'=> $req['orderid'],
           'emailtype'=> $req['emailtype'],
           'ordernumber'=> $pgorderval[0]['order_number'],
           'adminlogo'=> $adminlogo,
           'studiologo'=>$studiologo,
           'items'=> $items,
            'first_name'=> $first_name,
            'address'=> $address,
            'address2'=> $address2,
            'city'=> $city,
            'state'=> $state,
            'country'=> $country,
            'zip'=> $zip,
            'phone_number'=> $phone_number,
            'studio_name'=> $studio_name,
            'shipping_cost'=> $shipping_cost,
            'discount'=> @$discount,
            'discount_type'=>@$discount_type,
            'grand_total'=>@$grand_total,
           'firstline'=> $firstline,
           'firstlineadmin'=> $firstlineadmin,
            'cds_shipping_method'=> $itemval[0]['cds_shipping_method'],
            'cds_tracking'=> $itemval[0]['cds_tracking'],
            'cds_consignment_no'=> $itemval[0]['cds_consignment_no'],
            'cds_despatch_no'=> $itemval[0]['cds_despatch_no'],
            'currency'=> $req['currency_id'],
			'personalization_id'=> @$itemval[0]['personalization_id'],
			'pg_varient_id'=> @$itemval[0]['pg_varient_id']
        );

        $params = array_merge($cmnparams, $params);
        Yii::app()->theme = 'bootstrap';
        //$thtml = Yii::app()->controller->renderPartial('//email/'.'pg_email_trigger_user',array('params'=>$params),true);
        //$returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$toadmin,$cc,$bcc,'',$studio_name);
        
        $thtmladmin = Yii::app()->controller->renderPartial('//email/'.'pg_email_trigger_admin',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtmladmin,$adminsubject,$toadmin,$from,$cc,$bcc,'','Muvi Studio');
                
        return $returnVal;
    }
    public function fbLoginRequest($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        
        $site_url = Yii::app()->getBaseUrl(true);
        $logo = EMAIL_LOGO;
        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
        $toadmin ='facebook@muvi.com';
        //$toadmin ='ajit@muvi.com';
        $toclient =$req['email'];
        //$toclient ='ajit@muvi.com';
        $from='studio@muvi.com';
        $cc=array();
        $bcc=array();

        $params = array(
            'studioname'=>$req['studioname'],
            'name'=>$req['name'],
            'logo' => $logo,            
            );
        $params = array_merge($cmnparams, $params);
        Yii::app()->theme = 'bootstrap';
        if($req['emailtype']=='request')
        {
            $subjectadmin ='Request for Social Login';
            $subjectstudio ='Social Login activation submitted';
            
            $thtmladmin = Yii::app()->controller->renderPartial('//email/'.'fb_login_request_admin',array('params'=>$params),true);
            $returnVal= $this->sendmailViaAmazonsdk($thtmladmin,$subjectadmin,$toadmin,$toclient,$cc,$bcc,'',$req['studioname']);

            $thtmlstudio = Yii::app()->controller->renderPartial('//email/'.'fb_login_request_studio',array('params'=>$params),true);
            $returnVal= $this->sendmailViaAmazonsdk($thtmlstudio,$subjectstudio,$toclient,$toadmin,$cc,$bcc,'','Muvi');
        }
        else if($req['emailtype']=='active')
        {
            $subject ='Social Login activated!'; 
            $thtml = Yii::app()->controller->renderPartial('//email/'.'fb_login_request_confirmation',array('params'=>$params),true);                        
            $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$toclient,$toadmin,$cc,$bcc,'','Muvi');                    
        }
 }
    public function getMuviKartEmails($req,$email_type) {
        if (isset($req['studio_id']) && isset($email_type)) {
            
            $std = new Studio();
            $studio = $std->findByPk($req['studio_id']);
            $StudioUrl = $site_url = HTTP . $studio->domain;            
            $siteLogo = $logo_path = Yii::app()->common->getLogoFavPath($req['studio_id']);
            $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="site_logo" /></a>';
            
            //get ordernumber,buyer_id from pg_order table
            $pgorderval = Yii::app()->db->createCommand("SELECT order_number,buyer_id,shipping_cost,discount,discount_type,coupon_code,grand_total,transactions_id FROM pg_order WHERE id = ".$req['orderid']." AND studio_id=".$req['studio_id'])->queryAll();
            //get studio name
            $studio_name = Yii::app()->db->createCommand("SELECT name FROM studios WHERE id=".$req['studio_id'])->queryAll();
            //get name, email from sdk_users table
            $sdkusersval = Yii::app()->db->createCommand("SELECT email,display_name FROM sdk_users WHERE id = ".$pgorderval[0]['buyer_id']." AND studio_id=".$req['studio_id'])->queryAll();
            //get the items from pg_order_details
            if($req['emailtype'] == 'ordercancel' || $req['emailtype'] == 'ordershipped'){
                $sql = "SELECT * FROM pg_order_details WHERE order_id = ".$req['orderid']." AND id=".$req['item_id'];            
}
            else{
                $sql = "SELECT * FROM pg_order_details WHERE order_id = ".$req['orderid'];
            }

            $itemval = Yii::app()->db->createCommand($sql)->queryAll();
            foreach($itemval AS $val)
            {
                $items[] = $val;
            }
            $Method = $items['cds_shipping_method'];
            $Tracking = $items['cds_tracking'];
            $Consignment = $items['cds_consignment_no'];
            $Despatch = $items['cds_despatch_no'];
            
            //get the shipping details from pg_shipping_address
            $shipval = Yii::app()->db->createCommand("SELECT * FROM pg_shipping_address WHERE order_id = ".$req['orderid'])->queryAll(); 
            $country = Countries::model()->findByAttributes(array('code' => $shipval[0]['country']));
            $admin_email = User::model()->find(array('select'=>'email','condition' => 'studio_id=:studio_id and role_id=:role_id','params' => array(':studio_id' =>$req['studio_id'],':role_id'=>1)));        
            //All data fatched
            //Now set the veriables
            $OrderNumber = $pgorderval[0]['order_number'];
            $ItemName = $itemval[0]['name'];
            $Name=$shipval[0]['first_name'];        
            $Address1=$shipval[0]['address'];
            $Address2=$shipval[0]['address2'];
            $City=$shipval[0]['city'];
            $State=$shipval[0]['state'];
            $Country = $country->country;
            $Zip=$shipval[0]['zip'];
            $PhoneNumber=$shipval[0]['phone_number'];
            $shipping_cost=Yii::app()->common->formatPrice($pgorderval[0]['shipping_cost'],$req['currency_id']);
            $discount=($pgorderval[0]['discount']!='0.00')?Yii::app()->common->formatPrice($pgorderval[0]['discount'],$req['currency_id']):0;
            $grand_total = Yii::app()->common->formatPrice($pgorderval[0]['grand_total'],$req['currency_id']);
            $StudioName = $studio_name[0]['name'];
            $to=$sdkusersval[0]['email'];
            $toadmin = $admin_email['email'];//from
            $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $req['studio_id']),array('select'=>'notification_from_email'));
            if($linked_emails->notification_from_email){
                $toadmin = $linked_emails->notification_from_email;
            }
            $cc=array();
            $bcc=array('manas@muvi.com');           
            
            //generate the dynamic content
            $DynamicItems = '
                <table width="100%" cellspacing="1" cellpadding="2" bgcolor="#CCCCCC">
                    <tr bgcolor="#17A6B2">
                        <th align="center">Image</th>
                        <th align="center">Product</th>
                        <th align="center">Quantity</th>
                        <th align="center">Unit Price</th>
                        <th align="center">Subtotal</th>                                        
                    </tr>';
                    foreach ($items as $item) {
                        $itemtot = 0;
						if($item['personalization_id']!=''){                    
							$img_path = PGProduct::getPersionalizeImage($item['personalization_id']);
						}else{
							if($item['pg_varient_id']!=''){
								$img_path = PGProduct::getpgImage($item['product_id'],'thumb',$item['pg_varient_id']);
							}else{
								$img_path = PGProduct::getpgImage($item['product_id'],'thumb');
							}
						}
                        //get the currency id
                        $pgproduct = PGProduct::model()->find('id=:id', array(':id' => $item['product_id']));
                        if($item['price']!='0.00'){
                          $itemtot = $item['price'] * $item['quantity'];
                          $total = $total + $itemtot;
                        }                         
                        $price = ($item['price']!='0.00')?Yii::app()->common->formatPrice($item['price'],$req['currency_id']):'Free';
                        $subtotal = ($item['price']!='0.00')?Yii::app()->common->formatPrice($itemtot,$req['currency_id']):'Free';
                        
                        $DynamicItems .= '<tr bgcolor="#FFFFFF">';
                        $DynamicItems .= '<td align="center"><img src="'.$img_path.'" alt="'.$item["name"].'" style="max-width:100px;"></td>';
                        $DynamicItems .= '<td align="center">'.$item['name'].'</td>';
                        $DynamicItems .= '<td align="center">'.$item['quantity'].'</td>';
                        $DynamicItems .= '<td align="center">'.$price.'</td>';
                        $DynamicItems .= '<td align="center">'.$subtotal.'</td>';
                        $DynamicItems .= '</tr>';
                    }
                    $total = Yii::app()->common->formatPrice($total,$req['currency_id']);
            if($req['emailtype']!='ordercancel'){        
                $DynamicItems .= '<tr bgcolor="#FFFFFF"><th colspan="4" align="right">Subtotal:</th><th align="center">'.$total.'</th></tr>';
                //discount
                if($discount){
                    $DynamicItems .= '<tr bgcolor="#FFFFFF"><th colspan="4" align="right">Discount:</th><th align="center">'.$discount.'</th></tr>';                
                }        
                $DynamicItems .= '<tr bgcolor="#FFFFFF"><th colspan="4" align="right">Shipping Cost:</th><th align="center">'.$shipping_cost.'</th>';
                $DynamicItems .= '<tr bgcolor="#FFFFFF"><th colspan="4" align="right">Total:</th><th align="center">'.$grand_total.'</th>';        
            }
            $DynamicItems .= '</table>';        
                    
            $temp = New NotificationTemplates;
            $data = $temp->findAllByAttributes(array('studio_id' => $req['studio_id'], 'type' => $email_type));
            if (count($data) > 0) {                
            } else {
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }

            $temps = $data[0];
            //Subject
            $subject = $temps['subject'];
            $subject = htmlentities($subject);
            eval("\$subject = \"$subject\";");
            $subject = htmlspecialchars_decode($subject);
            //Content
            $content = (string) $temps["content"];
            $breaks = array("<br />", "<br>", "<br/>");
            $content = str_ireplace($breaks, "\r\n", $content);
            $content = htmlentities($content);
            eval("\$content = \"$content\";");
            $content = htmlspecialchars_decode($content);
 
            $params = array(
                'website_name' => $StudioName,
                'site_link' => $site_link,
                'logo' => $logo,
                'mailcontent' => $content
            );
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/pg_email_trigger_customised', array('params' => $params), true);
            //echo $thtml;exit;
            $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$toadmin,$cc,$bcc,'',$StudioName); 
        }
    }   
    
    public function monthlyYearlyRenewMailToReseller($req = array(), $invoice_file) {
        $cmnparams = self::getCommonParamSes();
        $toEmail = array($req['email']);
        $cc = '';
        $bcc = '';
        
        $plan = @$req['plan'].'ly';
        $adminSubject = ucwords($plan)." payment receipt";
		$emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
           
            
            $attached_file = "Please find the receipt attached here.";
        } else {
           $attachment=array();
            $attached_file = "";
}

        $params = array(
            'name'=> ucfirst($req['name']),
            'email' => $req['email'],
            'plan'=> $plan,
            'level_name'=> $req['level_name'],
            'billing_period'=> $req['billing_period'],
            'attached_file'=> $attached_file
        );

        $params = array_merge($cmnparams, $params);
       
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$emails_from[0];
         
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_monthly_yearly_renew',array('params'=>$params),true);
        
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment,$cc,$bcc);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }

    public function monthlyYearlyRenewResellerMailToSales($req = array()) {
        $plan = @$req['plan'].'ly';
        
        $cmnparams = self::getCommonParamSes(1);
        $params = array(
            'name' => ucfirst(@$req['name']),
            'customer'=> ucfirst(@$req['companyname']),
            'plan'=> $plan,
            'billing_amount'=> '$' . @$req['billing_amount']
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('invoice_paid');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "Reseller: ".ucwords($plan)." payment charged";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_monthly_yearly_renew_sales',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',@$req['name']);
        
        return $returnVal;
    }
    
    public function monthlyYearlyManualRenewResellerMailToSales($req = array(), $invoice_file = '') {
        $plan = ucfirst(@$req['plan']).'ly';
        
        $cmnparams = $this->getCommonParamSes(1);
        
        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
        } else {
           $attachment=array();
        }
        
        $params = array(
            'name' => ucfirst(@$req['name']),
            'customer'=> ucfirst(@$req['companyname']),
            'plan'=> $plan,
            'billing_amount'=> '$' . @$req['billing_amount'],
            'billing_period'=> $req['billing_period']
        );
        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('wire_transfer');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "Reseller payment alert - Wire transfer";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_monthly_yearly_manual_renew_sales',array('params'=>$params),true);
        
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }
    
    public function cardFailureMailToReseller($req = array()) {
        $cmnparams = $this->getCommonParamSes();
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $adminSubject = "Payment failed!";

        $params = array(
            'name'=> ucfirst($req['name']),
            'email'=> $req['email'],
           'card_last_fourdigit'=> $req['card_last_fourdigit']
        );

        $params = array_merge($cmnparams, $params);
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from= $autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/user_payment_failed',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }
    
    public function cardFailureMailResellerToAdmin($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $params = array(
            'name'=> ucfirst(@$req['name']),
            'email'=> ucfirst(@$req['email']),
            'company'=> ucfirst(@$req['companyname']),
            'plan'=> @$req['plan'],
            'card_last_fourdigit'=> @$req['card_last_fourdigit'],
            'error_message'=> @$req['ErrorMessage']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('payment_failed');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');

        $adminSubject = "Reseller: ". ucfirst(@$req['name']) . " payment failed";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/admin_payment_failed',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
       
        return $returnVal;
    }
    //send email when cloudfonturl is failed during payment
    public function failedCloudfontUrl($mailcontent =array()){
        $cmnparams = $this->getCommonParamSes();       
        $adminGroupEmail = array('dev-player@muvi.com');        
        $adminSubject = "Failed to create Cloudfront URL";
        $params = array(
            'studio_id'=>@$mailcontent['studio_id'],
            'studio_name'=>@$mailcontent['studio_name'],                        
            'domain_name'=>@$mailcontent['domain_name']
             );
        $params = array_merge($cmnparams, $params);        
        $fromMail ='studio@muvi.com';        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $fromMail;
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/failed_cloudfront',array('params'=>$params),true);        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');        
        return true;
    }
    public function mailRemoveDevice($mailcontent =array()){
        $cmnparams = $this->getCommonParamSes();   
        $params = array(
            'studio_id'=>@$mailcontent['studio_id'],
            'email'=>@$mailcontent['email'],                        
            'device'=>@$mailcontent['device'],
			'device_info'=>@$mailcontent['device_info'] 
             );
		$studio_id=$params['studio_id'];
		$studio_data =User::model()->findBySql('SELECT email,first_name FROM user where `studio_id`=:studio_id AND role_id=1 ORDER BY id LIMIT 1',array(':studio_id'=>$studio_id));
        $studio_email = $studio_data['email'];
		$studio_name=$studio_data['first_name'];
        $params['studio_name']=@$studio_name;
        $params = array_merge($cmnparams, $params);        
        $subject = "Request to Remove Device";
        $to = $studio_email;  
        // get the admin email of the studio
        $from = $params['email'];  
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/remove_device',array('params'=>$params),true);   
        $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','','Muvi');        
        return true;
	}
         public function resellerSubscriptionPurchaseMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
             
        $params = array(
            'package_name'=> ucwords(@$req['package_name']),
            'plan'=> ucwords(@$req['plan']),
            'amount_charged'=> '$' . @$req['amount_charged'],
            'name'=> ucfirst(@$req['name']),
            'email'=> @$req['email'],
            'phone'=> @$req['phone'],
            'company'=> @$req['companyname'],
            'payment_method' => @$req['payment_method'],
             );
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('subscription_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "New paying reseller!";
        $subject=$adminSubject;
       
        $to=$adminGroupEmail;
        
        $from=$autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/resellerSubscription_sales',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from); 
       
        return $returnVal;
}
     public function subscriptionPurchaseMailToReseller($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $adminSubject = 'Reseller '.ucwords($req['package_name']) . " subscription activated!";

        if (trim($invoice_file)) {
            $attachments = array(ROOT_DIR . 'docs/' . $invoice_file);
            $attached_file = "Please find the invoice attached here.";
        } else {       
            $attached_file = "";
            $attachments=array();
        }
        $params = array(
            'fname'=> ucfirst($req['name']),
            'email'=> $req['email'],
            'domain'=> $req['domain'],
            'attached_file'=> $attached_file);
        

        $params = array_merge($cmnparams, $params);
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$autosignup_from[0];
       
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/resellerSubscription_activation',array('params'=>$params),true);
        $returnVal= $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachments);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }  
    public function resellererrorMessageMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        $params = array(
            'name' => ucfirst(@$req['name']),
           'email'=> @$req['email'],
            'company'=> @$req['companyname'],
           'card_last_fourdigit'=> @$req['card_last_fourdigit'],
           'error_message'=> @$req['ErrorMessage']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('credit_card_failure');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $adminSubject = "Credit card failure of " . $req['name'];
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$autosignup_from[0];
        $cc=array();
        //$bcc=array('mohan@muvi.com','anshuman@muvi.com');
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/credit_card_failure',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc);
         
        return $returnVal;
    }
     public function raiseInvoicePaidMailToReseller($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array( $req['email']);
        $adminSubject = "Payment successful";

        if (trim($invoice_file)) {
            $attachment = array(ROOT_DIR . 'docs/' . $invoice_file);
            //$attachment_encoded = base64_encode($attachment);
           
            $attached_file = "Please find the attached receipt.";
        } else {
            $attachment=array();
            $attached_file = "";
        }

        $params = array(
            'name' => ucfirst($req['name']),
            'email' => $req['email'],
           'invoice_title' => $req['invoice_title'],
           'invoice_amount' => $req['invoice_amount'],
            'attached_file'=> $attached_file
               
             );

        $params = array_merge($cmnparams, $params);

        $template_name = 'invoice_paid';
       
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$autosignup_from[0];
        
         if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_invoice_paid',array('params'=>$params),true);
        //$template_name = 'invoice_paid_sales';
        $returnVal=  $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachment);
       
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }  
     public function raiseResellerInvoicePaidMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes();

        $params = array(
            'name'=> ucfirst(@$req['name']),
           'customer'=> ucfirst(@$req['companyname']),
            'invoice_title'=> $req['invoice_title'],
            'invoice_amount'=> $req['invoice_amount']
        );

        $params = array_merge($cmnparams, $params);

        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('invoice_paid');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');

        $adminSubject = "Invoice paid!";
        
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_invoice_paid_sales',array('params'=>$params),true);
        //$template_name = 'invoice_paid_sales';
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    }   
    
    public function resellerCustomersubscriptionPurchaseMailToSales($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        $cloud_hosting ='';
        if (isset($req['is_cloud_hosting']) && intval($req['is_cloud_hosting'])) {
            $cloud_hosting = '<strong>Add Cloud hosting for your application (powered by Amazon Web Services)</strong><br/>';
        }
        
        $params = array(
            'package_name'=> ucwords(@$req['package_name']),
            'applications'=> ucwords(@$req['applications']),
            'plan'=> ucwords(@$req['plan']),
            'amount_charged'=> '$' . @$req['amount_charged'],
            'name'=> ucfirst(@$req['name']),
            'email'=> @$req['email'],
            'phone'=> @$req['phone'],
            'cloud_hosting' => $cloud_hosting,
            'company'=> @$req['companyname'],
            'customer_name' =>@$req['customer_name']
             );
        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('subscription_recipients');
        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        
        $adminSubject = "New paying customer!";
        $subject=$adminSubject;
       
        $to=$adminGroupEmail;
        
        $from=$autosignup_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_customer_subscription_sales',array('params'=>$params),true);
        
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from); 
       
        return $returnVal;
    }
    public function resellerCustomersubscriptionPurchaseMailToReseller($req = array(), $invoice_file) {
        $cmnparams = $this->getCommonParamSes();

        $autosignup_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $toEmail = array($req['email']);
        $adminSubject = ucwords($req['package_name']) . " subscription activated!";

        if (trim($invoice_file)) {
            $attachments = array(ROOT_DIR . 'docs/' . $invoice_file);
            $attached_file = "Please find the invoice attached here.";
        } else {       
            $attached_file = "";
            $attachments=array();
        }

        $params = array(
            'fname'=> ucfirst($req['name']),
            'email'=> $req['email'],
            'domain'=> $req['domain'],
            'attached_file'=> $attached_file);
        

        $params = array_merge($cmnparams, $params);

        $template_name = 'subscription_activation';
        
        $subject=$adminSubject;
        $to=$toEmail;
        $from=$autosignup_from[0];
       
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        } else {
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/reseller_customer_subscription_activation',array('params'=>$params),true);
        
        $returnVal= $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachments);
        if (trim($invoice_file)) {
            unlink(ROOT_DIR . 'docs/' . $invoice_file);
        }
        return $returnVal;
    }
     public function cancelcustomerSubscriptionByResellerMailToSales($req = array()) {
        $config = Yii::app()->common->getConfigValue(array('onhold_period'), 'value');
        $cmnparams = $this->getCommonParamSes(1);
        $params = array(
            'name' => ucfirst(@$req['name']),
            'email' => @$req['email'],
            'phone' => @$req['phone'],
            'customer_name' => @$req['customer_name'],
            'customer_email' => @$req['customer_email']
            );

        $params = array_merge($cmnparams, $params);
        $adminGroupEmail = Yii::app()->common->MuviAdminEmailsNew('delink_from_reseller');
        $emails_from = Yii::app()->common->MuviAdminEmailsNew('billing_from');
        $adminSubject = "Delink From Reseller";
       
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from=$emails_from[0];
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/cancel_resellercustomer_subscription_sales',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from);
        
        return $returnVal;
    } 
	public function reportAbouseChat($req = array()) {
        $cmnparams = $this->getCommonParamSes(1);
        $params = array_merge($cmnparams, $req);
        $from = $req['from'];
        $adminSubject = $req['sent_to']=='admin'?"Unwanted behavior reported":"Acknowledgment receipt";
        $subject=$adminSubject;
        $to=$req['to'];
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/report_abuse_chat',array('params'=>$params),true);
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',@$req['name']);
        return $returnVal;
    }  
    public function sendEmailForNewTranslationString($req){
        $cmnparams = $this->getCommonParamSes();
        $to = array($req['email']);
        $subject = $req['subject'];
        $attachments=array();
        $params = array(
            'fname'=> ucfirst($req['name']),
            'email'=> $req['email'],
            'msg' => $req['msg']
        );
        $params = array_merge($cmnparams, $params);
        $from="no-reply@muvi.com";
        if (is_array($to)) {
            $to = rtrim(implode(',', $to), ',');
        }else{
            $to = $to;
        }
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/new_translate_keys',array('params'=>$params),true);
        $returnVal= $this->sendAttchmentMailViaAmazonsdk($to, $subject, $from, $thtml,$attachments);
        return $returnVal;
    }
    
    //    DRM_Bandwith_Mail || modified : aravind@muvi.com || date :23-08-2017
     public function drmBandwidthMail($mailcontent = '') {
           if($mailcontent!=""){
                $cmnparams = $this->getCommonParamSes();
                $host_ip = Yii::app()->params['host_ip'];
                if(in_array(HOST_IP,$host_ip)){
                    $adminGroupEmail = array('dev-player@muvi.com');
                    $cc=array('pragyan@muvi.com','rasmi@muvi.com');
                    $bcc= array();
                } else{
                    $adminGroupEmail = array('srutikant@muvi.com','aravind@muvi.com');
                    $cc=array();
                    $bcc=array();
                }
                $adminSubject = "Wrong Bandwidth Log";
                $params = array('mailcontent'=> $mailcontent);
                $params = array_merge($cmnparams, $params);
                $fromMail ='studio@muvi.com';
                $subject=$adminSubject;
                $to=$adminGroupEmail;
                $from= $fromMail;
                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/video_conversion',array('params'=>$params),true);
                $_send_mail_txt = $thtml."\n\n".$subject."\n\n".$to."\n\n".$from."\n\n".$cc."\n\n".$bcc."\n\n".''."\n\n".'Muvi';
                $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc,'','Muvi');
            }
        return $returnVal;
    }
    public function encodingPendingMail($mailcontent =''){
        $cmnparams = $this->getCommonParamSes();
        $host_ip = Yii::app()->params['host_ip'];
        if(in_array(HOST_IP,$host_ip)){
            $adminGroupEmail = array('support@muvi.com','accounts@muvi.com');  
            $bcc= array('dev-player@muvi.com');
            $cc=array();
        } else{
            $adminGroupEmail = array('srutikant@muvi.com');
            $cc=array();
            $bcc=array();
        }
        $adminSubject = "Encoding Pending status";
        $params = array('mailcontent'=> $mailcontent);
        $params = array_merge($cmnparams, $params);
        $fromMail ='studio@muvi.com';
        $subject=$adminSubject;
        $to=$adminGroupEmail;
        $from= $fromMail;
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/encoding_pending',array('params'=>$params),true);
       // $_send_mail_txt = $thtml."\n\n".$subject."\n\n".$to."\n\n".$from."\n\n".$cc."\n\n".$bcc."\n\n".''."\n\n".'Muvi';
        $returnVal= $this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,$cc,$bcc,'','Muvi');
        return $returnVal;
    }
}
