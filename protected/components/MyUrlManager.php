<?php
class MyUrlManager extends CUrlManager {
    private function getRules() {
        $studio_id = Yii::app()->common->getStudiosId();
        /*
         * RK: Commented these since it affected the Facebook login
		$p = new CHtmlPurifier();
		$p->options = array('URI.AllowedSchemes'=>array('http' => true,'https' => true,));
		$_SERVER['REQUEST_URI'] = $p->purify($_SERVER['REQUEST_URI']);	
         * 
         */	
        // URL Routing 
		if(!(SUB_DOMAIN =='studio' || SUB_DOMAIN =='devstudio')){
			$routs = UrlRouting::model()->findAllByAttributes(array('studio_id' => $studio_id));
			foreach ($routs as $key => $value) {
				$rules[$value->permalink] = $value->mapped_url;
			}
		}else{
			$rules['rtmp-streaming'] = 'livestream/rtmpFeedStreaming';
			$rules['live-streaming'] = 'livestream';
		}
        $rules['cmscustomize'] = 'cmscustomize/index';
        $rules['embed/livestream/<embed_id>'] = 'embed/livestream/uniq_id/<embed_id>';
        $rules['apps/<apptype>/template'] = 'apps/template/app/<apptype>';
        $rules['embed/<embed_id>'] = 'embed/index/embed_id/<embed_id>';
        $rules['share/<embed_id>'] = 'embed/index/embed_id/<embed_id>/isShare/true';
        $rules['embedTrailer/<uniq_id>'] = 'embedTrailer/index/uniq_id/<uniq_id>';
        $rules['player/<permalink>'] = 'player/playMovie/contentPlink/<permalink>';
        $rules['player/<permalink>/stream/<stream_id>'] = 'player/playMovie/contentPlink/<permalink>/stream/<stream_id>';
        $rules['player/<permalink>/stream/<stream_id>/season/<season_id>'] = 'player/playMovie/contentPlink/<permalink>/stream/<stream_id>/season/<season_id>';
        $rules['player/<permalink>/season/<season_id>/stream/<stream_id>/'] = 'player/playMovie/contentPlink/<permalink>/season/<season_id>/stream/<stream_id>';
        $rules['player/<permalink>/season/<season_id>'] = 'player/playMovie/contentPlink/<permalink>/season/<season_id>';
        $rules['playout'] = 'Tvguide/index';
        $rules['tvguide'] = 'Tvguide/index';				
        $rules['TvGuidePlayer/<permalink>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>';
        $rules['TvGuidePlayer/<permalink>/stream/<stream_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/stream/<stream_id>';
        $rules['TvGuidePlayer/<permalink>/stream/<stream_id>/season/<season_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/stream/<stream_id>/season/<season_id>';
        $rules['TvGuidePlayer/<permalink>/season/<season_id>/stream/<stream_id>/'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/season/<season_id>/stream/<stream_id>';
        $rules['TvGuidePlayer/<permalink>/season/<season_id>'] = 'TvGuidePlayer/playMovie/contentPlink/<permalink>/season/<season_id>';
        $rules['playing/<permalink>'] = 'playing/playMovie/contentPlink/<permalink>';
        $rules['playing/<permalink>/stream/<stream_id>'] = 'playing/playMovie/contentPlink/<permalink>/stream/<stream_id>';
        $rules['playing/<permalink>/stream/<stream_id>/season/<season_id>'] = 'playing/playMovie/contentPlink/<permalink>/stream/<stream_id>/season/<season_id>';
        $rules['playing/<permalink>/season/<season_id>/stream/<stream_id>/'] = 'playing/playMovie/contentPlink/<permalink>/season/<season_id>/stream/<stream_id>';
        $rules['playing/<permalink>/season/<season_id>'] = 'playing/playMovie/contentPlink/<permalink>/season/<season_id>';
        //$rules['howitworks'] = 'page/howitworks';
       // $rules['technology'] = 'page/technology';
        $rules['star/<title>'] = 'star/show/item/<title>';
        $rules['contactus'] = 'site/contactus';
        //Added to make available a home url for studio
        $has_home_url = StudioConfig::model()->getconfigvalue('has_home_url');
        if(@$has_home_url['config_value'] == 1){
            $rules['home'] = 'site/home';
        }        
        $rules["page/<title>"] = "page/show/permalink/<title>";
        $rules["page/<title>/mobile_view"] = "page/show/permalink/<title>/mobileview";
        $rules['sitemap.xml'] = 'sitemap/default/index';
        $rules['sitemap.html'] = 'sitemap/default/index/format/html';
        $rules['news-sitemap.xml'] = 'sitemap/default/newsSitemap';
        $rules['mrss_<unique_id>_<feed_id>.xml'] = 'sitemap/default/mrssFeed/mrss_id/<unique_id>/feed_id/<feed_id>';
        $rules['video-streaming-platform'] = 'site/video_streaming_platform';
        $rules['contactus'] = 'site/contactus';
        $rules['contactus/mobile_view'] = 'site/contactus/mobileview';
        $rules['byod'] = 'byod/index';
        $rules['partners'] = 'partners/index';        
        $rules['application'] = 'page/application';
        $rules['insertapplication'] = 'page/insertapplication';
        $rules['muvikart'] = 'shop/index';
        $rules['shop'] = 'Shop/index';
        //LInks for Blog by RKS
        $rules['blog'] = 'blog/index';
        $rules['customize'] = 'customize/index';
        $rules['cmscustomizeprint'] = 'cmscustomizeprint/index';
        $rules['blog/mobile_view'] = 'blog/index/mobileview';
        $rules['blog/saveUserComment'] = 'blog/saveUserComment';
        $rules['blog/savecomment'] = 'blog/savecomment';
        $rules['blog/LoadCommentForm'] = 'blog/LoadCommentForm';
        $rules['blog/<permalink>'] = 'blog/view';
        $rules['blog/<permalink>/mobile_view'] = 'blog/view/mobileview';
        $rules['faqs'] = 'faqs/index';
        $rules['testsetup'] = 'signup/index';
        $rules['faqs/mobile_view'] = 'faqs/index/mobileview';
        $rules['<controller:\w+>/<id:\d+>'] = '<controller>/view';
        $rules['<controller:\w+>/<action:\w+>/<id:\d+>'] = '<controller>/<action>';
        $rules['<controller:\w+>/<action:\w+>'] = '<controller>/<action>';
        $rules['robots.txt'] = 'site/robotsTxt';
        $rules['loaderio-37096f625715b1799d3360cc55c513f5.txt'] = 'site/loadTestMuvi';
		//echo "<pre>";print_r($rules);exit;
        if ((SUB_DOMAIN == 'studio' || SUB_DOMAIN == 'devstudio') && (HOST_IP != '52.77.47.122') && (HOST_IP != '52.211.41.107') && (HOST_IP != '34.233.242.44') && (HOST_IP != '52.70.64.85') && (HOST_IP != '52.65.244.101') && (HOST_IP != '52.213.72.59')) {
            $rules['/'] = Yii::app()->baseUrl . '/wpstudio';
		}
        return @$rules;
    }

    protected function processRules() {
        $ip_address = Yii::app()->request->getUserHostAddress();
        //Setting dummy data during sign up    
        $current_path = $_SERVER['REQUEST_URI'];
        $path_info = pathinfo($current_path);
        //Added by RK to resolve the smap traffic to the sign up
        if ((isset(Yii::app()->user->id) && Yii::app()->user->id > 0) || $path_info['filename'] == 'helps' || (trim($path_info['dirname'],'/') =='rest') || (trim(strtolower($path_info['dirname']),'/') =='restv5')) {
            
        }else if(isset($_POST) && count($_POST) > 0) {
            
        } else if (isset($_GET) && count($_GET) > 0 && SUB_DOMAIN == 'studio' && !isset(Yii::app()->user->id)) {
            foreach ($_GET as $key => $value) {
                    switch (true) {
                        case strpos($path_info['basename'], 'contacts.html') > -1 || strpos($path_info['dirname'], 'contacts.html') > -1:
                            $pattern = '/name|company|company_name|hour|phone|ccheck|email|message|wysija|alert|script/';
                            $new_redirect_url= '/contacts.html';
                            break;
                        case strpos($path_info['basename'], 'reseller-referral-application.html') > -1 || strpos($path_info['dirname'], 'reseller-referral-application.html') > -1:
                            $pattern = '/name|company|company_name|hour|ccheck|phone|email|message|wysija|alert|script/';
                            $new_redirect_url= '/reseller-referral-application.html';
                            break;
                        case strpos($path_info['basename'], 'webinar.html') > -1 || strpos($path_info['dirname'], 'webinar.html') > -1:
                            $pattern = '/name|company|company_name|hour|ccheck|phone|email|message|wysija|alert|script/';
                            $new_redirect_url= '/webinar.html';
                            break;
                        case strpos($path_info['basename'], 'pricing.html') > -1 || strpos($path_info['dirname'], 'pricing.html') > -1:
                            $pattern = '/name|company|company_name|hour|phone|ccheck|email|message|wysija|alert|script/';
                             $new_redirect_url= '/pricing.html';
                            break;
                        case strpos($path_info['basename'], 'competition.html') > -1 || strpos($path_info['dirname'], 'competition.html') > -1:
                            $splittedstring=explode("?",$current_path);
                            $splittedstring1=explode("=",$splittedstring[1]);
                            if($splittedstring1[0]=='Features_Name'){
                                $pattern = 'spam';
                                $new_redirect_url= '/competition.html';
                            }else{
                                $pattern = '/Features_Name|wysija|alert|script/';
                                $new_redirect_url= '/competition.html';
                            }
                            break;
                        case strpos($path_info['basename'], 'career.html') > -1 || strpos($path_info['dirname'], 'career.html') > -1:
                            if(strpos($current_path, 'script') > -1 || strpos($current_path, 'SRC') > -1 || strpos($current_path, 'alert') > -1 || strpos($current_path, 'From') > -1 || strpos($current_path, 'style') > -1 || strpos($current_path, 'onload') > -1 || strpos($current_path, 'body') > -1 || strpos($current_path, 'boDY') > -1 || strpos($current_path, 'frame') > -1 || strpos($current_path, 'img') > -1 || strpos($current_path, 'textarea') > -1 || strpos($current_path, 'href') > -1 || strpos($current_path, 'audio') > -1 || strpos($current_path, 'bgsound') > -1 || strpos($current_path, 'onactivate') > -1 || strpos($current_path, 'onbeforeactivate') > -1 || strpos($current_path, 'onfocusin') > -1 || strpos($current_path, 'onscroll') > -1 || strpos($current_path, 'embed') > -1 || strpos($current_path, 'eventsource') > -1 || strpos($current_path, 'form') > -1 || strpos($current_path, 'alias') > -1 || strpos($current_path, 'websec') > -1 || strpos($current_path, 'onerror') > -1 || strpos($current_path, '%') > -1 || strpos($current_path, 'onmousemove') > -1){
                                $pattern = 'spam';
                                $new_redirect_url= '/career.html';
                            }else{
                                $pattern = '/wysija|alert|script/';
                                $new_redirect_url= '/career.html';
                            }
                            break;
                        /*case strpos($current_path, 'news') > -1 && strpos($current_path, 'star') > -1:
                                $pattern = 'spam';
                                $new_redirect_url= '';
                            break;
                        case strpos($current_path, 'movies') > -1 && strpos($current_path, 'news') > -1:
                                $pattern = 'spam';
                                $new_redirect_url= '';
                            break;
                         */
                        case strpos($current_path, 'reviews') > -1 && strpos($current_path, 'page') > -1:
                                $pattern = 'spam';
                                $new_redirect_url= '';
                            break;

                        case strpos($path_info['basename'], 'community') > -1 || strpos($path_info['dirname'], 'community') > -1:
                            $splittedstring=explode("?",$current_path);
                            $splittedstring1=explode("=",$splittedstring[1]);
                            $splittedstring2=explode("&",$splittedstring1[1]);
                            if($splittedstring1[0]=='wpfs' && $splittedstring2[0]!='signup'){
                                $pattern = 'spam';
                                $new_redirect_url= '/community';
                            }else{
                                $pattern = '/websec|alias|wysija|alert|script|wysija/';
                                $new_redirect_url= '/community';
                            }
                            break;
                        case strpos($path_info['basename'], 'wpforo') > -1 || strpos($path_info['dirname'], 'wpforo') > -1:
                            $splittedstring=explode("?",$current_path);
                            $splittedstring1=explode("=",$splittedstring[1]);
                            $splittedstring2=explode("&",$splittedstring1[1]);
                            if($splittedstring1[0]=='wpforo' && $splittedstring2[0]!='signup'){
                                    $pattern = 'spam';
                                    $new_redirect_url= '';
                            }
                            break;
                        case strpos($path_info['basename'], 'lipi') > -1 || strpos($path_info['dirname'], 'lipi') > -1:
                            $splittedstring=explode("?",$current_path);
                            $splittedstring1=explode("=",$splittedstring[1]);
                            if($splittedstring1[0]=='lipi'){
                                    $pattern = 'spam';
                                    $new_redirect_url= '';
                            }
                            break;
                        case strpos($path_info['basename'], 'help.html') > -1 || strpos($path_info['dirname'], 'help.html') > -1:
                            $pattern = '/aq|alias|wysija|alert|script/';
                            $new_redirect_url= '/help.html';
                            break;
                        case strpos($path_info['basename'], 'features.html') > -1 || strpos($path_info['dirname'], 'features.html') > -1:
                            $pattern = '/q|alias|wysija|alert|script/';
                            $new_redirect_url= '/features.html';
                            break;
                        case strpos($path_info['basename'], 'wiki-home.html') > -1 || strpos($path_info['dirname'], 'wiki-home.html') > -1:
                            $pattern = '/aq|alias|wysija|alert|script/';
                            $new_redirect_url= '/wiki-home.html';
                            break;
                         case strpos($path_info['basename'], 'get_all_movies') > -1 && strpos($path_info['basename'], 'celeb_id') > -1 && strpos($path_info['basename'], 'page') > -1:
                            $pattern = '/celeb_id|page|wysija|alert|script/';
                            $new_redirect_url= '';
                            break;
                        case strpos($path_info['basename'], 'signup') > -1 || strpos($path_info['dirname'], 'signup') > -1:
                            $pattern = '/name|company_name|hour|companyname|phone|email|subdomain|wysija|alert|script/';
                            $new_redirect_url= '/signup';
                            break;
                        default:
                            $pattern = '/wysija|alert|script/';
                            $new_redirect_url = '';
                    }
                    if($pattern=='spam'){
                        $redirect_url = Yii::app()->getBaseUrl(true).$new_redirect_url;
                        header("Location: $redirect_url");
                        exit();
                    }
                    $key_pattern = '/aq|q/';
                    $matchkey = preg_match($pattern, strtolower($key));
                    $matchval = preg_match($pattern, strtolower($value));
                    if ($matchkey) {
                        $redirect_url = Yii::app()->getBaseUrl(true).$new_redirect_url;
                        header("Location: $redirect_url");
                        exit();
                    }
                    if (preg_match($key_pattern, strtolower($key)) > 0) {
                        $redirect_url = Yii::app()->getBaseUrl(true).$new_redirect_url;
                        header("Location: $redirect_url");
                        exit();
                    }
                
            }
        }
        //}


        $this->rules = $this->getRules();
        return parent::processRules();
    }

}
