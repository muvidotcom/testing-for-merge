<?php

/**
 * Ugc component is a separated component for the user generated componenets
 * @author suraja<suraja@muvi.com>
 * @editor chinmay
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );

class ugc extends AppComponent {

    function getCustomPosterSize($custom_form_id) {
        $posters = CustomMetadataForm::model()->find(array('select' => 'poster_size', 'condition' => 'id=:id', 'params' => array(':id' => $custom_form_id)));
        $dmn = $posters['poster_size'];
        if ($dmn) {
            $expl = explode('x', strtolower($dmn));
            $poster['vwidth'] = @$expl[0];
            $poster['vheight'] = @$expl[1];
            $poster['hwidth'] = @$expl[0];
            $poster['hheight'] = @$expl[1];
        } else {
            $poster_sizes = Yii::app()->Controller->poster_sizes;
            $horizontal = $poster_sizes['horizontal'];
            $vertical = $poster_sizes['vertical'];
            $poster['vwidth'] = $vertical['width'];
            $poster['vheight'] = $vertical['height'];
            $poster['hwidth'] = $horizontal['width'];
            $poster['hheight'] = $horizontal['height'];
        }
        return $poster;
    }

    public function saveUgc($custom_metadata_form_id, $requestData) {
        $data = $requestData['movie'];
        $Films = new Film();
        $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $custom_metadata_form_id)));
        $data['content_types_id'] = 1;
        if (!empty($cmfid)) {
            $data['content_types_id'] = Yii::app()->general->getcontents_types_id($cmfid);
        }
        if (!isset($data['parent_content_type_id']) || empty($data['parent_content_type_id'])) {
            $data['parent_content_type'] = $cmfid['parent_content_type_id'];
        }
        if($custom_metadata_form_id){
            $data['custom_metadata_form_id'] = $custom_metadata_form_id;
        }
        $movie = $Films->addContent($data);
        $movie_id = $movie['id'];
        $studio_id = Yii::app()->user->studio_id;

        /* if(!empty($data['id'])){
          UrlRouting::model()->deleteAll('permalink=:permalink AND mapped_url=:mapped_url',array(':permalink'=>$urlRouts['permalink'], ':mapped_url'=>$urlRouts['mapped_url']));
          } */
        $urlRouts['permalink'] = $movie['permalink'];
        $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
        $urlRoutObj = new UrlRouting();
        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $studio->show_sample_data = 0;
        $studio->save();
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
        $MovieStreams->is_episode = 0;

        $MovieStreams->sdk_user_id = Yii::app()->user->id;
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->content_publish_date = NULL;
        $chkUserReview = StudioConfig::model()->getconfigvalue('user_review_content');
        $MovieStreams->review_flag = !empty($chkUserReview) && $chkUserReview['config_value']==1?1:0;
        $MovieStreams->save();
        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $movieTags->addTags($data);
        $this->processContentImage($data['content_types_id'], $movie_id);
   
        if (isset($_REQUEST['content_filter_type'])) {
            $contentFilterCls = new ContentFilter();
            $addContentFilter = $contentFilterCls->addContentFilter($data, $data['content_type_id']);
        }

        if (HOST_IP != '127.0.0.1' && $MovieStreams->review_flag==0) {
            $solrObj = new SolrFunctions();
            if ($data['genre']) {
                foreach ($data['genre'] as $key => $val) {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $movie_id;
                    $solrArr['stream_id'] = $MovieStreams->id;
                    $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                    $solrArr['is_episode'] = $MovieStreams->is_episode;
                    $solrArr['name'] = $movie['name'];
                    $solrArr['permalink'] = $movie['permalink'];
                    $solrArr['studio_id'] = Yii::app()->user->studio_id;
                    $solrArr['display_name'] = 'content';
                    $solrArr['content_permalink'] = $movie['permalink'];
                    $solrArr['genre_val'] = $val;
                    $solrArr['product_format'] = '';
                    $ret = $solrObj->addSolrData($solrArr);
                }
            } else {
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $movie_id;
                $solrArr['stream_id'] = $MovieStreams->id;
                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                $solrArr['is_episode'] = $MovieStreams->is_episode;
                $solrArr['name'] = $movie['name'];
                $solrArr['permalink'] = $movie['permalink'];
                $solrArr['studio_id'] = Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'content';
                $solrArr['content_permalink'] = $movie['permalink'];
                $solrArr['genre_val'] = '';
                $solrArr['product_format'] = '';
                $ret = $solrObj->addSolrData($solrArr);
            }
        }
        return array(
            'success'=>1,
            'movie_id' => $movie['id'],
            'stream_uniq_id' => $MovieStreams->embed_id,
            'movie_stream_id' => $MovieStreams->id,
            'message'=>'Content added successfully'
        );
    }

    function updateUgc($custom_metadata_form_id, $data) {
        $studio_id = Yii::app()->user->studio_id;
        $language_id = Yii::app()->Controller->language_id;
        $movieData = $data['movie'];

        $cmfid = CustomMetadataForm::model()->find(array('select' => 'parent_content_type_id,content_type,is_child', 'condition' => 'id=:id', 'params' => array(':id' => $custom_metadata_form_id)));
        if (!isset($movieData['content_types_id']) || empty($movieData['content_types_id'])) {
            @$movieData['content_types_id'] = Yii::app()->general->getcontents_types_id($cmfid);
        }
        if (!isset($movieData['parent_content_type_id']) || empty($movieData['parent_content_type_id'])) {
            @$movieData['parent_content_type'] = $cmfid['parent_content_type_id'];
        }

        if (!empty($data['movie_id'])) {
            $Films = Film::model()->findByAttributes(array('id' => $data['movie_id']));
            $MovieStreams = movieStreams::model()->findByAttributes(array('id' => $data['stream_id'], 'movie_id' => $data['movie_id'], 'is_episode' => 0));
            if ($Films) {
                //Update content publish date if any
                $publish_date = NULL;
                if (!empty($_REQUEST['content_publish_date'])) {
                    if (!empty($_REQUEST['publish_date'])) {
                        $pdate = explode('/', $_REQUEST['publish_date']);
                        $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                        if (!empty($_REQUEST['publish_time'])) {
                            $publish_date .= ' ' . $_REQUEST['publish_time'];
                        }
                    }
                }
                $film_lang = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id, 'parent_id' => $_REQUEST['movie_id']));
                $movieData['custom10'] = self::createSerializeCustomDate($movieData);
                $movie_id = $_REQUEST['movie_id'];
                if (!empty($film_lang)) {
                    $Films = Film::model()->findByAttributes(array('id' => $film_lang->id));
                }
                if ($Films->language_id == $language_id) {
                    if ($Films->parent_id == 0) {
                        $Films->name = stripslashes($movieData['name']);
                        if (isset($movieData['release_date']) && $movieData['release_date'] != '1970-01-01' && strlen(trim($movieData['release_date'])) > 6)
                            $Films->release_date = date('Y-m-d', strtotime($movieData['release_date']));
                        else
                            $Films->release_date = null;
                        $Films->content_category_value = implode(',', $movieData['content_category_value']);
                        if (!empty($movieData['content_subcategory_value']))
                            $Films->content_subcategory_value = implode(',', $movieData['content_subcategory_value']);
                        $movieData['language'] = is_array(@$movieData['language']) ? array_map('ucwords', $movieData['language']) : $movieData['language'];
                        $Films->language = is_array(@$movieData['language']) ? json_encode(array_values(array_unique($movieData['language']))) : @$movieData['language'];
                        $movieData['genre'] = is_array($movieData['genre']) ? array_map('ucwords', $movieData['genre']) : $movieData['genre'];
                        $Films->genre = is_array(@$movieData['genre']) ? json_encode(array_values(array_unique($movieData['genre'])), JSON_UNESCAPED_UNICODE) : @$movieData['genre'];
                        $movieData['censer_rating'] = is_array(@$movieData['censer_rating']) ? array_map('ucwords', $movieData['censer_rating']) : $movieData['censer_rating'];
                        $Films->censor_rating = is_array(@$movieData['censer_rating']) ? json_encode(array_values(array_unique($movieData['censer_rating']))) : @$movieData['censer_rating'];
                        $Films->story = stripslashes($movieData['story']);
                        $Films->custom1 = @$movieData['custom1'] ? (is_array(@$movieData['custom1']) ? json_encode(@$movieData['custom1']) : @$movieData['custom1']) : '';
                        $Films->custom2 = @$movieData['custom2'] ? (is_array(@$movieData['custom2']) ? json_encode(@$movieData['custom2']) : @$movieData['custom2']) : '';
                        $Films->custom3 = @$movieData['custom3'] ? (is_array(@$movieData['custom3']) ? json_encode(@$movieData['custom3']) : @$movieData['custom3']) : '';
                        $Films->custom4 = @$movieData['custom4'] ? (is_array(@$movieData['custom4']) ? json_encode(@$movieData['custom4']) : @$movieData['custom4']) : '';
                        $Films->custom5 = @$movieData['custom5'] ? (is_array(@$movieData['custom5']) ? json_encode(@$movieData['custom5']) : @$movieData['custom5']) : '';
                        $Films->custom6 = @$movieData['custom6'] ? (is_array(@$movieData['custom6']) ? json_encode(@$movieData['custom6']) : @$movieData['custom6']) : '';
                        $Films->custom7 = @$movieData['custom7'] ? (is_array(@$movieData['custom7']) ? json_encode(@$movieData['custom7']) : @$movieData['custom7']) : '';
                        $Films->custom8 = @$movieData['custom8'] ? (is_array(@$movieData['custom8']) ? json_encode(@$movieData['custom8']) : @$movieData['custom8']) : '';
                        $Films->custom9 = @$movieData['custom9'] ? (is_array(@$movieData['custom9']) ? json_encode(@$movieData['custom9']) : @$movieData['custom9']) : '';
                        $Films->custom10 = @$movieData['custom10'] ? (is_array(@$movieData['custom10']) ? json_encode(@$movieData['custom10']) : @$movieData['custom10']) : '';
                        $Films->save();
                        $filmChildData = new Film;
                        $attr = array('content_category_value' => implode(',', $movieData['content_category_value']));
                        $condition = "parent_id =:id";
                        $params = array(':id' => $movie_id);
                        $filmChildData = $filmChildData->updateAll($attr, $condition, $params);
                    }else {
                        $Films->name = stripslashes($movieData['name']);
                        $Films->language = json_encode($movieData['language']);
                        $Films->genre = json_encode(array_values(array_unique($movieData['genre'])));
                        $Films->censor_rating = json_encode($data['censer_rating']);
                        $Films->story = stripslashes($movieData['story']);
                        $Films->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                        $Films->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                        $Films->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                        $Films->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                        $Films->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                        $Films->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                        $Films->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                        $Films->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                        $Films->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                        $Films->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                        $Films->save();
                    }

                    if (HOST_IP != '127.0.0.1') {
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id . " AND content_id:" . $movie_id . " AND stream_id:" . $MovieStreams->id);
                        $solrArr = array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $MovieStreams->id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $data['name'];
                        $solrArr['permalink'] = $Films->permalink;
                        $solrArr['studio_id'] = Yii::app()->user->studio_id;
                        $solrArr['display_name'] = 'content';
                        $solrArr['content_permalink'] = $Films->permalink;
                        $solrArr['genre_val'] = (count($data['genre']) > 1) ? implode(',', $data['genre']) : $data['genre'][0];
                        $solrArr['product_format'] = '';
                        if ($publish_date != NULL)
                            $solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
                        $ret = $solrobjnew->addSolrData($solrArr);
                    }
                }else {
                    $Film = new Film;
                    $Film->name = stripslashes($data['name']);
                    $Film->content_category_value = $Films->content_category_value;
                    $Film->content_types_id = $Films->content_types_id;
                    $Film->uniq_id = $Films->uniq_id;
                    $Film->language = json_encode($data['language']);
                    $Film->genre = json_encode(array_values(array_unique($data['genre'])));
                    $Film->censor_rating = json_encode($data['censer_rating']);
                    $Film->story = stripslashes($data['story']);
                    $Film->studio_id = $studio_id;
                    $Film->parent_id = $Films->id;
                    $Film->search_parent_id = $Films->id;
                    $Film->permalink = $Films->permalink;
                    $Film->language_id = $language_id;
                    $Film->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                    $Film->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                    $Film->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                    $Film->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                    $Film->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                    $Film->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                    $Film->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                    $Film->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                    $Film->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                    $Film->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                    $Film->save();
                    if (HOST_IP != '127.0.0.1') {
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id . " AND content_id:" . $movie_id . " AND stream_id:" . $MovieStreams->id);
                        $solrArr = array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $MovieStreams->id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $data['name'];
                        $solrArr['permalink'] = $Films->permalink;
                        $solrArr['studio_id'] = Yii::app()->user->studio_id;
                        $solrArr['display_name'] = 'content';
                        $solrArr['content_permalink'] = $Films->permalink;
                        $solrArr['genre_val'] = (count($data['genre']) > 1) ? implode(',', $data['genre']) : $data['genre'][0];
                        $solrArr['product_format'] = '';
                        if ($publish_date != NULL)
                            $solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
                        $ret = $solrObjnew->addSolrData($solrArr);
                    }
                }

                $MovieStreams->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream']) ? 2 : ((@$_REQUEST['download']) ? 1 : 0);

                $MovieStreams->content_publish_date = $publish_date;
                $MovieStreams->save();

                //Check and Save new Tags into database
                $movieTags = new MovieTag();
                $movieTags->addTags($data);
                $movie_id = $Films->id;
                if (isset($_REQUEST['content_filter_type'])) {
                    $contentFilterCls = new ContentFilter();
                    $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $Films->content_type_id);
                }
                $this->processContentImage($data['content_types_id'], $movie_id, $_REQUEST['movie_stream_id']);
                

                if (isset($_FILES['topbanner']) && !($_FILES['topbanner']['error'])) {
                    Yii::app()->controller->uploadPoster($_FILES['topbanner'], $movie_id, 'topbanner');
                }

                $response['success'] = 1;
                $response['message'] = 'Wow! Your content updated successfully';
            } else {
                $response['error'] = 1;
                $response['message'] = 'Oops! Sorry error in updating your data';
            }
        } else {
            $response['error'] = 1;
            $response['message'] = 'Oops! Sorry you are not authorised to access this data';
        }
        return $response;
    }

    function createSerializeCustomDate($data, $flag = 9) {
        $cus = array();
        foreach ($data as $key => $value) {
            $k = (int) str_replace('custom', '', $key);
            if ($k > $flag) {
                $cus[] = array($key => $value);
            }
        }
        return $cus;
    }

    function processContentImage($content_types_id = 1, $movie_id = '', $stream_id = '', $is_episode = '') {
        $studio_id = Yii::app()->common->getStudiosId();
        if ($movie_id) {
            $cmsql = "SELECT poster_size FROM custom_metadata_form WHERE id = (SELECT custom_metadata_form_id FROM films WHERE id=" . $movie_id . ")";
            $cmfid = Yii::app()->db->createCommand($cmsql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
        }
        if (isset($cmfid) && !empty($cmfid) && $cmfid->poster_size != '') {
            $vertical = strtolower($cmfid->poster_size);
            $horizontal = strtolower($cmfid->poster_size);
        } else {
            $poster_sizes = Yii::app()->general->getPosterSize($studio_id);
            $vertical = strtolower($poster_sizes['v_poster_dimension']);
            $horizontal = strtolower($poster_sizes['h_poster_dimension']);
        }

        $poster_object_type = 'films';
        $object_id = $movie_id;
        if ($is_episode) {
            $poster_object_type = 'moviestream';
            $object_id = $stream_id;
        }
        $thumb_size = $vertical;
        $standard_size = $vertical;

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
        if ($content_types_id == 2 || $content_types_id == 4 || $is_episode) {
            $cropDimension['episode'] = $horizontal;
        }
         
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            /*$cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);*/

            $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
            $ret = Yii::app()->controller->uploadPoster($_FILES['Filedata'], $object_id, $poster_object_type, $cropDimension, $path);
            if ($movie_id && is_numeric($movie_id) && ($dir != $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/')) {
                Yii::app()->common->rrmdir($dir);
            }
            if ($content_types_id == 2 || $is_episode) {
                movieStreams::model()->updateByPk($stream_id, array('is_poster' => 1));
            }
            return $ret;
        } else {
            return false;
        }
    }

    function RemoveSpaces($url) {

        $url = preg_replace('/\s+/', '-', trim($url));
        $url = str_replace("         ", "-", $url);
        $url = str_replace("        ", "-", $url);
        $url = str_replace("       ", "-", $url);
        $url = str_replace("      ", "-", $url);
        $url = str_replace("     ", "-", $url);
        $url = str_replace("    ", "-", $url);
        $url = str_replace("   ", "-", $url);
        $url = str_replace("  ", "-", $url);
        $url = str_replace(" ", "-", $url);

        return $url;
    }

    function RemoveUrlSpaces($url) {

        $url = preg_replace('/\s+/', '%20', trim($url));
        $url = str_replace("         ", "%20", $url);
        $url = str_replace("        ", "%20", $url);
        $url = str_replace("       ", "%20", $url);
        $url = str_replace("      ", "%20", $url);
        $url = str_replace("     ", "%20", $url);
        $url = str_replace("    ", "%20", $url);
        $url = str_replace("   ", "%20", $url);
        $url = str_replace("  ", "%20", $url);
        $url = str_replace(" ", "%20", $url);

        return $url;
    }

    function downloadVideo($file, $newfilename = '', $mimetype = '', $isremotefile = false) {
        $formattedhpath = "";
        $filesize = "";

        if (empty($file)) {
            die('Please enter file url to download...!');
            exit;
        }
        //Removing spaces and replacing with %20 ascii code
        $file = self::RemoveUrlSpaces($file);
        if (preg_match("#http|https://#", $file)) {
            $formattedhpath = "url";
        } else {
            $formattedhpath = "filepath";
        }

        if ($formattedhpath == "url") {

            $file_headers = @get_headers($file);

            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                die('File is not readable or not found...!');
                exit;
            }
        } elseif ($formattedhpath == "filepath") {

            if (@is_readable($file)) {
                die('File is not readable or not found...!');
                exit;
            }
        }


        //Fetching File Size Located in Remote Server
        if ($isremotefile && $formattedhpath == "url") {


            $data = @get_headers($file, true);
            if (!empty($data['Content-Length'])) {
                $filesize = (int) $data["Content-Length"];
            } else {

                ///If get_headers fails then try to fetch filesize with curl
                $ch = @curl_init();

                if (!@curl_setopt($ch, CURLOPT_URL, $file)) {
                    @curl_close($ch);
                    @exit;
                }

                @curl_setopt($ch, CURLOPT_NOBODY, true);
                @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                @curl_setopt($ch, CURLOPT_HEADER, true);
                @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                @curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
                @curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                @curl_exec($ch);

                if (!@curl_errno($ch)) {

                    $http_status = (int) @curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    if ($http_status >= 200 && $http_status <= 300)
                        $filesize = (int) @curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                }
                @curl_close($ch);
            }
        }elseif ($isremotefile && $formattedhpath == "filepath") {

            die('Error : Need complete URL of remote file...!');
            exit;
        } else {

            if ($formattedhpath == "url") {

                $data = @get_headers($file, true);
                $filesize = (int) $data["Content-Length"];
            } elseif ($formattedhpath == "filepath") {

                $filesize = (int) @filesize($file);
            }
        }
        if (empty($newfilename)) {
            $newfilename = @basename($file);
        } else {
            //Replacing any spaces with (-) hypen
            $newfilename = self::RemoveSpaces($newfilename);
        }
        $path_parts = @pathinfo($file);
        $myfileextension = $path_parts["extension"];
        $myfileextension = substr($myfileextension, 0, strpos($myfileextension, "?"));
        $newfilename = $newfilename . '.' . $myfileextension;

        if (empty($mimetype)) {

            ///Get the extension of the file

            switch ($myfileextension) {

                ///Audio and Video Files

                case 'mp3':
                    $mimetype = "audio/mpeg";
                    break;
                case 'wav':
                    $mimetype = "audio/x-wav";
                    break;
                case 'au':
                    $mimetype = "audio/basic";
                    break;
                case 'snd':
                    $mimetype = "audio/basic";
                    break;
                case 'm3u':
                    $mimetype = "audio/x-mpegurl";
                    break;
                case 'ra':
                    $mimetype = "audio/x-pn-realaudio";
                    break;
                case 'mp2':
                    $mimetype = "video/mpeg";
                    break;
                case 'mov':
                    $mimetype = "video/quicktime";
                    break;
                case 'qt':
                    $mimetype = "video/quicktime";
                    break;
                case 'mp4':
                    $mimetype = "video/mp4";
                    break;
                case 'm4a':
                    $mimetype = "audio/mp4";
                    break;
                case 'mp4a':
                    $mimetype = "audio/mp4";
                    break;
                case 'm4p':
                    $mimetype = "audio/mp4";
                    break;
                case 'm3a':
                    $mimetype = "audio/mpeg";
                    break;
                case 'm2a':
                    $mimetype = "audio/mpeg";
                    break;
                case 'mp2a':
                    $mimetype = "audio/mpeg";
                    break;
                case 'mp2':
                    $mimetype = "audio/mpeg";
                    break;
                case 'mpga':
                    $mimetype = "audio/mpeg";
                    break;
                case '3gp':
                    $mimetype = "video/3gpp";
                    break;
                case '3g2':
                    $mimetype = "video/3gpp2";
                    break;
                case 'mp4v':
                    $mimetype = "video/mp4";
                    break;
                case 'mpg4':
                    $mimetype = "video/mp4";
                    break;
                case 'm2v':
                    $mimetype = "video/mpeg";
                    break;
                case 'm1v':
                    $mimetype = "video/mpeg";
                    break;
                case 'mpe':
                    $mimetype = "video/mpeg";
                    break;
                case 'avi':
                    $mimetype = "video/x-msvideo";
                    break;
                case 'midi':
                    $mimetype = "audio/midi";
                    break;
                case 'mid':
                    $mimetype = "audio/mid";
                    break;
                case 'amr':
                    $mimetype = "audio/amr";
                    break;


                default:
                    $mimetype = "application/octet-stream";
            }
        }


        //off output buffering to decrease Server usage
        @ob_end_clean();

        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }
        header('Content-Description: File Transfer');
        header('Content-Type: ' . $mimetype);
        header('Content-Disposition: attachment; filename=' . $newfilename . '');
        header('Content-Transfer-Encoding: binary');
        header("Expires: 0");
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Length: ' . $filesize);


        ///Will Download 1 MB in chunkwise
        $chunk = 1 * (1024 * 1024);
        $nfile = @fopen($file, "rb");
        while (!feof($nfile)) {

            print(@fread($nfile, $chunk));
            @ob_flush();
            @flush();
        }
        @fclose($filen);
    }

    function contentForm($film = null) {
        $custom_content_types_id = 1;
        $movieStream = array();
        if (!empty($film)) {
            $custom_content_types_id = $film->content_types_id;
            $movieStream = $film->movie_streams[0];
        }
        $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($custom_content_types_id);
        $arg['arg']['editid'] = ($list[0]['cmfid'] != 0) ? $list[0]['cmfid'] : $list[0]['custom_metadata_form_id'];
        $customData = array();
        if ($arg['arg']['editid']) {
            $list[0]['metadata_form_id'] = $arg['arg']['editid'];
            $customData = $customComp->getCustomMetadata($studio_id, $arg['parent_content_type_id'], $arg['arg']);
        }

        $payment_form = Yii::app()->controller->renderPartial('//content_form/content_form', array('customData' => $customData), true);
        return $payment_form;
    }
    
    function getUGCMoiveUrl($stream_id = null, $studio_id=null) { 
        /* only for single part video */
        if ($stream_id) {
            if(!$studio_id){
                $studio_id = Yii::app()->common->getStudioId();
            }
            $movieUrl = '';
            $connectionwiki = Yii::app()->db;
            $data = $connectionwiki->createCommand("SELECT full_movie,id,wiki_data FROM movie_streams WHERE id=" . $stream_id . " AND is_episode=0 AND full_movie!=''")->queryRow();
            if ($data) {
                $s3dir = $studio_id.'/EncodedVideo/uploads/movie_stream/full_movie/';
                if ($data["wiki_data"] != "") {
                    return CDN_HTTP . "muviassetsdev.s3.amazonaws.com/" . $s3dir . $data["wiki_data"] . "/" . $data["full_movie"];
                } elseif ($data["full_movie"] != "") {
                    if (defined('S3BUCKET_ID')) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } elseif (isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id) {
                        $bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
                    } else {
                        $bucketInfo = Yii::app()->common->getBucketInfo('', Yii::app()->common->getStudioId());
                    }
                    $movieUrl = CDN_HTTP . $bucketInfo['cloudfront_url'] . '/' . $s3dir . $data["id"] . "/" . urlencode($data["full_movie"]);
                    //$movieUrl = VIDEO_URL.'/'.$s3dir.$data["id"]."/".urlencode($data["full_movie"]);
                    return $movieUrl;
                } else {
                    return;
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }

}

?>
