<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
    private $_id;
    const ERROR_OTP_EXPIRED = 3;
    const ERROR_INCORRECT_OTP = 4;
    const ERROR_GENERATE_OTP = 5;
    const ERROR_SUBSCRIPTION_DEACTIVATED = 6;
    const ERROR_SUBSCRIPTION_NULL = 7;
    public function authenticate() {
        $enc = new bCrypt();
        //$record = User::model()->findByAttributes(array('email' => $this->username, 'is_active' => 1));
        //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
        //ajit@muvi.com
        //added order and limit 
        $record = Yii::app()->db->createCommand()
                ->select('u.*,s.name,s.ga_profile_id,s.new_cdn_users,s.domain,s.subdomain,s.status,s.theme,s.contact_us_email,s.lunch_dt,s.domain,s.s3bucket_id,s.studio_s3bucket_id,s.is_embed_white_labled,s.is_subscribed,s.payment_status')
                ->from('user u')
                ->join('studios s', 'u.studio_id = s.id')
                ->where('email=:email AND is_active=:is_active AND is_deleted=:is_deleted AND role_id!=4', array(':email' => $this->username, ':is_active' => 1, ':is_deleted' => 0))
                ->order('u.id desc')
                ->limit(1)
                ->queryRow();
        
        //echo "<pre>";print_r($record);exit;
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //else if($record->password!==crypt($this->password,$record->password))
        } else if (!$enc->verify($this->password, $record['encrypted_password'])) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        //ajit@muvi.com - 16/9/16
        //Issue id 5314, Except admin other users cannot login when subscription is expired or payment is failed
        else if(@$record['role_id']!=1 && @$record['status']==1 && @$record['is_subscribed']==1 && $record['payment_status'] != 0){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }else {
            /* $this->_id=$record->id;
              $this->setState('first_name', $record->first_name);
              $this->setState('email', $record->email);
              $this->setState('lastname', $record->last_name);
              $this->setState('company', $record->company);
              $this->setState('created_at', $record->created_at);
              $this->setState('is_sdk', $record->is_sdk); */
            
            //tracking the login history
            $login = new StudioLoginHistory();
            $login->studio_id = $record['studio_id'];
            $login->studio_user_id = $record['id'];
            $login->login_at = new CDbExpression("NOW()");
            $login->login_ip = Yii::app()->request->getUserHostAddress();
            $login->save();
            $login_id = $login->id;             

            $this->_id = $record['id'];
            $this->setState('signup_step', @$record['signup_step']);
            $this->setState('is_admin', @$record['is_admin']);
            $this->setState('first_name', @$record['first_name']);
            $this->setState('email', @$record['email']);
            $this->setState('lastname', @$record['last_name']);
            $this->setState('company', @$record['name']);
            $this->setState('phone_no', @$record['phone_no']);
            $this->setState('created_at', @$record['created_at']);
            $this->setState('is_sdk', @$record['is_sdk']);
            $this->setState('role_id', @$record['role_id']);
            $this->setState('ga_profile_id', @$record['ga_profile_id']);
            $this->setState('studio_subdomain', @$record['subdomain']);
            $this->setState('studio_id', @$record['studio_id']);
            $this->setState('status', @$record['status']);
            $this->setState('theme', @$record['theme']);
            $this->setState('lunch_date', @$record['lunch_dt'] ? $record['lunch_dt'] : date('Y-m-d', strtotime($record['created_at'])));
            $this->setState('siteName', @$record['name']);
            $this->setState('siteUrl', @$record['domain']);
            $this->setState('is_embed_white_labled', @$record['is_embed_white_labled']);
            $this->setState('adminEmail', @$record['contact_us_email']);
            $this->setState('s3bucket_id',@$record['s3bucket_id']);
            $this->setState('studio_s3bucket_id',@$record['studio_s3bucket_id']);
            $this->setState('new_cdn_users',@$record['new_cdn_users']);
            $this->setState('reseller_id',@$record['reseller_id']);
            $this->setState('login_id',@$login_id);
          
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    
    public function authenticatepartner() {
        $enc = new bCrypt();
        $studio_id = Yii::app()->common->getStudiosId();
        //$record = User::model()->findByAttributes(array('email' => $this->username, 'is_active' => 1));
        $record = Yii::app()->db->createCommand()
                ->select('u.*,s.name,s.ga_profile_id,s.new_cdn_users,s.domain,s.subdomain,s.status,s.theme,s.contact_us_email,s.lunch_dt,s.domain,s.s3bucket_id,s.studio_s3bucket_id,s.is_embed_white_labled, s.reseller_id')
                ->from('user u')
                ->join('studios s', 'u.studio_id = s.id')
                ->where('email=:email AND studio_id=:studio_id AND role_id=:role_id AND is_active=:is_active AND is_deleted=:is_deleted AND is_login_allowed=:is_login_allowed', array(':email' => $this->username, ':studio_id' => $studio_id, ':role_id' => 4, ':is_active' => 1, ':is_deleted' => 0,':is_login_allowed' => 1))
                ->queryRow();
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //else if($record->password!==crypt($this->password,$record->password))
        } else if (!$enc->verify($this->password, $record['encrypted_password'])) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            //tracking the login history
            $login = new StudioLoginHistory();
            $login->studio_id = $record['studio_id'];
            $login->studio_user_id = $record['id'];
            $login->login_at = new CDbExpression("NOW()");
            $login->login_ip = Yii::app()->request->getUserHostAddress();
            $login->save();
            $login_id = $login->id;             

            $this->_id = $record['id'];
            $this->setState('signup_step', @$record['signup_step']);
            $this->setState('is_admin', @$record['is_admin']);
            $this->setState('first_name', @$record['first_name']);
            $this->setState('email', @$record['email']);
            $this->setState('lastname', @$record['last_name']);
            $this->setState('company', @$record['name']);
            $this->setState('created_at', @$record['created_at']);
            $this->setState('is_sdk', @$record['is_sdk']);
            $this->setState('role_id', @$record['role_id']);
            $this->setState('ga_profile_id', @$record['ga_profile_id']);
            $this->setState('studio_subdomain', @$record['subdomain']);
            $this->setState('studio_id', @$record['studio_id']);
            $this->setState('status', @$record['status']);
            $this->setState('theme', @$record['theme']);
            $this->setState('lunch_date', @$record['lunch_dt'] ? $record['lunch_dt'] : date('Y-m-d', strtotime($record['created_at'])));
            $this->setState('siteName', @$record['name']);
            $this->setState('siteUrl', @$record['domain']);
			$this->setState('is_embed_white_labled', @$record['is_embed_white_labled']);
            $this->setState('adminEmail', @$record['contact_us_email']);
            $this->setState('s3bucket_id',@$record['s3bucket_id']);
            $this->setState('studio_s3bucket_id',@$record['studio_s3bucket_id']);
            $this->setState('new_cdn_users',@$record['new_cdn_users']);
            $this->setState('reseller_id',@$record['reseller_id']);
            $this->setState('login_id',@$login_id);
            $this->setState('is_partner', 1);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }    
    public function authenticatereseller() {
        $enc = new bCrypt();
        $record = Yii::app()->db->createCommand()
                ->select('*')
                ->from('portal_user')
                ->where('email=:email', array(':email' => $this->username))
                ->queryRow();
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //else if($record->password!==crypt($this->password,$record->password))
        } else if (!$enc->verify($this->password, $record['encrypted_password'])) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $record['id'];
            $this->setState('first_name', @$record['name']);
            $this->setState('email', @$record['email']);
            $this->setState('role_id', @$record['role_id']);
            $this->setState('parent_id', @$record['parent_id']);
            $this->setState('status', @$record['status']);
            if (in_array(36,  explode (',',$record['permission_id']))) {
                $this->setState('master',1);
            }else if(in_array(35,  explode (',',$record['permission_id']))) {
                $this->setState('referrer',1);
            }
            else if(in_array(34,  explode (',',$record['permission_id']))) {
                $this->setState('reseller',1);
            }
            //after login into partner portal save the Db confidentials into session from database             
            Yii::app()->session['dbname'] = @$record['dbname'];
            Yii::app()->session['db_username'] =  @$record['db_username'];
            Yii::app()->session['db_password'] = @$record['db_password'];
            Yii::app()->session['host'] = @$record['host'];
            Yii::app()->session['port'] = @$record['port'];
            //after login into partner portal save the Db confidentials into session from database
           
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    //To make studio login
    public function authenticatestudio() {
        $enc = new bCrypt();
        $record = User::model()->findByAttributes(array('email' => $this->username));
        $record = Yii::app()->db->createCommand()
                    ->select('u.*,s.name,s.s3bucket_id,s.new_cdn_users,s.studio_s3bucket_id,s.ga_profile_id,s.domain,s.subdomain,s.status,s.theme,s.contact_us_email,s.lunch_dt,s.domain,s.is_embed_white_labled, s.reseller_id')
                ->from('user u')
                ->join('studios s', 'u.studio_id = s.id')
                    ->where('email=:email AND studio_id=:studio_id', array(':email' => $this->username,':studio_id'=>$_SESSION["login_studio_id"]))
                ->queryRow();
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {

            $this->_id = $record['id'];
            $this->setState('signup_step', @$record['signup_step']);
            $this->setState('is_admin', @$record['is_admin']);
            $this->setState('first_name', @$record['first_name']);
            $this->setState('email', @$record['email']);
            $this->setState('lastname', @$record['last_name']);
            $this->setState('company', @$record['name']);
            $this->setState('created_at', @$record['created_at']);
            $this->setState('is_sdk', @$record['is_sdk']);
            $this->setState('role_id', @$record['role_id']); 
            $this->setState('ga_profile_id', @$record['ga_profile_id']);
            $this->setState('studio_subdomain', @$record['subdomain']);
            $this->setState('studio_id', @$record['studio_id']);
            $this->setState('status', @$record['status']);
            $this->setState('theme', @$record['theme']);
            $this->setState('lunch_date', @$record['lunch_dt'] ? $record['lunch_dt'] : date('Y-m-d', strtotime($record['created_at'])));
            $this->setState('siteName', @$record['name']);
            $this->setState('siteUrl', @$record['domain']);
            $this->setState('is_embed_white_labled', @$record['is_embed_white_labled']);
            $this->setState('adminEmail', @$record['contact_us_email']);
            $this->setState('s3bucket_id',@$record['s3bucket_id']);
            $this->setState('studio_s3bucket_id',@$record['studio_s3bucket_id']);
            $this->setState('new_cdn_users',@$record['new_cdn_users']);
            $this->setState('reseller_id',@$record['reseller_id']);
            unset(Yii::app()->request->cookies['in_preview_theme']);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }    
    
    

    //To validate frontend user
    public function validate($chkLogin = 1) {
        $studio_id = Yii::app()->common->getStudioId();
        $std_id = Yii::app()->common->getStudiosId();
        $enc = new bCrypt();
        if($chkLogin === 3){
            $record = SdkUser::model()->findByAttributes(array('mobile_number' => $this->username, 'studio_id' => $std_id, 'status' => 1));
        }else{
        $record = SdkUser::model()->findByAttributes(array('email' => $this->username, 'studio_id' => $std_id, 'status' => 1));                
        }
        if (isset($record) == null || count($record) == 0) {
            if($chkLogin === 3){
                $match = SdkUser::model()->findByAttributes(array('mobile_number' => $this->username, 'status' => 1));
            }else{
            $match = SdkUser::model()->findByAttributes(array('email' => $this->username, 'status' => 1));
            }
            if($match->studio_id == $std_id || ($match->studio_id == 0 && $match->is_developer == 1))
            {
                 if (!$enc->verify($this->password, $match['encrypted_password'])) {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                } else {
                    $this->_id = $match->id;
                    $this->setState('email', $match->email);
                    $this->setState('mobile_number', $match->mobile_number);
                    $this->setState('display_name', $match->display_name);
                    $this->setState('studio_id', $match->studio_id);
                    $this->setState('is_developer', $match->is_developer);
                    $this->setState('is_studio_admin', $match->is_studio_admin);
                    $this->setState('add_video_log', $match->add_video_log);
                    $signupLocation = unserialize($match->signup_location);
                    $this->setState('countryCode', $signupLocation['country']);
                    if (isset($_SESSION['internetSpeed'])) {
                        $this->setState('internetSpeed', $_SESSION['internetSpeed']);
                    } else {
                        $this->setState('internetSpeed', "");
                    }
                    $this->errorCode = self::ERROR_NONE;
                }
            }
            else {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            }            
            //else if($record->password!==crypt($this->password,$record->password))
        }  /*else if($record->id = 8150){
            $this->_id = $record->id;
            $this->setState('email', $record->email);
            $this->setState('display_name', $record->display_name);
            $this->setState('studio_id', $record->studio_id); 
            $this->setState('is_developer', $record->is_developer);
            $this->setState('is_studio_admin', $record->is_studio_admin);
            $this->setState('add_video_log', $record->add_video_log);
            $this->errorCode = self::ERROR_NONE;
        }*/ else if (!$enc->verify($this->password, $record['encrypted_password'])) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $record->id;
            $this->setState('email', $record->email);
            $this->setState('mobile_number', $record->mobile_number);
            $this->setState('display_name', $record->display_name);
            $this->setState('studio_id', $record->studio_id); 
            $this->setState('is_developer', $record->is_developer);
            $this->setState('is_studio_admin', $record->is_studio_admin);
            $this->setState('add_video_log', $record->add_video_log);
            $this->setState('new_cdn_users', !empty($_SESSION['new_cdn_users'])?$_SESSION['new_cdn_users']:0);
              $signupLocation = unserialize($record->signup_location);
                    
                    $this->setState('countryCode', $signupLocation['country']);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    //Auto login if studio does not need login
    public function authenticatestudioowner() {
        $std_id = Yii::app()->common->getStudiosId();
        
        $enc = new bCrypt();
        $record = SdkUser::model()->findByAttributes(array('email' => $this->username, 'studio_id' => $std_id, 'status' => 1));                
        if (count($record) == 0) {
            $this->_id = $record->id;
            $this->setState('email', $record->email);
            $this->setState('display_name', $record->display_name);
            $this->setState('studio_id', $record->studio_id); 
            $this->setState('is_developer', $record->is_developer);
            $this->setState('is_studio_admin', $record->is_studio_admin);
            $this->setState('add_video_log', $record->add_video_log);
            $this->errorCode = self::ERROR_NONE;
        } else {
            $this->_id = $record->id;
            $this->setState('email', $record->email);
            $this->setState('display_name', $record->display_name);
            $this->setState('studio_id', $record->studio_id); 
            $this->setState('is_developer', $record->is_developer);
            $this->setState('is_studio_admin', $record->is_studio_admin);
            $this->setState('add_video_log', $record->add_video_log);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }      
/**
 * @method Private LoginwithSocialData() Login with Social Media Data's
 * @return bool true/false
 * @author Gayadhar<support@muvi.com> 
 * 
 */
	function loginwithSocialData($std_id='',$social_userid,$email,$type){
		if(!$std_id){
			$std_id = Yii::app()->common->getStudioId();
		}
		$arr = array('email' => $email, 'studio_id' => $std_id, 'status' => 1);
		if($type=='gplus'){
			$arr['gplus_userid'] = $social_userid;
		}else{
			$arr['fb_userid'] = $social_userid;
		}
        $record = SdkUser::model()->findByAttributes($arr);
		if($record){
			$this->_id = $record->id;
			$this->setState('email', $record->email);
			$this->setState('display_name', $record->display_name);
			$this->setState('studio_id', $record->studio_id);   
			$this->setState('is_developer', $record->is_developer);
			if(isset($_SESSION['internetSpeed'])){
				$this->setState('internetSpeed', $_SESSION['internetSpeed']);
			}else{
				$this->setState('internetSpeed', "");
			}
                        /**
                         * added by Sanjeev
                         * login log for social logins starts
                        */
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = $record->id;
                        $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $std_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->ip = $ip_address;
                        $login_history->save();
                        Yii::app()->session['login_history_id'] = $login_history->id;
                        /**
                         * login log for social logins ends
                         */
			$this->errorCode = self::ERROR_NONE;
        } else {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        return !$this->errorCode;
	}

    public function getId() {
        return $this->_id;
    }
    public function authenticatepartnerfordev() {
        $enc = new bCrypt();
        $studio_id = Yii::app()->common->getStudiosId();
        //$record = User::model()->findByAttributes(array('email' => $this->username, 'is_active' => 1));
        $record = Yii::app()->db->createCommand()
                ->select('u.*,s.name,s.ga_profile_id,s.new_cdn_users,s.domain,s.subdomain,s.status,s.theme,s.contact_us_email,s.lunch_dt,s.domain,s.s3bucket_id,s.studio_s3bucket_id,s.is_embed_white_labled')
                ->from('user u')
                ->join('studios s', 'u.studio_id = s.id')
                ->where('email=:email AND studio_id=:studio_id AND role_id=:role_id AND is_active=:is_active AND is_deleted=:is_deleted', array(':email' => $this->username, ':studio_id' => $studio_id, ':role_id' => 4, ':is_active' => 1, ':is_deleted' => 0))
                ->queryRow();
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //else if($record->password!==crypt($this->password,$record->password))
        }  else {
            //tracking the login history
            $login = new StudioLoginHistory();
            $login->studio_id = $record['studio_id'];
            $login->studio_user_id = $record['id'];
            $login->login_at = new CDbExpression("NOW()");
            $login->login_ip = Yii::app()->request->getUserHostAddress();
            $login->save();
            $login_id = $login->id;             

            $this->_id = $record['id'];
            $this->setState('signup_step', @$record['signup_step']);
            $this->setState('is_admin', @$record['is_admin']);
            $this->setState('first_name', @$record['first_name']);
            $this->setState('email', @$record['email']);
            $this->setState('lastname', @$record['last_name']);
            $this->setState('company', @$record['name']);
            $this->setState('created_at', @$record['created_at']);
            $this->setState('is_sdk', @$record['is_sdk']);
            $this->setState('role_id', @$record['role_id']);
            $this->setState('ga_profile_id', @$record['ga_profile_id']);
            $this->setState('studio_subdomain', @$record['subdomain']);
            $this->setState('studio_id', @$record['studio_id']);
            $this->setState('status', @$record['status']);
            $this->setState('theme', @$record['theme']);
            $this->setState('lunch_date', @$record['lunch_dt'] ? $record['lunch_dt'] : date('Y-m-d', strtotime($record['created_at'])));
            $this->setState('siteName', @$record['name']);
            $this->setState('siteUrl', @$record['domain']);
			$this->setState('is_embed_white_labled', @$record['is_embed_white_labled']);
            $this->setState('adminEmail', @$record['contact_us_email']);
            $this->setState('s3bucket_id',@$record['s3bucket_id']);
            $this->setState('studio_s3bucket_id',@$record['studio_s3bucket_id']);
            $this->setState('new_cdn_users',@$record['new_cdn_users']);
            $this->setState('login_id',@$login_id);
            $this->setState('is_partner', 1);
            $this->errorCode = self::ERROR_NONE;
}
        return !$this->errorCode;
    } 
    public function authenticateResellerFromAdmin() {
        $record = Yii::app()->db->createCommand()
                ->select('*')
                ->from('portal_user')
                ->where('email=:email', array(':email' => $this->username))
                ->queryRow();
		
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            //else if($record->password!==crypt($this->password,$record->password))
        } else {
            $this->_id = $record['id'];
            $this->setState('first_name', @$record['name']);
            $this->setState('email', @$record['email']);
            $this->setState('role_id', @$record['role_id']);
            $this->setState('parent_id', @$record['parent_id']);
            $this->setState('status', @$record['status']);
            if (in_array(36,  explode (',',$record['permission_id']))) {
                $this->setState('master',1);
            }else if(in_array(35,  explode (',',$record['permission_id']))) {
                $this->setState('referrer',1);
}
            else if(in_array(34,  explode (',',$record['permission_id']))) {
                $this->setState('reseller',1);
            }
            //after login into partner portal save the Db confidentials into session from database             
            Yii::app()->session['dbname'] = @$record['dbname'];
            Yii::app()->session['db_username'] =  @$record['db_username'];
            Yii::app()->session['db_password'] = @$record['db_password'];
            Yii::app()->session['host'] = @$record['host'];
            Yii::app()->session['port'] = @$record['port'];
            //after login into partner portal save the Db confidentials into session from database
           
            $this->errorCode = self::ERROR_NONE;
        }
		
        return !$this->errorCode;
    }
    
    public function validateOTP($data) {
        $studio_id = Yii::app()->common->getStudiosId();
        if(!$data['is_generateotp']){
            $this->errorCode =  self::ERROR_GENERATE_OTP;
            return !$this->errorCode; exit;
        }
        if (($data['optenable'] == 2 || $data['optenable'] == 1) && $data['chklogin'] == 3) {
            $conditions = array('studio_id' => $studio_id, 'mobile_number' => $data['mobile_number'], 'otp' => $data['otp'], 'status' => 1);
        }else{
            $conditions = array('studio_id' => $studio_id, 'email' => $data['email'], 'otp' => $data['otp'], 'status' => 1);
        }
        $match = SdkUser::model()->findByAttributes($conditions); 
        $today = gmdate("Y-m-d H:i:s");
        $enc = new bCrypt();
        if (!empty($match)) {
            if (strtotime($today) < strtotime($match['opt_expiry_time'])) { 
                if ($match->studio_id == $studio_id || ($match->studio_id == 0 && $match->is_developer == 1)) { 
                    if (!$enc->verify($data['password'], $match['encrypted_password']) && $data['optenable'] == 2) {
                        $this->errorCode = self::ERROR_PASSWORD_INVALID;
                    } else {
                        if($match->is_subscribe_inapi == 0){
                            $this->errorCode = self::ERROR_SUBSCRIPTION_NULL;
                        }else if($match->is_subscribe_inapi == 2){
                            $this->errorCode = self::ERROR_SUBSCRIPTION_DEACTIVATED;
                        }else{
                        $this->_id = $match->id;
                        $this->setState('email', $match->email);
                        $this->setState('mobile_number', $match->mobile_number);
                        $this->setState('display_name', $match->display_name);
                        $this->setState('studio_id', $match->studio_id);
                        $this->setState('is_developer', $match->is_developer);
                        $this->setState('is_studio_admin', $match->is_studio_admin);
                        $this->setState('add_video_log', $match->add_video_log);
                        if (isset($_SESSION['internetSpeed'])) {
                            $this->setState('internetSpeed', $_SESSION['internetSpeed']);
                        } else {
                            $this->setState('internetSpeed', "");
                        }
                        $this->errorCode = self::ERROR_NONE;
                        }
                    }
                } else {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                }
            } else {
                $this->errorCode = self::ERROR_OTP_EXPIRED;
            }
        } else {
            $this->errorCode =  self::ERROR_INCORRECT_OTP;
        }
        return !$this->errorCode;
    }   
}
