<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 * Description of PGVariantImage
 * @author pritam@muvi.com
 */

class PGVariantImg extends CApplicationComponent {

    function getProductVariantImageSize($studio_id) {
        $pg_standard = StudioConfig::model()->getConfig($studio_id, 'pg_poster_dimension');
        if (isset($pg_standard)) {
            $pg_standard_dimension = $pg_standard->config_value;
        } else {
            $pg_standard_dimension = "220x260";
        }
            $pg_thumb_dimension = "100x100";
        $data = array('standard_dimension' => $pg_standard_dimension, 'thumb_dimension' => $pg_thumb_dimension);
        return $data;
    }
    
    function processProductVariantImageMulti($productid, $fileinfo, $req) {
        $_FILES['pg_var_Filedata'] = $fileinfo;
        $_REQUEST = $req;
        $studio_id = Yii::app()->common->getStudiosId();
        $poster_sizes = self::getProductVariantImageSize($studio_id); 

        $variant_sku = $req['var']['sku'];
        
        if(empty($variant_sku)){
            $variant_sku = $req['variant_sku'];
        } else{ 
            $variant_sku = $req['var']['sku'];
        }
        $standard_size = strtolower($poster_sizes['standard_dimension']);
        $thumb_size = strtolower($poster_sizes['thumb_dimension']);

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/pgposters/' . $productid;
        if (!empty($_REQUEST['pg_var_fileCrop'])) {
			$fileVarCropNew = array();
            foreach($_REQUEST['pg_var_fileCrop'] as $key=>$fileVarCrop){
                if (isset($_FILES['pg_var_Filedata']) && !($_FILES['pg_var_Filedata']['error'][$key])) {
                    $cdimension = array('thumb' => "64x64");
                    $fileDataArr = array(
                            'name' => $_FILES['pg_var_Filedata']['name'][$key],
                            'type' => $_FILES['pg_var_Filedata']['type'][$key],
                            'tmp_name' => $_FILES['pg_var_Filedata']['tmp_name'][$key],
                            'error' => $_FILES['pg_var_Filedata']['error'][$key],
                            'size' => $_FILES['pg_var_Filedata']['size'][$key],
                            
                        );
                    $ret1 = Controller::uploadToImageGallery($fileDataArr, $cdimension);					
                            $fileVarCropNew['x1'] = $fileVarCrop['x18'];
                            $fileVarCropNew['y1'] = $fileVarCrop['y18'];
                            $fileVarCropNew['x2'] = $fileVarCrop['x28'];
                            $fileVarCropNew['y2'] = $fileVarCrop['y28'];
                            $fileVarCropNew['w'] = $fileVarCrop['w8'];
                            $fileVarCropNew['h'] = $fileVarCrop['h8'];
					
                    $path = Yii::app()->common->jcropImage($fileDataArr, $dir, $fileVarCropNew);
                    //print'<pre>'; print_r($path); exit;
                    $ret = self::uploadProductVariantImage($fileDataArr, $productid, $cropDimension, $path , $variant_sku);
                    if ($productid) {
                        Yii::app()->common->rrmdir($dir);
                    }
                    //return $ret;
                }
            } 
        }
        if(!empty($_REQUEST['pg_var_imageCrop'])){
            foreach($_REQUEST['pg_var_imageCrop'] as $imageVarKey=>$jcrop_allimage){
                $file_info = pathinfo($_REQUEST['pg_var_image_file_names'][$imageVarKey]);
                $image_name = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
                $original_name = $_REQUEST['pg_var_original_images'][$imageVarKey];
                
                $dimension['x1'] = $jcrop_allimage['x19'];
                $dimension['y1'] = $jcrop_allimage['y19'];
                $dimension['x2'] = $jcrop_allimage['x29'];
                $dimension['y2'] = $jcrop_allimage['y29'];
                $dimension['w'] = $jcrop_allimage['w9'];
                $dimension['h'] = $jcrop_allimage['h9'];
                
                $path = Yii::app()->common->jcroplibraryImage($image_name, $original_name, $dir, $dimension);
                $fileinfo['name'] = $image_name;
                $fileinfo['error'] = 0;
                $ret = self::uploadProductVariantImage($fileinfo, $productid, $cropDimension, $path, $variant_sku);
                if ($productid) {
                    Yii::app()->common->rrmdir($dir);
                }
            }
            /*$files = glob($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'protected/components/*');
            foreach($files as $file){
              if(is_file($file))
                unlink($file);
            }*/
        }
        return true;
    }

    function uploadProductVariantImage($fileinfo, $productid , $dimension , $jcropPath = '',$variant_sku = '') {
        //print'<pre>'; print_r($jcropPath); exit;

        if ($fileinfo) {
            $fileName = Yii::app()->common->fileName($fileinfo['name']);
            /*$posterData = PGProductImage::model()->find('product_id=:product_id', array('product_id' => $productid));
            if ($posterData) {
                $oldposter = $posterData->attributes;
                //Remove old poster images
                //Update poster data 
                $posterData->name = $fileName;
                $posterData->product_id = $productid;
                $posterData->feature = 1;
                $posterData->save();
                $uid = $productid;
            } else { */
            
                //Get variant Id for reference in pg_product_image 
                $variant_list = PGVarient::model()->findByAttributes(array('product_id' => $productid,'varient_sku' => $variant_sku,'studio_id' => Yii::app()->user->studio_id));
            
                $poster = new PGProductImage();
                $poster->name = $fileName;
                $poster->product_id = $productid;
                $poster->varient_id = $variant_list['id'];
                $poster->feature = 1;
                $poster->save();
                $uid = $productid;
            //}
        }
        $studio_id = Yii::app()->common->getStudioId();
        //$uid = $_REQUEST['movie_id'];
        require_once "Image.class.php";
        require_once "Config.class.php";
        require_once "Uploader.class.php";
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        require_once "amazon_sdk/sdk.class.php";
        spl_autoload_register(array('YiiBase', 'autoload'));
        defined('BASEPATH') ? '' : define("BASEPATH", dirname(__FILE__) . "/..");
        $config = Config::getInstance();
        $config->setUploadDir($_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/pgposters');
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        //print'<pre>'; print_r($bucketInfo); exit;

        $config->setBucketName($bucketInfo['bucket_name']);
        $s3_config = Yii::app()->common->getS3Details($studio_id);
        //print'<pre>'; print_r($s3_config); exit;
        
        $config->setS3Key($s3_config['key']);
        $config->setS3Secret($s3_config['secret']);
        $config->setAmount(250);  //maximum paralell uploads
        $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
        $config->setDimensions($dimension);   //resize to these sizes
        //usage of uploader class - this simple :)
        $uploader = new Uploader($uid);
        $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
        
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $ret = $uploader->uploadPoster($fileinfo, $unsignedBucketPath . "public/system/pgposters/", $jcropPath);
        $poster = array($ret);
        return $ret;
    }

}
