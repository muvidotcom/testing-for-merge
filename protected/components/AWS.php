<?php
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
class AWS extends CApplicationComponent {

    public function getBandwidthPrice($arg = array(),$studio=array()) {
        $data = array();
        if (isset($arg) && !empty($arg)) {
            $is_bandwidth = $arg['is_bandwidth'];
             $con = Yii::app()->db;
            if (intval($is_bandwidth)) {
                $filter = 1;
                $sql = "SELECT * FROM bandwidth_pricing where studio_id = '".$studio->id."'";
                $studio_res = $con->createCommand($sql)->queryRow();
                if(empty($studio_res)){
                    if(trim($studio->reseller_id) && intval($studio->reseller_id)){
                        $sql = "SELECT * FROM bandwidth_pricing where reseller_id = '".$studio->reseller_id."' and is_reseller = 1";
                        $studio_res = $con->createCommand($sql)->queryRow();
                        if(empty($studio_res)){
                             $sql = "SELECT * FROM bandwidth_pricing where studio_id = 0 and is_reseller = 0 and reseller_id = 0";
                             $studio_res = $con->createCommand($sql)->queryRow();   
                             $filter = 0;                             
                        }
                    }else if(empty($studio_res)){
                             $sql = "SELECT * FROM bandwidth_pricing where studio_id = 0 and is_reseller = 0 and reseller_id = 0";
                             $studio_res = $con->createCommand($sql)->queryRow();   
                             $filter = 0;                    
                    }
                }
                $total_usage = $arg['bandwidth'];
                $total_bandwidth = ceil($total_usage / 1024);//Make GB to TB
                if ($total_bandwidth > 1 || $studio_res['free'] == 0) {
                    if($studio_res['free'] != 0){
                    $total_bandwidth_consumed = number_format((float) ($total_usage / 1024), 3, '.', '');
                    $total_billable_bandwidth_consumed = $total_bandwidth_consumed - $studio_res['free'];
                    $price =  number_format((float) ($total_billable_bandwidth_consumed * $studio_res['price'] * 1024), 2, '.', '');
                    }else{
                      // $total_bandwidth_consumed =  number_format((float) ($total_usage), 3, '.', '');
                       $total_bandwidth_consumed = number_format((float) ($total_usage / 1024), 3, '.', '');
                       $total_billable_bandwidth_consumed = $total_bandwidth_consumed;
                       $bill_bandwidth = number_format((float) ($total_usage), 3, '.', '');
                       $price =  number_format((float) ($bill_bandwidth * $studio_res['price'] ), 2, '.', '');
                    }
                   // echo $total_bandwidth_consumed; exit;
					$data['price'] = $price;
                    $data['total_bandwidth_consumed'] = $total_bandwidth_consumed;
                    $data['total_billable_bandwidth_consumed'] = $total_billable_bandwidth_consumed;
                    $data['price_per_gb'] = $studio_res['price'];
                    $data['free_uses'] = $studio_res['free'];
                }
            } else {
                if (isset($arg['hour']) && !empty($arg['hour'])) {
                    $temp_hour = explode(':', $arg['hour']);

                    if(isset($temp_hour[1]) && $temp_hour[1] != '00'){
                        $hour = intval($temp_hour[0])+1;
                    }else{
                        $hour = intval($temp_hour[0]);
                    }

                    if ($hour > 0) {
                        $data['hour'] = $hour;
                        $data['price'] = $hour * 0.01; //1 Cent for 1 hour
                    }
                }
            }
        }

        return $data;
    }
	
	public function getBandwidthPrice_bak($arg = array()) {
        $data = array();
        
        if (isset($arg) && !empty($arg)) {
            $is_bandwidth = $arg['is_bandwidth'];
            
            if (intval($is_bandwidth)) {
                $total_usage = array_sum(array_column($arg['usage'], 'bandwidth'));
                $total_bandwidth = ceil($total_usage / 1024);//Make GB to TB

                if ($total_bandwidth > 1) {
                    $con = Yii::app()->db;
                    $price = 0;
                    $total_billable_bandwidth_consumed = 0;
                    $total_bandwidth_consumed = number_format((float) ($total_usage / 1024), 3, '.', '');

                    foreach ($arg['usage'] as $key => $value) {
                        $code = $key;
                        $continent = $value['continent'];
                        $usage = $value['bandwidth'];
                        $free_percentage = $usage * 100/$total_usage;

                        $data['bandwidth'][$code]['continent'] = $continent;
                        $data['bandwidth'][$code]['total_usage'] = number_format((float) ($usage/1024), 3, '.', '');
                        $data['bandwidth'][$code]['free_usage'] = $free_usage = number_format((float) ($free_percentage/100), 3, '.', '');
                        $data['bandwidth'][$code]['billable_usage'] = $billable_usage = number_format((float) ($data['bandwidth'][$code]['total_usage'] - $data['bandwidth'][$code]['free_usage']), 3, '.', '');
                        $total_billable_bandwidth_consumed+=$billable_usage;

                        $sql = "SELECT * FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND 
                        max_limit <= (SELECT if(a.max_limit>a.min_limit,a.max_limit,a.min_limit) AS max_value FROM (SELECT 
                        (SELECT IF(MAX(max_limit) IS NULL, 0, MAX(max_limit)) FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND max_limit <= " . $billable_usage . ") AS min_limit, 
                        (SELECT IF(MIN(max_limit) IS NULL, 0, MIN(max_limit)) FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND max_limit >= " . $billable_usage . ") AS max_limit) 
                        a) ORDER BY max_limit";

                        $res = $con->createCommand($sql)->queryAll();
                        if (isset($res) && !empty($res)) {
                            $loop = $skip = 0;
                            $min = 0;
                            while ($billable_usage > 0) {
                                $unit_usage = ($billable_usage>=$res[$loop]['max_limit']-$skip) ? (($res[$loop]['max_limit']-$skip)) : (min($billable_usage, $res[$loop]['max_limit']));
                                $unit_usage = number_format((float) ($unit_usage), 3, '.', '');
                                $calculate = $unit_usage * 1024;
                                $billable_usage = $billable_usage - $res[$loop]['max_limit'] + $skip;
                                $skip += $res[$loop]['max_limit'];
                                $price +=  number_format((float) ($calculate * $res[$loop]['price']), 2, '.', '');
                                $ind_price =  number_format((float) ($calculate * $res[$loop]['price']), 2, '.', '');

                                $max = $res[$loop]['max_limit'];
                                if ($res[$loop]['max_limit'] > 100) {
                                    $max = 1000;
                                }

                                $data['bandwidth'][$code]['pricing'][$loop]['unit_price'] = $res[$loop]['price'];
                                $data['bandwidth'][$code]['pricing'][$loop]['from'] = $min;
                                $data['bandwidth'][$code]['pricing'][$loop]['to'] = $max;
                                $data['bandwidth'][$code]['pricing'][$loop]['price'] = $ind_price;
                                $data['bandwidth'][$code]['pricing'][$loop]['unit_usage'] = $unit_usage;

                                $min = $res[$loop]['max_limit'];
                                $min+=1; 
                                ++$loop;
                            }
                        }
                    }

                    $data['price'] = $price;
                    $data['total_bandwidth_consumed'] = $total_bandwidth_consumed;
                    $data['total_billable_bandwidth_consumed'] = $total_billable_bandwidth_consumed;
                }
            } else {
                if (isset($arg['hour']) && !empty($arg['hour'])) {
                    $temp_hour = explode(':', $arg['hour']);

                    if(isset($temp_hour[1]) && $temp_hour[1] != '00'){
                        $hour = intval($temp_hour[0])+1;
                    }else{
                        $hour = intval($temp_hour[0]);
                    }

                    if ($hour > 0) {
                        $data['hour'] = $hour;
                        $data['price'] = $hour * 0.01; //1 Cent for 1 hour
                    }
                }
            }
        }

        return $data;
    }
    
    public function getBandwidthPriceForReport($arg = array()) {
        $data = array();
        
        if (isset($arg) && !empty($arg)) {
            $is_bandwidth = $arg['is_bandwidth'];
            
            if (intval($is_bandwidth)) {
                $total_usage = array_sum(array_column($arg['usage'], 'bandwidth'));
                $total_bandwidth = ceil($total_usage / 1024);//Make GB to TB
                
                if ($total_bandwidth > 1) {
                    $con = Yii::app()->db;
                    $price = 0;

                    foreach ($arg['usage'] as $key => $value) {
                        $code = $key;
                        $continent = $value['continent'];
                        $usage = $value['bandwidth'];

                        $data['bandwidth'][$code]['continent'] = $continent;
                        $data['bandwidth'][$code]['bandwidth'] = $billable_usage = number_format((float) ($usage/1024), 3, '.', '');
                        $total_billable_bandwidth_consumed+=$data['bandwidth'][$code]['bandwidth'];

                        $sql = "SELECT * FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND 
                        max_limit <= (SELECT if(a.max_limit>a.min_limit,a.max_limit,a.min_limit) AS max_value FROM (SELECT 
                        (SELECT IF(MAX(max_limit) IS NULL, 0, MAX(max_limit)) FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND max_limit <= " . $usage . ") AS min_limit, 
                        (SELECT IF(MIN(max_limit) IS NULL, 0, MIN(max_limit)) FROM bandwidth_pricing WHERE continent_code = '" . $code . "' AND max_limit >= " . $usage . ") AS max_limit) 
                        a) ORDER BY max_limit";

                        $res = $con->createCommand($sql)->queryAll();
                        if (isset($res) && !empty($res)) {
                            $loop = $skip = 0;
                            $min = 0;
                            while ($billable_usage > 0) {
                                $unit_usage = ($billable_usage>=$res[$loop]['max_limit']-$skip) ? (($res[$loop]['max_limit']-$skip)) : (min($billable_usage, $res[$loop]['max_limit']));
                                $unit_usage = number_format((float) ($unit_usage), 3, '.', '');
                                $calculate = $unit_usage * 1024;
                                $billable_usage = $billable_usage - $res[$loop]['max_limit'] + $skip;
                                $skip += $res[$loop]['max_limit'];
                                $price +=  number_format((float) ($calculate * $res[$loop]['price']), 2, '.', '');
                                $ind_price =  number_format((float) ($calculate * $res[$loop]['price']), 2, '.', '');
                                
                                $max = $res[$loop]['max_limit'];
                                if ($res[$loop]['max_limit'] > 100) {
                                    $max = 1000;
                                }
                                
                                $data['bandwidth'][$code]['pricing'][$loop]['unit_price'] = $res[$loop]['price'];
                                $data['bandwidth'][$code]['pricing'][$loop]['from'] = $min;
                                $data['bandwidth'][$code]['pricing'][$loop]['to'] = $max;
                                $data['bandwidth'][$code]['pricing'][$loop]['price'] = $ind_price;
                                $data['bandwidth'][$code]['pricing'][$loop]['unit_usage'] = $unit_usage;

                                $min = $res[$loop]['max_limit'];
                                $min+=1; 
                                ++$loop;
                            }
                        }
                    }

                    $data['price'] = $price;
                    $data['total_bandwidth'] = $total_billable_bandwidth_consumed;
                }
            } else {
                if (isset($arg['hour']) && !empty($arg['hour'])) {
                    $temp_hour = explode(':', $arg['hour']);

                    if(isset($temp_hour[1]) && $temp_hour[1] != '00'){
                        $hour = intval($temp_hour[0])+1;
                    }else{
                        $hour = intval($temp_hour[0]);
                    }

                    if ($hour > 0) {
                        $data['hour'] = $hour;
                        $data['price'] = $hour * 0.01; //1 Cent for 1 hour
                    }
                }
            }
            
        }
        return $data;
    }
    
    function createCloudFontUrlForNewSignUp($accessKey,$secretKey,$bucketName,$studio_id,$onpremise = 0){
        $client_cloud = CloudFrontClient::factory(array(
                'key'    => $accessKey,
                'secret' => $secretKey,
        ));
        $bucket      = $bucketName;
        $addVar = '';
        if($onpremise ==1){
            $addVar = date("Y-m-d H:i:s");   
        }
        $originIdforunsigned =   'studio-unsigned-'.$studio_id.$addVar;
        $originIdforsigned =   'studio-signed-'.$studio_id.$addVar;
        $originIdforVideo =   'studio-video-'.$studio_id.$addVar;
        $singedpath = '/'.$studio_id.'/EncodedVideo';
        $unsingedpath = '/'.$studio_id.'/public';
        $unsingedpathForVideo = '/'.$studio_id.'/RawVideo';
        $resultforunsingedUrl = $client_cloud->createDistribution(array(
                // CallerReference is required
                'CallerReference' => $originIdforunsigned,
                'Aliases' => array(
                    'Quantity' => 0,
                ),
                'DefaultRootObject' => '',
                // Origins is required
                'Origins' => array(
                    // Quantity is required
                    'Quantity' => 1,
                    'Items' => array(
                        array(
                            'Id' => $originIdforunsigned,
                            'DomainName' => $bucket.".s3.amazonaws.com",
                            'OriginPath' => $unsingedpath,                                
                            'S3OriginConfig' => array(
                                'OriginAccessIdentity' => '' 
                            )
                        ),
                    ),
                ),
                // DefaultCacheBehavior is required
                'DefaultCacheBehavior' => array(
                    // TargetOriginId is required
                    'TargetOriginId' => $originIdforunsigned,
                    // ForwardedValues is required
                    'ForwardedValues' => array(
                        // QueryString is required
                        'QueryString' => true,
                        // Cookies is required
                        'Cookies' => array(
                            // Forward is required
                            'Forward' => 'all',
                        ),
                    ),
                    // TrustedSigners is required
                    'TrustedSigners' => array(
                        // Enabled is required
                        'Enabled' => false,
                        // Quantity is required
                        'Quantity' => 0,
                    ),
                    // ViewerProtocolPolicy is required
                    'ViewerProtocolPolicy' => 'allow-all',
                    // MinTTL is required
                    'MinTTL' => 1,
                ),
                'CacheBehaviors' => array(
                    'Quantity' => 0,
                ),
                // Comment is required
                'Comment' => 'Unsigned url for studio -'.$studio_id,
                'Logging' => array(
                    'Enabled' => false,
                    'IncludeCookies' => false,
                    'Bucket' => $bucket.".s3.amazonaws.com",
                    'Prefix' => '',
                ),
                'PriceClass' => 'PriceClass_All',
                // Enabled is required
                'Enabled' => true,
            )); 
        
        $resultforunsingedUrlForVideo = $client_cloud->createDistribution(array(
                // CallerReference is required
                'CallerReference' => $originIdforVideo,
                'Aliases' => array(
                    'Quantity' => 0,
                ),
                'DefaultRootObject' => '',
                // Origins is required
                'Origins' => array(
                    // Quantity is required
                    'Quantity' => 1,
                    'Items' => array(
                        array(
                            'Id' => $originIdforVideo,
                            'DomainName' => $bucket.".s3.amazonaws.com",
                            'OriginPath' => $unsingedpathForVideo,                                
                            'S3OriginConfig' => array(
                                'OriginAccessIdentity' => '' 
                            )
                        ),
                    ),
                ),
                // DefaultCacheBehavior is required
                'DefaultCacheBehavior' => array(
                    // TargetOriginId is required
                    'TargetOriginId' => $originIdforVideo,
                    // ForwardedValues is required
                    'ForwardedValues' => array(
                        // QueryString is required
                        'QueryString' => true,
                        // Cookies is required
                        'Cookies' => array(
                            // Forward is required
                            'Forward' => 'all',
                        ),
                    ),
                    // TrustedSigners is required
                    'TrustedSigners' => array(
                        // Enabled is required
                        'Enabled' => false,
                        // Quantity is required
                        'Quantity' => 0,
                    ),
                    // ViewerProtocolPolicy is required
                    'ViewerProtocolPolicy' => 'allow-all',
                    // MinTTL is required
                    'MinTTL' => 1,
                ),
                'CacheBehaviors' => array(
                    'Quantity' => 0,
                ),
                // Comment is required
                'Comment' => 'Unsigned video url for studio -'.$studio_id,
                'Logging' => array(
                    'Enabled' => false,
                    'IncludeCookies' => false,
                    'Bucket' => $bucket.".s3.amazonaws.com",
                    'Prefix' => '',
                ),
                'PriceClass' => 'PriceClass_All',
                // Enabled is required
                'Enabled' => true,
            )); 

            //Signed Url
            $resultforsingedUrl = $client_cloud->createDistribution(array(
                // CallerReference is required
                'CallerReference' => $originIdforsigned,
                'Aliases' => array(
                    'Quantity' => 0,
                ),
                'DefaultRootObject' => '',
                // Origins is required
                'Origins' => array(
                    // Quantity is required
                    'Quantity' => 1,
                    'Items' => array(
                        array(
                            'Id' => $originIdforsigned,
                            'DomainName' => $bucket.".s3.amazonaws.com",
                            'OriginPath' => $singedpath,                                
                            'S3OriginConfig' => array(
                                'OriginAccessIdentity' => '' 
                            )
                        ),
                    ),
                ),
                // DefaultCacheBehavior is required
                'DefaultCacheBehavior' => array(
                    // TargetOriginId is required
                    'TargetOriginId' => $originIdforsigned,
                    // ForwardedValues is required
                    'ForwardedValues' => array(
                        // QueryString is required
                        'QueryString' => true,
                        // Cookies is required
                        'Cookies' => array(
                            // Forward is required
                            'Forward' => 'all',
                        ),
                    ),
                    // TrustedSigners is required
                    'TrustedSigners' => array(
                        // Enabled is required
                        'Enabled' => true,
                        // Quantity is required
                        'Quantity' => 1,
                        'Items' => array(
                            'AwsAccountNumber' => 'self',
                        ),
                    ),
                    // ViewerProtocolPolicy is required
                    'ViewerProtocolPolicy' => 'allow-all',
                    // MinTTL is required
                    'MinTTL' => 1,
                ),
                'CacheBehaviors' => array(
                    'Quantity' => 0,
                ),
                // Comment is required
                'Comment' => 'Signed url for studio -'.$studio_id,
                'Logging' => array(
                    'Enabled' => false,
                    'IncludeCookies' => false,
                    'Bucket' => $bucket.".s3.amazonaws.com",
                    'Prefix' => '',
                ),
                'PriceClass' => 'PriceClass_All',
                // Enabled is required
                'Enabled' => true,
            )); 

        return $resultforunsingedUrl->get('DomainName').":::".$resultforsingedUrl->get('DomainName').":::".$resultforunsingedUrlForVideo->get('DomainName').":::".$resultforunsingedUrl->get('Id').":::".$resultforsingedUrl->get('Id').":::".$resultforunsingedUrlForVideo->get('Id');
    }
    
 
    
    public function bytesTogb($bytes, $precision = 2) { 
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        return round($bytes / $gigabyte, $precision);
    }
    
    public function getOrigialPathForGAllery($new_cdn_user,$studio_id)
    {
            $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
            $unsignedBucketPath = $folderPath['unsignedFolderPath'];
            $s_uploaded_path=$unsignedBucketPath."imagegallery";// for new user
            if($new_cdn_user==0)//existing user
            {
               $s_uploaded_path=$s_uploaded_path.$studio_id; 
            }
            return $s_uploaded_path;
    }
    public function addStorageLog($data){
        $storage = new StudioStorage();
        $res = StudioStorage::model()->findByAttributes(array('studio_id' => $data['studio_id'],'file_type' => $data['file_type']));
        if(isset($res) && !empty($res)){
            $data['original_video_file_count'] += $res->original_video_file_count;
            $data['original_file_size'] += $res->original_file_size;
            $data['converted_video_file_count'] += $res->converted_video_file_count;
            $data['converted_file_size'] += $res->converted_file_size;
            $data['total_file_size'] += $res->total_file_size;
            $updateRes = $storage->updateLog($res->id,$data);
        }else{
            $updateRes = $storage->addLog($data);
        }
        return $updateRes;
    }
    
    public function getLocationBandwidthSizebak($studio_id = 0, $start_date = '', $end_date = '', $deviceType= 0, $is_bandwidth = 0,$movie_id ='',$is_partner = 0) {
        $data = array();
        
        if (intval($studio_id) && (trim($start_date) != '0000-00-00') && (trim($end_date) != '0000-00-00')) {
            if (intval($is_bandwidth)) {
                $deviceStr = '';
                if(intval($deviceType)){
                    $deviceStr = " AND device_type=".$deviceType;
                }
                $cond = '';
                if($is_partner){
                    if(trim($movie_id)){
                        $cond = " AND movie_id IN (".$movie_id.")";
                    }
                }

                $sql = "SELECT bl.continent_code, SUM(bl.buffer_size) AS buffer_size, c.name AS continent FROM bandwidth_log bl LEFT JOIN continents c on bl.continent_code=c.code  WHERE bl.studio_id = " . $studio_id . $deviceStr. $cond . " AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY bl.continent_code";
                $res = Yii::app()->db->createCommand($sql)->queryAll();
                
                if (!empty($res)) {
                    foreach ($res as $value) {
                        if (trim($value['continent_code'])) {
                            $code = $value['continent_code'];
                            $bandwidthArr['usage'][$code]['continent'] = $value['continent'];
                            $bandwidthArr['usage'][$code]['bandwidth'] = (($value['buffer_size']/1024)/1024);
                            $bandwidthArr['raw_bandwidth'] += $value['buffer_size'];
                        }
                    }
                }
                $data = $bandwidthArr;
            } else {
                $sql = "SELECT SUM(played_length) AS watched_hour FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY studio_id";
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                $data['hour'] = gmdate("H:i", $res['watched_hour']);
            }
        }
        
        return $data;
    }
    
	public function getLocationBandwidthSize($studio_id = 0, $start_date = '', $end_date = '', $deviceType= 0, $is_bandwidth = 0,$movie_id ='',$is_partner = 0) {
        $data = array();
        if (intval($studio_id) && (trim($start_date) != '0000-00-00') && (trim($end_date) != '0000-00-00')) {
            if (intval($is_bandwidth)) {
				
                $deviceStr = '';
                if(intval($deviceType)){
                    $deviceStr = " AND device_type=".$deviceType;
                }
                $cond = '';
                if($is_partner){
                    if(trim($movie_id)){
                        $cond = " AND movie_id IN (".$movie_id.")";
                    }
                }

                $sql = "SELECT SUM(bl.buffer_size) AS buffer_size FROM bandwidth_log bl WHERE bl.studio_id = " . $studio_id . $deviceStr. $cond . " AND (DATE_FORMAT(bl.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY bl.studio_id";
                //$sql = "SELECT SUM(bl.buffer_size) AS buffer_size FROM bandwidth_log bl WHERE studio_id=331 GROUP BY bl.studio_id";
                $res = Yii::app()->db->createCommand($sql)->queryRow();
				$bandwidthArr = array();
                if (!empty($res)) {
					$bandwidthArr['bandwidth'] = (($res['buffer_size']/1024)/1024);
					$bandwidthArr['raw_bandwidth'] = $res['buffer_size'];
                }
                $data = $bandwidthArr;
            } else {
                $sql = "SELECT SUM(played_length) AS watched_hour FROM video_logs WHERE studio_id = " . $studio_id . " AND (DATE_FORMAT(created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "') GROUP BY studio_id";
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                $data['hour'] = gmdate("H:i", $res['watched_hour']);
            }
        }
        
        return $data;
    }
	
    public function getStorageUsageOfStudio($studio_id) {
        $size = 0;
        $new_cdn_users = Studios::model()->findByPk($studio_id)->new_cdn_users;
        if($new_cdn_users){
            $sql = "SELECT storage_size FROM studio_storage WHERE studio_id = " . $studio_id;
            $data = Yii::app()->db->createCommand($sql)->queryRow();
            $size = $data['storage_size'];
        }else{
            $size = self::getStorageUsageOfOldCdnUsersStudio($studio_id);
        }
        return $size; // in GB
    }
    public function getStorageUsageOfOldCdnUsersStudio($studio_id) {
        $sql = "SELECT original_file,converted_file FROM movie_streams WHERE studio_id = " . $studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        
        $total_size = 0;
        $original_size = 0;
        $converted_size = 0;
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $row) {
                $converted = explode('-', $row['converted_file']);
                $converted_size += (isset($converted) && !empty($converted)) ? $converted[1] : 0;
            }
        }
        
        $vl_sql = "SELECT video_properties FROM video_management WHERE studio_id = " . $studio_id;
        $vl_data = Yii::app()->db->createCommand($vl_sql)->queryAll();
        
        if (isset($vl_data) && !empty($vl_data)) {
            foreach ($vl_data as $value) {
                
                $video_properties = json_decode($value['video_properties']);
                if(isset($video_properties) && !empty($video_properties)){
                    $video_size_string = $video_properties->FileSize;
                    $video_size = explode(' ', $video_size_string);
                    $original_size += $video_size[0];
                }
            }
        }
        $original_size = $original_size/1024; //converting file size to GB
        
        $total_size = $original_size + ($converted_size/1024);
        
        return $total_size; //$total_size in GB
    }
    
    public function getStorageCost($total_storage = 0, $bandwidth = 0) {
        $storage = array();
        $total_storage = number_format((float) ($total_storage/1024), 3, '.', '');
        
        if (abs($total_storage) > 0.01) {
            
            if (abs($bandwidth) > 0.01) {
                if (intval($total_storage / $bandwidth) >= 1) {
                    $storage['total_storage'] = $total_storage;
                    if (intval($bandwidth) > 1) {
                        $storage['free_storage'] = $storage['bandwidth'] = number_format((float) ($bandwidth), 3, '.', '');
                    } else {
                        $storage['bandwidth'] = 0;
                        $storage['free_storage'] = number_format((float) (1), 3, '.', '');
                    }

                    $billable_storage = number_format((float) ($storage['total_storage'] - $storage['free_storage']), 3, '.', '');

                    $storage['billable_storage'] = ($billable_storage > 0 && abs($billable_storage) > 0.01) ? $billable_storage : 0;
                    $storage['cost'] = number_format((float) ($storage['billable_storage'] * 1024 * STORAGE_CHARGE), 2, '.', '');
                }
            } else {
                $storage['total_storage'] = $total_storage;
                $storage['bandwidth'] = 0;
                $storage['free_storage'] = number_format((float) (1), 3, '.', '');
                
                $billable_storage = number_format((float) ($storage['total_storage'] - $storage['free_storage']), 3, '.', '');
                
                $storage['billable_storage'] = ($billable_storage > 0 && abs($billable_storage) > 0.01) ? $billable_storage : 0;
                $storage['cost'] = number_format((float) ($storage['billable_storage'] * 1024 * STORAGE_CHARGE), 2, '.', '');
            }
        }
        
        return $storage;
    }
    
    public function getStorageCost1($total_storage = 0, $bandwidth = 0) {
        $storage = array();
        
        if (abs($total_storage) > 0.01) {
            $storage['total_storage'] = number_format((float) ($total_storage/1024), 3, '.', '');
            $storage['free_storage'] = number_format((float) (1), 3, '.', '');
            $storage['bandwidth'] = $bandwidth;
            
            $billable_storage = number_format((float) (($storage['total_storage'] - $storage['free_storage']) - ($bandwidth)), 3, '.', '');
            
            $storage['billable_storage'] = ($billable_storage > 0 && abs($billable_storage) > 0.01) ? $billable_storage : 0;
            $storage['cost'] = number_format((float) ($storage['billable_storage'] * 1024 * STORAGE_CHARGE), 2, '.', '');
        }
        
        return $storage;
    }
    
    public function isIpAllowed($ip = Null) {
        if (strpos($_SERVER['HTTP_HOST'], 'muvi.in')) {
            return TRUE;
        } else {
            $studio_id = Yii::app()->common->getStudiosId();
            if($studio_id == 54){
                $ipArray = array("180.87.253.35");
            } else{
                $ipArray = array("203.129.207.98","180.87.253.35");
            }
            if (!isset($ip)) {
                $ip = CHttpRequest::getUserHostAddress();
            }

            if (in_array($ip, $ipArray)) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }
    
    /*
     * return total video storage size (in GB) of a studio in S3
     * author SKM <sanjeev@muvi.com>
     */
    public function getStudioS3Storage($studio_id  = 0)
    {
        if(isset($studio_id) && $studio_id){
            $movie_size = 0;
            $trailer_size = 0;
            $raw_size = 0;
            $raw_size_audio = 0;
            $raw_image = 0;
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
            $signedBucketPath = $folderPath['signedFolderPath'];
            $unsignedBucketPath = $folderPath['unsignedFolderPathForVideo'];
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            
            $prefix_encoded_movie = $signedBucketPath.'uploads/movie_stream/full_movie/';
            $prefix_trailer = $signedBucketPath.'uploads/trailers/';
            $prefix_raw_video = $unsignedBucketPath.'videogallery/';
            //Arvind modified for audio_gallery : 22-1-2017
            $prefix_raw_audio = $unsignedBucketPath.'audiogallery/';
            $prefix_raw_image = $unsignedBucketPath.'videogallery/videogallery-image/';
            
            $response_movie = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucketInfo['bucket_name'],
                        'Prefix' => $prefix_encoded_movie
            ));
            $response_trailer = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucketInfo['bucket_name'],
                        'Prefix' => $prefix_trailer
            ));
            $response_raw_video = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucketInfo['bucket_name'],
                        'Prefix' => $prefix_raw_video
            ));
            //Arvind modified for audio_gallery : 22-1-2017
            $response_raw_audio = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucketInfo['bucket_name'],
                        'Prefix' => $prefix_raw_audio
            ));
            $response_raw_image = $s3->getListObjectsIterator(array(
                        'Bucket' => $bucketInfo['bucket_name'],
                        'Prefix' => $prefix_raw_image
            ));
            
            foreach ($response_movie as $object_movie) {
                $movie_size += $object_movie['Size'];
            }
            foreach ($response_trailer as $object_trailer) {
                $trailer_size += $object_trailer['Size'];
            }
            foreach ($response_raw_video as $object_raw_video) {
                $raw_size += $object_raw_video['Size'];
            }
            //Arvind modified for audio_gallery : 22-1-2017
            foreach ($response_raw_audio as $object_raw_audio) {
                $raw_size_audio += $object_raw_audio['Size'];
            }
            foreach ($response_raw_image as $object_raw_image) {
                $raw_image += $object_raw_image['Size'];
            }
            //Arvind modified for audio_gallery : 22-1-2017
            $size = $movie_size + $trailer_size + $raw_size + $raw_size_audio - $raw_image;
            $total_size = $size/1024/1024/1024;
            return $total_size;
        }else{
            return 0 ;
        }
    }
    
    function getDRMCostForStudio($studio_id = 0, $total_views = 0) {
        $sql = "SELECT price FROM drm_pricing WHERE studio_id={$studio_id} OR studio_id=0 ORDER BY studio_id DESC LIMIT 0, 1";
        $res = Yii::app()->db->createCommand($sql)->queryRow();
        $price = (isset($res['price']) && trim($res['price'])) ? $res['price'] : 0;
        
        $data = array();
        if (abs($price) >= 0.01) {
            $price = number_format((float) ($price), 2, '.', '');
            
            $data['drm_price_per_view'] = $price;
            $data['drm_total_views'] = $total_views;
            $data['drm_price'] = number_format((float) ($total_views * $price), 2, '.', '');
        }
        return $data;
    }

    function getshUploadFolderPath($functionParams){
        $fieldName = $functionParams['field_name'];
        $isSingaporeBucket =0;
        if(@$functionParams['singapore_bucket'] == 1){
            $isSingaporeBucket =1;
        }
        $data = array();
        $serverSpecific = EncodingServerDetails::model()->getFolderPathForSpecificServer(HOST_IP,$fieldName);
        if(@$serverSpecific[$fieldName] != ''){
            $data['shfolder'] = $serverSpecific[$fieldName].'/';
            $data['threads'] = $serverSpecific['threads'];
        } else{
            if ($isSingaporeBucket ==1) {
                $singaporefolderPath = EncodingServerDetails::model()->getFolderPathForSingapore($fieldName);
                if(@$singaporefolderPath[$fieldName] !=''){
                    $data['shfolder'] = $singaporefolderPath[$fieldName].'/';
                    $data['threads'] = $singaporefolderPath['threads'];
                }
            }
            if(empty($data)){
                $folderPath = EncodingServerDetails::model()->getFolderPath($fieldName);
                if(@$folderPath[$fieldName] != ''){
                    $data['shfolder'] = $folderPath[$fieldName].'/';
                    $data['threads'] = $folderPath['threads'];
                }
            }
        }
        return $data;
    }

    /*     * * below method Create the SH file for URL based upload and Upload the SH file to S3 bucket for video management
     * @return boolen true/false
     * @author Suraja Dash<support@muvi.com> */

    function createUploadVideoSH($data, $auth, $videoName, $videoSync = 0, $videoS3filepath = "",$studio_id = 0,$liveStreamVideoSyncId = 0, $old_video_name ='') {
        $condition = '';
        $videoFolder = 'upload_video';
        $shFolder = 'upload_progress';
        $conversionBucket = 'muvistudio';
        $ffmpeg_path = FFMPEG_PATH;
        if (HOST_IP == '127.0.0.1') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        } else if (HOST_IP == '52.0.64.95') {
            $videoFolder = 'staging/upload_video';
            $shFolder = 'staging/upload_progress';
            $conversionBucket = 'stagingstudio';
        }

        $con = Yii::app()->db;
        if($studio_id == 0){
            $studio_id = Yii::app()->common->getStudiosId();
        }

        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);

        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $s3cfg = $bucketInfo['s3cmd_file_name'];
        $contentType = explode('/', $data['content_type']);
        $extenson = $contentType[count($contentType) - 1];
        $video_gallery = new VideoManagement();
        $video_gallery->studio_id = $studio_id;
        if(isset(Yii::app()->user->id)){
            $video_gallery->user_id = Yii::app()->user->id;
        }
        $video_gallery->video_remote_url = $data['url'];
        $video_gallery->creation_date = new CDbExpression("NOW()");
        $video_gallery->save();
        $gallery_id = $video_gallery->id;
        if ($videoS3filepath != "") {
            $videoSyncTempSave = new VideoSyncTemp;
            $videoSyncTempSave->video_management_id = $gallery_id;
            $videoSyncTempSave->studio_id = $studio_id;
            $videoSyncTempSave->file_path = $videoS3filepath;
            $videoSyncTempSave->save();
        }

        $updateUrl = Yii::app()->getBaseUrl(true) . "/conversion/UpdateUrlUploadInfo?video_name=" . $gallery_id;

        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $signedBucketPath = $folderPath['unsignedFolderPathForVideo'];

        $urlForVideoNotDownloaded = Yii::app()->getBaseUrl(true) . "/conversion/UploadUrlVideoNotDownloaded?rec_id=" . $gallery_id."MUVIMUVI".$liveStreamVideoSyncId;



        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
        ));

        $bucketHttpUrl = CDN_HTTP . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'videogallery/';
        $bucket_url = "s3://" . $bucketName . "/" . $signedBucketPath . "videogallery/";
        $updateUrl .= "MUVIMUVI" . $videoName;
        $updateUrl .= "MUVIMUVI".$liveStreamVideoSyncId;
        //Create shell script
        $file = 'uploadvideogallerySH_' . $gallery_id .HOST_IP. '.sh';
        $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file, 'w') or die('Cannot open file:  ' . $file);
        $cf = 'echo "${file%???}"';
        // Wget URL 
        $username = '';
        $password = '';
        if ($auth) {
            $username = $auth['access_username'];
            $password = $auth['access_password'];
        }
        $gotDir = 'cd /var/www/html/' . $videoFolder . '/videogallery_' . $gallery_id;
        if ($videoSync == 1) {
            $wgetCMD = "/usr/local/bin/s3cmd get --config /usr/local/bin/." . $s3cfg . " " . $data['s3videoPath'] . " \n";
        } else if ($videoSync == 2) {
            $wgetCMD = "/usr/local/bin/s3cmd get --config /usr/local/bin/.sonyvdsys3cfg " . $data['s3videoPath'] . " \n";
        } else if ($username && $password) {
            $data['url'] = str_replace('https://', '', $data['url']);
            $data['url'] = str_replace('www.', '', $data['url']);
            if (preg_match("~^(?:f)tps?://~i", $data['url'])) {
                $data['url'] = str_replace('ftp://', '', $data['url']);
                $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   ftp://' . $username . ':' . $password . '@' . $data['url'];
            } else {
                $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   https://' . $username . ':' . $password . '@' . $data['url'];
            }
        } else {
            $wgetCMD = 'wget -O ' . $videoName . ' -e --robots=off -r --level=0 -nc   ' . $data['url'];
        }
        if(isset($old_video_name) && $old_video_name!=''){
            $wgetCMD .= "mv /var/www/html/".$videoFolder."/videogallery_$gallery_id/$old_video_name /var/www/html/".$videoFolder."/videogallery_$gallery_id/$videoName";
        }
        $moveFile .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/" . $videoFolder . "/videogallery_" . $gallery_id . "/" . $videoName . " " . $bucket_url . " \n";
        $checkFileExistIfCondition .= 'if [ -s \'/var/www/html/' . $videoFolder . '/videogallery_' . $gallery_id . '/' . $videoName . "' ] \n then \n";
        $checkFileExistElseCondition .="\n\nelse\n\n";

        $checkFileExistElseCondition .= "curl $urlForVideoNotDownloaded\n";
        if ($videoSync == 1) {
            $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/." . $s3cfg . " " . $data['s3videoPath'] . " \n";
        } else if ($videoSync == 2) {
            $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/.sonyvdsys3cfg " . $data['s3videoPath'] . " \n";
        } else if ($videoSync == 3) {
            $moveFile .= "/usr/local/bin/s3cmd del --config /usr/local/bin/.s3cfg " . $data['s3videoPath'] . " \n";
        }
        $file_data = "file=`echo $0`\n" .
                "cf='" . $file . "'\n\n" .
                "if [ -f \"/var/www/html/$shFolder/conversionCompleted/$file\" ]\n\n" .
                "then\n\n" .
                "echo \"$file is running\"\n\n" .
                "else\n\n" .
                "sudo echo \"some file content\" > /var/www/html/$shFolder/conversionCompleted/$file\n\n" .
                "# create directory\n" .
                "mkdir /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "chmod 0777 /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "#Go to Dirctory \n" .
                $gotDir . "\n" .
                "# wget command\n" .
                $wgetCMD .
                "\n" .
                "\n\n\n# Check all the video file's are created\n" .
                $checkFileExistIfCondition .
                "\n# upload to s3\n" .
                $moveFile .
                "# update query\n" .
                "curl $updateUrl\n" .
                "# remove directory\n" .
                "rm -rf /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "# remove the process copy file\n" .
                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                "# remove the process file\n" .
                "rm /var/www/html/$shFolder/$file\n" .
                $checkFileExistElseCondition .
                "# remove directory\n" .
                "rm -rf /var/www/html/$videoFolder/videogallery_$gallery_id\n" .
                "# remove the process copy file\n" .
                "rm /var/www/html/$shFolder/conversionCompleted/$file\n" .
                "# remove the process file\n" .
                "rm /var/www/html/$shFolder/$file\n" .
                "fi\n\n" .
                "fi";
        fwrite($handle, $file_data);
        fclose($handle);
        //Uploading conversion script from local to s3 
        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
        ));
        $filePath = $_SERVER['DOCUMENT_ROOT'] . "/upload_progress/" . $file;
        $functionParams = array();
        $functionParams['field_name'] = 's3_upload_folder';
        if($bucketName == 'vimeoassets-singapore'){
            $functionParams['singapore_bucket'] = 1;
        }
        $shDataArray = Yii::app()->aws->getshUploadFolderPath($functionParams);
        $s3folderPath = $shDataArray['shfolder'];
        if ($s3->upload($conversionBucket, $s3folderPath . $file, fopen($filePath, 'rb'), 'public-read')) {
            //echo "Sh file created successfully";
            if (unlink($filePath)) {
                $video_management = VideoManagement::model()->findByPk($gallery_id);
                $video_management->flag_uploaded = 1;
                $video_management->video_name = $videoName;
                $video_management->save();
            }
        }
    }

    /**
     * @method private GenerateExplayToken($content_key,$key_id) It will generate the express token
     * @author Srutikant<support@muvi.com>
     * @param string $content_ke Uniquie content Key
     * @param string $key_id Uniqe key id
     * @return array A arrya of keys 
     */
    function generateExplayToken($conentKey = '', $keyId = '') {
        if ($conentKey && $keyId) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, WV_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $keyId . "&contentKey=" . $conentKey . "&securityLevel=1&hdcpOutputControl=0&expirationTime=%2B120");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);
            $return['wvToken'] = $server_output;
            curl_close($ch);

            //Playready DRM 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, PR_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $keyId . "&contentKey=" . $conentKey . "&rightsType=BuyToOwn&analogVideoOPL=100&compressedDigitalAudioOPL=100&compressedDigitalVideoOPL=100&uncompressedDigitalAudioOPL=100&uncompressedDigitalVideoOPL=100&expirationTime=%2B120");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $output = curl_exec($ch);
            $output = json_decode($output, TRUE);
            if(@$output['licenseAcquisitionUrl']){
                $output['licenseAcquisitionUrl'] = str_replace("http://","https://",$output['licenseAcquisitionUrl']);
            }
            $return['playReadyToken'] = @$output['licenseAcquisitionUrl'] . '?ExpressPlayToken=' . @$output['token'];
            curl_close($ch);

            //FairPlay DRM 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, FP_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&iv=" . $conentKey . "&contentKey=" . $keyId . "&expirationTime=%2B120");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $fairPlayoutput = curl_exec($ch);
            if(@$fairPlayoutput){
                if (HOST_IP == '52.0.64.95') {
                    $fairPlayoutput = str_replace("http://fp.test.expressplay.com:80/hms/fp/rights/","https://fp.test.expressplay.com:443/hms/fp/rights/",$fairPlayoutput);
                } else{
                    $fairPlayoutput = str_replace("http://fp.service.expressplay.com:80/hms/fp/rights/","https://fp.service.expressplay.com:443/hms/fp/rights/",$fairPlayoutput);
                }
            }
            $return['fairPlayToken'] = @$fairPlayoutput;
            curl_close($ch);
            
            return $return;
        } else {
            return false;
        }
    }
     function getDRMCostForStudioWithDevice($studio_id = 0, $total_views = array()) {
        $sql = "SELECT price,device_type,studio_id FROM drm_pricing WHERE studio_id={$studio_id} or studio_id = 0 ORDER BY device_type";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
 
        for($i = 0; $i<count($res);$i++){
            for($j=$i+1;$j<count($res);$j++){
                if($res[$i]['device_type'] == $res[$j]['device_type']){
                    if($res[$i]['studio_id'] > $res[$j]['studio_id']){
                        $j_arr[] = $j;
                   }else{
                       $i_arr[] = $i;
}
                }
            } 
        }
        foreach($j_arr as $key => $val){
            unset($res[$val]);
        }
        foreach($i_arr as $key => $val){
            unset($res[$val]);
        }
        $res = array_values($res);
        $price = 0;
         $data = array();
          $data['drm_price'] = 0;
          $data['drm_total_views'] = 0;
        foreach($res as $kay => $val){
            if($val['price'] > 0){
                switch($val['device_type']){
                    case 1:
                     $data['drm_price_per_web_view'] = $val['price'];
                       if(isset($total_views[0]['total_views_web']) && $total_views[0]['total_views_web'] > 0){
                           $data['drm_price_web'] = $total_views[0]['total_views_web'] * $data['drm_price_per_web_view'];
                           $data['drm_price'] += $data['drm_price_web'];
                           $data['drm_total_views'] += $total_views[0]['total_views_web'];
                           $data['drm_total_web_views'] = $total_views[0]['total_views_web'];
                       }
                    break;
                    case 2:
                     $data['drm_price_per_android_view'] = $val['price'];
                     if(isset($total_views[1]['total_views_android']) && $total_views[1]['total_views_android'] > 0){
                      $data['drm_price_android'] = $total_views[1]['total_views_android'] * $data['drm_price_per_android_view'];
                      $data['drm_price'] += $data['drm_price_android'];
                      $data['drm_total_views'] += $total_views[1]['total_views_android'];
                       $data['drm_total_android_views'] = $total_views[1]['total_views_android'];
                    }                       
                    break;  
                     case 3:
                     $data['drm_price_per_ios_view'] = $val['price'];
                     if(isset($total_views[2]['total_views_ios']) && $total_views[2]['total_views_ios'] > 0){
                      $data['drm_price_ios'] = $total_views[2]['total_views_ios'] * $data['drm_price_per_ios_view'];
                      $data['drm_price'] += $data['drm_price_ios'];
                      $data['drm_total_views'] += $total_views[2]['total_views_ios'];
                        $data['drm_total_ios_views'] = $total_views[2]['total_views_ios'];
                    }                         
                    break; 
                     case 3:
                         $data['drm_price_per_raku_view'] = $val['price'];
                     if(isset($total_views[3]['total_views_Roku']) && $total_views[3]['total_views_Roku'] > 0){
                      $data['drm_price_raku'] =  $total_views[3]['total_views_Roku'] * $data['drm_price_per_raku_view'];
                      $data['drm_price'] +=  $data['drm_price_raku'];
                      $data['drm_total_views'] += $total_views[2]['total_views_Raku'];
                      $data['drm_total_raku_views'] = $total_views[2]['total_views_Raku'];
                     }                           
                    break;                
                }
               
            }
        }
        $data['drm_price'] = number_format((float) ($data['drm_price']), 2, '.', ''); 
        $data['specified_device_type'] = 1;
        return $data;
    }   
    
    public function getAudioUrl($stream_id = 0, $bit_val='') {
        if(@$stream_id){
            $sql = Yii::app()->db->createCommand()
                    ->select('id,studio_id,full_movie')
                    ->from('movie_streams')
                    ->where('id ='.$stream_id);
            $data = $sql->queryAll();
            $bucketInfo = Yii::app()->common->getBucketInfo('', $data[0]['studio_id']);
            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
            if($stream_id && $bit_val=='getAudio'){
            $filePth = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $data[0]['id'] . '/' . $data[0]['full_movie'];
            }else{
                $audio_file_name = explode('.',$data[0]['full_movie']);
                if($bit_val=='Best'){
                    $audio_file_name = $audio_file_name[0].'.'.$audio_file_name[1];
                }else{
                    $audio_file_name = $audio_file_name[0].'_'.$bit_val.'.'.$audio_file_name[1];
                }
                $filePth = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $data[0]['id'] . '/' . $audio_file_name;
            }
            }
            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
            $url = Yii::app()->controller->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,1,"",60000);
            return $url;
        }
    
    public function sentMailForWrongBandwidthLog($data){
        $bandwidthUsageLimit = 10;
        $return = 1;
        if(@$data['request_type'] == 'mped_dash'){
            $videoType =  "DRM";
        } else{
            $videoType =  "Non-DRM";
        }
        if(((((@$data['bandwidth_used']/1024)/1024)  > $bandwidthUsageLimit) || ($data['bandwidth_used'] == 0 && $videoType != 'Non-DRM')) && @$data['buff_log_id']){
            $bandwidth_used = $data['bandwidth_used'];
            //once fire mail
            $temp_bandwidth_log = TempBandwidthLog::model()->getTempBandwidthLog($data['buff_log_id']);
            //if bandwidth_log_id not exists in tamp_table
            if(($temp_bandwidth_log[0]['bandwidth_log_id']=="") && empty($temp_bandwidth_log[0]['bandwidth_log_id'])){
                //mail content
                $mailcontent.="<p>Hi All,</p><br/>";
                $mailcontent .= "<table border='1' cellspacing='0' cellpadding='10'>";
                if($bandwidth_used==0){
                    $mailcontent .= "<tr style='text-align:center'><td colspan='6'><b>".$videoType." Bandwidth Information Remain Zero</b></td></tr>";
                }else{
                    $mailcontent .= "<tr style='text-align:center'><td colspan='6'><b>".$videoType." Bandwidth Information Exceeds >".$bandwidthUsageLimit."gb</b></td></tr>";
                }
                $mailcontent .= "<tr><th>URL</th><th>Request Content</th><th>BandwidthLog ID</th><th>Bandwidth Used</th><th>Device</th></tr>"
                . "<tr>"
                . "<td>".Yii::app()->getbaseUrl(true)."</td>"
                . "<td>".@$data['browser_details']."</td>"
                . "<td>".@$data['buff_log_id']."</td>"
                . "<td>".$bandwidth_used.'(KB)'."</td>";
                if(!isset($data['device_type']) || $data['device_type']==1){
                    $mailcontent .= "<td>Web</td>";
                } elseif ($data['device_type']==2) {
                    $mailcontent .= "<td>Android</td>";
                } elseif ($data['device_type']==3) {
                    $mailcontent .= "<td>IOS</td>";
                } elseif ($data['device_type']==4) {
                    $mailcontent .= "<td>Roku</td>";
                } elseif ($data['device_type']==5) {
                    $mailcontent .= "<td>Fire Tv</td>";
                }
                $mailcontent .= "</tr>";
                $mailcontent .= "</table>";
                $mailcontent .= "<br/><p>Regards,<br/>Team Muvi</p>";
                //For the 1st time bandwidth_log mail forward and insert to temp_table
                Yii::app()->email->drmBandwidthMail($mailcontent);
                $temp_bandwidth = new TempBandwidthLog();
                $temp_bandwidth->bandwidth_log_id = @$data['buff_log_id'];
                $temp_bandwidth->bandwidth_value = $bandwidth_used;
                $temp_bandwidth->browser_information = @$data['browser_details'];
                $temp_bandwidth->save();
            }else{
                //If bandwidth always exceeds 10gb
                if($bandwidth_used>0){
                    $res_data = TempBandwidthLog::model()->updateTempBandwidthLog($temp_bandwidth_log[0]['bandwidth_log_id'], @$data['browser_details'],@$bandwidth_used);
                }
                //If bandwidth always zero then
                else{
                    $temp_bandwidth = new TempBandwidthLog();
                    $temp_bandwidth->bandwidth_log_id = @$data['buff_log_id'];
                    $temp_bandwidth->bandwidth_value = $bandwidth_used;
                    $temp_bandwidth->browser_information = @$data['browser_details'];
                    $temp_bandwidth->save();
                }
            }
            $return = 0;
        }
        return $return;
    }
    
    public function getNginxServerIp($studio_id){
        $studioData = Yii::app()->db->createCommand()
					->select('is_subscribed,status,is_deleted,is_default')
					->from('studios')
					->where('id='.$studio_id)
					->queryRow();
       if(@$studioData['is_subscribed'] == 0 && @$studioData['status'] == 1 && @$studioData['is_deleted'] == 0  && @$studioData['is_default'] == 0){
            $nginxserverip = '34.233.242.44';
        } else if (HOST_IP == '52.0.64.95') {
            $nginxserverip = '34.195.230.134';  
        } else{
            $nginxserverip = '52.20.205.94';
        }
        return $nginxserverip;
    }
    
    public function getNginxServerDetails($ip) {
        $nginxServerDetails = array();
        if($ip == '34.233.242.44'){
            $nginxServerDetails['nginxserverlivecloudfronturl'] = 'https://d28givsgxmkl3a.cloudfront.net';
            $nginxServerDetails['nginxserverrecordcloudfronturl'] = 'https://d2h7prcocx7qs5.cloudfront.net';
            $nginxServerDetails['NGINX_IP_HTTP'] = 'https://livestreamlead.muvi.com';
            $nginxServerDetails['recording_folder_path'] = 'live/';
        } else if($ip == '34.195.230.134'){
            $nginxServerDetails['nginxserverlivecloudfronturl'] = 'http://34.195.230.134/live';
            $nginxServerDetails['nginxserverrecordcloudfronturl'] = 'http://34.195.230.134/record';
            $nginxServerDetails['NGINX_IP_HTTP'] = 'http://34.195.230.134';
            $nginxServerDetails['recording_folder_path'] = 'staging/';
        } else if($ip == '52.20.205.94'){
            $nginxServerDetails['nginxserverlivecloudfronturl'] = 'https://d2vo3ozpc06skj.cloudfront.net';
            $nginxServerDetails['nginxserverrecordcloudfronturl'] = 'https://d392hcdnl497u1.cloudfront.net';
            $nginxServerDetails['NGINX_IP_HTTP'] = 'https://livestream.muvi.com';
            $nginxServerDetails['recording_folder_path'] = 'live/';
        }
        return $nginxServerDetails;
    }
}
