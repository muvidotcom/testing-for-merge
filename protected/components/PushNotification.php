<?php

class Push extends AppComponent  {
 
    // push message title
    private $title;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;
 
    function __construct() {
         
    }
 
    public function setTitle($title) {
        $this->title = $title;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
 
    public function setPayload($data) {
        $this->data = $data;
    }
 
    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }
    
    public function getPushAndroid() {
        $res = array();
        $res['data']['user_id'] = $this->title;
        $res['data']['message'] = $this->message;
        return $res;
    }
    public function getPushIos() {
        $res = array();
        $res['title'] = $this->title;
        $res['body'] = $this->message;
        $res['user_id'] = $this->title;
        $res['message'] = $this->message;
        $res['content_available'] = true;
        return $res;
    }
     // sending push message to single user by firebase reg id
    public function send($to, $message) {
        $fields = array(
            'to' => $to,
            'data' => $message,
        );
        $result= self::sendPushNotification($fields);
        return $result; 
    }
 
    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        $result= self::sendPushNotification($fields);
        return $result;  
    }
    // sending push message to multiple users by firebase registration ids Android devices
    public function sendMultiple($reg_ids, $message) {
		$result = array();
		$newId = array_chunk($reg_ids, 1000);
			foreach ($newId as $reg_ids) {
        $fields = array(
            'registration_ids' => $reg_ids,
            'data' => $message,
        );
				$result[]= self::sendPushNotification($fields);
			}
        return $result;
    }
    // sending push message to multiple users by firebase registration ids Ios devices
    public function sendMultipleNotify($reg_ids2, $message) {
		$results = array();
		$newId2 = array_chunk($reg_ids2, 1000);
			foreach ($newId2 as $reg_ids2) {
        $fields = array(
            'registration_ids' => $reg_ids2,
            'notification' => $message,
        );
		}
			$result[]= self::sendPushNotification2($fields);
        return $result;
    }
   
    //Push notification for ANDROID devices
    
    public function sendPushNotification($fields) {
        $studio_id = Yii::app()->common->getStudiosId();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fcm = new FcmKey();
        $firebase_API = $fcm->getGpushNotificationAPIkey($studio_id);        
        $androidApi=$firebase_API['android'];
        $headers = array(
            'Authorization: key='. $androidApi,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
         
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        return $result;
    }
    
    //Push notification for Ios devices
    public function sendPushNotification2($fields) {
        $studio_id = Yii::app()->common->getStudiosId();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fcm = new FcmKey();
        $firebase_API = $fcm->getGpushNotificationAPIkey($studio_id); 
        $iosApi = $firebase_API['ios'];
        $headers=array(
            'Authorization: key='. $iosApi,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
         
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        return $result;
    }
}