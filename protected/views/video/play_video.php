<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true);?>/js/video.dev.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true);?>/js/vjs.youtube.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true);?>/js/videojs.watermark.js"></script>
<!--<script src="<?php echo Yii::app()->baseUrl;?>/js/videojs.hls.min.js"></script>-->
<link href="<?php echo Yii::app()->getbaseUrl(true);?>/common/css/flat-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true);?>/common/css/video_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true);?>/common/css/small_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true);?>/common/css/videojs.watermark.css" rel="stylesheet" type="text/css" />


<script>
		
	videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl;?>/js/video-js.swf";
	
</script>
<?php 
if($_SERVER['HTTP_HOST']!='localhost'){
require_once $_SERVER['DOCUMENT_ROOT'].Yii::app()->theme->baseUrl.'/views/layouts/analyticstracking.php';
}?>
<center>
	<div class="demo-video" >
		<video id = "video_block" class="video-js" controls
              preload="none" data-setup='{"techOrder": ["youtube", "html5","flash"]}'>
              <source src="" type="video/mp4">
            </video>
	</div> 
</center>
<div id="episode_block" class="episode_block">

</div>

<?php if(strtolower($movieData['content_type'])=='tv'){
		$ctype = 'tv-show';
	}else{
		$ctype = strtolower($movieData['content_type']);
	}?>
<script>
  var myPlayer,full_screen=false,request_sent = false,first_time = true, plus_content="", btn_html = "",show_control = true;
 
$(window).bind("load", function() {
	<?php if($_SERVER['HTTP_HOST']!='localhost' && !(Yii::app()->session['is_studio_admin'])){?>
	 ga('send', 'event', '<?php echo $movieData['content_type'];?>', '<?php echo $movieData['name'];?>'); 
	<?php }?>
 });
 
	$(document).ready(function() {
	
	$('#video_block').bind('contextmenu',function() { return false; });

	var movie_id = "<?php echo $_REQUEST['movie']?>";
	var full_movie = "<?php echo $fullmovie_path?>";
	var can_see = "<?php echo $can_see?>";
	var back_url = "<?php echo $this->createAbsoluteUrl($ctype.'/'.$per_link );?>";
	if(full_movie != ""){
		videojs("video_block").ready(function(){
			var is_restrict = "<?php echo $is_restrict?>";
			if(is_restrict == "1"){
				alert("This Movie is not allowed to watch in your country.");
				return;
			}else{
				if(can_see){
					$(".vjs-tech").mousemove(function(){
					  if(full_screen == true && show_control == false){
						$("#video_block .vjs-control-bar").show();
						$(".vjs-watermark").show();
						show_control = true;
						var timeout = setTimeout(function(){
						  if(full_screen == true){
							$("#video_block .vjs-control-bar").hide();
							$(".vjs-watermark").hide();
							show_control = false;
							
						  }
						},10000);
						$(".vjs-control-bar").mousemove(function() {
                            event.stopPropagation();
                		}).mouseout(function() {
                            event.stopPropagation();
                		});
					  }else{
						clearTimeout(timeout);
					  }
					});
					videojs("video_block").on("fullscreenchange",resize_player);
					var video_id = videojs("video_block");
					video_id.watermark({
						file: "<?php echo $v_logo;?>",
						xpos: 90,
						ypos: 75,
						xrepeat: 0,
						opacity: 0.5
					});
					$(".vjs-watermark").hide();
					$(".vjs-watermark").click(function(){
						window.location.href = back_url;
					});
                    $("#video_block").mouseenter(function() {
						$(".vjs-watermark").show();
					}).mouseleave(function() {
						$(".vjs-watermark").hide();
					});
					$(".vjs-watermark").mouseover(function() {
						event.stopPropagation();
					}).mouseout(function() {
						event.stopPropagation();
					});
					play_video(full_movie,movie_id);
				}else{
					show_warning_video();
				}
			}
		});
	}else{
		//show_more_info(movie_id);
	}
  });
  
  function resize_player(){
	if(full_screen == false){
      full_screen = true;
      var large_screen = setTimeout(function(){
        if(full_screen == true){
		  show_control = false;
		  $(".vjs-watermark").hide();
          $("#video_block .vjs-control-bar").hide();
        }
      },5000);
    }else{
      //clearTimeout(large_screen);
      full_screen = false;
      $("#video_block .vjs-control-bar").show();
    }
  }
  
  
  function show_warning_video(){
	alert('Seems like you have already watched 3 movies this month. Please come back next month to watch again.');
  }
  function play_video(video_link,movie_id){
	var url = "<?php echo Yii::app()->baseUrl;?>/video/add_log";
	var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
	var movie_id = "<?php echo $_REQUEST['movie']?>";
	var episode_number = "<?php echo $episode_number?>";
	var content_type = "<?php echo $content_type?>";
	//video_link='http://www.w3schools.com/html/mov_bbb.mp4';
    $("#vid_more_info").hide();
	$("#episode_block").html("");
	$("#episode_block").hide();
    var main_video = videojs("video_block");
	//main_video.src(video_link);
	if(video_link.indexOf('http://youtu') > -1){
		main_video.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio" });
    }else{
      main_video.src({type: "video/mp4", src: video_link });
    }
	
   //main_video.src({type: "video/mp4", src: video_link });
    main_video.play();
	main_video.on("play", function(){
		$("#episode_block").hide();
	});
        main_video.on("loadeddata", function(){
		$(".vjs-loading-spinner").remove();
	});
	var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
	if(is_studio_admin != "true"){
		main_video.on("loadstart", function(){
			$.post(url,{movie_id:movie_id,episode_id:episode_id,status:"start"},function(res){});
		});
		main_video.on("ended", function(){
			$.post(url,{movie_id:movie_id,episode_id:episode_id,status:"complete"},function(res){});
			if(content_type != 'movie'){
				var next_epiurl = "<?php echo Yii::app()->baseUrl;?>/video/get_next_episode";
				$.post(next_epiurl,{movie_id:movie_id,episode_id:episode_id,episode_number:episode_number},function(epi_res){
					$("#episode_block").html(epi_res);
					$("#episode_block").show();
				});
			}
		});	
	}
  }


   function get_infoimg_height(percent){
    var info_height = $("#vid_more_info").css("height");
    var img_px = parseInt(info_height)*(percent/100);
    return img_px+"px";
  }

  function get_infoimg_width(percent){
    var info_width = $("#vid_more_info").css("width");
    var img_px = parseInt(info_width)*(percent/100);
    return img_px+"px";
  }
  function backto_info(){
    var trailer = videojs("trailer_block");
    trailer.pause();
    $("#trailer_div").hide();
    $("#vid_more_info").show();
  }

  function close_info(){
    $("#vid_more_info").hide();
    $("#wannasee_block").hide();
    //$(".random_msg").hide();
  }
  $('.vjs-loading-spinner').ready(function() { 
        bg = $(".vjs-loading-spinner").css('background-image');
        bg = bg.replace('url(','').replace(')','');
        $(".vjs-loading-spinner").css('background-image', 'url('+bg + "?" + Math.random()+')');
  });
</script>
