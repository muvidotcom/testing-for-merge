<?php

require 'facebook-php-sdk/src/Facebook/autoload.php';
require_once 'google-plus-sdk/Google/autoload.php';
use Aws\Ses\SesClient; 
class LoginController extends Controller
{
    public $layout = 'blank';
    
    function actionIndex(){

        $model=new LoginForm;

        if (Yii::app()->request->isAjaxRequest){ 
            if (isset($_POST['LoginForm'])) {  
                $model->attributes=$_POST['LoginForm'];

                if ($model->validate() && $model->login()) {
                    
                    //Wpuser Login
                    $username = $_POST['LoginForm']['email'];
                    $password = $_POST['LoginForm']['password'];
                    if (WP_USE_THEMES == 1) {
                    $wpuser = Yii::app()->common->getWpUserLogin($username,$password);
                    }
                    
                    if (Yii::app()->user->signup_step == 0) {
                     $list_of_verified_email = array();
                    Yii::app()->user->setState('is_email_verified',1);
                    
                    $users = User::model()->findAll(array("condition" => "email = '" . $_POST['LoginForm']['email']."'"));
                   
                    
                    User::model()->updateByPk($users[0]['id'], array(
                    'is_email_verified' => Yii::app()->session['is_email_verified'],
                    ));
                   
                    //check email id for multi studio
                    $multi = User::model()->findAll("email=:email AND is_active=:is_active AND role_id IN (:role1 ,:role2 ,:role3)", array(':email' => $_POST['LoginForm']['email'],':is_active' => 1,':role1' => 1,':role2' => 2,':role3' => 3));
					$is_multi_store_enabled = StudioConfig::model()->getConfig(@$multi[0]['studio_id'],'is_multi_store_enabled');

                    //echo count($multi);exit;
                    if(count($multi)<=1){
						$array = array("login" => "success");
                    }else{
                        $_SESSION['login_attempt_email_id'] = $_POST['LoginForm']['email'];
                        $array = array("login" => "multisuccess");
                    }
					if(@$is_multi_store_enabled['config_value']){
						$_SESSION['parentchield_studio'] = $multi[0]['studio_id'];
						$array = array("login" => "success");
					} 
                    //set cookie for zopim testing
                     
                    } else {
                    $array = array("login" => "step_" . Yii::app()->user->signup_step);
                    //Yii::app()->user->setFlash("success", "Successfully logged in.");
                    }
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                }
                else{
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
            }
            $this->render('index', array('model'=>$model));
        }else{
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
    }
    
    public function actionForgotpassword() {
        $this->render('forgot');
        //$this->redirect(Yii::app()->getBaseUrl(true));
    }    
    
    function actionStudioLogin(){
        $_REQUEST['studio_email'] = urldecode($_REQUEST['studio_email']);
        if (isset($_REQUEST['bill']) && trim($_REQUEST['bill']) != '') {//When customer clicks on billing link from email
            $uniqid = trim($_REQUEST['bill']);
            $billing_infos = BillingInfos::model()->findByAttributes(array('uniqid' => $uniqid, 'is_paid' => 0));
            if (isset($billing_infos) && !empty($billing_infos)) {
                $studio_id = $billing_infos->studio_id;
                $user_id = $billing_infos->user_id;
                $user = User::model()->findByAttributes(array('id' => $user_id, 'studio_id' => $studio_id));
                if (isset($user) && !empty($user)) {
                    $model = new LoginForm;
                    $model->attributes = array('email' => $user->email, 'password' => $user->encrypted_password, 'rememberMe' => 0);
                    if ($model->dostudiologin()) {
                        $this->redirect($this->createUrl('payment/payBill/bill/'.$uniqid));
                    }
                } else {
                    $this->redirect(Yii::app()->getBaseUrl(true));
                }
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
        } else if(isset($_REQUEST['studio_email']) && $_REQUEST['studio_email'] != '' && isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] > 0) {//Super admin wants to login from super admin end or deleted customer wants to reactivate his account, after reactivation, login automatically
            $email = $_REQUEST['studio_email'];
            $studio_id = $_REQUEST['studio_id'];            
            $_SESSION["login_studio_id"] = $studio_id;
            if($_REQUEST['multistore_email']!=''){
                $_SESSION['login_attempt_email_id']=$_REQUEST['multistore_email'];
            }
            if ($_REQUEST['parentchield_studio'] != '') {
                $_SESSION['parentchield_studio'] = $_REQUEST['parentchield_studio'];
            }
            $users = User::model()->findAllByAttributes(array('email' => $email, 'studio_id' => $studio_id));
            if(count($users) > 0) {
                foreach($users as $user) {  
                    $model = new LoginForm;
                    $model->attributes = array('email' => $email, 'password' => $user->encrypted_password, 'rememberMe' => 0, 'studio_id' => $studio_id);
                    if ($model->dostudiologin()) {
                        $is_multi_store_enabled = StudioConfig::model()->getConfig($studio_id,'is_multi_store_enabled');
                        if($is_multi_store_enabled['config_value']){
                            $_SESSION['parentchield_studio'] = $studio_id;
                        }
                        $this->redirect($this->createUrl('admin/dashboard'));
                    }                        
                }
            }  
        }
    }
    
    function actionPartnerLogin(){
        if (isset($_POST['LoginForm']) && !empty($_POST['LoginForm'])) {
            $model = new LoginForm;
            $model->attributes = $_POST['LoginForm'];
            if ($model->loginpartner()){
                $array = array("login" => "success");
                $json = json_encode($array);
                echo $json;
                Yii::app()->end();
            } else {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } else {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }    

    function actionResellerLogin() {
		if(isset($_POST['reseller_login_superadmin']) && $_POST['reseller_login_superadmin'] == 'yes' && isset($_POST['portaluser_id']) && intval($_POST['portaluser_id']) && isset($_POST['portalusr_email']) && trim($_POST['portalusr_email'])){
            $email = $_POST['portalusr_email'];
            $user_id = $_POST['portaluser_id'];
            $users = PortalUser::model()->findByAttributes(array('email' => $email, 'id' => $user_id)); 
            
            $model = new LoginForm;
            $model->attributes = array('email' => $email, 'password' => $users->encrypted_password, 'rememberMe' => 0);
			if ($model->loginresellerfromadmin()) {
				$this->redirect($this->createUrl('partner/reseller'));
			} else {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
        } else if (isset($_POST['LoginForm']) && !empty($_POST['LoginForm'])) {
            $model = new LoginForm;
            $model->attributes = $_POST['LoginForm'];
            if ($model->loginreseller()) {
                    $array = array("login" => "success");
                    $json = json_encode($array);
                    echo $json;
                    Yii::app()->end();
                } else {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
        } else {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    function actionforgot() {
        $email = $_REQUEST["email"];
        $studio = $this->studio;
        $users = User::model()->findAllByAttributes(array('email' => $email));
        if($users){
            foreach($users as $user)
            {
                    $user_id = $user->id;
                    $reset_tok = md5(uniqid(mt_rand(), true));
                    $user->reset_password_token = $reset_tok;
                    $user->reset_password_sent_at = new CDbExpression("NOW()");
                    $user->update();
            }
            //issue id -5474 ajit@muvi.com
            //mail is seperated from the loop
            $to =array($email);                          
            $to_name = $user->first_name." ".$user->last_name;
                    $from = 'info@muvi.com';
                    $from_name = "Muvi";
                    $site_url = Yii::app()->getBaseUrl(true);

                    $logo = EMAIL_LOGO;
                    $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="studio" /></a>';

                    $reset_link = $site_url . '/login/resetpassword?user_id=' . $user_id . '&auth=' . $reset_tok;
                    $reset_link = $site_url . '/login/resetpassword/user/' . $reset_tok;
                    $rlink = '<a href="' . $reset_link . '">' . $reset_link . '</a>';

                    $params = array(
                        'website_name' > $studio->name,
                        'logo' => $logo,
                        'reset_link' => $rlink,
                        'username' => $to_name
                    );

                    $subject = 'Reset password for your Studio account';
                    $message = array('subject' => $subject,
                        'from_email' => $from,
                        'from_name' => $from_name,
                        'to' => array(
                            array(
                                'email' => $to,
                                'name' => $to_name,
                                'type' => 'to'
                            )
                        )
                    );
                    $template_name = 'studio_forgot_password';
                    // $this->mandrilEmail($template_name, $params, $message);   
                    Yii::app()->theme = 'bootstrap';
                    $thtml = Yii::app()->controller->renderPartial('//email/studio_forgot_password', array('params' => $params), true);
                    $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', 'Muvi');



                    $response = 'success';
                    $response_message = 'Please check your email to reset your password.';
                } else {
                    $response = 'failure';
                    $response_message = 'Email you have entered is not present.';
                }
        $ret = array('status' => $response, 'message' => $response_message);
        echo json_encode($ret);
        exit;
    }

    function actionResetPassword(){
        if(Yii::app()->theme->name=='bootstrap'){
            $this->layout='main';
        }else{
            $this->layout='partners'; 
            Yii::app()->theme = 'admin';
        }
        
        if (isset($_REQUEST['user']) && trim($_REQUEST['user'])) {
            //$sql = "SELECT * FROM user where reset_password_token = '".trim($_REQUEST['user'])."'";
            $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('user ')
                    ->where('reset_password_token =:reset_password_token', array(':reset_password_token' => trim($_REQUEST['user'])))
                    ->queryRow();
              
          if (isset($data) && !empty($data)) {
                $this->render('reset_password',array('data'=>$data));
            } else {
                $this->redirect($this->createUrl('login/set_password'));
                echo '<h3 style="line-height: 30px;text-align: center;">Reset link expired! Please reset password again to get a fresh password</h3>';
                
            }
        } else {
               $this->redirect($this->createUrl('login/set_password'));
               echo '<h3 style="line-height: 30px;text-align: center;">Reset link expired! Please reset password again to get a fresh password</h3>';
            
        }
    }    

    function actionresetresellerpwd(){
        $this->layout='partner';
        $sql = "SELECT * FROM portal_user where id = '".$_REQUEST['user_id']."'";
        $conn = Yii::app()->db;
        $data = $conn->createCommand($sql)->queryAll();
        if($data[0]['token'] == $_REQUEST["auth"]){
            $this->render('reset_password');
        }else{
            
        }
    }
  function actionset_password() {
        $this->layout = false;
        $response = 'error';
        $response_message = '<h3 style="line-height: 30px;text-align: center;">Reset link expired! Please reset password again to get a fresh password.</h3>';
        if (isset($_REQUEST['token']) && trim($_REQUEST['token'])) {
            $enc = new bCrypt();
            $token = $_REQUEST['token'];
            $user = User::model()->findByAttributes(array('reset_password_token' => $token));
            $email = $user->email;
            $encrypted_pass = $enc->hash($_REQUEST["new_password"]);
            $user->encrypted_password = $encrypted_pass;
            $user->reset_password_token = '';
            //$resp = $user->save();
            //reset password of all records of this email id
            //issue id -5474 ajit@muvi.com
            $sql = "update user set encrypted_password='" . $encrypted_pass . "',reset_password_token='' where email='" . $email . "'";
            $resp = Yii::app()->db->createCommand($sql)->execute();

            $sdk_users = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('sdk_users')
                    ->where('email=:email and studio_id=:studio_id', array(':email' => $user['email'], ':studio_id' => $user['studio_id']))
                    ->order('id desc')
                    ->queryAll();

            if (!empty($sdk_users)) {
                foreach ($sdk_users as $usr) {
                    $user_cls = new SdkUser;
                    $user_cls = $user_cls->findByPk($usr['id']);
                    $user_cls->encrypted_password = $user['encrypted_password'];
                    $user_cls->save();
                }
            }
            if ($resp) {
                $identity = new UserIdentity($email, $password);
                $identity->authenticatestudio();
                Yii::app()->user->login($identity);
                $response = 'success';
                $response_message = 'Your password has been reset. Please login using new password.';
            }
            $ret = array('status' => $response, 'message' => $response_message);
            echo json_encode($ret);
        } else {
            echo $response_message;
            exit;
        }
    }

    function actionsetpartner_password(){
        $studio_id = $this->studio->id;        
        $enc = new bCrypt();
        if (@$_POST['user_id']) {
            $user_id = $_POST['user_id'];
            $user = User::model()->findByPK($user_id);
            $email = $user->email;
            $encrypted_pass = $enc->hash($_REQUEST["new_password"]);
            $user->encrypted_password = $encrypted_pass;
            $resp = $user->update();   
            if($resp){
                echo "success";
            }else{
                echo "failure";
            }
        }else{
            echo "failure";
        }
    }
    function actionsetreseller_password(){   
        $enc = new bCrypt();
        if (@$_POST['user_id']) {
            $user_id = $_POST['user_id'];
            $user = PortalUser::model()->findByPK($user_id);
            $email = $user->email;
            $encrypted_pass = $enc->hash($_REQUEST["new_password"]);
            $user->encrypted_password = $encrypted_pass;
            $resp = $user->update();   
            if($resp){
                echo "success";
            }else{
                echo "failure";
            }
        }else{
            echo "failure";
        }
    }
/**
 * @method public facebookAuth() Authenticate Facebook Login
 * @author GDR<support@muvi.com>
 * @return HTML If success then go to user homepage else login page 
 * @lastupdate: By RKS for redirection after login
 */
    function actionFacebookAuth() {
        $session = new CHttpSession;
        $session->open();
        foreach ($_COOKIE as $k => $v) {
            if (strpos($k, "FBRLH_") !== FALSE) {
                $_SESSION[$k] = $v;
            }
        }
        $get_allcontents = Yii::app()->general->gethomepageList();
        $socialAuth = Yii::app()->common->getSocialAuths('');
        $redirectUrl = '';
        $fb = new Facebook\Facebook([
            'app_id' => $socialAuth['fb_app_id'],
            'app_secret' => $socialAuth['fb_secret'],
            'default_access_token' => isset($_SESSION['facebook_access_token']) ? $_SESSION['facebook_access_token'] : $socialAuth['fb_app_id']
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $studio_id = Yii::app()->common->getStudioId();
        try {
            $accessToken = $helper->getAccessToken($redirectUrl);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $this->clearFacebookCookie();
            // When Graph returns an error
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/social_error.log', "a+");
            fwrite($fp, "\n\n\t Date:- " . date('dS M Y h:i A') . " \t Graph returned an error: " . $e->getMessage() . ". \t Studio ID:-" . $studio_id . " \t FB_APP_ID:-" . $socialAuth['fb_app_id'] . " \t FB_SECRET:-" . $socialAuth['fb_secret']);
            fclose($fp);
            Yii::app()->user->setFlash('error', 'Error in Facebook Login. Please try login with your credentials');
            $this->redirect($this->createUrl('user/login'));
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $this->clearFacebookCookie();
            // When validation fails or other local issues
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/social_error.log', "a+");
            fwrite($fp, "\n\n\t Date:- " . date('dS M Y h:i A') . " \t Facebook SDK returned an error: " . $e->getMessage() . ". \t Studio ID:-" . $studio_id . " \t FB_APP_ID:-" . $socialAuth['fb_app_id'] . " \t FB_SECRET:-" . $socialAuth['fb_secret']);
            fclose($fp);
            Yii::app()->user->setFlash('error', 'Error in Facebook Login. Please try login with your credentials');
            $this->redirect($this->createUrl('user/login'));
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                $this->clearFacebookCookie();
                // When validation fails or other local issues
                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/social_error.log', "a+");
                fwrite($fp, "\n\n\t Date:- " . date('dS M Y h:i A') . " \t Error Description: " . $helper->getErrorDescription() . ". \t Studio ID:-" . $studio_id . " \t FB_APP_ID:-" . $socialAuth['fb_app_id'] . " \t FB_SECRET:-" . $socialAuth['fb_secret']);
                fclose($fp);
                Yii::app()->user->setFlash('error', $helper->getErrorDescription());
                $this->redirect($this->createUrl('user/login'));
                exit;
            } else {
                $this->clearFacebookCookie();
                Yii::app()->user->setFlash('error', 'Error in facebook login');
                $this->redirect($this->createUrl('user/login'));
                exit;
            }
        }
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        if (@$_COOKIE['movie_id'] && $_COOKIE['is_popup'] == "1") {
            $movie_id = $_COOKIE['movie_id'];
        } else {
            $movie_id = '0';
        }
        $is_ppv = $_COOKIE['is_ppv'];
		$current_url = $_COOKIE['current_url'];
        $aToken = $accessToken->getValue();
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        $fb->setDefaultAccessToken($accessToken->getValue());

        $response = $fb->get('/me?fields=name,email,gender,first_name');
        $userNode = $response->getGraphUser();
        $email = $userNode->getField('email');
        $name = $userNode->getField('name');
        $userid = $userNode->getField('id');
        $first_name = $userNode->getField('first_name');
        $sql = "SELECT permalink FROM films WHERE studio_id='" . $studio_id . "' AND uniq_id ='" . $movie_id . "'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $permalink = $data['permalink'];
        if (trim($email) == '' || is_null($email)) {
            $email = strtolower($first_name) . '@facebook.com';
        }
        $userData = SdkUser::model()->find('(email=:email OR fb_userid=:fb_userid) AND studio_id=:studio_id', array(':email' => $email, ':fb_userid' => $userid, ':studio_id' => $studio_id));
        if ($userData && ($userData->is_deleted || !$userData->status)) {
            Yii::app()->user->setFlash('error', 'Your account has been suspended. Please contact Studio Admin to login');
            $this->redirect($this->createUrl('user/login'));
            exit;
        } elseif ($userData) {
            if ($userData['fb_userid'] != $userid) {
                $userData->fb_userid = $userid;
                $userData->save();
            }
            $model = new LoginForm;
            $model->LoginWithSocial($userData, 'facebook');

            //Added By SNL: Check plan and payment gateway are exists or not
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            $activate = 0; //No plan and payment gateway set by studio
            $action = '';

            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $activate = 1;
                $subscription = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                //Check it will go reactivation page or acivation page

                if (isset($userData) && !empty($userData) && intval($userData->is_deleted) && intval($activate)) {
                    if ($subscription == '') {
                        $action = 'reactivate';
                    }
                } else if (intval($activate)) {
                    if ($subscription == '') {
                        $action = 'activate';
                    }
                }
            }
            if ($action != '' && $movie_id == '0') {
                if ($get_allcontents['value'] == 1) {
                    $url = $get_allcontents['url'];
                } else if ($is_ppv == 0) {
                    $url = Yii::app()->getBaseUrl(TRUE) . '/user/' . $action;
                }
                else{
                    $url = $current_url;
                }
            } else {
                if (@$_COOKIE['videoUrl']) {
                    $videoUrl = $_COOKIE['videoUrl'];
                    setcookie('videoUrl', '', time() - 36000);
                    $url = $videoUrl;
                } else {
                    if ($is_ppv == 0 && $_COOKIE['is_popup'] == "1") {
                        $url = Yii::app()->getBaseUrl(TRUE) . '/player/' . $permalink;
                    } else {
                        if ($movie_id != '0') {
                            $url = $current_url;
                        } else {
                            if ($get_allcontents['value'] == 1) {
                                $url = $get_allcontents['url'];
                            } else {
                                $url = Yii::app()->getBaseUrl(TRUE);
                            }
                        }
                    }
                }
            }
            echo "<script>window.close();window.opener.location.href='" . $url . "';</script>";
            exit;
        } else {
            $data = array('name' => $name, 'email' => $email, 'fb_userid' => $userid, 'gender' => $userNode->getField('gender'), 'studio_id' => $studio_id, 'fb_access_token' => $aToken);
            $this->RegisterAndLoginWithSocial($data, 'facebook', $movie_id, $is_ppv, $current_url);
            $this->redirect();
            exit;
        }
    }

/**
 * @method public RegisterAndLoginWithFacebook($data) Register and Login using facebook details
 * @author Gayadhar<support@muvi.com>
 * @return bool true/false
 * @lastupdate: By RKS for redirection after register
 */
    function RegisterAndLoginWithSocial($input, $type = 'facebook', $movie_id, $is_ppv, $current_url) {
        foreach ($input AS $key => $val) {
            $data['data'][$key] = $val;
        }
        $data['data']['password'] = '';

        $data['PAYMENT_GATEWAY'] = $this->PAYMENT_GATEWAY;
        $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID;
        $get_allcontents = Yii::app()->general->gethomepageList();
        $url = $get_allcontents['url'];

        $ret = SdkUser::model()->saveSdkUser($data, $data['studio_id']);
        if ($ret) {
            if (@$ret['error']) {
                Yii::app()->user->setFlash('error', $ret['error']);
                $this->redirect($this->createUrl('register'));
                exit;
            }
            // Send welcome email to user
            if (@$arr['is_subscribed']) {
                $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $arr['user_id'], 'welcome_email_with_subscription', '', '', '', '', '', '', $this->language_code);
            } else {
                $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $arr['user_id'], 'welcome_email', '', '', '', '', '', '', $this->language_code);
            }
            //Validate user identity
            $identity = new UserIdentity($input['email'], '');
            if ($type == 'gplus') {
                $identity->loginwithSocialData($input['studio_id'], $input['gplus_userid'], $input['email'], $type);
            } else {
                $identity->loginwithSocialData($input['studio_id'], $input['fb_userid'], $input['email'], $type);
            }
            Yii::app()->user->login($identity);
            $studio_id = Yii::app()->common->getStudioId();
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            $activate = 0; //No plan and payment gateway set by studio
            $action = '';

            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $activate = 1;
                $subscription = Yii::app()->common->isSubscribed(Yii::app()->user->id);
                //Check it will go reactivation page or acivation page

                if (isset($userData) && !empty($userData) && intval($userData->is_deleted) && intval($activate)) {
                    if ($subscription == '') {
                        $action = 'reactivate';
                    }
                } else if (intval($activate)) {
                    if ($subscription == '') {
                        $action = 'activate';
                    }
                }
            }
            if ($action != '' && $movie_id == '0') {
                if ($get_allcontents['value'] == 1) {
                    $url = $get_allcontents['url'];
                } else {
                    $url = Yii::app()->getBaseUrl(TRUE) . '/user/' . $action;
                }
            } else {
                if (@$_COOKIE['videoUrl']) {
                    $videoUrl = $_COOKIE['videoUrl'];
                    setcookie('videoUrl', '', time() - 36000);
                    $url = $videoUrl;
                } else {
                    if ($is_ppv == 0 && $_COOKIE['is_popup'] == "1") {
                        $url = Yii::app()->getBaseUrl(TRUE) . '/player/' . $permalink;
                    } else {
                        if ($movie_id != '0') {
                            $url = $current_url;
                        } else {
                            if ($get_allcontents['value'] == 1) {
                                $url = $get_allcontents['url'];
                            } else {
                                $url = Yii::app()->getBaseUrl(TRUE);
                            }
                        }
                    }
                }
            }
			if($type == 'facebook'){
				echo "<script>window.close();window.opener.location.href='" . $url . "';</script>";
				exit;
			}
		}
    }

/**
 * @method public googleplusAuth() Authenticate Google Plus Login
 * @author GDR<support@muvi.com>
 * @return HTML If success then go to user homepage else login page 
 */
	function actionGoogleplusAuth(){
		$session=new CHttpSession;
        $session->open();
		$socialAuth = Yii::app()->common->getSocialAuths('');
		if($socialAuth && $socialAuth['gplus_client_id']){
			$redirect_uri = Yii::app()->getBaseUrl(TRUE).'/login/googleplusAuth/';
			try{
				$client = new Google_Client();
				$client->setClientId($socialAuth['gplus_client_id']);
				$client->setClientSecret($socialAuth['gplus_client_secret']);
				$client->setRedirectUri($redirect_uri);
				if (isset($_GET['code'])) {
					$client->authenticate($_GET['code']);
					$_SESSION['access_token'] = $client->getAccessToken();
				}
				if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
					$client->setAccessToken($_SESSION['access_token']);
				}
				$service = new Google_Service_Oauth2($client);
				$user = $service->userinfo->get();
			}catch (Exception $e){
				$fp = fopen($_SERVER['DOCUMENT_ROOT']."/".SUB_FOLDER.'protected/runtime/social_error.log', "a+");
				fwrite($fp,"\n\n\t Date:- ".date('dS M Y h:i A')." \t Google API error: ".$e->getMessage().". \t Studio ID:-".$studio_id." \t FB_APP_ID:-".$socialAuth['fb_client_id']." \t FB_SECRET:-".$socialAuth['fb_client_secret']);
				fclose($fp);
				Yii::app()->user->setFlash('error','Error in Google Plus Login. Please try login with your credentials');
				$this->redirect($this->createUrl('user/login'));
			}
			$studio_id = Yii::app()->common->getStudioId();
			if(isset($user) && $user){
				$email = $user->email;
				$name = $user->name;
				$gplus_id = $user->id;
				$userData = SdkUser::model()->find('email=:email AND studio_id=:studio_id',array(':email'=>$email,':studio_id'=>$studio_id));
				if($userData){
					if($userData['gplus_userid'] != $gplus_id){
						$userData->gplus_userid = $gplus_id;
						$userData->save();
					}
					$model = new LoginForm;
					$model->LoginWithSocial($userData,'gplus');

					//Added By SNL: Check plan and payment gateway are exists or not
					$plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
					$activate = 0; //No plan and payment gateway set by studio
					$action = '';

					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$activate = 1;
						$subscription = Yii::app()->common->isSubscribed(Yii::app()->user->id);
						//Check it will go reactivation page or acivation page

						if (isset($userData) && !empty($userData) && intval($userData->is_deleted) && intval($activate)) {
							if ($subscription == '') {
								$action = 'reactivate';
							}
						} else if (intval($activate)) {
							if ($subscription == '') {
								$action = 'activate';
							}
						}
					}
					if($action){
						$url = Yii::app()->getBaseUrl(TRUE).'/user/'.$action;
					}else{
						if(@$_COOKIE['videoUrl']){
							$videoUrl = $_COOKIE['videoUrl'];
							setcookie('videoUrl','',time()-36000);
							$url = $videoUrl;
						}else{
							$url = Yii::app()->getbaseUrl(true);
						}
					}
					$this->redirect($url);exit;
			  }else{
				  $data = array('name'=> $name,'email'=>$email,'gplus_userid'=>$gplus_id,'studio_id'=>$studio_id);
				  $this->RegisterAndLoginWithSocial($data,'gplus');
				  $this->redirect();exit;
				}		
			}else{
				Yii::app()->user->setFlash('error','We are not able to fetch data with this account. Please try later');
				$this->redirect($this->createUrl('user/login'));exit;	
			}
		}else{
			Yii::app()->user->setFlash('error','Google Plus account not set properly');
			$this->redirect($this->createUrl('user/login'));exit;	
		}
	}
        
        function actionForgotPartners(){
        $email = $_REQUEST["email"];
        $studio = $this->studio;
        $user = User::model()->findByAttributes(array('email' => $email,'studio_id' => $studio->id,'role_id' =>4));
       
        if($user){
            $customer = User::model()->findByAttributes(array('studio_id' => $studio->id,'role_id' =>1));
            
                $user_id = $user->id;
                $reset_tok = md5(uniqid(mt_rand(), true));
                $user->reset_password_token = $reset_tok;
                $user->reset_password_sent_at = new CDbExpression("NOW()");
                $user->update();
                
                $to = $email;                          
                $to_name = $user->first_name." ".$user->last_name;
            $from = $customer->email;
            $from_name = $studio->name;
                $site_url = Yii::app()->getBaseUrl(true);

            $logo = $this->siteLogo;
            $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="'.ucfirst($studio->name).'" style="height: 50px;" /></a>';

                $reset_link = $site_url.'/login/resetpassword?user_id='.$user_id.'&auth='.$reset_tok;
                $reset_link = $site_url.'/login/resetpassword/user/'.$reset_tok;
                $rlink = '<a href="'.$reset_link.'">'.$reset_link.'</a>';

                $params = array(
                'website_name' => $studio->name,
                'logo'=> $logo,
                'reset_link'=> $rlink,
                'username'=> $to_name,
                'customer_email'=> $from
                );

            $subject = 'Reset password for your account';
                $message = array('subject' => $subject,
                    'from_email' => $from,
                    'from_name' => $from_name,
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $to_name,
                            'type' => 'to'
                        )	
                    )
                );
                $template_name= 'studio_partners_forgot_password';
            // $this->mandrilEmail($template_name, $params, $message);   
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/studio_partners_forgot_password',array('params'=>$params),true);
            $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',$from_name); 
   
                $response = 'success';
            $response_message = '<span style="color:green;"><b>Please check your email to reset your password.</b><span>';
        }else{
            $response = 'failure';
            $response_message = 'Email you have entered is not present.';
        }
        $ret = array('status' => $response, 'message' => $response_message);
        echo json_encode($ret);
        exit;
    }
    /* Forgot Password fro Reseller account */
    function actionForgotReseller() {
        $email = $_REQUEST["email"];
        $user = PortalUser::model()->findByAttributes(array('email' => $email));
        if ($user) {
            $user_id = $user->id;
            $reset_tok = md5(uniqid(mt_rand(), true));
            $user->token = $reset_tok;
            $user->update();

            $to = array($email);
            $to_name = $user->name;
            $from = 'info@muvi.com';
            $from_name = "Muvi Studio";
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = EMAIL_PARTNER_LOGO;
            $logo = '<a href="' . $site_url . '"><img src="' . $logo . '" alt="studio" /></a>';

            $reset_link = $site_url . '/login/resetresellerpwd?user_id=' . $user_id . '&auth=' . $reset_tok;
            //echo $reset_link;
            $rlink = '<a href="' . $reset_link . '">' . $reset_link . '</a>';

            $params = array(
                'website_name' => "Muvi Partner",
                'logo'=> $logo,
                'reset_link'=> $rlink,
                'username'=> $to_name,
                'customer_email'=> $from
            );
            $subject = 'Reset password for your Studio account';
            $message = array('subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => array(
                    array(
                        'email' => $to,
                        'name' => $to_name,
                        'type' => 'to'
                    )
                )
            );
            $template_name = 'studio_forgot_password';
            // $this->mandrilEmail($template_name, $params, $message);   
            Yii::app()->theme = 'bootstrap';
            $thtml = Yii::app()->controller->renderPartial('//email/studio_partners_forgot_password', array('params' => $params), true);
            $returnVal = $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $from_name);
            $response = 'success';
            $response_message = '<span style="color:green;"><b>Please check your email to reset your password.</b><span>';
        } else {
            $response = 'failure';
            $response_message = 'Email you have entered is not present.';
        }
        $ret = array('status' => $response, 'message' => $response_message);
        echo json_encode($ret);
        exit;
    }

}