<?php
class PaypalExpress
{
    private $_api_username;
    private $_api_password;
    private $_api_signature;
    private $_api_endpoint;
    private $_api_url;
    private $_api_version;    
    private $_env_type;
    private $_sbn_code;

    public function __construct($api_username, $api_pasword, $api_signature, $api_version = '64', $env_type = 'sandbox')
    {

        $this->_api_username = $api_username;
        $this->_api_password = $api_pasword;
        $this->_api_signature = $api_signature;
        $this->_api_version = $api_version;
        $this->_sbn_code = 'PP-ECWizard';
        if ($env_type === 'sandbox')
        {
            $this->_api_endpoint = "https://api-3t.sandbox.paypal.com/nvp";
            $this->_api_url = "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=";
        }
        else
        {
            $this->_api_endpoint = "https://api-3t.paypal.com/nvp";
            $this->_api_url = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";            
        }
    }  
    
    public function CallShortcutExpressCheckout( $paymentAmount, $email, $currencyCodeType, $paymentType, $paymentDesc, $returnURL, $cancelURL, $landing = 0, $address = array()) 
    {
            //------------------------------------------------------------------------------------------------------------------------------------
            // Construct the parameter string that describes the SetExpressCheckout API call in the shortcut implementation

            $nvpstr="&AMT=". $paymentAmount;
            $nvpstr = $nvpstr . "&PAYMENTACTION=" . $paymentType;
            $nvpstr = $nvpstr . "&BILLINGAGREEMENTDESCRIPTION=".urlencode($paymentDesc);
            $nvpstr = $nvpstr . "&BILLINGTYPE=RecurringPayments";
            $nvpstr = $nvpstr . "&RETURNURL=" . $returnURL;
            $nvpstr = $nvpstr . "&CANCELURL=" . $cancelURL;
            $nvpstr = $nvpstr . "&EMAIL=" . $email;

            if($landing == 0)
            {
                $name = isset($address['name'])?$address['name']:'';
                $street = isset($address['street'])?$address['street']:'';
                $city = isset($address['city'])?$address['city']:'';
                $state = isset($address['state'])?$address['state']:'';
                $country = isset($address['country'])?$address['country']:'';
                $zip = isset($address['zip'])?$address['zip']:'';

                $nvpstr = $nvpstr . "&LANDINGPAGE=Billing";
                $nvpstr = $nvpstr . "&SHIPTONAME=".  urldecode($name);
                $nvpstr = $nvpstr . "&SHIPTOSTREET=".  urldecode($street);
                $nvpstr = $nvpstr . "&SHIPTOCITY=".  urldecode($city);
                $nvpstr = $nvpstr . "&SHIPTOSTATE=".  urldecode($state);
                $nvpstr = $nvpstr . "&SHIPTOCOUNTRY=".  urldecode($country);
                $nvpstr = $nvpstr . "&SHIPTOZIP=".  urldecode($zip);
            }
            else
            {
                $nvpstr = $nvpstr . "&LANDINGPAGE=Login";
            }
            
            $nvpstr .= "&CUSTOM=21";

            $_SESSION["currencyCodeType"] = $currencyCodeType;	  
            $_SESSION["PaymentType"] = $paymentType;

            //'--------------------------------------------------------------------------------------------------------------- 
            //' Make the API call to PayPal
            //' If the API call succeded, then redirect the buyer to PayPal to begin to authorize payment.  
            //' If an error occured, show the resulting errors
            //'---------------------------------------------------------------------------------------------------------------

            //print_r($nvpstr);
            //exit();

            $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
            //print_r($resArray);
            $ack = strtoupper($resArray["ACK"]);
            if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
            {
                    $token = urldecode($resArray["TOKEN"]);
                    $_SESSION['TOKEN']=$token;
            }

        return $resArray;
    }            
    
    public function CallMarkExpressCheckout( $paymentAmount, $currencyCodeType, $paymentType, $returnURL, $cancelURL, $shipToName, $shipToStreet, $shipToCity, $shipToState, $shipToCountryCode, $shipToZip, $shipToStreet2, $phoneNum ) 
    {
        //------------------------------------------------------------------------------------------------------------------------------------
        // Construct the parameter string that describes the SetExpressCheckout API call in the shortcut implementation

        $nvpstr="&PAYMENTREQUEST_0_AMT=". $paymentAmount;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $paymentType;
        $nvpstr = $nvpstr . "&RETURNURL=" . $returnURL;
        $nvpstr = $nvpstr . "&CANCELURL=" . $cancelURL;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $currencyCodeType;
        $nvpstr = $nvpstr . "&ADDROVERRIDE=1";
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTONAME=" . $shipToName;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET=" . $shipToStreet;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET2=" . $shipToStreet2;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCITY=" . $shipToCity;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTATE=" . $shipToState;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=" . $shipToCountryCode;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOZIP=" . $shipToZip;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOPHONENUM=" . $phoneNum;

        $_SESSION["currencyCodeType"] = $currencyCodeType;	  
        $_SESSION["PaymentType"] = $paymentType;

        //'--------------------------------------------------------------------------------------------------------------- 
        //' Make the API call to PayPal
        //' If the API call succeded, then redirect the buyer to PayPal to begin to authorize payment.  
        //' If an error occured, show the resulting errors
        //'---------------------------------------------------------------------------------------------------------------
        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
        {
                $token = urldecode($resArray["TOKEN"]);
                $_SESSION['TOKEN']=$token;
        }
		   
        return $resArray;
    }
	
    public function GetShippingDetails( $token )
    {
        $nvpstr="&TOKEN=" . $token;
        $resArray = $this->hash_call("GetExpressCheckoutDetails", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);

        if($ack == "SUCCESS" || $ack=="SUCCESSWITHWARNING")
        {	
                /*$_SESSION['payer_id'] =	$resArray['PAYERID'];
                $_SESSION['email'] =	$resArray['EMAIL'];
                $_SESSION['firstName'] = $resArray["FIRSTNAME"]; 
                $_SESSION['lastName'] = $resArray["LASTNAME"]; 
                $_SESSION['shipToName'] = $resArray["SHIPTONAME"]; 
                $_SESSION['shipToStreet'] = $resArray["SHIPTOSTREET"]; 
                $_SESSION['shipToCity'] = $resArray["SHIPTOCITY"];
                $_SESSION['shipToState'] = $resArray["SHIPTOSTATE"];
                $_SESSION['shipToZip'] = $resArray["SHIPTOZIP"];
                $_SESSION['shipToCountry'] = $resArray["SHIPTOCOUNTRYCODE"];*/
        } 
        return $resArray;
    }
	
    public function ConfirmPayment( $FinalPaymentAmt, $token, $paymentType, $currencyCodeType, $payerID )
    {
        $serverName = urlencode($_SERVER['SERVER_NAME']);

        $nvpstr  = '&TOKEN=' . $token . '&PAYERID=' . $payerID . '&PAYMENTACTION=' . $paymentType . '&AMT=' . $FinalPaymentAmt;
        $nvpstr .= '&CURRENCYCODE=' . $currencyCodeType . '&IPADDRESS=' . $serverName; 
        $resArray = $this->hash_call("DoExpressCheckoutPayment", $nvpstr);
        return $resArray;
    }
	
    public function CreateRecurringPaymentsProfile($token, $email, $plan_desc, $period, $frequency, $amount, $currency, $trialperiod = '', $trialfrequency = '', $trialtotalcysles = '', $trial_amount = '')
    {
        $nvpstr="&TOKEN=".$token;
        $nvpstr.="&EMAIL=".$email;
        $nvpstr.="&PROFILESTARTDATE=".gmdate('Y-m-d\TH:i:s\Z');
        $nvpstr.="&DESC=".urlencode($plan_desc);
        $nvpstr.="&BILLINGPERIOD=".$period;
        $nvpstr.="&BILLINGFREQUENCY=".$frequency;
        $nvpstr.="&AUTOBILLOUTAMT=AddToNextBilling";
        $nvpstr.="&AMT=".$amount;

        if($trialperiod != '')
        {
            $nvpstr.="&TRIALBILLINGPERIOD=".$trialperiod;
            $nvpstr.="&TRIALBILLINGFREQUENCY=".$trialfrequency;
            $nvpstr.="&TRIALTOTALBILLINGCYCLES=".$trialtotalcysles;
            $nvpstr.="&TRIALAMT=".$trial_amount;  
        }

        $nvpstr.="&CURRENCYCODE=".$currency;
        $nvpstr.="&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        //'---------------------------------------------------------------------------
        //' Make the API call and store the results in an array.  
        //'	If the call was a success, show the authorization details, and provide
        //' 	an action to complete the payment.  
        //'	If failed, show the error
        //'---------------------------------------------------------------------------
        $resArray = $this->hash_call("CreateRecurringPaymentsProfile", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        return $resArray;
    }
    
    public function manageProfileStatus($profile_id, $action, $note = '')
    {
        $request_param = '&PROFILEID=' . urlencode($profile_id) . '&ACTION=' . $action;
        if ($note)
            $request_param.='&NOTE=' . urlencode($note);
        $result        = $this->hash_call("ManageRecurringPaymentsProfileStatus", $request_param);
        return $result;
    }   
    
    public function getProfileDetails($profile_id)
    {

        $request_param = '&PROFILEID=' . urlencode($profile_id);
        $result        = $this->hash_call('GetRecurringPaymentsProfileDetails', $request_param);
        return $result;
    }    

    public function DirectPayment( $paymentType, $paymentAmount, $creditCardType, $creditCardNumber, $expDate, $cvv2, $firstName, $lastName, $street, $city, $state, $zip, $countryCode, $currencyCode )
    {
        //Construct the parameter string that describes DoDirectPayment
        $nvpstr = "&AMT=" . $paymentAmount;
        $nvpstr = $nvpstr . "&CURRENCYCODE=" . $currencyCode;
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $paymentType;
        $nvpstr = $nvpstr . "&CREDITCARDTYPE=" . $creditCardType;
        $nvpstr = $nvpstr . "&ACCT=" . $creditCardNumber;
        $nvpstr = $nvpstr . "&EXPDATE=" . $expDate;
        $nvpstr = $nvpstr . "&CVV2=" . $cvv2;
        $nvpstr = $nvpstr . "&FIRSTNAME=" . $firstName;
        $nvpstr = $nvpstr . "&LASTNAME=" . $lastName;
        $nvpstr = $nvpstr . "&STREET=" . $street;
        $nvpstr = $nvpstr . "&CITY=" . $city;
        $nvpstr = $nvpstr . "&STATE=" . $state;
        $nvpstr = $nvpstr . "&COUNTRYCODE=" . $countryCode;
        $nvpstr = $nvpstr . "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        $resArray = $this->hash_call("DoDirectPayment", $nvpstr);

        return $resArray;
    }

    public function hash_call($methodName,$nvpStr)
    {
            //declaring of global variables
            $API_Endpoint = $this->_api_endpoint;
            $version = $this->_api_version;
            $API_UserName = $this->_api_username;
            $API_Password = $this->_api_password;
            $API_Signature = $this->_api_signature;
            $sBNCode = $this->_sbn_code;            

            //setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);

            //turning off the server and peer verification(TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST, 1);

        //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
       //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
            //if($USE_PROXY)
            //    curl_setopt ($ch, CURLOPT_PROXY, $PROXY_HOST. ":" . $PROXY_PORT);                

            //NVPRequest for submitting to server
            $nvpreq="METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($version) . "&PWD=" . urlencode($API_Password) . "&USER=" . urlencode($API_UserName) . "&SIGNATURE=" . urlencode($API_Signature) . $nvpStr . "&BUTTONSOURCE=" . urlencode($sBNCode);
            //exit();

            //var_dump($nvpreq);
            //setting the nvpreq as POST FIELD to curl
            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

            //getting response from server
            $response = curl_exec($ch);

            //convrting NVPResponse to an Associative Array
            $nvpResArray = $this->deformatNVP($response);
            $nvpReqArray = $this->deformatNVP($nvpreq);
            $_SESSION['nvpReqArray']=$nvpReqArray;

            if (curl_errno($ch)) 
            {
                    // moving to display page to display curl errors
                      $_SESSION['curl_error_no']=curl_errno($ch) ;
                      $_SESSION['curl_error_msg']=curl_error($ch);

                      //Execute the Error handling module to display errors. 
            } 
            else 
            {
                     //closing the curl
                    curl_close($ch);
            }

            return $nvpResArray;
    }
    
    public function RedirectToPayPal ( $token )
    {	
        
        $PAYPAL_URL = $this->_api_url;
        // Redirect to paypal.com here
        $payPalURL = $PAYPAL_URL . $token;
        header("Location: ".$payPalURL);
    }

    public function deformatNVP($nvpstr)
    {
        $intial=0;
        $nvpArray = array();

        while(strlen($nvpstr))
        {
                //postion of Key
                $keypos= strpos($nvpstr,'=');
                //position of value
                $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

                /*getting the Key and Value values and storing in a Associative Array*/
                $keyval=substr($nvpstr,$intial,$keypos);
                $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
                //decoding the respose
                $nvpArray[urldecode($keyval)] =urldecode( $valval);
                $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
        }
        return $nvpArray;
    }
    
    /*Added for Single payment*/
    //Added for PPV
    public function SetExpressCheckout($paymentInfo=array()){ 
        $amount = urlencode($paymentInfo['Order']['theTotal']);
        //description
        if($paymentInfo['Order']['description']){
            $desc = urlencode($paymentInfo['Order']['description']);
        }else{
            $desc = $this->defaultDescription;
        }
        //quantity
        if($paymentInfo['Order']['quantity']){
            $quantity = urlencode($paymentInfo['Order']['quantity']);
        }else{
            $quantity = $this->defaultQuantity;
        }
        
        $paymentType=urlencode('Sale'); 
        $currencyCode= $paymentInfo['Order']['currency']; 
        
        $number = time();                
         
        $returnURL =urlencode($paymentInfo['Order']['retuenUrl']); 
        $cancelURL =urlencode($paymentInfo['Order']['cancelUrl']);
        $landing = (isset($paymentInfo['Order']['landing']) && $paymentInfo['Order']['landing'] != '')?1:0;
        $solution = (isset($paymentInfo['Order']['solution']) && $paymentInfo['Order']['solution'] != '')?1:0;
        $nvpstr='&AMT='.$amount.'&PAYMENTACTION='.$paymentType.'&CURRENCYCODE='.$currencyCode.'&RETURNURL='.$returnURL.'&CANCELURL='.$cancelURL.'&DESC='.$desc.'&QTY='.$quantity;
        $nvpstr .= "&CUSTOM=21";
        
        if($landing == 0)
        {
            $nvpstr = $nvpstr . "&LANDINGPAGE=Billing";
        }
        else
        {
            $nvpstr = $nvpstr . "&LANDINGPAGE=Login";
        } 
        if($solution == 0)
        {
            $nvpstr = $nvpstr . "&&SOLUTIONTYPE=Sole";
        }
        $resArray=$this->hash_call("SetExpressCheckout",$nvpstr); 
        return $resArray; 
    }   
    
    public function GetExpressCheckoutDetails($token){ 
        $nvpstr='&TOKEN='.$token; 
        $resArray=$this->hash_call("GetExpressCheckoutDetails",$nvpstr); 
        return $resArray; 
    } 
    
    public function DoExpressCheckoutPayment($paymentInfo=array()){ 
        $paymentType = 'Sale'; 
        $serverName = $_SERVER['SERVER_NAME']; 
        $nvpstr='&TOKEN='.urlencode($paymentInfo['TOKEN']).'&PAYERID='.urlencode($paymentInfo['PAYERID']).'&PAYMENTACTION='.urlencode($paymentType).'&AMT='.urlencode($paymentInfo['AMT']).'&CURRENCYCODE='.urlencode($paymentInfo['CURRENCYCODE']).'&IPADDRESS='.urlencode($serverName); 
        $resArray=$this->hash_call("DoExpressCheckoutPayment",$nvpstr); 
        return $resArray; 
    }    
    
    public function APIError($errorNo,$errorMsg,$resArray){ 
        $resArray['Error']['Number']=$errorNo; 
        $resArray['Error']['Number']=$errorMsg; 
        return $resArray; 
    } 
    
    public function isCallSucceeded($resArray){
        $ack = strtoupper($resArray["ACK"]); 
        //Detect Errors 
        if($ack != "SUCCESS" && $ack != 'SUCCESSWITHWARNING'){ 
            return false;
        }else{
            return true;
        }
    }    
}
?>