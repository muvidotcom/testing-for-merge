<?php
require 's3bucket/aws-autoloader.php';
use Aws\CloudFront\CloudFrontClient;
class PlayingController extends Controller {
	protected function beforeAction($action) {
        parent::beforeAction($action);
		
        if (!(Yii::app()->user->id)) {
        $studio_id = Yii::app()->common->getStudiosId();
			$std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
			$free_content_login = 1;
			if(isset($std_config->config_value) && $std_config->config_value == 0){
				$free_content_login = 0;
            }
			
			if(intval($free_content_login)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
			$this->redirect(Yii::app()->getBaseUrl(TRUE).'/user/login');exit;
        }
        }
        return true;
    }    
	
    public function actionPlayMovie() {
        $language_id = $this->language_id;
        if (isset($_REQUEST['contentPlink']) && $_REQUEST['contentPlink']) {
            $permaLink = $_REQUEST['contentPlink'];
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are trying to play an invalid content');
            $this->redirect(Yii::app()->getBaseUrl(TRUE));
            exit;
        }
        $contentTypePermalink = @$_REQUEST['contentType'];
        $studio = new Studios();
        $studio = $this->studio;
        $studio_id = $studio->id;
        $authToken = OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select'=>'oauth_token'))->oauth_token;
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0; 
		if ($permaLink) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.story,f.permalink,f.content_types_id,f.mapped_id,f.content_category_value')
                    ->from('films f ')
                    ->where('f.permalink=:permalink AND f.studio_id=:studio_id AND parent_id = 0', array(':permalink' => $permaLink, ':studio_id' => $studio_id));
            $movie = $command->queryAll();
            $mapped_id = $movie[0]['mapped_id'];
            if($movie[0]['mapped_id']){
                $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.story,f.permalink,f.content_types_id,f.mapped_id,f.content_category_value')
                    ->from('films f ')
                    ->where('f.id=:id AND parent_id = 0', array(':id' => $movie[0]['mapped_id']));
                $movie = $command->queryAll();
            }
            if ($movie) {
                $movie_id = $movie[0]['id'];
                $categories = array();
                if(@$movie[0]['content_category_value']){
                    $sql = "SELECT category_name, permalink FROM content_category WHERE studio_id={$studio_id} AND id IN (".$movie[0]['content_category_value'].") AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND id IN (".$movie[0]['content_category_value'].") AND language_id={$language_id})) ORDER BY IF(parent_id >0, parent_id, id) DESC";
                    $cats = Yii::app()->db->createCommand($sql)->queryAll(); 
                    foreach($cats as $cat){
                        $categories[] = array(
                            'category_name' => $cat['category_name'],
                            'permalink' => $cat['permalink'],
                        );
                    }
                }
                if(@$mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
                $s_studio_id = $_SESSION[$studio_id]['parent_studio_id'];
               }else{
                $s_studio_id = $studio_id;
               }
               if(!Yii::app()->common->isGeoBlockContent(@$movie_id,'',$s_studio_id)){
                    Yii::app()->user->setFlash('error', $this->ServerMessage['content_not_available_in_your_country']);
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
                }
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['trying_play_invalid_content']);
                $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
            }
        }
        if ($movie[0]['content_types_id'] == 4) {
            $liveStream = Livestream::model()->find('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie[0]['id'], ':studio_id' => $studio_id));
            if (!$liveStream || !$liveStream->feed_url) {
                Yii::app()->user->setFlash('error', $this->ServerMessage['no_live_feed_available']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);exit;
            }
        } else {
            $stream_code = isset($_REQUEST['stream']) ? $_REQUEST['stream'] : '0';
            $season = isset($_REQUEST['season']) ? $_REQUEST['season'] : 0;

            $stream_id = 0;
            $purchase_type = '';
            if (trim($permaLink) != '0' && trim($stream_code) != '0') {
                //Get stream Id
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
                $purchase_type = 'episode';
            } else if (trim($permaLink) != '0' && trim($stream_code) == '0' && intval($season)) {
                $purchase_type = 'episode';
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id);
            }
            if (!trim($stream_id) || $stream_id <= 1) {
                Yii::app()->user->setFlash('error', $this->ServerMessage['video_content_not_available']);
                $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/' . $movie->permalink);exit;
            }
        }
        $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
        $free_content_login = 1;
        if(isset($std_config) && !empty($std_config)){
            $free_content_login = $std_config->config_value;
        }

    if (STUDIO_USER_ID == 55) {
            $mov_can_see = 'allowed';
        } else {
			if (isset($_SESSION['AuthorizationForContent']) && ($_SESSION['AuthorizationForContent'] == $stream_id)) {
				$mov_can_see = 'allowed';
			} else {
				$default_currency_id = $this->studio->default_currency_id;

				$can_see_data['movie_id'] = $movie_id;
				$can_see_data['stream_id'] = $stream_id;
				$can_see_data['season'] = $season;
				$can_see_data['purchase_type'] = $purchase_type;
				$can_see_data['studio_id'] = $studio_id;
				$can_see_data['user_id'] = $user_id;
				$can_see_data['default_currency_id'] = $default_currency_id;
				$mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
			}
			
             if(intval($free_content_login) && $user_id == 0) {
               Yii::app()->user->setFlash('error', 'Login to access the page.');
               $this->redirect(Yii::app()->getBaseUrl(TRUE).'/user/login');exit;
            }
        }
       // if ($movie[0]['content_types_id'] != 4) {
        /* get stream_code from stream */
        $movie_strm = movieStreams::model()->findByPk($stream_id);
        /* get stream_code from stream */
        $policyRules = array();
        $flag= 0;
        $stream_id_next = 0;
        $autoPlay = StudioConfig::model()->getConfig($studio_id, 'autoplay_next_episode');
        if ($autoPlay['config_value'] == 1) {
            if ($movie[0]['content_types_id'] == 3) {
                $ep_no = $movie_strm->episode_number;
                $command1 = Yii::app()->db->createCommand()
                        ->select('id , embed_id , episode_number ,series_number')
                        ->from('movie_streams')
                        ->where('movie_id = ' . $movie_id . ' AND episode_parent_id = 0 AND series_number = ' . $movie_strm->series_number . ' AND full_movie != "" AND episode_number >' . $ep_no)
                        ->order('episode_number')
                        ->limit(1);
                $result = $command1->queryAll(); 
                if (($result[0])=='') {
                    $ser_no = $movie_strm->series_number;
                    $command1 = Yii::app()->db->createCommand()
                        ->select('id , embed_id , episode_number ,series_number')
                        ->from('movie_streams')
                        ->where('movie_id = ' . $movie_id . ' AND episode_parent_id = 0 AND  full_movie != "" AND series_number >' . $ser_no)
                        ->order('series_number')
                        ->limit(1);
                    $result = $command1->queryAll();
                }
            }
            if($result[0]!=''){
                $flag = 1;
                $stream_id_next = $result[0]['id'];
                $embed_id_next = $result[0]['embed_id'];
            }
        }        
        if($movie_strm->embed_id){
            $policyRules = Yii::app()->policy->getRulesAPI($movie_strm->embed_id, $user_id, $studio_id);
        }
        //If a user has never subscribed and trying to play video
        if ($mov_can_see == "unsubscribed") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['activate_subscription_watch_video']);
            $this->redirect(Yii::app()->getBaseUrl(true) . "/user/activate");
            exit();
        }
        //If a user had subscribed and cancel
        else if ($mov_can_see == "cancelled") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['reactivate_subscription_watch_video']);
            $this->redirect(Yii::app()->getBaseUrl(true) . "/user/reactivate");
            exit();
        }
        //If video is not allowed country
        else if ($mov_can_see == "limitedcountry") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['video_restiction_in_your_country']);
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        } else if ($mov_can_see == "advancedpurchased") {
            Yii::app()->user->setFlash('error', $this->ServerMessage['already_purchase_this_content']);
            $this->redirect(Yii::app()->getBaseUrl(true));
            exit();
        } else if ($mov_can_see == 'unpaid') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['ppv_video_pay_and_watch']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if(!empty($policyRules['rules'])){
            Yii::app()->user->setFlash('error', $policyRules['rules']['message']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        }  else if ($mov_can_see == 'access_period') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['access_period_expired']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'watch_period') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['watch_period_expired']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'maximum') {
            Yii::app()->user->setFlash('error', $this->ServerMessage['crossed_max_limit_of_watching']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
        } else if ($mov_can_see == 'allowed') {
            $getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($studio_id, Yii::app()->user->id,$stream_id);
            if ($getDeviceRestriction != "" && $getDeviceRestriction == 0) {
                Yii::app()->user->setFlash('error', @$this->Language['restrict-streaming-device']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);
            } 
            if($mapped_id && (isset($_SESSION[$studio_id]['parent_studio_id']) && $_SESSION[$studio_id]['parent_studio_id'])){
                $bucketInfo = Yii::app()->common->getBucketInfo('', $_SESSION[$studio_id]['parent_studio_id']);
            }else{
                $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            }
            if ($movie_strm && (($movie_strm->is_demo == '1') || ($movie_strm->full_movie !=''))) {
                if($movie_strm->is_demo == 1){
                    $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/small.mp4';
                } else{
                    $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] .'/'.$bucketInfo['signedFolderPath']. 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/' . $movie_strm->full_movie;
                }
            }
            if (!$fullmovie_path && $movie[0]['content_types_id'] != 4) {
                Yii::app()->user->setFlash('error', $this->ServerMessage['video_content_not_available']);
                $this->redirect(Yii::app()->getBaseUrl(TRUE));
                exit;
            }
        
            $is_episode = 0;
            $content_id = $movie_id;
            if(!empty($movie_strm) && $movie_strm->is_episode == 1){
                $is_episode = 1;
                $content_id = $movie_strm->id;
            }
            $review_form = Yii::app()->general->reviewform($movie_id);
            $comment_form = Yii::app()->general->commentform($movie_id);
            $review = Yii::app()->general->review($movie_id);
            $content_data = Yii::app()->general->getContentData($content_id,$is_episode);
            $pageUrl = Yii::app()->request->getPathInfo();
            $play_path = Yii::app()->getBaseUrl(true)."/".preg_replace('/playing/', 'player', $pageUrl, 1);
            $this->render('play_video',array('play_path' =>$play_path, 'pageUrl'=>$pageUrl,'categories'=>json_encode($categories),'review_form'=>$review_form,'comment_form'=>$comment_form,'contents' => json_encode($content_data)));
        } else {
            $this->redirect(Yii::app()->baseUrl . '/user/login');
        }
    }  
            
	function getnew_secure_url($resourceKey, $streamHostUrl = "", $isAjax = "",$browserName="") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJYIDWFG3D6CNOYVA.pem',
                    'key_pair_id' => 'APKAJYIDWFG3D6CNOYVA',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d3ff6jhdmx1yhu.cloudfront.net';
        }
        if (Yii::app()->common->isMobile() || $browserName == "Safari") {
            $expires = time() + 50;
        }else if ($isAjax == 1) {
            $expires = time() + 20;
        } else {
            $expires = time() + 5;
        }

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }
	
	function getsecure_url($resourceKey, $streamHostUrl = "") {
        $this->layout = false;
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => 'pk-APKAJ5RUFF7WVAM5VHPQ.pem',
                    'key_pair_id' => 'APKAJ5RUFF7WVAM5VHPQ',
        ));
        if ($streamHostUrl == "") {
            $streamHostUrl = CDN_HTTP.'d62co02q89w6j.cloudfront.net';
        }
        $expires = time() + 250;
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));
        return $signedUrlCannedPolicy;
    }
}
