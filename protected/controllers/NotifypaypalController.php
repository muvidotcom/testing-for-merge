<?php

class NotifypaypalController extends Controller {

    public function actionIpn() {
        if (isset($_POST) && count($_POST) > 0) {
            $data = $_POST;
            $transaction_type = $data['txn_type'];
            $profile_id = $data['recurring_payment_id'];
            $subscription_data = UserSubscription::model()->getSubscriptionDetailsByProfileID($profile_id);
            $studio_id = $subscription_data['studio_id'];
            $user_id = $subscription_data['user_id'];
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
            $usersub = new UserSubscription;
            $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
            //Getting card detail
            $card = new SdkCardInfos;
            $card = $card->findByPk($subscription_data['card_id']);

            $first_name = $data['first_name'];
            $user_email = $data['payer_email'];
            $address1 = $data['address_street'];
            $city = $data['address_city'];
            $state = $data['address_state'];
            $country = $data['address_country_code'];
            $zip = $data['address_zip'];
            $ipn_track_id = $data['ipn_track_id'];
            $payer_id = $data['payer_id'];

            if ($transaction_type == 'recurring_payment') {

                $ip_address = Yii::app()->request->getUserHostAddress();
                $transaction = new Transaction;
                $transaction->user_id = $user_id;
                $transaction->studio_id = $studio_id;
                $transaction->plan_id = $usersub->plan_id;
                $transaction->transaction_date = gmdate('Y-m-d H:i:s');
                $transaction->payment_method = $plan_payment_gateway['gateways'][0]->short_code;
                $transaction->transaction_status = $data['profile_status'];
                ;
                $transaction->invoice_id = $data['txn_id'];
                $transaction->order_number = $profile_id;
                $transaction->dollar_amount = $data['mc_gross'];
                $transaction->amount = $data['mc_gross'];
                $transaction->response_text = json_encode($data);
                $transaction->subscription_id = $usersub->id;
                $transaction->fullname = $first_name;
                $transaction->address1 = $address1;
                $transaction->city = $city;
                $transaction->state = $state;
                $transaction->country = $country;
                $transaction->zip = $zip;
                $transaction->payer_id = $payer_id;
                $transaction->ipn_track_id = $ipn_track_id;
                $transaction->ip = $ip_address;
                $transaction->created_by = $user_id;
                $transaction->created_date = gmdate('Y-m-d H:i:s');
                $transaction->save();

                //Getting plan detail
                $plan = new SubscriptionPlans;
                $plan = $plan->findByPk($usersub->plan_id);

                $today = gmdate('Y-m-d H:i:s');
                $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                $start_date = Date("Y-m-d H:i:s", strtotime($today . "+{$recurrence_frequency}"));
                $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));

                $usersub->is_success = 1;
                $usersub->start_date = $start_date;
                $usersub->end_date = $end_date;
                $usersub->save();
                $studio = Studios::model()->findByPk($studio_id);
                $card_id = (isset($usersub->card_id) && $usersub->card_id) ? $usersub->card_id : 0;
                $card = SdkCardInfos::model()->findByPk($card_id);
                $user['amount'] = $data['mc_gross'];
                $user['card_holder_name'] = $card->card_holder_name;
                $user['card_last_fourdigit'] = $card->card_last_fourdigit;

                $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $data['txn_id']);
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_renew', $file_name);
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_renew', $studio_id);
                if ($isEmailToStudio) {
                    $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_monthly_payment', $user_id, '', $data['mc_gross']);
                }
            } elseif ($transaction_type == 'recurring_payment_failed' || $transaction_type == 'recurring_payment_skipped') {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $studio_payment_gateway = $plan_payment_gateway['gateways'][0];
                    $this->setPaymentGatwayVariable($studio_payment_gateway);
                    $payment_gateway_controller = 'Api' . $studio_payment_gateway->short_code . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $data = $payment_gateway::updateAutoBillOutAmt($profile_id);
                }
                $teModel = New TransactionErrors;
                $teModel->studio_id = $studio_id;
                $teModel->user_id = $user_id;
                $teModel->plan_id = $usersub->plan_id;
                $teModel->subscription_id = $usersub->id;
                $teModel->payment_method = $studio_payment_gateway->short_code;
                $teModel->response_text = json_encode($data);
                $teModel->created_date = gmdate('Y-m-d H:i:s');
                $teModel->save();

                $usersub->payment_key = '';
                $usersub->payment_status = 2; //Payment failed
                $usersub->partial_failed_date = gmdate('Y-m-d h:i:s'); //Payment failed date
                $usersub->partial_failed_due = $data['mc_gross'];
                $usersub->save();
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'payment_failed');
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('payment_failed', $studio_id);
                if ($isEmailToStudio) {
                    $admin_email = $this->sendStudioAdminEmails($studio_id, 'studio_admin_payment_failed', $user_id);
                }
            } elseif ($transaction_type == 'recurring_payment_profile_cancel') {
                
            } elseif ($transaction_type == 'recurring_payment_profile_created') {
                
            }
        }
    }

}
