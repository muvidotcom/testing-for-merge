<?php
class ApidocsController extends Controller{
	public $layout = 'main';
	public $defaultAction = 'introduction';
	public function init(){
		Yii::app()->theme = 'bootstrap';
		parent::init();
    }
	
	function actionApiInfo(){
		$permalink = $_REQUEST['permalink'];
		if($permalink){
			$plink = $permalink;
			$data = Apidocs::model()->findByAttributes(array('permalink'=>$permalink));
			$getItem = Apidocs::model()->with('api_request_params')->findAllByAttributes(array('id_parent'=>$data->id));
			if($permalink=='users'){
				$this->pageTitle = 'Muvi APIs for User Management';
				$this->pageDescription = 'Access user related API calls for Muvi ';
			}elseif($permalink=='content'){
				$this->pageTitle = 'Muvi APIs for Content Call and Content Management';
				$this->pageDescription = 'Muvi Video Streaming Platform APIs for Content Call and Management for your video streaming platform';
			}elseif($permalink=='search'){
				$this->pageTitle = 'Search Content & Data API for Muvi Video Streaming Platform';
				$this->pageDescription = 'Muvi APIs for Search Data & Contents';
			}
			$this->render('index',array('data'=>$data,'getItem'=>$getItem,'permalink'=>$plink));
		}else{
			$this->redirect($this->createUrl('apidocs/introduction'));exit;
		}
		
	}
	function actionIntroduction(){
		$this->pageTitle = 'Video streaming platform and Player API | API for Muvi';
		$this->pageDescription = 'Use Muvi Video Streaming Platform and Player APIs to create and stream your own video based web and mobile applications';
		$this->render('introduction');
	}
}
