<?php

class PermissionController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        return true;
    }

    function actionManageUsers() {
        if (Yii::app()->user->role_id != 1) {
            $url = $this->createUrl('admin/dashboard');
            $this->redirect($url);
        }

        $this->breadcrumbs = array('Settings' , 'Manage Permissions');
        $this->headerinfo = "Manage Permissions";
        $this->pageTitle = "Muvi | Manage permissions";
        $studio_id = Yii::app()->common->getStudiosId();
        $users = User::model()->findAll('studio_id=:studio_id AND is_active=:is_active AND (role_id=:t1 OR role_id=:t2 OR role_id=:t3)', array(':studio_id' => $studio_id, ':is_active' => 1, ':t1' => 1, ':t2' => 2, ':t3' => 3), array('order' => 'first_name ASC'));
        $roles = Role::model()->findAll('role_type=:t1 OR role_type=:t2', array(':t1' => 'Admin', ':t2' => 'Member'), array('order' => 'role_type ASC'));
        $a = array(1, 5, 33,34,35);
        $permissions = permission::model()->findAllByAttributes(array("id" => $a));
        $default_permissions = array(1,5,33,35);
        $this->render('manageusers', array('users' => $users, 'roles' => $roles, 'permissions' => $permissions, 'default_permissions'=>$default_permissions));
    }

    function actionSaveUserName() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['studio_id']) && isset($_REQUEST['first_name'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['user_id'], 'studio_id' => $_REQUEST['studio_id']));
            if (isset($user) && !empty($user)) {
                $user->first_name = trim($_REQUEST['first_name']);
                $user->last_name = trim($_REQUEST['last_name']);
                $user->save();
                Yii::app()->user->setFlash('success', 'User has been updated successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry, User can not be updated!');
            }
        }
        print 1;
        exit;
    }

    function actionSaveRole() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['studio_id']) && isset($_REQUEST['role_id'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['user_id'], 'studio_id' => $_REQUEST['studio_id']));

            if (isset($user) && !empty($user)) {
                $user->role_id = trim($_REQUEST['role_id']);
				if(trim($_REQUEST['role_id'])==2){
					$user->permission_id = '1,5,33';
				}else{
					$user->permission_id = '1,5';
				}
                $user->save();
            }
        }
        print 1;
        exit;
    }

    function actionDeleteUser() {
        $tickettingdb=$this->getAnotherDbconnection();
        $server_id = Yii::app()->common->getServerId();
        if (isset($_REQUEST['user']) && trim($_REQUEST['user']) && isset($_REQUEST['studio']) && trim($_REQUEST['studio'])) {
            $con = Yii::app()->db;
            $sql_usr = "UPDATE user SET is_active=0 WHERE studio_id=" . $_REQUEST['studio'] . " AND id =" . $_REQUEST['user'];
            $con->createCommand($sql_usr)->execute();
            $sql = "UPDATE master_user SET is_active=0 WHERE studio_id=" . $_REQUEST['studio'] . " AND user_id =" . $_REQUEST['user'] . " AND server_id =" . $server_id;
            $master = $tickettingdb->createCommand($sql)->execute();
            Yii::app()->user->setFlash('success', 'User has been deleted successfully.');
        } else {
            Yii::app()->user->setFlash('error', "Oops! User can't be deleted.");
        }

        $url = $this->createUrl('/permission/manageUsers');
        $this->redirect($url);
    }

    function actionCheckEmail() {
        $isExists = 0;
        //no need to check unique email id Multistore
        
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $studio_id = Yii::app()->common->getStudiosId();
            $email = trim($_REQUEST['email']);
            /*$sql = "SELECT * FROM user WHERE email='{$email}' AND studio_id = {$studio_id} AND role_id IN (2,3)";
            $users = Yii::app()->db->createCommand($sql)->queryAll();*/
            $user = new User;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']),'studio_id'=>$studio_id,'is_active'=>1));
            if (isset($users) && !empty($users)) {
                $isExists = 1;
            }
        }
        $res = array('isExists' => $isExists);
        echo json_encode($res);exit;
    }

    function actionNewuser() {
        if (isset($_REQUEST['data']['Admin']) && !empty($_REQUEST['data']['Admin'])) {
            $default_permission = $_REQUEST['data']['default_permission'];
            $default_permission = implode(',',$default_permission); 
            $studio_id = Yii::app()->user->studio_id;
            $data = $_REQUEST['data']['Admin'];
            //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
            //ajit@muvi.com
            //if email already exist copy the existing password to new record
            //if it is new email id then generate new password
            $existemail = User::model()->find(array('select'=>'encrypted_password','condition' => 'email=:email AND is_active=:is_active','params' => array(':email' =>$data['email'],':is_active' => 1)));
            if($existemail){
                $type='existing';
                $encrypted_pass = $existemail->encrypted_password;
            }else{
                $type='new';
			$format = new Format();
			$password = $format->randomPassword();
            $enc = new bCrypt();
            $encrypted_pass = $enc->hash($password);
            }
            //END - 6312
            
            $umodel = new User();
            $umodel->studio_id = $studio_id;
            $umodel->first_name = trim($data['first_name']);
            $umodel->email = trim($data['email']);
            $umodel->encrypted_password = $encrypted_pass;
            $umodel->created_at = gmdate('Y-m-d H:i:s');
            $umodel->is_active = 1;
            $umodel->is_sdk = 1;
            $umodel->role_id = $data['role_id'];
			if($data['role_id'] == 3){
				$umodel->permission_id = $default_permission;
			}
            $umodel->signup_step = 0;
            $umodel->save();
            $tickettingdb=$this->getAnotherDbconnection();
            $server_id = Yii::app()->common->getServerId();
            $master = $tickettingdb->createCommand("select id from ticket_master where studio_id = $studio_id and server_id = $server_id")->queryAll();
            $master_id = $master[0][id];
            
                $tickettingdb->createCommand()->insert('master_user', array(
                    'master_id' => $master_id,
                    'studio_id'=>$studio_id,
                    'email'=>trim($data['email']),
                    'first_name'=>trim($data['first_name']),
                    'is_active'=>1,
                    'permission_id'=>$default_permission,
                    'server_id'=> $server_id,
                    'user_id'=>$umodel->id,
                    'created_at'=>gmdate('Y-m-d H:i:s')
                ));

            //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
            //This section is removed by 6312, existing password is assigned to newly added email, so no need to update password now
            //ajit multistore
            //check the email id for multiple store
            //6312-$multiuser = User::model()->findAll('studio_id !=:studio_id AND is_active=:is_active AND email=:email', array(':studio_id' => $studio_id, ':is_active' => 1, ':email' => trim($data['email'])));
            //if present update the new password for all
            //6312-if(count($multiuser)){
            //6312-    $sql = "UPDATE user set encrypted_password ='".$encrypted_pass."' WHERE email='".trim($data['email'])."'";
            //6312-    Yii::app()->db->createCommand($sql)->execute();
            //6312-}
            //Send to email to that user
            $this->sendEmailToNewUser($umodel, $password, $type);
            Yii::app()->user->setFlash('success', 'New user has been added successfully.');
        } else {
            Yii::app()->user->setFlash('error', 'Invalid user data.');
        }

        $url = $this->createUrl('/permission/manageUsers');
        $this->redirect($url);
    }

    function sendEmailToNewUser($user = Null, $password = Null, $type = Null) {
        $studio_id = Yii::app()->user->studio_id;
        $std = new Studio();
        $studio = $this->studio;
        $site_url = 'http://' . $studio->domain;

        $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);

        $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="" /></a>';

        $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';

        //Check facebook link given or not
        if ($studio->fb_link != '') {
            $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        } else {
            $fb_link = '';
        }

        //Check twitter link given or not
        if ($studio->tw_link != '') {
            $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        } else {
            $twitter_link = '';
        }

        //Check google plus link given or not
        if ($studio->gp_link != '') {
            $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
        } else {
            $gplus_link = '';
        }

        //Set variables for different email types
        $email = $user->email;
        $FirstName = $user->first_name;
        $StudioName = $studio->name;
        $domain = Yii::app()->getBaseUrl(true);

        $subject = "You have been granted access to '" . $studio->domain . "' administration";
        //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
        if($type=='new'){
        $content = '<p>Dear ' . $FirstName . ',</p>
                    <p>You have been granted access to ' . $StudioName . ' administration</p> 
                    <p>Please find following url and credentials .</p>
                    <p style="display:block;margin:0 0 17px">
                        Url: <strong><span mc:edit="domain"><a href="' . $domain . '">' . $domain . '</a></span></strong><br/>
                        User Name: <strong><span mc:edit="email">' . $user->email . '</span></strong><br/>
                        Password: <strong><span mc:edit="password">' . $password . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team ' . $StudioName . '</p>';
        }else{
            $content = '<p>Dear ' . $FirstName . ',</p>
                    <p>You have been granted access to ' . $StudioName . ' administration. Please select ' . $StudioName . ' after login to view this store.</p>                     
                    <p style="display:block;margin:0 0 17px">
                        Url: <strong><span mc:edit="domain"><a href="' . $domain . '">' . $domain . '</a></span></strong><br/>                      
                    </p>
                    <p>Sincerely,</p>
                    <p>Team ' . $StudioName . '</p>';            
        }
        //END 6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
        $user_model = new User();
        $admin = $user_model->findByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'is_sdk' => 1, 'role_id' => 1));
        if (isset($admin) && !empty($admin)) {
            $from_mail = $admin->email;
        } else {
            $from_mail = 'info@' . $studio->domain;
        }
        
        $params = array(
            'website_name' => $StudioName,
             'site_link' => $site_link,
            'logo'=> $logo,
            'name'=> $FirstName,
            'mailcontent'=> $content
        );

        $message = array(
            'subject' => $subject,
            'from_email' => $from_mail,
            'from_name' => $studio->name,
            'to' => array(
                array(
                    'email' => $email,
                    'name' => $FirstName,
                    'type' => 'to'
                )
            )
        );

        $template_name = 'sdk_user_welcome_new';

        $obj = new Controller();
        //$obj->sendMandrilEmail($template_name, $params, $message);
        
        $to=array($email);
        //$from=$from_mail;
        $from='support@muvi.com';
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        $returnVal=$obj->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',$StudioName); 

        
        
        
    }

    function actionChangePermission() {
        $user_id = $_REQUEST['manage_cont_id'];
        $tickettingdb=$this->getAnotherDbconnection();
        $server_id = Yii::app()->common->getServerId();      
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['manage_cont_id']) && isset($_REQUEST['permission_id'])) {
            $user = User::model()->findByAttributes(array('id' => $_REQUEST['manage_cont_id'], 'studio_id' => $studio_id));
            if (isset($user) && !empty($user)) {
                /*$dbcon = Yii::app()->db;
                $sql = "select permission_id from user where id=" . ($_REQUEST['manage_cont_id']);
                $data = $dbcon->createCommand($sql)->queryRow();
                $new_data = $_REQUEST['permission_id'] . ',' . $data['permission_id'];
                $new_data = rtrim($new_data, ',');
                $newdata1 = implode(",", array_unique(explode(",", $new_data)));*/
	       $permissions = trim($_REQUEST['permission_id'],',') ? trim($_REQUEST['permission_id'],',') : '1';		
                $user->permission_id = $permissions;
                if($user->role_id != 1) {
                if($user->permission_id == '1,5,33'){
                    $user->role_id = 2;
                } else {
                    $user->role_id = 3;
                }
                } else {
                $master = $tickettingdb->createCommand("update ticket_master set permission_id = '$permissions' where user_id = $user_id and studio_id = $studio_id and server_id = $server_id")->execute();    
                }
                
                $master_users = $tickettingdb->createCommand("select count(*) as total from master_user where user_id = $user_id and studio_id = $studio_id and server_id = $server_id")->queryAll();
                if($master_users[0]['total'] > 0) {
                $tickettingdb->createCommand("update master_user set permission_id = '$permissions' where user_id = $user_id and studio_id = $studio_id and server_id = $server_id")->execute();
                } else { 
                    $master_id = $tickettingdb->createCommand("select id from ticket_master where studio_id = $studio_id and server_id = $server_id")->queryAll();
                    $tickettingdb->createCommand()->insert('master_user', array(
                        'master_id' => $master_id[0]['id'],
                        'server_id' => $server_id,
                        'studio_id'=>$user->studio_id,
                        'email' => $user->email,
                        'first_name' => $user->first_name,
                        'is_active' => 1,
                        'user_id' => $user_id,
                        'permission_id' => $permissions,
                    ));   
                }
                $user->save();
            }
        }
       exit;
    }

}
