<?php
Class ContentController extends Controller{
	public function init() {
        parent::init();
    }
    public $layout = false;
    public function actionFindepisodes() {
		$studio_id = Yii::app()->common->getStudioId();
		if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']) {
			$request_movie_id = Yii::app()->db->createCommand()->SELECT('mapped_id')->FROM('films')->WHERE('id=:movie_id', array(':movie_id' => $_REQUEST['movie_id']))->queryRow();
			$req_movie_id = ($request_movie_id['mapped_id']) ? $request_movie_id['mapped_id'] : $_REQUEST['movie_id'];
			$sort_order = ' ASC';
			$configValue = StudioConfig::model()->getConfig($studio_id, 'multipart_content_setting');
			if ($configValue && ($configValue->config_value == 0)) {
				$sort_order = ' DESC';
			}
			$cond = 'M.movie_id=:movie_id AND M.is_episode=:is_episode AND M.is_converted=:is_converted AND M.episode_parent_id=0 AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()))';
			$params = array(':movie_id' => $req_movie_id, ':is_episode' => 1, ':is_converted' => 1);
			if (isset($_SESSION[$studio_id]['multipart_cntpaging'])) {
				$multipart_cntpaging = $_SESSION[$studio_id]['multipart_cntpaging'];
			} else {
				$multipart_cntpaging = StudioConfig::model()->getConfig($studio_id, 'multipart_content_paging');
				$_SESSION[$studio_id]['multipart_cntpaging'] = $multipart_cntpaging;
			}
			if ($multipart_cntpaging && $multipart_cntpaging->config_value) {
				$pagination_limit = StudioConfig::model()->getConfig($studio_id, 'pagination_limit');
				$episode_limit = 15;
				if ($pagination_limit && $pagination_limit->config_value) {
					$episode_limit = $pagination_limit->config_value;
				}
				$offset = 0;
				if (isset($_REQUEST['page'])) {
					$offset = ($_REQUEST['page'] - 1) * $episode_limit;
				}
			} else {
				$episode_limit = (isset($_REQUEST['limit']) && $_REQUEST['limit'] > 0) ? $_REQUEST['limit'] : 2;
				$offset = @$_REQUEST['offset'];
			}
			if ($_REQUEST['series'] != '') {
				$cond .= " AND M.series_number=:series_number";
				$params[':series_number'] = $_REQUEST['series'];
			}
			$kvs = array();
			$streams = Yii::app()->db->createCommand()
					->SELECT('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.is_episode,M.is_converted,M.video_duration,M.id AS movie_stream_id,M.embed_id,F.content_category_value,F.permalink,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,M.episode_story,M.episode_title,M.series_number,M.episode_number,F.genre,F.release_date, F.content_types_id, M.last_updated_date')
					->FROM('films F, movie_streams M')
					->WHERE("F.id=M.movie_id AND M.is_episode=1 AND " . $cond, $params)
					->order('M.episode_number ' . $sort_order . ", M.episode_title " . $sort_order)
					->limit($episode_limit, $offset)
					->queryAll();
                        $all_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
			if ($streams) {
				$ostudioArr = array(3060, 54,1812);
				if (in_array($studio_id, $ostudioArr)) {
					$fobject = new Film();
					$kvs = $fobject->getContentListData($streams);
				} else {
					foreach ($streams as $data) {
						$kvs[] = Yii::app()->general->getContentData($data['movie_stream_id'], 1);
					}
				}
				$return = true;
				$status = "success";
				
				//$all_streams = movieStreams::model()->findAll(array('condition' => $cond, 'params' => $params, 'order' => 'episode_number '.$sort_order));
				$count_searched = count($streams);

				if ($multipart_cntpaging && $multipart_cntpaging->config_value) {
					$pagination_html = '<input type="hidden" id="data-count" class="data-count" value="' . $all_count . '" /> <input type="hidden" id="data-count1" value="' . $count_searched . '" />';
				}
				$msg = $this->renderPartial('episodes', array("kvs" => json_encode($kvs), 'total_episodes' => @$all_count, 'count_searched' => @$count_searched, 'pagination' => @$pagination_html), $return);
			} else {
				if ($offset == 0) {
					$status = "failure";
					$msg = '<p class="message">' . $this->Language['not_found'] . '</p>';
				} else {
					$status = "failure";
					$msg = false;
				}
			}
		} else {
			$status = "failure";
			$msg = '<p class="message">' . $this->Language['not_found'] . '</p>';
		}
		$msg = utf8_encode($msg);
		$result = array('status' => $status,'msg' => $msg);
		echo json_encode($result);exit;
	}

	public function actionSavereview() {
        $studio_id = Yii::app()->common->getStudiosId();
        $status = 'error';
        $message = $this->Language['ensure_before_review'];
        $user_id = Yii::app()->user->id;
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] > 0 && $user_id > 0) {
            
            $rate = new ContentRating();
            $rate = $rate->findAllByAttributes(array('studio_id' => $studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $user_id));  
            if(count($rate) > 0)
            {
                $message = $this->Language['already_added_your_review'];
                Yii::app()->user->setFlash('error', $message);   
                $status = 'error';
            }else{
                $content_id = $_REQUEST['content_id'];
                $rating_value = round($_REQUEST['rating'], 1);
                $ip_address = Yii::app()->request->getUserHostAddress();

                $rating = new ContentRating();
                $rating->content_id = $content_id;
                $rating->rating = $rating_value;
                $rating->review = $_REQUEST['review_comment'];
                $rating->created_date = new CDbExpression("NOW()");
                $rating->studio_id = $studio_id;
                $rating->user_ip = $ip_address;
                $rating->user_id = $user_id;
                $rating->status = 1;
                $rating->save();
                $message = $this->Language['review_saved_succesfully'];
                Yii::app()->user->setFlash('success', $message);   
                $status = 'success';
            }
        }
        $result = array('status' => $status, 'message' => $message);
        echo json_encode($result);
    }    
    
    public function actionSavenewsletter() {
        $status = 'error';
        $message = $this->ServerMessage['ensure_enter_email'];  
        $studio_id = Yii::app()->common->getStudiosId();      
        $config = new StudioConfig();
        $config = $config->getconfigvalue('newsletter');
        if($config['config_value'] == 1){
            if(isset($_REQUEST['newsletter-email']) && $_REQUEST['newsletter-email'] != ''){
                $email = trim($_REQUEST['newsletter-email']);
                $user = new SdkUser();
                $users = $user->find(array('condition' => 'studio_id=:studio_id AND email=:email', 'params' => array(':studio_id' => $studio_id, ':email' => $email), 'select' => array('select' => 'id')));
                $sub = new NewsletterSubscribers();
                $subs = $sub->findAllByAttributes(array('studio_id' => $studio_id, 'email' => $email));   
                if(count($subs) > 0){
                    $message = $this->ServerMessage['already_subscribed_to_newsletter'];
                } else {
                    $subscriber = new NewsletterSubscribers();
                    if (count($users) > 0) {
                        $subscriber->user_id = $users->id;
                    }
                    $subscriber->studio_id = $studio_id;
                    $subscriber->email = $email;
                    $subscriber->date_subscribed = new CDbExpression("NOW()");
                    $subscriber->save();
                    $message = $this->ServerMessage['thanks_for_subscribing_newsletter'];
                    Yii::app()->user->setFlash('success', $message);   
                    $status = 'success';                    
                }
            }
        } 
        $result = array('status' => $status, 'message' => $message);
        echo json_encode($result);        
    } 
    
    public function actionDeletesubscriber() {
        $url = $this->createUrl('userfeature/newsletter');
        if (isset($_REQUEST['sub_id']) && $_REQUEST['sub_id'] > 0) {
            $studio_id = Yii::app()->common->getStudioId();
            $sub = new NewsletterSubscribers();
            $sub = $sub->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['sub_id']));
            if (count($sub) > 0) {
                $sub->delete();
                Yii::app()->user->setFlash('success', 'Subscription is deleted!');
            } else {
                Yii::app()->user->setFlash('error', "You don't have access to this!");
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this!");
        }
        $this->redirect($url);
    }  
    
    public function actionloadFeaturedContents(){
        $section_id = $_REQUEST['section_id'];
        $this->layout = false;
        $sections = Yii::app()->general->FeaturedBlockContents(0, $section_id); 
        $this->render('featuredcontents', array('section' => json_encode(@$sections[0])));        
    }  
    
    public function actionloadFeaturedSections(){
        $this->layout = false;
        $offset = isset($_REQUEST['dataoffset'])?$_REQUEST['dataoffset']:0;
        $limit = isset($_REQUEST['viewlimit'])?$_REQUEST['viewlimit']:2;
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $template = Yii::app()->common->getTemplateDetails($controller->site_parent_theme);        

        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id;
        $cond.= ' AND template_id = ' . $template->id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;        
        $sections = FeaturedSection::model()->findAll($criteria);
        $return = array();
        foreach ($sections as $section) {
            $final_content = array();
            $total_contents = 0;
            foreach ($section->contents as $featured) {
                $total_contents++;
                if(Yii::app()->custom->LimitedContentHomePageData() == 1){
                    $final_content[] = Yii::app()->general->getLimitedContentData($featured->movie_id, $featured->is_episode);
                }else{
                    $final_content[] = Yii::app()->general->getContentData($featured->movie_id, $featured->is_episode);
                }
            }
            $return[] = array(
                'id' => $section->id,
                'title' => utf8_encode($section->title),
                'total' => $total_contents,
                'contents' => $final_content
            );
        }                        
        $this->render('contents', array('sections' => json_encode(@$return)));        
    } 
    public function actionTotalFeaturedContent(){
        $studio = $this->studio;
        $studio_id = $studio->id;
        $sql = "SELECT count(*) as total FROM featured_content WHERE studio_id={$studio_id}";
        $contents = Yii::app()->db->createCommand($sql)->queryRow();
        $total_contents = @$contents['total'];
        echo $total_contents;exit;
    }
    
    public function actionCanSeeContent(){
        $movie_id = isset($_POST['movie_id'])?$_POST['movie_id']:0;
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();  
        $default_currency_id = $this->studio->default_currency_id;
        $can_see_data['movie_id'] = $movie_id;
        $can_see_data['studio_id'] = $studio_id;
        $can_see_data['user_id'] = $user_id;
        $can_see_data['default_currency_id'] = $default_currency_id;
        $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
        echo $mov_can_see;
    }    
}

