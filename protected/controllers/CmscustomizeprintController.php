<?php
class CmsCustomizeprintController extends Controller {

    public function init() {
        parent::init();
    }

    public $defaultAction = 'index';
    public $layout = 'admin';
    protected function beforeAction($action) {
        Yii::app()->theme = 'admin';
        parent::beforeAction($action);
        return true;
    }
    public function actionIndex(){
        $this->render("index");
    }
}