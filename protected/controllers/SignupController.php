<?php
require_once 'FirstDataApi/FirstData.php';
require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Ses\SesClient; 
class SignupController extends Controller {

    public function init() {
        
        //Added by RK to resolve the smap traffic to the sign up
        if(isset($_GET)){
            $pattern = '/^textarea/';
            foreach($_GET as $key => $value){     
                $c = preg_match('/alert|script/', strtolower($value));
                if($c > 0){
                    $redirect_url = Yii::app()->getBaseUrl(true).'/signup/landing';
                    $this->redirect($redirect_url);
                    exit();
                }                
            }
        }
        
        parent::init();
        Yii::app()->theme = 'signup';
        return true;
    }
    
    //Added by RK to resolve the smap traffic to the sign up
    public function actionLanding(){
        $this->layout = 'blank';
    }

    /**
     * @method public Index() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    //Sign up Step 1
    function actionIndex() {
      
        $meta = Yii::app()->common->pagemeta('studiosignup');
        $this->pageTitle = "Launch your Audio / Video Streaming Platform Instantly, signup for 14 days free trial - Muvi";
        $this->pageDescription = "Launch your Audio/Video On-demand & Live Streaming Platform instantly in 1-Click at Zero upfront Investment. Signup now for our 14 days FREE Trial, no credit card required.";
//         if(isset($_REQUEST['email'])) {
//         $temporary_email_log = new TemporaryEmailLog();
//         $temporary_email_log -> name = $_REQUEST['name'];
//         $temporary_email_log -> email = $_REQUEST['email'];
//         $temporary_email_log -> form_data = serialize($_REQUEST);
//         $temporary_email_log -> page = 'signup';
//         $temporary_email_log -> save();
//         }
//         
//         if(isset($_REQUEST) && count($_REQUEST)){
//             $ret = Yii::app()->common->sendSampleEmail("ratikanta@muvi.com", "Signup Form Check in Muvi.com", $_REQUEST);
//         }
        
        $ip_address = @CHttpRequest::getUserHostAddress();
        $s3bucket_id = 1;
        $domain = Yii::app()->common->getStudiodomain();
        $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
        $country = $visitor_loc['country_name'];
        $region = $visitor_loc['region'];              
	
        if (isset($_REQUEST['is_submit']) && $_REQUEST['is_submit'] && isset($_REQUEST['email']) && $_REQUEST['email'] != '' && isset($_REQUEST['studio_email']) && strlen($_REQUEST['studio_email']) == 0 && !Yii::app()->session['back_btn']) {
            Yii::app()->session['purchasetype'] = $_REQUEST['purchasetype'];
            $latitude = $visitor_loc['latitude'];
            $longitude = $visitor_loc['longitude'];             
            $ref = array($latitude, $longitude);            
            $string = str_replace(' ', '', strtolower($_REQUEST['subdomain']));
            $inputDomain = str_replace('www.', '', preg_replace('/[^A-Za-z0-9.]/', '', $string));
            $usr = new User;
            if(!Yii::app()->session['studio_id']){
                //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
                //ajit@muvi.com
                //except studio admin email, other admin email can create new studio
                $data = $usr->findByAttributes(array('email' => $_REQUEST['email'],'role_id' => 1));
                if ($data) {
                    Yii::app()->user->setFlash('error', 'Account already exists! Please login using this Email Address.');
                    $this->redirect($this->createUrl());
                    exit;
                }
                //check Subdomain 
                if (isset($_REQUEST['subdomain']) && $_REQUEST['subdomain']) {
                    $data = Studios::model()->find('domain=:domain OR reserved_domain=:domain', array(':domain' => $inputDomain));
                    if ($data) {
                        Yii::app()->user->setFlash('error', 'This domain name already exists. Please try a different one.');
                        $this->redirect($this->createUrl());
                        exit;
                    }
                }
            }
            
            
            if (!$data) {
                //Find the bucket with respect to the client region and assign to that bucket.
                $countrys3location = new Countrys3location();
                $buckt_data = $countrys3location->getS3Location($country);
                if(isset($buckt_data) && count($buckt_data) && $buckt_data['s3buckets_id'] > 0){
                    $s3bucket_id = $buckt_data['s3buckets_id'];
                }else{
                    if ($latitude && $longitude) {
                        $s3buckets = S3bucket::model()->findAll();
                        foreach ($s3buckets AS $key => $val) {
                            if($val->is_signed == 1)
                            {
                                $buckts[$val->id][] = $val->bucket_name;
                                $buckts[$val->id][] = $val->region_code;
                                $buckts[$val->id][] = $val->latitude;
                                $buckts[$val->id][] = $val->longitude;
                            }
                        }
                        $distances = array_map(function($buckts) use($ref) {
                            $a = array_slice($buckts, -2);
                            return Yii::app()->common->getDistance($a, $ref);
                        }, $buckts);
                        asort($distances);
                        $bucketids = array_keys($distances);
                        if($bucketids[0] > 0)
                            $s3bucket_id = $bucketids[0];
                    }
                }
                //Get referrence link from where user come this site.
                $source = '';
                if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                    $source = Yii::app()->request->cookies['REFERRER'];
                    
                    //Unset source cookie
                    //unset($_COOKIE['REFERRER']);
                    //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
                }
                $sql = "SELECT id FROM currency WHERE code = 'USD'";
                $currency = Yii::app()->db->createCommand($sql)->queryRow();
                
                $subdomain = $this->createUniqSubdomain($inputDomain);
                $main_domain = $subdomain . '.' . $domain;
                $studio = new Studio();
                $studio->uniqid = Yii::app()->common->generateUniqNumber();
                $studio->name = $_REQUEST['companyname'];
                $studio->permalink = $subdomain;
                $studio->phone = @$_REQUEST['phone'];
                $studio->created_dt = gmdate('Y-m-d H:i:s');
                $studio->subdomain = $subdomain;
                $studio->domain = $main_domain;
                $studio->reserved_domain = $inputDomain;
                $studio->theme = $domai;
                $studio->parent_theme = '';
                $studio->s3bucket_id = $s3bucket_id;
                $studio->source = $source;
                $studio->default_currency_id = $currency['id'];
                $studio->new_cdn_users=1;
                $studio->status = 1;
                $studio->need_login = 1;
                $studio->robots_txt = 'User-agent: *
Disallow: /';
                $config = Yii::app()->common->getConfigValue(array('trial_period'), 'value');
                $studio->start_date = Date('Y-m-d H:i:s', strtotime("+{$config[0]['value']} days"));
                $end_date = $config[0]['value'] + 30;
                $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$end_date} days"));
                
                $studio->save();
                $studio_id = $studio->id;
                $studio->mrss_id = md5($studio_id);
                $studio->save();
                Yii::app()->session['studio_id'] = $studio_id;
                Yii::app()->session['back_btn'] = $studio_id;
                
                //Create Directory in s3 For studio
                if($s3bucket_id == ''){
                    $s3bucket_id = 1;
                }
                $bucketDetailsArr = Yii::app()->common->getBucketInfo($s3bucket_id);
                if(isset($bucketDetailsArr['bucket_name']) && $bucketDetailsArr['bucket_name'] != ''){
                    $client = S3Client::factory(array(
                            'key'    => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret,
                    ));
                     $key         = $studio_id.'/EncodedVideo/blank.txt';
                    $key1        = $studio_id.'/public/blank.txt';
                    $key2        = $studio_id.'/RawVideo/blank.txt';
                    $source_file = Yii::app()->getbaseUrl(true).'/blank.txt';
                    $acl         = 'public-read';
                    $bucket      = $bucketDetailsArr['bucket_name'];
                    $client->upload($bucket, $key, $source_file, $acl);
                    $client->upload($bucket, $key1, $source_file, $acl);
                    $client->upload($bucket, $key2, $source_file, $acl);
                }

                //Insert into PPV Buy
               $ppvBuy = new PpvBuy;
               $ppvBuy->studio_id = $studio_id;
               $ppvBuy->save();
               
               //Insert Default free content login
               $stdconfig = new StudioConfig;
               $stdconfig->config_key = 'free_content_login';
               $stdconfig->config_value = 1;
               $stdconfig->studio_id = $studio_id;
               $stdconfig->save();

               //Insert into Studio Manage Coupon
               $studioManageCoupon = new StudioManageCoupon;
               $studioManageCoupon->studio_id = $studio_id;
               $studioManageCoupon->save();
                
               // Create user in wordpress and  wpuser login
               if (WP_USE_THEMES == 1) {
               $wpuser_data = array(
                   'user_login'=> $_REQUEST['email'],
                   'first_name'  => $_REQUEST['name'],
                   'user_email'=> $_REQUEST['email'],
                   'user_pass'=> $_REQUEST['password']
               );
               $wpuser_id = wp_insert_user( $wpuser_data ) ; 
               add_user_meta( $wpuser_id, 'wpuser_type', '1');
               
                $username = $_REQUEST['email'];
                $password = $_REQUEST['password'];
                $wpuser = Yii::app()->common->getWpUserLogin($username,$password);
                }
                //Insert data into the user table 
                $enc = new bCrypt();         
                $encrypted_pass = $enc->hash($_REQUEST['password']);
                
                $umodel = new User();
                $umodel->studio_id = $studio->id;
                $umodel->first_name = $_REQUEST['name'];
                $umodel->email = $_REQUEST['email'];
                $umodel->encrypted_password = $encrypted_pass;
                $umodel->created_at = gmdate('Y-m-d H:i:s');
                $umodel->phone_no = @$_REQUEST['phone'];
                $umodel->signup_ip = $ip_address;
                $umodel->signup_location = serialize($geo_data);
                $umodel->is_active = 1;
                $umodel->is_sdk = 1;
                $umodel->role_id = 1;
                $umodel->signup_step = 1;
               if($umodel->save())
               {
                  $last_inserted_id=$umodel->id;
               }
                   
                //generate a autotoken in the oauth_registration table if we will excess free trial.              
                $data = OuathRegistration::model()->findByAttributes(array('studio_id'=>$studio_id));
                if(empty($data)){                
                    $OuathRegistration = new OuathRegistration();
                    $OuathRegistration->oauth_token=md5(time());
                    $OuathRegistration->studio_id=$studio_id;
                    $OuathRegistration->created_date=Date('Y-m-d H:i:s');
                    $OuathRegistration->created_by=$last_inserted_id;
                    $OuathRegistration->platform='mobile';
                    $OuathRegistration->save();
               }
                //Inserting into the front-end user table
                $user = new SdkUser;
                $user->is_studio_admin = 1;
                $user->email = $_REQUEST['email'];
                $user->studio_id = $studio_id;
                $user->signup_ip = $ip_address;
                $user->display_name = htmlspecialchars($_REQUEST['name']);
                $user->encrypted_password = $encrypted_pass;
                $user->status = 1;
                $user->created_date = new CDbExpression("NOW()");
                $user->save();
                //Inserting studio id in studio_config for site speed increase
				$landingPage = new StudioConfig();
				$landingPage->studio_id = $studio_id;
				$landingPage->config_key = 'hide_contents_landingpage';
				$landingPage->config_value = 1;
				$landingPage->save();
                
                //Inserting studio id and email to ticket master table
                 if ($user->save()) {
                    $tickettingdb=$this->getAnotherDbconnection();
                    $command = $tickettingdb->createCommand();
                    $command->insert('ticket_master', array(
                        'studio_id'=>$studio_id,
                        'studio_email'=>$_REQUEST['email'],
                        'studio_name'=>$_REQUEST['companyname'],
                        'user_id'=>$last_inserted_id,
                        'user_name'=>$_REQUEST['name'],
                        'user_type'=>1,
                        'user_sdk'=>1,
                        'studio_subscribed'=>1,
                        'creation_date'=>date("Y-m-d H:i:s")
                    ));
                    
                    $tickettingdb->active=false;
                 }   
                
                 /* Sanjib */
                $customField[0] = array(
                    'field_name' => 'name',
                    'input_type' => 1,
                    'form_type' => 1,
                    'field_label' => 'Full Name',
                    'placeholder' => 'Enter your name',
                    'field_id' => 'name',
                    'id_seq' => 1,
                    'status' => '1',
                    'is_required' => '1',
                    'is_default' => '1',
                    'is_unique' => '0',
                    'is_new' => 1
                );
                $customField[1] = array(
                    'field_name' => 'email',
                    'input_type' => 7,
                    'form_type' => 1,
                    'field_label' => 'Email',
                    'placeholder' => 'Emter your email id',
                    'field_id' => 'email_address',
                    'id_seq' => 2,
                    'status' => '1',
                    'is_required' => '1',
                    'is_default' => '1',
                    'is_unique' => '1',
                    'is_new' => 1
                );
                $customField[2] = array(
                    'field_name' => 'password',
                    'input_type' => 8,
                    'form_type' => 1,
                    'field_label' => 'Password',
                    'placeholder' => 'Enter your password',
                    'field_id' => 'join_password',
                    'id_seq' => 3,
                    'status' => '1',
                    'is_required' => '1',
                    'is_default' => '1',
                    'is_unique' => '0',
                    'is_new' => 1
                );
                $customField[3] = array(
                    'field_name' => 'confirm_password',
                    'input_type' => 8,
                    'form_type' => 1,
                    'field_label' => 'Confirm Password',
                    'placeholder' => 'Confirm Password',
                    'field_id' => 'confirm_password',
                    'id_seq' => 4,
                    'status' => '1',
                    'is_required' => '1',
                    'is_default' => '1',
                    'is_unique' => '0',
                    'is_new' => 1
                );

                for ($i = 0; $i <= 3; $i++) {
                    $customData = new CustomField();
                    $customData->studio_id = $studio_id;
                    $customData->field_name = $customField[$i]['field_name'];
                    $customData->input_type = $customField[$i]['input_type'];
                    $customData->form_type = 1;
                    $customData->field_label = $customField[$i]['field_label'];
                    $customData->placeholder = $customField[$i]['placeholder'];
                    $customData->field_id = $customField[$i]['field_id'];
                    $customData->id_seq = $customField[$i]['id_seq'];
                    $customData->status = $customField[$i]['status'];
                    $customData->is_required = $customField[$i]['is_required'];
                    $customData->is_default = $customField[$i]['is_default'];
                    $customData->is_unique = $customField[$i]['is_unique'];
                    $customData->is_new = $customField[$i]['is_new'];
                    $customData->save();
                }

                $traansData[0] = array('trans_key' => 'name', 'trans_value' => 'Full Name');
                $traansData[1] = array('trans_key' => 'name_placeholder', 'trans_value' => 'Enter your name');
                $traansData[2] = array('trans_key' => 'email_address', 'trans_value' => 'Email');
                $traansData[3] = array('trans_key' => 'email_address_placeholder', 'trans_value' => 'Emter your email id');
                $traansData[4] = array('trans_key' => 'join_password', 'trans_value' => 'Password');
                $traansData[5] = array('trans_key' => 'join_password_placeholder', 'trans_value' => 'Enter your password');
                $traansData[6] = array('trans_key' => 'confirm_password', 'trans_value' => 'Confirm Password');
                $traansData[7] = array('trans_key' => 'confirm_password_placeholder', 'trans_value' => 'Confirm Password');
                for ($i = 0; $i <= 7; $i++) {
                    TranslateKeyword::model()->insertNewKey($studio_id, $traansData[$i]['trans_key'], $traansData[$i]['trans_value'], 2);
                }
                 
                //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
                //ajit@muvi.com                
                $multiuser = User::model()->findAll('email=:email', array(':email' => trim($_REQUEST['email'])));
                //if present update the new password for all
                if(count($multiuser)){
                    $sql = "UPDATE user set encrypted_password ='".$encrypted_pass."' WHERE email='".trim($_REQUEST['email'])."'";
                    Yii::app()->db->createCommand($sql)->execute();
                }

                $_REQUEST['rememberMe'] = 0;

                $model = new LoginForm;
                $arr = array('email' => $_REQUEST['email'], 'password' => $_REQUEST['password'], 'rememberMe' => 0);
               
                $model->attributes = $arr;
                if ($model->validate() && $model->login()) {
                    $_REQUEST['domain'] = HTTP.$studio->domain;
					//if ((HOST_IP != '127.0.0.1') && (HOST_IP == '52.0.64.95')) {
						Yii::app()->email->emailToSales($_REQUEST);
						Yii::app()->email->emailToUser($_REQUEST);
						/*Add user to Hubspot*/
						$nm = explode(" ", $_REQUEST['name'], 2);
						Hubspot::AddToHubspot($_REQUEST['email'],$nm[0],$nm[1],$_REQUEST['phone']);
						/*Add user to madmimi*/
						$user = array('email' => $_REQUEST['email'], 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
						require('MadMimi.class.php');
						$mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
						$mimi->AddUser($user);
					//}
                    /*** END of hubspot and madmimi ****/
                    $addtional_param =  isset($_REQUEST['purchasetype']) && trim($_REQUEST['purchasetype'])? '?purchasetype='.$_REQUEST["purchasetype"] : '';   
                    $this->redirect($this->createUrl('signup/typeofcontent' . $addtional_param));
                } else {
                    $this->redirect($this->createUrl('signup'));
                    exit;
                }
            }
        }
        if (isset($_REQUEST['email']) && $_REQUEST['email'] != '' && isset(Yii::app()->session['studio_id']) && Yii::app()->session['studio_id'] && Yii::app()->session['back_btn']) {

                $studio_id = Yii::app()->session['studio_id'];
                $studio = new Studio();
                $data = array(
                    'name' => $_REQUEST['companyname'],
                    'phone' => @$_REQUEST['phone'], 
                );
                $studio->updateStudio($studio_id,$data);
                $umodel = new User();
                $enc = new bCrypt();         
                $encrypted_pass = $enc->hash($_REQUEST['password']);
                $userData = array(
                    'first_name' => $_REQUEST['name'],
                    'encrypted_password' => $encrypted_pass,
                    'phone_no' => @$_REQUEST['phone']
                    
                );
                $umodel->updateUser($studio_id,$userData);
                $user = new SdkUser;
                $studioUserData = array(
                    'display_name' => htmlspecialchars($_REQUEST['name']),
                    'encrypted_password' => $encrypted_pass  
                );
                $user->updateSdkUser($studio_id,$studioUserData);
                $_REQUEST['rememberMe'] = 0;

                $model = new LoginForm;
                $arr = array('email' => $_REQUEST['email'], 'password' => $_REQUEST['password'], 'rememberMe' => 0);
                $model->attributes = $arr;
                //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
                //ajit@muvi.com
                $multiuser = User::model()->findAll('email=:email', array(':email' => trim($_REQUEST['email'])));
                //if present update the new password for all
                if(count($multiuser)){
                    $sql = "UPDATE user set encrypted_password ='".$encrypted_pass."' WHERE email='".trim($_REQUEST['email'])."'";
                    Yii::app()->db->createCommand($sql)->execute();
                }

                if ($model->validate() && $model->login()) {
                    $_REQUEST['domain'] = 'http://' . $studio->domain;
                    $addtional_param =  isset($_REQUEST['purchasetype']) && trim($_REQUEST['purchasetype'])? '?purchasetype='.$_REQUEST["purchasetype"] : '';   
                    $this->redirect($this->createUrl('signup/typeofcontent' . $addtional_param));
                } else {
                    $this->redirect($this->createUrl('signup'));
                    exit;
                }
        }
        if(isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] && $_REQUEST['studio_id'] == Yii::app()->session['studio_id']){
            $studio_id = $_REQUEST['studio_id'];
            $std = new Studio();
            @$studio = $std->findByPk($studio_id);
            $usr = new User();
            @$user = $usr->findByAttributes(array('studio_id' => $studio_id));
        }
       if(WP_USE_THEMES == 1) {
          $wpuser = wp_get_current_user();
          $user_info = get_userdata( $wpuser->ID ); 
          $wpuser_arr = array(
              'id'=>$wpuser->ID,
              'name'=> get_user_meta( $wpuser->ID, 'wpuser_name', true ),
              'email'=> $user_info->user_email,
              'password'=>get_user_meta( $wpuser->ID, 'wpuser_password', true )
          );
        }
        
	Yii::app()->theme = 'signup';
        $this->render('index', array('country' => $country, 'region' => $region, 'domain' => $domain,'studio' => @$studio,'user' => @$user, 'wpuser'=>@$wpuser_arr));
    }

    /**
     * @method public domainName() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    //Sign up Step 2
    function actionDomainName() {
        //Page Meta 
        $meta = Yii::app()->common->pagemeta('domainname');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];

        $domain = Yii::app()->common->getStudiodomain();
        if (Yii::app()->user->isGuest) {
            $this->redirect($this->createUrl('signup/index'));
            exit;
        } else {
            if (Yii::app()->user->signup_step > 0) {
                if (Yii::app()->user->studio_subdomain) {
                    $this->redirect($this->createUrl('signup/typeofContent'));
                    exit;
                } else {
                    if (isset($_REQUEST['is_submit']) && $_REQUEST['is_submit']) {
                        $string = str_replace(' ', '', strtolower($_REQUEST['subdomain']));
                        $string = preg_replace('/[^A-Za-z0-9]/', '', $string);
                        $studio = Studios::model()->findByPk(Yii::app()->user->studio_id);
                        $studio->subdomain = $string;
                        $studio->domain = $string . '.' . $domain;
                        $studio->save();

                        $user = User::model()->findByPk(Yii::app()->user->id);
                        $user->signup_step = 0;
                        $user->save();
                        Yii::app()->user->signup_step = 0;

                        $this->addDefaultContentType(Yii::app()->user->studio_id);
                        Yii::app()->user->setState('studio_subdomain', $string);
                        $this->redirect($this->createUrl('signup/typeofContent'));
                        exit;
                    }
                    $this->render('domain', array('domain' => $domain));
                }
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
                exit;
            }
        }
    }

    /**
     * @method public typeofContent() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    //Sign up Step 3
    function actionTypeofContent() {
        //Page Meta 
        $meta = Yii::app()->common->pagemeta('signuptemplate');
        $this->pageTitle = "Add and manage your Video content - Muvi";
        $this->pageDescription = "Add, edit, upload all kinds of video content like; Movies, TV Shows, Events etc";
        if (isset(Yii::app()->user->id) && Yii::app()->user->id && Yii::app()->user->signup_step == 1) {
            if (isset(Yii::app()->user->studio_subdomain) && Yii::app()->user->studio_subdomain) {
                //Getting all available content types of the studio
                $studio_id = Yii::app()->user->studio_id;
                //$contenttypes = ContentType::model()->findAll();
                $dbcon = Yii::app()->db;
				$cc_cmd = $dbcon->createCommand("SELECT * FROM content_category WHERE studio_id={$studio_id} ORDER BY id ASC");
				$data = $cc_cmd->queryAll();
				
				$template = Template::model();
                $templates = $template->find('status=:status AND is_default=:is_default', array(':status' => '1', ':is_default' => 1));
                $this->render('contentType', array('data' => $data, 'contentTypes' => @$contenttypes, 'template' => @$templates));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
                exit;
            }
        } 
        /*elseif (isset(Yii::app()->user->id) && Yii::app()->user->id && Yii::app()->user->signup_step == 2) {
            $this->redirect(Yii::app()->getBaseUrl(true) . '/signup/paymentGateway');
            exit;
        }*/
        else {
            $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
            exit;
        }
    }
    
    /**
     * @method public addContent() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionAddContent() {
        $studio_id = Yii::app()->common->getStudioId();
        if (isset($_REQUEST['displayname']) && $_REQUEST['displayname']) {

            $contentModel = new StudioContentType();
            $data = $contentModel->find('(display_name=:display_name OR content_types_id=:content_types_id) AND studio_id=:studio_id', array(':display_name' => $_REQUEST['displayname'], ':content_types_id' => $_REQUEST['content_types_id'], ':studio_id' => Yii::app()->user->studio_id));
            if ($data) {
                $arr['succ'] = 0;
                $arr['msg'] = '';
                $data = $data->attributes;
                if ($data['content_type'] == $_REQUEST['content_type']) {
                    $arr['msg'] = "Oops! Sorry content type exsists with your studio";
                } elseif (strtolower($data['display_name']) == strtolower($_REQUEST['displayname'])) {
                    $arr['msg'] = "Oops! Sorry display name exsists with your studio";
                }
            } else {
                $content = new StudioContentType();
                $content->studio_id = Yii::app()->user->studio_id;
                $content->content_types_id = $_REQUEST['content_type'];
                $content->display_name = $_REQUEST['displayname'];
                $content->permalink = Yii::app()->common->formatPermalink($_REQUEST['displayname']);
                $content->muvi_alias = $this->getMuvialias($_REQUEST['content_type']);
                $content->created_at = gmdate('Y-m-d');
                $content->is_enabled = 1;
                $content->save();
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
            echo json_encode($arr);
            exit;
        }
        //Getting all available content types of the studio
        $stdcont = StudioContentType::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_enabled' => '1'), array('order' => 'id asc'));
        $templates = Template::model()->findAllByAttributes(array('status' => 1));

        //Getting all available content types of the studio
        $contenttypes = ContentType::model()->findAll();
        $this->render('contentType', array('data' => $stdcont, 'contentTypes' => $contenttypes, 'templates' => $templates));
    }

    /**
     * @method public updateContent() Update the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionUpdateContent() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id']) {
            $id = $_REQUEST['content_id'];
            $contentModel = new StudioContentType();
            $data = $contentModel->find('(display_name=:display_name OR content_types_id=:content_types_id) AND studio_id=:studio_id AND id!=:id', array(':display_name' => $_REQUEST['displayname'], ':content_types_id' => $_REQUEST['content_types_id'], ':studio_id' => $studio_id, 'id' => $id));
            if ($data) {
                $data = $data->attributes;
                if ($data['content_type'] == $_REQUEST['content_type']) {
                    $arr['msg'] = "Oops! Sorry content type exsists with your studio";
                } elseif (strtolower($data['display_name']) == strtolower($_REQUEST['displayname'])) {
                    $arr['msg'] = "Oops! Sorry display name exsists with your studio";
                }
            } else {
                $contentModel = StudioContentType::model()->findByPk($id);
                $contentModel->display_name = $_REQUEST['displayname'];
                $contentModel->permalink = Yii::app()->common->formatPermalink($_REQUEST['displayname']);
                $contentModel->save();
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public removeContent() Update the content details
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionRemoveContent() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['content_type_id']) && $_REQUEST['content_type_id']) {
            $id = $_REQUEST['content_type_id'];
            $sql = "SELECT F.id FROM films F,movie_streams S WHERE F.id=S.movie_id AND S.studio_id=" . $studio_id . " AND content_type_id=" . $id;
            $dbcon = Yii::app()->db;
            $data = $dbcon->createCommand($sql)->queryAll();
            if ($data) {
                $arr['msg'] = "Oops! Sorry this contnet type contains some data\n Remove all data under this category first and then try again!";
            } else {
                StudioContentType::model()->deleteByPk($id, 'studio_id=:studio_id', array(':studio_id' => $studio_id));
                //$contentModel =StudioContentType::model()->findByPk(); // assuming there is a post whose ID is 10
                //$contentModel->delete(); // delete the row from the database table
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public checkEmail() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionCheckEmail() {
        $arr['succ'] = 0;
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $usr = new User;
            $data = $usr->findByAttributes(array('email' => $_REQUEST['email']));
            if ($data) {
                $arr['succ'] = 0;
            } else {
                $arr['succ'] = 1;
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method public checkDomainEmail() Check both email and Subdomain if its already exists with us or not
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actioncheCkDomainEmail() {
        $arr = array('succ' => 0, 'email_error' => 0, 'domain_error' => 0, 'invalid_url' => 0);
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $usr = new User;
            //6312: Multiple stores PER email ID: Not able to use mail id in Free Trial sign up
            //ajit@muvi.com
            //except studio admin email, other admin email can create new studio
            $data = $usr->findByAttributes(array('email' => $_REQUEST['email'],'role_id' => 1));
            
            if ($data) {
                $studio = Studios::model()->findByPk($data->studio_id);
                if (isset($studio) && !empty($studio) && $studio->status == 4 && $studio->is_subscribed == 0 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Canceled
                    $arr['succ'] = 0;
                    $arr['purchase_error'] = 1;
                    $arr['uniqid'] = $studio->uniqid;
                } else if (isset($studio) && !empty($studio) && $studio->status == 0 && $studio->is_subscribed == 0 && $studio->is_deleted == 1 && $studio->is_default == 0) {//Deleted
                    $arr['succ'] = 0;
                    $arr['purchase_error'] = 1;
                    $arr['uniqid'] = $studio->uniqid;
                } else {
                    $arr['succ'] = 0;
                    $arr['email_error'] = 1;
                }
            } else {
                $arr['succ'] = 1;
            }
            
            if (isset($_REQUEST['subdomain']) && $_REQUEST['subdomain']) {
                if (filter_var('http://' . $_REQUEST['subdomain'], FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)) {
                    $string = str_replace(' ', '', strtolower($_REQUEST['subdomain']));
                    $subdomain = str_replace('www.', '', preg_replace('/[^A-Za-z0-9.]/', '', $string));
                    $data = Studios::model()->find('domain=:domain OR reserved_domain=:domain', array(':domain' => $subdomain));
                    if ($data) {
                        $arr['succ'] = 0;
                        $arr['domain_error'] = 1;
                    }
                } else {
                    $arr['succ'] = 0;
                    $arr['domain_error'] = 0;
                    $arr['invalid_url'] = 1;
                }
            }
        }
        echo json_encode($arr);
        exit;
    }
    function actioncheckDomainName() {
        if (isset($_REQUEST['company_name']) && $_REQUEST['company_name']) {
            $res =$this->checkDomainNameRecursion($_REQUEST['company_name']);
        }
        else
        {
         $res = '';
        
        }
        echo $res;
    }

    function checkDomainNameRecursion($subdomain) {
        $string = str_replace(' ', '', strtolower($subdomain));
        $subdomain = preg_replace('/[^A-Za-z0-9]/', '', $string);
        if (HOST_IP == '52.0.64.95') {
          $domain_end = "idogic.com";   
        }
        else {
        $domain_end = "muvi.com";
        }
        $domain = $subdomain . "." . $domain_end;
        $data = Studios::model()->find('domain=:domain OR reserved_domain=:domain', array(':domain' => $domain));
        
        if (count($data)>0) {
            $new_subdomain = $subdomain . mt_rand(2, 50);
            $this->checkDomainNameRecursion($new_subdomain);          
        } else {
            //$arr['succ'] = 1;
            echo $domain;
        }
    }

    /**
     * @method public checkEmail() Get user info and Register it with our Studio
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionCheckSubdomain() {
        $arr['succ'] = 0;
        if (isset($_REQUEST['subdomain']) && $_REQUEST['subdomain']) {
            $string = str_replace(' ', '', strtolower($_REQUEST['subdomain']));
            $subdomain = preg_replace('/[^A-Za-z0-9]/', '', $string);
            $data = Studios::model()->findByAttributes(array('subdomain' => $subdomain));
            if ($data || in_array($subdomain, $this->reservedDomain)) {
                $arr['succ'] = 0;
            } else {
                $arr['succ'] = 1;
            }
        }
        echo json_encode($arr);
        exit;
    }

    /**
     * @method protected setupStudio(int $studio_id) Setup the studio data
     * @author GDR<support@muvi.com>
     * @return boolen true/false
     */
    protected function setupStudio($studio_id, $templateName = '',$dummy_data = 1) {
        if ((Yii::app()->user->studio_id == $studio_id) && Yii::app()->user->studio_subdomain && $templateName != '') {
            $ip_address = CHttpRequest::getUserHostAddress();
            $default_color = '';
            $templates = Template::model()->findByAttributes(array('code' => $templateName));
            if (count($templates)) {
                $default_color = $templates->default_color;
            }

            $user = User::model()->findByPk(Yii::app()->user->id);
            $user->signup_step = 0;
            $user->save();
            Yii::app()->user->signup_step = 0;
            
            $studio_id = Yii::app()->user->studio_id;
            $studio = Studios::model()->findByPk($studio_id);
            $section = StudioBannerStyle::model()->findByAttributes(array('studio_id' => $studio_id));
            //setting the banner style
            if ($templateName == 'classic-byod' || $templateName == 'modern-byod'){
                $is_default='1';
                $bs=new BannerStyle();
                $banner=$bs->findByAttributes(array('is_default'=>$is_default)); 
                if (empty($section)) {
                    $section = new StudioBannerStyle();
                    $section->studio_id=$studio_id;
                    $section->banner_style_code =$banner->banner_style_code;
                    $section->h_banner_dimension =$banner->h_banner_dimension;
                    $section->v_banner_dimension =$banner->v_banner_dimension;
                    $section->auto_scroll=1;
                    $section->scroll_interval=$banner->scroll_interval;
                    $section->no_of_active_image=1;
                    $section->save();
                }
            } 
            //Setting dummy data during sign up    
            //if($ip_address == '117.247.67.108'){
                $menusObj = new Menu();
                $menusObj->studio_id = $studio_id;
                $menusObj->position ='top';
                $menusObj->status =1;
                $menusObj->save();
            if($dummy_data == 1){
                //Setting the dummydata
                $dummy = new DummyData();
                $dummy->studio_id = $studio_id;
                $dummy->status = '0';
                $dummy->save();
                $this->addDefaultContentType($studio_id,$menusObj->id);
                $this->CreateSampleData($studio_id);   
            }else{
                $dummy = new DummyData;
                $dummy->studio_id = $studio_id;
                $dummy->status = '2';
                $dummy->start_date = date('Y-m-d H:i:s');
                $dummy->end_date = date('Y-m-d H:i:s');
                $dummy->save();
            }                        
            $terms_id = $this->setDefaultContent($studio_id);
            
            $studio->terms_page = $terms_id;
            $studio->status = 1;
            $theme_folder = Yii::app()->user->studio_subdomain;
            $studio->theme = $theme_folder;
            $studio->parent_theme = $templateName;
            $studio->default_color = $default_color;
            $studio->save();
            Yii::app()->user->setState('status', 1);
            Yii::app()->user->setState('theme', $theme_folder);

            //Insert the data into the Client application table 
            

            // Creating the folder in the name of Subdomain and copying all the files inside the sdk theme folder

            if (!is_dir(ROOT_DIR . 'themes/' . $theme_folder)) {
                mkdir(ROOT_DIR . 'themes/' . $theme_folder);
            }
            //chown(ROOT_DIR.'themes/'.Yii::app()->user->studio_subdomain, 'root');
            chmod(ROOT_DIR . 'themes/' . $theme_folder, 0777);
            // shell command 
            $dest = ROOT_DIR . 'themes/' . $theme_folder . '';
            $source = ROOT_DIR . 'sdkThemes/' . $templateName;
            $return = $this->recurse_copy($source, $dest);
            //$cmd = 'sudo cp -R '.$source." ".$dest;
            //exec(escapeshellcmd($cmd),$output,$return);
            ///echo $cmd;print_r($output);exit;
            if(!empty($section)){
                $file = $section['banner_style_code']."/banner.html";
                $root = ROOT_DIR . 'bannerStyle/'.$file;
                chmod(ROOT_DIR . 'bannerStyle/', 0777);
                chmod($root, 0777);
                $path = ROOT_DIR . 'themes/' . $theme_folder . '/views/layouts/banner.html';
                if (@file_exists($path) && @file_exists($root)){
                    copy($root, $path);
                }
             }
            return $return;
        } else {
            return false;
        }
    }
    
    /*Added by RK for Dummydata at signup*/
    public function setDummyData($studio_id) {
        $url = Yii::app()->getBaseUrl(true) . '/signup/createSampleData';
        $fields = array('studio_id' => $studio_id);
        $fields_string = '';

        //url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }     
    
    public function CreateSampleData($studio_id){        
        $dummy = new DummyData();
        $dummies = $dummy->findAllByAttributes(array('status' => '0'));
        foreach ($dummies as $dummy_rec) {
            $dummy_rec->start_date = gmdate('Y-m-d H:i:s');
            $dummy_rec->status = '1';
            $dummy_rec->save();
            $studio_id = $dummy_rec->studio_id;
            $source_studio_id = DUMMY_DATA_STUDIO;
            //Setting the Banners
            Yii::app()->general->setBanners($studio_id, $source_studio_id);

            //Setting Celebrity
            Yii::app()->general->setCelebrity($studio_id, $source_studio_id);
            
            $has_video_uploaded = 0;
           /* $content = new StudioContentType();
            $content_types = $content->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id ASC'));
            foreach ($content_types as $content_data) {
                if ($content_data->content_types_id == 1) {*/
                    //add dummy Movie
            
            
                    $has_video_uploaded = Yii::app()->general->setDummymovie($studio_id, $source_studio_id, 1, $has_video_uploaded);
              
//} else if ($content_data->content_types_id == 3) {
                    //add dummy Tv
                    $has_video_uploaded = Yii::app()->general->setDummytv($studio_id, $source_studio_id, 3, $has_video_uploaded);
                //}
            //}
            
            Yii::app()->general->setVideoGallery($studio_id, $source_studio_id);
            
            
            $dummy_rec->end_date = gmdate('Y-m-d H:i:s');
            $dummy_rec->status = '2';
            $dummy_rec->save();            
        }        
    }
    
    function setDefaultContent($studio_id = Null) {
        if (isset($studio_id) && intval($studio_id)) {
            $ip_address = CHttpRequest::getUserHostAddress();

            $reason = new CancelReason();
            $reason->studio_id = $studio_id;
            $reason->reason = htmlspecialchars("Price is too expensive");
            $reason->save();

            $reason = new CancelReason();
            $reason->studio_id = $studio_id;
            $reason->reason = htmlspecialchars("Other");
            $reason->save();

            $menu = new Menu();
            $menu->studio_id = $studio_id;
            $menu->position = 'footer';
            $menu->status = '1';
            $menu->save();
            
            $page_name = 'About Us';
            $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
            $permalink = Yii::app()->common->formatPermalink($page_name);
            $page = new Page();
            $page->title = htmlspecialchars($page_name);
            $page->permalink = $permalink;
            $page->content = htmlspecialchars($page_content);
            $page->studio_id = $studio_id;
            $page->created_date = new CDbExpression("NOW()");
            $page->created_by = Yii::app()->user->id;
            $page->ip = $ip_address;
            $page->save();

            //Adding About us to footer Menu
            $menuitem = new MenuItem();
            $menuitem->studio_id = $studio_id;
            $menuitem->menu_id = $menu->id;
            $menuitem->link_type = 1;
            $menuitem->parent_id = '0';
            $menuitem->status = '1';
            $menuitem->value = $page->id;
            $menuitem->title = htmlspecialchars($page_name);
            $menuitem->permalink = $permalink;
            $menuitem->id_seq = 1;
            $menuitem->save();                                   
            
            $urlRouts['permalink'] = $permalink;
            $urlRouts['mapped_url'] = '/page/show/permalink/' . $permalink;
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
            
            $page_name = 'Terms & Privacy Policy';
            $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
            $permalink = Yii::app()->common->formatPermalink($page_name);
            $page = new Page();
            $page->title = htmlspecialchars($page_name);
            $page->permalink = $permalink;
            $page->content = htmlspecialchars($page_content);
            $page->studio_id = $studio_id;
            $page->created_date = new CDbExpression("NOW()");
            $page->created_by = Yii::app()->user->id;
            $page->ip = $ip_address;
            $page->save();
            
            //Adding About us to footer Menu
            $menuitem = new MenuItem();
            $menuitem->studio_id = $studio_id;
            $menuitem->menu_id = $menu->id;
            $menuitem->link_type = 1;
            $menuitem->parent_id = '0';
            $menuitem->status = '1';
            $menuitem->value = $page->id;
            $menuitem->title = htmlspecialchars($page_name);
            $menuitem->permalink = $permalink;
            $menuitem->id_seq = 2;
            $menuitem->save();              
            
            $urlRouts['permalink'] = $permalink;
            $urlRouts['mapped_url'] = '/page/show/permalink/' . $permalink;
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);
            $terms_id = $page->id;
        } else {
            $terms_id = 0;
        }
        
        return $terms_id;
    }    

    /**
     * @method public addDefaultContentType(string $studio_id) Add default Content Type to the studio
     * @return bool true/false
     * @author GDR<support@muvi.com>
     */
    function addDefaultContentType($studio_id = Null, $menuobj_id = null) {
        if (isset($studio_id) && trim($studio_id)) {
            $builder = Yii::app()->db->schema->commandBuilder;
            $command = $builder->createMultipleInsertCommand('studio_content_types', array(
                array('display_name' => 'Movie', 'permalink' => 'movie','content_category_value'=>1, 'studio_id' => $studio_id, 'content_types_id' => '1', 'muvi_alias' => 'movie','created_date' => new CDbExpression("NOW()"), 'is_enabled' => 1),
                array('display_name' => 'TV', 'permalink' => 'tv','content_category_value'=>2, 'studio_id' => $studio_id, 'content_types_id' => '3', 'muvi_alias' => 'tv','created_date' => new CDbExpression("NOW()"), 'is_enabled' => 1)
            ));
            $command->execute();
			
			$ip_address = CHttpRequest::getUserHostAddress();
			$builder1 = Yii::app()->db->schema->commandBuilder;
			$command1 = $builder1->createMultipleInsertCommand('content_category', array(
                array('category_name' => 'Movie', 'studio_id' => $studio_id, 'binary_value' => 1, 'created_date' => new CDbExpression("NOW()"), 'ip' => $ip_address,'permalink'=>'movie'),
                array('category_name' => 'TV', 'studio_id' => $studio_id, 'binary_value' => 2, 'created_date' => new CDbExpression("NOW()"), 'ip' => $ip_address,'permalink'=>'tv')
            ));
			$command1->execute();
			
			// Insert into menu table
			$menuItemObj = new MenuItem();
			$urlRoutingObj = new UrlRouting();
			$arr = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id'=>$studio_id)), 'id', 'category_name');
			//$arr = array('1'=>'Movie','2'=>'TV');
			foreach($arr AS $key=>$val){
				//Insert Into Menu Items 
				$menuItemObj->title= $val;
				$menuItemObj->menu_id= $menuobj_id;
				$menuItemObj->permalink = strtolower($val);
				$menuItemObj->studio_id = $studio_id;
				$menuItemObj->value = $key;
				$menuItemObj-> setIsNewRecord(true);
				$menuItemObj-> setPrimaryKey(NULL);
				$menuItemObj->save();
				$menu_id =$menuItemObj->id;
				$routsArr['permalink'] = strtolower($val);
				$routsArr['mapped_url'] = '/media/list/menu_item_id/'.$menu_id;
				$urlRoutingObj->addUrlRouting($routsArr,$studio_id);
			}
			return true;
        } else {
            return false;
        }
    }

    /**
     * @method public updateTemplate() Assigning a template to a studio 
     * @return boolen true/false
     * @author GDR<support@muvi.com>
     */
    function actionUpdateTemplate() {
        $theme = '';
        $content_sum = $app_sum = $menu_sum = 0;
        $dummy_data = 1;
        $studio_id = Yii::app()->user->studio_id;
        if(((isset($_REQUEST['content']) && count($_REQUEST['content'])!=0) || (isset($_REQUEST['content_physical']) && count($_REQUEST['content_physical'])!=0)) && (isset($_REQUEST['apps']) && count($_REQUEST['apps'])!=0) && (isset($_REQUEST['monetization']) && count($_REQUEST['monetization'])!=0)):
            $content      = isset($_REQUEST['content']) ? $_REQUEST['content'] : array();
            $apps         = isset($_REQUEST['apps']) ? $_REQUEST['apps'] : array();
            $monetization = isset($_REQUEST['monetization']) ? $_REQUEST['monetization'] : array();
            $content_sum = array_sum($content);
            $content_type = StudioContent::model()->updateContentSettings($studio_id,$content_sum);
            $app_sum = array_sum($apps);
            $app_menu = StudioAppMenu::model()->updateAppSettings($studio_id,$app_sum);
            $menu_sum = array_sum($monetization);
            $res = MonetizationMenuSettings::model()->updateMenuSettings($studio_id,$menu_sum);
            if($content_sum){
                $child_content_sum = ($content_sum==4)?24:($content_sum==7)?29:7;
                $child_content_type = StudioContent::model()->updateChildContentSettings($studio_id,$child_content_sum);
            }
        endif; 
        // Activate physical in signup step 2 
        $content_physical = isset($_REQUEST['content_physical']) ? $_REQUEST['content_physical'] : array();
        if(count($content_physical))
        {
        $sql = "INSERT INTO studio_extensions (id,extension_id,studio_id, social_sharing_status, comment_status,status,start_date) VALUES(NULL, '3', ".$studio_id.", '1','1','1',NOW())";
        $data = Yii::app()->db->createCommand($sql)->execute();
        } 
        if($content_sum == 4){
            $template  = 'audio-streaming';
            $dummy_data = 0;
        }elseif($content_sum == 0 && !empty($content_physical)){
            $template  = 'physical';
            $dummy_data = 0;
        }elseif($content_sum == 3 && !empty($content_physical) ){
            $template  = 'classic-byod';
        }else{
            $template  = 'modern-byod';
        }
        
        unset(Yii::app()->session['studio_id']);
        unset(Yii::app()->session['back_btn']);
        if (isset(Yii::app()->user->theme) && Yii::app()->user->theme) {
            $theme = Yii::app()->user->theme;
        }
        if (Yii::app()->user->studio_id) {
            $this->setupStudio(Yii::app()->user->studio_id, $template,$dummy_data);
            echo Yii::app()->user->studio_id;
        }
        //sleep(30);
        //$this->redirect($this->createUrl('admin/dashboard'));
        //$this->redirect($this->createUrl('signup/paymentGateway'));
        exit;
    }

    /**
     * @method public paymentGateway() PaymentGateway information
     * @author GDR<support@muvi.com>
     * @return HTML paymetn template
     */
    //Sign up Step 4
    function actionPaymentGateway() {
        //Page Meta 
        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && Yii::app()->user->signup_step == 2) {
            $meta = Yii::app()->common->pagemeta('signuppaymentmethod');
            $this->pageTitle = $meta['title'];
            $this->pageDescription = $meta['description'];
            $this->render('paymentMethod');
        } else {
            $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
        }
    }

    /**
     * @method public signupTerms() Defines the terms and condition of Signup
     * @author GDR<support@muvi.com>
     * @return HTML Terms template
     */
    //Sign up Step 5
    function actionSignupTerms() {
        //Page Meta 
        $meta = Yii::app()->common->pagemeta('signupterms');
        $this->pageTitle = $meta['title'];
        $this->pageDescription = $meta['description'];
        $this->render('signupterms');
    }

    protected function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
        return true;
    }

    protected function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * @method public paymentMethod() To accept card information from the Client
     * @author GDR<support@muvi.com>
     * @return HTML paymetn template
     */
    /* Payment details data saving */

    /**
     * This method is no more
     * 
     * @method public CreateConsumer To accept card information from the Client
     * @author SNL<support@muvi.com>
     * @return JSON object
     */
    public function actionCreateConsumer() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudioId();
        $ip_address = CHttpRequest::getUserHostAddress();
        //print '<pre>';print_r($_REQUEST);exit;
        if (isset($_REQUEST['card_number']) && !empty($_REQUEST['card_number'])) {
            $config = Yii::app()->common->getConfigValue(array('monthly_charge', 'trial_period'), 'value');

            $ciModel = New CardInfos;
            $data = array();
            $ciModel->user_id = $user_id;
            $ciModel->studio_id = $studio_id;
            $ciModel->payment_method = 'first_data';
            $ciModel->plan_amt = $config[0]['value'];
            $data['amount'] = 0;
            $data['card_number'] = $_REQUEST['card_number'];
            //$ciModel->card_number = $data['card_number'];
            $ciModel->card_holder_name = $data['card_name'] = $_REQUEST['card_name'];
            $ciModel->exp_month = $_REQUEST['exp_month'];
            $ciModel->exp_year = $_REQUEST['exp_year'];
            $data['exp'] = ($_REQUEST['exp_month'] < 10) ? '0' . $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2) : $_REQUEST['exp_month'] . substr($_REQUEST['exp_year'], -2);
            $data['cvv'] = $_REQUEST['cvv'];
            $address = $_REQUEST['address1'];
            $ciModel->address1 = $address;
            $ciModel->address2 = $data['address2'] = $_REQUEST['address2'];
            $ciModel->city = (isset($_REQUEST['city']) && trim($_REQUEST['city'])) ? $_REQUEST['city'] : '';
            $ciModel->state = (isset($_REQUEST['state']) && trim($_REQUEST['state'])) ? $_REQUEST['state'] : '';
            $ciModel->zip = $data['zip'] = (isset($_REQUEST['zip']) && trim($_REQUEST['zip'])) ? $_REQUEST['zip'] : '';

            $address .= (trim($_REQUEST['address2'])) ? ", " . trim($_REQUEST['address2']) : '';
            $address .= ", " . $_REQUEST['city'];
            $address .= ", " . $_REQUEST['state'];
            $data['address'] = $address;

            if (IS_LIVE_FIRSTDATA) {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
            } else {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
            }

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);
            if ($data['zip']) {
                $firstData->setCreditCardZipCode($data['zip']);
            }

            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            if ($data['address']) {
                $firstData->setCreditCardAddress($data['address']);
            }

            $firstData->process();

            //print '<pre>';print_r($firstData);exit;

            $card_last_fourdigit = str_replace(substr($data['card_number'], 0, strlen($data['card_number']) - 4), str_repeat("#", strlen($data['card_number']) - 4), $data['card_number']);
            $studio = Studios::model()->findByPk($studio_id);
            $req['name'] = Yii::app()->user->first_name;
            $req['email'] = Yii::app()->user->email;
            $req['companyname'] = $studio->name;
            $req['domain'] = 'http://' . $studio->domain;
            $req['card_last_fourdigit'] = $card_last_fourdigit;

            // Check
            if ($firstData->isError()) {
                $res['isSuccess'] = 0;
                $res['Code'] = $firstData->getErrorCode();
                $res['Message'] = 'Invalid Card details entered. Please check again';
                
                $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();
                
                //Keeping card error logs
                $ceModel = New CardErrors;
                $ceModel->studio_id = $studio_id;
                $ceModel->response_text = serialize($firstData);
                $ceModel->transaction_type = 'Create Consumer';
                $ceModel->created = new CDbExpression("NOW()");
                $ceModel->save();
                
                //Send an email to developer and support
                Yii::app()->email->errorMessageMailToSales($req);
            } else {
                $respons_type = $firstData->getBankResponseType();

                if ($respons_type == 'D') {
                    //Charge forceble in card validation and refund an amount
                    $data['amount'] = 0.1;
                    $data['type'] = $firstData->getCreditCardType();
                    $firstData = $this->authenticateToCard($data);
                }

                if ($firstData->getBankResponseType() == 'S') {
                    $res['isSuccess'] = 1;
                    $res['TransactionRecord'] = $firstData->getTransactionRecord();
                    $res['Code'] = $firstData->getBankResponseCode();

                    $ciModel->status = $res['Message'] = $firstData->getBankResponseMessage();
                    $ciModel->reference_no = $firstData->getTransactionTag();
                    $ciModel->card_type = $firstData->getCreditCardType();
                    //$ciModel->card_last_fourdigit = $firstData->getCreditCardNumber();
                    $ciModel->card_last_fourdigit = $card_last_fourdigit;
                    $ciModel->auth_num = $firstData->getAuthNumber();
                    $ciModel->token = $firstData->getTransArmorToken();

                    $ciModel->response_text = serialize($firstData);
                    $ciModel->ip = CHttpRequest::getUserHostAddress();
                    $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                    $ciModel->created = new CDbExpression("NOW()");

                    $ciModel->save();

                    /*
                      $payment_detail =  Yii::app()->common->getPaymentDetails();
                      $spgModel = New StudioPaymentGateways;
                      $spgModel->gateway_id = 1;
                      $spgModel->short_code = 'paypal';
                      $spgModel->studio_id = $studio_id;
                      $spgModel->api_username = $payment_detail['paypal_api_username'];
                      $spgModel->api_password = $payment_detail['paypal_api_password'];
                      $spgModel->api_signature = $payment_detail['paypal_api_signature'];
                      $spgModel->api_version = $payment_detail['paypal_api_version'];
                      $spgModel->api_mode = $payment_detail['paypal_api_mode'];
                      $spgModel->status = 1;
                      $spgModel->save();
                     * *
                     */

                    $user = User::model()->findByPk($user_id);
                    $user->signup_step = 0;
                    $user->is_active = 1;
                    $user->save();
                    Yii::app()->user->signup_step = 0;
                    Yii::app()->user->setState('signup_step', 0);

                    $reason = new CancelReason();
                    $reason->studio_id = $studio_id;
                    $reason->reason = htmlspecialchars("Price is too expensive");
                    $reason->save();

                    $reason = new CancelReason();
                    $reason->studio_id = $studio_id;
                    $reason->reason = htmlspecialchars("Other");
                    $reason->save();

                    $page_name = 'About Us';
                    $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
                    $permalink = Yii::app()->common->formatPermalink($page_name);
                    $page = new Page();
                    $page->title = htmlspecialchars($page_name);
                    $page->permalink = $permalink;
                    $page->content = htmlspecialchars($page_content);
                    $page->studio_id = $studio_id;
                    $page->created_date = new CDbExpression("NOW()");
                    $page->created_by = Yii::app()->user->id;
                    $page->ip = $ip_address;
                    $page->save();

                    $page_name = 'Terms & Privacy Policy';
                    $page_content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras erat nibh, viverra in ante porta, faucibus fringilla nibh. Ut fermentum turpis vel nisi efficitur, in varius risus eleifend. Praesent mauris sem, condimentum id aliquet in, iaculis eget tellus. Fusce hendrerit eget tortor ac blandit. Integer id porttitor erat. In ut ante dapibus, tristique justo at, dapibus sapien. Aenean viverra id mauris ut molestie. Aenean eget purus sollicitudin, commodo mauris in, feugiat nisi. Proin id faucibus nisi. Phasellus eu massa dolor. Mauris maximus massa at ligula interdum lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
                    $permalink = Yii::app()->common->formatPermalink($page_name);
                    $page = new Page();
                    $page->title = htmlspecialchars($page_name);
                    $page->permalink = $permalink;
                    $page->content = htmlspecialchars($page_content);
                    $page->studio_id = $studio_id;
                    $page->created_date = new CDbExpression("NOW()");
                    $page->created_by = Yii::app()->user->id;
                    $page->ip = $ip_address;
                    $page->save();
                    $terms_id = $page->id;

                    $studio->terms_page = $terms_id;
                    $studio->status = 1;
                    $studio->in_trial = 1;
                    $studio->start_date = Date('Y-m-d H:i:s', strtotime("+{$config[1]['value']} days"));
                    $end_date = $config[1]['value'] + 30;
                    $studio->end_date = Date('Y-m-d H:i:s', strtotime("+{$end_date} days"));
                    $studio->save();

                    Yii::app()->email->emailToUser($req);

                    if ($respons_type == 'D' && $firstData->getBankResponseType() == 'S') {
                        //Refund the athuenticate charge
                        $data['token'] = $firstData->getTransArmorToken();
                        $data['cardtype'] = $firstData->getCreditCardType();
                        $refund = $this->refundAuthCharge($data);
                    }
                }
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        print json_encode($res);
        exit;
    }
    
    function chargeToCard($data = array()) {
        if (isset($data) && !empty($data)) {
            if (IS_LIVE_FIRSTDATA) {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
            } else {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
            }

            $firstData->setTransactionType(FirstData::TRAN_PURCHASE);
            $firstData->setTransArmorToken($data['token'])
                    ->setCreditCardType($data['card_type'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }

    function authenticateToCard($data = array()) {
        if (isset($data) && !empty($data)) {
            if (IS_LIVE_FIRSTDATA) {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
            } else {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
            }

            $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
            $firstData->setCreditCardNumber($data['card_number'])
                    ->setCreditCardName($data['card_name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);
                    //->setCreditCardType($data['type'])
                    //->setReferenceNumber("Signup Charge");
            if ($data['zip']) {
                $firstData->setCreditCardZipCode($data['zip']);
            }

            if ($data['cvv']) {
                $firstData->setCreditCardVerification($data['cvv']);
            }

            if ($data['address']) {
                $firstData->setCreditCardAddress($data['address']);
            }

            $firstData->process();

            return $firstData;
        } else {
            return '';
        }
    }

    function refundAuthCharge($data = array()) {
        $res = array();
        if (isset($data) && !empty($data)) {
            if (IS_LIVE_FIRSTDATA) {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
            } else {
                $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
            }
            $firstData->setTransactionType(FirstData::TRAN_REFUND);

            $firstData->setCreditCardType($data['cardtype'])
                    ->setCreditCardNumber($data['number'])
                    ->setTransArmorToken($data['token'])
                    ->setCreditCardName($data['name'])
                    ->setCreditCardExpiration($data['exp'])
                    ->setAmount($data['amount']);

            $firstData->process();

            // Check
            if ($firstData->isError()) {
                $res['Code'] = $firstData->getErrorCode();
                $res['Message'] = $firstData->getErrorMessage();
            } else {
                $res['Code'] = $firstData->getBankResponseCode();
                $res['Message'] = $firstData->getBankResponseMessage();
            }
        }
        return $res;
    }

    function actionPaymentMethod_2() {
        $req['name'] = 'Sunil Kund';
        $req['email'] = 'sunil@muvi.com';
        $req['domain'] = 'http://sunil.muvi.com';
        Yii::app()->email->emailToUser($req);
        exit;
        /* $data = array();
          //$data['type'] = "05";
          $data['number'] = "4514560001050313";
          $data['name'] = "Rasmiranjan";
          $data['exp'] = "0217";
          //$data['amount'] = 0;
          //$data['zip'] = '751013';
          $data['cvv'] = '123';
          //$data['token'] = '1579290647751112';//To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
          $data['address'] = "Acharya Vihar, Bhubaneswar, Odisha, India";
          //$orderId = "0011";

          if (IS_LIVE_FIRSTDATA) {
          $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY);
          } else {
          $firstData = new FirstData(FIRSTDATA_API_LOGIN, FIRSTDATA_API_KEY, true);
          }

          $firstData->setTransactionType(FirstData::TRAN_PREAUTH);
          $firstData->setCreditCardNumber($data['number'])
          ->setCreditCardName($data['name'])
          ->setCreditCardVerification($data['cvv'])
          ->setCreditCardExpiration($data['exp']);

          $firstData->setCreditCardType($data['type'])
          //->setTransArmorToken($data['token'])
          ->setCreditCardNumber($data['number'])
          ->setCreditCardName($data['name'])
          ->setCreditCardExpiration($data['exp'])
          ->setPartialRedemption(1)
          ->setAmount($data['amount']);

          if ($data['zip']) {
          $firstData->setCreditCardZipCode($data['zip']);
          }

          if ($data['address']) {
          $firstData->setCreditCardAddress($data['address']);
          }

          $firstData->process();
          print '<pre>';print_r($firstData);
          print "<br/>Tranasction error:: ".$firstData->getErrorMessage();
          print '<br/>Response code :: '.$firstData->getBankResponseType();exit;


          if ($firstData->getBankResponseCode() != '530') {
          $data['amount'] = 0.1;
          $firstData = $this->authenticateToCard($data);

          $data['token'] = $firstData->getTransArmorToken();
          $data['cardtype'] = $firstData->getCreditCardType();
          $refund = $this->refundAuthCharge($data);
          }

          // Check
          if ($firstData->isError()) {
          $res['Code'] = $firstData->getErrorCode();
          $res['Message'] = $firstData->getErrorMessage();
          } else {
          $res['TransactionRecord'] = $firstData->getTransactionRecord();
          $res['AuthNumber'] = $firstData->getAuthNumber();
          $res['TransArmorToken'] = $firstData->getTransArmorToken();
          $res['CreditCardType'] = $firstData->getCreditCardType();
          $res['Code'] = $firstData->getBankResponseCode();
          $res['Message'] = $firstData->getBankResponseMessage();
          $res['tags'] = $firstData->getTransactionTag();
          }
          print '--Second Transaction with cust data----<pre>';print_r($firstData);print_r($res);exit; */
        $this->render('paymentMethod_2');
    }

    /**
     * @method public success() It will come up with Congratulation page and some instruction.
     * @author GDR<support@muvi.com>
     * @return HTML A page with HTML contents
     */
    function actionSuccess() {
        if (isset(Yii::app()->user->studio_id) && Yii::app()->user->studio_id && isset(Yii::app()->user->signup_step) && Yii::app()->user->signup_step == 0) {
            $this->pageTitle = "Congratulation! Your VOD platform is now ready - Muvi";
            $this->pageDescription = "Now, VOD Platform is ready. Preview your Website and Proceed Admin Panel to manage your videos and other settings.";
            $this->layout = 'success';
            $this->render('success');
        } else {
            $this->redirect($this->createUrl());
        }
    }

    /**
     * @method public createUniqSubdomain($domain_name) Creates a uniqie subdomain from the domain name
     * @return string Returns the uniqe subdomain name
     * @author GDR<support@muvi.com>
     */
    function createUniqSubdomain($domainName) {
        if ($domainName) {
            $domain = str_replace('www.', '', $domainName);
            $domain = explode('.', $domain);
            $subdomain = $domain[0];
            if ($subdomain) {
                if (in_array($subdomain, $this->reservedDomain)) {
                    $subdomain = $subdomain . '1';
                }
                $studio = new Studio();
                $avalDomain = $studio->checkSubdomain($subdomain);
                return $avalDomain;
            } else {
                return FALSE;
            }
        }
    }
    
    function actionPurchaseSubscription() {
        if (isset($_REQUEST['studio']) && trim($_REQUEST['studio'])) {//When users comes from link
            $this->layout = 'main_old';
            $uniqid = $_REQUEST['studio'];
            $studio = Studios::model()->findByAttributes(array('uniqid' => $uniqid));
            
            if (isset($studio) && !empty($studio)) {
                $isPurchased = 1;//For trial studio, subscribed studio and demo studio
                
                if (isset($studio) && !empty($studio) && $studio->status == 4 && $studio->is_subscribed == 0 && $studio->is_deleted == 0 && $studio->is_default == 0) {//For Canceled studio
                    $isPurchased = 0;
                } else if (isset($studio) && !empty($studio) && $studio->status == 0 && $studio->is_subscribed == 0 && $studio->is_deleted == 1 && $studio->is_default == 0) {//Deleted studio
                    $isPurchased = 0;
                }
                
                if ($isPurchased) {
                    Yii::app()->user->setFlash('error', 'Oops! Sorry this domain name has already taken by someone else.');
                    $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
                } else {
                    $meta = Yii::app()->common->pagemeta('signuppaymentmethod');
                    $this->pageTitle = $meta['title'];
                    $this->pageDescription = $meta['description'];
                    
                    $packages = Package::model()->getPackages();
                    $this->render('subscription', array('studio' => $studio, 'packages' => $packages));
                }
            } else {
                Yii::app()->user->setFlash('error', 'Opps! Invalid studio!');
                $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Opps! Invalid studio!');
            $this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
        }
    }
	function isAjaxRequest() {
		if (!Yii::app()->request->isAjaxRequest) {  
			$url = Yii::app()->createAbsoluteUrl();
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: '.$url);exit();
		}
	}
    
    function actionPurchase() {
		self::isAjaxRequest();
    $purifier = new CHtmlPurifier();
    $_POST = $purifier->purify($_POST);

    $this->layout = false; 
	if(@$_POST['csrfToken'] === @$_SESSION['csrfToken']) {  
        $isError = 0;
        
        if(isset($_POST['card_number']) && trim($_POST['card_number']) && isset($_POST['uniqid']) && trim($_POST['uniqid'])) {
            $uniqid = $_POST['uniqid'];
            $studio = Studios::model()->findByAttributes(array('uniqid' => $uniqid));
            
            $isPurchased = 1;//For trial studio, subscribed studio and demo studio
            $isDeleted = 0;
            
            if (isset($studio) && !empty($studio) && $studio->status == 4 && $studio->is_subscribed == 0 && $studio->is_deleted == 0 && $studio->is_default == 0) {//For Canceled studio
                $isPurchased = 0;
            } else if (isset($studio) && !empty($studio) && $studio->status == 0 && $studio->is_subscribed == 0 && $studio->is_deleted == 1 && $studio->is_default == 0) {//Deleted studio
                $isPurchased = 0;
                $isDeleted = 1;
            }
                        
            if (!$isPurchased) {
                $studio_id = $studio->id;
                $studio = Studios::model()->findByPk($studio_id);
                $user = User::model()->findByAttributes(array('studio_id' => $studio_id));
                $user_id = $user->id;
                
                $ip_address = CHttpRequest::getUserHostAddress();
                
                $dataInput = array();
                $dataInput['card_number'] = $_POST['card_number'];
                $dataInput['card_name'] = $_POST['card_name'];
                $dataInput['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
                $dataInput['amount'] = 0;
                $dataInput['zip'] = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
                $dataInput['cvv'] = $_POST['cvv'];
                $address = $_POST['address1'];
                $address .= (trim($_POST['address2'])) ? ", " . trim($_POST['address2']) : '';
                $address .= ", " . $_POST['city'];
                $address .= ", " . $_POST['state'];
                $dataInput['address'] = $address;
                
                $firstData_preauth = $this->authenticateToCard($dataInput);
                
                $card_last_fourdigit = str_replace(substr($dataInput['card_number'], 0, strlen($dataInput['card_number']) - 4), str_repeat("#", strlen($dataInput['card_number']) - 4), $dataInput['card_number']);
                
                $req['name'] = $user->first_name;
                $req['email'] = $user->email;
                $req['phone'] = $user->phone_no;
                $req['companyname'] = $studio->name;
                $req['domain'] = 'http://' . $studio->domain;
                $req['card_last_fourdigit'] = $card_last_fourdigit;
                
                if ($firstData_preauth->isError()) {
                    $res['isSuccess'] = 0;
                    $res['Code'] = $firstData_preauth->getErrorCode();
                    $res['Message'] = 'Invalid Card details entered. Please check again';

                    $req['ErrorMessage'] = (trim($firstData_preauth->getErrorMessage())) ? $firstData_preauth->getErrorMessage() : $firstData_preauth->getBankResponseMessage();

                    //Keeping card error logs
                    $ceModel = New CardErrors;
                    $ceModel->studio_id = $studio_id;
                    $ceModel->response_text = serialize($firstData_preauth);
                    $ceModel->transaction_type = 'Purchase Subscription';
                    $ceModel->created = new CDbExpression("NOW()");
                    $ceModel->save();

                    //Send an email to developer and support
                    Yii::app()->email->errorMessageMailToSales($req);
                    
                } else {
                    $respons_type = $firstData_preauth->getBankResponseType();
                    
                    if ($respons_type == 'S') {
                        $token = $firstData_preauth->getTransArmorToken();
                        $card_type = $firstData_preauth->getCreditCardType();
                        
                        //Getting all plans amount which has selected by user
                        $package = Package::model()->getPackages($_POST['packages']);
                        $app_price = Yii::app()->general->getAppPrice($package['package'][0]['code']);
                        $total_amount = 0;
                        if (isset($package) && !empty($package)) {
                            $package_name = $package['package'][0]['name'];
                            $package_codes_id = 0;
                            $total_amount = $base_price = $package['package'][0]['base_price'];
                            
                            $yearly_discount = $package['package'][0]['yearly_discount'];
                            
                            if (isset($_POST['pricing']) && !empty($_POST['pricing'])) {
                                $applications = Package::model()->getApplications($_POST['packages'], $_POST['pricing']);
                                $no_of_applications = count($applications);
                                if (isset($applications) && !empty($applications) && ($no_of_applications > 1)) {
                                    $total_amount = number_format((float)$total_amount + (($no_of_applications -1) * $app_price), 2, '.', '');
                                }
                            }
                            
                            if (isset($_POST['plan']) && trim($_POST['plan']) == 'Year') {
                                $total_amount = number_format((float)(($total_amount * 12) - ((($total_amount * 12) * $yearly_discount) / 100)), 2, '.', '');
                            }
                        }
                        
                        $data = array();
                        $data['token'] = $token; //To configure tokenization support for your live First Data account, first contact your merchant services provider and request a TransArmor token if you have not already done so.
                        $data['card_name'] = $_POST['card_name'];
                        $data['card_type'] = $card_type;
                        $data['exp'] = $dataInput['exp'];
                        $data['amount'] = $total_amount;
                        $data['exp'] = ($_POST['exp_month'] < 10) ? '0' . $_POST['exp_month'] . substr($_POST['exp_year'], -2) : $_POST['exp_month'] . substr($_POST['exp_year'], -2);
                        $data['cvv'] = $_POST['cvv'];
                        
                        $ciModel = New CardInfos;
                        $ciModel->user_id = $user_id;
                        $ciModel->studio_id = $studio_id;
                        $ciModel->payment_method = 'first_data';
                        $ciModel->plan_amt = $data['amount'];
                        $ciModel->card_holder_name = $_POST['card_name'];
                        $ciModel->exp_month = $_POST['exp_month'];
                        $ciModel->exp_year = $_POST['exp_year'];
                        $address = $_POST['address1'];
                        $ciModel->address1 = $address;
                        $ciModel->address2 = $_POST['address2'];
                        $ciModel->city = (isset($_POST['city']) && trim($_POST['city'])) ? $_POST['city'] : '';
                        $ciModel->state = (isset($_POST['state']) && trim($_POST['state'])) ? $_POST['state'] : '';
                        $ciModel->zip = (isset($_POST['zip']) && trim($_POST['zip'])) ? $_POST['zip'] : '';
                        
                        $firstData = $this->chargeToCard($data);
                        
                        if ($firstData->isError()) {
                            $res['isSuccess'] = 0;
                            $res['Code'] = $firstData->getErrorCode();
                            $res['Message'] = 'Invalid Card details entered. Please check again';

                            $req['ErrorMessage'] = (trim($firstData->getErrorMessage())) ? $firstData->getErrorMessage() : $firstData->getBankResponseMessage();

                            //Keeping card error logs
                            $ceModel = New CardErrors;
                            $ceModel->studio_id = $studio_id;
                            $ceModel->response_text = serialize($firstData);
                            $ceModel->transaction_type = 'Purchase Subscription fee for '.$package_name;
                            $ceModel->created = new CDbExpression("NOW()");
                            $ceModel->save();

                            //Send an email to developer and support
                            Yii::app()->email->errorMessageMailToSales($req);
                        } else {
                            if ($firstData->getBankResponseType() == 'S') {
                                $res['isSuccess'] = 1;
                                $res['TransactionRecord'] = $firstData->getTransactionRecord();
                                $res['Code'] = $firstData->getBankResponseCode();

                                $res['studio_id'] = $studio_id;
                                $res['studio_name'] = $studio->name;
                                $res['studio_email'] = $user->email;

                                $ciModel->status = $res['Message'] = $firstData->getBankResponseMessage();
                                $ciModel->reference_no = $firstData->getTransactionTag();
                                $ciModel->card_type = $card_type;
                                $ciModel->card_last_fourdigit = $card_last_fourdigit;
                                $ciModel->auth_num = $firstData->getAuthNumber();
                                $ciModel->token = $token;
                                $ciModel->response_text = serialize($firstData_preauth);
                                
                                $ciModel->ip = $ip_address;
                                $ciModel->country = Yii::app()->common->countryfromIP($ciModel->ip);
                                $ciModel->created = new CDbExpression("NOW()");
                                $ciModel->save();

                                //Insert new record for billing information
                                $biModel = New BillingInfos;
                                $biModel->studio_id = $studio_id;
                                $biModel->user_id = $user_id;
                                $uniqid = Yii::app()->common->generateUniqNumber();
                                $biModel->uniqid = $uniqid;
                                $biModel->title = 'Purchase Subscription fee for '.$package_name;
                                $biModel->billing_date = new CDbExpression("NOW()");
                                $biModel->created_date = new CDbExpression("NOW()");
                                $biModel->billing_amount = $billing_amount = $data['amount'];
                                $biModel->transaction_type = 1;
                                $biModel->save();
                                $billing_info_id = $biModel->id;

                                //Insert new records in bill detail and studio pricing plans
                                $allapps = '';
                                if (isset($_POST['pricing']) && !empty($_POST['pricing']) && isset($applications) && !empty($applications)) {
                                    
                                    $cntapp = 1;
                                    foreach ($applications as $key => $value) {
                                        if ($cntapp == 1) {
                                            $title = 'Monthly platform fee';
                                            $description = $package_name.' platform fee';
                                            $packageamount = $base_price;
                                        } else {
                                            $title = $value['name'];
                                            $description = $value['description'];
                                            $packageamount = $value['price'];
                                        }
                                        $cntapp++;
                                        
                                        $bdModel = New BillingDetails;
                                        $bdModel->billing_info_id = $billing_info_id;
                                        $bdModel->user_id = $user_id;
                                        $bdModel->title = $title;
                                        $bdModel->description = $description;
                                        $bdModel->amount = $packageamount;
                                        $bdModel->save();

                                        $sppModel = New StudioPricingPlan;
                                        $sppModel->studio_id = $studio_id;
                                        $sppModel->package_id = $value['parent_id'];
                                        $sppModel->application_id = $value['id'];
                                        $sppModel->plans = $_POST['plan'];
                                        $sppModel->is_cloud_hosting = 0;
                                        $sppModel->package_code_id = $package_codes_id;
                                        $sppModel->created_by = $user_id;
                                        $sppModel->created = new CDbExpression("NOW()");
                                        $sppModel->status = 1;
                                        $sppModel->save();
                                        
                                        $allapps.=$value['name']. ', ';
                                    }
                                    $allapps = rtrim($allapps,', ');
                                }  else {//Muvi Server
                                    $title = 'Monthly platform fee';
                                    $description = $package_name.' platform fee';
                                
                                    $bdModel = New BillingDetails;
                                    $bdModel->billing_info_id = $billing_info_id;
                                    $bdModel->user_id = $user_id;
                                    $bdModel->title = $title;
                                    $bdModel->description = $description;
                                    $bdModel->amount = $base_price;
                                    $bdModel->save();

                                    $sppModel = New StudioPricingPlan;
                                    $sppModel->studio_id = $studio_id;
                                    $sppModel->package_id = $package['package'][0]['id'];
                                    $sppModel->application_id = 0;
                                    $sppModel->plans = $_POST['plan'];
                                    $sppModel->is_cloud_hosting = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
                                    $sppModel->package_code_id = $package_codes_id;
                                    $sppModel->created_by = $user_id;
                                    $sppModel->created = new CDbExpression("NOW()");
                                    $sppModel->status = 1;
                                    $sppModel->save();
                                }

                                //Insert new transaction record in transaction infos;
                                $tiModel = New TransactionInfos;

                                //Set datas for inserting new record in transaction info table whether it returns success or error
                                $tiModel->card_info_id = $ciModel->id;
                                $tiModel->studio_id = $studio_id;
                                $tiModel->user_id = $user_id;
                                $tiModel->billing_info_id = $billing_info_id;
                                $tiModel->billing_amount = $billing_amount;
                                $tiModel->invoice_detail = $_POST['plan']."ly Subscription Charge";
                                $tiModel->is_success = 1;
                                $tiModel->invoice_id = $firstData->getTransactionTag();
                                $tiModel->order_num = $firstData->getAuthNumber();
                                $tiModel->paid_amount = $paid_amount = $firstData->getAmount();
                                $tiModel->response_text = serialize($firstData);
                                $tiModel->transaction_type = 'Purchase Subscription fee for '.$package_name;
                                $tiModel->created_date = new CDbExpression("NOW()");
                                $tiModel->save();

                                if (abs(($billing_amount - $paid_amount) / $paid_amount) < 0.00001) {
                                    $is_paid = 2;
                                } else {
                                    $is_paid = 1;
                                }

                                $invoice['studio'] = $studio;
                                $invoice['user'] = $user;
                                $invoice['card_info'] = $ciModel;
                                $invoice['billing_info_id'] = $billing_info_id;
                                $invoice['billing_amount'] = $billing_amount;
                                $invoice['package_name'] = @$package_name;
                                $invoice['package_code'] = @$package['package'][0]['code'];
                                $invoice['base_price'] = @$base_price;
                                $invoice['plan'] = @$_POST['plan'];
                                $invoice['yearly_discount'] = @$yearly_discount;
                                $invoice['no_of_applications'] = @$no_of_applications;
                                $invoice['applications'] = @$allapps;

                                $pdf = Yii::app()->pdf->adminSubscriptionReactivationInvoiceDetial($invoice);
                                $file_name = $pdf['pdf'];
                                
                                //Update subscription in studio table if tranasction is going to success or not.
                                $biModel = New BillingInfos;
                                $biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount,'is_paid' => $is_paid, 'detail' => $pdf['html']));
                                
                                //Save subscription info if payment has processed successfully
                                //if (intval($is_paid) == 2) {

                                    //If account has deleted previously, create default contents
                                    if ($isDeleted) {
                                        //Inserting into the front-end user table
                                        $sdkuser = new SdkUser;
                                        $sdkuser->is_studio_admin = 1;
                                        $sdkuser->email = $user->email;
                                        $sdkuser->studio_id = $studio_id;
                                        $sdkuser->signup_ip = $ip_address;
                                        $sdkuser->display_name = htmlspecialchars($_POST['name']);
                                        $sdkuser->encrypted_password = $user->encrypted_password;
                                        $sdkuser->status = 1;
                                        $sdkuser->created_date = new CDbExpression("NOW()");
                                        $sdkuser->save();

                                        //Add and set default content
                                        $this->addDefaultContentType($studio_id);

                                        $terms_id = $this->setDefaultContent($studio_id);
                                        $studio->terms_page = $terms_id;

                                        // Creating the folder in the name of Subdomain and copying all the files inside the sdk theme folder
                                        $theme_folder = $studio->theme;
                                        $templateName = $studio->parent_theme;
                                        if (!is_dir(ROOT_DIR . 'themes/' . $theme_folder)) {
                                            mkdir(ROOT_DIR . 'themes/' . $theme_folder);
                                        }
                                        //chown(ROOT_DIR.'themes/'.Yii::app()->user->studio_subdomain, 'root');
                                        chmod(ROOT_DIR . 'themes/' . $theme_folder, 0777);
                                        // shell command 
                                        $dest = ROOT_DIR . 'themes/' . $theme_folder . '';
                                        $source = ROOT_DIR . 'sdkThemes/' . $templateName;
                                        $return = $this->recurse_copy($source, $dest);
                                    }

                                    //Update status in user table
                                    $user->is_active = 1;
                                    $user->save();

                                    //Update status in studio table
                                    $studio->status = 1;
                                    $studio->is_subscribed = 1;
                                    $studio->is_deleted = 0;
                                    $studio->subscription_start = Date('Y-m-d H:i:s');
                                    $biModel = New BillingInfos;
                                    $biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
                                    if (isset($_POST['plan']) &&  trim($_POST['plan']) == 'Month') {
                                        $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                                        $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Months -1 Days"));
                                        $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Months"))));
                                    } else {
                                        $studio->start_date = Date('Y-m-d H:i:s', strtotime("+1 Years"));
                                        $studio->end_date = Date('Y-m-d H:i:s', strtotime("+2 Years -1 Days"));
                                        $biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+1 Years"))));
                                    }
                                    $studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime("+1 Months"));
                                    $studio->save();

                                    $scrModel = StudioCancelReason :: model() -> find(array("condition" => " studio_id=".$studio_id));
                                    if (isset($scrModel) && !empty($scrModel)) {
                                        $scrModel->delete();
                                    }

                                $req['package_name'] = $package_name;
                                $req['applications'] = @$allapps;
                                $req['plan'] = @$_POST['plan'];
                                $req['amount_charged'] = @$billing_amount;
                                $req['is_cloud_hosting'] = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
                                
                                Yii::app()->email->subscriptionPurchaseMailToSales($req);
                                Yii::app()->email->subscriptionPurchaseMailToUser($req, $file_name);
                                //}
                            } else {
                                $isError = 1;
                            }
                        }
                    } else {
                        $isError = 1;
                    }
                }
            } else {//If lead or subscribed try to purchase subscription
                $res['isSuccess'] = 0;
                $res['Code'] = 55;
                $res['Message'] = 'You are not allowed to purchase subscription. Please login using your Email Address.';
            }
        } else {//Invalid card detail
            $isError = 1;
        }
        
        if ($isError) {
            $res['isSuccess'] = 0;
            $res['Code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        
        print json_encode($res);
        exit;
    }
    }
    
    //Manual set up studios data
    public function actionSetupStudioDummyData() {
        $studio_id = 1224;
        $source_studio_id = DUMMY_DATA_STUDIO;
        $ip_address = CHttpRequest::getUserHostAddress();        
        if (isset($studio_id) && intval($studio_id)) {
            $studio = Studio::model()->findByPk($studio_id);
            
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $source_bucketInfo = Yii::app()->common->getBucketInfoForPoster($source_studio_id);
            $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $sourceBucket = $source_bucketInfo['bucket_name'];
            $targetBucket = $target_bucketInfo['bucket_name'];
            $s3dir = $source_studio_id . '/public/public/system/studio_banner/' . $source_studio_id . '/';
            $response = $s3->getListObjectsIterator(array(
                'Bucket' => $sourceBucket,
                'Prefix' => $s3dir
            ));
            foreach ($response as $object) {
                $key = $object['Key'];
                $key_arr = explode('/', $key);
                $batch = array();
                if ($key_arr['6'] != '') {
                    $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                    $sourceKeyname = $s3dir . $keypath;
                    echo "<br />".$targetKeyname = $unsignedFolderPath . 'public/system/studio_banner/' . $studio_id . '/' . $keypath;
                    $batch[] = $s3->getCommand('CopyObject', array(
                        'Bucket' => $targetBucket,
                        'Key' => $targetKeyname,
                        'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                        'ACL' => 'public-read',
                    ));
                }

                $s3->execute($batch);
            }
        }
    }
    
    
    public function actionSetupsinglecontent() {
        $studio_id = 1233;
        $source_studio_id = DUMMY_DATA_STUDIO;
        $content_type_id = 1;
        $ip_address = CHttpRequest::getUserHostAddress();
        if (isset($studio_id) && intval($studio_id)) {
            $studio = Studio::model()->findByPk($studio_id);
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $source_bucketInfo = Yii::app()->common->getBucketInfoForPoster($source_studio_id);
            $target_bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            
            $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
            $unsignedFolderPath = $folderPath['unsignedFolderPath'];
            $sourceBucket = $source_bucketInfo['bucket_name'];
            $targetBucket = $target_bucketInfo['bucket_name'];
            
            $dummystudio_content_type = StudioContentType::model()->findByAttributes(array('studio_id' => $source_studio_id, 'content_types_id' => $content_type_id));
            $dummycontent_type_id = $dummystudio_content_type->id;

            $studio_content_type = StudioContentType::model()->findByAttributes(array('studio_id' => $studio_id, 'content_types_id' => $content_type_id));
            $studio_content_type_id = $studio_content_type->id;

            $Films_data = new Film();
            $studio_films = $Films_data->findAllByAttributes(array('studio_id' => $source_studio_id, 'content_type_id' => $dummycontent_type_id));
            foreach ($studio_films as $film) {
                $film_new = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'name' => $film->name));
                $movie_id = $film_new->id;
                $dummydata_movie_id = $film->id;

                // upload poster
                $posters_data = Poster::model()->findAllByAttributes(array('object_id' => $dummydata_movie_id), array('order' => 'id ASC',));

                foreach ($posters_data as $poster) {
                    $posters = new Poster();
                    $posters = $posters->findByAttributes(array('object_id' => $movie_id, 'object_type' => $poster->object_type));
                    $poster_id = $posters->id;

                    $s3dir = $source_studio_id . '/public/public/system/posters/' . $poster->id . '/';
                    $response = $s3->getListObjectsIterator(array(
                        'Bucket' => $sourceBucket,
                        'Prefix' => $s3dir
                    ));
                    $batch = array();
                    foreach ($response as $object) {
                        $key = $object['Key'];
                        $key_arr = explode('/', $key);
                        $keypath = $key_arr['6'] . '/' . $key_arr['7'];
                        echo "<br />".$sourceKeyname = $s3dir . $keypath;
                        echo "<br />".$targetKeyname = $unsignedFolderPath . 'public/system/posters/' . $poster_id . '/' . $keypath;
                        $batch[] = $s3->getCommand('CopyObject', array(
                            'Bucket' => $targetBucket,
                            'Key' => $targetKeyname,
                            'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                            'ACL' => 'public-read',
                        ));
                    }
                    $s3->execute($batch);
                }
            }

        }
    }    
 /**
 * @method public removecontentCategory() Update the content details
 * @return HTML 
 * @author GDR<support@muvi.com>
 */	
    function actionRemoveContentCategory() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        if (isset($_REQUEST['category_id']) && $_REQUEST['category_id']) {
            $category = ContentCategories::model()->find('id=:category_id AND studio_id =:studio_id', array(':category_id' => $_REQUEST['category_id'], ':studio_id' => Yii::app()->user->studio_id));
            if ($category) {
                // Binary update query
                    $con = Yii::app()->db;
                    $fsql = 'UPDATE films SET content_category_value = content_category_value -  ' . $category->binary_value . ' WHERE ' . $category->binary_value . ' & content_category_value AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($fsql)->execute();
					
					//Updating the studio content types table value 
                    $msql = 'UPDATE studio_content_types SET content_category_value = content_category_value -  ' . $category->binary_value . ' WHERE ' . $category->binary_value . ' & content_category_value AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($msql)->execute();


                    ContentCategories::model()->deleteByPk($_REQUEST['category_id'], 'studio_id=:studio_id', array(':studio_id' => Yii::app()->user->studio_id));

                    // Update the content categroy table to re-assign the value 
                    //$csql = 'UPDATE content_category SET binary_value = binary_value >> 1 WHERE binary_value >' . $category->binary_value . ' AND studio_id = ' . Yii::app()->user->studio_id;
                    //$con->createCommand($csql)->execute();
                
					//Select from Menu items and remove those from url_routings
					
					$menuItems = MenuItem::model()->findAll('studio_id=:studio_id AND value=:value',array(':studio_id'=>Yii::app()->user->studio_id,':value'=>$category->binary_value));
					if($menuItems){
						foreach ($menuItems AS $key=>$val){
							// Delete from URL Routing
							$con->createCommand("DELETE FROM url_routing WHERE studio_id=".Yii::app()->user->studio_id." AND (permalink='".$val->permalink."' OR mapped_url='/media/list/menu_item_id/".$val->id."')")->execute();
						}
						// Delete From Menu Items
						$con->createCommand('DELETE FROM menu_items WHERE studio_id='.Yii::app()->user->studio_id.' AND value='.$category->binary_value)->execute();
					}
					
					// Update the menu items table to re-assign the value
					$menusql = 'UPDATE menu_items SET value = value -  '.$category->binary_value.' WHERE '.$category->binary_value.' & value AND studio_id='.Yii::app()->user->studio_id;
					$con->createCommand($menusql)->execute();
					
                Yii::app()->user->setFlash('success', ' Content Type  Deleted successfully.');
                $this->redirect($this->createUrl('category/manageCategory'));
            } else {
                Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
                $this->redirect($this->createUrl('category/manageCategory'));
            }
        } else {
            Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
            $this->redirect($this->createUrl('category/manageCategory'));
        }
    }
     /*
     * Wp Forum user signup with login
     * @author T.NANDY
     */
    public function actionWpuserSignup() {
        $this->layout =  false;
        $wpuser_data = array(
            'user_login' => $_REQUEST['email'],
            'first_name' => $_REQUEST['name'],
            'user_email' => $_REQUEST['email'],
            'user_pass' => $_REQUEST['password']
        );
        $wpuser_id = wp_insert_user($wpuser_data);
        if (count($wpuser_id->errors['existing_user_email']) == 0) {
            add_user_meta($wpuser_id, 'wpuser_type', '1');
            add_user_meta($wpuser_id, 'wpuser_name', $_REQUEST['name']);
            add_user_meta($wpuser_id, 'wpuser_password', $_REQUEST['password']);

            $username = $_REQUEST['email'];
            $password = $_REQUEST['password'];
            $wpuser = Yii::app()->common->getWpUserLogin($username, $password);
            $response = 'success';
            $message = 'Thank you for register';
        } else {
            $response = 'error';
            $message = 'Already Registred Please Login';
        }
        $ret = array('status' => $response, 'message' => $message);
        echo json_encode($ret);
    }
	
	function actionAccountSetup() {
		exit;
		$studio_id = 0;
		$user_id = 0;
		$studio = Studios::model()->findByPk($studio_id);
		$user = User::model()->findByPk($user_id);
		
		$_POST['packages'] = 26;
		$_POST['plan'] = 'Month';
		$_POST['cloud_hosting'] = 0;
		
		$package = Package::model()->getPackages($_POST['packages']);
		$total_amount = 0;
		if (isset($package) && !empty($package)) {
			$package_name = $package['package'][0]['name'];
			$package_codes_id = 0;
			$total_amount = $base_price = $package['package'][0]['base_price'] * 3;
		}

		//Insert new record for billing information
		$biModel = New BillingInfos;
		$biModel->studio_id = $studio_id;
		$biModel->user_id = $user_id;
		$uniqid = Yii::app()->common->generateUniqNumber();
		$biModel->uniqid = $uniqid;
		$biModel->title = 'Purchase Subscription fee for ' . $package_name;
		$biModel->billing_date = new CDbExpression("NOW()");
		$biModel->created_date = new CDbExpression("NOW()");
		$biModel->billing_amount = $billing_amount = $paid_amount = $total_amount;
		$biModel->transaction_type = 1;
		$biModel->save();
		$billing_info_id = $biModel->id;

		$title = 'Monthly platform fee';
		$description = $package_name . ' platform fee';

		$bdModel = New BillingDetails;
		$bdModel->billing_info_id = $billing_info_id;
		$bdModel->user_id = $user_id;
		$bdModel->title = $title;
		$bdModel->description = $description;
		$bdModel->amount = $base_price;
		$bdModel->save();

		$sppModel = New StudioPricingPlan;
		$sppModel->studio_id = $studio_id;
		$sppModel->package_id = $package['package'][0]['id'];
		$sppModel->application_id = 0;
		$sppModel->plans = $_POST['plan'];
		$sppModel->is_cloud_hosting = (isset($_POST['cloud_hosting']) && intval($_POST['cloud_hosting'])) ? 1 : 0;
		$sppModel->package_code_id = $package_codes_id;
		$sppModel->created_by = $user_id;
		$sppModel->created = new CDbExpression("NOW()");
		$sppModel->status = 1;
		$sppModel->save();

		//Insert new transaction record in transaction infos;
		$tiModel = New TransactionInfos;

		//Set datas for inserting new record in transaction info table whether it returns success or error
		$tiModel->card_info_id = 0;
		$tiModel->studio_id = $studio_id;
		$tiModel->user_id = $user_id;
		$tiModel->billing_info_id = $billing_info_id;
		$tiModel->billing_amount = $billing_amount;
		$tiModel->invoice_detail = $_POST['plan'] . "ly Subscription Charge";
		$tiModel->is_success = 1;
		$tiModel->invoice_id = 20170711120012;
		$tiModel->order_num = 20170711120020;
		$tiModel->paid_amount = $paid_amount;
		$tiModel->response_text = '';
		$tiModel->transaction_type = 'Purchase Subscription fee for ' . $package_name;
		$tiModel->created_date = new CDbExpression("NOW()");
		$tiModel->save();

		$biModel = New BillingInfos;
		$biModel->updateByPk($billing_info_id, array('paid_amount' => $paid_amount, 'is_paid' => 1, 'detail' => ''));


		//Inserting into the front-end user table
		$sdkuser = new SdkUser;
		$sdkuser->is_studio_admin = 1;
		$sdkuser->email = $user->email;
		$sdkuser->studio_id = $studio_id;
		$sdkuser->signup_ip = '';
		$sdkuser->display_name = htmlspecialchars($_REQUEST['name']);
		$sdkuser->encrypted_password = $user->encrypted_password;
		$sdkuser->status = 1;
		$sdkuser->created_date = new CDbExpression("NOW()");
		$sdkuser->save();

		//Add and set default content
		$this->addDefaultContentType($studio_id);

		$terms_id = $this->setDefaultContent($studio_id);
		$studio->terms_page = $terms_id;

		// Creating the folder in the name of Subdomain and copying all the files inside the sdk theme folder
		$theme_folder = $studio->theme;
		$templateName = $studio->parent_theme;
		if (!is_dir(ROOT_DIR . 'themes/' . $theme_folder)) {
			mkdir(ROOT_DIR . 'themes/' . $theme_folder);
		}
		//chown(ROOT_DIR.'themes/'.Yii::app()->user->studio_subdomain, 'root');
		chmod(ROOT_DIR . 'themes/' . $theme_folder, 0777);
		// shell command 
		$dest = ROOT_DIR . 'themes/' . $theme_folder . '';
		$source = ROOT_DIR . 'sdkThemes/' . $templateName;
		$return = $this->recurse_copy($source, $dest);

		//Update status in user table
		$user->is_active = 1;
		$user->save();

		//Update status in studio table
		$studio->status = 1;
		$studio->is_subscribed = 1;
		$studio->is_deleted = 0;
		$studio->subscription_start = Date('Y-m-d H:i:s');
		$biModel = New BillingInfos;
		$biModel->updateByPk($billing_info_id, array('billing_period_start' => Date('Y-m-d H:i:s')));
		if (isset($_POST['plan']) && trim($_POST['plan']) == 'Month') {
			$studio->start_date = Date('Y-m-d H:i:s', strtotime("+3 Months"));
			$studio->end_date = Date('Y-m-d H:i:s', strtotime("+4 Months -1 Days"));
			$biModel->updateByPk($billing_info_id, array('billing_period_end' => Date('Y-m-d H:i:s', strtotime("+3 Months"))));
		}
		$studio->bandwidth_date = Date('Y-m-d H:i:s', strtotime("+3 Months"));
		$studio->save();

		$scrModel = StudioCancelReason :: model()->find(array("condition" => " studio_id=" . $studio_id));
		if (isset($scrModel) && !empty($scrModel)) {
			$scrModel->delete();
		}
	}
	
	public function actionbuynow() {
		$this->pageTitle = "Add and manage your Video content - Muvi";
		$this->pageDescription = "Add, edit, upload all kinds of video content like; Movies, TV Shows, Events etc";
		if (isset(Yii::app()->user->id) && Yii::app()->user->id) {
			if (isset(Yii::app()->user->studio_id) && Yii::app()->user->studio_id) {
				//Getting all available content types of the studio
				$studio_id = Yii::app()->user->studio_id;
				$user_id = Yii::app()->user->id;
				$card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
				$packages = Package::model()->getPackages();
				$this->render('buynow', array('card' => $card, 'packages' => $packages));
			} else {
				$this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
				exit;
			}
		} else {
			$this->redirect(Yii::app()->getBaseUrl(true) . '/signup');
			exit;
		}
	}
	
	public function actionPurchasenow() {
		if (isset($_GET['studio']) && trim($_GET['studio'])) {
			if (isset(Yii::app()->user->id) && Yii::app()->user->id) {
				Yii::app()->session['is_popup'] =  (isset($this->studio->uniqid) && ($_GET['studio'] == $this->studio->uniqid)) ? 1 : 0;
				$this->redirect(Yii::app()->getBaseUrl(true) . '/admin/dashboard');
				exit;
			} else {
				Yii::app()->session['is_popup']=1;
				$this->redirect(Yii::app()->getBaseUrl(true));
				exit;
			}
		} else {
			$this->redirect(Yii::app()->getBaseUrl(true));
			exit;
		}
	}

}
