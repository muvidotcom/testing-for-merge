<?php

class TvguideController extends Controller {

    public function init() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        parent::init();
    }

    
    public $defaultAction = 'index';
    public $layout = 'main';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        if (!($this->has_tvguide)) {
            Yii::app()->user->setFlash('error', "Playout is currently deactivated.");
            $this->redirect($redirect_url);
            exit();
        }
        return true;
    }

    public function actionIndex() {
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $language_id = $this->language_id;
        $BusinessName = $studio->name;

        $permalink = str_replace("/",'',$_SERVER['REQUEST_URI']);
        $BusinessTitle = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id,'permalink'=>$permalink), array('select' => 'title'));
        if(!empty($BusinessTitle)):
            $title = utf8_decode($BusinessTitle->title);
        else:
            $title = $this->Language['live_tv'];
        endif;  
        $description = $BusinessName . ' tv guide';
        $keywords = '';

        $this->pageTitle = $title . ' | ' . $BusinessName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $default_img = $this->tvguideDefaultImage();
        $nowtime = date('Y-m-d H');
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $base_cloud_url .= "tv_guide/channel_logo/";
        $allchannels = tvGuide::model()->findAllByAttributes(array('studio_id' => $studio_id));
       
        foreach($allchannels as $key=>$channels){            
           $channel_guide[$key] = tvGuide::model()->allguideschannelwise($channels['id'],$start_time,$end_time);         
        }
       
        //get the studio tv guide timezone
        $getStudioConfig= StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name=$getStudioConfig->config_value;
        $tvguide_zone=$zone_name==''?'UTC':$zone_name;
              
        //get the studio tv guide timezone
       $date = new DateTime("now", new DateTimeZone($tvguide_zone) );
       $zonal_current_time=$date->format('m/d H:i');
       $currenttime=$date->format('Y-m-d H:i:s');
       
      
        $current_hour = date('H', strtotime($currenttime));
        $timeafter1hour = date('H', strtotime($currenttime . ' + 1 hours'));
        $timeafter2hour = date('H', strtotime($currenttime . ' + 2 hours'));
        $timeafter3hour = date('H', strtotime($currenttime . ' + 3 hours'));
        //$tvguide = $this->Gettimewiseguide($allchannels,$nowtime);
       $this->render('index',array('default_image'=>$default_img,'allchannels'=>$allchannels, 'channel_guide' => $channel_guide,'base_cloud_url'=>$base_cloud_url,'current_time'=>$zonal_current_time,'current_hour'=>$current_hour,'timeafter1hour'=>$timeafter1hour,'timeafter2hour'=>$timeafter2hour,'timeafter3hour'=>$timeafter3hour));
         }

    
    public function actiongetOldGuide()
    {
     $channel_id=$_REQUEST['id'];
     $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $BusinessName = $studio->name;

        
        $title = 'Playout | ' . $BusinessName;
        $description = $BusinessName . ' tv guide';
        $keywords = '';

        $this->pageTitle = $title;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $default_img = $this->tvguideDefaultImage();
        $nowtime = gmdate('Y-m-d H');
        
        $allchannels = tvGuide::model()->findAllByAttributes(array('id'=>$channel_id));
        
        $tvguide = $this->Gettimewiseguide($allchannels,$nowtime);
        $this->render('index1',array('default_image'=>$default_img,'tvguide'=>$tvguide,'id'=>$channel_id));
    }
    
    public function actionGetData_old($start_time = '', $end_time = '', $nowtime = '') {
        //$channel_id=@$_REQUEST['channel_id'];
        $default_video = '';
        $return = array();
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudiosId();
        
        $allchannels = tvGuide::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $nowtime = date('Y-m-d H');
       
       // $guide = $this->Gettimewiseguide($allchannels,$nowtime); 
        $currenttime = date('Y-m-d H:i:s');
        $current = date('H', strtotime($currenttime)).":30";
        $timeafter1hour = date('H', strtotime($currenttime . ' + 1 hours')).":30";
        $timeafter2hour = date('H', strtotime($currenttime . ' + 2 hours')).":30";
        $timeafter3hour = date('H', strtotime($currenttime . ' + 3 hours')).":30";
        $left_right = date('Y-m-d H:i:s');
        $time_slide = '<div class="col-xs-3 text-center">'.$current.'</div><div class="col-xs-3 text-center">'.$timeafter1hour.'</div><div class="col-xs-3 text-center">'.$timeafter2hour.'</div><div class="col-xs-3 text-center">'.$timeafter3hour.'</div>';
        //$return = array('time_slide'=>$time_slide,'left_right'=>$left_right,'response'=>$guide);
        $return = array('time_slide'=>$time_slide,'left_right'=>$left_right);
       
        echo json_encode($return);
    }
    
    
     public function actionGetData($start_time = '', $end_time = '', $nowtime = '') {
        //$channel_id=@$_REQUEST['channel_id'];
        $default_video = '';
        $return = array();
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudiosId();
        
        $allchannels = tvGuide::model()->findAllByAttributes(array('studio_id' => $studio_id));
        //get the studio tv guide timezone
        $getStudioConfig= StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name=$getStudioConfig->config_value;
        $tvguide_zone=$zone_name==''?'UTC':$zone_name;
       
        //get the studio tv guide timezone
        $date = new DateTime("now", new DateTimeZone($tvguide_zone) );
        $zonal_current_time=$date->format('Y-m-d H:i:s');
       
       // $nowtime = gmdate('Y-m-d H');
       
      
        //$currenttime = date('Y-m-d H:i:s');
        $currenttime =$zonal_current_time;
        $current = date('H', strtotime($currenttime)).":30";
        $timeafter1hour = date('H', strtotime($currenttime . ' + 1 hours')).":30";
        $timeafter2hour = date('H', strtotime($currenttime . ' + 2 hours')).":30";
        $timeafter3hour = date('H', strtotime($currenttime . ' + 3 hours')).":30";
       // $left_right = date('Y-m-d H:i:s');
          $left_right=$date->format('Y-m-d H:i:s');
       // $firsttimeinslide=gmdate('Y-m-d H', strtotime($left_right));
        $firsttimeinslide=date('Y-m-d H', strtotime($left_right));
        $start_time = $firsttimeinslide . ":00:00";
        $end_time = date('Y-m-d H', strtotime($start_time . ' + 4 hours')) . ":00:00";  
        $time_slide = '<div class="col-xs-3 text-center">'.$current.'</div><div class="col-xs-3 text-center">'.$timeafter1hour.'</div><div class="col-xs-3 text-center">'.$timeafter2hour.'</div><div class="col-xs-3 text-center">'.$timeafter3hour.'</div>';
        //$return = array('time_slide'=>$time_slide,'left_right'=>$left_right,'response'=>$guide);
        $return = array('time_slide'=>$time_slide,'left_right'=>$left_right,'nowtime'=>$firsttimeinslide, 'start_time'=>$start_time,'end_time'=>$end_time);
       
        echo json_encode($return);
    }
    
    public function actiongetvideodtls(){
        $movie_id = $_POST["movie_id"];
        $start_time = $_POST['start_time'];
        $end_time = $_POST['end_time'];
        $content_data = Yii::app()->general->getContentDataPermalink($movie_id,$start_time,$end_time); 
        echo json_encode($content_data);
    }

    public function actiongettime_old() {
        $studio_id = Yii::app()->common->getStudiosId();
        $timeguide = $_POST["timeguide"];
        $current_prog = @$_POST["current_program"];
        $prog_to_start = @$_POST["programs_to_start"];
        $timeguide = explode("_", $timeguide);
        $before_after = $timeguide[0];
        $nowtime = $timeguide[1];
        if ($before_after == "before") {
            $current = gmdate('H', strtotime($nowtime . ' - 1 hours')) . ":30";
            $timeafter1hour = gmdate('H', strtotime($nowtime)) . ":30";
            $timeafter2hour = gmdate('H', strtotime($nowtime . ' + 1 hours')) . ":30";
            $timeafter3hour = gmdate('H', strtotime($nowtime . ' + 2 hours')) . ":30";
            $left_right = gmdate('Y-m-d H:i:s', strtotime($nowtime . ' - 1 hours'));
        } elseif ($before_after == "after") {
            $current = gmdate('H', strtotime($nowtime . ' + 1 hours')) . ":30";
            $timeafter1hour = gmdate('H', strtotime($nowtime . ' + 2 hours')) . ":30";
            $timeafter2hour = gmdate('H', strtotime($nowtime . ' + 3 hours')) . ":30";
            $timeafter3hour = gmdate('H', strtotime($nowtime . ' + 4 hours')) . ":30";
            $left_right = gmdate('Y-m-d H:i:s', strtotime($nowtime . ' + 1 hours'));
        }
        $time_slide = '<div class="col-xs-3 text-center">' . $current . '</div><div class="col-xs-3 text-center">' . $timeafter1hour . '</div><div class="col-xs-3 text-center">' . $timeafter2hour . '</div><div class="col-xs-3 text-center">' . $timeafter3hour . '</div>';
        $allchannels = tvGuide::model()->findAllByAttributes(array('id' => @$_REQUEST['id']));
        $channel_guide = array();
        $firsttimeinslide = gmdate('Y-m-d H', strtotime($left_right));
        $start_time = $firsttimeinslide . ":00:00";
        $end_time = gmdate('Y-m-d H', strtotime($start_time . ' + 4 hours')) . ":00:00";
       
       // $guide = $this->Gettimewiseguide($allchannels, $firsttimeinslide, $start_time, $end_time, $current_prog, $prog_to_start);       
        $return = array('time_slide' => $time_slide, 'left_right' => $left_right,'nowtime'=>$firsttimeinslide, 'start_time'=>$start_time,'end_time'=>$end_time, 'current_prog'=>$current_prog,'prog_to_start'=>$prog_to_start);
        echo json_encode($return);
    }
        public function actiongettime() {
        $studio_id = Yii::app()->common->getStudiosId();
        $timeguide = $_POST["timeguide"];
        $current_prog = @$_POST["current_program"];
        $prog_to_start = @$_POST["programs_to_start"];
        $timeguide = explode("_", $timeguide);
        $before_after = $timeguide[0];
        $nowtime = $timeguide[1];
        if ($before_after == "before") {
            $current = date('H', strtotime($nowtime . ' - 1 hours')) . ":30";
            $timeafter1hour = date('H', strtotime($nowtime)) . ":30";
            $timeafter2hour = date('H', strtotime($nowtime . ' + 1 hours')) . ":30";
            $timeafter3hour = date('H', strtotime($nowtime . ' + 2 hours')) . ":30";
            $left_right = date('Y-m-d H:i:s', strtotime($nowtime . ' - 1 hours'));
        } elseif ($before_after == "after") {
            $current = date('H', strtotime($nowtime . ' + 1 hours')) . ":30";
            $timeafter1hour = date('H', strtotime($nowtime . ' + 2 hours')) . ":30";
            $timeafter2hour = date('H', strtotime($nowtime . ' + 3 hours')) . ":30";
            $timeafter3hour = date('H', strtotime($nowtime . ' + 4 hours')) . ":30";
            $left_right = date('Y-m-d H:i:s', strtotime($nowtime . ' + 1 hours'));
        }
        $time_slide = '<div class="col-xs-3 text-center">' . $current . '</div><div class="col-xs-3 text-center">' . $timeafter1hour . '</div><div class="col-xs-3 text-center">' . $timeafter2hour . '</div><div class="col-xs-3 text-center">' . $timeafter3hour . '</div>';
        $allchannels = tvGuide::model()->findAllByAttributes(array('id' => @$_REQUEST['id']));
        $channel_guide = array();
        $firsttimeinslide = gmdate('Y-m-d H', strtotime($left_right));
        $start_time = $firsttimeinslide . ":00:00";
        $end_time = gmdate('Y-m-d H', strtotime($start_time . ' + 4 hours')) . ":00:00";      
        $return = array('time_slide' => $time_slide, 'left_right' => $left_right,'nowtime'=>$firsttimeinslide, 'start_time'=>$start_time,'end_time'=>$end_time);
       
        echo json_encode($return);
    }
    
    
    
    
    public function Gettimewiseguide_old($allchannels,$nowtime,$start_time='',$end_time=''){
        $studio_id = Yii::app()->common->getStudiosId();
        $channel_guide = array();
        foreach($allchannels as $channels){
           $channel_guide[$channels->channel_name."*".$channels->channel_logo] = tvGuide::model()->allguideschannelwise($channels->id,$start_time,$end_time);
        }
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $base_cloud_url .= "tv_guide/channel_logo/";
        $guide ="";
        $i = 1;
        foreach($channel_guide as $key=>$channel){
            $total = count($channel);
            $logo_name = explode('*',$key);
            $name = @$logo_name[0];
            $logo = @$logo_name[1];
            if($logo !=""){
                $img_path = $base_cloud_url.$logo;
                $img = '<img height="35px;" src="'.$img_path.'" alt="'.$name.'">';
            }else{
                $img = $name;
            }
            $img_path = $base_cloud_url.$logo;
                    $guide .='<div class="col-lg-12 text-center" >
                        <div class="channel1 div17">
                            <div class="channel-number hidden-sm hidden-xs">'.$i.'</div>                
                            <div class="channel-list">'.$img.'</div>
                        </div>
                    <div class="channel1 div83" >';
            if(!empty($channel)){
            $percent_dark = 0;
            $j = 0;
            foreach($channel as $tvguide){
                $start_time = $tvguide['start_time'];
                $end_time = $tvguide['end_time'];
                $movie_id = $tvguide['movie_id'];                                
                $details  = $movie_id."_".$i."_".$start_time."_".$end_time;
                $details  = str_replace(' ', '', $details);
                $details  = str_replace(':', '_', $details);
                $percen = $this->calcdivpercen($start_time,$end_time,$nowtime);
                $div_percent = $percen['div_percent'];
                $div_before_percent = $percen['before_percent'];
                $now = gmdate('Y-m-d H:i:s');
                if($start_time <= $now && $end_time > $now){
                    $onclick="getvideodtls(this)";
                    $class="cursor_pointer";
                    if($details == $current_prog){
                        $class.=" red-bg";
                    }
                }else{
                    $onclick = "getvideoposterdtls(this)";
                    $class="cursor_pointer_program_tostart";
                    if($details == $prog_to_start){
                        $class.=" light-bg";
                    }
                }
                if($percen["before_percent"] == 0){
                   $percent_dark = $div_percent;
                   if($div_percent >= 1):
                   $guide .='<div class="channeldata '.$class.'" id="'.$details.'" style="width:'.$div_percent.'%" data-movie_id="'.$movie_id.'" data-start_time="'.$start_time.'" data-end_time="'.$end_time.'" onclick="'.$onclick.'" title="'.$tvguide["event_name"].'" data-toggle="tooltip" data-placement="top">'.$tvguide["event_name"].'</div>';
                   endif;
                }else{
                    if($j == 0){
                        $percent_dark += $div_before_percent + $div_percent;
                        if($div_before_percent >=1):
                        $guide.='<div class="noprogram" onclick="noprogram()" style="width:'.$div_before_percent.'%"></div>';
                        endif;
                        if($div_percent >= 1):
                        $guide .='<div class="channeldata '.$class.'" id="'.$details.'" style="width:'.$div_percent.'%" data-movie_id="'.$movie_id.'" data-start_time="'.$start_time.'" data-end_time="'.$end_time.'" onclick="'.$onclick.'" title="'.$tvguide["event_name"].'" data-toggle="tooltip" data-placement="top">'.$tvguide["event_name"].'</div>';
                        endif;
                    }else{
                        if($percent_dark >= $div_before_percent){
                            $div_before_percent = $percent_dark - $div_before_percent;
                        }else{
                           $div_before_percent =  $div_before_percent - $percent_dark; 
                        }
                       $percent_dark += $div_before_percent + $div_percent;
                       if($div_before_percent >= 1){
                       $guide.='<div class="noprogram" onclick="noprogram()" style="width:'.$div_before_percent.'%"></div>';
                       }
                       if($div_percent >= 1):
                       $guide .='<div class="channeldata '.$class.'" id="'.$details.'" style="width:'.$div_percent.'%" data-movie_id="'.$movie_id.'" data-start_time="'.$start_time.'" data-end_time="'.$end_time.'" onclick="'.$onclick.'" title="'.$tvguide["event_name"].'" data-toggle="tooltip" data-placement="top">'.$tvguide["event_name"].'</div>';
                       endif;
                    }
                }
                $j++;
                if($j == $total){
                    $after_percent = 100 - $percent_dark;
                    if($after_percent >= 1){
                    $guide.='<div class="noprogram"  onclick="noprogram()" style="width:'.$after_percent.'%"></div>';
                    }
                    $percent_dark = 0;
                } 
             }
            }else{
               $guide.='<div class="noprogram"  onclick="noprogram()" style="width:100%"></div>'; 
            }
            $guide .='</div></div>'; 
            $i++;
        }
        return $guide;   
    }
    
    
public function actionGettimewiseguide() {
        //ajax render view page for the Tv guide channel list
    
        if( $_POST['nowtime']=='' && $_POST['start_time']=='' && $_POST['end_time']=='')
        {
         $nowtime = gmdate('Y-m-d H');
         $start_time='';
         $end_time='';
        }
        else
        {
          $nowtime=$_POST['nowtime'];
          $start_time=$_POST['start_time'];
          $end_time=$_POST['end_time'];  
        }
       
        $studio_id = Yii::app()->common->getStudiosId();
       
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $base_cloud_url .= "tv_guide/channel_logo/";
        $channel_details = array();
        $allchannels = tvGuide::model()->findAllByAttributes(array('studio_id' => $studio_id));
        
        foreach ($allchannels as $key => $values) {
            $channel_details[$key]['id'] = $values['id'];
            $channel_details[$key]['channel_name'] = $values['channel_name'];
            $channel_details[$key]['channel_logo'] = $values['channel_logo'];
            $channel_details[$key]['channel_description'] = $values['channel_description'];
            $channel_details[$key]['base_cloud_url'] = $base_cloud_url;
        }

        foreach ($channel_details as $key => $channels) {
            $channel_details[$key]['channel_schedule'] = tvGuide::model()->allguideschannelwise($channels['id'], $start_time, $end_time);
        }
       $now = gmdate('Y-m-d H:i:s');
        foreach ($channel_details as $key => $schedule_details) {
            $total = count($schedule_details);
            $k=0;
            if (count($schedule_details['channel_schedule']) > 0) {
                $percent_dark = 0;
                $j = 0;
                foreach ($schedule_details['channel_schedule'] as $shkey => $eventslist) {
                    $start_time = $eventslist['start_time'];
                    $end_time = $eventslist['end_time'];
                    $movie_id = $eventslist['movie_id'];
                    $details = $movie_id . "_" . $i . "_" . $start_time . "_" . $end_time;
                    $details = str_replace(' ', '', $details);
                    $details = str_replace(':', '_', $details);                 
                    $percen = $this->calcdivpercen($start_time, $end_time, $nowtime);                  
                    $div_percent = $channel_details[$key]['channel_schedule'][$shkey]['div_percent'] = $percen['div_percent'];
                    $div_before_percent = $channel_details[$key]['channel_schedule'][$shkey]['div_before_percent'] = $percen['before_percent'];

                    $now = gmdate('Y-m-d H:i:s');
                    //set class of the channel bar.         
                    if ($start_time <= $now && $end_time > $now) { //for those time is in between the range.
                        $onclick = "getvideodtls(this)";
                        $class = "cursor_pointer";
                        if ($details == $current_prog) {
                            $class.=" red-bg";
                        }
                    } else {
                        $onclick = "getvideoposterdtls(this)"; //for those time is out of range.
                        $class = "cursor_pointer_program_tostart";
                        if ($details == $prog_to_start) {
                            $class.=" light-bg";
                        }
                    }
                    $channel_details[$key]['channel_schedule'][$shkey]['schedule_id'] = $details;
                    //fetch classes and other css details and add to the template
                    $channel_details[$key]['channel_schedule'][$shkey]['event_details'] = array();
                    if ($percen["before_percent"] == 0) {
                        $percent_dark = $div_percent;
                        if ($div_percent >= 1):
                            $newarray = array();
                            $newarray['width'] = $div_percent;
                            $newarray['class'] = $class;
                            $newarray['onclick'] = $onclick;

                            array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);

                        endif;
                    }else {
                        if ($j == 0) {

                            $percent_dark += $div_before_percent + $div_percent;
                            if ($div_before_percent >= 1):
                                $newarray = array();
                                $newarray['width'] = $div_before_percent;
                                $newarray['class'] = "noprogram";
                                $newarray['onclick'] = "noprogram()";

                                array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);

                            endif;
                            if ($div_percent >= 1):
                                $newarray = array();
                                $newarray['width'] = $div_percent;
                                $newarray['class'] = $class;
                                $newarray['onclick'] = $onclick;

                                array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);
                            endif;
                        }else {
                            if ($percent_dark >= $div_before_percent) {
                                $div_before_percent = $percent_dark - $div_before_percent;
                            } else {
                                $div_before_percent = $div_before_percent - $percent_dark;
                            }
                            $percent_dark += $div_before_percent + $div_percent;
                            //echo $div_before_percent."</br>";
                            if ($div_before_percent >= 1) {
                                $newarray = array();
                                $newarray['width'] = $div_before_percent;
                                $newarray['class'] = "noprogram";
                                $newarray['onclick'] = "noprogram()";

                                array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);
                            }
                            if ($div_percent >= 1):
                                $newarray = array();
                                $newarray['width'] = $div_percent;
                                $newarray['class'] = $class;
                                $newarray['onclick'] = $onclick;

                                array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);
                            endif;
                        }
                    }
                    $j++;
                    if ($j == $total) {
                        $after_percent = 100 - $percent_dark;
                        if ($after_percent >= 1) {
                            $newarray = array();
                            $newarray['width'] = $after_percent;
                            $newarray['class'] = "noprogram";
                            $newarray['onclick'] = "noprogram()";

                            array_push($channel_details[$key]['channel_schedule'][$shkey]['event_details'], $newarray);
                        }
                        $percent_dark = 0;
                    }
                }
            } else {

                $newarray = array();
                $newarray['width'] = "100";
                $newarray['class'] = "noprogram";
                $newarray['onclick'] = "noprogram()";
                $channel_details[$key]['channel_schedule'][$k]['event_details']=array();
                array_push($channel_details[$key]['channel_schedule'][$k]['event_details'], $newarray);
            }
        }
        $i++;
        $k++;
        
        echo $this->renderPartial('rendertimewiseguide', array('channel_details' => $channel_details));
        exit;
    }
    
    
    public function actiongetCurrentTime() {
        $studio_id = Yii::app()->common->getStudiosId();
        $getStudioConfig= StudioConfig::model()->getConfig($studio_id, 'tvguide_timezone');
        $zone_name=$getStudioConfig->config_value;
        $tvguide_zone=$zone_name==''?'UTC':$zone_name;
        $date = new DateTime("now", new DateTimeZone($tvguide_zone) );
        $zonal_current_time=$date->format('m/d H:i');
        echo $zonal_current_time;exit;    
    }

    public function actiongetChannelWiseDetail() {
        $channel_id = $_POST['channel_id'];
        $studio_id = Yii::app()->common->getStudiosId();
        //get the movie ids in that channel list
        $current_date = gmdate("Y-m-d H:i:s");

        $channel_sql = "SELECT a.channel_id,a.event_name,a.repeat_weekend,a.repeat_weekdays,a.repeat_hours,b.* FROM `stream_events` a,ls_schedule b where a.id=b.event_id and b.start_time <='" . $current_date . "' and b.end_time >'" . $current_date . "' and a.`channel_id`=" . $channel_id . "  and a.studio_id=" . $studio_id;
       
        $command = Yii::app()->db->createCommand($channel_sql);
        $list = $command->queryAll();
        $arr = array();
        $arr['movie_id'] = $list[0]['movie_id'];
        $arr['start_time'] = $list[0]['start_time'];
        $arr['end_time'] = $list[0]['end_time'];
        echo json_encode($arr);
    }

}
