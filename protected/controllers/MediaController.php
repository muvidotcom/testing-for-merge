<?php
class MediaController extends Controller{
	public function init() {
        require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        parent::init();
    }

    function actionactor_autocomplete(){
		$solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );
  
		if ( ! $solr->ping() ) {
			echo 'Solr service not responding.';
			exit;
		}
		$offset = 0;
		$limit = 5;
		$queries = array(
			"cat:Star AND name: ".$_REQUEST['term']." AND sku:".STUDIO_USER_ID
		);
		foreach ( $queries as $query ) {
		$response = $solr->search( $query, $offset, $limit );
		if ( $response->getHttpStatus() == 200 ) { 
		  if ( $response->response->numFound > 0 ) {
			foreach ( $response->response->docs as $doc ) { 
				$res[]=array('label'=>$doc->name,'id'=>$doc->content_id,'title' => $doc->title,'category' => $doc->cat);
			}
		  }
		}
		else {
		  echo $response->getHttpStatusMessage();
		}
		
		}
		echo json_encode($res);
		exit;
	}
	
	function actionlist() {
		$studio_id = $this->studio->id;
		$content = Yii::app()->general->content_count($studio_id);
		if ((isset($content) && ($content['content_count'] & 4))){
			if ($_SERVER['HTTP_X_PJAX'] == true) {
				$this->layout = false;
			}
		}
		if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
			unset(Yii::app()->session['backbtnlink']);
		}
		$pageUrl = Yii::app()->getBaseUrl(TRUE);
	
		$rec = isset($_REQUEST['selected_tab']) && intval($_REQUEST['selected_tab']) ? $_REQUEST['selected_tab'] : 0;
		$language_id = $this->language_id;
		$contentCount = Yii::app()->general->content_count($studio_id);	
		if (@$_REQUEST['menu_item_id'] || (@$_REQUEST['category'] || @$_REQUEST['subcategory'])) {
			if($_REQUEST['category']){
				$menuItems = Yii::app()->db->createCommand()
					->select('id as value,category_name as title,studio_id,binary_value,permalink,added_by,language_id,parent_id,is_poster,id_seq')
					->from('content_category')
					->where('id = '.$_REQUEST['category'])
					->setFetchMode(PDO::FETCH_OBJ)->queryRow();
			    $menuItems->title = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id,'value'=>$_REQUEST['category']), array('select' => 'title'))->title;
			}else{
				$menuItems = MenuItem::model()->findByPk($_REQUEST['menu_item_id']);
			}
			$margs['category_permalink'] = $menuItems->permalink;
			$margs['category_value'] = $menuItems->value;
			$margs['display_name'] = $menuItems->title;
			if($menuItems){
                $pageUrl .= '/'.$menuItems->permalink;
            } 
			if ($language_id != 20) {
				if($_REQUEST['category']){
					$trans_menu = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id, 'language_parent_id' => $_REQUEST['menu_item_id'], 'language_id' => $language_id, 'value'=> $_REQUEST['category']), array('select' => 'title'));
					if (!empty($trans_menu)) {
						$menuItems->title = utf8_encode($trans_menu->title);
					}else{
						$langcattitle = ContentCategories::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id, 'parent_id'=> $_REQUEST['category']), array('select' => 'category_name'));
						$menuItems->title = utf8_encode($langcattitle->category_name);
					}
				}else{
				$trans_menu = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id, 'language_parent_id' => $_REQUEST['menu_item_id'], 'language_id' => $language_id), array('select' => 'title'));
				if (!empty($trans_menu)) {
					$menuItems->title = utf8_encode($trans_menu->title);
				}
			}
			}else{
				$menuItems->title = utf8_encode($menuItems->title);
			}
			if (empty($menuItems)) {
				if(isset($_REQUEST['category']) || isset($_REQUEST['subcategory'])){}else{
					$content = '<div style="height:250px;text-align:center;"><h3>Sorry!, You are trying to access a invalid link . you may return to home or try using the search form</h3></div>';
					$this->render('//layouts/geoblock_error', array('content' => $content));
					exit;
				}
			}
			$dbcon = Yii::app()->db;
			$studio_id = Yii::app()->common->getStudiosId();

			//Added for category, subcategory content list
			$category_binary = $menuItems->value;
			$subcategory_binary = 0;
			if (@$_REQUEST['category'] || @$_REQUEST['subcategory']) {
				$category_binary = $_REQUEST['category'];
				$cat = ContentCategories::model()->categoryFromCommavalues($studio_id, $category_binary, 'category_name, permalink');
				if(@$_REQUEST['subcategory']){
					$subcategory_binary = $_REQUEST['subcategory'];
					$subcat = ContentSubcategory::model()->SubCategoryFromCommavalue($studio_id, $subcategory_binary, 'subcat_name, permalink');
					if($subcat){
						$pageUrl .= '/'.$subcat->permalink;
					}
				}
			}
			$pageUrl .= '/?';
			$page_size = $limit = Yii::app()->general->itemsPerPage();
                        if(isset($_REQUEST['limit']))
                            $page_size = $limit = $_REQUEST['limit'];
			$offset = 0;
			if (isset($_REQUEST['page'])) {
				$offset = ($_REQUEST['page'] - 1) * $limit;
			} else if (isset($_REQUEST['p'])) {
				$offset = ($_REQUEST['p'] - 1) * $limit;
				$page_number = $_REQUEST['p'];
			} else {
				$page_number = 1;
			}
			$filter_cond = ' AND F.status = 1';

			//genre filter
			if (isset($_REQUEST['genretag']) && $_REQUEST['genretag'] != '') {
				//$_REQUEST['genretag'] = preg_replace('/[^A-Za-z0-9\-,]/', '',@$_REQUEST['genretag']);
				$genretagdata = explode(',', $_REQUEST['genretag']);
			} else {
				$genretagdata = array();
			}
			if (isset($_REQUEST['genre']) && $_REQUEST['genre'] != '') {
				$_REQUEST['genre'] = preg_replace('/[^A-Za-z0-9\-,]/', '',@$_REQUEST['genre']);
				if (!in_array($_REQUEST['genre'], $genretagdata)) {
					array_push($genretagdata, $_REQUEST['genre']);
				}
				$newgen = $_REQUEST['genre'];
			}
			if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'rem') {
				$genretagdata = array_diff($genretagdata, array($_REQUEST['genre']));
				$newgen = '';
				if (count($genretagdata) == 0) {
					$url = $this->createUrl(Yii::app()->baseUrl . '/' . $menuItems->permalink . '?orderby=' . $_REQUEST['orderby']);
					$this->redirect($url);
					exit;
				}
			}
			$cnt = 0;
			if (count($genretagdata) > 0) {
				foreach ($genretagdata as $filval) {
					if ($filval != '') {
						$andor = ($cnt == 0) ? ' AND (' : ' OR';
						$filter_cond .= $andor . ' F.genre LIKE \'%' . $filval . "%' ";
						$cnt++;
					}
				}
				$filter_cond .= ")";
			}
			$genretag = implode(',', $genretagdata);
			$genretag = ltrim($genretag, ',');
			$pageUrl .= 'genretag='.$genretag.'&';
			//Filter with custom data
			$customFilterArr = array();
			if (@$_REQUEST['customFilter']) {
				foreach ($_REQUEST['customFilter'] AS $fkey => $fval) {
					$filterValue = array_values($fval);
					$filter_cond .= ' AND (';
					foreach ($filterValue AS $fvKeys => $fvVal) {
						if ($fvKeys) {
							$filter_cond .= " OR ";
						}
						$filter_cond .= '(LOWER(F.' . $fkey . ') = \'' . strtolower($fvVal) . '\') ';
						$customFilterArr[$fkey][$fvVal] = 1;
					}
					$filter_cond .= ")";
				}
			}

			$order = '';

			$cond = ' ';
			if (isset($_REQUEST['orderby'])) {
				$order = $_REQUEST['orderby'] = preg_replace('/[^A-Za-z0-9\-]/', '',@$_REQUEST['orderby']);
				if ($_REQUEST['orderby'] == 'lastupload') {
					$orderby = "  ORDER BY P.last_updated_date DESC";
				} else if ($_REQUEST['orderby'] == 'releasedate') {
					$orderby = "  ORDER BY P.release_date DESC";
				} else if ($_REQUEST['orderby'] == 'sortasc') {
					$orderby = "  ORDER BY P.name ASC";
				} else if ($_REQUEST['orderby'] == 'sortdesc') {
					$orderby = "  ORDER BY P.name DESC";
				}
				$pageUrl .= 'orderby='.$orderby;
			} else {
				$orderCond = ' ';
				if (@$subcategory_binary) {
					$orderCond .= " AND subcategory_value=" . $subcategory_binary;
				}
				$orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ' . $orderCond, array(':studio_id' => $studio_id, ':category_value' => $category_binary));
				if (@$orderData->method) {
					if (@$orderData->orderby_flag == 1) {// most viewed
						$mostviewed = 1;
						$orderby = "  order by Q.cnt DESC";
					} else if (@$orderData->orderby_flag == 2) {//Alphabetic A-Z
						$orderby = "  ORDER BY trim(P.name) ASC";
					} else if (@$orderData->orderby_flag == 3) {//Alphabetic Z-A
						$orderby = "  ORDER BY trim(P.name) DESC";
					} else {
						$orderby = " ORDER BY P.last_updated_date DESC ";
					}
				} else if ($orderData && @$orderData->ordered_stream_ids) {
					$orderby = " ORDER BY FIELD(P.movie_stream_id," . $orderData->ordered_stream_ids . ")";
				} else {
					$orderby = " ORDER BY P.last_updated_date DESC ";
				}
			}
			$filter_cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
			$renderPartialview = 'listView';
			$addQuery = '';
			$customFormObj = new CustomForms();
			$customData = $customFormObj->getFormCustomMetadat($studio_id);
			if($customData){
				foreach ($customData AS $ckey => $cvalue){
					$addQuery = ",F.".$cvalue['field_name'];
					//$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
					$customArr[] = array(
						'f_id' => trim($cvalue['f_id']),
						'field_name' => trim($cvalue['field_name']),
						'field_display_name' => trim($cvalue['f_display_name'])
					);
				}
			}
			
			if (isset(Yii::app()->request->cookies['for_preview']) && Yii::app()->request->cookies['for_preview']->value == 2) {
				$sql_data = "SELECT SQL_CALC_FOUND_ROWS P.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
						. "( SELECT M.movie_id,M.is_converted,M.video_duration,M.id AS movie_stream_id,M.embed_id,F.content_category_value,F.permalink,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id, M.last_updated_date ".$addQuery
						. "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=" . $studio_id;
				if ($category_binary > 0 || $subcategory_binary) {
                    /*$sql_data.= " AND (F.content_category_value & " . $category_binary . ")";
                    $sql_data.= " AND (F.content_subcategory_value & " . $subcategory_binary . ")";*/
					$sql_data.= " AND FIND_IN_SET($category_binary,F.content_category_value) ";
					if($subcategory_binary){
						$sql_data.= " AND FIND_IN_SET($subcategory_binary,F.content_subcategory_value) ";
					}
				} else {
                    //$sql_data.= " AND (F.content_category_value & " . $menuItems->value . ")";
					$sql_data.= " AND FIND_IN_SET($menuItems->value,F.content_category_value) ";
				}
				$sql_data .= " AND is_episode=0 " . $filter_cond . " ) AS P "
						. " LEFT JOIN livestream l ON P.movie_id = l.movie_id ";
				$where = "";
			} else {
				$sql_data1 = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
						. "( SELECT M.movie_id,M.is_converted,M.video_duration,M.id AS movie_stream_id,M.embed_id, F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id, M.last_updated_date ".$addQuery
						. "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.review_flag=0 AND M.studio_id=" . $studio_id;
				if ($category_binary > 0 || $subcategory_binary) {
                    /*$sql_data1.= " AND (F.content_category_value & " . $category_binary . ")";
                    $sql_data1.= " AND (F.content_subcategory_value & " . $subcategory_binary . ")";*/
					$sql_data1.= (@$category_binary)?" AND FIND_IN_SET(".$category_binary.",F.content_category_value) ":'';
					if($subcategory_binary){
						$sql_data1.= (@$subcategory_binary)?" AND FIND_IN_SET(".$subcategory_binary.",F.content_subcategory_value) ":'';
					}
				} else {
                    //$sql_data1.= " AND (F.content_category_value & " . $menuItems->value . ")";
					$sql_data1.= " AND FIND_IN_SET($menuItems->value,F.content_category_value) ";
				}
				$sql_data1 .= " AND is_episode=0 " . $filter_cond . " ) AS PM "
						. " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
				$visitor_loc = Yii::app()->common->getVisitorLocation();
				$country = $visitor_loc['country'];
				$sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
				$sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
				$where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
				
			}
			if ($mostviewed) {
				$sql_data .= " LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=" . $studio_id . " GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
			}
			$sql_data .= $where . " " . $orderby . " LIMIT " . $limit . " OFFSET " . $offset;
			$list = $dbcon->createCommand($sql_data)->queryAll();
			$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
			if ((isset($contentCount) && ($contentCount['content_count'] & 4))){
				$playlist = Yii::app()->db->createCommand()
					->select('*')
					->from('user_playlist_name u')
					->where('u.user_id = 0 AND u.studio_id=' . $studio_id . ' AND FIND_IN_SET('.$category_binary.',u.content_category_value)')
					->queryAll();
				$playlist_count = count($playlist);                               
			}
			
			$standaloneproduct =Yii::app()->db->createCommand()
				->select('*')
				->from('pg_product p')
				->where('p.studio_id=' . $studio_id . ' AND FIND_IN_SET('.$category_binary.',p.content_category_value)')
				->queryAll();
			$count_phy = count($standaloneproduct);
			if ($standaloneproduct) {
				foreach ($standaloneproduct AS $key => $val) {
					$standaloneproduct[$key]['poster'] = PGProduct::getpgImage($val['id'], 'standard');
					$price = Yii::app()->common->getPGPrices($val['id'], $default_currency_id);
					$standaloneproduct[$key]['price'] = Yii::app()->common->formatPrice($price['price'], $val['currency_id']);
					if (Yii::app()->common->isGeoBlockPGContent($val['id'])) {
						$tempprice = Yii::app()->common->getPGPrices($val['id'], $default_currency_id);
						if (!empty($tempprice)) {
							$standaloneproduct[$key]['sale_price'] = $tempprice['price'];
							$standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
						}
					} 
				}
			}
		} else {
			$this->redirect(Yii::app()->createAbsoluteUrl());exit;
		}
		$default_content = $this->contenttypes[0]->id;
		
		$meta = Yii::app()->common->listpagemeta(@$_REQUEST['menu_item_id']);
        $Words = utf8_decode($menuItems->title);
		if ($meta['title'] != '') {
			$title = $meta['title'];
		} else {
			if ($_SERVER['HTTP_X_PJAX'] == true) {
				$Words = strstr($Words, '?', true);
			}
			$title = $Words . ' | ' . $this->studio->name;
		}

		if ($meta['description'] != '') {
			$description = $meta['description'];
		} else {
			$description = '';
		}
		if ($meta['keywords'] != '') {
			$keywords = $meta['keywords'];
		} else {
			$keywords = '';
		}
		$sample = range($pages->offset + 1, $end);
		$movies = '';
        $ContentType = $Words;
		eval("\$title = \"$title\";");
		eval("\$description = \"$description\";");
		eval("\$keywords = \"$keywords\";");
		$this->pageTitle = $title;
		$this->pageDescription = $description;
		$this->pageKeywords = $keywords;
		$this->canonicalurl = Yii::app()->getBaseUrl(true) . '/' . $menuItems->permalink;
		$this->ogUrl = Yii::app()->getBaseUrl(true) . '/' . $menuItems->permalink;
		if($_SERVER['HTTP_X_PJAX'] == true){
			echo '<title>'.$title.'</title>';
		}
		//get the genre of this studio
		$genre = MovieTag::model()->findAll('taggable_type=:taggable_type AND studio_id=:studio_id ORDER BY name', array(':taggable_type' => 1, ':studio_id' => $this->studio->id));
		$gnrs = array();
		$genfilter = explode(',', $genretag);
		foreach ($genre as $gnr) {
			if (!in_array(trim($gnr['name']), $genfilter)) {
				$gnrs[] = $gnr['name'];
			}
		}

		$all_selected_genres = explode(',', $_REQUEST['genretag']);
		foreach ($all_selected_genres as $select_genre) {
			$selected_genres[] = $select_genre;
		}
		 
		$pageUrl = trim($pageUrl,'/?');
		Yii::app()->session['backbtnlink'] = $pageUrl;

			$k = 0;
			$ostudioArr = array(4301,4600,5174,5175,5477,3457,3060,3755,5446,9379); 
            if(in_array($studio_id,$ostudioArr)){
				$fobject = new Film();
				$data = $fobject->getContentListData($list, $margs, $customArr);
            }else{
				foreach ($list as $item) {
						$data[$k] = Yii::app()->general->getContentData($item['movie_id'], $is_episode = 0);
						$k++;
				}
            }
            if($k < $limit){
            if(!empty($playlist)){
				foreach ($playlist as $listdata){
						$data[$k]['list'] = $listdata;
						$play_id = $listdata['id'];
						$data[$k]['poster_playlist'] = $this->getPoster($play_id,'playlist_poster');
						$data[$k]['is_playlist'] = '1';
						$k++;
				}
                    $item_count = $k;
            }
            }
            if($k < $limit){
                    if(!empty($standaloneproduct)){
                            foreach ($standaloneproduct as $pgProduct){
                                    $data[$k]['physical'] = $pgProduct;
                                    $data[$k]['is_physical'] = '1';
                                    $k++;
                            }
                    }
            }
            if(!empty($standaloneproduct)){
                    $item_count = $item_count + $count_phy;
            }
            if(!empty($playlist)){
                    $item_count = $item_count + $playlist_count;
            }

            //Start the pagination
$newpage_url = $pageUrl;
//			$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $page_url = Yii::app()->general->full_url($newpage_url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');
            $pg = new bootPagination();
            $pg->pagenumber = $page_number;
            $pg->pagesize = $page_size;
            $pg->totalrecords = $item_count;
            $pg->showfirst = true;
            $pg->showlast = true;
            $pg->paginationcss = "pagination-normal";
            $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
            $pg->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1)
                    $pg->paginationUrl = $page_url . "&p=[p]";
            else
                    $pg->paginationUrl = $page_url . "?p=[p]";
            $pagination = $pg->process();

            $getListItemInfo = VdStudioConfig::model()->findByAttributes(array('studio_id'=>$studio_id), array('select' => 'list_item_info'));   
            //Assign the View
            $getListItemInfo = $getListItemInfo->list_item_info;
            $this->render('list', array('pagination' => $pagination, 'contents' => json_encode($data), 'orderby' => @$order, 'item_count' => @$item_count, 'page_size' => @$page_size, 'pages' => @$pages, 'sample' => @$sample, 'contentTypeInfo' => $contentTypeInfo, 'filterArr' => @$filterArr, 'renderPartialView' => $renderPartialview, 'category' => $menuItems, 'genres' => $gnrs, 'selected_genres' => $selected_genres, 'customFilterArr' => $customFilterArr, 'cat' => @$cat, 'subcat' => @$subcat, 'selected_tab' => $rec,'list_count'=>$playlist_count, 'VDListItemInfo'=>$getListItemInfo));
	}

    public function actioncomment_add(){
        if($this->studio->comment_activated == 1 && $this->user_id > 0){
		$comment = new Comment;
		$comment -> user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0)?$_REQUEST['user_id']:Yii::app()->user->id;
		$comment -> message = $_REQUEST['com_text'];
		$comment -> movie_id = $_REQUEST['movie_id'];
		$comment -> parent_id = isset($_REQUEST['parent_id'])?$_REQUEST['parent_id']:"";
		$comment -> created_at = date('Y-m-d H:i:s');
		$comment -> display_name = Yii::app()->user->display_name;
		$comment -> save();	
                $success_message = 'Thank you for your comment.';
                $comments = Yii::app()->general->comments($_REQUEST['movie_id']);
		$this->renderPartial('comment_list', array("comments" =>$comments, 'success_message' => $success_message));
            }
	}
                
        public function actionFilterSingleEpisode(){
			$episodes = $this->getEpisodestoShow($_REQUEST['movie_id'],$_REQUEST['s_no'],@$_REQUEST['contentTypePermalink']);        
			if(count($episodes) > 0){
				if(isset($_REQUEST['layout']) && $_REQUEST['layout'] == 'grid')
					$this->renderPartial('episode_grid', array("kvs" =>$episodes));
				else
					$this->renderPartial('episode_list', array("kvs" =>$episodes));                    
			}        
        }
        
	public function actionSingleEpisodes($layout = 'grid'){
		
		if(isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']){
			$studio_id = Yii::app()->common->getStudiosId();
			$Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'id'=>$_REQUEST['movie_id']));
            
			$this->layout= false;
			$cond = 'movie_id=:movie_id AND is_episode=:is_episode AND is_converted=:is_converted ';
			$params = array(':movie_id'=>$_REQUEST['movie_id'],':is_episode'=>1,':is_converted'=>1);
			if($_REQUEST['series'] != ''){
				$cond .= " AND series_number=:series_number";
				$params[':series_number']= $_REQUEST['series'];
			}
                        $kv = array();
			$contents = movieStreams::model()->findAll(array('condition'=>$cond,'params'=>$params,'order'=>'episode_number ASC','limit'=>100,'offset'=>$_REQUEST['page']));
			if(count($contents) > 0){
				if(defined('S3BUCKET_ID')){
					$bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
				}elseif(isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id){
					$bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
				}else{
					$bucketInfo = Yii::app()->common->getBucketInfo('',Yii::app()->common->getStudioId());
				}
				$format = new Format();
				
				foreach($contents as $content){
					$data = $content->attributes;
					$epstory = $format->make_links_clickable(@$data['episode_story']);
					$movie_stream_id  = $data['id'];
					$epi_url = CDN_HTTP.$bucketInfo['cloudfront_url'].'/uploads/movie_streams/full_movie/'.$movie_stream_id.'/'.$data['full_movie'];
					$epi_poster = $this->getPoster($movie_stream_id,'moviestream','episode');
					$kv[] = array('id'=>$data['id'],'embed_id'=>$data['embed_id'],'movie_id' => $data['movie_id'],'uniq_id'=>$Films->uniq_id, 'content_type_id'=>$Films->content_type_id,'ppv_plan_id'=>$Films->ppv_plan_id, 'episode_number' => $data['episode_number'],'series_number' => $data['series_number'],'episode_title' => $data['episode_title'],'episode_story' => $epstory,'episode_date' => $data['episode_date'],'episode_path' => $epi_url,'episode_poster' => $epi_poster,'permalink'=>$Films->permalink,'contentTypePermalink'=>@$_REQUEST['contentTypePermalink']);                                
                }
                if($layout == 'grid'){
                    $this->renderPartial('episode_grid', array("kvs" =>$kv));
                }else{
					$this->renderPartial('episode_list', array("kvs" =>$kv));
                }                            
			}else{
				echo "over";
			}
		}else{
			exit;
		}
	}            
	
	public function actionfilter_episode(){
		$episodes = $this->getEpisodes($_REQUEST['movie_id'],$_REQUEST['s_no']);
		$s_arr = array_pop($episodes);
		$format = new Format();
		foreach($episodes as $ke => $kv){
			$episodes[$ke]['contentTypePermalink'] = @$_REQUEST['contentTypePermalink'];
			$episodes[$ke]['episode_story'] = $format->make_links_clickable($kv['episode_story']);
		}
		
		$this->renderPartial('episode_list', array("kvs" =>$episodes));
	}
	
	public function actionget_episodes(){
		
		if(isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']){
                    $studio_id = Yii::app()->common->getStudiosId();
            	    $language_id = $this->language_id;
                    $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'id'=>$_REQUEST['movie_id']));
                        $sort_order = Yii::app()->custom->episodeSortOrder();
			$this->layout= false;
			$cond = 'movie_id=:movie_id AND is_episode=:is_episode AND is_converted=:is_converted AND episode_parent_id=0';
			$params = array(':movie_id'=>$_REQUEST['movie_id'],':is_episode'=>1,':is_converted'=>1);
			if($_REQUEST['series'] != ''){
				$cond .= " AND series_number=:series_number";
				$params[':series_number']= $_REQUEST['series'];
			}
			$streams = movieStreams::model()->findAll(array('condition'=>$cond,'params'=>$params,'order'=>'episode_number '.$sort_order,'limit'=>1000,'offset'=>$_REQUEST['page']));
			$item_count = count($streams);
			$kvs = array();
			if(count($streams) > 0){
				if(defined('S3BUCKET_ID')){
					$bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
				}elseif(isset(Yii::app()->user->s3bucket_id) && Yii::app()->user->s3bucket_id){
					$bucketInfo = Yii::app()->common->getBucketInfo(S3BUCKET_ID);
				}else{
					$bucketInfo = Yii::app()->common->getBucketInfo('',Yii::app()->common->getStudioId());
				}
				$format = new Format();
                                foreach($streams as $data){                            
					$movie_stream_id  = $data['id'];
                                        $langcontent = Yii::app()->custom->getTranslatedContent($movie_stream_id,1, $language_id);
		                    	if(array_key_exists($movie_stream_id,$langcontent['episode'])){
		                           $data['id'] = $langcontent['episode'][$movie_stream_id]->episode_parent_id;
		                           $data['episode_story'] = $langcontent['episode'][$movie_stream_id]->episode_story;
		                           $data['episode_title'] = $langcontent['episode'][$movie_stream_id]->episode_title;
		                        }
					$epstory = $format->make_links_clickable(@$data['episode_story']);
					$epi_url = CDN_HTTP.$bucketInfo['cloudfront_url'].'/uploads/movie_streams/full_movie/'.$movie_stream_id.'/'.$data['full_movie'];
					$epi_poster = $this->getPoster($movie_stream_id,'moviestream','episode');
					$kvs[] = array('id'=>$data['id'],'embed_id'=>$data['embed_id'],'movie_id' => $data['movie_id'],'uniq_id'=>$Films->uniq_id,'content_type_id'=>$Films->content_type_id, 'content_types_id' => $Films->content_types_id,'ppv_plan_id'=>$Films->ppv_plan_id,'episode_number' => $data['episode_number'],'series_number' => $data['series_number'],'episode_title' => $data['episode_title'],'episode_story' => $epstory,'episode_date' => $data['episode_date'],'episode_path' => $epi_url,'episode_poster' => $epi_poster,'permalink'=>$Films->permalink,'contentTypePermalink'=>@$_REQUEST['contentTypePermalink']);
                                }
				$this->renderPartial('episode_list', array("kvs" =>$kvs));
			}else{
				echo "over";
			}
		}else{
			exit;
		}
	}
	public function actionget_all_movies(){
            if(!empty($_REQUEST['celeb_id'])){
		$connection = Yii::app()->db;
		$cast_sql = "select films.id from films inner join movie_streams on films.id = movie_streams.movie_id inner join movie_casts on films.id = movie_casts.movie_id where films.parent_id = 0 AND movie_streams.episode_parent_id = 0 AND movie_casts.celebrity_id = ".$_REQUEST['celeb_id']." AND (movie_casts.movie_id IS NOT NULL and films.id IS NOT NULL and  movie_streams.studio_id = ".STUDIO_USER_ID." ) GROUP BY films.id,films.release_date ORDER BY films.release_date desc LIMIT ".$_REQUEST['page'].",1";
		$movie_id = $connection->createCommand($cast_sql)->queryAll();
		//print_r($movie_id);exit;
		if(count($movie_id) > 0){
			$data = $this->getMovieDetails($movie_id[0]['id']);
			$item = json_decode($data,TRUE);
			$this->layout= false;
			$this->renderPartial('/search/movie_block', array("movie_data" =>@$item));
		}else{echo "over";}   
            }else{echo "over";}
        }
        
    public function actionlistsubcategory(){
        $binary_value = $_REQUEST['binary_value'];
        $studio_id = Yii::app()->common->getStudiosId();
        $controller = Yii::app()->controller;
        $categories = array();
        $permalink = '';
        $cat_qry = "SELECT * FROM content_category WHERE studio_id = " . $studio_id . " AND id = '" . $binary_value . "' LIMIT 0, 1";
        $cat = Yii::app()->db->createCommand($cat_qry)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
        foreach($cat as $ct){
            $title = $ct->category_name;
            $permalink = $ct->permalink;
        }        
        $this->pageTitle = $title . ' | ' . $this->studio->name;
        $this->canonicalurl = Yii::app()->getBaseUrl(true) . '/' . $permalink;        
        
        $subcat_qry = "SELECT * FROM content_subcategory WHERE studio_id=" . $studio_id . " AND FIND_IN_SET(".$binary_value.",category_value) ORDER BY subcat_name ASC";
        $subcats = Yii::app()->db->createCommand($subcat_qry)->setFetchMode(PDO::FETCH_OBJ)->queryAll();   
        foreach($subcats as $category){
            $poster = $controller->getPoster($category->id, 'content_subcategory');
            $categories[] = array(
                'category_id' => $category->id,
                'category_name' => $category->subcat_name,
                'permalink' => $controller->siteurl.'/'.$permalink.'/'.$category->permalink,
                'category_poster' => $poster
            );            
        }
        $this->render('listsubcategory', array('subcategories' => json_encode($categories), 'title' => $title));
    } 
    public function actionallcontents() {
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        $studio_id = $this->studio->id;
        $dbcon = Yii::app()->db;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name,permalink,binary_value,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY category_name,IF(parent_id>0,parent_id,id) ASC";
        $cat = $dbcon->createCommand($sql)->queryAll();
        foreach ($cat as $category) {
            $category_id = $category['id'];
            if ($category['parent_id'] > 0) {
                $category_id = $category['parent_id'];
            }
            $poster = $this->getPoster($category_id, 'content_category');
            $category_name = $category['category_name'];
            $category_permalink = $this->siteurl . '/' . $category['permalink'];
            $binary_value = $category['binary_value'];
            $sql = "SELECT id,name,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND FIND_IN_SET(".$category['id'].",content_category_value) AND (parent_id=0 OR language_id=" . $language_id . ") ORDER BY name,parent_id ASC";
            $films = Yii::app()->db->createCommand($sql)->queryAll();
            $content_details = array();
            if (!empty($films)) {
                foreach ($films as $film) {
                    $id = $film['id'];
                    if ($film['parent_id'] > 0) {
                        $id = $film['parent_id'];
                    }
                    $content_details[$id] = $film;
                }
            }
            $channels[$category_name] = array(
                'category_name' => $category_name,
                'permalink' => $category_permalink,
                'category_poster' => $poster,
                'category' => $content_details
            );
        }
        $this->render('channel', array('contents' => json_encode($channels)));
    }

    public function actionallchannels() {
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        $studio_id = $this->studio->id;
        $dbcon = Yii::app()->db;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name,permalink,binary_value,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY category_name,IF(parent_id>0,parent_id,id) ASC";
        $cat = $dbcon->createCommand($sql)->queryAll();
        $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id, $studio_id);
        $favourites = $favlist['list'];
        foreach ($cat as $category) {
            $category_id = $category['id'];
            if ($category['parent_id'] > 0) {
                $category_id = $category['parent_id'];
            }
            $poster_category = $this->getPoster($category_id, 'content_category');
            $category_name = $category['category_name'];
            $category_list[$category['permalink']] = $category_name;
            $category_permalink = $this->siteurl . '/' . $category['permalink'];
            $binary_value = $category['binary_value'];
            $sql = "SELECT id,name,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND FIND_IN_SET(".$category['id'].",content_category_value) AND (parent_id=0 OR language_id=" . $language_id . ") ORDER BY name,parent_id ASC";
            $films = Yii::app()->db->createCommand($sql)->queryAll();
            $content_details = array();
            $poster_film = array();
            if (!empty($films)) {
                foreach ($films as $film) {
                    $id = $film['id'];
                    $fav_status = 1;
                    $login_status = 0;
                  if($user_id){
                        $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id,$studio_id,$user_id);
                        $fav_status = ($fav_status == 1)?0:1;
                        $login_status = 1;
                    }
                    $poster = $this->getPoster($id, 'films');
                    $permalink = $this->siteurl . '/' . $film['permalink'];
                    $content_details[$id] = array(
                        'content_id'=> $id,
                        'poster' => $poster,
                        'content_permalink' => $permalink,
                        'is_episode' => 0,
                        'fav_status' =>$fav_status,
                        'login_status' =>$login_status
                    );
                }
            }
            $channels[$category_name] = array(
                'category_name' => $category_name,
                'permalink' => $category_permalink,
                'link' => $category['permalink'],
                'category_poster' => $poster_category,
                'category' => $content_details
            );
        }
        $this->render('allchannel', array('contents' => json_encode($channels), 'favorite_content' => json_encode($favourites), 'category_list' => @$category_list));
    }
    public function actionFilterData(){
        if(isset($_REQUEST['contents'])){
            $contents = $_REQUEST['contents'];
            $lists = json_encode($contents['lists']);
            $this->renderPartial('filter', array('contents'=>$lists));
        }
    }
    public function actionDiscover(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('discover');
    }
    public function actionCollections(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('collections');
    }
    public function actionCollectionsGroup(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        if (isset($_REQUEST['category']) && $_REQUEST['category'] != "") {
            $category_id = ContentCategories::model()->findByAttributes(array('studio_id'=>$studio_id, 'permalink'=>$_REQUEST['category']), array('select' => 'id,category_name'));
            $posters = $this->getPoster($category_id['id'], 'content_category', 'original', $studio_id);
            $categoryname = $category_id['category_name'];
        }
        $this->render('collections_group', array('poster' => @$posters, 'categoryname' => @$categoryname));    
    }
    public function actionQuestionnaire(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('questionnaire');
    }
    public function actionQuestionnaireTrial(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('questionnaire_trial');
    }
    public function actionRecommendations(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('recommendations');
    }
    public function actionDiscoverLive(){
        $studio_id = Yii::app()->common->getStudiosId();
        $language_id = $this->language_id;
        $StudioName = $this->studio->name;
        $meta = Yii::app()->common->pagemeta('discover');
        if ($meta['title'] != '') {
            $title = $meta['title'];
        } else {
            $title = $this->Language['discover'];
        }
        if ($meta['description'] != '') {
            $description = $meta['description'];
        } else {
            $description = '';
        }

        if ($meta['keywords'] != '') {
            $keywords = $meta['keywords'];
        } else {
            $keywords = '';
        }
        eval("\$title = \"$title\";");
        eval("\$description = \"$description\";");
        eval("\$keywords = \"$keywords\";");
        $this->pageTitle = $title . ' | ' . $StudioName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        $this->render('discover_live');
    }
}
