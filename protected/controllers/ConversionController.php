<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
class ConversionController extends Controller{
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    function __construct() {
    }

    function actionUpdateMovieInfo(){
        
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        print_r($v);
		$lastKey = end(array_keys(($v)));
		$differentResolutions = '';
                $file_count = 0;
		foreach ($v as $key => $value) {
			if($key !=0 && $key !=1  && $key !=2 && $key !=3){
                            $file_count++;
                            if($lastKey == $key){
                                $differentResolutions .= $value;
                            }else{
                                $differentResolutions .= $value.',';
                            }
			}
		}
        $dateTime = date('Y-m-d H:i:s');
        $embed_id = md5($v[0]);
        
        $type = 'movie';
        $ms = movieStreams::model()->findByPk($v[0]);
        $studio_id = $ms->studio_id;
        
        
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        if($ms->encryption_key != '' && $ms->content_key != ''){
            $videoNameToBeUpdated = $v[2].".mp4";
            if($ms->is_mobile_drm_disable == 0){
                $videoNameToBeUpdated = $ms->full_movie;
            }
            $videoUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$v[0].'/'.$ms->full_movie;
        } else{
            $videoNameToBeUpdated = $v[2].".mp4";
            $videoUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$v[0].'/'.$v[2].'.mp4';
        }
        $duration = $this->getVideoDuration($videoUrl);
        
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.is_converted=1,ms.encoding_time=0,ms.is_demo=0,ms.embed_id ='".$embed_id."',ms.encoding_end_time='".$dateTime."',full_movie='".$videoNameToBeUpdated."',video_resolution='".$differentResolutions."',ms.video_duration = ".$duration." WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();
        $encodingData = Encoding::model()->findByAttributes(array('movie_stream_id'=>$v[0],'studio_id' => $studio_id));
        if($encodingData){
            $encodingLog = new EncodingLog;
            $encodingLog->studio_id= $encodingData->studio_id;
            $encodingLog->movie_stream_id= $encodingData->movie_stream_id;
            $encodingLog->size= $encodingData->size;
            $encodingLog->upload_time= $encodingData->upload_time;
            $encodingLog->encoding_server_id= $encodingData->encoding_server_id;
            $encodingLog->encoding_start_time= $encodingData->encoding_start_time;
            $encodingLog->expected_end_time= $encodingData->expected_end_time;
            $encodingLog->encoding_end_time = date('Y-m-d H:i:s');
            $encodingLog->status = "Completed";
            $encodingLog->save();
            $encodingData->delete();
        }
        $res = Yii::app()->common->getStorageSize($v[0],$studio_id,$type);
        $res_size = json_encode($res['res_size']);
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.original_file='".$res['original_file_count']."-".$res['original_file_size']."',ms.converted_file='".$res['converted_file_count']."-".$res['converted_file_size']."',ms.resolution_size = '".$res_size."' WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();

        $data = array(
            'studio_id' => $studio_id,
            'original_video_file_count' => $res['original_file_count'],
            'original_file_size' => $res['original_file_size'],
            'converted_video_file_count' => $res['converted_file_count'],
            'converted_file_size' => $res['converted_file_size'],
            'total_file_size' => $res['total_file_size'],
            'file_type' => $type,
            'last_updated_date' => $dateTime
        );
        Yii::app()->aws->addStorageLog($data);
        $videUploadedFromVideoGalley = array();
        if($v[3] != 0){
            $videUploadedFromVideoGalley = movieStreams::model()->findByAttributes(array('id'=>$v[0],'video_management_id' => $v[3]));
        }
        if(empty($videUploadedFromVideoGalley)){
            //Add video to video gallery
            if($ms->encryption_key != '' && $ms->content_key != ''){
                $fileName = $ms->full_movie;
                $fullPath = $videoUrl;
            } else{
                $fileName = $v[2].'.mp4';
                $fullPath = $videoUrl;
            }
            $videoGalleryId = Yii::app()->general->addvideoToVideoGallery($fileName,$fullPath,$studio_id);
            $movieStreams = movieStreams::model()->findByPk($v[0]);
            $movieStreams->video_management_id = $videoGalleryId;
            $movieStreams->save();
        }
    } 
    
    function actionupdateSyncVideo(){
        $this->layout = false;
        if($_REQUEST['movie_id'] != ''){
            $qry = "UPDATE movie_streams SET is_converted=1 WHERE  id=".$_REQUEST['movie_id'];
            Yii::app()->db->createCommand($qry)->execute();
        }
    }
    
    function actionUpdateMovieInfoDRM(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        print_r($v);
        $dateTime = date('Y-m-d H:i:s');
        $embed_id = md5($v[0]);
        $differentResolutions = "";
		$lastKey = end(array_keys(($v)));
        foreach ($v as $key => $value) {
			if($key !=0 && $key !=1  && $key !=2 && $key !=3){
                            $file_count++;
                            if($lastKey == $key){
                                $differentResolutions .= $value;
                            }else{
                                $differentResolutions .= $value.',';
                            }
			}
		}
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.encryption_key='".$v[2]."',ms.content_key='".$v[3]."',ms.video_resolution='".$differentResolutions."',ms.encoding_end_time='".$dateTime."',ms.is_multibitrate_offline=1,ms.has_hls_feed=1 WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();
    }
    
    function actionUpdateTrailerInfo(){
        $this->layout = false;
        $type = @$_REQUEST['type'];
        if($type=='physical'){
            $model = 'PGMovieTrailer';
            $table = 'pg_movie_trailer';
            $dir = 'physical';
        }else {
            $model = 'movieTrailer';
            $table = 'movie_trailer';
            $dir = 'trailers';
        }
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        print_r($v);
        $lastKey = end(array_keys(($v)));
        $differentResolutions = '';
        $file_count = 0;
        foreach ($v as $key => $value) {
            if($key !=0 && $key !=1  && $key !=2 && $key !=3){
                $file_count++;
                if($lastKey == $key){
                    $differentResolutions .= $value;
                }else{
                    $differentResolutions .= $value.',';
                }
            }
        }
        if($type=='physical'){
            $qry = 'select f.studio_id,mt.trailer_file_name as fileName from pg_movie_trailer mt, pg_product f,studios s where f.id = mt.movie_id AND f.studio_id = s.id AND mt.id ="'.$v[0].'"';
        } else {
        $qry = 'select f.studio_id,mt.trailer_file_name as fileName from movie_trailer mt, films f,studios s where f.id = mt.movie_id AND f.studio_id = s.id AND mt.id ='.$v[0];
        }
        $dataOfMT= Yii::app()->db->createCommand($qry)->queryAll();
        print_r($dataOfMT[0]['studio_id']);
        $studio_id = $dataOfMT[0]['studio_id'];
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $bucketCloudfrontUrl = CDN_HTTP.$bucketInfo['cloudfront_url'];
        $signedBucketPath = $bucketInfo['signedFolderPath'];
        $trailer_s3url = '/'.$signedBucketPath.'uploads/'.$dir.'/'.$v[0].'/'.$v[2].".mp4";
        $trailer_url = $bucketCloudfrontUrl.$trailer_s3url;
        $dateTime = date('Y-m-d H:i:s');
        $type = ($type=='physical')?'physical':'trailer';
        
        
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $videoUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/'.$dir.'/'.$v[0].'/'.$v[2].".mp4";
        $duration = $this->getVideoDuration($videoUrl);
        if($type=='physical'){
            $qry = "UPDATE pg_movie_trailer mt JOIN pg_product f SET mt.is_converted=1,mt.encoding_end_time='".$dateTime."',mt.trailer_file_name='".$v[2].".mp4',mt.video_remote_url='".$trailer_url."' ,mt.video_resolution='".$differentResolutions."',mt.video_duration ='".$duration."' WHERE  mt.id=".$v[0]." AND f.uniqid='".$v[1]."'";
        } else {
        $qry = "UPDATE movie_trailer mt JOIN films f SET mt.is_converted=1,mt.encoding_end_time='".$dateTime."',mt.trailer_file_name='".$v[2].".mp4',mt.video_remote_url='".$trailer_url."' ,mt.video_resolution='".$differentResolutions."',mt.video_duration ='".$duration."' WHERE  mt.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        }
        Yii::app()->db->createCommand($qry)->execute();
        
        $res = Yii::app()->common->getStorageSize($v[0],$studio_id,$type);
        $res_size = json_encode($res['res_size']);
        if($type=='physical'){
            $qry = "UPDATE pg_movie_trailer mt JOIN pg_product f SET mt.resolution_size = '".$res_size."',mt.video_duration =".$duration." WHERE  mt.id=".$v[0]." AND f.uniqid='".$v[1]."'";
        } else {
            $qry = "UPDATE movie_trailer mt JOIN films f SET mt.resolution_size = '".$res_size."',mt.video_duration =".$duration." WHERE  mt.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        }
        Yii::app()->db->createCommand($qry)->execute();
        
        $data = array(
            'studio_id' => $studio_id,
            'original_video_file_count' => $res['original_file_count'],
            'original_file_size' => $res['original_file_size'],
            'converted_video_file_count' => $res['converted_file_count'],
            'converted_file_size' => $res['converted_file_size'],
            'total_file_size' => $res['total_file_size'],
            'file_type' => $type,
            'last_updated_date' => $dateTime
        );
        Yii::app()->aws->addStorageLog($data);
        $videUploadedFromVideoGalley = array();
        if($v[3] != 0){
            $videUploadedFromVideoGalley = $model::model()->findByAttributes(array('id'=>$v[0],'video_management_id' => $v[3]));
        }
        if(empty($videUploadedFromVideoGalley)){
            //Add video to video gallery
            $fileName = $v[2].'.mp4';
            echo $fullPath = $bucketName.'/'.$signedBucketPath.'uploads/'.$dir.'/'.$v[0].'/'.$fileName;
            $videoGalleryId = Yii::app()->general->addvideoToVideoGallery($fileName,$fullPath,$studio_id);
            $movieTrailer = $model::model()->findByPk($v[0]);
            $movieTrailer->video_management_id = $videoGalleryId;
            $movieTrailer->save();
        }
    }
/**
 * @method public UpdateUploadInfo() It will update the upload information.
 * @author GDR<support@muvi.com>
 * @return bool True/false
 */	
	function actionUpdateUploadInfo(){
		$this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        echo "<pre>";print_r($v);
		$lastKey = end(array_keys(($v)));
		$sql = 'SELECT f.name,f.uniq_id,ms.* FROM films f, movie_streams ms WHERE f.id = ms.movie_id AND ms.id = '.$v[0].' AND f.uniq_id = \''.$v[1]."'";
		$data = Yii::app()->db->createCommand($sql)->queryAll();
		print $sql;print_r($data);//exit;
		if($data){
			$s3 = Yii::app()->common->connectToAwsS3($data[0]['studio_id']);
			$bucketInfo = Yii::app()->common->getBucketInfo('',$data[0]['studio_id']);
			$bucket = $bucketInfo['bucket_name'];
                        $folderPath = Yii::app()->common->getFolderPath("",$data[0]['studio_id']);
                        $signedBucketPath = $folderPath['signedFolderPath'];
			foreach($data AS $key=>$val){
				$streams = $val;
				$s3dir = $signedBucketPath.'uploads/movie_stream/full_movie/'.$streams['id']."/";
				if($streams['full_movie']){
					$response = $s3->getListObjectsIterator(array(
						'Bucket' => $bucket,
						'Prefix' => $s3dir
					));
					//print_r($response);
					foreach ($response as $object) {
						if($object['Key'] != $s3dir.$v[2]){
							$result = $s3->deleteObject(array(
								'Bucket' => $bucket,
								'Key'    => $object['Key']
							));
						}
						print_r($object);	
					}
				}
			}
		}else{
			return false;
		}
		$cond =  'ms.is_converted=0';
		if($data[0]['is_downloadable']==1){
			$cond =  'ms.is_converted=1';
		}
        $dateTime = date('Y-m-d H:i:s');
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.after_encoding=0,{$cond},is_download_progress=0,ms.upload_end_time='".$dateTime."',ms.has_sh=0,ms.video_resolution='',ms.mail_sent_for_video=0,full_movie='".$v[2]."' WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();
	}
        
    function actionUpdateUrlUploadInfo(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['video_name']);
        //print_r($v);
        
        $rec_id=$v[0];
        $video_name=$v[1];
        if(@$v[2] != '' && @$v[2] != 0){
            LivestreamVideoSync::model()->deleteByPk($v[2]);
        }
        $flag_uploaded=0;
        $video_details = VideoManagement::model()->findbyPk($rec_id);
        $studio_id=$video_details['studio_id'];
        $video_details->flag_uploaded=0;
        $video_details->video_name = $video_name;
        $video_details->save();
        if($video_details){
            Yii::app()->db->createCommand()->delete('video_sync_temp','video_management_id =:video_management_id AND studio_id =:studio_id', array(':video_management_id' => $rec_id, ':studio_id' => $studio_id));
        }
        $studio = Studio::model()->getStudioBucketInfo($studio_id);
        $newUser = $studio['new_cdn_users'];
       
        //add the video properties, thumb image creation
        
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
		$s3url  = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath($newUser,$studio_id);

        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";

        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
		$bucketCloudfrontUrl = 'http://' .$bucket.'.'.$s3url;
        $galleryVideoUrl = $unsignedFolderPathForVideo . 'videogallery/' . $video_name;
        $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryVideoUrl;$extsn=explode('.',$video_name);
        $ext=$extsn[1];
        //Generate video thumbnail
        $ffmpeg = FFMPEG_PATH;

        $path_parts = pathinfo($video_name);
        //change thumb image name as per ther video file
        $imgname = $path_parts['filename'] . '.jpg';
        $dir2 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/videogallery';
        if (!is_dir($dir2)) {
            mkdir($dir2);
            @chmod($dir2, 0777);
        }

        $dir1 = $dir2 . '/' . $studio_id;
        if (!is_dir($dir1)) {
            mkdir($dir1);
            @chmod($dir1, 0777);
        }
        $dir = $dir1 . '/images';

        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        $img_path = $dir . '/' . $imgname;
        $thumbImageFolder = $dir . '/thumbImage/';
        if (!is_dir($thumbImageFolder)) {
            mkdir($thumbImageFolder);
            @chmod($thumbImageFolder, 0777);
        }
        $second = 1;

        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $command = $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
        } else {
            $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
        }
        $output = shell_exec($command);
      
        $video_prop=Yii::app()->common->get_videoproperties($output,$ext);
        $resolution=$video_prop['resolution'];
        $ex_resolution=explode('x',$resolution);
        $width=$ex_resolution[0];
        $height=$ex_resolution[1];
        $maxWidth=280;
        $maxHeight=160;
       
        if ($maxWidth > $height) {
            $as_height = 160;
            $as_width  = 280;
        } else {
            $as_width  = floor(($width/$height)*$maxHeight);
            $as_height = $maxHeight;
        }       
        $size = $as_width.'x'.$as_height;
        $scale = "scale=".$as_width.":".$as_height;
//        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
//            $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf $scale $img_path -an -vcodec mjpeg 2>&1";
//        } else {
//            $cmd = "sudo $ffmpeg -ss $second -i $s3viewdir -frames 1 -q:v 1 -vf $scale $img_path -an -vcodec mjpeg 2>&1";
//        }
        $second = 1;
        
        // get the duration and a random place within that
        $cmd = "$ffmpeg -i $s3viewdir 2>&1";
        if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
            $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
            $second = rand(1, ($total - 1));
            $countFrame = Yii::app()->general->ImageDuration($total);
        }
        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
        } else {
            $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
        }
        
         exec($cmd, $output12, $return);
        $dataForFunction = array();
        $dataForFunction['studio_id'] = $studio_id;
        $dataForFunction['video_management_id'] = $video_details->id;
        $dataForFunction['video_url'] = $s3viewdir;
        $dataForFunction['countFrame'] = @$countFrame;
        $generateThumbSlider = Yii::app()->general->videoGalleryThumbSlider($dataForFunction);
       $head = array_change_key_case(get_headers($s3viewdir, TRUE));
      //  echo "<pre>";
       // print_r($head);
        $filesize1 = $head['content-length'];
        $filesize= Yii::app()->common->bytes2English($filesize1);
        $video_prop['FileSize']=$filesize;
        //$video_prop = array('FileSize' =>"Filesize(MB) :".$filesize , 'Duration' => $output[20], 'Audio' => $output[25], 'Video' => $output[21]);
        $video_properties = json_encode($video_prop);
        $src_path = $img_path;
        //$dest_file = $unsignedFolderPathForVideo.'videogallery/videogallery-image/'.$studio_id.'/'.$imgname;
        if ($newUser) {
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $imgname;
        } else {
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $imgname;
        }
        $acl = 'public-read';
        $result = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => $dest_file,
            'SourceFile' => $src_path,
            'ACL' => $acl,
        ));
        unlink($src_path);
        $video_details->thumb_image_name = $imgname;
        $video_details->video_properties = $video_properties;
        $video_details->duration = @$video_prop['duration'];
        if($generateThumbSlider == 1){
            $video_details-> thumbnail_status =1;  
        }
        $video_details->save();
        
    } 
    
    function actionUpdateVideoImageProperties(){
        $this->layout = false;
        //print_r($v);
        
        $studio_id = $_REQUEST['studio_id'];
        $idNotIn = @$_REQUEST['idNotIn'];
        if($idNotIn != ''){
            $condition =' and id not in('.$idNotIn.')';
        }
        $studio = Studio::model()->getStudioBucketInfo($studio_id);
        $newUser = $studio['new_cdn_users'];
       
        //add the video properties, thumb image creation
        
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];

        $folderPath = Yii::app()->common->getFolderPath($newUser,$studio_id);

        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";

        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
        echo $query = "select * from video_management where thumb_image_name ='' and studio_id=".$studio_id.$condition;
        $videoGallery = Yii::app()->db->createCommand($query)->queryall();
        print_r($videoGallery);
        if($videoGallery){
            foreach ($videoGallery as $videoGallerykey => $videoGalleryvalue) {
                $imageVid = str_replace(' ', '_', $videoGalleryvalue['video_name']);
                $video_name = str_replace(' ', '%2520', $videoGalleryvalue['video_name']);
                $video_details = VideoManagement::model()->findbyPk($videoGalleryvalue['id']);
                if ($newUser) {
                    $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $video_name;
                } else {
                    $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $studio_id . '/' . $video_name;
                }
                $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryVideoUrl;
                $extsn=explode('.',$video_name);
                $ext=$extsn[1];
                //Generate video thumbnail
                $ffmpeg = FFMPEG_PATH;

                $path_parts = pathinfo($imageVid);
                //change thumb image name as per ther video file
                $imgname = $path_parts['filename'] . '.jpg';
                $dir2 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/videogallery';
                if (!is_dir($dir2)) {
                    mkdir($dir2);
                    @chmod($dir2, 0777);
                }

                $dir1 = $dir2 . '/' . $studio_id;
                if (!is_dir($dir1)) {
                    mkdir($dir1);
                    @chmod($dir1, 0777);
                }
                $dir = $dir1 . '/images';

                if (!is_dir($dir)) {
                    mkdir($dir);
                    @chmod($dir, 0777);
                }
                $img_path = $dir . '/' . $imgname;
                //echo $img_path;
                $second = 1;

                if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                    $command = $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
                } else {
                    $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
                }
                //echo "<br/>".$command."<br/>";
                $output = shell_exec($command);

                $video_prop=Yii::app()->common->get_videoproperties($output,$ext);
                //print_r($video_prop);
                if(!empty($video_prop)){
                    $head = array_change_key_case(get_headers($s3viewdir, TRUE));
                    $filesize1 = $head['content-length'];
                    $filesize= Yii::app()->common->bytes2English($filesize1);
                    $video_prop['FileSize']=$filesize;
                    $video_properties = json_encode($video_prop);
                    $video_details->video_properties = $video_properties;
                    $video_details->save();
                }
                $resolution=$video_prop['resolution'];
                $ex_resolution=explode('x',$resolution);
                $width=$ex_resolution[0];
                $height=$ex_resolution[1];
                $maxWidth=280;
                $maxHeight=160;

                if ($maxWidth > $height) {
                    $as_height = 160;
                    $as_width  = 280;
                } else {
                    $as_width  = floor(($width/$height)*$maxHeight);
                    $as_height = $maxHeight;
                }       
                $size = $as_width.'x'.$as_height;
                $scale = "scale=".$as_width.":".$as_height;
                $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -frames 1 -q:v 1 -vf $scale $img_path -an -vcodec mjpeg 2>&1";
                //echo "<br/>".$cmd."<br/>";
                exec($cmd, $output12, $return);
                $src_path = $img_path;

                if ($newUser) {
                    $dest_file = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $imgname;
                } else {
                    $dest_file = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $imgname;
                }

                $acl = 'public-read';
                $result = $s3->putObject(array(
                    'Bucket' => $bucket,
                    'Key' => $dest_file,
                    'SourceFile' => $src_path,
                    'ACL' => $acl,
                ));
                unlink($src_path);
                $video_details->thumb_image_name = $imgname;
                $video_details->video_name = $video_name;
                $video_details->save();
            }
        }
        
    }
        
        
        
    function actionUploadUrlVideoNotDownloaded(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['rec_id']);
        $rec_id=$v[0];
        if(@$v[1] != '' && @$v[1] != 0){
            LivestreamVideoSync::model()->deleteByPk($v[1]);
        }
        
        $flag_uploaded=2;
        //$studio_id = Yii::app()->user->studio_id;
        $video_details = VideoManagement::model()->findbyPk($rec_id);
        if($video_details){
            Yii::app()->db->createCommand()->delete('video_sync_temp','video_management_id =:video_management_id AND studio_id =:studio_id', array(':video_management_id' => $rec_id, ':studio_id' => $studio_id));
        }
        $qry = "UPDATE video_management set flag_uploaded=".$flag_uploaded." WHERE  id =".$rec_id ;
        Yii::app()->db->createCommand($qry)->execute();
        
    }
    
    public function actionGetVideoSizeRes()
    {
        $sql = "SELECT id,studio_id,full_movie FROM movie_streams WHERE is_converted = 1";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $type = 'movie';
        foreach ($data as $value) {
            $movie_id = $value['id'];
            $studio_id = $value['studio_id'];
            $full_movie = $value['full_movie'];
            // get video resolution and size
            $res = Yii::app()->common->getStorageSize($movie_id,$studio_id,$type);
            if(isset($res['res_size']) && !empty($res['res_size'])){
                $res_size = json_encode($res['res_size']);
                $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
                // get video duration
                $bucketName = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $videoUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$movie_id.'/'.$full_movie;
                $duration = $this->getVideoDuration($videoUrl);
                $qry = "UPDATE movie_streams ms SET ms.resolution_size = '".$res_size."', ms.video_duration = '".$duration."' WHERE ms.id=".$movie_id;
                Yii::app()->db->createCommand($qry)->execute();
            }
        }
    }
    
    public function actiongetVideoPosterPath() {
        $file = array();
        //$file[]=(array('stream_id' => 6122, 'imagename' =>'16X9_DKS_Ep1TROG.jpg'));
        $file[]=(array('permalink' => 'american-motor-stories', 'imagename' =>'AMSeries_Cover.jpg'));
        $file[]=(array('permalink' => 'auto-revolution-roadside-with-delmo', 'imagename' =>'Autorevolution_Cover.jpg'));
        $file[]=(array('permalink' => 'ventura-nationals', 'imagename' =>'Ventura_Cover.jpg'));
        $file[]=(array('permalink' => 'hot-rod-gang', 'imagename' =>'hot-rod-gang.jpg'));


        $studio_id = 1132;
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $singedpath = $folderPath['signedFolderPath'];
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $unsingedpath = $folderPath['unsignedFolderPath'];
        $con = Yii::app()->db;
        $client = Yii::app()->common->connectToAwsS3($studio_id);

        foreach ($file as $key => $value) {
            //$sql = 'select ms.id,p.id as posterId,p.poster_file_name from movie_streams ms ,posters p where ms.id=p.object_id and p.object_type="moviestream" and ms.studio_id='.$studio_id.' and ms.id='.$value['stream_id'];
            $sql = 'select f.id,p.id as posterId,p.poster_file_name from films f ,posters p where f.id=p.object_id and p.object_type="films" and f.studio_id='.$studio_id.' and f.permalink="'.$value['permalink'].'"';
            $data  = Yii::app()->db->createCommand($sql)->queryAll();
            if($data){
                foreach($data as $dataKey => $dataVal)
                if(isset($dataVal["poster_file_name"]) && $dataVal["poster_file_name"] != ''){
                    $upload_path = $unsingedpath.'public/system/posters/'.$dataVal['posterId']. '/roku/' . $dataVal['poster_file_name'];
                    $img_path = $_SERVER['DOCUMENT_ROOT'].'/motornation/'.$value['imagename'];
//                    $result = $client->putObject(array(
//                        'ACL' => 'public-read',
//                        'Bucket' => $bucketName,
//                        'Key' => $upload_path,
//                        'SourceFile' => $img_path
//                            )
//                    );
                    $file[$key]['imgUr'] = 'http://'.$bucketName.'.'.$s3url.'/'.$upload_path;
                    $file[$key]['imageFullPath'] = $img_path;
//                    $posterTable = Poster::model()->findbyPK($dataVal['posterId']);
//                    $posterTable->is_roku_poster = 1;
//                    $posterTable->save();
                }
            }
        }

        echo "<pre>";
        print_r($file);
        exit;
        $studio_id = 1132;
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $singedpath = $folderPath['signedFolderPath'];
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $unsingedpath = $folderPath['unsignedFolderPath'];
        $sql = 'select f.id,ms.id AS stream_id,ms.full_movie,ms.is_episode from films f ,movie_streams ms where f.id=ms.movie_id and ms.is_episode=1 and ms.full_movie != "" and f.content_types_id =3 and ms.studio_id='.$studio_id.' group by ms.movie_id';
        //$sql = 'select f.id,ms.id AS stream_id,ms.full_movie,ms.is_episode from films f ,movie_streams ms where f.id=ms.movie_id and ms.full_movie != "" and ms.studio_id='.$studio_id;
        //$sql = 'select ms.id AS stream_id,ms.full_movie,ms.is_episode from movie_streams ms where ms.full_movie != "" and ms.is_poster=1 and ms.is_episode=1 and ms.studio_id='.$studio_id;
        $data  = Yii::app()->db->createCommand($sql)->queryAll();
        $videoDetails = array();
        $i = 0;
        $con = Yii::app()->db;
        $ffmpeg = FFMPEG_PATH;
        $client = Yii::app()->common->connectToAwsS3($studio_id);
        foreach ($data AS $k => $v) {
            $videoDetails[$i]['file'] = 'http://'.$bucketName.'.'.$s3url.'/'.$singedpath.'uploads/movie_stream/full_movie/'.$v['stream_id'].'/'.urlencode($v['full_movie']);
            if ($v['is_episode']) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  =". $v['stream_id'] ."  AND object_type='moviestream' ";
                
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $videoDetails[$i]['poster'] = 'http://'.$bucketName.'.'.$s3url.'/'.$unsingedpath.'public/system/posters/'.$val['id']. '/roku/' . $val['poster_file_name'];
                        $imgname = $val['poster_file_name'];
                        $postId= $val['id'];
                        $upload_path = $unsingedpath.'public/system/posters/'.$val['id']. '/roku/' . $val['poster_file_name'];
                    }
                }
            } 
            else {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id =" . $v['id'] . " AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                        $videoDetails[$i]['poster'] = 'http://'.$bucketName.'.'.$s3url.'/'.$unsingedpath.'public/system/posters/'.$val['id']. '/roku/' . $val['poster_file_name'];
                        $imgname = $val['poster_file_name'];
                        $postId= $val['id'];
                        $upload_path = $unsingedpath.'public/system/posters/'.$val['id']. '/roku/' . $val['poster_file_name'];
                    }
                }
            }
            
            
//            $s3viewdir = 'http://'.$bucketName.'.'.$s3url.'/'.$singedpath.'uploads/movie_stream/full_movie/'.$v['stream_id'].'/'.urlencode($v['full_movie']);
//
//            $upload_dir = $_SERVER['DOCUMENT_ROOT'] . "/images/videoThumbnail/";
//            $dir = $upload_dir .$postId  . "/";
//
//            if (!is_dir($dir)) {
//                mkdir($dir);
//                @chmod($dir, 0775);
//            }
//            $dir .="original/";
//            if (!is_dir($dir)) {
//                mkdir($dir);
//                chmod($dir, 0775);
//            }
//            $img_path = $dir . $imgname;
//            //echo "image path===".$img_path;
//            //$img_path = $upload_dir.'episode'.$val['episode_number'].".jpg";
//
//            $second = 1;
//
//            // get the duration and a random place within that
//            $cmd = "$ffmpeg -i $s3viewdir 2>&1";
//            if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
//                $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
//                $second = rand(1, ($total - 1));
//            }
//            //screenshot size
//            $size = '560x312';
//
//            // get the screenshot
//            //$cmd = "$ffmpeg -ss $second -i  $s3viewdir $img_path -r 1 -y -s $size -vframes 1 -an -vcodec mjpeg";
//            if (HOST_IP == '127.0.0.1') {
//                $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=800:600 $img_path -an -vcodec mjpeg 2>&1";
//            } else {
//                $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=800:600 $img_path -an -vcodec mjpeg 2>&1";
//            }
//            exec($cmd, $output, $return);
//
//
//            $result = $client->putObject(array(
//                'ACL' => 'public-read',
//                'Bucket' => $bucketName,
//                'Key' => $upload_path,
//                'SourceFile' => $img_path
//                    )
//            );
//            $posterTable = Poster::model()->findbyPK($postId);
//            $posterTable->is_roku_poster = 1;
//            $posterTable->save();
            $i++;
        }
        print_r($videoDetails);
    }
    
    public function actionupdatePalflexvideoData(){
        $studio_id = 1367;
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $qry = "select * from movie_streams where full_movie !='' and thirdparty_url = '' and is_converted=1 and video_management_id =0 and studio_id=1367";
        $dataResponse = Yii::app()->db->createCommand($qry)->queryAll();
        echo "<pre>";
        print_r($dataResponse);
        foreach($dataResponse as $dataResponseKey => $dataResponseVal){
            $fileName = $dataResponseVal['full_movie'];
            $fullPath = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$dataResponseVal['id'].'/'.$fileName;
            echo "<br>".$fullPath;
            $videoGalleryId = Yii::app()->general->addvideoToVideoGallery($fileName,$fullPath,$studio_id);
            $movieStreams = movieStreams::model()->findByPk($dataResponseVal['id']);
            $movieStreams->video_management_id = $videoGalleryId;
            $movieStreams->save();
        }
    }
    
    public function actionupdateStreamMPD(){
        if(@$_REQUEST['studio_id'] != ''){
            $studio_id = $_REQUEST['studio_id'];
            $is_drm_enable = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'drm_enable');
            if(@$is_drm_enable['config_value'] == 1){
                $streamData = Yii::app()->db->createCommand()
                    ->select('ms.id, ms.video_resolution, ms.content_key, ms.encryption_key')
                    ->from('movie_streams ms')
                    ->where('ms.is_converted =1 AND ms.full_movie!=\'\' AND ms.studio_id =' . $studio_id)
                    ->queryAll();
                if($streamData){
                    $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                    $signedBucketPath = $folderPath['signedFolderPath'];
                    $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                    $bucketName = $bucketInfo['bucket_name'];
                    $s3Url = $bucketInfo['s3url'] ;
                    $folderPathS3 = $signedBucketPath . 'uploads/movie_stream/full_movie/';
                    $s3 = Yii::app()->common->connectToAwsS3($studio_id);
                    $mpdFileChanged = array();
                    foreach ($streamData as $streamDatakey => $streamDatavalue) {
                        if($streamDatavalue['content_key'] && $streamDatavalue['encryption_key']){
                            $mpdFileChanged[$streamDatavalue['id']]['Video'] = 'No';
                            $mpdFileChanged[$streamDatavalue['id']]['path'] = 'http://'.$bucketName.".".$s3Url."/".$folderPathS3.$streamDatavalue['id'].'/stream.mpd';
                            $folderPath = $_SERVER['DOCUMENT_ROOT'] . "/progress/";
                            if($s3->doesObjectExist( $bucketName, $folderPathS3.$streamDatavalue['id'].'/stream.mpd')){
                                $result = $s3->getObject(array(
                                    'Bucket' => $bucketName,
                                    'Key'    => $folderPathS3.$streamDatavalue['id'].'/stream.mpd',
                                    'SaveAs' => $folderPath . 'stream.mpd'
                                ));
                                if(file_exists($folderPath . 'stream.mpd')){
                                    $file = $folderPath . "changeMpd.sh";
                                    if(file_exists($folderPath . 'stream.mpd')){
                                        $handle = fopen($file, 'w') or die('Cannot open file:  ' . $file);
                                        $moveFile = "";
                                        $moveFile .= "sed -i '/height=\"144\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"100000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"240\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"200000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"360\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"300000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"640\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"450000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"720\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"550000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"1080\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"650000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"1440\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"750000\"/' ".$folderPath . "stream.mpd \n";
                                        $moveFile .= "sed -i '/height=\"2160\"/s/bandwidth=\"*[0-9]*\"/bandwidth=\"1000000\"/' ".$folderPath . "stream.mpd \n";
                                        fwrite($handle, $moveFile);
                                        fclose($handle);
                                    }
                                    echo shell_exec('sh '.$file);
                                    $acl = 'public-read';
                                    $result_ = $s3->putObject(array(
                                        'Bucket' => $bucketName,
                                        'Key' => $folderPathS3.$streamDatavalue['id'].'/stream.mpd',
                                        'SourceFile' => $folderPath . 'stream.mpd',
                                        'ACL' => $acl,
                                        'ContentType'  => 'application/dash+xml',
                                    ));
                                    $mpdFileChanged[$streamDatavalue['id']]['Video'] = 'Yes';
                                    unlink($folderPath . "stream.mpd");
                                } else{
                                    echo "MPD file not exist";
                                }
                            } else{
                                echo "MPD file not exist";
                            }
                        }
                    }
                    echo "<pre>";
                    print_r($mpdFileChanged);
                    unlink($file);
                }
            }
        } else{
            echo "Please provide studio_id";
        }
    }
    
    function actionvideoLibraryImagggg(){
        $this->layout = false;
        //print_r($v);
        
        $studio_id = $_REQUEST['studio_id'];
        $idNotIn = @$_REQUEST['idNotIn'];
        $condition = '';
        if($idNotIn != ''){
            $condition =' and id not in('.$idNotIn.')';
        }
        $studio = Studio::model()->getStudioBucketInfo($studio_id);
        $newUser = $studio['new_cdn_users'];
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath($newUser,$studio_id);
        $ffmpeg = FFMPEG_PATH;

        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";
        
        $query = "select * from video_management where (thumb_image_name ='' or thumb_image_name is NULL) and flag_uploaded=0 and studio_id=".$studio_id.$condition;
        $videoGallery = Yii::app()->db->createCommand($query)->queryall();
        echo "<pre>";
        if($videoGallery){
            foreach ($videoGallery as $videoGallerykey => $videoGalleryvalue) {
                $video_name = str_replace(' ', '%20', $videoGalleryvalue['video_name']);
                $s3viewdir = 'http://'.$bucket.'.'.$s3url.'/' .$unsignedFolderPathForVideo. 'videogallery/' . $video_name;
                $dir2 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/videogallery';
                if (!is_dir($dir2)) {
                    mkdir($dir2);
                    @chmod($dir2, 0777);
                }

                $dir1 = $dir2 . '/' . $studio_id;
                if (!is_dir($dir1)) {
                    mkdir($dir1);
                    @chmod($dir1, 0777);
                }
                $dir = $dir1 . '/images';

                if (!is_dir($dir)) {
                    mkdir($dir);
                    @chmod($dir, 0777);
                }
                $path_parts = pathinfo($video_name);
                $imgname = $path_parts['filename'] . '.jpg';
                $img_path = $dir . '/' . $imgname;
                $as_height = 160;
                $as_width  = 280;
                $size = $as_width.'x'.$as_height;
                $scale = "scale=".$as_width.":".$as_height;
                $second = 1;
                if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
                    $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
                } else {
                    $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
                }
                exec($cmd, $output12, $return);
                $dest_file = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $imgname;
                $videoGallery[$videoGallerykey]['fullVideoPath'] = $s3viewdir;
                $videoGallery[$videoGallerykey]['imageFolderPathhh'] = $img_path;
                $videoGallery[$videoGallerykey]['imageCodeCommand'] = $cmd;
                if(file_exists($img_path)){
                    $acl = 'public-read';
                    $result = $s3->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $dest_file,
                        'SourceFile' => $img_path,
                        'ACL' => $acl,
                    ));
                    unlink($img_path);
                    $video_details = VideoManagement::model()->findbyPk($videoGalleryvalue['id']);
                    $video_details->thumb_image_name = $imgname;
                    $video_details->save();
                    $videoGallery[$videoGallerykey]['imageGot'] = 'Yes';
                }
            }
            print_r($videoGallery);
        }
    }
    
    public function actiongetEncodingServerData(){
        $folderName = '';
        if($_REQUEST['encoding_server_ip'] !='' && $_REQUEST['field_name'] !=''){
            $serverData = Yii::app()->db->createCommand()
                ->select($_REQUEST['field_name'])
                ->from('encoding_server_details')
                ->where($_REQUEST['field_name'].'!= "" and ip ="'.$_REQUEST['encoding_server_ip'].'"')
                ->queryRow();
            if(@$serverData[$_REQUEST['field_name']] != ''){
                $folderName = $serverData[$_REQUEST['field_name']];
}
        }
        echo $folderName;
    }
    
    public function actionstopLiveStreaming() {
        if(@$_REQUEST['movie_id'] != ''){
           $liveStreamData = Livestream::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id']));
            if(@$liveStreamData->feed_url != '' && @$liveStreamData->stream_url != '' && @$liveStreamData->stream_key != ''){
                $streamName = explode("?",@$liveStreamData->stream_key);
                if(@$streamName[0] != ''){
                    $liveStreamData->start_streaming = 0;
                    $liveStreamData->save();
                    if(@$_REQUEST['leadStop'] == 1){
                        $liveStreamLog = LivestreamCameraLog::model()->getDataByStudioId($liveStreamData->studio_id);
                        if(empty($liveStreamLog)){
                            $cameralog['studio_id'] = $liveStreamData->studio_id;
                            $cameralog['streaming_time_expired'] = 1;
            				$livestreamcameralogId = LivestreamCameraLog::model()->insertLogData($cameralog);
                        } else{
                            $liveStreamLog->streaming_time_expired = 1;
                            $liveStreamLog->save();
                        }
                        exit;
                    }
                    if($liveStreamData->is_recording == 1){
                        $studio_id = $liveStreamData->studio_id;
                        $streamName = $streamName[0];
                        $liveStreamVideoSync = new LivestreamVideoSync();
                        $liveStreamVideoSync->studio_id = $studio_id;
                        $liveStreamVideoSync->movie_id = $liveStreamData->movie_id;
                        $liveStreamVideoSync->stream_name = $streamName;
                        $liveStreamVideoSync->save();
                        $liveStreamVideoSyncId = $liveStreamVideoSync->id;


                        //Get recoded live stream file from s3
                        $s3 = S3Client::factory(array(
                            'key' => Yii::app()->params->s3_key,
                            'secret' => Yii::app()->params->s3_secret
                        ));
                        $videoOnlyName = $streamName;
                        $videoExt = 'mp4';
                        $video_name = $videoOnlyName.'.'.$videoExt;
                        $bucket = 'livestream-data';
                        $s3url = 's3.amazonaws.com';
                        $folderpath = '';
                        $streamUrlExplode  = explode("/",str_replace("rtmp://","",$liveStreamData->stream_url));
                        $nginxServerDetails = Yii::app()->aws->getNginxServerDetails(@$streamUrlExplode[0]); 
                        if(@$nginxServerDetails['recording_folder_path'] != ''){
                            $folderpath  = $nginxServerDetails['recording_folder_path'].$video_name;
                        }
                        $movie_size = 0;
                        if($folderpath){
                            while($movie_size <= 0){
                                $response_movie = $s3->getListObjectsIterator(array(
                                                'Bucket' => $bucket,
                                                'Prefix' => $folderpath
                                            ));
                                foreach ($response_movie as $object_movie) {
                                    if ($object_movie['Key'] == $folderpath) {
                                        $movie_size= $object_movie['Size'];
                                    }
                                }
                            }

                            if($movie_size > 0){
                                //Add the recorded video to video library
                                $sql = "select * from video_management where studio_id=" . $studio_id . " and video_name='" . $video_name . "' and flag_deleted=0";
                                $res = Yii::app()->db->createCommand($sql)->queryAll();
                                $check_video_availability = count($res);
                                if ($check_video_availability > 0) {
                                    $video_name = $videoOnlyName . strtotime(date("d-m-Y H:i:s")) . '.' . $videoExt;
                                }
                                $data['content_type'] = 'video/mp4';
                                $data['url'] = 'http://'.$bucket.'.'.$s3url.'/'.$folderpath;
                                $data['s3videoPath'] = 's3://'.$bucket.'/'.$folderpath;
                                Yii::app()->aws->createUploadVideoSH($data, "", $video_name,3,"",$studio_id,$liveStreamVideoSyncId);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function actionupdateExistingLiveStream() {
        //Not required
//        $sql = "select l.*,f.permalink from livestream l, studios s, films f where l.movie_id=f.id and l.studio_id=s.id and s.status=1 and l.feed_url !='' and l.feed_type=2 and l.feed_method='push' and s.id !=2907";
//        $res = Yii::app()->db->createCommand($sql)->queryAll();
//        if($res){
//            foreach ($res as $key => $value) {
//                $streamUrl = $value['permalink'].'-'.strtotime(date("Y-m-d H:i:s"));
//                $movie_id = $value['movie_id'];
//                $length = 8;
//                $userName = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
//                $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
//                $enc = new bCrypt();
//                $passwordEncrypt = $enc->hash($password);
//                $ch = curl_init();
//                curl_setopt($ch, CURLOPT_URL, 'http://'.nginxserverip.'/auth.php');
//                curl_setopt($ch, CURLOPT_POST, 1);
//                curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&stream_name=".$streamUrl."&user_name=".$userName."&password=".$passwordEncrypt."&serverUrl=".Yii::app()->getBaseUrl(TRUE)."/conversion/stopLiveStreaming?movie_id=".$movie_id);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//                $server_output = trim(curl_exec($ch));
//                curl_close($ch);
//                if($server_output != 'success'){
//                    Film::model()->deleteByPk($movie_id);
//                    $res[$key]['working'] = 'No';
//                } else{
//                    $liveFeeds['feed_url'] = 'rtmp://'.nginxserverip.'/live/'.$streamUrl;
//                    $liveFeeds['stream_url'] = 'rtmp://'.nginxserverip.'/live';
//                    $liveFeeds['stream_key'] = $streamUrl."?user=".$userName."&pass=".$password;
//                    $liveFeeds['is_recording'] = 0;
//                    Livestream::model()->updateAll($liveFeeds,'id='.$value['id']);
//                    $res[$key]['working'] = 'Yes';
//                }
//            }
//            echo "<pre>";
//            print_r($res);
//        }
    }
    
    public function actionUpdateVideoManagementInfo() {
        $this->layout = false;
        $videoManagementId = $_REQUEST['video_management_id'];
        $video_details = VideoManagement::model()->findbyPk($videoManagementId);
        if($video_details){
            $studio_id = $video_details->studio_id;
            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath('',$studio_id);
            $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
            $s3dir = $unsignedFolderPathForVideo . "videogallery/videogallery-image/".$videoManagementId."/";
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $response = $s3->getListObjectsIterator(array(
                'Bucket' => $bucket,
                'Prefix' => $s3dir
            ));
            $thumbImages = array();
            foreach ($response as $object) {
                $filesNameInfolder = str_replace($s3dir,"",$object['Key']);
                if($filesNameInfolder != ''){
                    $fileInfo = new SplFileInfo($filesNameInfolder);
                    $fileExt = $fileInfo->getExtension();
                    if($fileExt == 'png'){
                        $thumbImages[] = $filesNameInfolder;
                    }
                }	
            }
            $thumbImages = implode(",",$thumbImages);
            if($thumbImages){
                $video_details->thumbnail_images = $thumbImages;
            }
            $video_details->thumbnail_status=0;
            $video_details->save();
        }
    }
    
    //Author        : Arvind (aravind@muvi.com) || functionality : actionUploadUrlAudioNotDownloaded
    function actionUploadUrlAudioNotDownloaded(){
        $this->layout = false;
        $rec_id=$_REQUEST['rec_id'];
        $is_uploaded=2;
        $audio_gallery_details = AudioManagement::model()->findbyPk($rec_id);
        if($audio_gallery_details){
            Yii::app()->db->createCommand()->delete('video_sync_temp','video_management_id =:video_management_id AND studio_id =:studio_id', array(':video_management_id' => $rec_id, ':studio_id' => $studio_id));
        }
        $qry = "UPDATE audio_gallery set is_uploaded=".$is_uploaded." WHERE  id =".$rec_id ;
        Yii::app()->db->createCommand($qry)->execute();
    }

    // Author : Arvind (aravind@muvi.com) || functionality : actionUpdateAudioMovieInfo
    function actionUpdateAudioMovieInfo(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        //print_r($v);exit;
        $lastKey = end(array_keys(($v)));
        $differentResolutions = '';
        $file_count = 0;
        foreach ($v as $key => $value) {
        if(($key !=0 && $key !=1 ) && ($key !=2 && $key !=3 && $key!=5)){
                $file_count++;
                if($lastKey == $key){
                    $differentResolutions .= $value;
                }else{
                    $differentResolutions .= $value.',';
                }
            }
        }
        $dateTime = date('Y-m-d H:i:s');
        $embed_id = md5($v[0]);
        $type = 'movie_audio';
        $ms = movieStreams::model()->findByPk($v[0]);
        $studio_id = $ms->studio_id;
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $audioNameToBeUpdated = $ms->full_movie;
        $paraDetails['file_path'] = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$v[0].'/';
        $paraDetails['file_name'] = $audioNameToBeUpdated;
        $paraDetails['studio_id'] = $studio_id; 
        $audioUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$v[0].'/'.$audioNameToBeUpdated;
        $_fullName = $audioUrl;
        $res = Yii::app()->common->getAudioStorageSize($v[0],$studio_id,$type);
        $res_size = json_encode($res['res_size']);
        $audioNameToBeUpdated = $v[2].".mp3";
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.after_encoding=1,ms.is_converted=1,ms.embed_id ='".$embed_id."',ms.encoding_end_time='".$dateTime."',full_movie='".$audioNameToBeUpdated."',video_resolution='".$differentResolutions."' WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();
        $encodingData = Encoding::model()->findByAttributes(array('movie_stream_id'=>$v[0],'studio_id' => $studio_id));
        if($encodingData){
             $encodingLog = new EncodingLog;
             $encodingLog->studio_id= $encodingData->studio_id;
             $encodingLog->movie_stream_id= $encodingData->movie_stream_id;
             $encodingLog->size= $encodingData->size;
             $encodingLog->upload_time= $encodingData->upload_time;
             $encodingLog->encoding_server_id= $encodingData->encoding_server_id;
             $encodingLog->encoding_start_time= $encodingData->encoding_start_time;
             $encodingLog->expected_end_time= $encodingData->expected_end_time;
             $encodingLog->encoding_end_time = date('Y-m-d H:i:s');
             $encodingLog->status = "Completed";
             $encodingLog->save();
             $encodingData->delete();
        }
        $data = array(
            'studio_id' => $studio_id,
            'original_video_file_count' => $res['original_file_count'],
            'original_file_size' => $res['original_file_size'],
            'converted_video_file_count' => $res['converted_file_count'],
            'converted_file_size' => $res['converted_file_size'],
            'total_file_size' => $res['total_file_size'],
            'file_type' => $type,
            'last_updated_date' => $dateTime
        );
        Yii::app()->aws->addStorageLog($data);
        $audioUploadedFromVideoGalley = array();
        if($v[3] != 0){
            $audioUploadedFromVideoGalley = movieStreams::model()->findByAttributes(array('id'=>$v[0],'video_management_id' => $v[3]));
        }
        if(empty($audioUploadedFromVideoGalley)){
            //Add audio to audio gallery
            if($ms->encryption_key != '' && $ms->content_key != ''){
                $fileName = $ms->full_movie;
                $fullPath = $audioUrl;
            } else{
                $fileName = $v[2].'.mp3';
                $fullPath = $audioUrl;
            }
            $audioGalleryId = Yii::app()->general->addaudioToAudioGallery($paraDetails);
            $movieStreams = movieStreams::model()->findByPk($v[0]);
            $movieStreams->video_management_id = $audioGalleryId;
            $movieStreams->save();
       }
   } 
    //Author : Arvind (aravind@muvi.com) || functionality : actionUpdateAudioUrlUploadInfo
    function actionUpdateAudioUrlUploadInfo() {
        $this->layout = false;
        $request_res = explode('MUVIMUVI', $_REQUEST['gallery_id']);
        $gallery_id = $request_res[0];
        $audio_name = $request_res[1];
        $flag_uploaded = 0;
        $audio_details = AudioManagement::model()->findByPk($gallery_id);
        $studio_id = $audio_details['studio_id'];
        $audio_details->flag_uploaded = 0;
        $audio_details->audio_name = $audio_name;
        $audio_details->save();
        $studio = Studio::model()->getStudioBucketInfo($studio_id);
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $folderPath = Yii::app()->common->getFolderPath($new_cdn_user, $studio_id);
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "audiogallery/";
        //Add other audio properties, like as {Thumbnail Image,Creation,etc.}...pending #@vi aravind@muvi.com
        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
        if ($new_cdn_user) {
            $galleryAudioUrl = $unsignedBucketPathForViewVideo . 'audiogallery/' . $audio_name;
        } else {
            $galleryAudioUrl = $unsignedBucketPathForViewVideo . 'audiogallery/' . $studio_id . '/' . $audio_name;
        }
        $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryAudioUrl;
        $extsn = explode('.', $audio_name);
        $ext = $extsn[1];
        //Generate audio properties
        $ffmpeg = FFMPEG_PATH;
        $path_parts = pathinfo($audio_name);
        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $command = $ffmpeg . ' -i ' . $s3viewdir . '2>&1';
        } else {
            $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir . ' 2>&1';
        }
        $output = shell_exec($command);
        if($output){
        $currentAudioBitRate = Yii::app()->general->getAudioBitRates($galleryAudioUrl);
        $currentAudioBitRate = explode(" ", $currentAudioBitRate);
        if ($currentAudioBitRate) {
            $audio_details->thumb_image_name = null;
            $audio_details->video_properties = $currentAudioBitRate;
            $audio_details->save();
        }}
    }
	/**FOR FILE upload purpose***/
	function actionUpdateUrlUploadFileInfo(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['video_name']);
        //print_r($v);
        
        $rec_id=$v[0];
        $video_name=$v[1];
        if(@$v[2] != '' && @$v[2] != 0){
            LivestreamVideoSync::model()->deleteByPk($v[2]);
        }
        $flag_uploaded=0;
        $video_details = FileManagement::model()->findbyPk($rec_id);
        $studio_id=$video_details['studio_id'];
        $video_details->video_name = $video_name;
        $video_details->save();
        if($video_details){
            Yii::app()->db->createCommand()->delete('video_sync_temp','video_management_id =:video_management_id AND studio_id =:studio_id', array(':video_management_id' => $rec_id, ':studio_id' => $studio_id));
        }
        $studio = Studio::model()->getStudioBucketInfo($studio_id);
        $newUser = $studio['new_cdn_users'];
       
        //add the video properties, thumb image creation
        
        $s3 = Yii::app()->common->connectToAwsS3($studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucket = $bucketInfo['bucket_name'];

        $folderPath = Yii::app()->common->getFolderPath($newUser,$studio_id);

        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        $unsignedFolderPathForVideo = $folderPath['unsignedFolderPathForVideo'];
        $unsignedBucketPathForView = $bucketInfo['unsignedFolderPath'];
        $unsignedBucketPathForViewVideo = $bucketInfo['unsignedFolderPathForVideo'];
        $s3dir = $unsignedFolderPathForVideo . "videogallery/";

        $dt = new DateTime();
        $upload_end_time = $dt->format('Y-m-d H:i:s');
        $bucketCloudfrontUrl = 'http://' . $bucketInfo['videoRemoteUrl'];
        
        if ($newUser) {
            $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $video_name;
        } else {
            $galleryVideoUrl = $unsignedBucketPathForViewVideo . 'videogallery/' . $studio_id . '/' . $video_name;
        }
        $s3viewdir = $bucketCloudfrontUrl . '/' . $galleryVideoUrl;
        $extsn=explode('.',$video_name);
        $ext=$extsn[1];
        //Generate video thumbnail
        $ffmpeg = FFMPEG_PATH;

        $path_parts = pathinfo($video_name);
        //change thumb image name as per ther video file
        $imgname = $path_parts['filename'] . '.jpg';
        $dir2 = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/videogallery';
        if (!is_dir($dir2)) {
            mkdir($dir2);
            @chmod($dir2, 0777);
        }

        $dir1 = $dir2 . '/' . $studio_id;
        if (!is_dir($dir1)) {
            mkdir($dir1);
            @chmod($dir1, 0777);
        }
        $dir = $dir1 . '/images';

        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        $img_path = $dir . '/' . $imgname;
        //echo $img_path;
        $thumbImageFolder = $dir . '/thumbImage/';
        if (!is_dir($thumbImageFolder)) {
            mkdir($thumbImageFolder);
            @chmod($thumbImageFolder, 0777);
        }
        $second = 1;

        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $command = $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
        } else {
            $command = 'sudo ' . $ffmpeg . ' -i ' . $s3viewdir .' -vstats 2>&1';
        }
        $output = shell_exec($command);
      
        $video_prop=Yii::app()->common->get_videoproperties($output,$ext);
        $resolution=$video_prop['resolution'];
        $ex_resolution=explode('x',$resolution);
        $width=$ex_resolution[0];
        $height=$ex_resolution[1];
        $maxWidth=280;
        $maxHeight=160;
       
        if ($maxWidth > $height) {
            $as_height = 160;
            $as_width  = 280;
        } else {
            $as_width  = floor(($width/$height)*$maxHeight);
            $as_height = $maxHeight;
        }       
        $size = $as_width.'x'.$as_height;
        $scale = "scale=".$as_width.":".$as_height;
//        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
//            $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf $scale $img_path -an -vcodec mjpeg 2>&1";
//        } else {
//            $cmd = "sudo $ffmpeg -ss $second -i $s3viewdir -frames 1 -q:v 1 -vf $scale $img_path -an -vcodec mjpeg 2>&1";
//        }
        $second = 1;
        
        // get the duration and a random place within that
        $cmd = "$ffmpeg -i $s3viewdir 2>&1";
        if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
            $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
            $second = rand(1, ($total - 1));
            $countFrame = Yii::app()->general->ImageDuration($total);
        }
        if (HOST_IP == '127.0.0.1' || HOST_IP == '52.0.64.95') {
            $cmd = "$ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";
        } else {
            $cmd = "sudo $ffmpeg -ss $second -i  $s3viewdir -vf scale=560:312 $img_path -an -vcodec mjpeg 2>&1";

        }
        
        exec($cmd, $output12, $return);
        $dataForFunction = array();
        $dataForFunction['studio_id'] = $studio_id;
        $dataForFunction['video_management_id'] = $video_details->id;
        $dataForFunction['video_url'] = $s3viewdir;
        $dataForFunction['countFrame'] = @$countFrame;
        $generateThumbSlider = Yii::app()->general->videoGalleryThumbSlider($dataForFunction);
        $head = array_change_key_case(get_headers($s3viewdir, TRUE));
      //  echo "<pre>";
       // print_r($head);
        $filesize1 = $head['content-length'];
        $filesize= Yii::app()->common->bytes2English($filesize1);
        $video_prop['FileSize']=$filesize;
        //$video_prop = array('FileSize' =>"Filesize(MB) :".$filesize , 'Duration' => $output[20], 'Audio' => $output[25], 'Video' => $output[21]);
        $video_properties = json_encode($video_prop);
        $src_path = $img_path;
        //$dest_file = $unsignedFolderPathForVideo.'videogallery/videogallery-image/'.$studio_id.'/'.$imgname;
        if ($newUser) {
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/videogallery-image/' . $imgname;
        } else {
            $dest_file = $unsignedFolderPathForVideo . 'videogallery/' . $studio_id . '/videogallery-image/' . $imgname;
        }
        $acl = 'public-read';
        $result = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => $dest_file,
            'SourceFile' => $src_path,
            'ACL' => $acl,
        ));
        unlink($src_path);
        $video_details->thumb_image_name = $imgname;
        $video_details->video_properties = $video_properties;
        $video_details->duration = @$video_prop['duration'];
        if($generateThumbSlider == 1){
            $video_details-> thumbnail_status =1;  
        }
        $video_details->save();
        
    }
	function actionUploadUrlVideoNotDownloadedFIle(){
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['video_name']);
        $rec_id=$v[0];
        if(@$v[1] != '' && @$v[1] != 0){
            LivestreamVideoSync::model()->deleteByPk($v[1]);
        }
        
        $flag_uploaded=2;
        //$studio_id = Yii::app()->user->studio_id;
        $video_details = VideoManagement::model()->findbyPk($rec_id);
        if($video_details){
            Yii::app()->db->createCommand()->delete('video_sync_temp','video_management_id =:video_management_id AND studio_id =:studio_id', array(':video_management_id' => $rec_id, ':studio_id' => $studio_id));
        }
        $qry = "UPDATE video_management set flag_uploaded=".$flag_uploaded." WHERE  id =".$rec_id ;
        Yii::app()->db->createCommand($qry)->execute();
        
    }
    
    public function actionUpdateMovieInfoMobileNonDRM(){
        
        $this->layout = false;
        $v = explode('MUVIMUVI',$_REQUEST['stream_id']);
        print_r($v);
		$lastKey = end(array_keys(($v)));
		$differentResolutions = '';
                $file_count = 0;
		foreach ($v as $key => $value) {
			if($key !=0 && $key !=1  && $key !=2){
                            $file_count++;
                            if($lastKey == $key){
                                $differentResolutions .= $value;
                            }else{
                                $differentResolutions .= $value.',';
                            }
			}
		}
        $dateTime = date('Y-m-d H:i:s');
        
        $type = 'movie';
        $ms = movieStreams::model()->findByPk($v[0]);
        $studio_id = $ms->studio_id;
        
        
        $bucketInfo = Yii::app()->common->getBucketInfo("",$studio_id);
        $bucketName = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $videoNameToBeUpdated = $v[2].".mp4";
        $videoUrl = 'http://'.$bucketName.'.'.$s3url.'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$v[0].'/'.$v[2].'.mp4';
        $duration = $this->getVideoDuration($videoUrl);
        
        $qry = "UPDATE movie_streams ms JOIN films f SET is_mobile_drm_disable=1, full_movie='".$videoNameToBeUpdated."',video_resolution='".$differentResolutions."',ms.video_duration = ".$duration." WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();
        $res = Yii::app()->common->getStorageSize($v[0],$studio_id,$type);
        $res_size = json_encode($res['res_size']);
        $qry = "UPDATE movie_streams ms JOIN films f SET ms.original_file='".$res['original_file_count']."-".$res['original_file_size']."',ms.converted_file='".$res['converted_file_count']."-".$res['converted_file_size']."',ms.resolution_size = '".$res_size."' WHERE  f.id = ms.movie_id AND ms.id=".$v[0]." AND f.uniq_id='".$v[1]."'";
        Yii::app()->db->createCommand($qry)->execute();

        $data = array(
            'studio_id' => $studio_id,
            'original_video_file_count' => $res['original_file_count'],
            'original_file_size' => $res['original_file_size'],
            'converted_video_file_count' => $res['converted_file_count'],
            'converted_file_size' => $res['converted_file_size'],
            'total_file_size' => $res['total_file_size'],
            'file_type' => $type,
            'last_updated_date' => $dateTime
        );
        Yii::app()->aws->addStorageLog($data);
    }
}