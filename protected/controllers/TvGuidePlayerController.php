<?php
require 's3bucket/aws-autoloader.php';
use Aws\CloudFront\CloudFrontClient;
class TvGuidePlayerController extends Controller {
	protected function beforeAction($action) {
        parent::beforeAction($action);
        return true;
    }    
	
    public function actionPlayMovie() {
        Yii::app()->theme = 'bootstrap';
        //$permalink = 'avengers-age-of-ultron';
        if (isset($_REQUEST['contentPlink']) && $_REQUEST['contentPlink']) {
            $permaLink = $_REQUEST['contentPlink'];
            //$permalink = 'avengers-age-of-ultron';
        } else {
            Yii::app()->user->setFlash('error', 'Oops! You are trying to play an invalid content');
            $this->redirect(Yii::app()->getBaseUrl(TRUE));
            exit;
        }
        $contentTypePermalink = @$_REQUEST['contentType'];
        //$studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studios();
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = "";
        if(Yii::app()->user->id){
            $user_id = Yii::app()->user->id;
        }
        
        if ($permaLink) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.permalink=:permalink AND f.studio_id=:studio_id', array(':permalink' => $permaLink, ':studio_id' => $studio_id));
            $movie = $command->queryAll();
            if ($movie) {
                $movie_id = $movie[0]['id'];
                if(!Yii::app()->common->isGeoBlockContent(@$movie_id)){
                    Yii::app()->user->setFlash('error', 'This content is not available to stream in your country');
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
                }
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You are trying to play an invalid content');
                $this->redirect(Yii::app()->getBaseUrl(TRUE));exit;
            }
        }
        if ($movie[0]['content_types_id'] == 4) {
            $liveStream = Livestream::model()->find('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie[0]['id'], ':studio_id' => $studio_id));
            if (!$liveStream || !$liveStream->feed_url) {
                Yii::app()->user->setFlash('error', 'No Live Feed is available for this content');
                $this->redirect(Yii::app()->getBaseUrl(true) . '/' . $movie[0]['permalink']);exit;
            }
        } else {
            $stream_code = isset($_REQUEST['stream']) ? $_REQUEST['stream'] : '0';
            $season = isset($_REQUEST['season']) ? $_REQUEST['season'] : 0;

            $stream_id = 0;
            $purchase_type = '';
            if (trim($permaLink) != '0' && trim($stream_code) != '0') {
                //Get stream Id
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code);
                $purchase_type = 'episode';
            } else if (trim($permaLink) != '0' && trim($stream_code) == '0' && intval($season)) {
                $purchase_type = 'season';
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id);
            }

            if (!trim($stream_id) || $stream_id <= 1) {
                Yii::app()->user->setFlash('error', 'Oops! Sorry video for this content is not available!');
                $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/' . $movie->permalink);exit;
            }
        }
            $this->layout = false;
            if ($movie[0]['content_types_id'] == 4) {
                if ($liveStream->feed_type == 2) {
                    $this->renderPartial('//livestream/live_rtmpstreaming', array('livestream' => $liveStream));
                } else {
                    $this->renderPartial('//livestream/livefeed_streaming', array('livestream' => $liveStream));
                }
            } else {
                $fullmovie_path = $this->getFullVideoPath($stream_id);
                if (!$fullmovie_path) {
                    Yii::app()->user->setFlash('error', 'Oops! Sorry video for this content is not available!');
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));
                    exit;
                }
                $movie_stream = new movieStreams();
                $movie_strm = $movie_stream->findByPk($stream_id);
                $v_logo = Yii::app()->general->getPlayerLogo($studio_id);
				$res_size = json_decode($movie_strm->resolution_size,true);
                $video_duration = explode(':',$movie_strm->video_duration);
                $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
                $multipleVideo = array();
                $defaultResolution = 144;
                $showSetting =0;
                if($movie_strm->video_resolution){
                    $showSetting =1;
                }
                $multipleVideo = $this->getvideoResolution($movie_strm->video_resolution, $fullmovie_path);
                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path);
                $videoToBeplayed12 = explode(",", $videoToBeplayed);
                $fullmovie_path = $videoToBeplayed12[0];
                $defaultResolution = $videoToBeplayed12[1];
                $wiki_data = $movie_strm->wiki_data;
                $movie_id = $movie_strm->movie_id;
                $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                $internetSpeedImage = CDN_HTTP . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/check-download-speed.jpg';
                if(Yii::app()->common->isMobile()){
                    $videoWithExpTime = array();
                    $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                    foreach ($multipleVideo as $multipleVideokey => $multipleVideovalue) {
                        $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $multipleVideovalue));
                        $videoWithExpTime[$multipleVideokey] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl); 
                    }
                    $multipleVideo = array();
                    $multipleVideo = $videoWithExpTime;
                }else if(HOST_IP != '127.0.0.1'){
                    $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                    $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                    $ua = $this->getBrowser();
                    $yourbrowser = $ua['name'] ;
                    $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,'',$yourbrowser);
                }
                $subtitleFiles = array();
                if(($movie_strm->video_management_id != '') && ($movie_strm->video_management_id != 0)){
                    $subTitlesql = "select vs.id as subId, vs.filename,ls.* from video_subtitle vs,language_subtitle ls where vs.language_subtitle_id = ls.id and vs.video_gallery_id=" . $movie_strm->video_management_id ." and vs.studio_id = ".$studio_id." ORDER BY ( ls.name != 'English' ), ls.name ASC";
                   
                    $subTitleData = Yii::app()->db->createCommand($subTitlesql)->queryAll();
                    if($subTitleData){
                        $i =1 ;
                        foreach ($subTitleData as $subTitleDatakey => $subTitleDatavalue) {
                            $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                            $filePth = $clfntUrl."/".$bucketInfo['signedFolderPath']."subtitle/".$subTitleDatavalue['subId']."/".$subTitleDatavalue['filename'];
                            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                            $subtitleFiles[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl,1,"",60000);
                            $subtitleFiles[$i]['code'] =  $subTitleDatavalue['code'];
                            $subtitleFiles[$i]['language'] =  $subTitleDatavalue['name'];
                            $i ++;
                        }
                    }
                }
                $item_poster = $this->getPoster($movie_id, 'films', 'standard');
                $content_type = @$data[0]['content_type'];
                $per_link = @$data[0]['permalink'];

                //Adding the codes for the adservers RKS
                $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = '.$studio_id));
                $page_title = $movie[0]['name'];
                //$contenttype = Yii::app()->common->getContentTypeFromStudioContent($movie[0]['content_type_id']);
                $contenttype = $movie[0]['content_types_id'];

                if ($contenttype == 3) {
                    $page_title.= ' Season ' . $movie_strm->series_number;
                    if ($movie_strm->episode_title != '')
                        $page_title.= ' ' . $movie_strm->episode_title;
                    else
                        $page_title.= ' Episode ' . $movie_strm->episode_number;
                }
                $page_desc = 'You are watching ' . $page_title . ' at ' . $studio->name . '.';
                
                $ad_timing = array();
                $roll_after = str_replace('00:00:00,', '', $movie_strm->roll_after);
                $roll_after = str_replace('00:00:00', '', $roll_after);
                $roll_afters = explode(',', $roll_after);   
                foreach ($roll_afters as $roll) {
                    $roll_parts = explode(':', $roll);
                    $roll_after = 0;
                    if ($roll_parts[0] > 0)
                        $roll_after += 3600 * $roll_parts[0];
                    if ($roll_parts[1] > 0)
                        $roll_after += 60 * $roll_parts[1];
                    if ($roll_parts[2] > 0)
                        $roll_after += $roll_parts[2];   
                    $ad_timing[] = $roll_after;
                } 
                $timeDifference = 0;
                if(@$_REQUEST['start_time'] != ''){
                    $timeDifference = (strtotime(gmdate("Y-m-d H:i:s")) - strtotime($_REQUEST['start_time']));
                    if($timeDifference < 60){
                        $timeDifference = 0;
                    }
                }
                if(@$_REQUEST['old_value'] ==1){
                  $old_value=1; 
                }
                else
                {
                    $old_value=2; 
                }
               
                if(count($ad_timing) > 0)
                    $ad_type = 'vast';   
                $this->render('//video/tv_guide_play_video', array('timeDifference'=>$timeDifference,'duration'=>$duration,'res_size'=>$res_size,'showSetting' => $showSetting,'subtitleFiles' => $subtitleFiles,'internetSpeedImage' => @$internetSpeedImage, 'defaultResolution' => $defaultResolution, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => 'allowed', 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'content_type' => $content_type, 'episode_number' => $episode_number, 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio_ads' => $studio_ads, 'mvstream' => $movie_strm, 'play_type' => 'movie', 'movie_id' => $movie_id, 'stream_id' => $stream_id, 'video_limit' => $studio->limit_video, 'page_title' => $page_title, 'page_desc' => $page_desc,'old_value'=>$old_value));
            }
    }
    
    function getBrowser() 
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
        { 
            $bname = 'Internet Explorer'; 
            $ub = "MSIE"; 
        } 
        elseif(preg_match('/Firefox/i',$u_agent)) 
        { 
            $bname = 'Firefox'; 
            $ub = "Firefox"; 
        }
        elseif(preg_match('/OPR/i',$u_agent)) 
        { 
            $bname = 'Opera'; 
            $ub = "Opera"; 
        } 
        elseif(preg_match('/Chrome/i',$u_agent)) 
        { 
            $bname = 'Chrome'; 
            $ub = "Chrome"; 
        } 
        elseif(preg_match('/Safari/i',$u_agent)) 
        { 
            $bname = 'Safari'; 
            $ub = "Safari"; 
        } 
        elseif(preg_match('/Netscape/i',$u_agent)) 
        { 
            $bname = 'Netscape'; 
            $ub = "Netscape"; 
        } 

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
            'userAgent' => $u_agent,
            //'name'      => $bname,
            'name'      => $ub,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }
}


