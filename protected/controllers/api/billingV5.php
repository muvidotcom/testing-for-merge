<?php

trait Billing {

	/**
	 * @method public authenticateCard: It authenticate the credit card
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param string card_name*: Name as on Credit card
	 * @param String card_number*: Card Number 
	 * @param String cvv*: CVV/CVC code of credit card
	 * @param String expiry_month*: Expiry month as on Card
	 * @param String expiry_year*: Expiry Year as on Card
	 * @param String email: email of user, Mandatory field for stripe gateway but for other gateway, it is optional
	 * @author Sunil<sunil@muvi.com>
	 */
	public function actionAuthenticateCard() {
		$data = array();
		$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

		if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
			$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
			$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);

			if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
				$_REQUEST['exp_month'] = ltrim((string) @$_REQUEST['expiry_month'], '0');
				$_REQUEST['exp_year'] = @$_REQUEST['expiry_year'];

				if ($_REQUEST['card_name'] && $_REQUEST['card_number'] && $_REQUEST['exp_month'] && $_REQUEST['exp_year'] && $_REQUEST['cvv']) {
					$_REQUEST['isAPI'] = 1; //Only for stripe

					$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$res = $payment_gateway::processCard($_REQUEST);

					$data = json_decode($res, true);
					if (isset($data['isSuccess']) && intval($data['isSuccess'])) {
						$this->code = 200;
					} else {
						$this->code = 622;
					}
				} else {
					$this->code = 660;
				}
			} else {
				$this->code = 612;
			}
		} else {
			$this->code = 612;
		}

		$this->items = $data;
	}

	/**
	 * @method public getSuscriptionPlans: It will check if plan exists for a studio. If exists then return the plan lists
	 * @return json Return the json array of results
	 * @param String authToken* Auth token of the studio
	 * @param String movie_id: Movie uniq code. This will requred for subscription bundle
	 * @param String country: country code
	 * @param String lang_code: language code
	 * @author Sunil<sunil@muvi.com>
	 */
	public function actionGetSuscriptionPlans() {
		$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
		$language_id = Yii::app()->custom->getLanguage_id(@$_REQUEST['lang_code']);
		$data = array();
		$plan = array();
		$bundleplans = array();
		$all_plans = array();
		$default_plan_id = '';
		
		$isgateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
        if (isset($isgateway['gateways']) && !empty($isgateway['gateways'])) {
			$is_bunlde_plan = 0;
			if (isset($_REQUEST['movie_id']) && trim($_REQUEST['movie_id'])) {
				$command = Yii::app()->db->createCommand()
						->select('f.id,f.name,f.content_type_id,f.name,f.permalink,f.content_types_id')
						->from('films f ')
						->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
				$films = $command->queryRow();
				if (!empty($films)) {
					$bundleplans = Yii::app()->common->getAllSubscriptionsBundle($films['id'], $films['content_types_id'], '', '', $this->studio_id);
					if (!empty($bundleplans)) {
						foreach ($bundleplans as $key => $val) {
							if ($val->is_default) {
								$default_plan_id = $val->id;
							}
							$price_list = Yii::app()->common->getUserSubscriptionPrice($val->id, $default_currency_id, @$_REQUEST['country'], $this->studio_id);
							$bundleplans[$key] = $val->attributes;
							$bundleplans[$key]['price'] = $price_list['price'];
							(array) $currency = Currency::model()->findByPk($price_list['currency_id']);
							$bundleplans[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
						}
						$is_bunlde_plan = 1;
					}
				}
			}
			
			$ret = Yii::app()->common->isPaymentGatwayAndPlanExists($this->studio_id, $default_currency_id, @$_REQUEST['country'], $language_id, $is_bunlde_plan);
			if ($ret && !empty($ret)) {
				if (isset($ret['plans']) && !empty($ret['plans'])) {
					$plans = $ret['plans'];
					$default_plan_id = @$plans[0]->id;

					foreach ($plans AS $key => $val) {
						if ($val->is_default) {
							$default_plan_id = $val->id;
						}

						$plan[$key] = $val->attributes;
						$plan[$key]['price'] = $val->price;

						(array) $currency = Currency::model()->findByPk($val->currency_id);
						$plan[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
					}
				}
			}
			
			$all_plans = array_merge($plan, $bundleplans);
			if (!empty($all_plans)) {
				$this->code = 200;
				$data['plans'] = $all_plans;
				$data['default_plan'] = $default_plan_id;
			} else {
				$this->code = 648;
			}
		} else {
			$this->code = 612;
		}
		$this->items = $data;
	}

	/**
	 * @method public purchaseSubscription: Payment process for 2-step registration
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param String email*: Email address of the User
	 * @param String plan_id*: subscription plan id
	 * @param string card_name: Name as on Credit card
	 * @param String card_number: Card Number 
	 * @param String cvv: CVV as on card
	 * @param String expiry_month: Expiry month as on Card
	 * @param String expiry_year: Expiry Year as on Card
	 * @param String card_last_fourdigit: Last 4 digits of the card 
	 * @param String auth_num: Authentication Number
	 * @param String token: Mandatory Payment Token
	 * @param String card_type*: Type of the card
	 * @param String reference_no: Reference Number if any
	 * @param String response_text: Response text as returned by Payment gateways
	 * @param String status: Status of the payment gateways
	 * @param String profile_id*: User Profile id created in the payment gateways
	 * @author Sunil<sunil@muvi.com>
	 */
	function actionPurchaseSubscription() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$users = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status' => 1));
			if (isset($users) && !empty($users)) {
				$_REQUEST['data'] = $_REQUEST;
				$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				$gateway_code = '';
				if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
					$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
					$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
				}

				$_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
				$_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
				$_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];

				if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$studio = Studios::model()->findByPk($this->studio_id);
						$default_currency_id = $studio->default_currency_id;
						$plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'], $this->studio_id, $default_currency_id, $_REQUEST['country']);
						if (isset($plan_details) && !empty($plan_details)) {
							$is_register_success = 1;
							if (isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
								$price = $plan_details['price'];
								$currency_id = $plan_details['currency_id'];
								$currency = Currency::model()->findByPk($currency_id);

								$card = array();
								$_REQUEST['data']['currency_id'] = $card['currency_id'] = $currency->id;
								$card['currency_code'] = $currency->code;
								$card['currency_symbol'] = $currency->symbol;
								$card['amount'] = $price;
								$card['token'] = @$_REQUEST['token'];
								$card['profile_id'] = @$_REQUEST['profile_id'];
								$card['card_holder_name'] = @$_REQUEST['card_name'];
								$card['card_type'] = @$_REQUEST['card_type'];
								$card['exp_month'] = $_REQUEST['exp_month'];
								$card['exp_year'] = $_REQUEST['exp_year'];
								$card['studio_name'] = $studio->name;

								$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
								Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
								$payment_gateway = new $payment_gateway_controller();
								$trans_data = $payment_gateway::processTransactions($card);

								if (isset($trans_data['is_success']) && intval($trans_data['is_success'])) {
									$_REQUEST['data']['transaction_data']['transaction_status'] = $trans_data['transaction_status'];
									$_REQUEST['data']['transaction_data']['invoice_id'] = $trans_data['invoice_id'];
									$_REQUEST['data']['transaction_data']['order_number'] = $trans_data['order_number'];
									$_REQUEST['data']['transaction_data']['dollar_amount'] = $trans_data['dollar_amount'];
									$_REQUEST['data']['transaction_data']['amount'] = $trans_data['amount'];
									$_REQUEST['data']['transaction_data']['response_text'] = $trans_data['response_text'];
									$_REQUEST['data']['transaction_data']['is_success'] = $trans_data['is_success'];
								} else {
									$data = $trans_data;
									$is_register_success = 0;
									$this->code = 622;
								}
							}

							if ($is_register_success) {
								$ret = SdkUser::model()->saveUserPayment($_REQUEST, $this->studio_id);
								if (@$ret['is_subscribed']) {
									$file_name = '';
									if (isset($trans_data['is_success']) && intval($trans_data['is_success'])) {
										$user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
										$user['card_holder_name'] = $_REQUEST['data']['card_name'];
										$user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];
										$file_name = Yii::app()->pdf->invoiceDetial($this->studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
									}

									$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $users->id, 'welcome_email_with_subscription', $file_name);
									$admin_welcome_email = $this->sendStudioAdminEmails($this->studio_id, 'admin_new_paying_customer', $users->id, '', 0);

									$this->code = 200;
								} else {
									$this->code = 661;
								}
							} else {
								$this->code = 661;
							}
						} else {
							$this->code = 648;
						}
					} else {
						$this->code = 612;
					}
				} else {
					$this->code = 613;
				}
			} else {
				$this->code = 611;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method setSubscriptionForOtherDevices: Set the user subscription of User when purchase subscription from other device like Roku, Apple etc
	 * @return json Returns the response in json format
	 * @param String authToken*: Auth token of the studio
	 * @param string email*: Email of the user
	 * @param string plan_id*: Unique Plan id of the Subscription Plan
	 * @param int device_type*: Device type For Apple, Roku
	 * @param string out_source_user_id*: Apple, Roku user id. Mandatory field for Apple device
	 * @param string out_source_plan_id*: Apple product id. Mandatory field for Apple device
	 * @param float amount: Apple product amount
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionSetSubscriptionForOtherDevices() {
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$email = trim(@$_REQUEST['email']);
			$plan_id = trim(@$_REQUEST['plan_id']);
			$device_type = trim(@$_REQUEST['device_type']);
			$out_source_user_id = trim(@$_REQUEST['out_source_user_id']);
			$out_source_plan_id = @$_REQUEST['out_source_plan_id'];
			$amount = (isset($_REQUEST['amount']) && trim($_REQUEST['amount'])) ? number_format((float) ($_REQUEST['amount']), 2, '.', '') : number_format((float) (0), 2, '.', '');

			if (!empty($email) && !empty($plan_id) && !empty($device_type)) {
				$user = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status' => 1));
				if (isset($user) && !empty($user)) {
					$planData = SubscriptionPlans::model()->findByAttributes(array('unique_id' => $plan_id, 'studio_id' => $this->studio_id, 'status' => 1));
					if (isset($planData) && !empty($planData)) {
						$usModel = new UserSubscription;
						$subscriptionData = $usModel->findByAttributes(array('studio_id' => $this->studio_id, 'plan_id' => $planData->id, 'user_id' => $user->id, 'device_type' => $device_type, 'status' => 1));
						if (isset($subscriptionData) && !empty($subscriptionData)) {//User has already purchased the subscription
							$this->code = 736;
						} else {
							$start_date = $end_date = Null;
							$trail_period = $planData->trial_period;
							if (isset($trail_period) && (intval($trail_period) == 0)) {
								$start_date = gmdate('Y-m-d H:i:s');
								$recurrence_frequency = $planData->frequency . ' ' . strtolower($planData->recurrence) . "s";
								$end_date = date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
							} else {
								$recurrence_frequency = $planData->frequency . ' ' . strtolower($planData->recurrence) . "s";
								$trail_recurrence = $planData->trial_recurrence;
								$trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
								$start_date = gmdate('Y-m-d H:i:s', strtotime("+{$trial_period}"));
								$end_date = date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
							}
							$usModel->start_date = $start_date;
							$usModel->end_date = $end_date;
							$usModel->studio_id = $this->studio_id;
							$usModel->user_id = $user->id;
							$usModel->plan_id = $planData->id;
							$usModel->device_type = @$device_type;
							$usModel->status = 1;
							$usModel->created_by = $user->id;
							$usModel->created_date = gmdate('Y-m-d H:i:s');
							$usModel->outsource_plan_id = $out_source_plan_id;
							
							if (abs($amount) >= 0.01) {
								$usModel->amount = $amount;
							}
							$usModel->save();

							if (trim($out_source_user_id)) {
								$users = SdkUser::model()->findByPk($user->id);
								$users->out_source_user_id = $out_source_user_id;
								$users->save();
							}
							$this->code = 200;
						}
					} else {//Subscription plans are not found
						$this->code = 648;
					}
				} else {
					$this->code = 633; //User doesn't exists
				}
			} else {//Either email or plan id or device type is missing
				$this->code = 735;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	 * @method validateCouponForSubscriptionPlan: Validate the coupon code for subscription plans
	 * @return json Returns the response in json format
	 * @param String authToken*: Auth token of the studio
	 * @param int user_id*: user id
	 * @param string coupon*: Coupon code
	 * @param string plan_id*: subcription plan id
	 * @param int currency: currency id
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionValidateCouponForSubscriptionPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$arg = array();
			$arg['user_id'] = $user_id = trim(@$_REQUEST['user_id']);
			$arg['coupon'] = $coupon = trim(@$_REQUEST['coupon']);
			if ($user_id && $plan_id && $coupon) {
				$currency_id = (isset($_REQUEST['currency']) && intval($_REQUEST['currency'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($this->studio_id)->default_currency_id;
				$arg['studio_id'] = $this->studio_id;
				$arg['currency'] = $currency_id;
				$couponDetails = Yii::app()->billing->isValidCouponForSubscription($arg);

				if ($couponDetails != 0) {
					if ($couponDetails != 1) {
						$this->code = 200;

						$planDetails = SubscriptionPlans::model()->getPlanDetails($plan_id, $this->studio_id, $currency_id, '', 1);
						$data['discount'] = $couponDetails['discount_amount'];
						if ($couponDetails["discount_type"] == 1) {
							$final_price = $planDetails['price'] - ($planDetails['price'] * $couponDetails['discount_amount'] / 100);
							$data['full_amount'] = $planDetails['price'];
							$data['discount_amount'] = $final_price;
							$data['extend_free_trail'] = $couponDetails['extend_free_trail'];
							$data['discount_type'] = '%';
						} else {
							if (isset($_REQUEST['currency']) && intval($_REQUEST['currency'])) {
								$currencyDetails = Currency::model()->findByPk($_REQUEST['currency']);
								$final_price = $planDetails['price'] - $couponDetails['discount_amount'];
								if (abs($final_price) < 0.01) {
									$final_price = 0;
								}
								$new_final_price = number_format($final_price, 2, '.', '');
								$data['full_amount'] = $planDetails['price'];
								$data['discount_amount'] = $new_final_price;
								$data['extend_free_trail'] = $couponDetails['extend_free_trail'];
								$data['discount_type'] = $currencyDetails['symbol'];
							} else {
								$data['discount_type'] = '$';
							}
						}
					} else {
						$this->code = 651;
					}
				} else {
					$this->code = 650;
				}
			} else {//Either email or plan id or coupon code is missing
				$this->code = 0;
			}
		} else {
			$this->code = 662;
		}
		$this->items = $data;
	}

	/**
	 * @method public isContentAuthorized: Checks the user is authenticated to watch the content or not
	 * @return json Returns the list of data in json format
	 * @param String authToken*: Auth token of the studio
	 * @param string movie_id*: uniq id of film or muvi
	 * @param int user_id*: user id
	 * @param int season_id: season id
	 * @param string episode_id: uniq id of episode
	 * @param string purchase_type: show/episode for multipart only
	 * @param string country: Country code
	 * @param string lang_code: Language code
	 * @author Sunil<sunil@muvi.com>
	 */
	function actionIsContentAuthorized() {
		if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
			$studio_id = $this->studio_id;
			$user_id = $_REQUEST['user_id'];
			$lang_code = @$_REQUEST['lang_code'];
			$translate = $this->getTransData($lang_code, $studio_id);
			if (@$user_id) {
				$usercriteria = new CDbCriteria();
				$usercriteria->select = "is_developer";
				$usercriteria->condition = "id=$user_id";
				$userData = SdkUser::model()->find($usercriteria);
			}
			$data = array();

			$movie_code = @$_REQUEST['movie_id'];
			$movie_id = Yii::app()->common->getMovieId($movie_code);

			$stream_code = @$_REQUEST['episode_id'];
			$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

			//Get stream Id
			$stream_id = 0;
			if (trim($movie_code) != '0' && trim($stream_code) != '0') {
				$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
			} else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
				$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
			} else {
				$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
			}

			if (@$userData->is_developer) {
				$mov_can_see = 'allowed';
			} else {
				$can_see_data['movie_id'] = $movie_id;
				$can_see_data['season'] = @$season;
				$can_see_data['stream_id'] = $stream_id;
				$can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
				$can_see_data['studio_id'] = $studio_id;
				$can_see_data['user_id'] = $user_id;
				$can_see_data['country'] = @$_REQUEST['country'];

				$mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
			}

			$data['string_code'] = $mov_can_see;

			if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
				$this->code = 652;
			} else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
				$this->code = 653;
			} else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
				$this->code = 654;
			} else if ($mov_can_see == 'advancedpurchased') {
				$this->code = 655;
			} else if ($mov_can_see == 'access_period') {
				$this->code = 656;
			} else if ($mov_can_see == 'watch_period') {
				$this->code = 657;
			} else if ($mov_can_see == 'maximum') {
				$this->code = 658;
			} else if ($mov_can_see == 'already_purchased') {
				$this->code = 655;
			} else if ($mov_can_see == 'unpaid') {
				$this->code = 659;
				$data['member_subscribed'] = 0;
				$is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
				if ($is_subscribed) {
					$data['member_subscribed'] = 1;
				}
			} else if ($mov_can_see == 'allowed') {
				$this->code = 200;
			}
		} else {
			$this->code = 680;
		}

		$this->items = $data;
	}

	/**
	 * @method public getCardsList: It will check if the card can be saved from store's payment gateway and if yes, return lists of card detail
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionGetCardsList() {
		$studio_id = $this->studio_id;
		$user_id = @$_REQUEST['user_id'];
		$data = array();

		if ($user_id) {
			$gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
			if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
				$gateways = $gateway['gateways'];
				if (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) {
					$this->code = 200;
					$data['can_save_card'] = 1;

					$condcard = array(':studio_id' => $studio_id, ':user_id' => $user_id);
					$cardsql = Yii::app()->db->createCommand()
							->select('c.card_uniq_id AS card_id, c.gateway_id, c.card_name, c.card_holder_name, c.card_last_fourdigit, c.card_type, c.exp_month, c.exp_year')
							->from('sdk_card_infos c')
							->where('c.studio_id=:studio_id AND c.user_id=:user_id AND c.gateway_id !=1 AND c.gateway_id !=4', $condcard, array('order' => 'is_cancelled ASC, id DESC'), array('group' => 'exp_month, exp_year, card_holder_name, card_type, card_last_fourdigit'));
					$cards = $cardsql->queryAll();
					$data['cards'] = (!empty($cards)) ? $cards : array();
				} else {
					$this->code = 647;
					$data['can_save_card'] = 0;
				}
			} else {
				$this->code = 612;
			}
		} else {
			$this->code = 639;
		}

		$this->items = $data;
	}

	function isCouponExists() {
		$data = Yii::app()->general->monetizationMenuSetting($this->studio_id);
		$isCouponExists = 0;
		if (isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
			$sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$this->studio_id} AND c.status=1";
			$coupon = Yii::app()->db->createCommand($sql)->queryAll();
			if (isset($coupon) && !empty($coupon)) {
				$isCouponExists = 1;
			}
		}
		return $isCouponExists;
	}

	/**
	 * @method public validateCouponCode: It will validate coupon code if it is enabled
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String* coupon_code: Coupon code
	 * @param Int currency_id: currency id
	 * @param String lang_code: Language code
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionValidateCouponCode() {
		$data = array();
		$couponeDetails = Yii::app()->common->couponCodeIsValid($_REQUEST["coupon_code"], @$_REQUEST['user_id'], $this->studio_id, @$_REQUEST["currency_id"]);
		if ($couponeDetails != 0) {
			if ($couponeDetails != 1) {
				$this->code = 200;

				$data['discount'] = $couponeDetails['discount_amount'];
				if ($couponeDetails["discount_type"] == 1) {
					$data['discount_type'] = '%';
				} else {
					if ($_REQUEST["currency_id"] != 0) {
						$currencyDetails = Currency::model()->findByPk($_REQUEST["currency_id"]);
						$data['discount_type'] = $currencyDetails['symbol'];
					} else {
						$data['discount_type'] = '$';
					}
				}
			} else {
				$this->code = 651;
			}
		} else {
			$this->code = 650;
		}

		$this->items = $data;
	}

	/**
	 * @method public createPpvPayPalPaymentToken: Create a payment token for paypal express checkout
	 * @return json Json data with parameters
	 * @param String authToken*: Auth token of the studio
	 * @param int plan_id*: Plan id
	 * @param int user_id*: User id
	 * @param studio_id*: Studio id
	 * @param string movie_id*: Unique id of content
	 * @param int is_bundle
	 * @param int timeframe_id
	 * @param int is_advance_purchase
	 * @param int episode_id
	 * @param string episode_uniq_id
	 * @param int season_id
	 * @param string permalink
	 * @param string couponCode
	 * @author Sanjeev<sanjeev@muvi.com>
	 */
	public function actionCreatePpvPayPalPaymentToken() {
		$request_data = isset($_REQUEST) ? $_REQUEST : array();
		$plan_id = (isset($request_data['plan_id']) && $request_data['plan_id'] > 0) ? $request_data['plan_id'] : 0;
		$user_id = (isset($request_data['user_id']) && $request_data['user_id'] > 0) ? $request_data['user_id'] : 0;
		$studio_id = (isset($request_data['studio_id']) && $request_data['studio_id'] > 0) ? $request_data['studio_id'] : 0;
		$authToken = $request_data['authToken'];
		if ($user_id > 0) {
			if ($studio_id > 0) {
				$studio = Studio::model()->findByPk($studio_id, array('select' => 'domain'));
				if (isset($plan_id) && $plan_id > 0) {
					$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
					}
					$VideoDetails = '';
					$video_id = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $request_data['movie_id']), array('select' => 'id'))->id;
					//Check studio has plan or not
					$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

					$is_bundle = @$request_data['is_bundle'];
					$timeframe_id = @$request_data['timeframe_id'];
					$isadv = @$request_data['is_advance_purchase'];
					$timeframe_days = 0;
					$end_date = '';

					//Get plan detail which user selected at the time of registration
					$plan = PpvPlans::model()->findByPk($plan_id);
					$request_data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;

					if (intval($is_bundle) && intval($timeframe_id)) {
						(array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $request_data['timeframe_id'], $studio_id);
						$price = $price->attributes;
						$timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'], array('select' => 'days'))->days;
					} else {
						$plan_details = PpvPricing::model()->findByAttributes(array('ppv_plan_id' => $plan_id, 'status' => 1));
						$price = Yii::app()->common->getPPVPrices($plan->id, $plan_details->currency_id);
					}
					$currency = Currency::model()->findByPk($price['currency_id'], array('select' => 'id,code,symbol'));
					$data['currency_id'] = $currency->id;
					$data['currency_code'] = $currency->code;
					$data['currency_symbol'] = $currency->symbol;
					//Calculate ppv expiry time only for single part or episode only
					$start_date = Date('Y-m-d H:i:s');
					if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
						$time = strtotime($start_date);
						$expiry = $timeframe_days . ' ' . "days";
						$end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
					} else {
						if (intval($isadv) == 0) {
							$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
							$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

							$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
							$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

							$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
							$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

							$time = strtotime($start_date);
							if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
								$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
							}
							$data['view_restriction'] = $limit_video;
							$data['watch_period'] = $watch_period;
							$data['access_period'] = $access_period;
						}
					}
					if ($is_bundle) {//For PPV bundle
						$data['season_id'] = 0;
						$data['episode_id'] = 0;
						if (intval($is_subscribed_user)) {
							if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
								$data['amount'] = $amount = $price['show_subscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_subscribed'];
							}
						} else {
							if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
								$data['amount'] = $amount = $price['show_unsubscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_unsubscribed'];
							}
						}
					} else {
						//Set different prices according to schemes
						if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
							//Check which part wants to sell by studio admin
							$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'is_show,is_season,is_episode'));
							$is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
							$is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
							$is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

							if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && intval($request_data['season_id']) && isset($request_data['episode_id']) && trim($request_data['episode_id']) != '0') { //Find episode amount
								if (intval($is_episode)) {
									$streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']), array('select' => 'id,embed_id'));
									$data['episode_id'] = $streams->id;
									$data['episode_uniq_id'] = $streams->embed_id;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['episode_subscribed'];
									} else {
										$data['amount'] = $amount = $price['episode_unsubscribed'];
									}
								}
							} else if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && intval($request_data['season_id']) && isset($request_data['episode_id']) && trim($request_data['episode_id']) == 0) { //Find season amount
								if (intval($is_season)) {
									$data['episode_id'] = 0;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['season_subscribed'];
									} else {
										$data['amount'] = $amount = $price['season_unsubscribed'];
									}
								}
							} else if (isset($request_data['movie_id']) && trim($request_data['movie_id']) && isset($request_data['season_id']) && trim($request_data['season_id']) == 0 && isset($request_data['episode_id']) && trim($request_data['episode_id']) == 0) { //Find show amount
								if (intval($is_show)) {
									$data['season_id'] = 0;
									$data['episode_id'] = 0;
									if (intval($is_subscribed_user)) {
										$data['amount'] = $amount = $price['show_subscribed'];
									} else {
										$data['amount'] = $amount = $price['show_unsubscribed'];
									}
								}
							}
						} else {//Single part videos
							$data['season_id'] = 0;
							$data['episode_id'] = 0;
							if (intval($is_subscribed_user)) {
								$data['amount'] = $amount = $price['price_for_subscribed'];
							} else {
								$data['amount'] = $amount = $price['price_for_unsubscribed'];
							}
						}
					}
					$couponCode = '';
					//Calculate coupon if exists
					if ((isset($request_data['coupon']) && $request_data['coupon'] != '') || (isset($request_data['coupon_instafeez']) && $request_data['coupon_instafeez'] != '')) {
						$coupon = (isset($request_data['coupon']) && $request_data['coupon'] != '') ? $request_data['coupon'] : $request_data['coupon_instafeez'];
						$getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
						$data['amount'] = $amount = $getCoup["amount"];
						$couponCode = $getCoup["couponCode"];
					}
					if (intval($is_bundle)) {
						$VideoName = $plan->title;
					} else {
						$VideoName = trim(Yii::app()->common->getVideoname($video_id));
					}
					$VideoDetails = $VideoName;

					if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
						if ($data['season_id'] == 0) {
							$VideoDetails .= ', All Seasons';
						} else {
							$VideoDetails .= ', Season ' . $data['season_id'];
							if ($data['episode_id'] == 0) {
								$VideoDetails .= ', All Episodes';
							} else {
								$episodeName = Yii::app()->common->getEpisodename($streams->id);
								$episodeName = trim($episodeName);
								$VideoDetails .= ', ' . $episodeName;
							}
						}
					}
					$data['studio_id'] = $studio_id;
					$data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
					$log_data = array(
						'studio_id' => $studio_id,
						'user_id' => $user_id,
						'plan_id' => $plan_id,
						'uniq_id' => $request_data['movie_id'],
						'amount' => $data['amount'],
						'episode_id' => $request_data['episode_id'],
						'episode_uniq_id' => $request_data['episode_uniq_id'],
						'season_id' => $request_data['season_id'],
						'permalink' => $request_data['permalink'],
						'currency_id' => $data['currency_id'],
						'couponCode' => $request_data['coupon'],
						'is_bundle' => @$request_data['is_bundle'],
						'timeframe_id' => @$request_data['timeframe_id'],
						'isadv' => $isadv
					);
					$timeparts = explode(" ", microtime());
					$currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
					$reference_number = explode('.', $currenttime);
					$unique_id = $reference_number[0];
					$log = new PciLog();
					$log->unique_id = $unique_id;
					$log->log_data = json_encode($log_data);
					$log->save();
					if (strpos($studio->domain, 'idogic.com') !== false || strpos($studio->domain, 'muvi.com') !== false) {
						$pre = 'http://www.';
					} else {
						$pre = 'https://www.';
					}
					$muvi_token = $unique_id . "-" . $authToken;
					$data['returnURL'] = $pre . $studio->domain . "/rest/ppvSuccess?muvi_token=" . $muvi_token;
					$data['cancelURL'] = $pre . $studio->domain . "/rest/ppvPayPalCancel?muvi_token=" . $muvi_token;
					$data['user_id'] = $user_id;
					$payment_gateway_controller = 'ApipaypalproController';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$resArr = $payment_gateway::createToken($data);
					if ($resArr['ACK'] == 'Success') {
						$this->code = 200;
						$this->items['url'] = $resArr['REDIRECTURL'];
						$this->items['token'] = $resArr['TOKEN'];
					} else
						$this->code = 691;
				} else
					$this->code = 690;
			} else
				$this->code = 692;
		} else
			$this->code = 639;
	}

	/**
     * @method public ppvSuccess: Handles paypal retun token and do a expresscheckout(This is not an Api)
     * @return json Json data with parameters
	 * @param String muvi_token*
	 * @param String token*
	 * @author SKM<sanjeev@muvi.com>
    */
    public function actionPpvSuccess() {
		if (isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
			$muvi_token = explode('-',$_REQUEST['muvi_token']);
			$unique_id = trim(@$muvi_token[0]);
			if ($unique_id) {
				$log_data = PciLog::model()->findByAttributes(array('unique_id'=>$unique_id));
				$data = json_decode($log_data['log_data'],true);
				$studio_id = $data['studio_id'];
				$gateway_code = '';
				$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
				if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
					$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
					$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
				}

				$payment_gateway_controller = 'ApipaypalproController';
				Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
				$payment_gateway = new $payment_gateway_controller();
				$trans_data = $payment_gateway::processCheckoutDetails($_REQUEST['token']);
				$userdata['TOKEN'] = $trans_data['TOKEN'];
				$userdata['PAYERID'] = $trans_data['PAYERID'];
				$userdata['AMT'] = $trans_data['AMT'];
				$userdata['CURRENCYCODE'] = $trans_data['CURRENCYCODE'];
				$resArray = $payment_gateway::processDoPayment($userdata);
				
				if(isset($resArray) && $resArray['ACK'] == 'Success') {
					$plan_id = $data['plan_id'];
					$isadv = $data['isadv'];
					$planModel = new PpvPlans();
					$plan = $planModel->findByPk($plan_id);

					$start_date = Date('Y-m-d H:i:s');
					$end_date = '';
					if (intval($isadv) == 0) {
						$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
						$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

						$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
						$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

						$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
						$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

						$time = strtotime($start_date);
						if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
							$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
						}

						$data['view_restriction'] = $limit_video;
						$data['watch_period'] = $watch_period;
						$data['access_period'] = $access_period;
					}
					
					$Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$data['uniq_id']));
					$video_id = $Films->id;
					$VideoName = Yii::app()->common->getVideoname($video_id);
					$VideoDetails = $VideoName = trim($VideoName);
					$streams = movieStreams::model()->findByAttributes(array('studio_id'=>$studio_id, 'embed_id'=>$data['episode_id']));
					if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
						if ($data['season_id'] == 0) {
							$VideoDetails .= ', All Seasons';
						} else {
							$VideoDetails .= ', Season ' . $data['season_id'];
							if ($data['episode_id'] == 0) {
								$VideoDetails .= ', All Episodes';
							} else {
								$episodeName = Yii::app()->common->getEpisodename($streams->id);
								$episodeName = trim($episodeName);
								$VideoDetails .= ', ' . $episodeName;
							}
						}
					}
					
					$data['amount'] = $trans_data['amount'] = $trans_data['AMT'];
					$ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['couponCode'], $isadv, $gateway_code);
					
					$trans_data['transaction_status'] = $resArray['PAYMENTSTATUS'];
					$trans_data['invoice_id'] = $resArray['CORRELATIONID'];
					$trans_data['order_number'] = $resArray['TRANSACTIONID'];
					$trans_data['amount'] = $trans_data['AMT'];
					$trans_data['dollar_amount'] = $trans_data['AMT'];
					$trans_data['response_text'] = json_encode($trans_data);
					$transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $isadv);
					$ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
					PciLog::model()->deleteByPk($log_data['id']);
					
					$this->code = 200;
				} else {
					$this->code = 614;
				}
			} else {
				$this->code = 734;
			}
		} else {
			$this->code = 733;
		}
    }

	/**
     * @method public ppvPayPalCancel: Handles transaction cancled by user(This is not an Api)
     * @author SKM<sanjeev@muvi.com>
	 * @param String muvi_token*
     * @return json Json data with parameters
    */
    public function actionPpvPayPalCancel() {
        if (isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
			$muvi_token = explode('-',$_REQUEST['muvi_token']);
			$unique_id = trim(@$muvi_token[0]);
			if ($unique_id) {
				$log_data = AppPayPalLog::model()->findByAttributes(array('unique_id'=>$unique_id));
				AppPayPalLog::model()->deleteByPk($log_data['id']);
				$this->code = 200;
				$this->msg_code = 'transaction_canceled';
			} else {
				$this->code = 734;
			}
		} else {
			$this->code = 733;
		}
    }

	/**
	 * @method public purchaseHistory: It will display list of purchase History data 
	 * @return Json string
	 * @param String authToken*: Auth token of the studio
	 * @param int user_id*: user id
	 * @param string lang_code: Language code
	 * @author Sunil Nayak<suniln@muvi.com>
	 * 
	 */
	public function actionPurchaseHistory() {
		$user_id = @$_REQUEST['user_id'];
		$page_number = @$_REQUEST['limit'];
		$studio_id = $this->studio_id;
		if ($user_id > 0) {
			$subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);
			$userdate['expire_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date . '-1 Days'));
			$userdate['nextbilling_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date));
			//Pagination codes
			$items_per_page = 5;
			$page_size = $limit = $items_per_page;
			$offset = 0;
			if (isset($page_number)) {
				$offset = ($page_number - 1) * $limit;
			} else {
				$page_number = 1;
			}
			$transaction = Yii::app()->db->createCommand()
							->select('t.id, transaction_date, currency_id, amount, user_id, plan_id, payer_id, invoice_id ,subscription_id,ppv_subscription_id,movie_id,order_number,transaction_type,transaction_status,symbol AS currency_symbol,code AS currency_code')
							->from('transactions t')
							->join('currency c', 't.currency_id=c.id')
							->where('studio_id=:studio_id AND user_id=:user_id', array(':studio_id' => $studio_id, ':user_id' => $user_id))
							->order('t.id DESC')
							->limit($limit, $offset)->queryAll();
			$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
			$planModel = new PpvPlans();
			$transactions = array();
			$currdatetime = strtotime(date('y-m-d'));
			foreach ($transaction as $key => $details) {
				$transactions[$key]['invoice_id'] = $details['invoice_id'];
				$transactions[$key]['transaction_date'] = date('F d, Y', strtotime($details['transaction_date']));
				$transactions[$key]['amount'] = $details['amount'];
				$transactions[$key]['transaction_status'] = $details['transaction_status'];
				$transactions[$key]['currency_symbol'] = $details['currency_symbol'];
				$transactions[$key]['currency_code'] = $details['currency_code'];
				$transactions[$key]['id'] = $this->getUniqueIdEncode($details['id']);
				if ($details['transaction_type'] == 1) {
					$trans_type = 'Monthly subscription';
					$subscription_id = $details['subscription_id'];
					$startdate = UserSubscription::model()->findByAttributes(array('id' => $subscription_id), array('select' => 'start_date'))->start_date;
					//Check current date less than start date
					if ($currdatetime <= strtotime($startdate)) {
						$transactions[$key]['statusppv'] = 'Active';
					} else {
						$transactions[$key]['statusppv'] = 'N/A';
					}
					$transactions[$key]['Content_type'] = 'digital';
				} elseif ($details['transaction_type'] == 2) {
					$ppv_subscription_id = $details['ppv_subscription_id'];
					$subscription_details = PpvSubscription::model()->findByPk($ppv_subscription_id, array('select' => 'movie_id,season_id,episode_id,end_date'));
					$end_date = @$subscription_details->end_date;
					$season_id = @$subscription_details->season_id;
					$episode_id = @$subscription_details->episode_id;
					$movie_id = @$subscription_details->movie_id;
					$film_data = Film::model()->findByAttributes(array('id' => $movie_id), array('select' => 'id,name,content_types_id'));
					$movie_name = @$film_data->name;
					$content_types_id = @$film_data->content_types_id;
					$episode_name = ($episode_id > 0) ? $this->getEpisodeName($episode_id) : "";
					if ($season_id != 0 && $episode_id != 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> Season #" . $season_id . " ->" . $episode_name;
					} else if ($season_id == 0 && $episode_id != 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> " . $episode_name;
					} else if ($season_id != 0 && $episode_id == 0) {
						$transactions[$key]['movie_name'] = $movie_name . " -> Season #" . $season_id;
					} else {
						$transactions[$key]['movie_name'] = $movie_name;
					}
					$statusppv = 'N/A';
					if ($content_types_id == 1) {
						if ($currdatetime <= strtotime($end_date) && ($end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						}
					} else if ($content_types_id == 3) {
						if ($currdatetime <= strtotime($end_date) && ($end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						} else if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
							$statusppv = 'Active';
						} else if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
							$statusppv = 'Active';
						}
					}
					$transactions[$key]['statusppv'] = $statusppv;
					if (strtotime($end_date) != '' && $end_date != '0000-00-00 00:00:00') {
						$transactions[$key]['expiry_dateppv'] = date('F d, Y', strtotime($end_date));
					} else {
						$transactions[$key]['expiry_dateppv'] = 'N/A';
					}
					$trans_type = 'Pay-per-view' . "-" . $transactions[$key]['movie_name'];
					$transactions[$key]['Content_type'] = 'digital';
				} else if ($details['transaction_type'] == 3) {
					$title = $planModel->findByPk($details['plan_id'], array('select' => 'title'))->title;
					$transactions[$key]['movie_name'] = $title;
					$trans_type = 'Pre-order' . "<br>" . $title;
					$transactions[$key]['Content_type'] = 'digital';
				} else if ($details['transaction_type'] == 4) {
					$trans_type = 'physical';
					$orderquery = "SELECT d.id,d.name,d.price,d.quantity,d.product_id,d.item_status,d.cds_tracking,d.tracking_url,o.order_number,o.cds_order_status FROM pg_order_details d, pg_order o WHERE d.order_id=o.id AND o.transactions_id=" . $details['id'] . " ORDER BY id DESC";
					$order = Yii::app()->db->createCommand($orderquery)->queryAll();
					$transactions[$key]['details'] = $order;
					$transactions[$key]['order_item_id'] = $order[0]['id'];
					$transactions[$key]['order_number'] = $order[0]['order_number'];
					$transactions[$key]['cds_order_status'] = $order[0]['cds_order_status'];
					$transactions[$key]['cds_tracking'] = $order[0]['cds_tracking'];
					$transactions[$key]['tracking_url'] = $order[0]['tracking_url'];
					$transactions[$key]['Content_type'] = 'physical';
				} else if ($details['transaction_type'] == 5) {
					$title = $planModel->findByPk($details['plan_id'], array('select' => 'title'))->title;
					$ppv_subscription_id = $details['ppv_subscription_id'];
					$ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id), array('select' => 'timeframe_id'))->timeframe_id;
					$ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid), array('select' => 'validity_days'))->validity_days;
					$transaction_dates = $details['transaction_date'];
					$expirydate = date('F d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
					$expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
					if ($currdatetime <= $expirytime) {
						$statusbundles = 'Active';
					} else {
						$statusbundles = 'N/A';
					}
					$transactions[$key]['expiry_date'] = ($expirydate == "") ? $expirydate : "N/A";
					$transactions[$key]['movie_name'] = $title;
					$transactions[$key]['Content_type'] = 'digital';
					$trans_type = 'Pay-per-view Bundle' . "-" . $title;
					$transactions[$key]['statusppv'] = $statusbundles;
				} else {
					$trans_type = '';
				}
				$transactions[$key]['transaction_for'] = $trans_type;
			}
			$this->code = 200;
			$this->items['section'] = $transactions;
		} else
			$this->code = 663;
	}

	/**
	 * @method public transaction: It will give details of the transaction
	 * @return json object
	 * @param String authToken*: Auth token of the studio
	 * @param string encoded_id*: Uniq id
	 * @param int user_id*: user id
	 * @param string lang_code: Language code 
	 * @author Sunil Nayak<suniln@muvi.com>
	 */
	public function actionTransaction() {
		$user_id = @$_REQUEST['user_id'];
		if ($user_id > 0) {
			$encoded_id = (isset($_REQUEST['encoded_id']) && $_REQUEST['encoded_id'] != '') ? $_REQUEST['encoded_id'] : '';
			$id = $this->getUniqueIdDecode($encoded_id);
			if ($id > 0) {
				$transaction = Yii::app()->general->getTransactionDetails($id, $user_id, 1);
				$transaction_details['payment_method'] = $transaction['payment_method'];
				$transaction_details['transaction_status'] = $transaction['transaction_status'];
				$transaction_details['transaction_date'] = $transaction['transaction_date'];
				$transaction_details['amount'] = $transaction['amount'];
				$transaction_details['currency_symbol'] = $transaction['currency_symbol'];
				$transaction_details['currency_code'] = $transaction['currency_code'];
				$transaction_details['invoice_id'] = $transaction['invoice_id'];
				$transaction_details['order_number'] = $transaction['order_number'];
				$transaction_details['plan_name'] = $transaction['plan_name'];
				$transaction_details['plan_recurrence'] = $transaction['plan_recurrence'];
				$this->code = 663;
				$this->items['section'] = $transaction_details;
			} else
				$this->code = 689;
		} else
			$this->code = 663;
	}

	/**
	 * @method public getPurchaseInvoice: It returns invoice path
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: user id
	 * @param String device_type*: Android, iOS
	 * @param Int transaction_id*: Transaction id
	 * @author Sunil Nayak<suniln@muvi.com>
	 */
	function actionGetPurchaseInvoice() {
		$data = array();
		if ($_REQUEST['user_id'] && $_REQUEST['device_type'] && $_REQUEST['transaction_id']) {
			$transaction_id = $this->getUniqueIdDecode(@$_REQUEST['transaction_id']);
			$name = Yii::app()->pdf->downloadUserinvoice($this->studio_id, $transaction_id, @$_REQUEST['user_id'], '', '', @$_REQUEST['device_type']);
			if (trim($name)) {
				$this->code = 200;
				$data['invoice'] = $name;
			} else {
				$this->code = 663;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public deletePurchaseInvoice: It deletes the purchase invoice
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param String invoice*: Invoice id
	 * @author Sunil Nayak<suniln@muvi.com>
	 */
	function actionDeletePurchaseInvoice() {
		if ($_REQUEST['invoice']) {
			$invoice_path = ROOT_DIR . 'docs/' . $_REQUEST['invoice'];
			if (file_exists($invoice_path)) {
				unlink($invoice_path);
				$this->code = 200;
			} else {
				$this->code = 664;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	 * @method public getMonetizationMethods: It returns all monetization methods which is associated to a content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @param String stream_id: episode unique code
	 * @param Int season
	 * @param String purchase_type: show/episode for multipart only
	 * @author Ashis Barik<ashis@muvi.com>
	 */
	public function actionGetMonetizationMethods() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim($_REQUEST['user_id']);
			$movie_code = trim($_REQUEST['movie_id']) ? trim($_REQUEST['movie_id']) : 0;
			$stream_code = trim($_REQUEST['stream_id']);
			$season = trim($_REQUEST['season']) ? trim($_REQUEST['season']) : 0;
			$purchase_type = trim($_REQUEST['purchase_type']);

			if (isset($user_id) && $user_id > 0 && isset($movie_code) && !empty($movie_code)) {
				$cond = '';
				$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
				if ($stream_code) {
					$cond = " AND m.embed_id=:embed_id";
					$condArr[":embed_id"] = $stream_code;
				}
				if ($season) {
					$cond .= " AND m.series_number=:series_number";
					$condArr[":series_number"] = $season;
				}

				$command = Yii::app()->db->createCommand()
						->select('f.id,m.id as stream_id, m.series_number, f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
						->from('films f , movie_streams m')
						->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
				$films = $command->queryRow();
				$movie_id = $films['id'];
				$stream_id = @$films['stream_id'];
				$season_id = @$films['series_number'];
				$content_types_id = @$films['content_types_id'];
				if (intval($content_types_id) == 3) {
					$content = "'" . $movie_id . ":" . $season_id . ":" . $stream_id . "'";
				} else {
					$content = $movie_id;
				}

				$film = Film::model()->findByPk($movie_id);
				$monitization_data = array();
				$monitization_data['studio_id'] = $this->studio_id;
				$monitization_data['user_id'] = $user_id;
				$monitization_data['movie_id'] = $movie_id;
				$monitization_data['season'] = @$season_id;
				$monitization_data['stream_id'] = @$stream_id;
				$monitization_data['content'] = @$content;
				$monitization_data['film'] = $film;
				$monitization_data['purchase_type'] = $purchase_type;

				$monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
				$df_plans = array('ppv' => 0, 'pre_order' => 0, 'voucher' => 0, 'ppv_bundle' => 0);

				if (isset($monetization['monetization']) && !empty($monetization['monetization'])) {
					foreach ($monetization['monetization'] as $key => $value) {
						if (array_key_exists($key, $df_plans)) {
							$df_plans[$key] = 1;
						}
					}
					$data['monetizations'] = $monetization['monetization'];
					$data['monetization_plans'] = $df_plans;
				} else {
					$data['monetizations'] = array();
					$data['monetization_plans'] = $df_plans;
				}
				$this->code = 200;
			} else {
				$this->code = 662;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public getPpvPlan: It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @param String stream_id: stream unique code
	 * @param Int season: Season id
	 * @author Ashis Barik<ashis@muvi.com>
	 */
	public function actionGetPpvPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			$season_id = trim(@$_REQUEST['season']);
			$stream_code = trim(@$_REQUEST['stream_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
						$cond = '';
						$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
						if ($stream_code) {
							$cond = " AND m.embed_id=:embed_id";
							$condArr[":embed_id"] = $stream_code;
						}
						if ($season_id) {
							$cond .= " AND m.series_number=:series_number";
							$condArr[":series_number"] = $season_id;
						}

						$command = Yii::app()->db->createCommand()
								->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
								->from('films f, movie_streams m')
								->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$content_types_id = $films['content_types_id'];

							//If it has been on advance purchase
							$plan = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
							if (empty($plan)) {
								//Check ppv plan set or not by studio admin
								$plan = Yii::app()->common->getContentPaymentType($content_types_id, $films['ppv_plan_id'], $this->studio_id);
							}
							if (!empty($plan)) {
								$this->code = 200;
								if (intval($content_types_id) == 3) {
									$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
									$data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
									$data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
									$data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

									$EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);

									if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
										$series = explode(',', $EpDetails->series_number);
										sort($series);

										$data['max_season'] = $series[count($series) - 1];
										$data['min_season'] = $series[0];

										$data['series_number'] = $series_number = intval($season_id) ? $season_id : $series[0];

										$episql = "SELECT * FROM movie_streams WHERE studio_id=" . $this->studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
										$episodes = Yii::app()->db->createCommand($episql)->queryAll();
										$ep_data = array();
										if (isset($episodes) && !empty($episodes)) {
											for ($i = 0; $i < count($episodes); $i++) {
												$ep_data[$i]['id'] = $episodes[$i]['id'];
												$ep_data[$i]['movie_id'] = $episodes[$i]['movie_id'];
												$ep_data[$i]['episode_title'] = $episodes[$i]['episode_title'];
												$ep_data[$i]['embed_id'] = $episodes[$i]['embed_id'];
											}
										}
										$data['episode'] = $ep_data;
									}
								}
								$is_subscribed = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}

								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								$price = Yii::app()->common->getPPVPrices($plan->id, $default_currency_id);
								$currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;

								if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
									$season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
									$season_price_data['ppv_pricing_id'] = $price['id'];
									$season_price_data['studio_id'] = $this->studio_id;
									$season_price_data['currency_id'] = $price['currency_id'];
									$season_price_data['season'] = $season_id;
									$season_price_data['is_subscribed'] = $is_subscribed;
									$default_season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
									if ($default_season_price != -1) {
										$price['default_season_price'] = $default_season_price;
									}
								}

								$currency = Currency::model()->findByPk($currency_id);
								$validity = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');

								$data['is_coupon_exists'] = self::isCouponExists();
								$data['is_member_subscribed'] = @$member_subscribed;
								$data['price'] = $price;
								$data['currency_id'] = $currency->id;
								$data['currency_symbol'] = $currency->symbol;
								$data['ppv_validity'] = $validity;
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] = $card;
									}
								}
							} else {//Content is not in Pay-per-view plan
								$this->code = 666;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pay-per view is not enabled
						$this->code = 665;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public purchasePPV: It will process the payment for PPV content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @param int season_id: Season id
	 * @param String episode_id: Episode id
	 * @param String existing_card_id*: Uniq id. This is required when user is trying to payment through existing card
	 * @param string card_name*: Name as on Credit card
	 * @param String card_number*: Card Number 
	 * @param String cvv*: CVV/CVC of credit card
	 * @param String expiry_month*: Expiry month as on Card
	 * @param String expiry_year*: Expiry Year as on Card
	 * @param String profile_id*: User Profile id created in the payment gateways
	 * @param String card_last_fourdigit: Last 4 digits of the card 
	 * @param int currency_id: Currency id
	 * @param String coupon_code: Coupon code
	 * @param Boolean is_save_this_card: 1: true, 0: false
	 * @param String token: Payment Token
	 * @param String card_type: Type of the card
	 * @param String email: User email(Mandatory for Stripe gateway)
	 * @param String country: Country code
	 * @author Ashis Barik<ashis@muvi.com>
	 */
	public function actionPurchasePPV() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['movie_id']) && !empty($_REQUEST['movie_id'])) {
				$data['movie_id'] = @$_REQUEST['movie_id'];
				$data['season_id'] = @$_REQUEST['season_id'];
				$data['episode_id'] = @$_REQUEST['episode_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];

					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];
					$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];

					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}

				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 2)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = Yii::app()->common->checkAdvancePurchase($Films->id, $this->studio_id, 0);
								if (empty($plan)) {
									$plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
								}

								if (isset($plan) && !empty($plan)) {
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);

									$price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id'], $data['country']);
									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$end_date = '';

									$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'limit_video');
									$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

									$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'watch_period');
									$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

									$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');
									$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

									$time = strtotime($start_date);
									if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
										$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
									} elseif ((isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period))) {
										if (isset($plan->content_types_id) && intval($plan->content_types_id) != 3) {
											$end_date = date("Y-m-d H:i:s", strtotime("+{$watch_period}", $time));
										}
									}
									$data['view_restriction'] = $limit_video;
									$data['watch_period'] = $watch_period;
									$data['access_period'] = $access_period;

									if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
										//Check which part wants to sell by studio admin
										$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
										$is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
										$is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
										$is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

										if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
											if (intval($is_episode)) {
												$streams = movieStreams::model()->findByAttributes(array('studio_id' => $this->studio_id, 'embed_id' => $data['episode_id']));
												$data['episode_id'] = $streams->id;

												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['episode_subscribed'];
												} else {
													$data['amount'] = $price['episode_unsubscribed'];
												}
											}
										} else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
											if (intval($is_season)) {
												$data['episode_id'] = 0;

												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['season_subscribed'];
												} else {
													$data['amount'] = $price['season_unsubscribed'];
												}

												if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
													$season_price_data = array();
													$season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
													$season_price_data['ppv_pricing_id'] = $price['id'];
													$season_price_data['studio_id'] = $this->studio_id;
													$season_price_data['currency_id'] = $price['currency_id'];
													$season_price_data['season'] = $data['season_id'];
													$season_price_data['is_subscribed'] = $is_subscribed_user;

													$season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
													if ($season_price != -1) {
														$data['amount'] = $season_price;
													}
												}
											}
										} else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
											if (intval($is_show)) {
												$data['season_id'] = 0;
												$data['episode_id'] = 0;
												if (intval($is_subscribed_user)) {
													$data['amount'] = $price['show_subscribed'];
												} else {
													$data['amount'] = $price['show_unsubscribed'];
												}
											}
										}
									} else {//Single part videos
										$data['season_id'] = 0;
										$data['episode_id'] = 0;

										if (intval($is_subscribed_user)) {
											$data['amount'] = $price['price_for_subscribed'];
										} else {
											$data['amount'] = $price['price_for_unsubscribed'];
										}
									}

									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}

									$gateway_code = '';

									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}

									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = Yii::app()->common->getVideoname($Films->id);
										$VideoName = trim($VideoName);
										$VideoDetails = $VideoName;
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
											if ($data['season_id'] == 0) {
												$VideoDetails .= ', All Seasons';
											} else {
												$VideoDetails .= ', Season ' . $data['season_id'];
												if ($data['episode_id'] == 0) {
													$VideoDetails .= ', All Episodes';
												} else {
													$episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
													$episodeName = trim($episodeName);
													$VideoDetails .= ', ' . $episodeName;
												}
											}
										}

										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 0, $gateway_code);
											$ppv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 0, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 0, $gateway_code);
												$ppv_subscription_id = $set_ppv_subscription;

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $Films->id, $trans_data, $ppv_subscription_id, $gateway_code, 0);
												$ppv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 0, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pay-per-view plan
									$this->code = 666;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view is not enabled
							$this->code = 665;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				} else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	 * @method public getPreOrderPlan: It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @author Ashis Barik<ashis@muvi.com>
	 */
	public function actionGetPreOrderPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
						$command = Yii::app()->db->createCommand()
								->select('f.id,f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
								->from('films f ')
								->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id));
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$plan = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
							if (isset($plan) && !empty($plan)) {
								$this->code = 200;

								$is_subscribed = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}

								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								$price = Yii::app()->common->getPPVPrices($plan->id, $default_currency_id);
								$currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;
								$currency = Currency::model()->findByPk($currency_id);
								$validity = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'ppv', 'access_period');

								$data['isadv'] = 1;
								$data['is_coupon_exists'] = self::isCouponExists();
								$data['is_member_subscribed'] = @$member_subscribed;
								$data['price'] = $price;
								$data['currency_id'] = $currency->id;
								$data['currency_symbol'] = $currency->symbol;
								$data['preorder_validity'] = $validity;
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] = $card;
									}
								}
							} else {//Content is not in Pre order plan
								$this->code = 668;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pre order is not enabled
						$this->code = 667;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public purchasePreOrder: It will process the payment for pre order content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @param String existing_card_id*: Uniq id. This is required when user is trying to payment through existing card
	 * @param string card_name*: Name as on Credit card
	 * @param String card_number*: Card Number 
	 * @param String cvv*: CVV/CVC of credit card
	 * @param String expiry_month*: Expiry month as on Card
	 * @param String expiry_year*: Expiry Year as on Card
	 * @param String profile_id*: User Profile id created in the payment gateways
	 * @param String card_last_fourdigit: Last 4 digits of the card 
	 * @param int currency_id: Currency id
	 * @param String coupon_code: Coupon code
	 * @param Boolean is_save_this_card: 1: true, 0: false
	 * @param String token: Payment Token
	 * @param String card_type: Type of the card
	 * @param String email: User email(Mandatory for Stripe gateway)
	 * @param String country: Country code
	 * @author Sunil Kund<sunil@muvi.com>
	 */
	public function actionPurchasePreOrder() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['movie_id']) && !empty($_REQUEST['movie_id'])) {
				$data['movie_id'] = @$_REQUEST['movie_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];

					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];
					$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];

					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}

				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 16)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = Yii::app()->common->checkAdvancePurchase($Films->id, $this->studio_id, 0);
								if (isset($plan) && !empty($plan)) {
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);

									$price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id'], $data['country']);
									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$end_date = '';
									$data['season_id'] = 0;
									$data['episode_id'] = 0;

									if (intval($is_subscribed_user)) {
										$data['amount'] = $price['price_for_subscribed'];
									} else {
										$data['amount'] = $price['price_for_unsubscribed'];
									}

									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}

									$gateway_code = '';

									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}

									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = Yii::app()->common->getVideoname($Films->id);
										$VideoName = trim($VideoName);
										$VideoDetails = $VideoName;

										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$set_apv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 1, $gateway_code);
											$apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 1, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$set_apv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 1, $gateway_code);
												$ppv_subscription_id = $set_ppv_subscription;

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $Films->id, $trans_data, $ppv_subscription_id, $gateway_code, 1);
												$apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 1, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pre order plan
									$this->code = 668;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view is not enabled
							$this->code = 667;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				} else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	 * @method public getPpvBundlePlan: It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique code
	 * @author Sunil Kund<sunil@muvi.com>
	 */
	public function actionGetPpvBundlePlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

				if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
					$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
					if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
						$command = Yii::app()->db->createCommand()
								->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
								->from('films f ')
								->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id));
						$films = $command->queryRow();
						if (!empty($films)) {
							$movie_id = $films['id'];
							$sql = "SELECT al.*, GROUP_CONCAT(f.name) AS content_name FROM (SELECT t.*, pc.content_id AS movie_id FROM (SELECT pp.id, pp.title, pp.is_timeframe, pac.content_id FROM ppv_plans pp, ppv_advance_content pac WHERE pp.studio_id={$this->studio_id} AND pp.status=1 AND pp.is_timeframe=1 AND pp.is_advance_purchase=2 AND pp.id=pac.ppv_plan_id AND pac.content_id={$movie_id}) AS t, ppv_advance_content pc WHERE t.id=pc.ppv_plan_id) al, films f WHERE al.movie_id=f.id GROUP BY al.id";
							$plan = Yii::app()->db->createCommand($sql)->queryAll();
							if (isset($plan) && !empty($plan)) {
								$this->code = 200;
								$is_subscribed = Yii::app()->common->isSubscribed($user_id, $this->studio_id);
								if ($is_subscribed) {
									$member_subscribed = 1;
								}
								$data['is_ppv_bundle'] = 1;
								$data['is_member_subscribed'] = @$member_subscribed;
								$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
								foreach ($plan as $key => $value) {
									$data['plan'][$key]['plan_id'] = $value['id'];
									$data['plan'][$key]['title'] = $value['title'];
									$data['plan'][$key]['timeframe_prices'] = Yii::app()->common->getAllTimeFramePrices($value['id'], $default_currency_id, $this->studio_id);
								}
								$data['is_coupon_exists'] = self::isCouponExists();
								$data['can_save_card'] = @$gateway['gateways'][0]['paymentgt']['can_save_card'];
								if (intval(@$gateway['gateways'][0]['paymentgt']['can_save_card'])) {
									$card_sql = "SELECT tot.id, tot.gateway_id, tot.card_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$this->studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
									$card = Yii::app()->db->createCommand($card_sql)->queryAll();
									if (!empty($card)) {
										$data['card'] = $card;
									}
								}
							} else {//Content is not in Pay-per-view bundle plan
								$this->code = 725;
							}
						} else {//No content exists
							$this->code = 682;
						}
					} else {//Pay-per-view bundle is not enabled
						$this->code = 669;
					}
				} else {//No gateway found
					$this->code = 612;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public purchasePPVBundle: It will process the payment for PPV bundle content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: movie unique id
	 * @param Int plan_id*: ppv bundle plan id
	 * @param Int timeframe_id*: ppv bundle timeframe id
	 * @param String existing_card_id*: Card uniq id. This is required when user is trying to payment through existing card
	 * @param string card_name*: Name as on Credit card
	 * @param String card_number*: Card Number 
	 * @param String cvv*: CVV/CVC of credit card
	 * @param String expiry_month*: Expiry month as on Card
	 * @param String expiry_year*: Expiry Year as on Card
	 * @param String profile_id*: User Profile id created in the payment gateways
	 * @param String card_last_fourdigit: Last 4 digits of the card 
	 * @param int currency_id: Currency id
	 * @param String coupon_code: Coupon code
	 * @param Boolean is_save_this_card: 1: true, 0: false
	 * @param String token: Payment Token
	 * @param String card_type: Type of the card
	 * @param String email: User email(Mandatory for Stripe gateway)
	 * @param String country: Country code
	 * @author Sunil Kund<sunil@muvi.com>
	 */
	public function actionPurchasePPVBundle() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data['user_id'] = $user_id = @$_REQUEST['user_id'];
			$data['studio_id'] = $this->studio_id;
			if (intval($user_id) && isset($_REQUEST['plan_id']) && !empty($_REQUEST['plan_id'])) {
				$data['plan_id'] = @$_REQUEST['plan_id'];
				$data['timeframe_id'] = @$_REQUEST['timeframe_id'];
				$data['currency_id'] = @$_REQUEST['currency_id'];
				$data['coupon_code'] = @$_REQUEST['coupon_code'];

				$is_valid_card_information = 0;
				if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
					$is_valid_card_information = 1;
				} else {
					$data['card_holder_name'] = @$_REQUEST['card_name'];
					$data['card_number'] = @$_REQUEST['card_number'];
					$data['exp_month'] = @$_REQUEST['exp_month'];
					$data['exp_year'] = @$_REQUEST['exp_year'];
					$data['cvv'] = @$_REQUEST['cvv'];
					$data['profile_id'] = @$_REQUEST['profile_id'];

					$data['token'] = @$_REQUEST['token'];
					$data['card_type'] = @$_REQUEST['card_type'];
					$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
					$data['email'] = @$_REQUEST['email'];
					$data['country'] = @$_REQUEST['contry'];

					if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
						$is_valid_card_information = 1;
					}
				}

				if ($is_valid_card_information) {
					$gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
					if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
						$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
						if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 64)) {
							$Films = Film::model()->findByAttributes(array('studio_id' => $this->studio_id, 'uniq_id' => $data['movie_id']));
							if (!empty($Films)) {
								$plan = PpvPlans::model()->findByAttributes(array('id' => $data['plan_id'], 'studio_id' => $this->studio_id, 'is_advance_purchase' => 2));
								if (isset($plan) && !empty($plan)) {
									$data['is_bundle'] = 1;
									$is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $this->studio_id);

									(array) $price = Yii::app()->common->getTimeFramePrice($data['plan_id'], $data['timeframe_id'], $this->studio_id);
									$price = $price->attributes;
									$timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;

									$currency = Currency::model()->findByPk($price['currency_id']);
									$data['currency_id'] = $currency->id;
									$data['currency_code'] = $currency->code;
									$data['currency_symbol'] = $currency->symbol;

									//Calculate ppv expiry time
									$start_date = Date('Y-m-d H:i:s');
									$time = strtotime($start_date);
									$expiry = $timeframe_days . ' ' . "days";
									$end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));

									$data['movie_id'] = 0;
									$data['season_id'] = 0;
									$data['episode_id'] = 0;

									if (intval($is_subscribed_user)) {
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
											$data['amount'] = $price['show_subscribed'];
										} else {
											$data['amount'] = $price['price_for_subscribed'];
										}
									} else {
										if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
											$data['amount'] = $price['show_unsubscribed'];
										} else {
											$data['amount'] = $price['price_for_unsubscribed'];
										}
									}

									$couponCode = '';
									//Calculate coupon if exists
									if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
										$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $this->studio_id, $user_id, $data['currency_id']);
										$data['amount'] = $getCoup["amount"];
										$couponCode = $getCoup["couponCode"];
									}

									$gateway_code = '';

									if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
										$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $this->studio_id, 'user_id' => $user_id));
										$data['card_id'] = @$card->id;
										$data['token'] = @$card->token;
										$data['profile_id'] = @$card->profile_id;
										$data['card_holder_name'] = @$card->card_holder_name;
										$data['card_type'] = @$card->card_type;
										$data['exp_month'] = @$card->exp_month;
										$data['exp_year'] = @$card->exp_year;

										$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
										$gateway_code = $gateway_info->short_code;
									} else {
										$gateway_info = $gateway['gateways'];
										$gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
									}

									$this->setPaymentGatwayVariable($gateway_info);
									if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypal') {
										$VideoName = $plan->title;
										$VideoDetails = $VideoName = trim($VideoName);

										if (abs($data['amount']) < 0.01) {
											if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
												$data['gateway_code'] = $gateway_code;
												$crd = Yii::app()->billing->setCardInfo($data);
												$data['card_id'] = $crd;
											}

											$data['amount'] = 0;
											$ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 2, $gateway_code);
											$apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, 2, $VideoName, $VideoDetails);

											$this->code = 200;
										} else {
											$data['gateway_code'] = $gateway_code;
											$data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
											$data['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
											$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
											Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
											$payment_gateway = new $payment_gateway_controller();
											$trans_data = $payment_gateway::processTransactions($data);

											$is_paid = $trans_data['is_success'];
											if (intval($is_paid)) {
												$data['amount'] = $trans_data['amount'];
												if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
													$crd = Yii::app()->billing->setCardInfo($data);
													$data['card_id'] = $crd;
												}

												$ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan, $data, $Films->id, $start_date, $end_date, $data['coupon_code'], 0, $gateway_code);

												$transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $Films->id, $trans_data, $ppv_subscription_id, $gateway_code, 0);
												$apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, 0, $VideoName, $VideoDetails);

												$this->code = 200;
											} else {
												$this->code = 661;
											}
										}
									} else {
										$this->code = 737;
									}
								} else {//Content is not in Pay-per view bundle plan
									$this->code = 725;
								}
							} else {//No content exists
								$this->code = 682;
							}
						} else {//Pay-per view bundle is not enabled
							$this->code = 669;
						}
					} else {//No gateway found
						$this->code = 612;
					}
				} else {
					$this->code = 660;
				}
			} else {
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}
	}

	/**
	 * @method public getVoucherPlan: It returns plan detail about a content
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: Movie unique code
	 * @param String stream_id: stream unique code
	 * @param Int season: Season id
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionGetVoucherPlan() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			if ($user_id && $movie_code) {
				$season_id = trim(@$_REQUEST['season']);
				$stream_code = trim(@$_REQUEST['stream_id']);
				$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
				if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
					$cond = '';
					$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
					if ($stream_code) {
						$cond = " AND m.embed_id=:embed_id";
						$condArr[":embed_id"] = $stream_code;
					}
					if ($season_id) {
						$cond .= " AND m.series_number=:series_number";
						$condArr[":series_number"] = $season_id;
					}
					$command = Yii::app()->db->createCommand()
							->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
							->from('films f, movie_streams m')
							->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
					$films = $command->queryRow();
					if (!empty($films)) {
						$search_str = $movie_id = $films['id'];
						$content_types_id = $films['content_types_id'];

						if (intval($content_types_id) == 3) {
							$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
							$data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
							$data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
							$data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

							$EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);

							if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
								$series = explode(',', $EpDetails->series_number);
								sort($series);
								$data['max_season'] = $series[count($series) - 1];
								$data['min_season'] = $series[0];
								$data['series_number'] = $series_number = intval($season_id) ? $season_id : $series[0];
								$episql = "SELECT * FROM movie_streams WHERE studio_id=" . $this->studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
								$episodes = Yii::app()->db->createCommand($episql)->queryAll();
								$ep_data = array();
								if (isset($episodes) && !empty($episodes)) {
									for ($i = 0; $i < count($episodes); $i++) {
										$ep_data[$i]['id'] = $episodes[$i]['id'];
										$ep_data[$i]['movie_id'] = $episodes[$i]['movie_id'];
										$ep_data[$i]['episode_title'] = $episodes[$i]['episode_title'];
										$ep_data[$i]['embed_id'] = $episodes[$i]['embed_id'];
									}
								}
								$data['episode'] = $ep_data;
							}

							$search_str .= (intval($series_number)) ? ":" . $series_number : ":0";
							$search_str .= (intval($films['stream_id'])) ? ":" . $films['stream_id'] : ":0";
						}

						$content = "'" . $search_str . "'";
						$voucher_exist = Yii::app()->common->isVoucherExists($this->studio_id, $content);
						if ($voucher_exist) {
							$this->code = 200;
						} else {
							$data = array();
							$this->code = 727; //Voucher doesn't exist for this content
						}
					} else {//No content exists
						$this->code = 682;
					}
				} else {//Voucher is not enabled
					$this->code = 726;
				}
			} else {//Either user id or movie id is missing
				$this->code = 700;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	 * @method public validateVoucher It validates the voucher code
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String movie_id*: Movie unique code
	 * @param String voucher_code*: Voucher code
	 * @param String stream_id: stream unique code
	 * @param Int season: Season id
	 * @param String purchase_type: show/episode for multipart only
	 * @param String lang_code: Language code
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionValidateVoucher() {
		$data = array();
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			$voucher_code = trim(@$_REQUEST['voucher_code']);
			if ($user_id && $movie_code && $voucher_code) {
				$season_id = trim(@$_REQUEST['season']);
				$stream_code = trim(@$_REQUEST['stream_id']);
				$purchase_type = trim(@$_REQUEST['purchase_type']);
				$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
				if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
					$cond = '';
					$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
					if ($stream_code) {
						$cond = " AND m.embed_id=:embed_id";
						$condArr[":embed_id"] = $stream_code;
					}
					if ($season_id) {
						$cond .= " AND m.series_number=:series_number";
						$condArr[":series_number"] = $season_id;
					}
					$command = Yii::app()->db->createCommand()
							->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
							->from('films f, movie_streams m')
							->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
					$films = $command->queryRow();
					if (!empty($films)) {
						$content_id = $films['id'];
						$content_types_id = $films['content_types_id'];
						if ($content_types_id == 3) {
							if ($purchase_type == 'season') {
								$content_id .= ":" . $season_id;
							} else if ($purchase_type == 'episode') {
								$content_id .= (intval($season_id)) ? ":" . $season_id : ":0";
								$content_id .= ":" . $films['stream_id'];
							}
						}

						$voucherDetails = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $this->studio_id);
						switch ($voucherDetails) {
							case 1:
								$this->code = 200;
							case 2:
								$this->code = 729;
								$this->msg_code = 'invalid_voucher';
							case 3:
								$this->code = 730;
								$this->msg_code = 'voucher_already_used';
							case 4:
								$this->code = 200;
								$this->msg_code = 'free_content';
							case 5:
								$this->code = 200;
								$this->msg_code = 'already_purchase_this_content';
							case 6:
								$this->code = 656;
								$this->msg_code = 'access_period_expired';
							case 7:
								$this->code = 657;
								$this->msg_code = 'watch_period_expired';
							case 8:
								$this->code = 658;
								$this->msg_code = 'crossed_max_limit_of_watching';
						}
					} else {//No content exists
						$this->code = 682;
					}
				} else {//Voucher is not enabled
					$this->code = 726;
				}
			} else {//Either user id or movie id or voucher code is missing
				$this->code = 728;
			}
		} else {
			$this->code = 662;
		}

		$this->items = $data;
	}

	/**
	* @method public voucherSubscription: It will purchase a subscription using voucher
	* @return json Return the json array of results
	* @param String authToken*: Auth token of the studio
	* @param Int user_id*: User id
	* @param String movie_id*: Movie unique code
	* @param String voucher_code*: Voucher code
	* @param String stream_id: stream unique code
	* @param Int season: Season id
	* @param String purchase_type: show/episode for multipart only
	* @param String lang_code: Language code
	* @author Ashis<ashis@muvi.com>
	*/
    public function actionVoucherSubscription() {
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$user_id = trim(@$_REQUEST['user_id']);
			$movie_code = trim(@$_REQUEST['movie_id']);
			$voucher_code = trim(@$_REQUEST['voucher_code']);
			if ($user_id && $movie_code && $voucher_code) {
				$monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
				if(isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 128)) {
					$season_id = trim(@$_REQUEST['season']);
					$stream_code = trim(@$_REQUEST['stream_id']);
					$purchase_type = trim(@$_REQUEST['purchase_type']);
					$is_preorder = (isset($_REQUEST['is_preorder'])) ? intval($_REQUEST['is_preorder']) : 0;
					
					$cond = '';
					$condArr = array(':uniq_id' => $movie_code, ':studio_id' => $this->studio_id);
					if ($stream_code) {
						$cond = " AND m.embed_id=:embed_id";
						$condArr[":embed_id"] = $stream_code;
					}
					if ($season_id) {
						$cond .= " AND m.series_number=:series_number";
						$condArr[":series_number"] = $season_id;
					}
					$command = Yii::app()->db->createCommand()
									->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
									->from('films f, movie_streams m')
									->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
					$films = $command->queryRow();
					if (!empty($films)) {
						$content_id = $films['id'];
						$content_types_id = $films['content_types_id'];
						if ($content_types_id == 3) {
							if ($purchase_type == 'season') {
								$content_id .= ":" . $season_id;
							} else if ($purchase_type == 'episode') {
								$content_id .= (intval($season_id)) ? ":" . $season_id : ":0";
								$content_id .= ":" . $films['stream_id'];
							}
						}
						$getVoucher = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $this->studio_id);
						$is_subscribe = 0;
						switch ($getVoucher) {
							case 1:
								$this->code = 200;
								$is_subscribe = 1;
							case 2:
								$this->code = 729;
								$this->msg_code = 'invalid_voucher';
							case 3:
								$this->code = 730;
								$this->msg_code = 'voucher_already_used';
							case 4:
								$this->code = 200;
								$this->msg_code = 'free_content';
							case 5:
								$this->code = 200;
								$this->msg_code = 'already_purchase_this_content';
							case 6:
								$this->code = 656;
								$this->msg_code = 'access_period_expired';
							case 7:
								$this->code = 657;
								$this->msg_code = 'watch_period_expired';
							case 8:
								$this->code = 658;
								$this->msg_code = 'crossed_max_limit_of_watching';
						}
						if ($is_subscribe) {
							$sql = "SELECT id FROM voucher WHERE studio_id={$this->studio_id} AND voucher_code='" . $voucher_code . "'";
							$voucher = Yii::app()->db->createCommand($sql)->queryRow();

							$start_date = Date('Y-m-d H:i:s');
							$end_date = '';
							if (intval($is_preorder) == 0) {
								$limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'voucher', 'limit_video');
								$limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

								$watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'voucher', 'watch_period');
								$watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

								$access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($this->studio_id, 'voucher', 'access_period');
								$access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

								$time = strtotime($start_date);
								if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
									$end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
								}

								$data['view_restriction'] = $limit_video;
								$data['watch_period'] = $watch_period;
								$data['access_period'] = $access_period;
							}
							
							$data['content_types_id'] = $content_types_id;
							$data['voucher_id'] = $voucher['id'];
							$data['content_id'] = $content_id;
							$data['user_id'] = $user_id;
							$data['studio_id'] = $this->studio_id;
							$data['isadv'] = $is_preorder;
							$data['start_date'] = @$start_date;
							$data['end_date'] = @$end_date;
							
							$res = Yii::app()->billing->setVoucherSubscription($data);
						}
					} else { //No content exists
						$this->code = 682;
					}
				} else {
					$this->code = 726;
				}
			} else {//Either user id or movie id or voucher code is missing
				$this->code = 728;
			}
		} else {
			$this->code = 662;
		}
    }

	/**
	 * @method public purchaseMuvikart: It will process the payment for physical goods
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*: User id
	 * @param String existing_card_id*: Card uniq id. This is required when user is trying to payment through existing card
	 * @param String profile_id*: User Profile id created in the payment gateways
	 * @param Float amount*: Amount of item
	 * @param Json string cart_item*: Cart item
	 * @param Json string ship*: shipping detail
	 * @param Json string card*: Card detail
	 * @param String card_last_fourdigit: Last 4 digits of the card 
	 * @param Boolean is_save_this_card: 1: true, 0: false
	 * @param String token: Payment Token
	 * @param String card_type: Type of the card
	 * @param String email: User email
	 * @param int currency_id: Currency id
	 * @param String coupon_code: Coupon code
	 * @author Sanjeev<sanjeev@muvi.com>
	 */
	public function actionPurchaseMuvikart() {
		if (isset($_REQUEST) && !empty($_REQUEST)) {
			$data = array();
			$data['profile_id'] = @$_REQUEST['profile_id'];
			$data['card_type'] = @$_REQUEST['card_type'];
			$data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
			$data['token'] = @$_REQUEST['token'];
			$data['email'] = @$_REQUEST['email'];
			$data['currency_id'] = @$_REQUEST['currency_id'];
			$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
			$data['existing_card_id'] = @$_REQUEST['existing_card_id'];
			$data['coupon_code'] = @$_REQUEST['coupon_code'];
			$data['amount'] = @$_REQUEST['amount'];
			$data['dollar_amount'] = @$_REQUEST['amount'];
			$data['studio_id'] = $studio_id = $this->studio_id;
			$data['user_id'] = $user_id = $_REQUEST['user_id'];
			$_REQUEST["cart_item"] = json_decode($_REQUEST["cart_item"], true);
			$_REQUEST["ship"] = json_decode($_REQUEST["ship"], true);
			$data['cart_item'] = $_REQUEST["cart_item"];
			$data['ship'] = $_REQUEST["ship"];
			$gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
			if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
				$this->setPaymentGatwayVariable($gateway_info);

				if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
					$card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
					$data['card_id'] = @$card->id;
					$data['token'] = @$card->token;
					$data['profile_id'] = @$card->profile_id;
					$data['card_holder_name'] = @$card->card_holder_name;
					$data['card_type'] = @$card->card_type;
					$data['exp_month'] = @$card->exp_month;
					$data['exp_year'] = @$card->exp_year;
				}

				$couponCode = '';
				//Calculate coupon if exists
				if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
					$getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
					$data['amount'] = $getCoup["amount"];
					$couponCode = $getCoup["couponCode"];
					$data['coupon_code'] = $couponCode;
					$data['discount_type'] = 0; //$getCoup["discount_type"];
					$data['discount'] = $getCoup["coupon_amount"];
				}


				if ($data['amount'] > 0) {
					$currency = Currency::model()->findByPk($data['currency_id']);
					$data['currency_id'] = $currency->id;
					$data['currency_code'] = $currency->code;
					$data['currency_symbol'] = $currency->symbol;
					$timeparts = explode(" ", microtime());
					$currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
					$reference_number = explode('.', $currenttime);
					$data['order_reference_number'] = $reference_number[0];
					$payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
					Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
					$payment_gateway = new $payment_gateway_controller();
					$data['gateway_code'] = $gateway_info[0]->short_code;
					$data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
					$trans_data = $payment_gateway::processTransactions($data); //charge the card
					if (intval($trans_data['is_success'])) {
						if (intval($data['is_save_this_card'] == 1)) {
							$sciModel = New SdkCardInfos;
							$card = $data;
							$card['card_last_fourdigit'] = $res['card']['card_last_fourdigit'];
							$card['token'] = $res['card']['token'];
							$card['card_type'] = $res['card']['card_type'];
							$card['auth_num'] = $res['card']['auth_num'];
							$card['profile_id'] = $res['card']['profile_id'];
							$card['reference_no'] = $res['card']['reference_no'];
							$card['response_text'] = $res['card']['response_text'];
							$card['status'] = $res['card']['status'];
							$sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
						}
						//Save a transaction detail
						$transaction = new Transaction;
						$transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
						$data['transactions_id'] = $transaction_id;
						/* Save to order table */
						$data['hear_source'] = $_REQUEST['hear_source'];
						$pgorder = new PGOrder();
						$orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
						/* Save to shipping address */
						$pgshippingaddr = new PGShippingAddress();
						$pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
						/* Save to Order Details */
						$pgorderdetails = new PGOrderDetails();
						$pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


						//send email
						$req['orderid'] = $orderid;
						$req['emailtype'] = 'orderconfirm';
						$req['studio_id'] = $studio_id;
						$req['studio_name'] = @$data['studio_name'];
						$req['currency_id'] = $data['currency_id'];
						// Send email to user
						Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
						$isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
						if ($isEmailToStudio) {
							Yii::app()->email->pgEmailTriggers($req);
						}
						//create order in cds
						$webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
						if ($webservice) {
							if ($webservice->inventory_type == 'CDS') {
								$pgorder = new PGOrder();
								$pgorder->CreateCDSOrder($studio_id, $orderid);
								//PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
							}
						}
						$this->code = 200;
					} else {
						$this->code = 614;
						$this->msg_code = 'error_transc_process';
					}
				} else {
					$isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
					if ($isData) {
						$this->code = 200;
					}
				}
			} else {
				$this->code = 612;
			}
		} else {
			$this->code = 662;
		}
	}

	public function physicalDataInsert($datap, $pay, $trans_data, $ip, $short_code) {
		//Save a transaction detail
		$datap['transactions_id'] = $transaction_id = Transaction::model()->insertTrasactions($this->studio_id, $pay['user_id'], $datap['currency_id'], $trans_data, 4, $ip, $short_code);
		/* Save to order table */
		$datap['hear_source'] = $pay['hear_source'];
		$pgorder = new PGOrder();
		$orderid = $pgorder->insertOrder($this->studio_id, $pay['user_id'], $datap, $datap["cart_item"], $datap['ship'], $ip);
		/* Save to shipping address */
		$pgshippingaddr = new PGShippingAddress();
		$pgshippingaddr->insertAddress($this->studio_id, $pay['user_id'], $orderid, $datap['ship'], $ip);
		/* Save to Order Details */
		$pgorderdetails = new PGOrderDetails();
		$pgorderdetails->insertOrderDetails($orderid, $datap["cart_item"]);
		if ($pay['card_options'] != '') {
			$data = array('isSuccess' => 1);
			$data = json_encode($data);
		}
		//send email
		$req['orderid'] = $orderid;
		$req['emailtype'] = 'orderconfirm';
		$req['studio_id'] = $this->studio_id;
		$req['studio_name'] = $datap['studio_name'];
		$req['currency_id'] = $datap['currency_id'];
		// Send email to user
		Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
		$isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio_id);
		if ($isEmailToStudio) {
			Yii::app()->email->pgEmailTriggers($req);
		}
		if ($transaction_id) {
			$command = Yii::app()->db->createCommand();
			$command->delete('pg_cart', 'user_id=:userid AND studio_id:studio_id', array(':userid' => $pay['user_id'], ':studio_id' => $this->studio_id));
		}
		//create order in cds
		$webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
		if ($webservice) {
			if ($webservice->inventory_type == 'CDS') {
				$pgorder->CreateCDSOrder($this->studio_id, $orderid);
				//PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
			}
		}

		return true;
	}
    
    /**
     * @method public GetUserCredits: Get the list of credits based on user id
	 * @param String authToken*: Auth token of the studio
     * @param Int user_id*
     * @param Int offset
     * @param String lang_code: Language code
     * @author Sanjeev Kumar Malla<sanjeev@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionGetUserCredits() {
		$data = array();
		$studio_id = $this->studio_id;
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $studio_id);
		if (isset($_REQUEST['user_id']) && trim($_REQUEST['user_id'])) {
			$user_id = $_REQUEST['user_id'];
			$total_credit = Yii::app()->billing->getUserTotalCredit($user_id);
			$page_size = 10;
			$offset = 0;
			if (isset($_REQUEST['offset']) && intval($_REQUEST['offset'])) {
				$offset = ($_REQUEST['offset'] - 1) * $page_size;
			}
			$cdata = Yii::app()->billing->getAllUserCredits($user_id, $offset, $page_size);
			$user_all_credits = $cdata['data'];
			for ($i = 0; $i < count($user_all_credits); $i++) {
				if (intval($user_all_credits[$i]['rule_action_type']) == 1) {
					$user_all_credits[$i]['action_type'] = 'Subscrition';
				} else if (intval($user_all_credits[$i]['rule_action_type']) == 2) {
					$user_all_credits[$i]['action_type'] = 'Subscrition Renewal';
				}
			}
			$this->code = 200;
			$data['total_credit'] = $total_credit ? $total_credit : 0;
			$data['credit_count'] = $cdata['count'];
			$data['credit_list'] = $user_all_credits;
		} else {
			$this->code = 639;
		}

		$this->items = $data;
	}

	/**
	 * @method public getUserDebits: Get the list of credits based on user id
	 * @param String authToken*: Auth token of the studio
	 * @param Int user_id*
	 * @param Int offset
	 * @param String lang_code: Language code
	 * @author Sanjeev Kumar Malla<sanjeev@muvi.com>
	 * @return json Returns the list of data in json format success or corrospending error code
	 */
	public function actionGetUserDebits() {
		$res = array();
		$studio_id = $this->studio_id;
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $studio_id);
		if (isset($_REQUEST['user_id']) && trim($_REQUEST['user_id'])) {
			$user_id = $_REQUEST['user_id'];
			$page_size = 10;
			$offset = 0;
			if (isset($_REQUEST['offset']) && intval($_REQUEST['offset'])) {
				$offset = ($_REQUEST['offset'] - 1) * $page_size;
			}
			$data = Yii::app()->billing->getAllUserDebits($user_id, $offset, $page_size);
			$user_all_debits = $data['data'];
			$debit_count = $data['count'];

			for ($i = 0; $i < count($user_all_debits); $i++) {
				if (intval($user_all_debits[$i]['content_type']) == 1) {
					$user_all_debits[$i]['content_name'] = Film::model()->findByPk($user_all_debits[$i]['content'])->name;
				} else if (intval($user_all_debits[$i]['content_type']) == 3) {
					$temp_id_str = $user_all_debits[$i]['content'];
					$content_arr = explode(':', $temp_id_str);
					if (count($content_arr) == 2) {
						$user_all_debits[$i]['content_name'] = Film::model()->findByPk($content_arr[0])->name . " " . $this->Language['season'] . " " . $content_arr[1];
					} else if (count($content_arr) == 3) {
						$user_all_debits[$i]['content_name'] = Film::model()->findByPk($content_arr[0])->name . " " . $this->Language['season'] . " " . $content_arr[1] . " " . movieStreams::model()->findByPk($content_arr[2])->episode_title;
					}
				}
				if (intval($user_all_debits[$i]['transaction_type']) == 1) {
					$user_all_debits[$i]['transaction_type_name'] = $translate['Pay_Per_View'];
				} else {
					$user_all_debits[$i]['transaction_type_name'] = $translate['pre_order'];
				}
			}
			$this->code = 200;
			$res['debit_count'] = $debit_count;
			$res['debit_list'] = $user_all_debits;
		} else {
			$this->code = 639;
		}
		$this->items = $res;
	}
	
	public function actionSubscriptionBundlesPayment() {
		$res = array();
        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
			$studio_id = $this->studio_id;
			$user_id = $_REQUEST['user_id'];
			$lang_code = @$_REQUEST['lang_code'];

			$data = array();
			$data['studio_id'] = $studio_id;
			$data['user_id'] = $user_id;
			$data['movie_id'] = @$_REQUEST['movie_id'];
			$data['season_id'] = @$_REQUEST['season_id'];
			$data['episode_id'] = @$_REQUEST['episode_id'];
			$data['coupon_code'] = @$_REQUEST['coupon_code'];
			$data['is_advance'] = @$_REQUEST['is_advance'];
			$data['currency_id'] = @$_REQUEST['currency_id'];
			$data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
			$data['plandetailbundles_id'] = @$_REQUEST['plan_id'];
			$data['is_subscriptionbundle'] = @$_REQUEST['is_subscriptionbundle'];

            $user = SdkUser::model()->findByAttributes(array('id' => $user_id, 'studio_id' => $studio_id, 'status' => 1));
            if (!empty($user)) {
                $gateway_info = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($gateway_info['gateways']) && !empty($gateway_info['gateways'])) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($gateway_info['gateways']);

                    $monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
                    if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1)) {
                        if ((isset($data['plandetailbundles_id']) && intval($data['plandetailbundles_id']))) {
                            $plandetailbundles_id = $data['plandetailbundles_id'];
                            $plan_subscriptionbundles = SubscriptionPlans::model()->findByAttributes(array('id' => $plandetailbundles_id, 'studio_id' => $studio_id, 'status' => 1));
                            if (!empty($plan_subscriptionbundles)) {

                                $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1, 'plan_id' => $plandetailbundles_id));
                                if (empty($usersub)) {
                                    $is_valid_card_information = 0;
                                    if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
                                        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
                                        $is_valid_card_information = 1;
                                    } else {
                                        $data['card_holder_name'] = @$_REQUEST['card_name'];
                                        $data['card_number'] = @$_REQUEST['card_number'];
                                        $data['exp_month'] = @$_REQUEST['exp_month'];
                                        $data['exp_year'] = @$_REQUEST['exp_year'];
                                        $data['cvv'] = @$_REQUEST['cvv'];
                                        $data['profile_id'] = @$_REQUEST['profile_id'];
                                        $data['token'] = @$_REQUEST['token'];
                                        $data['card_type'] = @$_REQUEST['card_type'];
                                        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
                                        $data['email'] = @$_REQUEST['email'];
                                        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];

                                        if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
                                            $is_valid_card_information = 1;
                                        }
                                    }

                                    if ($is_valid_card_information) {
                                        $translate = $this->getTransData($lang_code, $studio_id);

                                        $VideoDetails = '';
                                        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
                                        $video_id = $Films->id;
                                        $isadv = '';
                                        $end_date = '';

                                        $pricebundles = Yii::app()->common->getSubscriptionBundlesPrice($plandetailbundles_id, $data['currency_id'], $studio_id);
                                        $currency = Currency::model()->findByPk($pricebundles['currency_id']);
                                        $data['currency_id'] = $currency->id;
                                        $data['currency_code'] = $currency->code;
                                        $data['currency_symbol'] = $currency->symbol;
                                        //Calculate ppv expiry time only for single part or episode only
                                        $start_date = Date('Y-m-d H:i:s');
                                        $is_transaction = 0;
                                        if (isset($plan_subscriptionbundles->trial_period) && intval($plan_subscriptionbundles->trial_period) == 0) {
                                            $is_transaction = 1;
                                            $start_date = Date('Y-m-d H:i:s');
                                            $time = strtotime($start_date);
                                            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                                            $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                            $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                                        } else {
                                            $trial_period = $plan_subscriptionbundles->trial_period . ' ' . strtolower($plan_subscriptionbundles->trial_recurrence) . "s";
                                            $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                                            $time = strtotime($start_date);
                                            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                                            $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                        }
                                        $data['season_id'] = 0;
                                        $data['episode_id'] = 0;
                                        $data['amount'] = $amount = $pricebundles['price'];
                                        if (isset($_REQUEST['coupon_code']) && trim($_REQUEST['coupon_code'])) {
                                            $datasubscriptionBundles = array();
                                            $datasubscriptionBundles['studio_id'] = Yii::app()->common->getStudiosId();
                                            $datasubscriptionBundles['coupon'] = $_REQUEST['coupon_code'];
                                            $datasubscriptionBundles['plan'] = $data['plandetailbundles_id'];
                                            $datasubscriptionBundles['user_id'] = $user_id;
                                            //getting the subscription bundles plan details
                                            $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($data['plandetailbundles_id'], $default_currency_id, $studio_id);
                                            $currency_id = (isset($planDetails[0]['currency_id']) && trim($planDetails[0]['currency_id'])) ? $planDetails[0]['currency_id'] : $default_currency_id;
                                            $currency = Currency::model()->findByPk($currency_id);
                                            $planDetails['currency_id'] = $datasubscriptionBundles['currency'] = $currency_id;
                                            $planDetails['couponCode'] = $_POST["data"]["coupon"];
                                            $planDetails['physical'] = $_POST["physical"];
                                            $couponDetails = Yii::app()->billing->isValidCouponForSubscription($datasubscriptionBundles);
                                            if ($couponDetails != 0) {
                                                $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails, $planDetails);
                                            } else {
                                                $res['isError'] = 1;
                                            }
                                            $data['amount'] = $amount = $res['discount_amount'];
                                        }

                                        $data['card_holder_name'] = @$data['card_name'];

                                        if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                                            $card = SdkCardInfos::model()->findByAttributes(array('id' => $data['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                                            $data['card_id'] = @$card->id;
                                            $data['token'] = @$card->token;
                                            $data['profile_id'] = @$card->profile_id;
                                            $data['card_holder_name'] = @$card->card_holder_name;
                                            $data['card_type'] = @$card->card_type;
                                            $data['exp_month'] = @$card->exp_month;
                                            $data['exp_year'] = @$card->exp_year;
                                            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                                            $gateway_code = $gateway_info->short_code;
                                        } else {
                                            $data['save_this_card'] = $data['is_save_this_card'];
                                        }

                                        if (trim($gateway_code)) {
                                            if (intval($data['is_subscriptionbundle'])) {
                                                $VideoName = $plan_subscriptionbundles->name;
                                            }
                                            if (abs($data['amount']) < 0.01) {
                                                if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                                    $data['gateway_code'] = $gateway_code;
                                                    $crd = Yii::app()->billing->setCardInfo($data);
                                                    $data['card_id'] = $crd;
                                                }
                                                $data['amount'] = 0;
                                                $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                                                $ppv_subscription_id = $set_user_subscription_bundles;
                                                if ($is_transaction) {
                                                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                                                }
                                                $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                                                $res['code'] = 200;
                                                $res['status'] = "OK";
                                            } else {
                                                $data['studio_id'] = $studio_id;
                                                $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

                                                $data['user_id'] = $user_id;
                                                $data['studio_name'] = Studio::model()->findByPk($studio_id)->name;
                                                $payment_gateway_controller = 'Api' . $gateway_code . 'Controller';
                                                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                                                $payment_gateway = new $payment_gateway_controller();
                                                $data['gateway_code'] = $gateway_code;
                                                if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                                                    $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
                                                }

                                                $trans_data = $payment_gateway::processTransactions($data);
                                                $is_paid = $trans_data['is_success'];
                                                if (intval($is_paid)) {
                                                    $data['amount'] = $trans_data['amount'];
                                                    if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                                        $crd = Yii::app()->billing->setCardInfo($data);
                                                        $data['card_id'] = $crd;
                                                    }
                                                    $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                                                    $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                                                    $ppv_subscription_id = $set_user_subscription_bundles;
                                                    if ($is_transaction) {
                                                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                                                    }
                                                    $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                                                    $res['code'] = 200;
                                                    $res['status'] = "OK";
                                                } else {
                                                    $res['code'] = 406;
                                                    $res['status'] = "Error";
                                                    $res['msg'] = "Error in transaction";
                                                    $res['response_text'] = $trans_data['response_text'];
                                                }
                                            }
                                        } else {
                                            $res['code'] = 406;
                                            $res['status'] = 'error';
                                            $res['msg'] = "Internal error occures.";
                                        }
                                    } else {
                                        $res['code'] = 406;
                                        $res['status'] = 'error';
                                        $res['msg'] = "Invalid Card details entered. Please check once again";
                                    }
                                } else {
                                    $res['code'] = 406;
                                    $res['status'] = 'error';
                                    $res['msg'] = "You have already purchased this subscription.";
                                }
                            } else {
                                $res['code'] = 406;
                                $res['status'] = 'error';
                                $res['msg'] = "Subscription plan doesn't exist";
                            }
                        } else {
                            $res['code'] = 406;
                            $res['status'] = 'error';
                            $res['msg'] = "Invalid subscription plan detail";
                        }
                    } else {//Subscription plan is disabled
                        $res['code'] = 406;
                        $res['status'] = 'error';
                        $res['msg'] = "Subscription plan is not enabled.";
                    }
                } else {//No gateway found
                    $res['code'] = 406;
                    $res['status'] = 'error';
                    $res['msg'] = "No payment gateway is found.";
                }
            } else {
                $res['code'] = 406;
                $res['status'] = 'error';
                $res['msg'] = "User doesn't belong to this store.";
            }
        } else {
            $res['code'] = 406;
            $res['status'] = 'error';
            $res['msg'] = "Invalid user detail.";
			$this->code = 639;
        }
		$this->items = $res;
    }

    /**
     * @method private myplans() plan listing those have user taken
     * @author sunil Nayak<suniln@muvi.com>
     * @return json Returns the list of data in json format
     * @param string user_id (*)
     * @param string authToken Auth token(*)
     * @param int currency_id (optional)
     */
    public function actionMyPlans() {
        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            $bundlesplans_cancelled = array();
            $bundlesplans = array();
            $default_currency_id = (isset($_REQUEST['currency_id']) && trim($_REQUEST['currency_id'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($this->studio_id)->default_currency_id;
            $bundleplan_payment_gateway = Yii::app()->common->isPaymentGatwayAndBundlesPlanExists($studio_id, $user_id, $default_currency_id);
            if (isset($bundleplan_payment_gateway) && !empty($bundleplan_payment_gateway)) {
                $gateways = $bundleplan_payment_gateway['gateways'];
                $bundlesplans = $bundleplan_payment_gateway['plans'];
            }

            $bundleplan_payment_gateway_cancelled = Yii::app()->common->isPaymentGatwayAndBundlesPlanExistscancelled($studio_id, $user_id, $default_currency_id);
            if (isset($bundleplan_payment_gateway_cancelled) && !empty($bundleplan_payment_gateway_cancelled)) {
                $gateways = $bundleplan_payment_gateway_cancelled['gateways'];
                $bundlesplans_cancelled = $bundleplan_payment_gateway_cancelled['plans'];
                $bundlesplans_cancelled[0]['status'] = 'cancelled';
            }

            $plans = array_merge($bundlesplans_cancelled, $bundlesplans);
            if (isset($plans) && !empty($plans)) {
                $data['code'] = 200;
                $data['Plan'] = $plans;
                $data['msg'] = "Ok";
            } else {
                $data['code'] = 406;
                $data['status'] = 'Error';
                $data['msg'] = "No Record Found!!";
            }
        } else {
            $data['code'] = 407;
            $data['status'] = 'Error';
            $data['msg'] = "Invalid user id";
        }

        echo json_encode($data);
        exit;
    }

    /**
     * @method private CancelsubscriptionPlan() Cancel the subscription plan those have user taken
     * @author sunil Nayak<suniln@muvi.com>
     * @return json Returns the list of data in json format
     * @param string cancel_note (*)
     * @param string authToken Auth token(*)
     * @param string user_id (*)
     * @param string cancelreason (*)
     * @param int is_admin (optional)
     */
    public function actionCancelsubscriptionPlan() {
        $res = array();
        $card = array();
        if (isset($_REQUEST['cancel_note']) && trim($_REQUEST['cancel_note'])) {
            $studio_id = $this->studio_id;
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = $_REQUEST['user_id'];
                //Getting user subscription detail to find out plan, card detail
                $usersub = new UserSubscription;
                $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1, 'plan_id' => $_REQUEST['plan_id']));
                if (!empty($usersub)) {

                    $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
                    $payment_gateway_type = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    //Getting plan detail
                    $plan = new SubscriptionPlans;
                    $plan = $plan->findByPk($usersub->plan_id);
                    //Getting card detail
                    $card = new SdkCardInfos;
                    $card = $card->findByPk($usersub->card_id);
                    //Deduct outstanding due if plan is post paid and sent invoice detail to user
                    $is_paid = 1;
                    $file_name = '';
                    if (isset($plan) && intval($plan->is_post_paid)) {
                        //If trial period expires
                        $start_date = date("Y-m-d", strtotime($usersub->start_date));
                        $today_date = gmdate('Y-m-d');
                        if (strtotime($start_date) < strtotime($today_date)) {
                            $trans_data = $this->processTransaction($card, $usersub);
                            $is_paid = $trans_data['is_success'];
                            //Save a transaction detail
                            if (intval($is_paid)) {
                                //Getting Ip address
                                $ip_address = Yii::app()->request->getUserHostAddress();
                                $transaction = new Transaction;
                                $transaction->user_id = $user_id;
                                $transaction->studio_id = $studio_id;
                                $transaction->plan_id = $usersub->plan_id;
                                $transaction->transaction_date = new CDbExpression("NOW()");
                                $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                                $transaction->transaction_status = $trans_data['transaction_status'];
                                $transaction->invoice_id = $trans_data['invoice_id'];
                                $transaction->order_number = $trans_data['order_number'];
                                $transaction->amount = $trans_data['amount'];
                                $transaction->response_text = $trans_data['response_text'];
                                $transaction->subscription_id = $usersub->id;
                                $transaction->ip = $ip_address;
                                $transaction->created_by = $user_id;
                                $transaction->created_date = new CDbExpression("NOW()");
                                $transaction->save();
                            }
                        }
                    }
                    if (intval($is_paid)) {
                        $success = 1;
                        //Cancel customer's account from payment gateway
                        $cancel_acnt = $this->cancelCustomerAccount($usersub, $card);
                        //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r($cancel_acnt, true));
                        if (isset($payment_gateway_type) && ($payment_gateway_type == 'paypal' || $payment_gateway_type == 'paypalpro') && ($usersub->card_id == '' || !$usersub->card_id)) {
                            if (isset($cancel_acnt) && $cancel_acnt['ACK'] == 'Success') {
                                $success = 1;
                            } else {
                                $success = 0;
                            }
                        } else {
                            //Delete card detail of user
                            if (trim($usersub->profile_id)) {
                                SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id AND profile_id = :profile_id', array(
                                    ':studio_id' => $studio_id,
                                    ':user_id' => $user_id,
                                    ':profile_id' => $usersub->profile_id
                                ));
                            }
                        }
                        if ($success) {
                            //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r('sunccess', true));
                            //Inactivate user subscription
                            $reason_id = isset($_REQUEST['cancelreason']) ? $_REQUEST['cancelreason'] : '';
                            $cancel_note = isset($_REQUEST['cancel_note']) ? $_REQUEST['cancel_note'] : '';
                            $usersub->status = 0;
                            $usersub->cancel_date = new CDbExpression("NOW()");
                            $usersub->cancel_reason_id = $reason_id;
                            $usersub->cancel_note = htmlspecialchars($cancel_note);
                            $usersub->payment_status = 0;
                            $usersub->partial_failed_date = '';
                            $usersub->partial_failed_due = 0;
                            $usersub->canceled_by = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? 0 : $user_id;
                            $usersub->save();
                            //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                            if ($plan->is_subscription_bundle == 0) {
                                $usr = new SdkUser;
                                $usr = $usr->findByPk($user_id);
                                $usr->is_deleted = 1;
                                $usr->deleted_at = new CDbExpression("NOW()");
                                $usr->will_cancel_on = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? '' : $usersub->start_date;
                                $usr->save();
                            }
                            //Send an email to user
                            if (isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin'])) {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation', $file_name, '', '', '', '', '', $lang_code);
                            } else {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation_user', $file_name, '', '', '', '', '', $lang_code);
                            }
                            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_cancellation', $studio_id);
                            if ($isEmailToStudio) {
                                $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled', $user_id, '', 0);
                            }
                            $data['code'] = 200;
                            $data['status'] = "Success";
                            $data['msg'] = "Subscription has been cancelled successfully.";
                        } else {
                            $data['code'] = 406;
                            $data['status'] = "Error";
                            $data['msg'] = "Subscription can not be cancelled. Please contact to store admin";
                        }
                    } else {
                        $data['code'] = 406;
                        $data['status'] = "Error";
                        $data['msg'] = "Transaction can't be processed.Subscription can't be cancelled";
                    }
                } else {
                    $data['code'] = 406;
                    $data['status'] = "Error";
                    $data['msg'] = "User has no subscription plan to cancel or subscription plan is cancelled already";
                }
            } else {
                $data['code'] = 406;
                $data['status'] = "Error";
                $data['msg'] = "Invalid user details";
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "Error";
            $data['msg'] = "Please add cancel note";
        }
        echo json_encode($data);
        exit;
    }

    function cancelCustomerAccount($usersub = Null, $card = Null) {
        $data = '';
        $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
        if (isset($this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::cancelCustomerAccount($usersub, $card);
        }
        return $data;
    }

}

;
