<?php

trait Content {

	/**
	 * @method private getContentList() Get the list of contents based on its content_type permalink
	 * @author GDR<support@muvi.com>VideoLogs

	 * @return json Returns the list of data in json format
	 * @param string $permalink Content type permalink
	 * @param string $oauthToken Auth token
	 * @param string $filter Filter conditions
	 * @param int $limit No. of records to be shown in each listing 
	 * @param int $offset Offset for the limit default 0
	 * 
	 */
	public function actionGetContentList() {
		if ($_REQUEST['permalink']) {
			$language_id = 20;
			if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != 'en') {
				$language_id = Yii::app()->custom->getLanguage_id($_REQUEST['lang_code']);
			}
			$translate = $this->getTransData($lang_code, $this->studio_id);
			$menuItemInfo = MenuItem::model()->find('studio_id=:studio_id AND permalink=:permalink', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink']));
			if ($menuItemInfo) {
				$studio_id = $this->studio_id;
				$page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
				$order = '';
				$cond = ' ';
				$offset = 0;
				if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
					$offset = ($_REQUEST['offset'] - 1) * $limit;
				}
				$pdo_cond_arr = array(':studio_id' => $studio_id, ':content_category_value' => $menuItemInfo->value);

				$_REQUEST['orderby'] = trim($_REQUEST['orderby']);
				if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
					$order = $_REQUEST['orderby'];
					if ($_REQUEST['orderby'] == 'lastupload') {
						$orderby = "  M.last_updated_date DESC";
						$neworderby = " P.last_updated_date DESC ";
					} else if ($_REQUEST['orderby'] == 'releasedate') {
						$orderby = $neworderby = " P.release_date DESC ";
					} else if ($_REQUEST['orderby'] == 'sortasc') {
						$orderby = $neworderby = " P.name ASC ";
					} else if ($_REQUEST['orderby'] == 'sortdesc') {
						$orderby = $neworderby = " P.name DESC ";
					}
				} else {
					$orderby = "  M.last_updated_date DESC ";
					$neworderby = " P.last_updated_date DESC ";
					$orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ', array(':studio_id' => $studio_id, ':category_value' => $menuItemInfo->value));
					if (@$orderData->method) {
						if (@$orderData->orderby_flag == 1) {// most viewed
							$mostviewed = 1;
							$neworderby = " Q.cnt DESC";
						} else if (@$orderData->orderby_flag == 2) {//Alphabetic A-Z
							$orderby = $neworderby = " P.name ASC ";
						} else if (@$orderData->orderby_flag == 3) {//Alphabetic Z-A
							$orderby = $neworderby = " P.name DESC ";
						}
					} else if ($orderData && $orderData->ordered_stream_ids) {
						$neworderby = $orderby = " FIELD(movie_stream_id," . $orderData->ordered_stream_ids . ")";
					}
				}
				if (@$_REQUEST['genre']) {
					if (is_array($_REQUEST['genre'])) {
						$cond .= " AND (";
						foreach ($_REQUEST['genre'] AS $gkey => $gval) {
							if ($gkey) {
								$cond .= " OR ";
							}
							$cond .= " (genre LIKE ('%" . $gval . "%'))";
						}
						$cond .= " ) ";
					} else {
						$cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
					}
				}
				$cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
				if ($_REQUEST['prev_dev']) {
					$command = Yii::app()->db->createCommand()
							->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id,P.uniq_id AS muvi_uniq_id,P.content_type_id, P.ppv_plan_id,P.permalink,P.name,P.content_type_id,M.full_movie,P.story,P.genre,P.release_date,P.content_types_id,M.is_converted, M.full_movie')
							->from('movie_streams M,films P')
							->where('M.movie_id = P.id AND M.studio_id=:studio_id AND (P.content_category_value & :content_category_value) AND is_episode=0 AND P.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
							->order($orderby)
							->limit($limit, $offset);
				} else {
					/* Add Geo block to listing by manas@muvi.com */
					if (isset($_REQUEST['country']) && $_REQUEST['country']) {
						$country = $_REQUEST['country'];
					} else {
						$visitor_loc = Yii::app()->common->getVisitorLocation();
						$country = $visitor_loc['country'];
					}
					$sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date"
							. " FROM movie_streams M,films F WHERE "
							. "M.movie_id = F.id AND M.studio_id=" . $studio_id . " AND (F.content_category_value & " . $menuItemInfo->value . ") AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
					$sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
					$sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
					if ($mostviewed) {
						$sql_data .= " LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=" . $studio_id . " GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
					}
					$sql_data .= " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby . " LIMIT " . $limit . " OFFSET " . $offset;
					$command = Yii::app()->db->createCommand($sql_data);
				}
				$list = $command->queryAll();
				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$is_payment_gateway_exist = 0;
				$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($payment_gateway) && !empty($payment_gateway)) {
					$is_payment_gateway_exist = 1;
				}
				//Get Posters for the Movies 
				$movieids = '';
				$movieList = '';
				if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
					//Retrive the total counts based on deviceType 
					$countQuery = Yii::app()->db->createCommand()
							->select('COUNT(DISTINCT F.id) AS cnt')
							->from('movie_streams M,films F')
							->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (F.content_category_value & :content_category_value) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
					$itemCount = $countQuery->queryAll();
					$item_count = @$itemCount[0]['cnt'];

					$newList = array();
					foreach ($list AS $k => $v) {
						if ($v['content_types_id'] == 3) {
							$epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
							//Added by prakash on 1st feb 2017        
						} else if ($v['content_types_id'] == 4) {
							$epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
							$epData = Yii::app()->db->createCommand($epSql)->queryAll();
							if (count($epData)) {
								$newList[] = $list[$k];
							}
						} else if (($v['content_types_id'] == 1 || $v['content_types_id'] == 2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
							$newList[] = $list[$k];
						}
					}
					$list = array();
					$list = $newList;
				}
				$domainName = $this->getDomainName();
				$livestream_content_ids = '';
				foreach ($list AS $k => $v) {
					$movieids .= "'" . $v['movie_id'] . "',";
					if ($v['content_types_id'] == 4) {
						$livestream_content_ids .= "'" . $v['movie_id'] . "',";
					}
					if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
					} else {
						$defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
					}
					$movie_id = $v['movie_id'];
					$v['poster_url'] = $defaultPoster;
					$list[$k]['poster_url'] = $defaultPoster;
					$list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
					$is_episode = 0;
					$list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
					if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
						$director = '';
						$actor = '';
						$castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
						if ($castsarr) {
							$actor = $castsarr['actor'];
							$actor = trim($actor, ',');
							unset($castsarr['actor']);
							$director = $castsarr['director'];
							unset($castsarr['director']);
						}
						$list[$k]['actor'] = @$actor;
						$list[$k]['director'] = @$director;
						$list[$k]['cast_detail'] = @$castsarr;
					}
				}

				$movieids = rtrim($movieids, ',');
				$livestream_content_ids = rtrim($livestream_content_ids, ',');
				$liveFeedUrls = array();
				if ($livestream_content_ids) {
					$StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (" . $livestream_content_ids . ') AND studio_id=' . $this->studio_id . "  ";
					$streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
					foreach ($streamData AS $skey => $sval) {
						$liveFeedUrls[$sval['movie_id']]['feed_url'] = $sval['feed_url'];
						$liveFeedUrls[$sval['movie_id']]['is_online'] = $sval['is_online'];
					}
				}
				if ($movieids) {
					$sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (" . $movieids . ") AND studio_id=" . $studio_id . " AND language_id=" . $language_id;
					$otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
					foreach ($otherlang1 as $key => $val) {
						$otherlang[$val['parent_id']] = $val;
					}

					$psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
					$posterData = Yii::app()->db->createCommand($psql)->queryAll();
					if ($posterData) {
						$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
						foreach ($posterData AS $key => $val) {
							$posterUrl = '';
							if ($val['object_type'] == 'films') {
								$posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
								$posters[$val['movie_id']] = $posterUrl;
								if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
									$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
								}
							} else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
								$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
							}
						}
					}
					//Get view count status
					$viewStatus = VideoLogs::model()->getViewStatus($movieids, $studio_id);
					if (@$viewStatus) {
						$viewStatusArr = '';
						foreach ($viewStatus AS $key => $val) {
							$viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
							$viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
						}
					}
					foreach ($list as $key => $val) {
						if (isset($otherlang[$val['movie_id']])) {
							$list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
							$list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
							$list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
							$list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
						}
						if (isset($posters[$val['movie_id']])) {
							if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
								$list[$key]['poster_url_for_tv'] = $postersforTv[$val['movie_id']];
							}
							if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
								$posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
							}
							$list[$key]['poster_url'] = $posters[$val['movie_id']];
						}
						if (@$viewStatusArr[$val['movie_id']]) {
							$list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
						} else {
							$list[$key]['viewStatus'] = array('viewcount' => "0", 'uniq_view_count' => "0");
						}
						if ($val['content_types_id'] == 4) {
							$list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
							$list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
						}
					}
				}
				$this->code = 200;
				$data['msg'] = $translate['btn_ok'];
				$data['movieList'] = @$list;
				if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
					$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
					if ($user) {
						$favourite = Yii::app()->db->createCommand()
								->select('*')
								->from('user_favourite_list')
								->where('user_id=:user_id AND studio_id=:studio_id AND status=:status', array(':user_id' => $_REQUEST['user_id'], ':studio_id' => $this->studio_id, ':status' => 1))
								->queryAll();
						$data['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
					}
				}
				$data['orderby'] = @$order;
				$data['item_count'] = @$item_count;
				$data['limit'] = @$page_size;
				$data['Ads'] = $this->getStudioAds();
				$this->items = @$data;
			} else {
				$this->code = 605;
			}
		} else {
			$this->code = 604;
		}
	}

	/**
	 * @method private getepisodeDetails() Get the details of Episode
	 * @author GDR<support@muvi.com>
	 * @return json Returns the list of episodes
	 * @param string $permalink Content type permalink
	 * @param string $oauthToken Auth token
	 * @param string $filter Filter conditions
	 * @param int $limit No. of records to be shown in each listing 
	 * @param int $has_video it will return those content which have video.
	 * @param int $offset Offset for the limit default 0
	 * 
	 */
	public function actionEpisodeDetails() {
		if ($_REQUEST['permalink']) {
			$lang_code = @$_REQUEST['lang_code'];
			$translate = $this->getTransData($lang_code, $this->studio_id);
			$language_id = 20;
			$ltFlag = 0;
			if ($lang_code && $lang_code != 'en') {
				$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			}
			$movie = Yii::app()->db->createCommand()
					->select('f.id,f.name,f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.content_type_id,f.content_types_id,ms.id AS movie_stream_id,f.permalink')
					->from('films f ,movie_streams ms ')
					->where('ms.movie_id = f.id AND ms.is_episode=0 AND f.permalink=:permalink AND f.studio_id=:studio_id ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
					->queryRow();
			if ($movie && (($movie['content_types_id'] == 3) || ($movie['content_types_id'] == 6))) {
				$movie_id = $movie['id'];
				if ($language_id != 20) {
					$langcontent = Yii::app()->custom->getTranslatedContent($movie_id, 0, $language_id, $this->studio_id);
					if (array_key_exists($movie_id, $langcontent['film'])) {
						$movie['name'] = $langcontent['film'][$movie_id]->name;
						$movie['story'] = $langcontent['film'][$movie_id]->story;
						$movie['genre'] = $langcontent['film'][$movie_id]->genre;
						$movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
					}
				}
				$cast = array();
				if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
					$director = '';
					$actor = '';
					$castsarr = $this->getCasts($movie_id, '', $language_id, $this->studio_id);
					if ($castsarr) {
						$actor = $castsarr['actor'];
						$actor = trim($actor, ',');
						unset($castsarr['actor']);
						$director = $castsarr['director'];
						unset($castsarr['director']);
					}
					$cast['actor'] = @$actor;
					$cast['director'] = @$director;
					$cast['cast_detail'] = @$castsarr;
				}
				$pdo_cond_arr = array(':movie_id' => $movie_id, ":is_episode" => 1);
				if (isset($_REQUEST['series_number']) && $_REQUEST['series_number']) {
					$cond = " AND series_number=:series_number";
					$pdo_cond_arr[':series_number'] = $_REQUEST['series_number'];
				}
				$forRokuCond = '';
				if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
					$forRokuCond = '  AND is_converted=:is_converted ';
					$pdo_cond_arr[':is_converted'] = 1;
				}
				$content_order = StudioConfig::model()->getConfig($this->studio_id, 'multipart_content_setting')->config_value;
				$order_type = ($content_order == 0) ? 'desc' : 'asc';
				$orderby = ' episode_number ' . $order_type;
				$page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
				$offset = 0;
				if (isset($_REQUEST['offset'])) {
					$offset = ($_REQUEST['offset'] - 1) * $limit;
				}
				$bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
				$bucket = $bucketInfo['bucket_name'];
				$s3url = $bucketInfo['s3url'];
				$folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
				$signedFolderPath = $folderPath['signedFolderPath'];
				$video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";
				$list = array();
				$addQuery = '';
				$customFormObj = new CustomForms();
				$customData = $customFormObj->getFormCustomMetadat($this->studio_id, 4);
				if ($customData) {
					foreach ($customData AS $ckey => $cvalue) {
						$addQuery = ", " . $cvalue['field_name'] . " AS " . addslashes($cvalue['f_id']);
						$customArr[$cvalue['f_id']] = $cvalue['f_display_name'];
					}
				}
				$command = Yii::app()->db->createCommand()
						->select('SQL_CALC_FOUND_ROWS (0),id,embed_id AS movie_stream_uniq_id,full_movie,episode_number,video_resolution,episode_title,series_number,episode_date,episode_story,IF((full_movie != "" AND is_converted = 1),CONCAT(\'' . $video_url . '\',id,\'/\',full_movie),"") AS video_url,thirdparty_url,rolltype,roll_after,video_duration' . $addQuery)
						->from('movie_streams')
						->where('movie_id=:movie_id AND is_episode=:is_episode AND episode_parent_id = 0 ' . $forRokuCond . $cond, $pdo_cond_arr)
						->order($orderby)
						->limit($limit, $offset);
				$list = $command->queryAll();

				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				if ($list) {
					$domainName = $this->getDomainName();
					$_REQUEST['is_mobile'] = 1;
					foreach ($list AS $k => $v) {
						$streamIds .= $v['id'] . ",";
					}
					$streamIds = trim($streamIds, ',');
					// Get All Translated Content. 
					if ($language_id != 20) {
						$sql = "SELECT id,`movie_id`,`episode_title`,`episode_story`,episode_parent_id FROM movie_streams WHERE episode_parent_id IN(" . $streamIds . ") AND studio_id=" . $studio_id . " AND episode_language_id=" . $language_id;
						$episodeData = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
						if ($episodeData) {
							foreach ($episodeData as $ekey => $evalue) {
								$langcontent['episode'][$evalue->id] = $evalue;
								$ltFlag = 1;
							}
						}
					}
					if ($streamIds) {
						$defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
						$postObj = new Poster();
						$pdata = $postObj->getPosterByids($streamIds, 'moviestream', $this->studio_id, 'episode');
					}
					foreach ($list AS $k => $v) {
						//$langcontent = Yii::app()->custom->getTranslatedContent($v['id'],1,$language_id,$this->studio_id);
						if ($ltFlag && (array_key_exists($v['id'], $langcontent['episode']))) {
							$list[$k]['episode_title'] = $langcontent['episode'][$v['id']]->episode_title;
							$list[$k]['episode_story'] = $langcontent['episode'][$v['id']]->episode_story;
						} else {
							$list[$k]['episode_title'] = $v['episode_title'];
							$list[$k]['episode_story'] = $v['episode_story'];
						}
						$list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
						$list[$k]['poster_url'] = $pdata[$v['id']] ? $pdata[$v['id']]['poster_url'] : $defaultPoster;

						if (isset($_REQUEST['has_video']) && $_REQUEST['has_video'] == 1) {
							$posterUrlForTv = $this->getPoster($v['id'], 'moviestream', 'roku', $this->studio_id);
							if ($posterUrlForTv != '') {
								$list[$k]['posterForTv'] = $posterUrlForTv;
							}
						}
						$list[$k]['movieUrlForTv'] = '';
						$list[$k]['video_url'] = '';
						$list[$k]['resolution'] = array();
						if ($v['thirdparty_url'] != "") {
							$info = pathinfo($v['thirdparty_url']);
							if (@$info["extension"] == "m3u8") {
								$list[$k]['movieUrlForTv'] = $v['thirdparty_url'];
								$list[$k]['video_url'] = $v['thirdparty_url'];
							}
							$list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
						} else if ($v['video_url'] != '') {
							$list[$k]['movieUrlForTv'] = $v['video_url'];
							$multipleVideo = $this->getvideoResolution($v['video_resolution'], $v['video_url']);
							$videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $v['video_url']);
							$videoToBeplayed12 = explode(",", $videoToBeplayed);
							$list[$k]['video_url'] = $videoToBeplayed12[0];
							$list[$k]['resolution'] = $multipleVideo;
							//Ad Details
							$list[$k]['adDetails'] = array();
							if ($v['rolltype'] == 1) {
								$list[$k]['adDetails'][] = 0;
							} elseif ($v['rolltype'] == 2) {
								$roleafterarr = explode(',', $v['roll_after']);
								foreach ($roleafterarr AS $ktime => $vtime) {
									$list[$k]['adDetails'][] = $this->convertTimetoSec($vtime);
								}
							} elseif ($v['rolltype'] == 3) {
								$list[$k]['adDetails'][] = $this->convertTimetoSec($v['video_duration']);
							}
						}
					}
				}
				$comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));

				$this->code = 200;
				$data['msg'] = $translate['btn_ok'];
				$data['name'] = $movie['name'];
				$data['muvi_uniq_id'] = $movie['muvi_uniq_id'];
				$data['custom_fields'] = $customArr;
				$data['is_ppv'] = $movie['is_ppv'];
				if (!empty($ppvarr)) {
					$data['ppv_pricing'] = $ppvarr;
					$data['currency'] = $currency->attributes;
				}
				$data['permalink'] = $movie['permalink'];
				$data['item_count'] = $item_count;
				$data['limit'] = $limit;
				$data['offset'] = $offset;
				$data['comments'] = $comments;
				$data['episode'] = $list;
				$data['cast'] = $cast;
				$this->items = $data;
			} else {
				$this->code = 608;
			}
		} else {
			$this->code = 607;
		}
	}

	/**
	 * @method private getContentDetails() Get Content details 
	 * @author GDR<support@muvi.com>
	 * @return json Returns details of the content
	 * @param string $permalink Content type permalink
	 * @param string $oauthToken Auth token
	 * @param string $filter Filter conditions
	 * @param int $limit No. of records to be shown in each listing 
	 * @param int $offset Offset for the limit default 0
	 * 
	 */
	public function actionGetContentDetails() {
		if ($_REQUEST['permalink']) {
			$lang_code = @$_REQUEST['lang_code'];
			$language_id = 20;
			if ($lang_code && $lang_code != 'en')
				$language_id = Yii::app()->custom->getLanguage_id($lang_code);

			$movie = Yii::app()->db->createCommand()
					->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.full_movie,ms.is_converted,ms.video_resolution,ms.embed_id AS movie_stream_uniq_id, f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.permalink,f.content_type_id,f.genre,f.release_date,f.censor_rating,f.story,ms.rolltype,ms.roll_after,ms.video_duration,ms.thirdparty_url,ms.is_episode')
					->from('films f ,movie_streams ms ')
					->where('ms.movie_id = f.id AND ms.is_episode=0 AND ((ms.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(ms.content_publish_date) = 0) OR (ms.content_publish_date <=NOW())) AND  f.permalink=:permalink AND  f.studio_id=:studio_id AND f.parent_id = 0 AND ms.episode_parent_id=0 ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
					->queryRow();
			if (isset($_REQUEST['country']) && $_REQUEST['country']) {
				$country = $_REQUEST['country'];
			} else {
				$visitor_loc = Yii::app()->common->getVisitorLocation();
				$country = $visitor_loc['country'];
			}
			if (Yii::app()->common->isGeoBlockContent(@$movie[0]['id'], @$movie[0]['movie_stream_id'], $this->studio_id, $country)) {
				if ($movie) {
					$thirdPartyUrl = '';
					$domainName = $this->getDomainName();
					$movie['censor_rating'] = stripslashes($movie['censor_rating']);
					$movie_id = $movie['id'];
					$langcontent = Yii::app()->custom->getTranslatedContent($movie_id, 0, $language_id, $this->studio_id);
					$arg['studio_id'] = $this->studio_id;
					$arg['movie_id'] = $movie_id;
					$arg['content_types_id'] = $movie['content_types_id'];

					$isFreeContent = Yii::app()->common->isFreeContent($arg);
					$movie['isFreeContent'] = $isFreeContent;

					$trailerThirdpartyUrl = '';
					$trailerUrl = '';
					$embedTrailerUrl = '';
					$trailerData = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
					if ($trailerData['trailer_file_name'] != '') {
						$embedTrailerUrl = $domainName . '/embedTrailer/' . $movie['muvi_uniq_id'];
					} else if ($trailerData['third_party_url'] != '') {
						$trailerThirdpartyUrl = $this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
					} else if ($trailerData['video_remote_url'] != '') {
						$trailerUrl = $this->getTrailer($movie_id, "", $this->studio_id);
					}

					$is_ppv = $is_advance = 0;
					if (intval($isFreeContent) == 0) {
						$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
						if (isset($payment_gateway) && !empty($payment_gateway)) {
							if ($movie['is_converted'] == 1) {
								$ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
								if (isset($ppv->id) && intval($ppv->id)) {
									$is_ppv = 1;
								} else {
									$ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
									$is_ppv = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
								}
							} else {
								$ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id);
								$is_advance = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
							}
						}
					}
					$ppvarr = array();
					$command = Yii::app()->db->createCommand()
							->select('default_currency_id,rating_activated')
							->from('studios')
							->where('id = ' . $this->studio_id);
					$studio = $command->queryAll();
					if (intval($is_ppv) || intval($is_advance)) {
						$price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
						(array) $currency = Currency::model()->findByPk($price['currency_id']);

						$ppvarr['id'] = $ppv->id;
						$ppvarr['title'] = $ppv->title;
						$ppvarr['pricing_id'] = $price['id'];
						$ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
						$ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];
						$ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
						$ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
						$ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];
						$ppvarr['show_subscribed'] = $price['show_subscribed'];
						$ppvarr['season_subscribed'] = $price['season_subscribed'];
						$ppvarr['episode_subscribed'] = $price['episode_subscribed'];

						$validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

						$ppvarr['validity_period'] = $validity->validity_period;
						$ppvarr['validity_recurrence'] = $validity->validity_recurrence;

						if (isset($ppv->content_types_id) && trim($ppv->content_types_id) == 3) {
							$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
							$ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
							$ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
							$ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
						}

						if (intval($is_ppv)) {
							$movie['is_ppv'] = $is_ppv;
						}
						if (intval($is_advance)) {
							$movie['is_advance'] = $is_advance;
						}
					} else {
						$movie['is_ppv'] = 0;
						$movie['is_advance'] = 0;
					}

					$castsarr = $this->getCasts($movie_id, '', $language_id, $this->studio_id);
					if ($castsarr) {
						$actor = $castsarr['actor'];
						$actor = trim($actor, ',');
						unset($castsarr['actor']);
						$director = $castsarr['director'];
						unset($castsarr['director']);
					}

					$cast_detail = $castsarr;
					$movie['actor'] = @$actor;
					$movie['director'] = @$director;
					$movie['cast_detail'] = @$castsarr;
					$movie['embeddedUrl'] = "";

					//Get view status 
					$viewStatus = VideoLogs::model()->getViewStatus($movie_id, $this->studio_id);
					$movie['viewStatus']['viewcount'] = @$viewStatus[0]['viewcount'] ? @$viewStatus[0]['viewcount'] : "0";
					$movie['viewStatus']['uniq_view_count'] = @$viewStatus[0]['u_viewcount'] ? @$viewStatus[0]['u_viewcount'] : "0";
					if ($movie['thirdparty_url'] != "") {
						$info = pathinfo($movie['thirdparty_url']);
						if (@$info["extension"] == "m3u8") {
							$movieUrlToBePlayed = $movie['thirdparty_url'];
							$movieUrl = $movie['thirdparty_url'];
						}
						$thirdPartyUrl = $this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
						$movie['embeddedUrl'] = $domainName . '/embed/' . $movie['movie_stream_uniq_id'];
					} else if ($movie['full_movie'] != '' && $movie['is_converted'] == 1) {
						$bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
						$folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
						$signedFolderPath = $folderPath['signedFolderPath'];
						$movieUrl = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/" . $movie['movie_stream_id'] . "/" . urlencode($movie['full_movie']);
						$movie['movieUrlForTv'] = $movieUrl;
						$_REQUEST['is_mobile'] = 1;
						$multipleVideo = $this->getvideoResolution($movie['video_resolution'], $movieUrl);
						$videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $movieUrl);
						$videoToBeplayed12 = explode(",", $videoToBeplayed);
						$movieUrlToBePlayed = $videoToBeplayed12[0];
						$movie['embeddedUrl'] = $domainName . '/embed/' . $movie['movie_stream_uniq_id'];
					}
					$movie['movieUrlForTv'] = @$movieUrl;
					$movie['movieUrl'] = @$movieUrlToBePlayed;
					//Added thirdparty url
					$movie['thirdparty_url'] = $thirdPartyUrl;
					if ($movie['content_types_id'] == 4) {
						$Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id' => $this->studio_id, ':movie_id' => $movie_id));
						if ($Ldata && $Ldata->feed_url) {
							$movie['embeddedUrl'] = $domainName . '/embed/livestream/' . $movie['muvi_uniq_id'];
							$movie['feed_url'] = $Ldata->feed_url;
						}
					}
					$movie['resolution'] = @$multipleVideo;
					$MovieBanner = $this->getPoster($movie_id, 'topbanner', 'original', $this->studio_id);
					if ($movie['content_types_id'] == 2) {
						$poster = $this->getPoster($movie_id, 'films', 'episode', $this->studio_id);
					} else {
						$poster = $this->getPoster($movie_id, 'films', 'thumb', $this->studio_id);
					}
					if (isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
						$posterForTv = $this->getPoster($movie_id, 'films', 'roku', $this->studio_id);
						if ($posterForTv != '') {
							$movie['posterForTv'] = $posterForTv;
						}
					}
					$movie['banner'] = $MovieBanner;
					$movie['poster'] = @$poster;
					$movie_name = $movie['name'];
					if ($movie['content_types_id'] == 3) {
						$EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);
						$seasons = $this->getContentSeasons($movie_id, $this->studio_id);
					}
					$comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));
					$movie['adDetails'] = array();
					if ($movie['rolltype'] == 1) {
						$movie['adDetails'][] = 0;
					} elseif ($movie['rolltype'] == 2) {
						$roleafterarr = explode(',', $movie['roll_after']);
						foreach ($roleafterarr AS $tkey => $tval) {
							$movie['adDetails'][] = $this->convertTimetoSec($tval);
						}
					} elseif ($movie['rolltype'] == 3) {
						$movie['adDetails'][] = $this->convertTimetoSec($movie['video_duration']);
					}
					if (array_key_exists($movie_id, $langcontent['film'])) {
						$movie['name'] = $langcontent['film'][$movie_id]->name;
						$movie['story'] = $langcontent['film'][$movie_id]->story;
						$movie['genre'] = $langcontent['film'][$movie_id]->genre;
						$movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
					}

					$movie['trailerThirdpartyUrl'] = $trailerThirdpartyUrl;
					$movie['trailerUrl'] = $trailerUrl;
					$movie['embedTrailerUrl'] = $embedTrailerUrl;
					$movie['is_episode'] = (isset($movie['is_episode']) && strlen($movie['is_episode']) > 0) ? $movie['is_episode'] : 0;
					if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
						$user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
						if ($user) {
							$favourite = Yii::app()->db->createCommand()
									->select('id')
									->from('user_favourite_list')
									->where('user_id=:user_id AND content_id=:content_id AND studio_id=:studio_id AND status=:status', array(':user_id' => $_REQUEST['user_id'], ':content_id' => $movie['id'], ':studio_id' => $this->studio_id, ':status' => 1))
									->queryAll();
							$movie['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
						}
					}
					$this->code = 200;
					$data['movie'] = $movie;
					if (!empty($ppvarr)) {
						if (intval($is_ppv)) {
							$data['ppv_pricing'] = $ppvarr;
						}
						if (intval($is_advance)) {
							$data['adv_pricing'] = $ppvarr;
						}
						$data['currency'] = $currency->attributes;
					}
					$data['comments'] = $comments;
					if ($studio[0]['rating_activated'] == 1) {
						$data['rating'] = $this->actionRating($movie_id);
					}
					$data['epDetails'] = $EpDetails;
					if (@$seasons)
						$data['seasons'] = $seasons;
				} else {
					$this->code = 605;
				}
			} else {
				$this->code = 770;
			}
		} else {
			$this->code = 604;
		}
		$this->items = @$data;
	}

	/**
	 * @method private searchData() Get Content details 
	 * @author GDR<support@muvi.com>
	 * @return json Returns details of the content
	 * @param string $permalink Content type permalink
	 * @param string $oauthToken Auth token
	 * @param string $filter Filter conditions
	 * @param int $limit No. of records to be shown in each listing 
	 * @param int $offset Offset for the limit default 0
	 * 
	 */
	public function actionsearchData() {
		if (isset($_REQUEST['q']) && $_REQUEST['q']) {
			$lang_code = @$_REQUEST['lang_code'];
			$translate = $this->getTransData($lang_code, $this->studio_id);
			$language_id = 20;
			if ($lang_code && $lang_code != 'en') {
				$language_id = Yii::app()->custom->getLanguage_id($lang_code);
			}
			if ($language_id == 20) {
				$langcond = ' AND (F.language_id = ' . $language_id . ' AND M.episode_language_id = ' . $language_id . ') ';
			} else {
				$langcond = ' AND (F.language_id = ' . $language_id . ' AND (IF (M.is_episode = 0, M.episode_language_id = 20 , M.episode_language_id=' . $language_id . '))) ';
			}
			$q = strtolower($_REQUEST['q']);
			$casts = Yii::app()->db->createCommand()
					->select('id,parent_id,language_id')
					->from('celebrities')
					->where('studio_id=:studio_id AND language_id=:language_id AND LOWER(name) LIKE  :search_string ', array(':studio_id' => $this->studio_id, ':search_string' => "%$q%", ':language_id' => $language_id))
					->queryAll();
			$cast_ids = array();
			$inCond = '';
			if (!empty($casts)) {
				foreach ($casts as $cast) {
					if ($cast['parent_id'] > 0) {
						$cast_ids[] = $cast['parent_id'];
					} else {
						$cast_ids[] = $cast['id'];
					}
				}
			}
			if (!empty($cast_ids)) {
				$cast_ids = implode(',', $cast_ids);
				$movieIds = Yii::app()->db->createCommand()
						->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
						->from('movie_casts mc')
						->where('mc.celebrity_id IN (' . $cast_ids . ') ')
						->queryAll();
				if (@$movieIds[0]['movie_ids']) {
					$movie_ids = $movieIds[0]['movie_ids'];
					if ($language_id == 20) {
						$film = Yii::app()->db->createCommand()
								->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
								->from('films f')
								->where('f.id IN (' . $movie_ids . ')')
								->queryAll();
					} else {
						$film = Yii::app()->db->createCommand()
								->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
								->from('films f')
								->where('f.parent_id IN (' . $movie_ids . ') AND language_id=' . $language_id)
								->queryAll();
					}
					if (!empty($film)) {
						$inCond = " OR F.id IN(" . $film[0]['movie_ids'] . ") ";
					}
				}
			}
			//Build the search query 
			$page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
			$offset = 0;
			if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
				$offset = ($_REQUEST['offset'] - 1) * $limit;
			}
			$forRokuCond = '';
			if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
				$forRokuCond = " AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) ";
			}
			$bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
			$bucket = $bucketInfo['bucket_name'];
			$s3url = $bucketInfo['s3url'];
			$folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
			$signedFolderPath = $folderPath['signedFolderPath'];
			$video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";

			$permalink = Yii::app()->getBaseUrl(TRUE) . '/';
			if ($_REQUEST['prev_dev']) {
				$command = Yii::app()->db->createCommand()
						->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
						->from('movie_streams M,films F')
						->where('M.movie_id = F.search_parent_id ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
						->order('F.name,M.episode_title')
						->limit($limit, $offset);
			} else {
				/* Add Geo block to listing by manas@muvi.com */
				if (isset($_REQUEST['country']) && $_REQUEST['country']) {
					$country = $_REQUEST['country'];
				} else {
					$visitor_loc = Yii::app()->common->getVisitorLocation();
					$country = $visitor_loc['country'];
				}
				$sql_data1 = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,F.parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
						. ' FROM movie_streams M,films F WHERE '
						. ' M.movie_id = F.search_parent_id  ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond;
				$sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $this->studio_id . " AND sc.country_code='{$country}'";
				$sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $limit . " OFFSET " . $offset;
				$command = Yii::app()->db->createCommand($sql_data);
				/* END */
			}
			$list = $command->queryAll();
			$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();

			if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
				//Retrive the total counts based on deviceType 
				$countQuery = Yii::app()->db->createCommand()
						->select('COUNT(*) as cnt')
						->from('movie_streams M,films F')
						->where('M.movie_id = F.search_parent_id  ' . $langcond . ' AND M.studio_id=:studio_id AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=\'\'))) AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id));
				$itemCount = $countQuery->queryAll();
				$item_count = @$itemCount[0]['cnt'];
				$newList = array();

				foreach ($list AS $ckey => $cval) {
					if ($cval['content_types_id'] == 3 && $cval['is_episode'] == 0) {
						$mpp_ids .= $cval['movie_id'] . ",";
					} elseif ($cval['content_types_id'] == 4) {
						$ls_ids .= $cval['movie_id'] . ",";
					}
				}
				$mpp_ids = trim($mpp_ids, ',');
				$ls_ids = trim($ls_ids, ',');
				if ($mpp_ids) {
					$epSql = " SELECT movie_id FROM movie_streams where movie_id IN (" . $mpp_ids . ') AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 GROUP BY movie_id";
					$epData = Yii::app()->db->createCommand($epSql)->queryColumn();
				}
				if ($ls_ids) {
					$lsSql = " SELECT id FROM livestream where movie_id IN (" . $ls_ids . ')AND studio_id=' . $this->studio_id . "  LIMIT 1";
					$lsData = Yii::app()->db->createCommand($lsSql)->queryColumn();
				}
				foreach ($list AS $k => $v) {
					if ($v['content_types_id'] == 3 && $v['is_episode'] == 0) {
						if (@$episodeData && (in_array($v['movie_id'], $episodeData))) {
							$newList[] = $list[$k];
						}
					} else if ($v['content_types_id'] == 4) {

						if (@$lsData && (in_array($v['movie_id'], $lsData))) {
							$newList[] = $list[$k];
						}
					} else if (($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
						$newList[] = $list[$k];
					}
				}
				$list = array();
				$list = $newList;
			}
			if ($list) {
				$view = array();
				$cnt = 1;
				$movieids = '';
				$stream_ids = '';
				$domainName = $this->getDomainName();
				foreach ($list AS $k => $v) {
					$list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
					$list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
					if ($v['episode_parent_id'] > 0) {
						$v['movie_stream_id'] = $v['episode_parent_id'];
						$list[$k]['movie_stream_id'] = $v['episode_parent_id'];
					}
					if ($v['is_episode']) {
						$stream_ids .= "'" . $v['movie_stream_id'] . "',";
					} else {
						$movieids .= "'" . $v['movie_id'] . "',";
						if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
							$director = '';
							$actor = '';
							$castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
							if ($castsarr) {
								$actor = $castsarr['actor'];
								$actor = trim($actor, ',');
								unset($castsarr['actor']);
								$director = $castsarr['director'];
								unset($castsarr['director']);
							}
							$list[$k]['actor'] = @$actor;
							$list[$k]['director'] = @$director;
							$list[$k]['cast_detail'] = @$castsarr;
						}
					}
					if ($v['content_types_id'] == 4) {
						$liveTV[] = $v['movie_id'];
					}
					if (isset($_REQUEST["has_video"]) && $_REQUEST["has_video"] == 1) {
						if (@$v['thirdparty_url'] != "") {
							$info = pathinfo(@$v['thirdparty_url']);
							if (@$info["extension"] == "m3u8") {
								$list[$k]['movieUrlForTV'] = @$v['thirdparty_url'];
							}
						} else {
							$list[$k]['movieUrlForTV'] = @$v['video_url'];
						}
					}
					if ($v['thirdparty_url'] != "") {
						$list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
					}
				}
				$viewcount = array();
				$movie_ids = rtrim($movieids, ',');
				$stream_ids = rtrim($stream_ids, ',');


				if ($movie_ids) {
					$cast = MovieCast::model()->findAll(array("condition" => "movie_id  IN(" . $movie_ids . ')'));
					foreach ($cast AS $key => $val) {
						if (in_array('actor', json_decode($val['cast_type']))) {
							$actor[$val['movie_id']] = @$actor[$val['movie_id']] . $val['celebrity']['name'] . ', ';
						}
					}
					$psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movie_ids . ") AND (object_type='films' OR object_type='tvapp') ";
					$posterData = Yii::app()->db->createCommand($psql)->queryAll();
					if ($posterData) {
						$posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
						foreach ($posterData AS $key => $val) {
							$size = 'thumb';
							if ($val['object_type'] == 'films') {
								$posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/' . $size . '/' . urlencode($val['poster_file_name']);
								$posters[$val['movie_id']] = $posterUrl;
								if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
									$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
								}
							} else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
								$postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
							}
						}
					}
				}
				$default_epp = POSTER_URL . '/' . 'no-image-h.png';
				$default_p = POSTER_URL . '/' . 'no-image-a.png';
				if (@$liveTV) {
					$lcriteria = new CDbCriteria();
					$lcriteria->select = "movie_id,IF(feed_type=1,'hls','rtmp') feed_type,feed_url,feed_method";
					$lcriteria->addInCondition("movie_id", $liveTV);
					$lcriteria->condition = "studio_id = $this->studio_id";
					$lresult = Livestream::model()->findAll($lcriteria);
					foreach ($lresult AS $key => $val) {
						$liveTvs[$val->movie_id]['feed_url'] = $val->feed_url;
						$liveTvs[$val->movie_id]['feed_type'] = $val->feed_type;
						$liveTvs[$val->movie_id]['feed_method'] = $val->feed_method;
					}
				}
				//Get view status for content other then episode
				$viewStatus = VideoLogs::model()->getViewStatus($movie_ids, $this->studio_id);

				if ($viewStatus) {
					$viewStatusArr = '';
					foreach ($viewStatus AS $key => $val) {
						$viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
						$viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
					}
				}
				//Get view status for Episodes 
				$viewEpisodeStatus = VideoLogs::model()->getEpisodeViewStatus($stream_ids, $this->studio_id);
				if ($viewEpisodeStatus) {
					$viewEpisodeStatusArr = '';
					foreach ($viewEpisodeStatus AS $key => $val) {
						$viewEpisodeStatusArr[$val['video_id']]['viewcount'] = $val['viewcount'];
						$viewEpisodeStatusArr[$val['video_id']]['uniq_view_count'] = $val['u_viewcount'];
					}
				}
				$default_status_view = array('viewcount' => "0", 'uniq_view_count' => "0");
				foreach ($list AS $key => $val) {
					$casts = '';
					if ($val['is_episode'] == 1) {
						if ($language_id != 20 && $val['episode_parent_id'] > 0) {
							$val['movie_stream_id'] = $val['episode_parent_id'];
						}
						//Get Posters for Episode
						$posterUrlForTv = $this->getPoster($val['movie_stream_id'], 'moviestream', 'roku', $this->studio_id);
						if ($posterUrlForTv != '') {
							$list[$key]['posterForTv'] = $posterUrlForTv;
						}
						$poster_url = $this->getPoster($val['movie_stream_id'], 'moviestream', 'episode', $this->studio_id);
						$list[$key]['viewStatus'] = @$viewEpisodeStatusArr[$val['movie_stream_id']] ? @$viewEpisodeStatusArr[$val['stream_id']] : $default_status_view;
					} else {
						if ($language_id != 20 && $val['parent_id'] > 0) {
							$val['movie_id'] = $val['parent_id'];
						}
						$poster_url = $posters[$val['movie_id']] ? $posters[$val['movie_id']] : $default_p;
						if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
							$poster_url = str_replace('/thumb/', '/episode/', $poster_url);
						}
						$casts = $actor[$val['movie_id']] ? rtrim($actor[$val['movie_id']], ', ') : '';
						$list[$key]['viewStatus'] = @$viewStatusArr[$val['movie_id']] ? @$viewStatusArr[$val['movie_id']] : $default_status_view;
						if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
							$list[$key]['posterForTv'] = $postersforTv[$val['movie_id']];
						}
					}
					$list[$key]['poster_url'] = $poster_url;
					if ($val['content_types_id'] == 4) {
						$list[$key]['feed_type'] = $liveTvs[$val['movie_id']]['feed_type'];
						$list[$key]['feed_url'] = $liveTvs[$val['movie_id']]['feed_url'];
						$list[$key]['feed_method'] = $liveTvs[$val['movie_id']]['feed_method'];
					}
					$list[$key]['actor'] = $casts;
				}
				$list[$key]['adDetails'] = array();
				if ($val['rolltype'] == 1) {
					$list[$key]['adDetails'][] = 0;
				} elseif ($val['rolltype'] == 2) {
					$roleafterarr = explode(',', $val['roll_after']);
					foreach ($roleafterarr AS $tkey => $tval) {
						$list[$key]['adDetails'][] = $this->convertTimetoSec($tval);
					}
				} elseif ($val['rolltype'] == 3) {
					$list[$key]['adDetails'][] = $this->convertTimetoSec($val['video_duration']);
				}
			}


			$searchLogs = new SearchLog();
			$newlisting = array();
			$listings = $list;
			if (!empty($listings)) {
				foreach ($listings as $listnew) {
					$searchLogs->setIsNewRecord(true);
					$searchLogs->id = null;
					$searchLogs->studio_id = $this->studio_id;
					$searchLogs->search_string = $_REQUEST['q'];
					$searchLogs->object_category = "content";
					$searchLogs->object_id = $lists['movie_id'];
					$searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
					$searchLogs->created_date = gmdate('Y-m-d H:i:s');
					$searchLogs->ip = $_SERVER['REMOTE_ADDR'];
					$searchLogs->save();
					$content_id = $list_id;
					$is_episode = $listnew['is_episode'];
					if ($is_episode == 1) {
						$content_id = $stream_id;
					}
				}
			} else {
				$searchLogs->studio_id = $this->studio_id;
				$searchLogs->search_string = $_REQUEST['q'];
				$searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
				$searchLogs->created_date = gmdate('Y-m-d H:i:s');
				$searchLogs->ip = $_SERVER['REMOTE_ADDR'];
				$searchLogs->save();
			}
			$this->code = 200;
			$data['msg'] = $translate['btn_ok'];
			$data['search'] = $list;
			$data['limit'] = $limit;
			$data['offset'] = $offset;
			$data['item_count'] = $item_count;
			$data['Ads'] = $this->getStudioAds();
			$this->items = @$data;
		} else {
			$this->code = 609;
		}
	}

	/**
	 * @method public checkEmailExistance($email) It will check if an email exists with the studio or not
	 * @return json Return the json array of results
	 * @param String $authToken Auth token of the studio
	 * @param String $email Email to be validated
	 */
	public function actionCheckEmailExistance($req = array()) {
		if ($req)
			$_REQUEST = $req;
		$code = 752;
		$email_status = 0;
		if (isset($_REQUEST) && isset($_REQUEST['email'])) {
			$user = new SdkUser;
			$users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id));
			if (isset($users) && !empty($users)) {
				$code = 200;
				$email_status = 1;
			}
		}
		$this->code = $code;
		$this->items = array('isExists' => $email_status);
		return true;
	}

	/**
	 * @method GetStatByType() Return the 
	 * @param String $authToken A authToken
	 * @param array $filterType filter on specific type like genre, language,rating etc
	 * @author Gayadhar<support@muvi.com>
	 * @return Json user details 
	 */
	function actionGetStatByType() {
		if ($_REQUEST['filterType']) {
			if (strtolower(trim($_REQUEST['filterType'])) == 'genre') {
				$genreArr = array();
				$gsql = 'SELECT genre, COUNT(*) AS cnt FROM films WHERE studio_id =' . $this->studio_id . ' AND genre is not NULL AND genre !=\'\' AND genre !=\'null\' GROUP BY genre';
				$res = Yii::app()->db->createCommand($gsql)->queryAll();
				if ($res) {
					foreach ($res As $key => $val) {
						$expArr = json_decode($val['genre'], TRUE);
						if (is_array($expArr)) {
							foreach ($expArr AS $k => $v) {
								$mygenre = strtolower(trim($v));
								$data['content_statistics'][$mygenre] += $val['cnt'];
							}
						}
					}
					$this->code = 200;
					$this->items = @$data;
					return true;
				}
				$this->code = 642;
				return true;
			}
		}
		$this->code = 641;
		return true;
	}

	function actiongetCategoryList() {
		$studio_id = $this->studio_id;
		$cat = Yii::app()->db->createCommand("SELECT * FROM content_category WHERE studio_id={$studio_id} AND parent_id=0 ORDER BY id DESC")->queryAll();
		if (!empty($cat)) {
			$cat_img_option = Yii::app()->custom->getCatImgOption($studio_id);
			$cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
			foreach ($cat as $k => $cat_data) {
				$list[$k]['category_id'] = $cat_data['id'];
				$list[$k]['category_name'] = $cat_data['category_name'];
				$list[$k]['permalink'] = $cat_data['permalink'];
				$list[$k]['category_img_url'] = $this->getPoster($cat_data['id'], 'content_category', 'original', $studio_id);
				$list[$k]['cat_img_size'] = @$cat_img_size;
			}
			$this->code = 200;
		} else {
			$this->code = 670;
		}
		$this->items = @$list;
	}

	/**
	 * @method public getGenreList() returns unique genre value either custom_metadata_field or movie_tags table
	 * @author SKM<prakash@muvi.com>
	 * @param string authToken* string lang_code.
	 * @return json Json data with parameters
	 */
	function actiongetGenreList() {
		$lang_code = @$_REQUEST['lang_code'];
		$customGenre = CustomMetadataField::model()->findByAttributes(array('studio_id' => $this->studio_id, 'f_id' => 'genre'), array('select' => 'f_value'));
		if ($customGenre) {
			$allgenre = json_decode($customGenre->f_value, TRUE);
			if ((isset($allgenre[$lang_code])) && is_array($allgenre[$lang_code])) {
				$this->items = $allgenre[$lang_code];
			} else if ((isset($allgenre['en'])) && is_array($allgenre['en'])) {
				$this->items = $allgenre['en'];
			} else {
				$this->items = $allgenre;
			}
		} else {
			$this->items = Yii::app()->db->createCommand()->select('name')->from('movie_tags')->where('taggable_type=1 AND studio_id=:studio_id', array(':studio_id' => $this->studio_id))->group('name')->order('name')->queryColumn();
		}
		$this->code = $this->items ? 200 : 642;
	}

	/*	 * @method  return scr part from thirdparty url
	 * @author SKM<prakash@muvi.com>
	 * @return string
	 */
	
	public function getFilmName($movie_id) {
		$movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=" . $movie_id)->queryROW();
		return $movie_name['name'];
	}

	public function getEpisodeName($episode_id) {
		$movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=" . $episode_id)->queryROW();
		return $movie_name['episode_title'];
	}

	/**
	 * @purpose get the cast and crew
	 * @param movie_id Mandatory
	 * @Note Applicable only for single part, multipart content. Not for live streams and episodes
	 * @Author <ajit@muvi.com>
	 */
	function actiongetCelibrity() {
		$movie_uniqueid = @$_REQUEST['movie_id'];
		if ($movie_uniqueid) {
			$studio_id = $this->studio_id;
			$Films = Film::model()->find(array('select' => 'id', 'condition' => 'studio_id=:studio_id and uniq_id=:uniq_id', 'params' => array(':studio_id' => $studio_id, ':uniq_id' => $movie_uniqueid)));
			$movie_id = $Films->id;
			if ($movie_id) {
				$sql = "SELECT C.id,C.name,C.birthdate,C.birthplace,C.summary,C.twitterid,C.permalink,C.alias_name,C.meta_title,C.meta_description,C.meta_keywords FROM celebrities C WHERE C.id IN(SELECT celebrity_id FROM movie_casts MC WHERE MC.movie_id = " . $movie_id . ") AND C.studio_id = " . $studio_id;
				$command = Yii::app()->db->createCommand($sql);
				$list = $command->queryAll();
				if ($list) {
					foreach ($list as $key => $val) {
						//get celebrity type
						$cast_name = MovieCast::model()->find(array('select' => 'cast_type', 'condition' => 'celebrity_id=:celebrity_id', 'params' => array(':celebrity_id' => $val['id'])));
						$celebrity[$key]['name'] = $val['name'];
						$celebrity[$key]['permalink'] = $val['permalink'];
						$celebrity[$key]['summary'] = $val['summary'];
						$celebrity[$key]['cast_type'] = $cast_name->cast_type;
						$celebrity[$key]['celebrity_image'] = $this->getPoster($val['id'], 'celebrity', 'medium', $studio_id);
					}
					$data['code'] = 200;
					$data['status'] = "OK";
					$data['celibrity'] = $celebrity;
				} else {
					$data['code'] = 448;
					$data['status'] = "Failure";
					$data['msg'] = "Celebrity not found.";
				}
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = "Records for this id not found";
			}
		} else {
			$data['code'] = 448;
			$data['status'] = "Failure";
			$data['msg'] = "Required parameters not found";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	function actiongetEmojis() {
		$emo = Emojis::model()->findAll();
		if ($emo) {
			foreach ($emo as $key => $val) {
				$set[$key]['code'] = $val['code'];
				$set[$key]['name'] = $val['emoji_name'];
				$set[$key]['url'] = $val['emoji_url'];
			}
			$data['code'] = 200;
			$data['status'] = "OK";
			$data['emojis'] = $set;
		} else {
			$data['code'] = 448;
			$data['status'] = "Failure";
			$data['msg'] = "Emoji not found.";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	/**
	 * @purpose get the total no of videos and total duration of videos
	 * @param authToken Mandatory
	 * @Author Biswajit Parida<biswajit@muvi.com>
	 */
	public function actionGetTotalNoOfVideo() {
		$mapped_videos = 0;
		$mapped_duration = $total_duration = 0;
		$studio_id = $this->studio_id;
		$sql = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*,ms.id as movie_stream_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted FROM video_management vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (vm.id=mt.video_management_id) WHERE  vm.studio_id=' . $studio_id . ' GROUP BY vm.id  order by vm.video_name ASC';
		$allvideo = Yii::app()->db->createCommand($sql)->queryAll();
		$total_videos = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
		if (!empty($allvideo)) {
			foreach ($allvideo as $videos) {
				$duration = Yii::app()->custom->convertTimeToMiliseconds($videos['duration']);
				if (trim($duration) == "") {
					$duration = 0;
				}
				$total_duration += $duration;
				if (($videos['movieConverted'] == 1)) {
					$mapped_videos++;
					$mapped_duration += $duration;
				} elseif (($videos['trailerConverted'] == 1)) {
					$mapped_videos++;
					$mapped_duration += $duration;
				}
			}
		}
		//$total_duration  = Yii::app()->custom->formatMilliseconds($total_duration);
		//$mapped_duration = Yii::app()->custom->formatMilliseconds($mapped_duration);
		$data = array(
			'total_videos' => $total_videos,
			'total_duration' => $total_duration,
			'mapped_videos' => $mapped_videos,
			'mapped_duration' => $mapped_duration
		);
		echo json_encode($data);
		exit;
	}

	/**
	 * @purpose get the categories of a content
	 * @param authToken,content_category_value Mandatory, lang_code Optional
	 * @Author Biswajit Parida<biswajit@muvi.com>
	 */
	public function actionGetCategoriesOfContent() {
		$lang_code = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
		$language_id = Yii::app()->custom->getLanguage_id($lang_code);
		$studio_id = $this->studio_id;
		$sql = "SELECT id,binary_value,category_name,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
		$contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
		$contentCategories = CHtml::listData($contentCategory, 'binary_value', 'category_name');
		$content_category_value = $_REQUEST['content_category_value'];
		$categories = Yii::app()->Helper->categoryNameFromBvalue($content_category_value, $contentCategories);
		echo $categories;
		exit;
	}

	/**
	 * @purpose get the categories of a content
	 * @param authToken,user_id Mandatory
	 * @<ajit@muvi.com>
	 */
	public function actionMyLibrary() {
		$user_id = $_REQUEST['user_id'];
		$studio_id = $this->studio_id;
		$lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
		$translate = $this->getTransData($lang_code, $studio_id);
		if (isset($user_id) && intval($user_id) && $user_id != 0) {
			$transactions = Yii::app()->db->createCommand()
					->select('plan_id,subscription_id,ppv_subscription_id,transaction_type')
					->from('transactions')
					->where('studio_id = ' . $studio_id . ' AND user_id=' . $user_id . ' AND transaction_type IN (2,5,6)')
					->order('id DESC')
					->queryAll();

			$planModel = new PpvPlans();
			$library = array();
			$newarr = array();
			$free = array();
			foreach ($transactions as $key => $details) {
				$ppv_subscription_id = $details['ppv_subscription_id'];
				$ppv_subscription = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id));
				if ($details['transaction_type'] == 2) {
					$film = Film::model()->findByAttributes(array('id' => $ppv_subscription->movie_id));
					$movie_id = $ppv_subscription->movie_id;
					$end_date = $ppv_subscription->end_date ? $ppv_subscription->end_date == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $ppv_subscription->end_date : '0000-00-00 00:00:00';
					$season_id = $ppv_subscription->season_id;
					$episode_id = $ppv_subscription->episode_id;
					$movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
					$content_types_id = $film->content_types_id;
					$episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
					$movie_names = $movie_name;
					if ($season_id != 0 && $episode_id != 0) {
						$movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
					}
					if ($season_id == 0 && $episode_id != 0) {
						$movie_names = $movie_name . " -> " . $episode_name;
					}
					if ($season_id != 0 && $episode_id == 0) {
						$movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
					}
					$currdatetime = strtotime(date('y-m-d'));
					$expirytime = strtotime($end_date);
					$statusppv = 'N/A';
					if ($content_types_id == 1) {
						if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						}
						$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
						if ($end_date == '0000-00-00 00:00:00') {
							$statusppv = 'Active';
						}
					}
					if ($content_types_id == 4) {
						if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
							$statusppv = 'Active';
						}
						$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
						if ($end_date == '0000-00-00 00:00:00') {
							$statusppv = 'Active';
						}
					}
					if ($content_types_id == 3) {
						$datamovie_stream = Yii::app()->db->createCommand()
								->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
								->from('movie_streams ms ')
								->where('ms.id=' . $episode_id)
								->queryRow();
						$embed_id = $datamovie_stream['embed_id'];
						if ($currdatetime <= $expirytime && strtotime($end_date) != '0000-00-00 00:00:00') {
							$statusppv = 'Active';
						}
						if ($season_id != 0 && $episode_id == 0) {
							$statusppv = 'Active';
						}
						if ($end_date == '0000-00-00 00:00:00') {
							$statusppv = 'Active';
						}
						if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
							$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
						}
						if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
							$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
						}
						if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
							$baseurl = Yii::app()->getBaseUrl(true) . '/player/' . $film->permalink . '/stream/' . $embed_id;
						}
					}
					if ($statusppv == 'Active') {
						if ($content_types_id == 2) {
							$poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
						} else {
							$poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
						}
						if ($content_types_id == 3) {
							if ($episode_id != 0) {
								$stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
							} else if ($season_id != 0 && $episode_id == 0) {
								$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
							} else {
								$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
							}
						} else {
							$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
						}
						$library[$key]['name'] = $movie_names;
						$library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
						$library[$key]['movie_id'] = $movie_id;
						$library[$key]['movie_stream_id'] = $stm->id;
						$library[$key]['is_episode'] = ($episode_id != 0) ? '1' : isset($stm->is_episode) ? $stm->is_episode : 0;
						$library[$key]['muvi_uniq_id'] = $film->uniq_id;
						$library[$key]['content_types_id'] = $film->content_types_id;
						$library[$key]['content_type_id'] = $film->content_type_id;
						$library[$key]['baseurl'] = $baseurl;
						$library[$key]['poster_url'] = $poster;
						$library[$key]['video_duration'] = $stm->video_duration;
						$library[$key]['genres'] = json_decode($film->genre);
						$library[$key]['permalink'] = $film->permalink;
						$library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
						$library[$key]['full_movie'] = $stm->full_movie;
						$library[$key]['story'] = $film->story;
						;
						$library[$key]['release_date'] = $film->release_date;
						$library[$key]['is_converted'] = $stm->is_converted;
						$library[$key]['isFreeContent'] = 0;
						$library[$key]['season_id'] = $season_id;
					}
				} elseif ($details['transaction_type'] == 5) {
					$ppvplanid = PpvSubscription::model()->find(array('select' => 'timeframe_id', 'condition' => 'id=:id', 'params' => array(':id' => $ppv_subscription_id)))->timeframe_id;
					$ppvtimeframes = Ppvtimeframes::model()->find(array('select' => 'validity_days', 'condition' => 'id=:id', 'params' => array(':id' => $ppvplanid)))->validity_days;
					$ppvbundlesmovieid = Yii::app()->db->createCommand()
							->SELECT('content_id')
							->from('ppv_advance_content')
							->where('ppv_plan_id=' . $details['plan_id'])
							->queryAll();
					$expirydate = date('M d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
					$currdatetime = strtotime(date('y-m-d'));
					$expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
					if ($currdatetime <= $expirytime) {
						foreach ($ppvbundlesmovieid as $keyb => $contentid) {
							$movie_id = $contentid['content_id'];
							$film = Film::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $movie_id)));
							if ($film->content_types_id == 2) {
								$poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
							} else {
								$poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
							}
							$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));

							$library[$key]['name'] = $film->name;
							$library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
							$library[$key]['movie_id'] = $movie_id;
							$library[$key]['movie_stream_id'] = $stm->id;
							$library[$key]['is_episode'] = isset($stm->is_episode) ? $stm->is_episode : 0;
							$library[$key]['muvi_uniq_id'] = $film->uniq_id;
							$library[$key]['content_types_id'] = $film->content_types_id;
							$library[$key]['content_type_id'] = $film->content_type_id;
							$library[$key]['baseurl'] = $baseurl;
							$library[$key]['poster_url'] = $poster;
							$library[$key]['video_duration'] = $stm->video_duration;
							$library[$key]['genres'] = json_decode($film->genre);
							$library[$key]['permalink'] = $film->permalink;
							$library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
							$library[$key]['full_movie'] = $stm->full_movie;
							$library[$key]['story'] = $film->story;
							;
							$library[$key]['release_date'] = $film->release_date;
							$library[$key]['is_converted'] = $stm->is_converted;
							$library[$key]['isFreeContent'] = 0;
							$library[$key]['season_id'] = $stm->series_number;
						}
					}
				} elseif ($details['transaction_type'] == 6) {

					$ppv_subscription_id = $details['ppv_subscription_id'];
					$PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
					$PpvSubscription = Yii::app()->db->createCommand()
							->SELECT('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
							->from('ppv_subscriptions')
							->where('id=' . $ppv_subscription_id)
							->queryRow();
					$end_date = $PpvSubscription['end_date'] ? $PpvSubscription['end_date'] == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $PpvSubscription['end_date'] : '0000-00-00 00:00:00';
					$start_date = $PpvSubscription['start_date'];
					$season_id = $PpvSubscription['season_id'];
					$episode_id = $PpvSubscription['episode_id'];
					$movie_id = $PpvSubscription['movie_id'];
					$currdatetime = strtotime(date('y-m-d'));
					$expirytime = strtotime($end_date);
					$movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
					$episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
					$film = Film::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $movie_id)));
					$statusvoucher = 'N/A';
					if ($currdatetime <= $expirytime && $end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
						$statusvoucher = 'Active';
					}
					if ($end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
						$statusvoucher = 'Active';
					}
					if ($end_date == '0000-00-00 00:00:00') {
						$statusvoucher = 'Active';
					}
					if ($statusvoucher == 'Active') {
						$datamovie_stream = Yii::app()->db->createCommand()
								->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
								->from('movie_streams ms ')
								->where('ms.id=' . $episode_id)
								->queryRow();
						$embed_id = $datamovie_stream['embed_id'];
						$vmovie_names = $movie_name;

						if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
							$vmovie_names = $movie_name;
							$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
						}
						if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
							$vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
							$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '/stream/' . $embed_id;
						}
						if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
							$vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
							$baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
						}
						if ($film->content_types_id == 2) {
							$poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
						} else {
							$poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
						}

						if ($film->content_types_id == 3) {
							if ($episode_id != 0) {
								$stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
							} else if ($season_id != 0 && $episode_id == 0) {
								$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
							} else {
								$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
							}
						} else {
							$stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
						}

						$library[$key]['name'] = $vmovie_names;
						$library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
						$library[$key]['movie_id'] = $movie_id;
						$library[$key]['movie_stream_id'] = $stm->id;
						$library[$key]['is_episode'] = isset($datamovie_stream['is_episode']) ? $datamovie_stream['is_episode'] : 0;
						$library[$key]['muvi_uniq_id'] = $film->uniq_id;
						$library[$key]['content_types_id'] = $film->content_types_id;
						$library[$key]['content_type_id'] = $film->content_type_id;
						$library[$key]['baseurl'] = $baseurl;
						$library[$key]['poster_url'] = $poster;
						$library[$key]['video_duration'] = $stm->video_duration;
						$library[$key]['genres'] = json_decode($film->genre);
						$library[$key]['permalink'] = $film->permalink;
						$library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
						$library[$key]['full_movie'] = $stm->full_movie;
						$library[$key]['story'] = $film->story;
						;
						$library[$key]['release_date'] = $film->release_date;
						$library[$key]['is_converted'] = $stm->is_converted;
						$library[$key]['isFreeContent'] = 0;
						$library[$key]['season_id'] = $season_id;
					}
				}
			}
			$newarr = array_merge($library, $free);
			if ($newarr) {
				$data['code'] = 200;
				$data['status'] = "OK";
				$data['mylibrary'] = $newarr;
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = "Content not found.";
			}
		} else {
			$data['code'] = 400;
			$data['status'] = "Failure";
			$data['msg'] = "User id not found.";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}
    /**
     * @method public BlockedChatUserList : This function will return list of blocked chat users
     * @return json Return the json array of results
     * @author prakash<support@muvi.com>
     * @mantis id:11890
     * @param String authToken* : Auth token of the studio
     * @param Integer blocked_by* : user id who blocked
     * @param String lang_code : User language Code
     * 
     */
    public function actionBlockedChatUserList() {
        $blocked_by = isset($_REQUEST['blocked_by']) && $_REQUEST['blocked_by'] > 0 ? $_REQUEST['blocked_by'] : 0;
        if ($blocked_by) {
            $command = Yii::app()->db->createCommand("SELECT u.*,p.poster_file_name FROM(SELECT cb.id,cb.blocked_to,cb.device_type,cb.block_status,cb.updated_date,su.display_name FROM chat_user_block cb INNER JOIN sdk_users su ON cb.blocked_by!=0 AND cb.blocked_to=su.id AND cb.block_status=1 AND cb.studio_id={$this->studio_id} AND cb.blocked_by=:blocked_by LIMIT 300) as u LEFT JOIN posters p ON u.blocked_to=p.object_id AND p.object_type='profilepicture' ORDER BY u.updated_date DESC")->bindValue(':blocked_by', $blocked_by)->queryAll();
            $user_list = array();
            $cloudfront_url = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            foreach ($command as $key => $value) {
                $profile_image_name = $value['poster_file_name'];
                $user_id = $value['blocked_to'];
                $profile_picture_path = $this->getChatProfilePicture($user_id, $cloudfront_url, 'thumb', $profile_image_name, $this->studio_id);
                $value['poster_file_name'] = $profile_picture_path;
                $user_list[] = $value;
            }
            $this->code = 200;
            $this->items = $user_list;
        } else {
            $this->code = 639;
        }
    }

    /**
     * @method public BlockUnblockChatUser : This function is used to Block and UnBlock chat user
     * @return json Return the json array of results
     * @author prakash<support@muvi.com>
     * @mantis id:11890
     * @param String authToken* : Auth token of the studio
     * @param Integer blocked_by* : user id who blocked
     * @param Integer blocked_to* : user id whom to blocked
     * @param Integer device_type* : Blocked from which device (1->web,2->android,3->ios,4->roku)
     * @param Integer block_status : 1->blocked 0->unblock
     * @param String lang_code : User language Code
     * 
     */
    public function actionBlockUnblockChatUser() {
        $device_type = isset($_REQUEST['device_type']) && $_REQUEST['device_type'] > 0 && $_REQUEST['device_type'] <= 4 ? $_REQUEST['device_type'] : 0;
        $suc_flag = 0;
        if ($device_type) {
            $blocked_by = isset($_REQUEST['blocked_by']) && $_REQUEST['blocked_by'] > 0 ? $_REQUEST['blocked_by'] : 0;
            if ($blocked_by) {
                $blocked_to = isset($_REQUEST['blocked_to']) && $_REQUEST['blocked_to'] > 0 ? $_REQUEST['blocked_to'] : 0;
                if ($blocked_to) {
                    $block_status = isset($_REQUEST['block_status']) && $_REQUEST['block_status'] > 0 ? 1 : 0;
                    $chat_user_block_data = ChatUserBlock::model()->findByAttributes(array(), 'studio_id=:studio_id AND blocked_by=:blocked_by AND blocked_to=:blocked_to', array(':studio_id' => $this->studio_id, ':blocked_by' => $blocked_by, ':blocked_to' => $blocked_to));
                    if ($chat_user_block_data) {
                        $chat_user_block_data->device_type = $device_type;
                        $chat_user_block_data->block_status = $block_status;
                        $chat_user_block_data->ip_address = CHttpRequest::getUserHostAddress();
                        $chat_user_block_data->updated_date = date('Y-m-d H:i:s');
                        $chat_user_block_data->save();
                        $suc_flag = 1;
                    } else {
                        //validate actual blocked by userid is available or not
                        $chk_blocked_by_user = SdkUser::model()->countByAttributes(array(), 'studio_id=:studio_id AND id=:blocked_by', array(':studio_id' => $this->studio_id, ':blocked_by' => $blocked_by));
                        if ($chk_blocked_by_user) {
                            $chk_blocked_to_user = SdkUser::model()->countByAttributes(array(), 'studio_id=:studio_id AND id=:blocked_to', array(':studio_id' => $this->studio_id, ':blocked_to' => $blocked_to));
                            if ($chk_blocked_to_user) {
                                $chat_user_block_data = new ChatUserBlock();
                                $chat_user_block_data->studio_id = $this->studio_id;
                                $chat_user_block_data->blocked_by = $blocked_by;
                                $chat_user_block_data->blocked_to = $blocked_to;
                                $chat_user_block_data->device_type = $device_type;
                                $chat_user_block_data->block_status = $block_status;
                                $chat_user_block_data->ip_address = CHttpRequest::getUserHostAddress();
                                $chat_user_block_data->created_date = date('Y-m-d H:i:s');
                                $chat_user_block_data->updated_date = date('Y-m-d H:i:s');
                                $chat_user_block_data->save();
                                $suc_flag = 1;
                            } else {
                                $this->code = 743;
                            }
                        } else {
                            $this->code = 742;
                        }
                    }
                    if ($suc_flag) {
                        $this->code = 200;
                        $this->msg_code = $block_status ? 'chat_user_blocked_succ' : 'chat_user_unblock_succ';
                    }
                } else {
                    $this->code = 639;
                }
            } else {
                $this->code = 639;
            }
        } else {
            $this->code = 741;
        }
    }

};
