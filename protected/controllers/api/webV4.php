<?php
trait WebV4 {

    protected function actionRating($content_id) {
        $ratings = Yii::app()->db->createCommand()
                ->select('ROUND(AVG(rating),1) as rating')
                ->from('content_ratings')
                ->where('content_id=:content_id AND studio_id=:studio_id AND status=1', array(':content_id' => $content_id, ':studio_id' => $this->studio_id))
                ->queryRow();
        return $ratings['rating'] == 0 ? 0 : $ratings['rating'];
    }

    public function actionReviews() {
        if ($_REQUEST['content_id']) {
            $content_id = $_REQUEST['content_id'];
            $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 5;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $sql = "SELECT r.*, u.display_name FROM content_ratings r LEFT JOIN sdk_users u ON r.user_id=u.id WHERE r.studio_id =" . $this->studio_id .
                    " AND r.content_id=$content_id AND r.status=1 ORDER BY r.created_date DESC LIMIT $offset, $limit";
            $ratings = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach ($ratings as $rating) {
                $review[$i]['rating'] = $rating["rating"];
                $review[$i]['profile_pic'] = $this->getProfilePicture($rating['user_id'], 'profilepicture');
                $review[$i]['content'] = substr($rating['review'], 0, 50);
                $review[$i]['user_name'] = $rating['display_name'];
                $review[$i]['date'] = Yii::app()->common->YYMMDD($rating['created_date']);
                $i++;
            }
            $data['code'] = 200;
            $data['msg'] = 'OK';
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = count($ratings);
            $data['review'] = $review;
        } else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = "Content permalink is required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public Savereview()
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @return json message of success or failure 
     * @param int $content_id
     * @param int $rating
     * @param string $review_comment 
     * @param int $user_id
     */
    public function actionSavereview() {
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] > 0 && $_REQUEST['user_id'] > 0) {
            $rating = new ContentRating();
            $rate = $rating->findAllByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
            if (count($rate) > 0) {
                $data['code'] = 456;
                $data['status'] = "Already Reviewed";
                $data['msg'] = "Your have already added your review to this content.";
            } else {
                if (isset($_REQUEST['rating']) && ($_REQUEST['rating'] > 0) && ($_REQUEST['rating'] < 5)) {
                    $rating->content_id = $_REQUEST['content_id'];
                    $rating->rating = round($_REQUEST['rating'], 1);
                    $rating->review = nl2br(addslashes($_REQUEST['review_comment']));
                    $rating->created_date = new CDbExpression("NOW()");
                    $rating->studio_id = $this->studio_id;
                    $rating->user_ip = Yii::app()->getRequest()->getUserHostAddress();
                    $rating->user_id = $_REQUEST['user_id'];
                    $rating->status = 1;
                    $rating->save();
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Your review is saved successfully.";
                } else {
                    $data['code'] = 455;
                    $data['status'] = "Failure";
                    $data['msg'] = "Please rate the content , and should be within 0 to 5";
                }
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "Invalid Login Details";
            $data['msg'] = "Ensure you are logged in and you have filled all fields.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public GetBannerList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerList() {
        $banners = StudioBanner::model()->findAllByAttributes(array('studio_id' => $this->studio_id,  'is_published' => 1), array('order' => 'id_seq ASC'));
        if ($banners) {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            foreach ($banners AS $key => $banner) {
                $banner_src = $banner->image_name;
                $banner_title = $banner->title;
                $banner_id = $banner->id;
                $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode($banner_src);
            }
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id DESC'));
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
        } else {
            $data['code'] = 434;
            $data['status'] = 'Failure';
            $data['msg'] = 'No banner uploaded';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public GetBannerSectionList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerSectionList($nojson = 1) {
        $studio_id = $this->studio_id;
        $sql = "SELECT t.id FROM studios s,templates t WHERE s.parent_theme =t.code AND s.id =" . $studio_id;
        $res = Yii::app()->db->createCommand($sql)->queryRow();
        $template_id = $res['id'];
        $banner_secs = BannerSection::model()->with(array('banners' => array('alias' => 'bn')))->findAll(array('condition' => 't.template_id=' . $template_id . ' and bn.studio_id = ' . $studio_id, 'order' => 'bn.id_seq ASC, bn.id DESC'));
        if ($banner_secs) {
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
            $data['code'] = 200;
            $data['status'] = 'OK';
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            if ($template_id == 10) {
                $count = 0;
                foreach ($banner_secs AS $key => $banner) {
                    $bnr = $banner->banners;
                    $banner_src = trim($bnr[0]->image_name);
                    $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$count]['original'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                    $data['banners'][$count]['mobile_view'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . urlencode($banner_src);
                    $count++;
                }
            } else {
                $bannerproperties = array();
                $bannerProp = Yii::app()->db->createCommand()
                    ->select("*")
                    ->from('studio_banner_properties')
                    ->where('studio_id=:studio_id AND (language_id=:language_id OR parent_id =0) AND id NOT IN (SELECT parent_id FROM studio_banner_properties WHERE studio_id=:studio_id AND language_id=:language_id)', array(':studio_id' => $studio_id,':language_id'=>$language_id))
                    ->setFetchMode(PDO::FETCH_OBJ)->queryAll();
                if(!empty($bannerProp)){
                    foreach($bannerProp as $bannerPropTemp){
                        if(!empty($bannerPropTemp->banner_id))
                            $bannerproperties[$bannerPropTemp->banner_id] = $bannerPropTemp;
                    }
                }
                $banners = $banner_secs[0]->banners;
                foreach ($banners AS $key => $banner) {
                    $banner_src = trim($banner->image_name);
                    $bannerType = $banner->banner_type;
                    $bannerID = $banner->id;
                    $bannerOriginalSrc = "";
                    $bannerOriMobileViewSrc = "";
                    $videoRemoteURL = "";
                    $videoPlaceholderImg = "";
                    $videoPlaceholderThumbImg = "";
                    if($bannerType == 1){
                        $bannerOriginalSrc = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . urlencode($banner_src);
                        $bannerOriMobileViewSrc = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . urlencode($banner_src);
                    }else{
                       $videoRemoteURL = $banner->video_remote_url;
                       $videoPlaceholderImg = $banner->video_placeholder_img;
                       $videoPlaceholderThumbImg = $banner->video_placeholder_thumb_img;
                }
                    $data['banner_url'][] = $bannerOriginalSrc;
                    $data['banners'][$key]['banner_type'] = $bannerType;
                    $data['banners'][$key]['original'] = $bannerOriginalSrc;
                    $data['banners'][$key]['mobile_view'] = $bannerOriMobileViewSrc;
                    $data['banners'][$key]['video_remote_url'] = $videoRemoteURL;
                    $data['banners'][$key]['video_placeholder_img'] = $videoPlaceholderImg;
                    $data['banners'][$key]['video_placeholder_thumb_img']= $videoPlaceholderThumbImg;
                    $data['banners'][$key]['banner_text_heading']=$bannerproperties[$bannerID]->banner_text_heading;
                    $data['banners'][$key]['banner_text_description']=$bannerproperties[$bannerID]->banner_text_description;
                    $data['banners'][$key]['banner_button_text']=$bannerproperties[$bannerID]->banner_button_text;
                    $data['banners'][$key]['banner_button_link']=$bannerproperties[$bannerID]->banner_button_link;
                    $data['banners'][$key]['banner_button_type']=$bannerproperties[$bannerID]->banner_button_type;
            }
            }
            $bannerStyle = StudioBannerStyle::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'banner_style_code'));
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
            $data['banner_style'] = $bannerStyle['banner_style_code'];
        } else {
            $data['code'] = 434;
            $data['status'] = 'Failure';
            $data['msg'] = 'No banner uploaded';
        }
        if ($nojson == 1) {
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        } else {
            return $data;
        }
    }

    /**
     * @method getmenuList() Returns the list of content Categories
     * @param String $authToken A authToken
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetMenuList() {
        $list = array();
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $menuType = 'top'; // For web only
        if ((@$_REQUEST['menutype'] || @$_REQUEST['menuType'])) {
            $menuType = @$_REQUEST['menutype'] ? $_REQUEST['menutype'] : $_REQUEST['menuType'];
        }
        $sql = "SELECT mi.id,mi.link_type,mi.parent_id,mi.title as display_name,mi.permalink FROM menu_items mi,menus m WHERE mi.menu_id = m.id AND m.position='{$menuType}' AND mi.studio_id={$this->studio_id} AND language_parent_id = 0 ORDER BY parent_id,id_seq ASC";
        $list = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $list = Yii::app()->custom->getTranslatedMenuList($this->studio_id, $language_id, $list);
        }
        $finalArr = array();
        if ($list) {
            foreach ($list AS $key => $val) {
                if ($val['link_type'] == 1) {
                    if (strstr($val['permalink'], 'http://') || strstr($val['permalink'], 'httpS://')) {
                        $list[$key]['web_url'] = $val['permalink'];
                    } else {
                        $list[$key]['web_url'] = 'http://' . $defaultdomain . '/page/' . $val['permalink'];
                    }
                } elseif ($val['permalink'] == '#') {
                    $list[$key]['web_url'] = 'http://' . $defaultdomain;
                } else {
                    if (strstr($val['permalink'], 'http://') || strstr($val['permalink'], 'httpS://')) {
                        $list[$key]['web_url'] = $val['permalink'];
                    } else {
                        $list[$key]['web_url'] = 'http://' . $defaultdomain . '/' . $val['permalink'];
                    }
                }
                if ($val['parent_id'] == 0) {
                    $parentArr[] = $list[$key];
                } else {
                    $childArr[$val['parent_id']][] = $list[$key];
                }
            }
            $finalArr = @$parentArr;
            foreach (@$parentArr AS $key => $val) {
                if (@$childArr[$val['id']]) {
                    $finalArr = array_merge($finalArr, $childArr[$val['id']]);
                }
            }
        }
        $sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$this->studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
        $pages = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $pages = Yii::app()->custom->getStaticPages($this->studio_id, $language_id, $pages);
        }
        $defaultdomain = @$pages[0]['domain'];
        if (trim($defaultdomain) == "") {
            $studio_details = Studios::model()->findByPk($this->studio_id, array('select' => 'domain'));
            $defaultdomain = $studio_details->domain;
        }
        $default = array(0 => 0, 'link_type' => 'internal', 'display_name' => $translate['contact_us'], 'permalink' => 'contactus', 'url' => 'http://' . $defaultdomain . '/contactus');
        array_push($pages, $default);

        $data['status'] = 'OK';
        $data['code'] = 200;
        $data['msg'] = $translate['success'];
        $data['total_menu'] = count(@$list);
        $data['menu'] = $finalArr;
        $data['footer_menu'] = $pages;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /*
     * 6204: Featured Content API
     * ajit@muvi.com
     */

    public function actionHomepage() {
        $data['BannerSectionList'] = $this->actiongetBannerSectionList(0);
        $data['SectionName'] = $this->actiongetSectionName(0);
        $this->setHeader($data['SectionName']['code']);
        echo json_encode($data);
        exit;
    }

    public function actiongetSectionName($nojson = 1) {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $sections = FeaturedSection::model()->findAll('studio_id=:studio_id AND is_active=:is_active AND parent_id = 0 ORDER BY id_seq', array(':studio_id' => $studio_id, ':is_active' => 1));
        if ($language_id != 20) {
            $translated = CHtml::listData(FeaturedSection::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select' => 'parent_id,title')), 'parent_id', 'title');
            if (!empty($sections)) {
                foreach ($sections as $section) {
                    if (array_key_exists($section->id, $translated)) {
                        $section->title = $translated[$section->id];
                    }
                    $sectionsdetail[] = $section;
                }
            }
        } else {
            $sectionsdetail = $sections;
        }
        if ($sectionsdetail) {
            foreach ($sectionsdetail as $key => $sec) {
                $fsections[$key]['studio_id'] = $sec->studio_id;
                $fsections[$key]['language_id'] = $sec->language_id;
                $fsections[$key]['title'] = $sec->title;
                $fsections[$key]['section_id'] = $sec->id;
                $total_content = FeaturedContent::model()->countByAttributes(array(
                    'section_id' => $sec->id,
                    'studio_id' => $studio_id
                ));
                $fsections[$key]['total'] = $total_content;
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['section'] = @$fsections;
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Section not found";
        }

        if ($nojson == 1) {
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        } else {
            return $data;
        }
    }

    public function actiongetFeaturedContent() {
        if(HOST_IP != '52.0.64.95' || $_SERVER['HTTP_HOST'] != '52.0.64.95'){
            $studio_id = $this->studio_id;
            $section_id = $_REQUEST['section_id'];
            $lang_code = @$_REQUEST['lang_code'];
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $controller = Yii::app()->controller;
            $sql = "SELECT * FROM homepage WHERE studio_id =" . $studio_id . " AND section_id=" . $section_id . " ORDER BY id_seq ASC";
            $con = Yii::app()->db;
            $movieids = '';
            $fecontents = $con->createCommand($sql)->queryAll();
            $keycounter = 0;
            $section = FeaturedSection::model()->findByPk($section_id);
            if($language_id !=20):
                $trans_section = FeaturedSection::model()->findByAttributes(array('parent_id'=>$section_id, 'language_id'=>$language_id,'studio_id'=>$studio_id))->title;
                if($trans_section):
                    $section->title = $trans_section;
                endif;
            endif;         
            if ($fecontents) {
                $domainName = $this->getDomainName();
                $restriction = StudioContentRestriction::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                    $country = $_REQUEST['country'];
                } else {
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $country = $visitor_loc['country'];
                }
                foreach ($fecontents AS $key => $val) {
                    $langcontent = Yii::app()->custom->getTranslatedContent($val['movie_id'], $val['is_episode'], $language_id, $studio_id);
                    $content[$keycon]['is_episode'] = $val['is_episode'];
                    if ($val['is_episode'] == 2) {
                        $sql = "SELECT P.* FROM pg_product P WHERE P.id = '" . $val['movie_id'] . "'";
                        $standaloneproduct = $con->createCommand($sql)->queryAll();
                        if ($standaloneproduct) {
                            foreach ($standaloneproduct AS $key => $val1) {
                                if (Yii::app()->common->isGeoBlockPGContent($val1['id'])) {
                                    $cont_name = $val1['name'];
                                    $story = $val1['description'];
                                    $permalink = $val1['permalink'];
                                    $short_story = substr(Yii::app()->common->htmlchars_encode_to_html($story), 0, 80);
                                    $tempprice = Yii::app()->common->getPGPrices($val['id'], Yii::app()->controller->studio->default_currency_id);
                                    if (!empty($tempprice)) {
                                        $standaloneproduct[$key]['sale_price'] = $tempprice['price'];
                                        $standaloneproduct[$key]['currency_id'] = $tempprice['currency_id'];
                                    }
                                    $poster = PGProduct::getpgImage($val1['id'], 'standard');
                                    $formatted_price = Yii::app()->common->formatPrice($standaloneproduct[$key]['sale_price'], $standaloneproduct[$key]['currency_id']);

                                    $final_content[$key] = array(
                                        'movie_id' => $movie_id,
                                        'title' => $cont_name,
                                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                        'poster' => $poster,
                                        'data_type' => 4,
                                        'uniq_id' => $movie_uniq_id,
                                        'short_story' => utf8_encode($short_story),
                                        'price' => $formatted_price,
                                        'status' => $val['status'],
                                        'is_episode' => 2
                                    );
                                }
                            }
                        }
                    } else {
                        /* if($geoData && in_array($val['movie_id'], $geoData)){
                          continue;
                          } */
                        if (Yii::app()->common->isGeoBlockContent(@$val['movie_id'], 0, $studio_id, $country)) {
                            $arg['studio_id'] = $studio_id;
                            $arg['movie_id'] = $val['movie_id'];
                            $arg['season_id'] = 0;
                            $arg['episode_id'] = 0;
                            $arg['content_types_id'] = $content_types_id = $val['content_types_id'];
                            $isFreeContent = Yii::app()->common->isFreeContent($arg);
                            $embededurl = $domainName . '/embed/' . $val['embed_id'];
                            if ($val['is_episode'] == 1) {
                                if (array_key_exists($val['movie_id'], $langcontent['episode'])) {
                                    $val['episode_title'] = $langcontent['episode'][$val['movie_id']]->episode_title;
                                    $val['episode_story'] = $langcontent['episode'][$val['movie_id']]->episode_story;
                                }
                                $cont_name = ($val['episode_title'] != '') ? $val['episode_title'] : "SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                                $story = $val['episode_story'];
                                $release = $val['episode_date'];
                                $poster = $controller->getPoster($val['stream_id'], 'moviestream', 'episode', $this->studio_id);
                            } else {
                                if (array_key_exists($val['movie_id'], $langcontent['film'])) {
                                    $val['name'] = $langcontent['film'][$val['movie_id']]->name;
                                    $val['story'] = $langcontent['film'][$val['movie_id']]->story;
                                    $val['genre'] = $langcontent['film'][$val['movie_id']]->genre;
                                    $val['censor_rating'] = $langcontent['film'][$val['movie_id']]->censor_rating;
                                    $val['language'] = $langcontent['film'][$val['movie_id']]->language;
                                }
                                $cont_name = $val['name'];
                                $story = $val['story'];
                                $release = $val['release_date'];
                                $stream = movieStreams::model()->findByPk($val['stream_id']);
                                if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                                    $poster = $controller->getPoster($val['movie_id'], 'films', 'episode', $this->studio_id);
                                } else {
                                    $poster = $controller->getPoster($val['movie_id'], 'films', 'standard', $this->studio_id);
                                }
                            }
                            $geo = GeoblockContent::model()->find('movie_id=:movie_id', array(':movie_id' => $val['movie_id']));

                            //Get view count status
                            $viewStatus = VideoLogs::model()->getViewStatus($val['movie_id'], $studio_id);
                            $viewStatusArr = array();
                            if (@$viewStatus) {
                                foreach ($viewStatus AS $valarr) {
                                    $viewStatusArr['viewcount'] = $valarr['viewcount'];
                                    $viewStatusArr['uniq_view_count'] = $valarr['u_viewcount'];
                                }
                            }
                            if (isset($stream['content_publish_date']) && @$stream['content_publish_date'] && (strtotime($stream['content_publish_date']) > time())) {

                            } else {
                                $final_content[$keycounter]['is_episode'] = $val['is_episode'];
                                $final_content[$keycounter]['movie_stream_uniq_id'] = $val['embed_id'];
                                $final_content[$keycounter]['movie_id'] = $val['movie_id'];
                                $final_content[$keycounter]['movie_stream_id'] = $val['stream_id'];
                                $final_content[$keycounter]['muvi_uniq_id'] = $val['uniq_id'];
                                $final_content[$keycounter]['content_type_id'] = $val['content_type_id'];
                                $final_content[$keycounter]['ppv_plan_id'] = $val['ppv_plan_id'];
                                $final_content[$keycounter]['permalink'] = $val['fplink'];
                                $final_content[$keycounter]['name'] = $cont_name;
                                $final_content[$keycounter]['full_movie'] = $stream->full_movie;
                                $final_content[$keycounter]['story'] = $story;
                                $final_content[$keycounter]['genre'] = json_decode($val['genre']);
                                $final_content[$keycounter]['censor_rating'] = ($val['censor_rating'] != '') ? implode(',', json_decode($val['censor_rating'])) . '' : '';
                                $final_content[$keycounter]['release_date'] = $release;
                                $final_content[$keycounter]['content_types_id'] = $val['content_types_id'];
                                $final_content[$keycounter]['is_converted'] = $val['is_converted'];
                                $final_content[$keycounter]['last_updated_date'] = '';
                                $final_content[$keycounter]['movieid'] = $geo->movie_id;
                                $final_content[$keycounter]['geocategory_id'] = $geo->geocategory_id;
                                $final_content[$keycounter]['category_id'] = $restriction->category_id;
                                $final_content[$keycounter]['studio_id'] = $studio_id;
                                $final_content[$keycounter]['country_code'] = $restriction->country_code;
                                $final_content[$keycounter]['ip'] = $restriction->ip;
                                $final_content[$keycounter]['poster_url'] = $poster;
                                $final_content[$keycounter]['isFreeContent'] = $isFreeContent;
                                $final_content[$keycounter]['embeddedUrl'] = $embededurl;
                                $final_content[$keycounter]['viewStatus'] = $viewStatusArr;
                                $keycounter++;
                            }
                        }
                    }
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = (array)$final_content;
                $data['title'] = (string)$section->title;              
            } else {
                $data['code'] = 448;
                $data['status'] = "Error";
                $data['msg'] = "Featured content not found";
            }
        }else{
           $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Featured content not found"; 
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionTextTranslation() {
        $studio_id = $this->studio_id;
        if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") {
            $lang_code = $_REQUEST['lang_code'];
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $studio = Studio::model()->findByPk($studio_id, array('select' => 'theme'));
            $theme = $studio->theme;
            $lang = TranslateKeyword::model()->getAllTranslatedMessages($studio_id, $lang_code, $language_id, $theme);
            $lang = self::getFullLangTranslation($lang); 
            $language = Yii::app()->controller->getAllTranslation($lang, $studio_id);
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['translation'] = $language;
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Language code not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionGetLanguageList() {
        $studio_id = $this->studio_id;
        $data['code'] = 200;
        $data['status'] = "OK";
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.translated_name,sl.status,sl.frontend_show_status FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE  l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";

        $studio_languages = $con->createCommand($sql)->queryAll();
        $lang_list = array();
        $i = 0;
        foreach ($studio_languages as $lang) {
            if (($lang['frontend_show_status'] != "0") && ($lang['status'] == 1 || $lang['code'] == 'en')) {
                if (trim($lang['translated_name']) != "") {
                    $lang['name'] = $lang['translated_name'];
                }
                $lang_list[$i]['code'] = $lang['code'];
                $lang_list[$i]['language'] = $lang['name'];
                $i++;
            }
        }
        $data['lang_list'] = $lang_list;
        $data['default_lang'] = $studio_languages[0]['default_language'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method actionGetMenus() Returns the list of menus in tree structure
     * @param String $authToken A authToken
     * @author Ratikanta<support@muvi.com>
     * @return Returns the list of menus in tree structure
     */
    function actionGetMenus() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $mainmenu = Yii::app()->custom->getMainMenuStructure('array', $language_id, $studio_id);
        $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        $usermenu = Yii::app()->custom->getUserMenuStructure('array', $language_id, $studio_id, $user_id, $translate);
        $sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
        $pages = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $pages = Yii::app()->custom->getStaticPages($studio_id, $language_id, $pages);
        }
        $org_menu = array('mainmenu' => $mainmenu, 'usermenu' => $usermenu, 'footer_menu' => $pages);
        if ($org_menu) {
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
            $data['menus'] = $org_menu;
        } else {
            $data['code'] = 447;
            $data['status'] = "Error";
            $data['msg'] = "No Menu found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionGetStaticPagedetails() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        if (isset($_REQUEST['permalink']) && $_REQUEST['permalink'] != "") {
            $permalink = $_REQUEST['permalink'];
            $sql = "SELECT * FROM pages where studio_id=" . $studio_id . " AND permalink ='" . $permalink . "' AND status=1 AND parent_id=0";
            $page = Yii::app()->db->createCommand($sql)->queryRow();
            if (!empty($page)) {
                if ($language_id != 20) {
                    $page = Yii::app()->custom->getStaticPageDetails($studio_id, $language_id, $page);
                }
                $page['content'] = html_entity_decode($page['content']);
                $data['status'] = 'OK';
                $data['code'] = 200;
                $data['msg'] = 'Success';
                $data['page_details'] = $page;
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Content not found.";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Required parameters not found";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionContactUs() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['email']) && trim($_REQUEST['email']) != "") {
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $studio = Studio::model()->findByPk($studio_id, array('select' => 'name,contact_us_email,domain'));
            $subject = $studio->name . ' :: New Contact Us';
            $site_url = "https://" . $studio->domain;
            $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);
            $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
            $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
            $msg =$originalMessage = htmlspecialchars(@$_REQUEST['message']);
            $useremail = @$_REQUEST['email'];
            $username = '';
            if (isset($_REQUEST['name']) && strlen(trim($_REQUEST['name'])) > 0) {
                $username = htmlspecialchars($_REQUEST['name']);
            }
            $template_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                "message" => $msg
            );
            if ($studio->contact_us_email) {
                $admin_to [] = $studio_email = $studio->contact_us_email;
                if (strstr($studio_email, ',')) {
                    $admin_to = explode(',', $studio_email);
                    $studio_email = $admin_to[0];
                }
            } else {
                $studio_user = User::model()->findByAttributes(array('studio_id' => $studio_id));
                $studio_email = $studio_user->email;
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }
                $admin_to[] = $studio_email;
            }
            $studio_name = $StudioName = $studio->name;
            $EndUserName = $username;
            $admin_subject = $subject;
            $admin_from = $useremail;
            $email_type = 'contact_us';
            $admin_cc = Yii::app()->common->emailNotificationLinks($studio_id, $email_type);
            if (empty($admin_cc) || $admin_cc != "") {
                $admin_cc = array();
            }
            $user_to = array($useremail);
            $user_from = $studio_email;
            $content = "SELECT * FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND (language_id = " . $language_id . " OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND language_id = " . $language_id . "))";
            $data = Yii::app()->db->createCommand($content)->queryAll();
            if (empty($data)) {
                $temp = New NotificationTemplates;
                $data = $temp->findAllByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $user_subject = $data[0]['subject'];
            eval("\$user_subject = \"$user_subject\";");
            $user_content = htmlspecialchars($data[0]['content']);
            eval("\$user_content = \"$user_content\";");
            $user_content = htmlspecialchars_decode($user_content);
            $template_user_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                'content' => $user_content
            );
            Yii::app()->theme = 'bootstrap';
            $admin_html = Yii::app()->controller->renderPartial('//email/studio_contactus', array('params' => $template_content), true);
            $user_html = Yii::app()->controller->renderPartial('//email/studio_contactus_user', array('params' => $template_user_content), true);
            $returnVal_admin = $this->sendmailViaAmazonsdk($admin_html, $admin_subject, $admin_to, $admin_from, $admin_cc, '', '', $username);
            $returnVal_user = $this->sendmailViaAmazonsdk($user_html, $user_subject, $user_to, $user_from, '', '', '', $studio_name);
            $contact['status'] = 'OK';
            $contact['code'] = 200;
            $contact['msg'] = 'Success';
            $contact['success_msg'] = $translate['thanks_for_contact'];
        } else {
            $contact['code'] = 448;
            $contact['status'] = "Failure";
            $contact['msg'] = "Failure";
            $contact['error_msg'] = $translate['oops_invalid_email'];
        }
        $this->setHeader($contact['code']);
        echo json_encode($contact);
        exit;
    }

    public function actionGetFilteredContent() {
        if (isset($_REQUEST)) {
            $list = array();
            $cat = array();
            $order = true;
            $studio_id = $this->studio_id;
            $lang_code = @$_REQUEST['lang_code'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $is_episode = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] != "") ? $_REQUEST['is_episode'] : "";
            $group_by_release = (isset($_REQUEST['group_by_release']) && $_REQUEST['group_by_release'] != "") ? $_REQUEST['group_by_release'] : "";
            $group_by_starttime = (isset($_REQUEST['group_by_starttime']) && $_REQUEST['group_by_starttime'] != "") ? $_REQUEST['group_by_starttime'] : "";
            $translate = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_category_value = 0;
            $condition = "";
            $sort_by = 'F.name';
            $sort_cond = $sort_by;
            $order_by = 'ASC';
            $cond = '';
            $group_by = (isset($_REQUEST['group_by']) && $_REQUEST['group_by'] != '') ? $_REQUEST['group_by'] : '';
            if (isset($_REQUEST['category']) && $_REQUEST['category'] != "") {
                $categories = $_REQUEST['category'];
                if (!is_array($categories)) {
                    $categories = explode(",", $categories);
                }
                $permalink = "";
                if (!empty($categories)) {
                    $cond = " AND permalink IN ";
                    $total = count($categories);
                    for ($i = 0; $i < $total; $i++) {
                        $comma = " ";
                        if ($total != ($i + 1)) {
                            $comma = ",";
                        }
                        $permalink .= "'" . $categories[$i] . "'";
                        $permalink .= $comma;
                    }
                    $cond .= " (" . $permalink . ") ";
                }
                $sql = "SELECT GROUP_CONCAT(id) as content_category_value FROM content_category WHERE parent_id =0 AND studio_id=" . $studio_id . " " . $cond;
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($res)) {
                    $content_category_value = $res['content_category_value'];
                }
            }

                        //Filter by Subcategory
           if (isset($_REQUEST['subcategory']) && ($_REQUEST['subcategory'] != '')) {
                $subcategories = $_REQUEST['subcategory'];
                if (!is_array($subcategories)) {
                    $subcat = explode(",", $subcategories);
                }
                $subpermalink = "";
                if (!empty($subcat)) {
                    $cond = " AND permalink IN ";
                    $totalsub = count($subcat);
                    for ($k = 0; $k < $totalsub; $k++) {
                        $comma = " ";
                        if ($totalsub != ($k + 1)) {
                            $comma = ",";
                        }
                        $subpermalink .= "'" . $subcat[$k] . "'";
                        $subpermalink .= $comma;
                    }
                    $cond .= " (" . $subpermalink . ") ";
                }
                $sql = "SELECT GROUP_CONCAT(category_value) as content_category_value FROM content_subcategory WHERE studio_id=" . $studio_id . " " . $cond;
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($res)) {
                    $content_category_value = $res['content_category_value'];
                }
            }            


            if ($group_by == "category") {
                $sql = "SELECT id,category_name,permalink,binary_value FROM content_category WHERE parent_id =0 AND studio_id=" . $studio_id . "  " . $cond . " ORDER BY category_name ASC";
                $cat = Yii::app()->db->createCommand($sql)->queryAll();
            }
            if ($content_category_value) {
                foreach (explode(',', $content_category_value) as $key => $value) {
                    $condition1 .= " FIND_IN_SET($value, F.content_category_value) OR ";
                }
                $condition .= " AND (" . rtrim($condition1, 'OR ') . ") ";
            }
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if (isset($_REQUEST['order'])) {
                $order_by = $_REQUEST['order'];
            }
            if (isset($_REQUEST['sort'])) {
                $sort = explode(',', $_REQUEST['sort']);
                $sort_cond = array();
                foreach ($sort as $fvalue) {
                    $fvalue = explode(':', $fvalue);
                    $sort_value = $fvalue[0];
                    $sort_order = "";
                    if (count($fvalue) > 1) {
                        $order = false;
                        $sort_order = " " . $fvalue[1];
                    }
                    $fields = $this->GetTableName($sort_value);
                    $ts = ($fields['table'] == "movie_streams") ? "M" : "F";
                    if (trim($fields['field']) != "") {
                        $field_name = $ts . "." . $fields['field'];
                        if ((trim($sort_order) == "" && !($order))) {
                            $sort_order = " " . $order_by;
                        }
                        $sort_cond[] = $field_name . $sort_order;
                    }
                }
                $sort_by = implode(',', $sort_cond);
            }
            if ($order == true) {
                $order = $sort_by . " " . $order_by;
            } else {
                $order = $sort_by;
            }
            $filter_cond = "";
            foreach ($_REQUEST as $key => $value) {
                $get_filter_details = $this->GetTableName($key);
                if (!empty($get_filter_details)) {
                    if (trim($value) != "") {
                        $ts = ($get_filter_details['table'] == "movie_streams") ? "M" : "F";
                        $field_name = $ts . "." . $get_filter_details['field'];
                        $fdata = explode(',', trim($value));
                        $total = count($fdata);
                        for ($i = 0; $i < $total; $i++) {
                            $andor = ($i == 0) ? ' AND (' : ' OR';
                            $filter_cond .= $andor . ' ' . $field_name . ' LIKE \'%' . $fdata[$i] . "%' ";
                        }
                        $filter_cond .= ") ";
                    }
                }
            }
            $diff_days = isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0 ? $_REQUEST['before_days'] : 7;
            $diff_day = $diff_days + 1 . "days";
            if (isset($_REQUEST['live_field']) && in_array($_REQUEST['live_field'], array('start_time'))) {
                $date_today = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
                $filter_cond .= " AND " . $_REQUEST['live_field'] . " >='" . $date_today . "'";
            }
            if (isset($_REQUEST['live_range']) && in_array($_REQUEST['live_range'], array('start_time'))) {
                $date_today = date('Y-m-d', strtotime(date('Y-m-d')));
                $date_to = date('Y-m-d', strtotime(date('Y-m-d') . "-1 day"));
                $date_from = date('Y-m-d', strtotime(date('Y-m-d') . "-" . $diff_day));
                $filter_cond .= " AND " . $_REQUEST['live_range'] . " >='" . $date_from . "' AND " . $_REQUEST['live_range'] . " <='" . $date_to . "'";
            }
            if (isset($_REQUEST['range_field']) && in_array($_REQUEST['range_field'], array('release_date'))) {
                $date_today = date('Y-m-d', strtotime(date('Y-m-d')));
                $date_to = date('Y-m-d', strtotime(date('Y-m-d') . "-1 day"));
                $date_from = date('Y-m-d', strtotime(date('Y-m-d') . "-" . $diff_day));
                $filter_cond .= " AND " . $_REQUEST['range_field'] . " >='" . $date_from . "' AND " . $_REQUEST['range_field'] . " <='" . $date_to . "'";
            }
            if (isset($_REQUEST['today_field']) && in_array($_REQUEST['today_field'], array('release_date'))) {
                $date_today = date('Y-m-d', strtotime(date('Y-m-d')));
                $filter_cond .= " AND " . $_REQUEST['today_field'] . " ='" . $date_today . "'";
            }
            
                                //Filter by Cast and Crew
            if (isset($_REQUEST['cast']) && ($_REQUEST['cast'] != 'null')) {
                $castFilter = $_REQUEST['cast'];
                if (!is_array($castFilter)) {
                    $cast = explode(",", $castFilter);
                }
                $castName = "";
                if (!empty($cast)) {
                    $cond = " AND name IN ";
                    $tot = count($cast);
                    for ($j = 0; $j < $tot; $j++) {
                        $comma = " ";
                        if ($tot != ($j + 1)) {
                            $comma = ",";
                        }
                        $castName .= "'" . $cast[$j] . "'";
                        $castName .= $comma;
                    }
                    $cond .= " (" . $castName . ") ";
                }
                $casts = Yii::app()->db->createCommand()
                        ->select('id,parent_id,language_id,name')
                        ->from('celebrities')
                        ->where('studio_id='.$studio_id.' AND language_id='.$language_id.''.$cond)
                        ->queryAll();
                $cast_ids = array();
                if (!empty($casts)) {
                    foreach ($casts as $cast) {
                        if ($cast['parent_id'] > 0) {
                            $cast_ids[] = $cast['parent_id'];
                        } else {
                            $cast_ids[] = $cast['id'];
                        }
                    }
                }
                
                if (!empty($cast_ids)) {
                    $movieIds = Yii::app()->db->createCommand()
                            ->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
                            ->from('movie_casts mc')
                            ->where('mc.celebrity_id IN (' . implode(',', $cast_ids) . ') ')
                            ->queryAll();
                    if (@$movieIds[0]['movie_ids']) {
                        $filter_cond .= ' AND F.id IN (' . $movieIds[0]['movie_ids'] . ') ';
                    } else {
                        $filter_cond .= ' AND F.id =0 ';
                    }
                }
            }
            
            $episode_cond = "";
            if ($is_episode != "") {
                $episode_cond = " AND M.is_episode=" . $is_episode;
            }
            if (!empty($cat)) {
                foreach ($cat as $category) {
                    $category_id = $category['id'];
                    $poster_category = $this->getPoster($category_id, 'content_category');
                    $category_name = $category['category_name'];
                    $category_permalink = $this->siteurl . '/' . $category['permalink'];
                    $binary_value = $category['binary_value'];
                    $sql = "SELECT id,name,story,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND FIND_IN_SET($category_id,content_category_value) AND parent_id=0 ORDER BY name,parent_id ASC";
                    $films = Yii::app()->db->createCommand($sql)->queryAll();
                    $content_details = array();
                    $poster_film = array();
                    if (!empty($films)) {
                        foreach ($films as $film) {
                            $id = $film['id'];
                            $name = $film['name'];
                            $story = $film['story'];
                            $langcontent = Yii::app()->custom->getTranslatedContent($id, 0, $language_id, $studio_id);
                            if (array_key_exists($id, $langcontent['film'])) {
                                $name = $langcontent['film'][$id]->name;
                                $story = $langcontent['film'][$id]->story;
                            }
                            $fav_status = 1;
                            $login_status = 0;
                            if ($user_id) {
                                $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id, $studio_id, $user_id);
                                $fav_status = ($fav_status == 1) ? 0 : 1;
                                $login_status = 1;
                            }
                            $poster = $this->getPoster($id, 'films');
                            $permalink = $this->siteurl . '/' . $film['permalink'];
                            $content_details[] = array(
                                'content_id' => $id,
                                'name' => $name,
                                'story' => $story,
                                'poster' => $poster,
                                'content_permalink' => $permalink,
                                'is_episode' => 0,
                                'fav_status' => $fav_status,
                                'login_status' => $login_status
                            );
                        }
                    }
                   //$content_details = array_unique($content_details, SORT_REGULAR);
                    $channels[] = array(
                        'category_name' => $category_name,
                        'permalink' => $category_permalink,
                        'link' => $category['permalink'],
                        'category_poster' => $poster_category,
                        'content_list' => $content_details
                    );
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $channels;
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            } else {
				$studio_id = $this->studio_id;
				if(($studio_id=='2626')){
					$addQuery = '';
					$customFormObj = new CustomForms();
					$customData = $customFormObj->getFormCustomMetadat($this->studio_id);
					if($customData){
						foreach ($customData AS $ckey => $cvalue){
							$addQuery = ",F.".$cvalue['field_name'];
							$customArr[] = array(
								'f_id' => trim($cvalue['f_id']),
								'field_name' => trim($cvalue['field_name']),
								'field_display_name' => trim($cvalue['f_display_name'])
							);
						}
					}
					$cmd = Yii::app()->db->createCommand()
							->select('SQL_CALC_FOUND_ROWS (0), M.movie_id,M.is_converted,M.video_duration,M.id AS movie_stream_id,M.embed_id, F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id, M.last_updated_date '.$addQuery)
							->from('movie_streams M,films F')
							->where('M.movie_id = F.id AND F.studio_id =' . $this->studio_id . ' AND F.parent_id = 0 AND M.episode_parent_id =0 ' . $condition . ' ' . $filter_cond . '' . $episode_cond)
							->order($order)
							->limit($limit, $offset);
					$contentList = $cmd->QueryAll();
					$fobject = new Film();
					$list = $fobject->getContentListData($contentList, @$margs, $customArr,$this->studio_id,$language_id,'',$translate);
				}else{
					$command = Yii::app()->db->createCommand()
							->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.language,F.content_types_id,F.language_id,M.episode_language_id')
							->from('movie_streams M,films F')
							->where('M.movie_id = F.id AND F.studio_id =' . $this->studio_id . ' AND F.parent_id = 0 AND M.episode_parent_id =0 ' . $condition . ' ' . $filter_cond . '' . $episode_cond)
							->order($order)
							->limit($limit, $offset);
					$contents = $command->QueryAll();
					$customData = array();
					$list = array();
					if (!empty($contents)) {
						foreach ($contents as $content) {
							$is_episode = $content['is_episode'];
							$content_id = $content['movie_id'];
							if ($is_episode == 1) {
								$content_id = $content['movie_stream_id'];
							}
							$list[] = Yii::app()->general->getContentData($content_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
						}
					}
				}
				$item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                if ($group_by_release) {
                    $res = array();
                    foreach ($list as $c) {
                        $r_date = $c['full_release_date'];
                        $res[$r_date][] = $c;
                    }
                    $list = $res;
                }
                if ($group_by_starttime) {
                    $res = array();
                    foreach ($list as $c) {
                        $r_date = $c['start_time'];
                        $res[$r_date][] = $c;
                    }
                    $list = $res;
                }

                //$list = array_unique($list, SORT_REGULAR);
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $list;
                if (!empty($list)) {
                    $data['total_content'] = $item_count;
                } else {
                    $data['msg'] = "No content found";
                }
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid data sent.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionCheckFavourite() {
        $studio_id = $this->studio_id;
        $favenable = $this->CheckFavouriteEnable($studio_id);
        $data['code'] = 200;
        $data['status'] = "Success";
        $data['msg'] = $favenable;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionViewFavourite() {
        $user_id = $_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $fav_status = $this->CheckFavouriteEnable($studio_id);
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        if (($fav_status != 0)) {
            if ($user_id > 0) {
                $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id, $studio_id, $page_size, $offset, $language_id);
                $item_count = @$favlist['total'];
                $list = @$favlist['list'];
                $data['status'] = 200;
                $data['msg'] = 'OK';
                $data['movieList'] = @$list;
                $data['item_count'] = @$item_count;
                $data['limit'] = @$page_size;
            } else {
                $data['code'] = 411;
                $data['status'] = "Failure";
                $data['msg'] = "User Id not found";
            }
        } else {
            $data['code'] = 412;
            $data['status'] = "Failure";
            $data['msg'] = "Add to favourite is not enable";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionAddtoFavlist() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '')) {
            $uniq_id = $_REQUEST['movie_uniq_id'];
            $content_type = isset($_REQUEST['content_type']) ? $_REQUEST['content_type'] : '0';
            $films = new Film;
            $field = 'uniq_id';
            $parent_field = 'parent_id';
            if (($content_type == '1') || ($content_type == 1)) {
                $films = new movieStreams;
                $field = 'embed_id';
				$parent_field = 'episode_parent_id';
            }
            $user_id = @$_REQUEST['user_id'];
            if ($user_id > 0) {
                $data = $films->findByAttributes(array($field => $uniq_id, 'studio_id' => $studio_id,$parent_field => 0), array('select' => 'id'));
                if (!empty($data)) {
                    $content_id = $data->id;
                    $user_fav_stat = UserFavouriteList::model()->findByAttributes(array('content_id' => $content_id, 'content_type' => $content_type, 'studio_id' => $studio_id, 'user_id' => $user_id));
                    if (empty($user_fav_stat)) {
                        $usr_fav = new UserFavouriteList;
                        $usr_fav->status = '1';
                        $usr_fav->content_id = $content_id;
                        $usr_fav->content_type = $content_type;
                        $usr_fav->studio_id = $studio_id;
                        $usr_fav->user_id = $user_id;
                        $usr_fav->date_added = new CDbExpression("NOW()");
                        $usr_fav->last_updated_at = new CDbExpression("NOW()");
                    } else {
                        $usr_fav = UserFavouriteList::model()->findByPk($user_fav_stat->id);
                        if ($user_fav_stat->status == '0') {
                            $usr_fav->status = '1';
                            $usr_fav->last_updated_at = new CDbExpression("NOW()");
                        } else {
                            $res['code'] = 448;
                            $res['status'] = "Failure";
                            $res['msg'] = $translate['already_added_favourite'];
                            $this->setHeader($res['code']);
                            echo json_encode($res);
                            exit;
                        }
                    }
                    $usr_fav->save();
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = $translate['added_to_fav'];
                } else {
                    $res['code'] = 406;
                    $res['status'] = "Failure";
                    $res['msg'] = $translate['required_data_not_found'];
                }
            } else {
                $res['code'] = 401;
                $res['status'] = "Failure";
                $res['msg'] = $translate['need_to_sign_in'];
            }
        } else {
            $res['code'] = 406;
            $res['status'] = "Failure";
            $res['msg'] = $translate['required_data_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionDeleteFavList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '')) {
            $uniq_id = $_REQUEST['movie_uniq_id'];
            $content_type = isset($_REQUEST['content_type']) ? $_REQUEST['content_type'] : '0';
            $user_id = @$_REQUEST['user_id'];
            if ($user_id > 0) {
                $films = new Film;
                $field = 'uniq_id';
                if (($content_type == '1') || ($content_type == 1)) {
                    $films = new movieStreams;
                    $field = 'embed_id';
                }
                $data = $films->findByAttributes(array($field => $uniq_id, 'studio_id' => $studio_id,'parent_id' => 0), array('select' => 'id'));
                if (!empty($data)) {
                    $content_id = $data['id'];
                    $params = array(':content_id' => $content_id, ':content_type' => $content_type, ':studio_id' => $studio_id, ':user_id' => $user_id);
                    $user_fav_stat = UserFavouriteList::model()->deleteAll('content_id = :content_id AND content_type = :content_type AND studio_id = :studio_id AND user_id = :user_id', $params);
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = $translate['content_remove_favourite'];
                } else {
                    $res['code'] = 406;
                    $res['status'] = "Failure";
                    $res['msg'] = $translate['required_data_not_found'];
                }
            } else {
                $res['code'] = 401;
                $res['status'] = "Failure";
                $res['msg'] = $translate['need_to_sign_in'];
            }
        } else {
            $res['code'] = 406;
            $res['status'] = "Failure";
            $res['msg'] = $translate['required_data_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionNotificationLists() {
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $diff_days = isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0 ? $_REQUEST['before_days'] : 7;
        $sql = "SELECT * FROM `push_notification_log` WHERE `studio_id` = '{$studio_id}' AND `device_id` = '{$device_id}' AND (`status` = '1' OR `status` = '2')";
        $sql.= " AND `created_date` >= DATE_SUB(CURDATE(), INTERVAL $diff_days DAY)";
        $all_notification = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($all_notification)) {
            $unread_cnt = 0;
            foreach ($all_notification as $all_notif) {
                if ($all_notif['status'] == 1)
                    $unread_cnt++;
            }

            $res['code'] = 200;
            $res['status'] = "Success";
            $res['notifyList'] = @$all_notification;
            $res['count'] = count($all_notification);
            $res['count_unread'] = @$unread_cnt;
        }else {
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['no_notification'];
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionReadNotification() {
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $message_unique_id = $_REQUEST['message_unique_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($device_id) && $device_id != '') && (isset($message_unique_id) && $message_unique_id != '')) {
            $notification = new PushNotificationLog;
            $notificationstatus = $notification->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id, 'message_unique_id' => $message_unique_id, 'status' => '1'));
            $notificationstatus->status = '2';
            $notificationstatus->save();
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['notification_read'];
        } else {
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = 'Device Id not found';
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionGetNoOfUnreadNotification() {
        if (isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != "") {
            $studio_id = $this->studio_id;
            $device_id = $_REQUEST['device_id'];
            $status = '1';
            $diff_days = isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0 ? $_REQUEST['before_days'] : 7;
            $create_date = " AND `created_date` >= DATE_SUB(CURDATE(), INTERVAL $diff_days DAY)";
            $notificationstatus = PushNotificationLog::model()->count("studio_id = :studio_id AND device_id =:device_id AND status = :status " . $create_date, array(':studio_id' => $studio_id, ':device_id' => $device_id, ':status' => $status));
            if ($notificationstatus) {
                $data['code'] = 200;
                $data['status'] = "Success";
                $data['count'] = $notificationstatus;
            } else {
                $data['code'] = 400;
                $data['status'] = "Success";
                $data['count'] = 0;
            }
        } else {
            $data['code'] = 405;
            $data['status'] = "Failure";
            $data['count'] = 0;
        }
        $data['count'] = (string) $data['count'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionReadAllNotification() {
        $studio_id = $this->studio_id;
        $device_id = $_REQUEST['device_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($device_id) && $device_id != '')) {
            $notification = new PushNotificationLog;
            $attr = array('status' => '2');
            $condition = "studio_id=:studio_id AND device_id =:device_id AND status =:status";
            $params = array(':studio_id' => $studio_id, ':device_id' => $device_id, ':status' => '1');
            $notificationstatus = $notification->updateAll($attr, $condition, $params);
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['all_notification_read'];
        } else {
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = 'Device Id not found';
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionBookEvent() {
        $studio_id = $this->studio_id;
        $curr_time = gmdate('Y-m-d H:i:s');
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id'] != "") {
            $lang_code = @$_REQUEST['lang_code'];
            $time_offset = isset($_REQUEST['timezone_offset']) && $_REQUEST['timezone_offset'] != "" ? $_REQUEST['timezone_offset'] : 0;
            $translate = $this->getTransData($lang_code, $studio_id);
            $movie_id = $_REQUEST['movie_id'];
            $is_episode = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] != "") ? $_REQUEST['is_episode'] : 0;
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $start_time = (isset($_REQUEST['start_time']) && $_REQUEST['start_time'] != "") ? $_REQUEST['start_time'] : 0;
            if ($user_id > 0) {
                if ($start_time == 0) {
                    if ($is_episode == 0) {
                        $movie_details = Film::model()->findByPk($movie_id, array('select' => 'start_time'));
                        $start_time = $movie_details->start_time;
                    } elseif ($is_episode == 1) {
                        $ep_details = movieStreams::model()->findByPk($movie_id, array('select' => 'movie_id'));
                        $movie_details = Film::model()->findByPk($ep_details->movie_id, array('select' => 'start_time'));
                        $start_time = $movie_details->start_time;
                    }
                }
                $start_time = date("Y-m-d H:i:s", strtotime($time_offset . " minutes", strtotime($start_time)));
                if (strtotime($start_time) > strtotime($curr_time)) {
                    $book_event = BookedContent::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_type' => $is_episode, 'content_id' => $movie_id));
                    if (empty($book_event)) {
                        $booked_content = new BookedContent;
                        $booked_content->user_id = $user_id;
                        $booked_content->studio_id = $studio_id;
                        $booked_content->content_type = $is_episode;
                        $booked_content->content_id = $movie_id;
                        $booked_content->booked_time = $start_time;
                        $booked_content->save();
                    } else {
                        $book_event->booked_time = $start_time;
                        $book_event->notification_status = '0';
                        $book_event->save();
                    }
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = $translate['content_saved_to_calender'];
                } else {
                    $res['code'] = 406;
                    $res['status'] = "Failure";
                    $res['msg'] = $translate['booking_time_should_greater_than_current'];
                }
            } else {
                $res['code'] = 401;
                $res['status'] = "Failure";
                $res['msg'] = $translate['need_to_sign_in'];
            }
        } else {
            $res['code'] = 406;
            $res['status'] = "Failure";
            $res['msg'] = $translate['required_data_not_found'];
        }
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose Check Calender to get contents added for current time 
     * @return  Json String
     * @author Biswajit<biswajit@muvi.com>
     */

    public function actionCheckCalender() {
        if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") {
            $date = gmdate('Y-m-d H:i');
            $studio_id = $this->studio_id;
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
            $user_id = $_REQUEST['user_id'];
            $reminder = 0;
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'reminder');
            if (!empty($reminder_status)) {
                $reminder = $reminder_status['config_value'];
            }
            $reminder_before = 0;
            if ($reminder) {
                $reminder_before_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'reminder_before');
                if (!empty($reminder_before_status)) {
                    $reminder_before = $reminder_before_status['config_value'];
                }
                if ($reminder_before) {
                    $time = new DateTime($date);
                    $time->add(new DateInterval('PT' . $reminder_before . 'M'));
                    $date = $time->format('Y-m-d H:i');
                }
                $command = Yii::app()->db->createCommand()
                        ->select('BC.id, BC.content_id, BC.content_type')
                        ->from('booked_content BC')
                        ->where('BC.studio_id=' . $studio_id . ' AND BC.user_id=' . $user_id . ' AND BC.notification_status="0" AND BC.booked_time LIKE "%' . $date . '%" ');
                $contents = $command->QueryAll();
                $films = array();
                $episodes = array();
                $movielist = array();
                if (!empty($contents)) {
                    foreach ($contents as $content) {
                        if ($content['content_type'] == 0) {
                            $films[] = $content['content_id'];
                        } else {
                            $episodes[] = $content['content_id'];
                        }
                        $booked_content = BookedContent::model()->findByPk($content['id']);
                        $booked_content->notification_status = '1';
                        $booked_content->save();
                    }
                    $film_content = array();
                    $episode_content = array();
                    if (!empty($films)) {
                        $film = implode(',', $films);
                        $command = Yii::app()->db->createCommand()
                                ->select('F.id, F.name, F.story, F.permalink, M.is_episode')
                                ->from('films F, movie_streams M')
                                ->where('F.id = M.movie_id AND F.studio_id=' . $studio_id . ' AND F.parent_id=0 AND M.is_episode=0 AND F.id IN(' . $film . ')');
                        $film_content = $command->QueryAll();
                    }
                    if (!empty($episodes)) {
                        $episode = implode(',', $episodes);
                        $command = Yii::app()->db->createCommand()
                                ->select('M.id, M.episode_title as name, M.episode_story as story, CONCAT(F.permalink, "/stream/", M.embed_id) as permalink, M.is_episode')
                                ->from('movie_streams M, films F')
                                ->where('F.id = M.movie_id AND F.studio_id=' . $studio_id . ' AND M.episode_parent_id=0 AND F.parent_id=0 AND M.is_episode=1 AND M.id IN(' . $episode . ')');
                        $episode_content = $command->QueryAll();
                    }
                    $movielist = array_merge($film_content, $episode_content);
                    $msgs = $translate['playing_now'];
                    if ($reminder_before > 0) {
                        $time = $reminder_before;
                        $msgs = $translate['start_in_minutes'];
                        $msgs = htmlentities($msgs);
                        eval("\$msgs = \"$msgs\";");
                        $msgs = htmlspecialchars_decode($msgs);
                    }

                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['movielist'] = $movielist;
                    $res['reminder_message'] = $msgs;
                } else {
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = "No Content Found";
                    $res['movielist'] = $movielist;
                }
            } else {
                $res['code'] = 406;
                $res['status'] = "Failure";
                $res['msg'] = "Reminder Feature is not available";
            }
        } else {
            $res['code'] = 401;
            $res['status'] = "Failure";
            $res['msg'] = "Need to sign in";
        }
        echo json_encode($res);
        exit;
    }

    public function actionloadFeaturedSections() {
        $offset = isset($_REQUEST['dataoffset']) ? $_REQUEST['dataoffset'] : 0;
        $limit = isset($_REQUEST['viewlimit']) ? $_REQUEST['viewlimit'] : 10;
        $studio_id = $this->studio_id;
        $return =  $translatedSections = array();
        if ($_REQUEST['adv_info'] == "true" && isset($_REQUEST['adv_info'])){
            $vdFeaturedSection = VdStudioConfig::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
            if (!empty($vdFeaturedSection) && $vdFeaturedSection['feature_loading_style'] != 1){
                $limit = $vdFeaturedSection['featured_section_count'];
            }
        }
        
        $themes = $_REQUEST['themes'] ? $_REQUEST['themes'] : '';
        $controller = Yii::app()->controller;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0 ? $_REQUEST['user_id'] : 0;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.parent_id = 0 AND t.studio_id = ' . $studio_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $sections = FeaturedSection::model()->findAll($criteria);
        if($language_id !=20){
            $translatedSections = FeaturedSection::model()->getTranslatedFeaturedSections($sections, $studio_id, $language_id);
        }
        
        $return = array();
        if ($sections) {
            $addQuery = '';
            $customFormObj = new CustomForms();
            $customData = $customFormObj->getFormCustomMetadat($studio_id);
            if ($customData) {
                foreach ($customData AS $ckey => $cvalue) {
                    $addQuery = ",F." . $cvalue['field_name'];
                    //$customArr[$cvalue['f_id']] = $cvalue['f_display_name']; 
                    $customArr[] = array(
                        'f_id' => trim($cvalue['f_id']),
                        'field_name' => trim($cvalue['field_name']),
                        'field_display_name' => trim($cvalue['f_display_name'])
                    );
                }
            }
            $visitor_loc = Yii::app()->common->getVisitorLocation();
            $country = $visitor_loc['country'];
            $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
            foreach ($sections as $section) {
                if(array_key_exists($section->id, $translatedSections)){
                    $section->title = $translatedSections[$section->id];
                }
                $final_content = array();
                if ($section->section_type == 1 && count($section->contents)) {
                    $streamIds = '';
                    $movieIds = '';
                    $idSeq = '';
                    $idSeqplay = '';
                    $final_content = array();
                    $final_physical = array();
                    $Seq = array();
                    $total_contents = 0;
                    $is_playlist = 0;
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
                    } else {
                        foreach ($section->contents as $featured) {
                            if ($featured->is_episode == 1) {
                                $streamIds .="," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            } elseif ($featured->is_episode == 4) {
                                $playlistId .= "," . $featured->movie_id;
                                $is_playlist = 1;
                                $is_episode = $featured->is_episode;
                                $idSeqplay .= "," . $featured->id_seq;
                            } else {
                                $movieIds .= "," . $featured->movie_id;
                                $idSeq .= "," . $featured->id_seq;
                            }
                        }
                        $cond = '';
                        if (@$movieIds || @$streamIds) {
                            $cond = " AND (";
                            if (@$movieIds) {
                                $movieIds = ltrim($movieIds, ',');
                                $cond .="(F.id IN(" . $movieIds . ") AND is_episode=0) OR ";
                                $orderBy = " ORDER BY FIELD(P.movie_id," . $movieIds . ")";
                            }
                            if ($streamIds) {
                                $streamIds = ltrim($streamIds, ',');
                                $cond .=" (M.id IN(" . $streamIds . "))";
                                $orderBy = " ORDER BY FIELD(P.movie_stream_id," . $streamIds . ")";
                            } else {
                                $cond = str_replace(' OR ', ' ', $cond);
                            }
                            $cond .= ")";
                        }
                        $cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                        $sql = "SELECT  PM.*, l.id as livestream_id,l.feed_type,l.feed_url,l.feed_method FROM "
                                . "( SELECT M.movie_id,M.is_converted,M.video_duration,M.is_episode,M.embed_id,M.is_downloadable,M.id AS movie_stream_id,F.permalink,F.content_category_value,F.name,F.mapped_id,F.censor_rating,M.mapped_stream_id,F.uniq_id,F.content_type_id,F.ppv_plan_id,M.full_movie,F.story,F.genre,F.release_date, F.content_types_id,M.episode_title,M.episode_date,M.episode_story,M.episode_number,M.series_number, M.last_updated_date " . $addQuery
                                . "FROM movie_streams M,films F WHERE F.parent_id = 0 AND M.movie_id = F.id AND F.status = 1 AND M.studio_id=" . $studio_id . " " . $cond . " ) AS PM "
                                . " LEFT JOIN livestream l ON PM.movie_id = l.movie_id";
                        $sql_data = "SELECT P.* FROM (SELECT t.*,g.* FROM (" . $sql . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                        $where = " WHERE P.geocategory_id IS NULL OR P.country_code='{$country}'";
                        $sql_data .= $where . " " . $orderBy;
                        $list = Yii::app()->db->createCommand($sql_data)->queryAll();
                        if ($is_playlist == 1) {
                            $Seqid = ltrim($idSeq, ',');
                            $Seq = explode(',', $Seqid);
                        }
                        $fobject = new Film();
                        $final_content = $fobject->getContentListData($list, $margs, $customArr, $this->studio_id, $language_id, $themes, $translate, $Seq);
                        if ($is_playlist == 1) {
                            $movieIds = ltrim($playlistId, ',');
                            $idSeqplay = ltrim($idSeqplay, ',');
                            $seqId = explode(',', $idSeqplay);
                            $command1 = Yii::app()->db->createCommand()
                                    ->select('id,playlist_name,playlist_permalink')
                                    ->from('user_playlist_name u')
                                    ->where('u.studio_id=' . $studio_id . ' AND u.user_id = 0 And u.id IN(' . $movieIds . ') ORDER BY FIELD(u.id,' . $movieIds . ')');
                            $data = $command1->QueryAll();
                            $tot = count($final_content);
                            $k = 0;
                            if ($data) {
                                foreach ($data as $res) {
                                    $playlist_id = $res['id'];
                                    $playlist_name = $res['playlist_name'];
                                    $permalink = $res['playlist_permalink'];
                                    $poster = $controller->getPoster($playlist_id, 'playlist_poster');
                                    $final_content[$seqId[$k]] = array(
                                        'movie_id' => $playlist_id,
                                        'title' => utf8_encode($playlist_name),
                                        'permalink' => Yii::app()->getbaseUrl(true) . '/' . $permalink,
                                        'poster' => $poster,
                                        'is_episode' => $is_episode,
                                        'seq_feat' => $seqId[$k]
                                    );
                                    $k++;
                                    $tot ++;
                                }
                            }
                        }
                        ksort($final_content);
                        $final_content = array_values(array_filter($final_content));
                    }
                } else if($section->section_type == 2) {
                    $section_category = json_decode($section->section_category);
                    $content_types = json_decode($section->content_types);
                    $section_type = $section->section_type;
                    $content_limit = $section->content_limit;
                    $viewmorecpermalink = '';
                    if($section->is_viewmore == 1){
                        //$getPermalink = ContentCategories::model()->findByPk($section_category[0], array('select' => 'permalink'));
                        $getPermalink = MenuItem::model()->findByAttributes(array('value' => $section_category[0], 'studio_id'=>$studio_id), array('select' => 'permalink'));
                        $viewmorecpermalink = $getPermalink->permalink;
                    }
                    if ($section->content_type == 1) {
                        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id, $section_type, $section_category, $content_limit);
                    } else {
                        $section_criteria = $section->section_criteria;
                        //$language_id = $section->language_id;
                        $content_type = $section->content_type;
                        //$customData = array();
                        $findContent = Film::model()->getContentID($studio_id, $section_criteria, $section_category, $content_limit, $content_type,$content_types);
                        $customData = Yii::app()->Helper->getRelationalField($studio_id, 1);
                        $i = 0; 
                        if (!empty($findContent)) { 
                            foreach ($findContent as $data) { 
                                if ($section_criteria == 6) {
                                    if ($data[0]['is_episode'] == 1) {
                                        $movie_id = $data[0]['stream_id'];
                                    } else {
                                        $movie_id = $data[0]['movie_id'];
                                    }
                                    $is_episode = $data[0]['is_episode'];
                                }else if ($section_criteria == 2 || $section_criteria == 7) {
                                    $movie_id = $data['movie_id'];
                                    $is_episode = $data['is_episode'];
                                }else {
                                    if ($data['is_episode'] == 1) {
                                        $movie_id = $data['stream_id'];
                                    } else {
                                        $movie_id = $data['movie_id'];
                                    }
                                    $is_episode = $data['is_episode'];
                                }
                                $final_content[$i] = Yii::app()->general->getContentData($movie_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
                                $i++;
                            }
                        } else {
                            $final_content = array();
                        }
                    } 
                }
                $return[] = array(
                    'id' => $section->id,
                    'title' => utf8_encode($section->title),
                    'content_type' => $section['content_type'],
                    'total' => count($final_content),
                    'layout_design' => $section->layout_design,
                    'cpermalink'=>$viewmorecpermalink,
                    'column_count' => $section->column_count,
                    'meta_data_info' => json_decode($section->meta_data_info),
                    'contents' => $final_content
                );
                }
            }
        $res['code'] = 200;
        $res['status'] = "OK";
        if ($_REQUEST['adv_info'] == "true" && isset($_REQUEST['adv_info'])) {
            $res['loading_style'] = $vdFeaturedSection['feature_loading_style'];
            $res['section_count'] = $vdFeaturedSection['featured_section_count'];
        } else {
            $res['loading_style'] = '0';
            $res['section_count'] = '0';
        }
        $res['section'] = $return;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose add the content to playlist
     * *allPlaylist (get all the playlist)
     * @return  string 
     * @author Biswajitdas<biswajitdas@muvi.com>
     */

    public function actionallPlaylist() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $list = Yii::app()->common->getAllplaylist($studio_id, $user_id, 0, 0, 0, $language_id);
            $res['code'] = 405;
            $res['status'] = "Success";
            $res['msg'] = $list;
            echo json_encode($res);
            exit;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = $translate['need_to_sign_in'];
            echo json_encode($res);
            exit;
        }
    }

    /*
     * @purpose Get all playlists of a user
     * *allUserPlaylist
     * @return  string 
     * @author Debayan Chakravarty<support@muvi.com>
     */

    public function actionAudioAllUserPlayList() {
        $studio_id = $this->studio_id;
        $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        if ($user_id > 0) {
            $list = Yii::app()->common->getAllAudioplaylist($studio_id, $user_id);
            $res['code'] = 405;
            $res['status'] = "Success";
            $res['msg'] = $list;
            echo json_encode($res);
            exit;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }

    /*
     * @purpose get the contents of a playlist
     * *actionAudioUserPlayListDetail 
     * @return  string 
     * @author Debayan Chakravarty<support@muvi.com>
     */

    public function actionAudioUserPlaylistDetail() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        if (isset($_REQUEST['list_id']) && trim($_REQUEST['list_id']) != "") {
            if ($user_id > 0) {
                $playlist_id = $_REQUEST['list_id'];
                $userplaylist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $playlist_id), array('select' => 'playlist_name, content_category_value'));
                if (!empty($userplaylist)) {
                    $command = Yii::app()->db->createCommand()
                            ->select('content_id,content_type')
                            ->from('user_playlist p')
                            ->where('p.user_id = ' . $user_id . ' AND p.studio_id=' . $studio_id . ' AND p.playlist_id =' . $playlist_id);
                    $playItem = $command->queryAll();
                    $poster_playlist = $this->getPoster($playlist_id, 'playlist_poster');
                    $playlist['list_id'] = $playlist_id;
                    $playlist['list_name'] = $userplaylist['playlist_name'];
                    $playlist['content_category'] = $userplaylist['content_category_value'];
                    $playlist['poster_playlist'] = $poster_playlist;
                    $playlist['count'] = count($playItem);
                    $k = 0;
                    $list_item = array();
                    if (count($playItem) > 0) {
                        foreach ($playItem as $item) {
                            $movie_id = $item['content_id'];
                            $is_episode = $item['content_type'];
                            $command1 = Yii::app()->db->createCommand()
                                    ->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id,ms.id')
                                    ->from('films f,movie_streams ms')
                                    ->where('f.id=ms.movie_id AND ms.id=' . $movie_id . ' AND ms.is_episode = ' . $is_episode . ' AND ms.is_converted = 1 AND f.parent_id = 0 AND ms.episode_parent_id = 0 ');
                            $list = $command1->QueryRow();
                            if (!empty($list)) {
                                $audio_id = $list['movie_id'];
                                $stream_uniq_id = $list['embed_id'];
                                $movie_uniq_id = $list['uniq_id'];
                                if ($is_episode == '1') {
                                    $title = $list['episode_title'];
                                    $audioid = $movie_id;
                                    $type = 'moviestream';
                                } else {
                                    $title = $list['name'];
                                    $audioid = $audio_id;
                                    $type = 'films';
                                }
                                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                                $bucketName = $bucketInfo['bucket_name'];
                                $s3url = $bucketInfo['s3url'];
                                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                                $signedBucketPath = $folderPath['signedFolderPath'];
                                $is_favourite = Yii::app()->common->isContentFav($studio_id, $user_id, $audioid, $is_episode);
                                $list_item[$k]['movie_stream_id'] = $movie_id;
                                $list_item[$k]['movie_id'] = $audio_id;
                                $list_item[$k]['is_episode'] = $is_episode;
                                //$list_item[$k]['url'] = Yii::app()->aws->getAudioUrl($list['id']);
                                $list_item[$k]['url'] = 'https://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $list['id'] . '/' . $list['full_movie'];
                                $list_item[$k]['audio_poster'] = $this->getPoster($audioid, $type, 'original', $studio_id);
                                $list_item[$k]['title'] = $title;
                                $list_item[$k]['permalink'] = $list['permalink'];
                                $list_item[$k]['is_favourite'] = $is_favourite;
                                $list_item[$k]['movie_uniq_id'] = $movie_uniq_id;
                                $list_item[$k]['stream_uniq_id'] = $stream_uniq_id;
                                $cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
                                $celeb_name = array();
                                if (!empty($cast_details)) {
                                    foreach ($cast_details as $casts) {
                                        if (trim($casts['celeb_name']) != "") {
                                            $celeb_name[] = $casts['celeb_name'];
                                        }
                                    }
                                }
                                $casts = implode(',', $celeb_name);
                                $list_item[$k]['cast'] = $casts;
                                $k++;
                            }
                        }
                        $playlist['lists'] = $list_item;
                        $res['code'] = 200;
                        $res['message'] = 'success';
                        $res['data'] = $playlist;
                    } else {
                        $res['code'] = 400;
                        $res['message'] = $translate['content_not_found'];
                        $res['status'] = 'failure';
                    }
                } else {
                    $res['code'] = 405;
                    $res['status'] = "Failure";
                    $res['msg'] = $translate['need_to_sign_in'];
                }
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = $translate['no_playlist_found'];
            }
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose create a new playlist
     * *actionCreateNewPlayList 
     * @return  string 
     * @author Biswajit Parida<support@muvi.com>
     */

    public function actionCreateNewPlayList() {
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlistname']) && ($_REQUEST['playlistname'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))):
            $playlistnme = $_REQUEST['playlistname'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_name' => $playlistnme));
            if (empty($playlist)) {
                $playlistNew = new UserPlaylistName;
                $playlistNew->user_id = $user_id;
                $playlistNew->studio_id = $studio_id;
                $playlistNew->playlist_name = $playlistnme;
                $playlistNew->save();
                $playlist_id = $playlistNew->id;
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_created'];
                $res['playlist_id'] = $playlist_id;
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = $translate['playlist_already_exist'];
            }
        else:
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        endif;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionaddToPlaylist() {
        $studio_id = $this->studio_id;
        if ((isset($_REQUEST['playlistname']) && ($_REQUEST['playlistname'] != '')) || (isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] > 0)) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlistnme = @$_REQUEST['playlistname'];
            $playlist_id = (isset($_REQUEST['playlist_id']) && $_REQUEST['playlist_id'] > 0) ? $_REQUEST['playlist_id'] : 0;
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $movie_id = $_REQUEST['content_id'];
            $is_episode = $_REQUEST['is_episode'];
            $lang_code = $_REQUEST['lang_code'];
            $is_content = $_REQUEST['is_content'];
            $que_id = (isset($_REQUEST['que_id']) && $_REQUEST['que_id'] != "") ? $_REQUEST['que_id'] : '';
            $translate = $this->getTransData($lang_code, $studio_id);
            if ($playlist_id == 0) {
                $playlistNew = new UserPlaylistName;
                $playlistNew->user_id = $user_id;
                $playlistNew->studio_id = $studio_id;
                $playlistNew->playlist_name = $playlistnme;
                $playlistNew->save();
                $playlist_id = $playlistNew->id;
            }
            $playlist_name = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 0);
            $total_playlist = count($playlist_name);
            if ($movie_id != '' && $is_content == 1) {
                $result = self::AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id, $lang_code, $que_id, $total_playlist);
            } elseif ($que_id != '') {
                $result = self::AddQueuePlaylist($user_id, $playlist_id, $lang_code, $que_id, $total_playlist);
            } else {
                $res['code'] = 400;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_created'];
                $res['total'] = $total_playlist;
                $result = $res;
            }
            echo json_encode($result);
            exit;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }

    public function actionDeletePlaylist() {
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlist_id = $_REQUEST['playlist_id'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != "") ? $_REQUEST['user_id'] : 0;
            $playlist = UserPlaylistName::model()->findByPk($playlist_id);
            if (empty($playlist)) {
                $res['code'] = 400;
                $res['status'] = "Failure";
                $res['msg'] = 'Invalid playlist';
                echo json_encode($res);
                exit;
            } elseif ($playlist->user_id != $user_id) {
                $res['code'] = 400;
                $res['status'] = "Failure";
                $res['msg'] = 'Invalid User';
                echo json_encode($res);
                exit;
            } else {
                $param = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id);
                $playItem = UserPlaylist::model()->deleteAll('playlist_id = :playlist_id AND user_id = :user_id AND studio_id = :studio_id', $param);
                $playlist = UserPlaylistName::model()->deleteAll('user_id = :user_id AND id = :playlist_id AND studio_id = :studio_id', $param);
                $playlist_name = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 0);
                $total_playlist = count($playlist_name);
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_deleted'];
                $res['total'] = $total_playlist;
                echo json_encode($res);
                exit;
            }
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = 'Need To signin';
            echo json_encode($res);
            exit;
        }
    }

    public function actionGetAudioPlaylist() {
        $studio_id = $this->studio_id;
        if ((isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlist_id = $_REQUEST['playlist_id'];
            $user_id = @$_REQUEST['user_id'];
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $command = Yii::app()->db->createCommand()
                    ->select('content_id,content_type')
                    ->from('user_playlist p')
                    ->where('p.user_id =:user_id  AND p.studio_id=:studio_id AND p.playlist_id =:playlist_id', array(':user_id'=>$user_id,':studio_id'=>$studio_id,':playlist_id'=>$playlist_id));
            $playItem = $command->queryAll();
            $playlist = array();
            $k = 0;
            $base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
            if (count($playItem) > 0) {
                foreach ($playItem as $item) {
                    $movie_id = $item['content_id'];
                    $is_episode = $item['content_type'];

                    $command1 = Yii::app()->db->createCommand()
                            ->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id')
                            ->from('films f,movie_streams ms')
                            ->where('f.id=ms.movie_id AND ms.id=:movie_id  AND ms.is_episode =:is_episode AND ms.is_converted = 1',array(':movie_id'=>$movie_id,':is_episode'=>$is_episode));
                    $list = $command1->QueryRow();
                    if (!empty($list)) {
                        $audio_id = $list['movie_id'];
                        if ($is_episode == '1') {
                            $title = $list['episode_title'];
                            $audioid = $movie_id;
                            $type = 'moviestream';
                            $uniq_id = $list['embed_id'];
                        } else {
                            $title = $list['name'];
                            $audioid = $audio_id;
                            $type = 'films';
                            $uniq_id = $list['uniq_id'];
                        }
                        $is_favourite = Yii::app()->common->isContentFav($studio_id, $user_id, $audioid, $is_episode);
                        $playlist[$k]['content_id'] = $movie_id;
                        $playlist[$k]['movie_id'] = $audio_id;
                        $playlist[$k]['is_episode'] = $is_episode;
                        $playlist[$k]['url'] = $base_cloud_url . $list['full_movie'];
                        $playlist[$k]['audio_poster'] = $this->getPoster($audioid, $type);
                        $playlist[$k]['title'] = $title;
                        $playlist[$k]['permalink'] = $list['permalink'];
                        $playlist[$k]['is_favourite'] = $is_favourite;
                        $playlist[$k]['uniq_id'] = $uniq_id;
                        $cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
                        $celeb_name = array();
                        if (!empty($cast_details)) {
                            foreach ($cast_details as $casts) {
                                if (trim($casts['celeb_name']) != "") {
                                    $celeb_name[] = $casts['celeb_name'];
                                }
                            }
                        }
                        $casts = implode(',', $celeb_name);
                        $playlist[$k]['cast'] = $casts;
                        $k++;
                    }
                }
                if ($playlist) {
                    $res['code'] = 200;
                    $res['message'] = 'success';
                    $res['data'] = $playlist;
                } else {
                    $res['code'] = 405;
                    $res['message'] = 'error';
                    $res['data'] = $playlist;
                }
            } else {
                $res['code'] = 400;
                $res['message'] = $translate['invalid_playlist'];
                $res['status'] = 'failure';
            }
        } else {
            $res['code'] = 400;
            $res['message'] = $translate['invalid_playlist'];
            $res['status'] = 'failure';
        }
        echo json_encode($res);
        exit;
    }

    public function actionPlayListNameEdit() {
        $studio_id = $this->studio_id;
        $lang_code = $_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_name']) && ($_REQUEST['playlist_name'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $playlist_id = $_REQUEST['playlist_id'];
            $playlist_name = $_REQUEST['playlist_name'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylistName::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'id' => $playlist_id));
            if (!empty($playlist)) {
                $playlistUp = $playlist->findByPK($playlist->id);
                $playlistUp->playlist_name = $playlist_name;
                $playlistUp->save();
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['playlist_updated'];
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = $translate['no_playlist_found'];
            }
        } else {
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = $translate['playlist_name_not_blank'];
        }
        echo json_encode($res);
        exit;
    }

    function AddContentPlaylist($user_id, $movie_id, $is_episode, $playlist_id, $lang_code, $que_id, $total_playlist) {
        $studio_id = $this->studio_id;
        $translate = $this->getTransData($lang_code, $studio_id);
        $playlistitem = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'playlist_id' => $playlist_id, 'content_id' => $movie_id, 'content_type' => $is_episode));
        if (empty($playlistitem)) {
            $playlistItem = new UserPlaylist;
            $playlistItem->user_id = $user_id;
            $playlistItem->studio_id = $studio_id;
            $playlistItem->playlist_id = $playlist_id;
            $playlistItem->content_id = $movie_id;
            $playlistItem->content_type = $is_episode;
            $playlistItem->save();
            $msg = $translate['content_added_to_playlist'];
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['content_added_to_playlist'];
            $res['total'] = $total_playlist;
        } else {
            $res['code'] = 400;
            $res['status'] = "Success";
            $res['msg'] = $translate['content_added_to_playlist'];
        }
        return $res;
    }

    function AddQueuePlaylist($user_id, $playlist_id, $lang_code, $que_id, $total_playlist) {
        $studio_id = $this->studio_id;
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id_que = 0;
        $queue = QueueList::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id_que, 'id' => $que_id));
        $old_data = unserialize($queue->queuelist_data);
        $playlistitem = Yii::app()->db->createCommand()
                ->select('content_id')
                ->from('user_playlist u')
                ->where('u.studio_id=' . $studio_id . ' AND u.playlist_id = ' . $playlist_id . ' AND u.user_id = ' . $user_id . '')
                ->queryAll();
        if (count($old_data) > 0) {
            foreach ($old_data as $data) {
                $movie_id = $data['content_id'];
                $is_episode = $data['is_episode'];
                $match = array_search($movie_id, array_column($playlistitem, 'content_id'));
                if (empty($match) && $match !== 0) {
                    $playlistItem = new UserPlaylist;
                    $playlistItem->user_id = $user_id;
                    $playlistItem->studio_id = $studio_id;
                    $playlistItem->playlist_id = $playlist_id;
                    $playlistItem->content_id = $movie_id;
                    $playlistItem->content_type = $is_episode;
                    $playlistItem->save();
                    $msg = $translate['content_added_to_playlist'];
                } else {
                    $msg = $translate['content_added_to_playlist'];
                }
            }

            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $msg;
            $res['total'] = $total_playlist;
        } else {
            $res['code'] = 400;
            $res['status'] = "Error";
            $res['msg'] = $translate['invalid_data'];
        }
        return $res;
    }

    public function actionUpdateQueueList() {
        $studio_id = $this->studio_id;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
        $new_data = $_REQUEST['queue_data'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $is_del = 0;
        $is_init = 1;
        $tot = '';
        $is_exist = 0;
        if ((isset($_REQUEST['queue_id'])) && ($_REQUEST['queue_id'] > 0)) {
            $queue = QueueList::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'id' => $_REQUEST['queue_id']));
            if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'add') {
                $old_data = unserialize($queue->queuelist_data);
                if (count($new_data) > 0) {
                    $old_data = array();
                    $old_data = $new_data;
                }
                $queue->queuelist_data = serialize($old_data);
                $queue->save();
                $queuelist_id = $queue->id;
            } elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete') {
                $data = unserialize($queue->queuelist_data);
                $content_id = $_REQUEST['content_id'];
                $match_key = array_search($content_id, array_column($data, 'content_id'));
                $total = count($data);
                unset($data[$match_key]);
                $old_data = array_values($data);
                $tot = $total - 1;
                $queue->queuelist_data = serialize($old_data);
                $queue->save();
                $queuelist_id = $queue->id;
                $is_del = 1;
            } elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'add_new') {
                $old_data = unserialize($queue->queuelist_data);
                if (count($new_data) > 0) {
                    $total = count($old_data);
                    $k = 0;
                    $match_new = array_search($new_data[$k]['content_id'], array_column($old_data, 'content_id'));
                    if ($old_data) {
                        if (empty($match_new) && $match_new !== 0) {
                            $tot = $total + 1;
                            $old_data[$total] = $new_data[$k];
                            $msg = $translate['added_to_queue'];
                        } else {
                            $tot = $total;
                            $msg = $translate['added_to_queue'];
                            $new_data = '';
                            $is_exist = 1;
                        }
                        $queue->queuelist_data = serialize($old_data);
                    }
                    $queue->save();
                    $queuelist_id = $queue->id;
                }
            } elseif (isset($_REQUEST['action']) && $_REQUEST['action'] == 'clear_que') {
                $old_data = unserialize($queue->queuelist_data);
                if (count($old_data) > 0) {
                    $queue->delete();
                    $msg = 'delete';
                    $old_data = '';
                    $is_del = 1;
                }
            }
        } else {
            if ($_REQUEST['action'] == 'add_new') {
                $msg = $translate['added_to_queue'];
                $is_init = 0;
            }
            $old_data = $new_data;
            $queue = new QueueList;
            $queue->studio_id = $studio_id;
            $queue->queuelist_data = serialize($new_data);
            $queue->user_id = $user_id;
            $queue->save();
            $queuelist_id = $queue->id;
        }
        $res['code'] = 200;
        $res['queue_id'] = $queuelist_id;
        $res['data'] = $old_data;
        $res['index'] = $tot;
        $res['msg'] = $msg;
        $res['is_del'] = $is_del;
        $res['is_init'] = $is_init;
        $res['is_exist'] = $is_exist;
        echo json_encode($res);
        exit;
    }

    public function actionAddToQue() {
        $studio_id = $this->studio_id;
        $movie_id = $_REQUEST['content_id'];
        $is_episode = $_REQUEST['is_episode'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $command1 = Yii::app()->db->createCommand()
                ->select('f.name,f.permalink,ms.episode_title,ms.movie_id,ms.full_movie,ms.is_episode,ms.embed_id,f.uniq_id')
                ->from('films f,movie_streams ms')
                ->where('f.id=ms.movie_id AND ms.id=' . $movie_id . ' AND ms.is_episode = ' . $is_episode . '');
        $list = $command1->QueryRow();
        $playlist = array();
        $base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);
        if (!empty($list)) {
            $k = 0;
            $audio_id = $list['movie_id'];
            if ($is_episode == '1') {
                $title = $list['episode_title'];
                $audioid = $movie_id;
                $type = 'moviestream';
                $uniq_id = $list['embed_id'];
            } else {
                $title = $list['name'];
                $audioid = $audio_id;
                $type = 'films';
                $uniq_id = $list['uniq_id'];
            }
            $is_favourite = Yii::app()->common->isContentFav($studio_id, $user_id, $audioid, $is_episode);
            $playlist[$k]['content_id'] = $movie_id;
            $playlist[$k]['movie_id'] = $audio_id;
            $playlist[$k]['is_episode'] = $is_episode;
            $playlist[$k]['url'] = $base_cloud_url . $list['full_movie'];
            $playlist[$k]['audio_poster'] = $this->getPoster($audioid, $type);
            $playlist[$k]['title'] = $title;
            $playlist[$k]['permalink'] = $list['permalink'];
            $playlist[$k]['is_favourite'] = $is_favourite;
            $playlist[$k]['uniq_id'] = $uniq_id;
            $cast_details = $this->getCasts($audio_id, '', $language_id, $studio_id);
            $celeb_name = array();
            if (!empty($cast_details)) {
                foreach ($cast_details as $casts) {
                    if (trim($casts['celeb_name']) != "") {
                        $celeb_name[] = $casts['celeb_name'];
                    }
                }
            }
            $casts = implode(',', $celeb_name);
            $playlist[$k]['cast'] = $casts;
            if ($playlist) {
                $res['code'] = 200;
                $res['message'] = 'success';
                $res['data'] = $playlist;
            } else {
                $res['code'] = 405;
                $res['message'] = 'error';
                $res['data'] = $playlist;
            }
        } else {
            $res['code'] = 400;
            $res['message'] = $translate['invalid_data'];
            $res['status'] = 'failure';
        }
        echo json_encode($res);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: adding customizer log 
     *
     */
    public function actionAddCustomizerLog() {
        $product_id = @$_REQUEST['product_id'];
        $user_id = @$_REQUEST['user_id'];

        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        if ($product_id > 0) {
            $log = new CustomizerLog();
            $log->created_date = new CDbExpression("NOW()");
            $log->studio_id = $this->studio_id;
            $log->product_id = $product_id;
            $log->user_id = $user_id;
            $log->status = 1;
            $log->save();
            $log_id = $log->id;
            if ($log_id) {
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['msg'] = "log created";
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Record can not be inserted";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Record can not be inserted";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: adding cms customizer log 
     *
     */
    public function actionAddCmsCustomizerLog() {
        $product_id = @$_REQUEST['product_id'];
        $user_id = @$_REQUEST['user_id'];

        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        if ($product_id > 0) {
            $log = new CmsCustomizerLog();
            $log->created_date = new CDbExpression("NOW()");
            $log->studio_id = $this->studio_id;
            $log->product_id = $product_id;
            $log->user_id = $user_id;
            $log->status = 1;
            $log->save();
            $log_id = $log->id;
            if ($log_id) {
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['msg'] = "log created";
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Record can not be inserted";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Record can not be inserted";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: updating customizer log 
     *
     */
    function actionUpdateCustomizerLogDetails() {
        $log_id = @$_REQUEST['id'];
        $log = CustomizerLogDetails::model()->findByPk($log_id, array('select' => 'id'));
        if (!$log)
            $log = new CustomizerLogDetails();
        $log->created_date = new CDbExpression("NOW()");
        $log->studio_id = $this->studio_id;
        $log->product_id = $product_id;
        $log->user_id = $user_id;
        $log->status = 1;
        $log->save();
    }

    /** author:debayan@muvi.com
     *  use: get customizer log to ng 
     *
     */
    function actionGetCustomizerLogInfo() {

        $command = Yii::app()->db->createCommand()
                ->select('id, user_id, product_image')
                ->from('customizer_log')
                ->where('studio_id=:studio_id', array(':studio_id' => $this->studio_id))
                ->order('id DESC')
                ->limit('1');
        $getNewLog = $command->queryRow();
        echo json_encode($getNewLog);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get cms customizer log to ng 
     *
     */
    function actionGetCmsCustomizerLogInfo() {

        $command = Yii::app()->db->createCommand()
                ->select('id, user_id, product_image, studio_id')
                ->from('cms_customizer_log')
                ->where('studio_id=:studio_id', array(':studio_id' => $this->studio_id))
                ->order('id DESC')
                ->limit('1');
        $getNewLog = $command->queryRow();
        echo json_encode($getNewLog);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting customizer log info of that perticular id 
     *
     */
    function actionGetCustomizerLogReady() {
        $data = json_decode(file_get_contents("php://input"));

        $val = str_replace('"', '', $data);

        $command = Yii::app()->db->createCommand()
                ->select('id,product_id, product_image')
                ->from('customizer_log')
                ->where("id =:id", array(':id' => $val));
        $getLog = $command->queryRow();
        echo json_encode($getLog);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting cms customizer log info of that perticular id 
     *
     */
    function actionGetCmsCustomizerLogReady() {
        $data = json_decode(file_get_contents("php://input"));

        $val = str_replace('"', '', $data);
        $command = Yii::app()->db->createCommand()
                ->select('id,studio_id,product_id, product_image')
                ->from('cms_customizer_log')
                ->where("product_id =:product_id", array(':product_id' => $val));
        $getLog = $command->queryRow();
        echo json_encode($getLog);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting customizer fontstyle
     *
     */
    function actionGetCustomizerFontStyle() {
        $product_id = json_decode(file_get_contents("php://input"));
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_fonts_style')
                ->where('studio_id=:studio_id  AND text_style_show_status = "1" AND product_id=:product_id', array(':studio_id' => $this->studio_id, ':product_id' => $product_id));
        $getstyles = $command->queryAll();
        echo json_encode($getstyles);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting cms customizer fontstyle
     *
     */
    function actionGetCmsCustomizerFontStyle() {
        $product_id = json_decode(file_get_contents("php://input"));
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_fonts_style')
                ->where('studio_id=:studio_id AND product_id=:product_id', array(':studio_id' => $this->studio_id, ':product_id' => $product_id));
        $getstyles = $command->queryAll();
        echo json_encode($getstyles);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting customizer colors 
     *
     */
    function actionGetCustomizerColors() {
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_font_colors')
                ->where('studio_id=:studio_id AND color_show_status = "1"', array(':studio_id' => $this->studio_id));
        $getColors = $command->queryAll();
        echo json_encode($getColors);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: getting cms customizer colors 
     *
     */
    function actionGetCmsCustomizerColors() {
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_font_colors')
                ->where("studio_id=:studio_id", array(':studio_id' => $this->studio_id));
        $getColors = $command->queryAll();
        echo json_encode($getColors);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: add cms customizer color when submited
     *
     */
    function actionAddCmsCustomizerColor() {
        $data = json_decode(file_get_contents("php://input"));
        $colorCode = $data->color_code;
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_font_colors')
                ->where('studio_id=:studio_id AND color_code=:color_code', array(':studio_id' => $this->studio_id, ':color_code' => $colorCode));
        $getColors = $command->queryAll();
        if ($getColors) {
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "color exits";
        } else {
            $log = new CmsCustomizerFontColors();
            $log->studio_id = $data->studio_id;
            $log->color_text = $data->color_text;
            $log->color_code = $data->color_code;
            $log->tool_type_id = $data->tool_type_id;
            $log->save();
            $log_id = $log->id;
            if ($log_id) {
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['msg'] = "log created";
            } else {
                $cdata['code'] = 448;
                $cdata['status'] = "Failure";
                $cdata['msg'] = "Record can not be inserted";
            }
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: display cms customizer color if checked
     * Last Mddification : biswajitdas@muvi.com
     */
    function actionUpdateCmsColorStatusCheckedValue() {
        $data = json_decode(file_get_contents("php://input"));
        $id = $data->id;
		$customize_id = $data->change_customize_id;
        $log = CmsCustomizerFontColors::model()->findByAttributes(array('id'=>$id,'customize_text_id'=>$customize_id)); //here i have to change
		if(!empty($log)){
			$log = CmsCustomizerFontColors::model()->findByPk($id);
        $log->color_show_status = $data->color_show_status;
			$log->customize_text_id = $customize_id;
		}else{
			$log = CmsCustomizerFontColors::model()->findByAttributes(array('id'=>$id,'color_code'=>$data->color_code));
			if($log->customize_text_id == ''){
				$log->color_show_status = $data->color_show_status;
				$log->customize_text_id = $customize_id;
			}else{
				$log = new CmsCustomizerFontColors;
				$log->color_text='new color';
				$log->color_code =$data->color_code;
				$log->studio_id = $this->studio_id;
				$log->tool_type_id  =$data->tool_type_id ;
				$log->color_show_status = $data->color_show_status;
				$log->customize_text_id = $customize_id;
			}
		}
        $log->save();
		$data->id=$log->id;
        echo json_encode($data);
        exit;
    }

    function actionUpdateCmsTextStatusValue() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data->id;
		$customize_id = $data->change_customize_id;
		$log = CmsCustomizerFontsStyle::model()->findByAttributes(array('id'=>$log_id,'customize_text_id'=>$customize_id)); //here i have to change
		if(!empty($log)){
        $log = CmsCustomizerFontsStyle::model()->findByPk($log_id);
        $log->text_style_show_status = $data->text_style_show_status;
			$log->customize_text_id = $customize_id;
		}else{
			$log = CmsCustomizerFontsStyle::model()->findByAttributes(array('id'=>$log_id,'font_style_text'=>$data->text_code));
			if($log->customize_text_id == ''){
				$log->text_style_show_status = $data->text_style_show_status;
				$log->customize_text_id = $data->change_customize_id;
			}else{
				$log = new CmsCustomizerFontsStyle;
				$log->font_style =$data->text_code;
				$log->studio_id = $this->studio_id;
				$log->product_id =$data->product_id;
				$log->font_style_text = $data->text_code;
				$log->tool_type_id  =$data->tool_type_id ;
				$log->text_style_show_status = $data->text_style_show_status;
				$log->customize_text_id = $customize_id;
			}
		}
        $log->save();
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: delete cms customizer color 
     *
     */
    function actionCmsColorDelete() {
        $data = json_decode(file_get_contents("php://input"));
        $id = $data;
        $log = CmsCustomizerFontColors::model()->deleteByPk($id);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get customizer text 
     *
     */
    function actionGetCustomizerTexts() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, texts, font_size, font_weight, font_color, font_style, coordinatesy, coordinatesx, degrees, image_height, image_width, max_length, max_font_size, min_font_size, font_family, font_underline')
                ->from('customizer_log_details')
                ->where("reset_flag ='0' AND image_url ='null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionCustomizerHideText() {
        $Id = json_decode(file_get_contents("php://input"));
        $log = CustomizerLogDetails::model()->findByPk($Id);
        if ($log != null) {
            $log->reset_flag = 1;
            $log->save();
        }
    }

    function actionCustomizerShowAll() {
        $log_id = json_decode(file_get_contents("php://input"));

        $log = new CustomizerLogDetails;
        $attr = array('reset_flag' => 0);
        $condition = "log_id=:log_id";
        $params = array(':log_id' => $log_id);
        $log = $log->updateAll($attr, $condition, $params);
    }

    function actionCustomizerShowHiddenText() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, texts, font_size, font_weight, font_color, font_style, coordinatesy, coordinatesx, degrees, image_height, image_width, max_length, max_font_size, min_font_size, font_family, font_underline')
                ->from('customizer_log_details')
                ->where("reset_flag ='1' AND image_url ='null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionCustomizerShowHiddenImage() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, image_url, image_height, image_width, coordinatesy, coordinatesx, degrees')
                ->from('customizer_log_details')
                ->where("reset_flag = '1' AND texts = 'null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getSelectedImages = $command->queryAll();
        echo json_encode($getSelectedImages);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get cms customizer text 
     *
     */
    function actionGetCmsCustomizerTexts() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, texts, font_size, font_weight, font_color, font_style, coordinatesy, coordinatesx, degrees, image_height, image_width, max_length, max_font_size, min_font_size, z_index_value')
                ->from('cms_customizer_log_details')
                ->where("reset_flag ='0' AND image_url ='null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionGetCustomizerDefaultImage() {
        $data = json_decode(file_get_contents("php://input"));
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('customizer_images')
                ->where('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        $getDefaultImages = $command->queryAll();
        echo json_encode($getDefaultImages);
        exit;
    }

    function actionCheckCmsLog(){
        $data = json_decode(file_get_contents("php://input"));
        $log_id =  $data;
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_log')
                ->where('id =:log_id AND actual_product_width_ratio IS NULL AND actual_product_height_ratio IS NULL', array(':log_id' => $log_id));
        $checkAllLog = $command->queryAll();
        if($checkAllLog){
                $log = CmsCustomizerLog::model()->findByPk($log_id);
                $log->actual_product_width_ratio = 1.7;
                $log->actual_product_height_ratio = 1.7;
                $log->height_in_cm = 30;
                $log->width_in_cm = 30;
                $log->save();   
                 $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "insert";
        }
        else{
              $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "ok not null";
        }
         $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionCheckLogId() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('customizer_log_details')
                ->where('log_id =:log_id', array(':log_id' => $log_id));
        $checkAllLog = $command->queryAll();
        if (!empty($checkAllLog)) {
            echo "log exist";
        } else {
            echo "no log";
        }
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get customizer images 
     *
     */
    function actionGetCustomizerSelectedImage() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, image_url, image_height, image_width, coordinatesy, coordinatesx, degrees')
                ->from('customizer_log_details')
                ->where("reset_flag = '0' AND texts = 'null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getSelectedImages = $command->queryAll();
        echo json_encode($getSelectedImages);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get cms customizer images 
     *
     */
    function actionGetCmsCustomizerSelectedImage() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id, image_url, image_height, image_width, coordinatesy, coordinatesx, degrees, z_index_value')
                ->from('cms_customizer_log_details')
                ->where("reset_flag = '0' AND texts = 'null' AND log_id =:log_id", array(':log_id' => $log_id));
        $getSelectedImages = $command->queryAll();
        echo json_encode($getSelectedImages);
        exit;
    }

    function actionCmsAddStyleText() {
        $prod_id = json_decode(file_get_contents("php://input"));
        $studio_id = $this->studio_id;
        $command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('cms_customizer_fonts_style')
                ->where("product_id =:product_id", array(':product_id' => $prod_id));
        $log = $command->queryAll();
        if (!empty($log)) {
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log exist";
        } else {
            $insert = Yii::app()->db->createCommand()
                    ->select('font_style, tool_type_id, font_style_text, text_style_show_status')
                    ->from('cms_customizer_fonts_style')
                    ->where('studio_id= 0')
                    ->queryAll();
            if (!empty($insert)) {
                foreach ($insert as $data) {
                    $log = new CmsCustomizerFontsStyle();
                    $log->font_style = $data['font_style'];
                    $log->studio_id = $studio_id;
                    $log->product_id = $prod_id;
                    $log->tool_type_id = $data['tool_type_id'];
                    $log->font_style_text = $data['font_style_text'];
                    $log->text_style_show_status = $data['text_style_show_status'];
                    $log->save();
                    $cdata['code'] = 200;
                    $cdata['status'] = "Ok";
                    $cdata['msg'] = "created";
                }
            }
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: get customizer add text
     *
     */
    function actionAddCustomizerText() {
        $logs = json_decode(file_get_contents("php://input"));
        if (!empty($logs)) {
            $log = new CustomizerLogDetails();
            foreach ($logs as $data) {
                $log->log_id = $data->log_id;
                $log->last_updated = new CDbExpression("NOW()");
                $log->user_id = $data->user_id;
                $log->tool_type_id = $data->tool_type_id;
                $log->coordinatesx = $data->coordinatesx;
                $log->texts = $data->texts;
                $log->font_size = $data->font_size;
                $log->font_color_id = $data->font_color_id;
                $log->image_id = $data->image_id;
                $log->image_url = $data->image_url;
                $log->image_height = $data->image_height;
                $log->image_width = $data->image_width;
                $log->coordinatesy = $data->coordinatesy;
                $log->font_weight = $data->font_weight;
                $log->font_style_id = $data->font_style_id;
                $log->reset_flag = $data->reset_flag;
                $log->degrees = $data->degrees;
                $log->font_color = $data->font_color;
                $log->font_style = $data->font_style;
                $log->max_length = $data->max_length;
                $log->max_font_size = $data->max_font_size;
                $log->min_font_size = $data->min_font_size;
                $log->setIsNewRecord(true);
                $log->setPrimaryKey(NULL);
                $log->save();
            }
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log created";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: add cms customizer add text and images
     *
     */
    function actionAddCustomizerImage() {
        $logs = json_decode(file_get_contents("php://input"));

        if (!empty($logs)) {
            $log = new CustomizerLogDetails();
            foreach ($logs as $data) {
                $log->log_id = $data->log_id;
                $log->last_updated = new CDbExpression("NOW()");
                $log->user_id = $data->user_id;
                $log->tool_type_id = $data->tool_type_id;
                $log->coordinatesx = $data->coordinatesx;
                $log->texts = "null";
                $log->font_size = $data->font_size;
                $log->font_color_id = $data->font_color_id;
                $log->image_id = $data->image_id;
                $log->image_url = $data->image_url;
                $log->image_height = $data->image_height;
                $log->image_width = $data->image_width;
                $log->coordinatesy = $data->coordinatesy;
                $log->font_weight = $data->font_weight;
                $log->font_style_id = $data->font_style_id;
                $log->reset_flag = $data->reset_flag;
                $log->degrees = $data->degrees;
                $log->font_color = $data->font_color;
                $log->font_style = $data->font_style;
                $log->max_length = $data->max_length;
                $log->max_font_size = $data->max_font_size;
                $log->setIsNewRecord(true);
                $log->setPrimaryKey(NULL);
                $log->save();
            }
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log created";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: add cms customizer add text and images
     *
     */
    function actionAddCmsCustomizerText() {
        $data = json_decode(file_get_contents("php://input"));
        //echo "<pre>";print_r($data);exit;
        $log = new CmsCustomizerLogDetails(); //here i ahve to change
        $log->log_id = $data->log_id;
        $log->last_updated = new CDbExpression("NOW()");
        $log->user_id = $data->user_id;
        $log->tool_type_id = $data->tool_type_id;
        $log->coordinatesx = $data->coordinatesx;
        $log->texts = $data->texts;
        $log->font_size = $data->font_size;
        $log->font_color_id = $data->font_color_id;
        $log->image_id = $data->image_id;
        $log->image_url = $data->image_url;
        $log->image_height = $data->image_height;
        $log->image_width = $data->image_width;
        $log->coordinatesy = $data->coordinatesy;
        $log->font_weight = $data->font_weight;
        $log->font_style_id = $data->font_style_id;
        $log->reset_flag = $data->reset_flag;
        $log->degrees = $data->degrees;
        $log->font_color = $data->font_color;
        $log->font_style = $data->font_style;
        $log->max_length = $data->max_length;
        $log->max_font_size = $data->max_font_size;
        $log->min_font_size = $data->min_font_size;
		$log->z_index_value = ($data->z_index) ? $data->z_index : '150';
        $log->save();
        $log_id = $log->id;
		$indexValue = ($log->z_index_value) + 1;
        if ($log_id) {
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log created";
			$cdata['index'] = $indexValue;
			$cdata['id'] = $log_id;
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: delete customizer log detail
     *
     */
    function actionDeleteCustomizerDetailLog() {
        $data_id = json_decode(file_get_contents("php://input"));
        $log = CustomizerLogDetails::model()->findByPk($data_id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->reset_flag = 1;
            $log->save();
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['msg'] = "log created";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: delete cms customizer log detail
     *
     */
    function actionDeleteCmsCustomizerDetailLog() {
        $data_id = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerLogDetails::model()->findByPk($data_id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->reset_flag = 1;
            $log->save();
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['msg'] = "log created";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: update customizer log detail Text
     *
     */
    function actionUpdateText() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CustomizerLogDetails::model()->findByPk($data->id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->texts = $data->texts;
            $log->font_size = $data->font_size;
            $log->coordinatesy = $data->coordinatesy;
            $log->font_weight = $data->font_weight;
            $log->degrees = $data->degrees;
            $log->font_color = $data->font_color;
            $log->font_style = $data->font_style;
            $log->max_length = $data->max_length;
            $log->min_font_size = $data->min_font_size;
            $log->font_family = $data->font_family;
            $log->font_underline = $data->font_underline;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log created";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: update  cms customizer log detail Text
     *
     */
    function actionUpdateCmsText() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerLogDetails::model()->findByPk($data->id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->texts = $data->texts;
            $log->font_size = $data->font_size;
            $log->coordinatesy = $data->coordinatesy;
            $log->font_weight = $data->font_weight;
            $log->degrees = $data->degrees;
            $log->font_color = $data->font_color;
            $log->font_style = $data->font_style;
            $log->max_length = $data->max_length;
            $log->max_font_size = $data->max_font_size;
            $log->min_font_size = $data->min_font_size;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log updated";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: update customizer log detail Text
     *
     */
    function actionUpdateSelectedImage() {
        $data = json_decode(file_get_contents("php://input"));

        $log = CustomizerLogDetails::model()->findByPk($data->id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->coordinatesy = $data->coordinatesy;
            $log->image_height = $data->image_height;
            $log->image_width = $data->image_width;
            $log->degrees = $data->degrees;
            $log->save();
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['msg'] = "log updated";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: update cms customizer log detail Image
     *
     */
    function actionUpdateCmsSelectedImage() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerLogDetails::model()->findByPk($data->id);
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->coordinatesy = $data->coordinatesy;
            $log->image_height = $data->image_height;
            $log->image_width = $data->image_width;
            $log->degrees = $data->degrees;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log updated";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    /** author:debayan@muvi.com
     *  use: rest all customize log detail
     *
     */
    function actionCustomizerDeleteAll() {
        $data = json_decode(file_get_contents("php://input"));
        $cond = "log_id = $data";
        //print_r($cond);
        $attr = array("last_updated" => new CDbExpression("NOW()"), "reset_flag" => 1);
        $cust = new CustomizerLogDetails;
        $cust->updateAll($attr, $cond);
        exit;
    }

    function actionUserCustomizerMaster() {
        $data = json_decode(file_get_contents("php://input"));
        $product_id = $data->product_id;
        $command = Yii::app()->db->createCommand()
                ->select('id, product_id, coordinatesy, coordinatesx, degrees, master_height, master_width, master_height_in_cm, master_width_in_cm')
                ->from('cms_customizer_master')
                ->where("product_id =:product_id", array(':product_id' => $product_id));
        $getBox = $command->queryAll();
        echo json_encode($getBox);
    }

    function actionCreateCmsCustomizerMaster() {
        $data = json_decode(file_get_contents("php://input"));
        $product_id = $data->product_id;
        $val = str_replace('"', '', $product_id);
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_master')
                ->where("product_id =:product_id", array(':product_id' => $val));
        $checkAllLog = $command->queryAll();
        if (!empty($checkAllLog)) {
            $command = Yii::app()->db->createCommand()
                    ->select('id, product_id, coordinatesy, coordinatesx, degrees, master_height, master_width,  master_height_in_cm, master_width_in_cm')
                    ->from('cms_customizer_master')
                    ->where("product_id =:product_id", array(':product_id' => $val));
            $getBox = $command->queryAll();
            echo json_encode($getBox);
        } else {
            $data = json_decode(file_get_contents("php://input"));
            $log = new CmsCustomizerMaster();
            $product_id = $data->product_id;
            $val = str_replace('"', '', $product_id);
            $log->product_id = $val;
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->coordinatesy = $data->coordinatesy;
            $log->degrees = $data->degrees;
            $log->master_width = $data->master_width;
            $log->master_height = $data->master_height;
            $log->master_width_in_cm = $data->master_width_in_cm;
            $log->master_height_in_cm = $data->master_height_in_cm;
            $log->save();
            $log_id = $log->id;
            if ($log_id) {
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['msg'] = "log created";
                $cdata['id'] = $log->id;
                $cdata['product_id'] = $log->product_id;
                $cdata['coordinatesx'] = $log->coordinatesx;
                $cdata['coordinatesy'] = $log->coordinatesy;
                $cdata['degrees'] = $log->degrees;
                $cdata['master_width'] = $log->master_width;
                $cdata['master_height'] = $log->master_height;
            } else {
                $cdata['code'] = 448;
                $cdata['status'] = "Failure";
                $cdata['msg'] = "Record can not be inserted";
            }
            $this->setHeader($cdata['code']);
            echo json_encode($cdata);
        }
        exit;
    }

    function actionResetMasterBox() {
        $data = json_decode(file_get_contents("php://input"));
        $id = $data;
        $log = CmsCustomizerMaster::model()->deleteByPk($id);
        $cdata['code'] = 200;
        $cdata['status'] = "Ok";
        $cdata['msg'] = "log created";
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionUpdateCmsCustomizerMaster() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerMaster::model()->findByPk($data->id);
        if($data->master_height != null && $data->master_width != null){
        if (!empty($log)) {
            $log->last_updated = new CDbExpression("NOW()");
            $log->coordinatesx = $data->coordinatesx;
            $log->coordinatesy = $data->coordinatesy;
            $log->degrees = $data->degrees;
            $log->master_width = $data->master_width;
            $log->master_height = $data->master_height;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
                $cdata['msg'] = "master height,width,degrees and coordinates updated ";
            } 
            else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        }
        else if($data->master_width !=null && $data->master_height == null){
              if (!empty($log)) {
                $log->last_updated = new CDbExpression("NOW()");
                $log->master_width = $data->master_width;
                $log->save();
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['status'] = "master width updated";
            } 
            else {
                $cdata['code'] = 448;
                $cdata['status'] = "Failure";
                $cdata['msg'] = "Record can not be inserted";
              }
           
        }
        else{
            if (!empty($log)) {
                $log->last_updated = new CDbExpression("NOW()");
                $log->master_height = $data->master_height;
                $log->save();
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['status'] = "master height is updated";
            } 
            else {
                $cdata['code'] = 448;
                $cdata['status'] = "Failure";
                $cdata['msg'] = "Record can not be inserted";
              }
            
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionGetCmsCustomizerGlobalFonts() {
        $command = Yii::app()->db->createCommand()
                ->select('id, font_name, font_family, font_sample, studio_id,')
                ->from('cms_customizer_global_fonts')
                ->where("avalable_flag = '0' ");
        $getCmsGlobalFont = $command->queryAll();
        echo json_encode($getCmsGlobalFont);
    }

    function actionCustomizerSetTextBlank() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CustomizerLogDetails::model()->findByPk($data->id);
        if ($log) {
            $log->texts = "";
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "success";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
    }

    function actionCustomizerSetImageBlank() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CustomizerLogDetails::model()->findByPk($data->id);
        if ($log) {
            $log->image_url = Yii::app()->custom->getDefaultPersonalizationImage();
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "success";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be inserted";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
    }
    function actionCmsAddFontFamily() {
        $data = json_decode(file_get_contents("php://input"));
        $log = new CmsCustomizerFontFamily();
        $product_id = $data->product_id;
        $studio_id = $data->studio_id;
        $family_name = $data->family_name;
        $command = Yii::app()->db->createCommand()
                ->select('family_name')
                ->from('cms_customizer_fonts')
                ->where("product_id =:product_id AND studio_id = :studio_id AND family_name = :family_name", array(':product_id' => $product_id, ':studio_id' => $studio_id, ':family_name' => $family_name));
        $font = $command->queryAll();
        if ($font) {
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "This Style Already Exit";
            $this->setHeader($cdata['code']);
            echo json_encode($cdata);
        } else {
            if ($data != null) {
                $log->family_name = $data->family_name;
                $log->font_file_path = $data->font_file_path;
                $log->is_enabled = $data->is_enabled;
                $log->studio_id = $data->studio_id;
                $log->product_id = $data->product_id;
                $log->save();
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['msg'] = "success";
            } else {
                $cdata['code'] = 448;
                $cdata['status'] = "Failure";
                $cdata['msg'] = "Record can not be inserted";
            }
            $this->setHeader($cdata['code']);
            echo json_encode($cdata);
        }
        exit;
    }

    function actionGetCmsFontFamily() {
        $data = json_decode(file_get_contents("php://input"));
        $product_id = $data->product_id;
        $studio_id = $data->studio_id;
        $command = Yii::app()->db->createCommand()
                ->select('id, family_name, font_file_path, is_enabled, studio_id, product_id')
                ->from('cms_customizer_fonts')
                ->where("product_id =:product_id AND studio_id = :studio_id", array(':product_id' => $product_id, ':studio_id' => $studio_id));
        $getFontFamily = $command->queryAll();
        echo json_encode($getFontFamily);
        exit;
    }
    function actionGetUserFontFamily() {
        $data = json_decode(file_get_contents("php://input"));
        $product_id = $data->product_id;
        $studio_id = $data->studio_id;
        $command = Yii::app()->db->createCommand()
                ->select('id, family_name, font_file_path, is_enabled, studio_id, product_id')
                ->from('cms_customizer_fonts')
                ->where("product_id =:product_id AND studio_id = :studio_id AND is_enabled = '1'", array(':product_id' => $product_id, ':studio_id' => $studio_id));
        $getFontFamily = $command->queryAll();
        echo json_encode($getFontFamily);
        exit;
    }

    function actionDeleteCmsFontFamily() {
        $data = json_decode(file_get_contents("php://input"));
        $id = $data;
        $log = CmsCustomizerFontFamily::model()->deleteByPk($id);
        exit;
    }

    function actionUpdateFontFamilyIsEnable() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerFontFamily::model()->findByPk($data->id);
        if (!empty($log)) {
            $log->is_enabled = $data->is_enabled;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log updated";
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "Failure";
            $cdata['msg'] = "Record can not be updated";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionProductActualRatio() {
        $data = json_decode(file_get_contents("php://input"));
        $log = CmsCustomizerLog::model()->findByPk($data->log_id);
        $width = $data->actual_product_width_ratio;
        $height = $data->actual_product_height_ratio;
        $width_in_cm = $data->width_in_cm;
        $height_in_cm = $data->height_in_cm;
        if ($width != null) {
            $log->actual_product_width_ratio = $width;
            $log->width_in_cm = $width_in_cm;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log width";
        }
        if ($height != null) {
            $log->actual_product_height_ratio = $height;
            $log->height_in_cm = $height_in_cm;
            $log->save();
            $cdata['code'] = 200;
            $cdata['status'] = "Ok";
            $cdata['msg'] = "log height";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionGetCustomizerRatioId() {
        $data = json_decode(file_get_contents("php://input"));
        $product_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('cms_customizer_log')
                ->where('product_id =:product_id', array(':product_id' => $product_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionGetCmsCustomizerRatioSize() {
        $data = json_decode(file_get_contents("php://input"));
        $val = str_replace('"', '', $data);
        $product_id = $val;
        $command = Yii::app()->db->createCommand()
                ->select('id, height_in_cm, width_in_cm, actual_product_width_ratio, actual_product_height_ratio')
                ->from('cms_customizer_log')
                ->where('product_id =:product_id', array(':product_id' => $product_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionCmsMasterSize() {
        $data = json_decode(file_get_contents("php://input"));
        $val = str_replace('"', '', $data->product_id);
        $log = CmsCustomizerMaster::model()->findByAttributes((array('product_id' => $val)));
        $height = $data->master_height;
        $width = $data->master_width;
        if ($log != null) {
            if ($width != null && $width != 0) {
                $log->master_width_in_cm = $width;
                $log->save();
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['msg'] = "log width";
            }
            if ($height != null && $height != 0) {
                $log->master_height_in_cm = $height;
                $log->save();
                $cdata['code'] = 200;
                $cdata['status'] = "Ok";
                $cdata['msg'] = "log height";
            }
        } else {
            $cdata['code'] = 448;
            $cdata['status'] = "failed";
            $cdata['msg'] = "failed to update";
        }
        $this->setHeader($cdata['code']);
        echo json_encode($cdata);
        exit;
    }

    function actionGetMasterBoxSize() {
        $data = json_decode(file_get_contents("php://input"));
        $val = str_replace('"', '', $data);
        $product_id = $val;
        $command = Yii::app()->db->createCommand()
                ->select('id, master_width_in_cm, master_height_in_cm')
                ->from('cms_customizer_master')
                ->where('product_id =:product_id', array(':product_id' => $product_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionGetCustomizerOriginalSize() {
        $data = json_decode(file_get_contents("php://input"));
        $log_id = $data;
        $command = Yii::app()->db->createCommand()
                ->select('actual_product_width_ratio, actual_product_height_ratio')
                ->from('cms_customizer_log')
                ->where('id =:log_id', array(':log_id' => $log_id));
        $getTexts = $command->queryAll();
        echo json_encode($getTexts);
        exit;
    }

    function actionUploadImage() {
        $user_log_det_id = $_REQUEST["userlogId"];
        $studio_id = $this->studio_id;
        if ($user_log_det_id) {
            if ((isset($_FILES["uploadFile"]["name"]) && $_FILES["uploadFile"]["name"] != '')) {
                $imgName = $_FILES['uploadFile']['name'];
                $pathinfo = pathinfo($imgName);
                $imgName = $pathinfo['filename'] . time() . '.' . $pathinfo['extension'];
                $imageData = $_FILES['uploadFile']['tmp_name'];
                $folder_path = $_SERVER['DOCUMENT_ROOT'] . "/images/public/system/customized_images";
                if (!file_exists($folder_path)) {
                    mkdir($folder_path, 0777, true);
                    chmod($folder_path, 0777);
                } else {
                    chmod($folder_path, 0777);
                }
                $customized_image_path = $folder_path . '/' . $studio_id;
                $local_path = $customized_image_path . "/";
                if (!is_dir($local_path)) {
                    mkdir($local_path);
                    @chmod($local_path, 0777);
                } else {
                    @chmod($local_path, 0777);
                }
                $target_path = $local_path . $imgName;
                $ret = copy($imageData, $target_path);
                if ($ret && file_exists($target_path)) {
                    $thumb_size = '120x120';
                    $size = '600x600';
                    require_once "Image.class.php";
                    require_once "Config.class.php";
                    require_once "Uploader.class.php";
                    spl_autoload_unregister(array('YiiBase', 'autoload'));
                    require_once "amazon_sdk/sdk.class.php";
                    spl_autoload_register(array('YiiBase', 'autoload'));
                    define("BASEPATH", dirname(__FILE__) . "/..");
                    $config = Config::getInstance();
                    $config->setUploadDir($folder_path);
                    $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                    $bucketName = $bucketInfoForLogo['bucket_name'];
                    $config->setBucketName($bucketName);
                    $s3_config = Yii::app()->common->getS3Details($studio_id);
                    $config->setS3Key($s3_config['key']);
                    $config->setS3Secret($s3_config['secret']);
                    $config->setAmount(250);  //maximum paralell uploads
                    $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
                    $config->setDimensions(array('studio_thumb' => $thumb_size, 'original' => $size));   //resize to these sizes
                    $uploader = new Uploader($studio_id);
                    $ret = $uploader->uploadFiletoS3($bucketName, $unsignedBucketPath . 'public/system/customized_images/', $imgName, $studio_id, $user_log_det_id, $target_path);
                    if ($ret) {
                        $log = CustomizerLogDetails::model()->findByPk($user_log_det_id);
                        $log->last_updated = new CDbExpression("NOW()");
                        $log->image_url = $ret['original'];
                        $log->reset_flag = '0';
                        $log->save();
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "success";
                        $data['url'] = $ret;
                        $data['id'] = $user_log_det_id;
                    } else {
                        $data['code'] = 448;
                        $data['status'] = "Failure";
                        $data['msg'] = "not ok";
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionPrintSave() {
        if (isset($_POST['data'])) {
            $studio_id = $this->studio_id;
            $template = Studio::model()->find(array("select" => "theme", "condition" => " id=" . $studio_id));
            $img = $_POST['data'];
            $imageData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
            $imgname = 'canvasimage' . '.png';
            $folder_path = $_SERVER['DOCUMENT_ROOT'] . "/images/public/system/customized_images";
            if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                chmod($folder_path, 0777);
            }
            $customized_image_path = $folder_path . '/' . $studio_id;
            $local_path = $customized_image_path . "/";
            if (!is_dir($local_path)) {
                mkdir($local_path);
                @chmod($local_path, 0777);
            }
            $target_path = $local_path . $imgname;
            $ret = file_put_contents($target_path, $imageData);
            if ($ret) {
                $folder_path_pdf = $_SERVER['DOCUMENT_ROOT'] . "/images/public/system/customized_pdf";
                if (!file_exists($folder_path_pdf)) {
                    mkdir($folder_path_pdf, 0777, true);
                    chmod($folder_path_pdf, 0777);
                }
                $customized_pdf_path = $folder_path_pdf . '/' . $studio_id;
                $local_pdf_path = $customized_pdf_path . "/";
                if (!is_dir($local_pdf_path)) {
                    mkdir($local_pdf_path);
                    @chmod($local_pdf_path, 0777);
                }
                $masterWidth = ($_POST['masterWidthx']);
                $masterHeight = ($_POST['masterHeightx']);

                require_once('fpdf/fpdf.php');
                $pdfname = 'customize.pdf';
                $target_pdf_path = $local_pdf_path . $pdfname;
                //$pdf = new FPDF();
                $pdf = new FPDF('P', 'cm', array($masterWidth, $masterHeight));
                $pdf->AddPage();
                $pdf->Image($target_path, 0, 0, -72);
                $pdf->Output($target_pdf_path, 'F');
                $URL = "/images/public/system/customized_pdf" . "/" . $studio_id . "/" . $pdfname;
                echo json_encode($URL);
                unlink($target_path);
                exit;
            }
        }
    }

    function actionSave() {

        if (isset($_POST['data'])) {
            $studio_id = $this->studio_id;
            $customized_id = 1;

            $template = Studio::model()->find(array("select" => "theme", "condition" => " id=" . $studio_id));
            $img = $_POST['data'];

            $imageData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));

            $imgname = time() . '.jpg';
            $folder_path = $_SERVER['DOCUMENT_ROOT'] . "/images/public/system/customized_images";
            $customized_image_path = $folder_path . '/' . $studio_id;
            $local_path = $customized_image_path . "/";
            if (!is_dir($local_path)) {
                mkdir($local_path);
                @chmod($local_path, 0777);
            }
            $target_path = $local_path . $imgname;
            $ret = file_put_contents($target_path, $imageData);
            if ($ret && file_exists($target_path)) {
                $thumb_size = '120x120';
                $size = '600x600';
                require_once "Image.class.php";
                require_once "Config.class.php";
                require_once "Uploader.class.php";
                spl_autoload_unregister(array('YiiBase', 'autoload'));
                require_once "amazon_sdk/sdk.class.php";
                spl_autoload_register(array('YiiBase', 'autoload'));

                define("BASEPATH", dirname(__FILE__) . "/..");
                $config = Config::getInstance();
                $config->setUploadDir($folder_path);
                $bucketInfoForLogo = Yii::app()->common->getBucketInfoForPoster($studio_id);
                $bucketName = $bucketInfoForLogo['bucket_name'];
                $config->setBucketName($bucketName);
                $s3_config = Yii::app()->common->getS3Details($studio_id);
                $config->setS3Key($s3_config['key']);
                $config->setS3Secret($s3_config['secret']);
                $config->setAmount(250);  //maximum paralell uploads
                $config->setMimeTypes(array("jpg", "gif", "png", 'jpeg')); //allowed extensions
                $config->setDimensions(array('studio_thumb' => $thumb_size, 'original' => $size));   //resize to these sizes

                $uploader = new Uploader($studio_id);
                $ret = $uploader->uploadFiletoS3($bucketName, $unsignedBucketPath . 'public/system/customized_images/', $imgname, $studio_id, $customized_id, $target_path);
                $log_id = $_COOKIE['cust_log_id'];

                $log = CustomizerLog::model()->findByPk($log_id);
                $log->created_date = new CDbExpression("NOW()");
                $log->image_url_output = $ret['original'];


                if ($log->save()) {
                    $data['code'] = 200;
                    $data['status'] = "Ok";
                    $data['msg'] = "success";
                } else {
                    $data['code'] = 200;
                    $data['status'] = "not ok";
                    $data['msg'] = "fail";
                }

                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }

        exit;
    }

    /*
     * @purpose for reminder listing page 
     * @author Sweta<sweta@muvi.com>
     */

    public function actionViewBookedContent() {
        $user_id = @$_REQUEST['user_id'];
        $date_today = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')));
        $key = (isset($_REQUEST['key']) && $_REQUEST['key'] != "") ? $_REQUEST['key'] : "";
        if ($user_id > 0) {
            if($key == 'upcoming'){
               $date = " AND c.booked_time >= '".$date_today."'";
            }
            elseif($key == 'expired'){
               $date = " AND c.booked_time < '".$date_today."'";
            }
            else{
               $date = ''; 
            }
            $lang_code = @$_REQUEST['lang_code'];
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $reminder_status = StudioConfig::model()->getconfigvalueForStudio($this->studio_id, 'reminder');
            if (!empty($reminder_status) && $reminder_status['config_value'] == 1) {
                $booklist = Yii::app()->db->createCommand()->select('c.id,c.content_id,c.content_type')->from('booked_content c')->where("c.studio_id=".$this->studio_id." AND c.user_id='".$user_id."' ".$date." ORDER BY id ASC")->queryAll();
                $final_list = array();
                if (!empty($booklist)) {
                    foreach ($booklist as $list) {
                        $final_list[] = Yii::app()->general->getContentData($list['content_id'], $list['content_type'], array(), $language_id, $this->studio_id, $user_id, '', 1);
                    }
                }
                $data['code'] = 200;
                $data['status'] = "Success";
                $data['key'] = $key;
                $data['book_list'] = $final_list;
                $data['count'] = count($final_list);
            } else {
                $data['code'] = 412;
                $data['status'] = "Failure";
                $data['msg'] = "Reminder is not enable";
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Failure";
            $data['msg'] = "User Id not found";
        }
        echo json_encode($data);
        exit;
    }

    public function actionAddContentRating() {
        $studio_id = $this->studio_id;
        $is_enable_rating = Yii::app()->db->createCommand()
                ->select('rating_activated')
                ->from('studios')
                ->where('id =:studio_id', array(':studio_id' => $this->studio_id))
                ->queryRow();
        if ($is_enable_rating['rating_activated']) {
            if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id']) && !empty($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
                $rate = new ContentRating();
                $rate = $rate::model()->find('studio_id=:studio_id AND content_id=:content_id AND user_id=:user_id', array(':studio_id' => $studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
                if (count($rate) > 0) {
                    $data['rating'] = 0;
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Review already added";
                } else {
                    if (!empty($_REQUEST['rating']) || !empty($_REQUEST['review'])) {
                        $review = !empty($_REQUEST['review']) ? urldecode($_REQUEST['review']) : '';
                        $rating = new ContentRating();
                        $rating->content_id = $_REQUEST['content_id'];
                        $rating->rating = $_REQUEST['rating'];
                        $rating->review = nl2br(addslashes($review));
                        $rating->created_date = new CDbExpression("NOW()");
                        $rating->studio_id = $studio_id;
                        $rating->user_ip = CHttpRequest::getUserHostAddress();
                        $rating->user_id = $_REQUEST['user_id'];
                        $rating->status = 1;
                        $rating->save();
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Review saved successfully";
                    } else {
                        $data['rating'] = 1;
                        $data['code'] = 200;
                        $data['status'] = "Ok";
                        $data['msg'] = "Review not added yet";
                    }
                }
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Required Parameter Missing";
            }
        } else {
            $data['code'] = 469;
            $data['status'] = "Failure";
            $data['msg'] = "Rating disabled";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionViewContentRating() {
        $studio_id = $this->studio_id;
        $is_enable_rating = Yii::app()->db->createCommand()
                ->select('rating_activated')
                ->from('studios')
                ->where('id =:studio_id', array(':studio_id' => $this->studio_id))
                ->queryRow();
        if ($is_enable_rating['rating_activated']) {
            if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id'])) {
                $showrating = 1;
                if (!empty($_REQUEST['user_id'])) {
                    $condition = " AND user_id = " . $_REQUEST['user_id'];
                    $urating = Yii::app()->db->createCommand()
                            ->select('CR.rating,CR.review,CR.created_date,U.display_name')
                            ->from('content_ratings CR,sdk_users U')
                            ->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id' . $condition, array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
                            ->queryAll();
                    if (count($urating) > 0) {
                        $showrating = 0;
                    }
                }
                /*$rating = Yii::app()->db->createCommand()
                        ->select('CR.id,CR.rating,CR.review,CR.created_date,U.display_name,CR.status')
                        ->from('content_ratings CR,sdk_users U')
                        ->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id', array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
                        ->order('CR.id DESC')
                        ->queryAll();*/
                $rating = Yii::app()->db->createCommand()
                        ->select('CR.id,CR.rating,CR.review,CR.created_date,CR.status, CR.user_id')
                        ->from('content_ratings CR')
                        ->where('CR.content_id=:content_id AND CR.studio_id=:studio_id AND CR.status=1', array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
                        ->order('CR.id DESC')
                        ->queryAll();
                $review = array();
                if (count($rating) > 0) {
                    foreach ($rating as $key => $val) {
                        $display_name = SdkUser::model()->find(array('select'=>'display_name', 'condition'=>'id=:id', 'params'=>array(':id'=>$val['user_id'])))->display_name;
                        $review[$key]['display_name'] = $display_name;
                        $review[$key]['created_date'] = $val['created_date'];
                        $review[$key]['rating'] = $val['rating'];
                        $review[$key]['review'] = $val['review'];
                        $review[$key]['status'] = $val['status'];
                    }
                }
                $data['code'] = 200;
                $data['status'] = "Ok";
                $data['rating'] = $review;
                $data['showrating'] = $showrating;
                $data['msg'] = "Review found";
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Required Parameter Missing";
            }
        } else {
            $data['code'] = 469;
            $data['status'] = "Failure";
            $data['msg'] = "Rating disabled";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionCheckRating() {
        $studio_id = $this->studio_id;
        if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id']) && !empty($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {

            $rate = new ContentRating();
            $rate = $rate::model()->find('studio_id=:studio_id AND content_id=:content_id AND user_id=:user_id', array(':studio_id' => $studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
            if (count($rate) > 0) {
                $data['rating'] = 0;
            } else {
                $data['rating'] = 1;
            }
            $data['code'] = 200;
            $data['status'] = "Ok";
            $data['msg'] = "Review saved successfully";
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Required Parameter Missing";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private GetAppMenu() Get the list of Mobile & TV Apps Menu 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of menu in json format
     * 
     */
    public function actionGetAppMenu() {
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $this->studio_id);
        $app_menu = AppMenu::model()->findByAttributes(array('studio_id' => $this->studio_id), array('select' => 'id'));
        if (!empty($app_menu)) {
            $menu_items = AppMenuItems::model()->getAllMenus($this->studio_id, $app_menu->id, $language_id);
            if (!empty($menu_items)) {
                foreach ($menu_items as $key => $value) {
                    $category_id = 0;
                    $isSubcategoryPresent = 0;
                    if ($value['link_type'] == '0') {
                        $sql = "SELECT c.id,s.id as sid FROM content_category c LEFT JOIN content_subcategory s "
                                . "ON c.id = s.category_value WHERE c.studio_id =" . $this->studio_id . " AND c.permalink='" . $value['permalink'] . "'";
                        $cat = Yii::app()->db->createCommand($sql)->queryRow();
                        if ($cat) {
                            $category_id = $cat['id'];
                            $isSubcategoryPresent = ($cat['sid']) ? 1 : 0;
                        }
                    }
                    $menu_items[$key]['category_id'] = $category_id;
                    $menu_items[$key]['isSubcategoryPresent'] = $isSubcategoryPresent;
                }

                $sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$this->studio_id} AND parent_id=0 AND status=1) AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
                $pages = Yii::app()->db->createCommand($sql)->queryAll();
                if ($language_id != 20) {
                    $pages = Yii::app()->custom->getStaticPages($this->studio_id, $language_id, $pages);
                }
                $defaultdomain = @$pages[0]['domain'];
                if (trim($defaultdomain) == "") {
                    $studio_details = Studios::model()->findByPk($this->studio_id, array('select' => 'domain'));
                    $defaultdomain = $studio_details->domain;
                }
                $default = array(0 => 0, 'link_type' => 'internal', 'display_name' => $translate['contact_us'], 'permalink' => 'contactus', 'url' => 'http://' . $defaultdomain . '/contactus');
                array_push($pages, $default);

                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['menu_items'] = $menu_items;
                $data['footer_menu'] = $pages;
            } else {
                $data['code'] = 204;
                $data['status'] = 'Failure';
                $data['msg'] = $translate['no_data'];
            }
        } else {
            $data['code'] = 204;
            $data['status'] = 'Failure';
            $data['msg'] = $translate['no_data'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private GetAppHomePage() Get home page for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of banners and featured sections in json format
     * 
     */
    public function actionGetAppHomePage() {
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $home_layout = StudioConfig::model()->getConfig($this->studio_id, 'home_layout');
        $banners = AppBanners::model()->findAllByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id_seq ASC', 'select' => 'id,image_name,banner_url'));
        $sections = $appbanner = array();
        $banner_text = "";
        if (!empty($banners)) {
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            $i = 0;
            foreach ($banners AS $key => $banner) {
                $banner_src = $banner->image_name;
                $banner_id = $banner->id;
                $appbanner[$i]['image_path'] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode(trim($banner_src));
                $appbanner[$i]['banner_url'] = $banner->banner_url;
                $i++;
            }
            $banner_text = AppBannerText::model()->getBannerText($this->studio_id, $language_id, 'banner_text')->banner_text;
        }
        if (!empty($home_layout) && ($home_layout['config_value'] == 1)) {
            $featured = 0;
        } else {
            $featured = 1;
            $sections = AppFeaturedSections::model()->getAllFeaturedSections($this->studio_id, $language_id);
        }
        $data['code'] = 200;
        $data['status'] = 'OK';
        $data['BannerSectionList'] = @$appbanner;
        $data['banner_text'] = @$banner_text;
        $data['is_featured'] = $featured;
        $data['SectionName'] = @$sections;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private GetAppFeaturedContent() Get featured contents of a particular section for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of featured contents in json format
     * @param  section_id integer
     * 
     */
    public function actionGetAppFeaturedContent() {
        $lang_code = @$_REQUEST['lang_code'];
        $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['section_id']) && $_REQUEST['section_id'] != "") {
            $domainName = $this->getDomainName();
            $contents = AppFeaturedContent::model()->getAppFeatured($_REQUEST['section_id'], $this->studio_id, $language_id, $user_id, $domainName);
            if (!empty($contents)) {
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = $contents;
            } else {
                $data['code'] = 204;
                $data['status'] = 'Failure';
                $data['msg'] = $translate['content_not_found'];
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = $translate['section_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    //only for test purpose function of billing team
    function actionCheckExpiry() {
        $studio_id = $_REQUEST['studio_id'];
        $user_id = $_REQUEST['user_id'];
        $embed_id = $_REQUEST['embed_id'];
        $return = Yii::app()->common->getContentExpiryDate($studio_id, $user_id, $embed_id);
        print_r($return);
        exit;
    }
    /*
     * @purpose for listing the free content
     * @return  Json String
     * @author Sweta<sweta@muvi.com>
    */
    public function actionGetFreeContentList(){
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $command = Yii::app()->db->createCommand()
                ->select("*")
                ->from('free_contents')
                ->where("studio_id=:studio_id",array(":studio_id"=>$studio_id));
        $freecontent = $command->queryAll();
        $final_list = array();
        if(!empty($freecontent)):
        foreach($freecontent as $content){
            $episode_id = $content['episode_id'];
            if($episode_id == 0){
               $content_id =  $content['movie_id'];
               $is_episode = 0; 
            }else{
               $content_id =  $episode_id;
               $is_episode = 1;
            }
            $final_list[] = Yii::app()->general->getContentData($content_id, $is_episode, array(), $language_id, $studio_id, $user_id, $translate);
        }
        endif;
        if($final_list){
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['freecontent_list'] = $final_list;
            $data['total_count'] = count($final_list);
        }
        else {
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
            }
        echo json_encode($data);
        exit;
    }
    /**
     * @method GetRelatedBlogContents
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related contents of a blog post 
     * @return  Json Array
     */
    public function actionGetRelatedBlogContents(){
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $this->studio_id);
        if(isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0){
            $user_id     = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $rcontents   = BlogRelatedContent::model()->getRelatedContentsOfBlogPost($_REQUEST['post_id'], $this->studio_id, $language_id, $user_id, $translate);
            if($rcontents){
                $data['code']     = 200;
                $data['status']   = 'success';
                $data['contents'] = $rcontents; 
            }else{
                $data['code']   = 204;
                $data['status'] = 'failure';
                $data['msg']    = $translate['no_data'];
            }
        }else{
            $data['code']   = 411;
            $data['status'] = "Error";
            $data['msg']    = $translate['required_data_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /**
     * @method GetRelatedBlogPosts
     * @author Biswajit Parida<support@muvi.in>
     * @purpose For getting related posts of a blog post 
     * @return  Json Array
     */
    public function actionGetRelatedBlogPosts(){
        $lang_code   = @$_REQUEST['lang_code'];
        $type        = (isset($_REQUEST['type']) && $_REQUEST['type'] !="") ? $_REQUEST['type'] : 'blog';
        $is_episode  = isset($_REQUEST['is_episode']) ? $_REQUEST['is_episode'] : 0;
        $translate   = $this->getTransData($lang_code, $this->studio_id);
        if(isset($_REQUEST['post_id']) && $_REQUEST['post_id'] > 0){
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $rposts = BlogRelatedContent::model()->getRelatedPostOfBlogPost($_REQUEST['post_id'], $this->studio_id, $type, $is_episode, $language_id);
            if($rposts){
                $data['code'] = 200;
                $data['status'] = 'success';
                $data['contents'] = $rposts; 
            }else{
                $data['code'] = 204;
                $data['status'] = 'failure';
                $data['msg'] = $translate['no_data'];
            }
        }else{
            $data['code'] = 411;
            $data['status'] = "Error";
            $data['msg'] = $translate['required_data_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /**
     * @method Get category and Subcategory
     * @author Sweta<sweta@muvi.com>
     * @purpose For getting related subcategory of a category
     * @return  Json Array
     */
    public function actionGetCategorySubCategory() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $command1 = Yii::app()->db->createCommand()
                ->select('c.id,c.category_name,c.permalink as category_permalink')
                ->from('content_category c')
                ->where('c.parent_id=0 AND c.studio_id=:studio_id ORDER BY id DESC', array(':studio_id' => $studio_id));
        $categorylistt = $command1->queryAll();
        if ($language_id != 20) {
            $translated = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select' => 'parent_id,category_name')), 'parent_id', 'category_name');
            if (!empty($categorylistt)) {
                foreach ($categorylistt as $catlist) {
                if (array_key_exists($catlist['id'], $translated)) {
                        $catlist['category_name'] = $translated[$catlist['id']];
                    }
                    $categorylist[] = $catlist;
                }
            }
        }else {
            $categorylist = $categorylistt;
        }
        if (!empty($categorylist)) {
            $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
            foreach ($categorylist as $k => $categorydata) {
                $category_id = $categorydata['id'];
                $categorylist[$k]['category_img_url'] = $this->getPoster($category_id, 'content_category', 'original', $studio_id);
                $categorylist[$k]['category_img_size'] = @$cat_img_size;
                $command2 = Yii::app()->db->createCommand()
                        ->select('s.id as subcategory_id,s.category_value,s.subcat_name as subcategory_name,s.permalink as subcategory_permalink')
                        ->from('content_subcategory s')
                        ->where('s.category_value=:category_value AND s.studio_id=:studio_id', array(':studio_id' => $studio_id, ':category_value' => $category_id));
                $subcategorylist = $command2->queryAll();
                if (!empty($subcategorylist)) {
                    foreach ($subcategorylist as $key => $subcategorydata) {
                        $subcategorylist[$key]['subcategory_img_url'] = $this->getPoster($subcategorydata['id'], 'content_subcategory', 'original', $studio_id);
                    }
                    $categorylist[$k]['subcategory_list'] = $subcategorylist;
                }
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['categorysubcategory_list'] = $categorylist;
            $data['total_count'] = count($categorylist);
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "No category added yet";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }
	/**
     * @method Reset all the customizing Did by user
     * @author BiswajitD<biswajitdas@muvi.com>
    */
	public function actionUserCustomizerReset(){
		$studio_id = $this->studio_id;
		$user_log_id = json_decode(file_get_contents("php://input"));
		$user_customize_id = CustomizerLogDetails::model()->findByAttributes(array('log_id'=>$user_log_id),array('select'=>'id'))->id;
		$product_id = CustomizerLog::model()->findByAttributes(array('studio_id'=>$studio_id,'id'=>$user_log_id),array('select'=>'product_id'))->product_id;
		$cms_customize_log_id = CmsCustomizerLog::model()->findByAttributes(array('studio_id'=>$studio_id,'product_id'=>$product_id),array('select'=>'id'))->id;
		$cms_customize_log_details = CmsCustomizerLogDetails::model()->findAllByAttributes(array('log_id'=>$cms_customize_log_id,'reset_flag'=>'0'));
		$user_custo_log = CustomizerLogDetails::model()->deleteAll('log_id = :log_id', array('log_id'=>$user_log_id));
		foreach($cms_customize_log_details as $cms_log_details){
			$user_customize_log = new CustomizerLogDetails();
			$user_customize_log->coordinatesx = $cms_log_details->coordinatesx;
			$user_customize_log->log_id = $user_log_id;
			$user_customize_log->texts = $cms_log_details->texts;
			$user_customize_log->last_updated = new CDbExpression("NOW()");
			$user_customize_log->font_size = $cms_log_details->font_size;
			$user_customize_log->font_family_id = $cms_log_details->font_family_id;
			$user_customize_log->image_id = $cms_log_details->image_id;
			$user_customize_log->image_url = $cms_log_details->image_url;
			$user_customize_log->image_height = $cms_log_details->image_height;
			$user_customize_log->image_width = $cms_log_details->image_width;
			$user_customize_log->coordinatesy = $cms_log_details->coordinatesy;
			$user_customize_log->font_weight = $cms_log_details->font_weight;
			$user_customize_log->font_style_id = $cms_log_details->font_style_id;
			$user_customize_log->reset_flag = 0;
			$user_customize_log->degrees = $cms_log_details->degrees;
			$user_customize_log->font_color = $cms_log_details->font_color;
			$user_customize_log->font_style = $cms_log_details->font_style;
			$user_customize_log->max_length = $cms_log_details->max_length;
			$user_customize_log->max_font_size = $cms_log_details->max_font_size;
			$user_customize_log->min_font_size = $cms_log_details->min_font_size;
			$user_customize_log->save();
}
		$data['code'] = 200;
		$data['status'] = "Ok";
		$data['msg'] = "Update";
		echo json_encode($data);exit;
	}
	public function actionGetCmsSelectedColorText(){
		$studio_id = $this->studio_id;
		$text_id = json_decode(file_get_contents("php://input"));
		$command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_font_colors')
                ->where("studio_id=:studio_id" , array(':studio_id' => $studio_id));
       $colors = $command->queryAll();
	   foreach ($colors as $data){
		   if($text_id != $data['customize_text_id']){
			   $data['color_show_status'] = '0';
		   }
		   $result[] = $data; 
	   }
        echo json_encode($result);
	}
	 function actionGetCmsSelectedFontStyle() {
        $input = json_decode(file_get_contents("php://input"));
        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('cms_customizer_fonts_style')
                ->where('studio_id=:studio_id AND product_id=:product_id', array(':studio_id' => $this->studio_id,':product_id'=>$input->product_id));
        $getstyles = $command->queryAll();
        foreach ($getstyles as $data){
		   if($input->customize_id != $data['customize_text_id']){
			   $data['text_style_show_status'] = '0';
		   }
		   $result[] = $data; 
	   }
        echo json_encode($result);
    }
}
