<?php
trait BillingV4 {

    /**
     * @method public authUserPaymentInfo() It will users card details and return proper message
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param string $nameOnCard Name as on Credit card
     * @param String $cvv CVV as on card
     * @param String $expiryMonth Expiry month as on Card
     * @param String $expiryYear Expiry Year as on Card
     * @param String $cardNumber Card Number 
     * @param String $email user email : Specifically require for stripe
     */
    public function actionAuthUserPaymentInfo() {
        $data = '';
        $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
        if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
            $gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
            $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
        } else {
            $res['status'] = 'Failure';
            $res['code'] = 419;
            $res['msg'] = 'No active payment gateway';
            $data = json_encode($res);
        }

        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
            $_REQUEST['card_name'] = $_REQUEST['nameOnCard'];
            $_REQUEST['card_number'] = $_REQUEST['cardNumber'];
            $_REQUEST['exp_month'] = ltrim((string) $_REQUEST['expiryMonth'], '0');
            $_REQUEST['exp_year'] = $_REQUEST['expiryYear'];

            $_REQUEST['isAPI'] = 1; //Only for stripe

            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::processCard($_REQUEST);

            $res = json_decode($data, true);
            if (isset($res['isSuccess']) && intval($res['isSuccess'])) {
                if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual' && $this->PAYMENT_GATEWAY[$gateway_code] != 'paypalpro') {
                    if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
                        //$default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
                        $studioData = Yii::app()->db->createCommand("SELECT default_currency_id FROM studios WHERE id = {$this->studio_id}")->queryRow();
                        $default_currency_id = $studioData['default_currency_id'];

                        $plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'], $this->studio_id, $default_currency_id, $_REQUEST['country']);
                        if (isset($plan_details) && !empty($plan_details) && isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
                            $price = $plan_details['price'];
                            $currency_id = $plan_details['currency_id'];
                            $currency = Currency::model()->findByPk($currency_id);

                            //Prepare an array to charge from given card
                            $user['currency_id'] = $currency->id;
                            $user['currency_code'] = $currency->code;
                            $user['currency_symbol'] = $currency->symbol;
                            $user['amount'] = $price;
                            $user['token'] = @$res['card']['token'];
                            $user['profile_id'] = @$res['card']['profile_id'];
                            $user['card_holder_name'] = $_REQUEST['card_name'];
                            $user['card_type'] = @$res['card']['card_type'];
                            $user['exp_month'] = $_REQUEST['exp_month'];
                            $user['exp_year'] = $_REQUEST['exp_year'];
                            $user['studio_name'] = Studios::model()->findByPk($this->studio_id)->name;
                            $user['user_email'] = $_REQUEST['email'];
                            $trans_data = $payment_gateway::processTransactions($user);

                            if (intval($trans_data['is_success'])) {
                                $data = json_decode($data, true);

                                $data['transaction_status'] = $trans_data['transaction_status'];
                                $data['transaction_invoice_id'] = $trans_data['invoice_id'];
                                $data['transaction_order_number'] = $trans_data['order_number'];
                                $data['transaction_dollar_amount'] = $trans_data['dollar_amount'];
                                $data['transaction_amount'] = $trans_data['amount'];
                                $data['transaction_response_text'] = $trans_data['response_text'];
                                $data['transaction_is_success'] = $trans_data['is_success'];

                                $data = json_encode($data);
                            } else {
                                $data = json_encode($trans_data);
                            }
                        }
                    }
                }
            }
        } else {
            $res['status'] = 'Failure';
            $res['code'] = 419;
            $res['msg'] = 'No active payment gatway';
            $data = json_encode($res);
        }

        $data1 = json_decode($data, TRUE);
        $this->setHeader(@$data1['code'] == 100 ? 200 : @$data1['code']);
        echo json_encode($data1);
        exit;
    }

    /**
     * @method public getStudioPlanLists() It will check if plan exists for a studio. If exists then return the plan lists
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     */
    public function actionGetStudioPlanLists() {
        $studio_id = $this->studio_id;
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $country = @$_REQUEST['country'];
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);

        $ret = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id, $default_currency_id, $country, $language_id);
        $data['code'] = 200;
        $data['status'] = "OK";
        if ($ret && !empty($ret)) {

            if (isset($ret['plans']) && !empty($ret['plans'])) {
                $plans = $ret['plans'];
                $default_plan_id = @$plans[0]->id;

                foreach ($plans AS $key => $val) {
                    if ($val->is_default) {
                        $default_plan_id = $val->id;
                    }

                    $plan[$key] = $val->attributes;
                    $plan[$key]['price'] = $val->price;

                    (array) $currency = Currency::model()->findByPk($val->currency_id);
                    $plan[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
                }
                $data['plans'] = $plan;
                $data['default_plan'] = $default_plan_id;
            } else {
                $data['code'] = 457;
                $data['plans'] = '';
                $data['gateways'] = '';
                $data['msg'] = "No subscription plan found!";
            }
            //$paymentGate = $ret['gateways'];
            //echo "<pre>";print_r($paymentGate);exit;
            //$data['gateways'] = $paymentGate[0]->attributes;
            //$data['msg'] = "Payment is enabled & plan exists!";
        } else {
            $data['code'] = 424;
            $data['plans'] = '';
            $data['gateways'] = '';
            $data['msg'] = "Payment gateway is not enabled for this studio.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public registerUser() Register the user's details 
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param string $nameOnCard Name as on Credit card
     * @param String $cvv CVV as on card
     * @param String $expiryMonth Expiry month as on Card
     * @param String $expiryYear Expiry Year as on Card
     * @param String $cardNumber Card Number 
     * @param String $name Full Name of the User
     * @param String $email Email address of the User
     * @param String $password Login Password 
     * @param String $planId Login Password 
     * @param String $card_last_fourdigit Last 4 digits of the card 
     * @param String $auth_num Authentication Number
     * @param String $token Payment Token
     * @param String $cardType Type of the card
     * @param String $referenceNo Reference Number if any
     * @param String $response_text Response text as returned by Payment gateways
     * @param String $status Status of the payment gateways
     * @param String $profileId User Profile id created in the payment gateways
     * 
     */
    public function actionRegisterUser() {
        $_REQUEST['data'] = $_REQUEST;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if ($this->actionCheckEmailExistance($_REQUEST)) {
                $studio_id = $this->studio_id;
                $name = trim($_REQUEST['data']['name']);
                $email = trim($_REQUEST['data']['email']);
                $password = $_REQUEST['data']['password'];

                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

                $gateway_code = '';
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                }

                $_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];

                if (isset($_REQUEST['transaction_is_success']) && intval($_REQUEST['transaction_is_success'])) {
                    $_REQUEST['data']['transaction_data']['transaction_status'] = $_REQUEST['transaction_status'];
                    $_REQUEST['data']['transaction_data']['invoice_id'] = $_REQUEST['transaction_invoice_id'];
                    $_REQUEST['data']['transaction_data']['order_number'] = $_REQUEST['transaction_order_number'];
                    $_REQUEST['data']['transaction_data']['dollar_amount'] = $_REQUEST['transaction_dollar_amount'];
                    $_REQUEST['data']['transaction_data']['amount'] = $_REQUEST['transaction_amount'];
                    $_REQUEST['data']['transaction_data']['response_text'] = $_REQUEST['transaction_response_text'];
                    $_REQUEST['data']['transaction_data']['is_success'] = $_REQUEST['transaction_is_success'];
                }

                $ret = SdkUser::model()->saveSdkUser($_REQUEST, $studio_id);

                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {
                        $file_name = '';

                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
                    } else {
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                    }
                    $_REQUEST['sdk_user_id'] = $ret['user_id'];
                    /* if ($_REQUEST['livestream'] == 1) {
                      $this->registerStreamUser($_REQUEST);
                      } */
                    $this->actionLogin($_REQUEST);
                } else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in registration';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            } else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in registration';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }

    //multipart user regiistration part-1 start
    function actionRegisterUserDetails() {
        $_REQUEST['data'] = $_REQUEST;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if ($this->actionCheckEmailExistance($_REQUEST)) {
                $studio_id = $this->studio_id;
                $name = trim($_REQUEST['data']['name']);
                $email = trim($_REQUEST['data']['email']);
                $password = $_REQUEST['data']['password'];

                $ret = SdkUser::model()->saveUserDetails($_REQUEST, $studio_id);
                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    } else {
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                    }
                    $_REQUEST['sdk_user_id'] = $ret['user_id'];
                    if ($_REQUEST['livestream'] == 1) {
                        $this->registerStreamUser($_REQUEST);
                    }
                    $this->actionLogin($_REQUEST);
                } else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in registration';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            } else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in registration';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }

    //multipart user registration part-1 end
    //multipart user registration part-2 start
    function actionRegisterUserPayment() {
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $user = SdkUser::model()->findAllByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $studio_id));
            if (isset($user) && !empty($user)) {

                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

                $gateway_code = '';
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                }

                $_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                $_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                $_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];

                if (isset($_REQUEST['transaction_is_success']) && intval($_REQUEST['transaction_is_success'])) {
                    $_REQUEST['data']['transaction_data']['transaction_status'] = $_REQUEST['transaction_status'];
                    $_REQUEST['data']['transaction_data']['invoice_id'] = $_REQUEST['transaction_invoice_id'];
                    $_REQUEST['data']['transaction_data']['order_number'] = $_REQUEST['transaction_order_number'];
                    $_REQUEST['data']['transaction_data']['dollar_amount'] = $_REQUEST['transaction_dollar_amount'];
                    $_REQUEST['data']['transaction_data']['amount'] = $_REQUEST['transaction_amount'];
                    $_REQUEST['data']['transaction_data']['response_text'] = $_REQUEST['transaction_response_text'];
                    $_REQUEST['data']['transaction_data']['is_success'] = $_REQUEST['transaction_is_success'];
                }
                $ret = SdkUser::model()->saveUserPayment($_REQUEST, $studio_id);

                if ($ret) {
                    if (@$ret['error']) {
                        $data['code'] = 420;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Invalid Plan!';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {

                        $file_name = '';
                        if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
                            $studio = Studios::model()->findByPk($studio_id);
                            $user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
                            $user['card_holder_name'] = $_REQUEST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
                        }
                        $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
                        $admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $ret['user_id'], '', 0);

                        $data['code'] = 200;
                        $data['msg'] = 'OK';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                } else {
                    $data['code'] = 421;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Error in processing payment!';
                    $this->setHeader($data['code']);
                    echo json_encode($data);
                    exit;
                }
            } else {
                $data['code'] = 421;
                $data['status'] = 'Failure';
                $data['msg'] = 'Error in processing payment!';
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }
        }
    }

    //multipart user registration part-2 end

    /**
     * @method private isPPVSubscribed() Check the ppv subscription taken by user or not
     * @author SMK<support@muvi.com>
     * @return json Returns the list of data in json format
     * @param string $movie_id uniq id of film or muvi
     * @param int $season_id season (optional)
     * @param string $episode_id uniq id of stream (optional)
     * @param string $purchase_type show/episode for multipart only (optional)
     * @param string $oauthToken Auth token
     * 
     */
    function actionIsPPVSubscribed() {
        if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];

            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);

            if (@$user_id) {
                $usercriteria = new CDbCriteria();
                $usercriteria->select = "is_developer";
                $usercriteria->condition = "id=$user_id";
                $userData = SdkUser::model()->find($usercriteria);
            }
            $data = array();

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $movie_id = Yii::app()->common->getMovieId($movie_code);

            $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
            $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            if (@$userData->is_developer) {
                $mov_can_see = 'allowed';
            } else {
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['season'] = @$season;
                $can_see_data['stream_id'] = $stream_id;
                $can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['country'] = @$_REQUEST['country'];
                $can_see_data['is_app'] = 1;
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            }

            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['status'] = "False";
                $data['msg'] = $translate['activate_subscription_watch_video'];
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['status'] = "False";
                $data['msg'] = $translate['reactivate_subscription_watch_video'];
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['status'] = "False";
                $data['msg'] = $translate['video_restiction_in_your_country'];
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'access_period') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['access_period_expired'];
            } else if ($mov_can_see == 'watch_period') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['watch_period_expired'];
            } else if ($mov_can_see == 'maximum') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['crossed_max_limit_of_watching'];
            } else if ($mov_can_see == 'already_purchased') {
                $data['code'] = 428;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'allowed_watch_duration') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
                $data['is_daily_watch_duration'] = 1;
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else if ($mov_can_see == 'unpaid') {
                $data['code'] = 430;
                $data['status'] = "False";
                $data['msg'] = 'Unpaid';

                $data['member_subscribed'] = 0;
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
                }
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = "Muvi id required";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionisContentAuthorized() {
        if ($_REQUEST['movie_id'] && $_REQUEST['user_id']) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
            if (@$user_id) {
                $usercriteria = new CDbCriteria();
                $usercriteria->select = "is_developer";
                $usercriteria->condition = "id=$user_id";
                $userData = SdkUser::model()->find($usercriteria);
            }
            $data = array();

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $movie_id = Yii::app()->common->getMovieId($movie_code);

            $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
            $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            if (@$userData->is_developer) {
                $mov_can_see = 'allowed';
            } else {
                $can_see_data['movie_id'] = $movie_id;
                $can_see_data['season'] = @$season;
                $can_see_data['stream_id'] = $stream_id;
                $can_see_data['purchase_type'] = @$_REQUEST['purchase_type'];
                $can_see_data['studio_id'] = $studio_id;
                $can_see_data['user_id'] = $user_id;
                $can_see_data['country'] = @$_REQUEST['country'];
                $can_see_data['is_app'] = 1;
                $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            }

            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['activate_subscription_watch_video'];
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['reactivate_subscription_watch_video'];
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['video_restiction_in_your_country'];
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'access_period') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['access_period_expired'];
            } else if ($mov_can_see == 'watch_period') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['watch_period_expired'];
            } else if ($mov_can_see == 'maximum') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['crossed_max_limit_of_watching'];
            } else if ($mov_can_see == 'already_purchased') {
                $data['code'] = 428;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = $translate['already_purchase_this_content'];
            } else if ($mov_can_see == 'allowed_watch_duration') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
                $data['is_daily_watch_duration'] = 1;
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else if ($mov_can_see == 'unpaid') {
                $data['code'] = 430;
                $data['string_code'] = $mov_can_see;
                $data['status'] = "False";
                $data['msg'] = 'Unpaid';

                $data['member_subscribed'] = 0;
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $data['member_subscribed'] = 1;
                }
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = "Muvi id required";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	
	public function actionPpvpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['card_holder_name'] = @$_REQUEST['card_name'];
        $data['card_number'] = @$_REQUEST['card_number'];
        $data['exp_month'] = @$_REQUEST['exp_month'];
        $data['exp_year'] = @$_REQUEST['exp_year'];
        $data['cvv'] = @$_REQUEST['cvv'];
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];

        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];

        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;

        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id'],'parent_id'=>0));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }

        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }

            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }

            $gateway_code = '';

            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;

                $gateway_info = StudioPaymentGateways::model()->findAllByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                $gateway_code = $gateway_info[0]->short_code;
            } else {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $gateway_info = $plan_payment_gateway['gateways'];
                }
            }

            $this->setPaymentGatwayVariable($gateway_info);

            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);

                $VideoDetails = $VideoName;
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }

                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') {
                    
                } else if (abs($data['amount']) < 0.01) {

                    if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }

                    $data['amount'] = 0;
                    $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon_code'], $is_advance, $gateway_code);

                    $ppv_subscription_id = $set_ppv_subscription;
                    $trans_data = array();
                    $trans_data['transaction_status'] = 'succeeded';
                    $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['dollar_amount'] = $data['amount'];
                    $trans_data['amount'] = $data['amount'];
                    $trans_data['response_text'] = '';
                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $is_advance);

                    $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $is_advance, $VideoName, $VideoDetails);

                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

                    if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])) {
                        
                    }

                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                        //echo json_encode($data);exit;
                    }
                    $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                    if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                        $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
                    }
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $trans_data = $payment_gateway::processTransactions($data);

                    $is_paid = $trans_data['is_success'];
                    if (intval($is_paid)) {
                        $data['amount'] = $trans_data['amount'];
                        if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }

                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon_code'], $is_advance, $gateway_code);
                        $ppv_subscription_id = $set_ppv_subscription;

                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $is_advance, $VideoName, $VideoDetails);

                        $res['code'] = 200;
                        $res['status'] = "OK";
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = $translate['error_transc_process'];
                        $res['response_text'] = $trans_data['response_text'];
                    }
                }
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Studio has no payment gateway";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionPpvpayment_test() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['card_holder_name'] = @$_REQUEST['card_name'];
        $data['card_number'] = @$_REQUEST['card_number'];
        $data['exp_month'] = @$_REQUEST['exp_month'];
        $data['exp_year'] = @$_REQUEST['exp_year'];
        $data['cvv'] = @$_REQUEST['cvv'];
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];

        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];

        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;

        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id'],'parent_id'=>0));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);
        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }

        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }

            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $contentArg = array();
                if (isset($_REQUEST['is_specific_coupon']) && intval($_REQUEST['is_specific_coupon'])) {
                    $contentArg['studio_id'] = $studio_id;
                    $contentArg['movie_id'] = $video_id;
                    $contentArg['season_id'] = @$data['season_id'];
                    $contentArg['episode_id'] = @$data['episode_id'];
                }
                $getCoup = Yii::app()->common->getCouponDiscount_test($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id'],'',$contentArg);
                
                if(isset($getCoup["couponCode"]) && trim($getCoup["couponCode"])!=""){
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
            }
            $gateway_code = '';

            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;

                $gateway_info = StudioPaymentGateways::model()->findAllByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                $gateway_code = $gateway_info[0]->short_code;
            } else {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $gateway_info = $plan_payment_gateway['gateways'];
                }
            }
            $this->setPaymentGatwayVariable($gateway_info);

            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);

                $VideoDetails = $VideoName;
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }

                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') {
                    
                } else if (abs($data['amount']) < 0.01) {

                    if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }

                    $data['amount'] = 0;
                    $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $couponCode, $is_advance, $gateway_code);

                    $ppv_subscription_id = $set_ppv_subscription;
                    $trans_data = array();
                    $trans_data['transaction_status'] = 'succeeded';
                    $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                    $trans_data['dollar_amount'] = $data['amount'];
                    $trans_data['amount'] = $data['amount'];
                    $trans_data['response_text'] = '';
                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $is_advance);

                    $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $is_advance, $VideoName, $VideoDetails);

                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

                    if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])) {
                        
                    }

                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                        //echo json_encode($data);exit;
                    }
                    $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                    if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                        $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
                    }
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $trans_data = $payment_gateway::processTransactions($data);
                    $is_paid = $trans_data['is_success'];
                    if (intval($is_paid)) {
                        $data['amount'] = $trans_data['amount'];
                        if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }

                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $couponCode, $is_advance, $gateway_code);
                        $ppv_subscription_id = $set_ppv_subscription;

                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $is_advance, $VideoName, $VideoDetails);

                        $res['code'] = 200;
                        $res['status'] = "OK";
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = $translate['error_transc_process'];
                        $res['response_text'] = $trans_data['response_text'];
                    }
                }
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Studio has no payment gateway";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public getPpvDetails() It will check and return PPV details for a content
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $content_id Content id 
     * @param string $stream_id Stream Id 
     * @return json Json string
     */
    public function actionGetPpvDetails() {
        $studio_id = $this->studio_id;
        $content_id = $_REQUEST['content_id'];
        $stream_id = $_REQUEST['stream_id'];
        if ($content_id && $stream_id) {
            $command = Yii::app()->db->createCommand()
                    ->select('f.ppv_plan_id,f.content_type_id,f.content_types_id')
                    ->from('films f ,movie_streams m')
                    ->where('f.id=m.movie_id AND f.id=:content_id AND f.studio_id=:studio_id AND m.id = :stream_id', array(':content_id' => $content_id, ':studio_id' => $studio_id, ":stream_id" => $stream_id));
            $movie = $command->queryAll();
            if ($movie) {
                if ($movie[0]['ppv_plan_id']) {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $movie[0]['ppv_plan_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
                } else {
                    $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_content_types_id' => $movie[0]['content_type_id'], 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 1, 'is_advance_purchase' => 0));
                }
                if (@$ppvPlans) {
                    $data['code'] = 200;
                    $data['status'] = 'OK';
                    $data['msg'] = "PPV Details";
                    if ($movie[0]['content_types_id'] != 3) {
                        $data['ppvPlans']['price_for_unsubscribed'] = $ppvPlans->price_for_unsubscribed;
                        $data['ppvPlans']['price_for_subscribed'] = $ppvPlans->price_for_subscribed;
                    } else {
                        $data['ppvPlans']['show_unsubscribed'] = $ppvPlans->show_unsubscribed;
                        $data['ppvPlans']['show_subscribed'] = $ppvPlans->show_subscribed;
                        $data['ppvPlans']['season_unsubscribed'] = $ppvPlans->season_unsubscribed;
                        $data['ppvPlans']['season_subscribed'] = $ppvPlans->season_subscribed;
                        $data['ppvPlans']['episode_subscribed'] = $ppvPlans->episode_subscribed;
                    }
                } else {
                    $data['code'] = 439;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'PPV not enabled for this content';
                }
            } else {
                $data['code'] = 438;
                $data['status'] = 'Failure';
                $data['msg'] = 'No content availbe for the information';
            }
        } else {
            $data['code'] = 437;
            $data['status'] = 'Failure';
            $data['msg'] = 'Content id and Stream id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionGetCardsListForPPV() {
        $studio_id = $this->studio_id;
        $user_id = @$_REQUEST['user_id'];
        $data = array();

        if ($user_id) {
            $can_save_card = 0;
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                if (isset($gateways[0]) && !empty($gateways[0])) {
                    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
                }
            }

            $data['code'] = 200;
            $data['status'] = "OK";
            $data['can_save_card'] = $can_save_card;

            $card_sql = "SELECT tot.card_id, tot.card_name, tot.card_holder_name, tot.card_last_fourdigit, tot.card_type FROM (SELECT card_uniq_id AS card_id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
            $cards = Yii::app()->db->createCommand($card_sql)->queryAll();

            if (!empty($cards)) {
                $data['cards'] = $cards;
            } else {
                $data['msg'] = "No card found!";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "User id required";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public CreatePpvPayPalPaymentToken() create a payment token for paypal express checkout
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
     */
    public function actionCreatePpvPayPalPaymentToken() {
        $request_data = $_REQUEST;
        $plan_id = $request_data['plan_id'];
        $user_id = $request_data['user_id'];
        $studio_id = $request_data['studio_id'];
        $authToken = $request_data['authToken'];
        $studio = Studio::model()->findByPk($studio_id);

        if (isset($plan_id) && $plan_id > 0) {
            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
            }
            $VideoDetails = '';
            $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $request_data['movie_id']));
            $video_id = $Films->id;
            //Check studio has plan or not
            $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

            $is_bundle = @$request_data['is_bundle'];
            $timeframe_id = @$request_data['timeframe_id'];
            $isadv = @$request_data['is_advance_purchase'];
            $timeframe_days = 0;
            $end_date = '';

            //Get plan detail which user selected at the time of registration
            $plan = PpvPlans::model()->findByPk($plan_id);
            $request_data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;

            if (intval($is_bundle) && intval($timeframe_id)) {
                (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $request_data['timeframe_id'], $studio_id);
                $price = $price->attributes;

                $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
            } else {
                $plan_details = PpvPricing::model()->findByAttributes(array('ppv_plan_id' => $plan_id, 'status' => 1));
                $price = Yii::app()->common->getPPVPrices($plan->id, $plan_details->currency_id);
            }
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_id'] = $currency->id;
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;


            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
                $time = strtotime($start_date);
                $expiry = $timeframe_days . ' ' . "days";
                $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
            } else {
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                    }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }
            }

            if ($is_bundle) {//For PPV bundle
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    }
                } else {
                    if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                        $data['amount'] = $amount = $price['show_unsubscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            } else {
                //Set different prices according to schemes
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                    //Check which part wants to sell by studio admin
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                        if (intval($is_episode)) {
                            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                            $data['episode_id'] = $streams->id;
                            $data['episode_uniq_id'] = $streams->embed_id;

                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['episode_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['episode_unsubscribed'];
                            }
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                        if (intval($is_season)) {
                            $data['episode_id'] = 0;

                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['season_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['season_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for season
                        }
                    } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                        if (intval($is_show)) {
                            $data['season_id'] = 0;
                            $data['episode_id'] = 0;
                            if (intval($is_subscribed_user)) {
                                $data['amount'] = $amount = $price['show_subscribed'];
                            } else {
                                $data['amount'] = $amount = $price['show_unsubscribed'];
                            }

                            //$end_date = '';//Make unlimited time limit for show
                        }
                    }
                } else {//Single part videos
                    $data['season_id'] = 0;
                    $data['episode_id'] = 0;

                    if (intval($is_subscribed_user)) {
                        $data['amount'] = $amount = $price['price_for_subscribed'];
                    } else {
                        $data['amount'] = $amount = $price['price_for_unsubscribed'];
                    }
                }
            }
            $couponCode = '';
            //Calculate coupon if exists
            if ((isset($request_data['coupon']) && $request_data['coupon'] != '') || (isset($request_data['coupon_instafeez']) && $request_data['coupon_instafeez'] != '')) {
                $coupon = (isset($request_data['coupon']) && $request_data['coupon'] != '') ? $request_data['coupon'] : $request_data['coupon_instafeez'];
                $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $amount = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
            if (intval($is_bundle)) {
                $VideoName = $plan->title;
            } else {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
            }

            $VideoDetails = $VideoName;

            if (!$is_bundle && isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }

            $data['studio_id'] = $studio_id;

            $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

            $log_data = array(
                'studio_id' => $studio_id,
                'user_id' => $user_id,
                'plan_id' => $plan_id,
                'uniq_id' => $request_data['movie_id'],
                'amount' => $data['amount'],
                'episode_id' => $request_data['episode_id'],
                'episode_uniq_id' => $request_data['episode_uniq_id'],
                'season_id' => $request_data['season_id'],
                'permalink' => $request_data['permalink'],
                'currency_id' => $data['currency_id'],
                'couponCode' => $request_data['coupon'],
                'is_bundle' => @$request_data['is_bundle'],
                'timeframe_id' => @$request_data['timeframe_id'],
                'isadv' => $isadv
            );
            $timeparts = explode(" ", microtime());
            $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
            $reference_number = explode('.', $currenttime);
            $unique_id = $reference_number[0];
            $log = new PciLog();
            $log->unique_id = $unique_id;
            $log->log_data = json_encode($log_data);
            $log->save();
            if (strpos($studio->domain, 'idogic.com') !== false || strpos($studio->domain, 'muvi.com') !== false) {
                $pre = 'http://www.';
            } else {
                $pre = 'https://www.';
            }

            $muvi_token = $unique_id . "-" . $authToken;

            $data['returnURL'] = $pre . $studio->domain . "/rest/ppvSuccess?muvi_token=" . $muvi_token;
            $data['cancelURL'] = $pre . $studio->domain . "/rest/ppvPayPalCancel?muvi_token=" . $muvi_token;
            $data['user_id'] = $user_id;
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArr = $payment_gateway::createToken($data);
            if ($resArr['ACK'] == 'Success') {
                $res['code'] = 200;
                $res['status'] = "OK";
                $res['url'] = $resArr['REDIRECTURL'];
                $res['token'] = $resArr['TOKEN'];
            } else {
                $res['code'] = 458;
                $res['status'] = "Error";
                $res['msg'] = "Transaction error!";
            }
        } else {
            $res['code'] = 457;
            $res['status'] = "Error";
            $res['msg'] = "No plans found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public PpvSuccess() handles paypal retun token and do a expresscheckout
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
     */
    public function actionPpvSuccess() {
        $muvi_token = explode('-', $_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        if (isset($unique_id) && trim($unique_id)) {
            $log_data = PciLog::model()->findByAttributes(array('unique_id' => $unique_id));
            $data = json_decode($log_data['log_data'], true);
            $studio_id = $data['studio_id'];
            $user_id = $data['user_id'];

            $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
            }

            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $trans_data = $payment_gateway::processCheckoutDetails($_REQUEST['token']);
            $userdata['TOKEN'] = $trans_data['TOKEN'];
            $userdata['PAYERID'] = $trans_data['PAYERID'];
            $userdata['AMT'] = $trans_data['AMT'];
            $userdata['CURRENCYCODE'] = $trans_data['CURRENCYCODE'];
            $resArray = $payment_gateway::processDoPayment($userdata);

            if (isset($resArray) && $resArray['ACK'] == 'Success') {
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
                $isadv = $data['isadv'];
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);

                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                    }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }

                $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['uniq_id']));
                $video_id = $Films->id;
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
                $VideoDetails = $VideoName;
                $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($streams->id);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }
                $data['amount'] = $trans_data['AMT'];
                $ppv_subscription_id = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['couponCode'], $isadv, $gateway_code);
                $trans_data['transaction_status'] = $resArray['PAYMENTSTATUS'];
                $trans_data['invoice_id'] = $resArray['CORRELATIONID'];
                $trans_data['order_number'] = $resArray['TRANSACTIONID'];
                $trans_data['amount'] = $trans_data['AMT'];
                $trans_data['dollar_amount'] = $trans_data['AMT'];
                $trans_data['response_text'] = json_encode($trans_data);
                $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                $trans_data['amount'] = $trans_data['AMT'];
                $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
                PciLog::model()->deleteByPk($log_data['id']);
                $res['code'] = 200;
                $res['status'] = "OK";
            } else {
                $res['code'] = 458;
                $res['status'] = "Error";
                $res['msg'] = "Transaction error!";
            }
        } else {
            $res['code'] = 400;
            $res['status'] = "Error";
            $res['msg'] = "Bad Request";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public PpvPayPalCancel() handles transaction cancled by user
     * @author SKM<sanjeev@muvi.com>
     * @return json Json data with parameters
     */
    public function actionPpvPayPalCancel() {
        $muvi_token = explode('-', $_REQUEST['muvi_token']);
        $unique_id = $muvi_token[0];
        $log_data = AppPayPalLog::model()->findByAttributes(array('unique_id' => $unique_id));
        AppPayPalLog::model()->deleteByPk($log_data['id']);
        $data['code'] = 460;
        $data['status'] = "Error";
        $data['msg'] = "Transaction canceled!";
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    //API for purchase history Listing by suniln
    public function actionPurchaseHistory() {
        $user_id = @$_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        if ($user_id > 0) {
            $subscribed_user_data = Yii::app()->common->isSubscribed($user_id, $studio_id, 1);
            $userdate['expire_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date . '-1 Days'));
            $userdate['nextbilling_date'] = gmdate("F d, Y", strtotime($subscribed_user_data->start_date));
            //Pagination codes
            $limit = (isset($_REQUEST['limit']) && intval($_REQUEST['limit'])) ? $_REQUEST['limit'] : 10;
            $offset = (isset($_REQUEST['offset']) && $_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
            $page = $offset < 1 ? 1 : $offset;
            $offset = ($page - 1) * ($limit);

            $sql = "select SQL_CALC_FOUND_ROWS id,transaction_date,currency_id,amount,user_id,studio_id,plan_id,currency_id,transaction_date,payer_id,invoice_id,subscription_id,ppv_subscription_id,movie_id,order_number,transaction_type,transaction_status from transactions WHERE studio_id=" . $studio_id . " AND user_id=" . $user_id . "  ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;

            $transaction = Yii::app()->db->createCommand($sql)->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();
            $planModel = new PpvPlans();
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
            foreach ($transaction as $key => $details) {
                $transactions[$key]['invoice_id'] = $details['invoice_id'];
                $transactions[$key]['transaction_date'] = date('F d, Y', strtotime($details['transaction_date']));
                $transactions[$key]['amount'] = $details['amount'];
                $transactions[$key]['transaction_status'] = $translate['success'];
                $currency = Currency::model()->findByPk($details['currency_id']);
                $transactions[$key]['currency_symbol'] = $currency->symbol;
                $transactions[$key]['currency_code'] = $currency->code;
                $transactions[$key]['id'] = $this->getUniqueIdEncode($details['id']);
                if ($details['transaction_type'] == 1) {
                    $trans_type = 'Monthly subscription';
                    $subscription_id = $details['subscription_id'];
                    $startdate = UserSubscription::model()->findByAttributes(array('id' => $subscription_id))->start_date;
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($startdate);
                    if ($currdatetime <= $expirytime && strtotime($startdate) != '') {
                        $statusppv = $translate['active'];
                    } else {
                        $statusppv = 'N/A';
                    }
                    $transactions[$key]['statusppv'] = $statusppv;
                    $transactions[$key]['Content_type'] = 'digital';
                } elseif ($details['transaction_type'] == 2) {

                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $ppv_subscription_details = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id));
                    $end_date =$ppv_subscription_details->end_date;
                    $watch_period =$ppv_subscription_details->watch_period;
                    $access_period = $ppv_subscription_details->access_period;
                    $created_date =$ppv_subscription_details->created_date;
                    $view_restriction = $ppv_subscription_details->view_restriction;
                    $season_id = $ppv_subscription_details->season_id;
                    $episode_id = $ppv_subscription_details->episode_id;
                    $movie_id = $ppv_subscription_details->movie_id;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    $content_types_id = Film::model()->findByAttributes(array('id' => $movie_id))->content_types_id;
                    $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                    $movie_names = $movie_name;
                    if ($season_id != 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " ->".$translate['season']." #" . $season_id . " ->" . $episode_name;
                    }
                    if ($season_id == 0 && $episode_id != 0) {
                        $movie_names = $movie_name . " -> " . $episode_name;
                    }
                    if ($season_id != 0 && $episode_id == 0) {
                        $movie_names = $movie_name . " ->".$translate['season']." #" . $season_id;
                    }
                    $transactions[$key]['movie_name'] = $movie_names;
                     $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($end_date);
                   $statusppv='N/A';
                      if($content_types_id==1){
                    if($currdatetime<=$expirytime && (strtotime($end_date)!='' || $end_date!='0000-00-00 00:00:00') && trim($access_period)){
                       $statusppv=$translate['active'];
                        }
                    else{
                       $statusppv='N/A';  
                    }
                     }

                    if ($content_types_id == 3) {
                        if ($currdatetime <= $expirytime && strtotime($end_date) != '') {
                            $statusppv = $translate['active'];
                        }
                        if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                            $statusppv = $translate['active'];
                        }
                        if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                            $statusppv = $translate['active'];
                        }
                    }
                   
                    if(strtotime($end_date)!='' && $end_date!='0000-00-00 00:00:00' && trim($access_period)){
                    $expirydateppv= date('F d, Y', strtotime($end_date));
                        $transactions[$key]['expiry_dateppv'] = $expirydateppv;
                    }else{
                    $expirydateppv='N/A'; 
                        $transactions[$key]['expiry_dateppv'] = $expirydateppv;
                    }
                    if($watch_period){
                        $setsubscription_data['user_id'] =  $user_id;
                        $setsubscription_data['studio_id'] = $studio_id;
                        $setsubscription_data['created_date'] = $created_date;
                        $setsubscription_data['access_period'] = $access_period;
                        $setsubscription_data['watch_period'] = $watch_period;
                        $setsubscription_data['view_restriction'] = $view_restriction;
                        $setsubscription_data['movie_id'] = $movie_id;
                        if ($movie_id != 0 && $season_id != 0 && $episode_id != 0){
                            $stream_id = $episode_id;
                            $setsubscription_data['movie_id'] = $movie_id;
                            $flag_value=1;
                            $watch_duration_=Yii::app()->common->isWatchPeriodOnContentExceeds($setsubscription_data,$stream_id,$flag_value);
                            if($watch_duration_ != 1){
                            $transactions[$key]['watch_period_ends']=$watch_duration_;
                                $statusppv = $translate['active'];
                            }
                            }else if($content_types_id != 3){
                            $muvi_stream = movieStreams::model()->findByAttributes(array('movie_id'=> $movie_id,'studio_id'=>$studio_id))->id;
                            $stream_id = $muvi_stream;
                            $flag_value=1;
                            $watch_duration_=Yii::app()->common->isWatchPeriodOnContentExceeds($setsubscription_data,$stream_id,$flag_value);
                                if($watch_duration_ != 1){
                             $transactions[$key]['watch_period_ends']=$watch_duration_;
                                 $statusppv = $translate['active'];
                            }
                        }
                    }
                    $transactions[$key]['statusppv']=$statusppv;
                    $trans_type = $translate['Pay_Per_View'] . "-";
                    $trans_type .= $movie_names;
                    $transactions[$key]['Content_type'] = 'digital';
                } elseif ($details['transaction_type'] == 3) {
                    $title = $planModel->findByPk($details['plan_id'])->title;
                    $transactions[$key]['movie_name'] = $title;
                    $trans_type = $translate['pre_order'] . "<br>";
                    $trans_type .= $title;
                    $transactions[$key]['Content_type'] = 'digital';
                } elseif ($details['transaction_type'] == 4) {
                    $trans_type = 'physical';
                    $orderquery = "SELECT d.id,d.order_id,d.name,d.price,d.quantity,d.product_id,d.item_status,d.cds_tracking,d.tracking_url,o.order_number,o.cds_order_status FROM pg_order_details d, pg_order o WHERE d.order_id=o.id AND o.transactions_id=" . $details['id'] . " ORDER BY id DESC";
                    $order = Yii::app()->db->createCommand($orderquery)->queryAll();
                    $transactions[$key]['details'] = $order;
                    $transactions[$key]['order_item_id'] = $order[0]['id'];
                    $transactions[$key]['order_id'] = $order[0]['order_id'];
                    $transactions[$key]['order_number'] = $order[0]['order_number'];
                    $transactions[$key]['cds_order_status'] = $order[0]['cds_order_status'];
                    $transactions[$key]['cds_tracking'] = $order[0]['cds_tracking'];
                    $transactions[$key]['tracking_url'] = $order[0]['tracking_url'];
                    $transactions[$key]['Content_type'] = 'physical';
                } elseif ($details['transaction_type'] == 5) {
                    $title = $planModel->findByPk($details['plan_id'])->title;
                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $ppvplanid = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id))->timeframe_id;
                    $ppvtimeframes = Ppvtimeframes::model()->findByAttributes(array('id' => $ppvplanid))->validity_days;
                    $transaction_dates = $details['transaction_date'];
                    $expirydate = date('F d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
                    if ($currdatetime <= $expirytime) {
                        $statusbundles = $translate['active'];
                    } else {
                        $statusbundles = 'N/A';
                    }
                    if ($expirydate == "") {
                        $expirydate = "N/A";
                    }
                    $transactions[$key]['expiry_date'] = $expirydate;
                    $transactions[$key]['movie_name'] = $title;
                    $transactions[$key]['Content_type'] = 'digital';
                    $trans_type = 'Pay-per-view Bundle' . "-";
                    $transactions[$key]['statusppv'] = $statusbundles;
                    $trans_type.= $title;
                }elseif ($details['transaction_type'] == 6) {
                    $ppv_subscription_id = $details['ppv_subscription_id'];
                    $transactions[$key]['order_type'] = $this->Language['voucher_redemption'];
                    $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,created_date,watch_period,access_period,view_restriction,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
                    $end_date = $PpvSubscription['end_date'];
                    $start_date = $PpvSubscription['start_date'];
                    $season_id = $PpvSubscription['season_id'];
                    $episode_id = $PpvSubscription['episode_id'];
                    $movie_id = $PpvSubscription['movie_id'];
                    $created_date = $PpvSubscription['created_date'];
                    $watch_period=$PpvSubscription['watch_period'];
                    $access_period = $PpvSubscription['access_period'];
                    $view_restriction = $ppv_subscription_details['view_restriction'];
                    $content_types_id = Film::model()->findByAttributes(array('id' => $movie_id))->content_types_id;
                    $currdatetime = strtotime(date('y-m-d'));
                    $expirytime = strtotime($end_date);
                    $permalink = Film::model()->findByAttributes(array('id' => $movie_id))->permalink;
                    $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                    $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                    
                    if ($end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "" && trim($access_period)) {
                        $expirydatevoucher = date('jS M y H:i \H\R\S', strtotime($end_date));
                        $transactions[$key]['expiry_date'] =  $expirydatevoucher;
                } else {
                        $expirydatevoucher = "N/A";
                        $transactions[$key]['expiry_date'] = '';
                    }
                    if ($currdatetime <= $expirytime && $end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "" && trim($access_period)) {
                        $statusvoucher = $this->Language['active'];
                    } else {
                        $statusvoucher = 'N/A';
                    }

                    $transactions[$key]['ppv_subscription_id'] = $ppv_subscription_id;
                    $datamovie_stream = Yii::app()->db->createCommand()
                            ->SELECT('f.id,f.name,f.permalink,ms.id AS stream_id,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id')
                            ->from('films f ,movie_streams ms ')
                            ->where('f.id = ms.movie_id AND ms.id=' . $episode_id)
                            ->queryRow();
                    $embed_id = $datamovie_stream['embed_id'];
                    $vmovie_names = $movie_name;
                    if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                        $vmovie_names = $movie_name;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink;
                    }
                    if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                        $vmovie_names = $movie_name . " ->  " . $this->Language['season'] . " " . $season_id . " ->" . $episode_name;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/player/' . $permalink . '/stream/' . $embed_id;
                    }
                    if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                        $vmovie_names = $movie_name . " ->  " . $this->Language['season'] . " " . $season_id;
                        $transactions[$key]['baseurl'] = Yii::app()->getBaseUrl(true) . '/' . $permalink . '?season=' . $season_id;
                    }
                    $trans_type = $vmovie_names;
                  if($watch_period){    
                        $setsubscription_data['user_id'] =  $user_id;
                        $setsubscription_data['studio_id'] = $studio_id;
                        $setsubscription_data['created_date'] = $created_date;
                        $setsubscription_data['access_period'] = $access_period;
                        $setsubscription_data['watch_period'] = $watch_period;
                        $setsubscription_data['view_restriction'] = $view_restriction;
                        $setsubscription_data['movie_id'] = $movie_id;
                        if($content_types_id != 3){
                                
                                $muvi_stream = movieStreams::model()->findByAttributes(array('movie_id'=> $movie_id,'studio_id'=>$studio_id))->id;
                                $stream_id = $muvi_stream;
                                $flag_value=1;
                                $watch_duration_=Yii::app()->common->isWatchPeriodOnContentExceeds($setsubscription_data,$stream_id,$flag_value);
                                if($watch_duration_ != 1){
                                 $transactions[$key]['watch_period_ends']=($watch_duration_);
                                    $statusvoucher = $this->Language['active'];
                            }
                        } else if($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                                $stream_id = $episode_id;
                                $flag_value=1;
                                $watch_duration_=Yii::app()->common->isWatchPeriodOnContentExceeds($setsubscription_data,$stream_id,$flag_value);
                                if($watch_duration_ != 1){
                                 $transactions[$key]['watch_period_ends']=($watch_duration_);
                                 $statusvoucher = $this->Language['active'];
                            }
                        }
                  }
                }else{
                    $trans_type = '';
                }

                $transactions[$key]['transaction_for'] = $trans_type;
            }
            if ($transactions == "") {
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = array();
            } else {
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = $transactions;
            }
        } else {
            $data['code'] = 449;
            $data['status'] = "Error";
            $data['msg'] = "User Authentication Error.";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    //API for Order Details/transactions Details Page By suniln 
    public function actionTransaction() {
        $user_id = @$_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        if ($user_id > 0 && $studio_id > 0) {
            $id = $this->getUniqueIdDecode(@$_REQUEST['id']);
            $transaction = Yii::app()->general->getTransactionDetails($id, $user_id, 1);
            $transaction_details['payment_method'] = $transaction['payment_method'];
            $transaction_details['transaction_status'] = $transaction['transaction_status'];
            $transaction_details['transaction_date'] = $transaction['transaction_date'];
            $transaction_details['amount'] = $transaction['amount'];
            $transaction_details['currency_symbol'] = $transaction['currency_symbol'];
            $transaction_details['currency_code'] = $transaction['currency_code'];
            $transaction_details['invoice_id'] = $transaction['invoice_id'];
            $transaction_details['order_number'] = $transaction['order_number'];
            $transaction_details['plan_name'] = $transaction['plan_name'];
            $transaction_details['plan_recurrence'] = $transaction['plan_recurrence'];
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['section'] = $transaction_details;
        } else {
            $data['code'] = 450;
            $data['status'] = "Error";
            $data['msg'] = "User Authentication Error.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    //api for invoice details
    function actionGetInvoicePDF() {
        $user_id = @$_REQUEST['user_id'];
        $device_type = @$_REQUEST['device_type'];
        $transaction_id = $this->getUniqueIdDecode(@$_REQUEST['id']);
        if ($user_id) {
            if (isset($transaction_id) && $transaction_id) {
                $studio_id = $this->studio_id;
                $name = Yii::app()->pdf->downloadUserinvoice($studio_id, $transaction_id, $user_id, '', '', $device_type);
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['section'] = $name;
            } else {
                $data['code'] = 451;
                $data['status'] = "Error";
                $data['msg'] = "Transaction ID error";
            }
        } else {
            $data['code'] = 452;
            $data['status'] = "Error";
            $data['msg'] = "User Authentication Error.";
        }
        echo json_encode($data);
        exit;
    }

    function actionDeleteInvoicePath() {
        $file = @$_REQUEST['filepath'];
        if (unlink(ROOT_DIR . 'docs/' . $file)) {
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Successfully Delete";
        } else {
            $data['code'] = 453;
            $data['status'] = "Error";
            $data['msg'] = "Successfully not Delete";
        }
        echo json_encode($data);
        exit;
    }

    public function actionGetMonetizationDetails() {
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        $data = array();

        $userid = trim($_REQUEST['data']['user_id']);
        $movieid = trim($_REQUEST['data']['movie_id']);
        $streamid = trim($_REQUEST['data']['stream_id']);
        $seasonid = trim($_REQUEST['data']['season']);
        $purchase_type = trim($_REQUEST['data']['purchase_type']);


        if (isset($userid) && $userid > 0 && isset($movieid) && !empty($movieid)) {
            $data['movie_id'] = $movie_code = isset($movieid) ? $movieid : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];

            $content_types_id = $data['content_types_id'] = $films['content_types_id'];
            $data['stream_id'] = $stream_code = isset($streamid) ? $streamid : '0';
            $season = !empty($seasonid) ? $seasonid : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            $season_id = $season;
            if ($purchase_type == 'episode' && $season == 0) {
                $season_id = movieStreams::model()->findByPk($stream_id)->series_number;
            }

            $film = Film::model()->findByPk($movie_id);

            if (intval($content_types_id) == 3) {
                $content = "'" . $movie_id . ":" . $season_id . ":" . $stream_id . "'";
            } else {
                $content = $movie_id;
            }

            $monitization_data = array();
            $monitization_data['studio_id'] = $studio_id;
            $monitization_data['user_id'] = $user_id;
            $monitization_data['movie_id'] = $movie_id;
            $monitization_data['season'] = @$season_id;
            $monitization_data['stream_id'] = @$stream_id;
            $monitization_data['content'] = @$content;
            $monitization_data['film'] = $film;
            $monitization_data['purchase_type'] = $purchase_type;

            $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);

            $df_plans = array('ppv' => 0, 'pre_order' => 0, 'voucher' => 0, 'ppv_bundle' => 0, 'subscription_bundles' => 0);

            $monetization_data = $monetization['monetization'];
            foreach ($monetization_data as $key => $value) {
                if (array_key_exists($key, $df_plans)) {
                    $df_plans[$key] = 1;
                }
            }

            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
            $data['monetizations'] = $monetization_data;
            $data['monetization_plans'] = $df_plans;
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        }
    }

    public function actionGetPpvPlan() {
        $_REQUEST['data'] = $_REQUEST;

        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);

        $data = array();


        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;

            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];

            $content_types_id = $films['content_types_id'];

            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = isset($season_id) ? $season_id : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            if (intval($content_types_id) == 3) {
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                $EpDetails = $this->getEpisodeToPlay($movie_id, $studio_id);

                if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                    $series = explode(',', $EpDetails->series_number);
                    sort($series);

                    $max_season = $data['max_season'] = $series[count($series) - 1];
                    $min_season = $data['min_season'] = $series[0];

                    $series_number = intval($season) ? $season : $series[0];

                    $episql = "SELECT * FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $series_number . " AND episode_parent_id=0 ORDER BY episode_number ASC";
                    $episodes = Yii::app()->db->createCommand($episql)->queryAll();
                    $ep_data = array();
                    $episode_val = array();
                    if (isset($episodes) && !empty($episodes)) {
                        for ($i = 0; $i < count($episodes); $i++) {
                            $episode_val['id'] = $episodes[$i]['id'];
                            $episode_val['movie_id'] = $episodes[$i]['movie_id'];
                            $episode_val['episode_title'] = $episodes[$i]['episode_title'];
                            $episode_val['embed_id'] = $episodes[$i]['embed_id'];
                            $ep_data[$i] = $episode_val;
                        }
                    }
                }
            }


            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            $connection = Yii::app()->db;

            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];

                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $member_subscribed = 1;
                }

                $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                $cards = $connection->createCommand($card_sql)->queryAll();

                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                if (empty($plan)) {
                    //Check ppv plan set or not by studio admin
                    $plan = Yii::app()->common->getContentPaymentType($content_types_id, $films['ppv_plan_id'], $studio_id);
                }

                $ppv_id = $plan->id;
                $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;
                if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status'])) {
                    $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                    $season_price_data['ppv_pricing_id'] = $price['id'];
                    $season_price_data['studio_id'] = $studio_id;
                    $season_price_data['currency_id'] = $price['currency_id'];
                    $season_price_data['season'] = (intval($season)) ? $season : 1;
                    $season_price_data['is_subscribed'] = $is_subscribed;
                    $default_season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                    if ($default_season_price != -1) {
                        $price['default_season_price'] = $default_season_price;
                    }
                }

                $currency = Currency::model()->findByPk($currency_id);
                $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $data['is_coupon_exists'] = self::isCouponExists();

                $data['movie_code'] = $movie_code;
                $data['content_types_id'] = $content_types_id;
                $data['is_show'] = $is_show;
                $data['is_season'] = $is_season;
                $data['is_episode'] = $is_episode;
                $data['max_season'] = $max_season;
                $data['min_season'] = $min_season;
                $data['series_number'] = $series_number;
                $data['series_number'] = $member_subscribed;
                $data['episode'] = $ep_data;
                $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                $data['card'] = $cards;
                $data['price'] = $price;
                $data['currency_id'] = $currency->id;
                $data['currency_symbol'] = $currency->symbol;
                $data['ppv_buy'] = $currency->symbol;
                $data['ppv_validity'] = $validity;
                $data['user_id'] = $user_id;
                $data['film'] = $films;
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success';
            } else {
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionGetPreOrderPlan() {
        $_REQUEST['data'] = $_REQUEST;

        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);

        $data = array();

        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;

            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_type_id = $films['content_types_id'];

            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            $connection = Yii::app()->db;

            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];

                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $is_subscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                if ($is_subscribed) {
                    $member_subscribed = 1;
                }

                $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                $cards = $connection->createCommand($card_sql)->queryAll();

                //If it has been on advance purchase
                $plan = Yii::app()->common->checkAdvancePurchase($movie_id, $studio_id, 0);
                if (isset($plan) && !empty($plan)) {
                    $ppv_id = $plan->id;
                    $data['isadv'] = 1;

                    $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
                    $price = Yii::app()->common->getPPVPrices($ppv_id, $default_currency_id);
                    $currency_id = (isset($price['currency_id']) && trim($price['currency_id'])) ? $price['currency_id'] : $default_currency_id;

                    $currency = Currency::model()->findByPk($currency_id);

                    $validity = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                    $data['is_coupon_exists'] = self::isCouponExists();
                    $data['movie_id'] = $movie_code;
                    $data['films'] = $films;
                    $data['currency_id'] = $currency->id;
                    $data['currency_symbol'] = $currency->symbol;
                    $data['content_type_id'] = $content_type_id;
                    $data['card'] = $card;
                    $data['member_subscribed'] = $member_subscribed;
                    $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                    $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                    $data['preorder_validity'] = $validity;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                } else {
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                    $data['msg'] = "Pre-Order plan not exist.";
                }
            } else {
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionGetPpvBundlePlan() {
        $_REQUEST['data'] = $_REQUEST;

        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);

        $data = array();

        //For login or registered user
        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;
            $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
            if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                $gateways = $gateway['gateways'];
                $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                $movie_code = isset($movie_id) ? $movie_id : '0';
                $command = Yii::app()->db->createCommand()
                        ->select('f.id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                        ->from('films f ')
                        ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
                $films = $command->queryRow();
                $movie_id = $films['id'];
                $content_types_id = $data['content_types_id'] = $films['content_types_id'];
                $is_ppv_bundle = 1;
                $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;

                $plan = Yii::app()->common->getAllPPVBundle($movie_id, $content_types_id, $films['ppv_plan_id'], '', $studio_id);
                if (isset($plan) && !empty($plan)) {

                    $ppv_id = $plan[0]->id;
                    if (isset($plan[0]->is_timeframe) && intval($plan[0]->is_timeframe)) {
                        $timeframe_prices = Yii::app()->common->getAllTimeFramePrices($ppv_id, $default_currency_id, $studio_id);
                        $currency_id = (isset($timeframe_prices[0]['currency_id']) && trim($timeframe_prices[0]['currency_id'])) ? $timeframe_prices[0]['currency_id'] : $default_currency_id;
                    }

                    $currency = Currency::model()->findByPk($currency_id);
                    $data['is_coupon_exists'] = self::isCouponExists();

                    $card_sql = "SELECT tot.* FROM (SELECT id, gateway_id, card_name, card_holder_name, card_last_fourdigit, card_type, exp_month, exp_year FROM `sdk_card_infos` `t` WHERE studio_id={$studio_id} AND user_id = {$user_id} AND gateway_id != 1 AND gateway_id != 4 ORDER BY is_cancelled ASC, id DESC) AS tot GROUP BY tot.exp_month, tot.exp_year, tot.card_holder_name, tot.card_type, tot.card_last_fourdigit";
                    $connection = Yii::app()->db;
                    $cards = $connection->createCommand($card_sql)->queryAll();
                    $data['movie_id'] = $movie_code;
                    $data['films'] = $films;
                    $data['is_ppv_bundle'] = $is_ppv_bundle;
                    $data['currency_id'] = $currency->id;
                    $data['currency_symbol'] = $currency->symbol;
                    $data['content_type_id'] = $content_types_id;
                    $data['card'] = $cards;
                    $data['can_save_card'] = $gateways[0]['paymentgt']['can_save_card'];
                    $data['gateway_short_code'] = $gateways[0]['paymentgt']['short_code'];
                    $data['timeframe_prices'] = $timeframe_prices;
                    $data['bundle_exp_date'] = $plan[0]->expiry_date;
                    $data['bundle_description'] = $plan[0]->expiry_date;
                    $data['bundle_time_frame'] = $plan[0]->is_timeframe;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                } else {
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                    $data['msg'] = "PPV Bundle Plan not exist.";
                }
            } else {
                $data['code'] = 400;
                $data['status'] = "Failure";
                $data['msg'] = "Payment Gateway not exist.";
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionGetVoucherPlan() {
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);

        $data = array();

        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;

            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            if (intval($stream_id)) {
                if (intval($content_types_id) == 3) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    $EpDetails = $this->getEpisodeToPlay($movie_id, $studio_id);

                    if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                        $series = explode(',', $EpDetails->series_number);
                        sort($series);

                        $max_season = $series[count($series) - 1];
                        $min_season = $series[0];

                        $episql = "SELECT * FROM movie_streams WHERE studio_id=" . $studio_id . " AND is_episode=1 AND movie_id=" . $movie_id . " AND series_number=" . $min_season . " ORDER BY episode_number ASC";
                        $episodes = Yii::app()->db->createCommand($episql)->queryAll();

                        $episode_data = array();
                        $episode_val = array();
                        if (isset($episodes) && !empty($episodes)) {
                            for ($i = 0; $i < count($episodes); $i++) {
                                $episode_val['id'] = $episodes[$i]['id'];
                                $episode_val['movie_id'] = $episodes[$i]['movie_id'];
                                $episode_val['episode_title'] = $episodes[$i]['episode_title'];
                                $episode_val['embed_id'] = $episodes[$i]['embed_id'];
                                $episode_data[$i] = $episode_val;
                            }
                        }
                    }
                }

                $sql_mstream = Yii::app()->db->createCommand()
                        ->select('ms.id,ms.movie_id,ms.episode_number,ms.series_number,ms.episode_title')
                        ->from('movie_streams ms ')
                        ->where(' ms.id=:ms_id AND ms.movie_id=:movie_id', array(':ms_id' => $stream_id, ':movie_id' => $movie_id));
                $stream_data = $sql_mstream->queryAll();
                $content_name = "";
                $search_str = "";
                $con_type = "";
                if ($stream_data[0]['movie_id'] != "") {
                    $search_str .= $stream_data[0]['movie_id'];
                    $content_name .= $films['name'];
                }if ($stream_data[0]['series_number'] != 0 && intval($content_types_id) == 3) {
                    $search_str .= ":" . $stream_data[0]['series_number'];
                    $content_name .= "-Season-" . $stream_data[0]['series_number'];
                }if ($stream_data[0]['episode_number'] != "" && intval($content_types_id) == 3) {
                    if ($stream_data[0]['series_number'] == 0) {
                        $search_str .= ":0:" . $stream_data[0]['id'];
                    } else {
                        $search_str .= ":" . $stream_data[0]['id'];
                        $content_name .= "-Season-" . $stream_data[0]['episode_title'];
                    }
                }

                $content = "'" . $search_str . "'";
                $voucher_exist = Yii::app()->common->isVoucherExists($studio_id, $content);
                if ($voucher_exist) {
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                    $data['user_id'] = $user_id;
                    $data['content_types_id'] = $content_types_id;
                    $data['is_show'] = $is_show;
                    $data['is_season'] = $is_season;
                    $data['is_episode'] = $is_episode;
                    $data['max_season'] = $max_season;
                    $data['min_season'] = $min_season;
                    $data['episodes'] = $episode_data;
                    $data['film'] = $films;
                } else {
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                    $data['msg'] = "Voucher not exist for this content";
                }
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "User id or Movie id not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionValidateVoucher() {
        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        $voucher_code = @trim($_REQUEST['data']['voucher_code']);
        $purchase_type = @trim($_REQUEST['data']['purchase_type']);
        $lang_code = @$_REQUEST['lang_code'];

        $data = array();


        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $studio_id = $this->studio_id;
            $translate = $this->getTransData($lang_code, $studio_id);
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            $content_id = "";
            if ($content_types_id == 3 && $stream_id != "" && $purchase_type == 'episode') {
                if ($movie_id != "") {
                    $content_id .= $movie_id;
                }if ($season_id != "") {
                    $content_id .= ":" . $season;
                }if ($stream_id != "") {
                    $content_id .= ":" . $stream_id;
                }
            } else if ($content_types_id == 3 && $purchase_type == 'season') {
                if ($movie_id != "") {
                    $content_id .= $movie_id;
                }if ($season_id != "") {
                    $content_id .= ":" . $season_id;
                }
            } else {
                $content_id .= $movie_id;
            }

            if (isset($voucher_code) && $voucher_code && isset($content_id)) {
                $voucherDetails = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $studio_id);
                switch ($voucherDetails) {
                    case 1:
                        $data['movie_id'] = $movie_code;
                        $data['season'] = $season;
                        $data['stream_id'] = $stream_code;
                        $data['content_types_id'] = $content_types_id;
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                        break;
                    case 2:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['invalid_voucher'];
                        $data['code'] = 400;
                        $data['status'] = "Failure";
                        break;
                    case 3:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['voucher_already_used'];
                        $data['code'] = 458;
                        $data['status'] = "Failure";
                        break;
                    case 4:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['free_content'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 5:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['already_purchased_content'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 6:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['access_period_expired'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 7:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['watch_period_expired'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                    case 8:
                        $data['isError'] = $voucherDetails;
                        $data['msg'] = $translate['crossed_max_limit_of_watching'];
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        break;
                }
            } else {
                $data['msg'] = 'Voucher Code not exist!';
                $data['code'] = 400;
                $data['status'] = "Failure";
            }
        } else {
            $data['msg'] = 'User id or Movie id not found.';
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionVoucherSubscription() {

        $_REQUEST['data'] = $_REQUEST;
        $user_id = trim($_REQUEST['data']['user_id']);
        $movie_id = trim($_REQUEST['data']['movie_id']);
        $season_id = @trim($_REQUEST['data']['season']);
        $stream_id = @trim($_REQUEST['data']['stream_id']);
        $voucher_code = @trim($_REQUEST['data']['voucher_code']);
        $purchase_type = @trim($_REQUEST['data']['purchase_type']);
        $is_preorder = @trim($_REQUEST['data']['is_preorder']);
        $lang_code = @$_REQUEST['lang_code'];
        $data = array();


        if (isset($user_id) && $user_id > 0 && isset($movie_id) && !empty($movie_id)) {
            $data = array();
            $studio_id = $this->studio_id;
            $translate = $this->getTransData($lang_code, $studio_id);
            $movie_code = isset($movie_id) ? $movie_id : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $content_types_id = $films['content_types_id'];
            $stream_code = isset($stream_id) ? $stream_id : '0';
            $season = !empty($season_id) ? $season_id : 0;


            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }

            $content_id = "";
            if ($content_types_id == 3 && $stream_id != "" && $purchase_type == 'episode') {
                if ($movie_id != "") {
                    $content_id .= $movie_id;
                }if ($season_id != "") {
                    $content_id .= ":" . $season;
                }if ($stream_id != "") {
                    $content_id .= ":" . $stream_id;
                }
            } else if ($content_types_id == 3 && $purchase_type == 'season') {
                if ($movie_id != "") {
                    $content_id .= $movie_id;
                }if ($season_id != "") {
                    $content_id .= ":" . $season_id;
                }
            } else {
                $content_id .= $movie_id;
            }
            $isadv = (isset($is_preorder)) ? intval($is_preorder) : 0;

            $getVoucher = Yii::app()->common->voucherCodeValid($voucher_code, $user_id, $content_id, $studio_id);
            if ($getVoucher == 2) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['invalid_voucher'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 3) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['voucher_already_used'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 4) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['free_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else if ($getVoucher == 5) {
                $data['isError'] = $getVoucher;
                $data['msg'] = $translate['already_purchased_content'];
                $data['code'] = 400;
                $data['status'] = "Failure";
            } else {
                $sql = "SELECT id FROM voucher WHERE studio_id={$studio_id} AND voucher_code='" . $voucher_code . "'";
                $voucher_id = Yii::app()->db->createCommand($sql)->queryRow();

                $start_date = Date('Y-m-d H:i:s');
                $end_date = '';
                if (!empty($voucher_id)) {
                    if (intval($isadv) == 0) {
                        $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'limit_video');
                        $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                        $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'watch_period');
                        $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                        $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'voucher', 'access_period');
                        $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                        $time = strtotime($start_date);
                        if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                            $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                        }

                        $data['view_restriction'] = $limit_video;
                        $data['watch_period'] = $watch_period;
                        $data['access_period'] = $access_period;
                    }
                    $data['movie_id'] = $movie_code;
                    $data['content_types_id'] = $content_types_id;
                    $data['voucher_id'] = $voucher_id['id'];
                    $data['content_id'] = $content_id;
                    $data['user_id'] = $user_id;
                    $data['studio_id'] = $studio_id;
                    $data['isadv'] = $isadv;
                    $data['start_date'] = @$start_date;
                    $data['end_date'] = @$end_date;

                    $VideoName = CouponOnly::model()->getContentInfo($content_id, $isadv);
                    $res = Yii::app()->billing->setVoucherSubscription($data);

                    if ($isadv) {
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    } else {
                        $data['status'] = 'OK';
                        $data['code'] = 200;
                        $data['msg'] = 'Success';
                    }
                } else {
                    $data['isError'] = $getVoucher;
                    $data['msg'] = $translate['invalid_voucher'];
                    $data['code'] = 400;
                    $data['status'] = "Failure";
                }
            }
        } else {
            $data['isError'] = $getVoucher;
            $data['msg'] = $translate['invalid_voucher'];
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actionGetppvPlans() {
        $_REQUEST['data'] = $_REQUEST;
        $studio_id = $this->studio_id;
        $data = array();

        $userid = trim($_REQUEST['data']['user_id']);
        $movieid = trim($_REQUEST['data']['movie_id']);
        $streamid = trim($_REQUEST['data']['stream_id']);
        $seasonid = trim($_REQUEST['data']['season']);
        $purchase_type = trim($_REQUEST['data']['purchase_type']);

        //For login or registered user
        if (isset($userid) && $userid > 0 && isset($movieid) && !empty($movieid)) {
            $studio_id = $this->studio_id;
            $user_id = $userid;

            $movie_code = isset($movieid) ? $movieid : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id, f.uniq_id, f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where('f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();
            $movie_id = $films['id'];

            $data = array();

            $stream_id = $stream_code = !empty($streamid) ? $streamid : '0';
            $multipart_season = $season = !empty($seasonid) ? $seasonid : 0;
            $purchase_type = @$purchase_type;

            //Get stream Id
            $stream_id = 0;
            if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
            } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, '', $studio_id);
            } else {
                $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
            }
            $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
            $can_see_data = array();
            $can_see_data['movie_id'] = $movie_id;
            $can_see_data['stream_id'] = @$stream_id;
            $can_see_data['season'] = @$season;
            $can_see_data['purchase_type'] = @$purchase_type;
            $can_see_data['studio_id'] = $studio_id;
            $can_see_data['user_id'] = $user_id;
            $can_see_data['default_currency_id'] = $default_currency_id;
            $can_see_data['is_monetizations_menu'] = 1;
            $can_see_data['is_app'] = 1;
            //print_r($can_see_data);exit;

            $mov_can_see = Yii::app()->common->canSeeMovie($can_see_data);
            $error = array();
            if ($mov_can_see == "unsubscribed") {//If a user has never subscribed and trying to play video
                $data['code'] = 425;
                $data['status'] = "False";
                $data['msg'] = 'Please activate your subscription to watch video.';
            } else if ($mov_can_see == "cancelled") {//If a user had subscribed and cancel
                $data['code'] = 426;
                $data['status'] = "False";
                $data['msg'] = 'Please reactivate your subscription to watch video.';
            } else if ($mov_can_see == "limitedcountry") {//If video is not allowed country
                $data['code'] = 427;
                $data['status'] = "False";
                $data['msg'] = 'This video is not allowed to play in your country.';
            } else if ($mov_can_see == 'advancedpurchased') {
                $data['code'] = 431;
                $data['status'] = "False";
                $data['msg'] = 'You have already purchased this content earlier.';
            } else if ($mov_can_see == 'allowed_watch_duration') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
                $data['is_daily_watch_duration'] = 1;
            } else if ($mov_can_see == 'allowed') {
                $data['code'] = 429;
                $data['status'] = "OK";
                $data['msg'] = 'You will be allow to watch this video.';
            } else {
                if (array_key_exists('playble_error_msg', $mov_can_see)) {
                    if ($mov_can_see['playble_error_msg'] == 'access_period') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, your access period for this content has been expired';
                    } else if ($mov_can_see['playble_error_msg'] == 'watch_period') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, your watch period for this content has been expired';
                    } else if ($mov_can_see['playble_error_msg'] == 'maximum') {
                        $data['code'] = 428;
                        $data['status'] = "False";
                        $data['msg'] = 'Sorry, you have crossed the maximum limit of watching this content.';
                    }
                } else {

                    $monetization = $mov_can_see;
                    $content_types_id = $films['content_types_id'];

                    $data['isadv'] = $isadv = isset($_REQUEST['isadv']) ? $_REQUEST['isadv'] : 0;
                    $data['is_ppv_bundle'] = $is_ppv_bundle = isset($_REQUEST['is_ppv_bundle']) ? $_REQUEST['is_ppv_bundle'] : 0;

                    if (intval($content_types_id) == 3) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                        $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    $gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
                    if (isset($gateway['gateways']) && !empty($gateway['gateways'])) {
                        $gateways = $gateway['gateways'];
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
                    }
                    $is_preorder_content = @$_POST['isadv'];
                    $not_refresh_on_close = (isset($_POST['onCloseNotRefresh']) && $_POST['onCloseNotRefresh'] == 1 ) ? true : false;

                    $data['monetization'] = $monetization;
                    $data['films'] = $films;
                    $data['stream_id'] = $stream_id;
                    $data['multipart_season'] = $multipart_season;
                    $data['purchase_type'] = $purchase_type;
                    $data['is_preorder_content'] = $is_preorder_content;
                    $data['not_refresh_on_close'] = $not_refresh_on_close;
                    $data['status'] = 'OK';
                    $data['code'] = 200;
                    $data['msg'] = 'Success';
                }
            }
        } else {
            $data['msg'] = 'User id or Movie id not found.';
            $data['code'] = 400;
            $data['status'] = "Failure";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method SetSubscriptionForOtherDevices Set the user subscription of User when purchase subscription from other device like Roku, Apple etc
     * @return json Returns the response in json format
     * @param string $email Email of the user
     * @param string $plan_id Unique Plan id of the Subscription Plan
     * @param int $device_type Device type For Roku
     * @param string out_source_user_id*: Apple, Roku user id. Mandatory field for Apple device
	 * @param string out_source_plan_id*: Apple product id. Mandatory field for Apple device
	 * @param float amount: Apple product amount
     * @author Ashis<ashis@muvi.com>
     */
    public function actionSetSubscriptionForOtherDevices() {
        $email = @$_REQUEST['email'];
        $plan_id = @$_REQUEST['plan_id'];
        $device_type = @$_REQUEST['device_type'];
        $out_source_user_id = @$_REQUEST['out_source_user_id'];
        $out_source_plan_id = @$_REQUEST['out_source_plan_id'];
        $amount = (isset($_REQUEST['amount']) && trim($_REQUEST['amount'])) ? number_format((float) ($_REQUEST['amount']), 2, '.', '') : number_format((float) (0), 2, '.', '');

        if (!empty($email) && !empty($plan_id) && !empty($device_type)) {
            $user = SdkUser::model()->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id, 'status' => 1));
            if (isset($user) && !empty($user)) {
                $planData = SubscriptionPlans::model()->findByAttributes(array('unique_id' => $plan_id, 'studio_id' => $this->studio_id, 'status' => 1));
                if (isset($planData) && !empty($planData)) {
                    $usModel = new UserSubscription;
                    $subscriptionData = $usModel->findByAttributes(array('studio_id' => $this->studio_id, 'plan_id' => $planData->id, 'user_id' => $user->id, 'device_type' => $device_type, 'status' => 1));
                    if (isset($subscriptionData) && !empty($subscriptionData)) {
                        $res['code'] = 652;
                        $res['message'] = 'User has already taken subscription.';
                        $res['status'] = 'FAILURE';
                    } else {
						$start_date = $end_date = Null;
						$trail_period = $planData->trial_period;
						if (isset($trail_period) && (intval($trail_period) == 0)) {
							$start_date = Date('Y-m-d H:i:s');
                                                        $time = strtotime($start_date);
                                                        $recurrence_frequency = $planData->frequency . ' ' . strtolower($planData->recurrence) . "s";
                                                        $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                                        $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                                                    } else {
							$recurrence_frequency = $planData->frequency . ' ' . strtolower($planData->recurrence) . "s";
							$trail_recurrence = $planData->trial_recurrence;
							$trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
							$start_date = gmdate('Y-m-d H:i:s', strtotime("+{$trial_period}"));
							$end_date = date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
						}
						$usModel->start_date = $start_date;
						$usModel->end_date = $end_date;
                        $usModel->studio_id = $this->studio_id;
                        $usModel->user_id = $user->id;
                        $usModel->plan_id = $planData->id;
                        $usModel->device_type = @$device_type;
                        $usModel->status = 1;
                        $usModel->created_by = $user->id;
                        $usModel->created_date = gmdate('Y-m-d H:i:s');
                        $usModel->outsource_plan_id = $out_source_plan_id;
                        if (abs($amount) >= 0.01) {
                            $usModel->amount = $amount;
                        }
                        $usModel->save();

                        if (trim($out_source_user_id)) {
                            $users = SdkUser::model()->findByPk($user->id);
                            $users->out_source_user_id = $out_source_user_id;
                            $users->save();
                        }
                        $res['code'] = 200;
                        $res['message'] = 'SUCCESS';
                        $res['status'] = 'OK';
                    }
                } else {
                    $res['code'] = 653;
                    $res['message'] = 'No subscription plan found.';
                    $res['status'] = 'FAILURE';
                }
            } else {
                $res['code'] = 654;
                $res['message'] = 'User does not exist.';
                $res['status'] = 'FAILURE';
            }
        } else {
            $res['code'] = 655;
            $res['message'] = 'Either email or plan_id or device_type is missing.';
            $res['status'] = 'FAILURE';
        }
        echo json_encode($res);
        exit;
    }

    public function actionMuvikartpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $data = array();
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['amount'] = @$_REQUEST['amount'];
        $data['dollar_amount'] = @$_REQUEST['amount'];
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        $_REQUEST["cart_item"] = json_decode($_REQUEST["cart_item"], true);
        $_REQUEST["ship"] = json_decode($_REQUEST["ship"], true);
        $data['cart_item'] = $_REQUEST["cart_item"];
        $data['ship'] = $_REQUEST["ship"];
        $gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
        if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
            $this->setPaymentGatwayVariable($gateway_info);

            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
                $data['coupon_code'] = $couponCode;
                $data['discount_type'] = 0; //$getCoup["discount_type"];
                $data['discount'] = $getCoup["coupon_amount"];
            }

            if ($data['amount'] > 0) {
                $currency = Currency::model()->findByPk($data['currency_id']);
                $data['currency_id'] = $currency->id;
                $data['currency_code'] = $currency->code;
                $data['currency_symbol'] = $currency->symbol;
                $timeparts = explode(" ", microtime());
                $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                $reference_number = explode('.', $currenttime);
                $data['order_reference_number'] = $reference_number[0];
                $payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                $payment_gateway = new $payment_gateway_controller();
                $data['gateway_code'] = $gateway_info[0]->short_code;
                $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                    $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
                }
                $trans_data = $payment_gateway::processTransactions($data); //charge the card
                if (intval($trans_data['is_success'])) {
                    if (intval($data['is_save_this_card'] == 1)) {
                        $card_info = json_decode(@$_REQUEST['card'], true);
                        $sciModel = New SdkCardInfos;
                        $card = $data;
                        $card['card_last_fourdigit'] = @$card_info['card']['card_last_fourdigit'];
                        $card['token'] = @$card_info['card']['token'];
                        $card['card_type'] = @$card_info['card']['card_type'];
                        $card['auth_num'] = @$card_info['card']['auth_num'];
                        $card['profile_id'] = @$card_info['card']['profile_id'];
                        $card['reference_no'] = @$card_info['card']['reference_no'];
                        $card['response_text'] = @$card_info['card']['response_text'];
                        $card['status'] = @$card_info['card']['status'];
                        $card['gateway_code'] = $data['gateway_code'];
                        $sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
                    }
                    //Save a transaction detail
                    $transaction = new Transaction;
                    $transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
                    $data['transactions_id'] = $transaction_id;
                    /* Save to order table */
                    $data['hear_source'] = $_REQUEST['hear_source'];
                    $pgorder = new PGOrder();
                    $orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
                    /* Save to shipping address */
                    $pgshippingaddr = new PGShippingAddress();
                    $pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
                    /* Save to Order Details */
                    $pgorderdetails = new PGOrderDetails();
                    $pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


                    //send email
                    $req['orderid'] = $orderid;
                    $req['emailtype'] = 'orderconfirm';
                    $req['studio_id'] = $studio_id;
                    $req['studio_name'] = @$data['studio_name'];
                    $req['currency_id'] = $data['currency_id'];
                    // Send email to user
                    Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                    $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
                    if ($isEmailToStudio) {
                        Yii::app()->email->pgEmailTriggers($req);
                    }
                    if ($transaction_id) {
                        $command = Yii::app()->db->createCommand();
                        $command->delete('pg_cart', 'user_id=:user_id', array(':user_id' => $user_id));
                    }
                    //create order in cds
                    $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                    if ($webservice) {
                        if ($webservice->inventory_type == 'CDS') {
                            $pgorder = new PGOrder();
                            $pgorder->CreateCDSOrder($studio_id, $orderid);
                            //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                        }
                    }
                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $res['code'] = 411;
                    $res['status'] = "Error";
                    $res['msg'] = $translate['error_transc_process'];
                    $res['response_text'] = $trans_data['response_text'];
                }
            } else {
                $trans_data = array();
                $trans_data['transaction_status'] = 'succeeded';
                $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
                $trans_data['dollar_amount'] = $data['dollar_amount'];
                $trans_data['paid_amount'] = $data['amount'];
                $trans_data['bill_amount'] = $data['amount'];
                $trans_data['response_text'] = json_encode($trans_data);
                $isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
                if ($isData) {
                    $res['code'] = 200;
                    $res['status'] = "OK";
                }
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no payment gateway";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method private Muvikartpaymentnew() Payment for muvicart new
     * @author satyajit rout<satyajit@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
	public function actionMuvikartpaymentnew() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['ship_id']) && isset($_REQUEST['cart_item_id'])) {
            $studio_id = $this->studio_id;
            $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);
            if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
                $ip = CHttpRequest::getUserHostAddress();
                $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                $data['ship_id'] = $_REQUEST['ship_id'];
                $cart_item = PGCart::model()->findByPk($data['cart_item_id']);
                if (count($cart_item) > 0) {
                    $currency_id = (isset($_REQUEST['currency_id'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($studio_id)->default_currency_id;
                    $user_id = $_REQUEST['user_id'];
                    $lang_code = @$_REQUEST['lang_code'];
                    $translate = $this->getTransData($lang_code, $studio_id);
                    $data = array();
                    $data['currency_id'] = $currency_id;
                    $data['email'] = @$_REQUEST['email'];
                    $data['currency_id'] = $currency_id;
                    $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
                    $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
                    $data['coupon_code'] = @$_REQUEST['coupon_code'];
                    $data['studio_id'] = $studio_id;
                    $data['user_id'] = $user_id;
                    $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                    $data['ship_id'] = $_REQUEST['ship_id'];
                    $cart_item = PGCart::model()->findByPk($data['cart_item_id']);

                    $_REQUEST["cart_item"] = json_decode($cart_item->cart_item, true);
                    $amount_cal = 0;
                    foreach ($_REQUEST["cart_item"] as $key => $val) {
                        //  $amount_cal += ($val['price'] * $val['quantity']);
                        $amount_cal += $val['price'];
                    }
                    if (isset($_REQUEST['shipp_cost'])) {
                        $amount_cal = $amount_cal + $_REQUEST['shipp_cost'];
                        $data['shipping_cost'] = $_REQUEST['shipp_cost'];
                    }
                    $ship_item = PGSavedAddress::model()->findByPk($data['ship_id']);
                    $ship_arr = array('first_name' => $ship_item->first_name,
                        'address' => $ship_item->address,
                        'address2' => $ship_item->address2,
                        'city' => $ship_item->city,
                        'state' => $ship_item->state,
                        'country' => $ship_item->country,
                        'zip' => $ship_item->zip,
                        'phone_number' => $ship_item->phone_number,
                        'email' => $ship_item->email,
                    );
                    $_REQUEST["ship"] = $ship_arr;
                    $data['amount'] = $amount_cal;
                    $data['dollar_amount'] = $amount_cal;
                    $data['cart_item'] = $_REQUEST["cart_item"];
                    $data['ship'] = $_REQUEST["ship"];

                    $gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
                    if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
                        $this->setPaymentGatwayVariable($gateway_info);

                        if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                            $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                            $data['card_last_fourdigit'] = @$card->card_last_fourdigit;
                            $data['card_id'] = @$card->id;
                            $data['token'] = @$card->token;
                            $data['profile_id'] = @$card->profile_id;
                            $data['card_holder_name'] = @$card->card_holder_name;
                            $data['card_type'] = @$card->card_type;
                            $data['exp_month'] = @$card->exp_month;
                            $data['exp_year'] = @$card->exp_year;
                        } else {
                            $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
                            $data['token'] = @$_REQUEST['token'];
                            $data['profile_id'] = @$_REQUEST['profile_id'];
                            $data['card_type'] = @$_REQUEST['card_type'];
                        }
                        $couponCode = '';
                        //Calculate coupon if exists
                        if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                            $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                            $data['amount'] = $getCoup["amount"];
                            $couponCode = $getCoup["couponCode"];
                            $data['coupon_code'] = $couponCode;
                            $data['discount_type'] = 0; //$getCoup["discount_type"];
                            $data['discount'] = $getCoup["coupon_amount"];
                        }

                        if ($data['amount'] > 0) {
                            $currency = Currency::model()->findByPk($data['currency_id']);
                            $data['currency_id'] = $currency->id;
                            $data['currency_code'] = $currency->code;
                            $data['currency_symbol'] = $currency->symbol;
                            $timeparts = explode(" ", microtime());
                            $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                            $reference_number = explode('.', $currenttime);
                            $data['order_reference_number'] = $reference_number[0];
                            $payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data['gateway_code'] = $gateway_info[0]->short_code;
                            $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                            $trans_data = $payment_gateway::processTransactions($data); //charge the card
                            if (intval($trans_data['is_success'])) {
                                if (intval($data['is_save_this_card'] == 1)) {
                                    $sciModel = New SdkCardInfos;
                                    $card = $data;
                                    $card['card_last_fourdigit'] = $_REQUEST['card_last_fourdigit'];
                                    $card['token'] = $_REQUEST['token'];
                                    $card['card_type'] = $_REQUEST['card_type'];
                                    $card['auth_num'] = $_REQUEST['auth_num'];
                                    $card['profile_id'] = $_REQUEST['profile_id'];
                                    $card['reference_no'] = $_REQUEST['reference_no'];
                                    $card['response_text'] = $_REQUEST['response_text'];
                                    $card['status'] = $_REQUEST['status'];
                                    $sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
                                }
                                //Save a transaction detail
                                $transaction = new Transaction;
                                $transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
                                $data['transactions_id'] = $transaction_id;
                                /* Save to order table */
                                $data['hear_source'] = $_REQUEST['hear_source'];
                                $pgorder = new PGOrder();
                                $orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
                                /* Save to shipping address */
                                $pgshippingaddr = new PGShippingAddress();
                                $pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
                                /* Save to Order Details */
                                $pgorderdetails = new PGOrderDetails();
                                $pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


                                //send email
                                $req['orderid'] = $orderid;
                                $req['emailtype'] = 'orderconfirm';
                                $req['studio_id'] = $studio_id;
                                $req['studio_name'] = @$data['studio_name'];
                                $req['currency_id'] = $data['currency_id'];
                                // Send email to user
                                Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
                                if ($isEmailToStudio) {
                                    Yii::app()->email->pgEmailTriggers($req);
                                }
                                //create order in cds
                                $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                                if ($webservice) {
                                    if ($webservice->inventory_type == 'CDS') {
                                        $pgorder = new PGOrder();
                                        $pgorder->CreateCDSOrder($studio_id, $orderid);
                                        //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                                    }
                                }
                                PGCart::model()->deleteByPk($data['cart_item_id']);
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($transaction_id);
                                $res['orderid'] = $orderid;
                            } else {
                                $res['code'] = 411;
                                $res['status'] = "Error";
                                $res['msg'] = $translate['error_transc_process'];
                                $res['response_text'] = $trans_data['response_text'];
                            }
                        } else {
                            $trans_data = array(
                                'invoice_id' => Yii::app()->common->generateUniqNumber(),
                                'order_number' => Yii::app()->common->generateUniqNumber(),
                                'transaction_status' => 'Success',
                                'paid_amount' => 0.00,
                                'dollar_amount' => 0.00
                            );

                            $isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
                            if ($isData) {
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($isData);
                            }
                        }
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = "Studio has no payment gateway";
                    }
                } else {
                    $res['code'] = 411;
                    $res['status'] = "Error";
                    $res['msg'] = "You don't have any item in your cart";
                }
            } else {
                $res['code'] = 412;
                $res['status'] = "Error";
                $res['msg'] = "Studio has not enable muvi Kart";
            }
        } else {
            $res['code'] = 413;
            $res['status'] = "Error";
            $res['msg'] = "Pleae provide required parameter";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }
	
    public function actionMuvikartpaymentnew_test() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['ship_id']) && isset($_REQUEST['cart_item_id'])) {
            $studio_id = $this->studio_id;
            $is_muvi_kart_enable = Yii::app()->general->getStoreLink($studio_id, 1);
            if (isset($is_muvi_kart_enable) && intval($is_muvi_kart_enable)) {
                $ip = CHttpRequest::getUserHostAddress();
                $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                $data['ship_id'] = $_REQUEST['ship_id'];
                $cart_item = PGCart::model()->findByPk($data['cart_item_id']);
                if (count($cart_item) > 0) {
                    $currency_id = (isset($_REQUEST['currency_id'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($studio_id)->default_currency_id;
                    $user_id = $_REQUEST['user_id'];
                    $lang_code = @$_REQUEST['lang_code'];
                    $translate = $this->getTransData($lang_code, $studio_id);
                    $data = array();
                    $data['currency_id'] = $currency_id;
                    $data['email'] = @$_REQUEST['email'];
                    $data['currency_id'] = $currency_id;
                    $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
                    $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
                    $data['coupon_code'] = @$_REQUEST['coupon_code'];
                    $data['studio_id'] = $studio_id;
                    $data['user_id'] = $user_id;
                    $data['cart_item_id'] = $_REQUEST['cart_item_id'];
                    $data['ship_id'] = $_REQUEST['ship_id'];
                    $cart_item = PGCart::model()->findByPk($data['cart_item_id']);

                    $_REQUEST["cart_item"] = json_decode($cart_item->cart_item, true);
                    $amount_cal = 0;
                    $prodArr = array();
                    $n=0;
                    foreach ($_REQUEST["cart_item"] as $key => $val) {
                        //  $amount_cal += ($val['price'] * $val['quantity']);
                        $amount_cal += $val['price'];
                        $prodArr[$n]['product_id'] = $val['id'];
                        $prodArr[$n]['quantity'] = $val['quantity'];
                        $prodArr[$n]['price'] = $val['price'];
                        $prodArr[$n]['name'] = $val['name'];
                        $prodArr[$n]['is_api'] = 1;
                        $n++;
                    }
                    
                    if (isset($_REQUEST['shipp_cost'])) {
                        $amount_cal = $amount_cal + $_REQUEST['shipp_cost'];
                        $data['shipping_cost'] = $_REQUEST['shipp_cost'];
                    }
                    $ship_item = PGSavedAddress::model()->findByPk($data['ship_id']);
                    $ship_arr = array('first_name' => $ship_item->first_name,
                        'address' => $ship_item->address,
                        'address2' => $ship_item->address2,
                        'city' => $ship_item->city,
                        'state' => $ship_item->state,
                        'country' => $ship_item->country,
                        'zip' => $ship_item->zip,
                        'phone_number' => $ship_item->phone_number,
                        'email' => $ship_item->email,
                    );
                    $_REQUEST["ship"] = $ship_arr;
                    $data['amount'] = $amount_cal;
                    $data['dollar_amount'] = $amount_cal;
                    $data['cart_item'] = $_REQUEST["cart_item"];
                    $data['ship'] = $_REQUEST["ship"];

                    $gateway_info[0] = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'status' => 1, 'is_primary' => 1));
                    if ($gateway_info[0]->short_code != '') {//check Payment Gatway Exists
                        $this->setPaymentGatwayVariable($gateway_info);

                        if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                            $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                            $data['card_last_fourdigit'] = @$card->card_last_fourdigit;
                            $data['card_id'] = @$card->id;
                            $data['token'] = @$card->token;
                            $data['profile_id'] = @$card->profile_id;
                            $data['card_holder_name'] = @$card->card_holder_name;
                            $data['card_type'] = @$card->card_type;
                            $data['exp_month'] = @$card->exp_month;
                            $data['exp_year'] = @$card->exp_year;
                        } else {
                            $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
                            $data['token'] = @$_REQUEST['token'];
                            $data['profile_id'] = @$_REQUEST['profile_id'];
                            $data['card_type'] = @$_REQUEST['card_type'];
                        }
                        $couponCode = '';
                        //Calculate coupon if exists
                        if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                            $getCoup = Yii::app()->common->getCouponDiscount_test($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id'],$prodArr);
                           if(isset($getCoup["couponCode"])  && $getCoup["couponCode"]!=''){
                            $data['amount'] = $getCoup["amount"];
                            $couponCode = $getCoup["couponCode"];
                            $data['coupon_code'] = $couponCode;
                            $data['discount_type'] = 0; //$getCoup["discount_type"];
                            $data['discount'] = $getCoup["coupon_amount"];
                        }
                        }

                        if ($data['amount'] > 0) {
                            $currency = Currency::model()->findByPk($data['currency_id']);
                            $data['currency_id'] = $currency->id;
                            $data['currency_code'] = $currency->code;
                            $data['currency_symbol'] = $currency->symbol;
                            $timeparts = explode(" ", microtime());
                            $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                            $reference_number = explode('.', $currenttime);
                            $data['order_reference_number'] = $reference_number[0];
                            $payment_gateway_controller = 'Api' . $gateway_info[0]->short_code . 'Controller';
                            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                            $payment_gateway = new $payment_gateway_controller();
                            $data['gateway_code'] = $gateway_info[0]->short_code;
                            $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                            $trans_data = $payment_gateway::processTransactions($data); //charge the card
                            if (intval($trans_data['is_success'])) {
                                if (intval($data['is_save_this_card'] == 1)) {
                                    $sciModel = New SdkCardInfos;
                                    $card = $data;
                                    $card['card_last_fourdigit'] = $_REQUEST['card_last_fourdigit'];
                                    $card['token'] = $_REQUEST['token'];
                                    $card['card_type'] = $_REQUEST['card_type'];
                                    $card['auth_num'] = $_REQUEST['auth_num'];
                                    $card['profile_id'] = $_REQUEST['profile_id'];
                                    $card['reference_no'] = $_REQUEST['reference_no'];
                                    $card['response_text'] = $_REQUEST['response_text'];
                                    $card['status'] = $_REQUEST['status'];
                                    $sciModel->insertCardInfos($studio_id, $user_id, $card, $ip);
                                }
                                //Save a transaction detail
                                $transaction = new Transaction;
                                $transaction_id = $transaction->insertTrasactions($studio_id, $user_id, $data['currency_id'], $trans_data, 4, $ip, $gateway_info[0]->short_code);
                                $data['transactions_id'] = $transaction_id;
                                /* Save to order table */
                                $data['hear_source'] = $_REQUEST['hear_source'];
                                $pgorder = new PGOrder();
                                $orderid = $pgorder->insertOrder($studio_id, $user_id, $data, $_REQUEST["cart_item"], $_REQUEST['ship'], $ip);
                                /* Save to shipping address */
                                $pgshippingaddr = new PGShippingAddress();
                                $pgshippingaddr->insertAddress($studio_id, $user_id, $orderid, $_REQUEST['ship'], $ip);
                                /* Save to Order Details */
                                $pgorderdetails = new PGOrderDetails();
                                $pgorderdetails->insertOrderDetails($orderid, $_REQUEST["cart_item"]);


                                //send email
                                $req['orderid'] = $orderid;
                                $req['emailtype'] = 'orderconfirm';
                                $req['studio_id'] = $studio_id;
                                $req['studio_name'] = @$data['studio_name'];
                                $req['currency_id'] = $data['currency_id'];
                                // Send email to user
                                Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $studio_id);
                                if ($isEmailToStudio) {
                                    Yii::app()->email->pgEmailTriggers($req);
                                }
                                //create order in cds
                                $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
                                if ($webservice) {
                                    if ($webservice->inventory_type == 'CDS') {
                                        $pgorder = new PGOrder();
                                        $pgorder->CreateCDSOrder($studio_id, $orderid);
                                        //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                                    }
                                }
                                PGCart::model()->deleteByPk($data['cart_item_id']);
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($transaction_id);
                                $res['orderid'] = $orderid;
                            } else {
                                $res['code'] = 411;
                                $res['status'] = "Error";
                                $res['msg'] = $translate['error_transc_process'];
                                $res['response_text'] = $trans_data['response_text'];
                            }
                        } else {
                            $trans_data = array(
                                'invoice_id' => Yii::app()->common->generateUniqNumber(),
                                'order_number' => Yii::app()->common->generateUniqNumber(),
                                'transaction_status' => 'Success',
                                'paid_amount' => 0.00,
                                'dollar_amount' => 0.00
                            );

                            $isData = self::physicalDataInsert($data, $_REQUEST, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info[0]->short_code]);
                            if ($isData) {
                                $res['code'] = 200;
                                $res['status'] = "OK";
                                $res['id'] = $this->getUniqueIdEncode($isData);
                            }
                        }
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = "Studio has no payment gateway";
                    }
                } else {
                    $res['code'] = 411;
                    $res['status'] = "Error";
                    $res['msg'] = "You don't have any item in your cart";
                }
            } else {
                $res['code'] = 412;
                $res['status'] = "Error";
                $res['msg'] = "Studio has not enable muvi Kart";
            }
        } else {
            $res['code'] = 413;
            $res['status'] = "Error";
            $res['msg'] = "Pleae provide required parameter";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public GetUserCredits() Get the list of credits based on user id
     * @param user_id, studio_id
     * @author Sanjeev Kumar Malla<sanjeev@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionGetUserCredits() {
        $studio_id = isset($_REQUEST['studio_id']) && trim($_REQUEST['studio_id']) ? $_REQUEST['studio_id'] : $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['user_id']) && trim($_REQUEST['user_id'])) {
            $user_id = $_REQUEST['user_id'];
            $total_credit = Yii::app()->billing->getUserTotalCredit($user_id);
            $page_size = 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && intval($_REQUEST['offset'])) {
                $offset = ($_REQUEST['offset'] - 1) * $page_size;
            }
            $cdata = Yii::app()->billing->getAllUserCredits($user_id, $offset, $page_size);
            $user_all_credits = $cdata['data'];
            for ($i = 0; $i < count($user_all_credits); $i++) {
                if (intval($user_all_credits[$i]['rule_action_type']) == 1) {
                    $user_all_credits[$i]['action_type'] = 'Subscription';
                } else if (intval($user_all_credits[$i]['rule_action_type']) == 2) {
                    $user_all_credits[$i]['action_type'] = 'Subscription Renewal';
                }
            }

            $res['code'] = 200;
            $res['status'] = "Success";
            $res['total_credit'] = $total_credit ? $total_credit : 0;
            $res['credit_count'] = $cdata['count'];
            $res['credit_list'] = $user_all_credits;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public GetUserDebits() Get the list of credits based on user id
     * @param user_id, studio_id
     * @author Sanjeev Kumar Malla<sanjeev@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionGetUserDebits() {
        $studio_id = isset($_REQUEST['studio_id']) && trim($_REQUEST['studio_id']) ? $_REQUEST['studio_id'] : $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['user_id']) && trim($_REQUEST['user_id'])) {
            $user_id = $_REQUEST['user_id'];
            $page_size = 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && intval($_REQUEST['offset'])) {
                $offset = ($_REQUEST['offset'] - 1) * $page_size;
            }
            $data = Yii::app()->billing->getAllUserDebits($user_id, $offset, $page_size);
            $user_all_debits = $data['data'];
            $debit_count = $data['count'];

            for ($i = 0; $i < count($user_all_debits); $i++) {
                if (intval($user_all_debits[$i]['content_type']) == 1) {
                    $user_all_debits[$i]['content_name'] = Film::model()->findByPk($user_all_debits[$i]['content'])->name;
                } else if (intval($user_all_debits[$i]['content_type']) == 3) {
                    $temp_id_str = $user_all_debits[$i]['content'];
                    $content_arr = explode(':', $temp_id_str);
                    if (count($content_arr) == 2) {
                        $user_all_debits[$i]['content_name'] = Film::model()->findByPk($content_arr[0])->name . " " . $this->Language['season'] . " " . $content_arr[1];
                    } else if (count($content_arr) == 3) {
                        $user_all_debits[$i]['content_name'] = Film::model()->findByPk($content_arr[0])->name . " " . $this->Language['season'] . " " . $content_arr[1] . " " . movieStreams::model()->findByPk($content_arr[2])->episode_title;
                    }
                }
                if (intval($user_all_debits[$i]['transaction_type']) == 1) {
                    $user_all_debits[$i]['transaction_type_name'] = $translate['Pay_Per_View'];
                } else {
                    $user_all_debits[$i]['transaction_type_name'] = $translate['pre_order'];
                }
            }
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['debit_count'] = $debit_count;
            $res['debit_list'] = $user_all_debits;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function physicalDataInsert($datap, $pay, $trans_data, $ip, $short_code) {
        //Save a transaction detail
        $transaction = new Transaction;
        $transaction_id = $transaction->insertTrasactions($this->studio_id, $pay['user_id'], $datap['currency_id'], $trans_data, 4, $ip, $short_code);
        $datap['transactions_id'] = $transaction_id;
        /* Save to order table */
        $datap['hear_source'] = $pay['hear_source'];
        $pgorder = new PGOrder();
        $orderid = $pgorder->insertOrder($this->studio_id, $pay['user_id'], $datap, $datap["cart_item"], $datap['ship'], $ip);
        /* Save to shipping address */
        $pgshippingaddr = new PGShippingAddress();
        $pgshippingaddr->insertAddress($this->studio_id, $pay['user_id'], $orderid, $datap['ship'], $ip);
        /* Save to Order Details */
        $pgorderdetails = new PGOrderDetails();
        $pgorderdetails->insertOrderDetails($orderid, $datap["cart_item"]);
        if ($pay['card_options'] != '') {
            $data = array('isSuccess' => 1);
            $data = json_encode($data);
        }
        //send email
        $req['orderid'] = $orderid;
        $req['emailtype'] = 'orderconfirm';
        $req['studio_id'] = $this->studio_id;
        $req['studio_name'] = $datap['studio_name'];
        $req['currency_id'] = $datap['currency_id'];
        // Send email to user
        Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio_id);
        if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
        }
        if ($transaction_id) {
            $command = Yii::app()->db->createCommand();
            $command->delete('pg_cart', 'user_id=:user_id', array(':user_id' => $datap['user_id']));
        }
        //create order in cds
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($webservice) {
            if ($webservice->inventory_type == 'CDS') {
                $pgorder->CreateCDSOrder($this->studio_id, $orderid);
                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
            }
        }
        return 1;
    }

    //subscription bundle plan list
    public function actionGetStudioSubscriptionPlanLists() {
        $studio_id = $this->studio_id;
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $country = @$_REQUEST['country'];
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);

        $bundleplans = array();

        if (isset($_REQUEST['movie_id']) && trim($_REQUEST['movie_id'])) {
            $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_type_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f ')
                    ->where(' f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id', array(':uniq_id' => $movie_code, ':studio_id' => $studio_id));
            $films = $command->queryRow();

            if (!empty($films)) {
                $movie_id = @$films['id'];
                $content_types_id = @$films['content_types_id'];
                $bundleplans = Yii::app()->common->getAllSubscriptionsBundle($movie_id, $content_types_id, '', '', $studio_id);

                if (!empty($bundleplans)) {
                    foreach ($bundleplans as $key => $val) {
                        if ($val->is_default) {
                            $default_plan_id = $val->id;
                        }
                        $price_list = Yii::app()->common->getUserSubscriptionPrice($val->id, $default_currency_id, @$country, $studio_id);
                        $bundleplans[$key] = $val->attributes;
                        $bundleplans[$key]['price'] = $price_list['price'];
                        (array) $currency = Currency::model()->findByPk($price_list['currency_id']);
                        $bundleplans[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
                    }
                }
            }
        }

        $isgateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
        if (isset($isgateway['gateways']) && !empty($isgateway['gateways'])) {
            $ret = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id, $default_currency_id, $country, $language_id, 1);
            if (isset($ret['plans']) && !empty($ret['plans'])) {
                $plans = $ret['plans'];
                $default_plan_id = @$plans[0]->id;

                foreach ($plans AS $key => $val) {
                    if ($val->is_default) {
                        $default_plan_id = $val->id;
                    }

                    $plan[$key] = $val->attributes;
                    $plan[$key]['price'] = $val->price;

                    (array) $currency = Currency::model()->findByPk($val->currency_id);
                    $plan[$key]['currency'] = isset($currency->attributes) && !empty($currency->attributes) ? $currency->attributes : array();
                }

                $data['code'] = 200;
                $data['status'] = "OK";
                $data['plans'] = array_merge($plan, $bundleplans);
                $data['default_plan'] = $default_plan_id;
            } else {
                $data['code'] = 457;
                $data['plans'] = '';
                $data['gateways'] = '';
                $data['msg'] = "No subscription plan found!";
            }
        } else {
            $data['code'] = 424;
            $data['plans'] = '';
            $data['gateways'] = '';
            $data['msg'] = "Payment gateway is not enabled";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    //subscription bundles By sunil nayak
    public function actionSubscriptionBundlesPayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code = @$_REQUEST['lang_code'];

        $data = array();
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['plandetailbundles_id'] = @$_REQUEST['plan_id'];
        $data['is_subscriptionbundle'] = @$_REQUEST['is_subscriptionbundle'];

        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            $user = SdkUser::model()->findByAttributes(array('id' => $user_id, 'studio_id' => $studio_id, 'status' => 1));
            if (!empty($user)) {
                $gateway_info = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($gateway_info['gateways']) && !empty($gateway_info['gateways'])) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $this->setPaymentGatwayVariable($gateway_info['gateways']);

                    $monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
                    if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1)) {
                        if ((isset($data['plandetailbundles_id']) && intval($data['plandetailbundles_id']))) {
                            $plandetailbundles_id = $data['plandetailbundles_id'];
                            $plan_subscriptionbundles = SubscriptionPlans::model()->findByAttributes(array('id' => $plandetailbundles_id, 'studio_id' => $studio_id, 'status' => 1));
                            if (!empty($plan_subscriptionbundles)) {

                                $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1, 'plan_id' => $plandetailbundles_id));
                                if (empty($usersub)) {
                                    $is_valid_card_information = 0;
                                    if (isset($_REQUEST['existing_card_id']) && trim($_REQUEST['existing_card_id'])) {
                                        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
                                        $is_valid_card_information = 1;
                                    } else {
                                        $data['card_holder_name'] = @$_REQUEST['card_name'];
                                        $data['card_number'] = @$_REQUEST['card_number'];
                                        $data['exp_month'] = @$_REQUEST['exp_month'];
                                        $data['exp_year'] = @$_REQUEST['exp_year'];
                                        $data['cvv'] = @$_REQUEST['cvv'];
                                        $data['profile_id'] = @$_REQUEST['profile_id'];
                                        $data['token'] = @$_REQUEST['token'];
                                        $data['card_type'] = @$_REQUEST['card_type'];
                                        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
                                        $data['email'] = @$_REQUEST['email'];
                                        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];

                                        if (trim($data['card_holder_name']) && trim($data['card_number']) && trim($data['exp_month']) && trim($data['exp_year']) && trim($data['cvv']) && trim($data['profile_id'])) {
                                            $is_valid_card_information = 1;
                                        }
                                    }

                                    if ($is_valid_card_information) {
                                        $translate = $this->getTransData($lang_code, $studio_id);

                                        $VideoDetails = '';
                                        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
                                        $video_id = $Films->id;
                                        $isadv = '';
                                        $end_date = '';

                                        $pricebundles = Yii::app()->common->getSubscriptionBundlesPrice($plandetailbundles_id, $data['currency_id'], $studio_id);
                                        $currency = Currency::model()->findByPk($pricebundles['currency_id']);
                                        $data['currency_id'] = $currency->id;
                                        $data['currency_code'] = $currency->code;
                                        $data['currency_symbol'] = $currency->symbol;
                                        //Calculate ppv expiry time only for single part or episode only
                                        $start_date = Date('Y-m-d H:i:s');
                                        $is_transaction = 0;
                                        if (isset($plan_subscriptionbundles->trial_period) && intval($plan_subscriptionbundles->trial_period) == 0) {
                                            $is_transaction = 1;
                                            $start_date = Date('Y-m-d H:i:s');
                                            $time = strtotime($start_date);
                                            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                                            $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                            $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                                        } else {
                                            $trial_period = $plan_subscriptionbundles->trial_period . ' ' . strtolower($plan_subscriptionbundles->trial_recurrence) . "s";
                                            $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                                            $time = strtotime($start_date);
                                            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                                            $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                                        }
                                        $data['season_id'] = 0;
                                        $data['episode_id'] = 0;
                                        $data['amount'] = $amount = $pricebundles['price'];
                                        if (isset($_REQUEST['coupon_code']) && trim($_REQUEST['coupon_code'])) {
                                            $datasubscriptionBundles = array();
                                            $datasubscriptionBundles['studio_id'] = Yii::app()->common->getStudiosId();
                                            $datasubscriptionBundles['coupon'] = $_REQUEST['coupon_code'];
                                            $datasubscriptionBundles['plan'] = $data['plandetailbundles_id'];
                                            $datasubscriptionBundles['user_id'] = $user_id;
                                            //getting the subscription bundles plan details
                                            $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($data['plandetailbundles_id'], $default_currency_id, $studio_id);
                                            $currency_id = (isset($planDetails[0]['currency_id']) && trim($planDetails[0]['currency_id'])) ? $planDetails[0]['currency_id'] : $default_currency_id;
                                            $currency = Currency::model()->findByPk($currency_id);
                                            $planDetails['currency_id'] = $datasubscriptionBundles['currency'] = $currency_id;
                                            $planDetails['couponCode'] = $_POST["data"]["coupon"];
                                            $planDetails['physical'] = $_POST["physical"];
                                            $couponDetails = Yii::app()->billing->isValidCouponForSubscription($datasubscriptionBundles);
                                            if ($couponDetails != 0) {
                                                $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails, $planDetails);
                                            } else {
                                                $res['isError'] = 1;
                                            }
                                            $data['amount'] = $amount = $res['discount_amount'];
                                        }

                                        $data['card_holder_name'] = @$data['card_name'];

                                        if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                                            $card = SdkCardInfos::model()->findByAttributes(array('id' => $data['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                                            $data['card_id'] = @$card->id;
                                            $data['token'] = @$card->token;
                                            $data['profile_id'] = @$card->profile_id;
                                            $data['card_holder_name'] = @$card->card_holder_name;
                                            $data['card_type'] = @$card->card_type;
                                            $data['exp_month'] = @$card->exp_month;
                                            $data['exp_year'] = @$card->exp_year;
                                            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                                            $gateway_code = $gateway_info->short_code;
                                        } else {
                                            $data['save_this_card'] = $data['is_save_this_card'];
                                        }

                                        if (trim($gateway_code)) {
                                            if (intval($data['is_subscriptionbundle'])) {
                                                $VideoName = $plan_subscriptionbundles->name;
                                            }
                                            if (abs($data['amount']) < 0.01) {
                                                if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                                    $data['gateway_code'] = $gateway_code;
                                                    $crd = Yii::app()->billing->setCardInfo($data);
                                                    $data['card_id'] = $crd;
                                                }
                                                $data['amount'] = 0;
                                                $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                                                $ppv_subscription_id = $set_user_subscription_bundles;
                                                if ($is_transaction) {
                                                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                                                }
                                                $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                                                if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                                                    echo json_encode($data);
                                                    exit;
                                                }
                                                $res['code'] = 200;
                                                $res['status'] = "OK";
                                            } else {
                                                $data['studio_id'] = $studio_id;
                                                $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];

                                                $data['user_id'] = $user_id;
                                                $data['studio_name'] = Studio::model()->findByPk($studio_id)->name;
                                                $payment_gateway_controller = 'Api' . $gateway_code . 'Controller';
                                                Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                                                $payment_gateway = new $payment_gateway_controller();
                                                $data['gateway_code'] = $gateway_code;
                                                if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                                                    $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
                                                }

                                                $trans_data = $payment_gateway::processTransactions($data);
                                                $is_paid = $trans_data['is_success'];
                                                if (intval($is_paid)) {
                                                    $data['amount'] = $trans_data['amount'];
                                                    if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                                        $crd = Yii::app()->billing->setCardInfo($data);
                                                        $data['card_id'] = $crd;
                                                    }
                                                    $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                                                    $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                                                    $ppv_subscription_id = $set_user_subscription_bundles;
                                                    if ($is_transaction) {
                                                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                                                    }
                                                    $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                                                    $res['code'] = 200;
                                                    $res['status'] = "OK";
                                                } else {
                                                    $res['code'] = 406;
                                                    $res['status'] = "Error";
                                                    $res['msg'] = "Error in transaction";
                                                    $res['response_text'] = $trans_data['response_text'];
                                                }
                                            }
                                        } else {
                                            $res['code'] = 406;
                                            $res['status'] = 'error';
                                            $res['msg'] = "Internal error occures.";
                                        }
                                    } else {
                                        $res['code'] = 406;
                                        $res['status'] = 'error';
                                        $res['msg'] = "Invalid Card details entered. Please check once again";
                                    }
                                } else {
                                    $res['code'] = 406;
                                    $res['status'] = 'error';
                                    $res['msg'] = "You have already purchased this subscription.";
                                }
                            } else {
                                $res['code'] = 406;
                                $res['status'] = 'error';
                                $res['msg'] = "Subscription plan doesn't exist";
                            }
                        } else {
                            $res['code'] = 406;
                            $res['status'] = 'error';
                            $res['msg'] = "Invalid subscription plan detail";
                        }
                    } else {//Subscription plan is disabled
                        $res['code'] = 406;
                        $res['status'] = 'error';
                        $res['msg'] = "Subscription plan is not enabled.";
                    }
                } else {//No gateway found
                    $res['code'] = 406;
                    $res['status'] = 'error';
                    $res['msg'] = "No payment gateway is found.";
                }
            } else {
                $res['code'] = 406;
                $res['status'] = 'error';
                $res['msg'] = "User doesn't belong to this store.";
            }
        } else {
            $res['code'] = 406;
            $res['status'] = 'error';
            $res['msg'] = "Invalid user detail.";
        }
        echo json_encode($res);
        exit;
    }

    /**
     * @method private myplans() plan listing those have user taken
     * @author sunil Nayak<suniln@muvi.com>
     * @return json Returns the list of data in json format
     * @param string user_id (*)
     * @param string authToken Auth token(*)
     * @param int currency_id (optional)
     */
    public function actionMyPlans() {
        if (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) {
            $studio_id = $this->studio_id;
            $user_id = $_REQUEST['user_id'];
            $bundlesplans_cancelled = array();
            $bundlesplans = array();
            $default_currency_id = (isset($_REQUEST['currency_id']) && trim($_REQUEST['currency_id'])) ? $_REQUEST['currency_id'] : Studio::model()->findByPk($this->studio_id)->default_currency_id;
            $bundleplan_payment_gateway = Yii::app()->common->isPaymentGatwayAndBundlesPlanExists($studio_id, $user_id, $default_currency_id);
            if (isset($bundleplan_payment_gateway) && !empty($bundleplan_payment_gateway)) {
                $gateways = $bundleplan_payment_gateway['gateways'];
                $bundlesplans = $bundleplan_payment_gateway['plans'];
            }

            $bundleplan_payment_gateway_cancelled = Yii::app()->common->isPaymentGatwayAndBundlesPlanExistscancelled($studio_id, $user_id, $default_currency_id);
            if (isset($bundleplan_payment_gateway_cancelled) && !empty($bundleplan_payment_gateway_cancelled)) {
                $gateways = $bundleplan_payment_gateway_cancelled['gateways'];
                $bundlesplans_cancelled = $bundleplan_payment_gateway_cancelled['plans'];
                $bundlesplans_cancelled[0]['status'] = 'cancelled';
            }

            $plans = array_merge($bundlesplans_cancelled, $bundlesplans);
            if (isset($plans) && !empty($plans)) {
                $data['code'] = 200;
                $data['Plan'] = $plans;
                $data['msg'] = "Ok";
            } else {
                $data['code'] = 406;
                $data['status'] = 'Error';
                $data['msg'] = "No Record Found!!";
            }
        } else {
            $data['code'] = 407;
            $data['status'] = 'Error';
            $data['msg'] = "Invalid user id";
        }

        echo json_encode($data);
        exit;
    }

    /**
     * @method private CancelsubscriptionPlan() Cancel the subscription plan those have user taken
     * @author sunil Nayak<suniln@muvi.com>
     * @return json Returns the list of data in json format
     * @param string cancel_note (*)
     * @param string authToken Auth token(*)
     * @param string user_id (*)
     * @param string cancelreason (*)
     * @param int is_admin (optional)
     */
    public function actionCancelsubscriptionPlan() {
        $res = array();
        $card = array();
        if (isset($_REQUEST['cancel_note']) && trim($_REQUEST['cancel_note'])) {
            $studio_id = $this->studio_id;
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = $_REQUEST['user_id'];
                //Getting user subscription detail to find out plan, card detail
                $usersub = new UserSubscription;
                $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1, 'plan_id' => $_REQUEST['plan_id']));
                if (!empty($usersub)) {

                    $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
                    $payment_gateway_type = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    //Getting plan detail
                    $plan = new SubscriptionPlans;
                    $plan = $plan->findByPk($usersub->plan_id);
                    //Getting card detail
                    $card = new SdkCardInfos;
                    $card = $card->findByPk($usersub->card_id);
                    //Deduct outstanding due if plan is post paid and sent invoice detail to user
                    $is_paid = 1;
                    $file_name = '';
                    if (isset($plan) && intval($plan->is_post_paid)) {
                        //If trial period expires
                        $start_date = date("Y-m-d", strtotime($usersub->start_date));
                        $today_date = gmdate('Y-m-d');
                        if (strtotime($start_date) < strtotime($today_date)) {
                            $trans_data = $this->processTransaction($card, $usersub);
                            $is_paid = $trans_data['is_success'];
                            //Save a transaction detail
                            if (intval($is_paid)) {
                                //Getting Ip address
                                $ip_address = Yii::app()->request->getUserHostAddress();
                                $transaction = new Transaction;
                                $transaction->user_id = $user_id;
                                $transaction->studio_id = $studio_id;
                                $transaction->plan_id = $usersub->plan_id;
                                $transaction->transaction_date = new CDbExpression("NOW()");
                                $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                                $transaction->transaction_status = $trans_data['transaction_status'];
                                $transaction->invoice_id = $trans_data['invoice_id'];
                                $transaction->order_number = $trans_data['order_number'];
                                $transaction->amount = $trans_data['amount'];
                                $transaction->response_text = $trans_data['response_text'];
                                $transaction->subscription_id = $usersub->id;
                                $transaction->ip = $ip_address;
                                $transaction->created_by = $user_id;
                                $transaction->created_date = new CDbExpression("NOW()");
                                $transaction->save();
                            }
                        }
                    }
                    if (intval($is_paid)) {
                        $success = 1;
                        //Cancel customer's account from payment gateway
                        $cancel_acnt = $this->cancelCustomerAccount($usersub, $card);
                        //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r($cancel_acnt, true));
                        if (isset($payment_gateway_type) && ($payment_gateway_type == 'paypal' || $payment_gateway_type == 'paypalpro') && ($usersub->card_id == '' || !$usersub->card_id)) {
                            if (isset($cancel_acnt) && $cancel_acnt['ACK'] == 'Success') {
                                $success = 1;
                            } else {
                                $success = 0;
                            }
                        } else {
                            //Delete card detail of user
                            if (trim($usersub->profile_id)) {
                                SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id AND profile_id = :profile_id', array(
                                    ':studio_id' => $studio_id,
                                    ':user_id' => $user_id,
                                    ':profile_id' => $usersub->profile_id
                                ));
                            }
                        }
                        if ($success) {
                            //fwrite($fp, "Dt: ".date('d-m-Y H:i:s').'-----'.print_r('sunccess', true));
                            //Inactivate user subscription
                            $reason_id = isset($_REQUEST['cancelreason']) ? $_REQUEST['cancelreason'] : '';
                            $cancel_note = isset($_REQUEST['cancel_note']) ? $_REQUEST['cancel_note'] : '';
                            $usersub->status = 0;
                            $usersub->cancel_date = new CDbExpression("NOW()");
                            $usersub->cancel_reason_id = $reason_id;
                            $usersub->cancel_note = htmlspecialchars($cancel_note);
                            $usersub->payment_status = 0;
                            $usersub->partial_failed_date = '';
                            $usersub->partial_failed_due = 0;
                            $usersub->canceled_by = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? 0 : $user_id;
                            $usersub->save();
                            //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                            if ($plan->is_subscription_bundle == 0) {
                                $usr = new SdkUser;
                                $usr = $usr->findByPk($user_id);
                                $usr->is_deleted = 1;
                                $usr->deleted_at = new CDbExpression("NOW()");
                                $usr->will_cancel_on = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? '' : $usersub->start_date;
                                $usr->save();
                            }
                            //Send an email to user
                            if (isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin'])) {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation', $file_name, '', '', '', '', '', $lang_code);
                            } else {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation_user', $file_name, '', '', '', '', '', $lang_code);
                            }
                            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_cancellation', $studio_id);
                            if ($isEmailToStudio) {
                                $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled', $user_id, '', 0);
                            }
                            $data['code'] = 200;
                            $data['status'] = "Success";
                            $data['msg'] = "Subscription has been cancelled successfully.";
                        } else {
                            $data['code'] = 406;
                            $data['status'] = "Error";
                            $data['msg'] = "Subscription can not be cancelled. Please contact to store admin";
                        }
                    } else {
                        $data['code'] = 406;
                        $data['status'] = "Error";
                        $data['msg'] = "Transaction can't be processed.Subscription can't be cancelled";
                    }
                } else {
                    $data['code'] = 406;
                    $data['status'] = "Error";
                    $data['msg'] = "User has no subscription plan to cancel or subscription plan is cancelled already";
                }
            } else {
                $data['code'] = 406;
                $data['status'] = "Error";
                $data['msg'] = "Invalid user details";
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "Error";
            $data['msg'] = "Please add cancel note";
        }
        echo json_encode($data);
        exit;
    }
public function actionInAppPpvpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id'],'parent_id'=>0));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);
        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }

        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;
            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '0000-00-00 00:00:00';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }
            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount

                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount

                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount

                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }
            $VideoName = Yii::app()->common->getVideoname($video_id);
            $VideoName = trim($VideoName);
            $VideoDetails = $VideoName;
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }
            $data['paymentDesc'] = $VideoDetails . ' - ' . $data['amount'];
            $data['in_app'] = 1;

            $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                $data['user_email'] = SdkUser::model()->findByPk($_REQUEST['user_id'])->email;
            }

            $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date);
            $ppv_subscription_id = $set_ppv_subscription;

            $trans_data['transaction_status'] = 'success';
            $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
            $trans_data['order_number'] = Yii::app()->common->generateUniqNumber();
            $trans_data['amount'] = $data['amount'];
            $trans_data['dollar_amount']= '';
            $trans_data['response_text']= '';

            $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id);
            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, '', $VideoName, $VideoDetails);

            $res['code'] = 200;
            $res['status'] = "OK";

        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }
        

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    function cancelCustomerAccount($usersub = Null, $card = Null) {
        $data = '';
        $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
        if (isset($this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY[$gateway_info->short_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::cancelCustomerAccount($usersub, $card);
        }
        return $data;
    }

}
