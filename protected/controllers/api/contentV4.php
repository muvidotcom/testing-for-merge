<?php
trait ContentV4 {

    /**
     * @method private getContentList() Get the list of contents based on its content_type permalink
     * @author GDR<support@muvi.com>VideoLogs

     * @return json Returns the list of data in json format
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentList() {
        if ($_REQUEST['permalink']) {
			$category_flag = 0;
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
			$user_id = (isset($_REQUEST['user_id']) && is_numeric($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
            if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != 'en')
                $language_id = Yii::app()->custom->getLanguage_id($_REQUEST['lang_code']);
            else
                $language_id = 20;
			$menuItemInfo = MenuItem::model()->find('studio_id=:studio_id AND permalink=:permalink AND (language_id=:language_id OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id=:studio_id AND permalink=:permalink AND language_id=:language_id))', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink'], ':language_id'=>$language_id));
            if (empty($menuItemInfo)) {
				$menuItemInfo = AppMenuItems::model()->find('studio_id=:studio_id AND permalink=:permalink AND (language_id=:language_id OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM app_menu_items WHERE studio_id=:studio_id AND permalink=:permalink AND language_id=:language_id))', array(':studio_id' => $this->studio_id, ':permalink' => $_REQUEST['permalink'], ':language_id'=>$language_id));
            }
			if(empty($menuItemInfo)){
				$menuItemInfo = Yii::app()->db->createCommand()
					->select('id as value,category_name as title,studio_id,binary_value,permalink,added_by,language_id,parent_id,is_poster,id_seq')
					->from('content_category')
					->where('studio_id='.$this->studio_id.' AND permalink = "'.$_REQUEST['permalink'].'" AND (language_id='.$language_id.' OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id='.$this->studio_id.' AND permalink = "'.$_REQUEST['permalink'].'" AND language_id='.$language_id.'))')
					->queryRow();
				$category_flag = 1;
			}
            if ($menuItemInfo) {
                $menuInfo = ($category_flag)?$menuItemInfo:$menuItemInfo->attributes;
                $content_type = $contentTypeInfo['id'];
                $content_types_id = $contentTypeInfo['content_types_id'];
                $studio_id = $this->studio_id;
                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }
                $pdo_cond_arr = array(':studio_id' => $this->studio_id, ':content_category_value' => $menuInfo['value']);

                $order = '';

                $cond = ' ';
                $pdo_cond = '';
                $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                $orderData = ContentOrdering::model()->find('studio_id=:studio_id AND category_value=:category_value ', array(':studio_id' => $studio_id, ':category_value' => $menuInfo['value']));
                if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
                    $order = $_REQUEST['orderby'];
                    if ($_REQUEST['orderby'] == 'lastupload') {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'releasedate') {
                        $orderby = "  F.release_date DESC";
                        $neworderby = " P.release_date DESC ";
                    } else if ($_REQUEST['orderby'] == 'sortasc') {
                        $orderby = "  F.name ASC";
                        $neworderby = " P.name ASC ";
                    } else if ($_REQUEST['orderby'] == 'sortdesc') {
                        $orderby = "  F.name DESC";
                        $neworderby = " P.name DESC ";
                    }
                } else if (@$orderData->method) {
                    if (@$orderData->orderby_flag == 1) {// most viewed
                        $mostviewed = 1;
                        $neworderby = " Q.cnt DESC";
                    } else if (@$orderData->orderby_flag == 2) {//Alphabetic A-Z
                        $orderby = "  trim(F.name) ASC";
                        $neworderby = " trim(P.name) ASC ";
                    } else if (@$orderData->orderby_flag == 3) {//Alphabetic Z-A
                        $orderby = "  trim(F.name) DESC";
                        $neworderby = " trim(P.name) DESC ";
                    } else {
                        $orderby = "  M.last_updated_date DESC";
                        $neworderby = " P.last_updated_date DESC ";
                    }
                } else if ($orderData && $orderData->ordered_stream_ids) {
                    $orderby = " FIELD(movie_stream_id," . $orderData->ordered_stream_ids . ")";
                    $neworderby = " FIELD(movie_stream_id," . $orderData->ordered_stream_ids . ")";
                } else {
                    $orderby = "  M.last_updated_date DESC ";
                    $neworderby = " P.last_updated_date DESC ";
                }
                if (@$_REQUEST['genre']) {
                    if (is_array($_REQUEST['genre'])) {
                        $cond .= " AND (";
                        foreach ($_REQUEST['genre'] AS $gkey => $gval) {
                            if ($gkey) {
                                $cond .= " OR ";
                            }
                            $cond .= " (genre LIKE ('%" . $gval . "%'))";
                        }
                        $cond .= " ) ";
                    } else {
                        $cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
                    }
                }
                $cond.=(isset($_REQUEST['is_publishdate']) && is_numeric($_REQUEST['is_publishdate']) && $_REQUEST['is_publishdate']==1) ? "" : ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) '; 
                if ($_REQUEST['prev_dev']) {
                    $command = Yii::app()->db->createCommand()
                            ->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.content_publish_date,M.movie_id,M.id AS movie_stream_id,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted, M.full_movie,F.language AS content_language,F.censor_rating,M.is_downloadable')
                            ->from('movie_streams M,films F')
                            ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (FIND_IN_SET(:content_category_value,F.content_category_value)) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
                            ->order($orderby)
                            ->limit($limit, $offset);
                } else {
                    /* Add Geo block to listing by manas@muvi.com */
                    if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                        $country = $_REQUEST['country'];
                    } else {
                        $visitor_loc = Yii::app()->common->getVisitorLocation();
                        $country = $visitor_loc['country'];
                    }
                    $sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.content_publish_date,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date,F.language AS content_language,F.censor_rating,M.is_downloadable"
                            . " FROM movie_streams M,films F WHERE "
                            . "M.movie_id = F.id AND M.studio_id=" . $studio_id . " AND  (FIND_IN_SET(" . $menuInfo['value'] . ",F.content_category_value)) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
                    $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
                    $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                    if ($mostviewed) {
                        $sql_data.=" LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=" . $studio_id . " GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
                    }
                    $sql_data.=" WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby . " LIMIT " . $limit . " OFFSET " . $offset;
                    $command = Yii::app()->db->createCommand($sql_data);
                }
                $list = $command->queryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }
                //Get Posters for the Movies 
                $movieids = '';
                $movieList = '';
                if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                    //Retrive the total counts based on deviceType 
                    $countQuery = Yii::app()->db->createCommand()
                            ->select('COUNT(DISTINCT F.id) AS cnt')
                            ->from('movie_streams M,films F')
                            ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND (FIND_IN_SET(:content_category_value,F.content_category_value)) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
                    $itemCount = $countQuery->queryAll();
                    $item_count = @$itemCount[0]['cnt'];

                    $newList = array();
                    foreach ($list AS $k => $v) {
                        if ($v['content_types_id'] == 3) {
                            $epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
                            $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                            if (count($epData)) {
                                $newList[] = $list[$k];
                            }
                            //Added by prakash on 1st feb 2017        
                        } else if ($v['content_types_id'] == 4) {
                            $epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
                            $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                            if (count($epData)) {
                                $newList[] = $list[$k];
                            }
                        } else if (($v['content_types_id'] == 1 || $v['content_types_id'] == 2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                            $newList[] = $list[$k];
                        }
                    }
                    $list = array();
                    $list = $newList;
                }
                $domainName = $this->getDomainName();
                $livestream_content_ids = '';
                foreach ($list AS $k => $v) {
                    $movieids .="'" . $v['movie_id'] . "',";
                    if ($v['content_types_id'] == 4) {
                        $livestream_content_ids .="'" . $v['movie_id'] . "',";
                    }
                    if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
                        $defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
                    } else {
                        $defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
                    }
                    $movie_id = $v['movie_id'];
                    $v['poster_url'] = $defaultPoster;
                    if ($_REQUEST['user_id']) {
                        $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id, @$_REQUEST['user_id'], $movie_id, $v['movie_stream_id']);
                        array_push($list[$k]['watch_duration'] = $watch_duration['resume_time']);
                        $duration_query = Yii::app()->db->createCommand()
                                ->select('video_duration')
                                ->from('movie_streams')
                                ->where('id = ' . $v['movie_stream_id']);
                        $total_video_duration = $duration_query->queryAll();
                        array_push($list[$k]['video_duration'] = $total_video_duration['video_duration']);
                    }
                    $list[$k]['poster_url'] = $defaultPoster;
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
                    $is_episode = 0;
					$is_fav_status = 0;
					if ($user_id > 0) {
						$fav = UserFavouriteList::model()->countByAttributes(array("user_id" => $user_id, "studio_id" => $studio_id, "content_id" => $v['movie_id'], "content_type" => '0', "status" => '1'));
						$is_fav_status = ($fav > 0) ? 1 : 0;
					}
					$list[$k]['is_favorite'] =  $is_fav_status;
                    $arg['studio_id'] = $this->studio_id;
                    $arg['movie_id'] = $v['movie_id'];
                    $arg['season_id'] = 0;
                    $arg['episode_id'] = 0;
                    $arg['content_types_id'] = $v['content_types_id'];

                    $isFreeContent = Yii::app()->common->isFreeContent($arg);
                    $list[$k]['isFreeContent'] = $isFreeContent;
                    $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                    $is_ppv = $is_advance = 0;
                    if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
                        if ($v['is_converted'] == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
                            $is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
                        }
                    }

                    if (intval($is_ppv)) {
                        $list[$k]['is_ppv'] = $is_ppv;
                    }
                    if (intval($is_advance)) {
                        $list[$k]['is_advance'] = $is_advance;
                    }

                    if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                        $director = '';
                        $actor = '';
                        $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                        if ($castsarr) {
                            $actor = $castsarr['actor'];
                            $actor = trim($actor, ',');
                            unset($castsarr['actor']);
                            $director = $castsarr['director'];
                            unset($castsarr['director']);
                        }
                        $list[$k]['actor'] = @$actor;
                        $list[$k]['director'] = @$director;
                        $list[$k]['cast_detail'] = @$castsarr;
                    }
                }

                $movieids = rtrim($movieids, ',');
                $livestream_content_ids = rtrim($livestream_content_ids, ',');
                $liveFeedUrls = array();
                if ($livestream_content_ids) {
                    $StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (" . $livestream_content_ids . ') AND studio_id=' . $this->studio_id . "  ";
                    $streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
                    foreach ($streamData AS $skey => $sval) {
                        $liveFeedUrls[$sval['movie_id']]['feed_url'] = $sval['feed_url'];
                        $liveFeedUrls[$sval['movie_id']]['is_online'] = $sval['is_online'];
                    }
                }
                if ($movieids) {
                    $sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (" . $movieids . ") AND studio_id=" . $studio_id . " AND language_id=" . $language_id;
                    $otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
                    foreach ($otherlang1 as $key => $val) {
                        $otherlang[$val['parent_id']] = $val;
                    }

                    $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                        foreach ($posterData AS $key => $val) {
                            $posterUrl = '';
                            if ($val['object_type'] == 'films') {
                                $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
                                $posters[$val['movie_id']] = $posterUrl;
                                if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                }
                            } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                            }
                        }
                    }
                    //Get view count status
                    $viewStatus = VideoLogs::model()->getViewStatus($movieids, $studio_id);
                    if (@$viewStatus) {
                        $viewStatusArr = '';
                        foreach ($viewStatus AS $key => $val) {
                            $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                            $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                        }
                    }
                    foreach ($list as $key => $val) {
                        if (isset($otherlang[$val['movie_id']])) {
                            $list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
                            $list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
                            $list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
                            $list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
                        }
                        if (isset($posters[$val['movie_id']])) {
                            if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                                $list[$key]['poster_url_for_tv'] = $postersforTv[$val['movie_id']];
                            }
                            if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
                                $posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
                            }
                            $list[$key]['poster_url'] = $posters[$val['movie_id']];
                        }
                        if (@$viewStatusArr[$val['movie_id']]) {
                            $list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
                        } else {
                            $list[$key]['viewStatus'] = array('viewcount' => "0", 'uniq_view_count' => "0");
                        }
                        if ($val['content_types_id'] == 4) {
                            $list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
                            $list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
                        }
                        $list[$key]['custom_meta_data'] = $this->getCustomMetadata($list[$key], $translate);
                    }
                }
                $data['status'] = 200;
                $data['msg'] = $translate['btn_ok'];
                $data['movieList'] = @$list;
                
                if (isset($_REQUEST['vd_info']) && $_REQUEST['vd_info'] > 0) {
                    $item_info = VdStudioConfig::model()->findByPk($_REQUEST['vd_info'], array('select' => 'list_item_info'));
                    if (!empty($item_info)) {
                        $data['list_item_info'] = json_decode($item_info->list_item_info);
                    }
                }
                $data['orderby'] = @$order;
                $data['item_count'] = @$item_count;
                $data['limit'] = @$page_size;
                $data['Ads'] = $this->getStudioAds();
				$data['menu_title'] = @$menuInfo['title'];
            } else {
                $data['code'] = 412;
                $data['status'] = "Invalid Content Type";
                $data['msg'] = $translate['provided_permalink_invalid'];
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Invalid Content Type";
            $data['msg'] = $translate['content_type_permalink_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;
    }

    /*     * actionEpisodeDetails

     * @method GetSystemMetaData It will return the system metadata names
     * @author Biswajit<biswajit@muvi.com>
     * @return array array of system meta fields
     */

    public function actionGetSystemMetaData() {
        $system_vals = array();
        $cmfs = CustomMetadataField::model()->findAllByAttributes(array("studio_id" => $this->studio_id), array('select' => 'f_display_name,f_name'));
        if (empty($cmfs)) {
            $cmfs = CustomMetadataField::model()->findAllByAttributes(array("studio_id" => 0), array('select' => 'f_display_name,f_name'));
        }
        $system_val['field_id'] = 'poster_url';
        $system_val['display_name'] = 'Poster';
        $system_vals[] = $system_val;
        $system_val['field_id'] = 'title';
        $system_val['display_name'] = 'Title';
        $system_vals[] = $system_val;
        $system_val['field_id'] = 'censor_rating';
        $system_val['display_name'] = 'Censor Rating';
        $system_vals[] = $system_val;
        $system_val['field_id'] = 'reviewsummary';
        $system_val['display_name'] = 'User Rating';
        $system_vals[] = $system_val;
        $system_val['field_id'] = 'price';
        $system_val['display_name'] = 'Price';
        $system_vals[] = $system_val;
        $system_val['field_id'] = 'add_to_fav';
        $system_val['display_name'] = 'Addto Fav,Calendar Button';
        $system_vals[]=$system_val;
         $system_val['field_id'] = 'add_to_playlist_n_que';
        $system_val['display_name'] = 'Addto Playlist,Queue Button';
        $system_vals[]=$system_val;
        $system_val['field_id'] = 'play_butn';
        $system_val['display_name'] = 'Play Button';
        $system_vals[] = $system_val;

        if (!empty($cmfs)):
            foreach ($cmfs as $cmf):
                //$system_vals[trim($cmf->f_id)] = trim($cmf->f_display_name);
                $system_val['field_id'] = trim($cmf->f_name);
                $system_val['display_name'] = trim($cmf->f_display_name);
                $system_vals[] = $system_val;
            endforeach;
        endif;
        $data['code'] = 200;
        $data['status'] = "Success";
        $data['metadata_field'] = $system_vals;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
   public function actionGetShopSystemMetaData(){
        $system_vals = array();
        $customComp = new CustomForms();
        $arg['content_type'] = 0;
        $arg['is_child'] = 0;
            $cmfs = $customComp->getCustomMetadata($this->studio_id,5, $arg);
        if(empty($cmfs)){
            $cmfs = $customComp->getCustomMetadata(0,5, $arg);
        }
        unset($cmfs['formData']);
        $system_val['field_id'] = 'poster_url';
        $system_val['display_name'] = 'Poster';
        $system_vals[] = $system_val;
	$system_val['field_id'] = 'title';
        $system_val['display_name'] = 'Title';
        $system_vals[]=$system_val;        
        $system_val['field_id'] = 'price';
        $system_val['display_name'] = 'Price';
        $system_vals[]=$system_val;
        $system_val['field_id'] = 'trailer_butn';
        $system_val['display_name'] = 'Trailer Button';
        $system_vals[]=$system_val;
        $system_val['field_id'] = 'buy_butn';
        $system_val['display_name'] = 'Buy Button';
        $system_vals[]=$system_val;
        $system_val['field_id'] = 'qty_field';
        $system_val['display_name'] = 'Quantity Field';
        $system_vals[]=$system_val;

        
        if(!empty($cmfs)):
            foreach($cmfs as $cmf):
                $system_val['field_id'] = trim($cmf['f_name']);
                $system_val['display_name'] = trim($cmf['f_display_name']);
                $system_vals[]=$system_val;
            endforeach;
        endif;
        $data['code'] = 200; 
        $data['status'] = "Success";
        $data['metadata_field'] = $system_vals;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /**
     * @method private getepisodeDetails() Get the details of Episode
     * @author GDR<support@muvi.com>
     * @return json Returns the list of episodes
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionEpisodeDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_order = StudioConfig::model()->getConfig($this->studio_id, 'multipart_content_setting')->config_value;
            $order_type = ($content_order == 0) ? 'desc' : 'asc';
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.content_type_id,f.content_types_id,ms.id AS movie_stream_id,f.permalink')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND f.permalink=:permalink AND f.studio_id=:studio_id ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryAll();
            if ($movie) {
                $movie = $movie[0];
                $movie_id = $movie['id'];
                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id, 0, $language_id, $this->studio_id);
                if (array_key_exists($movie_id, $langcontent['film'])) {
                    $movie['name'] = $langcontent['film'][$movie_id]->name;
                    $movie['story'] = $langcontent['film'][$movie_id]->story;
                    $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                    $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                }
                $cast = array();
                if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                    $director = '';
                    $actor = '';
                    $castsarr = $this->getCasts($movie_id, '', $language_id, $this->studio_id);
                    if ($castsarr) {
                        $actor = $castsarr['actor'];
                        $actor = trim($actor, ',');
                        unset($castsarr['actor']);
                        $director = $castsarr['director'];
                        unset($castsarr['director']);
                    }
                    $cast['actor'] = @$actor;
                    $cast['director'] = @$director;
                    $cast['cast_detail'] = @$castsarr;
                }

                //Checking PPV set or not
                $ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
                $ppvarr = array();

                if ((isset($ppv) && !empty($ppv))) {
                    $command = Yii::app()->db->createCommand()
                            ->select('default_currency_id')
                            ->from('studios')
                            ->where('id = ' . $this->studio_id);
                    $studio = $command->queryAll();

                    $price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
                    (array) $currency = Currency::model()->findByPk($price['currency_id']);

                    $ppvarr['id'] = $ppv->id;
                    $ppvarr['title'] = $ppv->title;

                    $ppvarr['pricing_id'] = $price['id'];

                    $ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
                    $ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];

                    $ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
                    $ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
                    $ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];

                    $ppvarr['show_subscribed'] = $price['show_subscribed'];
                    $ppvarr['season_subscribed'] = $price['season_subscribed'];
                    $ppvarr['episode_subscribed'] = $price['episode_subscribed'];

                    $validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

                    $ppvarr['validity_period'] = $validity->validity_period;
                    $ppvarr['validity_recurrence'] = $validity->validity_recurrence;

                    if (isset($ppv->content_types_id) && (trim($ppv->content_types_id) == 3 || trim($ppv->content_types_id) == 6)) {
                        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
                        $ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                        $ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                        $ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                    }

                    $movie['is_ppv'] = 1;
                } else {
                    $movie['is_ppv'] = 0;
                }

                if ($movie['content_types_id'] != 3 && $movie['content_types_id'] != 6) {
                    $data['code'] = 415;
                    $data['status'] = "Invalid Content";
                    $data['msg'] = "Invalid content to get episode details";
                    echo json_encode($data);
                    exit;
                }
                $pdo_cond_arr = array(':movie_id' => $movie_id, ":is_episode" => 1);
                if (isset($_REQUEST['series_number']) && $_REQUEST['series_number']) {
                    $cond = " AND series_number=:series_number";
                    $pdo_cond_arr[':series_number'] = $_REQUEST['series_number'];
                }
                $forRokuCond = '';
                if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                    $forRokuCond = '  AND is_converted=:is_converted ';
                    $pdo_cond_arr[':is_converted'] = 1;
                }
                $orderby = ' episode_number ' . $order_type;

                $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset'])) {
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                }

                $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                $signedFolderPath = $folderPath['signedFolderPath'];
                $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";
                $list = array();
                $addQuery = '';
                $customFormObj = new CustomForms();
                $customData = $customFormObj->getFormCustomMetadat($this->studio_id, 4);
                if ($customData) {
                    foreach ($customData AS $ckey => $cvalue) {
						$fnumber = (int) str_replace('custom', '', $cvalue['field_name']);
						if($fnumber<=6)
							$addQuery = ", " . $cvalue['field_name'] . " AS " . addslashes($cvalue['f_id']);
                        $customArr[$cvalue['f_id']] = $cvalue['f_display_name'];
                    }
                }
				$addQuery = ',custom_metadata_form_id,custom1,custom2,custom3,custom4,custom5,custom6';
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),id,embed_id AS movie_stream_uniq_id,full_movie,episode_number,video_resolution,episode_title,series_number,episode_date,episode_story,IF((full_movie != "" AND is_converted = 1),CONCAT(\'' . $video_url . '\',id,\'/\',full_movie),"") AS video_url,thirdparty_url,rolltype,roll_after,video_duration' . $addQuery)
                        ->from('movie_streams')
                        ->where('movie_id=:movie_id AND is_episode=:is_episode AND ((content_publish_date IS NULL) OR (UNIX_TIMESTAMP(content_publish_date) = 0) OR (content_publish_date <=NOW())) AND episode_parent_id = 0 ' . $forRokuCond . $cond, $pdo_cond_arr)
                        ->order($orderby)
                        ->limit($limit, $offset);
                $list = $command->queryAll();

                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
				$c_form_ids = CHtml::listData($list, 'custom_metadata_form_id', 'custom_metadata_form_id');
				$cforms = array();
				$customComp = new CustomForms();
				foreach ($c_form_ids as $key => $value) {
					$cforms[$value] = $customComp->getEpisodeCustomData($value,$this->studio_id);
				}
                if ($list) {
                    $domainName = $this->getDomainName();
                    $_REQUEST['is_mobile'] = 1;
                    foreach ($list AS $k => $v) {
						$list[$k]['custom_field'] = $customComp->getEpisodeCustomValue($cforms[$v['custom_metadata_form_id']],$v);
                        $langcontent = Yii::app()->custom->getTranslatedContent($v['id'], 1, $language_id, $this->studio_id);
                        if (array_key_exists($v['id'], $langcontent['episode'])) {
                            $list[$k]['episode_title'] = $langcontent['episode'][$v['id']]->episode_title;
                            $list[$k]['episode_story'] = $langcontent['episode'][$v['id']]->episode_story;
                        }
                        $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                        $defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
                        $v['poster_url'] = $defaultPoster;
                        $list[$k]['poster_url'] = $this->getPoster($v['id'], 'moviestream', 'episode', $this->studio_id);
                        if (isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                             $TvAppPosterImg = $this->getPoster($v['id'], 'tvapp','original',$this->studio_id);
                              $list[$k]['poster_url_for_tv'] = $TvAppPosterImg;
                            $posterUrlForTv = $this->getPoster($v['id'], 'moviestream', 'roku', $this->studio_id);
                            if ($posterUrlForTv != '') {
                                $list[$k]['posterForTv'] = $posterUrlForTv;
                            }
                        }
                        if ($_REQUEST['user_id']) {
                            $studio_id = $this->studio_id;
                            $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id, @$_REQUEST['user_id'], $movie_id, $v['id']);
                            array_push($list[$k]['watch_duration'] = $watch_duration['resume_time']);
                        }
                        $list[$k]['movieUrlForTv'] = '';
                        $list[$k]['video_url'] = '';
                        $list[$k]['resolution'] = array();
                        if ($v['thirdparty_url'] != "") {
                            $info = pathinfo($v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8") {
                                $list[$k]['movieUrlForTv'] = $v['thirdparty_url'];
                                $list[$k]['video_url'] = $v['thirdparty_url'];
                            }
                            $list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                        } else if ($v['video_url'] != '') {
                            $list[$k]['movieUrlForTv'] = $v['video_url'];
                            $multipleVideo = $this->getvideoResolution($v['video_resolution'], $v['video_url']);
                            $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $v['video_url']);
                            $videoToBeplayed12 = explode(",", $videoToBeplayed);
                            $list[$k]['video_url'] = $videoToBeplayed12[0];
                            $list[$k]['resolution'] = $multipleVideo;
                            //Ad Details
                            $list[$k]['adDetails'] = array();
                            if ($v['rolltype'] == 1) {
                                $list[$k]['adDetails'][] = 0;
                            } elseif ($v['rolltype'] == 2) {
                                $roleafterarr = explode(',', $v['roll_after']);
                                foreach ($roleafterarr AS $ktime => $vtime) {
                                    $list[$k]['adDetails'][] = $this->convertTimetoSec($vtime);
                                }
                            } elseif ($v['rolltype'] == 3) {
                                $list[$k]['adDetails'][] = $this->convertTimetoSec($v['video_duration']);
                            }
                        }
                    }
                }
                $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));

                $data['code'] = 200;
                $data['msg'] = $translate['btn_ok'];
                $data['name'] = $movie['name'];
                $data['muvi_uniq_id'] = $movie['muvi_uniq_id'];
                $data['custom_fields'] = $customArr;
                $data['is_ppv'] = $movie['is_ppv'];
                if (!empty($ppvarr)) {
                    $data['ppv_pricing'] = $ppvarr;
                    $data['currency'] = $currency->attributes;
                }
                $data['permalink'] = $movie['permalink'];
                $data['item_count'] = $item_count;
                $data['limit'] = $limit;
                $data['offset'] = $offset;
                $data['comments'] = $comments;
                $data['episode'] = $list;
                $data['cast'] = $cast;
                if(@$_REQUEST['series_number']){
                   $season_total_duration = Yii::app()->db->createCommand()
                        ->select('sum(Left(video_duration,2) * 3600 + substring(video_duration, 4,2) * 60 + substring(video_duration, 7,2))')
                        ->from('movie_streams')
                        ->where('movie_id = '. $movie_id . ' and series_number = '.@$_REQUEST['series_number'] )
                        ->queryScalar();
                    if($season_total_duration != "")
                        $data['season_total_duration'] =$season_total_duration;
                    else
                        $data['season_total_duration'] = 0;
                }
            } else {
                $data['code'] = 413;
                $data['status'] = "Invalid Permalink";
                $data['msg'] = $translate['content_permalink_invalid'];
            }
        } else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = $translate['content_permalink_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private getContentDetails() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actionGetContentDetails() {
        if ($_REQUEST['permalink']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            if(isset($_REQUEST['is_publishdate']) && is_numeric($_REQUEST['is_publishdate']) && $_REQUEST['is_publishdate']==1){
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.full_movie,ms.content_publish_date,ms.is_converted,ms.video_resolution,ms.embed_id AS movie_stream_uniq_id, f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.permalink,f.content_type_id,f.genre,f.release_date,f.censor_rating,f.story,ms.rolltype,ms.roll_after,ms.video_duration,ms.thirdparty_url,ms.is_episode,f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10,f.language AS content_language,ms.is_downloadable,f.custom_metadata_form_id')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND f.permalink=:permalink AND  f.studio_id=:studio_id AND f.parent_id = 0 AND ms.episode_parent_id=0 ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryAll();
            }else
            {
             $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.content_publish_date,ms.full_movie,ms.is_converted,ms.video_resolution,ms.embed_id AS movie_stream_uniq_id, f.uniq_id AS muvi_uniq_id,f.ppv_plan_id, f.permalink,f.content_type_id,f.genre,f.release_date,f.censor_rating,f.story,ms.rolltype,ms.roll_after,ms.video_duration,ms.thirdparty_url,ms.is_episode,f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10,f.language AS content_language,ms.is_downloadable,f.custom_metadata_form_id')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ms.is_episode=0 AND ((ms.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(ms.content_publish_date) = 0) OR (ms.content_publish_date <=NOW())) AND  f.permalink=:permalink AND  f.studio_id=:studio_id AND f.parent_id = 0 AND ms.episode_parent_id=0 ', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryAll();    
            }
            if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                $country = $_REQUEST['country'];
            } else {
                $visitor_loc = Yii::app()->common->getVisitorLocation();
                $country = $visitor_loc['country'];
            }
            if (Yii::app()->common->isGeoBlockContent(@$movie[0]['id'], @$movie[0]['movie_stream_id'], $this->studio_id, $country)) {//Auther manas@muvi.com
                if ($movie) {
                    $movie = $movie[0];
                    $thirdPartyUrl = '';
                    $domainName = $this->getDomainName();
                    $movie['censor_rating'] = stripslashes($movie['censor_rating']);
                    $movie_id = $movie['id'];
                    $langcontent = Yii::app()->custom->getTranslatedContent($movie_id, 0, $language_id, $this->studio_id);
                    $arg['studio_id'] = $this->studio_id;
                    $arg['movie_id'] = $movie_id;
                    $arg['season_id'] = 0;
                    $arg['episode_id'] = 0;
                    $arg['content_types_id'] = $movie['content_types_id'];

                    $isFreeContent = Yii::app()->common->isFreeContent($arg);
                    $movie['isFreeContent'] = $isFreeContent;

                    //get trailer info from movie_trailer table 
                    $trailerThirdpartyUrl = '';
                    $trailerUrl = '';
                    $embedTrailerUrl = '';
                    $trailerData = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
                    if ($trailerData['trailer_file_name'] != '') {
                        $embedTrailerUrl = $domainName . '/embedTrailer/' . $movie['muvi_uniq_id'];
                        if ($trailerData['third_party_url'] != '') {
                            $trailerThirdpartyUrl = $this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                        } else if ($trailerData['video_remote_url'] != '') {
                            $trailerUrl = $this->getTrailer($movie_id, 0, "", $this->studio_id);
                        }
                    }
                    //End of trailer part    
                    //Checking PPV set or not
                    $is_ppv = $is_advance = 0;
                    if (intval($isFreeContent) == 0) {

                        $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);

                        if (isset($payment_gateway) && !empty($payment_gateway)) {
                            if ($movie['is_converted'] == 1) {
                                $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id, 0);
                                if (isset($ppv->id) && intval($ppv->id)) {
                                    $is_ppv = 1;
                                } else {
                                    $ppv = Yii::app()->common->getContentPaymentType($movie['content_types_id'], $movie['ppv_plan_id'], $this->studio_id);
                                    $is_ppv = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                                }
                            } else {
                                $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, $this->studio_id);
                                $is_advance = (isset($ppv->id) && intval($ppv->id)) ? 1 : 0;
                            }
                        }
                    }

                    $ppvarr = array();

                    if (intval($is_ppv) || intval($is_advance)) {
                        $command = Yii::app()->db->createCommand()
                                ->select('default_currency_id,rating_activated')
                                ->from('studios')
                                ->where('id = ' . $this->studio_id);
                        $studio = $command->queryAll();

                        $price = Yii::app()->common->getPPVPrices($ppv->id, $studio[0]['default_currency_id'], $_REQUEST['country']);
                        (array) $currency = Currency::model()->findByPk($price['currency_id']);

                        $ppvarr['id'] = $ppv->id;
                        $ppvarr['title'] = $ppv->title;

                        $ppvarr['pricing_id'] = $price['id'];

                        $ppvarr['price_for_unsubscribed'] = $price['price_for_unsubscribed'];
                        $ppvarr['price_for_subscribed'] = $price['price_for_subscribed'];

                        $ppvarr['show_unsubscribed'] = $price['show_unsubscribed'];
                        $ppvarr['season_unsubscribed'] = $price['season_unsubscribed'];
                        $ppvarr['episode_unsubscribed'] = $price['episode_unsubscribed'];

                        $ppvarr['show_subscribed'] = $price['show_subscribed'];
                        $ppvarr['season_subscribed'] = $price['season_subscribed'];
                        $ppvarr['episode_subscribed'] = $price['episode_subscribed'];

                        $validity = PpvValidity::model()->findByAttributes(array('studio_id' => $this->studio_id));

                        $ppvarr['validity_period'] = $validity->validity_period;
                        $ppvarr['validity_recurrence'] = $validity->validity_recurrence;

                        if (isset($ppv->content_types_id) && trim($ppv->content_types_id) == 3) {
                            $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $this->studio_id));
                            $ppvarr['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                            $ppvarr['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                            $ppvarr['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;
                        }

                        if (intval($is_ppv)) {
                            $movie['is_ppv'] = $is_ppv;
                        }
                        if (intval($is_advance)) {
                            $movie['is_advance'] = $is_advance;
                        }
                    } else {
                        $command = Yii::app()->db->createCommand()
                                ->select('rating_activated')
                                ->from('studios')
                                ->where('id = ' . $this->studio_id);
                        $studio = $command->queryAll();
                        $movie['is_ppv'] = 0;
                        $movie['is_advance'] = 0;
                    }

                    $castsarr = $this->getCasts($movie_id, '', $language_id, $this->studio_id);
                    if ($castsarr) {
                        $actor = $castsarr['actor'];
                        $actor = trim($actor, ',');
                        unset($castsarr['actor']);
                        $director = $castsarr['director'];
                        unset($castsarr['director']);
                    }

                    $cast_detail = $castsarr;
                    $movie['actor'] = @$actor;
                    $movie['director'] = @$director;
                    $movie['cast_detail'] = @$castsarr;
                    $movie['embeddedUrl'] = "";

                    //Get view status 
                    $viewStatus = VideoLogs::model()->getViewStatus($movie_id, $this->studio_id);
                    $movie['viewStatus']['viewcount'] = @$viewStatus[0]['viewcount'] ? @$viewStatus[0]['viewcount'] : "0";
                    $movie['viewStatus']['uniq_view_count'] = @$viewStatus[0]['u_viewcount'] ? @$viewStatus[0]['u_viewcount'] : "0";
                    if ($movie['thirdparty_url'] != "") {
                        $info = pathinfo($movie['thirdparty_url']);
                        if (@$info["extension"] == "m3u8") {
                            $movieUrlToBePlayed = $movie['thirdparty_url'];
                            $movieUrl = $movie['thirdparty_url'];
                        }
                        $thirdPartyUrl = $this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
                        $movie['embeddedUrl'] = $domainName . '/embed/' . $movie['movie_stream_uniq_id'];
                    } else if ($movie['full_movie'] != '' && $movie['is_converted'] == 1) {
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                        $bucket = $bucketInfo['bucket_name'];
                        $s3url = $bucketInfo['s3url'];
                        $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
                        $signedFolderPath = $folderPath['signedFolderPath'];
                        $movieUrl = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/" . $movie['movie_stream_id'] . "/" . urlencode($movie['full_movie']);
                        $movie['movieUrlForTv'] = $movieUrl;
                        $_REQUEST['is_mobile'] = 1;
                        $multipleVideo = $this->getvideoResolution($movie['video_resolution'], $movieUrl);

                        $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $movieUrl);
                        $videoToBeplayed12 = explode(",", $videoToBeplayed);
                        $movieUrlToBePlayed = $videoToBeplayed12[0];
                        $movie['embeddedUrl'] = $domainName . '/embed/' . $movie['movie_stream_uniq_id'];
                    }
                    $movie['movieUrlForTv'] = @$movieUrl;
                    $movie['movieUrl'] = @$movieUrlToBePlayed;
                    //Added thirdparty url
                    $movie['thirdparty_url'] = $thirdPartyUrl;
                    if ($movie['content_types_id'] == 4) {
                        $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id' => $this->studio_id, ':movie_id' => $movie_id));
                        if ($Ldata && $Ldata->feed_url) {
                            $movie['embeddedUrl'] = $domainName . '/embed/livestream/' . $movie['muvi_uniq_id'];
                            $movie['feed_url'] = $Ldata->feed_url;
                        }
                    }
                    $movie['resolution'] = @$multipleVideo;
                    $MovieBanner = $this->getPoster($movie_id, 'topbanner', 'original', $this->studio_id);
                    if ($movie['content_types_id'] == 2) {
                        $poster = $this->getPoster($movie_id, 'films', 'episode', $this->studio_id);
                    } else {
                        $poster = $this->getPoster($movie_id, 'films', 'thumb', $this->studio_id);
                    }
                    if (isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                        $posterForTv = $this->getPoster($movie_id, 'films', 'roku', $this->studio_id);
                        if ($posterForTv != '') {
                            $movie['posterForTv'] = $posterForTv;
                        }
                    }
                    $movie['banner'] = $MovieBanner;
                    $movie['poster'] = @$poster;
                    //$data = $this -> getMovieDetails($movie_id,0);
                    //$item = json_decode($data,TRUE);
                    $movie['story'] = preg_replace("/[\\\n\\r]+/", "", $movie['story']);
                    $movie_name = $movie['name'];
                    if ($movie['content_types_id'] == 3) {
                        $EpDetails = $this->getEpisodeToPlay($movie_id, $this->studio_id);
                        $seasons = $this->getContentSeasons($movie_id, $this->studio_id);
                    }
                    $comments = Comment::model()->findAll('movie_id=:movie_id AND  (parent_id IS NULL OR parent_id =0)  ORDER BY id desc', array(':movie_id' => $movie_id));
                    $movie['adDetails'] = array();
                    if ($movie['rolltype'] == 1) {
                        $movie['adDetails'][] = 0;
                    } elseif ($movie['rolltype'] == 2) {
                        $roleafterarr = explode(',', $movie['roll_after']);
                        foreach ($roleafterarr AS $tkey => $tval) {
                            $movie['adDetails'][] = $this->convertTimetoSec($tval);
                        }
                    } elseif ($movie['rolltype'] == 3) {
                        $movie['adDetails'][] = $this->convertTimetoSec($movie['video_duration']);
                    }
                    if (array_key_exists($movie_id, $langcontent['film'])) {
                        $movie = Yii::app()->Helper->getLanuageCustomValue($movie, @$langcontent['film'][$movie_id], 0);
                        $movie['name'] = $langcontent['film'][$movie_id]->name;
                        $movie['story'] = $langcontent['film'][$movie_id]->story;
                        $movie['genre'] = $langcontent['film'][$movie_id]->genre;
                        $movie['censor_rating'] = $langcontent['film'][$movie_id]->censor_rating;
                    }
                    //trailer part
                    $movie['trailerThirdpartyUrl'] = $trailerThirdpartyUrl;
                    $movie['trailerUrl'] = $trailerUrl;
                    $movie['embedTrailerUrl'] = $embedTrailerUrl;
                    $movie['is_episode'] = (isset($movie['is_episode']) && strlen($movie['is_episode']) > 0) ? $movie['is_episode'] : 0;
                    //end trailer part                 
                    //favorite items
                    if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
                        $user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
                        if ($user) {
                            $favourite = Yii::app()->db->createCommand()
                                    ->select('*')
                                    ->from('user_favourite_list')
                                    ->where('user_id=:user_id AND content_id=:content_id AND studio_id=:studio_id AND status=:status', array(':user_id' => $_REQUEST['user_id'], ':content_id' => $movie['id'], ':studio_id' => $this->studio_id, ':status' => 1))
                                    ->queryAll();
                            $movie['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
                        }
                    }
                    $movie['custom_meta_data'] = $this->getCustomMetadata($movie, $translate);
                    $data['code'] = 200;
                    $data['msg'] = $translate['btn_ok'];
                    $data['movie'] = $movie;

                    if (!empty($ppvarr)) {
                        if (intval($is_ppv)) {
                            $data['ppv_pricing'] = $ppvarr;
                        }
                        if (intval($is_advance)) {
                            $data['adv_pricing'] = $ppvarr;
                        }
                        $data['currency'] = $currency->attributes;
                    }
                    $data['comments'] = $comments;
                    if ($studio[0]['rating_activated'] == 1) {
                        $data['rating'] = $this->actionRating($movie_id);
                        $reviews = ContentRating::model()->count('content_id=:content_id AND studio_id=:studio_id AND status=1', array(':content_id' => $movie_id, ':studio_id' => $this->studio_id));
                        $data['review'] = $reviews;
                    }
                    $data['epDetails'] = $EpDetails;
                    if (@$seasons)
                        $data['seasons'] = $seasons;
                } else {
                    $data['code'] = 413;
                    $data['status'] = "Invalid Permalink";
                    $data['msg'] = $translate['content_permalink_invalid'];
                }
            } else {
                $data['code'] = 414;
                $data['status'] = "Invalid Content";
                $data['msg'] = $translate['content_not_available_in_your_country'];
            }
        } else {
            $data['code'] = 414;
            $data['status'] = "Invalid Content";
            $data['msg'] = $translate['content_permalink_required'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        exit;
    }

    /**
     * @method private searchData() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns details of the content
     * @param string $permalink Content type permalink
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * Dont delete actionsearchData_bak() we will use in the future
     */
    public function actionsearchData_bak() {
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            if ($language_id == 20) {
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND M.episode_language_id = ' . $language_id . ') ';
            } else {
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND (IF (M.is_episode = 0, M.episode_language_id = 20 , M.episode_language_id=' . $language_id . '))) ';
            }
            $langcond_en = ' AND (F.language_id = 20 AND M.episode_language_id = 20) ';
            $q = utf8_decode(utf8_encode($_REQUEST['q']));
            $casts = Yii::app()->db->createCommand()
                    ->select('id,parent_id,language_id')
                    ->from('celebrities')
                    ->where('studio_id=:studio_id AND language_id=:language_id AND LOWER(name) LIKE  :search_string ', array(':studio_id' => $this->studio_id, ':search_string' => "%$q%", ':language_id' => $language_id))
                    ->queryAll();
            $cast_ids = $child_list = $alllists = array();
            $inCond = $notin_cond = '';
            if (!empty($casts)) {
                foreach ($casts as $cast) {
                    if ($cast['parent_id'] > 0) {
                        $cast_ids[] = $cast['parent_id'];
                    } else {
                        $cast_ids[] = $cast['id'];
                    }
                }
            }
            if (!empty($cast_ids)) {
                $cast_ids = implode(',', $cast_ids);
                $movieIds = Yii::app()->db->createCommand()
                        ->select('GROUP_CONCAT(DISTINCT mc.movie_id) AS movie_ids')
                        ->from('movie_casts mc')
                        ->where('mc.celebrity_id IN (' . $cast_ids . ') ')
                        ->queryAll();
                if (@$movieIds[0]['movie_ids']) {
                    $movie_ids = $movieIds[0]['movie_ids'];
                    if ($language_id == 20) {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.id IN (' . $movie_ids . ')')
                                ->queryAll();
                    } else {
                        $film = Yii::app()->db->createCommand()
                                ->select('GROUP_CONCAT(DISTINCT f.id) AS movie_ids')
                                ->from('films f')
                                ->where('f.parent_id IN (' . $movie_ids . ') AND language_id=' . $language_id)
                                ->queryAll();
                    }
                    if (@$film[0]['movie_ids']) {
                        $inCond = " OR F.id IN(" . $film[0]['movie_ids'] . ") ";
                    }
                }
            }
            //Build the search query 
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $maxlimit = $limit + 10;
            $forRokuCond = '';
            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                $forRokuCond = " AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) ";
            }
            $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3url = $bucketInfo['s3url'];
            $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
            $signedFolderPath = $folderPath['signedFolderPath'];
            $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";

            $permalink = Yii::app()->getBaseUrl(TRUE) . '/';
            /* $command = Yii::app()->db->createCommand()
              ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration')
              ->from('movie_streams M,films F')
              ->where('M.movie_id = F.id AND  M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
              ->order('F.name,M.episode_title')
              ->limit($limit, $offset); */
            if ($_REQUEST['prev_dev']) {
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                        ->order('F.name,M.episode_title')
                        ->limit($maxlimit, $offset);
                $list = $command->queryAll();
                if (!empty($list)) {
                    foreach ($list as $translated) {
                        if ($translated['is_episode'] == 1) {
                            $child_list['movie_stream'][] = $movie_id = $translated['episode_parent_id'];
                        } else {
                            $child_list['films'][] = $movie_id = $translated['search_parent_id'];
                        }
                        $alllists[$movie_id] = $translated;
                    }
                }
                if (isset($child_list['movie_stream']) && !empty($child_list['movie_stream'])) {
                    $notin_cond .= ' AND M.id NOT IN (' . implode($child_list['movie_stream']) . ') ';
                }
                if (isset($child_list['films']) && !empty($child_list['films'])) {
                    $notin_cond .= ' AND F.search_parent_id NOT IN (' . implode($child_list['films']) . ') ';
                }
                if ($language_id != 20) {
                    $command_en = Yii::app()->db->createCommand()
                            ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                            ->from('movie_streams M,films F')
                            ->where('M.movie_id = F.search_parent_id ' . $langcond_en . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond . $notin_cond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                            ->order('F.name,M.episode_title')
                            ->limit($maxlimit, $offset);
                }
            } else {
                /* Add Geo block to listing by manas@muvi.com */
                if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                    $country = $_REQUEST['country'];
                } else {
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $country = $visitor_loc['country'];
                }
                $sql_data1 = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                        . ' FROM movie_streams M,films F WHERE '
                        . ' M.movie_id = F.search_parent_id  ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond;
                $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $this->studio_id . " AND sc.country_code='{$country}'";
                $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $maxlimit . " OFFSET " . $offset;
                $command = Yii::app()->db->createCommand($sql_data);
                $list = $command->queryAll();
                if ($language_id != 20) {
                    if (!empty($list)) {
                        foreach ($list as $translated) {
                            if ($translated['is_episode'] == 1) {
                                $child_list['movie_stream'][] = $translated['episode_parent_id'];
                            } else {
                                $child_list['films'][] = $translated['search_parent_id'];
                            }
                            $alllists[] = $translated;
                        }
                    }

                    if (isset($child_list['movie_stream']) && !empty($child_list['movie_stream'])) {
                        $notin_cond .= ' AND M.id NOT IN (' . implode($child_list['movie_stream']) . ') ';
                    }
                    if (isset($child_list['films']) && !empty($child_list['films'])) {
                        $notin_cond .= ' AND F.search_parent_id NOT IN (' . implode($child_list['films']) . ') ';
                    }
                    $sql_data1_en = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.censor_rating,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                            . ' FROM movie_streams M,films F WHERE '
                            . ' M.movie_id = F.search_parent_id  ' . $langcond_en . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond . $notin_cond;
                    $sql_data_en = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1_en . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $maxlimit . " OFFSET " . $offset;
                    $command_en = Yii::app()->db->createCommand($sql_data_en);
                }
                /* END */
            }
            if ($language_id != 20) {
                $list_en = $command_en->queryAll();
                if (!empty($list_en)) {
                    foreach ($list_en as $original) {
                        if ($original['is_episode'] == 1) {
                            if (!in_array($original['movie_stream_id'], @$child_list['movie_stream'])) {
                                $alllists[] = $original;
                            }
                        } else {
                            if (!in_array($original['search_parent_id'], @$child_list['films'])) {
                                $alllists[] = $original;
                            }
                        }
                    }
                }
                $list = $alllists;
            }
            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                $newList = array();
                foreach ($list AS $k => $v) {
                    /* if($v['content_types_id'] != 4){ 
                      $is_convertedQuery = "SELECT * from movie_streams where is_converted =1 and full_movie !='' and movie_id=" . $v['movie_id'];
                      $count_Is_converted = Yii::app()->db->createCommand($is_convertedQuery)->execute();
                      if ($count_Is_converted > 0) {
                      $newList[] = $list[$k];
                      }
                      } */

                    if ($v['content_types_id'] == 3 && $v['is_episode'] == 0) {
                        $epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1  LIMIT 1";

                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                        //Added by prakash on 1st Feb 2017
                    } else if ($v['content_types_id'] == 4) {
                        $epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                    } else if (($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                        $newList[] = $list[$k];
                    }
                }
                $list = array();
                $list = $newList;
            }
            /*             * ****** 9677  Future live content available in Search API  ******** */
            if ($language_id != 20) {
                $lncode = $langcond_en;
            } else {
                $lncode = $langcond;
            }
            $removefuturedatasql = Yii::app()->db->createCommand()
                    ->select('M.movie_id,M.id AS movie_stream_id,M.is_episode')
                    ->from('movie_streams M,films F')
                    ->where('M.movie_id = F.search_parent_id ' . $lncode . ' AND is_episode=0 AND (M.content_publish_date > NOW()) AND M.studio_id=:studio_id ', array(':studio_id' => $this->studio_id));
            $removefuturedatalist = $removefuturedatasql->queryAll();
            $removefuturedatalist = CHtml::listData($removefuturedatalist, 'movie_id', 'movie_id');

            if ($list) {
                foreach ($list AS $k => $v) {
                    if (!empty($removefuturedatalist) && in_array($v['movie_id'], $removefuturedatalist)) {
                        unset($list[$k]);
                    }
                }
            }
            /*             * ****** END 9677 ******** */
            $item_count = count($list);
            if ($list) {
                $view = array();
                $cnt = 1;
                $movieids = '';
                $stream_ids = '';
                $domainName = $this->getDomainName();
                foreach ($list AS $k => $v) {
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
                    $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                    if ($v['episode_parent_id'] > 0) {
                        $v['movie_stream_id'] = $v['episode_parent_id'];
                        $list[$k]['movie_stream_id'] = $v['episode_parent_id'];
                    }
                    if ($_REQUEST['user_id']) {
                        $studio_id = $this->studio_id;
                        $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id, @$_REQUEST['user_id'], $v['movie_id'], $v['movie_stream_id']);
                        array_push($list[$k]['watch_duration'] = $watch_duration['resume_time']);
                    }
                    if ($v['is_episode']) {
                        $stream_ids .= "'" . $v['movie_stream_id'] . "',";
                    } else {
                        $movieids .= "'" . $v['movie_id'] . "',";
                        if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                            $director = '';
                            $actor = '';
                            $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                            if ($castsarr) {
                                $actor = $castsarr['actor'];
                                $actor = trim($actor, ',');
                                unset($castsarr['actor']);
                                $director = $castsarr['director'];
                                unset($castsarr['director']);
                            }
                            $list[$k]['actor'] = @$actor;
                            $list[$k]['director'] = @$director;
                            $list[$k]['cast_detail'] = @$castsarr;
                        }
                    }
                    if ($v['content_types_id'] == 4) {
                        $liveTV[] = $v['movie_id'];
                    }
                    if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                        if (@$v['thirdparty_url'] != "") {
                            $info = pathinfo(@$v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8") {
                                $list[$k]['movieUrlForTV'] = @$v['thirdparty_url'];
                            }
                        } else {
                            $list[$k]['movieUrlForTV'] = @$v['video_url'];
                        }
                    }
                    if ($v['thirdparty_url'] != "") {
                        $list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                    }
                }
                $viewcount = array();
                $movie_ids = rtrim($movieids, ',');
                $stream_ids = rtrim($stream_ids, ',');


                if ($movie_ids) {
                    $cast = MovieCast::model()->findAll(array("condition" => "movie_id  IN(" . $movie_ids . ')'));
                    foreach ($cast AS $key => $val) {
                        if (in_array('actor', json_decode($val['cast_type']))) {
                            $actor[$val['movie_id']] = @$actor[$val['movie_id']] . $val['celebrity']['name'] . ', ';
                        }
                    }
                    $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movie_ids . ") AND (object_type='films' OR object_type='tvapp') ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
                        foreach ($posterData AS $key => $val) {
                            $size = 'thumb';
                            if ($val['object_type'] == 'films') {
                                $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/' . $size . '/' . urlencode($val['poster_file_name']);
                                $posters[$val['movie_id']] = $posterUrl;
                                if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                }
                            } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                            }
                        }
                    }
                }
                $default_epp = POSTER_URL . '/' . 'no-image-h.png';
                $default_p = POSTER_URL . '/' . 'no-image-a.png';
                if (@$liveTV) {
                    $lcriteria = new CDbCriteria();
                    $lcriteria->select = "movie_id,IF(feed_type=1,'hls','rtmp') feed_type,feed_url,feed_method";
                    $lcriteria->addInCondition("movie_id", $liveTV);
                    $lcriteria->condition = "studio_id = $this->studio_id";
                    $lresult = Livestream::model()->findAll($lcriteria);
                    foreach ($lresult AS $key => $val) {
                        $liveTvs[$val->movie_id]['feed_url'] = $val->feed_url;
                        $liveTvs[$val->movie_id]['feed_type'] = $val->feed_type;
                        $liveTvs[$val->movie_id]['feed_method'] = $val->feed_method;
                    }
                }

                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }

                //Get view status for content other then episode
                $viewStatus = VideoLogs::model()->getViewStatus($movie_ids, $this->studio_id);

                if ($viewStatus) {
                    $viewStatusArr = '';
                    foreach ($viewStatus AS $key => $val) {
                        $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                        $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                //Get view status for Episodes 
                $viewEpisodeStatus = VideoLogs::model()->getEpisodeViewStatus($stream_ids, $this->studio_id);
                if ($viewEpisodeStatus) {
                    $viewEpisodeStatusArr = '';
                    foreach ($viewEpisodeStatus AS $key => $val) {
                        $viewEpisodeStatusArr[$val['video_id']]['viewcount'] = $val['viewcount'];
                        $viewEpisodeStatusArr[$val['video_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                $default_status_view = array('viewcount' => "0", 'uniq_view_count' => "0");
                foreach ($list AS $key => $val) {
                    $casts = '';
                    if ($val['is_episode'] == 1) {
                        if ($language_id != 20 && $val['episode_parent_id'] > 0) {
                            $val['movie_stream_id'] = $val['episode_parent_id'];
                        }
                        //Get Posters for Episode
                        $posterUrlForTv = $this->getPoster($val['movie_stream_id'], 'tvapp', 'original', $this->studio_id);
                        if ($posterUrlForTv != '') {
                            $list[$key]['posterForTv'] = $posterUrlForTv;
                        }
                        $poster_url = $this->getPoster($val['movie_stream_id'], 'moviestream', 'episode', $this->studio_id);
                        $list[$key]['viewStatus'] = @$viewEpisodeStatusArr[$val['movie_stream_id']] ? @$viewEpisodeStatusArr[$val['stream_id']] : $default_status_view;
                    } else {
                        if ($language_id != 20 && $val['parent_id'] > 0) {
                            $val['movie_id'] = $val['parent_id'];
                        }
                        $poster_url = $posters[$val['movie_id']] ? $posters[$val['movie_id']] : $default_p;
                        if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                            $poster_url = str_replace('/thumb/', '/episode/', $poster_url);
                        }
                        $casts = $actor[$val['movie_id']] ? rtrim($actor[$val['movie_id']], ', ') : '';
                        $list[$key]['viewStatus'] = @$viewStatusArr[$val['movie_id']] ? @$viewStatusArr[$val['movie_id']] : $default_status_view;
                        if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                            $list[$key]['posterForTv'] = $postersforTv[$val['movie_id']];
                        }
                    }
                    $list[$key]['poster_url'] = $poster_url;
                    if ($val['content_types_id'] == 4) {
                        $list[$key]['feed_type'] = $liveTvs[$val['movie_id']]['feed_type'];
                        $list[$key]['feed_url'] = $liveTvs[$val['movie_id']]['feed_url'];
                        $list[$key]['feed_method'] = $liveTvs[$val['movie_id']]['feed_method'];
                    }
                    $list[$key]['actor'] = $casts;

                    $arg['studio_id'] = $this->studio_id;
                    $arg['movie_id'] = $v['movie_id'];
                    $arg['season_id'] = 0;
                    $arg['episode_id'] = 0;
                    $arg['content_types_id'] = $v['content_types_id'];

                    $isFreeContent = Yii::app()->common->isFreeContent($arg);
                    $list[$key]['isFreeContent'] = $isFreeContent;

                    //Checking PPV set or advance set or not
                    $is_ppv = $is_advance = 0;
                    if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
                        if ($v['is_converted'] == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
                            $is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
                        }
                    }

                    $list[$key]['is_ppv'] = $is_ppv;
                    $list[$key]['is_advance'] = $is_advance;
                }
                $list[$key]['adDetails'] = array();
                if ($val['rolltype'] == 1) {
                    $list[$key]['adDetails'][] = 0;
                } elseif ($val['rolltype'] == 2) {
                    $roleafterarr = explode(',', $val['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $list[$key]['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($val['rolltype'] == 3) {
                    $list[$key]['adDetails'][] = $this->convertTimetoSec($val['video_duration']);
                }
                /* if(isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku'){
                  $newList = array();
                  foreach ($list AS $k => $v) {
                  if(($content_types_id != 4) && ($v['movieUrlForTV'] != "")){
                  $newList[] = $list[$k];
                  }
                  }
                  $list = array();
                  $list = $newList;
                  } */
            }
            $list = array_slice($list, 0, $limit);
            $searchLogs = new SearchLog();
            $newlisting = array();
            $listings = $list;
            if (!empty($listings)) {
                foreach ($listings as $listnew) {
                    $searchLogs->setIsNewRecord(true);
                    $searchLogs->id = null;
                    $searchLogs->studio_id = $this->studio_id;
                    $searchLogs->search_string = $_REQUEST['q'];
                    $searchLogs->object_category = "content";
                    $searchLogs->object_id = $lists['movie_id'];
                    $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                    $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                    $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                    $searchLogs->save();
                    $content_id = $list_id;
                    $is_episode = $listnew['is_episode'];
                    if ($is_episode == 1) {
                        $content_id = $stream_id;
                    }
                }
            } else {
                $searchLogs->studio_id = $this->studio_id;
                $searchLogs->search_string = $_REQUEST['q'];
                $searchLogs->user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                $searchLogs->created_date = gmdate('Y-m-d H:i:s');
                $searchLogs->ip = $_SERVER['REMOTE_ADDR'];
                $searchLogs->save();
            }
            $data['code'] = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = $item_count;
            $data['Ads'] = $this->getStudioAds();
        } else {
            $data['code'] = 416;
            $data['status'] = "Search Parameter required";
            $data['msg'] = $translate['search_parameter_required'];
        }
        $data['item_count'] = (string) $data['item_count'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionsearchData() {
        if (isset($_REQUEST['q']) && $_REQUEST['q']) {
            require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
            $solr = new Apache_Solr_Service('localhost', '8983', '/solr');
            $studio_id = $this->studio_id;
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            //$q = utf8_decode(utf8_encode($_REQUEST['q']));
            
            $purifier = new CHtmlPurifier();
            $_REQUEST['search_field'] = $purifier->purify($_REQUEST['q']);

            $searck_key = Yii::app()->common->escapeSolrValueWithoutSpace(strtolower(trim($_REQUEST['search_field'])));

            $searck_key_words = array_unique(explode(" ", $searck_key));
            foreach ($searck_key_words as $skey => $sval) {
                if (trim($sval)) {
                    $finalArr[] = trim($sval);
                }
            }

            $add_extra_search = (count($finalArr) > 1) ? implode(" OR name: ", $finalArr) : $finalArr[0];


            $offset = 0; $limit = 10; $key = 0;
            $physicalId = $celphysicalId = array();
            if (isset($_REQUEST['limit']) && $_REQUEST['limit'] > 0) {
                $limit = $_REQUEST['limit'];
            }
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                //$offset = $_REQUEST['offset'];
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            
            $list = Yii::app()->common->searchData($studio_id, $searck_key, $offset, $limit, $language_id, $solr);
            foreach($list['data'] as $k=>$v){
                $list['data'][$k]['details_permalink'] = $v['permalink'];
                $list['data'][$k]['permalink'] = $v['c_permalink'];
                $list['data'][$k]['muvi_uniq_id'] = $v['movie_uniq_id'];
                $list['data'][$k]['poster_url'] = $v['poster'];
                $list['data'][$k]['film_id'] = $v['movie_id'];
                $list['data'][$k]['genre'] = json_encode($v['genres']);
                $list['data'][$k]['censor_rating'] = json_encode($v['censor_rating']);
                $list['data'][$k]['release_date'] = date('Y-m-d', strtotime($v['full_release_date']));
                $list['data'][$k]['language_id'] = $language_id;
            }
            
			foreach($list['data'] as $k=>$v){
                $list['data'][$k]['details_permalink'] = $v['permalink'];
                $list['data'][$k]['permalink'] = $v['c_permalink'];
                $list['data'][$k]['muvi_uniq_id'] = $v['movie_uniq_id'];
                $list['data'][$k]['poster_url'] = $v['poster'];
                $list['data'][$k]['film_id'] = $v['movie_id'];
                $list['data'][$k]['genre'] = json_encode($v['genres']);
                $list['data'][$k]['censor_rating'] = json_encode($v['censor_rating']);
                $list['data'][$k]['release_date'] = date('Y-m-d', strtotime($v['full_release_date']));
                $list['data'][$k]['language_id'] = $language_id;
				$list['data'][$k]['episode_story'] = '';
				$list['data'][$k]['parent_id'] = '';
				$list['data'][$k]['search_parent_id'] = '';
				$list['data'][$k]['episode_parent_id'] = '';
				$list['data'][$k]['episode_language_id'] = '';
				$list['data'][$k]['episode_title'] = '';
				$list['data'][$k]['video_url'] = '';
            }
            if (Yii::app()->general->getStoreLink($studio_id, 1)) {
                $query = "cat:muvikart AND (name: " . $searck_key . "* OR genre: " . $searck_key . "* OR product_sku:" . $searck_key . "* OR format:" . $searck_key . "*) AND sku:" . $studio_id;
                $response = $solr->search($query, $offset, $limit);
                if ($response->getHttpStatus() == 200) {
                    if ($response->response->numFound > 0) {
                        foreach ($response->response->docs as $doc) {
                            $physicalId[] = $doc->content_id;
                            if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                                $logdata[$key]['object_id'] = $doc->content_id;
                                $logdata[$key]['object_category'] = $doc->features;
                                $logdata[$key]['studio_id'] = $doc->sku;
                                $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                                $logdata[$key]['is_physical'] = 1;
                                $logdata[$key]['is_search'] = 1;
                                $key++;
                            }
                        }
                    }
                }
            }
            
            $theme = Studio::model()->find(array('select' => 'parent_theme', 'condition' => 'id=:studio_id', 'params' => array(':studio_id' => $studio_id)));
            
            if ($theme->parent_theme == 'physical') {
                $query = "cat:star AND (name: " . $searck_key . "* ) AND sku:" . $studio_id . " ";
                $response = $solr->search($query, $offset, $limit);
                if ($response->getHttpStatus() == 200) {
                    if ($response->response->numFound > 0) {
                        foreach ($response->response->docs as $doc) {
                            $celphysicalId[] = $doc->content_id;
                            if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                                $logdata[$key]['object_id'] = $doc->content_id;
                                $logdata[$key]['object_category'] = $doc->features;
                                $logdata[$key]['studio_id'] = $doc->sku;
                                $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                                $key++;
                            }
                        }
                    }
                }
                $query = "cat:star AND (name: " . $add_extra_search . " ) AND sku:" . $studio_id . " ";
                $response = $solr->search($query, $offset, $limit);
                if ($response->getHttpStatus() == 200) {
                    if ($response->response->numFound > 0) {
                        foreach ($response->response->docs as $doc) {
                            $celphysicalId[] = $doc->content_id;
                            if (strtolower($doc->name) == strtolower($_REQUEST['search_field'])) {
                                $logdata[$key]['object_id'] = $doc->content_id;
                                $logdata[$key]['object_category'] = $doc->features;
                                $logdata[$key]['studio_id'] = $doc->sku;
                                $logdata[$key]['search_string'] = @$_REQUEST['search_field'];
                                $key++;
                            }
                        }
                    }
                }
                if ($celphysicalId) {
                    $celphysicalId = array_unique($celphysicalId);
                    $dbcon = Yii::app()->db;
                    $data_cast = $dbcon->createCommand('SELECT product_id FROM movie_casts WHERE celebrity_id IN(' . implode(',', $celphysicalId) . ') AND product_id!=0')->queryAll();
                    foreach ($data_cast AS $key => $val) {
                        $physicalId[] = $val['product_id'];
                    }
                }
            }
            
            $physicalId = array_unique($physicalId);
            $studio = Yii::app()->db->createCommand()
                    ->select('default_currency_id')
                    ->from('studios')
                    ->where('id = ' . $studio_id)
                    ->queryRow();
            $default_currency_id = $studio['default_currency_id'];
            foreach ($physicalId as $item) {
                $product = Yii::app()->db->createCommand("SELECT id,name as title,permalink,sale_price,currency_id,is_preorder,status, custom_fields FROM pg_product WHERE  id=" . $item . " AND( publish_date IS NULL OR publish_date <= now() )")->queryROW();
                if ($product) {
                    if (Yii::app()->common->isGeoBlockPGContent($product["id"])) {//Auther manas@muvi.com
                        $product['permalink'] = Yii::app()->getbaseUrl(true) . '/' . $product['permalink'];
                        $product['poster'] = PGProduct::getpgImage($product['id'], 'standard');
                        $product['is_physical'] = 1;
                        $product['name'] = $product["title"];
                        $product['is_search'] = 1;
                        if ($product['poster'] == '/img/no-image.jpg') {
                            $product['poster'] = '/img/No-Image-Vertical.png';
                        }
                        $tempprice = Yii::app()->common->getPGPrices($product['id'], $default_currency_id);
                        if (!empty($tempprice)) {
                            $product['price'] = $tempprice['price'];
                            $product['currency_id'] = $tempprice['currency_id'];
                        } else {
                            $product['price'] = $product['sale_price'];
                            $product['currency_id'] = $product['currency_id'];
                        }

                        $list['data'][] = $product;
                    }
                }
            }

            $data['code'] = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list['data'];
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = count($list['data']);
            $data['Ads'] = $this->getStudioAds();
        } else {
            $data['code'] = 416;
            $data['status'] = "Search Parameter required";
            $data['msg'] = $translate['search_parameter_required'];
        }
        $data['item_count'] = (string) $data['item_count'];
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public CheckGeoBlock() Get IP Address from API request and check if allowed in current country
     * @author RKS<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $ip_address IP Address of the users device
     */
    public function actionCheckGeoBlock() {
        $ip_address = isset($_REQUEST['ip']) ? $_REQUEST['ip'] : 'No IP';
        if ($this->studio_id == 1977) {
            //Keeping the log of API calls for Maxmind
            $log = new ApiRequestIp();
            $log->ip_address = $ip_address;
            $log->studio_id = $this->studio_id;
            $log->save();
        }
        if (isset($_REQUEST) && count($_REQUEST) > 0 && isset($_REQUEST['ip'])) {
            $ip_address = $_REQUEST['ip'];
            $has_error = 0;

            $check_anonymous_ip = Yii::app()->mvsecurity->hasMaxmindSuspiciousIP($this->studio_id);
            if ($check_anonymous_ip == 1) {
                $is_anonymous = Yii::app()->mvsecurity->checkAnonymousIP($ip_address);
                if ($is_anonymous > 0) {
                    $has_error = 1;
                    $data['code'] = 463;
                    $data['status'] = "OK";
                    $data['msg'] = "Your IP looks suspicious. Please contact admin/technical support team at support@vishwammedia.com.";
                }
            }
            if ($has_error == 0) {
                $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
                $country = $visitor_loc['country'];
                $std_countr = StudioCountryRestriction::model();
                $studio_restr = $std_countr->findByAttributes(array('studio_id' => $this->studio_id, 'country_code' => $country));
                if (count($studio_restr) > 0) {
                    $data['code'] = 454;
                    $data['msg'] = "Account is Blocked in this country";
                } else {
                    $data['code'] = 200;
                    $data['msg'] = "Website can be accessed from your country";
                }
                $data['status'] = "OK";
                $data['country'] = $country;
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "failure";
            $data['msg'] = "IP address required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }

    function actiongetCategoryList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $catlists = ContentCategories::model()->findAll('studio_id=:studio_id AND parent_id = 0 ORDER BY id DESC', array(':studio_id' => $studio_id));
        if ($language_id != 20) {
            $translated = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select' => 'parent_id,category_name')), 'parent_id', 'category_name');
            if (!empty($catlists)) {
                foreach ($catlists as $cats) {
                    if (array_key_exists($cats->id, $translated)) {
                        $cats->category_name = $translated[$cats->id];
                    }
                    $cat[] = $cats;
                }
            }
        } else {
            $cat = $catlists;
        }
        if (!empty($cat)) {
            $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
            foreach ($cat as $k => $cat_data) {
                $list[$k]['category_id'] = $cat_data['id'];
                $list[$k]['category_name'] = $cat_data['category_name'];
                $list[$k]['permalink'] = $cat_data['permalink'];
                $list[$k]['category_img_url'] = $this->getPoster($cat_data['id'], 'content_category', 'original', $studio_id);
                $list[$k]['cat_img_size'] = @$cat_img_size;
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['category_list'] = $list;
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "No category added yet";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actiongetGenreList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $customGenre = CustomMetadataField::model()->find("studio_id=:studio_id AND f_id='genre' ", array(':studio_id' => $studio_id));
        if ($customGenre) {
            $allgenre = json_decode($customGenre->f_value, TRUE);


            if ((isset($allgenre[$lang_code])) && is_array($allgenre[$lang_code])) {
                $data['genre_list'] = $allgenre[$lang_code];
            } else if ((isset($allgenre['en'])) && is_array($allgenre['en'])) {
                $data['genre_list'] = $allgenre['en'];
            } else {
                $data['genre_list'] = $allgenre;
            }
        } else {
            //$generes = MovieTag::model()->findAll('studio_id=:studio_id AND taggable_type=:taggable_type ORDER BY name', array(':studio_id' => $studio_id, ':taggable_type' => 1));         
            $criteria = new CDbCriteria();
            $criteria->select = 't.id,t.name';
            $criteria->condition = 't.studio_id=:studio_id AND t.taggable_type=:taggable_type';
            $criteria->params = array(':studio_id' => $studio_id, ':taggable_type' => 1);
            $criteria->group = 't.name';
            $criteria->order = 't.name ASC';
            $generes = MovieTag::model()->findAll($criteria);

            if ($generes) {
                foreach ($generes as $gen_data) {
                    $data['genre_list'][] = $gen_data['name'];
                }
            }
        }
        if (@$data['genre_list']) {
            $data['code'] = 200;
            $data['status'] = "OK";
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "no_genre_found";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /* API to initilise the sdk 
     * By <suraja@muvi.com>,ticket #6247
     * Modified By <manas@muvi.com>,ticket #8655
     */

    public function actioninitialiseSdk() {
        $studio_id = $this->studio_id;
        //check whether the hask key exists for the studio or not ?
        if (isset($_REQUEST['package_name']) && $_REQUEST['package_name'] != "") {
            $apiKeyData = OuathRegistration::model()->find('studio_id=:studio_id AND status=:status', array(':studio_id' => $studio_id, ':status' => '1'));
            if (@$apiKeyData) {
                $hashkey = $apiKeyData->hask_key;
                $packg_name = $apiKeyData->package_name;
                if ($apiKeyData->package_name == $_REQUEST['package_name']) {
                    if (empty(trim($hashkey))) {
                        //generate the new hask key and send it.
                        $key = md5(time());
                        //$hashkey = str_replace(" ", "", $packg_name);
                        //$hask_key=$hashkey.$key;          
                        $apiKeyData->hask_key = $key;
                        $apiKeyData->save();
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['hashkey'] = $key;
                        $data['pkgname'] = $packg_name;
                        $data['server_time'] = date('Y-m-d H:i:s');
                        $data['msg'] = "Hashkey created successfully";
                    } else {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['hashkey'] = $hashkey;
                        $data['pkgname'] = $packg_name;
                        $data['server_time'] = date('Y-m-d H:i:s');
                        $data['msg'] = "Hashkey already exists";
                    }
                } else {
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['hashkey'] = '';
                    $data['pkgname'] = $packg_name;
                    $data['server_time'] = date('Y-m-d H:i:s');
                    $data['msg'] = "Invalid Package name";
                }
            } else {
                $data['code'] = 448;
                $data['status'] = "Error";
                $data['msg'] = "Incorrect OauthToken";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Package name required";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @purpose get the cast and crew
     * @param movie_id Mandatory
     * @Note Applicable only for single part, multipart content. Not for live streams and episodes
     * @Author <ajit@muvi.com>
     */
    function actiongetCelibrity() {
        $lang_code = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
        $studio_id = $this->studio_id;
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $movie_uniqueid = @$_REQUEST['movie_id'];
        if ($movie_uniqueid) {
            $Films = Film::model()->find(array('select' => 'id', 'condition' => 'studio_id=:studio_id and uniq_id=:uniq_id', 'params' => array(':studio_id' => $studio_id, ':uniq_id' => $movie_uniqueid)));
            $movie_id = $Films->id;
            if ($movie_id) {
                /* $sql = "SELECT C.id,C.name,C.birthdate,C.birthplace,C.summary,C.twitterid,C.permalink,C.alias_name,C.meta_title,C.meta_description,C.meta_keywords FROM celebrities C WHERE C.id IN(SELECT celebrity_id FROM movie_casts MC WHERE MC.movie_id = " . $movie_id . ") AND C.studio_id = " . $studio_id;
                  $command = Yii::app()->db->createCommand($sql);
                  $list = $command->queryAll(); */

                $movie_casts = Yii::app()->custom->getCastCrew($movie_id, $studio_id, $language_id);
                if ($movie_casts) {
                    $celebrity = array();
                    foreach ($movie_casts as $key => $val) {
                        //get celebrity type
                        //$cast_name = MovieCast::model()->find(array('select' => 'cast_type', 'condition' => 'celebrity_id=:celebrity_id', 'params' => array(':celebrity_id' => $val['id'])));
                        //$movie_casts_roles = json_decode($cast_name->cast_type,true);
                        $movie_casts_roles = json_decode($val['cast_type'], true);
                        $castRoles = array();
                        foreach ($movie_casts_roles as $movie_casts_role) {
                            $castRole = $movie_casts_role;
                            if ($language_id != 20) {
                                if (strtolower($movie_casts_role) == 'actor' || strtolower($movie_casts_role) == 'actors') {
                                    $castRole = $translate['actors'] == '' ? $castRole : $translate['actors'];
                                } if (strtolower($movie_casts_role) == 'director' || strtolower($movie_casts_role) == 'directors') {
                                    $castRole = $translate['directors'] == '' ? $castRole : $translate['directors'];
                                }
                            }
                            $castRoles[] = $castRole;
                        }
                        $getCastsRole = '["' . implode('","', $castRoles) . '"]';
                        $celebrity[$key]['name'] = $val['name'];
                        $celebrity[$key]['permalink'] = $val['permalink'];
                        $celebrity[$key]['summary'] = $val['summary'];
                        $celebrity[$key]['cast_type'] = $getCastsRole;
                        $img = $this->getPoster($val['celebrity_id'], 'celebrity', 'thumb', $studio_id);
                        if (getimagesize($img) == false) {
                            $img = str_replace('/thumb/', '/medium/', $img);
                        }
                        $celebrity[$key]['celebrity_image'] = $img;
                    }
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['celibrity'] = $celebrity;
                } else {
                    $data['code'] = 448;
                    $data['status'] = "Failure";
                    $data['msg'] = $translate['no_celebrity_found'];
                }
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = $translate['no_record_found'];
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = $translate['required_data_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actiongetEmojis() {
        $emo = Emojis::model()->findAll();
        $device_id = $_REQUEST['deviceId'];
        if (!empty($emo) && !empty($device_id)) {
            foreach ($emo as $key => $val) {
                $set[$key]['code'] = $val['code'];
                $set[$key]['name'] = $val['emoji_name'];
                $code = ltrim($val['code'], ':');
                $name = $code . '.png';
                $path = 'emoji-' . $device_id . 'x/' . $name;
                $set[$key]['url'] = str_replace($name, $path, $val['emoji_url']);
            }
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['emojis'] = $set;
        } else {
            $data['code'] = 448;
            $data['status'] = "Failure";
            $data['msg'] = "Emoji not found.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @purpose get the categories of a content
     * @param authToken,content_category_value Mandatory, lang_code Optional
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetCategoriesOfContent() {
        $lang_code = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : 'en';
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $studio_id = $this->studio_id;
        $sql = "SELECT id,binary_value,category_name,parent_id,language_id FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'binary_value', 'category_name');
        $content_category_value = $_REQUEST['content_category_value'];
        $categories = Yii::app()->Helper->categoryNameFromCommavalues($content_category_value, $contentCategories);
        echo $categories;
        exit;
    }

    /**
     * @purpose get the categories of a content
     * @param authToken,user_id Mandatory
     * @<ajit@muvi.com>
     */
    public function actionMyLibrary() {
        $user_id = $_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        $lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
        $translate = $this->getTransData($lang_code, $studio_id);
        $playerPage = 'player';
        $player_with_details = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'player_with_details');
        if (isset($player_with_details['config_value']) && intval($player_with_details['config_value'])) {
            $playerPage = 'playing';
        }
        if (isset($user_id) && intval($user_id) && $user_id != 0) {
            $transactions = Yii::app()->db->createCommand()
                    ->select('plan_id,subscription_id,ppv_subscription_id,transaction_type')
                    ->from('transactions')
                    ->where('studio_id = ' . $studio_id . ' AND user_id=' . $user_id . ' AND transaction_type IN (2,5,6)')
                    ->order('id DESC')
                    ->queryAll();

            $planModel = new PpvPlans();
            $library = array();
            $newarr = array();
            $plibrary = array();
            foreach ($transactions as $key => $details) {
                $ppv_subscription_id = $details['ppv_subscription_id'];
                $ppv_subscription = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id, 'status'=>1));
                if (Film::model()->exists('id=:id', array(':id' => $ppv_subscription->movie_id))) {
                    if ($details['transaction_type'] == 2) {
                        $film = Film::model()->findByAttributes(array('id' => $ppv_subscription->movie_id));
                        $movie_id = $ppv_subscription->movie_id;
                        $end_date = $ppv_subscription->end_date ? $ppv_subscription->end_date == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $ppv_subscription->end_date : '0000-00-00 00:00:00';
                        $season_id = $ppv_subscription->season_id;
                        $episode_id = $ppv_subscription->episode_id;
                        $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                        $content_types_id = $film->content_types_id;
                        $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                        $movie_names = $movie_name;
                        if ($season_id != 0 && $episode_id != 0) {
                            $movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
                        }
                        if ($season_id == 0 && $episode_id != 0) {
                            $movie_names = $movie_name . " -> " . $episode_name;
                        }
                        if ($season_id != 0 && $episode_id == 0) {
                            $movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
                        }
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($end_date);
                        $statusppv = 'N/A';
                        if ($content_types_id == 1) {
                            if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                                $statusppv = 'Active';
                            }
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                        }
                        if ($content_types_id == 4) {
                            if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                                $statusppv = 'Active';
                            }
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                        }
                        if ($content_types_id == 3) {
                            $datamovie_stream = Yii::app()->db->createCommand()
                                    ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                                    ->from('movie_streams ms ')
                                    ->where('ms.id=' . $episode_id)
                                    ->queryRow();
                            $embed_id = $datamovie_stream['embed_id'];
                            if ($currdatetime <= $expirytime && strtotime($end_date) != '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                            if ($season_id != 0 && $episode_id == 0) {
                                $statusppv = 'Active';
                            }
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                            if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $playerPage . '/' . $film->permalink . '/stream/' . $embed_id;
                            }
                        }
                        if ($statusppv == 'Active') {
                            if ($content_types_id == 2) {
                                $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            } else {
                                $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                            }
                            if ($content_types_id == 3) {
                                if ($episode_id != 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
                                } else if ($season_id != 0 && $episode_id == 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
                                } else {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                                }
                            } else {
                                $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                            }
                            $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie_id, $stm->id);
                            $library[$key]['name'] = $movie_names;
                            $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                            $library[$key]['movie_id'] = $movie_id;
                            $library[$key]['movie_stream_id'] = $stm->id;
                            $library[$key]['is_episode'] = ($episode_id != 0) ? '1' : isset($stm->is_episode) ? $stm->is_episode : 0;
                            $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                            $library[$key]['content_types_id'] = $film->content_types_id;
                            $library[$key]['content_type_id'] = $film->content_type_id;
                            $library[$key]['baseurl'] = $baseurl;
                            $library[$key]['poster_url'] = $poster;
                            $library[$key]['video_duration'] = $stm->video_duration;
                            $library[$key]['watch_duration'] = $watch_duration['resume_time'];
                            $library[$key]['genres'] = json_decode($film->genre);
                            $library[$key]['permalink'] = $film->permalink;
                            $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                            $library[$key]['full_movie'] = $stm->full_movie;
                            $library[$key]['story'] = $film->story;
                            $library[$key]['release_date'] = $film->release_date;
                            $library[$key]['is_converted'] = $stm->is_converted;
                            $library[$key]['isFreeContent'] = 0;
                            $library[$key]['season_id'] = $season_id;
                        }
                    } elseif ($details['transaction_type'] == 6) {

                        $ppv_subscription_id = $details['ppv_subscription_id'];
                        $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
                        $PpvSubscription = Yii::app()->db->createCommand()
                                ->SELECT('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
                                ->from('ppv_subscriptions')
                                ->where('id=' . $ppv_subscription_id)
                                ->queryRow();
                        $end_date = $PpvSubscription['end_date'] ? $PpvSubscription['end_date'] == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $PpvSubscription['end_date'] : '0000-00-00 00:00:00';
                        $start_date = $PpvSubscription['start_date'];
                        $season_id = $PpvSubscription['season_id'];
                        $episode_id = $PpvSubscription['episode_id'];
                        $movie_id = $PpvSubscription['movie_id'];
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($end_date);
                        $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                        $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                        $film = Film::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $movie_id)));
                        $statusvoucher = 'N/A';
                        if ($currdatetime <= $expirytime && $end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                            $statusvoucher = 'Active';
                        }
                        if ($end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                            $statusvoucher = 'Active';
                        }
                        if ($end_date == '0000-00-00 00:00:00') {
                            $statusvoucher = 'Active';
                        }
                        if ($statusvoucher == 'Active') {
                            $datamovie_stream = Yii::app()->db->createCommand()
                                    ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                                    ->from('movie_streams ms ')
                                    ->where('ms.id=' . $episode_id)
                                    ->queryRow();
                            $embed_id = $datamovie_stream['embed_id'];
                            $vmovie_names = $movie_name;

                            if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                                $vmovie_names = $movie_name;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                                $vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '/stream/' . $embed_id;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                                $vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
                            }
                            if ($film->content_types_id == 2) {
                                $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            } else {
                                $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                            }

                            if ($film->content_types_id == 3) {
                                if ($episode_id != 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
                                } else if ($season_id != 0 && $episode_id == 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
                                } else {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                                }
                            } else {
                                $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                            }
                            $watch_duration = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie_id, $stm->id);
                            $library[$key]['name'] = $vmovie_names;
                            $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                            $library[$key]['movie_id'] = $movie_id;
                            $library[$key]['movie_stream_id'] = $stm->id;
                            $library[$key]['is_episode'] = isset($datamovie_stream['is_episode']) ? $datamovie_stream['is_episode'] : 0;
                            $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                            $library[$key]['content_types_id'] = $film->content_types_id;
                            $library[$key]['content_type_id'] = $film->content_type_id;
                            $library[$key]['baseurl'] = $baseurl;
                            $library[$key]['poster_url'] = $poster;
                            $library[$key]['video_duration'] = $stm->video_duration;
                            $library[$key]['watch_duration'] = $watch_duration['resume_time'];
                            $library[$key]['genres'] = json_decode($film->genre);
                            $library[$key]['permalink'] = $film->permalink;
                            $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                            $library[$key]['full_movie'] = $stm->full_movie;
                            $library[$key]['story'] = $film->story;
                            $library[$key]['release_date'] = $film->release_date;
                            $library[$key]['is_converted'] = $stm->is_converted;
                            $library[$key]['isFreeContent'] = 0;
                            $library[$key]['season_id'] = $season_id;
                        }
                    }
                }
            if($details['transaction_type']==5){                    
                    $ppvplanid = PpvSubscription::model()->find(array('select'=>'timeframe_id','condition' => 'id=:id','params' => array(':id' =>$ppv_subscription_id)))->timeframe_id;                    
                    $ppvtimeframes = Ppvtimeframes::model()->find(array('select'=>'validity_days','condition' => 'id=:id','params' => array(':id' =>$ppvplanid)))->validity_days;
                    $ppvbundlesmovieid = Yii::app()->db->createCommand()
                        ->SELECT('content_id')
                        ->from('ppv_advance_content')
                        ->where('ppv_plan_id='.$details['plan_id'])
                        ->queryAll();
                    $expirydate= date('M d, Y', strtotime($transaction_dates. ' + '.$ppvtimeframes.' days'));
                    $currdatetime=strtotime(date('y-m-d'));
                    $expirytime=strtotime($transaction_dates. ' + '.$ppvtimeframes.' days');
                    if($currdatetime<=$expirytime){
                        foreach($ppvbundlesmovieid as $keyb=>$contentid){
                            $film = Film::model()->find(array('condition' => 'id=:id','params' => array(':id' =>$contentid['content_id'])));
                            if($film->content_types_id == 2){
                               $poster = $this->getPoster($contentid['content_id'], 'films', 'episode', $studio_id);
                            }else{
                               $poster = $this->getPoster($contentid['content_id'], 'films', 'standard', $studio_id);
							}
                        $stm = movieStreams::model()->findByAttributes(array('movie_id'=>$contentid['content_id']));
                        
                        $plibrary[$keyb]['name'] = $film->name;
                        $plibrary[$keyb]['movie_stream_uniq_id'] = $stm->embed_id;
                        $plibrary[$keyb]['movie_id'] = $contentid['content_id'];
                        $plibrary[$keyb]['movie_stream_id'] = $stm->id;
                        $plibrary[$keyb]['is_episode'] = isset($stm->is_episode)?$stm->is_episode:0;
                        $plibrary[$keyb]['muvi_uniq_id'] = $film->uniq_id;
                        $plibrary[$keyb]['content_types_id'] = $film->content_types_id;
                        $plibrary[$keyb]['content_type_id'] = $film->content_type_id;
                        $plibrary[$keyb]['baseurl'] = $baseurl;
                        $plibrary[$keyb]['poster_url'] = $poster;
                        $plibrary[$keyb]['video_duration'] = $stm->video_duration;   
                        $plibrary[$keyb]['genres'] = json_decode($film->genre);
                        $plibrary[$keyb]['permalink'] = $film->permalink;
                        $plibrary[$keyb]['ppv_plan_id'] = $film->ppv_plan_id;
                        $plibrary[$keyb]['full_movie'] = $stm->full_movie;
                        $plibrary[$keyb]['story'] = $film->story;;
                        $plibrary[$keyb]['release_date'] = $film->release_date;
                        $plibrary[$keyb]['is_converted'] = $stm->is_converted;
                        $plibrary[$keyb]['isFreeContent'] = 0;
                        $plibrary[$keyb]['season_id'] = $stm->series_number;
                    }                 
                    }                    
                }            
            
        }
            $newarr = array_merge($library,$plibrary);
            if ($newarr) {
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['mylibrary'] = $newarr;
            } else {
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = $translate['content_not_found'];
            }
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = $translate['user_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /*
     * @purpose for filter custom manage metadata 
     * @author Sweta<sweta@muvi.com>
     */

    public function actionGetCustomFilter() {
        $lists = array();
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $command = Yii::app()->db->createCommand()
                ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value as options')
                ->from('custom_metadata_field f')
                ->where('f.f_type IN(2,3) AND f.studio_id=' . $studio_id . ' ORDER BY id ASC');
        $filterdata = $command->queryAll();
        if (!empty($filterdata)) {
            foreach ($filterdata as $listdata) {
                $options = json_decode($listdata['options'], TRUE);
                if ((isset($options[$lang_code])) && is_array($options[$lang_code])) {
                    $options = $options[$lang_code];
                } else if ((isset($options['en'])) && is_array($options['en'])) {
                    $options = $options['en'];
                } else {
                    $options = $options;
                }
                $listdata['f_display_name'] = isset($translate[$listdata['f_name']]) ? $translate[$listdata['f_name']] : $listdata['f_display_name'];
                $listdata['options'] = $options;
                $lists[] = $listdata;
            }
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['filter_list'] = $lists;
        } else {
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
        }
        echo json_encode($data);
        exit;
    }

    public function actionDeleteContent() {
        $lang_code = $_REQUEST['lang_code'];
        $studio_id = $this->studio_id;
        $translate = $this->getTransData($lang_code, $studio_id);
        if ((isset($_REQUEST['playlist_id']) && ($_REQUEST['playlist_id'] != '')) && (isset($_REQUEST['user_id']) && ($_REQUEST['user_id'] != ''))) {
            $content_id = $_REQUEST['content_id'];
            $playlist_id = $_REQUEST['playlist_id'];
            $user_id = $_REQUEST['user_id'];
            $playlist = UserPlaylist::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'content_id' => $content_id, 'playlist_id' => $playlist_id));
            if (!empty($playlist)) {
                $params = array(':playlist_id' => $playlist_id, ':user_id' => $user_id, ':studio_id' => $studio_id, ':content_id' => $content_id);
                $userList = UserPlaylist::model()->deleteAll('content_id = :content_id AND playlist_id = :playlist_id AND studio_id = :studio_id AND user_id = :user_id', $params);
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = $translate['content_remove_playlist'];
            } else {
                $res['code'] = 405;
                $res['status'] = "Failure";
                $res['msg'] = 'No content found';
            }
        } else {
            $res['code'] = 407;
            $res['status'] = "Failure";
            $res['msg'] = 'Invalid data';
        }
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose for cast listing page 
     * @author Sweta<sweta@muvi.com>
     */

    public function actionGetCastList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $studio_id);
        $command = Yii::app()->db->createCommand()
                ->select('c.id,c.name,c.summary,c.permalink')
                ->from('celebrities c')
                ->where("c.parent_id='0' AND c.studio_id='$studio_id' ORDER BY id ASC");
        $celeblistt = $command->queryAll();
        if ($language_id != 20) {
            $translated = CHtml::listData(Celebrity::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select' => 'parent_id,name')), 'parent_id', 'name');
            if (!empty($celeblistt)) {
                foreach ($celeblistt as $castlist) {
                if (array_key_exists($castlist['id'], $translated)) {
                        $castlist['name'] = $translated[$castlist['id']];
                    }
                    $celeblist[] = $castlist;
                }
            }
        }else {
            $celeblist = $celeblistt;
        }
        foreach ($celeblist as $key => $celeb) {
            $celeb_image = $this->getPoster($celeb['id'], 'celebrity');
            $celeblist[$key]['content_id'] = $celeb['id'];
            $celeblist[$key]['celeb_image'] = $celeb_image;
            $celeblist[$key]['roles'] = Yii::app()->custom->getCastRole($celeb['id'], $translate);
        }
        if ($celeblist) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['cast_list'] = $celeblist;
        } else {
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
        }
        echo json_encode($data);
        exit;
    }

    /** author:suraja@muvi.com
     *  use:for updating the chat stream details
     *
     */
    public function actionUpdateUserChatDeatils() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['chatbanstatus'] = 2; //when no data avaialable
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['userid']) && is_numeric($_REQUEST['userid']) && $_REQUEST['userid'] != '') {
            //$getuser = SdkUser::model()->findByAttributes(array('id' => $_REQUEST['userid'], 'studio_id' => $this->studio_id));
            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['userid']));
            $getuser = $command->queryRow();
            if (isset($getuser) && count($getuser) > 0) {

                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid'] != '') {
                    $movie = Film::model()->with(array("live_stream" => array("id")))->find("t.id=" . $_REQUEST['movieid']);
                    // echo $command1->live_stream[0]->id;;exit;
                    if (isset($movie) && count($movie) > 0) {
                        $stream = Yii::app()->db->createCommand()
                                ->select('id,is_banned')
                                ->from('chat_stream')
                                ->where('sdkuser_id=:userid and movie_id=:movie_id and stream_id=:stream_id', array(':movie_id' => $_REQUEST['movieid'], ':userid' => $_REQUEST['userid'], ':stream_id' => $movie->live_stream[0]->id));

                        $getstream = $stream->queryRow();
                        if ($getstream['id']) {
                            $chatstream = ChatStream::model()->findByPk($getstream['id']);
                            $chatstream->studio_id = $this->studio_id;
                            $chatstream->ip_address = CHttpRequest::getUserHostAddress();
                        } else {
                            $chatstream = new ChatStream();
                            $chatstream->sdkuser_id = $_REQUEST['userid'];
                            $chatstream->studio_id = $this->studio_id;
                            $chatstream->movie_id = $_REQUEST['movieid'];
                            $chatstream->stream_id = $movie->live_stream[0]->id;
                            $chatstream->ip_address = CHttpRequest::getUserHostAddress();
                            $chatstream->created_date = new CDbExpression("NOW()");
                            $chatstream->save();
                        }

                        $chatid = $chatstream->id;
                        if ($chatid) {
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['chatbanstatus'] = $chatstream->is_banned;
                            $data['msg'] = "Chat details updated";
                        }
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:suraja@muvi.com
     *  use:for updating the chat ban/unban status
     *
     */
    public function actionUpdateChatBanStatus() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['chatbanstatus'] = 2; //when no data avaialable
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['userid']) && is_numeric($_REQUEST['userid']) && $_REQUEST['userid'] != '') {

            $command = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['userid']));
            $getuser = $command->queryRow();
            if (isset($getuser) && count($getuser) > 0) {
                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid'] != '') {
                    $movie = Film::model()->with(array("live_stream" => array("id")))->find("t.id=" . $_REQUEST['movieid']);

                    if (isset($movie) && count($movie) > 0) {
                        if (isset($_REQUEST['devicetype']) && is_numeric($_REQUEST['devicetype']) && isset($_REQUEST['isbanned']) && is_numeric($_REQUEST['isbanned'])) {
                            $stream = Yii::app()->db->createCommand()
                                    ->select('id,is_banned')
                                    ->from('chat_stream')
                                    ->where('sdkuser_id=:userid and movie_id=:movie_id and stream_id=:stream_id', array(':movie_id' => $_REQUEST['movieid'], ':userid' => $_REQUEST['userid'], ':stream_id' => $movie->live_stream[0]->id));

                            $getstream = $stream->queryRow();

                            if ($getstream['id']) {
                                $command = Yii::app()->db->createCommand();
                                $result = $command->update('chat_stream', array(
                                    'device_type' => $_REQUEST['devicetype'],
                                    'is_banned' => $_REQUEST['isbanned']
                                        ), 'movie_id=:movie_id AND sdkuser_id=:sdkuser_id', array(':movie_id' => $_REQUEST['movieid'], ':sdkuser_id' => $_REQUEST['userid']));


                                if ($result) {
                                    $data['code'] = 200;
                                    $data['status'] = "Ok";
                                    $data['chatbanstatus'] = $_REQUEST['isbanned'];
                                    $data['msg'] = "Chat ban status updated";
                                }
                            } else {
                                $data['chatbanstatus'] = $getstream['isbanned'];
                            }
                        }
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /** author:suraja@muvi.com
     *  use: report abuse a chat
     *
     */
    public function actionReportabuseChat() {
        $data['code'] = 467;
        $data['status'] = "Failure";
        $data['msg'] = "Insufficient data";
        if (isset($_REQUEST['reportedby']) && is_numeric($_REQUEST['reportedby']) && isset($_REQUEST['reportedto']) && is_numeric($_REQUEST['reportedto'])) {
            $command1 = Yii::app()->db->createCommand()
                    ->select('id, email, display_name')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['reportedby']));
            $getreportedby = $command1->queryRow();

            $command2 = Yii::app()->db->createCommand()
                    ->select('id, email, display_name')
                    ->from('sdk_users')
                    ->where('id=:userid and studio_id=:studio_id', array(':studio_id' => $this->studio_id, ':userid' => $_REQUEST['reportedto']));
            $getreportedto = $command2->queryRow();

            if (isset($getreportedby) && isset($getreportedto) && count($getreportedby) > 0 && count($getreportedto) > 0) {

                if (isset($_REQUEST['movieid']) && is_numeric($_REQUEST['movieid']) && $_REQUEST['movieid'] != '') {
                    $movie = Film::model()->with(array("live_stream" => array("id")))->find("t.id=" . $_REQUEST['movieid']);
                    if (isset($movie) && count($movie) > 0) {
                        $chatabuse = new ChatReportAbuse();
                        $chatabuse->report_by = $_REQUEST['reportedby'];
                        $chatabuse->reported_to = $_REQUEST['reportedto'];
                        $chatabuse->movie_id = $_REQUEST['movieid'];
                        $chatabuse->stream_id = $movie->live_stream[0]->id;
                        $chatabuse->message = @$_REQUEST['message'];
                        $chatabuse->ip = CHttpRequest::getUserHostAddress();
                        $chatabuse->created_date = new CDbExpression("NOW()");
                        $chatabuse->save();
                        $abuseid = $chatabuse->id;
                        if ($abuseid) {
                            $getStudioAdmin = User::model()->with(array('studio' => array('select' => 'name')))->find('role_id=1 AND is_sdk=1 AND studio_id=:studio_id', array(':studio_id' => $this->studio_id));
                            if (count($getStudioAdmin) > 0) {
                                $req = array(
                                    'name' => $getStudioAdmin->first_name,
                                    'studio_name' => $getStudioAdmin->studio['name'],
                                    'from' => $getreportedby['email'],
                                    'to' => $getStudioAdmin->email,
                                    'reported_by_name' => $getreportedby['display_name'],
                                    'reported_to_name' => $getreportedto['display_name'],
                                    'sent_to' => 'admin',
                                );
                                $adminEmail = Yii::app()->email->reportAbouseChat($req);
                            }
                            $req2 = array(
                                'name' => $getreportedby['display_name'],
                                'studio_name' => $getStudioAdmin->studio['name'],
                                'from' => $getStudioAdmin->email,
                                'to' => $getreportedby['email'],
                                'reported_by_name' => $getreportedby['display_name'],
                                'reported_to_name' => $getreportedto['display_name'],
                                'sent_to' => 'reportedby',
                            );
                            $adminEmail = Yii::app()->email->reportAbouseChat($req2);
                            $data['code'] = 200;
                            $data['status'] = "Ok";
                            $data['msg'] = "Abuse reported successfully";
                        }
                    }
                }
            }
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionGetCastDetail() {
        if (!empty($_REQUEST['permalink'])) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != strtolower('en')) {
                $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
            } else {
                $language_id = 20;
            }
            $celb = Celebrity::model()->find('permalink=:permalink AND studio_id=:studio_id AND(language_id=:language_id OR parent_id=0 AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id=:studio_id AND permalink=:permalink AND language_id=:language_id))', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id, ':language_id' => $language_id));
            if ($celb) {
                if ($celb->parent_id > 0) {
                    $celb->id = $celb->parent_id;
                }
                $celeb_image = $this->getPoster($celb->id, 'celebrity', 'medium', $this->studio_id);
                $custom_data = CastCustomField::model()->getAllCustomData($celb->id, $this->studio_id, $language_id, $translate);
                $data['status'] = 200;
                $data['name'] = $celb->name;
                $data['summary'] = $celb->summary;
                $data['cast_image'] = $celeb_image;
                $data['custom_data'] = $custom_data;

                $cast_sql = "select films.id from films inner join movie_streams on films.id = movie_streams.movie_id inner join movie_casts on films.id = movie_casts.movie_id where movie_casts.celebrity_id = " . $celb->id . " AND (movie_casts.movie_id IS NOT NULL and films.id IS NOT NULL and  movie_streams.studio_id = " . $this->studio_id . " ) GROUP BY films.id,films.release_date ORDER BY films.release_date desc";
                $movies = Yii::app()->db->createCommand($cast_sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
                $data['movieList'] = array();
                if (count($movies) > 0) {
                    $studio_id = $this->studio_id;
                    $page_size = $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                    $offset = 0;
                    if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                        $offset = ($_REQUEST['offset'] - 1) * $limit;
                    }
                    $ids = array();
                    foreach ($movies as $key => $value) {
                        $ids[] = $value->id;
                    }
                    $idsStr = implode(',', $ids);
                    $pdo_cond_arr = array(':studio_id' => $this->studio_id, ':movie_id' => $idsStr);

                    $order = '';
                    $cond = ' ';
                    $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                    $orderby = "  M.last_updated_date DESC ";
                    $neworderby = " P.last_updated_date DESC ";
                    if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
                        $order = $_REQUEST['orderby'];
                        if ($_REQUEST['orderby'] == strtolower('lastupload')) {
                            $orderby = "  M.last_updated_date DESC";
                            $neworderby = " P.last_updated_date DESC ";
                        } else if ($_REQUEST['orderby'] == strtolower('releasedate')) {
                            $orderby = "  F.release_date DESC";
                            $neworderby = " P.release_date DESC ";
                        } else if ($_REQUEST['orderby'] == strtolower('sortasc')) {
                            $orderby = "  F.name ASC";
                            $neworderby = " P.name ASC ";
                        } else if ($_REQUEST['orderby'] == strtolower('sortdesc')) {
                            $orderby = "  F.name DESC";
                            $neworderby = " P.name DESC ";
                        }
                    }
                    if (@$_REQUEST['genre']) {
                        if (is_array($_REQUEST['genre'])) {
                            $cond .= " AND (";
                            foreach ($_REQUEST['genre'] AS $gkey => $gval) {
                                if ($gkey) {
                                    $cond .= " OR ";
                                }
                                $cond .= " (genre LIKE ('%" . $gval . "%'))";
                            }
                            $cond .= " ) ";
                        } else {
                            $cond .= " AND genre LIKE ('%" . $_REQUEST['genre'] . "%')";
                        }
                    }
                    $cond .= ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW()) ) ';
                    if ($_REQUEST['prev_dev']) {
                        $command = Yii::app()->db->createCommand()
                                ->select('SQL_CALC_FOUND_ROWS (0),M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted, M.full_movie')
                                ->from('movie_streams M,films F')
                                ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND F.id IN(:movie_id) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0  ' . $cond, $pdo_cond_arr)
                                ->order($orderby)
                                ->limit($limit, $offset);
                    } else {
                        /* Add Geo block to listing by manas@muvi.com */
                        if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                            $country = $_REQUEST['country'];
                        } else {
                            $visitor_loc = Yii::app()->common->getVisitorLocation();
                            $country = $visitor_loc['country'];
                        }
                        $sql_data1 = "SELECT M.embed_id AS movie_stream_uniq_id,M.movie_id,M.id AS movie_stream_id, M.is_episode,F.uniq_id AS muvi_uniq_id,F.content_type_id, F.ppv_plan_id,F.permalink,F.name,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,M.is_converted,M.last_updated_date"
                                . " FROM movie_streams M,films F WHERE "
                                . "M.movie_id = F.id AND M.studio_id=" . $studio_id . " AND F.id IN ($idsStr) AND is_episode=0 AND F.parent_id=0 AND M.episode_parent_id=0 " . $cond;
                        $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $studio_id . " AND sc.country_code='{$country}'";
                        $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P ";
                        if ($mostviewed) {
                            $sql_data.=" LEFT JOIN (SELECT COUNT(v.movie_id) AS cnt,v.movie_id FROM video_logs v WHERE `studio_id`=" . $studio_id . " GROUP BY v.movie_id ) AS Q ON P.movie_id = Q.movie_id";
                        }
                        $sql_data.=" WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY " . $neworderby . " LIMIT " . $limit . " OFFSET " . $offset;
                        $command = Yii::app()->db->createCommand($sql_data);
                    }
                    $list = $command->queryAll();
                    $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                    $is_payment_gateway_exist = 0;
                    $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    if (isset($payment_gateway) && !empty($payment_gateway)) {
                        $is_payment_gateway_exist = 1;
                    }
                    //Get Posters for the Movies 
                    $movieids = '';
                    $movieList = '';
                    if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                        //Retrive the total counts based on deviceType 
                        $countQuery = Yii::app()->db->createCommand()
                                ->select('COUNT(DISTINCT F.id) AS cnt')
                                ->from('movie_streams M,films F')
                                ->where('M.movie_id = F.id AND M.studio_id=:studio_id AND F.id IN(:movie_id) AND (IF(F.content_types_id =3,is_episode=1, is_episode=0)) AND (F.content_types_id =4 OR (M.is_converted =1 AND M.full_movie !=\'\')) AND F.parent_id=0 AND M.episode_parent_id=0 ' . $cond, $pdo_cond_arr);
                        $itemCount = $countQuery->queryAll();
                        $item_count = @$itemCount[0]['cnt'];

                        $newList = array();
                        foreach ($list AS $k => $v) {
                            if ($v['content_types_id'] == 3) {
                                $epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1 AND episode_parent_id=0 LIMIT 1";
                                $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                                if (count($epData)) {
                                    $newList[] = $list[$k];
                                }
                                //Added by prakash on 1st feb 2017        
                            } else if ($v['content_types_id'] == 4) {
                                $epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
                                $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                                if (count($epData)) {
                                    $newList[] = $list[$k];
                                }
                            } else if (($v['content_types_id'] == 1 || $v['content_types_id'] == 2 ) && ($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                                $newList[] = $list[$k];
                            }
                        }
                        $list = array();
                        $list = $newList;
                    }
                    $domainName = $this->getDomainName();
                    $livestream_content_ids = '';
                    foreach ($list AS $k => $v) {
                        $movieids .="'" . $v['movie_id'] . "',";
                        if ($v['content_types_id'] == 4) {
                            $livestream_content_ids .="'" . $v['movie_id'] . "',";
                        }
                        if ($v['content_types_id'] == 2 || $v['content_types_id'] == 4) {
                            $defaultPoster = POSTER_URL . '/' . 'no-image-h.png';
                        } else {
                            $defaultPoster = POSTER_URL . '/' . 'no-image-a.png';
                        }
                        $movie_id = $v['movie_id'];
                        $v['poster_url'] = $defaultPoster;
                        $list[$k]['poster_url'] = $defaultPoster;
                        $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
                        $is_episode = 0;

                        $arg['studio_id'] = $this->studio_id;
                        $arg['movie_id'] = $v['movie_id'];
                        $arg['season_id'] = 0;
                        $arg['episode_id'] = 0;
                        $arg['content_types_id'] = $v['content_types_id'];

                        $isFreeContent = Yii::app()->common->isFreeContent($arg);
                        $list[$k]['isFreeContent'] = $isFreeContent;
                        $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                        $is_ppv = $is_advance = 0;
                        if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
                            if ($v['is_converted'] == 1) {
                                $ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
                                if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                    $is_ppv = 1;
                                } else {
                                    $ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
                                    $is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
                                }
                            } else {
                                $adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
                                $is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
                            }
                        }

                        if (intval($is_ppv)) {
                            $list[$k]['is_ppv'] = $is_ppv;
                        }
                        if (intval($is_advance)) {
                            $list[$k]['is_advance'] = $is_advance;
                        }

                        if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                            $director = '';
                            $actor = '';
                            $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                            if ($castsarr) {
                                $actor = $castsarr['actor'];
                                $actor = trim($actor, ',');
                                unset($castsarr['actor']);
                                $director = $castsarr['director'];
                                unset($castsarr['director']);
                            }
                            $list[$k]['actor'] = @$actor;
                            $list[$k]['director'] = @$director;
                            $list[$k]['cast_detail'] = @$castsarr;
                        }
                    }

                    $movieids = rtrim($movieids, ',');
                    $livestream_content_ids = rtrim($livestream_content_ids, ',');
                    $liveFeedUrls = array();
                    if ($livestream_content_ids) {
                        $StreamSql = " SELECT feed_url,movie_id,is_online FROM livestream where movie_id IN (" . $livestream_content_ids . ') AND studio_id=' . $this->studio_id . "  ";
                        $streamData = Yii::app()->db->createCommand($StreamSql)->queryAll();
                        foreach ($streamData AS $skey => $sval) {
                            $liveFeedUrls[$sval['movie_id']]['feed_url'] = $sval['feed_url'];
                            $liveFeedUrls[$sval['movie_id']]['is_online'] = $sval['is_online'];
                        }
                    }
                    if ($movieids) {
                        $sql = "SELECT id,name,language,censor_rating,genre,story,parent_id FROM films WHERE parent_id IN (" . $movieids . ") AND studio_id=" . $studio_id . " AND language_id=" . $language_id;
                        $otherlang1 = Yii::app()->db->createCommand($sql)->queryAll();
                        foreach ($otherlang1 as $key => $val) {
                            $otherlang[$val['parent_id']] = $val;
                        }

                        $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movieids . ") AND (object_type='films' OR object_type='tvapp') ";
                        $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                        if ($posterData) {
                            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                            foreach ($posterData AS $key => $val) {
                                $posterUrl = '';
                                if ($val['object_type'] == 'films') {
                                    $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/thumb/' . urlencode($val['poster_file_name']);
                                    $posters[$val['movie_id']] = $posterUrl;
                                    if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                        $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                    }
                                } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                                }
                            }
                        }
                        //Get view count status
                        $viewStatus = VideoLogs::model()->getViewStatus($movieids, $studio_id);
                        if (@$viewStatus) {
                            $viewStatusArr = '';
                            foreach ($viewStatus AS $key => $val) {
                                $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                                $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                            }
                        }
                        foreach ($list as $key => $val) {
                            if (isset($otherlang[$val['movie_id']])) {
                                $list[$key]['name'] = $otherlang[$val['movie_id']]['name'];
                                $list[$key]['story'] = $otherlang[$val['movie_id']]['story'];
                                $list[$key]['genre'] = $otherlang[$val['movie_id']]['genre'];
                                $list[$key]['censor_rating'] = $otherlang[$val['movie_id']]['censor_rating'];
                            }
                            if (isset($posters[$val['movie_id']])) {
                                if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                                    $list[$key]['poster_url_for_tv'] = $postersforTv[$val['movie_id']];
                                }
                                if (($val['content_types_id'] == 2) || ($val['content_types_id'] == 4)) {
                                    $posters[$val['movie_id']] = str_replace('/thumb/', '/episode/', $posters[$val['movie_id']]);
                                }
                                $list[$key]['poster_url'] = $posters[$val['movie_id']];
                            }
                            if (@$viewStatusArr[$val['movie_id']]) {
                                $list[$key]['viewStatus'] = $viewStatusArr[$val['movie_id']];
                            } else {
                                $list[$key]['viewStatus'] = array('viewcount' => "0", 'uniq_view_count' => "0");
                            }
                            if ($val['content_types_id'] == 4) {
                                $list[$key]['feed_url'] = @$liveFeedUrls[$val['movie_id']]['feed_url'];
                                $list[$key]['is_online'] = @$liveFeedUrls[$val['movie_id']]['is_online'];
                            }
                        }
                    }

                    $data['msg'] = $translate['btn_ok'];
                    $data['movieList'] = @$list;
                    if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] && is_numeric($_REQUEST['user_id'])) {
                        $user = SdkUser::model()->countByAttributes(array('id' => $_REQUEST['user_id']));
                        if ($user) {
                            $favourite = Yii::app()->db->createCommand()
                                    ->select('*')
                                    ->from('user_favourite_list')
                                    ->where('user_id=:user_id AND studio_id=:studio_id AND status=:status', array(':user_id' => $_REQUEST['user_id'], ':studio_id' => $this->studio_id, ':status' => 1))
                                    ->queryAll();

                            $data['is_favorite'] = (count($favourite) > 0) ? 1 : 0;
                        }
                    }

                    $data['orderby'] = @$order;
                    $data['item_count'] = @$item_count;
                    $data['limit'] = @$page_size;
                    $data['Ads'] = $this->getStudioAds();
                }
            } else {
                $data['code'] = 464;
                $data['status'] = "Failure";
                $data['msg'] = "Cast permalink is invalid!";
            }
        } else {
            $data['code'] = 465;
            $data['status'] = "Failure";
            $data['msg'] = "Cast permalink required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionGetAllCast() {
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != strtolower('en')) {
            $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
        } else {
            $language_id = 20;
        }
        $studio_id = $this->studio_id;
        $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
        $orderby = " created_date DESC";
        if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
            if ($_REQUEST['orderby'] == strtolower('lastupdate')) {
                $orderby = " last_updated_date DESC";
            } else if ($_REQUEST['orderby'] == strtolower('sortasc')) {
                $orderby = " name ASC";
            } else if ($_REQUEST['orderby'] == strtolower('sortdesc')) {
                $orderby = " name DESC";
            }
        }
        $command = Yii::app()->db->createCommand()
                ->select('name, permalink, summary')
                ->from('celebrities')
                ->where("studio_id=:studio_id AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM celebrities WHERE studio_id={$studio_id} AND language_id={$language_id}))", array(':studio_id' => $studio_id))
                ->order($orderby)
                ->limit($limit, $offset)
                ->queryAll();
        $res['code'] = 200;
        $res['data'] = $command;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @param stream_id, content_id
     * @author Sanjib<support@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionRelatedContent() {
        $studio_id = $this->studio_id;
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] && $_REQUEST['content_stream_id'] && isset($_REQUEST['content_stream_id'])) {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
            $user_id = isset($_REQUEST['user_id']) && trim($_REQUEST['user_id']) > 0 ? $_REQUEST['user_id'] : 0;
            $movies = RelatedContent::model()->findAll('studio_id=:studio_id AND movie_id=:movie_id AND movie_stream_id=:movie_stream_id', array(':studio_id' => $studio_id, ':movie_id' => $_REQUEST['content_id'], ':movie_stream_id' => $_REQUEST['content_stream_id']));
            foreach ($movies as $key => $value) {
                $contentDetails[] = Yii::app()->general->getContentData($value['content_id'], $value['content_type'], array(), $language_id, $studio_id, $user_id, $translate);
            }
            if (!empty($contentDetails)) {
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['msg'] = "Success";
                $data['contentData'] = $contentDetails;
                $data['total_count'] = count($contentDetails);
            } else {
                $data['code'] = 204;
                $data['status'] = 'failure';
                $data['msg'] = 'No data found';
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Error";
            $data['msg'] = "Please provide stream_id and content_id";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private getHybridSearch() Get the list of contents based on its content_type
     * @author Chinmay<chinmay@muvi.com>
     * @return json Returns the list of data in json format
     * @param string $title Content type title
     * @param string $oauthToken Auth token
     * @param string $filter Filter conditions
     * @param int $limit No. of records to be shown in each listing 
     * @param int $offset Offset for the limit default 0
     * 
     */
    public function actiongetHybridSearch() {
        $cond = '';
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $this->studio_id);
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $q = utf8_decode(utf8_encode($_REQUEST['q']));
        //Build the search query 
        $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
        $offset = 0;
        if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
            $offset = ($_REQUEST['offset'] - 1) * $limit;
        }
        $maxlimit = $limit + 10;
        $forRokuCond = '';

        $condArr = array();
        $permalink = Yii::app()->getBaseUrl(TRUE) . '/';
        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
        $bucket = $bucketInfo['bucket_name'];
        $s3url = $bucketInfo['s3url'];
        $folderPath = Yii::app()->common->getFolderPath('', $this->studio_id);
        $signedFolderPath = $folderPath['signedFolderPath'];

        /*  ########### $_REQUEST['type'] = (1=video, 2=Audio, 3=Physical) - (Default = 1) ########### */
        if (!empty($_REQUEST['type']) && $_REQUEST['type'] == 3) {
            $studio_id = $this->studio_id;
            $baseurl = Yii::app()->getBaseUrl(TRUE);
            $permalink = $_REQUEST['permalink'];
            $offset = $_REQUEST['offset'];
            $limit = $_REQUEST['limit'];
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }

            if (isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != '') {
                $_REQUEST['orderby'] = trim($_REQUEST['orderby']);
                $order = $_REQUEST['orderby'];
                if ($_REQUEST['orderby'] == 'releasedate') {
                    $orderby = "  p.release_date DESC";
                } else if ($_REQUEST['orderby'] == 'sortasc') {
                    $orderby = "  p.name ASC";
                } else if ($_REQUEST['orderby'] == 'sortdesc') {
                    $orderby = "  p.name DESC";
                } else if ($_REQUEST['orderby'] == 'pricedesc') {
                    $orderby = "  p.sale_price DESC";
                } else if ($_REQUEST['orderby'] == 'priceasc') {
                    $orderby = "  p.sale_price ASC";
                }
            } else {
                $orderby = "  p.updated_date DESC ";
            }

            $pdo_cond_arr = array(':studio_id' => $this->studio_id);
            $order = '';
            $cond = ' ';
            $pdo_cond = '';

            if (isset($_REQUEST['filter_by']) && $_REQUEST['filter_by'] != '') {
                $_REQUEST['filter_by'] = trim($_REQUEST['filter_by']);
                if ($_REQUEST['filter_by'] == 'exclude_out_of_stock')
                    $cond .= ' AND p.status!=3';
            }
            if (!empty($_REQUEST['q'])) {
                $cond .= ' AND p.name LIKE "%' . addslashes($_REQUEST['q']) . '%" ';
            }


            $cond .= ' AND p.is_deleted!=1 AND ((p.publish_date IS NULL) OR  (UNIX_TIMESTAMP(p.publish_date) = 0) OR (p.publish_date <=NOW()) ) ';
            $command = Yii::app()->db->createCommand()
                    ->select("SQL_CALC_FOUND_ROWS (0), p.*")
                    ->from("pg_product p")
                    ->where("p.studio_id = :studio_id" . $cond, $pdo_cond_arr)
                    ->order($orderby)
                    ->limit($limit, $offset);
            $list = $command->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();

            foreach ($list as $key => $val) {
                $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $image = Yii::app()->db->createCommand("SELECT * FROM pg_product_image WHERE product_id=" . $val['id'])->queryAll();
                if (!empty($image)) {
                    $pgposter = $url . '/system/pgposters/' . $val['id'] . '/standard/' . $image[0]['name'];
                    $pgposteroriginal = $url . '/system/pgposters/' . $val['id'] . '/original/' . $image[0]['name'];
                    if (false === file_get_contents($pgposter)) {
                        $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                    }
                } else {
                    $pgposteroriginal = $pgposter = $baseurl . '/img/no-image.jpg';
                }
                if ($val['currency_id']) {
                    $currency_id = $val['currency_id'];
                } else {
                    $currency = Yii::app()->db->createCommand()
                            ->select("currency_id,price")
                            ->from("pg_multi_currency")
                            ->where("product_id =:product_id and studio_id=:studio_id", array(":product_id" => $val['id'], ':studio_id' => $this->studio_id))
                            ->queryAll();
                    $currency_id = $currency[0]['currency_id'];
                    $list[$key]['sale_price'] = $currency[0]['price'];
                }
                $currencydetails = Yii::app()->db->createCommand()
                        ->select("symbol")
                        ->from("currency")
                        ->where("id =:id", array(":id" => $currency_id))
                        ->queryRow();
                $symbol = $currencydetails['symbol'];
                $list[$key]['currency_symbol'] = $symbol;
                $list[$key]['poster_url'] = $pgposter;
                $list[$key]['poster_original'] = $pgposteroriginal;
            }
            $data['code'] = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = @$item_count;
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        } else {
            if ($language_id == 20) {
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND M.episode_language_id = ' . $language_id . ') ';
            } else {
                $langcond = ' AND (F.language_id = ' . $language_id . ' AND (IF (M.is_episode = 0, M.episode_language_id = 20 , M.episode_language_id=' . $language_id . '))) ';
            }
            $langcond_en = ' AND (F.language_id = 20 AND M.episode_language_id = 20) ';
            if (!empty($_REQUEST['type']) && $_REQUEST['type'] == 2) {
                $inCond = " AND F.content_types_id IN(5,6,8) ";
                $multiParentId = 6;
            } else {
                $inCond = " AND F.content_types_id IN(1,2,3,4) ";
                $multiParentId = 3;
            }
            $video_url = CDN_HTTP . $bucket . "." . $s3url . "/" . $signedFolderPath . "uploads/movie_stream/full_movie/";
            if (isset($_REQUEST['q']) && trim($_REQUEST['q'])) {
                $cond .= " AND (F.name LIKE ('%" . addslashes($_REQUEST['q']) . "%') OR M.episode_title LIKE ('%" . addslashes($_REQUEST['q']) . "%')) ";
            }

            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                if (!empty($_REQUEST['type']) && $_REQUEST['type'] == 2) {
                    $forRokuCond = " AND (F.content_types_id =8 OR (IF( F.content_types_id =6 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) ";
                } else {
                    $forRokuCond = " AND (F.content_types_id =4 OR (IF( F.content_types_id =3 AND M.is_episode=0, M.is_converted =0 , M.is_converted =1 AND M.full_movie !=''))) ";
                }
            }

            if ($_REQUEST['prev_dev']) {
                $command = Yii::app()->db->createCommand()
                        ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=' . $multiParentId . ' AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                        ->from('movie_streams M,films F')
                        ->where('M.movie_id = F.search_parent_id ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string ' . $inCond . ' ) ' . $forRokuCond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                        ->order('F.name,M.episode_title')
                        ->limit($maxlimit, $offset);
                $list = $command->queryAll();
                if (!empty($list)) {
                    foreach ($list as $translated) {
                        if ($translated['is_episode'] == 1) {
                            $child_list['movie_stream'][] = $movie_id = $translated['episode_parent_id'];
                        } else {
                            $child_list['films'][] = $movie_id = $translated['search_parent_id'];
                        }
                        $alllists[$movie_id] = $translated;
                    }
                }
                if (isset($child_list['movie_stream']) && !empty($child_list['movie_stream'])) {
                    $notin_cond .= ' AND M.id NOT IN (' . implode($child_list['movie_stream']) . ') ';
                }
                if (isset($child_list['films']) && !empty($child_list['films'])) {
                    $notin_cond .= ' AND F.search_parent_id NOT IN (' . implode($child_list['films']) . ') ';
                }
                if ($language_id != 20) {
                    $command_en = Yii::app()->db->createCommand()
                            ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url')
                            ->from('movie_streams M,films F')
                            ->where('M.movie_id = F.search_parent_id ' . $langcond_en . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND   M.studio_id=:studio_id AND (F.name LIKE :search_string OR M.episode_title LIKE :search_string) AND F.content_types_id IN(1,3,4) ' . $forRokuCond . $notin_cond, array(':search_string' => "%$q%", ':studio_id' => $this->studio_id))
                            ->order('F.name,M.episode_title')
                            ->limit($maxlimit, $offset);
                }
            } else {
                if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                    $country = $_REQUEST['country'];
                } else {
                    $visitor_loc = Yii::app()->common->getVisitorLocation();
                    $country = $visitor_loc['country'];
                }
                $sql_data1 = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=' . $multiParentId . ' AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                        . ' FROM movie_streams M,films F WHERE '
                        . ' M.movie_id = F.search_parent_id  ' . $langcond . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . $cond . $inCond . $forRokuCond;
                $sql_geo = "SELECT DISTINCT gc.`movie_id` as movieid, gc.`geocategory_id`,sc.`studio_id`,sc.`category_id`,sc.`country_code`,sc.`ip` FROM geoblockcontent gc LEFT JOIN studio_content_restriction as sc ON gc.geocategory_id = sc.category_id AND sc.studio_id = " . $this->studio_id . " AND sc.country_code='{$country}'";
                $sql_data = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1 . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $maxlimit . " OFFSET " . $offset;
                //echo $sql_data; exit;
                $command = Yii::app()->db->createCommand($sql_data);
                $list = $command->queryAll();
                if ($language_id != 20) {
                    if (!empty($list)) {
                        foreach ($list as $translated) {
                            if ($translated['is_episode'] == 1) {
                                $child_list['movie_stream'][] = $translated['episode_parent_id'];
                            } else {
                                $child_list['films'][] = $translated['search_parent_id'];
                            }
                            $alllists[] = $translated;
                        }
                    }

                    if (isset($child_list['movie_stream']) && !empty($child_list['movie_stream'])) {
                        $notin_cond .= ' AND M.id NOT IN (' . implode($child_list['movie_stream']) . ') ';
                    }
                    if (isset($child_list['films']) && !empty($child_list['films'])) {
                        $notin_cond .= ' AND F.search_parent_id NOT IN (' . implode($child_list['films']) . ') ';
                    }
                    $sql_data1_en = 'SELECT F.id as film_id,M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.content_types_id,F.parent_id,F.search_parent_id,F.language_id,M.episode_parent_id,M.episode_language_id,M.episode_title,M.episode_number, IF((M.is_episode!=1 AND F.content_types_id=3 AND M.full_movie != "" AND M.is_converted = 1), \'\',CONCAT(\'' . $video_url . '\',M.id,\'/\',M.full_movie)) AS video_url,CONCAT(\'' . $permalink . '\',F.permalink) AS details_permalink,M.embed_id AS movie_stream_uniq_id,F.uniq_id AS muvi_uniq_id,F.ppv_plan_id, M.rolltype,M.roll_after,M.video_duration,M.thirdparty_url'
                            . ' FROM movie_streams M,films F WHERE '
                            . ' M.movie_id = F.search_parent_id  ' . $langcond_en . ' AND ((M.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(M.content_publish_date) = 0) OR (M.content_publish_date <=NOW())) AND M.studio_id=' . $this->studio_id . ' AND (F.name LIKE \'%' . $q . '%\' OR M.episode_title LIKE \'%' . $q . '%\'' . $inCond . ' ) ' . $forRokuCond . $notin_cond;
                    $sql_data_en = "SELECT SQL_CALC_FOUND_ROWS P.* FROM (SELECT t.*,g.* FROM (" . $sql_data1_en . ") AS t LEFT JOIN (" . $sql_geo . ") AS g ON t.movie_id = g.movieid) AS P WHERE P.geocategory_id IS NULL OR P.country_code='{$country}' ORDER BY P.name,P.episode_title LIMIT " . $maxlimit . " OFFSET " . $offset;
                    $command_en = Yii::app()->db->createCommand($sql_data_en);
                }
            }
            if ($language_id != 20) {
                $list_en = $command_en->queryAll();
                if (!empty($list_en)) {
                    foreach ($list_en as $original) {
                        if ($original['is_episode'] == 1) {
                            if (!in_array($original['movie_stream_id'], @$child_list['movie_stream'])) {
                                $alllists[] = $original;
                            }
                        } else {
                            if (!in_array($original['search_parent_id'], @$child_list['films'])) {
                                $alllists[] = $original;
                            }
                        }
                    }
                }
                $list = $alllists;
            }
            if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                $newList = array();
                foreach ($list AS $k => $v) {
                    if ($v['content_types_id'] == 3 && $v['is_episode'] == 0) {
                        $epSql = " SELECT id FROM movie_streams where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . " AND is_converted=1 AND full_movie !='' AND is_episode=1  LIMIT 1";

                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                        //Added by prakash on 1st Feb 2017
                    } else if ($v['content_types_id'] == 4) {
                        $epSql = " SELECT id FROM livestream where movie_id=" . $v['movie_id'] . ' AND studio_id=' . $this->studio_id . "  LIMIT 1";
                        $epData = Yii::app()->db->createCommand($epSql)->queryAll();
                        if (count($epData)) {
                            $newList[] = $list[$k];
                        }
                    } else if (($v['is_converted'] == 1) && ($v['full_movie'] != '')) {
                        $newList[] = $list[$k];
                    }
                }
                $list = array();
                $list = $newList;
            }
            $item_count = count($list);
            if ($list) {
                $view = array();
                $cnt = 1;
                $movieids = '';
                $stream_ids = '';
                $domainName = $this->getDomainName();
                foreach ($list AS $k => $v) {
                    $list[$k]['is_episode'] = (($v['is_episode']) && strlen($v['is_episode']) > 0) ? $v['is_episode'] : 0;
                    $list[$k]['embeddedUrl'] = $domainName . '/embed/' . $v['movie_stream_uniq_id'];
                    if ($v['episode_parent_id'] > 0) {
                        $v['movie_stream_id'] = $v['episode_parent_id'];
                        $list[$k]['movie_stream_id'] = $v['episode_parent_id'];
                    }
                    if ($v['is_episode']) {
                        $stream_ids .= "'" . $v['movie_stream_id'] . "',";
                    } else {
                        $movieids .= "'" . $v['movie_id'] . "',";
                        if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                            $director = '';
                            $actor = '';
                            $castsarr = $this->getCasts($v['movie_id'], '', $language_id, $this->studio_id);
                            if ($castsarr) {
                                $actor = $castsarr['actor'];
                                $actor = trim($actor, ',');
                                unset($castsarr['actor']);
                                $director = $castsarr['director'];
                                unset($castsarr['director']);
                            }
                            $list[$k]['actor'] = @$actor;
                            $list[$k]['director'] = @$director;
                            $list[$k]['cast_detail'] = @$castsarr;
                        }
                    }
                    if ($v['content_types_id'] == 4) {
                        $liveTV[] = $v['movie_id'];
                    }
                    if (isset($_REQUEST["deviceType"]) && $_REQUEST["deviceType"] == 'roku') {
                        if (@$v['thirdparty_url'] != "") {
                            $info = pathinfo(@$v['thirdparty_url']);
                            if (@$info["extension"] == "m3u8") {
                                $list[$k]['movieUrlForTV'] = @$v['thirdparty_url'];
                            }
                        } else {
                            $list[$k]['movieUrlForTV'] = @$v['video_url'];
                        }
                    }
                    if ($v['thirdparty_url'] != "") {
                        $list[$k]['thirdparty_url'] = $this->getSrcFromThirdPartyUrl($v['thirdparty_url']);
                    }
                }
                $viewcount = array();
                $movie_ids = rtrim($movieids, ',');
                $stream_ids = rtrim($stream_ids, ',');


                if ($movie_ids) {
                    $cast = MovieCast::model()->findAll(array("condition" => "movie_id  IN(" . $movie_ids . ')'));
                    foreach ($cast AS $key => $val) {
                        if (in_array('actor', json_decode($val['cast_type']))) {
                            $actor[$val['movie_id']] = @$actor[$val['movie_id']] . $val['celebrity']['name'] . ', ';
                        }
                    }
                    $psql = "SELECT id,poster_file_name,object_id AS movie_id,is_roku_poster,object_type FROM posters WHERE object_id  IN(" . $movie_ids . ") AND (object_type='films' OR object_type='tvapp') ";
                    $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                    if ($posterData) {
                        $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
                        foreach ($posterData AS $key => $val) {
                            $size = 'thumb';
                            if ($val['object_type'] == 'films') {
                                $posterUrl = $posterCdnUrl . '/system/posters/' . $val['id'] . '/' . $size . '/' . urlencode($val['poster_file_name']);
                                $posters[$val['movie_id']] = $posterUrl;
                                if (($val['is_roku_poster'] == 1) && isset($_REQUEST['deviceType']) && $_REQUEST['deviceType'] == 'roku') {
                                    $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/roku/' . urlencode($val['poster_file_name']);
                                }
                            } else if (($val['object_type'] == 'tvapp') && (@$_REQUEST['deviceType'] == 'roku')) {
                                $postersforTv[$val['movie_id']] = $posterCdnUrl . '/system/posters/' . $val['id'] . '/original/' . urlencode($val['poster_file_name']);
                            }
                        }
                    }
                }
                $default_epp = POSTER_URL . '/' . 'no-image-h.png';
                $default_p = POSTER_URL . '/' . 'no-image-a.png';
                if (@$liveTV) {
                    $lcriteria = new CDbCriteria();
                    $lcriteria->select = "movie_id,IF(feed_type=1,'hls','rtmp') feed_type,feed_url,feed_method";
                    $lcriteria->addInCondition("movie_id", $liveTV);
                    $lcriteria->condition = "studio_id = $this->studio_id";
                    $lresult = Livestream::model()->findAll($lcriteria);
                    foreach ($lresult AS $key => $val) {
                        $liveTvs[$val->movie_id]['feed_url'] = $val->feed_url;
                        $liveTvs[$val->movie_id]['feed_type'] = $val->feed_type;
                        $liveTvs[$val->movie_id]['feed_method'] = $val->feed_method;
                    }
                }

                $is_payment_gateway_exist = 0;
                $payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($payment_gateway) && !empty($payment_gateway)) {
                    $is_payment_gateway_exist = 1;
                }

                //Get view status for content other then episode
                $viewStatus = VideoLogs::model()->getViewStatus($movie_ids, $this->studio_id);

                if ($viewStatus) {
                    $viewStatusArr = '';
                    foreach ($viewStatus AS $key => $val) {
                        $viewStatusArr[$val['movie_id']]['viewcount'] = $val['viewcount'];
                        $viewStatusArr[$val['movie_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                //Get view status for Episodes 
                $viewEpisodeStatus = VideoLogs::model()->getEpisodeViewStatus($stream_ids, $this->studio_id);
                if ($viewEpisodeStatus) {
                    $viewEpisodeStatusArr = '';
                    foreach ($viewEpisodeStatus AS $key => $val) {
                        $viewEpisodeStatusArr[$val['video_id']]['viewcount'] = $val['viewcount'];
                        $viewEpisodeStatusArr[$val['video_id']]['uniq_view_count'] = $val['u_viewcount'];
                    }
                }
                $default_status_view = array('viewcount' => "0", 'uniq_view_count' => "0");
                foreach ($list AS $key => $val) {
                    $casts = '';
                    if ($val['is_episode'] == 1) {
                        if ($language_id != 20 && $val['episode_parent_id'] > 0) {
                            $val['movie_stream_id'] = $val['episode_parent_id'];
                        }
                        //Get Posters for Episode
                        $posterUrlForTv = $this->getPoster($val['movie_stream_id'], 'moviestream', 'roku', $this->studio_id);
                        if ($posterUrlForTv != '') {
                            $list[$key]['posterForTv'] = $posterUrlForTv;
                        }
                        $poster_url = $this->getPoster($val['movie_stream_id'], 'moviestream', 'episode', $this->studio_id);
                        $list[$key]['viewStatus'] = @$viewEpisodeStatusArr[$val['movie_stream_id']] ? @$viewEpisodeStatusArr[$val['stream_id']] : $default_status_view;
                    } else {
                        if ($language_id != 20 && $val['parent_id'] > 0) {
                            $val['movie_id'] = $val['parent_id'];
                        }
                        $poster_url = $posters[$val['movie_id']] ? $posters[$val['movie_id']] : $default_p;
                        if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                            $poster_url = str_replace('/thumb/', '/episode/', $poster_url);
                        }
                        $casts = $actor[$val['movie_id']] ? rtrim($actor[$val['movie_id']], ', ') : '';
                        $list[$key]['viewStatus'] = @$viewStatusArr[$val['movie_id']] ? @$viewStatusArr[$val['movie_id']] : $default_status_view;
                        if (isset($postersforTv[$val['movie_id']]) && $postersforTv[$val['movie_id']] != '') {
                            $list[$key]['posterForTv'] = $postersforTv[$val['movie_id']];
                        }
                    }
                    $list[$key]['poster_url'] = $poster_url;
                    if ($val['content_types_id'] == 4) {
                        $list[$key]['feed_type'] = $liveTvs[$val['movie_id']]['feed_type'];
                        $list[$key]['feed_url'] = $liveTvs[$val['movie_id']]['feed_url'];
                        $list[$key]['feed_method'] = $liveTvs[$val['movie_id']]['feed_method'];
                    }
                    $list[$key]['actor'] = $casts;

                    $arg['studio_id'] = $this->studio_id;
                    $arg['movie_id'] = $v['movie_id'];
                    $arg['season_id'] = 0;
                    $arg['episode_id'] = 0;
                    $arg['content_types_id'] = $v['content_types_id'];

                    $isFreeContent = Yii::app()->common->isFreeContent($arg);
                    $list[$key]['isFreeContent'] = $isFreeContent;

                    //Checking PPV set or advance set or not
                    $is_ppv = $is_advance = 0;
                    if ((intval($isFreeContent) == 0) && ($is_payment_gateway_exist == 1)) {
                        if ($v['is_converted'] == 1) {
                            $ppv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id, 0);
                            if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
                                $is_ppv = 1;
                            } else {
                                $ppv_plan = Yii::app()->common->getContentPaymentType($v['content_types_id'], $v['ppv_plan_id'], $this->studio_id);
                                $is_ppv = (isset($ppv_plan->id) && intval($ppv_plan->id)) ? 1 : 0;
                            }
                        } else {
                            $adv_plan = Yii::app()->common->checkAdvancePurchase($v['movie_id'], $this->studio_id);
                            $is_advance = (isset($adv_plan->id) && intval($adv_plan->id)) ? 1 : 0;
                        }
                    }

                    $list[$key]['is_ppv'] = $is_ppv;
                    $list[$key]['is_advance'] = $is_advance;
                }
                $list[$key]['adDetails'] = array();
                if ($val['rolltype'] == 1) {
                    $list[$key]['adDetails'][] = 0;
                } elseif ($val['rolltype'] == 2) {
                    $roleafterarr = explode(',', $val['roll_after']);
                    foreach ($roleafterarr AS $tkey => $tval) {
                        $list[$key]['adDetails'][] = $this->convertTimetoSec($tval);
                    }
                } elseif ($val['rolltype'] == 3) {
                    $list[$key]['adDetails'][] = $this->convertTimetoSec($val['video_duration']);
                }
            }
            if ($item_count > $limit) {
                $list = array_slice($list, $offset, $limit);
            }
            $data['code'] = 200;
            $data['msg'] = $translate['btn_ok'];
            $data['search'] = $list;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = $item_count;
            $data['Ads'] = $this->getStudioAds();
            $this->setHeader($data['code']);
            echo json_encode($data);
            exit;
        }
    }

    /**
     * @method private hybridMyLibrary() Get the list of contents based on it parent content
     * @param user_id
     * @author Chinu <chinmay@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionhybridMyLibrary() {
        $user_id = $_REQUEST['user_id'];
        $studio_id = $this->studio_id;
        $lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
        $translate = $this->getTransData($lang_code, $studio_id);
        $playerPage = 'player';
        $player_with_details = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'player_with_details');
        if (isset($player_with_details['config_value']) && intval($player_with_details['config_value'])) {
            $playerPage = 'playing';
        }
        if (isset($user_id) && intval($user_id) && $user_id != 0) {
            $transactions = Yii::app()->db->createCommand()
                    ->select('plan_id,subscription_id,ppv_subscription_id,transaction_type')
                    ->from('transactions')
                    ->where('studio_id = ' . $studio_id . ' AND user_id=' . $user_id . ' AND transaction_type IN (2,5,6)')
                    ->order('id DESC')
                    ->queryAll();

            $planModel = new PpvPlans();
            $library = array();
            $newarr = array();
            $free = array();
            foreach ($transactions as $key => $details) {
                $ppv_subscription_id = $details['ppv_subscription_id'];
                $ppv_subscription = PpvSubscription::model()->findByAttributes(array('id' => $ppv_subscription_id));
                if (Film::model()->exists('id=:id', array(':id' => $ppv_subscription->movie_id))) {
                    if ($details['transaction_type'] == 2) {
                        $film = Film::model()->findByAttributes(array('id' => $ppv_subscription->movie_id));
                        $movie_id = $ppv_subscription->movie_id;
                        $end_date = $ppv_subscription->end_date ? $ppv_subscription->end_date == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $ppv_subscription->end_date : '0000-00-00 00:00:00';
                        $season_id = $ppv_subscription->season_id;
                        $episode_id = $ppv_subscription->episode_id;
                        $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                        $content_types_id = $film->content_types_id;
                        $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                        $movie_names = $movie_name;
                        if ($season_id != 0 && $episode_id != 0) {
                            $movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
                        }
                        if ($season_id == 0 && $episode_id != 0) {
                            $movie_names = $movie_name . " -> " . $episode_name;
                        }
                        if ($season_id != 0 && $episode_id == 0) {
                            $movie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
                        }
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($end_date);
                        $statusppv = 'N/A';
                        if ($content_types_id == 1) {
                            if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                                $statusppv = 'Active';
                            }
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                        }
                        if ($content_types_id == 4) {
                            if ($currdatetime <= $expirytime && (strtotime($end_date) != '' || $end_date != '0000-00-00 00:00:00')) {
                                $statusppv = 'Active';
                            }
                            $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                        }
                        if ($content_types_id == 3) {
                            $datamovie_stream = Yii::app()->db->createCommand()
                                    ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                                    ->from('movie_streams ms ')
                                    ->where('ms.id=' . $episode_id)
                                    ->queryRow();
                            $embed_id = $datamovie_stream['embed_id'];
                            if ($currdatetime <= $expirytime && strtotime($end_date) != '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                            if ($season_id != 0 && $episode_id == 0) {
                                $statusppv = 'Active';
                            }
                            if ($end_date == '0000-00-00 00:00:00') {
                                $statusppv = 'Active';
                            }
                            if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $playerPage . '/' . $film->permalink . '/stream/' . $embed_id;
                            }
                        }
                        if ($statusppv == 'Active') {
                            if ($content_types_id == 2) {
                                $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            } else {
                                $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                            }
                            if ($content_types_id == 3) {
                                if ($episode_id != 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
                                } else if ($season_id != 0 && $episode_id == 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
                                } else {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                                }
                            } else {
                                $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                            }
                            $library[$key]['name'] = $movie_names;
                            $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                            $library[$key]['movie_id'] = $movie_id;
                            $library[$key]['movie_stream_id'] = $stm->id;
                            $library[$key]['is_episode'] = ($episode_id != 0) ? '1' : isset($stm->is_episode) ? $stm->is_episode : 0;
                            $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                            $library[$key]['content_types_id'] = $film->content_types_id;
                            $library[$key]['content_type_id'] = $film->content_type_id;
                            $library[$key]['baseurl'] = $baseurl;
                            $library[$key]['poster_url'] = $poster;
                            $library[$key]['video_duration'] = $stm->video_duration;
                            $library[$key]['genres'] = json_decode($film->genre);
                            $library[$key]['permalink'] = $film->permalink;
                            $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                            $library[$key]['full_movie'] = $stm->full_movie;
                            $library[$key]['story'] = $film->story;
                            ;
                            $library[$key]['release_date'] = $film->release_date;
                            $library[$key]['is_converted'] = $stm->is_converted;
                            $library[$key]['isFreeContent'] = 0;
                            $library[$key]['season_id'] = $season_id;
                        }
                    } elseif ($details['transaction_type'] == 5) {
                        $ppvplanid = PpvSubscription::model()->find(array('select' => 'timeframe_id', 'condition' => 'id=:id', 'params' => array(':id' => $ppv_subscription_id)))->timeframe_id;
                        $ppvtimeframes = Ppvtimeframes::model()->find(array('select' => 'validity_days', 'condition' => 'id=:id', 'params' => array(':id' => $ppvplanid)))->validity_days;
                        $ppvbundlesmovieid = Yii::app()->db->createCommand()
                                ->SELECT('content_id')
                                ->from('ppv_advance_content')
                                ->where('ppv_plan_id=' . $details['plan_id'])
                                ->queryAll();
                        $expirydate = date('M d, Y', strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days'));
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($transaction_dates . ' + ' . $ppvtimeframes . ' days');
                        if ($currdatetime <= $expirytime) {
                            foreach ($ppvbundlesmovieid as $keyb => $contentid) {
                                $movie_id = $contentid['content_id'];
                                $film = Film::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $movie_id)));
                                if ($film->content_types_id == 2) {
                                    $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                                } else {
                                    $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                                }
                                $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));

                                $library[$key]['name'] = $film->name;
                                $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                                $library[$key]['movie_id'] = $movie_id;
                                $library[$key]['movie_stream_id'] = $stm->id;
                                $library[$key]['is_episode'] = isset($stm->is_episode) ? $stm->is_episode : 0;
                                $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                                $library[$key]['content_types_id'] = $film->content_types_id;
                                $library[$key]['content_type_id'] = $film->content_type_id;
                                $library[$key]['baseurl'] = $baseurl;
                                $library[$key]['poster_url'] = $poster;
                                $library[$key]['video_duration'] = $stm->video_duration;
                                $library[$key]['genres'] = json_decode($film->genre);
                                $library[$key]['permalink'] = $film->permalink;
                                $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                                $library[$key]['full_movie'] = $stm->full_movie;
                                $library[$key]['story'] = $film->story;
                                ;
                                $library[$key]['release_date'] = $film->release_date;
                                $library[$key]['is_converted'] = $stm->is_converted;
                                $library[$key]['isFreeContent'] = 0;
                                $library[$key]['season_id'] = $stm->series_number;
                            }
                        }
                    } elseif ($details['transaction_type'] == 6) {

                        $ppv_subscription_id = $details['ppv_subscription_id'];
                        $PpvSubscription = Yii::app()->db->createCommand("select end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id from ppv_subscriptions WHERE id=" . $ppv_subscription_id)->queryRow();
                        $PpvSubscription = Yii::app()->db->createCommand()
                                ->SELECT('end_date,start_date,season_id,episode_id,movie_id,ppv_plan_id')
                                ->from('ppv_subscriptions')
                                ->where('id=' . $ppv_subscription_id)
                                ->queryRow();
                        $end_date = $PpvSubscription['end_date'] ? $PpvSubscription['end_date'] == '1970-01-01 00:00:00' ? '0000-00-00 00:00:00' : $PpvSubscription['end_date'] : '0000-00-00 00:00:00';
                        $start_date = $PpvSubscription['start_date'];
                        $season_id = $PpvSubscription['season_id'];
                        $episode_id = $PpvSubscription['episode_id'];
                        $movie_id = $PpvSubscription['movie_id'];
                        $currdatetime = strtotime(date('y-m-d'));
                        $expirytime = strtotime($end_date);
                        $movie_name = isset($movie_id) ? $this->getFilmName($movie_id) : "";
                        $episode_name = isset($episode_id) ? $this->getEpisodeName($episode_id) : "";
                        $film = Film::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $movie_id)));
                        $statusvoucher = 'N/A';
                        if ($currdatetime <= $expirytime && $end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                            $statusvoucher = 'Active';
                        }
                        if ($end_date != '0000-00-00 00:00:00' && strtotime($end_date) != "") {
                            $statusvoucher = 'Active';
                        }
                        if ($end_date == '0000-00-00 00:00:00') {
                            $statusvoucher = 'Active';
                        }
                        if ($statusvoucher == 'Active') {
                            $datamovie_stream = Yii::app()->db->createCommand()
                                    ->SELECT('ms.id AS stream_id,ms.episode_title,ms.episode_number,ms.series_number,ms.embed_id,ms.is_episode,ms.series_number')
                                    ->from('movie_streams ms ')
                                    ->where('ms.id=' . $episode_id)
                                    ->queryRow();
                            $embed_id = $datamovie_stream['embed_id'];
                            $vmovie_names = $movie_name;

                            if ($movie_id != 0 && $season_id == 0 && $episode_id == 0) {
                                $vmovie_names = $movie_name;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id != 0) {
                                $vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id . " ->" . $episode_name;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '/stream/' . $embed_id;
                            }
                            if ($movie_id != 0 && $season_id != 0 && $episode_id == 0) {
                                $vmovie_names = $movie_name . " -> " . $translate['season'] . " " . $season_id;
                                $baseurl = Yii::app()->getBaseUrl(true) . '/' . $film->permalink . '?season=' . $season_id;
                            }
                            if ($film->content_types_id == 2) {
                                $poster = $this->getPoster($movie_id, 'films', 'episode', $studio_id);
                            } else {
                                $poster = $this->getPoster($movie_id, 'films', 'standard', $studio_id);
                            }

                            if ($film->content_types_id == 3) {
                                if ($episode_id != 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('id' => $episode_id));
                                } else if ($season_id != 0 && $episode_id == 0) {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id, 'is_episode' => 0));
                                } else {
                                    $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                                }
                            } else {
                                $stm = movieStreams::model()->findByAttributes(array('movie_id' => $movie_id));
                            }

                            $library[$key]['name'] = $vmovie_names;
                            $library[$key]['movie_stream_uniq_id'] = $stm->embed_id;
                            $library[$key]['movie_id'] = $movie_id;
                            $library[$key]['movie_stream_id'] = $stm->id;
                            $library[$key]['is_episode'] = isset($datamovie_stream['is_episode']) ? $datamovie_stream['is_episode'] : 0;
                            $library[$key]['muvi_uniq_id'] = $film->uniq_id;
                            $library[$key]['content_types_id'] = $film->content_types_id;
                            $library[$key]['content_type_id'] = $film->content_type_id;
                            $library[$key]['baseurl'] = $baseurl;
                            $library[$key]['poster_url'] = $poster;
                            $library[$key]['video_duration'] = $stm->video_duration;
                            $library[$key]['genres'] = json_decode($film->genre);
                            $library[$key]['permalink'] = $film->permalink;
                            $library[$key]['ppv_plan_id'] = $film->ppv_plan_id;
                            $library[$key]['full_movie'] = $stm->full_movie;
                            $library[$key]['story'] = $film->story;
                            ;
                            $library[$key]['release_date'] = $film->release_date;
                            $library[$key]['is_converted'] = $stm->is_converted;
                            $library[$key]['isFreeContent'] = 0;
                            $library[$key]['season_id'] = $season_id;
                        }
                    }
                }
            }
            $newarr = array_merge($library, $free);

            $limit = isset($_REQUEST['limit']) && $_REQUEST['limit'] ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }

            if (!empty($_REQUEST['type']) && $_REQUEST['type'] == 2) {
                $types = array(5, 6, 8);
            } else {
                $types = array(1, 3, 4);
            }
            $finalArr = array();
            foreach ($newarr as $splitArr) {
                if (in_array($splitArr['content_types_id'], $types)) {
                    $finalArr[] = $splitArr;
                }
            }
            $list = $finalArr;
            if (count($finalArr) > $limit) {
                $list = array_slice($finalArr, $offset, $limit);
            }


            $data['code'] = 200;
            $data['status'] = "OK";
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['total_count'] = count($finalArr);
            $data['mylibrary'] = $list;
        } else {
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = $translate['user_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method private hybridMyFavourite() Get the list of contents based on it parent content
     * @param user_id
     * @author Chinu <chinmay@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionhybridMyFavourite() {
        $user_id = $_REQUEST['user_id'];
        if ($user_id) {
            $studio_id = $this->studio_id;
            $lang_code = @$_REQUEST['lang_code'];
            $q = isset($_REQUEST['q']) ? trim($_REQUEST['q']) : '';
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $fav_status = $this->CheckFavouriteEnable($studio_id);
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if (($fav_status != 0)) {
                if ($user_id > 0) {
                    $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($user_id, $studio_id, null, null, $language_id);
                    if (!empty($_REQUEST['type']) && $_REQUEST['type'] == 2) {
                        $types = array(5, 6, 8);
                    } else {
                        $types = array(1, 3, 4);
                    }
                    $finalArr = array();
                    foreach ($favlist['list'] as $splitArr) {
                        if (in_array($splitArr['content_types_id'], $types)) {
                            if (trim($q)) {
                                if (stripos($splitArr['title'], $q) !== false) {
                                    $finalArr[] = $splitArr;
                                }
                            } else {
                                $finalArr[] = $splitArr;
                            }
                        }
                    }
                    $list = '';
                    if (count($finalArr) <= $limit) {
                        $list = $finalArr;
                    } else if (count($finalArr) > $limit) {
                        $list = array_slice($finalArr, $offset, $limit);
                    }
                    $item_count = @$favlist['total'];
                    //$list = @$favlist['list'];
                    $data['status'] = 200;
                    $data['msg'] = 'OK';
                    $data['movieList'] = @$list;
                    $data['item_count'] = count($finalArr);
                    $data['limit'] = @$page_size;
                } else {
                    $data['code'] = 411;
                    $data['status'] = "Failure";
                    $data['msg'] = "User Id not found";
                }
            } else {
                $data['code'] = 412;
                $data['status'] = "Failure";
                $data['msg'] = "Add to favourite is not enable";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "User id required";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function actiongetSubCategoryList() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $category_id = @$_REQUEST['category_id'];
        if ($category_id) {
            $cat_cond = '';
            $category_value = explode(',', $category_id);
            foreach ($category_value as $key => $value) {
                $cat_cond .= "FIND_IN_SET($value,category_value) OR ";
            }
            $cat_cond = rtrim($cat_cond, 'OR ');
            //$language_id = Yii::app()->custom->getLanguage_id($lang_code);
            //$translate   = $this->getTransData($lang_code, $studio_id);
            $cat = ContentSubcategory::model()->findAll('studio_id=:studio_id AND ' . $cat_cond . ' ORDER BY id DESC', array(':studio_id' => $studio_id));
            /* if($language_id !=20){
              $translated = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id), array('select'=>'parent_id,title')),'parent_id','title');
              if(!empty($catlists)){
              foreach($catlists as $cats){
              if (array_key_exists($cats->id, $translated)) {
              $cats->title = $translated[$cats->id];
              }
              $cat[] = $cats;
              }
              }
              }else{
              $cat = $catlists;
              } */
            if (!empty($cat)) {
                //$cat_img_size = Yii::app()->custom->getCatImgSize($studio_id,'subcategory_poster_size');
                foreach ($cat as $k => $cat_data) {
                    $list[$k]['subcategory_id'] = $cat_data['id'];
                    $list[$k]['subcat_name'] = $cat_data['subcat_name'];
                    $list[$k]['permalink'] = $cat_data['permalink'];
                    //$list[$k]['category_img_url'] = $this->getPoster($cat_data['id'], 'content_subcategory','original',$studio_id);
                    //$list[$k]['cat_img_size'] = @$cat_img_size;    
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['sub_category_list'] = $list;
            } else {
                $data['code'] = 448;
                $data['status'] = "Error";
                $data['msg'] = "No Subcategory added yet";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "Please provide corrent category id";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionCheckDownloadContent() {
        if (isset($_REQUEST['vlink']) && $_REQUEST['vlink']) {
            $permaLink = $_REQUEST['vlink'];
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Please provide corrent permalink";
        }
        if ($permaLink) {
            $studio_id = $this->studio_id;
            $movie = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.content_types_id,ms.id AS movie_stream_id,ms.full_movie,ms.is_downloadable')
                    ->from('films f ,movie_streams ms ')
                    ->where('ms.movie_id = f.id AND ((ms.content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(ms.content_publish_date) = 0) OR (ms.content_publish_date <=NOW())) AND  f.permalink=:permalink AND  f.studio_id=:studio_id', array(':permalink' => $permaLink, ':studio_id' => $this->studio_id))
                    ->queryAll();
            if (!empty($movie) && (($movie[0]['is_downloadable'] == 1) || ($movie[0]['is_downloadable'] == 2))) {
                $data['code'] = 200;
                $data['is_download'] = '1';
            } else {
                $data['code'] = 200;
                $data['is_download'] = '0';
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Please provide corrent permalink";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionDownloadContent() {
        if (isset($_REQUEST['vlink']) && $_REQUEST['vlink']) {
            $permaLink = $_REQUEST['vlink'];
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Please provide corrent permalink";
        }
        if ($permaLink) {
            $studio_id = $this->studio_id;
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.content_types_id,f.mapped_id')
                    ->from('films f ')
                    ->where('f.permalink=:permalink AND f.studio_id=:studio_id', array(':permalink' => $permaLink, ':studio_id' => $studio_id));
            $movie = $command->queryRow();
            if ($movie) {
                $stream_id = Yii::app()->common->getStreamId($movie['id']);
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                $fullmovie_path = $this->getFullVideoPath($stream_id, 0, $studio_id);
                $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 0, "", 36000, $studio_id);
                //Yii::app()->usercontent->downloadVideo($fullmovie_path, $movie['name']);
                $res['code'] = 200;
                $res['status'] = "OK";
                $res['file_path'] = $fullmovie_path;
                $res['file_name'] = $movie['name'];
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Please provide corrent permalink";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Please provide corrent permalink";
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
        exit;
    }
        /**
	 * @method public BlockedChatUserList : This function will return list of blocked chat users
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @mantis id:11890
	 * @param String authToken* : Auth token of the studio
	 * @param Integer blocked_by* : user id who blocked
	 * @param String lang_code : User language Code
	 * 
	 */
	public function actionBlockedChatUserList() {
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $this->studio_id);
		$blocked_by = isset($_REQUEST['blocked_by']) && $_REQUEST['blocked_by'] > 0 ? $_REQUEST['blocked_by'] : 0;
		if ($blocked_by) {
            $command = Yii::app()->db->createCommand("SELECT u.*,p.poster_file_name FROM(SELECT cb.id,cb.blocked_to,cb.device_type,cb.block_status,cb.updated_date,su.display_name FROM chat_user_block cb INNER JOIN sdk_users su ON cb.blocked_by!=0 AND cb.blocked_to=su.id AND cb.block_status=1 AND cb.studio_id={$this->studio_id} AND cb.blocked_by=:blocked_by LIMIT 300) as u LEFT JOIN posters p ON u.blocked_to=p.object_id AND p.object_type='profilepicture' ORDER BY u.updated_date DESC")->bindValue(':blocked_by', $blocked_by)->queryAll();
			$user_list = array();
			$cloudfront_url = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
			foreach ($command as $key => $value) {
				$profile_image_name = $value['poster_file_name'];
                $user_id = $value['blocked_to'];
				$profile_picture_path = $this->getChatProfilePicture($user_id, $cloudfront_url, 'thumb', $profile_image_name, $this->studio_id);
				$value['poster_file_name'] = $profile_picture_path;
				$user_list[] = $value;
			}
			$res['code'] = 200;
			$res['status'] = "OK";
			$res['blocked_chat_user_list'] = $user_list;
		} else {
			$res['code'] = 639;
			$res['status'] = "Failure";
			$res['msg'] = $translate['user_id_required']; //"Blocked by whom user id required";
		}
		$this->setHeader($data['code']);
		echo json_encode($res);
		exit;
	}

	/**
	 * @method public BlockUnblockChatUser : This function is used to Block and UnBlock chat user
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @mantis id:11890
	 * @param String authToken* : Auth token of the studio
	 * @param Integer blocked_by* : user id who blocked
	 * @param Integer blocked_to* : user id whom to blocked
	 * @param Integer device_type* : Blocked from which device (1->web,2->android,3->ios,4->roku)
	 * @param Integer block_status : 1->blocked 0->unblock
	 * @param String lang_code : User language Code
	 * 
	 */
	public function actionBlockUnblockChatUser() {
		$lang_code = @$_REQUEST['lang_code'];
		$translate = $this->getTransData($lang_code, $this->studio_id);
		$device_type = isset($_REQUEST['device_type']) && $_REQUEST['device_type'] > 0 && $_REQUEST['device_type'] <= 4 ? $_REQUEST['device_type'] : 0;
		$suc_flag = 0;
		if ($device_type) {
			$blocked_by = isset($_REQUEST['blocked_by']) && $_REQUEST['blocked_by'] > 0 ? $_REQUEST['blocked_by'] : 0;
			if ($blocked_by) {
				$blocked_to = isset($_REQUEST['blocked_to']) && $_REQUEST['blocked_to'] > 0 ? $_REQUEST['blocked_to'] : 0;
				if ($blocked_to) {
					if ($blocked_by != $blocked_to) {
						$block_status = isset($_REQUEST['block_status']) && $_REQUEST['block_status'] > 0 ? 1 : 0;
						$chat_user_block_data = ChatUserBlock::model()->findByAttributes(array(), 'studio_id=:studio_id AND blocked_by=:blocked_by AND blocked_to=:blocked_to', array(':studio_id' => $this->studio_id, ':blocked_by' => $blocked_by, ':blocked_to' => $blocked_to));
						if ($chat_user_block_data) {
							$chat_user_block_data->device_type = $device_type;
							$chat_user_block_data->block_status = $block_status;
							$chat_user_block_data->ip_address = CHttpRequest::getUserHostAddress();
							$chat_user_block_data->updated_date = date('Y-m-d H:i:s');
							$chat_user_block_data->save();
							$suc_flag = 1;
						} else {
							//validate actual blocked by userid is available or not
							$chk_blocked_by_user = SdkUser::model()->countByAttributes(array(), 'studio_id=:studio_id AND id=:blocked_by', array(':studio_id' => $this->studio_id, ':blocked_by' => $blocked_by));
							if ($chk_blocked_by_user) {
								$chk_blocked_to_user = SdkUser::model()->countByAttributes(array(), 'studio_id=:studio_id AND id=:blocked_to', array(':studio_id' => $this->studio_id, ':blocked_to' => $blocked_to));
								if ($chk_blocked_to_user) {
									$chat_user_block_data = new ChatUserBlock();
									$chat_user_block_data->studio_id = $this->studio_id;
									$chat_user_block_data->blocked_by = $blocked_by;
									$chat_user_block_data->blocked_to = $blocked_to;
									$chat_user_block_data->device_type = $device_type;
									$chat_user_block_data->block_status = $block_status;
									$chat_user_block_data->ip_address = CHttpRequest::getUserHostAddress();
									$chat_user_block_data->created_date = date('Y-m-d H:i:s');
									$chat_user_block_data->updated_date = date('Y-m-d H:i:s');
									$chat_user_block_data->save();
									$suc_flag = 1;
								} else {
									$res['code'] = 641;
									$res['status'] = "Failure";
									$res['msg'] = $translate['blocked_to_user_id_not_exist']; //"The user whom to block not exist";        
								}
							} else {
								$res['code'] = 642;
								$res['status'] = "Failure";
								$res['msg'] = $translate['blocked_by_user_id_not_exist']; //"The user who block not exist";
							}
						}
						if ($suc_flag) {
							$res['code'] = 200;
							$res['status'] = "OK";
							$res['msg'] = $block_status ? $translate['chat_user_blocked_succ'] : $translate['chat_user_unblock_succ'];
						}
					} else {
						$res['code'] = 643;
						$res['status'] = "Failure";
						$res['msg'] = $translate['blockedby_notsame_blockedto']; //"Blocked by should not be same with blocked to";
					}
				} else {
					$res['code'] = 639;
					$res['status'] = "Failure";
					$res['msg'] = $translate['user_id_required']; //"who is blocked user id required";
				}
			} else {
				$res['code'] = 639;
				$res['status'] = "Failure";
				$res['msg'] = $translate['user_id_required']; //"Blocked by whom user id required";
			}
		} else {
			$res['code'] = 640;
			$res['status'] = "Failure";
			$res['msg'] = $translate['valid_device_type_missing']; //"Valid device type is missing";
		}
		$this->setHeader($data['code']);
		echo json_encode($res);
		exit;
	}

}
