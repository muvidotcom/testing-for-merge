<?php

trait Web {
    /* By Biswajit das(biswajitdas@muvi.com)(for check whether user login or not) */

    public function actionCheckIfUserLoggedIn() {
        $loginD = new LoginHistory();
        $loginData = $loginD->findByAttributes(array('studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id'], 'device_id' => $_REQUEST['device_id'], 'device_type' => $_REQUEST['device_type']));
        $this->code = 200;
        $this->items['is_login'] = $loginData ? '1' : '0';
    }

    public function actionUpdateGoogleid() {
        if (isset($_REQUEST['user_id']) && isset($_REQUEST['device_id'])) {
            $loginD = new LoginHistory();
            $loginData = $loginD->findByAttributes(array('studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id'], 'device_id' => $_REQUEST['device_id'], 'logout_at' => '0000-00-00 00:00:00'));
            if ($loginData) {
                $loginData->google_id = $_REQUEST['google_id'];
                $loginData->save();
                $this->code = 200;
            } else {
                $this->code = 703;
            }
        } else {
            $this->code = 672;
        }
    }

    public function actionReviews() {
        if ($_REQUEST['content_id']) {
            $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 5;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $ratings = Yii::app()->db->createCommand()
                    ->select(' r.rating, r.review, r.created_date, r.user_id, u.display_name AS user_name ')
                    ->from("content_ratings r")
                    ->leftJoin('sdk_users u', 'r.user_id=u.id')
                    ->where('r.studio_id = :studio_id AND r.content_id=:content_id AND r.status=1', array(':studio_id' => $this->studio_id, ':content_id' => $_REQUEST['content_id']))
                    ->limit($limit)
                    ->offset($offset)
                    ->order('r.created_date DESC')
                    ->queryAll();
            foreach ($ratings as $k => $rating) {
                $ratings[$k]['profile_pic'] = $this->getProfilePicture($rating['user_id'], 'profilepicture');
                $ratings[$k]['content'] = substr($rating['review'], 0, 50);
                $ratings[$k]['date'] = Yii::app()->common->YYMMDD($rating['created_date']);
            }
            $this->code = 200;
            $data['limit'] = $limit;
            $data['offset'] = $offset;
            $data['item_count'] = count($ratings);
            $data['review'] = $ratings;
            $this->items = $data;
        } else {
            $this->code = 607;
        }
    }
	/**
     * @method public socialAuth() It will authenticate the social Login 
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $fb_userId The user id return by the User
     * @param string $email Email return by the app
     * @param String $name Name as returned by the application
     * @return json Json string
     */
    function actionSocialAuth() {
        $req = $_REQUEST;
        $studio_id = $this->studio_id;
        if (@$req['fb_userid']) {
            if (@$req['email']) {
                $userData = SdkUser::model()->find('(email=:email OR fb_userid=:fb_userid) AND studio_id=:studio_id', array(':email' => $req['email'], ':fb_userid' => $req['fb_userid'], ':studio_id' => $studio_id));
                if ($userData && ($userData->is_deleted || !$userData->status)) {
                    $this->code = 753;
                    return true;
                } elseif ($userData) {
                    if ($userData['fb_userid'] != $req['fb_userid']) {
                        $userData->fb_userid = $req['fb_userid'];
                        $userData->save();
                    }  
                    $data = $this->getUserData($userData);
                    if ($userData->add_video_log) {
                        $google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                        $device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                        $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '';
                        $data['login_history_id'] = (int) $this->save_login_history($this->studio_id, $userData->id, $google_id, $device_id, $device_type, CHttpRequest::getUserHostAddress());
                    }
                    
                    $this->code = 200; 
                    $this->items = $data; 
                    return true;
                } else {
                    $email = $res['data']['email'] = @$req['email'];
                    $name = $res['data']['name'] = @$req['name'];
                    $res['data']['fb_userid'] = @$req['fb_userid'];
                    $res['data']['password'] = @$req['password'] ? $req['password'] : '';
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    $gateway_code = '';
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                        $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                    }
                    $res['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                    $res['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                    $res['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];

                    $ret = SdkUser::model()->saveSdkUser($res, $studio_id);
                    if ($ret) {
                        if (@$ret['error']) {
                            $this->code = 613;
                            return true;
                        }
                        // Send welcome email to user
                        Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], (@$ret['is_subscribed'])?'welcome_email_with_subscription':'welcome_email');
                        $data = $this->getUserData($userData);

                        if ($userData->add_video_log) {
                            $google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                            $device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                            $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '';
                            $data['login_history_id'] = (int) $this->save_login_history($this->studio_id, $userData->id, $google_id, $device_id, $device_type, CHttpRequest::getUserHostAddress());
                        }
                        $this->code = 200;
                        $this->items = $data;
                        return true;
                    } else {
                        $this->code = 661;
                        return true;
                    }
                }
            }
            $this->code = 610;
            return true;
        }        
        $this->code = 626;
        return true;
    }
    protected function actionRating($content_id) {
        $rating_value = 0;
        $num_rating = 0;
        $rating = new ContentRating();
        $ratings = $rating->findAllByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $content_id, 'status' => 1), array('order' => 'id desc'));
        foreach ($ratings as $rating) {
            $rating_value+= $rating->rating;
            $num_rating++;
        }
        $rat_val = 0;
        if ($num_rating > 0) {
            $rat_val = ($rating_value / $num_rating);
            $rat_val = round($rat_val, 1);
        }
        return $rat_val;
    }    
    
    /**
     * @method getFbUserstatus() Returns the Facebook user status 
     * @param String $authToken A authToken
     * @param String $fbuser_id Facebook User id
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetFbUserStatus() {
        $userData = SdkUser::model()->find('(fb_userid=:fb_userid) AND studio_id=:studio_id', array(':fb_userid' => @$_REQUEST['fb_userid'], ':studio_id' => $this->studio_id));
        if ($userData && ($userData->is_deleted || !$userData->status)) {
            $this->code = 200;
            return true;
        } elseif ($userData) {            
            $this->items = $this->getUserData($userData);
            $this->code = 200;
            return true;            
        }
        $this->items = array('is_newuser' => 1);
        $this->code = 200;
        return true; 
    }
    
    public function actiongetWebsiteSettings() {
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }      
    
    /**
     * @method public Savereview()
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @return json message of success or failure 
     * @param int $content_id
     * @param int $rating
     * @param string $review_comment 
     * @param int $user_id
     */
    public function actionSavereview() {
        if (isset($_REQUEST['content_id']) && $_REQUEST['content_id'] > 0 && $_REQUEST['user_id'] > 0) {
            $rating = new ContentRating();
            $rate = $rating->countByAttributes(array('studio_id' => $this->studio_id, 'content_id' => $_REQUEST['content_id'], 'user_id' => $_REQUEST['user_id']));
            if ($rate > 0) {
                $this->code = 676;
            } else {
                if (isset($_REQUEST['rating']) && $_REQUEST['rating'] > 0) {
                    $rating->content_id = $_REQUEST['content_id'];
                    $rating->rating = round($_REQUEST['rating'], 1);
                    $rating->review = $_REQUEST['review_comment'];
                    $rating->created_date = new CDbExpression("NOW()");
                    $rating->studio_id = $this->studio_id;
                    $rating->user_ip = Yii::app()->getRequest()->getUserHostAddress();
                    $rating->user_id = $_REQUEST['user_id'];
                    $rating->status = 1;
                    $rating->save();
                    $this->code = 200;
                } else {
                    $this->code = 677;
                }
            }
        } else {
            $this->code = 678;
        }
    }

    /**
     * @method private forgotPassword() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns array of message and response code based on the action success/failure
     * @param string $email	Valid email of the user
     * @param string $oauthToken Auth token
     * 
     */
    public function actionforgotPassword() {
        $this->code = 610;
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $email = trim($_REQUEST['email']);
            $user = SdkUser::model()->find('studio_id = :studio_id AND email = :email', array(':studio_id' => $this->studio_id, ':email' => $email));
            $studio = Studio::model()->findByPk($this->studio_id);
            if ($user) {
                $enc = new bCrypt();
                $user->reset_password_token = $enc->hash($email);
                $user->save();
                $fb_link = $twitter_link = $gplus_link = '';
                $site_url = 'http://' . $studio->domain;
                $logo = '<a href="' . $site_url . '"><img src="' . Yii::app()->common->getLogoFavPath($this->studio_id) . '" /></a>';
                $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
                if ($studio->fb_link != '')
                    $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                if ($studio->tw_link != '')
                    $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                if ($studio->gp_link != '')
                    $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';

                $reset_link = $site_url . '/user/resetpassword?auth=' . $enc->hash($email) . '&email=' . $email;
                $studio_email = Yii::app()->common->getStudioEmail($this->studio_id);

                $template = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id' => $this->studio_id, ':type' => 'forgot_password'));
                if (!$template) {
                    $template = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id' => 0, ':type' => 'forgot_password'));
                }
                //Subject
                $subject = $template['subject'];
                eval("\$subject = \"$subject\";");
                $content = (string) $template["content"];
                $breaks = array("<br />", "<br>", "<br/>");
                $content = htmlentities(str_ireplace($breaks, "\r\n", $content));
                eval("\$content = \"$content\";");
                $content = htmlspecialchars_decode($content);

                $params = array('website_name' => $studio->name,
                    'logo' => $logo,
                    'site_link' => $site_link,
                    'reset_link' => '<a href="' . $reset_link . '">' . $reset_link . '</a>',
                    'username' => $user->display_name,
                    'fb_link' => @$fb_link,
                    'twitter_link' => @$twitter_link,
                    'gplus_link' => @$gplus_link,
                    'supportemail' => $studio_email,
                    'website_address' => $studio->address,
                    'content' => $content
                );

                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/sdk_reset_password_user', array('params' => $params), true);
                $return_param = $this->sendmailViaAmazonsdk($thtml, $subject, $user->email, $studio_email, "", "", "", $studio->name);                //$return_param = $this->mandrilEmail($template_name, $params, $message);
                $this->code = 200;
                return true;
            } else {
                $this->code = 752;
            }
        }
        return true;
    }

    /**
     * @method public GetBannerList() It will return the list of top banners for the studio
     * @param string authToken* ,string lang_code
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerList() {
        $banners = StudioBanner::model()->findAllByAttributes(array('studio_id' => $this->studio_id, 'banner_section' => 6, 'is_published' => 1), array('order' => 'id_seq ASC'));
        if ($banners) {
            $data = array();
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            foreach ($banners AS $key => $banner) {
                $banner_src = $banner->image_name;
                $banner_title = $banner->title;
                $banner_id = $banner->id;
                $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $this->studio_id . "/original/" . urlencode($banner_src);
            }
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $this->studio_id), array('order' => 'id DESC'));
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
            $this->code = 200;
            $this->items = $data;
        } else
            $this->code = 625;
    }

    /**
     * @method public GetBannerSectionList() It will return the list of top banners for the studio
     * @param string $authToken Authentication Token
     * @author Gayadhar<support@muvi.com>
     * @return json Json array
     */
    function actionGetBannerSectionList($nojson = 1) {
        $studio_id = $this->studio_id;
        $sql = "SELECT t.id FROM studios s,templates t WHERE s.parent_theme =t.code AND s.id =" . $studio_id;
        $res = Yii::app()->db->createCommand($sql)->queryRow();
        $template_id = $res['id'];
        $banners = BannerSection::model()->with(array('banners' => array('alias' => 'bn')))->findAll(array('condition' => 't.template_id=' . $template_id . ' and bn.studio_id = ' . $studio_id, 'order' => 'bn.id_seq ASC, bn.id DESC'));
        $banners = $banners[0]->banners;
        $data = array();
        if ($banners) {
            $bnr = new BannerText();
            $banner_text = $bnr->findByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
            $posterCdnUrl = Yii::app()->common->getPosterCloudFrontPath($this->studio_id);
            foreach ($banners AS $key => $banner) {
                $banner_src = urlencode(trim($banner->image_name));
                $data['banner_url'][] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                $data['banners'][$key]['original'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                $data['banners'][$key]['mobile_view'] = $posterCdnUrl . "/system/studio_banner/" . $studio_id . "/mobile_view/" . $banner_src;
            }
            $data['text'] = (isset($banner_text->bannercontent) && $banner_text->bannercontent != '') ? html_entity_decode($banner_text->bannercontent) : '';
            $data['join_btn_txt'] = (isset($banner_text->show_join_btn) && $banner_text->show_join_btn > 0) ? html_entity_decode($banner_text->join_btn_txt) : '';
            $this->code = 200;
        } else {
            $this->code = 625;
        }
        if ($nojson = 1) {
            $this->items = $data;
        } else {
            return $data;
        }
    }

    /**
     * @method public getPorfileDetails() It will fetch the profile details of the logged in user
     * @param string $authToken Authtoken
     * @param string $email
     * @param int $user_id
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetProfileDetails() {
        $code = 634;
        if (@$_REQUEST['email'] || @$_REQUEST['user_id']) {
            $userData = SdkUser::model()->find('studio_id =:studio_id AND status=:status AND (id=:user_id OR email=:email)', array(':studio_id' => $this->studio_id, ':status' => 1, ':user_id' => @$_REQUEST['user_id'], ':email' => @$_REQUEST['email']));
            if ($userData) {
                $this->items = $this->getUserData($userData, true);
                $this->code = 200;
                return true;
            }
            $code = 633;
        }
        $this->code = $code;
        return true;
    }

    /**
     * @method updateUserProfile() Returns the profile details along with profile image
     * @param String $authToken A authToken
     * @param array $data profile data in array format
     * @param array $image optional image info 
     * @author Gayadhar<support@muvi.com>
     * @return Json user details 
     */
    function actionUpdateUserProfile() {
        if (isset($_REQUEST['user_id'])) {
            $user_id = (int) @$_REQUEST['user_id'];
            if ($_FILES && ($_FILES['error'] == 0)) {
                $ret = $this->uploadProfilePics($user_id, $this->studio_id, $_FILES['file']);
            }
            $sdkUsers = SdkUser::model()->findByPk($user_id);
            if ($sdkUsers) {
                if (@$_REQUEST['nick_name'])
                    $sdkUsers->nick_name = $_REQUEST['nick_name'];
                if (@$_REQUEST['name'])
                    $sdkUsers->display_name = $_REQUEST['name'];
                if (@$_REQUEST['password']) {
                    $enc = new bCrypt();
                    $pass = $enc->hash($_REQUEST['password']);
                    $sdkUsers->encrypted_password = $pass;
                }
                $sdkUsers->last_updated_date = new CDbExpression("NOW()");
                $sdkUsers->save();
                Yii::app()->custom->updateCustomFieldValue($user_id, $this->studio_id);
            }
            $this->code = 200;
            $this->items = array('name' => $sdkUsers->display_name, 'email' => $sdkUsers->email, 'nick_name' => $sdkUsers->nick_name, 'profile_image' => $this->getProfilePicture($user_id, 'profilepicture', 'thumb', $this->studio_id));
            return true;
        }
        $this->code = 639;
        return true;
    }

    /**
     * @method public CheckGeoBlock() Get IP Address from API request and check if allowed in current country
     * @author RKS<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $ip_address IP Address of the users device
     */
    public function actionCheckGeoBlock() {
        $ip_address = isset($_REQUEST['ip']) ? $_REQUEST['ip'] : 'No IP';
        if (isset($_REQUEST) && count($_REQUEST) > 0 && isset($_REQUEST['ip'])) {
            $check_anonymous_ip = Yii::app()->mvsecurity->hasMaxmindSuspiciousIP($this->studio_id);
            if ($check_anonymous_ip == 1) {
                $is_anonymous = Yii::app()->mvsecurity->checkAnonymousIP($ip_address);
                if ($is_anonymous > 0) {
                    $this->code = 200;
                    $this->msg_code = 754;
                    return;
                }
            }
            $visitor_loc = Yii::app()->common->getVisitorLocation($ip_address);
            $country = $visitor_loc['country'];
            $std_countr = StudioCountryRestriction::model();
            $studio_restr = $std_countr->findByAttributes(array('studio_id' => $this->studio_id, 'country_code' => $country));
            $this->code = (count($studio_restr) > 0) ? 755 : 200;
            $this->items = $country;
        } else {
            $this->code = 635;
        }
    }

    /**
     * @method public Homepage() It will give section name list
     * Param string authToken* ,string lang_code 
     * @author Prakash<support@muvi.com>
     * @return json object
     */
    public function actionHomepage() {
        $this->items['banner_section_list'] = $this->actiongetBannerSectionList(0);
        $this->items['section_name_list'] = $this->actiongetSectionName(0);
        $this->code = 200;
    }

    /**
     * @method public getSectionName() It will give section name list
     * param string authToken* ,string lang_code 
     * @author Prakash<support@muvi.com>
     * @return json object
     */
    public function actiongetSectionName($nojson = 1) {
        $sections = FeaturedSection::model()->findAll('studio_id=:studio_id AND is_active=:is_active  ORDER BY id_seq', array(':studio_id' => $this->studio_id, ':is_active' => 1));
        $section = array();
        if ($sections) {
            foreach ($sections as $key => $sec) {
                $section[$key]['studio_id'] = $sec->studio_id;
                $section[$key]['language_id'] = $sec->language_id;
                $section[$key]['title'] = $sec->title;
                $section[$key]['section_id'] = $sec->id;
            }
            $this->code = 200;
        } else {
            $this->code = 688;
        }
        if ($nojson == 1) {
            $this->items['section'] = $section;
        } else {
            return $section;
        }
    }

    /**
     * @method public getFeaturedContent() for getting content section_id wise .This function is used in Mobile App end
     * @author Prakash Chandra Nayak rout<support@muvi.com>
     * @param string authToken* , int section_id* ,string country , string lang_code
     * @return json Returns the list of data in json format success or corresponding error code
     */
    public function actiongetFeaturedContent() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $fecontents = Yii::app()->db->createCommand()->select('*')->from('homepage')->where('studio_id=:studio_id AND section_id=:section_id', array(':studio_id' => $this->studio_id, ':section_id' => $_REQUEST['section_id']))->queryAll();
        if ($fecontents) {
            $domainName = $this->getDomainName();
            $restriction = StudioContentRestriction::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
            if (isset($_REQUEST['country']) && $_REQUEST['country']) {
                $country = $_REQUEST['country'];
            } else {
                $visitor_loc = Yii::app()->common->getVisitorLocation();
                $country = $visitor_loc['country'];
            }
            foreach ($fecontents AS $key => $val) {
                $langcontent = Yii::app()->custom->getTranslatedContent($val['movie_id'], $val['is_episode'], $language_id, $studio_id);
                if ($val['is_episode'] == 2) {
                    $standaloneproduct = Yii::app()->db->createCommand("SELECT * FROM pg_product WHERE id = '" . $val['movie_id'] . "'")->queryRow();
                    if ($standaloneproduct) {
                        if (Yii::app()->common->isGeoBlockPGContent($standaloneproduct['id'])) {
                            $short_story = substr(Yii::app()->common->htmlchars_encode_to_html($standaloneproduct['description']), 0, 200);
                            $tempprice = Yii::app()->common->getPGPrices($standaloneproduct['id'], Yii::app()->controller->studio->default_currency_id);
                            if (!empty($tempprice)) {
                                $standaloneproduct['sale_price'] = $tempprice['price'];
                                $standaloneproduct['currency_id'] = $tempprice['currency_id'];
                            }
                            $poster = PGProduct::getpgImage($standaloneproduct['id'], 'standard');
                            $formatted_price = Yii::app()->common->formatPrice($standaloneproduct['sale_price'], $standaloneproduct['currency_id']);
                            $final_content[$key] = array(
                                'movie_id' => $standaloneproduct['id'],
                                'title' => utf8_encode($standaloneproduct['name']),
                                'permalink' => Yii::app()->getbaseUrl(true) . '/' . $standaloneproduct['permalink'],
                                'poster' => $poster,
                                'data_type' => 4,
                                'uniq_id' => $standaloneproduct['uniqid'],
                                'short_story' => utf8_encode($short_story),
                                'price' => $formatted_price,
                                'status' => $standaloneproduct['status'],
                                'is_episode' => 2
                            );
                        }
                    }
                } else {
                    if (Yii::app()->common->isGeoBlockContent(@$val['movie_id'], 0, $studio_id, $country)) {
                        $arg['studio_id'] = $studio_id;
                        $arg['movie_id'] = $val['movie_id'];
                        $arg['season_id'] = 0;
                        $arg['episode_id'] = 0;
                        $arg['content_types_id'] = $val['content_types_id'];
                        $isFreeContent = Yii::app()->common->isFreeContent($arg);
                        $embededurl = $domainName . '/embed/' . $val['embed_id'];
                        if ($val['is_episode'] == 1) {
                            if (array_key_exists($val['movie_id'], $langcontent['episode'])) {
                                $val['episode_title'] = $langcontent['episode'][$val['movie_id']]->episode_title;
                                $val['episode_story'] = $langcontent['episode'][$val['movie_id']]->episode_story;
                            }
                            $cont_name = ($val['episode_title'] != '') ? $val['episode_title'] : "SEASON " . $val['series_number'] . ", EPISODE " . $val['episode_number'];
                            $story = $val['episode_story'];
                            $release = $val['episode_date'];
                            $poster = Yii::app()->controller->getPoster($val['stream_id'], 'moviestream', 'episode', $this->studio_id);
                        } else {
                            if (array_key_exists($val['movie_id'], $langcontent['film'])) {
                                $val['name'] = $langcontent['film'][$val['movie_id']]->name;
                                $val['story'] = $langcontent['film'][$val['movie_id']]->story;
                                $val['genre'] = $langcontent['film'][$val['movie_id']]->genre;
                                $val['censor_rating'] = $langcontent['film'][$val['movie_id']]->censor_rating;
                                $val['language'] = $langcontent['film'][$val['movie_id']]->language;
                            }
                            $cont_name = $val['name'];
                            $story = $val['story'];
                            $release = $val['release_date'];
                            $stream = movieStreams::model()->findByPk($val['stream_id']);
                            if ($val['content_types_id'] == 2 || $val['content_types_id'] == 4) {
                                $poster = Yii::app()->controller->getPoster($val['movie_id'], 'films', 'episode', $this->studio_id);
                            } else {
                                $poster = Yii::app()->controller->getPoster($val['movie_id'], 'films', 'standard', $this->studio_id);
                            }
                        }
                        $geo = GeoblockContent::model()->find('movie_id=:movie_id', array(':movie_id' => $val['movie_id']));
                        //Get view count status
                        $viewStatus = VideoLogs::model()->getViewStatus($val['movie_id'], $studio_id);
                        if ($viewStatus) {
                            $viewStatusArr = array();
                            foreach ($viewStatus AS $valarr) {
                                $viewStatusArr['viewcount'] = $valarr['viewcount'];
                                $viewStatusArr['uniq_view_count'] = $valarr['u_viewcount'];
                            }
                        }
                        if (isset($stream['content_publish_date']) && @$stream['content_publish_date'] && $stream['content_publish_date'] > gmdate("Y-m-d H:i:s")) {
                            
                        } else {
                            $final_content[$key]['is_episode'] = $val['is_episode'];
                            $final_content[$key]['movie_stream_uniq_id'] = $val['embed_id'];
                            $final_content[$key]['movie_id'] = $val['movie_id'];
                            $final_content[$key]['movie_stream_id'] = $val['stream_id'];
                            $final_content[$key]['muvi_uniq_id'] = $val['uniq_id'];
                            $final_content[$key]['content_type_id'] = $val['content_type_id'];
                            $final_content[$key]['ppv_plan_id'] = $val['ppv_plan_id'];
                            $final_content[$key]['permalink'] = $val['fplink'];
                            $final_content[$key]['name'] = utf8_encode($cont_name);
                            $final_content[$key]['full_movie'] = $stream->full_movie;
                            $final_content[$key]['story'] = $story;
                            $final_content[$key]['genre'] = json_decode($val['genre']);
                            $final_content[$key]['censor_rating'] = ($val['censor_rating'] != '') ? implode(',', json_decode($val['censor_rating'])) . '&nbsp;' : '';
                            $final_content[$key]['release_date'] = $release;
                            $final_content[$key]['content_types_id'] = $val['content_types_id'];
                            $final_content[$key]['is_converted'] = $val['is_converted'];
                            $final_content[$key]['last_updated_date'] = '';
                            $final_content[$key]['movieid'] = $geo->movie_id;
                            $final_content[$key]['geocategory_id'] = $geo->geocategory_id;
                            $final_content[$key]['category_id'] = $restriction->category_id;
                            $final_content[$key]['studio_id'] = $studio_id;
                            $final_content[$key]['country_code'] = $restriction->country_code;
                            $final_content[$key]['ip'] = $restriction->ip;
                            $final_content[$key]['poster_url'] = $poster;
                            $final_content[$key]['isFreeContent'] = $isFreeContent;
                            $final_content[$key]['embeddedUrl'] = $embededurl;
                            $final_content[$key]['viewStatus'] = $viewStatusArr;
                        }
                    }
                }
            }
            $this->code = 200;
            $this->items = $final_content;
        } else {
            $this->code = 756;
        }
    }

    /**
     * @method public InitialiseSdk() is used to provide haskey and package name
     * @author suraja<support@muvi.com>ticket #6247
     * @param String authToken* , String package_name*, String lang_code
     * @return json Returns the list of data in json format success or corresponding error code
     */
    public function actionInitialiseSdk() {
        //check whether the hask key exists for the studio or not ?
        if (isset($_REQUEST['package_name']) && $_REQUEST['package_name'] != "") {
            $apiKeyData = OuathRegistration::model()->find('studio_id=:studio_id AND status=:status', array(':studio_id' => $this->studio_id, ':status' => '1'));
            if (@$apiKeyData) {
                $hashkey = $apiKeyData->hask_key;
                $packg_name = $apiKeyData->package_name;
                if ($apiKeyData->package_name == $_REQUEST['package_name']) {
                    if (empty(trim($hashkey))) {
                        //generate the new hask key and send it.
                        $key = md5(time());
                        $apiKeyData->hask_key = $key;
                        $apiKeyData->save();
                        $this->items['hash_key'] = $key;
                        $this->items['package_name'] = $packg_name;
                        $this->items['server_time'] = gmdate('Y-m-d H:i:s');
                        $this->msg_code = 'hashkey_created_success';
                    } else {
                        $this->code = 200;
                        $this->items['hash_key'] = $hashkey;
                        $this->items['package_name'] = $packg_name;
                        $this->items['server_time'] = gmdate('Y-m-d H:i:s');
                        $this->msg_code = 'hashkey_exists';
                    }
                } else {
                    $this->code = 200;
                    $this->items['hash_key'] = '';
                    $this->items['package_name'] = $packg_name;
                    $this->items['server_time'] = gmdate('Y-m-d H:i:s');
                    $this->msg_code = 'invalid_pakage_name';
                }
            } else
                $this->code = 698;
        } else
            $this->code = 697;
    }

    /**
     * @method public TextTranslation() is used to provide haskey and package name
     * @author Biswojit Parida<support@muvi.com>ticket #6247
     * @param String authToken* ,String lang_code*
     * @return json Returns the list of data in json format success or corresponding error code
     */
    public function actionTextTranslation() {
        if (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") {
            $lang_code = $_REQUEST['lang_code'];
            $theme = Studio::model()->findByPk($studio_id, array('select' => 'theme'))->theme;
            if (file_exists(ROOT_DIR . "languages/" . $theme . "/" . trim($lang_code) . ".php")) {
                $lang = include( ROOT_DIR . "languages/" . $theme . "/" . trim($lang_code) . ".php");
            } elseif (file_exists(ROOT_DIR . 'languages/studio/' . trim($lang_code) . '.php')) {
                $lang = include(ROOT_DIR . 'languages/studio/' . trim($lang_code) . '.php');
            } else {
                $lang = include(ROOT_DIR . 'languages/studio/en.php');
            }
            $language = Yii::app()->controller->getAllTranslation($lang, $this->studio_id);
            $this->code = 200;
            $this->items['translation'] = $language;
        } else
            $this->code = 774;
    }

    /**
     * @method private GetLanguageList()returns list of language
     * @author Prakash chandra nayak<support@muvi.com>
     * @Param String authToken*,String lang_code
     * @return json Returns the list of menu in json format     
     */
    public function actionGetLanguageList() {
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.translated_name,sl.status,sl.frontend_show_status FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$this->studio_id}) WHERE  l.code='en' OR (sl.studio_id={$this->studio_id})) AS a, studios s WHERE s.id={$this->studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        $studio_languages = Yii::app()->db->createCommand($sql)->queryAll();
        $lang_list = array();
        foreach ($studio_languages as $key => $lang) {
            if (($lang['frontend_show_status'] != "0") && ($lang['status'] == 1 || $lang['code'] == 'en')) {
                if (trim($lang['translated_name']) != "") {
                    $lang['name'] = $lang['translated_name'];
                }
                $lang_list[$key]['code'] = $lang['code'];
                $lang_list[$key]['language'] = $lang['name'];
            }
        }
        $this->code = 200;
        $this->items['lang_list'] = $lang_list;
        $this->items['default_lang'] = @$studio_languages[0]['default_language'];
    }

    /**
     * @method GetMenus() Returns the list of menus in tree structure
     * @param String $authToken A authToken
     * @author Ratikanta<support@muvi.com>
     * @return Returns the list of menus in tree structure
     */
    function actionGetMenus() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $mainmenu = Yii::app()->custom->getMainMenuStructure('array', $language_id, $studio_id);
        $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) ? $_REQUEST['user_id'] : 0;
        $usermenu = Yii::app()->custom->getUserMenuStructure('array', $language_id, $studio_id, $user_id, $translate);
        $sql = "SELECT domain, p.link_type,p.id, p.display_name, p.permalink, IF(p.link_type='external', p.external_url, concat('http://', domain, '/page/',p.permalink)) AS url FROM 
                    (SELECT id_seq, studio_id, external_url,id, link_type, title as display_name, permalink FROM pages WHERE studio_id={$studio_id} AND parent_id=0 AND status=1 AND permalink != 'terms-privacy-policy') AS p LEFT JOIN studios AS s ON p.studio_id=s.`id` ORDER BY p.id_seq ASC ";
        $pages = Yii::app()->db->createCommand($sql)->queryAll();
        if ($language_id != 20) {
            $pages = Yii::app()->custom->getStaticPages($studio_id, $language_id, $pages);
        }
        $this->code = 200;
        $this->items = array('mainmenu' => $mainmenu, 'usermenu' => $usermenu, 'footer_menu' => $pages);
    }

    /**
     * @method private GetStaticPagedetails()returns content of static page
     * @author Prakash chandra nayak<support@muvi.com>
     * @Param String authToken* ,String permalink*,String lang_code
     * @return json Returns the list of menu in json format     
     */
    public function actionGetStaticPageDetails() {
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $permalink = (isset($_REQUEST['permalink']) && $_REQUEST['permalink'] != "") ? $_REQUEST['permalink'] : '';
        if ($permalink) {
            $page = Yii::app()->db->createCommand()->select("*")->from('pages')->where('studio_id=:studio_id AND permalink=:permalink AND status=1 AND parent_id=0', array(':studio_id' => $this->studio_id, ':permalink' => $permalink))->queryRow();
            if (!empty($page)) {
                if ($language_id != 20) {
                    $page = Yii::app()->custom->getStaticPageDetails($this->studio_id, $language_id, $page);
                }
                $page['content'] = html_entity_decode($page['content']);
                $this->code = 200;
                $this->items['page_details'] = $page;
            } else
                $this->code = 693;
        } else
            $this->code = 604;
    }

    /**
     * @method public ContactUs()sent mail to provided email
     * @author Prakash chandra nayak<support@muvi.com>
     * @Param String authToken* ,String email*,String lang_code,String message,String name
     * @return message in json format     
     */
    public function actionContactUs() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        if (isset($_REQUEST['email']) && trim($_REQUEST['email']) != "") {
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $studio = Studio::model()->findByPk($studio_id, array('select' => 'name,contact_us_email,domain'));
            $subject = $studio->name . ' :: New Contact Us';
            $site_url = "https://" . $studio->domain;
            $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);
            $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
            $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
            $msg = htmlspecialchars(@$_REQUEST['message']);
            $useremail = @$_REQUEST['email'];
            $username = '';
            if (isset($_REQUEST['name']) && strlen(trim($_REQUEST['name'])) > 0) {
                $username = htmlspecialchars($_REQUEST['name']);
            }
            $template_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                "message" => $msg
            );
            if ($studio->contact_us_email) {
                $admin_to [] = $studio_email = $studio->contact_us_email;
                if (strstr($studio_email, ',')) {
                    $admin_to = explode(',', $studio_email);
                    $studio_email = $admin_to[0];
                }
            } else {
                $studio_user = User::model()->findByAttributes(array('studio_id' => $studio_id));
                $studio_email = $studio_user->email;
                $linked_emails = EmailNotificationLinkedEmail::model()->findByAttributes(array('studio_id' => $studio_id), array('select' => 'notification_from_email'));
                if ($linked_emails->notification_from_email) {
                    $studio_email = $linked_emails->notification_from_email;
                }
                $admin_to[] = $studio_email;
            }
            $studio_name = $StudioName = $studio->name;
            $EndUserName = $username;
            $admin_subject = $subject;
            $admin_from = $useremail;
            $email_type = 'contact_us';
            $admin_cc = Yii::app()->common->emailNotificationLinks($studio_id, $email_type);
            if (empty($admin_cc) || $admin_cc != "") {
                $admin_cc = array();
            }
            $user_to = array($useremail);
            $user_from = $studio_email;
            $content = "SELECT * FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND (language_id = " . $language_id . " OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= '" . $email_type . "' AND studio_id=" . $studio_id . " AND language_id = " . $language_id . "))";
            $data = Yii::app()->db->createCommand($content)->queryRow();
            if (empty($data)) {
                $temp = New NotificationTemplates;
                $data = $temp->findByAttributes(array('studio_id' => 0, 'type' => $email_type));
            }
            $user_subject = $data['subject'];
            eval("\$user_subject = \"$user_subject\";");
            $user_content = htmlspecialchars($data['content']);
            eval("\$user_content = \"$user_content\";");
            $user_content = htmlspecialchars_decode($user_content);
            $template_user_content = array(
                'website_name' => $studio->name,
                'logo' => $logo,
                'site_link' => $site_link,
                "username" => $username,
                "email" => $useremail,
                'content' => $user_content
            );
            Yii::app()->theme = 'bootstrap';
            $admin_html = Yii::app()->controller->renderPartial('//email/studio_contactus', array('params' => $template_content), true);
            $user_html = Yii::app()->controller->renderPartial('//email/studio_contactus_user', array('params' => $template_user_content), true);
            $returnVal_admin = $this->sendmailViaAmazonsdk($admin_html, $admin_subject, $admin_to, $admin_from, $admin_cc, '', '', $username);
            $returnVal_user = $this->sendmailViaAmazonsdk($user_html, $user_subject, $user_to, $user_from, '', '', '', $studio_name);
            $this->code = 200;
            $this->msg_code = 'thanks_for_contact';
        } else
            $this->code = 611;
    }
    public function actionGetFilteredContent(){
        if(isset($_REQUEST)){
            $list = array();
            $cat  = array();
            $studio_id   = $this->studio_id;
            $lang_code   = @$_REQUEST['lang_code'];
            $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] !="") ? $_REQUEST['user_id'] : 0;
            $is_episode = (isset($_REQUEST['is_episode']) && $_REQUEST['is_episode'] !="") ? $_REQUEST['is_episode'] : "";
            $translate   = $this->getTransData($lang_code, $studio_id);
            $language_id = Yii::app()->custom->getLanguage_id($lang_code);
            $content_category_value = 0;
            $condition = "";
            $sort_by   = 'F.name';
            $order_by  = 'ASC';
            $cond      = '';
            $group_by  = (isset($_REQUEST['group_by']) && $_REQUEST['group_by'] != '') ? $_REQUEST['group_by']  : '';
            if(isset($_REQUEST['category']) && $_REQUEST['category'] !=""){
                $categories = $_REQUEST['category'];
                if(!is_array($categories)){
                    $categories = explode("," , $categories);
                }
                $permalink = "";
                if(!empty($categories)){
                    $cond = " AND permalink IN ";
                    $total = count($categories);
                    for($i = 0; $i <$total; $i++){
                        $comma = " ";
                        if($total != ($i+1)){
                            $comma = ",";
                        }
                        $permalink .= "'".$categories[$i]."'";
                        $permalink .= $comma;
                    }
                    $cond .= " (".$permalink.") ";
                }
                $sql = "SELECT SUM(binary_value) as content_category_value FROM content_category WHERE parent_id =0 AND studio_id=".$studio_id." ".$cond;
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                if(!empty($res)){
                    $content_category_value = $res['content_category_value'];
                }
            }
            if($group_by == "category"){
                $sql = "SELECT id,category_name,permalink,binary_value FROM content_category WHERE parent_id =0 AND studio_id=".$studio_id."  ".$cond." ORDER BY category_name ASC";
                $cat = Yii::app()->db->createCommand($sql)->queryAll();
            }
            if($content_category_value > 0){
                $condition = " AND (F.content_category_value & ".$content_category_value.") ";
            }
            $page_size = $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset'] > 0) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            if(isset($_REQUEST['sort'])){
                $sort_by = $_REQUEST['sort'];
            }
            if(isset($_REQUEST['order'])){
                $order_by = $_REQUEST['order'];
            }
            $filter_cond = "";
            $request_array = array('authToken','lang_code','category','sort','order','limit','offset','Host','User-Agent','Accept','Accept-Encoding','Cookie','Content-Type','Content-Length');
            $film_array  = array('name','language','genre','rating','country','censor_rating','release_date');
            $movie_stream_array  = array('episode_title','episode_story','episode_date');
            foreach($_REQUEST as $key=>$value){
                if(!in_array($key,$request_array)){
                    if(in_array($key, $film_array)){
                        $get_filter_details = array(
                            'table' => 'films',
                            'field' => $key
                        );
                    }elseif(in_array($key, $movie_stream_array)){
                        $get_filter_details = array(
                            'table' => 'movie_streams',
                            'field' => $key
                        );
                    }else{
                        $get_filter_details = Yii::app()->custom->getFilterTableData($key, $this->studio_id);
                    }
                    if(!empty($get_filter_details)){
                        if(trim($value) != ""){
                            $ts = ($get_filter_details['table'] == "movie_streams") ? "M" : "F" ;
                            $field_name = $ts.".".$get_filter_details['field']; 
                            $fdata = explode(',' , trim($value));
                            $total = count($fdata);
                            for($i = 0; $i < $total; $i++){
                                $andor = ($i == 0) ? ' AND (' : ' OR';
                                $filter_cond .= $andor . ' '.$field_name. ' LIKE \'%' . $fdata[$i] . "%' ";
                            }
                            $filter_cond .= ") ";
                        }
                    }
                }
            }
            $episode_cond = "";
            if($is_episode !=""){
                $episode_cond = " AND M.is_episode=".$is_episode;
            }
            if(!empty($cat)){
                foreach ($cat as $category) {
                    $category_id = $category['id'];
                    $poster_category = $this->getPoster($category_id, 'content_category');
                    $category_name = $category['category_name'];
                    $category_permalink = $this->siteurl . '/' . $category['permalink'];
                    $binary_value = $category['binary_value'];
                    $sql = "SELECT id,name,story,permalink,parent_id FROM films WHERE studio_id =" . $studio_id . " AND content_category_value & " . $binary_value . " AND parent_id=0 ORDER BY name,parent_id ASC";
                    $films = Yii::app()->db->createCommand($sql)->queryAll();
                    $content_details = array();
                    $poster_film = array();
                    if (!empty($films)) {
                        foreach ($films as $film) {
                            $id = $film['id'];
                            $name = $film['name'];
                            $story = $film['story'];
                            $langcontent = Yii::app()->custom->getTranslatedContent($id, 0, $language_id, $studio_id);
                            if (array_key_exists($id, $langcontent['film'])) {
                                $name = $langcontent['film'][$id]->name;
                                $story = $langcontent['film'][$id]->story;
                            }
                            $fav_status = 1;
                            $login_status = 0;
                            if ($user_id) {
                                $fav_status = UserFavouriteList::model()->getFavouriteContentStatus($id, $studio_id, $user_id);
                                $fav_status = ($fav_status == 1) ? 0 : 1;
                                $login_status = 1;
                            }
                            $poster = $this->getPoster($id, 'films');
                            $permalink = $this->siteurl . '/' . $film['permalink'];
                            $content_details[] = array(
                                'content_id' => $id,
                                'name' => $name,
                                'story' => $story,
                                'poster' => $poster,
                                'content_permalink' => $permalink,
                                'is_episode' => 0,
                                'fav_status' => $fav_status,
                                'login_status' => $login_status
                            );
                        }
                    }

                    $channels[] = array(
                        'category_name' => $category_name,
                        'permalink' => $category_permalink,
                        'link' => $category['permalink'],
                        'category_poster' => $poster_category,
                        'content_list' => $content_details
                    );
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $channels;
                $this->setHeader($data['code']);
                echo json_encode($data);exit;
            }else{
                $command = Yii::app()->db->createCommand()
                    ->select('SQL_CALC_FOUND_ROWS (0),M.movie_id,M.id AS movie_stream_id,M.is_converted, M.episode_story,M.is_episode,F.permalink,F.name,F.name as display_name,F.content_type_id,M.full_movie,F.story,F.genre,F.release_date,F.language,F.content_types_id,F.language_id,M.episode_language_id')
                    ->from('movie_streams M,films F')
                    ->where('M.movie_id = F.id AND F.studio_id ='.$this->studio_id.' AND F.parent_id = 0 AND M.episode_parent_id =0 '.$condition.' '.$filter_cond.''.$episode_cond)
                    ->order($sort_by,$order_by)
                    ->limit($limit,$offset);
                $contents = $command->QueryAll();
                $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                $customData = array();
                $list       = array();
                if(!empty($contents)){
                    foreach($contents as $content){
                        $is_episode = $content['is_episode'];
                        $content_id = $content['movie_id'];
                        if($is_episode == 1){
                            $content_id = $content['movie_stream_id'];
                        }
                        $list[] = Yii::app()->general->getContentData($content_id, $is_episode, $customData, $language_id, $studio_id, $user_id, $translate);
                    }
                }
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['lists'] = $list;
                if(!empty($list)){
                    $data['total_content'] = $item_count;
                }else{
                    $data['msg'] = "No content found";    
                }
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid data sent.";    
        }
        $this->setHeader($data['code']);
        echo json_encode($data);exit;
    }     
    
    public function actionCheckFavourite(){
        $studio_id=$this->studio_id;
        $favenable=$this->CheckFavouriteEnable($studio_id);
        $data['code'] = 200;
        $data['status'] = "Success";
        $data['msg'] = $favenable;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }     

    /**
     * @method public ViewFavourite() provides favorite list of end user
     * @author Prakash chandra nayak<support@muvi.com>
     * @Param String authToken* ,Int user_id*,String lang_code,Int limit,Int offset
     * @return message in json format     
     */
    public function actionViewFavourite() {
        if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '') {
            $fav_status = $this->CheckFavouriteEnable($this->studio_id);
            if ($fav_status != 0) {
                $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
                $offset = 0;
                if (isset($_REQUEST['offset']) && $_REQUEST['offset'])
                    $offset = ($_REQUEST['offset'] - 1) * $limit;
                $langid = Yii::app()->custom->getLanguage_id(@$_REQUEST['lang_code']);
                $favlist = UserFavouriteList::model()->getUsersFavouriteContentList($_REQUEST['user_id'], $this->studio_id, $limit, $offset, $langid);
                $this->items['item_count'] = @$favlist['total'];
                $this->items['movie_list'] = @$favlist['list'];
                $this->items['limit'] = $limit;
                $this->code = 200;
            } else {
                $this->code = 702;
            }
        } else {
            $this->code = 633;
        }
    }

    public function actionAddtoFavlist() {
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '')) {
            $data = Yii::app()->db->createCommand()->select('id')->from("films")->where('uniq_id =:uniq_id', array(':uniq_id' => $_REQUEST['movie_uniq_id']))->queryRow();
            $content_type = isset($_REQUEST['content_type']) ? @$_REQUEST['content_type'] : '0';
            $user_fav_stat = UserFavouriteList::model()->findByAttributes(array('content_id' => $data['id'], 'content_type' => $content_type, 'studio_id' => $this->studio_id, 'user_id' => $_REQUEST['user_id']));
            if (empty($user_fav_stat)) {
                $usr_fav = new UserFavouriteList;
                $usr_fav->status = '1';
                $usr_fav->content_id = $data['id'];
                $usr_fav->content_type = $content_type;
                $usr_fav->studio_id = $this->studio_id;
                $usr_fav->user_id = $_REQUEST['user_id'];
                $usr_fav->last_updated_at = $usr_fav->date_added = date('Y-m-d H:i:s');
                $usr_fav->save();
                $this->code = 200;
            } else {
                $this->code = 701;
            }
        } else {
            $this->code = 700;
        }
    }

    public function actionDeleteFavList() {
        if ((isset($_REQUEST['movie_uniq_id']) && ($_REQUEST['movie_uniq_id'] != '')) && (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '')) {
            $data = Yii::app()->db->createCommand()->select('id')->from("films")->where('uniq_id =:uniq_id', array(':uniq_id' => $_REQUEST['movie_uniq_id']))->queryRow();
            $content_type = isset($_REQUEST['content_type']) ? @$_REQUEST['content_type'] : '0';
            $params = array(':content_id' => $data['id'], ':content_type' => $content_type, ':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']);
            $user_fav_stat = UserFavouriteList::model()->deleteAll('content_id = :content_id AND content_type = :content_type AND studio_id = :studio_id AND user_id = :user_id', $params);
            $this->code = 200;
        } else {
            $this->code = 700;
        }
    }

    /*     * @method public RegisterMyDevice() registering device for sending push notification
     * @author Biswajit Das<support@muvi.com>
     * @param string authToken* , string device_id* ,string fcm_token*,int device_type,string lang_code 
     */

    public function actionRegisterMyDevice() {
        $device_id = (isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '') ? $_REQUEST['device_id'] : '';
        $fcm_token = (isset($_REQUEST['fcm_token']) && $_REQUEST['fcm_token'] != '') ? @$_REQUEST['fcm_token'] : '';
        if ($device_id && $fcm_token) {
            $notification = new RegisterDevice;
            $registerdevice = $notification->findByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $device_id));
            if ($registerdevice) {
                $registerdevice->fcm_token = $fcm_token;
                $registerdevice->save();
            } else {
                $register_device = new RegisterDevice;
                $register_device->studio_id = $this->studio_id;
                $register_device->device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                $register_device->device_id = $device_id;
                $register_device->fcm_token = $fcm_token;
                $register_device->save();
            }
            $this->code = 200;
            $this->msg_code = 'device_registerd';
        } else
            $this->code = 685;
    }

    /*     * @method public GetNotificationList()() It will list push notification list log device wise 
     * @param string authToken* , string device_id* ,int before_days,string lang_code
     * @author Prakash<support@muvi.com>
     * @return Json object
     */

    public function actionGetNotificationList() {
        $device_id = (isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '') ? $_REQUEST['device_id'] : '';
        if ($device_id) {
            $diff_days = (isset($_REQUEST['before_days']) && $_REQUEST['before_days'] > 0 ) ? $_REQUEST['before_days'] : 7;
            $all_notification = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('push_notification_log')
                            ->where("`studio_id`=:studio_id AND status > '0' AND `device_id`=:device_id AND `created_date` >= DATE_SUB(CURDATE(), INTERVAL :diff_days DAY)", array(':studio_id' => $this->studio_id, ':device_id' => $device_id, ':diff_days' => $diff_days))->queryAll();
            if ($all_notification) {
                $unread_cnt = 0;
                foreach ($all_notification as $values) {
                    if ($values['status'] == 1)
                        $unread_cnt++;
                }
                $this->code = 200;
                $this->items['count'] = count($all_notification);
                $this->items['count_unread'] = $unread_cnt;
                $this->items['notify_list'] = $all_notification;
            }else {
                $this->code = 200;
                $this->msg_code = 'no_notification';
            }
        } else
            $this->code = 687;
    }

    public function actionReadNotification() {
        if ((isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '') && (isset($_REQUEST['message_unique_id']) && $_REQUEST['message_unique_id'] != '')) {
            $notification = new PushNotificationLog;
            $notificationstatus = $notification->findByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $_REQUEST['device_id'], 'message_unique_id' => $_REQUEST['message_unique_id'], 'status' => '1'));
            if ($notificationstatus) {
                $notificationstatus->status = '2';
                $notificationstatus->save();
                $this->msg_code = 'notification_read';
                $this->code = 200;
            } else {
                $this->code = 705;
            }
        } else {
            $this->code = 671;
        }
    }

    public function actionGetNoOfUnreadNotification() {
        $notification = new PushNotificationLog;
        $notificationstatus = $notification->findAllByAttributes(array('studio_id' => $this->studio_id, 'device_id' => $_REQUEST['device_id'], 'status' => '1'));
        $this->items['count'] = count($notificationstatus);
        $this->code = 200;
    }

    public function actionReadAllNotification() {
        if ((isset($_REQUEST['device_id']) && $_REQUEST['device_id'] != '')) {
            $notification = new PushNotificationLog;
            $params = array(':studio_id' => $this->studio_id, ':device_id' => $_REQUEST['device_id'], ':status' => '1');
            $notificationstatus = $notification->updateAll(array('status' => '2'), "studio_id=:studio_id AND device_id =:device_id AND status =:status", $params);
            $this->code = 200;
        } else {
            $this->code = 671;
        }
    }
    /*
     * @author RK<developer@muvi.com>
     * To load the homepage contents
     */

    public function actionloadFeaturedSections() {
        $offset = isset($_REQUEST['dataoffset']) ? $_REQUEST['dataoffset'] : 0;
        $limit = isset($_REQUEST['viewlimit']) ? $_REQUEST['viewlimit'] : 2;
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $default_currency_id = Studio::model()->findByPk($this->studio_id)->default_currency_id;
        $translate = $this->getTransData($lang_code, $studio_id);
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0 ? $_REQUEST['user_id'] : 0;
        // query criteria
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'contents' => array(// this is for fetching data
                'together' => false,
                'condition' => 'contents.studio_id = ' . $studio_id,
                'order' => 'contents.id_seq ASC'
            ),
        );
        $cond = 't.is_active = 1 AND t.studio_id = ' . $studio_id . ' AND language_id=' . $language_id;
        $criteria->condition = $cond;
        $criteria->order = 't.id_seq ASC, t.id ASC';
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $sections = FeaturedSection::model()->findAll($criteria);
        $return = array();
        foreach ($sections as $section) {
            $final_content = array();
            $total_contents = 0;
            if ($section->content_type == 1) {
                $final_content = Yii::app()->custom->SectionProducts($section->id, $studio_id, $default_currency_id);
            } else {
                foreach ($section->contents as $featured) {
                    $final_content[] = Yii::app()->general->getContentData($featured->movie_id, $featured->is_episode, array(), $language_id, $studio_id, $user_id, $translate);
                }
            }
            $return[] = array(
                'id' => $section->id,
                'title' => utf8_encode($section->title),
                'content_type' => $section['content_type'],
                'total' => count($final_content),
                'contents' => $final_content
            );
        }

        $res['code'] = 200;
        $res['status'] = "OK";
        $res['section'] = $return;
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /*
     * @purpose for filter custom manage metadata 
     * @author Sweta<sweta@muvi.com>
     */

    public function actionGetCustomFilter() {
        $lists = array();
        $studio_id = $this->studio_id;
        $command = Yii::app()->db->createCommand()
                ->select('f.id,f.f_type,f.f_display_name,f.f_name,f.f_id,f.f_value as options')
                ->from('custom_metadata_field f')
                ->where('f.f_type IN(2,3) AND f.studio_id=' . $studio_id . ' ORDER BY id ASC');
        $filterdata = $command->queryAll();
        if (!empty($filterdata)) {
            foreach ($filterdata as $listdata) {
                $options = json_decode($listdata['options']);
                if (!is_array($options) && $listdata['options']) {
                    if (!in_array(trim($listdata['options']), array('null', 'NULL', '[', '""'))) {
                        $options = explode(",", $listdata['options']);
                    }
                }
                $listdata['options'] = $options;
                $lists[] = $listdata;
            }
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['filter_list'] = $lists;
        } else {
            $data['code'] = 452;
            $data['status'] = 'Failure';
            $data['msg'] = 'No data found';
        }
        echo json_encode($data);
        exit;
    }

    /**
     * @method private GetAppMenu() Get the list of Mobile & TV Apps Menu 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @Param String authToken* , String lang_code
     * @return json Returns the list of menu in json format     
     */
    public function actionGetAppMenu() {
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $app_menu = AppMenu::model()->findByAttributes(array('studio_id' => $this->studio_id), array('select' => 'id'));
        if (!empty($app_menu)) {
            $menu_items = AppMenuItems::model()->getAllMenus($this->studio_id, $app_menu->id, $language_id);
            if (!empty($menu_items)) {
                $this->code = 200;
                $this->items['menu_items'] = $menu_items;
            } else
                $this->code = 773;
        } else
            $this->code = 773;
    }

    /**
     * @method private GetAppFeaturedContent() Get featured contents of a particular section for  Mobile & TV Apps 
     * @author Biswajit Parida<biswajit@muvi.com>
     * @return json Returns the list of featured contents in json format
     * @param string authToken*,string section_id*, string lang_code.
     * 
     */
    public function actionGetAppFeaturedContent() {
        $lang_code = @$_REQUEST['lang_code'];
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['section_id']) && $_REQUEST['section_id'] != "") {
            $domainName = $this->getDomainName();
            $contents = AppFeaturedContent::model()->getAppFeatured($_REQUEST['section_id'], $this->studio_id, $language_id, 0, $domainName);
            if (!empty($contents)) {
                $this->code = 200;
                $this->items = $contents;
            } else
                $this->code = 693;
        } else
            $this->code = 688;
    }

    public function actionGetAPIServer() {
        if (isset($_REQUEST['ip']) && $_REQUEST['ip'] != "") {
            $ip = $_REQUEST['ip'];
        } else {
            $ip = CHttpRequest::getUserHostAddress();
        }
        $geo_data = new EGeoIP();
        $geo_data->locate($ip);
        $latitude = $geo_data->getLatitude();
        $longitude = $geo_data->getLongitude();
        $ref = array($latitude, $longitude);
        $command = Yii::app()->db->createCommand()->select('*')->from('api_server');
        $locations = $command->queryAll();
        foreach ($locations AS $key => $val) {
            $apiserver[$val['id_server']][] = $val['url'];
            $apiserver[$val['id_server']][] = $val['latitude'];
            $apiserver[$val['id_server']][] = $val['longitude'];
            $server[$val['id_server']] = $val;
        }
        if ($latitude && $longitude) {
            $distances = array_map(function($apiserver) use($ref) {
                $a = array_slice($apiserver, -2);
                return Yii::app()->common->getDistance($a, $ref);
            }, $apiserver);
            asort($distances);
            $bucketids = array_keys($distances);
            if ($bucketids[0] > 0)
                $s3bucket_id = $bucketids[0];
        } else {
            $s3bucket_id = 5;
        }
        $res['code'] = 200;
        $res['status'] = "OK";
        $res['url'] = $server[$s3bucket_id]['url'];
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    public function actionViewContentRating() {
        $studio_id = $this->studio_id;
        $is_enable_rating = Yii::app()->db->createCommand()->select('rating_activated')->from('studios')->where('id =:studio_id', array(':studio_id' => $this->studio_id))->queryRow();
        if ($is_enable_rating['rating_activated']) {
            if (!empty($_REQUEST['content_id']) && intval($_REQUEST['content_id'])) {
                $showrating = 1;
                if (isset($_REQUEST['user_id']) && trim($_REQUEST['user_id']) != '') {
                    $condition = " AND user_id = :user_id ";
                    $urating = Yii::app()->db->createCommand()
                            ->select('CR.rating,CR.review,CR.created_date,U.display_name')
                            ->from('content_ratings CR,sdk_users U')
                            ->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id' . $condition, array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id, ':user_id' => $_REQUEST['user_id']))
                            ->queryAll();
                    if (count($urating) > 0) {
                        $showrating = 0;
                    }
                }
                $rating = Yii::app()->db->createCommand()
                        ->select('CR.id,CR.rating,CR.review,CR.created_date,U.display_name,CR.status')
                        ->from('content_ratings CR,sdk_users U')
                        ->where('CR.user_id = U.id AND CR.content_id=:content_id AND CR.studio_id=:studio_id', array(':content_id' => $_REQUEST['content_id'], ':studio_id' => $studio_id))
                        ->order('CR.id DESC')
                        ->queryAll();
                $review = array();
                foreach ($rating as $key => $val) {
                    $review[$key]['display_name'] = $val['display_name'];
                    $review[$key]['created_date'] = $val['created_date'];
                    $review[$key]['rating'] = $val['rating'];
                    $review[$key]['review'] = $val['review'];
                    $review[$key]['status'] = $val['status'];
                }
                $this->code = 200;
                $this->items['rating'] = $review;
                $this->items['showrating'] = $showrating;
            } else
                $this->code = 777;
        } else
            $this->code = 776;
    }
};
