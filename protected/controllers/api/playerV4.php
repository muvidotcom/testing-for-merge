<?php
trait PlayerV4 {

    /**
     * @method public getVideoDetails() It will give details of the video
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $content_uniq_id Uniq id of 
     * @param string $stream_uniq_id Stream Id 
     * @return json Json string
     */
    function actionGetVideoDetails() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        $content_uniq_id = @$_REQUEST['content_uniq_id'];
        $stream_uniq_id = @$_REQUEST['stream_uniq_id'];
        $internetSpeed = 0;
        if (isset($_REQUEST['internet_speed']) && $_REQUEST['internet_speed'] != '') {
            $internetSpeed = $_REQUEST['internet_speed'];
        }
        if ($content_uniq_id && $stream_uniq_id) {
            $cond = array(':uniq_id' => $content_uniq_id, ':stream_uniq_id' => $stream_uniq_id);
            $command = Yii::app()->db->createCommand()
                    ->select('F.id,F.name,F.content_types_id,S.id AS stream_id,S.rolltype AS rollType,S.roll_after AS rollAfter,S.video_resolution,S.enable_ad AS adActive,S.full_movie,S.embed_id,S.is_demo,S.is_converted,F.uniq_id,S.content_key,S.encryption_key,S.thirdparty_url,S.video_management_id,S.is_offline, S.is_episode')
                    ->from('films F, movie_streams S')
                    ->where('F.id=S.movie_id AND F.uniq_id=:uniq_id AND S.embed_id=:stream_uniq_id ', $cond);
            $contentDetails = $command->queryAll();
            if ($contentDetails) {
                $language_nameArr = array();
                $domainName = $this->getDomainName();
                $movie = $contentDetails[0];
                $thirdparty_url = '';
                $fullmovie_path = '';
                $embedUrl = '';
                $trailerThirdpartyUrl = '';
                $trailerUrl = '';
                $embedTrailerUrl = '';
                $adActive = $contentDetails[0]['adActive'];
                $rollType = $contentDetails[0]['rollType'];
                $roll_after = $contentDetails[0]['rollAfter'];
                $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '') ? $_REQUEST['user_id'] : 0;
                $stream_id = ($movie['stream_id'] > 0) ? $movie['stream_id'] : 0;
                $parameters = array();
                $parameters['movie_id'] = $movie['id'];
                $parameters['stream_id'] = $stream_id;
                $parameters['studio_id'] = $studio_id;
                $activePlayCount = VideoLogs::model()->activePlayLog($parameters);
                $streamingRestriction = 0;
                $getDeviceRestrictionSetByStudio = 0;
                $download_status = 1;
                $waterMarkOnPlayer = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'embed_watermark');
                $watermark_details = array();
                $watermark_details['status'] = 0;
                if(@$waterMarkOnPlayer['config_value'] == 1){
                    $watermark= WatermarkPlayer::model()->getStudioId($studio_id);
                    $watermark_details['status'] = 1;
                    $watermark_details['email'] = @$watermark['email'];
                    $watermark_details['ip'] = @$watermark['ip'];
                    $watermark_details['date'] = @$watermark['date'];
                }
                $data['is_watermark'] = $watermark_details;
                if ($user_id > 0) {
                    $playLengthArr = VideoLogs::model()->getvideoDurationPlayed($studio_id, $user_id, $movie['id'], $stream_id);
                    $getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($studio_id, $user_id);
                    if ($getDeviceRestriction != '' && $getDeviceRestriction > 0) {
                        $streamingRestriction = 1;
                    }
                    if ($getDeviceRestriction != "") {
                        $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
                        if (@$getDeviceRestrictionSetByStudioData->config_value != "") {
                            $getDeviceRestrictionSetByStudio = $getDeviceRestrictionSetByStudioData->config_value;
                        }
                    }
                    //offline watch period start
                    if ($movie['content_types_id'] != 4) {
                        //Check record is available in offline_vie_log table
                        $offline_details = OfflineView::model()->findByAttributes(array('studio_id' => $studio_id, 'video_id' => $movie['stream_id'], 'user_id' => $user_id));
                        if ($offline_details) {
                            $current_date_expiry_time = strtotime(gmdate('Y-m-d H:i:s')) * 1000;
                            if ($current_date_expiry_time >= $offline_details->access_expiry_time) {
                                $download_status = 0;
                            }
                        } else {
                            //call billing function
                            $access_info = Yii::app()->common->getContentExpiryDate($studio_id, $user_id, $movie['embed_id']);
                            if ($access_info['is_valid']) {
                                if ($access_info['expiry_date'] != '') {
                                    $config_value = Yii::app()->db->createCommand()
                                            ->select('config_key,config_value')
                                            ->from('studio_config')
                                            ->where("studio_id=:studio_id AND config_key IN('offline_view_access_period','offline_view_access_period_limit')", array(':studio_id' => $this->studio_id))
                                            ->queryAll();
                                    $config_value_arr = CHtml::listData($config_value, 'config_key', 'config_value');
                                    $current_date_expiry_time = strtotime(gmdate('Y-m-d H:i:s')) * 1000;
                                    if ($config_value_arr['offline_view_access_period'] == 1) {
                                        $config_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $current_date_expiry_time);
                                        $billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
                                        $access_expiry_time = ($billing_expiry_time <= $config_expiry_time) ? $billing_expiry_time : $config_expiry_time;
                                        if ($current_date_expiry_time >= $access_expiry_time) {
                                            $download_status = 0;
                                        }
                                    } else {
                                        $billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
                                        if ($current_date_expiry_time >= $billing_expiry_time) {
                                            $download_status = 0;
                                        }
                                    }
                                }
                            } else {
                                $download_status = 0;
                            }
                        }
                    }
                    //offline watch period end
                }
                $played_length = !empty($playLengthArr['played_length']) ? $playLengthArr['played_length'] : '';
                //get trailer info from movie_trailer table                                      
                $movie_id = $movie['id'];
                $trailerData = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id' => $movie_id));
                if ($trailerData['trailer_file_name'] != '') {
                    $embedTrailerUrl = $domainName . '/embedTrailer/' . $movie['uniq_id'];
                    if ($trailerData['third_party_url'] != '') {
                        $trailerThirdpartyUrl = $this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                    } else if ($trailerData['video_remote_url'] != '') {
                        $trailerUrl = $this->getTrailer($movie_id, 0, "", $this->studio_id);
                    }
                }
                //End of trailer part
                if ($movie['content_types_id'] == 4) {
                    $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id' => $this->studio_id, ':movie_id' => $movie['id']));
                    if ($Ldata && $Ldata->feed_url) {
                        $embedUrl = $domainName . '/embed/livestream/' . $movie['uniq_id'];
                        $fullmovie_path = $Ldata->feed_url;
                        if ($Ldata->feed_type == 2) {
                            $streamUrlExplode  = explode("/",str_replace("rtmp://","",$Ldata->stream_url));
                            $nginxServerIp = @$streamUrlExplode[0];
                            $nginxServerDetails = Yii::app()->aws->getNginxServerDetails($nginxServerIp); 
                            if (strpos($Ldata->feed_url, 'rtmp://' . $nginxServerIp . '/live') !== false) {
                                $fullmovie_path = str_replace("rtmp://" . $nginxServerIp . "/live", @$nginxServerDetails['nginxserverlivecloudfronturl'], $Ldata->feed_url) . "/index.m3u8";
                            } else if (strpos($Ldata->feed_url, 'rtmp://' . $nginxServerIp . '/record') !== false) {
                                $fullmovie_path = str_replace("rtmp://" . $nginxServerIp . "/record", @$nginxServerDetails['nginxserverrecordcloudfronturl'], $Ldata->feed_url) . "/index.m3u8";
                            }
                        }
                    }
                } else {
                    if ($movie['full_movie'] != '') {
                        $embedUrl = $domainName . '/embed/' . $movie['embed_id'];
                        if ($movie['thirdparty_url'] != '') {
                            $thirdparty_url = $this->getSrcFromThirdPartyUrl($movie['thirdparty_url']);
                        } else {
                            //Check thirdparty url is not present then pass video url with details                            
                            $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
                            $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/movie_stream/full_movie/" . $movie['stream_id'] . "/" . $movie['full_movie'];
                            if ($movie['is_demo'] == 1) {
                                $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/small.mp4";
                            }
                            $movie['movieUrlForTv'] = $fullmovie_path;
                            $multipleVideo = $this->getvideoResolution($movie['video_resolution'], $fullmovie_path);
                            $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $internetSpeed);
                            $videoToBeplayed12 = explode(",", $videoToBeplayed);
                            $fullmovie_path = $videoToBeplayed12[0];
                            $data['videoResolution'] = $videoToBeplayed12[1];
                            $clfntUrl = 'https://' . $bucketInfo['cloudfront_url'];
                            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                            $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000, $studio_id);
                            //added by prakash for signed url on 29th march 2017
                            $multipleVideoSignedArr = array();
                            foreach ($multipleVideo as $resKey => $videoPath) {
                                $subArr = array();
                                if ($resKey) {
                                    $subArr['resolution'] = $resKey;
                                    $rel_path_multiple = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $videoPath));
                                    $subArr['url'] = $this->getnew_secure_urlForEmbeded($rel_path_multiple, $clfntUrl, "", "", 60000, $studio_id);
                                    $multipleVideoSignedArr[] = $subArr;
                                }
                            }
                            $data['videoDetails'] = $multipleVideoSignedArr;
                            $data['newvideoUrl'] = $multipleVideoForUnsigned[144];
                            $offline = 0;
                            $app = Yii::app()->general->apps_count($studio_id);
                            if (empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))) {
                                $offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
                                if (($offline_val['config_value'] == 1 || $offline_val == '') && ($movie['is_offline'] == 1) && (@$movie['content_types_id'] == 1 || (@$movie['content_types_id'] == 3 && @$movie['is_episode'] == 1))) {
                                    $offline = 1;
                                }
                            }
                            $data['is_offline'] = $offline;
                            if ($movie['content_key'] && $movie['encryption_key']) {
                                $folderPath = Yii::app()->common->getFolderPath("", $this->studio_id);
                                $signedBucketPath = $folderPath['signedFolderPath'];
                                $fullmovie_path = 'https://' . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie['stream_id'] . '/stream.mpd';
                                $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($this->studio_id, 'drm_cloudfront_url');
                                if ($getStudioConfig) {
                                    if (@$getStudioConfig['config_value'] != '') {
                                        $fullmovie_path = 'https://' . $getStudioConfig['config_value'] . '/uploads/movie_stream/full_movie/' . $movie['stream_id'] . '/stream.mpd';
                                    }
                                }
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, MS3_URL);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&contentId=urn:marlin:kid:" . $movie['encryption_key'] . "&contentKey=" . $movie['content_key'] . "&ms3Extension=wudo,false,AAAAAA==&ms3Scheme=true&contentURL=" . $fullmovie_path);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                $server_output = curl_exec($ch);
                                $data['studio_approved_url'] = $server_output;
                                curl_close($ch);
                                //get LicenceUrl									
                                $licence_url = $this->generateExplayWideVineToken($movie['content_key'], $movie['encryption_key']);
                                $data['licenseUrl'] = $licence_url ? $licence_url : '';
                            }
                        }
                    }
                }
                /**
                 *  srt/vtt subtitle files for api.
                 * @author Prangya.
                 */
                if (isset($movie['video_management_id']) && ($movie['video_management_id'] > 0)) {
                    $video_management_id = $movie['video_management_id'];
                    $cond_subtitle = array(':video_management_id' => $video_management_id);
                    $command = Yii::app()->db->createCommand()
                            ->select('vs.id AS subId,vs.filename,ls.name,ls.code,vs.video_gallery_id,vs.display_name as subtitle_name,')
                            ->from('video_subtitle vs,language_subtitle ls')
                            ->where('vs.language_subtitle_id = ls.id AND vs.video_gallery_id=:video_management_id', $cond_subtitle);
                    $getVideoSubtitle = $command->queryAll();
                    if ($getVideoSubtitle) {
                        $i = 0;
                        foreach ($getVideoSubtitle as $getVideoSubtitlekey => $getVideoSubtitlevalue) {
                            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                            $signedFolderPath = $folderPath['signedFolderPath'];
                            $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                            $bucketname = $bucketInfo['bucket_name'];
                            $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                            $s3url = Yii::app()->common->connectToAwsS3($studio_id);
                            $filePth = $clfntUrl . "/" . $bucketInfo['signedFolderPath'] . "subtitle/" . $getVideoSubtitlevalue['subId'] . "/" . $getVideoSubtitlevalue['filename'];
                            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                            $language_nameArr[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1, "", 60000, $studio_id);
                            $language_nameArr[$i]['code'] = $getVideoSubtitlevalue['code'];
                            if ($getVideoSubtitlevalue['subtitle_name'] != '') {
                                $subtitileName = $getVideoSubtitlevalue['subtitle_name'];
                            } else {
                                $subtitileName = $getVideoSubtitlevalue['name'];
                            }
                            if (@$translate[$getVideoSubtitlevalue['name']] != '') {
                                $language_nameArr[$i]['language'] = @$translate[$subtitileName];
                            } else {
                                $language_nameArr[$i]['language'] = $subtitileName;
                            }
                            $i ++;
                        }
                    }
                }
                // end of subtitle part.
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = "Video Details";
                $data['videoUrl'] = $fullmovie_path;
                $data['thirdparty_url'] = $thirdparty_url;
                $data['emed_url'] = $embedUrl;
                $data['trailerUrl'] = $trailerUrl;
                $data['trailerThirdpartyUrl'] = $trailerThirdpartyUrl;
                $data['embedTrailerUrl'] = $embedTrailerUrl;
                $data['played_length'] = $played_length;
                $data['subTitle'] = $language_nameArr;
                $data['streaming_restriction'] = $streamingRestriction;
                $data['no_streaming_device'] = $getDeviceRestrictionSetByStudio;
                $data['msg'] = @$translate['restrict-streaming-device'];
                $data['no_of_views'] = @$activePlayCount;
                if ($user_id > 0) {
                    $data['download_status'] = $download_status;
                }
                $ads['adActive'] = $adActive;
                $ads['adNetwork'] = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($this->studio_id);
                $start = $mid = $end = 0;
                if ($rollType == '7' || $rollType == '5' || $rollType == '3' || $rollType == '1') {
                    $start = 1;
                }
                if ($rollType == '7' || $rollType == '3' || $rollType == '6' || $rollType == '2') {
                    $mid = 1;
                }
                if ($rollType == '7' || $rollType == '5' || $rollType == '6' || $rollType == '4') {
                    $end = 1;
                }
                $adsTime['start'] = $start;
                $adsTime['mid'] = $mid;
                $adsTime['end'] = $end;
                $roll_after = explode(',', $roll_after);
                if (in_array("end", $roll_after)) {
                    array_pop($roll_after);
                }
                if (in_array("0", $roll_after)) {
                    $roll_after = array_reverse($roll_after);
                    array_pop($roll_after);
                    $roll_after = array_reverse($roll_after);
                }
                //convert ads streaming time to seconds
                $time_seconds = "";
                $i = 0;
                while ($i < count($roll_after)) {
                    if (count($roll_after) > 1)
                        $time_seconds .= $this->getHHMMSSToseconds($roll_after[$i++]) . ',';
                    else
                        $time_seconds .= $this->getHHMMSSToseconds($roll_after[$i++]);
                }
                $adsTime['midroll_values'] = $time_seconds;
                if ($ads['adNetwork'][1]['ad_network_id'] != "" && $ads['adNetwork'][1]['ad_network_id'] != 3) {
                    $ads['adsTime'] = $adsTime;
                }
                if ((@$ads['adNetwork'][1]['ad_network_id'] != "") && (@$ads['adNetwork'][1]['ad_network_id'] != "") && ($ads['adActive'] != 0) && (@$ads['adNetwork'][0]['menu'] & 4)) {
                    $data['adsDetails'] = $ads;
                }
            } else {
                $data['code'] = 438;
                $data['status'] = 'Failure';
                $data['msg'] = 'No content availbe for the information';
            }
        } else {
            $data['code'] = 437;
            $data['status'] = 'Failure';
            $data['msg'] = 'Content id and Stream id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public startUpstream() It will start the upstream automatically and will generate the HLS url 
     * @param string $authToken Authtoken
     * @param string $channel_id Authtoken
     * @param string $title Title of the stream
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStartUpstream() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_start_time = gmdate('Y-m-d H:i:s');
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid content id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getliveUserlist() It will return the list of users who are currently live streaming 
     * @param string $authToken Authtoken
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetLiveUserlist() {
        $lsuser = LivestreamUsers::model()->findAll('is_online=1 AND studio_id =:studio_id', array(':studio_id' => $this->studio_id));
        if ($lsuser) {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $data['msg'] = 'Available users list';
            foreach ($lsuser AS $key => $val) {
                $data['users'][$key]['name'] = $val->name;
                $data['users'][$key]['nick_name'] = $val->nick_name;
                $data['users'][$key]['title'] = $val->channel_title;
                $data['users'][$key]['hls_path'] = $val->pull_url;
                $data['users'][$key]['rtmp_path'] = $val->push_url;
            }
        } else {
            $data['code'] = 200;
            $data['status'] = 'OK';
            $data['msg'] = 'No online user available';
            $data['users'] = array();
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getChannelOnlineStatus() It will return the list of users who are currently live streaming 
     * @param string $authToken Authtoken
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actiongetChannelOnlineStatus() {
        if ($_REQUEST['permalink']) {
            $movie = Yii::app()->db->createCommand()->select('f.content_types_id,ls.is_online')->from('films f ,livestream ls ')
                    ->where('ls.movie_id = f.id AND f.content_types_id=4 AND f.permalink=:permalink AND  f.studio_id=:studio_id', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();
            if ($movie) {
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['is_online'] = $movie["is_online"] ? (int) $movie["is_online"] : 0;
                $data['msg'] = $movie["is_online"] ? "Online" : 'Offline';
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid permalink';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'permalink required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public stopUpstream() It will stop the upstream 
     * @param string $authToken Authtoken
     * @param string $channel_id Channel id of the streaming
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionStopUpstream() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 0;
                $lsuser->stream_start_time = '';
                $lsuser->stream_update_time = '';
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success!';
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid movie id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public channelLiveStatus() It will stop the upstream 
     * @param string $authToken Authtoken
     * @param string $channel_id Channel id of the streaming
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionChannelStatus() {
        if (@$_REQUEST['movie_id']) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = "Stream is online now";
            } else {
                $data['code'] = 441;
                $data['status'] = 'Failure';
                $data['msg'] = 'Invalid movie id';
            }
        } else {
            $data['code'] = 440;
            $data['status'] = 'Failure';
            $data['msg'] = 'A movie id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionVideoLogs() {
        $data = array();

        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if ($user_id) {
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                } else {
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }
                    $parameters = array();
                    $parameters['movie_id'] = $movie_id;
                    $parameters['stream_id'] = $stream_id;
                    $parameters['studio_id'] = $studio_id;
                    $activePlayCount = VideoLogs::model()->activePlayLog($parameters);

                    $played_length = $_REQUEST['played_length'];
                    $video_length = isset($_REQUEST['video_length']) ? $_REQUEST['video_length'] : '0';
                    $watch_status = $_REQUEST['watch_status'];
                    $device_type = $_REQUEST['device_type'];
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $restrictDeviceId = 0;
                    if ($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
                        $restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
                        if (@$_REQUEST['is_active_stream_closed'] == 1) {
                            RestrictDevice::model()->deleteData($restrictStreamId);
                        } else {
                            $postData = array();
                            $postData['id'] = $restrictStreamId;
                            $postData['studio_id'] = $studio_id;
                            $postData['user_id'] = $user_id;
                            $postData['movie_stream_id'] = $stream_id;
                            $restrictDeviceId = RestrictDevice::model()->dataSave($postData);
                        }
                    }
                    $video_log = new VideoLogs();
                    $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;

                    $trailer_id = '';
                    if ($content_type == 2) {
                        $trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        $trailer_id = $trailerData->id;
                        $stream_id = 0;
                    }
                    $video_log->trailer_id = $trailer_id;

                    if ($log_id > 0) {
                        $video_log = VideoLogs::model()->findByPk($log_id);
                        $video_log->updated_date = new CDbExpression("NOW()");
                    } else {
                        $video_log->created_date = new CDbExpression("NOW()");
                        $video_log->ip = $ip_address;
                        $video_log->video_length = $video_length;
                        $video_log->device_type = $device_type;
                    }
                    $video_log->played_length = $played_length;
                    $video_log->movie_id = $movie_id;
                    $video_log->video_id = $stream_id;
                    $video_log->user_id = $user_id;
                    $video_log->device_id = $device_id;
                    $video_log->content_type = $content_type;
                    $video_log->studio_id = $studio_id;
                    $video_log->watch_status = $watch_status;
                    $video_log->resume_time = $played_length;
                    if ($video_log->save()) {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['log_id'] = $video_log->id;
                        $data['restrict_stream_id'] = $restrictDeviceId;
                        $data['no_of_views'] = @$activePlayCount;
                    } else {
                        $data['code'] = 500;
                        $data['status'] = "Error";
                        $data['msg'] = "Internal Server Error";
                    }
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionVideoLogNew() {
        $data = array();

        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if ($user_id) {
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                } else {
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }
                    $parameters = array();
                    $parameters['movie_id'] = $movie_id;
                    $parameters['stream_id'] = $stream_id;
                    $parameters['studio_id'] = $studio_id;
                    $activePlayCount = VideoLogs::model()->activePlayLog($parameters);

                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $restrictDeviceId = 0;
                    if ($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
                        $restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
                        if (@$_REQUEST['is_active_stream_closed'] == 1) {
                            RestrictDevice::model()->deleteData($restrictStreamId);
                        } else {
                            $postData = array();
                            $postData['id'] = $restrictStreamId;
                            $postData['studio_id'] = $studio_id;
                            $postData['user_id'] = $user_id;
                            $postData['movie_stream_id'] = $stream_id;
                            $restrictDeviceId = RestrictDevice::model()->dataSave($postData);
                        }
                    }
                    $dataForSave = array();
                    $dataForSave = $_REQUEST;
                    $dataForSave['movie_id'] = $movie_id;
                    $dataForSave['stream_id'] = $stream_id;
                    $dataForSave['studio_id'] = $studio_id;
                    $dataForSave['user_id'] = $user_id;
                    $dataForSave['ip_address'] = $ip_address;
                    $dataForSave['trailer_id'] = "";
                    $dataForSave['status'] = $_REQUEST['watch_status'];
                    $dataForSave['content_type'] = $content_type;
                    $dataForSave['enableWatchDuration'] = @$_REQUEST['enableWatchDuration'];
                    if ($content_type == 2) {
                        $trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        $dataForSave['trailer_id'] = $trailerData->id;
                        $dataForSave['stream_id'] = "";
                    }
                    $video_log_id = VideoLogs::model()->dataSave($dataForSave);
                    $avail_time = 1;
                    if (isset($_REQUEST['enableWatchDuration']) && $_REQUEST['enableWatchDuration'] == 1 && $_REQUEST['user_id'] > 0) {
                        $sum = Yii::app()->db->createCommand()
                                ->select('sum(played_length)')
                                ->from('video_logs t')
                                ->where('t.user_id =' . $_REQUEST['user_id'] . ' and date(t.created_date )="' . date("Y-m-d") . '" and t.is_watch_durationenabled = 1')
                                ->queryScalar();
                        $watch_duration_sec = Yii::app()->custom->restrictDevices($studio_id, 'daily_watch_duration');
                        if ($watch_duration_sec <= $sum) {
                            $avail_time = 0;
                        }
                    }
                    if ($video_log_id) {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['log_id'] = @$video_log_id[1];
                        $data['log_temp_id'] = @$video_log_id[0];
                        $data['restrict_stream_id'] = $restrictDeviceId;
                        $data['no_of_views'] = @$activePlayCount;
                        $data['avail_time'] = $avail_time;
                    } else {
                        $data['code'] = 500;
                        $data['status'] = "Error";
                        $data['msg'] = "Internal Server Error";
                    }
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionBufferLogs() {
        $data = array();
        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if ($user_id) {
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                } else {
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }

                    $device_type = $_REQUEST['device_type'];
                    $resolution = (isset($_REQUEST['resolution'])) ? $_REQUEST['resolution'] : "";
                    $start_time = (isset($_REQUEST['start_time'])) ? $_REQUEST['start_time'] : 0;
                    $end_time = (isset($_REQUEST['end_time'])) ? $_REQUEST['end_time'] : 0;
                    $log_unique_id = (isset($_REQUEST['log_unique_id']) && trim(($_REQUEST['log_unique_id']))) ? $_REQUEST['log_unique_id'] : '';
                    $log_id = (isset($_REQUEST['log_id']) && trim(($_REQUEST['log_id']))) ? $_REQUEST['log_id'] : '';
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $played_time = $end_time - $start_time;
                    $bandwidthType = 1;
                    if (@$_REQUEST['downloaded_bandwidth'] != '') {
                        $bandwidth_used = @$_REQUEST['downloaded_bandwidth'];
                        $bandwidthType = 2;
                    } else if (strtolower(@$_REQUEST['video_type']) == 'mped_dash') {
                        //mped_dash
                        $bandwidth_used = @$_REQUEST['totalBandwidth'];
                    } else {
                        if ($content_type == 2) {
                            $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        } else {
                            $movie_stream_data = movieStreams::model()->findByPk($stream_id);
                        }
                        $res_size = json_decode($movie_stream_data->resolution_size, true);
                        $video_duration = explode(':', $movie_stream_data->video_duration);
                        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
                        $size = $res_size[$resolution];
                        $bandwidth_used = ($size / $duration) * $played_time;
                    }

                    if (isset($log_id) && $log_id) {
                        //$buff_log = BufferLogs::model()->findByAttributes(array('unique_id' => $log_unique_id, 'studio_id' => $studio_id, 'user_id' => $user_id, 'movie_id' => $movie_id, 'resolution' => $resolution));
                        $buff_log = BufferLogs::model()->findByPk($log_id);
                        $buff_log_id = $log_id;
                        $unique_id = $log_unique_id;
                        $bandwidthData = array();
                        $bandwidthData = $_REQUEST;
                        $bandwidthData['bandwidth_used'] = $bandwidth_used;
                        $bandwidthData['buff_log_id'] = $log_id;
                        $bandwidthData['request_type'] = @$_REQUEST['video_type'];
                        $is_log = Yii::app()->aws->sentMailForWrongBandwidthLog($bandwidthData);

                        if (isset($buff_log) && !empty($buff_log) && $is_log == 1) {
                            $buff_log->end_time = $end_time;
                            $buff_log->buffer_size = $bandwidth_used;
                            $buff_log->played_time = $end_time - $buff_log->start_time;
                            $buff_log->save();
                        }
                    } else {

                        if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
                            if ($device_type == 4) {
                                $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
                            } else {
                                $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
                            }
                            $city = $buff_log->city;
                            $region = $buff_log->region;
                            $country = $buff_log->country;
                            $country_code = $buff_log->country_code;
                            $continent_code = $buff_log->continent_code;
                            $latitude = $buff_log->latitude;
                            $longitude = $buff_log->longitude;
                        } else {
                            $ip_address = Yii::app()->request->getUserHostAddress();
                            $location = IP2Location::model()->getLocation($ip_address);
                            $city = @$location['city_name'];
                            $region = @$location['region_name'];
                            $country = @$location['country_name'];
                            $country_code = @$location['country_code'];
                            $continent_code = @$location['continent_code'];
                            $latitude = @$location['latitude'];
                            $longitude = @$location['longitude'];
                        }
                        $unique_id = md5(uniqid(rand(), true));
                        $buff_log = new BufferLogs();
                        $buff_log->studio_id = $studio_id;
                        $buff_log->unique_id = $unique_id;
                        $buff_log->user_id = $user_id;
                        $buff_log->device_id = $device_id;
                        $buff_log->movie_id = $movie_id;
                        $buff_log->video_id = $stream_id;
                        $buff_log->resolution = $resolution;
                        $buff_log->start_time = $start_time;
                        $buff_log->end_time = $end_time;
                        $buff_log->played_time = $played_time;
                        $buff_log->buffer_size = $bandwidth_used;
                        $buff_log->city = $city;
                        $buff_log->region = $region;
                        $buff_log->country = $country;
                        $buff_log->country_code = $country_code;
                        $buff_log->continent_code = $continent_code;
                        $buff_log->latitude = $latitude;
                        $buff_log->longitude = $longitude;
                        $buff_log->device_type = $device_type;
                        $buff_log->content_type = $content_type;
                        $buff_log->ip = $ip_address;
                        $buff_log->bandwidth_type = $bandwidthType;
                        $buff_log->created_date = date('Y-m-d H:i:s');
                        $buff_log->save();
                        $buff_log_id = $buff_log->id;
                    }

                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['location'] = 1;
                    $data['log_id'] = $buff_log_id;
                    $data['log_unique_id'] = $unique_id;
                    $data['request_data'] = @$_REQUEST['request_data'];
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actionUpdateBufferLogs() {
        $data = array();

        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if ($user_id) {
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                } else {
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);

                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;

                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }

                    $device_type = $_REQUEST['device_type'];
                    $resolution = $_REQUEST['resolution'];
                    $start_time = $_REQUEST['start_time'];
                    $end_time = $_REQUEST['end_time'];
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $movie_stream_data = movieStreams::model()->findByPk($stream_id);
                    if ($content_type == 2) {
                        $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                    }
                    $res_size = json_decode($movie_stream_data->resolution_size, true);
                    $video_duration = explode(':', $movie_stream_data->video_duration);
                    $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);

                    $size = $res_size[$resolution];
                    $played_time = $end_time - $start_time;
                    $bandwidth_used = ($size / $duration) * $played_time;

                    $unique_id = md5(uniqid(rand(), true));
                    if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
                        if ($device_type == 4) {
                            $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
                        } else {
                            $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
                        }

                        $city = $buff_log->city;
                        $region = $buff_log->region;
                        $country = $buff_log->country;
                        $country_code = $buff_log->country_code;
                        $continent_code = $buff_log->continent_code;
                        $latitude = $buff_log->latitude;
                        $longitude = $buff_log->longitude;
                    } else {
                        $ip_address = Yii::app()->request->getUserHostAddress();
                        $location = IP2Location::model()->getLocation($ip_address);
                        $city = @$location['city_name'];
                        $region = @$location['region_name'];
                        $country = @$location['country_name'];
                        $country_code = @$location['country_code'];
                        $continent_code = @$location['continent_code'];
                        $latitude = @$location['latitude'];
                        $longitude = @$location['longitude'];
                    }

                    $buff_log = new BufferLogs();
                    $buff_log->studio_id = $studio_id;
                    $buff_log->unique_id = $unique_id;
                    $buff_log->user_id = $user_id;
                    $buff_log->device_id = $device_id;
                    $buff_log->movie_id = $movie_id;
                    $buff_log->video_id = $stream_id;
                    $buff_log->resolution = $resolution;
                    $buff_log->start_time = $start_time;
                    $buff_log->end_time = $end_time;
                    $buff_log->played_time = $played_time;
                    $buff_log->buffer_size = $bandwidth_used;
                    $buff_log->city = $city;
                    $buff_log->region = $region;
                    $buff_log->country = $country;
                    $buff_log->country_code = $country_code;
                    $buff_log->continent_code = $continent_code;
                    $buff_log->latitude = $latitude;
                    $buff_log->longitude = $longitude;
                    $buff_log->device_type = $device_type;
                    $buff_log->content_type = $content_type;
                    $buff_log->ip = $ip_address;
                    $buff_log->created_date = date('Y-m-d H:i:s');
                    $buff_log->save();
                    $buff_log_id = $buff_log->id;

                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['log_id'] = $buff_log_id;
                    $data['log_unique_id'] = $unique_id;
                    $data['location'] = 1;
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method BufferLogsNew() this method is used for bufferlog of non-drm video in chromecast receiver end
     * @param String $authToken A authToken,string movie_id,Int user_id,String ip_address etc 
     * @author prakash<support@muvi.com>
     * @return Json data
     */
    public function actionBufferLogsNew() {
        $data = array();
        if (isset($_REQUEST['ip_address']) && isset($_REQUEST['movie_id'])) {
            $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : '';
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $studio_id = $this->studio_id;
                $user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
                if ($user_id) {
                    $add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
                } else {
                    $add_video_log = 1;
                }
                $isAllowed = 0;
                $device_id = 0;
                if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
                    $isAllowed = 1;
                    $device_id = $_REQUEST['user_id'];
                }
                if ($add_video_log || $isAllowed) {
                    $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
                    $movie_id = Yii::app()->common->getMovieId($movie_code);
                    $stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
                    $season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
                    //Get stream Id
                    $stream_id = 0;
                    if (trim($movie_code) != '0' && trim($stream_code) != '0') {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
                    } else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
                    } else {
                        $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
                    }
                    $location = (isset($_REQUEST['location']) && $_REQUEST['location'] > 0) ? $_REQUEST['location'] : 0;
                    $device_type = isset($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 0;
                    $resolution = (isset($_REQUEST['resolution'])) ? $_REQUEST['resolution'] : "";
                    $start_time = (isset($_REQUEST['start_time'])) ? $_REQUEST['start_time'] : 0;
                    $end_time = (isset($_REQUEST['end_time'])) ? $_REQUEST['end_time'] : 0;
                    $log_unique_id = (isset($_REQUEST['log_unique_id']) && trim(($_REQUEST['log_unique_id']))) ? $_REQUEST['log_unique_id'] : '';
                    $buffer_log_id = (isset($_REQUEST['log_id']) && trim(($_REQUEST['log_id']))) ? $_REQUEST['log_id'] : '';
                    $buffer_log_temp_id = (isset($data['buffer_log_temp_id']) && intval($data['buffer_log_temp_id'])) ? $data['buffer_log_temp_id'] : 0;
                    $content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
                    $played_time = $end_time - $start_time;
                    $bandwidthType = 1;
                    if (@$_REQUEST['downloaded_bandwidth'] != '') {
                        $bandwidth_used = @$_REQUEST['downloaded_bandwidth'];
                        $bandwidthType = 2;
                    } else if (strtolower(@$_REQUEST['video_type']) == 'mped_dash') {
                        //mped_dash
                        $bandwidth_used = @$_REQUEST['totalBandwidth'];
                    } else {
                        if ($content_type == 2) {
                            $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
                        } else {
                            $movie_stream_data = movieStreams::model()->findByPk($stream_id);
                        }
                        $res_size = json_decode($movie_stream_data->resolution_size, true);
                        $video_duration = explode(':', $movie_stream_data->video_duration);
                        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
                        $size = $res_size[$resolution];
                        $bandwidth_used = ($size / $duration) * $played_time;
                    }
                    $dataForSave = array();
                    $dataForSave = $_REQUEST;
                    $dataForSave['studio_id'] = $studio_id;
                    $dataForSave['buffer_log_id'] = $buffer_log_id;
                    $dataForSave['buffer_log_temp_id'] = $buffer_log_temp_id;
                    $dataForSave['user_id'] = $user_id;
                    $dataForSave['movie_id'] = $movie_id;
                    $dataForSave['video_id'] = $stream_id;
                    $dataForSave['resolution'] = $resolution;
                    $dataForSave['start_time'] = $start_time;
                    $dataForSave['end_time'] = $end_time;
                    $dataForSave['bandwidth_used'] = $bandwidth_used;
                    $dataForSave['ip_address'] = $ip_address;
                    $dataForSave['played_time'] = $played_time;
                    $dataForSave['location'] = $location;
					$dataForSave['device_type'] = $device_type;
                    $dataForSave['device_id'] = $device_id;
					$dataForSave['bandwidth_type'] = $bandwidthType;
                    $bandwidthData = array();
                    $bandwidthData = $_REQUEST;
                    $bandwidthData['bandwidth_used'] = $bandwidth_used;
                    $bandwidthData['buff_log_id'] = $buffer_log_id;
                    $bandwidthData['request_type'] = @$_REQUEST['video_type'];
                    $is_log = Yii::app()->aws->sentMailForWrongBandwidthLog($bandwidthData);
                    if($is_log){
                        $buffer_log_data = BufferLogs::model()->dataSave($dataForSave);
                    }
                    if ($buffer_log_data) {
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['location'] = 1;
                        $data['log_id'] = @$buffer_log_data[0];
                        $data['log_unique_id'] = @$buffer_log_data[1];
                        $data['buffer_log_temp_id'] = @$buffer_log_data[2];
                    }elseif($is_log == 0){
                        $data['code'] = 200;
						$data['status'] = "OK";
						$data['location'] = 1;
						$data['log_id'] = $buffer_log_id;
						$data['log_unique_id'] = "";
						$data['buffer_log_temp_id'] = $buffer_log_temp_id;	
                    } else {
                        $data['code'] = 500;
                        $data['status'] = "Error";
                        $data['msg'] = "Internal Server Error";
                    }
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Adding log is not allowed!";
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method addVideoViewLogs() It will add the video view logs, Increase the video view count,Icrease the played duration, Increase the buffer size
     * @param String $authToken A authToken
     * @param Integer user_id 
     * @param Integer movie_id 
     * @param Integer stream_id
     * @param String	played_duration
     * @param String	buffer_length
     * @author Gayadhar<support@muvi.com>
     * @return Json Success/failuer
     */
    function actionAddVideoViewLogs() {
        $req = $_REQUEST;
        if (@$req['movie_id']) {
            $movie_id = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
            $stream_id = isset($_REQUEST['stream_id']) ? $_REQUEST['stream_id'] : 0;
            $video_log = new VideoLogs();
            $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
            if ($log_id > 0) {
                $video_log = VideoLogs::model()->findByPk($log_id);
                $video_log->updated_date = new CDbExpression("NOW()");
                $video_log->played_length = @$_REQUEST['played_duration'] ? @$_REQUEST['played_duration'] : 0;
            } else {
                $video_log->created_date = new CDbExpression("NOW()");
                $video_log->ip = $ip_address;
                $video_log->video_length = @$_REQUEST['buffer_length'] ? @$_REQUEST['buffer_length'] : 0;
            }
            $video_log->movie_id = $movie_id;
            $video_log->video_id = $stream_id;
            $video_log->user_id = @$_REQUEST['user_id'] ? @$_REQUEST['user_id'] : 0;
            $video_log->studio_id = $this->studio_id;
            $video_log->watch_status = @$_REQUEST['status'] ? @$_REQUEST['status'] : 'start';
            if ($video_log->save()) {
                $data['code'] = 200;
                $data['status'] = "Success";
                $data['msg'] = "Video Log updated successfully";
                $data['log_id'] = $video_log->id;
            } else {
                $data['code'] = 452;
                $data['status'] = "Error";
                $data['msg'] = "Error in saving data";
            }
        } else {
            $data['code'] = 453;
            $data['status'] = "Error";
            $data['msg'] = "Movie id is required";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    public function actiongetEmbedUrl() {
        $movieStreamData = movieStreams::model()->find('studio_id=:studio_id AND id=:id', array(':studio_id' => $this->studio_id, ':id' => $_REQUEST['movie_stream_id']));
        if ($movieStreamData) {
            $data['status'] = 'OK';
            $data['code'] = 200;
            $data['msg'] = 'Success';
            $domainName = Yii::app()->getBaseUrl(TRUE);
            $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=" . $this->studio_id;
            $stData = Yii::app()->db->createCommand($sql)->queryAll();
            if (@$stData[0]['is_embed_white_labled']) {
                $domainName = @$stData[0]['domain'] ? 'http://' . @$stData[0]['domain'] : $domainName;
            }
            $data['emed_url'] = $domainName . '/embed/' . $movieStreamData->embed_id;
        } else {
            $data['code'] = 447;
            $data['status'] = "Error";
            $data['msg'] = "No data found!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /*     * @method Get Marlin BB Offline content
     * @author SKP<srutikant@muvi.com>
     * @return Json data with parameters
     */

    public function actiongetMarlinBBOffline() {
        $studio_id = $this->studio_id;
        $stream_unique_id = @$_REQUEST['stream_unique_id'];
        if ($stream_unique_id != '') {
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $stream_unique_id));
            if ($streams) {
                if (@$streams->encryption_key != '' && @$streams->content_key != '' && @$streams->is_offline == 1) {
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'drm_cloudfront_url');
                    $fileName = substr($streams->full_movie, 0, -4) . '.mlv';
                    if (@$getStudioConfig['config_value'] != '') {
                        $fullmovie_path = 'https://' . $getStudioConfig['config_value'] . '/uploads/movie_stream/full_movie/' . $streams->id . '/' . $fileName;
                    } else {
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                        $signedBucketPath = $folderPath['signedFolderPath'];
                        $fullmovie_path = 'https://' . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $streams->id . '/' . $fileName;
                    }
                    if ($streams->is_multibitrate_offline == 1) {
                        $multOfflineVal = $this->getvideoResolution($streams->video_resolution, $fullmovie_path);
                        foreach ($multOfflineVal as $resKey => $videoPath) {
                            $subArr['resolution'] = $resKey;
                            $subArr['url'] = $videoPath;
                            $token['multiple_resolution'][] = $subArr;
                        }
                    }
                    $token['file'] = $fullmovie_path;
                    $token['token'] = file_get_contents('https://bb-gen.test.expressplay.com/hms/bb/token?customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&actionTokenType=1&rightsType=BuyToOwn&outputControlOverride=urn:marlin:organization:intertrust:wudo,ImageConstraintLevel,0&cookie=MY_TEST01&contentId=' . $streams->content_key . '&contentKey=' . $streams->encryption_key);
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['data'] = $token;
                } else {
                    $data['code'] = 438;
                    $data['status'] = "Error";
                    $data['msg'] = "No such content found";
                }
            } else {
                $data['code'] = 438;
                $data['status'] = "Error";
                $data['msg'] = "No such content found";
            }
        } else {
            $data['code'] = 438;
            $data['status'] = "Error";
            $data['msg'] = "Stream unique id not found";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public assignBroadCastToContent() It will create content against broadcaster under a category. 
     * @author Gayadhar<support@muvi.com>
     * @param string $content_name Name of the BroadCast
     * @param string $category_id Id of the content category
     * @param int $user_id Logged in user id
     * @param text $description Description of the content
     * @return HTML 
     */
    function actionAssignBroadCastToContent() {
        $required_params = array('category_id' => 1, 'content_name' => 1, 'user_id' => 1);
        if (@$_REQUEST['category_id']) {
            unset($required_params['category_id']);
        }
        if (@$_REQUEST['content_name']) {
            unset($required_params['content_name']);
        }
        if (@$_REQUEST['user_id']) {
            unset($required_params['user_id']);
        }
        if (empty($required_params)) {
            $studio_id = $this->studio_id;
            $arr['succ'] = 0;
            $Films = new Film();
            $data['name'] = $_REQUEST['content_name'];
            $data['story'] = @$_REQUEST['description'];
            $catData = Yii::app()->db->CreateCommand("SELECT id,binary_value,category_name,permalink FROM content_category WHERE id={$_REQUEST['category_id']} AND studio_id={$this->studio_id}")->queryRow();
            $data['content_category_value'] = array($catData['id']);
            $data['parent_content_type'] = 2;
            $data['content_types_id'] = 4;
            $movie = $Films->addContent($data, $this->studio_id);
            $movie_id = $movie['id'];
            $arr['uniq_id'] = $movie['uniq_id'];

            //Live stream set up
            $streamUrl = $movie['permalink'] . '-' . strtotime(date("Y-m-d H:i:s"));
            $length = 8;
            $userName = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
            $password = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
            $enc = new bCrypt();
            $passwordEncrypt = $enc->hash($password);
            $ch = curl_init();
            $nginxServerIp = Yii::app()->aws->getNginxServerIp($studio_id); 
            curl_setopt($ch, CURLOPT_URL, 'http://' . $nginxServerIp . '/auth.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&is_active=1&stream_name=" . $streamUrl . "&user_name=" . $userName . "&password=" . $passwordEncrypt . "&serverUrl=" . Yii::app()->getBaseUrl(TRUE) . "/conversion/stopLiveStreaming?movie_id" . $movie_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = trim(curl_exec($ch));
            curl_close($ch);
            if ($server_output != 'success') {
                Film::model()->deleteByPk($movie_id);
                $data['code'] = 458;
                $data['status'] = "Failure";
                $data['msg'] = "Due to some reason live stream content was unable to create. Please try again";
                $this->setHeader($data['code']);
                echo json_encode($data);
                exit;
            }

            //Adding permalink to url routing 
            $urlRouts['permalink'] = $movie['permalink'];
            $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);

            //Insert Into Movie streams table
            $MovieStreams = new movieStreams();
            $MovieStreams->studio_id = $studio_id;
            $MovieStreams->movie_id = $movie_id;
            $MovieStreams->is_episode = 0;
            $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
            $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
            $embed_uniqid = Yii::app()->common->generateUniqNumber();
            $MovieStreams->embed_id = $embed_uniqid;
            $publish_date = NULL;
            $MovieStreams->content_publish_date = $publish_date;
            $MovieStreams->save();

            //Upload Poster
            //$this->processContentImage(4,$movie_id,$MovieStreams->id);
            //Adding the feed infos into live Stream table
            $feedData['feed_url'] = 'rtmp://' . $nginxServerIp . '/live/' . $streamUrl;
            $feedData['stream_url'] = 'rtmp://' . $nginxServerIp . '/live';
            $feedData['stream_key'] = $streamUrl . "?user=" . $userName . "&pass=" . $password;
            $feedData['feed_type'] = 2;
            $feedData['is_recording'] = 0;
            $feedData['method_type'] = 'push';
            $feedData['start_streaming'] = 1;

            $livestream = new Livestream();
            $livestream->saveFeeds($feedData, $studio_id, $movie_id, $_REQUEST['user_id']);
            if (HOST_IP != '127.0.0.1') {
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $movie_id;
                $solrArr['stream_id'] = $MovieStreams->id;
                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                $solrArr['is_episode'] = 0;
                $solrArr['name'] = $movie['name'];
                $solrArr['permalink'] = $movie['permalink'];
                $solrArr['studio_id'] = $studio_id;
                $solrArr['display_name'] = 'livestream';
                $solrArr['content_permalink'] = $movie['permalink'];
                $solrObj = new SolrFunctions();
                $ret = $solrObj->addSolrData($solrArr);
            }
            $data = array();
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = 'Broadcast Content created successfully.';
            $data['feed_url'] = $feedData['stream_url'] . "/" . $feedData['stream_key'];
            $data['stream_url'] = $feedData['stream_url'];
            $data['stream_key'] = $feedData['stream_key'];
            $data['movie_id'] = $movie_id;
            $data['uniq_id'] = $movie['uniq_id'];
            $data['movie_stream_uniq_id'] = $embed_uniqid;
        } else {
            $data['code'] = 458;
            $data['status'] = "Failure";
            $data['msg'] = implode(',', $required_params) . ": required param(s) missing";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @purpose get the total no of videos and total duration of videos
     * @param authToken Mandatory
     * @Author Biswajit Parida<biswajit@muvi.com>
     */
    public function actionGetTotalNoOfVideo() {
        $mapped_videos = 0;
        $mapped_duration = $total_duration = 0;
        $studio_id = $this->studio_id;
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0), vm.*,ms.id as movie_stream_id,ms.is_converted as movieConverted,mt.is_converted as trailerConverted FROM video_management vm LEFT JOIN movie_streams ms ON (vm.id=ms.video_management_id) LEFT JOIN movie_trailer mt ON (vm.id=mt.video_management_id) WHERE  vm.studio_id=' . $studio_id . ' GROUP BY vm.id  order by vm.video_name ASC';
        $allvideo = Yii::app()->db->createCommand($sql)->queryAll();
        $total_videos = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        if (!empty($allvideo)) {
            foreach ($allvideo as $videos) {
                $duration = Yii::app()->custom->convertTimeToMiliseconds($videos['duration']);
                if (trim($duration) == "") {
                    $duration = 0;
                }
                $total_duration += $duration;
                if (($videos['movieConverted'] == 1)) {
                    $mapped_videos++;
                    $mapped_duration += $duration;
                } elseif (($videos['trailerConverted'] == 1)) {
                    $mapped_videos++;
                    $mapped_duration += $duration;
                }
            }
        }
        //$total_duration  = Yii::app()->custom->formatMilliseconds($total_duration);
        //$mapped_duration = Yii::app()->custom->formatMilliseconds($mapped_duration);
        $data = array(
            'total_videos' => $total_videos,
            'total_duration' => $total_duration,
            'mapped_videos' => $mapped_videos,
            'mapped_duration' => $mapped_duration
        );
        echo json_encode($data);
        exit;
    }

    /* Added by Biswajit Das(biswajitdas@muvi.com) for registering device for sending push notification */

    public function actionRegisterMyDevice() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);

        if ((isset($_REQUEST['device_id']) && ($_REQUEST['device_id'] != '')) && (isset($_REQUEST['fcm_token']) && $_REQUEST['fcm_token'] != '')) {
            $device_id = $_REQUEST['device_id'];
            $notification = new RegisterDevice;
            $registerdevice = $notification->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
            if (empty($registerdevice)) {
                $register_device = new RegisterDevice;
                $register_device->studio_id = $studio_id;
                $register_device->device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                $register_device->device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                $register_device->fcm_token = isset($_REQUEST['fcm_token']) ? @$_REQUEST['fcm_token'] : '';
                $register_device->save();
            } else {
                $registerdevice->fcm_token = isset($_REQUEST['fcm_token']) ? @$_REQUEST['fcm_token'] : '';
                $registerdevice->save();
            }
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['msg'] = $translate['device_registerd'];
        } else {
            $res['code'] = 400;
            $res['status'] = "Failure";
            $res['msg'] = $translate['device_not_registerd'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * API for Device Management Listing
     */
    public function actionManageDevices() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['user_id'])) {
            $user_id = $_REQUEST['user_id'];
            $sql = Yii::app()->db->createCommand()
                    ->select(' * ')
                    ->from("device_management")
                    ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $studio_id, ':user_id' => $user_id))
                    ->order('id ASC')
                    ->queryAll();
            if ($sql) {
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = @$translate['device_list'];
                $res['device_list'] = $sql;
            } else {
                $res['code'] = 452;
                $res['status'] = 'Failure';
                $res['msg'] = @$translate['no_devices_available'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['user_information_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * This function is used to request studio admin to remove device
     */
    public function actionRemoveDevice() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                $user_id = $_REQUEST['user_id'];
                $device = $_REQUEST['device'];
                $device_details = Device::model()->findBySql('SELECT * FROM `device_management` WHERE `studio_id`=:studio_id AND `user_id`=:user_id AND `device`=:device AND flag=0 LIMIT 1', array(':studio_id' => $studio_id, ':user_id' => $user_id, ':device' => $device));
                if (!empty($device_details)) {
                    $id = $device_details['id'];
                    $device_info = $device_details['device_info'];
                    Device::model()->updateByPk($id, array('flag' => 1, 'deleted_date' => date('Y-m-d H:i:s')));
                    /* if (NotificationSetting::model()->isEmailNotificationRemoveDevice($studio_id, 'device_management')) {
                      $user_data = SdkUser::model()->findByPk($user_id, array('select' => 'email'));
                      $email = $user_data['email'];
                      $mailcontent = array('studio_id' => $studio_id,'device' => $device,'device_info'=>$device_info, 'email'=>$email);
                      Yii::app()->email->mailRemoveDevice($mailcontent);
                      } */
                    $res['code'] = 200;
                    $res['status'] = "Success";
                    $res['msg'] = @$translate['remove_device_request_succ'];
                } else {
                    $res['code'] = 450;
                    $res['status'] = "Failure";
                    $res['msg'] = @$translate['device_not_found_deleted'];
                }
            } else {
                $res['code'] = 450;
                $res['status'] = "Failure";
                $res['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['device_information_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * This function is used to check a device will add or not
     */
    public function actionCheckDevice() {
        $studio_id = $this->studio_id;
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $studio_id);
        if (isset($_REQUEST['device']) && $_REQUEST['device'] != '') {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = @$_REQUEST['user_id'];
                $device = @$_REQUEST['device'];
                $google_id = @$_REQUEST['google_id'];
                $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                if (Yii::app()->custom->restrictDevices($this->studio_id, 'restrict_no_devices')) {
                    $chkDeviceLimit = Yii::app()->custom->restrictDevices($studio_id, 'limit_devices');
                    $all_devices = Yii::app()->db->createCommand()
                            ->select(' count(*) AS cnt, SUM(IF(device=:device, 1, 0)) AS exist ')
                            ->from("device_management")
                            ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $studio_id, ':user_id' => $user_id, ':device' => $device))
                            ->queryrow();
                    if ($all_devices['exist'] == 0) {
                        if ($all_devices['cnt'] < $chkDeviceLimit) {
                            $this->actionAddDevice();
                        } else {
                            $data['code'] = 450;
                            $data['status'] = "failure";
                            $data['msg'] = @$translate['exceed_no_devices'];
                        }
                    } else {
                        //update device_management table for google_id
                        $sql = "UPDATE device_management SET google_id = :google_id WHERE studio_id =:studio_id AND user_id=:user_id AND device=:device AND flag=0";
                        $command = Yii::app()->db->createCommand($sql);
                        $command->execute(array(':google_id' => $google_id, ':studio_id' => $studio_id, ':user_id' => $user_id, ':device' => $device));
                        $data['code'] = 200;
                        $data['status'] = "Success";
                        $data['msg'] = @$translate['successful'];
                    }
                } else {
                    $data['code'] = 450;
                    $data['status'] = "Failure";
                    $data['msg'] = @$translate['device_restriction_not_enable'];
                }
            } else {
                $data['code'] = 450;
                $data['status'] = "Failure";
                $data['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $data['code'] = 450;
            $data['status'] = "Failure";
            $data['msg'] = @$translate['device_id_not_found'];
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * This function is used to add new devices
     */
    public function actionAddDevice() {
        $lang_code = @$_REQUEST['lang_code'];
        $translate = $this->getTransData($lang_code, $this->studio_id);
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $devicelist = new Device();
                $devicelist->user_id = @$_REQUEST['user_id'];
                $devicelist->studio_id = $this->studio_id;
                $devicelist->device = @$_REQUEST['device'];
                $devicelist->device_info = @$_REQUEST['device_info'];
                $devicelist->device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                $devicelist->google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                $devicelist->created_by = @$_REQUEST['user_id'];
                $devicelist->created_date = date('Y-m-d H:i:s');
                $devicelist->save();
                $res['code'] = 200;
                $res['status'] = "Success";
                $res['msg'] = @$translate['device_added_success'];
            } else {
                $res['code'] = 450;
                $res['status'] = "Failure";
                $res['msg'] = @$translate['user_information_not_found'];
            }
        } else {
            $res['code'] = 450;
            $res['status'] = "Failure";
            $res['msg'] = @$translate['device_id_not_found'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }

    /**
     * @method public getOfflineViewRemainingTime() for offline view
     * @author Prakash Chandra Nayak rout<support@muvi.com>
     * @param string authToken* , string stream_uniq_id* ,int user_id ,int watch_remaining_time ,string device_id ,int device_type,string request_data
     * @return json Returns the list of data in json format success or corrospending error code
     */
    function actionGetOfflineViewRemainingTime() {
        $stream_uniq_id = isset($_REQUEST['stream_uniq_id']) ? $_REQUEST['stream_uniq_id'] : '';
        $lang_code = @$_REQUEST['lang_code'];
        if ($stream_uniq_id) {
            $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
            if ($user_id) {
                $device_type = isset($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 2;
                $device_id = @$_REQUEST['device_id'];
                $request_data = @$_REQUEST['request_data'];
                //check stream uniq id is available in offline_view_log table				
                $offline_view_log_data = OfflineView::model()->findByAttributes(array(), 'stream_uniq_id=:stream_uniq_id AND user_id=:user_id', array(':stream_uniq_id' => $stream_uniq_id, ':user_id' => $user_id));
                $translate = $this->getTransData($lang_code, $this->studio_id);
                if ($offline_view_log_data) {
                    $current_date = gmdate('Y-m-d H:i:s');
                    Yii::app()->db->createCommand()->update('offline_view_log', array('device_id' => $device_id, 'device_type' => $device_type, 'updated_date' => $current_date), 'id = :id', array(':id' => $offline_view_log_data['id']));
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['stream_uniq_id'] = $offline_view_log_data->stream_uniq_id;
                    $data['request_data'] = $request_data;
                    $data['user_id'] = $offline_view_log_data->user_id;
                    $data['device_id'] = $device_id;
                    $data['access_expiry_time'] = $offline_view_log_data->access_expiry_time;
                    $data['created_date'] = (strtotime($offline_view_log_data->created_date) * 1000);
                    $expiry_date_second = ceil($offline_view_log_data->access_expiry_time / 1000);
                    $current_date_timestamp = strtotime($current_date);
                    $gap_second = $expiry_date_second - $current_date_timestamp;
                    if ($gap_second > 0) {
                        $hour = floor($gap_second / 3600);
                        $rest_seconds = $gap_second % 3600;
                        $minutes = floor($rest_seconds / 60);
                        $hour_value = $hour <= 9 ? '0' . $hour : $hour;
                        $minutes_value = $minutes <= 9 ? '0' . $minutes : $minutes;
                        $disp_time = $hour_value . ':' . $minutes_value;
                    } else {
                        $disp_time = '00:00';
                    }
                    $data['download_complete_msg'] = @$translate['download_complete_msg'] . $disp_time;
                } else {
                    //Get data from movie_streams table
                    $movie_streams_info = movieStreams::model()->findByAttributes(array(), array(
                        'select' => array('id', 'movie_id', 'embed_id'),
                        'condition' => 'embed_id = :stream_uniq_id',
                        'params' => array(':stream_uniq_id' => $stream_uniq_id)
                    ));
                    if ($movie_streams_info) {
                        //call billing function
                        $access_expiry_time = 0;
                        $access_info = Yii::app()->common->getContentExpiryDate($this->studio_id, $user_id, $movie_streams_info->embed_id);
                        $current_date = (gmdate('Y-m-d H:i:s'));
                        $cur_date_timestamp_millisecond = (strtotime($current_date) * 1000);
                        if ($access_info['is_valid']) {
                            $save_flag = 1;
                            $config_value = Yii::app()->db->createCommand()
                                    ->select('config_key,config_value')
                                    ->from('studio_config')
                                    ->where("studio_id=:studio_id AND config_key IN('offline_view_access_period','offline_view_access_period_limit')", array(':studio_id' => $this->studio_id))
                                    ->queryAll();
                            $config_value_arr = CHtml::listData($config_value, 'config_key', 'config_value');
                            if ($access_info['expiry_date'] != '') {
                                //check access period is enabled or not
                                if ($config_value_arr['offline_view_access_period'] == 1) {
                                    //compare expirydate value with config value and take smaller value as expiry time
                                    $config_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $cur_date_timestamp_millisecond);
                                    $billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
                                    $access_expiry_time = ($billing_expiry_time <= $config_expiry_time) ? $billing_expiry_time : $config_expiry_time;
                                } else {
                                    //if access period is not active then take the billing expiry time
                                    $access_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
                                }
                            } else {
                                //check access period is enabled or not
                                if ($config_value_arr['offline_view_access_period'] == 1) {
                                    //get value from config table when expiry date is blank
                                    $access_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $cur_date_timestamp_millisecond);
                                } else {
                                    $save_flag = 0;
                                }
                            }
                            if ($save_flag) {
                                $offline = new OfflineView();
                                $offline->studio_id = $this->studio_id;
                                $offline->movie_id = $movie_streams_info->movie_id;
                                $offline->video_id = $movie_streams_info->id;
                                $offline->stream_uniq_id = $movie_streams_info->embed_id;
                                $offline->user_id = $user_id;
                                $offline->access_expiry_time = $access_expiry_time;
                                $offline->device_id = $device_id;
                                $offline->device_type = $device_type;
                                $offline->created_date = $current_date;
                                $offline->updated_date = $current_date;
                                $offline->save();
                            }
                        }
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['stream_uniq_id'] = $movie_streams_info->embed_id;
                        $data['request_data'] = $request_data;
                        $data['user_id'] = $user_id;
                        $data['device_id'] = $device_id;
                        $data['access_expiry_time'] = ($access_expiry_time) ? $access_expiry_time : -1;
                        $data['created_date'] = $cur_date_timestamp_millisecond;
                        if ($access_expiry_time) {
                            $expiry_date_second = ceil($access_expiry_time / 1000);
                            $current_date = gmdate('Y-m-d H:i:s');
                            $current_date_timestamp = strtotime($current_date);
                            $gap_second = $expiry_date_second - $current_date_timestamp;
                            if ($gap_second > 0) {
                                $hour = floor($gap_second / 3600);
                                $rest_seconds = $gap_second % 3600;
                                $minutes = floor($rest_seconds / 60);
                                $hour_value = $hour <= 9 ? '0' . $hour : $hour;
                                $minutes_value = $minutes <= 9 ? '0' . $minutes : $minutes;
                                $disp_time = $hour_value . ':' . $minutes_value;
                            } else {
                                $disp_time = '00:00';
                            }
                            $data['download_complete_msg'] = @$translate['download_complete_msg'] . $disp_time;
                        } else {
                            $data['download_complete_msg'] = '';
                        }
                    } else {
                        $data['code'] = 411;
                        $data['status'] = "Error";
                        $data['msg'] = "Stream unique id not found";
                    }
                }
            } else {
                $data['code'] = 411;
                $data['status'] = "Error";
                $data['msg'] = "Please provide user id";
            }
        } else {
            $data['code'] = 411;
            $data['status'] = "Error";
            $data['msg'] = "Please provide stream unique id";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

}
