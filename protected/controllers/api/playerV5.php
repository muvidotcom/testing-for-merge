<?php
trait Player {
	/**
	 * @method public getVideoDetails : This function will return details of a video
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String content_uniq_id* : Content unique id
	 * @param String stream_uniq_id* : Stream unique id
	 * @param Integer user_id : User id
	 * @param Integer internet_speed : User internet speed
	 * @param String lang_code : User language Code
	 * 
	*/
     function actionGetVideoDetails() {		                         
        if (isset($_REQUEST['content_uniq_id']) && $_REQUEST['content_uniq_id']!='' && isset($_REQUEST['stream_uniq_id']) && $_REQUEST['stream_uniq_id']!='') {	
			$internetSpeed = (isset($_REQUEST['internet_speed']) && $_REQUEST['internet_speed'] > 0)?$_REQUEST['internet_speed']:0;  
			$contentDetails = Yii::app()->db->createCommand()
							->select('F.id,F.name,F.content_types_id,S.id AS stream_id,S.rolltype AS rollType,S.roll_after AS rollAfter,S.video_resolution,S.enable_ad AS adActive,S.full_movie,S.embed_id,S.is_demo,S.is_converted,F.uniq_id,S.content_key,S.encryption_key,S.thirdparty_url,S.video_management_id,S.is_offline')
							->from('films F, movie_streams S')
							->where('F.id=S.movie_id AND F.uniq_id=:uniq_id AND S.embed_id=:stream_uniq_id',array(':uniq_id' => $_REQUEST['content_uniq_id'], ':stream_uniq_id' => $_REQUEST['stream_uniq_id']))
							->queryRow();						
            if ($contentDetails) {
					$lang_code = @$_REQUEST['lang_code'];
					$translate   = $this->getTransData($lang_code,$this->studio_id);
                    $language_nameArr = array();
                    $domainName = $this->getDomainName();                   
                    $user_id = (isset($_REQUEST['user_id'])&& $_REQUEST['user_id'] > 0)?$_REQUEST['user_id']:0;  					
					$bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
					$folderPath = Yii::app()->common->getFolderPath("",$this->studio_id);
                    $signedBucketPath = $folderPath['signedFolderPath'];
					$streamingRestriction = 0;
                    $getDeviceRestrictionSetByStudio = 0;
					$download_status = 1;
                    $waterMarkOnPlayer = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'embed_watermark');
                    $watermark_details = array();
                    $watermark_details['status'] = 0;
                    if(@$waterMarkOnPlayer['config_value'] == 1){
                        $watermark= WatermarkPlayer::model()->getStudioId($studio_id);
                        $watermark_details['status'] = 1;
                        $watermark_details['email'] = @$watermark['email'];
                        $watermark_details['ip'] = @$watermark['ip'];
                        $watermark_details['date'] = @$watermark['date'];
                    }
                    $data['is_watermark'] = $watermark_details;
                    if($user_id>0){
                        $playLengthArr = Yii::app()->db->createCommand()
								->select('played_length') 
								->from('video_logs')
								->where('user_id=:user_id AND video_id=:stream_id' ,array(':user_id' => $user_id,':stream_id'=>$contentDetails['stream_id']))
								->order('id DESC')
								->limit(1)->queryRow();
						$getDeviceRestriction = StudioConfig::model()->getdeviceRestiction($this->studio_id, $user_id);
						$streamingRestriction = ($getDeviceRestriction > 0)? 1: 0;                       
                        if($getDeviceRestriction != ""){
                            $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($this->studio_id, 'restrict_streaming_device');
                            if(@$getDeviceRestrictionSetByStudioData->config_value != ""){
                                $getDeviceRestrictionSetByStudio = $getDeviceRestrictionSetByStudioData->config_value;
                            } 
                        }
						//offline watch period start
						if ($contentDetails['content_types_id'] != 4) {
							//Check record is available in offline_vie_log table
							$offline_details = OfflineView::model()->findByAttributes(array('studio_id' => $this->studio_id, 'video_id' => $contentDetails['stream_id'], 'user_id' => $user_id));
							if ($offline_details) {
								$current_date_expiry_time = strtotime(gmdate('Y-m-d H:i:s')) * 1000;
								if ($current_date_expiry_time >= $offline_details->access_expiry_time) {
									$download_status = 0;
								}
							} else {
								//call billing function
								$access_info = Yii::app()->common->getContentExpiryDate($this->studio_id, $user_id, $contentDetails['embed_id']);
								if ($access_info['is_valid']) {
									if ($access_info['expiry_date'] != '') {
										$config_value = Yii::app()->db->createCommand()
												->select('config_key,config_value')
												->from('studio_config')
												->where("studio_id=:studio_id AND config_key IN('offline_view_access_period','offline_view_access_period_limit')", array(':studio_id' => $this->studio_id))
												->queryAll();
										$config_value_arr = CHtml::listData($config_value, 'config_key', 'config_value');
										$current_date_expiry_time = strtotime(gmdate('Y-m-d H:i:s')) * 1000;
										if ($config_value_arr['offline_view_access_period'] == 1) {
											$config_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $current_date_expiry_time);
											$billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
											$access_expiry_time = ($billing_expiry_time <= $config_expiry_time) ? $billing_expiry_time : $config_expiry_time;
											if ($current_date_expiry_time >= $access_expiry_time) {
												$download_status = 0;
											}
										} else {
											$billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
											if ($current_date_expiry_time >= $billing_expiry_time) {
												$download_status = 0;
											}
										}
									}
								} else {
									$download_status = 0;
								}
							}
						}
					//offline watch period end
                    } 						
                    $played_length = !empty($playLengthArr['played_length']) ? $playLengthArr['played_length']: null;
                    //Get trailer info from movie_trailer table                                      
                    $trailerData = movieTrailer::model()->find('movie_id=:movie_id', array(':movie_id'=>$contentDetails['id']));
                    if($trailerData['trailer_file_name'] != ''){
                        $embedTrailerUrl = $domainName.'/embedTrailer/'.$contentDetails['uniq_id'];
                        if($trailerData['third_party_url'] != ''){
                            $trailerThirdpartyUrl = $this->getSrcFromThirdPartyUrl($trailerData['third_party_url']);
                        }else if($trailerData['video_remote_url'] != ''){
                            $trailerUrl = $this->getTrailer($contentDetails['id'],0,"",$this->studio_id);
                        }  
                    }                    
                    //End of trailer part
                   if($contentDetails['content_types_id'] == 4){
                        $Ldata = Livestream::model()->find("studio_id=:studio_id AND movie_id=:movie_id", array(':studio_id'=>$this->studio_id,':movie_id'=>$contentDetails['id']));
                        if($Ldata && $Ldata->feed_url){
                            $embedUrl= $domainName.'/embed/livestream/'.$contentDetails['uniq_id'];
                            $fullmovie_path = $Ldata->feed_url;
                            if ($Ldata->feed_type == 2) {
                                $streamUrlExplode  = explode("/",str_replace("rtmp://","",$Ldata->stream_url));
                                $nginxServerIp = @$streamUrlExplode[0];
                                $nginxServerDetails = Yii::app()->aws->getNginxServerDetails($nginxServerIp); 
                                if (strpos($Ldata->feed_url, 'rtmp://' . $nginxServerIp . '/live') !== false) {
                                    $fullmovie_path = str_replace("rtmp://" . $nginxServerIp . "/live", @$nginxServerDetails['nginxserverlivecloudfronturl'], $Ldata->feed_url) . "/index.m3u8";
                                } else if (strpos($Ldata->feed_url, 'rtmp://' . $nginxServerIp . '/record') !== false) {
                                    $fullmovie_path = str_replace("rtmp://" . $nginxServerIp . "/record", @$nginxServerDetails['nginxserverrecordcloudfronturl'], $Ldata->feed_url) . "/index.m3u8";
                                }
                            }
                        }
                    }else{
                        if($contentDetails['full_movie']!=''){
								$embedUrl = $domainName.'/embed/'.$contentDetails['embed_id'];
                           if($contentDetails['thirdparty_url'] != ''){
                                $thirdparty_url = $this->getSrcFromThirdPartyUrl($contentDetails['thirdparty_url']);
                           }else{                            
                              //Check thirdparty url is not present then pass video url with details                                                           
                                $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/movie_stream/full_movie/" . $contentDetails['stream_id'] . "/" . $contentDetails['full_movie'];
                                if($contentDetails['is_demo'] == 1){
                                    $fullmovie_path = CDN_HTTP . $bucketInfo['cloudfront_url'] . "/" . $bucketInfo['signedFolderPath'] . "uploads/small.mp4";
                                }                                                                
                                $multipleVideo = $this->getvideoResolution($contentDetails['video_resolution'], $fullmovie_path);
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $internetSpeed);
                                $videoToBeplayed12 = explode(",", $videoToBeplayed);
                                $fullmovie_path = $videoToBeplayed12[0];
								$this->items['video_resolution'] = $videoToBeplayed12[1];                                
                                $clfntUrl = 'https://' . $bucketInfo['cloudfront_url'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000,$this->studio_id);
                                //added by prakash for signed url on 29th march 2017
                                $multipleVideoSignedArr=array();
                                foreach($multipleVideo as $resKey => $videoPath){
                                    $subArr = array();
                                    if($resKey){
                                        $subArr['resolution'] = $resKey;
                                        $subArr['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, "", "", 60000,$this->studio_id);
                                        $multipleVideoSignedArr[] = $subArr;
                                    }                                                               
                                }                                                                                               
								$this->items['video_details'] = $multipleVideoSignedArr;
								$this->items['new_video_url'] = $multipleVideoForUnsigned[144];
                                if($contentDetails['content_key'] && $contentDetails['encryption_key']){                                    
                                    $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$contentDetails['stream_id'].'/stream.mpd';
                                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($this->studio_id,'drm_cloudfront_url');
                                    if($getStudioConfig){
                                        if(@$getStudioConfig['config_value'] != ''){
                                            $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$contentDetails['stream_id'].'/stream.mpd';
                                        }
                                    }
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL,MS3_URL);
                                    curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&contentId=urn:marlin:kid:" . $contentDetails['encryption_key'] . "&contentKey=" . $contentDetails['content_key'] . "&ms3Scheme=true&contentURL=" . $fullmovie_path);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
                                    $server_output = curl_exec ($ch);
									$this->items['studio_approved_url'] = $server_output;
									$this->items['is_offline'] = $contentDetails['is_offline'];                                    
                                    curl_close ($ch);
									//get LicenceUrl									
									$licence_url = $this->generateExplayWideVineToken($contentDetails['content_key'],$contentDetails['encryption_key']);									
									$this->items['license_url'] = $licence_url? $licence_url: '';
                                }                            
                           }
                        }
                    }
               //Subtitle section start
                if($contentDetails['video_management_id'] > 0){						
                        $getVideoSubtitle = Yii::app()->db->createCommand()
											->select('vs.id AS subId,vs.filename,ls.name,ls.code,vs.video_gallery_id,') 
											->from('video_subtitle vs,language_subtitle ls')
											->where('vs.language_subtitle_id = ls.id AND vs.video_gallery_id=:video_management_id',array(':video_management_id' => $contentDetails['video_management_id']))
											->queryAll();												
                        if($getVideoSubtitle) {     								
                            $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];                                
                            foreach ($getVideoSubtitle as $subtitlekey => $getVideoSubtitlevalue) {                                
                                $filePth = $clfntUrl . "/" . $bucketInfo['signedFolderPath'] . "subtitle/" . $getVideoSubtitlevalue['subId'] . "/" . $getVideoSubtitlevalue['filename'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                                $language_nameArr[$subtitlekey]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1, "", 60000,$this->studio_id);
                                $language_nameArr[$subtitlekey]['code'] = $getVideoSubtitlevalue['code'];
								if (@$translate[$getVideoSubtitlevalue['name']]!='')
									$language_nameArr[$subtitlekey]['language'] = @$translate[$getVideoSubtitlevalue['name']];  
								else
									$language_nameArr[$subtitlekey]['language'] = $getVideoSubtitlevalue['name'];								                             
                            } 
                        }
                }
                 // End of subtitle section.
				//Ads section 
				$ads =array();
				$ads['ad_active'] = $contentDetails['adActive'];
				if($contentDetails['adActive']){
					$adsTime=array();
					$ads['ad_network'] = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($this->studio_id);										
					$rollType = $contentDetails['rollType'];
					$adsTime['start'] = ($rollType == '7' || $rollType == '5' || $rollType == '3' || $rollType == '1')?1:0;
					$adsTime['mid'] = ($rollType == '7' || $rollType == '3' || $rollType == '6' || $rollType == '2')?1:0;
					$adsTime['end'] = ($rollType == '7' || $rollType == '5' || $rollType == '6' || $rollType == '4')?1:0;					
					$roll_after = explode(',', $contentDetails['rollAfter']);
					if (in_array("end", $roll_after)) {
						array_pop($roll_after);
					}
					if (in_array("0", $roll_after)) {						
						array_shift($roll_after);						
					}
					//convert ads streaming time to seconds
					$time_seconds ="";
					foreach($roll_after as $k=>$time){
						if($time_seconds=="")
							$time_seconds = Yii::app()->general->getHHMMSSToseconds($time);
						else
							$time_seconds.=','.Yii::app()->general->getHHMMSSToseconds($time);
					}
					$adsTime['midroll_values'] = $time_seconds;
					if($ads['ad_network'][1]['ad_network_id']>0 && $ads['ad_network'][1]['ad_network_id']!=3){
						$ads['ads_time'] = $adsTime;
					}
				}
                $this->code = 200;                 
				$this->items['video_url'] = @$fullmovie_path;
				$this->items['thirdparty_url'] = @$thirdparty_url;
				$this->items['embed_url'] = @$embedUrl;
				$this->items['trailer_url'] = @$trailerUrl;
				$this->items['trailer_thirdparty_url'] = @$trailerThirdpartyUrl;
				$this->items['embed_trailer_url'] = @$embedTrailerUrl;
				$this->items['played_length'] = @$played_length;
				$this->items['subtitle'] = @$language_nameArr;
				$this->items['streaming_restriction'] = $streamingRestriction;
                $this->items['no_streaming_device'] = $getDeviceRestrictionSetByStudio;
				$this->items['restrict_streaming_msg'] = @$translate['restrict-streaming-device'];
				$this->items['ads_details'] = $ads;
				if($user_id > 0){
					$this->items['download_status'] = $download_status;
				}
            } else 
                $this->code = 629;                 
        } else 
			$this->code = 628;             
    }
	
	/**
	 * @method public StartUpstream : This function will start upstreaming for a particular channel id
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param Integer movie_id* : movie id (Auto-increment id of films table)
	 * @param String lang_code : User language Code
	 * 
	*/	 
    function actionStartUpstream() {		
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_start_time = $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');                
                $lsuser->save();
				$this->code = 200;				            
            } else {
				$this->code = 608;             
            }
        } else {
			$this->code = 644;          
        }      
    }  
	/**
	 * @method public GetLiveUserlist : This function will return all broadcasters stream( or content) who are live now
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 	 
	 * @param String lang_code : User language Code
	 * 
	*/	 	
    function actionGetLiveUserlist() {   				
		$lsuser = Yii::app()->db->createCommand()
                            ->select('name,nick_name,channel_title as title,pull_url AS hls_path,push_url AS rtmp_path') 
                            ->from('livestream_users')
                            ->where('studio_id =:studio_id AND is_online=1' ,array(':studio_id' => $this->studio_id))
                            ->order('id ASC')
							->queryAll();		
		$this->code = 200;
		$this->items['users'] = $lsuser;				       				 
    }
	/**
	 * @method public getChannelOnlineStatus : To check if a particular broadcaster or feed is online or not
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param String permalink* : Permalink of content	 
	 * @param String lang_code : User language Code
	 * 
	*/	
    function actiongetChannelOnlineStatus() {
		$data=array();
		if (isset($_REQUEST['permalink']) && $_REQUEST['permalink']!='') {
            $movie = Yii::app()->db->createCommand()->select('f.content_types_id,ls.is_online')->from('films f ,livestream ls ')
                    ->where('ls.movie_id = f.id AND f.content_types_id=4 AND f.permalink=:permalink AND  f.studio_id=:studio_id', array(':permalink' => $_REQUEST['permalink'], ':studio_id' => $this->studio_id))
                    ->queryRow();			
			if($movie){
					$this->code = 200;                 	
                    $data['is_online'] = $movie["is_online"] ? (int)$movie["is_online"] : 0;
                    $data['msg'] = $movie["is_online"] ? "Online" : 'Offline';
					$this->items = $data;
            } else {
				$this->code = 605;                
			}
        } else {
			$this->code = 604;			
		}				     
    }
	/**
	 * @method public StopUpstream : Stop upstream a particular channel id
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param Integer movie_id* : movie id (Auto-increment id of films table)	 
	 * @param String lang_code : User language Code
	 * 
	*/		
    function actionStopUpstream() {
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 0;
                $lsuser->stream_start_time = $lsuser->stream_update_time= '';                
                $lsuser->save();
                $this->code = 200;               
            } else {
                $this->code = 680;                
            }
        } else {
            $this->code = 644;           
        }       
    }
	
	/**
	 * @method public ChannelStatus : To inform backend that a particular channel is active
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param Integer movie_id* : movie id (Auto-increment id of films table)	 
	 * @param String lang_code : User language Code
	 * 
	*/		
    function actionChannelStatus() {
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']>0) {
            $lsuser = Livestream::model()->find('movie_id=:movie_id AND studio_id =:studio_id', array(':movie_id' => $_REQUEST['movie_id'], ':studio_id' => $this->studio_id));
            if ($lsuser) {
                $lsuser->is_online = 1;
                $lsuser->stream_update_time = gmdate('Y-m-d H:i:s');
                $lsuser->save();
                $this->code = 200;                                
            } else {
                $this->code = 680;                
            }
        } else {
            $this->code = 644;            
        }        
    }
	
	/**
	 * @method public VideoLogs : This function will store video log data and returns log id 
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String movie_id* : Content unique id
	 * @param String ip_address* : End user ip address
	 * @param Integer user_id : User id
	 * @param Integer device_type : Device type(1->web 2->android 3->ios 4->Roku)
	 * @param String episode_id : Stream unique id
	 * @param Integer season_id : Season id
	 * @param Float played_length :Played length value in second
	 * @param Float video_length :Video Duration value in second
	 * @param String watch_status : Watch status of played video(start,halfplay,complete)
	 * @param Integer content_type : To know video or trailer (1->Normal video,2->trailer)
	 * @param Integer is_streaming_restriction : Restrict no of simultaneous streaming devices is enabled or not (0/1)
	 * @param Integer restrict_stream_id : Auto-increment id of restrict_device table
	 * @param Integer is_active_stream_closed : Delete data from restrict_device table when value is 1 (0/1)
	 * @param Integer log_id : Auto-increment id of video_logs table	
	 * @param String lang_code : User language Code
	 * 
	*/	
    public function actionVideoLogs() {
		$movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
        if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {						
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$played_length = $_REQUEST['played_length'];
						$video_length = isset($_REQUEST['video_length']) ? $_REQUEST['video_length'] : 0;
						$watch_status = $_REQUEST['watch_status'];
						$device_type = $_REQUEST['device_type'];
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$restrictDeviceId = 0;
						if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
							$restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
							if(@$_REQUEST['is_active_stream_closed'] == 1){
								RestrictDevice::model()->deleteData($restrictStreamId);
							} else {
								$postData = array();
								$postData['id'] = $restrictStreamId;
								$postData['studio_id'] = $studio_id;
								$postData['user_id'] = $user_id;
								$postData['movie_stream_id'] = $stream_id;
								$restrictDeviceId = RestrictDevice::model()->dataSave($postData);
							}
						}
						$video_log = new VideoLogs();
						$log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;

						$trailer_id='';
						if($content_type == 2){
							$trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							$trailer_id=$trailerData->id;
							$stream_id=0;
						}
						$video_log->trailer_id = $trailer_id;
						
						if ($log_id > 0) {
							$video_log = VideoLogs::model()->findByPk($log_id);
							$video_log->updated_date = new CDbExpression("NOW()");
							$video_log->played_length = $played_length;
						} else {
							$video_log->created_date = new CDbExpression("NOW()");
							$video_log->ip = $ip_address;
							$video_log->video_length = $video_length;
							$video_log->device_type = $device_type;
						}
						$video_log->movie_id = $movie_id;
						$video_log->video_id = $stream_id;
						$video_log->user_id = $user_id;
						$video_log->device_id = $device_id;
						$video_log->content_type = $content_type;
						$video_log->studio_id = $studio_id;
						$video_log->watch_status = $watch_status;
						if ($video_log->save()) {
							$this->code = 200;														
							$this->items['log_id'] = $video_log->id;
							$this->items['restrict_stream_id'] = $restrictDeviceId;
						} else 						
							$this->code = 500;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635; 	
		}else
			$this->code = 684; 		
    }
	/**
	 * @method public VideoLogNew : This function will store video log data in both video_logs and video_log_temp table and returns log_id and log_temp_id
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String movie_id* : Content unique id
	 * @param String ip_address* : End user ip address
	 * @param Integer user_id : User id
	 * @param Integer device_type : Device type(1->web 2->android 3->ios 4->Roku)
	 * @param String episode_id : Stream unique id
	 * @param Integer season_id : Season id
	 * @param Float played_length :Played length value in second
	 * @param Float video_length :Video Duration value in second
	 * @param String watch_status : Watch status of played video(start,halfplay,complete)
	 * @param Integer content_type : To know video or trailer (1->Normal video,2->trailer)
	 * @param Integer is_streaming_restriction : Restrict no of simultaneous streaming devices is enabled or not (0/1)
	 * @param Integer restrict_stream_id : Auto-increment id of restrict_device table
	 * @param Integer is_active_stream_closed : Delete data from restrict_device table when value is 1 (0/1)
	 * @param Integer log_id : Auto-increment id of video_logs table
	 * @param Integer log_temp_id : Auto-increment id of video_log_temp table
	 * @param Float resume_time : current time of player	
	 * @param String lang_code : User language Code
	 * @param Integer enable_watch_duration :Value is (0 or 1)Enable watch duration is enabled or not 
	 * 
	*/		
	public function actionVideoLogNew() {    		
		$movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
        if($movie_code){
            $ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					if($user_id){
						$add_video_log = SdkUser::model()->findByPk($user_id)->add_video_log;
					}else{
						$add_video_log = 1;
					}
					$isAllowed = $device_id = 0;					
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $_REQUEST['user_id'];
					}
					if ($add_video_log || $isAllowed) {						
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) ) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;												
						//Get stream Id
						$stream_id = 0;						
						if (trim($movie_code) != '0' && trim($stream_code) != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);							
						} else if (trim($movie_code) != '0' && trim($stream_code) == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);							
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);							
						}						
						$parameters = array();
						$parameters['movie_id'] = $movie_id;
						$parameters['stream_id'] = $stream_id;
						$parameters['studio_id'] = $studio_id;
						$activePlayCount = VideoLogs::model()->activePlayLog($parameters);

						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$restrictDeviceId = 0;
						if($user_id > 0 && @$_REQUEST['is_streaming_restriction'] == 1) {
							$restrictStreamId = (isset($_REQUEST['restrict_stream_id'])) ? $_REQUEST['restrict_stream_id'] : 0;
							if(@$_REQUEST['is_active_stream_closed'] == 1){
								RestrictDevice::model()->deleteData($restrictStreamId);
							} else {
								$postData = array();
								$postData['id'] = $restrictStreamId;
								$postData['studio_id'] = $studio_id;
								$postData['user_id'] = $user_id;
								$postData['movie_stream_id'] = $stream_id;
								$restrictDeviceId = RestrictDevice::model()->dataSave($postData);
							}
						}
						$dataForSave = array();
						$dataForSave = $_REQUEST;
						$dataForSave['movie_id'] = $movie_id;
						$dataForSave['stream_id'] = $stream_id;
						$dataForSave['studio_id'] = $studio_id;
						$dataForSave['user_id'] = $user_id;
						$dataForSave['ip_address'] = $ip_address;
						$dataForSave['trailer_id'] = "";
						$dataForSave['status'] = $_REQUEST['watch_status'];
						$dataForSave['content_type'] = $content_type;
						if($content_type == 2){
							$trailerData = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							$dataForSave['trailer_id']=$trailerData->id;
							$dataForSave['stream_id']="";
						}
						$video_log_id = VideoLogs::model()->dataSave($dataForSave);
						$avail_time = 1;
						if(isset($_REQUEST['enable_watch_duration']) && $_REQUEST['enable_watch_duration'] == 1 && $user_id > 0) {
							$sum = Yii::app()->db->createCommand()
							   ->select('sum(played_length)')
							   ->from('video_logs t')
							   ->where('t.user_id ='.$user_id. ' and date(t.created_date )="'. date("Y-m-d").'" and t.is_watch_durationenabled = 1')
							   ->queryScalar();
							$watch_duration_sec = Yii::app()->custom->restrictDevices($studio_id, 'daily_watch_duration');
							if($watch_duration_sec <= $sum){
								$avail_time = 0;
							}
						}
						if ($video_log_id) {
							$this->code = 200;														
							$this->items['log_id'] = @$video_log_id[1];
							$this->items['log_temp_id'] = @$video_log_id[0];
							$this->items['restrict_stream_id'] = (string)$restrictDeviceId;
							$this->items['no_of_views'] = @$activePlayCount;							
							$this->items['avail_time'] = (string)$avail_time;							
						} else 
							$this->code = 500;
					} else 
						$this->code = 637;
				} else 
					$this->code = 636;
			}else
				$this->code = 635;
        } else 
			$this->code = 684; 
    }
	/**
	 * @method public BufferLogs : This function will store store Buffer log data in bandwidth_log table and returns buffer_log_id and buffer_log_unique_id
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String movie_id* : Content unique id
	 * @param String ip_address* : End user ip address
	 * @param Integer user_id : User id
	 * @param Integer device_type : Device type(1->web 2->android 3->ios 4->Roku)	
	 * @param String episode_id : Stream unique id
	 * @param Integer season_id : Season id	 
	 * @param String resolution : Resolution value(BEST,360 etc)
	 * @param Float start_time : Start time value in second	
	 * @param Float end_time : End time value in second
	 * @param String buffer_log_unique_id : Buffer log unique is the unique id of bandwidth_log table
	 * @param Integer buffer_log_id : Auto-increment id of bandwidth_log table
	 * @param Integer content_type : To know video or trailer (1->Normal video,2->trailer)
	 * @param Float downloaded_bandwidth : Bandwidth consumed for offline video
	 * @param String video_type : Video type value (mped_dash or "").When value is mped_dash then provides total_bandwidth
	 * @param Float total_bandwidth :Consumed bandwidth value
	 * @param Integer location : When value is 0 then find country,city,state,country etc info from ip-address
	 * @param String lang_code : User language Code	 
	*/			
    public function actionBufferLogs() {
        $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
		if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$device_type = $_REQUEST['device_type'];
						$resolution = $_REQUEST['resolution'];
						$start_time = $_REQUEST['start_time'];
						$end_time = $_REQUEST['end_time'];
						$log_unique_id = (isset($_REQUEST['buffer_log_unique_id']) && trim(($_REQUEST['buffer_log_unique_id']))) ? $_REQUEST['buffer_log_unique_id'] : '';
						$log_id = (isset($_REQUEST['buffer_log_id']) && trim(($_REQUEST['buffer_log_id']))) ? $_REQUEST['buffer_log_id'] : '';
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$played_time = $end_time - $start_time;
						$bandwidthType = 1;
						if (@$_REQUEST['downloaded_bandwidth'] != '') {
							$bandwidth_used = @$_REQUEST['downloaded_bandwidth'];
							$bandwidthType = 2;
						} else if(strtolower(@$_REQUEST['video_type']) == 'mped_dash'){
							//mped_dash
							$bandwidth_used = @$_REQUEST['total_bandwidth'];
						}else{
							if($content_type == 2){
								$movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							} else{
								$movie_stream_data = movieStreams::model()->findByPk($stream_id);
							}
							$res_size = json_decode($movie_stream_data->resolution_size, true);
							$video_duration = explode(':', $movie_stream_data->video_duration);
							$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
							$size = $res_size[$resolution];
							$bandwidth_used = ($size / $duration) * $played_time;
						}
						if (isset($log_id) && $log_id) {							
							$buff_log = BufferLogs::model()->findByPk($log_id);
							if (isset($buff_log) && !empty($buff_log)) {
								$buff_log->end_time = $end_time;
								$buff_log->buffer_size = $bandwidth_used;
								$buff_log->played_time = $end_time - $buff_log->start_time;
								$buff_log->save();
								$buff_log_id = $log_id;
								$unique_id = $log_unique_id;
							}
						} else {
							$new_buff_log = new BufferLogs();
							if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
								if ($device_type == 4) {
									$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id),array('order'=>'id DESC'));
								} else {
									$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id),array('order'=>'id DESC'));
								}								
								$new_buff_log->city = $buff_log->city;
								$new_buff_log->region = $buff_log->region;
								$new_buff_log->country = $buff_log->country;
								$new_buff_log->country_code = $buff_log->country_code;
								$new_buff_log->continent_code = $buff_log->continent_code;
								$new_buff_log->latitude = $buff_log->latitude;
								$new_buff_log->longitude = $buff_log->longitude;
							}else{
								$ip_address = Yii::app()->request->getUserHostAddress();
								$geo_data = new EGeoIP();
								$geo_data->locate($ip_address);								
								$new_buff_log->city = @$geo_data->getCity();
								$new_buff_log->region = @$geo_data->getRegion();
								$new_buff_log->country = @$geo_data->getCountryName();
								$new_buff_log->country_code = @$geo_data->getCountryCode();
								$new_buff_log->continent_code = @$geo_data->getContinentCode();
								$new_buff_log->latitude = @$geo_data->getLatitude();
								$new_buff_log->longitude = @$geo_data->getLongitude();
							}
							$unique_id = md5(uniqid(rand(), true));							
							$new_buff_log->studio_id = $studio_id;
							$new_buff_log->unique_id = $unique_id;
							$new_buff_log->user_id = $user_id;
							$new_buff_log->device_id = $device_id;
							$new_buff_log->movie_id = $movie_id;
							$new_buff_log->video_id = $stream_id;
							$new_buff_log->resolution = $resolution;
							$new_buff_log->start_time = $start_time;
							$new_buff_log->end_time = $end_time;
							$new_buff_log->played_time = $played_time;
							$new_buff_log->buffer_size = $bandwidth_used;							
							$new_buff_log->device_type = $device_type;
							$new_buff_log->content_type = $content_type;
							$new_buff_log->ip = $ip_address;
							$new_buff_log->created_date = date('Y-m-d H:i:s');
							$new_buff_log->bandwidth_type = $bandwidthType;
							$new_buff_log->save();
							$buff_log_id = $new_buff_log->id;
						}
						$this->code = 200;		
						$this->items['location'] = 1;
						$this->items['buffer_log_id'] = $buff_log_id;
						$this->items['buffer_log_unique_id'] = $unique_id;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635;
		}else
			$this->code = 684;
    }
	/**
	 * @method public UpdateBufferLogs : This function will store Buffer log data in bandwidth_log table and returns buffer_log_id and buffer_log_unique_id
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String movie_id* : Content unique id
	 * @param String ip_address* : End user ip address
	 * @param Integer user_id : User id
	 * @param Integer device_type : Device type(1->web 2->android 3->ios 4->Roku)	
	 * @param String episode_id : Stream unique id
	 * @param Integer season_id : Season id	 
	 * @param String resolution : Resolution value(BEST,360 etc)
	 * @param Float start_time : Start time value in second	
	 * @param Float end_time : End time value in second
	 * @param Integer content_type : To know video or trailer (1->Normal video,2->trailer)	 	
	 * @param Integer location : When value is 0 then find country,city,state,country etc info from ip-address
	 * @param String lang_code : User language Code	 
	*/				
    public function actionUpdateBufferLogs() {
       $movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
		if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)) {
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed) {
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$device_type = $_REQUEST['device_type'];
						$resolution = $_REQUEST['resolution'];
						$start_time = $_REQUEST['start_time'];
						$end_time = $_REQUEST['end_time'];
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$movie_stream_data = movieStreams::model()->findByPk($stream_id);
						if($content_type == 2){
							$movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
						}
						$res_size = json_decode($movie_stream_data->resolution_size, true);
						$video_duration = explode(':', $movie_stream_data->video_duration);
						$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
						$size = $res_size[$resolution];
						$played_time = $end_time - $start_time;
						$bandwidth_used = ($size / $duration) * $played_time;
						$unique_id = md5(uniqid(rand(), true));
						$new_buff_log = new BufferLogs();
						if (isset($_REQUEST['location']) && $_REQUEST['location'] == 1) {
							if ($device_type == 4) {
								$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'device_id' => $device_id));
							} else {
								$buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
							}							
							$new_buff_log->city = $buff_log->city;
							$new_buff_log->region = $buff_log->region;
							$new_buff_log->country = $buff_log->country;
							$new_buff_log->country_code = $buff_log->country_code;
							$new_buff_log->continent_code = $buff_log->continent_code;
							$new_buff_log->latitude = $buff_log->latitude;
							$new_buff_log->longitude = $buff_log->longitude;
						} else {
							$ip_address = Yii::app()->request->getUserHostAddress();
							$geo_data = new EGeoIP();
							$geo_data->locate($ip_address);							
							$new_buff_log->city = @$geo_data->getCity();
							$new_buff_log->region = @$geo_data->getRegion();
							$new_buff_log->country = @$geo_data->getCountryName();
							$new_buff_log->country_code = @$geo_data->getCountryCode();
							$new_buff_log->continent_code = @$geo_data->getContinentCode();
							$new_buff_log->latitude = @$geo_data->getLatitude();
							$new_buff_log->longitude = @$geo_data->getLongitude();
						}
						$new_buff_log->studio_id = $studio_id;
						$new_buff_log->unique_id = $unique_id;
						$new_buff_log->user_id = $user_id;
						$new_buff_log->device_id = $device_id;
						$new_buff_log->movie_id = $movie_id;
						$new_buff_log->video_id = $stream_id;
						$new_buff_log->resolution = $resolution;
						$new_buff_log->start_time = $start_time;
						$new_buff_log->end_time = $end_time;
						$new_buff_log->played_time = $played_time;
						$new_buff_log->buffer_size = $bandwidth_used;							
						$new_buff_log->device_type = $device_type;
						$new_buff_log->content_type = $content_type;
						$new_buff_log->ip = $ip_address;						
						$new_buff_log->created_date = date('Y-m-d H:i:s');
						$new_buff_log->save();
						$buff_log_id = $new_buff_log->id;						
						$this->code = 200;								
						$this->items['buffer_log_id'] = $buff_log_id;
						$this->items['buffer_log_unique_id'] = $unique_id;	
						$this->items['location'] = 1;						
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635;
		}else
			$this->code = 684;
    }
	
	/**
	 * @method public GetEmbedUrl : This function returns embed url of stream
	 * @return json Return the json array of results
	 * @author Srutikant<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param Integer movie_stream_id* : movie stream id (Auto-increment id of movie_streams table)	 
	 * @param String lang_code : User language Code
	 * 
	*/	
    public function actionGetEmbedUrl() {
		if(isset($_REQUEST['movie_stream_id']) && $_REQUEST['movie_stream_id']>0){
			$movieStreamData = Yii::app()->db->createCommand()
							->select('s.is_embed_white_labled,s.domain,ms.embed_id')
							->from('studios s')
							->join('movie_streams ms', 's.id=ms.studio_id')
							->where('s.id=:studio_id AND ms.id=:stream_id', array(':studio_id'=>$this->studio_id,':stream_id'=>$_REQUEST['movie_stream_id']))
							->queryRow();
			if($movieStreamData){
				$domainName = Yii::app()->getBaseUrl(TRUE);
				if($movieStreamData['is_embed_white_labled'])
					$domainName = $movieStreamData['domain']? 'http://'.$movieStreamData['domain']:$domainName; 				
				$this->code = 200;
				$this->items['embed_url']=$domainName .'/embed/'.$movieStreamData['embed_id'];			
			}else
				$this->code = 682;			
		}else
			$this->code = 681;		
    }
	
	/**
	 * @method public getMarlinBBOffline : This function returns file path,token of offline video
	 * @return json Return the json array of results
	 * @author Srutikant<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param String stream_unique_id* : stream uniq id (embed_id of movie_streams table)	 
	 * @param String lang_code : User language Code
	 */	
    public function actiongetMarlinBBOffline(){
        $studio_id = $this->studio_id;
        $stream_unique_id = @$_REQUEST['stream_unique_id'];
        if($stream_unique_id != ''){
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $stream_unique_id));
            if($streams){
                 if(@$streams->encryption_key != '' && @$streams->content_key != '' &&  @$streams->is_offline == 1){
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id,'drm_cloudfront_url');
                    $fileName = substr($streams->full_movie, 0, -4).'.mlv';
                    if(@$getStudioConfig['config_value'] != ''){
                        $fullmovie_path = 'https://'.$getStudioConfig['config_value'].'/uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    } else{
                        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
                        $folderPath = Yii::app()->common->getFolderPath("",$studio_id);
                        $signedBucketPath = $folderPath['signedFolderPath'];
                        $fullmovie_path = 'https://'.$bucketInfo['bucket_name'].'.'.$bucketInfo['s3url'].'/'.$signedBucketPath.'uploads/movie_stream/full_movie/'.$streams->id.'/'.$fileName;
                    }
					if($streams->is_multibitrate_offline == 1){ 
                        $multOfflineVal = $this->getvideoResolution($streams->video_resolution, $fullmovie_path);
                        foreach($multOfflineVal as $resKey => $videoPath){
                            $subArr['resolution'] = $resKey;
                            $subArr['url'] = $videoPath;
                            $token['multiple_resolution'][] = $subArr;
                        }
                    }
					$token['file'] = $fullmovie_path;
                    $token['token'] = file_get_contents('https://bb-gen.test.expressplay.com/hms/bb/token?customerAuthenticator=200988,058524ac8dc0459cb9e4d497136563ba&actionTokenType=1&rightsType=BuyToOwn&outputControlOverride=urn:marlin:organization:intertrust:wudo,ImageConstraintLevel,0&cookie=MY_TEST01&contentId='.$streams->content_key.'&contentKey='.$streams->encryption_key);
					$this->code = 200;
					$this->items = $token;					
                }else
					$this->code = 693;
            }else
				$this->code = 693;
        }else
			$this->code = 694;
    }  	
	
	/**
 * @method public assignBroadCastToContent() It will create content against broadcaster under a category. 
 * @author Gayadhar<support@muvi.com>
 * @param String authToken* : Auth token of the studio
 * @param string $content_name*: Name of the BroadCast
 * @param Integer category_id* : Id of the content category
 * @param Integer user_id* : Logged in user id
 * @param text description : Description of the content
 * @param String content_name* : Name of the content
 * @param String lang_code : User language Code	 
 * @return HTML 
 */	
	function actionAssignBroadCastToContent(){
		$category_id = (isset($_REQUEST['category_id']) && $_REQUEST['category_id']>0)?$_REQUEST['category_id']:0;
		$user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id']>0)?$_REQUEST['user_id']:0;
		$content_name = isset($_REQUEST['content_name'])?$_REQUEST['content_name']:'';
		if($category_id && $user_id && $content_name){
			$studio_id = $this->studio_id;			
			$Films = new Film();
			$data['name'] = $_REQUEST['content_name'];
			$data['story'] = @$_REQUEST['description'];
			$catData = Yii::app()->db->CreateCommand("SELECT id,binary_value,category_name,permalink FROM content_category WHERE id={$category_id} AND studio_id={$this->studio_id}")->queryRow();			
			$data['content_category_value'] = array($catData['id']);
			$data['parent_content_type'] = 2;
			$data['content_types_id'] = 4;
			$movie = $Films->addContent($data,$this->studio_id);
			$movie_id = $movie['id'];			
			//Live stream set up
            $streamUrl = $movie['permalink'].'-'.strtotime(date("Y-m-d H:i:s"));
            $length = 8;
            $userName = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            $enc = new bCrypt();
            $passwordEncrypt = $enc->hash($password);
            $ch = curl_init();
            $nginxServerIp = Yii::app()->aws->getNginxServerIp($studio_id); 
            curl_setopt($ch, CURLOPT_URL, 'http://'.$nginxServerIp.'/auth.php');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=save&is_active=1&stream_name=".$streamUrl."&user_name=".$userName."&password=".$passwordEncrypt."&serverUrl=".Yii::app()->getBaseUrl(TRUE)."/conversion/stopLiveStreaming?movie_id".$movie_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = trim(curl_exec($ch));
            curl_close($ch);
			if($server_output != 'success'){
                Film::model()->deleteByPk($movie_id);
				$this->code = 778;
				$this->items = $data;
            }else{
				//Adding permalink to url routing 
				$urlRouts['permalink'] = $movie['permalink'];
				$urlRouts['mapped_url'] = '/movie/show/content_id/'.$movie_id;
				$urlRoutObj = new UrlRouting();
				$urlRoutings = $urlRoutObj->addUrlRouting($urlRouts,$studio_id);

				//Insert Into Movie streams table
				$MovieStreams = new movieStreams();
				$MovieStreams->studio_id = $studio_id;
				$MovieStreams->movie_id = $movie_id;
				$MovieStreams->is_episode = 0;
				$MovieStreams->created_date = gmdate('Y-m-d H:i:s');
				$MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
				$embed_uniqid = Yii::app()->common->generateUniqNumber();
				$MovieStreams->embed_id = $embed_uniqid; 
				$publish_date = NULL;
				$MovieStreams->content_publish_date = $publish_date;
				$MovieStreams->save();
				//Adding the feed infos into live Stream table
				$feedData['feed_url'] = 'rtmp://'.$nginxServerIp.'/live/'.$streamUrl;
				$feedData['stream_url'] = 'rtmp://'.$nginxServerIp.'/live';
				$feedData['stream_key'] = $streamUrl."?user=".$userName."&pass=".$password;
				$feedData['feed_type'] = 2;
				$feedData['is_recording'] = 0;
				$feedData['method_type'] = 'push';
				$feedData['start_streaming'] = 1;

				$livestream = new Livestream();
				$livestream->saveFeeds($feedData,$studio_id,$movie_id,$user_id);
				if(HOST_IP !='127.0.0.1'){
					$solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
					$solrArr['content_id'] = $movie_id;
					$solrArr['stream_id'] = $MovieStreams->id;
					$solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
					$solrArr['is_episode'] = 0;
					$solrArr['name'] = $movie['name'];
					$solrArr['permalink'] = $movie['permalink'];
					$solrArr['studio_id'] =  $studio_id;
					$solrArr['display_name'] = 'livestream';
					$solrArr['content_permalink'] =  $movie['permalink'];
					$solrObj = new SolrFunctions();
					$ret = $solrObj->addSolrData($solrArr);
				} 
				$this->code = 200;
				$this->msg_code = 'broadcast_content_created_success';
				$data = array();				
				$data['feed_url'] = $feedData['stream_url']."/".$feedData['stream_key'];
				$data['stream_url'] = $feedData['stream_url'];
				$data['stream_key'] = $feedData['stream_key'];
				$data['movie_id'] = $movie_id;
				$data['uniq_id'] = $movie['uniq_id'];
				$this->items = $data;
			}
		} else {
			$this->code = 777;
		}
	}	
	/**
	 * @method public GetOfflineViewRemainingTime : This function returns remaining time of offline view of downloaded video
	 * @return json Return the json array of results
	 * @author Prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param String stream_uniq_id* : stream uniq id (embed_id of movie_streams table)	 
	 * @param Integer user_id* : User id
	 * @param String device_id : Unique code device wise
	 * @param Integer device_type : Device type(2->android,3->Ios)
	 * @param String request_data : Unique data from mobile end	
	 * @param String lang_code : User language Code
	 * 
	*/			
	function actionGetOfflineViewRemainingTime(){
		$stream_uniq_id = isset($_REQUEST['stream_uniq_id'])? $_REQUEST['stream_uniq_id'] : '';
		$lang_code = @$_REQUEST['lang_code'];
		if($stream_uniq_id){
			$user_id = isset($_REQUEST['user_id'])? $_REQUEST['user_id'] : 0;
			if($user_id){
				$device_type = isset($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 2;
				$device_id = @$_REQUEST['device_id'];
				$request_data = @$_REQUEST['request_data'];
				//check stream uniq id is available in offline_view_log table				
				$offline_view_log_data = OfflineView::model()->findByAttributes(array(), 'stream_uniq_id=:stream_uniq_id AND user_id=:user_id', array(':stream_uniq_id' => $stream_uniq_id, ':user_id' => $user_id));
				$translate = $this->getTransData($lang_code, $this->studio_id);
				if($offline_view_log_data){
					$current_date =(date('Y-m-d H:i:s'));					
					Yii::app()->db->createCommand()->update('offline_view_log', array('device_id' => $device_id, 'device_type' => $device_type, 'updated_date' => $current_date), 'id = :id', array(':id' => $offline_view_log_data['id']));
					$this->code = 200;					
					$this->items['stream_uniq_id'] = $offline_view_log_data->stream_uniq_id;
					$this->items['request_data'] = $request_data;
					$this->items['user_id'] = $user_id;
					$this->items['device_id'] = $device_id;					
					$this->items['access_expiry_time'] = $offline_view_log_data->access_expiry_time;
					$this->items['created_date'] = (strtotime($offline_view_log_data->created_date)*1000);
					$expiry_date_second = ceil($offline_view_log_data->access_expiry_time / 1000);
					$current_date_timestamp = strtotime($current_date);
					$gap_second = $expiry_date_second - $current_date_timestamp;
					if ($gap_second > 0) {
						$hour = floor($gap_second / 3600);
						$rest_seconds = $gap_second % 3600;
						$minutes = floor($rest_seconds / 60);
						$hour_value = $hour <= 9 ? '0' . $hour : $hour;
						$minutes_value = $minutes <= 9 ? '0' . $minutes : $minutes;
						$disp_time = $hour_value . ':' . $minutes_value;
					} else {
						$disp_time = '00:00';
					}
					$this->items['download_complete_msg'] = @$translate['download_complete_msg'] . $disp_time;
				}else{
					//Get data from movie_streams table
					$movie_streams_info = movieStreams::model()->findByAttributes(array(), array(
						'select' => array('id', 'movie_id', 'embed_id'),
						'condition' => 'embed_id = :stream_uniq_id',
						'params' => array(':stream_uniq_id' => $stream_uniq_id)
					));
					if($movie_streams_info){
						//call billing function
						$access_expiry_time = 0;
						$access_info = Yii::app()->common->getContentExpiryDate($this->studio_id, $user_id, $movie_streams_info->embed_id);
						$current_date = (gmdate('Y-m-d H:i:s'));
						$cur_date_timestamp_millisecond = (strtotime($current_date) * 1000);
						if ($access_info['is_valid']) {
							$save_flag = 1;
							$config_value = Yii::app()->db->createCommand()
									->select('config_key,config_value')
									->from('studio_config')
									->where("studio_id=:studio_id AND config_key IN('offline_view_access_period','offline_view_access_period_limit')", array(':studio_id' => $this->studio_id))
									->queryAll();
							$config_value_arr = CHtml::listData($config_value, 'config_key', 'config_value');
							if ($access_info['expiry_date'] != '') {
								//check access period is enabled or not
								if ($config_value_arr['offline_view_access_period'] == 1) {
									//compare expirydate value with config value and take smaller value as expiry time
									$config_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $cur_date_timestamp_millisecond);
									$billing_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
									$access_expiry_time = ($billing_expiry_time <= $config_expiry_time) ? $billing_expiry_time : $config_expiry_time;
								} else {
									//if access period is not active then take the billing expiry time
									$access_expiry_time = strtotime(gmdate('Y-m-d H:i:s', strtotime($access_info['expiry_date']))) * 1000;
								}
							} else {
								//check access period is enabled or not
								if ($config_value_arr['offline_view_access_period'] == 1) {
									//get value from config table when expiry date is blank
									$access_expiry_time = (($config_value_arr['offline_view_access_period_limit'] * 24 * 60 * 60 * 1000) + $cur_date_timestamp_millisecond);
								} else {
									$save_flag = 0;
								}
							}
							if ($save_flag) {
								$offline = new OfflineView();
								$offline->studio_id = $this->studio_id;
								$offline->movie_id = $movie_streams_info->movie_id;
								$offline->video_id = $movie_streams_info->id;
								$offline->stream_uniq_id = $movie_streams_info->embed_id;
								$offline->user_id = $user_id;
								$offline->access_expiry_time = $access_expiry_time;
								$offline->device_id = $device_id;
								$offline->device_type = $device_type;
								$offline->created_date = $current_date;
								$offline->updated_date = $current_date;
								$offline->save();
							}
						}		
						$this->code = 200;						
						$this->items['stream_uniq_id'] = $movie_streams_info->embed_id;
						$this->items['request_data'] = $request_data;
						$this->items['user_id'] = $user_id;
						$this->items['device_id'] = $device_id;						
						$this->items['access_expiry_time'] = ($access_expiry_time) ? $access_expiry_time : -1;
						$this->items['created_date'] = $cur_date_timestamp_millisecond;
						if ($access_expiry_time) {
							$expiry_date_second = ceil($access_expiry_time / 1000);
							$current_date = gmdate('Y-m-d H:i:s');
							$current_date_timestamp = strtotime($current_date);
							$gap_second = $expiry_date_second - $current_date_timestamp;
							if ($gap_second > 0) {
								$hour = floor($gap_second / 3600);
								$rest_seconds = $gap_second % 3600;
								$minutes = floor($rest_seconds / 60);
								$hour_value = $hour <= 9 ? '0' . $hour : $hour;
								$minutes_value = $minutes <= 9 ? '0' . $minutes : $minutes;
								$disp_time = $hour_value . ':' . $minutes_value;
							} else {
								$disp_time = '00:00';
							}
							$this->items['download_complete_msg'] = @$translate['download_complete_msg'] . $disp_time;
						} else {
							$this->items['download_complete_msg'] = '';
						}
					}else
						$this->code = 695;
				}				
			}else
				$this->code = 639;
		}else
			$this->code = 694; 		
	}	
	
	/* @method private generateExplayWideVineToken :This function is used to return  Licence Url
	 * @param string $conentKey , string $encryptionKey
     * @return  string or boolean value
     * @author Prakash<support@muvi.com>
    */
	private function generateExplayWideVineToken($conentKey = '', $encryptionKey = '') {
        if ($conentKey && $encryptionKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, WV_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $encryptionKey . "&contentKey=" . $conentKey . "&securityLevel=1&hdcpOutputControl=0&expirationTime=%2B120");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);            
            curl_close($ch);
            return ($server_output)?$server_output:false;
        } else {
            return false;
        }
    }
	
	/**
	 * @method public BufferLogsNew : This function is used in chrome-cast receiver end  for Non-Drm  Video only.This function store data in bandwidth_log and bandwidth_log_temp table
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String movie_id* : Content unique id
	 * @param String ip_address* : End user ip address
	 * @param Integer user_id : User id
	 * @param Integer device_type : Device type(1->web 2->android 3->ios 4->Roku)	
	 * @param String episode_id : Stream unique id
	 * @param Integer season_id : Season id	 
	 * @param String resolution : Resolution value(BEST,360 etc)
	 * @param Float start_time : Start time value in second	
	 * @param Float end_time : End time value in second	 
	 * @param String buffer_log_unique_id : Buffer log unique is the unique id of bandwidth_log table
	 * @param Integer log_id : Auto-increment id of bandwidth_log table
	 * @param Integer buffer_log_temp_id : Auto-increment id of bandwidth_log_temp table	
	 * @param Integer content_type : To know video or trailer (1->Normal video,2->trailer)
	 * @param String video_type : Video type value (mped_dash or "").To know Drm and Non-Drm video
	 * @param Float downloaded_bandwidth : Bandwidth consumed for offline video
	 * @param Float total_bandwidth :Consumed bandwidth value
	 * @param Integer location : When value is 0 then find country,city,state,country etc info from ip-address
	 * @param String lang_code : User language Code	 
	*/		
	public function actionBufferLogsNew() {					
		$movie_code = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : '0';
		if($movie_code){
			$ip_address = (isset($_REQUEST['ip_address']) && $_REQUEST['ip_address']!='') ? $_REQUEST['ip_address'] : '';
			if($ip_address){
				if (Yii::app()->aws->isIpAllowed($ip_address)){
					$studio_id = $this->studio_id;
					$user_id = (isset($_REQUEST['user_id']) && intval($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
					$add_video_log = ($user_id)?SdkUser::model()->findByPk($user_id)->add_video_log :1;
					$isAllowed =$device_id = 0;
					if (isset($_REQUEST['device_type']) && $_REQUEST['device_type'] == 4) {
						$isAllowed = 1;
						$device_id = $user_id;
					}
					if ($add_video_log || $isAllowed){
						$movie_id = Yii::app()->common->getMovieId($movie_code);
						$stream_code = (isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id'])) ? $_REQUEST['episode_id'] : '0';
						$season = (isset($_REQUEST['season_id']) && intval($_REQUEST['season_id'])) ? $_REQUEST['season_id'] : 0;
						 //Get stream Id
						$stream_id = 0;
						if ($movie_code != '0' && $stream_code != '0') {
							$stream_id = Yii::app()->common->getStreamId($movie_id, $stream_code, '', $studio_id);
						} else if ($movie_code != '0' && $stream_code == '0' && intval($season)) {
							$stream_id = Yii::app()->common->getStreamId($movie_id, 0, $season, $studio_id);
						} else {
							$stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
						}
						$location = (isset($_REQUEST['location']) && $_REQUEST['location']>0)? $_REQUEST['location'] :0;
						$device_type = isset($_REQUEST['device_type'])?$_REQUEST['device_type']:0;
						$resolution = (isset($_REQUEST['resolution'])) ? $_REQUEST['resolution'] : "";
						$start_time = (isset($_REQUEST['start_time'])) ? $_REQUEST['start_time'] : 0;
						$end_time = (isset($_REQUEST['end_time'])) ? $_REQUEST['end_time'] : 0;
						$log_unique_id = (isset($_REQUEST['log_unique_id']) && trim(($_REQUEST['log_unique_id']))) ? $_REQUEST['log_unique_id'] : '';
						$buffer_log_id = (isset($_REQUEST['log_id']) && trim(($_REQUEST['log_id']))) ? $_REQUEST['log_id'] : '';
						$buffer_log_temp_id = (isset($data['buffer_log_temp_id']) && intval($data['buffer_log_temp_id'])) ? $data['buffer_log_temp_id'] : 0;
						$content_type = (isset($_REQUEST['content_type']) && intval($_REQUEST['content_type'])) ? $_REQUEST['content_type'] : 1;
						$played_time = $end_time - $start_time;
						$bandwidthType = 1;
						if (@$_REQUEST['downloaded_bandwidth'] != '') {
							$bandwidth_used = @$_REQUEST['downloaded_bandwidth'];
							$bandwidthType = 2;
						} else if (strtolower(@$_REQUEST['video_type']) == 'mped_dash') {
							//mped_dash
							$bandwidth_used = @$_REQUEST['total_bandwidth'];
						} else {
							if ($content_type == 2) {
								$movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $movie_id));
							} else {
								$movie_stream_data = movieStreams::model()->findByPk($stream_id);
							}
							$res_size = json_decode($movie_stream_data->resolution_size, true);					
							$video_duration = explode(':', $movie_stream_data->video_duration);
							$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
							$size = $res_size[$resolution];
							$bandwidth_used = ($size / $duration) * $played_time;
						}
						$dataForSave = array();
						$dataForSave = $_REQUEST;					
						$dataForSave['studio_id'] = $studio_id;
						$dataForSave['buffer_log_id'] = $buffer_log_id;
						$dataForSave['buffer_log_temp_id'] = $buffer_log_temp_id;
						$dataForSave['user_id'] = $user_id;
						$dataForSave['movie_id'] = $movie_id;
						$dataForSave['video_id'] = $stream_id;
						$dataForSave['resolution'] = $resolution;
						$dataForSave['start_time'] = $start_time;
						$dataForSave['end_time'] = $end_time;
						$dataForSave['bandwidth_used'] = $bandwidth_used;	
						$dataForSave['ip_address'] = $ip_address;
						$dataForSave['played_time'] = $played_time;
						$dataForSave['location'] = $location;
						$dataForSave['device_type'] = $device_type;
						$dataForSave['device_id'] = $device_id;	
						$dataForSave['bandwidth_type'] = $bandwidthType;						
						$buffer_log_data = BufferLogs::model()->dataSave($dataForSave);
						if($buffer_log_data){
							$this->code = 200;	
							$this->items['location'] = 1;
							$this->items['log_id'] = @$buffer_log_data[0];
							$this->items['buffer_log_unique_id'] = @$buffer_log_data[1];
							$this->items['buffer_log_temp_id'] = @$buffer_log_data[2];												
						}else
							$this->code = 500;												
					}else
						$this->code = 637;
				}else
					$this->code = 636;
			}else
				$this->code = 635;
		}else
			$this->code = 684;		
	}	
		
	/**
	 * @method public ManageDevices : This function is used to return list of devices of a user. 
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	
	 * @param Integer user_id* : End user id	
	 * @param String lang_code : User language Code
	 * 
	*/
    public function actionManageDevices() {
        if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']>0) {
            $this->items = Yii::app()->db->createCommand()->select(' * ')->from("device_management")->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $this->studio_id, ':user_id' => $_REQUEST['user_id']))->order('id ASC')->queryAll();
            $this->code = $this->items ? 200 : 674;
        } else {
            $this->code = 672;
        }
    }
	/**
	 * @method public RemoveDevice : This function is used to change flag field value 0 to 1 in device_management table.Here 1 means user requested to admin to remove the particular device. 
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String device* : Unique id of device
	 * @param Integer user_id* : End user id	
	 * @param String lang_code : User language Code
	 * 
	*/
    public function actionRemoveDevice() {
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) {
                $studio_id = $this->studio_id;
                $user_id = $_REQUEST['user_id'];
                $device = $_REQUEST['device'];
                $device_details = Device::model()->findBySql('SELECT * FROM `device_management` WHERE `studio_id`=:studio_id AND `user_id`=:user_id AND `device`=:device AND flag=0 LIMIT 1', array(':studio_id' => $studio_id, ':user_id' => $user_id, ':device' => $device));
                if (!empty($device_details)) {
                    Device::model()->updateByPk($device_details['id'], array('flag' => 1, 'deleted_date' => date('Y-m-d H:i:s')));
                    if (NotificationSetting::model()->isEmailNotificationRemoveDevice($studio_id, 'device_management')) {
                        $user_data = SdkUser::model()->findByPk($user_id, array('select' => 'email'));
                        $mailcontent = array('studio_id' => $studio_id, 'device' => $device, 'device_info' => $device_details['device_info'], 'email' => $user_data['email']);
                        Yii::app()->email->mailRemoveDevice($mailcontent);
                    }
                    $this->code = 200;
                } else {
                    $this->code = 673;
                }
            } else {
                $this->code = 672;
            }
        } else {
            $this->code = 671;
        }
    }
    
	/**
	 * @method public CheckDevice : This function is used to check and add device in device_management table
	 * @return json Return the json array of results
	 * @author prakash<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String device* : Unique id of device
	 * @param Integer user_id* : End user id
	 * @param String google_id : Google id for push notification
	 * @param Integer device_type : Type of device(1->android,2->mobile)
	 * @param String device_info : Information about device and versions
	 * @param String lang_code : User language Code
	 * 
	*/
    public function actionCheckDevice() {
        if (isset($_REQUEST['device']) && ($_REQUEST['device'])) {
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = @$_REQUEST['user_id'];
                $device = @$_REQUEST['device'];
                $google_id = @$_REQUEST['google_id'];
                $device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                if (Yii::app()->custom->restrictDevices($this->studio_id, 'restrict_no_devices')) {
                    $chkDeviceLimit = Yii::app()->custom->restrictDevices($this->studio_id, 'limit_devices');
                    $all_devices = Yii::app()->db->createCommand()->select(' count(*) AS cnt, SUM(IF(device=:device, 1, 0)) AS exist ')->from("device_management")
                                    ->where('studio_id =:studio_id AND user_id=:user_id AND flag<=1', array(':studio_id' => $this->studio_id, ':user_id' => $user_id, ':device' => $device))->queryrow();
                    if ($all_devices['exist'] == 0) {
                        if ($all_devices['cnt'] < $chkDeviceLimit) {                           
							$devicelist = new Device();
							$devicelist->user_id = $user_id;
							$devicelist->studio_id = $this->studio_id;
							$devicelist->device = $device;
							$devicelist->device_info = @$_REQUEST['device_info'];
							$devicelist->device_type = $device_type;
							$devicelist->google_id = $google_id;
							$devicelist->created_by = $user_id;
							$devicelist->created_date = date('Y-m-d H:i:s');
							$devicelist->save();
							$this->code = 200;
                        } else {
                            $this->code = 675;
                        }
                    } else {
                        $sql = "UPDATE device_management SET google_id = :google_id WHERE studio_id =:studio_id AND user_id=:user_id AND device=:device AND flag=0";
                        Yii::app()->db->createCommand($sql)->execute(array(':google_id' => $google_id, ':studio_id' => $this->studio_id, ':user_id' => $user_id, ':device' => $device));
                        $this->code = 200;
                    }
                } else {
                    $this->code = 674;
                }
            } else {
                $this->code = 672;
            }
        } else {
            $this->code = 671;
        }
    }    		
};