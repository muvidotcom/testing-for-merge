<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\Ses\SesClient; 
class TicketController extends Controller {
    public $layout = 'admin';
    public $headerinfo = '';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);

        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect($redirect_url);exit;
        }else{
             $this->checkPermission();
        }

        if (!isset(yii::app()->user->is_sdk) && !yii::app()->user->is_sdk) {
            $studioArray = array('customerslist', 'addbill', 'editbill', 'maketransaction', 'Suspend', 'resume');
            if (in_array(Yii::app()->controller->action->id, $studioArray)) {
                $this->redirect($redirect_url);exit;
            }
        }
        return true;
    }

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $model = new TicketForm;
        if (isset($_POST['TicketForm'])) {
            $model->attributes = $_POST['TicketForm'];
        }
        $this->render('addTicket', array('model' => $model));
    }

    
    public function actionTicketList() {
        $this->pageTitle = Yii::app()->name . ' - Support';
        $this->breadcrumbs = array('Support', 'All Tickets');
        $tickettingdb = $this->getAnotherDbconnection();
        $email = Yii::app()->user->email;
	 $studio_id = Yii::app()->common->getStudiosId();
        //check whether the user is a admin of the studio or general_user       
        $user_sql = "select * from user where studio_id=" . $studio_id;
        $reg_studioemail = Yii::app()->db->createCommand($user_sql)->queryAll();       

        if (Yii::app()->common->hasPermission('support', 'view')) {
            $master_sql = "select id from ticket_master where studio_email='" . $reg_studioemail[0]['email'] . "'";
            $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
       
            $model = new TicketForm;
            $cond = "ticket_master_id=" . $master_id[0]['id'];
            
            if (isset($_REQUEST['searchtext']) && $_REQUEST['searchtext'] != '') {
                $cond .= " and title like '%{$_REQUEST['searchtext']}%'";
            }            
            if (isset($_REQUEST['reporter']) && $_REQUEST['reporter'] != 'All') {
                $cond .= " and creater_id ='{$_REQUEST['reporter']}'";
            }            
            if (!isset($_REQUEST['status'])) {
                $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Open') {
            $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Re-opened') {
            $cond .= " and status='Re-opened'";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Closed') {
            $cond .= " and status='Closed'";
            }
            if (isset($_REQUEST['priority']) && $_REQUEST['priority'] != 'All') {
                $cond .= " and priority ='{$_REQUEST['priority']}'";
            }
            if (isset($_REQUEST['app']) && $_REQUEST['app'] != 0) {
                $cond .= " and app ='{$_REQUEST['app']}'";
            }
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'All') {
                $type = $_REQUEST['type'];
                if ($_REQUEST['type'] == 'NewFeature')
                    $type = 'New Feature';

                $cond .= " and type ='{$type}'";
            }
            $orderby = 'last_updated_date DESC';
            if($_REQUEST['sortby'] == 'asc'){
             $orderby = "last_updated_date ASC";
            }
            
            if (isset($_REQUEST['ticket']) && $_REQUEST['ticket'] != '') {
                $cond .= " and id ='{$_REQUEST['ticket']}'";
            }
            $limit = " LIMIT 0, 20";
            $tickets = $model->ticketList($cond,$orderby,$limit);
            $total_tickets = $model->ticketList($cond);
            Yii::app()->request->cookies['total_ticket'] = new CHttpCookie('total_ticket', count($total_tickets));
            
        $dev_hours = $tickettingdb->createCommand("SELECT sum(dev_hours) as total FROM ticket where status in ('New','Working') and type='New Feature' and studio_id=" . $studio_id)->queryAll();

            $remaining_dev_hours = Yii::app()->db->createCommand("SELECT purchased_devhours FROM studios where id=" . $studio_id)->queryAll();

            //card details for payment
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();

            $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
            $muvi_apps = MuviApps::model()->findAll();
            $reporters = Yii::app()->db->createCommand("SELECT id,email,first_name FROM `user` WHERE `studio_id` = $studio_id")->queryAll();
            $this->render('ticketList', array('tickets' => $tickets, 'total_dev_hours' => $remaining_dev_hours[0]['purchased_devhours'], 'card' => $card, 'muvi_apps' => $muvi_apps, 'reporters' => $reporters));
            $tickettingdb->active = false;
        }else {
            $tickettingdb->active = false;
            Yii::app()->user->setFlash('error', 'you are not an authorized user!');
            $url = $this->createUrl('admin/dashboard');
             $this->redirect($url);
        }
    }
    public function actionAutoSearchticket()
    {
        $tickettingdb = $this->getAnotherDbconnection();
        $studio_id = Yii::app()->common->getStudiosId();
        //check whether the user is a admin of the studio or general_user       
        $user_sql = "select * from user where studio_id=" . $studio_id . " and role_id=1";
        $reg_studioemail = Yii::app()->db->createCommand($user_sql)->queryAll();       

        if (Yii::app()->common->hasPermission('support', 'view')) {
            $model = new TicketForm;                         
            $master_sql = "select id from ticket_master where studio_email='" . $reg_studioemail[0]['email'] . "'";
            $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
            
            $cond = "ticket_master_id=" . $master_id[0]['id'];
            ///$cond = "ticket_master_id=" . $master_id[0]['id'] ." OR portal_user_id IN (select id from portal_user where parent_id = (select refer_id from user_relation where studio_id = $studio_id))";
            if (isset($_REQUEST['reporter']) && $_REQUEST['reporter'] != 'All') {
                $cond .= " and creater_id ='{$_REQUEST['reporter']}'";
            }       
            if (!isset($_REQUEST['status'])) {
                $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Open') {
            $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Re-opened') {
            $cond .= " and status='Re-opened'";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Closed') {
            $cond .= " and status='Closed'";
            }
            if (isset($_REQUEST['priority']) && $_REQUEST['priority'] != 'All') {
                $cond .= " and priority ='{$_REQUEST['priority']}'";
            }
            if (isset($_REQUEST['app']) && $_REQUEST['app'] != 0) {
                $cond .= " and app ='{$_REQUEST['app']}'";
            }
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'All') {
                $type = $_REQUEST['type'];
                if ($_REQUEST['type'] == 'NewFeature')
                    $type = 'New Feature';

                $cond .= " and type ='{$type}'";
            }
            //echo $cond;
            $total_new_ticket = $model->ticketList($cond);
            Yii::app()->request->cookies['total_new_ticket'] = new CHttpCookie('total_new_ticket', count($total_new_ticket));
            $total_ticket = Yii::app()->request->cookies['total_ticket']->value;
            $total_search_ticket = Yii::app()->request->cookies['total_new_ticket']->value;
            
            if($total_search_ticket > $total_ticket ){                
            //Yii::app()->request->cookies['total_ticket'] = new CHttpCookie('total_ticket', count($total_new_ticket));
            $new_tickets = $total_search_ticket -$total_ticket;  
            
            $orderby = 'last_updated_date DESC';
            $limit = " LIMIT 0, $new_tickets";
            $tickets = $model->ticketList($cond,$orderby,$limit);
            $count_searched = count($tickets); 
            
            $remaining_dev_hours = Yii::app()->db->createCommand("SELECT purchased_devhours FROM studios where id=" . $studio_id)->queryAll();

            //card details for payment
            $user_id = Yii::app()->user->id;
            $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
            //$this->render('ticketList', array('list' => $list, 'total_pages' => $total_pages,'total_tickets' => $total_tickets,'total_dev_hours'=>$remaining_dev_hours[0]['purchased_devhours'],'card'=>$card));
            $tickettingdb->active = false;
            $this->renderPartial('ticketrender_autosearch', array('list' => $tickets, 'count_searched' => $count_searched, 'total_dev_hours' => $remaining_dev_hours[0]['purchased_devhours'], 'card' => $card));
              
            }
            
        }
    }
    public function actionsearchTicket() {
        $studio_id = Yii::app()->common->getStudiosId();
        $this->pageTitle = Yii::app()->name . ' - Support';
        $this->breadcrumbs = array('Support', 'All Tickets');
        $email = Yii::app()->user->email;
        $tickettingdb = $this->getAnotherDbconnection();
	 //check whether the user is a admin of the studio or general_user       
        $user_sql="select * from user where studio_id=".$studio_id." and role_id=1";   
        $reg_studioemail=Yii::app()->db->createCommand($user_sql)->queryAll();   

        $master_sql="select id from ticket_master where studio_email='".$reg_studioemail[0]['email']."'";         
        $master_id = $tickettingdb->createCommand($master_sql)->queryAll();

        if (Yii::app()->common->hasPermission('support', 'view')) {

            $num_rec_per_page = 20;
            $start_from = 0;
            if (isset($_REQUEST['page'])) {
                $start_from = ($_REQUEST['page'] - 1) * $num_rec_per_page;
            }
            $model = new TicketForm;
            
            $limit = " LIMIT $start_from, $num_rec_per_page";
            $cond = "ticket_master_id=" . $master_id[0]['id'];
            if (isset($_REQUEST['searchtext']) && $_REQUEST['searchtext'] != '') {
                $cond .= " and title like '%{$_REQUEST['searchtext']}%'";
            }
            if (isset($_REQUEST['reporter']) && $_REQUEST['reporter'] != 'All') {
                $cond .= " and creater_id ='{$_REQUEST['reporter']}'";
            }
            if (!isset($_REQUEST['status'])) {
                $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Open') {
            $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Re-opened') {
            $cond .= " and status='Re-opened'";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Closed') {
            $cond .= " and status='Closed'";
            }
            if (isset($_REQUEST['priority']) && $_REQUEST['priority'] != 'All') {
                $cond .= " and priority ='{$_REQUEST['priority']}'";
            }
            if (isset($_REQUEST['app']) && $_REQUEST['app'] != 0) {
                $cond .= " and app ='{$_REQUEST['app']}'";
            }
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'All') {
                $type = $_REQUEST['type'];
                if ($_REQUEST['type'] == 'NewFeature')
                    $type = 'New Feature';
            
                $cond .= " and type ='{$type}'";
            }
            $orderby = 'last_updated_date DESC';
            if($_REQUEST['sortby'] == 'asc'){
             $orderby = "last_updated_date ASC";
            }
            
            $total_query = "SELECT SQL_CALC_FOUND_ROWS(0),i.* from ticket i where ticket_master_id=" . $master_id[0]['id'] . " and studio_id=" . $studio_id;

            $res = $tickettingdb->createCommand($total_query)->queryAll();
            $total_tickets = count($res);
            
            $allticket = $model->ticketList($cond, $orderby, $limit);
            $count_searched = count($allticket);

            $dev_hours = $tickettingdb->createCommand("SELECT sum(dev_hours) as total FROM ticket where status in ('New','Working') and type='New Feature' and studio_id=" . $studio_id)->queryAll();
            $remaining_dev_hours = Yii::app()->db->createCommand("SELECT purchased_devhours FROM studios where id=" . $studio_id)->queryAll();

            //card details for payment
            $user_id = Yii::app()->user->id;
            $card = CardInfos::model()->findAll(array('condition' => 'studio_id = ' . $studio_id . ' AND user_id = ' . $user_id, 'order' => 'is_cancelled ASC'));
            //$this->render('ticketList', array('list' => $list, 'total_pages' => $total_pages,'total_tickets' => $total_tickets,'total_dev_hours'=>$remaining_dev_hours[0]['purchased_devhours'],'card'=>$card));
            $tickettingdb->active = false;
        }

        $this->renderPartial('ticketrenderdata', array('list' => $allticket, 'count_searched' => $count_searched, 'total_tickets' => $total_tickets, 'total_dev_hours' => $remaining_dev_hours[0]['purchased_devhours'], 'card' => $card));
    }
    
    public function actionuseDevhour()
    {
        $ticket_id=$_POST['ticket_id'];
        $dev_hour=$_POST['dev_hour'];
        $ticket_id=$_POST['ticket_id'];
        $studio_id=$_POST['studio_id'];
        $std=new Studio();
        $studio = $std->findByPk($studio_id);
        $purchased_devhours=$studio->purchased_devhours;
       
        $used_hours_from_purchased=$studio->used_hours_from_purchased;
        
       if($purchased_devhours<$dev_hour || $purchased_devhours==0)
       {
           $ret['message']='Not Sufficient DevHous';
           $ret['status']=0;
           
       }
       else{
            
            $totalremaining_devhour = $purchased_devhours-$dev_hour;
            $std1=Studio::model()->updateByPk($studio_id,array("purchased_devhours"=>$totalremaining_devhour)); 
           
            $totalused_devhour = $used_hours_from_purchased+$dev_hour;
            $std2=Studio::model()->updateByPk($studio_id,array("used_hours_from_purchased"=>$totalused_devhour));  
           
           //update flag after purchased
            $ticket=TicketForm::model()->updateByPk($ticket_id,array("purchase_status"=>1));  
           
            //insert record to dev hours used info
            $insert_sql="insert into `devhour_used_infos`( `studio_id`, `ticket_id`, `hours_purchased`, `creation_date`) values(".$studio_id.",".$ticket_id.",".$purchased_devhours.",'".date("Y-m-d H:i:s")."')";
            Yii::app()->db->createCommand($insert_sql)->execute();
            //send email to admin and customer reg the devhour purchase info.
            $user_id = Yii::app()->user->id;
            
            $user = User::model()->findByPk($user_id);
            $req['name'] = $user->first_name;
            $req['email'] = $user->email;
            $req['ticket_id'] = $ticket_id;
            $req['dev_hour']=$dev_hour;
            $req['remaining_devhour']=$totalremaining_devhour;
           
            $customeremail= Yii::app()->email->approveDevhourEmailtoCustomer($req);
            $adminemail= Yii::app()->email->approveDevhourEmailtoAdmin($req);
            
            
             
             //send email to admin and customer reg the devhour purchase info.
            $ret['message']='Sufficient DevHous';
            $ret['status']=1;
       }
        echo json_encode($ret);exit; 
    }
    
     public function actionaddTicket() {
        $this->pageTitle = Yii::app()->name . ' - Add Ticket';
        $this->breadcrumbs = array('Support','All tickets'=>array('ticket/ticketList'),'Add Ticket');
        $this->headerinfo = "Add Ticket";
        $model = new TicketForm;
        if (!isset($_GET['error']))
            unset($_SESSION['description']);
        if (isset($_POST['TicketForm']))
            $model->attributes = $_POST['TicketForm'];
        $assigned_to = $model->getOwner();
        $studio = $model->getStudio();
        $this->render('addTicket', array('model' => $model, 'assigned_to' => $assigned_to, 'studio' => $studio));
    }


    public function actionInsertTicket() {
        $server_id = Yii::app()->common->getServerId();
        $attachment_patharray=array();
        $email=Yii::app()->user->email;   
        $tickettingdb=$this->getAnotherDbconnection();
        
        $studio_id = Yii::app()->common->getStudiosId();
        //check whether the user is a admin of the studio or general_user       
        $user_sql="select * from user where studio_id=".$studio_id." and role_id=1";   
        $reg_studioemail=Yii::app()->db->createCommand($user_sql)->queryAll();   
        
        $master_sql = "select * from ticket_master where studio_id =" .$studio_id. " and server_id = ". $server_id;         
        $ticket_master = $tickettingdb->createCommand($master_sql)->queryAll(); 
        
        $_POST['TicketForm']['ticket_master_id'] = $ticket_master[0]['id'];
        $_POST['TicketForm']['studio_id'] = $studio_id;
        $model = new TicketForm;
        if (empty($_POST['TicketForm']['title']) && trim($_POST['TicketForm']['title'])=='') {
            Yii::app()->user->setFlash('error','Ticket title can not be empty');
                $this->redirect('ticketList');  
                exit;
        
        }
        if (isset($_POST['TicketForm']) && !empty($_POST['TicketForm'])) {
           // print_r($_POST['TicketForm']);
           // exit;
            $_POST['TicketForm']['creater_id']=Yii::app()->user->id;
            $model->attributes = $_POST['TicketForm'];
            $ticketNumber = $model->insertTicket($_POST['TicketForm']);
            
            if ($ticketNumber) {
                if (!empty($_FILES['upload_file1']['name'])) {
                    $attachments = TicketController::attachFile($_FILES, $ticketNumber);
                    $arr['TicketForm']['attachment'] = implode($attachments, ',');
                    $arr['TicketForm']['id_ticket'] = $ticketNumber;
                    $model->updateTicket($arr['TicketForm']);
                    
                }
                //code for uploading attachment with email
                $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                foreach ($attachments as $k){
                $attachment_patharray[]=$src."/".$k;
                }
                
                $studio_id = Yii::app()->common->getStudiosId();
                $std = new Studio();
                $studio = $std->findByPk($studio_id);
                $title =($_POST['TicketForm']['title']);
                if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                $description =($_POST['TicketForm']['description']);
                if (strlen($description) > 16) 
                {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                    
                }
                
                $cc_email_list = $tickettingdb->createCommand("SELECT ticket_email_cc FROM ticket where id=$ticketNumber")->queryAll();                
                $cc_email_list = Yii::app()->common->multiexplode($cc_email_list[0]['ticket_email_cc']);

                $highvalue = '';
                if($studio->highvalue == 1) {
                    $highvalue = '(High-Value)';
                }
                $customer_type = 'Lead';
                $cc_to_sales = array(cc_to_sales);                  
                if($ticket_master[0]['studio_subscribed'] == 1) {
                    $cc_to_sales = array();
                    $customer_type = 'Customer';
                }
                $site_url = Yii::app()->getBaseUrl(true);
                $subject = $ticketNumber . "_" . $title . "_Ticket Created";

                if($_SERVER['HTTP_HOST'] == "www.idogic.com") {
                   $store_ticket_url = "http://www.idogic.com";
                   $admin_ticket_url = "http://www.admin.idogic.com";
                } else if($_SERVER['HTTP_HOST'] == "www.edocent.com") {
                   $store_ticket_url = "http://www.edocent.com";
                   $admin_ticket_url = "http://www.admin.edocent.com";
                } else {
                $store_ticket_url = "https://www.muvi.com";
                $admin_ticket_url = "https://admin.muvi.com";
                }
                  
                if($ticket_master[0]['base_url']!='')
                {
                    $store_ticket_url = $ticket_master[0]['base_url'];
                }
                $admin_url = "<a target='_blank' href='".$admin_ticket_url."/ticket/viewTicket/id/".$ticketNumber."'>$ticketNumber</a>";
                $store_url = "<a target='_blank' href='". $store_ticket_url ."/ticket/viewTicket/id/".$ticketNumber."'>$ticketNumber</a>";                
  
                $apps = array('All','Website','iOS App','Android App','Roku App','Android TV App','Fire TV App','Apple TV App','MUVI Server');
                $params = array(
                   'name' => ucfirst($ticket_master[0]['studio_name']),
                   'title'=> $_POST['TicketForm']['title'],
                   'description' => nl2br(stripslashes(htmlentities($_POST['TicketForm']['description']))),
                   'admin_url'=> $admin_url,
                   'store_url'=> $store_url,
                   'priority' => $_POST['TicketForm']['priority'],
                   'customer_type' => $customer_type,
                   'highvalue' => $highvalue,
                   'app' => $apps[$_POST['TicketForm']['app']],
                   'ticket_id' => $ticketNumber,
                   'subject' => $subject
               );
                
                $to_admin = ticket_username;
                $from = ticket_username;
                 $test_email_subject="Muvitest ticket";
                 
                 if(strpos($_POST['TicketForm']['description'],$test_email_subject)!== false)
                 {
                     $from=ticket_username_staging;
                     $to_admin=ticket_username_staging;
                 }
                Yii::app()->theme = 'bootstrap';
                $htmltoAdmin = Yii::app()->controller->renderPartial('//email/create_ticket_from_studio_to_admin',array('params'=>$params),true);
                $ret_val1=$this->sendAttchmentMailViaAmazonsdk($to_admin,$subject,$from,$htmltoAdmin,$attachment_patharray,$cc_to_sales);

                $ticket_users = Yii::app()->common->getTicketUsers($ticket_master[0]['id']);
                Yii::app()->theme = 'bootstrap';
                $thtmltoCustomer = Yii::app()->controller->renderPartial('//email/create_ticket_from_studio_to_user',array('params'=>$params),true);
                $ret_val2=$this->sendAttchmentMailViaAmazonsdk($ticket_users,$subject,$from,$thtmltoCustomer,$attachment_patharray,$cc_email_list);
             
                Yii::app()->user->setFlash('success','Ticket Added Successfully');
                $this->redirect('ticketList');
            }
        } else
            $this->render('addTicket', array('model' => $model));
    }

    
    public function actionUpdateTicket() {
     	$this->pageTitle = Yii::app()->name . ' - Update Ticket';
        $this->breadcrumbs = array('Support'=>array('ticket/ticketList'),'Update Ticket');
        $this->headerinfo = "Update Ticket";
        $model = new TicketForm;
        if (!isset($_POST['TicketForm'])) { //Edit Ticket
            $id = isset($_POST['id']) ? $_POST['id'] : $_GET['id'];
            $cond = "id=" . $id . " AND studio_id=" . Yii::app()->common->getStudiosId();
            $ticket = $model->ticketList($cond);
            if (empty($ticket)) {
                $this->render('error');
                exit;
            }
            $this->render('updateTicket', array('ticket' => $ticket[0], 'model' => $model));
        } else { //Update Ticket
            if (empty($_POST['TicketForm']['title']) || trim($_POST['TicketForm']['title'])=='') {
            Yii::app()->user->setFlash('error','Ticket title can not be empty');
                $this->redirect('ticketList');  
                exit;
        
                 }
            $ticket_eta_Details = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
            $before_array = $ticket_eta_Details[0];
            $cc_flag=1;
                if(($before_array['title']==$_POST['TicketForm']['title']) && ($before_array['description']==$_POST['TicketForm']['description']))
                {

                  $cc_flag=0; 
                }
                
             
            $_POST['TicketForm']['title']=  addslashes($_POST['TicketForm']['title']);
            $attachments=array();$prevfiles=array();
            if (!empty($_FILES['upload_file1']['name']))
                $attachments = TicketController::attachFile($_FILES, $_POST['TicketForm']['id_ticket']);
               
                //code for uploading attachment with email
                $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                foreach ($attachments as $k){
                $attachment_patharray[]=$src."/".$k;
                }
                
                $prevfiles = !empty($_POST['prevfiles']) ? explode(',',$_POST['prevfiles']): $prevfiles ;
                $attachments = array_merge($attachments, $prevfiles);
                $_POST['TicketForm']['attachment'] = !empty($attachments) ? implode($attachments, ',') : '';
            $studio_id = Yii::app()->common->getStudiosId();
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            if ($model->updateTicket($_POST['TicketForm'])) {
                $ticketDetails = $model->ticketList("id=" . $_POST['TicketForm']['id_ticket']);
                $description =($_POST['TicketForm']['description']);
                
                //send email to cc if title and description changed
                $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" .$studio_id)->queryAll();
                
               $cc_email_list=array();
               if($before_array['ticket_email_cc']!='' && $cc_flag==1)
               {
                   $cc_email_list=Yii::app()->common->multiexplode($before_array['ticket_email_cc']);
                  if(($before_array['ticket_email_cc']!=$_POST['TicketForm']['ticket_email_cc']))
                  {
                      $cc_email_list=Yii::app()->common->multiexplode($_POST['TicketForm']['ticket_email_cc']);
                  }
                foreach ($cc_email_list as $key => $value)
                       {
                           if (trim($value) == ticket_username || trim($value) == $user[0]['email'])
                        {
                            unset($cc_email_list[$key]);
                        }
                       }
            

               }
               $cc_to_sales = array();
                if($studio->is_subscribed == 0) {
                  $cc_to_sales = array(cc_to_sales);  
                }
                if (strlen($description) > 16)
                {
                    $description = wordwrap($description, 16);
                    $description = substr($description, 0, strpos($description, "\n"));
                }
                
                 $title =  addslashes($_POST['TicketForm']['title']);
                if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                if ($_POST['TicketForm']['status'] == 'Closed') {
                $subject = $_POST['TicketForm']['id_ticket'] . "_" . $title . " Closed";
                }else if($_POST['TicketForm']['status'] == 'Re-opened') {
                $subject = $_POST['TicketForm']['id_ticket'] . "_" . $title . " Re-Opened";
                }
                 else{
                $subject = $_POST['TicketForm']['id_ticket'] . "_" .$title." Updated";           
                }
                $html = TicketController::ticketDetails($ticketDetails);

                $to_admin = array(ticket_username);
                $test_email_subject="Muvitest ticket";
                 if(strpos($_POST['TicketForm']['description'],$test_email_subject)!== false)
                 {
                     $to_admin=array(ticket_username_staging);
                 }
                $from = ticket_username;
                $this->sendAttchmentMailViaAmazonsdk($to_admin,$subject,$from,$html,$attachment_patharray,$cc_to_sales,'','');

                $uri = '';
                if (!empty($_REQUEST['search']))
                    $uri.="/search/".urlencode($_REQUEST['search']);
                if (!empty($_REQUEST['sortBy']))
                    $uri.="/sortBy/{$_REQUEST['sortBy']}";
                if (!empty($_REQUEST['page']))
                    $uri.="/page/{$_REQUEST['page']}";
                Yii::app()->user->setFlash('success','Ticket Updated Successfully');
                $this->redirect('ticketList' . $uri);
            }
        }
    }

    function attachFile($files, $id_ticket) {
        $model = new TicketForm;
        foreach ($files as $k => $v) {
            if (!empty($v['name'])) {
                $filename = time() . '_' . str_replace(' ', '_',$v['name']);
                if ($filename) {
                    $fileUrl= Controller::uploadAttachments($filename, $v,  $id_ticket);
                    $new['id_ticket'] = $id_ticket;
                    $new['attachment_url'] = $fileUrl['url'];
                    $model->updateTicket($new);
                    $attachments[] = $filename;
                    //echo $filename;exit;
                    //Add attachment to email if the user added while creating ticket
                    //get the file details from s3 buckket after upload to s3bucket
                    $s3path=$fileUrl['url'].$filename;
                    
                    $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment' ;
                     if (!is_dir($src)) {
                      mkdir($src, 0777);
                        }
                   
                   file_put_contents($src."/".$filename, file_get_contents($s3path));
                }
            }
        }
        return $attachments;
    }

    function attachFile_note($files,$id_note,$id_ticket)   {             
        $model = new TicketForm;
        foreach ($files as $k => $v) {
            if (!empty($v['name'])) {
                $filename = strtotime(date("Y-m-d H:i:s")) . '_' . str_replace(' ', '_', $v['name']);
                if ($filename) {               
                    $fileUrl = Controller::uploadAttachments($filename, $v, $id_ticket);                    
                    $new['attachment_url'] = $fileUrl['url'];
                                    
                    $attachments['file_name'][] = $filename;
                    $attachments['url']= $fileUrl['url'];
                    $src=$_SERVER['DOCUMENT_ROOT'] . '/images/attachment' ;
                    $attachments['path'][] = $src . "/" . $filename;
               
                    $s3path=$fileUrl['url'].$filename;
                    
                    
                     if (!is_dir($src)) {
                      mkdir($src, 0777);
                        }
                   
                   file_put_contents($src."/".$filename, file_get_contents($s3path));
                }
            }
        }
        
        return $attachments;
    }

    function editAttachFile_note($files, $note_id, $id_ticket, $note_content, $all_file_list, $existing_file_url) {
       
        
        $attachments = array();
        $model = new TicketForm;
         $new['added_by'] = Yii::app()->user->id; //Yii::app()->common->getAgent();//Yii::app()->user->name;
         $new['id_user'] =  Yii::app()->common->getStudiosId();
         $new['add_date'] = date('Y-m-d H:i:s');
         $new['updated_date'] = date('Y-m-d H:i:s');
         $new['id'] = $note_id;
         $all_image_url='';
        if ($files['upload_file_edit1']['name'] != '') {
            foreach ($files as $k => $v) {
                if (!empty($v['name'])) {
                    $filename = strtotime(date("Y-m-d H:i:s")) . '_' . str_replace(' ', '_', $v['name']);

                    if ($filename) {
                        $fileUrl = $this->uploadAttachments($filename, $v, $id_ticket);

                        $attachments['imagefile'][] = $filename;
                        $src = $_SERVER['DOCUMENT_ROOT'] . '/images/attachment';
                        $attachments['path'][] = $src . "/" . $filename;
                         $all_image_url=implode(',',$attachments['path']);
                        
                        $s3path = $fileUrl['url'] . $filename;
                        if (!is_dir($src)) {
                            mkdir($src, 0777);
                        }

                        file_put_contents($src . "/" . $filename, file_get_contents($s3path));
                    }
                }
            }       
            $all_attachment = implode(",", $attachments['imagefile']);
            $all_attachment = $all_attachment . "," . $all_file_list;

            //get all image details
            $new['id_ticket'] = $id_ticket;
            $new['note'] = $note_content;
            $new['attachment_url'] = $fileUrl['url'];
            $new['attachment'] = addslashes($all_attachment);
            
                   
            //$new['id_note'] =  $note_id;
            $model->updateNote($new);
            //echo $filename;exit;
        } else {
            if ($all_file_list != '') {
                $new['id_ticket'] = $id_ticket;
                $new['note'] = $note_content;
                $new['attachment_url'] = $existing_file_url;
                $new['attachment'] = $all_file_list;
                $new['id'] = $note_id;
                //$new['id_note'] =  $note_id;
                $model->updateNote($new);

                
            } else {
                $new['id_ticket'] = $id_ticket;
                $new['note'] = $note_content;
                $new['attachment_url'] = "";
                $new['attachment'] = "";
                $new['id'] = $note_id;
                //$new['id_note'] =  $note_id;
                $model->updateNote($new);

               
            }
        }
        $new['all_attachment_path']=$all_image_url;
        return $new;
    }

    /* View Ticket Detail */

    public function actionViewTicket() {
        $tickettingdb=$this->getAnotherDbconnection();
        $ticket_id = $_REQUEST['id'];
        $studio_id = Yii::app()->common->getStudiosId();
        if (is_numeric($ticket_id)) {
        $model = new TicketForm;
        $cond = "id=" . $ticket_id . " AND studio_id=" . $studio_id;
        $ticket = $model->ticketList($cond);
        if (strlen($ticket['title']) > 35) {
            $page_title = wordwrap($ticket['title'], 35);
            $page_title = substr($page_title, 0, strpos($page_title, "\n"));
        } else {
           $page_title = $ticket['title'];
        }
        $this->pageTitle = ' "Ticket  ' . $ticket['id'] . ' : ' . $page_title . '"';
        $this->breadcrumbs = array('Ticket Detail',);
        if (empty($ticket)) {
            $this->render('error');
            exit;
        }
        $ticketnotes = $model->ticketNotes($ticket_id);
        $master_id = $ticket[0]['ticket_master_id'];
        $sql = "select id,user_name from ticket_master where id = $master_id";
        $master_user = $tickettingdb->createCommand($sql)->queryAll();
        $this->render('ticketDetail', array('ticket' => $ticket[0], 'notes' => $ticketnotes, 'master_user'=>  $master_user[0]['user_name']));
        } else {
            $this->render('error');
            exit;
        }

    }

    function  actionDeleteTicket(){
        $tickettingdb=$this->getAnotherDbconnection();
        $model = new TicketForm;
        //$del = $model->deleteRecord($_POST['id'],'ticket');
        
        $id_ticket=$_REQUEST['id'];
        
        /************************* code by suraja to close the ticket *****************/
            $last_updated_date = date('Y-m-d H:i:s');
            $last_updated_by = Yii::app()->common->getStudiosId();
            
            $status='Closed';
           $update_qry='UPDATE ticket SET last_updated_date="'.$last_updated_date.'", last_updated_by='.$last_updated_by.',status="'.$status.'" where id='.$id_ticket;

            $rid = $tickettingdb->createCommand($update_qry)->execute();
        
        $select_qry='select title,description,studio_id from  ticket where id='.$id_ticket;    
        $data = $tickettingdb->createCommand($select_qry)->queryAll();
        $tickettingdb->active=false;
        $studio_id=$data[0]['studio_id'];
      //$description=trim($data[0]['description']);
       $description=trim($data[0]['title']);
       
            $std = new Studio();
            $studioname = $studio_id != 1 ? $std->findByPk($studio_id)->name : 'Super Admin';
            $ticketDetails = $model->ticketList("id=" . $id_ticket);
            $ticketnotes = $model->ticketNotes($id_ticket);
           
            if (!empty($ticketnotes)) {
            foreach ($ticketnotes as $key => $updates) {
                if ($key == 0) {
                    $update=stripslashes(nl2br(htmlentities($updates['note'])));
                    if (strlen($update) > 16) {
                    $update = wordwrap($update, 16);
                    $update = substr($update, 0, strpos($update, "\n"));
            } 
                }
            }
        }else{
            $update ="Support ticket is";
        }
        
              $subject = $id_ticket . "_" . $description  . " Closed";
            $html = TicketController::ticketDetails($ticketDetails);
            $adminEmail = array(ticket_username);
              /*if ($studio_id != 1) {
                $user = Yii::app()->db->createCommand("SELECT email FROM user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id)->queryAll();
                $studioemail = $user[0]['email'];
                $this->sendmailViaMandrill($toStd . $html . $htmlstd."</p>" . $template[1], $subject, array(array("email" => $studioemail))); //Yii::app()->user->email
                }*/
                $to_admin = $adminEmail;
                $from = ticket_username;
                $this->sendmailViaAmazonsdk($html,$subject,$to_admin,$from);
                   
            
            
        /******************** code by suraja to close the ticket **************/
        
        Yii::app()->user->setFlash('success','Ticket Closed Successfully');
        $this->redirect($this->createUrl("ticket/ticketList"));
    }
    /* Add note/update to ticket */

    public function actionAddNote() {
        //if there is no note content 
        $attachment_patharray = array();
        $tickettingdb=$this->getAnotherDbconnection();
        //show message if there is noo more description on note 
        if (empty($_POST['note']) && trim($_POST['note']) == '') {

            $uri = '';
            if (!empty($_REQUEST['search']))
                $uri.="/search/" . urlencode($_REQUEST['search']);
            if (!empty($_REQUEST['sortBy']))
                $uri.="/sortBy/{$_REQUEST['sortBy']}";
            if (!empty($_REQUEST['page']))
                $uri.="/page/{$_REQUEST['page']}";
            Yii::app()->user->setFlash('error', 'Note description can not be empty');
            $this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");
        }
        if (isset($_POST['note']) && !empty($_POST['note'])) {
           
            $model = new TicketForm;
         
             $attachments['attachment'] = array();
            $prevfiles = array();
          //  if (!empty($_FILES['upload_file1']['name'])) {
                
                $note = $_POST['note'];
                $ticket_id = $_POST['id_ticket'];

                $insertid = $model->insertNote($note, $ticket_id);
           // }
            
            if ($insertid) {
                //if (!empty($_FILES['upload_file1']['name'])) {
                    $attachments = TicketController::attachFile_note($_FILES, $insertid, $ticket_id);
                   if (!empty($_FILES['upload_file1']['name'])) {
                    //$attachment_patharray[] = implode(",", $attachments['path']);
                       $attachment_patharray = $attachments['path'];
                   }
                    $arr['TicketForm']['attachment'] = implode( ',',$attachments['file_name']);
                    $arr['TicketForm']['attachment_url'] = $attachments['url'];
                    $arr['TicketForm']['id'] = $insertid;
                    $arr['TicketForm']['added_by'] = Yii::app()->common->getAgent(); //Yii::app()->user->name;
                    $arr['TicketForm']['id_user'] = Yii::app()->user->id;
                    $arr['TicketForm']['user_name'] = Yii::app()->user->first_name;
                    $arr['TicketForm']['studio_name'] = Yii::app()->user->siteName;
                    $arr['TicketForm']['add_date'] = date('Y-m-d H:i:s');
                    $arr['TicketForm']['updated_date'] = date('Y-m-d H:i:s');

                    $model->updateNote($arr['TicketForm']);
               // }
            }
           
            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title = $ticketDetails[0]['title'];
            $studio_id = $ticketDetails[0]['studio_id'];
            $ticket_master_id = $ticketDetails[0]['ticket_master_id'];
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $ticket_master = $tickettingdb->createCommand("SELECT * FROM ticket_master where id=$ticket_master_id")->queryAll();
            $cc_email_list = Yii::app()->common->multiexplode($ticketDetails[0]['ticket_email_cc']);
             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == ticket_username || trim($value) == $ticket_master[0]['studio_email'])
                {
                    unset($cc_email_list[$key]);
                }
            }

            if($studio->is_subscribed == 0) {
                  $cc_to_sales = array(cc_to_sales);  
             }
            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);
            $note =($_POST['note']);
                if (strlen($note) > 16) 
                {
                    $note = wordwrap($note, 16);
                    $note = substr($note, 0, strpos($note, "\n"));
                    
                }
                 if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
                
            $subject = $_POST['id_ticket'] . "_" .$title." Updated";
            $to_admin = ticket_username;
                $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin=ticket_username_staging;
                 }
            $from = ticket_username;
             $email=array($to_admin);
            $ret_val = $this->sendAttchmentMailViaAmazonsdk($email, $subject, $from, $html, $attachment_patharray, $cc_to_sales, '', '');
            
//            $admin_email = array();
//            $admin_email = Yii::app()->common->emailNotificationLinks($studio_id,'support_tickets');
//            if(count($admin_email)==0){
//                $admin_email[]=  $ticket_master[0]['studio_email'];
//             }
//            $to=$admin_email;
            $ticket_users = Yii::app()->common->getTicketUsers($ticket_master_id);
            $html .= '<p>You will hear from Muvi Support team soon. You can add an update to the ticket by logging into <a href="https://www.muvi.com/">Muvi</a> or simply replying to this email.</p>';
            $html .='<p>Regards, <br/> Muvi Support</p>';
            $retval=$this->sendmailViaAmazonsdk($html,$subject,$ticket_users,$from,$cc_email_list);
            
            $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
            }
        $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/".urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        Yii::app()->user->setFlash('success', 'Update Added Successfully');
        $this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");   
    }

    
    public function actionEditNote() {
      
        $tickettingdb=$this->getAnotherDbconnection();
        //show message if there is noo more description on note 
        if (empty($_POST['note']) && trim($_POST['note']) == '') {

            $uri = '';
            if (!empty($_REQUEST['search']))
                $uri.="/search/" . urlencode($_REQUEST['search']);
            if (!empty($_REQUEST['sortBy']))
                $uri.="/sortBy/{$_REQUEST['sortBy']}";
            if (!empty($_REQUEST['page']))
                $uri.="/page/{$_REQUEST['page']}";
            Yii::app()->user->setFlash('error', 'Note description can not be empty');
           
        }

       else if (isset($_POST['note']) && !empty($_POST['note'])) {

            $model = new TicketForm;
            $attachments['attachment'] = array();
            $prevfiles = array();
            
           // if (!empty($_FILES['upload_file_edit1']['name'])){
                $ticketDetails = $model->noteList("id=". $_POST['id_note']);
                $existing_attachment=$ticketDetails[0]['attachment'];
                $existing_file_url=$ticketDetails[0]['attachment_url'];
                
                $all_attachment = $existing_attachment;
                $attachments = TicketController::editAttachFile_note($_FILES, $_POST['id_note'], $_POST['id_ticket'], $_POST['editor_val'], $all_attachment,$existing_file_url);

                // }
              if($attachments['all_attachment_path']!=''){
              $attachment_patharray=explode(',',$attachments['all_attachment_path']); 
                }else
                {
                 $attachment_patharray=array();   
                }  
            if (!empty($_FILES['upload_file1']['name'])) {
                $note = $_POST['note'];
                $ticket_id = $_POST['id_ticket'];

                $insertid = $model->insertNote($note, $ticket_id,$_POST['id_note']);
            }
            

            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title = $ticketDetails[0]['title'];
            $studio_id = $ticketDetails[0]['studio_id'];
            $ticket_master_id = $ticketDetails[0]['ticket_master_id'];
            $std = new Studio();
            $studio = $std->findByPk($studio_id);

            $cc_email_list = Yii::app()->common->multiexplode($ticketDetails[0]['ticket_email_cc']);
            $ticket_master = $tickettingdb->createCommand("SELECT * FROM ticket_master where id=$ticket_master_id")->queryAll();
             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == ticket_username || trim($value) == $ticket_master[0]['studio_email'])
                {
                    unset($cc_email_list[$key]);
                }
            }
             if($studio->is_subscribed == 0) {
                  $cc_to_sales = array(cc_to_sales);  
             }

            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);

            $note = ($_POST['note']);
            if (strlen($note) > 16) {
                $note = wordwrap($note, 16);
                $note = substr($note, 0, strpos($note, "\n"));
            }
            if (strlen($title) > 16) {
                $title = wordwrap($title, 16);
                $title = substr($title, 0, strpos($title, "\n"));
            }

            $subject = $_POST['id_ticket'] . "_" . $title . " Updated";

            $to_admin = ticket_username;
            $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin=ticket_username_staging;
                 }
            $from = ticket_username;
            $email=array($to_admin);
            $ret_val = $this->sendAttchmentMailViaAmazonsdk($email, $subject, $from, $html, $attachment_patharray, $cc_to_sales, '', '');

            $admin_email = array();
            $admin_email = Yii::app()->common->emailNotificationLinks($studio_id,'support_tickets');
            if(count($admin_email)==0){
                $admin_email[] = $ticket_master[0]['studio_email'];
             }
            $to=$admin_email;

            $html .= '<p>You will hear from Muvi Support team soon. You can add an update to the ticket by logging into <a href="https://www.muvi.com/">Muvi</a> or simply replying to this email.</p>';
            $html .='<p>Regards, <br/> Muvi Support</p>';
            $retval=$this->sendmailViaAmazonsdk($html,$subject,$to,$from,$cc_email_list);

            $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
          
            $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/" . urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        Yii::app()->user->setFlash('success', 'Update Added Successfully');
        }
            
        
       
    }

    public function actionEditNote1() {


        if (isset($_POST['editor_val']) && !empty($_POST['editor_val'])) {
            
           
            $model = new TicketForm;
            $attachments['attachment'] = array();
            $prevfiles = array();
         
           if ($_FILES['upload_file1']['name']!='')
            {
            $ticketDetails = $model->noteList("id=". $_POST['id_note']);
            $existing_attachment=$ticketDetails[0]['attachment'];
            $existing_file_url=$ticketDetails[0]['attachment_url'];
            $all_attachment = $existing_attachment;
           
            $attachments = TicketController::editAttachFile_note($_FILES,$_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);
            
            }
            else
            {
                
            $ticketDetails = $model->noteList("id=". $_POST['id_note']);
            $existing_attachment=$ticketDetails[0]['attachment'];
            $existing_file_url=$ticketDetails[0]['attachment_url'];
            $all_attachment = $existing_attachment;
                if($all_attachment!='')
                {
                 $attachments = TicketController::editAttachFile_note($_FILES,$_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);   
                }else{
                $attachments=TicketController::editAttachFile_note($_FILES, $_POST['id_note'],$_POST['id_ticket'], $_POST['editor_val'],$all_attachment,$existing_file_url);   
                }
            }
            
             if(!empty($attachments['attachment_url'])){
            $src=$_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/attachment';
                
                $attachment_patharray[]=$src."/".$attachments['file_name'];
               
           }
            
           
            if (isset($_POST['id_note'])) {
                
                $note = $model->addNote($_POST['editor_val'], '', $_POST['id_note'], $attachments['attachment'], $attachments['attachment_url']);
            } else
                $model->addNote($_POST['editor_val'], $_POST['id_ticket'], $attachments['note_id'], $attachments['attachment'], $attachments['attachment_url']);
            
            
            /* Send update mail */
            $ticketDetails = $model->ticketList("id=" . $_POST['id_ticket']);
            $title=$ticketDetails[0]['title'];
            $studio_id = Yii::app()->common->getStudiosId();
            $std = new Studio();
            $studio = $std->findByPk($studio_id);
            $html = TicketController::ticketDetails($ticketDetails);
            //$template = TicketController::tempaleFormat($ticketDetails);
            $query_user_email = "select * from user where is_sdk=1 AND role_id=1 AND studio_id=" . $studio_id;
            $user_details = Yii::app()->db->createCommand($query_user_email)->queryAll();
             $useremail=$user_details[0]['email'];
            $cc_email_list=explode(",",$ticketDetails[0]['ticket_email_cc']);
            
             foreach ($cc_email_list as $key => $value)
            {
                if (trim($value) == 'support@muvi.com' || trim($value) == trim($user_details[0]['email']))
                {
                    unset($cc_email_list[$key]);
                }
            }
             $cc=implode(',',$cc_email_list);
            $note =($_POST['note']);
                if (strlen($note) > 16) 
                {
                    $note = wordwrap($note, 16);
                    $note = substr($note, 0, strpos($note, "\n"));
                    
                }
                 if (strlen($title) > 16) 
                {
                    $title = wordwrap($title, 16);
                    $title = substr($title, 0, strpos($title, "\n"));
                    
                }
            $toStd =  "<p> Hi ".ucfirst($studio->name).",<br /><br />";
            $toAdmn =  "<p> Hi Admin,<br /><br />";
            $subject = "Ticket" . $_POST['id_ticket'] . "_" .$title." Updated";
             /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
              
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            //$this->sendmailViaMandrill($toStd.$html, $subject, array(array("email" => Yii::app()->user->email)));
            /*******Sending email to studio admin stopped as per the mantis ticket requirement(#4207)*******/
            $to_admin='support@muvi.com';
            $test_email_subject="Muvitest ticket";
                 if(strpos($ticketDetails[0]['description'],$test_email_subject)!== false)
                 {
                     $to_admin=ticket_username_staging;
                 }
            $from='support@muvi.com';
            
            $email=array($to_admin);
           $this->sendAttchmentMailViaAmazonsdk($email,$subject,$from,$toAdmn.$html,$attachment_patharray,$cc_email_list,'','');
           $new['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($new);
            if (isset($_POST['id_note'])) {
               
            }
        }
        
        $uri = '';
        if (!empty($_REQUEST['search']))
            $uri.="/search/".urlencode($_REQUEST['search']);
        if (!empty($_REQUEST['sortBy']))
            $uri.="/sortBy/{$_REQUEST['sortBy']}";
        if (!empty($_REQUEST['page']))
            $uri.="/page/{$_REQUEST['page']}";
        //Yii::app()->user->setFlash('success','Update Added Successfully');
        //$this->redirect("viewTicket" . $uri . "/id/{$_POST['id_ticket']}");
    }

    public function actionDeletenote() {
        $id_note = $_POST['id'];
        $table = "ticket_notes";
        $model = new TicketForm;
        return $model->deleteRecord($id_note, $table);
    }
    function actionDeleteImage(){
        if($_REQUEST['is_ajax']){
            $s3 = S3Client::factory(array(
                    'key'    => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
            ));
            
            $result = $s3->deleteObject(array(
                    'Bucket' => Yii::app()->params->s3_ticket_attachments,
                    'Key'    => 'uploads/'.$_POST['id_ticket']."/".$_POST['image']
            ));
            $model = new TicketForm;
            $ticket = $model->ticketList("id=".$_POST['id_ticket']);
            $attachment= explode(',', $ticket[0]['attachment']);
            $key = array_search($_POST['image'], $attachment);
            if($key>=0)
                unset($attachment[$key]);
            $attachment = implode(',',$attachment);
            $arr['TicketForm']['attachment'] = $attachment;
            $arr['TicketForm']['id_ticket'] = $_POST['id_ticket'];
            $model->updateTicket($arr['TicketForm']);
            $ret['attachment']=$attachment;
            $ret['deleted']=1;
            echo json_encode($ret);exit;
        }
    }
    
    function actiondeletenoteImage(){
        $tickettingdb=$this->getAnotherDbconnection();
        if($_POST['is_ajax']){
            $s3 = S3Client::factory(array(
                    'key'    => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret,
            ));
            
            $result = $s3->deleteObject(array(
                    'Bucket' => Yii::app()->params->s3_ticket_attachments,
                    'Key'    => 'uploads/'.$_POST['id_ticket']."/".$_POST['image']
            ));
            $model = new TicketForm;
            $ticket = $model->noteList("id=".$_POST['note_id']);
            
            $attachment= explode(',', $ticket[0]['attachment']);
            
            $key=$_POST['key'];
            
            if($key>=0)
            {
            unset($attachment[$key]);
            }
          
            $attachment = implode(',',$attachment);
           
            $arr['TicketForm']['attachment'] = $attachment;
            $arr['TicketForm']['id_ticket'] = $_POST['id_ticket'];
           
            $sql="UPDATE ticket_notes SET attachment='".$attachment."' WHERE id=".$_POST['note_id'];
            
            $tickettingdb->createCommand($sql)->execute();
            $tickettingdb->active=false;
            $ret['attachment']=$attachment;
            $ret['deleted']=1;
            
            echo json_encode($ret);exit;
        }
    }
    
    /* Send mail to client if admin replies directly from CMS Ticket List */
    public function actionReplyClient() {
        $model = new TicketForm;
        if ($_POST['sendmail'])
            return $model->replyToClient($_POST);
        else
            return $model->getUserDetail($_POST['id'], $_POST['studio_id']);
    }

    /* Fetch Ticket Description and updates added to the ticket */

    function ticketDetails($ticket) {
        $tickettingdb = Yii::app()->controller->getAnotherDbconnection();
        $master_id = $ticket[0]['ticket_master_id'];
        $store_name = "All Store";
        if($master_id != 0) {
          $master_sql = "select id,studio_name from ticket_master where id=".$master_id;         
          $master = $tickettingdb->createCommand($master_sql)->queryAll();   
          $store_name = $master[0]['studio_name'];
        }
        $model = new TicketForm;
        //$template = TicketController::tempaleFormat();
        $ticketnotes = $model->ticketNotes($ticket[0]['id']);
        if ($ticket[0]['status'] == 'Closed') {
            $html = "<p> Your support ticket  {$ticket[0]['id']} is Closed Now.</p> ";
         } 
        else  if ($ticket[0]['status'] == 'Re-opened') {
            $html = "<p> Your support ticket  {$ticket[0]['id']} is Re-Opened.</p> ";
        }
         else {
            $html = "<p>Your support ticket  {$ticket[0]['id']} is updated. </p>";
        }
        
        if (!empty($ticketnotes)) {
            $html.="<hr style='border-top:1px dotted #000;' />";
            foreach ($ticketnotes as $k=>$updates) {
                    //$note_added_by = $updates['id_user']==1? 'Muvi' : $std->findByPk($ticket[0]['studio_id'])->name;
                    $note_added_by = Yii::app()->common->getTicketNoteUser($updates['id']);
                    $updated_on = date('M d, Y', strtotime($updates['updated_date']));
                    $html.=str_replace("\xC2\xA0", " ",stripslashes((trim($updates['note']))));
                    $html.="<br />By: {$note_added_by} on {$updated_on}";
                    $html.="<hr style='border-top:1px dotted #000;' />";
            }
        }
        //$html.= "Reported By: {$reported_by},&nbsp;Status: {$ticket[0]['status']},&nbsp;Priority: {$ticket[0]['priority']} <br /><br />";
        $html.= "Description:" . str_replace("\xC2\xA0", " ",stripslashes(trim($ticket[0]['description'])));
        return $html;
        exit;
    }
   
     function tempaleFormat() {
        if ($_SERVER['HTTP_HOST'] == "admin.muvi.com")
            $url = "https://www.muvi.com";
        else
            $url = "http://www.studio.muvi.in";
        $tpl[0] = '<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-repeat:repeat-x" width="100%">
                    <tbody>
                        <tr>
                            <td align="left" style="padding-left:20px;"><a href="'.$url.'"><img src="'.EMAIL_LOGO.'"/></a></td>
                        </tr>
                     <tr>
                      <td>
                       <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                        <tbody>
                         <tr style="background-color:#f3f3f3">
                          <td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"></div>
                          </td>
                          <td></td>
                         </tr>
                        </tbody>
                       </table>
                       <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                       <tbody>
                        <tr>
                         <td>
                          <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">';

        $tpl[1] = '<br /><br /><p style="display:block;margin:0 0 17px">
                                Regards,<br />
                                Muvi Team
                            </p>
 
      </div>
     </td>
    </tr>
                 
   </tbody>
   </table>
      </td>
    </tr>
  </tbody>
</table>';

        return $tpl;
    }

public function actiongetStatusWise()
    {
        $this->layout=false;
        $email=Yii::app()->user->email; 
        $tickettingdb=$this->getAnotherDbconnection();
        $studio_id = Yii::app()->common->getStudiosId();
        $master_sql="select id from ticket_master where studio_email='".$email."'";         
        $master_id=$tickettingdb->createCommand($master_sql)->queryAll();      
        $status=$_REQUEST['status'];
        $cond = " ticket_master_id=" . $master_id[0]['id'];
        if($status=="Open"){
        $cond.=" and status in('New','Working','re-opened','Re-opened')";
        }
        else if($status=="Closed")
        {
           $cond.=" and status ='Closed'";
            
        }else
        {
         $cond.=" ";  
        }
        
        $num_rec_per_page = 20;
        $start_from=0;
     
        $orderby = ' last_updated_date DESC';
        $limit = " LIMIT $start_from, $num_rec_per_page";
     
        $res = $tickettingdb->createCommand("SELECT count(*) as total FROM ticket where".$cond)->queryAll();
      
        $total_records = $res[0]['total'];
        $total_pages = ceil($total_records / $num_rec_per_page);
        $model = new TicketForm;
        $list = $model->ticketList($cond, $orderby, $limit);
        $this->renderPartial('ticketstatusrender', array('list' => $list, 'total_pages' => $total_pages,'total_records' => $total_records));
        
    
}

    public function actionJumptoTicketnumber()
    {        
         $tickettingdb=$this->getAnotherDbconnection();
         $ticket_no = $_POST['ticket_no'];
         $ticket_sql = "select id from ticket where id = $ticket_no";
         $ticket = $tickettingdb->createCommand($ticket_sql)->queryAll();
         if(count($ticket)>0){
            $this->redirect('/ticket/viewTicket/id/'.$ticket_no); 
         } else {
            echo '<script> alert("This is not a Valid Ticket Number!"); location.href="'.$_SERVER['HTTP_REFERER'].'"</script>';
         }
    }
    public function actionExportTickets(){        
        $tickettingdb = $this->getAnotherDbconnection();
        $studio_id = Yii::app()->common->getStudiosId();
        $server_id = Yii::app()->common->getServerId();
        
            $master_sql = "select id from ticket_master where studio_id='" . $studio_id . "' and server_id='".$server_id."'";
            $master_id = $tickettingdb->createCommand($master_sql)->queryAll();
            $model = new TicketForm;
            $cond = "ticket_master_id=" . $master_id[0]['id'];
            
            if (isset($_REQUEST['searchtext']) && $_REQUEST['searchtext'] != '') {
                $cond .= " and title like '%{$_REQUEST['searchtext']}%'";
            }            
            if (isset($_REQUEST['reporter']) && $_REQUEST['reporter'] != 'All') {
                $cond .= " and creater_id ='{$_REQUEST['reporter']}'";
            }       
            if (!isset($_REQUEST['status'])) {
                $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Open') {
            $cond .= " and (status ='Open' || status='Re-opened')";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Re-opened') {
            $cond .= " and status='Re-opened'";
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] == 'Closed') {
            $cond .= " and status='Closed'";
            }
            if (isset($_REQUEST['priority']) && $_REQUEST['priority'] != 'All') {
                $cond .= " and priority ='{$_REQUEST['priority']}'";
            }
            if (isset($_REQUEST['app']) && $_REQUEST['app'] != 0) {
                $cond .= " and app ='{$_REQUEST['app']}'";
            }
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'All') {
                $type = $_REQUEST['type'];
                if ($_REQUEST['type'] == 'NewFeature')
                    $type = 'New Feature';

                $cond .= " and type ='{$type}'";
            }
            $orderby = 'last_updated_date DESC';
            if($_REQUEST['sortby'] == 'asc'){
             $orderby = "last_updated_date ASC";
            }
            $tickets = $model->ticketList($cond,$orderby);
        
        $headArr[0] = array('Ticket#','Ticket Title','Priority','App', 'Latest Update','Status');
        $sheetName[0] = 'Ticket';
        $csvData[0] = array();
        $sheet = $i = 1;
        foreach ($tickets as $key => $details) {  
        $app_id = $details['app'];
        $sql = "SELECT app_name from muvi_apps where app_id=".$app_id;
        $apps = $tickettingdb->createCommand($sql)->queryAll();
        $app_name = $apps[0]['app_name'];
            $csvData[0][] = array(
                $details['id'],
                $details['title'],
                $details['priority'],
                $app_name,
                date('M d, h:ia', strtotime($details['last_updated_date'])),
                $details['status']
            );
            $i++;
        }
        $filename = 'ticket_'.date('Ymd_His');
        $type = 'xls'; 
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$csvData,$filename,$type);
        exit;
    }

}


