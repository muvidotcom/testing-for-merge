<?php

class UserfeatureController extends Controller {

    public $defaultAction = 'ratings';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        $this->pageTitle = Yii::app()->name . ' | Manage Home';
        return true;
    }

    /**
     * @function to save the rating option of the studio
     * @author RK<support@muvi.in>
     * @return HTML 
     */
    public function actionRatings() {
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);

        $this->breadcrumbs = array('User Features' , 'Ratings');
        $this->headerinfo = "Ratings";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Ratings';

        $rate = new ContentRating();
        $ratings = $rate->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id DESC'));
        $this->render('ratings', array('ratings' => $ratings, 'studio' => $std));
    }

    public function actionSavefeature() {

        $studio = $this->studio;

        if (isset($_REQUEST['rating_status']) && $_REQUEST['rating_status'] != 0) {
            $studio->rating_activated = 1;
        } else
            $studio->rating_activated = 0;
        $studio->save();
        Yii::app()->user->setFlash('success', 'Settings updated successfully!');
        $url = $this->createUrl('userfeature/ratings');
        $this->redirect($url);
    }

    public function actionDeleterating() {

        if (isset($_REQUEST['deleterating']) && $_REQUEST['deleterating'] > 0) {
            $studio_id = Yii::app()->common->getStudioId();
            $rate = new ContentRating();
            $rating = $rate->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['deleterating']));
            if (count($rating) > 0) {
                $rating->delete();
                Yii::app()->user->setFlash('success', 'Review is deleted!');
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            } else {
                Yii::app()->user->setFlash('error', "You don't have access to this review!");
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this review!");
            $url = $this->createUrl('userfeature/ratings');
            $this->redirect($url);
        }
    }

    public function actionReviewstatus() {

        if (isset($_REQUEST['reviewstatus']) && $_REQUEST['reviewstatus'] > 0) {
            $studio_id = Yii::app()->common->getStudioId();
            $rate = new ContentRating();
            $rating = $rate->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['reviewstatus']));
            if (count($rating) > 0) {
                $status = ($rating->status == 1) ? 0 : 1;
                $rating->status = $status;
                $rating->save();
                Yii::app()->user->setFlash('success', 'Review status changes!');
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            } else {
                Yii::app()->user->setFlash('error', "You don't have access to this review!");
                $url = $this->createUrl('userfeature/ratings');
                $this->redirect($url);
            }
        } else {
            Yii::app()->user->setFlash('error', "You don't have access to this review!");
            $url = $this->createUrl('userfeature/ratings');
            $this->redirect($url);
        }
    }
    
    public function actionNewsletter(){        
        $studio_id = Yii::app()->common->getStudiosId();
        $this->pageTitle="Muvi | Newsletter Subscribers";
        $this->breadcrumbs = array('User Features' , 'Newsletter Subscribers');
        $this->headerinfo = "Newsletter Subscribers";
        $sub = new NewsletterSubscribers();
        $subscribers = $sub->findAllByAttributes(array('studio_id' => $studio_id));             
        $this->render('newsletter', array('subscribers' => $subscribers));
    }   
    
    public function actionExportSubscribersExcel() {
        $studio_id = Yii::app()->common->getStudiosId();
        $headArr[0] = array('Email', 'Subscription Date', 'Registered User');
        $sheetName = array('Newsletter Subscribers');
        $sheet = 1;
        
        $sub = new NewsletterSubscribers();
        $subscribers = $sub->findAllByAttributes(array('studio_id' => $studio_id)); 
        $data = array();
        $regUser = '';
        foreach ($subscribers as $subscriber) {
            if ($subscriber->user_id) {
                $regUser = 'Yes';
            } else {
                $regUser = 'No';
            }
            $data[0][] = array(
                $subscriber->email,
                Yii::app()->common->phpDate($subscriber->date_subscribed),
                $regUser
            );            
        }

        $filename = 'newsletter_subscribers_' . date('Ymd_His');
        $type = 'xls';
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }    
    /*
     * Author ajit@muvi.com
     * 14/9/2016
     * user can enable facebook login through this function
     * issue id 4005
     */
    public function actionSocialnetworks() {
        $studio_id = Yii::app()->common->getStudioId();

        $std = Studio::model()->find(array('select'=>'status,is_subscribed,social_logins,is_default','condition' => 'id=:studio_id','params' => array(':studio_id' =>$studio_id)));
        $this->breadcrumbs = array('User Features' , 'Social network integration');
        $this->headerinfo = "Social network integration";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Social network integration';

        $social = new SocialLoginInfos();
        $sociallogininfos = $social->findByAttributes(array('studio_id' => $studio_id));
        $this->render('socialnetworks', array('sociallogininfos'=>$sociallogininfos,'status'=>$std));
}

    /*
     * Author ajit@muvi.com
     * 12/9/2016
     * user can enable facebook login through this function
     * issue id 4856
     */

    function actionfacebooklogin() {
        //echo "<pre>";print_r($_POST);exit;
        @extract($_POST);
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
		$enb_fblogin = array_sum($enb_fblogin);
        //check if enable checkbox is clicked or not
        if ($enb_fblogin) {
            //set parameter for email
            $user = User::model()->findByPk($user_id);
            $req['studioname'] = $this->studio->name;
            $req['name'] = $user->first_name;
            $req['email'] = $user->email;

            if ((isset($redirect_fb) && $redirect_fb == 'redirect_fb') || (isset($redirect_gp) && $redirect_gp == 'redirect_gp')){
				$st = '';
				$enable_array = array(0);
				if(($enb_fblogin & 1) && (isset($redirect_fb) && $redirect_fb == 'redirect_fb')){
					$st .= 'status = 1';
					$param = 'social_logins + 1';
				}
				if ($enb_fblogin & 2 && (isset($redirect_gp) && $redirect_gp == 'redirect_gp')){
					$st .= ' ,gplus_status = 1 ';
					$param = 'social_logins + 2';
				}
				$st = trim($st,' ,');
                Yii::app()->db->createCommand('UPDATE social_login_infos SET '.$st.' WHERE studio_id=' . $studio_id)->execute();

                //update status in studio table
				$sql_studio = 'UPDATE studios SET social_logins = '.$param.' WHERE id='.$studio_id;
				Yii::app()->db->createCommand($sql_studio)->execute();

                //send email
                $req['emailtype'] = 'active';
                Yii::app()->email->fbLoginRequest($req);
                Yii::app()->user->setFlash('success', "Social login activated successfully.");
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            }
            if ($id != '') {
                $studio = SocialLoginInfos::model()->findByPk($id);
                $studio->fb_app_id = $app_id;
                $studio->fb_secret = $app_secret;
                $studio->fb_app_verson = $app_version;
				$studio->gplus_client_id = $gplus_client_id;
                $studio->gplus_client_secret = $gplus_client_secret;
				if($whose_fb){
				$studio->status = ($enb_fblogin & 1)?(($whose_fb == 'muvi_fb') ? 2 : 3):$studio->status;
                $studio->whose_fb = ($enb_fblogin & 1)?$whose_fb:$studio->whose_fb;
				}
				if($whose_gplus){
					$studio->gplus_status = ($enb_fblogin & 2)?(($whose_gplus == 'muvi_gplus') ? 2 : 3) : $studio->gplus_status;
					$studio->whose_gplus = ($enb_fblogin & 2)?$whose_gplus:$studio->whose_gplus;
				}
                $studio->save();
                Yii::app()->user->setFlash('success', "Data updated successfully.");
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            } else {
                //save data to db - social_login_infos               
                $login = New SocialLoginInfos;           
                $login->studio_id = $studio_id;
                $login->fb_app_id = $app_id;
                $login->fb_secret = $app_secret;
                $login->fb_app_verson = $app_version;
                $login->created_date = gmdate('Y-m-d H:i:s');
                $login->created_by = $user_id;
                $login->status = ($enb_fblogin & 1)?(($whose_fb == 'muvi_fb') ? 2 : 3):0;
                $login->whose_fb = ($enb_fblogin & 1)?$whose_fb:'';
				$login->gplus_status = ($enb_fblogin & 2)?(($whose_gplus == 'muvi_gplus') ? 2 : 3) : 0;
				$login->whose_gplus = ($enb_fblogin & 2)?$whose_gplus:'';
				$login->gplus_client_id = $gplus_client_id;
				$login->gplus_client_secret = $gplus_client_secret;
                $login->save();
                //send email to admin and customer
                $req['emailtype'] = 'request';
                Yii::app()->email->fbLoginRequest($req);
                Yii::app()->user->setFlash('success', 'Social login request saved successfully');
                $this->redirect($this->createUrl('userfeature/socialnetworks'));
                exit;
            }
        }
    }

    function actionfacebookloginactive() {        
		//echo "<pre>";print_r($_POST);exit;
        @extract($_POST);
        $studio_id = Yii::app()->user->studio_id;
		if($social_logins==0){//deactivate
			$sql = 'UPDATE studios SET social_logins = social_logins -  1 WHERE id='.$studio_id;
			Yii::app()->db->createCommand($sql)->execute();
		}else if($social_logins==1){//activate
			$sql = 'UPDATE studios SET social_logins = social_logins + 1 WHERE id='.$studio_id;
			Yii::app()->db->createCommand($sql)->execute();
		}
        Yii::app()->user->setFlash('success', "Status updated successfully.");                
		echo 1;
        //$this->redirect($this->createUrl('userfeature/socialnetworks'));
        exit;
    }
        
	function actiongoogleloginactive() {
		//echo "<pre>";print_r($_POST);exit;
        @extract($_POST);
        $studio_id = Yii::app()->user->studio_id;
		if($social_logins==0){//deactivate
			$sql = 'UPDATE studios SET social_logins = social_logins -  2 WHERE id='.$studio_id;
			Yii::app()->db->createCommand($sql)->execute();
		}else if($social_logins==1){//activate
			$sql = 'UPDATE studios SET social_logins = social_logins + 2 WHERE id='.$studio_id;
			Yii::app()->db->createCommand($sql)->execute();
		}
        Yii::app()->user->setFlash('success', "Status updated successfully.");                
		echo 1;
        //$this->redirect($this->createUrl('userfeature/socialnetworks'));
        exit;
    }
        
    /*
     * 6372: My Library feature for a website
     * ajit@muvi.com, 31/01/2017
     * @settings for my library menu in frontend
     */    
    /*
    public function actionmyLibrary() {        
        $this->breadcrumbs = array('User Features' , 'My Library');
        $this->headerinfo = "My Library";
        $this->pageTitle = Yii::app()->name . ' | ' . 'My Library';
        $studio_id = Yii::app()->common->getStudioId();
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        if(count($_POST) && $_POST['sup']=='sup'){
            if (isset($_POST['mylibrary_activated']) && $_POST['mylibrary_activated'] == 1) {
                $std->mylibrary_activated = 1;
            } else {
                $std->mylibrary_activated = 0;
            }
            $std->save();
            Yii::app()->user->setFlash('success', 'Settings updated successfully!');
            $url = $this->createUrl('userfeature/mylibrary');
            $this->redirect($url);
        }
        $this->render('mylibrary', array('studio' => $std));
    } 
    
    public function actionAddToFavourite(){
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Add to Favorites');
        $this->headerinfo = "Add to Favorites";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Add to Favorites';
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        $status = 0;
        if(!empty($fav_status)){
            $status = $fav_status['config_value'];
        }
        $this->render('addtofavourite', array('status'=>$status));
    }
    public function actionUpdateFavourite(){
        if($_POST){
            $studio_id = Yii::app()->common->getStudioId();
            $fav_status = StudioConfig::model()->getConfig($studio_id,'user_favourite_list');
            $config_value = isset($_POST['favliststatus']) && intval($_POST['favliststatus'])?$_POST['favliststatus']:0;
            if(empty($fav_status)){
                $config = new StudioConfig;
                $config->studio_id = $studio_id;
                $config->config_key = 'user_favourite_list';
            }else{
                $config = StudioConfig::model()->findByPk($fav_status->id);
            }
            $config->config_value = $config_value;
            $config->save();
            Yii::app()->user->setFlash('success', "Status updated successfully.");                
            $this->redirect($this->createUrl('userfeature/addtofavourite'));
            exit; 
        }else{
            Yii::app()->user->setFlash('error', "You do not have access to this page.");                
            $this->redirect($this->createUrl('admin/dashboard'));
            exit;
        }
    }
    */
    public function actionSettings(){
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Settings');
        $this->headerinfo = "Settings";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Settings';
        //save
        if($_POST){
            //add to favorite            
            $fav_status = StudioConfig::model()->getConfig($studio_id,'user_favourite_list');
            $config_value = isset($_POST['addfav'])?$_POST['addfav']:0;
            $config = new StudioConfig;
            if(empty($fav_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_favourite_list';
            }else{
                $config = StudioConfig::model()->findByPk($fav_status->id);
            }
            $config->config_value = $config_value;
            $config->save();
            
            //my library
            $std = new Studio();
            $std = $std->findByPk($studio_id);
            $mylibrary_activated = isset($_POST['mylibrary_activated'])?$_POST['mylibrary_activated']:0;
            $std->mylibrary_activated = $mylibrary_activated;            
            $std->save();           
            
            //Verify Email for Registration
            $verify_email = StudioConfig::model()->getConfig($studio_id, 'Verify_Email_for_Registration');
            $verify_value = isset($_POST['verify']) ? $_POST['verify'] : 0;
            $vconfig = new StudioConfig;
            if (empty($verify_email)) {
                $vconfig->studio_id = $studio_id;
                $vconfig->config_key = 'Verify_Email_for_Registration';
            } else {
                $vconfig = StudioConfig::model()->findByPk($verify_email->id);
            }
            $vconfig->config_value = $verify_value;
            $vconfig->save();


            //watch history
            $watch_status = StudioConfig::model()->getConfig($studio_id,'studio_watch_history');
            $watch_value = isset($_POST['watch'])?$_POST['duration']:0;
            $wconfig = new StudioConfig;
            if(empty($watch_status)){
                //$config = new StudioConfig;
                $wconfig->studio_id = $studio_id;
                $wconfig->config_key = 'studio_watch_history';
            }else{
                $wconfig = StudioConfig::model()->findByPk($watch_status->id);
            }
            $wconfig->config_value = $watch_value;
            $wconfig->save(); 
            
            //reminder status
            $reminder_status = StudioConfig::model()->getConfig($studio_id,'reminder');
            $reminder_value  = isset($_POST['reminder'])? $_POST['reminder'] : 0;
            $rconfig = new StudioConfig;
            if(empty($reminder_status)){
                $rconfig->studio_id  = $studio_id;
                $rconfig->config_key = 'reminder';
            }else{
                $rconfig = StudioConfig::model()->findByPk($reminder_status->id);
            }
            $rconfig->config_value = $reminder_value;
            $rconfig->save(); 
            if($reminder_value){
                $notification_status = StudioConfig::model()->getConfig($studio_id, 'notification');
                $notification_value  = isset($_POST['notification'])? $_POST['notification'] : 0;
                $nconfig = new StudioConfig;
                if(empty($notification_status)){
                    $nconfig->studio_id  = $studio_id;
                    $nconfig->config_key = 'notification';
                }else{
                    $nconfig = StudioConfig::model()->findByPk($notification_status->id);
                }
                $nconfig->config_value = $notification_value;
                $nconfig->save(); 
                if($notification_value){
                    $reminder_before_status = StudioConfig::model()->getConfig($studio_id,'reminder_before');
                    $reminder_before_value  = isset($_POST['notify_before'])? $_POST['notify_before'] : 0;
                    $rbconfig = new StudioConfig;
                    if(empty($reminder_before_status)){
                        $rbconfig->studio_id  = $studio_id;
                        $rbconfig->config_key = 'reminder_before';
                    }else{
                        $rbconfig = StudioConfig::model()->findByPk($reminder_before_status->id);
                    }
                    $rbconfig->config_value = $reminder_before_value;
                    $rbconfig->save(); 
                }
            }
			$pllist_status = StudioConfig::model()->getConfig($studio_id,'user_playlist');
            $pllist_value = isset($_POST['enplaylist'])?$_POST['enplaylist']:0;
            $config = new StudioConfig;
            if(empty($pllist_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_playlist';
            }else{
                $config = StudioConfig::model()->findByPk($pllist_status->id);
            }
            $config->config_value = $pllist_value;
            $config->save();
			$que_status = StudioConfig::model()->getConfig($studio_id,'user_queue_list');
            $queue_value = isset($_POST['adque'])?$_POST['adque']:0;
            $config = new StudioConfig;
            if(empty($que_status)){                
                $config->studio_id = $studio_id;
                $config->config_key = 'user_queue_list';
            }else{
                $config = StudioConfig::model()->findByPk($que_status->id);
            }
            $config->config_value = $queue_value;
            $config->save();
            
            /* User Generated and User Review content */           
            $userGenerateContent = StudioConfig::model()->getConfig($studio_id, 'user_generated_content');
            $is_generate = !empty($_POST['user_generated_content']) ? 1 : 0;
            $ugcconfig = new StudioConfig;
            if (empty($userGenerateContent)) {
                //$config = new StudioConfig;
                $ugcconfig->studio_id = $studio_id;
                $ugcconfig->config_key = 'user_generated_content';
            } else {
                $ugcconfig = StudioConfig::model()->findByPk($userGenerateContent->id);
            }
            $ugcconfig->config_value = $is_generate;
            $ugcconfig->save();
            if ($ugcconfig == 1) {
                $ugcReview = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_review_content'));
                if (empty($ugcReview)) {
                    $review_generate = new StudioConfig();
                    $review_generate->studio_id = $studio_id;
                    $review_generate->config_key = 'user_review_content';
                } else {
                    $review_generate = StudioConfig::model()->findByPk($ugcReview->id);
                    $review_generate->id = $ugcReview->id;
                }
                $review_generate->config_value = !empty($_POST['user_review_content']) ? 1 : 0;
                $review_generate->save();
            }
			
            Yii::app()->user->setFlash('success', "Status updated successfully.");                
            $this->redirect($this->createUrl('userfeature/Settings'));
            exit; 
        } 
        
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        $status = 0;
        if(!empty($fav_status)){
            $status = $fav_status['config_value'];
        }
        $verify_email = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'Verify_Email_for_Registration');
        $verify_val = 0;
        if (!empty($verify_email)) {
            $verify_val = $verify_email['config_value'];
        }
        $watch_status = StudioConfig::model()->getConfig($studio_id, 'studio_watch_history');
        $watch_val = 0;
        if(!empty($watch_status)){
            $watch_val = $watch_status['config_value'];
        }  
        $reminder_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder');
        $reminder = 0;
        $notification = 0;
        $reminder_before = "";
        if(!empty($reminder_config)){
            $reminder = $reminder_config['config_value'];
            $notification_config    = StudioConfig::model()->getconfigvalueForStudio($studio_id,'notification');
            if(!empty($notification_config)){
                $notification = $notification_config['config_value'];
                $reminder_before_config = StudioConfig::model()->getconfigvalueForStudio($studio_id,'reminder_before');
                if(!empty($reminder_before_config)){
                    $reminder_before = $reminder_before_config['config_value'];
                } 
            }
        }
	
	
		$que_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_queue_list');
		$statusQueue = 0;
		if (!empty($que_status)) {
			$statusQueue = $que_status['config_value'];
		}
		$pl_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_playlist');
        $statusPlaylist = 0;
        if(!empty($pl_status)){
            $statusPlaylist= $pl_status['config_value'];
        }
        $std = Studio::model()->find(array('select'=>'mylibrary_activated','condition' => 'id=:studio_id','params' => array(':studio_id' =>$studio_id)));
        
        $user_generated_content = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_generated_content'));
        $user_review_content = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_review_content'));
        
        $this->render('settings', array('status'=>$status,'studio' => $std, 'verify_val' => $verify_val, 'watch_val' => $watch_val,'reminder' => $reminder, 'notification'=>$notification, 'reminder_before' => $reminder_before,'queue_enabled'=>$statusQueue,'playlist_enable'=>$statusPlaylist, 'is_generate_ugc' => $user_generated_content->config_value, 'is_review_ugc' => $user_review_content->config_value, 'studio_id'=>$studio_id));
    }
	public function actionUserRestrictions(){		
		$studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'User Restrictions');
        $this->headerinfo = "User Restrictions";
        $this->pageTitle = Yii::app()->name . ' | ' . 'User Restrictions';
		$studio_id = Yii::app()->common->getStudioId();
		 if(isset($_POST) && !empty($_POST)){
			 //Restrict No of devices
			 if(isset($_POST['restrict_no_devices'])){
					$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_no_devices');
					if(isset($sconfig) && count($sconfig) > 0){                
						$sconfig->config_value = 1;
						$sconfig->save();
					}else{                        
						$sconfig = New StudioConfig;
						$sconfig->studio_id = $studio_id;
						$sconfig->config_key = 'restrict_no_devices';
						$sconfig->config_value = 1;
						$sconfig->save();
}
			 }else{
					$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_no_devices');
					if(isset($sconfig) && count($sconfig) > 0){
						$sconfig->config_value = 0;
						$sconfig->save();
					}else{            
						$sconfig = New StudioConfig;
						$sconfig->studio_id = $studio_id;
						$sconfig->config_key = 'restrict_no_devices';
						$sconfig->config_value = 0;
						$sconfig->save();
					}
			 }	
			 //Limit Devices
			if (isset($_POST['limit_devices']) && $_POST['limit_devices'] !="") {
				$limit_devices = trim($_POST['limit_devices']);
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_devices');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = $limit_devices > 0 ? $limit_devices : 1;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_devices';
					$sconfig->config_value = $limit_devices > 0 ? $limit_devices : 1;
					$sconfig->save();
				}
			} else {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_devices');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 1;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_devices';
					$sconfig->config_value = 1;
					$sconfig->save();
				}
			} 
			//Device Switch Duration
			if (isset($_POST['restrict_device_switch_duration'])) {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = 1;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_device_switch_duration';
					$sconfig->config_value = 1;
					$sconfig->save();
				}            
			}else{
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_device_switch_duration';
					$sconfig->config_value = 0;
					$sconfig->save();
				}
			}
			//Limit Device switch duration
			if (isset($_POST['limit_device_switch_duration']) && $_POST['limit_device_switch_duration'] !="") {
				$limit_device_switch_duration = isset($_POST['restrict_device_switch_duration'])?trim($_POST['limit_device_switch_duration']):0;
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){                
					$sconfig->config_value = $limit_device_switch_duration > 0 ? $limit_device_switch_duration : 0;
					$sconfig->save();
				}else{                        
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_device_switch_duration';
					$sconfig->config_value = $limit_device_switch_duration > 0 ? $limit_device_switch_duration : 0;
					$sconfig->save();
				}
			} else {
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_device_switch_duration');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{            
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'limit_device_switch_duration';
					$sconfig->config_value = 0;
					$sconfig->save();
				}
			}  
			//Restrict no of streaming devices
			if(isset($_POST['restrict_streaming_devices']) && $_POST['restrict_streaming_devices'] !=""){
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
				$limit_streaming_devices = (isset($_POST['limit_streaming_devices']) && $_POST['limit_streaming_devices']>0)?$_POST['limit_streaming_devices']:0;
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = $limit_streaming_devices;
					$sconfig->save();
				}else{
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_streaming_device';
					$sconfig->config_value = $limit_streaming_devices;
					$sconfig->save();
				}
			}else{
				$sconfig = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
				if(isset($sconfig) && count($sconfig) > 0){
					$sconfig->config_value = 0;
					$sconfig->save();
				}else{
					$sconfig = New StudioConfig;
					$sconfig->studio_id = $studio_id;
					$sconfig->config_key = 'restrict_streaming_device';
					$sconfig->config_value = 0;
					$sconfig->save();
				}				
			}
            //Daily watch duration
            if(isset($_POST['watch_duration']) && $_POST['watch_duration'] != "" ){
                $sconfig = StudioConfig::model()->getConfig($studio_id, 'daily_watch_duration');
                $watch_duration = (isset($_POST['watch_duration'])) ? $_POST['watch_duration'] : 0;
                $watch_duration_sec = strtotime("1970-01-01 $watch_duration UTC");
                if (isset($sconfig) && count($sconfig) > 0) {
                    $sconfig->config_value = $watch_duration_sec;
                    $sconfig->save();
                } else {
                    $sconfig = New StudioConfig;
                    $sconfig->studio_id = $studio_id;
                    $sconfig->config_key = 'daily_watch_duration';
                    $sconfig->config_value = $watch_duration_sec;
                    $sconfig->save();
                }
            } else {
                $sconfig = StudioConfig::model()->getConfig($studio_id, 'daily_watch_duration');
                if (isset($sconfig) && count($sconfig) > 0) {
                    $sconfig->config_value = 0;
                    $sconfig->save();
                } else {
                    $sconfig = New StudioConfig;
                    $sconfig->studio_id = $studio_id;
                    $sconfig->config_key = 'daily_watch_duration';
                    $sconfig->config_value = 0;
                    $sconfig->save();
                }
            }
			 Yii::app()->user->setFlash('success', "Status updated successfully.");                
             $this->redirect($this->createUrl('userfeature/UserRestrictions'));
             exit;
		 }
			 //check app selected or not
            $checkAppSelected=Yii::app()->custom->checkAppSelected($studio_id);
            //Restrict Devices
            $restrict_no_devices=Yii::app()->custom->restrictDevices($studio_id,'restrict_no_devices');
            $limit_devices=($restrict_no_devices>0)?Yii::app()->custom->restrictDevices($studio_id,'limit_devices'):1;           
            //Restrict Device switch Duration
            $restrict_device_switch_duration=Yii::app()->custom->restrictDevices($studio_id,'restrict_device_switch_duration');
            $limit_device_switch_duration=($restrict_device_switch_duration>0)?Yii::app()->custom->restrictDevices($studio_id,'limit_device_switch_duration'):5;
			//Get no streaming devices
			$restrict_streaming_devices = Yii::app()->custom->restrictDevices($studio_id,'restrict_streaming_device');
            //Get Daily watch duration
            $watch_duration_sec = Yii::app()->custom->restrictDevices($studio_id, 'daily_watch_duration');
            if($watch_duration_sec >0){
                $watch_duration = date('H:i', mktime(0, 0, $watch_duration_sec));
            }
            $this->render('user_restrictions', array('restrict_no_devices' => $restrict_no_devices, 'limit_devices' => $limit_devices, 'restrict_device_switch_duration' => $restrict_device_switch_duration, 'limit_device_switch_duration' => $limit_device_switch_duration, 'checkAppSelected' => $checkAppSelected, 'restrict_streaming_devices' => $restrict_streaming_devices , 'watch_duration' => $watch_duration ));
    }

	public function actionmanageQueue() {
		$studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('User Features' , 'Manage Queue');
        $this->headerinfo = "Manage Queue";
        $this->pageTitle = Yii::app()->name . ' | ' . 'Manage Queue';
		if($_POST['queuebtn'] == 'save' ){
			//Auto play next episode
			$autoPlay = StudioConfig::model()->getConfig($studio_id, 'autoplay_next_episode');
			$config_value = isset($_POST['autoPlay']) ? $_POST['autoPlay'] : 0;
			$config = new StudioConfig;
			if (empty($autoPlay)) {
				$config->studio_id = $studio_id;
				$config->config_key = 'autoplay_next_episode';
			} else {
				$config = StudioConfig::model()->findByPk($autoPlay->id);
			}
			$config->config_value = $config_value;
			$config->save();
			//Realted content
			$relCon = StudioConfig::model()->getConfig($studio_id, 'related_content');
			$config_rel_value = isset($_POST['related_content']) ? $_POST['related_content'] : 0;
			$configrel = new StudioConfig;
			if (empty($relCon)) {
				$configrel->studio_id = $studio_id;
				$configrel->config_key = 'related_content';
			} else {
				$configrel = StudioConfig::model()->findByPk($relCon->id);
			}
			$configrel->config_value = $config_rel_value;
			$configrel->save();
		}
		$auto_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'autoplay_next_episode');
		$status_auto = 1;
		if ($auto_status != '') {
			$status_auto = $auto_status['config_value'];
		}
		$rel_content_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'related_content');
		$status_rel = 0;
		if ($rel_content_status != '') {
			$status_rel = $rel_content_status['config_value'];
		}
		$this->render('managequeue',array('autoplay_enable'=>$status_auto,'related_content_enable'=>$status_rel));		 
    }
    public function actionMobileApps() {
		$chromecast = 0;
		$offline = 0;
		$offline_view_access_period = 0;
		$offline_view_access_period_limit = 1;

		$studio_id = Yii::app()->common->getStudioId();
		$chromecast_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'chromecast');
		if ($chromecast_val['config_value'] == 1 || $chromecast_val == '') {
			$chromecast = 1;
		}
		$offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
		if ($offline_val['config_value'] == 1 || $offline_val == '') {
			$offline = 1;
		}
		$offline_view_access_period_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view_access_period');
		if ($offline_view_access_period_val['config_value'] == 1) {
			$offline_view_access_period = 1;
		}
		$offline_view_access_period_limit_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view_access_period_limit');
		if ($offline_view_access_period_limit_val['config_value']) {
			$offline_view_access_period_limit = $offline_view_access_period_limit_val['config_value'];
		}

		$this->breadcrumbs = ['User Features', 'Mobile Apps'];
		$this->headerinfo = "Mobile Apps";
		$this->pageTitle = Yii::app()->name . ' | ' . 'Mobile Apps';
		$this->render('mobileapps', array('chromecast' => $chromecast, 'offline' => $offline, 'offline_view_access_period' => $offline_view_access_period, 'offline_view_access_period_limit' => $offline_view_access_period_limit));
	}
	public function actionSaveApps() {
		$studio_id = Yii::app()->common->getStudioId();
		$update_chrm = $_POST['chromecast'];
		$update_off = $_POST['offline_view'];
		$offline_view_access_period = isset($_POST['offline_view_access_period']) ? $_POST['offline_view_access_period'] : 0;
		$offline_view_access_period_limit = isset($_POST['offline_view_access_period_limit']) ? $_POST['offline_view_access_period_limit'] : 1;

		if ($update_chrm == '') {
			$update_chrm = 0;
		}
		if ($update_off == '') {
			$update_off = 0;
		}
		$chromecast_key = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'chromecast'));
		if ($chromecast_key != '') {
			Yii::app()->db->createCommand()->update('studio_config', array('config_value' => $update_chrm), 'id = :id', array(':id' => $chromecast_key['id']));
		} else {
			Yii::app()->db->createCommand()->insert('studio_config', array('studio_id' => $studio_id, 'config_key' => 'chromecast', 'config_value' => $update_chrm), 'id = :id', array(':id' => $chromecast_key['id']));
		}
		$offline_key = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'offline_view'));
		if ($offline_key != '') {
			Yii::app()->db->createCommand()->update('studio_config', array('config_value' => $update_off), 'id = :id', array(':id' => $offline_key['id']));
		} else {
			Yii::app()->db->createCommand()->insert('studio_config', array('studio_id' => $studio_id, 'config_key' => 'offline_view', 'config_value' => $update_off), 'id = :id', array(':id' => $offline_key['id']));
		}

		$offline_view_access_period_key = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'offline_view_access_period'));
		if ($offline_view_access_period_key != '') {
			Yii::app()->db->createCommand()->update('studio_config', array('config_value' => $offline_view_access_period), 'id = :id', array(':id' => $offline_view_access_period_key['id']));
		} else {
			Yii::app()->db->createCommand()->insert('studio_config', array('studio_id' => $studio_id, 'config_key' => 'offline_view_access_period', 'config_value' => $offline_view_access_period), 'id = :id', array(':id' => $offline_view_access_period_key['id']));
		}

		$offline_view_access_period_limit_key = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'offline_view_access_period_limit'));
		if ($offline_view_access_period_limit_key != '') {
			Yii::app()->db->createCommand()->update('studio_config', array('config_value' => $offline_view_access_period_limit), 'id = :id', array(':id' => $offline_view_access_period_limit_key['id']));
		} else {
			Yii::app()->db->createCommand()->insert('studio_config', array('studio_id' => $studio_id, 'config_key' => 'offline_view_access_period_limit', 'config_value' => $offline_view_access_period_limit), 'id = :id', array(':id' => $offline_view_access_period_limit_key['id']));
		}

		Yii::app()->user->setFlash('success', "Status updated successfully.");
		$this->redirect($this->createUrl('userfeature/mobileapps'));
		exit;
	}

     public function actioncustomprofile() {
        $studio_id = Yii::app()->common->getStudioId();
        $this->pageTitle = "Muvi | Manage User Profile";
        $this->breadcrumbs = array('End User Account', 'Manage User Profile' => array('userfeature/customprofile'));
        $this->headerinfo = "Manage User Profile";
        $getDefaultField = CustomField::model()->findAll('studio_id=:studio_id AND status=:status ORDER BY id_seq', array(':studio_id' => $studio_id, ':status' => '1'));
        //echo "<pre>";print_r($getDefaultField); exit;
        $this->render('customprofile', array('defaultField' => $getDefaultField));
    }

    public function actionAddNewField() {
        $field_type = array('Textarea', 'TextField', 'Dropdown', 'List', 'Radio', 'Checkbox', 'Date Picker');
        if (isset($_POST['id']) && is_numeric($_POST['id'])) {
            $id = $_POST['id'];
            $flag = $_POST['flag'];
            $cf = CustomField::model()->findByPk($id);
        }
        $this->renderPartial('addnewfield', array('field_type' => $field_type, 'cf' => $cf, 'flag' => $flag, 'lang_code'=>@$_POST['lang_code']));
    }

    public function actionSaveNewField() {
        if (isset($_POST['formData']) && $_POST['formData']) {
            parse_str($_POST['formData'], $formData);
            $formData = $formData['data'];
            //$msg = "Field inserted successfully.";
            if (!empty($formData)) {
                if ($formData['edit_id'] && is_numeric($formData['edit_id'])) {
                    $id = $formData['edit_id'];
                }
                $studio_id = $this->studio->id;
                $ret = CustomField::model()->saveCustomField($formData, $studio_id, $id);
                $msg = ($last_id) ? $msg : 0;
                echo $ret['msg'];
            }
        }
    }

    public function actiongetCustomField() {
        $studio_id = Yii::app()->common->getStudioId();
        $getCsutonField = CustomField::model()->findAll('studio_id=:studio_id AND status=:status', array(':studio_id' => $studio_id, ':status' => '0'));
        $getDefaultField = CustomField::model()->findAll('studio_id=:studio_id AND status=:status ORDER BY id_seq', array(':studio_id' => $studio_id, ':status' => '1'));
        $this->renderPartial('custom_field', array('template_data' => $getCsutonField, 'defaultField' => $getDefaultField));
    }

    public function actionRemoveCustomField() {
        $flag = 0;
        if (isset($_POST['id']) && is_numeric($_POST['id'])) {
            $id = $_POST['id'];
            $removecf = CustomField::model()->findByPk($id);
            $placeholder_key = $removecf->field_id . '_placeholder';
            $row = CustomField::model()->deleteByPk($id, 'studio_id=:studio_id', array(':studio_id' => $this->studio->id));
            if ($row) {
                $flag = 1;
                TranslateKeyword::model()->DeleteKey($this->studio->id, $removecf->field_id);
                TranslateKeyword::model()->DeleteKey($this->studio->id, $placeholder_key);
            }
        }
        echo $flag;
        exit;
    }

    public function actionSaveCustomeForm() {
        $studio_id = Yii::app()->common->getStudioId();
        $custom = $_POST['custom'];
        $ucommand = Yii::app()->db->createCommand();
        if (!empty($custom)) {
            $sort = 1;
            foreach ($custom as $key => $value) { //print_r($value); exit;
                $command = Yii::app()->db->createCommand();
                $id[] = $value['id'];
                $update = $command->update('custom_field', array('field_name' => $value['field_name'], 'id_seq' => $sort, 'status' => '1'), 'id=:id AND studio_id=:studio_id', array(':id' => $value['id'], ':studio_id' => $studio_id));
                $sort++;
            }
            $id = implode(',', $id);
            $update = $ucommand->update('custom_field', array('status' => '0'), 'id NOT IN (' . $id . ') AND studio_id=:studio_id', array(':studio_id' => $studio_id));
            Yii::app()->user->setFlash('success', 'Custom field saved successfully.');
        } else if (empty($custom) && $_POST['data_available']) {
            $update = $ucommand->update('custom_field', array('status' => '0'), 'studio_id=:studio_id', array(':studio_id' => $studio_id));
            Yii::app()->user->setFlash('success', 'Custom field saved successfully.');
        } else {
            Yii::app()->user->setFlash('error', 'Custom field failure to save.');
        }
        $this->redirect($this->createUrl('userfeature/customprofile'));
    }

    public function actionCheckUniqueFieldName() {
        $formData = $_POST;
        $formData = trim($_POST['fieldname']);
        $f_name = '';
        $studio_id = $this->studio->id;
        if (!empty($formData)) {
            $f_name = self::checkname($studio_id, str_replace(" ", "_", strtolower($formData))); //check name of field exist or not  
        }
        echo $f_name;
    }

    function checkname($studio_id, $f_name, $f_display_name = false) {
        $exists = CustomField::model()->exists('studio_id=:sid AND field_id=:name', array(":sid" => $studio_id, ":name" => $f_name));
        $checkarray = self::CheckUniqueArray();
        if ($exists || in_array($f_name, $checkarray)) {
            $f_name = self::createfname($f_name);
            return self::checkname($studio_id, $f_name, $f_display_name);
        } else {
            return $f_name;
        }
    }

    public function createfname($f_name) {
        $format_perm = preg_replace("/\d+$/", "", $f_name);
        $parts = explode($format_perm, $f_name);
        $end = 1;
        if (count($parts) > 0) {
            $end = (int) $parts[1];
            $end++;
        }
        $f_name = $format_perm . $end;
        return $f_name;
    }

    function CheckUniqueArray() {
        return array('content_name', 'name', 'movie_id', 'is_episode', 'title', 'content_title', 'play_btn', 'permalink', 'poster', 'data_type', 'is_landscape', 'release_date', 'full_release_date', 'censor_rating', 'movie_uniq_id', 'stream_uniq_id', 'video_duration', 'video_duration_text', 'ppv', 'payment_type', 'is_converted', 'movie_stream_id', 'uniq_id', 'content_type_id', 'content_types_id', 'ppv_plan_id', 'full_movie', 'story', 'short_story', 'genres', 'display_name', 'content_permalink', 'trailer_url', 'trailer_is_converted', 'trailer_player', 'casts', 'casting', 'content_banner', 'reviewformonly', 'reviewsummary', 'reviews', 'defaultresolution', 'multiplevideo');
    }

    public function actioneditprofile() {
        $this->layout = false;
        $studio_id = Yii::app()->common->getStudioId();
        if (isset($_POST['id']) && $_POST['id']) {
            $id = $_POST['id'];
            $usr = new SdkUser();
            $user = $usr->findByPk($id);
			$is_cms = 1;
            $custom_fields = Yii::app()->general->getCustomFields($studio_id, 1, $id, $user, 2, $is_cms);
            $this->render('edituserprofile', array('user' => $user, 'studio_id' => $studio_id, 'custom_fields' => $custom_fields));
        }
        exit;
    }

    public function actiongetUserList() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $trans_word = array();
        $form_type = 1;
        $sheet = 1;
        $getUser = SdkUser::model()->findAll(array('select' => 'id,email,display_name', 'condition' => 'studio_id=:studio_id AND is_studio_admin!=1', 'params' => array(':studio_id' => $studio_id)));
        $getCustomField = CustomField::model()->findAll('studio_id=:studio_id AND form_type=1 AND input_type!=8 AND (is_default=:is_default OR status=:status) ORDER BY id_seq ASC', array(':studio_id' => $studio_id, ':is_default' => '1', ':status' => 1));
        $count = 0;
        foreach ($getUser as $user) {
            if ($this->chkStudioForCustomProfile) {
                $i = 0;
                if (!empty($getCustomField)) {
                    foreach ($getCustomField as $keys => $field) {
                        if ($count === 0) {
                            $headArr[0][] = $field->field_label;
                        }
                        $value = Yii::app()->general->getCustomFieldsValue($studio_id, $form_type, $user->id, $field->id);
                        $customValue = $value[0];
                        if (count($value) > 1) {
                            $customValue = implode(',', $value);
                        }

                        if ($field->field_id == trim('name')) {
                            $list[0][$count][$i] = $user->display_name;
                        } else if ($field->field_id == trim('email_address')) {
                            $list[0][$count][$i] = $user->email;
                        } else {
                            $list[0][$count][$i] = $customValue;
                        }
                        $i++;
                    }
                }
            } else {
                if ($count === 0) {
                    $headArr[0][0] = 'Name';
                    $headArr[0][1] = 'Email';
                }
                $list[0][$count][0] = $user->display_name;
                $list[0][$count][1] = $user->email;
                $i = 2;
                if (!empty($getCustomField)) {
                    foreach ($getCustomField as $keys => $field) {
                        if ($count === 0) {
                            $headArr[0][$i] = $field->field_label;
                        }
                        $value = Yii::app()->general->getCustomFieldsValue($studio_id, $form_type, $user->id, $field->id);
                        $customValue = $value[0];
                        if (count($value) > 1) {
                            $customValue = implode(',', $value);
                        }
                        $list[0][$count][$i] = $customValue;
                        $i++;
                    }
                }
            }
            $count++;
        }
        $type = 'xls';
        $filename = 'user_list' . date('Ymd_His');
        $headArr = array_values($headArr);
        $sheetName = array('userlist');
        $data = array_values($list);
        $sheet = 1;
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }
}
