<?php

class PricingController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public $layout='main';     
    public $defaultAction = 'calculator';
    public function actionCalculator(){
        $this->pageTitle = 'Muvi VoD ROI Calculator - Cost to build VoD platform, Netflix, Hulu, HBO Go Clone';
        $this->pageKeywords = 'Muvi.com, Muvi SDK, SDK, VoD, PPV, Video on Demand, Video, Pay per View, video content, upload video library, monetize video, video monetization, video revenue';
        $this->pageDescription = 'Calculate the bandwidth cost or cost to build your own VoD platform like Netflix clone, Hulu clone, HBO Go clone, and calculate the ROI of your VoD business';            
        $this->render('calculator');
    }
}