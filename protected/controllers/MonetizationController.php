<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
class MonetizationController extends Controller {

    public $defaultAction = 'home';
    public $headerinfo = '';
    public $layout = 'admin';
    public $isAllowResetPolicy;

    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        $this->isAllowResetPolicy=true;
        return true;
    }

    function actionSubscriptions() {
        $this->breadcrumbs = array('Monetization', 'Subscriptions (SVOD)');
        $this->headerinfo = 'Subscriptions (SVOD)';
        $this->pageTitle = Yii::app()->name . ' | ' . Subscriptions . ' (SVOD)';

        $studio = $this->studio;
        $studio_id = $studio->id;
        $language_id = $this->language_id;
        $spsql = "SELECT sp.id AS plan_id, sp.status AS plan_status, sp.*, spr.id AS pricing_id, spr.status AS price_status, spr.price,spr.currency_id,spr.subscription_plan_id, cu.code, cu.symbol FROM subscription_plans sp LEFT JOIN subscription_pricing spr ON (sp.id=spr.subscription_plan_id AND spr.status=1) LEFT JOIN currency AS cu ON (spr.currency_id=cu.id) WHERE sp.studio_id={$studio_id} AND (sp.language_id={$language_id} OR sp.parent_id=0 AND sp.id NOT IN (SELECT parent_id FROM subscription_plans WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY status DESC, id_sequency ASC, IF(sp.parent_id >0, sp.parent_id, sp.id) ASC, FIND_IN_SET(spr.currency_id, {$studio->default_currency_id}) DESC";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($spsql)->queryAll();

        $res = array();
        if (isset($data) && !empty($data)) {
            $array = array('pricing_id', 'subscription_plan_id', 'currency_id', 'price', 'price_status', 'code', 'symbol');

            foreach ($data as $key => $value) {
            //$csql = "SELECT f.id AS content_id, f.name FROM films f LEFT JOIN subscriptionbundles_content as pc on pc.content_id=f.id WHERE f.studio_id=".$studio_id." AND pc.subscriptionbundles_plan_id=".$value['id']." ORDER BY f.content_types_id, f.name ASC";
            //$contentNames = $dbcon->createCommand($csql)->queryAll(); 
            $contentNames = Yii::app()->db->createCommand()
                       ->select(' f.id AS content_id, f.name ')
                       ->from("films  f")
                       ->leftJoin('subscriptionbundles_content pc' , 'pc.content_id=f.id')
                       ->where('f.studio_id=:studio_id AND pc.subscriptionbundles_plan_id=:subscriptionbundles_plan_id',array(':studio_id'=>$studio_id,':subscriptionbundles_plan_id'=>$value['id']))
                       ->order(' f.content_types_id, f.name ASC ') 
                       ->queryAll(); 
            
            $value['content_name']= $contentNames;
                if ($value['plan_id'] == $value['subscription_plan_id']) {
                    if (!in_array($value['plan_id']['plan'], $res)) {
                        $res[$value['plan_id']]['plan'] = $value;

                        foreach ($array as $key1 => $value1) {
                            unset($res[$value['plan_id']]['plan'][$value1]);
                        }
                    }

                    foreach ($array as $key1 => $value1) {
                        $res[$value['plan_id']]['price'][$value['pricing_id']][$value1] = $value[$value1];
                    }
                }elseif($value['subscription_plan_id'] ==""){
                    $subprices = Yii::app()->general->checkSubPrice($value['parent_id'],$studio->default_currency_id);
                    if($subprices){
                        foreach($subprices as $subprice){
                        $value['pricing_id']           = $subprice['pricing_id'];
                        $value['price_status']         = $subprice['price_status'];
                        $value['price']                = $subprice['price'];
                        $value['currency_id']          = $subprice['currency_id'];
                        $value['subscription_plan_id'] = $subprice['subscription_plan_id'];
                        $value['code']                 = $subprice['code'];
                        $value['symbol']               = $subprice['symbol'];
                        if (!in_array($value['plan_id']['plan'], $res)) {
                            $res[$value['plan_id']]['plan'] = $value;

                            foreach ($array as $key1 => $value1) {
                                unset($res[$value['plan_id']]['plan'][$value1]);
                            }
                        }
                        foreach ($array as $key1 => $value1) {
                            $res[$value['plan_id']]['price'][$value['pricing_id']][$value1] = $value[$value1];
                        }
                        }
                    }
                }
            }
        }
        $has_pay_gateway = Yii::app()->common->checkPaymentGateway();
        $this->render('subscriptions', array('data' => $res, 'language_id' => $language_id, 'has_pay_gateway' => $has_pay_gateway));
    }
    function actionAddEditSubscription() {
        $this->layout = false;
        $res = '';
        $language_id = $this->language_id;
        $studio = $this->studio;
        $studio_id = $studio->id;

        if (isset($_REQUEST['subscription_plan_id']) && !empty($_REQUEST['subscription_plan_id'])) {
            $isPurchased = Yii::app()->policyrule->chkUesrSubscription($_REQUEST['subscription_plan_id']);
            $con = Yii::app()->db;
            $spsql = "SELECT sp.id AS plan_id,sp.short_desc, sp.status AS plan_status, sp.*, spr.id AS pricing_id, spr.status AS price_status, spr.price,spr.currency_id,spr.subscription_plan_id, cu.code, cu.symbol FROM subscription_plans sp LEFT JOIN subscription_pricing spr ON (sp.id=spr.subscription_plan_id) LEFT JOIN currency AS cu ON (spr.currency_id=cu.id) WHERE sp.id={$_REQUEST['subscription_plan_id']} AND sp.studio_id={$studio_id} AND (sp.language_id={$language_id} OR sp.parent_id=0 AND sp.id NOT IN (SELECT parent_id FROM subscription_plans WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY sp.id ASC,sp.status DESC, FIND_IN_SET(spr.currency_id, {$studio->default_currency_id}) DESC";
            $res = $con->createCommand($spsql)->queryAll();
            $ress = $res[0];
                $subpric = Yii::app()->general->checkSubPrice($ress['parent_id']);
                if($ress['subscription_plan_id'] ==""){
                    foreach($subpric as $subprice){
                        $value['pricing_id']           = $subprice['pricing_id'];
                        $value['price_status']         = $subprice['price_status'];
                        $value['price']                = $subprice['price'];
                        $value['currency_id']          = $subprice['currency_id'];
                        $value['subscription_plan_id'] = $subprice['subscription_plan_id'];
                        $value['code']                 = $subprice['code'];
                        $value['symbol']               = $subprice['symbol'];
                        $result[]=array_replace($ress,$value);
                    }
                    $res = $result;
                }
        } else if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $data = $_REQUEST['data'];
            $isPurchased = Yii::app()->policyrule->chkUesrSubscription($data['id']);
            if (isset($data['id']) && !empty($data['id'])) {
                $SubscriptionPlansModel = SubscriptionPlans::model()->findByPk($data['id']);
                $SubscriptionPlansModel->last_updated_by = Yii::app()->user->id;
                $SubscriptionPlansModel->last_updated_date = date('Y-m-d H:i:s');
                if ($language_id != $SubscriptionPlansModel->language_id) {
                    $SubscriptionPlansModel = New SubscriptionPlans;
                    $SubscriptionPlansModel->parent_id = $data['id'];
                    $SubscriptionPlansModel->last_updated_by = '';
                    $SubscriptionPlansModel->last_updated_date = '';
                    $SubscriptionPlansModel->created_by = Yii::app()->user->id;
                    $SubscriptionPlansModel->created_date = date('Y-m-d H:i:s');
                    $SubscriptionPlansModel->language_id = $language_id;
                    $SubscriptionPlansModel->id_sequency = $SubscriptionPlansModel->id_sequency;
                }
                $err_msg = 'Oops! Sorry, Subscription can not be updated!';
                $suc_msg = 'Subscription has updated successfully';
            } else {
                $con = Yii::app()->db;
                $sql = "SELECT MAX(id_sequency) as id_sequency FROM subscription_plans WHERE studio_id={$studio_id} AND parent_id=0";
                $id_sequency = $con->createCommand($sql)->queryROW();
                $id_sequency = (isset($id_sequency['id_sequency'])) ? $id_sequency['id_sequency'] + 1 : 0;
                
                $SubscriptionPlansModel = New SubscriptionPlans;
                $err_msg = 'Oops! Sorry, Subscription can not be added!';
                $suc_msg = 'Subscription has added successfully';
                $SubscriptionPlansModel->created_by = Yii::app()->user->id;
                $SubscriptionPlansModel->created_date = date('Y-m-d H:i:s');
                $SubscriptionPlansModel->parent_id = 0;
                $SubscriptionPlansModel->id_sequency = $id_sequency;
            }

            $duration = explode(' ', $data['recurrence']);
            $frequency = $duration[0];
            $recurrence = $duration[1];
            
            $SubscriptionPlansModel->studio_id = $studio_id;
            $SubscriptionPlansModel->status = 1;
            $SubscriptionPlansModel->name = $data['name'];
            $SubscriptionPlansModel->short_desc = (isset($data['short_desc']) && trim($data['short_desc'])) ? $data['short_desc'] : '';
            $SubscriptionPlansModel->recurrence = $recurrence;
            $SubscriptionPlansModel->unique_id = Yii::app()->common->generateUniqNumber();
            $SubscriptionPlansModel->frequency = $frequency;
            if (isset($_REQUEST['free_trial_period']) && intval(['free_trial_period'])) {
                if (isset($data['trial_recurrence']) && trim($data['trial_recurrence'])) {
                    $trial_period = explode(" ", $data['trial_recurrence']);
                    $SubscriptionPlansModel->trial_period = $trial_period[0];
                    $SubscriptionPlansModel->trial_recurrence=$trial_period[1] ;
                }
            } else {
                $SubscriptionPlansModel->trial_period = 0;
                $SubscriptionPlansModel->trial_recurrence = '';
            }
            $SubscriptionPlansModel->is_subscription_bundle  = 0; 
             if (isset($_REQUEST['specific_content']) && intval($_REQUEST['specific_content'])) {
                $SubscriptionPlansModel->is_subscription_bundle  = 1; 
             }
             if (!empty($data['rule'])) {
               if(!$isPurchased){
                $SubscriptionPlansModel->rules_id = $data['rule'];
               }
             }
            
            if ($SubscriptionPlansModel->save()) {
                $plan_id = $SubscriptionPlansModel->id;
                SubscriptionBundlesContent::model()->deleteAll("subscriptionbundles_plan_id=:subscriptionbundles_plan_id", array(':subscriptionbundles_plan_id' => $plan_id));
                 if (isset($_REQUEST['data']['content']) && !empty($_REQUEST['data']['content'] && intval($_REQUEST['specific_content']))) {
                //Before saving, 1st delete all content related to plan and then save the content
                //Save content
                foreach ($_REQUEST['data']['content'] as $key => $value) {
                    $content = New SubscriptionBundlesContent;
                    $content->subscriptionbundles_plan_id = $plan_id;
                    $content->content_id = $value;
                    $content->save();
                }
            }else{
                 SubscriptionBundlesContent::model()->deleteAll("subscriptionbundles_plan_id=:subscriptionbundles_plan_id", array(':subscriptionbundles_plan_id' => $plan_id));

            }
                //Update all child records when get update of parent record 
                if ((isset($data['id']) && !empty($data['id']))) {
                    if ($SubscriptionPlansModel->parent_id == 0) {
                        $trial = ",trial_recurrence='',trial_period=0";
                        if (isset($_REQUEST['free_trial_period']) && intval(['free_trial_period'])) {
                            if (isset($data['trial_recurrence']) && trim($data['trial_recurrence'])) {
                                $trial_period = explode(" ", $data['trial_recurrence']);
                                $trial = ",trial_recurrence='{$trial_period[1]}',trial_period={$trial_period[0]}";
                            }
                        }
                        $where = " studio_id= {$studio_id} AND parent_id=" . $data['id'];
                        $sql = "UPDATE subscription_plans SET recurrence ='{$recurrence}', frequency={$frequency} " . $trial . " WHERE " . $where;
                        $dbcon = Yii::app()->db;
                        $cc_cmd = $dbcon->createCommand($sql);
                        $cc_cmd->execute();
                    }
                }

                $isPrice = 1;
                if ((isset($data['id']) && !empty($data['id']))) {
                    if ($SubscriptionPlansModel->parent_id != 0) {
                        $isPrice = 0;
                    }
                }

                //Update price record when parent record is changed
                if ($isPrice) {
                    //Update status to disable first
                    if (isset($data['id']) && !empty($data['id'])) {
                        $sql = "UPDATE subscription_pricing SET status=0 WHERE subscription_plan_id=" . $plan_id;
                        $dbcon = Yii::app()->db;
                        $cc_cmd = $dbcon->createCommand($sql);
                        $cc_cmd->execute();
                    }

                    $status = explode(',', $data['pricestatus']);
                    foreach ($status as $key => $value) {
                        $key = array_search($value, $data['currency_id']);

                        $sprModel = New SubscriptionPricing;

                        if (isset($data['pricing_id'][$key]) && intval($data['pricing_id'][$key])) {
                            $sprModel = $sprModel->findByPk($data['pricing_id'][$key]);
                        }

                        $sprModel->subscription_plan_id = $plan_id;
                        $sprModel->currency_id = $data['currency_id'][$key];
                        $sprModel->price = $data['price'][$key];
                        $sprModel->status = 1;
                        $sprModel->save();
                    }
                }

                Yii::app()->user->setFlash('success', $suc_msg);
            } else {
                Yii::app()->user->setFlash('error', $err_msg);
            }

            $url = $this->createUrl('monetization/subscriptions');
            $this->redirect($url);
        }
      if(isset($_REQUEST['subscription_plan_id']) && !empty($_REQUEST['subscription_plan_id'])){
           $id = $_REQUEST['subscription_plan_id'];
           $content = self::getSubscriptionBundlesContents($id);
        }
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('addeditsubscription', array('res' => $res,'content'=>$content,'currency' => $currency, 'studio' => $studio, 'language_id' => $language_id, 'policyStatus' => PolicyRules::model()->getConfigRule(), 'isPurchased'=>$isPurchased));
    }

    function actiondisableSubscription() {
        if (isset($_REQUEST['id_subscription']) && !empty($_REQUEST['id_subscription'])) {
            $SubscriptionPlansModel = SubscriptionPlans::model()->findByPk($_REQUEST['id_subscription']);
            $SubscriptionPlansModel->status = 0;
            $SubscriptionPlansModel->save();
            if ($SubscriptionPlansModel->parent_id == 0) {
                $studio_id = Yii::app()->user->studio_id;

                $sql = "UPDATE subscription_plans SET status=0 WHERE studio_id=". $studio_id." AND parent_id=".$SubscriptionPlansModel->id;
                $dbcon = Yii::app()->db;
                $cc_cmd = $dbcon->createCommand($sql);
                $cc_cmd->execute();
            }
            Yii::app()->user->setFlash('success', 'Subscription has disabled successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Subscription can not be disabled!');
        }

        $url = $this->createUrl('monetization/subscriptions');
        $this->redirect($url);
    }

    function actionEnableSubscription() {
        if (isset($_REQUEST['id_subscription']) && !empty($_REQUEST['id_subscription'])) {
            $SubscriptionPlansModel = SubscriptionPlans::model()->findByPk($_REQUEST['id_subscription']);
            $SubscriptionPlansModel->status = 1;
            $SubscriptionPlansModel->save();
            if ($SubscriptionPlansModel->parent_id == 0) {
                    $studio_id = Yii::app()->user->studio_id;

                    $sql = "UPDATE subscription_plans SET status=1 WHERE studio_id=". $studio_id." AND parent_id=".$SubscriptionPlansModel->id;
                    $dbcon = Yii::app()->db;
                    $cc_cmd = $dbcon->createCommand($sql);
                    $cc_cmd->execute();
                }
            Yii::app()->user->setFlash('success', 'Subscription has enabled successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Subscription can not be enabled!');
        }

        $url = $this->createUrl('monetization/subscriptions');
        $this->redirect($url);
    }

    function actionmakeDefaultSubscription() {
        if (isset($_POST['id_subscription']) && !empty($_POST['id_subscription'])) {
            $studio_id = Yii::app()->user->studio_id;
            $dbcon = Yii::app()->db;
            
            $sql = "UPDATE subscription_plans SET is_default=0 WHERE studio_id=". $studio_id;
            $cc_cmd = $dbcon->createCommand($sql);
            $cc_cmd->execute();
            
            $SubscriptionPlansModel = SubscriptionPlans::model()->findByPk($_POST['id_subscription']);
            $SubscriptionPlansModel->is_default = 1;
            $SubscriptionPlansModel->save();
                
            if ($SubscriptionPlansModel->parent_id == 0) {
                $sql1 = "UPDATE subscription_plans SET is_default=1 WHERE studio_id=". $studio_id." AND parent_id=".$SubscriptionPlansModel->id;
                $cc_cmd1 = $dbcon->createCommand($sql1);
                $cc_cmd1->execute();
            }
            Yii::app()->user->setFlash('success', 'Subscription has made default successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Subscription can not be made default!');
        }

        $url = $this->createUrl('monetization/subscriptions');
        $this->redirect($url);
    }
    
    function actionSortSubscriptionPlans() {
        foreach ($_REQUEST as $key => $value) {
            $order = explode('=', $_REQUEST['order']);
            $sort = $_REQUEST['sort'];
            
            if (!empty($order) && isset($order[1]) && intval($order[1])) {
                $plan_id = (int) $order[1];
                $pos = 0;
                
                $subscriptionPlans = new SubscriptionPlans;
                $attr= array('id_sequency'=>$pos);
                $condition = "id=:id OR parent_id =:id";
                $params = array(':id'=>$plan_id);
                $subscriptionPlans = $subscriptionPlans->updateAll($attr,$condition,$params);
                
                if (!empty($sort)) {
                    foreach ($sort as $position => $item) {
                        $pos = $position + 1;
                        
                        $subscriptionPlan = new SubscriptionPlans;
                        $attr1= array('id_sequency'=>$pos);
                        $condition1 = "id=:id OR parent_id =:id";
                        $params1 = array(':id'=>$item);
                        $subscriptionPlan = $subscriptionPlan->updateAll($attr1,$condition1,$params1);
                    }
                }
            }
        }
        echo "success";
        exit;
    }

    function actionPpv() {
        $this->pageTitle = Yii::app()->name . ' | ' . ' Set PPV ';
        $this->headerinfo = 'Set PPV';
        $this->breadcrumbs = array('Monetization', 'Set PPV');

        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $data = $all_multi = $all_single = $multi_cat = $single_cat = array();
        
        $isPPV = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPV['menu']) && !empty($isPPV['menu']) && ($isPPV['menu'] & 2)) {
        
            $ppvPlans = PpvPlans::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'status' => 1, 'is_advance_purchase' => 0));

            if (isset($ppvPlans) && !empty($ppvPlans)) {
                $con = Yii::app()->db;

                foreach ($ppvPlans as $key1 => $value1) {

                    $sql = "SELECT pr.*, pr.id AS ppv_pricing_id, cu.* FROM ppv_pricing pr LEFT JOIN currency AS cu 
                    ON (pr.currency_id=cu.id) WHERE pr.ppv_plan_id=".$value1->id." AND pr.status=1 ORDER BY FIND_IN_SET(cu.id, ".$studio->default_currency_id.") DESC";
                    $ppvPricing = $con->createCommand($sql)->queryAll();

                    if (isset($value1->is_all) && intval($value1->is_all) == 1 && isset($value1->content_types_id) && ($value1->content_types_id == 1)) {
                        $all_single['plans'] = $value1;
                        $all_single['pricing'] = $ppvPricing;
                    } else if (isset($value1->is_all) && intval($value1->is_all) == 1 && isset($value1->content_types_id) && ($value1->content_types_id == 3)) {
                        $all_multi['plans'] = $value1;
                        $all_multi['pricing'] = $ppvPricing;
                    }

                    if (isset($value1->is_all) && intval($value1->is_all) == 0 && isset($value1->content_types_id) && ($value1->content_types_id == 1)) {
                        $single_cat[$value1->id]['plans'] = $value1;
                        $single_cat[$value1->id]['pricing'] = $ppvPricing;
                    } else if (isset($value1->is_all) && intval($value1->is_all) == 0 && isset($value1->content_types_id) && ($value1->content_types_id == 3)) {
                        $multi_cat[$value1->id]['plans'] = $value1;
                        $multi_cat[$value1->id]['pricing'] = $ppvPricing;
                    }
                }
            }

            $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
            $data['all_single'] = $all_single;
            $data['all_multi'] = $all_multi;
            $data['single_cat'] = $single_cat;
            $data['multi_cat'] = $multi_cat;
            $data['isPPVEnable'] = 1;
        } else {
            $data['isPPVEnable'] = 0;
        }
        //print '<pre>';print_r($data);exit;
        
        $this->render('ppv', array('studio' => $studio, 'data' => $data, 'ppv_buy' => $ppv_buy));
    }
    
    function actionDisableAllPPV() {
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'is_all' => 1, 'is_advance_purchase' => 0, 'id' => $_REQUEST['id_ppv']));
            
            if (isset($ppvPlans) && !empty($ppvPlans)) {
                $ppvPlans->status = 0;
                $ppvPlans->save();
                Yii::app()->user->setFlash('success', 'PPV has been disabled successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry, PPV can not be disabled!');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, PPV can not be disabled!');
        }

        $url = $this->createUrl('monetization/ppv');
        $this->redirect($url);
    }
    
    function actionEnableAllPPV() {
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'is_all' => 1, 'is_advance_purchase' => 0, 'id' => $_REQUEST['id_ppv']));
            
            if (isset($ppvPlans) && !empty($ppvPlans)) {
                $ppvPlans->status = 1;
                $ppvPlans->save();
                Yii::app()->user->setFlash('success', 'PPV has been enabled successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry, PPV can not be enabled!');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, PPV can not be enabled!');
        }

        $url = $this->createUrl('monetization/ppv');
        $this->redirect($url);
    }
    
    function actionIsAllPPV() {
        $is_exist = 0;
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $ppvPlans = PpvPlans::model()->findByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'is_all' => 1, 'is_advance_purchase' => 0, 'id' => $_REQUEST['id_ppv']));
            
            if (!empty($ppvPlans)) {
                $is_exist = 1;
            }
        }
        
        print $is_exist;exit;
    }
    
    function actionAddEditAllPPV() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $ppvPlans = $ppvPricing = array();
        
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $id = $_REQUEST['id_ppv'];
            $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $id, 'studio_id' => $studio_id, 'is_deleted' => 0, 'is_all' => 1, 'is_advance_purchase' => 0, 'content_types_id' => $type));
            
            if (!empty($ppvPlans)) {
                $con = Yii::app()->db;
                $sql = "SELECT pr.*, pr.id AS ppv_pricing_id, cu.*,cu.id AS currency_id FROM ppv_pricing pr LEFT JOIN currency AS cu 
                ON (pr.currency_id=cu.id) WHERE pr.ppv_plan_id=".$id." AND pr.status = 1 ORDER BY FIND_IN_SET(cu.id, ".$studio->default_currency_id.") DESC";
                $pricing = $con->createCommand($sql)->queryAll();
            }
            
            foreach ($pricing as $key => $price){
                if(intval($price['ppv_season_status'])){
                    $add_sql = "SELECT * FROM ppv_multi_season_pricing WHERE ppv_pricing_id={$price['ppv_pricing_id']} AND status = 1";
                    $additional_pricing = $con->createCommand($add_sql)->queryAll();
                    $pricing[$key]['multi_season_pricing'] = $additional_pricing;
        }
        
            }
        }
        
        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
        //$currency = Currency::model()->findAll(array('order' => 'code'));
        //$sql = "SELECT cu.* FROM currency cu,currency_supports cs, payment_gateways pg, studio_payment_gateways spg WHERE  cu.id = cs.currency_id AND cs.payment_gateway_id = pg.id AND pg.id = spg.gateway_id AND spg.status = 1 AND spg.studio_id = ".$studio_id;
        //$currency = Yii::app()->db->createCommand($sql)->queryAll();
        $currency = Yii::app()->common->getStudioCurrency();
        
        $this->render('allppv', array('type' => $type, 'ppvPlans' => $ppvPlans, 'pricing' => $pricing, 'ppv_buy' => $ppv_buy, 'currency' => $currency, 'studio' => $studio, 'policyStatus' => PolicyRules::model()->getConfigRule(), 'isPurchased'=>Yii::app()->policyrule->chkPPVSubscription($id)));
    }
    
    function actionAddEditSinglePPV() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = Yii::app()->user->id;
        
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                $con = Yii::app()->db;
                $sql_usr = "UPDATE ppv_pricing SET status=0 WHERE ppv_plan_id =" . $_REQUEST['data']['id'];
                $con->createCommand($sql_usr)->execute();
                
                $ppvModel = PpvPlans::model()->findByPk($_REQUEST['data']['id']);
                $ppvModel->last_updated_by = $user_id;
                 $ppvModel->description =$_POST['data']['description'] ;
                $ppvModel->last_updated_date = new CDbExpression("NOW()");
            } else {
                $ppvModel = New PpvPlans;
                $ppvModel->created_by = $user_id;
                $ppvModel->created_date = new CDbExpression("NOW()");
            }
            
            $isPurchased = Yii::app()->policyrule->chkPPVSubscription($_REQUEST['data']['id']);
            $ppvModel->studio_id = $studio_id;
            $ppvModel->description =$_POST['data']['description'] ;
            $ppvModel->user_id = $user_id;
            $ppvModel->content_types_id = $_REQUEST['data']['content_types_id'];
            $ppvModel->status = 1;
            $ppvModel->is_all = $_REQUEST['data']['is_all'];

            if (isset($_REQUEST['data']['title']) && trim($_REQUEST['data']['title'])) {
                $ppvModel->title = $_REQUEST['data']['title'];
                $ppvModel->tag = str_replace(" ", "_", strtolower($_REQUEST['data']['title']));
            }
            if (!empty($_POST['data']['rule']) || $this->isAllowResetPolicy == true) {
                if (!$isPurchased) {
                    $ppvModel->rules_id = !empty($_POST['data']['rule']) ? $_POST['data']['rule'] : 0;
                }
            }
            
            $ppvModel->save();
            $_REQUEST['data']['id'] = $ppvModel->id;
            
            $status = explode(',', $_REQUEST['data']['status']);
            
            foreach ($status as $key => $value) {
                $key = array_search($value, $_REQUEST['data']['currency_id']);
                
                $ppvPricingModel = New PpvPricing;
                
                if (isset($_REQUEST['data']['pricing_id'][$key]) && intval($_REQUEST['data']['pricing_id'][$key])) {
                    $ppvPricingModel = $ppvPricingModel->findByPk($_REQUEST['data']['pricing_id'][$key]);
                }

                $ppvPricingModel->ppv_plan_id = $_REQUEST['data']['id'];
                $ppvPricingModel->currency_id = $_REQUEST['data']['currency_id'][$key];
                $ppvPricingModel->price_for_unsubscribed = $_REQUEST['data']['price_for_unsubscribed'][$key];
                $ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_for_subscribed'][$key];
                $ppvPricingModel->status = 1;
                $ppvPricingModel->save();
            }
            
            Yii::app()->user->setFlash('success', 'Pricing has been updated successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in set of pricing');
        }

        $url = $this->createUrl('monetization/ppv');
        $this->redirect($url);
    }
    
    function actionAddEditMultiPPV() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = Yii::app()->user->id;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $currency_arr = $_REQUEST['data']['currency_id'];
            $_REQUEST['data']['currency_id'] = array();
            $temp_currency_arr = array();
            for($c=0;$c<count($currency_arr);$c++){
                if(!in_array($currency_arr[$c],$temp_currency_arr)){
                    array_push($temp_currency_arr,$currency_arr[$c]);
                    $_REQUEST['data']['currency_id'][] = $currency_arr[$c];
                }
            }
            $_REQUEST['data']['status'] = implode (',' , $_REQUEST['data']['currency_id']);
            if(isset($_REQUEST['data']['multi_check_subscriber']) && count($_REQUEST['data']['multi_check_subscriber'])){}else{
                array_walk($_REQUEST['data']['season_number_subscribed'],function(&$value){$value=0;});
                array_walk($_REQUEST['data']['season_multi_subscribed'],function(&$value){$value=0.00;});
            }
            if(isset($_REQUEST['data']['multi_check_unsubscriber']) && count($_REQUEST['data']['multi_check_unsubscriber'])){}else{
                array_walk($_REQUEST['data']['season_number_unsubscribed'],function(&$value){$value=0;});
                array_walk($_REQUEST['data']['season_multi_unsubscribed'],function(&$value){$value=0.00;});
            }
        
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                $con = Yii::app()->db;
                $sql_usr = "UPDATE ppv_pricing SET status=0 WHERE ppv_plan_id =" . $_REQUEST['data']['id'];
                $sql_usr .= ";UPDATE ppv_multi_season_pricing SET status=0 WHERE ppv_plan_id =" . $_REQUEST['data']['id'];
                $con->createCommand($sql_usr)->execute();
                
                $ppvModel = PpvPlans::model()->findByPk($_REQUEST['data']['id']);
                $ppvModel->last_updated_by = $user_id;
                $ppvModel->last_updated_date = new CDbExpression("NOW()");
            } else {
                $ppvModel = New PpvPlans;
                $ppvModel->created_by = $user_id;
                $ppvModel->created_date = new CDbExpression("NOW()");
            }
            
            $ppvModel->studio_id = $studio_id;
            $ppvModel->user_id = $user_id;
            $ppvModel->content_types_id = $_REQUEST['data']['content_types_id'];
            $ppvModel->status = 1;
            $ppvModel->is_all = $_REQUEST['data']['is_all'];

            if (isset($_REQUEST['data']['title']) && trim($_REQUEST['data']['title'])) {
                $ppvModel->title = $_REQUEST['data']['title'];
                $ppvModel->tag = str_replace(" ", "_", strtolower($_REQUEST['data']['title']));
            }
            $isPurchased = Yii::app()->policyrule->chkUesrSubscription($_REQUEST['data']['id']);
            if (!empty($_POST['data']['rule']) || $this->isAllowResetPolicy == true) {
                if (!$isPurchased) {
                    $ppvModel->rules_id = !empty($_POST['data']['rule']) ? $_POST['data']['rule'] : 0;
                }
            }
            $ppvModel->description = $_REQUEST['data']['description'];
            $ppvModel->save();
            $_REQUEST['data']['id'] = $ppvModel->id;
            
            $status = explode(',', $_REQUEST['data']['status']);
            
            foreach ($status as $key => $value) {
                if((isset($_REQUEST['data']['show_unsubscribed'][$key]) && $_REQUEST['data']['show_unsubscribed'][$key]) || (isset($_REQUEST['data']['season_unsubscribed'][$key]) && $_REQUEST['data']['season_unsubscribed'][$key]) || (isset($_REQUEST['data']['episode_subscribed'][$key]) && $_REQUEST['data']['episode_subscribed'][$key])){
                $key = array_search($value, $_REQUEST['data']['currency_id']);
                $ppvPricingModel = New PpvPricing;
                
                if (isset($_REQUEST['data']['pricing_id'][$key]) && intval($_REQUEST['data']['pricing_id'][$key])) {
                    $ppvPricingModel = $ppvPricingModel->findByPk($_REQUEST['data']['pricing_id'][$key]);
                }

                $ppvPricingModel->ppv_plan_id = $_REQUEST['data']['id'];
                $ppvPricingModel->currency_id = $_REQUEST['data']['currency_id'][$key];
                
                $ppvPricingModel->show_unsubscribed = $_REQUEST['data']['show_unsubscribed'][$key];
                $ppvPricingModel->season_unsubscribed = $_REQUEST['data']['season_unsubscribed'][$key];
                $ppvPricingModel->episode_unsubscribed = $_REQUEST['data']['episode_unsubscribed'][$key];
                $ppvPricingModel->show_subscribed = isset($_REQUEST['data']['show_subscribed'][$key]) && trim($_REQUEST['data']['show_subscribed'][$key]) ? $_REQUEST['data']['show_subscribed'][$key] : $_REQUEST['data']['show_unsubscribed'][$key];
                $ppvPricingModel->season_subscribed = isset($_REQUEST['data']['season_subscribed'][$key]) && trim($_REQUEST['data']['season_subscribed'][$key]) ? $_REQUEST['data']['season_subscribed'][$key] : $_REQUEST['data']['season_unsubscribed'][$key];
                $ppvPricingModel->episode_subscribed = isset($_REQUEST['data']['episode_subscribed'][$key]) && trim($_REQUEST['data']['episode_subscribed'][$key]) ? $_REQUEST['data']['episode_subscribed'][$key] : $_REQUEST['data']['episode_unsubscribed'][$key];
                
                
                /*saving multi season price*/
                
                $temp_season_un = array();
                $temp_season_s = array();
                for($i = 0; $i < count($_REQUEST['data']['season_number_unsubscribed'][$_REQUEST['data']['currency_id'][$key]]); $i++){
                    $temp_season_un[$_REQUEST['data']['season_number_unsubscribed'][$_REQUEST['data']['currency_id'][$key]][$i]] = $_REQUEST['data']['season_multi_unsubscribed'][$_REQUEST['data']['currency_id'][$key]][$i];
                }

                for($i = 0; $i < count($_REQUEST['data']['season_number_subscribed'][$_REQUEST['data']['currency_id'][$key]]); $i++){
                    $temp_season_s[$_REQUEST['data']['season_number_subscribed'][$_REQUEST['data']['currency_id'][$key]][$i]] = $_REQUEST['data']['season_multi_subscribed'][$_REQUEST['data']['currency_id'][$key]][$i];
                }
                
                $multi_uns_count = count($_REQUEST['data']['season_number_unsubscribed'][$_REQUEST['data']['currency_id'][$key]]);
                $multi_s_count = count($_REQUEST['data']['season_number_subscribed'][$_REQUEST['data']['currency_id'][$key]]);
                
                $ppv_season_status = 0;
                if($multi_uns_count || $multi_s_count){
                    $ppv_season_status = 1;
                }
                
                $ppvPricingModel->ppv_season_status = $ppv_season_status;
                $ppvPricingModel->status = 1;
                $ppvPricingModel->save();
                $ppv_pricing_id = $ppvPricingModel->id;
                for ($i=0;$i<= max(max(array_keys($temp_season_un)),max(array_keys($temp_season_s)));$i++){
                    if(array_key_exists($i,$temp_season_un) || array_key_exists($i,$temp_season_s)){
                        $ppvMultiSeasonPricingModel = new PpvMultiSeasonPricing;
                        $ppvMultiSeasonPricingModel->ppv_plan_id = $_REQUEST['data']['id'];
                        $ppvMultiSeasonPricingModel->ppv_pricing_id = $ppv_pricing_id;
                        $ppvMultiSeasonPricingModel->currency_id = $_REQUEST['data']['currency_id'][$key];
                        $ppvMultiSeasonPricingModel->season_number_unsubscribed = array_key_exists($i,$temp_season_un) ? $i : 0;
                        $season_number_subscribed = 0;
                        if(isset($_REQUEST['data']['season_subscribed'][$key]) && trim($_REQUEST['data']['season_subscribed'][$key])){
                            if(array_key_exists($i,$temp_season_s)){
                                $season_number_subscribed = $i;
            }
                        }else{
                            if(array_key_exists($i,$temp_season_un)){
                                $season_number_subscribed = $i;
                            }
                        }
                        $ppvMultiSeasonPricingModel->season_number_subscribed = $season_number_subscribed;
                        $ppvMultiSeasonPricingModel->season_unsubscribed = array_key_exists($i,$temp_season_un) ? $temp_season_un[$i] : 0;
                        $season_subscribed = 0;
                        if(isset($_REQUEST['data']['season_subscribed'][$key]) && trim($_REQUEST['data']['season_subscribed'][$key])){
                            if(array_key_exists($i,$temp_season_s)){
                                $season_subscribed = $temp_season_s[$i];
                            }
                        }else{
                            if(array_key_exists($i,$temp_season_un)){
                                $season_subscribed = $temp_season_un[$i];
                            }
                        }
                        $ppvMultiSeasonPricingModel->season_subscribed = $season_subscribed;
                        $ppvMultiSeasonPricingModel->status = 1;
                        $ppvMultiSeasonPricingModel->save();
                    }
                }
            }
            }
            Yii::app()->user->setFlash('success', 'Pricing has been updated successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in set of pricing');
        }

        $url = $this->createUrl('monetization/ppv');
        $this->redirect($url);
    }
    
    function actionAddEditCategoryPPV() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $ppvPlans = $ppvPricing = array();
        
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $id = $_REQUEST['id_ppv'];
            $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $id, 'studio_id' => $studio_id, 'is_deleted' => 0, 'is_all' => 0, 'is_advance_purchase' => 0, 'content_types_id' => $type));
            
            if (!empty($ppvPlans)) {
                $con = Yii::app()->db;
                $sql = "SELECT pr.*, pr.id AS ppv_pricing_id, cu.* FROM ppv_pricing pr LEFT JOIN currency AS cu 
                ON (pr.currency_id=cu.id) WHERE pr.ppv_plan_id=".$id." AND pr.status=1 ORDER BY FIND_IN_SET(cu.id, ".$studio->default_currency_id.") DESC";
                $pricing = $con->createCommand($sql)->queryAll();
                foreach ($pricing as $key => $price){
                    if(intval($price['ppv_season_status'])){
                        $add_sql = "SELECT * FROM ppv_multi_season_pricing WHERE ppv_pricing_id={$price['ppv_pricing_id']} AND status = 1";
                        $additional_pricing = $con->createCommand($add_sql)->queryAll();
                        $pricing[$key]['multi_season_pricing'] = $additional_pricing;
            }
        }
            }
        }
        
        $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
        //$currency = Currency::model()->findAll(array('order' => 'code'));
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('categoryppv', array('type' => $type, 'ppvPlans' => $ppvPlans, 'pricing' => $pricing, 'ppv_buy' => $ppv_buy, 'currency' => $currency, 'studio' => $studio, 'policyStatus' => PolicyRules::model()->getConfigRule(), 'isPurchased'=>Yii::app()->policyrule->chkPPVSubscription($id)));
    }
    
    
   
    
    
    
    function actionDeletePPV() {
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $user_id = Yii::app()->user->id;
            $id_ppv = $_REQUEST['id_ppv'];
            
            //Set ppv plan id to zero which had assigned category
            $con = Yii::app()->db;
            $sql_usr = "UPDATE films SET ppv_plan_id=0 WHERE studio_id=" . $studio_id . " AND ppv_plan_id =" . $id_ppv;
            $con->createCommand($sql_usr)->execute();
            
            //Delete all ppv record
            $ppvModel = PpvPlans::model()->findByPk($id_ppv);
            $ppvModel->status = 0;
            $ppvModel->is_deleted = 1;
            $ppvModel->last_updated_by = $user_id;
            $ppvModel->last_updated_date = new CDbExpression("NOW()");
            $ppvModel->save();
            
            Yii::app()->user->setFlash('success', 'PPV category has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, PPV category can not be deleted!');
        }

        $url = $this->createUrl('monetization/ppv');
        $this->redirect($url);
    }
    
    function actionSetPPVCategory() {
        $this->layout = false;

        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $film = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $_REQUEST['data']['movie_id']));
            if (isset($film) && !empty($film)) {
                $plan_id=implode(',',$_REQUEST['data']['plan_id']);
                $ids = rtrim($plan_id,',');
                $film->ppv_plan_id = $ids;
                $film->save();

                Yii::app()->user->setFlash('success', 'PPV category has been set successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Error in set of PPV ');
            }

            $url = $this->createUrl('admin/managecontent');
            $this->redirect($url);
        } else if (isset($_REQUEST['content_types_id']) && isset($_REQUEST['movie_id'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $res['content_types_id'] = $_REQUEST['content_types_id'];
            $res['movie_id'] = $_REQUEST['movie_id'];
            $ppv_categories = $film = $ppv_buy = '';

            //Check ppv is enable or not in studio
            $studio = Studio::model()->findByPk($studio_id);
            
            
            $isPPVEnabled = Yii::app()->general->monetizationMenuSetting($studio_id);
            if(isset($isPPVEnabled['menu']) && !empty($isPPVEnabled['menu']) && ($isPPVEnabled['menu'] & 2)) {
                
                if (isset($_REQUEST['content_types_id']) && intval($_REQUEST['content_types_id'])) {
                    $content_types_id = $_REQUEST['content_types_id'];
                    if (intval($_REQUEST['content_types_id']) == 1 || intval($_REQUEST['content_types_id']) == 2  || intval($_REQUEST['content_types_id']) == 4) {
                        $content_types_id = 1;
                    }
                }
                
                $ppvModel = New PpvPlans;
                $ppv_categories = $ppvModel->findAllByAttributes(array('studio_id' => $studio_id, 'content_types_id' => $content_types_id, 'is_deleted' => 0, 'is_all' => 0, 'is_advance_purchase' => 0));
                
                $pricing = array();
                
                if (isset($ppv_categories) && !empty($ppv_categories)) {
                    $con = Yii::app()->db;
                    foreach ($ppv_categories as $key => $value) {
                        $sql = "SELECT pr.*, pr.id AS ppv_pricing_id, cu.* FROM ppv_pricing pr LEFT JOIN currency AS cu 
                        ON (pr.currency_id=cu.id) WHERE pr.ppv_plan_id=".$value->id." AND pr.status=1 ORDER BY FIND_IN_SET(cu.id, ".$studio->default_currency_id.") DESC";
                        $ppvPricing = $con->createCommand($sql)->queryAll();
                        
                        if (isset($ppvPricing) && !empty($ppvPricing)) {
                            $pricing[$value->id] = $ppvPricing;
                        }
                    }
                }
                
                $film = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $_REQUEST['movie_id']));

                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
            }

            $this->render('ppvcategory', array('ppv_categories' => $ppv_categories, 'pricing' => $pricing, 'res' => $res, 'film' => $film, 'ppv_buy' => $ppv_buy));
        } else {
            Yii::app()->user->setFlash('error', "Oops! Pay per view can't be set.");
            $url = $this->createUrl('admin/managecontent');
            $this->redirect($url);
        }
    }

    function actionCoupons() {

            $page_size = 20;
            $offset = 0;
            if (isset($_REQUEST['page'])) {
                $offset = ($_REQUEST['page'] - 1) * $page_size + 1;
            }
            $this->pageTitle = Yii::app()->name . ' | ' . ' Coupons';
            $this->breadcrumbs = array('Monetization', 'Coupons');
            $this->headerinfo = 'Coupons';
            $studio_id = Yii::app()->user->studio_id;
            
			$coupon_type = 1;
                    $table_name = "coupon c";
			if (isset($_GET['type']) && (intval($_GET['type'])) == 2) {
                    $table_name = "coupon_subscription c";
				$coupon_type = 2;
                } 
            
            $coupon_sql = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),c.*')
                ->from($table_name)
                ->where("c.status=1 and c.studio_id = " . $studio_id)
                ->order('c.id DESC')
                ->limit($page_size,$offset);
            $data = $coupon_sql->queryAll();
            
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            $pages = new CPagination($item_count);
            $pages->setPageSize($page_size);
            $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
            $sample = range($pages->offset + 1, $end);
            $menu = Yii::app()->general->monetizationMenuSetting($studio_id);
            
            //$currency = (array)(Currency::model()->findAll(array('order' => 'code')));
            $currency = Yii::app()->common->getStudioCurrency();
            $this->render('coupons', array('data' => $data, 'menu'=>$menu, 'studio' => $studio, 'email' => $email, 'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample,'currency'=>$currency, 'coupon_type' => $coupon_type));
        
    }
    
    //VOUCHER START
    
    function actionVoucher() {
            $page_size = 20;
            $offset = 0;
            if (isset($_REQUEST['page'])) {
                $offset = ($_REQUEST['page'] - 1) * $page_size + 1;
            }
            $this->pageTitle = Yii::app()->name . ' | ' . ' Voucher';
            $this->breadcrumbs = array('Monetization', 'Voucher');
            $this->headerinfo = 'Voucher';
            $studio_id = Yii::app()->user->studio_id;
            $coupon_sql = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),c.*')
                ->from('voucher c')
                ->where("c.status=1 and c.studio_id = " . $studio_id)
                ->order('c.id DESC')
                ->limit($page_size,$offset);
            $data = $coupon_sql->queryAll();
            $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
            
            $free_contents = FreeContent::model()->findAllByAttributes(array('studio_id' => $studio_id));
        
            $content = array();
            if (isset($free_contents) && !empty($free_contents)) {
                $cnt = 0;
                foreach ($free_contents as $key => $value) {
                    $arg['movie_id'] = $value->movie_id;
                    $arg['season_id'] = $value->season_id;
                    $arg['episode_id'] = $value->episode_id;
                    $arg['content_types_id'] = $value->content_types_id;
                    $content[$cnt] = self::getSelectedFreeContent($arg);
                    $cnt++;
                }
            }
            
            $pages = new CPagination($item_count);
            $pages->setPageSize($page_size);
            $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
            $sample = range($pages->offset + 1, $end);
            
            $id = ''; /* Edit voucher - for future propose */
            
            $this->render('voucher', array('data' => $data,'content' => $content,'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample, 'policyStatus' => PolicyRules::model()->getConfigRule(), 'isPurchased'=>Yii::app()->policyrule->chkVoucherSubscription($id)));
    }

	function actionaddVoucher() {
        $studio_id = Yii::app()->user->studio_id;
        $db_user = Yii::app()->db;
        $contentCoup = array();
        if(isset($_REQUEST['data']) && !empty($_REQUEST['data'])){
            foreach($_REQUEST['data'] as $dataKey => $dataVal){
                $x =0;
                foreach($dataVal as $dataValKey => $dataValVal){
                    $contentCoup[$x][$dataKey] = $dataValVal;
                    $x++;
                }
            }
        }
        $isPurchased = Yii::app()->policyrule->chkVoucherSubscription();
        $rules_id = 0;
        if (!empty($_POST['data']['rule']) || $this->isAllowResetPolicy == true) {
            if (!$isPurchased) {
                $rules_id = !empty($_POST['data']['rule']) ? $_POST['data']['rule'] : 0;
            }
        }
        if (isset($_REQUEST['coupon_type']) && $_REQUEST['coupon_type'] == 0) {
            $noOfCoupon = $_REQUEST['no_of_coupon'];
            $x = 1;
            $vsql = "INSERT INTO `voucher`(`studio_id`, `rules_id`, `voucher_code`, `valid_from`, `valid_until`, `is_allcontent`, `content`, `created_date`, `created_by`, `ip`, `status`, `voucher_type`) VALUES ";
            $sql_val = array();
            $cnum = 1000;
            while ($x <= $noOfCoupon) {
                $character_set_array = array();
                $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                $temp_array = array();
                foreach ($character_set_array as $character_set) {
                    for ($i = 0; $i < $character_set['count']; $i++) {
                        $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                    }
                }
                shuffle($temp_array);
                $randomString = implode('', $temp_array);
                if ($randomString != '') {
                    $sql = "SELECT count(*) as totalCount FROM voucher  where studio_id = " . $studio_id . " and voucher_code ='" . $randomString . "'";
                    $data = $db_user->createCommand($sql)->queryAll();
                    if (isset($data[0]['totalCount']) && $data[0]['totalCount'] < 1) {
                        
                        $new_content_data = "";
                        if($_REQUEST['apply_cont']!=1){
                            $content = array();
                            foreach($contentCoup as $contentCoupKey => $contentCoupVal){
                                $content_arr = explode('_', $contentCoupVal['content']);
                                if($content_arr[0]!=0){
                                    $content_data = $content_arr[0];
                                    if($content_arr[1]!=0){
                                        $content_data = $content_arr[0].":".$content_arr[1];
                                        if($content_arr[2]!=0){
                                            $content_data = $content_arr[0].":".$content_arr[1].":".$content_arr[2];
                                        }
                                    }
                                }

                                array_push($content, $content_data);

                            }
                            $curr_data = implode(',', $content);
                            $new_content_data = ",".$curr_data.",";
                        }
                        $valid_form = date('Y-m-d', strtotime($_REQUEST['valid_from']));
                        $valid_until = date('Y-m-d', strtotime($_REQUEST['valid_until']));
                        $is_allcontent = $_REQUEST['apply_cont'];
                        $created_by = Yii::app()->user->id;
                        $created_date = gmdate('Y-m-d H:i:s');
                        $ip = Yii::app()->common->getIP();
                        if($new_content_data!=""){
                            $content_data = $new_content_data;
                        }else{
                            $content_data = NULL;
                        }
                        $val_data = "(".$studio_id.",'".$rules_id."','".$randomString."','".$valid_form."','".$valid_until."','".$is_allcontent."','".$content_data."','".$created_date."','".$created_by."','".$ip."',1,0)";
                        array_push($sql_val,$val_data);
                        
                        $x++;
                        if ($x == $noOfCoupon) {
                            Yii::app()->user->setFlash('success', 'Wow! Your Voucher has been created successfully!');
                        }
                    }
                }
            }
            $arr_split = array_chunk($sql_val,$cnum);
            for($i=0; $i < count($arr_split);$i++){
                $data_str = implode(",",$arr_split[$i]);
                $qry_str = $vsql.$data_str;
                $exe_voucher = $db_user->createCommand($qry_str)->execute();
            }
        } else if (isset($_REQUEST['coupon_type']) && $_REQUEST['coupon_type'] == 1) {
            $sql = "SELECT id FROM voucher  where studio_id = " . $studio_id . " and voucher_code ='" . $_REQUEST['coupon_code'] . "' and status=1 LIMIT 0,1";
            $data = $db_user->createCommand($sql)->queryRow();

             if (isset($data) && !empty($data)) {
                Yii::app()->user->setFlash('error', 'Voucher code already exist.');
            } else {
                $new_content_data = "";
                if($_REQUEST['apply_cont2']!=1){
                    $content = array();
                    foreach($contentCoup as $contentCoupKey => $contentCoupVal){
                        $content_arr = explode('_', $contentCoupVal['content']);
                        if($content_arr[0]!=0){
                            $content_data = $content_arr[0];
                            if($content_arr[1]!=0){
                                $content_data = $content_arr[0].":".$content_arr[1];
                                if($content_arr[2]!=0){
                                    $content_data = $content_arr[0].":".$content_arr[1].":".$content_arr[2];
                                }
                            }
                        }

                        array_push($content, $content_data);

                    }
                    $curr_data = implode(',', $content);
                    $new_content_data = ",".$curr_data.",";
                }
                $couponobj = new CouponOnly();
                $couponobj->voucher_code = $_REQUEST['coupon_code'];
                $couponobj->valid_from = date('Y-m-d', strtotime($_REQUEST['valid_from']));
                $couponobj->valid_until = date('Y-m-d', strtotime($_REQUEST['valid_until']));
                if($new_content_data!=""){
                $couponobj->content = $new_content_data;
                }
                $couponobj->is_allcontent = $_REQUEST['apply_cont2'];
                $couponobj->restrict_usage = $_REQUEST['restrict_user'];
                $couponobj->voucher_type = 1;
                $couponobj->studio_id = $studio_id;
                $couponobj->rules_id = $rules_id;
                $couponobj->created_by = Yii::app()->user->id;
                $couponobj->created_date = gmdate('Y-m-d H:i:s');
                $couponobj->ip = Yii::app()->common->getIP();
                $couponobj->save();
                Yii::app()->user->setFlash('success', 'Wow! Your Voucher added successfully!');
            }
        }
        $this->redirect($this->createUrl('monetization/voucher'));
        exit;
    }
    
     function actionDeleteAllVoucher() {
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $couponId = implode(',', $_REQUEST['data']);
			$couponobj = new CouponOnly();
			$res = $couponobj->deleteVoucher(Yii::app()->user->studio_id, $couponId);
            Yii::app()->user->setFlash('success', 'Vouchers have deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Vouchers can not be deleted!');
        }
        $this->redirect($this->createUrl('monetization/voucher'));
    }
    
    function actionVoucher_history() {
        if ($_REQUEST['cpn_code']) {
            $a = ($_REQUEST['cpn_code']);
            $studio_id = Yii::app()->common->getStudiosId();
            $couponobj = new CouponOnly();
            $getcoupon_dataobj = $couponobj->getcoupon_history($a);
            $data = $getcoupon_dataobj;
            if($data['is_allcontent']!=1){
                $f_cont = CouponOnly::model()->getContentInfo($data['content']);
                            
            }else{
                $f_cont = "All";
            }
            $valid_today = strtotime(date('Y-m-d'));
            $valid_from = strtotime(date($data['valid_from']));
            $valid_until = strtotime(date($data['valid_until']));
            $check_numberof_use = PpvSubscription::model()->checkVoucherUsed($studio_id,$data['id']);
            if((count($check_numberof_use) == 0 && ($valid_from <= $valid_today && $valid_today <= $valid_until)) || ($data['voucher_type'] == 1 && ($valid_from <= $valid_today && $valid_today <= $valid_until))){
            $valid = 'Yes';
            }else{
                    $valid = 'No';
            }
            
            if ($data['voucher_type'] == 1) {
                $coupon_type = "Multi-use";
            } else {
                $coupon_type = "Once-use";
            }
            if ($data['user_can_use'] == 1) {
                $coupon_use = "Multiple times";
            } else {
                $coupon_use = "Once";
            }
            $used_by = '-';
            if ($data['used_by'] != '0') {
                if ($data['voucher_type'] == 1) {
                    $used_by = '';
                    $userIdList = explode(",", $data['used_by']);
                    foreach ($userIdList as $userIdListKey => $userIdListVal) {
                        if ($userIdListKey == 0) {
                            $used_by .= Yii::app()->common->getuseremail($userIdListVal);
                        } else {
                            $used_by .= ", " . Yii::app()->common->getuseremail($userIdListVal);
                        }
                    }
                } else {
                    $used_by = Yii::app()->common->getuseremail($data['used_by']);
                }
            }
           
                    
            $ret = array('id' => $data['id'],'content' => $f_cont, 'restrict' => $data['restrict_usage'], 'coupon_type' => $coupon_type, 'coupon_use' => $coupon_use, 'created_date' => date('M jS,Y h:i:s', strtotime($data['created_date'])), 'valid_from' => date('M jS,Y', strtotime($data['valid_from'])), 'valid_until' => date('M jS,Y', strtotime($data['valid_until'])), 'is_valid' => $valid, 'used_by' => $used_by, 'coupon_code' => $data['voucher_code'], 'cupon_used' => $data['used_by']);
            echo json_encode($ret);
        }
    }
    
    function actionvoucherCsv_download() {
        $this->layout = false;
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
        
        $studio_id = Yii::app()->user->studio_id;
		$searchText = isset($_REQUEST['searchText']) && trim($_REQUEST['searchText']) ? $_REQUEST['searchText'] : '';
        $limit = isset($_REQUEST['limit']) && trim($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $couponobj = new CouponOnly();
        $getcouponobj = $couponobj->getcoupon_data($studio_id, $searchText,$limit,1);
		
        $dataCsv = '';
        if ($getcouponobj) {
            $headings = "Sl no" . "\t" . "Voucher" . "\t" . "Voucher Type" . "\t" . "Content" . "\t" . "Valid" . "\t" . "Used" . "\t" . "Restrict Usage per User" . "\t" . "Used Date"."\n";
            $i = 1;

            for ($k = 0; $k < count($getcouponobj); $k++) {
                $userList = '-';
                $now = strtotime(date("Y-m-d")); // or your date as well
                $cpn_date = strtotime($getcouponobj[$k]['valid_until']);
                $datediff = $cpn_date - $now;
               
                if ($getcouponobj[$k]['used_by'] != '0') {
                    if ($getcouponobj[$k]['coupon_type'] == 1) {
                        $userList = '';
                        $userIdList = explode(",", $getcouponobj[$k]['used_by']);
                        foreach ($userIdList as $userIdListKey => $userIdListVal) {
                            if ($userIdListKey == 0) {
                                $userList .= Yii::app()->common->getuseremail($userIdListVal);
                            } else {
                                $userList .= " | " . Yii::app()->common->getuseremail($userIdListVal);
                            }
                        }
                    } else {
                        $userList = Yii::app()->common->getuseremail($getcouponobj[$k]['used_by']);
                    }
                }
                if($getcouponobj[$k]['is_allcontent']!=1){
                            $content_str = CouponOnly::model()->getContentInfo($getcouponobj[$k]['content']);
                            $cont_str = $content_str;
                        }else{
                            $cont_str = "All";
                        }
                $dataCsv .= $i . "\t" . $getcouponobj[$k]['voucher_code'] . "\t" . (($getcouponobj[$k]['voucher_type'] == 1) ? 'Multi-use' : 'Once-use') . "\t" . $cont_str . "\t" . (($getcouponobj[$k]['used_by'] == 0 && $datediff >= 0) || ($getcouponobj[$k]['voucher_type'] == 1 && $datediff >= 0) ? 'Yes' : 'No') . "\t" . (($getcouponobj[$k]['used_by'] == 0) ? '-' : 'Yes') . "\t" . $getcouponobj[$k]['restrict_usage'] . "\t" . (($getcouponobj[$k]['cused_date'] == 0) ? '-' : $getcouponobj[$k]['cused_date'])."\n";

                $i++;
            }
        }
        $fileName = Voucher . "_" . date('m-d-Y') . ".xls";
        ob_clean();
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header("Content-disposition: attachment; filename=" . $fileName);
        header("Content-Transfer-Encoding: binary");
        echo $headings . $dataCsv;
        exit;
    }
    
    function actionshowVoucherSearch(){
        $studio_id = Yii::app()->user->studio_id;
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        $search_text = (isset($_REQUEST['search_text']) && trim($_REQUEST['search_text'])) ? $_REQUEST['search_text'] : '';
        $views = '';
        $content_data= CouponOnly::model()->getCouponSearchData($studio_id,$search_text);
            $this->renderPartial('couponOnlySearchData',array('data'=>$content_data));
    }
    
    //COUPONS ONLY END
    
    function actionajaxForCouponCurrency(){
        $this->layout = false;
        $studio = $this->studio;
       // $currency = Currency::model()->findAll(array('order' => 'code'));
        $discountType = $_REQUEST['discount_type'];
        $couponType = $_REQUEST['coupon_type'];
        //$currency = (array)(Currency::model()->findAll(array('order' => 'code')));
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('coupon_currency', array('currency'=>$currency,'studio' => $studio,'discountType' => $discountType,'couponType' => $couponType));
    }
    
    function actionaddCoupons() {
        $studio_id = Yii::app()->common->getStudiosId();
        $db_user = Yii::app()->db;
        $currencCoup = array();
        
        if(isset($_REQUEST['data']) && !empty($_REQUEST['data'])){
            foreach($_REQUEST['data'] as $dataKey => $dataVal){
                $x =0;
                foreach($dataVal as $dataValKey => $dataValVal){
                    $currencCoup[$x][$dataKey] = $dataValVal;
                    $x++;
                }
            }
        }
        if (isset($_REQUEST['coupon_type']) && $_REQUEST['coupon_type'] == 0) {
            $table = "coupon";
            if($_REQUEST['payment_type_singleuse']=='recurring'){
                $table = "coupon_subscription";
            }
            $noOfCoupon = $_REQUEST['no_of_coupon'];
            $x = 1;
            while ($x <= $noOfCoupon) {
                $character_set_array = array();
                $character_set_array[] = array('count' => 5, 'characters' => 'CDEFGHIJKLMNOPQRSTUVWXYZ');
                $character_set_array[] = array('count' => 3, 'characters' => '0123456789');
                $temp_array = array();
                foreach ($character_set_array as $character_set) {
                    for ($i = 0; $i < $character_set['count']; $i++) {
                        $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
                    }
                }
                shuffle($temp_array);
                $randomString = implode('', $temp_array);
                if ($randomString != '') {
                    $sql = "SELECT count(*) as totalCount FROM ".$table." where studio_id = " . $studio_id . " and coupon_code ='" . $randomString . "'";
                    $data = $db_user->createCommand($sql)->queryAll();
                    if (isset($data[0]['totalCount']) && $data[0]['totalCount'] < 1) {
                        
                        if($_REQUEST['payment_type_singleuse']=='recurring'){
                            $couponobj = new CouponSubscription();
                            $couponobj->extend_free_trail = $_POST['free_trail_onceuse'];
                            $couponobj->discount_multiple_cycle = $_POST['discount_cycle_onceuse'];
                        }else{
                        $couponobj = new Coupon();
                        }
                        $couponobj->coupon_code = $randomString;
                        $couponobj->discount_type = $_REQUEST['amount_cash'];
                        if($_REQUEST['amount_cash']==1){
                            $couponobj->discount_amount = $_REQUEST['amount_val'];
                        }else{
                            $couponobj->discount_amount = 0.00;
                        }
                        $couponobj->valid_from = date('Y-m-d', strtotime($_REQUEST['valid_from']));
                        $couponobj->valid_until = date('Y-m-d', strtotime($_REQUEST['valid_until']));
                        $cont_category = 0;
                        $is_all = 1;
                        if($_REQUEST['singleuse_apply_cont']==0){
                            $is_all = 0;
                            if($_REQUEST['singleuse_apply_cont_category']==1){
                                $cont_category = 1;
                                $content = array();
                                if(isset($currencCoup[0]['content_digital']) && $currencCoup[0]['content_digital'] != ""){
                                    foreach($currencCoup as $contentCoupKey => $contentCoupVal){
                                        if($contentCoupVal['content_digital'] != ""){
                                            $content_arr = explode('_', $contentCoupVal['content_digital']);
                                            if($content_arr[0]!=0){
                                                $content_data = $content_arr[0];
                                                if($content_arr[1]!=0){
                                                    $content_data = $content_arr[0].":".$content_arr[1];
                                                    if($content_arr[2]!=0){
                                                        $content_data = $content_arr[0].":".$content_arr[1].":".$content_arr[2];
                                                    }
                                                }
                                            }
                                            array_push($content, $content_data);
                                        }
                                    }
                                    $curr_data = implode(',', $content);
                                    $new_content_data = ",".$curr_data.",";
                                }else{
                                    $cont_category = 0;
                                    $is_all = 1;
                                }
                            }
                            
                            if($_REQUEST['singleuse_apply_cont_category']==0){
                                $cont_category = 2;
                                $content = array();
                                if(isset($currencCoup[0]['content_physical']) && $currencCoup[0]['content_physical'] != ""){
                                    foreach($currencCoup as $contentCoupKey => $contentCoupVal){
                                        if($contentCoupVal['content_physical'] != ""){
                                            $content_data = $contentCoupVal['content_physical'];
                                            array_push($content, $content_data);
                                        }
                                    }
                                    $curr_data = implode(',', $content);
                                    $new_content_data = ",".$curr_data.",";
                                }else{
                                    $cont_category = 0;
                                    $is_all = 1;
                                }
                            }
                        }
                        if($_REQUEST['payment_type_singleuse']=='onetime'){
                        if($new_content_data!=""){
                            $couponobj->specific_content = $new_content_data;
                        }
                        $couponobj->is_all = $is_all;
                        $couponobj->content_category = $cont_category;
                        }
                        $couponobj->used_by = 0;
                        $couponobj->studio_id = $studio_id;
                        $couponobj->created_by = Yii::app()->user->id;
                        $couponobj->created_date = gmdate('Y-m-d H:i:s');
                        $couponobj->ip = Yii::app()->common->getIP();
                        $couponobj->save();
                        if($_REQUEST['amount_cash']==0){
                            foreach($currencCoup as $currencCoupKey => $currencCoupVal){

                                        if(isset($currencCoupVal['currency_id']) && isset($currencCoupVal['amount_value'])){
                                        $couponId = $couponobj->id;
                                    if($_REQUEST['payment_type_singleuse']=='recurring'){
                                        $couponCurrency = new CouponCurrencySubscription();
                                    }else{
                                        $couponCurrency = new CouponCurrency();
                                    }
                                        $couponCurrency->coupon_id = $couponId;
                                        $couponCurrency->currency_id = $currencCoupVal['currency_id'];
                                        $couponCurrency->discount_amount = $currencCoupVal['amount_value'];
                                        $couponCurrency->save();
                                    }
                            }
                        }
                        $x++;
                        if ($x == $noOfCoupon) {
                            Yii::app()->user->setFlash('success', 'Wow! Your Coupon has been created successfully!');
                        }
                    }
                }
            }
        } else if (isset($_REQUEST['coupon_type']) && $_REQUEST['coupon_type'] == 1) {
            $table = "coupon";
            if($_REQUEST['payment_type_multiuse']=='recurring'){
                $table = "coupon_subscription";
            }
            $sql = "SELECT count(*) as totalCount FROM ".$table." where studio_id = " . $studio_id . " and coupon_code ='" . $_REQUEST['coupon_code'] . "' and status=1";
            $data = $db_user->createCommand($sql)->queryAll();

            if (isset($data[0]['totalCount']) && $data[0]['totalCount'] > 0) {
                Yii::app()->user->setFlash('error', 'Coupon code already exist.');
            } else {
                if($_REQUEST['payment_type_multiuse']=='recurring'){
                    $couponobj = new CouponSubscription();
                    $couponobj->extend_free_trail = $_POST['free_trail_multiuse'];
                    $couponobj->discount_multiple_cycle = $_POST['discount_cycle_multiuse'];
                }else{
                $couponobj = new Coupon();
                }
                $couponobj->coupon_code = $_REQUEST['coupon_code'];
                $couponobj->discount_type = $_REQUEST['amount_cash'];
                if($_REQUEST['amount_cash']==1){
                    $couponobj->discount_amount = $_REQUEST['amount_val'];
                }else{
                    $couponobj->discount_amount = 0.00;
                }
                $couponobj->valid_from = date('Y-m-d', strtotime($_REQUEST['valid_from']));
                $couponobj->valid_until = date('Y-m-d', strtotime($_REQUEST['valid_until']));
                $couponobj->restrict_usage = $_REQUEST['restrict_user'];
                $couponobj->coupon_type = 1;
                $couponobj->user_can_use = 1;//$_REQUEST['user_can_use'];
                
                $cont_category = 0;
                $is_all = 1;
                if($_REQUEST['multiuse_apply_cont']==0){
                    $is_all = 0;
                    if($_REQUEST['multiuse_apply_cont_category']==1){
                        $cont_category = 1;
                        $content = array();
                        if(isset($currencCoup[0]['content_digital']) && $currencCoup[0]['content_digital'] != ""){
                            foreach($currencCoup as $contentCoupKey => $contentCoupVal){
                                if($contentCoupVal['content_digital'] != ""){
                                    $content_arr = explode('_', $contentCoupVal['content_digital']);
                                    if($content_arr[0]!=0){
                                        $content_data = $content_arr[0];
                                        if($content_arr[1]!=0){
                                            $content_data = $content_arr[0].":".$content_arr[1];
                                            if($content_arr[2]!=0){
                                                $content_data = $content_arr[0].":".$content_arr[1].":".$content_arr[2];
                                            }
                                        }
                                    }
                                    array_push($content, $content_data);
                                }
                            }
                            $curr_data = implode(',', $content);
                            $new_content_data = ",".$curr_data.",";
                        }else{
                            $cont_category = 0;
                            $is_all = 1;
                        }
                    }

                    if($_REQUEST['multiuse_apply_cont_category']==0){
                        $cont_category = 2;
                        $content = array();
                        if(isset($currencCoup[0]['content_physical']) && $currencCoup[0]['content_physical'] != ""){
                            foreach($currencCoup as $contentCoupKey => $contentCoupVal){
                                if($contentCoupVal['content_physical'] != ""){
                                    $content_data = $contentCoupVal['content_physical'];
                                    array_push($content, $content_data);
                                }
                            }
                            $curr_data = implode(',', $content);
                            $new_content_data = ",".$curr_data.",";
                        }else{
                            $cont_category = 0;
                            $is_all = 1;
                        }
                    }
                }
                if($_REQUEST['payment_type_multiuse']=='onetime'){
                if($new_content_data!=""){
                    $couponobj->specific_content = $new_content_data;
                }
                $couponobj->is_all = $is_all;
                $couponobj->content_category = $cont_category;
                }
                $couponobj->used_by = 0;
                $couponobj->studio_id = $studio_id;
                $couponobj->created_by = Yii::app()->user->id;
                $couponobj->created_date = gmdate('Y-m-d H:i:s');
                $couponobj->ip = Yii::app()->common->getIP();
                $couponobj->save();
                if($_REQUEST['amount_cash']==0){
                    foreach($currencCoup as $currencCoupKey => $currencCoupVal){
                        if(isset($currencCoupVal['currency_id']) && isset($currencCoupVal['amount_value'])){
                        $couponId = $couponobj->id;
                        if($_REQUEST['payment_type_multiuse']=='recurring'){
                            $couponCurrency = new CouponCurrencySubscription();
                        }else{
                        $couponCurrency = new CouponCurrency();
                        }
                        $couponCurrency->coupon_id = $couponId;
                        $couponCurrency->currency_id = $currencCoupVal['currency_id'];
                        $couponCurrency->discount_amount = $currencCoupVal['amount_value'];
                        $couponCurrency->save();
                        }
                    }
                }
                Yii::app()->user->setFlash('success', 'Wow! Your Coupon added successfully!');
            }
        }
        if($_REQUEST['payment_type_multiuse']=='recurring' || $_REQUEST['payment_type_singleuse']=='recurring'){
            $this->redirect($this->createUrl('monetization/coupons/type/2'));
        exit;
        }else{
            $this->redirect($this->createUrl('monetization/coupons/type/1'));
            exit;
    }
    
    }
    
    function actionshowCouponSearch(){
        $studio_id = Yii::app()->user->studio_id;
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        $search_text = (isset($_REQUEST['search_text']) && trim($_REQUEST['search_text'])) ? $_REQUEST['search_text'] : '';
        $views = '';
		if (isset($_GET['type']) && intval($_GET['type']) == 2) {
			$content_data = CouponSubscription::model()->getCouponSearchData($studio_id, $search_text);
            } else {
			$content_data = Coupon::model()->getCouponSearchData($studio_id, $search_text);
            }
        
            $this->renderPartial('couponSeacrhData',array('data'=>$content_data));
        
        
    }
    
    function actionshowUserlist() {
        $userList = '';
        if ($_REQUEST['id'] != '') {
            if($_POST['type'] == 2){
                $couponData = CouponSubscription::model()->findByPk($_REQUEST['id']);
            }else{
            $couponData = Coupon::model()->findByPk($_REQUEST['id']);
            }
            $userKey = explode(",", $couponData['used_by']);
            $usrk = '';
            foreach ($userKey as $userKeyKey => $userKeyVal) {
                if ($userKeyKey == 0) {
                    $usrk[] = $userKeyVal;
                } else {
                    $usrk[] = $userKeyVal;
                }
            }
            $userIdList = array_count_values($usrk);
            if ($couponData['user_can_use'] == 1) {
                foreach ($userIdList as $userIdListKey => $userIdListVal) {
                    $userList .= Yii::app()->common->getuseremail($userIdListKey) . " (No of times used - " . $userIdListVal . ")<br/>";
                }
            } else {
                foreach ($userIdList as $userIdListKey => $userIdListVal) {
                    $userList .= Yii::app()->common->getuseremail($userIdListKey) . "<br/>";
                }
            }
        }
        echo $userList;
    }

    function actioncouponcsv_download() {
        $this->layout = false;
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
        
        $limit = isset($_REQUEST['limit']) && trim($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_GET['type']) && intval($_GET['type']) ==2) {
                $couponobj = new CouponSubscription();
                $getcouponobj = $couponobj->getcoupon_data($studio_id);
			$k = 0;
                $title_addon = "\t" . "Discount Cycle" . "\t" . "Extend free trail";
			$data_addon = "\t" . $getcouponobj[$k]['discount_multiple_cycle'] . "\t" . $getcouponobj[$k]['extend_free_trail'];
        } else {
            $title_addon = "";
            $data_addon = "";
            $couponobj = new Coupon();
            $getcouponobj = $couponobj->getcoupon_data($studio_id,$limit,1);
        }
		
        //$Coupon = Coupon::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
        $dataCsv = '';
        if ($getcouponobj) {
            $headings = "Sl no" . "\t" . "Coupon" . "\t" . "Coupon Type" . "\t" . "Used by a single user" . "\t" . "Valid" . "\t" . "Used" . "\t" . "User" .$title_addon. "\t" . "Used Date". "\t" . "Content Category". "\t" . "Content"."\n";
            $i = 1;

            for ($k = 0; $k < count($getcouponobj); $k++) {
                $userList = '-';
                if ($getcouponobj[$k]['used_by'] != '0') {
                    if ($getcouponobj[$k]['coupon_type'] == 1) {
                        $userList = '';
                        $userIdList = explode(",", $getcouponobj[$k]['used_by']);
                        foreach ($userIdList as $userIdListKey => $userIdListVal) {
                            if ($userIdListKey == 0) {
                                $userList .= Yii::app()->common->getuseremail($userIdListVal);
                            } else {
                                $userList .= " | " . Yii::app()->common->getuseremail($userIdListVal);
                            }
                        }
                    } else {
                        $userList = Yii::app()->common->getuseremail($getcouponobj[$k]['used_by']);
                    }
                }

                if($getcouponobj[$k]['is_all']!=1){
                    if($getcouponobj[$k]['content_category']==1){
                        $cont_cat = "Digital";
                        $content_str = Coupon::model()->getContentInfo($getcouponobj[$k]['specific_content']);
                        $cont_str = $content_str;
                    }else if($getcouponobj[$k]['content_category']==2){
                        $cont_cat = "Physical";
                        $content_str = Coupon::model()->getContentInfoPhysical($getcouponobj[$k]['specific_content']);
                        $cont_str = $content_str;
                    }else{
                        $cont_cat = "All";
                        $cont_str = "All";
                    }
                }else{
                    $cont_cat = "All";
                    $cont_str = "All";
                }

                $dataCsv .= $i . "\t" . $getcouponobj[$k]['coupon_code'] . "\t" . (($getcouponobj[$k]['coupon_type'] == 1) ? 'Multi-use' : 'Once-use') . "\t" . (($getcouponobj[$k]['user_can_use'] == 1) ? 'Multiple times' : 'Once') . "\t" . (($getcouponobj[$k]['used_by'] == 0) ? 'Yes' : 'No') . "\t" . (($getcouponobj[$k]['used_by'] == 0) ? '-' : 'Yes') . "\t" . $userList .$data_addon. "\t" . (($getcouponobj[$k]['cused_date'] == 0) ? '-' : $getcouponobj[$k]['cused_date'])."\t" .$cont_cat ."\t".$cont_str."\n";

                $i++;
            }
        }
        $fileName = Coupon . "_" . date('m-d-Y') . ".xls";
        ob_clean();
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header("Content-disposition: attachment; filename=" . $fileName);
        header("Content-Transfer-Encoding: binary");
        echo $headings . $dataCsv;
        exit;
    }

    function actionCoupon_history() {
        if ($_REQUEST['cpn_code']) {
            $a = ($_REQUEST['cpn_code']);
			$studio_id = Yii::app()->user->studio_id;

			if (isset($_GET['type']) && intval($_GET['type'])==2) {
                    $table_coupon = "coupon_subscription";
                    $table_currency = "coupon_currency_subscription";
                    $couponobj = new CouponSubscription();
                    $getcoupon_dataobj = $couponobj->getcoupon_history($a);
            } else {
                $table_coupon = "coupon";
                $table_currency = "coupon_currency";
                $couponobj = new Coupon();
				$getcoupon_dataobj = $couponobj->getcoupon_history($a);
            }
		$data = $getcoupon_dataobj;	
            $valid_today = strtotime(gmdate('Y-m-d'));
            $valid_from = strtotime(date($data['valid_from']));
            $valid_until = strtotime(date($data['valid_until']));
            if (($data['used_by'] == 0 && ($valid_from <= $valid_today && $valid_today <= $valid_until)) || ($data['coupon_type'] == 1 && ($valid_from <= $valid_today && $valid_today <= $valid_until))) {
                $valid = 'Yes';
            } else {
                $valid = 'No';
            }
            $restrict_use = $data['restrict_usage'];
            if ($data['coupon_type'] == 1) {
                $coupon_type = "Multi-use";
            } else {
                $coupon_type = "Once-use";
            }
            
            if ($data['content_category'] == 2) {
                $coupon_category = "Physical";
            } else if($data['content_category'] == 1) {
                $coupon_category = "Digital";
            } else {
                $coupon_category = "All";
            }
            
            if ($data['user_can_use'] == 1) {
                $coupon_use = "Multiple times";
            } else {
                $coupon_use = "Once";
            }
            
            if ($data['extend_free_trail'] > 0) {
                $extend_free_trail = $data['extend_free_trail']."Day(s)";
            } else {
                $extend_free_trail = 0;
            }
            
            if ($data['discount_multiple_cycle'] > 0) {
                $discount_multiple_cycle = $data['discount_multiple_cycle']."Cycle(s)";
            } else {
                $discount_multiple_cycle = 0;
            }
            $used_by = '-';
            if ($data['used_by'] != '0') {
                if ($data['coupon_type'] == 1) {
                    $used_by = '';
                    $userIdList = explode(",", $data['used_by']);
                    foreach ($userIdList as $userIdListKey => $userIdListVal) {
                        if ($userIdListKey == 0) {
                            $used_by .= Yii::app()->common->getuseremail($userIdListVal);
                        } else {
                            $used_by .= ", " . Yii::app()->common->getuseremail($userIdListVal);
                        }
                    }
                } else {
                    $used_by = Yii::app()->common->getuseremail($data['used_by']);
                }
            }
           if($data['discount_amount'] == '0.00'){
                $couponCurrency = Yii::app()->db->createCommand()
                ->select('cu.code as currencyCode,cc.discount_amount as discAmt')
                ->from($table_currency.' cc, '.$table_coupon.' co , currency cu')
                ->where('cc.coupon_id=co.id AND cc.currency_id = cu.id AND cc.coupon_id='.$data['id'])
                ->queryAll();
                if($couponCurrency){
                    if($data['amount_cash'] == 0){
                        $discTyp = 'Amount';
                    } else{
                        $discTyp = '%';
                    }
                    $amountTable = '<table class="table table-hover" ><tr><th>Currency Code</th><th>'.$discTyp.'</th></tr>';
                    foreach($couponCurrency as $couponCurrencyKey => $couponCurrencyVal){
                        $amountTable .= '<tr><td>'.$couponCurrencyVal['currencyCode'].'</td><td>'.$couponCurrencyVal['discAmt'].'</td></tr>';
                    }
                    $amountTable .='</table>';
                    $data['discount_amount'] = $amountTable;
                }
            }
            $f_cont = "";
            if($table_coupon == "coupon"){
                if($data['is_all']==0){
                    if($data['content_category']==1){
                        $f_cont = Coupon::model()->getContentInfo($data['specific_content']);
                    }
                    
                    if($data['content_category']==2){
                        $f_cont = Coupon::model()->getContentInfoPhysical($data['specific_content']);
                    }
                }else{
                    $f_cont = "All";
                }
            }
                    
            $ret = array('id' => $data['id'], 'extend_free_trail' => $extend_free_trail, 'discount_multiple_cycle' => $discount_multiple_cycle, 'coupon_type' => $coupon_type, 'coupon_category' => $coupon_category, 'coupon_use' => $coupon_use, 'created_date' => date('M jS,Y h:i:s', strtotime($data['created_date'])), 'discount_type' => $data['discount_type'], 'discount_amount' => $data['discount_amount'],'valid_from' => date('M jS,Y', strtotime($data['valid_from'])), 'valid_until' => date('M jS,Y', strtotime($data['valid_until'])), 'is_valid' => $valid, 'used_date' => date('M jS,Y', strtotime($data['used_date'])), 'used_by' => $used_by, 'coupon_code' => $data['coupon_code'], 'cupon_used' => $data['used_by'], 'res_use' => $restrict_use, 'f_cont'=>$f_cont);
            echo json_encode($ret);
        }
    }

    function actionDeleteAllCoupon() {
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            $checkedData = $_REQUEST['data'];
                $type = 1;
			if (isset($_GET['type']) && intval($_GET['type'])==2) {
				$type = 2;
            }
            $couponId = implode(',', $checkedData);
			$res = $this->actionDeleteCoupon($_REQUEST['studio_id'], $couponId, $type);
            Yii::app()->user->setFlash('success', 'Coupons have deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Coupons can not be deleted!');
        }
        $this->redirect($this->createUrl('monetization/coupons'));
    }

    function actionDeleteCoupon($studio_id = NULL, $id = NULL, $type) {
        if (isset($id)) {
            if (isset($type) && intval($type) == 2) {
                    $url = 'monetization/coupons/type/2';
                    $couponobj = new CouponSubscription();
            } else {
				$url = 'monetization/coupons/type/1';
                $couponobj = new Coupon();
            }
			
            $delete_couponobj = $couponobj->delete_coupon($studio_id, $id);
            Yii::app()->user->setFlash('success', 'Coupon deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Coupon can not be deleted!');
        }
        $this->redirect($this->createUrl($url));
        exit;
    }

    function actionPayment() {
        $this->breadcrumbs = array('Monetization', 'Payment Gateway');
        $this->headerinfo = 'Payment Gateway';
        $this->pageTitle = "Muvi | Payment Gateway";

        $studio = $this->studio;
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_POST['data']) && !empty($_POST['data'])) {
            $data = $_POST['data'];
            $ip_address = Yii::app()->request->getUserHostAddress(); 
           /* $target_action = 'monetization_payment';
            $submitted_params = serialize($_REQUEST);
            $session_params = serialize($_SESSION);
            $cookie_params = serialize($_COOKIE);
            $s3sql = "INSERT INTO chkrequest ";
            $s3sql.= "(ip_address, target_action, submitted_params, session_params, cookie_params)";
            $s3sql.= " VALUES ('" . $ip_address . "', '" . $target_action . "', '" . $submitted_params . "', '" . $session_params . "', '" . $cookie_params . "')";
            Yii::app()->db->createCommand($s3sql)->execute(); */
            
            if(isset($data['short_code']) && trim($data['short_code']) == 'stripe'){
                $logfile = dirname(__FILE__).'/stripe.txt';
                $msg .= "\n--------------------------Success : Payment gateway integrated---------------------------------------\n";
                $data_log = serialize($data);
                $msg.= $data_log."\n";
                file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
            }
            
            $StudioPaymentGatewaysModel = New StudioPaymentGateways;
            $StudioPaymentGatewaysModel->studio_id = $studio_id;
            $StudioPaymentGatewaysModel->gateway_id = $data['gateway_id'];
            $StudioPaymentGatewaysModel->short_code = (isset($data['payment_type']) && trim($data['payment_type']) == 'manual') ? 'manual' : trim($data['short_code']);
            $StudioPaymentGatewaysModel->api_username = (isset($data['api_username']) && trim($data['api_username'])) ? trim($data['api_username']) : '';
            $StudioPaymentGatewaysModel->api_password = (isset($data['api_password']) && trim($data['api_password'])) ? trim($data['api_password']) : '';
            $StudioPaymentGatewaysModel->api_signature = (isset($data['api_signature']) && trim($data['api_signature'])) ? trim($data['api_signature']) : '';
            $StudioPaymentGatewaysModel->non_3d_secure = (isset($data['non_3d_secure']) && trim($data['non_3d_secure'])) ? trim($data['non_3d_secure']) : '';
            $StudioPaymentGatewaysModel->api_mode = (isset($data['api_mode']) && trim($data['api_mode'])) ? trim($data['api_mode']) : 'live';
            $StudioPaymentGatewaysModel->status = 1;
            $StudioPaymentGatewaysModel->is_primary = 1;
            $StudioPaymentGatewaysModel->is_pci_compliance = (isset($data['is_pci_compliance']) && intval($data['is_pci_compliance'])) ? intval($data['is_pci_compliance']) : 0;
            $StudioPaymentGatewaysModel->api_addons = isset($data['api_addons']) && trim($data['api_addons']) ? $data['api_addons'] : '';
            $StudioPaymentGatewaysModel->created_by = Yii::app()->user->id;
            $StudioPaymentGatewaysModel->created_date = new CDbExpression("NOW()");
            $StudioPaymentGatewaysModel->ip = $ip_address;
            $StudioPaymentGatewaysModel->save();
            $res=MonetizationMenuSettings::model()->updateMenuSettings($studio_id,0);
            $url = $this->createUrl('monetization/payment');
            $this->redirect($url);
        } else {
            $gateway_data = array();
            $this->setPaymentGatwayVariable($gateway_data);
            
            $sql = "SELECT spg.*, s.id AS studio_id, s.name, s.status, s.is_subscribed, 
            s.is_default FROM studios s LEFT JOIN studio_payment_gateways spg 
            ON (spg.studio_id = s.id AND spg.status=1) WHERE s.id=" . $studio_id . " ORDER BY spg.is_primary DESC";
            $dbcon = Yii::app()->db;
            $data = $dbcon->createCommand($sql)->queryAll();
            if(isset($data) && trim($data[0]['short_code'])){
                    $payment_gateways = PaymentGateways::model()->find(array('condition' => 'is_payment_gateway = 1 AND short_code != "testgateway" AND short_code = "'.$data[0]['short_code'].'"'));
                $this->render('gatewaysuccess', array('data' => $data, 'payment_gateways' => $payment_gateways, 'studio' => $studio));
            }else{
                    $sql = "SELECT GROUP_CONCAT(user_id) AS user_id FROM `transactions` WHERE `transactions`.`studio_id` IN ({$studio_id}) AND `transactions`.`payment_method` IN ('testgateway')";
                    $dbcon = Yii::app()->db;
                    $is_test_transactions =$dbcon->createCommand($sql)->queryAll();  
                 $is_tested_data= array();
                 $is_tested_data=$is_test_transactions[0]['user_id'];
                  
                   if(isset($is_tested_data) && !empty($is_tested_data)){
                        $is_test_transactions=1;
                    }else{
                        $is_test_transactions=0; 
                    }
                    $payment_gateways = PaymentGateways::model()->findAll(array('condition' => 'is_payment_gateway = 1 AND short_code != "testgateway"', 'order' => 'name'));
                    $this->render('payment', array('data' => $data, 'payment_gateways' => $payment_gateways, 'studio' => $studio,'is_test_transactions'=>$is_test_transactions));
            }
        }
    }

    function actionPaymentGateways() {
        $this->layout = false;
        $studio = $this->studio;
        
        if(isset($_POST['short_code']) && trim($_POST['short_code'])){
            $view = $_POST['short_code'];
            $this->render($view, array('studio' => $studio));
        }else{
            echo 0;
        }
    }

    function actionValidatePaymentGatewayCredentials() {
        $data = $_POST['data'];
        $API_USER = $data['api_username'];
        $API_PASSWORD = $data['api_password'];
        $IS_LIVE_MODE = $data['api_mode'];

        $payment_gateway_controller = 'Api' . $data['short_code'] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $res = $payment_gateway::validatePaymentGatewayCredentials($API_USER, $API_PASSWORD, $IS_LIVE_MODE);
        echo json_encode($res);exit;
    }
    
    function actionSetGatewayCredentials(){
        if (isset($_POST['data']) && !empty($_POST['data'])) {
            $data[0] = (object)$_POST['data'];
            $this->setPaymentGatwayVariable($data);
            echo 1;
        } else {
            echo 0;
        }
    }
    function actionAddEditRestictions(){        
        if(isset($_REQUEST['category_name']) && $_REQUEST['category_name']){
            $geoblock_category = $_REQUEST['category_name'];
            $selectvalue = $_REQUEST['selectvalue'];
            $category_id = $_REQUEST['category_id'];
        }else{
            $geoblock_category = "";
            $selectvalue='';
        }
        $this->renderPartial('geoblock_category', array('studio' => $this->studio->id,"category_name"=>$geoblock_category,'cnt'=>$_REQUEST['cnt'],"selectvalue"=>$selectvalue,"category_id"=>$category_id));
    }
    function actionAddGeoCategory(){
        parse_str($_REQUEST['selectvalue'],$selectvalue);        
        $countries = Countries::model()->findAll();
        $category_id = $_REQUEST['category_id'];
        $restricted_countries = isset($selectvalue)?$selectvalue['selected_countries_content'][$_REQUEST['cnt']]:array();
        $this->renderPartial('geoblock_country', array('studio' => $this->studio->id,"countries"=>$countries,"restricted_countries"=>$restricted_countries,"category_name"=>$_REQUEST['category_name'],"cnt"=>$_REQUEST['cnt'],'categoryid'=>$category_id));        
    }
    function actionDeleteRestictions(){
        GeoBlockCategory::model()->deleteAll("id = '" . $_REQUEST['delid'] . "'");
        StudioContentRestriction::model()->deleteAll("category_id = '" . $_REQUEST['delid'] . "'");
        GeoblockContent::model()->deleteAll("geocategory_id = '" . $_REQUEST['delid'] . "'");
        echo 1;
        exit;
    }
    function actionOpenGeoCategory(){
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $this->studio->id));
        $cat_array = CHtml::listData($grestcat, 'id', 'category_name');
        $gc = GeoblockContent::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id'],'movie_stream_id'=>$_REQUEST['movie_stream_id']));
        $categoryid = !empty($gc)?$gc['geocategory_id']:"";
        $this->renderPartial('geocategory', array('studio' => $this->studio->id,'cat_array'=>$cat_array,'movie_id'=>$_REQUEST['movie_id'],'movie_stream_id'=>$_REQUEST['movie_stream_id'],'categoryid'=>$categoryid));
    }
    function actionAddGeoBlockToContent(){
        $gc = GeoblockContent::model()->findByAttributes(array('movie_id' => $_REQUEST['gmovie_id'],'movie_stream_id'=>$_REQUEST['gmovie_stream_id']));
        if(!empty($gc) && !empty($_REQUEST['category_name'])){
            $gc->geocategory_id = $_REQUEST['category_name'];
            $gc->update();
        }elseif(!empty($gc) && empty($_REQUEST['category_name'])){
            $gc->delete();
        }elseif(empty($gc) && !empty($_REQUEST['category_name'])){
            $geo = New GeoblockContent;
            $geo->movie_id = $_REQUEST['gmovie_id'];
            $geo->movie_stream_id = $_REQUEST['gmovie_stream_id'];
            $geo->geocategory_id = $_REQUEST['category_name'];
            $geo->save();
        }elseif(empty($gc) && empty($_REQUEST['category_name'])){
        }
        echo 1;
        exit;
    }
    function actionRestrictions() {

            $this->breadcrumbs = array('Monetization', 'Restrictions');
            $this->headerinfo = 'Restrictions';
            $this->pageTitle = "Muvi | Restrictions";

            $studio_id = Yii::app()->user->studio_id;
            $geoblock['active'] = 0;
            $geoblock['website'] = 0;
            $geoblock['content'] = 0;
            $restricted_countries = array();
            $srest = StudioCountryRestriction::model();
            $srest = $srest->findAllByAttributes(array('studio_id' => $studio_id));
            /* Geo Block Specific content */
            $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));            
            /* END */
            $ppvValidity = array();
            $ppvValidity = PpvValidity::model()->findByAttributes(array('studio_id' => $studio_id));
            if (count($srest) > 0) {
                $geoblock['active'] = 1;
                $geoblock['website'] = 1;
                foreach ($srest as $value) {
                    $restricted_countries[] = $value['country_code'];
                }
            }
            if (count($grestcat) > 0) {
                $geoblock['active'] = 1;
                $geoblock['content'] = 1;
                foreach ($grestcat as $value) {
                    $grest = StudioContentRestriction::model()->findAllByAttributes(array('studio_id' => $studio_id,'category_id' => $value['id']));
                    foreach ($grest as $value1) {
                        $restricted_content_countries[$value['id']][] = $value1['country_code'];
                    }
                }
            }
            //All countries
            $countries = Countries::model()->findAll();
            //$sql = "SELECT cu.* FROM payment_gateways pg, currency cu, currency_supports cs WHERE pg.id=4 AND cu.id = cs.currency_id AND cs.payment_gateway_id = pg.id GROUP BY cu.id";
            //$sql = "SELECT cu.* FROM currency cu,currency_supports cs, payment_gateways pg, studio_payment_gateways spg WHERE  cu.id = cs.currency_id AND cs.payment_gateway_id = pg.id AND pg.id = spg.gateway_id AND spg.status = 1 AND spg.studio_id = ".$studio_id;
            //$currency = Yii::app()->db->createCommand($sql)->queryAll();
            //print "<pre>";print_r($currency);print "</pre>";
            //$currency = Currency::model()->findAll(array('order' => 'code'));
            $currency = Yii::app()->common->getStudioCurrency();
            $this->render('restrictions', array('studio' => $this->studio,'grestcat'=>$grestcat, 'geoblock' => $geoblock, 'restricted_countries' => $restricted_countries, 'restricted_content_countries' => $restricted_content_countries, 'countries' => $countries, 'ppvValidity' => $ppvValidity, 'currency' => $currency));
        
    }

    function actionSaverestrictions() {
        $studio_id = Yii::app()->user->studio_id;
        $std = new Studio();
        $std = $std->findByPk($studio_id);
        parse_str($_REQUEST["data"], $_REQUEST);
        if (isset($_REQUEST['need_login'])) {
            $std->need_login = 0;
            $std->save();
        } else {
            $std->need_login = 1;
            $std->save();
        }
        $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
        if(isset($_REQUEST['login_free_content'])){
            if(isset($std_config) && !empty($std_config)){
                $std_config->config_value = 1;
                $std_config->save();
            } 
        } else{  
            if(count($std_config) <= 0){
                $std_config = New StudioConfig; 
                $std_config->studio_id = $studio_id;
                $std_config->config_key = 'free_content_login';
                $std_config->config_value = 0;
                $std_config->save(); 
                
                $std_id = $std_config->id; 
            } else {
                $std_config->config_value = 0;
                $std_config->save();               
            }
        }

        //For Simultaneous login
        if (isset($_REQUEST['hide_simultaneous_login'])) {
            $sconfig = StudioConfig::model()->getConfig($studio_id, 'hide_simultaneous_login');
            if(isset($sconfig) && count($sconfig) > 0){                
                $sconfig->config_value = 1;
                $sconfig->save();
            }else{                        
                $sconfig = New StudioConfig;
                $sconfig->studio_id = $studio_id;
                $sconfig->config_key = 'hide_simultaneous_login';
                $sconfig->config_value = 1;
                $sconfig->save();
            }
        } else {
            $sconfig = StudioConfig::model()->getConfig($studio_id, 'hide_simultaneous_login');
            if(isset($sconfig) && count($sconfig) > 0){
                $sconfig->config_value = 0;
                $sconfig->save();
            }else{            
                $sconfig = New StudioConfig;
                $sconfig->studio_id = $studio_id;
                $sconfig->config_key = 'hide_simultaneous_login';
                $sconfig->config_value = 0;
                $sconfig->save();
            }
        }         
        
        //For Login Limit
        if (isset($_REQUEST['limit_login']) && $_REQUEST['limit_login'] !="") {
            $limit_login = trim($_REQUEST['limit_login']);
            $sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_login');
            if(isset($sconfig) && count($sconfig) > 0){                
                $sconfig->config_value = $limit_login > 0 ? $limit_login : 1;
                $sconfig->save();
            }else{                        
                $sconfig = New StudioConfig;
                $sconfig->studio_id = $studio_id;
                $sconfig->config_key = 'limit_login';
                $sconfig->config_value = $limit_login > 0 ? $limit_login : 1;
                $sconfig->save();
            }
        } else {
            $sconfig = StudioConfig::model()->getConfig($studio_id, 'limit_login');
            if(isset($sconfig) && count($sconfig) > 0){
                $sconfig->config_value = 1;
                $sconfig->save();
            }else{            
                $sconfig = New StudioConfig;
                $sconfig->studio_id = $studio_id;
                $sconfig->config_key = 'limit_login';
                $sconfig->config_value = 1;
                $sconfig->save();
            }
        }       
        
        if (isset($_REQUEST['default_currency_id'])) {
            $std->default_currency_id = $_REQUEST['default_currency_id'];
            $std->save();
        }
        
        if (isset($_REQUEST['selected_countries']) && !empty($_REQUEST['selected_countries']) && isset($_REQUEST['geoblock_active']) && $_REQUEST['geoblock_active'] == 1) {
            $countries = $_REQUEST['selected_countries'];
        } else {
            $countries = '';
        }

        if (count($countries) > 0) {
            StudioCountryRestriction::model()->deleteAll("studio_id = '" . $studio_id . "'");
            foreach ($countries as $country) {
                $scr = New StudioCountryRestriction;
                $scr->studio_id = $studio_id;
                $scr->country_code = $country;
                $scr->created_date = new CDbExpression("NOW()");
                $scr->created_by = Yii::app()->user->id;
                $scr->save();
            }
        }
        if(!isset($_REQUEST['geoblock_content'])){
            StudioContentRestriction::model()->deleteAll("studio_id = '" . $studio_id . "'");
            foreach ($_REQUEST['catname'] as $key => $value) {
                $geoblockcategory = New GeoBlockCategory;
                $geoblockcategory->deleteByPk($_REQUEST['categoryid'][$key]);
            }
        }
        if (isset($_REQUEST['geoblock_content']) && $_REQUEST['geoblock_content'] == 1) {
            StudioContentRestriction::model()->deleteAll("studio_id = '" . $studio_id . "'");
            foreach ($_REQUEST['catname'] as $key => $value) {
                $geoblockcategory = New GeoBlockCategory;
                $categoryid = $geoblockcategory->findByAttributes(array('id' =>$_REQUEST['categoryid'][$key]));
                if ($categoryid) {
                    $categoryid->category_name = $value;
                    $insertflag = $categoryid->save();
                    $id = $categoryid->id;
                }else{
                    $geoblockcategory->studio_id = $studio_id;
                    $geoblockcategory->category_name = $value;
                    $insertflag = $geoblockcategory->save();
                    $id = $geoblockcategory->id;
                }
                if($insertflag){
                    if(isset($_REQUEST['selected_countries_content'][$key])){
                        foreach ($_REQUEST['selected_countries_content'][$key] as $country) {
                            $scrc = New StudioContentRestriction;
                            $scrc->studio_id = $studio_id;
                            $scrc->category_id = $id;
                            $scrc->country_code = $country;
                            $scrc->created_date = new CDbExpression("NOW()");
                            $scrc->created_by = Yii::app()->user->id;
                            $scrc->ip = $_SERVER["REMOTE_ADDR"];
                            $scrc->save();
                        }
                    }
                }
            }
        } 
        
        $arr = array('msg' => 'Countries saved successfully.');
        echo json_encode($arr);
        exit;
    }
    
    function actionPpvSettings() {
        $this->breadcrumbs = array('Monetization', 'PPV Settings');
        $this->headerinfo = 'PPV Settings';
        $this->pageTitle = "Muvi | PPV Settings";
        
        $studio_id = $this->studio->id;
        $monetization_code = 'ppv';
        
        if (isset($_POST) && !empty($_POST)) {
            $is_update = 0;
            
            if (isset($_POST['view_restriction']) && $_POST['view_restriction'] && isset($_POST['limit_video']) && intval($_POST['limit_video']) >= 0) {
                $is_update = 1;
            } else if (isset($_POST['access_restriction']) && intval($_POST['access_restriction']) && isset($_POST['access_period']['validity_period']) && intval($_POST['access_period']['validity_period']) >=0) {
                $is_update = 1;
            } else if (isset($_POST['watch_restriction']) && intval($_POST['watch_restriction']) && isset($_POST['watch_period']['validity_period']) && intval($_POST['watch_period']['validity_period']) >=0) {
                $is_update = 1;
            }
            
            if (intval($is_update)) {
                PpvValidity::model()->deleteAll('studio_id = :studio_id AND monetization_code=:monetization_code', array(':studio_id' => $studio_id, ':monetization_code' => $monetization_code));

                if (isset($_POST['view_restriction']) && $_POST['view_restriction'] && isset($_POST['limit_video']) && intval($_POST['limit_video']) >= 0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = 'limit_video';
                    $validityModel->validity_period = $_POST['limit_video'];
                    $validityModel->validity_recurrence = '';
                    $validityModel->save();
                }

                if (isset($_POST['access_restriction']) && intval($_POST['access_restriction']) && isset($_POST['access_period']['validity_period']) && intval($_POST['access_period']['validity_period']) >=0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = $_POST['access_period']['playbility_access_key'];
                    $validityModel->validity_period = $_POST['access_period']['validity_period'];
                    $validityModel->validity_recurrence = $_POST['access_period']['validity_recurrence'];
                    $validityModel->save();
                }

                if (isset($_POST['watch_restriction']) && intval($_POST['watch_restriction']) && isset($_POST['watch_period']['validity_period']) && intval($_POST['watch_period']['validity_period']) >=0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = $_POST['watch_period']['playbility_access_key'];
                    $validityModel->validity_period = $_POST['watch_period']['validity_period'];
                    $validityModel->validity_recurrence = $_POST['watch_period']['validity_recurrence'];
                    $validityModel->save();
                }
            } else {
                PpvValidity::model()->deleteAll('studio_id = :studio_id AND monetization_code=:monetization_code', array(':studio_id' => $studio_id, ':monetization_code' => $monetization_code));
            }
            
            Yii::app()->user->setFlash('success', 'PPV settings updated successfully');
            $url = $this->createUrl('monetization/ppvSettings');
            $this->redirect($url);
        } else {
            $ppvValidity = PpvValidity::model()->findAllByAttributes(array('studio_id' => $studio_id, 'monetization_code' => $monetization_code));
            $this->render('ppvsettings',array('ppvValidity' => $ppvValidity));
        }
    }
    
    function actionVoucherSettings() {
        $this->breadcrumbs = array('Monetization', 'Voucher Settings');
        $this->headerinfo = 'Voucher Settings';
        $this->pageTitle = "Muvi | Voucher Settings";
        
        $studio_id = $this->studio->id;
        $monetization_code = 'voucher';
        
        if (isset($_POST) && !empty($_POST)) {
            $is_update = 0;
            
            if (isset($_POST['view_restriction']) && $_POST['view_restriction'] && isset($_POST['limit_video']) && intval($_POST['limit_video']) >= 0) {
                $is_update = 1;
            } else if (isset($_POST['access_restriction']) && intval($_POST['access_restriction']) && isset($_POST['access_period']['validity_period']) && intval($_POST['access_period']['validity_period']) >=0) {
                $is_update = 1;
            } else if (isset($_POST['watch_restriction']) && intval($_POST['watch_restriction']) && isset($_POST['watch_period']['validity_period']) && intval($_POST['watch_period']['validity_period']) >=0) {
                $is_update = 1;
            }
            
            if (intval($is_update)) {
                PpvValidity::model()->deleteAll('studio_id = :studio_id AND monetization_code=:monetization_code', array(':studio_id' => $studio_id, ':monetization_code' => $monetization_code));

                if (isset($_POST['view_restriction']) && $_POST['view_restriction'] && isset($_POST['limit_video']) && intval($_POST['limit_video']) >= 0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = 'limit_video';
                    $validityModel->validity_period = $_POST['limit_video'];
                    $validityModel->validity_recurrence = '';
                    $validityModel->save();
                }

                if (isset($_POST['access_restriction']) && intval($_POST['access_restriction']) && isset($_POST['access_period']['validity_period']) && intval($_POST['access_period']['validity_period']) >=0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = $_POST['access_period']['playbility_access_key'];
                    $validityModel->validity_period = $_POST['access_period']['validity_period'];
                    $validityModel->validity_recurrence = $_POST['access_period']['validity_recurrence'];
                    $validityModel->save();
                }

                if (isset($_POST['watch_restriction']) && intval($_POST['watch_restriction']) && isset($_POST['watch_period']['validity_period']) && intval($_POST['watch_period']['validity_period']) >=0) {
                    $validityModel = new PpvValidity();
                    $validityModel->studio_id = $studio_id;
                    $validityModel->monetization_code = $monetization_code;
                    $validityModel->playbility_access_key = $_POST['watch_period']['playbility_access_key'];
                    $validityModel->validity_period = $_POST['watch_period']['validity_period'];
                    $validityModel->validity_recurrence = $_POST['watch_period']['validity_recurrence'];
                    $validityModel->save();
                }
            } else {
                PpvValidity::model()->deleteAll('studio_id = :studio_id AND monetization_code=:monetization_code', array(':studio_id' => $studio_id, ':monetization_code' => $monetization_code));
            }
            
            Yii::app()->user->setFlash('success', 'Voucher settings updated successfully');
            $url = $this->createUrl('monetization/voucherSettings');
            $this->redirect($url);
        } else {
            $voucherValidity = PpvValidity::model()->findAllByAttributes(array('studio_id' => $studio_id, 'monetization_code' => $monetization_code));
            $this->render('vouchersettings',array('voucherValidity' => $voucherValidity));
        }
    }
 
 	function isAjaxRequest() {
		if (!Yii::app()->request->isAjaxRequest) {
			$url = Yii::app()->createAbsoluteUrl();
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: '.$url);exit();
		}
	}   
    
    function actionTransactionForGatewayIntegration() {
    self::isAjaxRequest();
    $purifier = new CHtmlPurifier();
    $_POST = $purifier->purify($_POST);
    $this->layout = false;     
    if(@$_POST['csrfToken'] === @$_SESSION['csrfToken']) {  
        $res['isSuccess'] = 0;
        if (isset($_POST) && !empty($_POST)) {
            $user = array();
            $user['studio_id'] = Yii::app()->user->studio_id;
            $user['card_name'] = $_POST['card_name'];
            $user['card_number'] = $_POST['card_number'];
            $user['exp_month'] = $_POST['exp_month'];
            $user['exp_year'] = $_POST['exp_year'];
            $user['cvv'] = $_POST['cvv'];
            $user['amount'] = '1.00';
            $user['currency_code'] = isset($_SESSION['currency_code']) && trim($_SESSION['currency_code']) ? $_SESSION['currency_code'] : 'USD';
            $user['email'] = Yii::app()->user->email;
            //$user['stripetoken'] =  isset($_POST['stripetoken']) ? $_POST['stripetoken'] : '';
            $gateway = array();
               parse_str(str_replace('amp;', '', $_POST['data']), $gateway);
            $user['dprp_status'] = $gateway['dprp_status'];
            if (DOMAIN == 'idogic' || HOST_IP != '52.0.232.150') {
                $gateway['data']['api_mode'] = 'sandbox';
            }else{
                $gateway['data']['api_mode'] = 'live';
            }
            $data[0] = (object) $gateway['data'];
            //Set the payment gateway variables
            $this->setPaymentGatwayVariable($data);
            //Initiallise the payment gateway
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway['data']['short_code']] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $res = $payment_gateway::sampleIntegrationTransaction($user);
        }
        
        echo json_encode($res);
    }
    }
    function processInvoiceLogoImage($theme_folder='',$cropDimension='',$type='logos'){
		$studio_id = Yii::app()->common->getStudiosId();
		$url = $this->createUrl('/monetization/invoice');
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $theme_folder;
		if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
			$file_info = pathinfo($_FILES['Filedata']['name']);
			$extension = array('PNG', 'png', 'JPG', 'jpeg', 'JPEG', 'jpg');
			if (!in_array($file_info['extension'], $extension)) {
				Yii::app()->user->setFlash('error', 'Please upload valid files formats(png,jpg,jpeg).');
				$url = $this->createUrl('/monetization/invoice');
				$this->redirect($url);exit;            
			}

			if($_REQUEST['fileimage']['w']=='' && $_REQUEST['fileimage']['h']==''){
				Yii::app()->user->setFlash('error', 'Please crop image to upload.');
				$this->redirect($url);exit;                
			}
			$cdimension = array('thumb' => "64x64");
			$ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);
			
			$path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
			if(@$path){
				$ret = $this->uploadLogo($_FILES['Filedata'], $cropDimension,$theme_folder, $path,$type);
				Yii::app()->common->rrmdir($dir.'/jcrop');
			}else{
				Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
				$this->redirect($url);exit;
			}
			return $ret;
			
		} else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {
			
			$file_info = pathinfo($_REQUEST['g_image_file_name']);
			$_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
			$jcrop_allimage = $_REQUEST['jcrop_allimage'];
			$image_name = $_REQUEST['g_image_file_name'];

			$dimension['x1'] = $jcrop_allimage['x13'];
			$dimension['y1'] = $jcrop_allimage['y13'];
			$dimension['x2'] = $jcrop_allimage['x23'];
			$dimension['y2'] = $jcrop_allimage['y23'];
			$dimension['w'] = $jcrop_allimage['w3'];
			$dimension['h'] = $jcrop_allimage['h3'];
			
			$path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
			if(@$path){
				$fileinfo['name'] = $_REQUEST['g_image_file_name'];
				$fileinfo['error'] = 0;
				$_FILES['Filedata']['name'] = $_REQUEST['g_image_file_name'];
				$ret = $this->uploadLogo($fileinfo, $cropDimension,$theme_folder, $path,$type);
			}else{
				Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
				$this->redirect($url);exit;
			}
			Yii::app()->common->rrmdir($dir.'/jcrop');
			return $ret;
		} else {
			return false;
		}
	}
    public function actionInvoice(){
        $this->breadcrumbs = array('Monetization', 'Invoice');
        $this->headerinfo = 'Invoice';
        $this->pageTitle = "Muvi | Invoice";
        $studio = $this->studio;
        $studio_id = $studio->id;
        $language_id = $this->language_id;
        //Insert Invoice Logo Dimension//
        $check_invoice_logo = StudioConfig::model()->find(array('condition'=>'config_key=:config_key AND studio_id=:studio_id','params'=>array(':config_key'=>'invoice_logo_dimension', ':studio_id'=>$studio_id)));
        if(isset($_POST['width']) && isset($_POST['height'])){
            $invoice_logo_width = $_POST['width'];
            $invoice_logo_height = $_POST['height'];
            $config_key = 'invoice_logo_dimension';
            $config_val = $invoice_logo_width.'x'.$invoice_logo_height;
            
            
            if($check_invoice_logo){
                $post = StudioConfig::model()->findByPk($check_invoice_logo->id);
            }else{
                $post=new StudioConfig();
                $post->config_key = $config_key;
                $post->studio_id = $studio_id;
            }
            
            $post->config_value = $config_val;
            $post->save();
            exit;
        }
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $logo_dimension = Yii::app()->general->getInvoiceLogoDimension($studio_id);
        $dimension['invoicelogo'] = $logo_dimension;
        $this->render('invoice',array('studio' => $studio,'studio_id' => $studio_id,'dimension' => $dimension, 'all_images' => $all_images,'language_id' => $language_id, 'get_invoice_logo'=>$check_invoice_logo));
    }
    public function actionSaveinvoicelogo() {
        $studio_id = Yii::app()->user->studio_id;
        $new_cdn_user = Yii::app()->user->new_cdn_users;
        $user_id = Yii::app()->user->id;
        $std = $this->studio;
        $theme_folder = $std->theme;
        $parent_theme = $std->parent_theme;
        $logo_dimension = Yii::app()->general->getInvoiceLogoDimension($studio_id);
        $width = $logo_dimension['logo_width'];
        $height = $logo_dimension['logo_height'];
		$cropDimension = array('original' => $width.'x'.$height);
		if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
            $return = $this->processInvoiceLogoImage($theme_folder,$cropDimension);
			$imgName = $_FILES['Filedata']['name'];
			$std->show_sample_data = 0;
			$std->invoice_logo_file = $imgName;
			$std->save();
			$this->redirect($this->createUrl('/monetization/invoice'));exit;
        } else {
            Yii::app()->user->setFlash('error', 'Please choose image to upload.');
            $url = $this->createUrl('/monetization/invoice');
            $this->redirect($url);
            exit;
        }
    }
    
    public function actioninvoiceimageGallery(){
        $studio_id = Yii::app()->user->studio_id;
        $all_images = ImageManagement::model()->get_imagedetails_by_studio_id($studio_id);
        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
        $this->renderPartial('invoice_image_gallery_with_crop',array('all_images' => $all_images,'base_cloud_url'=>$base_cloud_url));
    }
    public function actionSettings()
    {
        /*1.subscription---1*/
        /*2.pay-per-view---2*/
        /*3.advertising----4*/
        /*4.freecontent----8*/
        /*5.advancepurchase----16*/
        /*6.coupons----32*/
        /*7.Ppv Bundles----64*/
         /*8.voucher----128*/
        /*9.muvi credits----256*/
        
        $this->breadcrumbs = array('Monetization', 'Settings');
        $this->headerinfo = 'Settings';
        $this->pageTitle = "Muvi | Settings";
        $studio_id = $this->studio->id;                   
        $config = StudioConfig::model()->getconfigvalue('free_content_login');  
		$payment_gateway = Yii::app()->common->isPaymentGatwayExists($studio_id);
        if(isset($_REQUEST) && !empty($_REQUEST)){
            parse_str($_REQUEST["monetization"], $_REQUEST);
            $menu_sum = array_sum($_REQUEST);
            $res = MonetizationMenuSettings::model()->updateMenuSettings($studio_id,$menu_sum);
            /*$url = $this->createUrl('monetization/settings');
            $this->redirect($url);*/
            echo 1;exit;
        }else{
            $studio_id = Yii::app()->user->studio_id;
            $geoblock['active'] = 0;
            $geoblock['website'] = 0;
            $geoblock['content'] = 0;
            $restricted_countries = array();
            $srest = StudioCountryRestriction::model();
            $srest = $srest->findAllByAttributes(array('studio_id' => $studio_id));
            /* Geo Block Specific content */
            $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));            
            /* END */
            if (count($srest) > 0) {
                $geoblock['active'] = 1;
                $geoblock['website'] = 1;
                foreach ($srest as $value) {
                    $restricted_countries[] = $value['country_code'];
                }
            }
            if (count($grestcat) > 0) {
                $geoblock['active'] = 1;
                $geoblock['content'] = 1;
                foreach ($grestcat as $value) {
                    $grest = StudioContentRestriction::model()->findAllByAttributes(array('studio_id' => $studio_id,'category_id' => $value['id']));
                    foreach ($grest as $value1) {
                        $restricted_content_countries[$value['id']][] = $value1['country_code'];
                    }
                }
            }
            //All countries
            $countries = Countries::model()->findAll();
            $currency = Yii::app()->common->getStudioCurrency();
           
            $isPopup = 0;
            $data = Yii::app()->general->monetizationMenuSetting($studio_id);
            $hide_simultaneous_login = Yii::app()->custom->hideSimultaneousLoginStatus($studio_id);
            $limit_login = 1;
            if($hide_simultaneous_login == true){
                $limit_login = Yii::app()->custom->limitLogin($studio_id);
            }
			//Restrict Devices
            $restrict_no_devices=Yii::app()->custom->restrictDevices($studio_id,'restrict_no_devices');
            $limit_devices=($restrict_no_devices>0)?Yii::app()->custom->restrictDevices($studio_id,'limit_devices'):1;  
			
             $sql = "SELECT spg.*, s.id AS studio_id, s.name, s.status, s.is_subscribed, 
            s.is_default FROM studios s LEFT JOIN studio_payment_gateways spg 
            ON (spg.studio_id = s.id AND spg.status=1) WHERE s.id=" . $studio_id . " ORDER BY spg.is_primary DESC";
            $dbcon = Yii::app()->db;
            $subscriptiondata = $dbcon->createCommand($sql)->queryAll();
            $this->render('settings',array('data' => $data,'subscriptiondata'=>$subscriptiondata,'popup' => $isPopup,'studio' => $this->studio,'grestcat'=>$grestcat, 'geoblock' => $geoblock, 'restricted_countries' => $restricted_countries, 'restricted_content_countries' => $restricted_content_countries, 'countries' => $countries, 'currency' => $currency, 'hide_simultaneous_login' => $hide_simultaneous_login,'limit_login'=>$limit_login,'restrict_no_devices'=>$restrict_no_devices,'limit_devices'=>$limit_devices,'config'=>$config,'payment_gateway'=>$payment_gateway));
        }
		//Clear the session stored monitization settings.
		$_SESSION[$studio_id]['MonitizationSettings']='';
		unset($_SESSION[$studio_id]['MonitizationSettings']);
    }
    
    public function actionCheckSettings()
    {
        $studio_id = $this->studio->id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($data) && !empty($data)){
            echo json_encode($data);
        }else{
            $isPopup = 1;
            $this->renderPartial('settings',array('data' => $data,'popup' => $isPopup));
        }
    }
    
    public function actionEndusersupport()
    {
        $this->pageTitle = Yii::app()->name .' | ' . 'End User Support';
        $page_size = 20;
        $this->breadcrumbs = array('Support', 'End User Support');
        $this->headerinfo = 'End User Support';
        $this->render('endusersupport',array('page_size'=>$page_size));
    }
    
    public function actionGetEndUserList() {
        $type = (isset($_REQUEST['user_type']) && trim($_REQUEST['user_type'])) ? $_REQUEST['user_type'] : 'all';
        $searchKey = (isset($_REQUEST['search_value']) && trim($_REQUEST['search_value'])) ? $_REQUEST['search_value'] : '';
        $page_size = 20;
        $offset = (isset($_REQUEST['page'])) ? (($_REQUEST['page'] - 1) * $page_size) : 0;
        
        $res = Report::model()->endUserList($type, $offset, $page_size, $searchKey);
        $this->renderPartial('endusersupportlist', array('data' => $res, 'page_size' => $page_size, 'type' => $type));
    }
    
    public function actionCurrencysetup()
    {
        $this->pageTitle = Yii::app()->name .' | ' . 'Currency Setup';
        $this->breadcrumbs = array('Reports' => array('report/currencysetup'), 'Currency Setup');
        
        $studio_id = $this->studio->id;
        $sql = "SELECT cu.*,scs.id as scs_id,scs.status,scs.is_default FROM currency cu,studio_currency_support scs WHERE  cu.id = scs.currency_id AND scs.studio_id = ".$studio_id;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        //print_r($data);
        $this->render('currencysetup',array('data'=>$data));
    }
    
    public function actionFilterCurrency()
    {
        $return_arr = array();
        $studio_id = $this->studio->id;
        if (isset($_GET['term']) && trim($_GET['term'])){
            $sql = "SELECT cu.code,cu.title FROM currency cu LEFT JOIN (currency_supports cs,payment_gateways pg,studio_payment_gateways spg) ON (cu.id = cs.currency_id AND cs.payment_gateway_id = pg.id AND pg.id = spg.gateway_id AND spg.status = 1 AND spg.studio_id = ".$studio_id.") WHERE cu.id NOT IN (SELECT scs.currency_id FROM studio_currency_support scs WHERE studio_id = ".$studio_id.") AND (cu.code like '%".$_GET['term']."%' OR cu.title like '%".$_GET['term']."%') GROUP BY code";
            $currency = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($currency as $curr){
                $return_arr[] = array(
                                    'id' => $curr['code'],
                                    'label' => $curr['title'],
                                );
            }
        }
        echo json_encode($return_arr);
    }
    
    public function actionAddCurrency()
    {
        $res = array();
        $studio = $this->studio;
        $studio_id = $studio->id;
        if(isset($_POST) && trim($_POST['currency'])) {
            $sql = "SELECT id FROM currency WHERE code = '".$_POST['currency']."' OR title  LIKE '%".ucwords(strtolower($_POST['currency']))."%'";
            $currData = Yii::app()->db->createCommand($sql)->queryAll();
            if(isset($currData) && !empty($currData) && count($currData) >= 1){
                $scs = StudioCurrencySupport::model()->findByAttributes(array('studio_id'=> $studio_id,'currency_id' => $currData[0]['id']));
                if(isset($scs) && !empty($scs)){
                    $res['isCurr'] = 1;
                    $res['error'] = 'Currency already added!';
                }else{
                    $dsql = "SELECT * FROM studio_currency_support WHERE studio_id = ".$studio_id." AND status = 1";
                    $data = Yii::app()->db->createCommand($dsql)->queryAll();
                    $stdcurr = new StudioCurrencySupport();
                    $stdcurr->studio_id = $studio_id;
                    $stdcurr->currency_id = $currData[0]['id'];
                    $stdcurr->created_by = Yii::app()->user->id;
                    $stdcurr->created_date = Date('Y-m-d H:i:s');
                    if(isset($data) && empty($data)){
                        $stdcurr->is_default = 1;
                        $studio->default_currency_id = $currData[0]['id'];
                        $studio->save();
                    }
                    $stdcurr->save();
                    $res['isCurr'] = 0;
                }
            }else{
                $res['isCurr'] = 1;
                $res['error'] = "Currency doesn't supported or doesn't exists.";
            }
            echo json_encode($res);
        }
    }
    
    public function actionGetCurrencyList()
    {
        $studio_id = $this->studio->id;
        $sql = "SELECT cu.*,scs.id as scs_id,scs.status,scs.is_default FROM currency cu,studio_currency_support scs WHERE  cu.id = scs.currency_id AND scs.studio_id = ".$studio_id." ORDER BY scs.is_default desc";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $this->renderPartial('supportedcurrency',array('data' => $data));
    }
    
    public function actionMakeDefault()
    {
        $studio = $this->studio;
        $studio_id = $this->studio->id;
        if(isset($_REQUEST['id']) && $_REQUEST['id']){
            $sql = "UPDATE studio_currency_support SET is_default = 0 WHERE studio_id = ".$studio_id;
            Yii::app()->db->createCommand($sql)->execute();
            $scs = StudioCurrencySupport::model()->findByPk(array('id' => $_REQUEST['id']));
            $scs->is_default = 1;
            $scs->save();
            $studio->default_currency_id = $scs->currency_id;
            $studio->save();
            echo 1;
        }
    }


    public function actionAdvancePurchase() {
        $this->pageTitle = Yii::app()->name . ' | ' . ' Pre-Order ';
        $this->headerinfo = 'Pre-Order';
        $this->breadcrumbs = array('Monetization', 'Pre-Order');
        
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $data = $adv_single = $adv_multi = array();
        $isAPV = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isAPV['menu']) && !empty($isAPV['menu']) && ($isAPV['menu'] & 16)) {
            $advPlans = PpvPlans::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'status' => 1, 'is_advance_purchase' => 1));

            if (isset($advPlans) && !empty($advPlans)) {

                foreach ($advPlans as $key1 => $value1) {
                    $plan_id = $value1->id;
                    $pricing = self::getAdvPrices($studio->default_currency_id, $plan_id);
                    $adv_content = self::getAdvContents($plan_id);

                    if (isset($value1->content_types_id) && ($value1->content_types_id == 1)) {
                        $adv_single[$value1->id]['plans'] = $value1;
                        $adv_single[$value1->id]['pricing'] = $pricing;
                        $adv_single[$value1->id]['contents'] = $adv_content;
                    } else if (isset($value1->content_types_id) && ($value1->content_types_id == 3)) {
                        $adv_multi[$value1->id]['plans'] = $value1;
                        $adv_multi[$value1->id]['pricing'] = $pricing;
                        $adv_multi[$value1->id]['contents'] = $adv_content;
                    }
                }
            }

            $data['adv_single'] = $adv_single;
            $data['adv_multi'] = $adv_multi;
            $data['isAPVEnable'] = 1;
        } else {
            $data['isAPVEnable'] = 0;
        }
        //print '<pre>';print_r($data);exit;
        $this->render('advance_purchase', array('studio' => $studio, 'data' => $data));
    }
    
    function getAdvPrices($default_currency_id = 0, $plan_id = 0) {
        $pricing = '';
        
        if (intval($default_currency_id) && intval($plan_id)) {
            $con = Yii::app()->db;
            
            $sql = "SELECT pr.*, pr.id AS ppv_pricing_id, cu.* FROM ppv_pricing pr LEFT JOIN currency AS cu 
            ON (pr.currency_id=cu.id) WHERE pr.ppv_plan_id=".$plan_id." AND pr.status=1 ORDER BY FIND_IN_SET(cu.id, ".$default_currency_id.") DESC";
            $pricing = $con->createCommand($sql)->queryAll();
        }
        
        return $pricing;
    }
    
    function getAdvContents($plan_id = 0) {
        $adv_content = '';
        
        if (intval($plan_id)) {
            $studio = $this->studio;
            $studio_id = $studio->id;
            
            $con = Yii::app()->db;

            $csql = "SELECT pac.content_id, fm.name FROM ppv_advance_content pac, films fm WHERE fm.studio_id=".$studio_id." AND pac.content_id=fm.id AND pac.ppv_plan_id=".$plan_id;
            $adv_content = $con->createCommand($csql)->queryAll();
        }
        
        return $adv_content;
    }
    
    function actionAdvContents() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $csql = "SELECT f.id AS content_id, f.name FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id."
        AND f.content_types_id IN (1, 4) AND f.id = ms.movie_id AND ms.is_episode=0 AND ms.is_converted=0 AND 
        f.id NOT IN (SELECT content_id FROM ppv_advance_content WHERE ppv_plan_id IN 
        (SELECT id FROM ppv_plans WHERE status=1 AND is_deleted=0 AND is_advance_purchase=1 AND studio_id=".$studio_id." AND content_types_id IN (1, 4))) 
        ORDER BY f.name ASC";
        $content = $con->createCommand($csql)->queryAll();

        echo json_encode($content);
        exit;
    }
    
    function actionAddEditCategoryAdvPurchase() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $advPlans = $pricing = $content = array();
        
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $id = $_REQUEST['id_ppv'];
            $advPlans = PpvPlans::model()->findByAttributes(array('id' => $id, 'studio_id' => $studio_id, 'is_deleted' => 0, 'is_advance_purchase' => 1, 'content_types_id' => $type));
            
            if (!empty($advPlans)) {
                $pricing = self::getAdvPrices($studio->default_currency_id, $id);
                $content = self::getAdvContents($id);
            }
        }
        
        //$currency = (array)(Currency::model()->findAll(array('order' => 'code')));
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('categoryadv', array('type' => $type, 'advPlans' => $advPlans, 'pricing' => $pricing, 'content' => $content, 'currency' => $currency, 'studio' => $studio));
    }
    
    
    function actionAddEditSingleAdv() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = Yii::app()->user->id;
        
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                $con = Yii::app()->db;
                $sql_usr = "UPDATE ppv_pricing SET status=0 WHERE ppv_plan_id =" . $_REQUEST['data']['id'];
                $con->createCommand($sql_usr)->execute();
                
                $ppvModel = PpvPlans::model()->findByPk($_REQUEST['data']['id']);
                $ppvModel->last_updated_by = $user_id;
                $ppvModel->last_updated_date = new CDbExpression("NOW()");
            } else {
                $ppvModel = New PpvPlans;
                $ppvModel->created_by = $user_id;
                $ppvModel->created_date = new CDbExpression("NOW()");
            }
            
            //Save plan
            $ppvModel->studio_id = $studio_id;
            $ppvModel->user_id = $user_id;
            $ppvModel->content_types_id = $_REQUEST['data']['content_types_id'];
            $ppvModel->status = 1;
            $ppvModel->is_advance_purchase = $_REQUEST['data']['is_advance_purchase'];
            $ppvModel->is_all = $_REQUEST['data']['is_all'];
            $ppvModel->start_date = new CDbExpression("NOW()");
            $ppvModel->expiry_date = date('Y-m-d', strtotime($_REQUEST['data']['expiry_date']));

            if (isset($_REQUEST['data']['title']) && trim($_REQUEST['data']['title'])) {
                $ppvModel->title = $_REQUEST['data']['title'];
                $ppvModel->tag = str_replace(" ", "_", strtolower($_REQUEST['data']['title']));
            }
            
            $ppvModel->description = $_REQUEST['data']['description'];
            
            $ppvModel->save();
            $ppv_plan_id = $ppvModel->id;
            
            $status = explode(',', $_REQUEST['data']['allstatus']);
            
            //Save price in pricing table
            if (!empty($status)) {
                foreach ($status as $key => $value) {
                    $key = array_search($value, $_REQUEST['data']['currency_id']);

                    $ppvPricingModel = New PpvPricing;

                    if (isset($_REQUEST['data']['pricing_id'][$key]) && intval($_REQUEST['data']['pricing_id'][$key])) {
                        $ppvPricingModel = $ppvPricingModel->findByPk($_REQUEST['data']['pricing_id'][$key]);
                    }

                    $ppvPricingModel->ppv_plan_id = $ppv_plan_id;
                    $ppvPricingModel->currency_id = $_REQUEST['data']['currency_id'][$key];
                    $ppvPricingModel->price_for_unsubscribed = $_REQUEST['data']['price_for_unsubscribed'][$key];
                    $ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_for_subscribed'][$key];
                    $ppvPricingModel->status = 1;
                    $ppvPricingModel->save();
                }
            }
            
            //Save contents
            if (isset($_REQUEST['data']['content']) && !empty($_REQUEST['data']['content'])) {
                
                //Before saving, 1st delete all content related to plan and then save the content
                PpvAdvanceContent::model()->deleteAll("ppv_plan_id=:ppv_plan_id", array(':ppv_plan_id' => $ppv_plan_id));
                
                //Save content
                foreach ($_REQUEST['data']['content'] as $key => $value) {
                    $content = New PpvAdvanceContent;
                    $content->ppv_plan_id = $ppv_plan_id;
                    $content->content_id = $value;
                    
                    $content->save();
                }
            }
            
            Yii::app()->user->setFlash('success', 'Pre-Order has been updated successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in set of pre-order');
        }

        $url = $this->createUrl('monetization/advancePurchase');
        $this->redirect($url);
    }
    
    
    
    
    
    
    function actionDeleteAdV() {
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
            $studio = $this->studio;
            $studio_id = $studio->id;
            $user_id = Yii::app()->user->id;
            $id_ppv = $_REQUEST['id_ppv'];
            
            //Delete all ppv record
            $ppvModel = PpvPlans::model()->findByPk($id_ppv);
            $ppvModel->status = 0;
            $ppvModel->is_deleted = 1;
            $ppvModel->last_updated_by = $user_id;
            $ppvModel->last_updated_date = new CDbExpression("NOW()");
            $ppvModel->save();
            
            Yii::app()->user->setFlash('success', 'Pre-Order has been deleted successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry, Pre-Order can not be deleted!');
        }

        $url = $this->createUrl('monetization/advancePurchase');
        $this->redirect($url);
    }
    
    function actionFreeContent() {
        $this->pageTitle = Yii::app()->name . ' | ' . ' Free Content ';
        $this->headerinfo = 'Free Content';
        $this->breadcrumbs = array('Monetization', 'Free Content');
        
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $free_contents = FreeContent::model()->findAllByAttributes(array('studio_id' => $studio_id));
        
        $data = array();
        if (isset($free_contents) && !empty($free_contents)) {
            $cnt = 0;
            foreach ($free_contents as $key => $value) {
                $arg['movie_id'] = $value->movie_id;
                $arg['season_id'] = $value->season_id;
                $arg['episode_id'] = $value->episode_id;
                $arg['content_types_id'] = $value->content_types_id;
                $data[$cnt] = self::getSelectedFreeContent($arg);
                $cnt++;
            }
        }
        
        $this->render('free_content', array('data' => $data));
    }
    
    function getSelectedFreeContent($arg = array()) {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $movie_id = $content_types_id = $season_id = $episode_id = 0;
        $data = array();
        
        if (isset($arg) && !empty($arg)) {
            $movie_id = (isset($arg['movie_id']) && intval($arg['movie_id'])) ? $arg['movie_id'] : 0;
            $season_id = (isset($arg['season_id']) && intval($arg['season_id'])) ? $arg['season_id'] : 0;
            $episode_id = (isset($arg['episode_id']) && intval($arg['episode_id'])) ? $arg['episode_id'] : 0;
            $content_types_id = (isset($arg['content_types_id']) && intval($arg['content_types_id'])) ? $arg['content_types_id'] : 0;
            
            $cond.= (isset($arg['movie_id']) && intval($arg['movie_id'])) ? ' AND f.id='.$arg['movie_id'] : '';
            $cond.= (isset($arg['season_id']) && intval($arg['season_id'])) ? ' AND ms.series_number='.$arg['season_id'] : '';
            $cond.= (isset($arg['episode_id']) && intval($arg['episode_id'])) ? ' AND ms.id='.$arg['episode_id'] : '';
            $cond.= (isset($arg['content_types_id']) && intval($arg['content_types_id'])) ? ' AND f.content_types_id='.$arg['content_types_id'] : '';
        }
        
        if ($movie_id ==0 && $season_id == 0 && $episode_id == 0 && $content_types_id !=0) {
            $csql = "SELECT content_types_id FROM studio_content_types WHERE content_types_id =".$content_types_id." AND studio_id=".$studio_id." GROUP BY content_types_id ORDER BY content_types_id ASC";
            $content = $con->createCommand($csql)->queryRow();
            
            if (isset($content) && !empty($content)) {
                $data['content_id'] = '0_0_0_'.$content_types_id;
                $data['name'] = ($content_types_id == 3) ? "All Multi-part" : "All Single-part";
            }
        } else {
            $csql = "SELECT f.id AS movie_id, f.name, f.content_types_id, ms.id AS episode_id, ms.is_episode, ms.episode_title, ms.series_number 
            FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id = ms.movie_id ".$cond."
            ORDER BY f.content_types_id, f.name, ms.is_episode ASC, ms.episode_title ASC, ms.series_number ASC, f.id ASC LIMIT 0, 1";
            $content = $con->createCommand($csql)->queryRow();

            if (isset($content) && !empty($content)) {
                if (isset($arg['content_types_id']) && intval($arg['content_types_id']) == 3) {
                    if (isset($arg['season_id']) && intval($arg['season_id']) && (isset($arg['episode_id']) && intval($arg['episode_id']))) {//Episode
                        $data['content_id'] = $content['movie_id'].'_'.$arg['season_id'].'_'.$arg['episode_id'].'_'.$arg['content_types_id'];
                        if ($studio_id == 2471) {
                            $data['name'] = str_replace("'", "u0027", strtolower($content['name'])).': '.str_replace("'", "u0027", strtolower($content['episode_title']));
                        } else {
                            $data['name'] = str_replace("'", "u0027", strtolower($content['name'])).': Season-'.$arg['season_id'].': '.str_replace("'", "u0027", strtolower($content['episode_title']));
                        }
                    } else if (isset($arg['season_id']) && intval($arg['season_id']) == 0 && (isset($arg['episode_id']) && intval($arg['episode_id']))) {//Episode only for tata
                        $data['content_id'] = $content['movie_id'].'_'.$arg['season_id'].'_'.$arg['episode_id'].'_'.$arg['content_types_id'];
                        if ($studio_id == 2471) {
                            $data['name'] = str_replace("'", "u0027", strtolower($content['name'])).': '.str_replace("'", "u0027", strtolower($content['episode_title']));
                        }
                    } else if (isset($arg['season_id']) && intval($arg['season_id']) && (isset($arg['episode_id']) && intval($arg['episode_id']) == 0)) {//Season
                        $data['content_id'] = $content['movie_id'].'_'.$arg['season_id'].'_0_'.$arg['content_types_id'];
                        $data['name'] = str_replace("'", "u0027", strtolower($content['name'])).': Season-'.$arg['season_id'];
                    } else if (isset($arg['season_id']) && intval($arg['season_id']) == 0 && (isset($arg['episode_id']) && intval($arg['episode_id']) == 0)) {//Show
                        $data['content_id'] = $content['movie_id'].'_0_0_'.$arg['content_types_id'];
                        $data['name'] = str_replace("'", "u0027", strtolower($content['name']));
                    }
                } else {
                    $data['content_id'] = $content['movie_id'].'_0_0_'.$arg['content_types_id'];
                    $data['name'] = str_replace("'", "u0027", strtolower($content['name']));
                }
            }
        }
        
        return $data;
    }
    
    /*START:Get Content Function for Coupon*/
    public function actionGetContentsCouponDigital() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $res = array();
        $data = array();
        $cnt = 0;
        
        $csql = "SELECT f.id AS movie_id, f.name, f.content_types_id, ms.id AS episode_id, ms.is_episode, ms.episode_title, ms.series_number 
        FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id = ms.movie_id AND f.parent_id =0 AND ms.episode_parent_id=0
        ORDER BY f.content_types_id, f.name, ms.is_episode ASC, ms.episode_title ASC, ms.series_number ASC, f.id ASC";
        $content = $con->createCommand($csql)->queryAll();
        
        if (isset($content) && !empty($content)) {
            foreach ($content as $key => $value) {
                if ($value['content_types_id'] == 3) {
                    
                    if (intval($value['is_episode'])) {
                        $data[$value['movie_id']]['season'][$value['series_number']][$value['episode_id']] = $value['episode_title'];
                    } else {
                        $data[$value['movie_id']]['name'] = $value['name'];
                        $data[$value['movie_id']]['content_types_id'] = $value['content_types_id'];
                    }
                } else {
                    $data[$value['movie_id']]['name'] = $value['name'];
                    $data[$value['movie_id']]['content_types_id'] = $value['content_types_id'];
                }
            }
        }
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $content_id = $key.'_0_0_'.$value['content_types_id'];
                $res[$cnt]['content_id'] = $content_id;
                $res[$cnt]['name'] = $value['name'];
                
                if (isset($value['season']) && !empty($value['season'])) {
                    foreach ($value['season'] as $key1 => $season) {
                        if (intval($key1)) {
                        $cnt++;
                        $content_id = $key.'_'.$key1.'_0_'.$value['content_types_id'];
                        $res[$cnt]['content_id'] = $content_id;
                        if ($studio_id != 2471) {
                            $res[$cnt]['name'] = $value['name'].': Season-'.$key1;
                        }
                            
                        foreach ($season as $key2 => $episode) {
                            $cnt++;
                            $content_id = $key.'_'.$key1.'_'.$key2.'_'.$value['content_types_id'];
                            $res[$cnt]['content_id'] = $content_id;
                            if ($studio_id == 2471) {
                                $res[$cnt]['name'] = $value['name'].': '.$episode;
                            } else {
                                $res[$cnt]['name'] = $value['name'].': Season-'.$key1.': '.$episode;
                            }
                        }
                    }
                }
                }
                $cnt++;
            }
        }
        
        
        echo json_encode($res);
        exit;
    }
    
    public function actionGetContentsCouponPhysical() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $res = array();
        $data = array();
        $cnt = 0;
        
        $csql = "SELECT * FROM `pg_product` WHERE studio_id=".$studio_id;
        $content = $con->createCommand($csql)->queryAll();
        
        if (isset($content) && !empty($content)) {
            $cnt = 0;
            foreach ($content as $key => $value) {
                $res[$cnt]['content_id'] = $value['id'];
                $res[$cnt]['name'] = $value['name'];
                $cnt++;
            }
        }
        echo json_encode($res);
        exit;
    }
    /*END:Get Content Function for Coupon*/
    
    public function actionGetContents() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $res = array();
        $data = array();
        $cnt = 0;
        
        if (!isset($_REQUEST['voucher']) || !intval($_REQUEST['voucher'])) {
        $sql = "SELECT content_types_id FROM studio_content_types WHERE (content_types_id =1 OR content_types_id = 3) AND studio_id=".$studio_id." GROUP BY content_types_id ORDER BY content_types_id ASC";
        $content_types = $con->createCommand($sql)->queryAll();
        
        if (isset($content_types) && !empty($content_types)) {
            foreach ($content_types as $key => $value) {
                $content_id = '0_0_0_'.$value['content_types_id'];
                $res[$cnt]['content_id'] = $content_id;
                $res[$cnt]['name'] = ($value['content_types_id'] == 3) ? "All Multi-part" : "All Single-part";
            
                $cnt++;
            }
        }
        }
        
        $csql = "SELECT f.id AS movie_id, f.name, f.content_types_id, ms.id AS episode_id, ms.is_episode, ms.episode_title, ms.series_number 
        FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id = ms.movie_id AND f.parent_id =0 AND ms.episode_parent_id=0
        ORDER BY f.content_types_id, f.name, ms.is_episode ASC, ms.episode_title ASC, ms.series_number ASC, f.id ASC";
        $content = $con->createCommand($csql)->queryAll();
        
        if (isset($content) && !empty($content)) {
            foreach ($content as $key => $value) {
                if ($value['content_types_id'] == 3) {
                    
                    if (intval($value['is_episode'])) {
                        $data[$value['movie_id']]['season'][$value['series_number']][$value['episode_id']] = $value['episode_title'];
                    } else {
                        $data[$value['movie_id']]['name'] = $value['name'];
                        $data[$value['movie_id']]['content_types_id'] = $value['content_types_id'];
                    }
                } else {
                    $data[$value['movie_id']]['name'] = $value['name'];
                    $data[$value['movie_id']]['content_types_id'] = $value['content_types_id'];
                }
            }
        }
        
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $content_id = $key.'_0_0_'.$value['content_types_id'];
                $res[$cnt]['content_id'] = $content_id;
                $res[$cnt]['name'] = $value['name'];
                
                if (isset($value['season']) && !empty($value['season'])) {
                    foreach ($value['season'] as $key1 => $season) {
                        if (intval($key1)) {
                        $cnt++;
                        $content_id = $key.'_'.$key1.'_0_'.$value['content_types_id'];
                        $res[$cnt]['content_id'] = $content_id;
                        if ($studio_id != 2471) {
                            $res[$cnt]['name'] = $value['name'].': Season-'.$key1;
                        }
                            
                        foreach ($season as $key2 => $episode) {
                            $cnt++;
                            $content_id = $key.'_'.$key1.'_'.$key2.'_'.$value['content_types_id'];
                            $res[$cnt]['content_id'] = $content_id;
                            if ($studio_id == 2471) {
                                $res[$cnt]['name'] = $value['name'].': '.$episode;
                            } else {
                                $res[$cnt]['name'] = $value['name'].': Season-'.$key1.': '.$episode;
                            }
                        }
                    }
                }
                }
                $cnt++;
            }
        }
        
        echo json_encode($res);
        exit;
    }
    
    function actionAddEditFreeContent() {
        if (isset($_POST['data']) && !empty($_POST['data'])) {
            $studio = $this->studio;
            $studio_id = $studio->id;
        
            FreeContent::model()->deleteAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
            
            if (isset($_POST['data']['content']) && !empty($_POST['data']['content'])) {
                foreach ($_POST['data']['content'] as $key => $content) {
                    $data = explode('_', $content);

                    $FreeContent = new FreeContent();
                    $FreeContent->studio_id = $studio_id;
                    $FreeContent->movie_id = $data[0];
                    $FreeContent->season_id = $data[1];
                    $FreeContent->episode_id = $data[2];
                    $FreeContent->content_types_id = $data[3];
                    $FreeContent->save();
                }
            }
            
            if (isset($_POST['data']['is_update']) && intval($_POST['data']['is_update'])) {
                Yii::app()->user->setFlash('success', 'Free Content has been updated successfully');
            } else if (isset($_POST['data']['content']) && !empty($_POST['data']['content']) && intval($_POST['data']['is_update']) == 0) {
                Yii::app()->user->setFlash('success', 'Free Content has been added successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Opps! No content has been selected!');
            }
        } else {
            Yii::app()->user->setFlash('error', 'Opps! No content has been selected!');
        }
        
        $url = $this->createUrl('monetization/freeContent');
        $this->redirect($url);
    }
     function actionPpvBundles() {
        $this->pageTitle = Yii::app()->name . ' | ' . ' Pay-per-view Bundles ';
        $this->headerinfo = 'Pay-per-view Bundles';
        $this->breadcrumbs = array('Monetization', 'Pay-per-view Bundles');
        $studio = $this->studio;
        $studio_id = $studio->id;
        $data = $all_multi = $all_single = $multi_cat = $single_cat = array();
        
        $isPPVbundle = Yii::app()->general->monetizationMenuSetting($studio_id);
        if(isset($isPPVbundle['menu']) && !empty($isPPVbundle['menu']) && ($isPPVbundle['menu'] & 64)) {
        
            $ppvPlans = PpvPlans::model()->findAllByAttributes(array('studio_id' => $studio_id, 'is_deleted' => 0, 'status' => 1,'is_advance_purchase'=>2));
        
            
            if (isset($ppvPlans) && !empty($ppvPlans)) {
                $con = Yii::app()->db;
                foreach ($ppvPlans as $key => $value) {
                $ppv_plan_id=$data[$key]['id']=$value->id;
                $csql = "select ppvp.price_for_unsubscribed,ppvp.price_for_subscribed from ppv_pricing as ppvp where ppvp.ppv_plan_id=".$ppv_plan_id;
                $content = $con->createCommand($csql)->queryAll();
                $data[$key]['title']=$value->title;
                $data[$key]['id']=$value->id;
                $data[$key]['subscribe_price']=$content[0]['price_for_subscribed'];
                $data[$key]['unsubscribe_price']=$content[0]['price_for_unsubscribed'];
                $fnamesql = "select f.name,pd.content_id from ppv_advance_content as pd LEFT JOIN films as f on f.id=pd.content_id where pd.ppv_plan_id=".$ppv_plan_id;
                $contentname = $con->createCommand($fnamesql)->queryAll();
                 $data[$key]['contentname']=$contentname;
                $timeframesql = "select pt.price_for_unsubscribed,pt.price_for_subscribed,pt.validity_days,pt.currency_id from ppv_timeframe as pt where pt.ppv_plan_id=".$ppv_plan_id." AND status=1";
                $timeframename = $con->createCommand($timeframesql)->queryAll();
                $data[$key]['timeframename']=$timeframename;
                }
            } 
           
        } 
        $this->render('ppvbundles', array('studio' => $studio, 'data' => $data));
    }
      function actionAddEditCategoryPPVbundles() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $ppvPlans = $ppvPricing = array();
        if (isset($_REQUEST['id_ppv']) && !empty($_REQUEST['id_ppv'])) {
           $id = $_REQUEST['id_ppv'];
           $content = self::getPpvContents($id);
            $ppvPlans = PpvPlans::model()->findByAttributes(array('id' => $id, 'studio_id' => $studio_id, 'is_deleted' => 0, 'is_advance_purchase' => 2));
           
            if (!empty($ppvPlans)) {
                $con = Yii::app()->db;
                if($ppvPlans->is_timeframe==1)
                {
                $sqltimeframe = "SELECT id,price_for_subscribed,price_for_unsubscribed,validity_days,ppv_timeframelable_id  FROM ppv_timeframe WHERE ppv_plan_id=".$id." AND status=1";
                $pricingtimeframe = $con->createCommand($sqltimeframe)->queryAll();
                }
                
            }
        }
        $currencySymbol = Currency::model()->findByAttributes(array('id' => $studio->default_currency_id));
        $symbolofcurrency=$currencySymbol->symbol;
        //$ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
        //$currency = Currency::model()->findAll(array('order' => 'code'));
        $currency = Yii::app()->common->getStudioCurrency();
        $this->render('categoryppvbundles', array('type' => $type, 'advPlans' => $ppvPlans, 'pricing' => $pricingtimeframe, 'pricingtimeframe' => $pricingtimeframe, 'currency' => $currency, 'studio' => $studio,'content'=>$content,'symbolofcurrency'=>$symbolofcurrency, 'policyStatus' => PolicyRules::model()->getConfigRule(), 'isPurchased'=>Yii::app()->policyrule->chkPPVSubscription($id)));
    }
    
      function actionaddppvbundles() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = Yii::app()->user->id;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                $con = Yii::app()->db;
                $sql_usr = "UPDATE ppv_pricing SET status=0 WHERE ppv_plan_id =" . $_REQUEST['data']['id'];
                $con->createCommand($sql_usr)->execute();
                $ppvModel = PpvPlans::model()->findByPk($_REQUEST['data']['id']);
                $ppvModel->last_updated_by = $user_id;
                $ppvModel->last_updated_date = new CDbExpression("NOW()");
            } else {
                $ppvModel = New PpvPlans;
                $ppvModel->created_by = $user_id;
                $ppvModel->created_date = new CDbExpression("NOW()");
            }
            //Save plan
            $ppvModel->studio_id = $studio_id;
            $ppvModel->user_id = $user_id;
            $ppvModel->content_types_id = 0;
            $ppvModel->status = 1;
            $ppvModel->is_advance_purchase = $_REQUEST['data']['is_advance_purchase'];
            $ppvModel->is_all = $_REQUEST['data']['is_all'];
            $ppvModel->start_date = new CDbExpression("NOW()");
            if(isset($_REQUEST['data']['settimeframe']) && ($_REQUEST['data']['settimeframe']==1)){
            $ppvModel->is_timeframe = 1;
            }
            //$ppvModel->expiry_date = date('Y-m-d', strtotime($_REQUEST['data']['expiry_date']));
            if (isset($_REQUEST['data']['title']) && trim($_REQUEST['data']['title'])) {
                $ppvModel->title = $_REQUEST['data']['title'];
                $ppvModel->tag = str_replace(" ", "_", strtolower($_REQUEST['data']['title']));
            }
            $isPurchased = Yii::app()->policyrule->chkUesrSubscription($_REQUEST['data']['id']);
            if (!empty($_POST['data']['rule']) || $this->isAllowResetPolicy == true) {
                if (!$isPurchased) {
                    $ppvModel->rules_id = !empty($_POST['data']['rule']) ? $_POST['data']['rule'] : 0;
                }
            }
            $ppvModel->save();
            $ppv_plan_id = $ppvModel->id;
            if(isset($_REQUEST['data']['settimeframe']) && ($_REQUEST['data']['settimeframe']==1)){
            $countdata=count($_REQUEST['data']['days']);
            $con = Yii::app()->db;
             for($i=0;$i<$countdata;$i++){
             $days=$_REQUEST['data']['days'][$i]; 
            $sqltimeframecount = "SELECT count(id) as countrows FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
            $pricingtimeframecount = $con->createCommand($sqltimeframecount)->queryRow();
            if($pricingtimeframecount['countrows']==0){
             $ppvPricinglableModel = New PpvTimeframeLable;
             //Save price in ppv price lable table
                   $ppvPricinglableModel->studio_id = $studio_id;
                   $ppvPricinglableModel->days = $days;
                   $ppvPricinglableModel->title = $days.' Days';
                   $ppvPricinglableModel->save();     
                   $ppv_timeframelable_id = $ppvPricinglableModel->id; 
                   }
             }     
            for($i=0;$i<$countdata;$i++){
            $ppvtimeframeModel = New Ppvtimeframes;
            $days=$_REQUEST['data']['days'][$i];  
            //$sqltimeframecount = "SELECT count(id) as countrows FROM ppv_timeframe WHERE ppv_plan_id=".$ppv_plan_id." AND validity_days=".$days;
              $sqltimeframelable_id = "SELECT id FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
              $timeframelablearr= $con->createCommand($sqltimeframelable_id)->queryRow();
              $timeframelableidd=$timeframelablearr['id'];
            //$timeframecurrency_id=$_REQUEST['data']['timeframecurrency_id'][$i]; 
            $price_for_unsubscribed=$_REQUEST['data']['price_for_unsubscribed'][$i];
            $price_for_subscribed=$_REQUEST['data']['price_for_subscribed'][$i];
            $ppvtimeframeModel->ppv_plan_id=$ppv_plan_id;
            $ppvtimeframeModel->validity_days=$days;
            $ppvtimeframeModel->ppv_timeframelable_id=$timeframelableidd;
            $ppvtimeframeModel->currency_id=$studio->default_currency_id;
            $ppvtimeframeModel->price_for_unsubscribed=$price_for_unsubscribed;
            $ppvtimeframeModel->price_for_subscribed=$price_for_subscribed;
            $ppvtimeframeModel->save();  
           
            
            }
            }
            //Save price in pricing table
                  /*  $ppvPricingModel = New PpvPricing;
                    if (isset($_REQUEST['data']['pricing_id']) && intval($_REQUEST['data']['pricing_id'])) {
                        $ppvPricingModel = $ppvPricingModel->findByPk($_REQUEST['data']['pricing_id']);
                    }
                    $ppvPricingModel->ppv_plan_id = $ppv_plan_id;
                    $ppvPricingModel->currency_id = $studio->default_currency_id;
                    $ppvPricingModel->price_for_unsubscribed = $_REQUEST['data']['price_nonsubscribe'];
                    $ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_nonsubscribe'];
                    //$ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_subscribe'];
                    $ppvPricingModel->status = 1;
                    $ppvPricingModel->save();*/
            //Save contents
            if (isset($_REQUEST['data']['content']) && !empty($_REQUEST['data']['content'])) {
                //Before saving, 1st delete all content related to plan and then save the content
                PpvAdvanceContent::model()->deleteAll("ppv_plan_id=:ppv_plan_id", array(':ppv_plan_id' => $ppv_plan_id));
                //Save content
                foreach ($_REQUEST['data']['content'] as $key => $value) {
                    $content = New PpvAdvanceContent;
                    $content->ppv_plan_id = $ppv_plan_id;
                    $content->content_id = $value;
                    $content->save();
                }
            }
            
            Yii::app()->user->setFlash('success', 'Ppv Bundles has been Added successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in set of pre-order');
        }

        $url = $this->createUrl('monetization/ppvbundles');
        $this->redirect($url);
    }
    
        function actioneditppvbundles() {
        $studio = $this->studio;
        $studio_id = $studio->id;
        $user_id = Yii::app()->user->id;
        if (isset($_REQUEST['data']) && !empty($_REQUEST['data'])) {
            if (isset($_REQUEST['data']['id']) && !empty($_REQUEST['data']['id'])) {
                $con = Yii::app()->db;
                $ppvModel = PpvPlans::model()->findByPk($_REQUEST['data']['id']);
                $ppvModel->last_updated_by = $user_id;
                $ppvModel->last_updated_date = new CDbExpression("NOW()");
            //Save plan
            if(isset($_REQUEST['data']['settimeframe']) && ($_REQUEST['data']['settimeframe']==1)){
            $ppvModel->is_timeframe = 1;
            }
            else {$ppvModel->is_timeframe = 0;}
            if (isset($_REQUEST['data']['title']) && trim($_REQUEST['data']['title'])) {
                $ppvModel->title = $_REQUEST['data']['title'];
                $ppvModel->tag = str_replace(" ", "_", strtolower($_REQUEST['data']['title']));
            }
            $isPurchased = Yii::app()->policyrule->chkUesrSubscription($_REQUEST['data']['id']);
            if (!empty($_POST['data']['rule']) || $this->isAllowResetPolicy == true) {
                    if (!$isPurchased) {
                        $ppvModel->rules_id = !empty($_POST['data']['rule']) ? $_POST['data']['rule'] : 0;
                }
            }
            $ppvModel->save();
            $ppv_plan_id = $ppvModel->id;
             } 
            if(!isset($_REQUEST['data']['settimeframe']) && ($_REQUEST['data']['settimeframe']!=1)){
              $sqltimeframe_idupdate = "update ppv_timeframe set status=0 WHERE ppv_plan_id=".$ppv_plan_id;
              $pricingtimeframeid = $con->createCommand($sqltimeframe_idupdate)->execute();
            }
            if(isset($_REQUEST['data']['settimeframe']) && ($_REQUEST['data']['settimeframe']==1)){
             $countdata=count($_REQUEST['data']['days']);
             for($i=0;$i<$countdata;$i++){
             $days=$_REQUEST['data']['days'][$i]; 
             $sqltimeframecount = "SELECT count(id) as countrows FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
             $pricingtimeframecount = $con->createCommand($sqltimeframecount)->queryRow();
             if($pricingtimeframecount['countrows']==0){
                   $ppvPricinglableModel = New PpvTimeframeLable;
                   $ppvPricinglableModel->studio_id = $studio_id;
                   $ppvPricinglableModel->days = $days;
                   $ppvPricinglableModel->title = $days.' Days';
                   $ppvPricinglableModel->save();
             }  
             }
             $countdatauu=count($_REQUEST['data']['daysu']);
              for($i=0;$i<$countdatauu;$i++){
             $days=$_REQUEST['data']['daysu'][$i]; 
             $sqltimeframecount = "SELECT count(id) as countrows FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
             $pricingtimeframecount = $con->createCommand($sqltimeframecount)->queryRow();
             if($pricingtimeframecount['countrows']==0){
                   $ppvPricinglableModel = New PpvTimeframeLable;
                   $ppvPricinglableModel->studio_id = $studio_id;
                   $ppvPricinglableModel->days = $days;
                   $ppvPricinglableModel->title = $days.' Days';
                   $ppvPricinglableModel->save();
             }  
             }     
              $countdata=count($_REQUEST['data']['days']);
              $sqltimeframe_idupdate = "update ppv_timeframe set status=0 WHERE ppv_plan_id=".$ppv_plan_id;
              $pricingtimeframeid = $con->createCommand($sqltimeframe_idupdate)->execute();
              if(isset($_REQUEST['data']['ppv_timeframe_id'])&& $_REQUEST['data']['ppv_timeframe_id']!=''){
              for($i=0;$i<$countdata;$i++){
              $days=$_REQUEST['data']['days'][$i]; 
              $price_for_unsubscribed=$_REQUEST['data']['price_for_unsubscribed'][$i];
              $price_for_subscribed=$_REQUEST['data']['price_for_subscribed'][$i];
              $ppv_timeframe_iddd=$_REQUEST['data']['ppv_timeframe_id'][$i];
              $ppvtimeframeModel = New Ppvtimeframes;
              $ppvtimeframeModel = $ppvtimeframeModel->findByPk($ppv_timeframe_iddd);
              $sqltimeframelable_id = "SELECT id FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
              $timeframelablearr= $con->createCommand($sqltimeframelable_id)->queryRow();
              $timeframelableidd=$timeframelablearr['id'];
              $ppvtimeframeModel->ppv_plan_id=$ppv_plan_id;
              $ppvtimeframeModel->validity_days=$days;
              $ppvtimeframeModel->status=1;
              $ppvtimeframeModel->ppv_timeframelable_id=$timeframelableidd;
              $ppvtimeframeModel->currency_id=$studio->default_currency_id;
              $ppvtimeframeModel->price_for_unsubscribed=$price_for_unsubscribed;
              $ppvtimeframeModel->price_for_subscribed=$price_for_subscribed;
              $ppvtimeframeModel->save(); 
            }
              }
              $countdatau=count($_REQUEST['data']['update']);
            for($i=0;$i<$countdatau;$i++){
            $days=$_REQUEST['data']['daysu'][$i]; 
            $sqltimeframecount = "SELECT count(id) as countrows FROM ppv_timeframe WHERE ppv_plan_id=".$ppv_plan_id." AND validity_days=".$days." AND status=1";
            $pricingtimeframecount = $con->createCommand($sqltimeframecount)->queryRow();
            if($pricingtimeframecount['countrows']==0){
              $price_for_unsubscribed=$_REQUEST['data']['price_for_unsubscribedu'][$i];
              $price_for_subscribed=$_REQUEST['data']['price_for_subscribedu'][$i];
              $ppv_timeframe_iddd=$_REQUEST['data']['ppv_timeframe_id'][$i];
              $sqltimeframelable_id = "SELECT id FROM ppv_timeframe_lable WHERE studio_id=".$studio_id." AND days=".$days;
              $timeframelablearr= $con->createCommand($sqltimeframelable_id)->queryRow();
              $timeframelableidd=$timeframelablearr['id'];
              $ppvtimeframeModel = New Ppvtimeframes;
              $ppvtimeframeModel->ppv_plan_id=$ppv_plan_id;
              $ppvtimeframeModel->validity_days=$days;
              $ppvtimeframeModel->status=1;
              $ppvtimeframeModel->ppv_timeframelable_id=$timeframelableidd;
              $ppvtimeframeModel->currency_id=$studio->default_currency_id;
              $ppvtimeframeModel->price_for_unsubscribed=$price_for_unsubscribed;
              $ppvtimeframeModel->price_for_subscribed=$price_for_subscribed;
              $ppvtimeframeModel->save();   
            }
            }
              }
            //Save price in pricing table
                   /* $ppvPricingModel = New PpvPricing;
                    $ppvPricingModel = $ppvPricingModel->findByAttributes(array('ppv_plan_id' => $ppv_plan_id));
                    $ppvPricingModel->ppv_plan_id = $ppv_plan_id;
                    $ppvPricingModel->currency_id = $studio->default_currency_id;
                    $ppvPricingModel->price_for_unsubscribed = $_REQUEST['data']['price_nonsubscribe'];
                    $ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_nonsubscribe'];
                    //$ppvPricingModel->price_for_subscribed = $_REQUEST['data']['price_subscribe'];
                    $ppvPricingModel->status = 1;
                    $ppvPricingModel->save();*/
            //Save contents
            if (isset($_REQUEST['data']['content']) && !empty($_REQUEST['data']['content'])) {
                //Before saving, 1st delete all content related to plan and then save the content
                PpvAdvanceContent::model()->deleteAll("ppv_plan_id=:ppv_plan_id", array(':ppv_plan_id' => $ppv_plan_id));
                //Save content
                foreach ($_REQUEST['data']['content'] as $key => $value) {
                    $content = New PpvAdvanceContent;
                    $content->ppv_plan_id = $ppv_plan_id;
                    $content->content_id = $value;
                    $content->save();
                }
            }
            
            Yii::app()->user->setFlash('success', 'Ppv Bundles has been updated successfully');
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Error in set of pre-order');
        }
        $url = $this->createUrl('monetization/ppvbundles');
        $this->redirect($url);
    }
     function actionBundleContents() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $csql ="SELECT DISTINCT f.id AS content_id, f.name FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id=ms.movie_id AND ((ms.is_episode=0 AND ms.is_converted=1) OR (ms.is_episode=0 AND ms.is_converted=0 AND f.content_types_id=3) OR (ms.is_episode=0 AND ms.is_converted=0 AND f.content_types_id=4)) ORDER BY f.content_types_id, f.name ASC";
        //$csql="SELECT f.id AS content_id, f.name FROM films f WHERE f.studio_id=".$studio_id." ORDER BY f.content_types_id, f.name ASC";
        $content = $con->createCommand($csql)->queryAll();
        echo json_encode($content);
        exit;
    }
    
    function getPpvContents($id) {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        
        $csql = 'SELECT f.id AS content_id, replace(f.name, "\'", "u0027") as name FROM films f LEFT JOIN ppv_advance_content as pc on pc.content_id=f.id WHERE f.studio_id='.$studio_id.' AND pc.ppv_plan_id='.$id.' ORDER BY f.content_types_id, f.name ASC';
        $content = $con->createCommand($csql)->queryAll();
        return $content;
        
    }
    
    function getSubscriptionBundlesContents($id) {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $csql = 'SELECT f.id AS content_id, replace(f.name, "\'", "u0027") as name FROM films f LEFT JOIN subscriptionbundles_content as pc on pc.content_id=f.id WHERE f.studio_id='.$studio_id.' AND pc.subscriptionbundles_plan_id='.$id.' ORDER BY f.content_types_id, f.name ASC';
        //$csql = "SELECT f.id AS content_id, f.name FROM films f LEFT JOIN subscriptionbundles_content as pc on pc.content_id=f.id WHERE f.studio_id=".$studio_id." AND pc.subscriptionbundles_plan_id=".$id." ORDER BY f.content_types_id, f.name ASC";
        $content = $con->createCommand($csql)->queryAll();
        return $content;
        
    }
    
    function actionDeletePpvBundle() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $deletedid=$_REQUEST['deleteid'];
        $csql = "UPDATE ppv_plans set status=0,is_deleted=1 where id=".$deletedid." AND studio_id=".$studio_id;
        $deleteppv = $con->createCommand($csql)->execute();
        $csqlprice = "UPDATE ppv_pricing set status=0 where ppv_plan_id=".$deletedid;
        $deleteppvprice = $con->createCommand($csqlprice)->execute();
        $csqlppvtimeframe = "UPDATE ppv_timeframe set status=0 where ppv_plan_id=".$deletedid;
        $deleteppv = $con->createCommand($csqlppvtimeframe)->execute();
        Yii::app()->user->setFlash('success', 'Ppv Bundles has been deleted successfully');
        echo 1;
    }
    //View subscriptionF
    public function actionViewsubscriptionsContents(){
      $this->layout = false;
        $res = '';
        $language_id = $this->language_id;
        $studio = $this->studio;
        $studio_id = $studio->id;
       
        if(isset($_REQUEST['subscription_plan_id']) && !empty($_REQUEST['subscription_plan_id'])){
           $id = $_REQUEST['subscription_plan_id'];
           $content = self::getSubscriptionBundlesContents($id);
           $plan_name=$_REQUEST['plan_name'];
        $this->render('viewbundlescontents', array('content'=>$content,'studio' => $studio,'plan_id'=>$_REQUEST['subscription_plan_id'],'plan_name'=>$plan_name));
  }
  else{
      $plan_id=$_REQUEST['plan_id'];
       //Before saving, 1st delete all content related to plan and then save the content
                SubscriptionBundlesContent::model()->deleteAll("subscriptionbundles_plan_id=:subscriptionbundles_plan_id", array(':subscriptionbundles_plan_id' => $plan_id));
                //Save content
                foreach ($_REQUEST['data']['content'] as $key => $value) {
                    $content = New SubscriptionBundlesContent;
                    $content->subscriptionbundles_plan_id = $plan_id;
                    $content->content_id = $value;
                    $content->save();
                }
                $suc_msg = 'Subscription Bundles Content has updated successfully';
                 Yii::app()->user->setFlash('success', $suc_msg);
        $url = $this->createUrl('monetization/subscriptions');
        $this->redirect($url);   
  }
        }
 /*
 * Author:Prangya
 *#7217: Device_management code for adding deleting device details.
 */
    public function actiondeviceName(){
            $user_id = $_REQUEST['user_id']; 
            $studio_id = Yii::app()->user->studio_id;              
            //cancel devices 
            if(isset($_POST['choice']) && $_POST['choice']=='cancel_device'){
                 if (isset($_POST['id']) && $_POST['id'] > 0) {
                    $device = Device::model()->findByPk($_POST['id']);
                    $device->flag=0;
                    $device->save();
                 }
            //delete devices     
            }else if(isset($_POST['choice']) && $_POST['choice']=='delete_device'){ 
                 if (isset($_POST['id']) && $_POST['id'] > 0) {
                    $device = Device::model()->findByPk($_POST['id']);
                    $device->flag=9;
                    $device->save();
					//Push Notification 
					$this->deleteDevicePushNotification($device);			
                 }
            }
            $command = Yii::app()->db->createCommand()
                      ->select('*')
                      ->from('device_management')                    
                      ->where('studio_id =:studio_id AND user_id=:user_id AND flag=0' ,array(':studio_id' => $studio_id,':user_id' => $user_id))
                      ->order('id DESC');
             $devicearray = $command->queryAll();                   
             //get list of requested delete devices name
             //$requestedDeleteDevices = Device::model()->findAll("studio_id =:studio_id AND user_id=:user_id AND flag=1", array(':studio_id' => $studio_id, ':user_id' => $user_id)); 
             $this->renderPartial('devicemanagement', array('devicearray'=>$devicearray,'requestedDeleteDevices'=>@$requestedDeleteDevices));
             exit;
        }
	/**
	 * deleteDevicePushNotification is used to send notification at the time of delete device
	 * @param type $device
	 * @Author<prakash>
	 * @return boolean
	 */	
	 public function deleteDevicePushNotification($device){		 
		if($device){
			if(!empty($device->google_id)){
				$reg_ids=array();
				array_push($reg_ids,$device->google_id);
				$push = new Push();
				$title=$device->user_id;
				$message=CHtml::encode($device->device_info)." Device Removed Successfully";				
				$push->setTitle($title);
				$push->setMessage($message);
				//Android Devices
				if($device->device_type==1){
					$android_json_data = $push->getPushAndroid();
					$android_response = $push->sendMultiple($reg_ids, $android_json_data);
				//IOS devices
				}else if($device->device_type==2){
					$ios_json_data = $push->getPushIos();
					$ios_response = $push->sendMultipleNotify($reg_ids, $ios_json_data);
				}
			 }
		}
		return true;		
	 }	
     public function actionAddDevicelist() {
        $user_id = $_REQUEST['user_id'];
        $device_name = $_REQUEST['devicename'];
        $studio_id = Yii::app()->common->getStudioId();
        $created_by=SdkUser::model()->findByAttributes(array('studio_id' => $studio_id,'is_studio_admin' => 1));
        //check same device id is exist or not
        $chkDevice=Device::model()->findByAttributes(array('studio_id' => $studio_id,'user_id' =>$user_id,'device'=>$device_name));
       if($chkDevice){
           if($chkDevice->flag==9){              
               $chkDevice->flag=0;
               $chkDevice->save();
           }
       }else{
            $devicelist = new Device();
            $devicelist->user_id = $user_id;
            $devicelist->studio_id = $studio_id;
            $devicelist->device = $device_name;
            $devicelist->created_by=$created_by->id;
            $devicelist->created_date=date('Y-m-d H:i:s');
            $devicelist->save();
       } 
    } 
	
	public function actionisIntegratePaymentgateway(){
        $gateway=array();
        $gateway=$_POST['data'];
        parse_str($_POST['data'], $gateway);
        $gateway['data']['amount'] = '1.00';
        $gateway['data']['studio_id'] = Yii::app()->user->studio_id;
        $gateway['data']['currency_code'] = 'USD';
        $timeparts = explode(" ", microtime());
        $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
        $reference_number = explode('.', $currenttime);
        $gateway['data']['reference_number'] = $reference_number[0];
        $gateway['data']['email'] = Yii::app()->user->email;
        $gateway['data']['shopper_reference'] = Yii::app()->user->id . $reference_number[0];
        if($gateway['data']['short_code']=='adyen'){
        
        $gateway['data']['resURL'] = Yii::app()->getBaseUrl(true) . "/monetization/adyenPayment";
        $shopper_country = $_SESSION['country_name'];
        $_SESSION['adyen_data']['data']=$gateway;
        $lang_code = isset($this->language_code) && trim($this->language_code) ? $this->language_code : Yii::app()->common->getLanguageCode($shopper_country);
        $gateway['data']['shopper_locale'] = trim($lang_code) == 'en' ? 'en_GB' : $lang_code;
        }else if($gateway['data']['short_code']=='sbw'){
            $_SESSION['sbw_data']['data']=$gateway; 
        }else if($gateway['data']['short_code']=='sisp'){
            //card details
            $gateway['data']['card_name']=$_POST['card_name'];
            $gateway['data']['card_number']=$_POST['card_number'];
            $gateway['data']['exp_month']=$_POST['exp_month'];
            $gateway['data']['exp_year']=$_POST['exp_year'];
            $gateway['data']['security_code']=$_POST['cvv'];
            $gateway['data']['iso_num']='USD';
            $gateway['data']['merchantRef']=$reference_number[0];
            $gateway['data']['merchantSession']=Yii::app()->common->generateUniqNumber();
        }
         $data[0] = (object) $gateway['data'];
        $this->setPaymentGatwayVariable($data);
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway['data']['short_code']] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
       $res = $payment_gateway::processcard($gateway['data']);
       if($gateway['data']['short_code']=='sbw'){
       $res = json_decode($res, true);
       $res['success_url'] = Yii::app()->getBaseUrl(true) . "/monetization/sbwPayment";
       }  
       if($gateway['data']['short_code']=='sisp'){
         $_SESSION['sisp_data']['data']=$gateway; 
         $res['urlMerchantResponse'] = Yii::app()->getBaseUrl(true) . "/monetization/sispPayment";  
       }
       echo json_encode($res);exit;   
    }
    function actionadyenPayment() {
        $data=array();
        if (isset($_REQUEST['authResult']) && (trim(strtoupper($_REQUEST['authResult'])) == 'AUTHORISED')) {
            $is_success = 1;
        }else if(isset($_REQUEST['authResult']) && (trim(strtoupper($_REQUEST['authResult'])) == 'PENDING') && isset($_REQUEST['merchantSig']) && trim($_REQUEST['merchantSig']) == trim($data["merchantSig"])){
            $is_success = 1;
        }
        if(isset($is_success)&& trim($is_success== 1)){
            $data=$_SESSION['adyen_data']['data'];
            $data=$data['data'];
            //print'<pre>';print_r($data);exit;
            $studio_id = Yii::app()->user->studio_id;
             $StudioPaymentGatewaysModel = New StudioPaymentGateways;
                $StudioPaymentGatewaysModel->studio_id = $studio_id;
                $StudioPaymentGatewaysModel->gateway_id = $data['gateway_id'];
                $StudioPaymentGatewaysModel->short_code = (isset($data['payment_type']) && trim($data['payment_type']) == 'manual') ? 'manual' : trim($data['short_code']);
                $StudioPaymentGatewaysModel->api_username = (isset($data['api_username']) && trim($data['api_username'])) ? trim($data['api_username']) : '';
                $StudioPaymentGatewaysModel->api_password = (isset($data['api_password']) && trim($data['api_password'])) ? trim($data['api_password']) : '';
                $StudioPaymentGatewaysModel->api_signature = (isset($data['api_signature']) && trim($data['api_signature'])) ? trim($data['api_signature']) : '';
                $StudioPaymentGatewaysModel->non_3d_secure = (isset($data['non_3d_secure']) && trim($data['non_3d_secure'])) ? trim($data['non_3d_secure']) : '';
                $StudioPaymentGatewaysModel->api_mode = (isset($data['api_mode']) && trim($data['api_mode'])) ? trim($data['api_mode']) : 'live';
                $StudioPaymentGatewaysModel->status = 1;
                $StudioPaymentGatewaysModel->is_primary = 1;
                $StudioPaymentGatewaysModel->is_pci_compliance = (isset($data['is_pci_compliance']) && intval($data['is_pci_compliance'])) ? intval($data['is_pci_compliance']) : 0;
                $StudioPaymentGatewaysModel->api_addons = isset($data['api_addons']) && trim($data['api_addons']) ? $data['api_addons'] : '';
                $StudioPaymentGatewaysModel->created_by = Yii::app()->user->id;
                $StudioPaymentGatewaysModel->created_date = new CDbExpression("NOW()");
                $StudioPaymentGatewaysModel->ip = $ip_address;
                $StudioPaymentGatewaysModel->save();
            $gateway_id = PaymentGateways::model()->findAllByAttributes(array('id' => $data['gateway_id']));
            $menu = MonetizationMenuSettings::model()->findAllByAttributes(array('studio_id' => $studio_id));

            $res=MonetizationMenuSettings::model()->updateMenuSettings($studio_id,0);
            //print'<pre>'; print_r($res);exit;
         $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');
         exit;
        }else{
          Yii::app()->user->setFlash('error', 'Oops! You have entered invalid API Credential');
          $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');  
        }
        
    }
     function actionsbwPayment() {
         
        $data=array();
        if (isset($_REQUEST) && trim($_REQUEST['result']) == 'success') {
            $gatways =$_SESSION['sbw_data']['data'];
            $gatway[0] = (object) $gatways['data'];
            $this->setPaymentGatwayVariable($gatway);
            $from_data['data'] = Yii::app()->session['redirect_form_data'];
            $from_data['PAYMENT_GATEWAY'] = 'sbw';
            $from_data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID['sbw'];
            $gateway_code = 'sbw';
            $user['profile_id'] = $_REQUEST['bill_id'];
            $user['amount']=1;   
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::processTransaction($user);
           if($resArray['status']=='success') {
               $data=$gatways['data'];
               
                $studio_id = Yii::app()->user->studio_id;
                $StudioPaymentGatewaysModel = New StudioPaymentGateways;
                $StudioPaymentGatewaysModel->studio_id = $studio_id;
                $StudioPaymentGatewaysModel->gateway_id = $data['gateway_id'];
                $StudioPaymentGatewaysModel->short_code = (isset($data['payment_type']) && trim($data['payment_type']) == 'manual') ? 'manual' : trim($data['short_code']);
                $StudioPaymentGatewaysModel->api_username = (isset($data['api_username']) && trim($data['api_username'])) ? trim($data['api_username']) : '';
                $StudioPaymentGatewaysModel->api_password = (isset($data['api_password']) && trim($data['api_password'])) ? trim($data['api_password']) : '';
                $StudioPaymentGatewaysModel->api_signature = (isset($data['api_signature']) && trim($data['api_signature'])) ? trim($data['api_signature']) : '';
                $StudioPaymentGatewaysModel->non_3d_secure = (isset($data['non_3d_secure']) && trim($data['non_3d_secure'])) ? trim($data['non_3d_secure']) : '';
                $StudioPaymentGatewaysModel->api_mode = (isset($data['api_mode']) && trim($data['api_mode'])) ? trim($data['api_mode']) : 'live';
                $StudioPaymentGatewaysModel->status = 1;
                $StudioPaymentGatewaysModel->is_primary = 1;
                $StudioPaymentGatewaysModel->is_pci_compliance = (isset($data['is_pci_compliance']) && intval($data['is_pci_compliance'])) ? intval($data['is_pci_compliance']) : 0;
                $StudioPaymentGatewaysModel->api_addons = isset($data['api_addons']) && trim($data['api_addons']) ? $data['api_addons'] : '';
                $StudioPaymentGatewaysModel->created_by = Yii::app()->user->id;
                $StudioPaymentGatewaysModel->created_date = new CDbExpression("NOW()");
                $StudioPaymentGatewaysModel->ip = $ip_address;
                $StudioPaymentGatewaysModel->save();
                $gateway_id = PaymentGateways::model()->findAllByAttributes(array('id' => $data['gateway_id']));
                $menu = MonetizationMenuSettings::model()->findAllByAttributes(array('studio_id' => $studio_id));
                $editedmenu=$menu[0]['menu'];
                 $gateway_id = PaymentGateways::model()->findAllByAttributes(array('id' => $data['gateway_id']));
                $menu = MonetizationMenuSettings::model()->findAllByAttributes(array('studio_id' => $studio_id));
                $res=MonetizationMenuSettings::model()->updateMenuSettings($studio_id,0);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');
           }
        }else{
          Yii::app()->user->setFlash('error', 'Oops! You have entered invalid API Credential');
          $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');  
        }
        
    }
     function actionsispPayment() {
         //print_r($_REQUEST);exit;
        $data=array();
       if (isset($_REQUEST['messageType']) && trim(strtoupper($_REQUEST['messageType'])) == 8) {
            $data=$_SESSION['sisp_data']['data'];
            $studio_id = Yii::app()->user->studio_id;
             $StudioPaymentGatewaysModel = New StudioPaymentGateways;
                $StudioPaymentGatewaysModel->studio_id = $studio_id;
                $StudioPaymentGatewaysModel->gateway_id = $data['gateway_id'];
                $StudioPaymentGatewaysModel->short_code = (isset($data['payment_type']) && trim($data['payment_type']) == 'manual') ? 'manual' : trim($data['short_code']);
                $StudioPaymentGatewaysModel->api_username = (isset($data['api_username']) && trim($data['api_username'])) ? trim($data['api_username']) : '';
                $StudioPaymentGatewaysModel->api_password = (isset($data['api_password']) && trim($data['api_password'])) ? trim($data['api_password']) : '';
                $StudioPaymentGatewaysModel->api_signature = (isset($data['api_signature']) && trim($data['api_signature'])) ? trim($data['api_signature']) : '';
                $StudioPaymentGatewaysModel->non_3d_secure = (isset($data['non_3d_secure']) && trim($data['non_3d_secure'])) ? trim($data['non_3d_secure']) : '';
                $StudioPaymentGatewaysModel->api_mode = (isset($data['api_mode']) && trim($data['api_mode'])) ? trim($data['api_mode']) : 'live';
                $StudioPaymentGatewaysModel->status = 1;
                $StudioPaymentGatewaysModel->is_primary = 1;
                $StudioPaymentGatewaysModel->is_pci_compliance = (isset($data['is_pci_compliance']) && intval($data['is_pci_compliance'])) ? intval($data['is_pci_compliance']) : 0;
                $StudioPaymentGatewaysModel->api_addons = isset($data['api_addons']) && trim($data['api_addons']) ? $data['api_addons'] : '';
                $StudioPaymentGatewaysModel->created_by = Yii::app()->user->id;
                $StudioPaymentGatewaysModel->created_date = new CDbExpression("NOW()");
                $StudioPaymentGatewaysModel->ip = $ip_address;
                $StudioPaymentGatewaysModel->save();
                $gateway_id = PaymentGateways::model()->findAllByAttributes(array('id' => $data['gateway_id']));
                $menu = MonetizationMenuSettings::model()->findAllByAttributes(array('studio_id' => $studio_id));
                $res=MonetizationMenuSettings::model()->updateMenuSettings($studio_id,0);
         $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');
         exit;
        }else{
          Yii::app()->user->setFlash('error', 'Oops! You have entered invalid API Credential');
          $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');  
        }
        
    }
   public function actioncreditRule(){
        $this->breadcrumbs = array('Monetization', 'Credits', 'Credit Rules');
        $this->headerinfo = 'Credit Rules';
        $this->pageTitle = Yii::app()->name . ' | Credit Rules';
        $studio_id = Yii::app()->user->studio_id;
        $credit_rules = CreditRules::model()->findAllByAttributes(array('studio_id'=>$studio_id),array('order' => 'status desc'));
       $subscription_details = array();
        foreach($credit_rules as $key => $val){
           $subscription_details[$val->id] = SubscriptionPlans::model()->findByPk($val['plan_id']);
        }
       
        $this->render('credit_rule', array('data'=>$credit_rules,'studio_id'=>$studio_id,'subscription_details'=>$subscription_details));
   }
    public function actiongetcreditPopup(){
        $studio_id = Yii::app()->user->studio_id;
        $credit_rules = array();
        $type = 'add';
        if(isset($_POST['edit'])){
          $credit_rules = CreditRules::model()->findByPk($_POST['id']);
          $type = 'edit';
        }
        $subscription_plans = SubscriptionPlans::model()->findAllByAttributes(array('studio_id'=>$studio_id,'status'=>1));
        $html = $this->renderpartial('credit_rule_popup', array('subscription_plan'=>$subscription_plans,'credit_rule'=>$credit_rules,'type'=>$type));
        echo $html; 
        exit;
    }
    public function actionsavecreditRule(){
        if(isset($_POST['data'])){
            parse_str($_POST['data'],$req_data);
            if(isset($req_data['edit']) && isset($req_data['credit_id'])){
                $pricing_ids = SubscriptionPricing::model()->get_pricing_ids($req_data['plans']);
                $credit_rules = CreditRules::model()->findByPk($req_data['credit_id']);
                $credit_rules->rule_name = $req_data['rule_name'];
                $credit_rules->rule_action = $req_data['credit_action'];
                $credit_rules->credit_value = $req_data['credit_value'];
                $credit_rules->plan_id = $req_data['plans'];
                if($req_data['validity'] == 1){
                    $credit_rules->is_automated = 1;
                    $credit_rules->credit_validity = '';
                }else{
                    $credit_rules->is_automated = 0;
                    $credit_rules->credit_validity = $req_data['validity_period'];
                }                
                $credit_rules->plan_pricing_ids = ','.$pricing_ids['pricing_ids'].',';
                $credit_rules->update();
                Yii::app()->user->setFlash('success', 'Rule Updated successfully');
            }else{
            $studio_id = Yii::app()->user->studio_id;
            $ip_address = Yii::app()->request->getUserHostAddress(); 
            $pricing_ids = SubscriptionPricing::model()->get_pricing_ids($req_data['plans']);
            $credit_rules_model = new CreditRules;
            $credit_rules_model->studio_id = $studio_id;
            $credit_rules_model->rule_name = $req_data['rule_name'];
            $credit_rules_model->rule_action = $req_data['credit_action'];
            $credit_rules_model->credit_value = $req_data['credit_value'];
            $credit_rules_model->plan_id = $req_data['plans'];
            if($req_data['validity'] == 1){
                $credit_rules_model->is_automated = 1;
                $credit_rules_model->credit_validity = '';
            }else{
                $credit_rules_model->is_automated = 0;
                $credit_rules_model->credit_validity = $req_data['validity_period'];
            }
            $credit_rules_model->plan_pricing_ids = ','.$pricing_ids['pricing_ids'].',';
            $credit_rules_model->created_date = new CDbExpression("NOW()");
            $credit_rules_model->save();
            Yii::app()->user->setFlash('success', 'Rule created successfully');
            }
            echo 1;
        }
    }
    public function actiondeletecreditRule(){
        if(isset($_POST['data'])){
            parse_str($_POST['data'],$req_data);
            if(!empty($req_data['data'])){
                foreach($req_data['data'] as $key => $val){
                    CreditRules::model()->deleteByPk(array('id'=>$val,'studio_id'=>$req_data['studio_id']));
                }
            }
            Yii::app()->user->setFlash('success', 'Credit rule deleted successfully');
        }
    }
    public function actioncontentCredit(){
        $this->breadcrumbs = array('Monetization', 'Credits', 'Content Credits');
        $this->headerinfo = 'Credit Categories';
        $this->pageTitle = Yii::app()->name . ' | Credit Categories';
        $studio_id = Yii::app()->user->studio_id;
        $data = MuviCredit::model()->getContentCategoryData($studio_id);
        if(!empty($data)){
            foreach($data as $key => $val){
                if($val['is_allcontent'] == 0){
                        if($val['content_category_id'] != ''){
                            $category_name = ContentCategories::model()->getContent_category_name($val['content_category_id']);
                            $data[$key]['content_category'] = $category_name;
                        }else if($val['content'] != ''){
                             $content_name = Film::model()->getcontentNameFromId($val['content']);
                             $data[$key]['content_category'] = $content_name;
                        }
                        
                }else{
                    $data[$key]['content_category'] = 'All Content';
                }
            }
        }
        $this->render('content_credit', array('studio_id'=>$studio_id,'data'=>$data));       
    }
    public function actiongetcreditcontentPopup(){
         $studio_id = Yii::app()->user->studio_id;
        $credit_content = array();
        $type = 'add';
        $sel_cat = array();
        $sel_content = array();
        if(isset($_POST['edit'])){
          $credit_content = MuviCredit::model()->findByPk($_POST['id']);
         // print_r($credit_content);
          $type = 'edit';
          if($credit_content->content_category_id != ''){
              $available_cat = explode(',',trim($credit_content->content_category_id));
              foreach($available_cat as $key => $val){
                  $sel_cat[] = array('content_id'=>$val, 'name'=>ContentCategories::model()->getContent_category_name($val));
              }
          }else if($credit_content->content != ''){
              $avl_content = explode(',',trim($credit_content->content,','));
              foreach($avl_content as $key => $val){
                  $sel_content[] = array('content_id'=>$val, 'name'=>Film::model()->getcontentNameFromId($val));
              }
          }
        }
        $subscription_plans = SubscriptionPlans::model()->findAllByAttributes(array('studio_id'=>$studio_id,'status'=>1));
        $content_cat = ContentCategories::model()->getCategories($studio_id); 
        $cat = array();
        foreach($content_cat as $key => $val){
           $cat[] = array('content_id' => $key, 'name' => $val); 
        }
        $html = $this->renderpartial('credit_content_popup', array('subscription_plan'=>$subscription_plans,'credit_rule'=>$credit_content,'type'=>$type,'sel_cat'=>$available_cat,'sel_content'=>$sel_content,'content_cat'=>$cat));
        echo $html; 
        exit;       
    }
     public function actioncreditContents() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $csql ="SELECT DISTINCT f.id AS content_id, f.name FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id=ms.movie_id AND ((ms.is_episode=0 AND ms.is_converted=1) OR (ms.is_episode=0 AND ms.is_converted=0 AND f.content_types_id=3) OR (ms.is_episode=0 AND ms.is_converted=0 AND f.content_types_id=4)) ORDER BY f.content_types_id, f.name ASC";
        //$csql="SELECT f.id AS content_id, f.name FROM films f WHERE f.studio_id=".$studio_id." ORDER BY f.content_types_id, f.name ASC";
        $content = $con->createCommand($csql)->queryAll();
        echo json_encode($content);
        exit;
    } 
    public function actioncreditContentCategory(){
        $studio = $this->studio;
        $studio_id = $studio->id;
        $content_cat = ContentCategories::model()->getCategories($studio_id);
        $cat = array();
        foreach($content_cat as $key => $val){
           $cat[] = array('content_id' => $key, 'name' => $val); 
        }
        echo json_encode($cat);
        exit;
    }
    public function actionsaveCreditCategory(){
        $studio = $this->studio;
        $studio_id = $studio->id;
        $ip_address = Yii::app()->request->getUserHostAddress();
        if(isset($_POST['data'])){
            parse_str($_POST['data'],$req_data);
            if($req_data['edit']){
                $muvicredit_model = MuviCredit::model()->findByPk($req_data['cid']);
            }else{
                $muvicredit_model = new MuviCredit();
            }
          //  print_r($req_data);
            $muvicredit_model->studio_id = $studio_id;
            $muvicredit_model->name = $req_data['category_name'];
            $muvicredit_model->is_allcontent = 0;
            $muvicredit_model->content_category_id = '';
             $muvicredit_model->content = '';
            if($req_data['credit_action'] == 3){
                 $muvicredit_model->is_allcontent = 1;
            }else{
                $muvicredit_model->is_allcontent = 0;
            }
            if($req_data['credit_action'] == 2){
                $cat = implode(',',$req_data['content_category']);
                $muvicredit_model->content_category_id = $cat;
                $available_content = Film::model()->getAvailableContent($cat);
                $muvicredit_model->content = ','.$available_content['content_id'].',';
            }
            if($req_data['credit_action'] == 1){
                $content = implode(',',$req_data['content']);
               $muvicredit_model->content = ','.$content.',';
            }
            $muvicredit_model->credit_value = $req_data['credit_value'];
            $muvicredit_model->created_date = new CDbExpression("NOW()");
            $muvicredit_model->created_by = $studio_id;
            $muvicredit_model->status = 1;
            $muvicredit_model->ip = $ip_address;
            if($req_data['edit']){
                $muvicredit_model->update();
                Yii::app()->user->setFlash('success', 'Categry updated successfully');
            }else{
                $muvicredit_model->save();
                Yii::app()->user->setFlash('success', 'Categry created successfully');
            }
            
        }
    }
    public function actiondeleteCreditCategory(){
        if(isset($_POST['data'])){
            parse_str($_POST['data'],$req_data);
            foreach($req_data['data'] as $key => $val){
                MuviCredit::model()->deleteByPk(array('id'=>$val,'studio_id'=>$req_data['studio_id']));
            }
            Yii::app()->user->setFlash('success', 'Categry Deleted successfully');
        }
    }
    public function actiongetppvcategorypricedetails(){
        $data=$_POST;
        $con = Yii::app()->db;
        $ppvPricing = array();
       $sql = "SELECT pps.title as title,pp.*,pps.description FROM ppv_pricing pp,ppv_plans pps WHERE pp.ppv_plan_id=pps.id AND pp.ppv_plan_id=".$data['plan_id']." AND pp.status=1";
        $ppvPricing = $con->createCommand($sql)->queryALL();
        for($i=0;$i<count($ppvPricing);$i++){
       $sql_currency="SELECT symbol,code FROM currency WHERE id=".$ppvPricing[$i]['currency_id'];
        $ppvPricing[$i]['symbol'] = $con->createCommand($sql_currency)->queryROW();}
        if($ppvPricing[0]['description']==NULL){
           $ppvPricing[0]['description']=''; 
        }
         echo json_encode($ppvPricing);exit;   
    
}
    public function actiondisableEnableCreditRule(){
        if(isset($_POST['id'])){
            $credit_rule = CreditRules::model()->findByPk($_POST['id']);
            switch($_POST['type']){
                case 'disable':
                    $credit_rule->status = 0;
                    Yii::app()->user->setFlash('success', 'Credit rule has disabled successfully');
                break;
                case 'enable':
                    $credit_rule->status = 1;
                    Yii::app()->user->setFlash('success', 'Credit rule has enabled successfully');                    
                break;
            }
            $credit_rule->update();
            
        }
    }
    public function actiondisableEnableCreditCategory(){
        if(isset($_POST['id'])){
            $credit_category = MuviCredit::model()->findByPk($_POST['id']);
            switch($_POST['type']){
                case 'disable':
                    $credit_category->status = 0;
                    Yii::app()->user->setFlash('success', 'Credit category has disabled successfully');
                break;
                case 'enable':
                    $credit_category->status = 1;
                    Yii::app()->user->setFlash('success', 'Credit category has enabled successfully');                    
                break;
            }
            $credit_category->update();
            
        }       
	}	
    public function actioncheckPlanExist(){
        $muvicredit_model = new MuviCredit();
        $type = $_POST['type'];
        switch($type){
            case 'edit':
                $plan_exist_check = CreditRules::model()->checkplanexist($_POST['action'],$_POST['plan_id'],$_POST['rule_id']);
                if(!empty($plan_exist_check)){
                    echo 0;
                }else{
                    echo 1;
                }
            break;
            case 'add':
                 $plan_exist_check = CreditRules::model()->checkplanexistForAdd($_POST['action'],$_POST['plan_id']);
                 if(!empty($plan_exist_check)){
                    echo 0;
                }else{
                    echo 1;
                }               
            break;
        }
    }
     public function actionRemoveTestUsersBeforeIntegration() {
    $cond = ''; 
     $dbcon = Yii::app()->db;
     $studio_id = Yii::app()->common->getStudiosId();
     
      $sql = "SELECT GROUP_CONCAT(user_id) AS user_id FROM `transactions` WHERE `transactions`.`studio_id` IN ({$studio_id}) AND `transactions`.`payment_method` IN ('testgateway')";
      $data =$dbcon->createCommand($sql)->queryAll();
        if ((isset($studio_id) && !empty($studio_id)) && (isset($data[0]['user_id']) && !empty($data[0]['user_id']))) {
                $sql_trn = "DELETE FROM `transactions` WHERE `transactions`.`user_id` IN ({$data[0]['user_id']})";
                $dbcon->createCommand($sql_trn)->execute();
                 
                $sql_ci = "DELETE FROM `sdk_card_infos` WHERE `sdk_card_infos`.`user_id` IN ({$data[0]['user_id']})";
                $dbcon->createCommand($sql_ci)->execute();
                
                 $sql_ppv = "DELETE FROM `ppv_subscriptions` WHERE `ppv_subscriptions`.`user_id` IN ({$data[0]['user_id']})";
                $dbcon->createCommand($sql_ppv)->execute();
                 
                $sql_ppv = "DELETE FROM `monetization_menu_settings` WHERE `studio_id` IN ({$studio_id})";
                $dbcon->createCommand($sql_ppv)->execute(); 
                
                $sql_lh = "DELETE FROM `login_history` WHERE `login_history`.`user_id` IN ({$data[0]['user_id']})";
                $dbcon->createCommand($sql_lh)->execute();
                $sql_po = "SELECT GROUP_CONCAT(id) AS order_id from pg_order where buyer_id IN ({$data[0]['user_id']})";
                $data_po = $dbcon->createCommand($sql_po)->queryRow();
                if (isset($data_po['order_id']) && !empty($data_po['order_id'])) {
                    $data_po['order_id'] = trim($data_po['order_id'], ',');

                    $sql_pod = "DELETE FROM `pg_order_details` WHERE `pg_order_details`.`order_id` IN ({$data_po['order_id']})";
                    $dbcon->createCommand($sql_pod)->execute();

                    $sql_po = "DELETE FROM `pg_order` WHERE `pg_order`.`id` IN ({$data_po['order_id']})";
                    $dbcon->createCommand($sql_po)->execute();
}
             echo json_encode('True');exit;
        }
       else{
           echo json_encode('False');exit;
       }
       
    }
	
	public function actioncheckppvtitle(){
        if(isset($_POST['content_type'])){
            $studio = $this->studio;
            $studio_id = $studio->id;
            $user_id = Yii::app()->user->id; 
            $id= $_POST['data_id'];
            $title = PpvPlans::model()->findAll('studio_id =:studio_id AND user_id =:user_id AND content_types_id =:content_id AND title =:title AND id <> :id', array(':studio_id'=>$studio_id,':user_id'=>$user_id,':content_id'=>$_POST['content_type'],':title'=>$_POST['title'],':id'=>$id));
            if(!empty($title)){
                echo 1;
            }else{
                echo 0;
            }
        }
    }
	function actionSetCCAvenueCredential(){
        if (isset($_POST['data']) && !empty($_POST['data'])) {
             $data[0] = (object)$_POST['data'];
             $studio_id = Yii::app()->user->studio_id;
             $ip_address = Yii::app()->request->getUserHostAddress(); 
             $StudioPaymentGatewaysModel = New StudioPaymentGateways;
                $StudioPaymentGatewaysModel->studio_id = $studio_id;
                $StudioPaymentGatewaysModel->gateway_id = $data[0]->gateway_id;
                $StudioPaymentGatewaysModel->short_code = $data[0]->short_code;
                $StudioPaymentGatewaysModel->api_username = $data[0]->api_username;
                $StudioPaymentGatewaysModel->api_password = $data[0]->api_password;
                $StudioPaymentGatewaysModel->api_signature = $data[0]->api_signature;
                $StudioPaymentGatewaysModel->non_3d_secure = '';
                $StudioPaymentGatewaysModel->api_mode = $data[0]->api_mode;
                $StudioPaymentGatewaysModel->status = 1;
                $StudioPaymentGatewaysModel->is_primary = 1;
                $StudioPaymentGatewaysModel->is_pci_compliance = $data[0]->is_pci_compliance;
                $StudioPaymentGatewaysModel->api_addons = '';
                $StudioPaymentGatewaysModel->created_by = Yii::app()->user->id;
                $StudioPaymentGatewaysModel->created_date = new CDbExpression("NOW()");
                $StudioPaymentGatewaysModel->ip = $ip_address;
                $StudioPaymentGatewaysModel->save();
            $gateway_id = PaymentGateways::model()->findAllByAttributes(array('id' => $data[0]->gateway_id));
            $menu = MonetizationMenuSettings::model()->findAllByAttributes(array('studio_id' => $studio_id));
            $res=MonetizationMenuSettings::model()->updateMenuSettings($studio_id,0);
            echo 1;
            // $this->redirect(Yii::app()->getBaseUrl(true) . '/monetization/payment');    
                      
        }else{
            echo 0;
        }
    }
}