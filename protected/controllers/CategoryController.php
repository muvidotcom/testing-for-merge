<?php
class CategoryController extends Controller{
    public $defaultAction = 'ManageCategory';
	public $headerinfo='';
    public $layout = 'admin';
	public $image;
    protected function beforeAction($action){
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin'; 
        if(!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        }else {
            $this->checkPermission();
        }
        return true;
    }
/**
 * @method public manageCategory() Manage Content Categories
 * @author Gayadhar <support@muvi.com>
 * @return HTML 
 */	
    function actionManageCategory() {
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;
        $this->pageTitle = "Muvi | Content Category List";
        $this->breadcrumbs = array('Manage Content', 'Manage Metadata');
        $this->headerinfo = "Manage Metadata";
        $dbcon = Yii::app()->db;
        $language_id = $this->language_id;
        $cc_cmd = $dbcon->createCommand("SELECT * FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id})) ORDER BY IF(parent_id >0, parent_id, id) DESC");
        $data = $cc_cmd->queryAll();
        $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
        $cat_img_option = Yii::app()->custom->getCatImgOption($studio_id);
        $contentform = self::actionGetFormList();//Content Form listing by manas@muvi.com
        $this->render('managecategory', array('data' => $data, 'language_id' => $language_id,'cat_img_size'=>@$cat_img_size,'cat_img_option'=>@$cat_img_option,'content_metadata' =>$contentform,'formid'=>$formid));
    }
    
    function actionAjaxContent() {
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $data = array();
        $cat_img_option = Yii::app()->custom->getCatImgOption($studio_id);
        $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id);
        if (isset($_REQUEST['content_id'])) {
            $id = $_REQUEST['content_id'];
            $permalink = $_REQUEST['permalink'];
            
            $type = 'Edit';

            $data = Yii::app()->db->createCommand()
                       ->select('*')
                       ->from('content_category')
                       ->where("studio_id=" . $studio_id . " AND permalink='" . $permalink . "' AND (language_id=" . $language_id . " OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id=" . $studio_id . " AND permalink='" . $permalink . "' AND language_id=" . $language_id . "))")
                       ->setFetchMode(PDO::FETCH_OBJ)->queryROW();
            $posters = $this->getPoster($id, 'content_category');
	}
        $this->render('ajax_content', array('data' => $data, 'type' => $type, 'parent_id' => $id,'poster'=>@$posters,'cat_img_size'=>@$cat_img_size,'cat_img_option'=>@$cat_img_option));
    }
    
 /**
 * @method public addContentCategory() Manage Content Categories
 * @author Gayadhar <support@muvi.com>
 * @return HTML 
 */	
    function actionAddContentCategory() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        $displayName = trim(@$_REQUEST['displayname']);
        if (@$displayName) {
            $contentModel = new ContentCategories();
                $language_id = $this->language_id;
            $studio_id =$this->studio->id;
            $category_language_id = (isset($_REQUEST['category_language_id']) && trim($_REQUEST['category_language_id'])) ? trim($_REQUEST['category_language_id']) : '';
            if (@$category_language_id != "" && @$category_language_id != $language_id) {
                $cat_id = $_REQUEST['category_id'];
                $cdata = ContentCategories::model()->findByPk($cat_id, array('select' => 'binary_value,permalink'));
                $contentModel->category_name = $displayName;
                $contentModel->parent_id = $cat_id;
                $contentModel->studio_id = $studio_id;
                $contentModel->binary_value = $cdata->binary_value;
                $contentModel->permalink = $cdata->permalink;
                $contentModel->language_id = $language_id;
                $contentModel->created_date = new CDbExpression("NOW()");
                $contentModel->added_by = Yii::app()->user->id;
                $contentModel->ip = $ip_address;
                $contentModel->save();
                Yii::app()->user->setFlash('success', 'Content category updated successfully.');
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
                echo json_encode($arr);
                exit;
            }
                $data = $contentModel->find('(LOWER(category_name)=:display_name) AND studio_id=:studio_id AND language_id=:language_id', array(':display_name' => strtolower($displayName), ':studio_id' => Yii::app()->user->studio_id, ':language_id' => $language_id));

            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry category name already exsists with your studio";
            } else {
                $ip_address = CHttpRequest::getUserHostAddress();
                $con = Yii::app()->db;
                $sql = " SELECT binary_value FROM content_category WHERE studio_id=" . Yii::app()->user->studio_id . " AND parent_id = 0 ORDER BY binary_value ASC";
                //$sql = 'SELECT MAX(binary_value) maxBvalue FROM content_category WHERE studio_id =' . Yii::app()->user->studio_id;
                $bvalue = $con->createCommand($sql)->queryAll();
                foreach ($bvalue AS $key => $val) {
                    if ($val['binary_value'] != pow(2, $key)) {
                        $binary_value = pow(2, $key);
                        break;
                    }
                }
                if (!@$binary_value) {
                    $binary_value = pow(2, @$key + 1);
                }

                // Get Menu id 
                $Menus = Menu::model()->find('studio_id=:studio_id AND position=\'top\'', array(':studio_id' => Yii::app()->user->studio_id));

                //$binary_value = $bvalue[0]['maxBvalue'] ? ($bvalue[0]['maxBvalue'] * 2) : 1;
                
                $contentModel->studio_id = Yii::app()->user->studio_id;
                $permalink = Yii::app()->general->generatePermalink($displayName);
                $contentModel->permalink = $permalink;
                $contentModel->binary_value = $binary_value;
                $contentModel->category_name = $displayName;
                $contentModel->created_date = new CDbExpression("NOW()");
                $contentModel->added_by = Yii::app()->user->id;
                $contentModel->ip = $ip_address;
                $contentModel->save();
                $cat_id = $contentModel->id;
                // Adding to Studio Content Types for menu uses
                $studioContentTypes = new StudioContentType();
                $studioContentTypes->display_name = Yii::app()->common->formatPermalink($displayName);
                $studioContentTypes->content_category_value = $binary_value;
                $studioContentTypes->studio_id = Yii::app()->user->studio_id;
                $studioContentTypes->is_enabled = 1;
                $studioContentTypes->created_at = new CDbExpression("NOW()");
                $studioContentTypes->save();

                /*if(@$Menus->auto_categories == '1'){
                    //Adding to Menu Items table by default
                    $menuItems = new MenuItem();
                    $menuItems->title = $displayName;
                    $menuItems->menu_id = @$Menus->id;
                    $menuItems->permalink = $permalink;
                    $menuItems->value = $binary_value;
                    $menuItems->studio_id = Yii::app()->user->studio_id;
                    $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                    $menuItems->save();*/
                    
                    //Adding data to Url routing table
                    $urlRouting = new UrlRouting();
                    $urlRouting->permalink = $permalink;
					//$issubCat_enabled = Yii::app()->custom->getCatImgOption(Yii::app()->user->studio_id,'subcategory_enabled');
					//if($issubCat_enabled){
					//	$urlRouting->mapped_url = 'media/listsubcategory/binary_value/'.$binary_value;
				//}else{
					$urlRouting->mapped_url = '/media/list/category/' . $cat_id;
                    $urlRouting->studio_id = Yii::app()->user->studio_id;
                    $urlRouting->created_date = new CDbExpression("NOW()");
                    $urlRouting->ip = $ip_address;
                    $urlRouting->save();                    
                //}

                Yii::app()->user->setFlash('success', $_REQUEST['displayname'] . ' added successfully.');
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = @$_REQUEST['img_width'];
                $height = @$_REQUEST['img_height'];
                $cropDimension = array('thumb' => $width.'x'.$height, 'standard' => $width.'x'.$height,'cmsthumb'=>'100x100');
                $object_id = $cat_id;
                $return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, 'content_category', $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],$object_id);
                if($return !=''){
                    ContentCategories::model()->updateByPk($object_id,array('is_poster'=>1));
                }
            }
            echo json_encode($arr);
            exit;
        }
    }
    /**
 * @method public updateContentCategory() Update Content Category Name
 * @author Gayadhar <support@muvi.com>
 * @return HTML 
 */		
    function actionUpdateContentCategory() {
		$arr['succ'] = 0;$arr['msg'] = '';
        if (isset($_REQUEST['category_id']) && $_REQUEST['category_id']) {
            
            $contentModel = new ContentCategories();
                $language_id = $this->language_id;
                $data = $contentModel->find('(category_name=:display_name) AND studio_id=:studio_id AND id!=:id AND language_id=:language_id', array(':display_name' => $_REQUEST['displayname'], ':studio_id' => Yii::app()->user->studio_id, 'id' => $_REQUEST['category_id'], ':language_id' => $language_id));
            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry category name already exist with your studio";
            } else {
                $contentModel = ContentCategories::model()->findByPk($_REQUEST['category_id']);
                $contentModel->category_name = $_REQUEST['displayname'];
                $contentModel->save();
                Yii::app()->user->setFlash('success', $_REQUEST['displayname'] . ' updated successfully.');
				$arr['succ'] = 1 ;$arr['msg'] = 'Success';
            }
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = @$_REQUEST['img_width'];
                $height = @$_REQUEST['img_height'];
                $cropDimension = array('thumb' => $width.'x'.$height, 'standard' => $width.'x'.$height,'cmsthumb'=>'100x100');
                $object_id = $_REQUEST['category_id'];
                $object_type = "content_category";
                $removeposter = $this->removePosters($object_id, $object_type);
                $return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, $object_type, $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],$object_id);
                if($return !=''){
                    ContentCategories::model()->updateByPk($object_id,array('is_poster'=>1));
				}
			}
		}
		echo json_encode($arr);exit;
    }
/**
 * @method public removeContent() Update the content details
 * @return HTML 
 * @author GDR<support@muvi.com>
 */	
    function actionRemoveContentCategory() {
        $arr['succ'] = 0;
        $arr['msg'] = '';
        if (isset($_REQUEST['category_id']) && $_REQUEST['category_id']) {
            $category = ContentCategories::model()->find('id=:category_id AND studio_id =:studio_id', array(':category_id' => $_REQUEST['category_id'], ':studio_id' => Yii::app()->user->studio_id));
            if ($category) {
                // Binary update query
                    $con = Yii::app()->db;
					$fsql = "UPDATE films SET 
						content_category_value = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', content_category_value, ','), ',". $category->id .",', ',')),
						content_subcategory_value='' 
						WHERE FIND_IN_SET('". $category->id ."', content_category_value) AND studio_id=" . Yii::app()->user->studio_id;
                    //$fsql = 'UPDATE films SET content_category_value = content_category_value -  ' . $category->binary_value . ', content_subcategory_value=\'\' WHERE FIND_IN_SET('.$category->binary_value.',content_category_value) AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($fsql)->execute();
                    
                    //Updating subcategory category values
					$subsql = "UPDATE content_subcategory SET 
						category_value = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', category_value, ','), ',". $category->id .",', ','))
						WHERE FIND_IN_SET('". $category->id ."', category_value) AND studio_id=" . Yii::app()->user->studio_id;
                    //$subsql ='UPDATE content_subcategory SET category_value = category_value -  ' . $category->binary_value . ' WHERE ' . $category->binary_value . ' & category_value AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($subsql)->execute();

                    // Delete if Subcategory belongs to no category
                    $delete_sub_sql = 'DELETE FROM content_subcategory WHERE category_value=0 AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($delete_sub_sql)->execute();
					
					//Updating the studio content types table value 
                    $msql = 'UPDATE studio_content_types SET content_category_value = content_category_value -  ' . $category->binary_value . ' WHERE ' . $category->binary_value . ' & content_category_value AND studio_id=' . Yii::app()->user->studio_id;
                    $con->createCommand($msql)->execute();

                        ContentCategories::model()->deleteAll('studio_id = :studio_id AND (id = :id OR parent_id = :parent_id)', array(
                            ':studio_id' => Yii::app()->user->studio_id,
                            ':id' => $_REQUEST['category_id'],
                            ':parent_id' => $_REQUEST['category_id'],
                        ));
                       UrlRouting::model()->deleteAll('studio_id = :studio_id AND permalink = :permalink', array(
                            ':studio_id' => Yii::app()->user->studio_id,
                            ':permalink' => $category->permalink,
                          ));
                    // Update the content categroy table to re-assign the value 
                    //$csql = 'UPDATE content_category SET binary_value = binary_value >> 1 WHERE binary_value >' . $category->binary_value . ' AND studio_id = ' . Yii::app()->user->studio_id;
                    //$con->createCommand($csql)->execute();
                
					//Select from Menu items and remove those from url_routings
					
					$menuItems = MenuItem::model()->findAll('studio_id=:studio_id AND value=:value',array(':studio_id'=>Yii::app()->user->studio_id,':value'=>$category->id));
					if($menuItems){
						foreach ($menuItems AS $key=>$val){
							// Delete from URL Routing
							$con->createCommand("DELETE FROM url_routing WHERE studio_id=".Yii::app()->user->studio_id." AND (permalink='".$val->permalink."' OR mapped_url='/media/list/menu_item_id/".$val->id."')")->execute();
						}
						// Delete From Menu Items
						$con->createCommand('DELETE FROM menu_items WHERE studio_id='.Yii::app()->user->studio_id.' AND value='.$category->id)->execute();
					}
					
					// Update the menu items table to re-assign the value
					/*$menusql = 'UPDATE menu_items SET value = value -  '.$category->binary_value.' WHERE '.$category->binary_value.' & value AND studio_id='.Yii::app()->user->studio_id;
					$con->createCommand($menusql)->execute();*/
					
                $object_id = $_REQUEST['category_id'];
                $object_type = "content_category";
                $removeposter = $this->removePosters($object_id, $object_type);        
                Yii::app()->user->setFlash('success', ' Content Type  Deleted successfully.');
                $this->redirect($this->createUrl('category/manageCategory'));
            } else {
                Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
                $this->redirect($this->createUrl('category/manageCategory'));
            }
        } else {
            Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
            $this->redirect($this->createUrl('category/manageCategory'));
        }
    }
/**
 * @method public removeContentSubcategory() Update the content details
 * @return HTML 
 * @author GDR<support@muvi.com>
 */	
    function actionRemoveContentsubCategory() {
        $arr['succ'] = 0;$arr['msg'] = '';
        if (isset($_REQUEST['subcategory_id']) && $_REQUEST['subcategory_id']) {
            $category = ContentSubcategory::model()->find('id=:category_id AND studio_id =:studio_id', array(':category_id' => $_REQUEST['subcategory_id'], ':studio_id' => Yii::app()->user->studio_id));
            if ($category) {
                // Binary update query
				$con = Yii::app()->db;
				$fsql = "UPDATE films SET 
						content_subcategory_value = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', content_subcategory_value, ','), ',". $category->id .",', ','))
						WHERE FIND_IN_SET('". $category->id ."', content_subcategory_value) AND studio_id=" . Yii::app()->user->studio_id;
                    
				//$fsql = 'UPDATE films SET content_subcategory_value = content_subcategory_value -  ' . $category->subcat_binary_value . ' WHERE ' . $category->subcat_binary_value . ' & content_subcategory_value AND studio_id=' . Yii::app()->user->studio_id;
				$con->createCommand($fsql)->execute();

				ContentSubcategory::model()->deleteByPk($_REQUEST['subcategory_id'], 'studio_id=:studio_id', array(':studio_id' => Yii::app()->user->studio_id));
                    
                // Update the content categroy table to re-assign the value 
                $object_id = $_REQUEST['subcategory_id'];
                $object_type = "content_subcategory";
                $removeposter = $this->removePosters($object_id, $object_type);        
                Yii::app()->user->setFlash('success', ' Content Type  Deleted successfully.');
                $this->redirect($this->createUrl('category/manageSubCategory'));
            } else {
                Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
                $this->redirect($this->createUrl('category/manageSubCategory'));
            }
        } else {
            Yii::app()->user->setFlash('error', ' Oops! Sorry you don\'t have access to delete the category.');
            $this->redirect($this->createUrl('category/manageSubCategory'));
        }
    }
/**
 * 
 */
	function  actionUpdateBinaryValue(){
		$con = Yii::app()->db;
		$data = $con->createCommand('SELECT GROUP_CONCAT(id) as ids,COUNT(studio_id) as cnt,studio_id FROM content_category GROUP BY studio_id')->queryAll();
		foreach($data AS $key =>$val){
			if($val['cnt'] == 1){
				$con->createCommand('UPDATE  content_category SET binary_value=1 WHERE studio_id='.$val['studio_id'])->execute();
			}else{
				$ids = explode(',', $val['ids']);
				for($i=0;$i<$val['cnt'];$i++){
					$sql = "UPDATE content_category SET binary_value = ".  pow(2, $i)."  WHERE studio_id=".$val['studio_id']." AND id=".$ids[$i];
					$con->createCommand($sql)->execute();
				}
			}
		}
		echo 'Updated ';exit;
	}
	public function actionManageSubCategory(){
		$cond = " ";
		$contentType = '';
		$studio_id = Yii::app()->user->studio_id;
		$this->pageTitle = "Muvi | Content Sub Category List";
		$this->breadcrumbs = array('Manage Content','Content Category'=>array('category/manageCategory'), 'Content Sub Category');
		$this->headerinfo = "Content Sub Category";
		$dbcon = Yii::app()->db;
		$language_id = $this->language_id;
		$cc_cmd = $dbcon->createCommand("SELECT * FROM content_subcategory WHERE studio_id={$studio_id}  ORDER BY  id DESC");
		$data = $cc_cmd->queryAll();

		$sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id}";
		$contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();
		$contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
		$cat_img_option = Yii::app()->custom->getCatImgOption($studio_id,'subcategory_poster_option');
        $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id,'subcategory_poster_size');
		$this->render('managesubcategory', array('data' => $data, 'language_id' => $language_id,'cat_img_size'=>@$cat_img_size,'cat_img_option'=>@$cat_img_option,'contentList'=>$contentTypes));
	}
	
	function actionAjaxsubContent() {
        $this->layout = false;
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
		$data = array();
		if (isset($_REQUEST['content_id'])) {
            $id = $_REQUEST['content_id'];
            $type = 'Edit';
           	$data = Yii::app()->db->createCommand()
					   ->select('*')
					   ->from('content_subcategory')
					   ->where("studio_id=" . $studio_id . " AND id=" .$id)
					   ->setFetchMode(PDO::FETCH_OBJ)->queryROW();
			$posters = $this->getPoster($id, 'content_subcategory');
		}
        
        $cat_img_option = Yii::app()->custom->getCatImgOption($studio_id,'subcategory_poster_option');
        $cat_img_size = Yii::app()->custom->getCatImgSize($studio_id,'subcategory_poster_size');
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} ";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();
        $this->render('ajax_subcontent', array('data' => $data,'poster'=>@$posters,'cat_img_size'=>@$cat_img_size,'cat_img_option'=>@$cat_img_option,'category'=>$contenttypeList));
    }
/**
 * @method public addContentCategory() Manage Content Categories
 * @author Gayadhar <support@muvi.com>
 * @return HTML 
 */	
    function actionAddContentSubCategory() {
        $arr['succ'] = 0;$arr['msg'] = '';
        $displayName = trim(@$_REQUEST['displayname']);
        if (@$displayName) {
			$cond =" ";
			foreach ($_REQUEST['category_parent'] as $key => $value) {
				$cat_cond .= " FIND_IN_SET($value,category_value) OR ";
				}
			$cond .=rtrim($cat_cond, 'OR ');
			$contentSubModel = new ContentSubcategory();
            $data = $contentSubModel->find('(LOWER(subcat_name)=:display_name) AND studio_id=:studio_id AND ('.$cond.')', array(':display_name' => strtolower($displayName), ':studio_id' => Yii::app()->user->studio_id));
            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry sub category name already exsists with your studio";
				 echo json_encode($arr);exit;
            } else {
                $ip_address = CHttpRequest::getUserHostAddress();
                $con = Yii::app()->db;
                $sql = " SELECT subcat_binary_value FROM content_subcategory WHERE studio_id=" . Yii::app()->user->studio_id . " ORDER BY subcat_binary_value ASC";
                $bvalue = $con->createCommand($sql)->queryAll();
                
				foreach ($bvalue AS $key => $val) {
                    if ($val['subcat_binary_value'] != pow(2, $key)) {
                        $binary_value = pow(2, $key);
                        break;
                    }
                }
                if (!@$binary_value) {
                    $binary_value = pow(2, @$key + 1);
                }

                $contentSubModel->studio_id = Yii::app()->user->studio_id;
                $permalink = Yii::app()->general->generatePermalink($displayName);
				
				$contentSubModel->permalink = $permalink;
				$contentSubModel->subcat_binary_value = $binary_value;
                $contentSubModel->subcat_name = $displayName;
                $contentSubModel->category_value = implode(',',$_REQUEST['category_parent']);
                $contentSubModel->created_date = new CDbExpression("NOW()");
               
                $contentSubModel->ip = $ip_address;
                $contentSubModel->save();
                $cat_id = $contentSubModel->id;
               
				//Adding data to Url routing table
				$contentCategory = ContentCategories::model()->findall('studio_id=:studio_id AND id IN ('.implode(',', $_REQUEST['category_parent']).')',array(':studio_id'=>Yii::app()->user->studio_id));
				foreach($contentCategory as $key=>$val){
					$urlRouting = new UrlRouting();
					$urlRouting->permalink = $val->permalink."/".$permalink;
					$urlRouting->mapped_url = '/media/list/category/'.$val->id.'/subcategory/'.$cat_id;
					$urlRouting->studio_id = Yii::app()->user->studio_id;
					$urlRouting->created_date = new CDbExpression("NOW()");
					$urlRouting->ip = $ip_address;
					$urlRouting->save();
				}
                
                Yii::app()->user->setFlash('success', $_REQUEST['displayname'] . ' added successfully.');
                $arr['succ'] = 1;
                $arr['msg'] = 'Success';
            }
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = @$_REQUEST['img_width'];
                $height = @$_REQUEST['img_height'];
                $cropDimension = array('thumb' => $width.'x'.$height, 'standard' => $width.'x'.$height,'cmsthumb'=>'100x100');
                $object_id = $cat_id;
                $return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, 'content_subcategory', $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],$object_id);
            }
            echo json_encode($arr);exit;
        }
    }	
/**
 * @method public updateSubcontentCategory() Update Content Category Name
 * @author Gayadhar <support@muvi.com>
 * @return HTML 
 */		
    function actionUpdateSubContentCategory() {
		$arr['succ'] = 0;$arr['msg'] = '';
        if (isset($_REQUEST['sub_category_id']) && $_REQUEST['sub_category_id']) { 
			$category_value = (int)$_REQUEST['category_value'];
            $contentModel = new ContentSubcategory();
            $data = $contentModel->find('(subcat_name=:display_name) AND studio_id=:studio_id AND id!=:id AND category_value ='.$category_value, array(':display_name' => $_REQUEST['displayname'], ':studio_id' => Yii::app()->user->studio_id, 'id' => $_REQUEST['sub_category_id']));
            
            if ($data) {
                $data = $data->attributes;
                $arr['msg'] = "Oops! Sorry subcategory name already exsists with your studio";
				echo json_encode($arr);exit;
            } else {
                $contentModel = ContentSubcategory::model()->findByPk($_REQUEST['sub_category_id']);
                $contentModel->subcat_name = $_REQUEST['displayname'];
                $contentModel->save();
                Yii::app()->user->setFlash('success', $_REQUEST['displayname'] . ' updated successfully.');
				$arr['succ'] = 1 ;$arr['msg'] = 'Success';
            }
            if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '') || (isset($_REQUEST['g_image_file_name']) && $_REQUEST['g_image_file_name'] != '')) {
                $theme_folder = $this->studio->theme;
                $width = @$_REQUEST['img_width'];
                $height = @$_REQUEST['img_height'];
                $cropDimension = array('thumb' => $width.'x'.$height, 'standard' => $width.'x'.$height,'cmsthumb'=>'100x100');
                $object_id = $_REQUEST['sub_category_id'];
                $object_type = "content_subcategory";
                $removeposter = $this->removePosters($object_id, $object_type);
                $return = Yii::app()->custom->processUploadImage($_FILES['Filedata'], $_REQUEST['fileimage'], $theme_folder, $cropDimension, $object_type, $_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $_REQUEST['jcrop_allimage'],$object_id);
			}
		}
		echo json_encode($arr);exit;
    }	
    /*Category reordering by manas@muvi.com*/
    public function actionDivReorder(){
        if ($_POST['order']) {
            $j = 1;
            foreach ($_POST['order'] as $k => $v) {
                $attr= array('id_seq'=>$j);
                $condition = "studio_id=:studio_id AND id=:id";
                $params = array(':studio_id'=>$this->studio->id,':id'=>$v);
                $feat = ContentCategories::model()->updateAll($attr,$condition,$params);
                $j++;
}
        }
    }
    /**
     * Form generation as per content type
     * if studio have custom form fetch from custom table or show default
     * @author manas@muvi.com
     * @param int $view_flag render view file or return array
     * @param int $parent_content_type_id
     * @param array $arg
     */
    public function actionCustomFormGeneration($studio_id,$parent_content_type_id = 1,$arg){
        $customComp = new CustomForms();
        $customData = $customComp->getCustomMetadata($studio_id,$parent_content_type_id,$arg);         
	$formData = $customData['formData'];
	unset($customData['formData']);
            return $customData;
        }
    public function actionSaveCustomeForm() {
        $formField = $_POST['formfield'];
        $custom = $_POST['custom'];
		$excaptionarray = array('censor_rating', 'language', 'genre');
        if (isset($formField['parent_content_type_id']) && is_numeric($formField['parent_content_type_id'])) {
            $studio_id = $this->studio->id;
            $arg['content_type'] = $formField['content_type'];
            $arg['is_child'] = $formField['is_child'] = ($formField['content_type']==0)?0:$formField['is_child'];
            $binary = CustomMetadataForm::model()->getbinaryvaluesofcustomform($formField);
            $formField['content_types_id'] = Yii::app()->general->getcontents_types_id($formField);
            $formField['form_type_id'] = Yii::app()->general->getmetadata_form_type_id($formField);
            if($_POST['editid'] && is_numeric($_POST['editid'])){
                $arg['editid'] = $_POST['editid'];
                $ins_id = CustomMetadataForm::model()->insertFormData($studio_id,$formField,$binary,$arg['editid']);
            }else{
                $ins_id = CustomMetadataForm::model()->insertFormData($studio_id,$formField,$binary);
            }
            $customComp = new CustomForms();
            $customData = $customComp->getCustomMetadata($studio_id,$formField['parent_content_type_id'],@$arg);
            $metadata_form_type_id =  Yii::app()->general->getmetadata_form_type_id($formField);
            unset($custom['content_type']);
            if(@$arg['editid'] && @$customData){//Update
                $update = 1;
                unset($customData['formData']);                
                $DBcmffids = CHtml::listData($customData, 'id', 'id');                             
            }
                $j=1;
            $customindex = self::getlastcustomid($studio_id,$metadata_form_type_id,$ins_id);
                $DBarray = array();
                if(in_array($metadata_form_type_id, array(4,9))){
                    $fcm = new MovieStreamsCustomMetadata;
                }else if($metadata_form_type_id==6){
                    $fcm = new PGCustomMetadata;
                }else{
                    $fcm = new FilmCustomMetadata;
                }
                foreach ($custom as $key => $value) {
                if(@$update){
                    $cmffmodel = CustomMetadataFormField::model()->find('custom_field_id=:custom_field_id and custom_form_id=:custom_form_id',array(':custom_field_id'=>$value['id'],':custom_form_id'=>$ins_id));
                    if($cmffmodel){
                        $cmffmodel->id_seq = $j;
                        $cmffmodel->save();
                        array_push($DBarray, $value['id']);
						$insert = 0;
                    }else{$insert = 1;}
                }else{$insert = 1;}
                if(@$insert){
                    if(isset($value['data_type']) && ($value['data_type']=='custom')){
                        unset($value['data_type']);
                        $value['field_type']=1;
					}else if(in_array($value['f_name'],$excaptionarray)){
						$value['field_type']=1;
                    }
                    $found = CustomMetadataField::model()->findByAttributes(array('studio_id' => $studio_id, 'f_type' => $value['f_type'], 'f_name' => $value['f_name'],'f_display_name'=>$value['f_display_name']));
                    if(empty($found)){
                        $tempid = CustomMetadataField::model()->insertFieldData($studio_id,$value);
                    }else{
                        $tempid = $found['id'];
                    }                    
                    CustomMetadataFormField::model()->insertFormFieldData($ins_id,$tempid,$j);
                }
                    //insert into film_custom_metadata if it is custom field
					$fcmmodel = $fcm->find('custom_field_id=:custom_field_id and studio_id=:studio_id and custom_metadata_form_id=:custom_metadata_form_id', array(':custom_field_id' => $value['id'], ':studio_id' => $studio_id, ':custom_metadata_form_id' => $ins_id));
                    if(empty($fcmmodel)){
                    if(@$value['field_type']==1){
                        $arr = self::getavailablefield(0,0);
                        if(!in_array($value['f_name'],$arr)){
                            CustomMetadataFormField::model()->addContentTocustommeta($fcm,$customindex,$value['id'],$studio_id,$ins_id);
                            $customindex++;
                                }
                                }
                            }                            
                    $j++;
                    /*update display name*/
                    $attr= array('f_display_name'=>$value['f_display_name']);
                    $condition = "studio_id=:studio_id AND id=:id";
                    $params = array(':studio_id'=>$studio_id,':id'=>$value['id']);
                    $feat = CustomMetadataField::model()->updateAll($attr,$condition,$params);
                    /*End*/
                }
            if(@$update){
                $diff = array_diff($DBcmffids, $DBarray);
                if(!empty($diff)){
                    CustomMetadataFormField::model()->deleteAll('custom_field_id IN (' . implode(',', $diff) . ')');
					if($fcm){
						$fcm->model()->deleteAll('custom_field_id IN (' . implode(',', $diff) . ')');
					}
                } 
			} 
            if($update){
                Yii::app()->user->setFlash('success', 'Form updated successfully.');
                    }else{
                Yii::app()->user->setFlash('success', 'Form added successfully.');
                    }
                    }else{
            Yii::app()->user->setFlash('error', 'Form updation failure.');
        }
        $this->redirect($this->createUrl('category/manageCategory'));
    }
    public function actionAddNewField(){
        $field_type = array('TextField','Textarea','Dropdown','List','Rich Text');
        if(isset($_POST['id']) && is_numeric($_POST['id'])){
            $id = $_POST['id'];
            $cf = CustomMetadataField::model()->findByPk($id);            
        }
        $this->renderPartial('addnewfield', array('field_type'=>$field_type,'cf'=>$cf,'lang_code'=>@$_POST['lang_code']));
    }
    public function actionSaveNewField(){
        parse_str($_POST['formData'],$formData);
        //$formData = $_POST;
		$CustomFieldLang = @$formData['CustomFieldLang'];
        $formData = $formData['data'];
		if($formData['f_type']>1 && ($formData['f_type']!=4)){$jsontrue = 1;}else{$jsontrue = 0;}
        if(!empty($formData)){
                $studio_id = $this->studio->id;                
                if($formData['edit_id'] && is_numeric($formData['edit_id'])){
                    $cmfield = CustomMetadataField::model()->find('id=:id AND studio_id=:studio_id',array(':id'=>$formData['edit_id'],':studio_id'=>$studio_id));
                    if($cmfield){
                    $msg = "Field updated successfully.";
					$trans = TranslateKeyword::model()->UpdateKey($studio_id, $formData['f_id'], $formData['f_display_name']);
                    }else{$insert = 1;}
                }else{$insert = 1;}
                if($insert){
                    if(in_array($formData['f_id'],self::getavailablefield(0,0))){
                        $f_name = $formData['f_id'];
                }else{
					if(@$formData['f_id']){
						$togetfname = $formData['f_id'];
					}else{
						$togetfname = $formData['f_display_name'];
                    }
					$togetfname = trim($togetfname);
					$f_name = str_replace(" ", "_", strtolower($togetfname));
					$f_name = preg_replace('/[^a-zA-Z0-9]/', '_', $f_name);
					$f_name = self::checkname($studio_id, $f_name, $togetfname);//check name of field exist or not
				}				
                    $msg = "Field inserted successfully.";
                    $cmfield = New CustomMetadataField;
                    $cmfield->f_name = $f_name;
                    $cmfield->f_id = $f_name;
                    $cmfield->studio_id = $studio_id;
                    $cmfield->field_type = 1;//field type = custom
				}else{
					if($jsontrue){
						$cs_value = json_decode($cmfield->f_value,true);
					}
				}
				if(($cmfield->f_name=='genre')){
                    $dt['genre'] = ($jsontrue)?array_values($formData['f_value']):0;
                    if(is_array($dt['genre'])){
                        $movieTags = new MovieTag();
                        $movieTags->addTagsFromCustomfield($dt);
                    }
                }
                $cmfield->f_display_name = $formData['f_display_name'];                
                $cmfield->f_type = $formData['f_type'];
				if($jsontrue){
					if(@$cs_value && @$CustomFieldLang){
						if(!array_key_exists('en', $cs_value)){
							$tempvalue['en'] = $cs_value;
						}else{
							$tempvalue = $cs_value;
						}
						$tempvalue[$CustomFieldLang] = array_values($formData['f_value']);
					}else{
						$tempvalue['en'] = array_values($formData['f_value']);
					}
					$f_value = $tempvalue;
				}
				$cmfield->f_value = ($jsontrue)?json_encode($f_value):NULL;
                $cmfield->save();
                $tempid = $cmfield->id;
            if($tempid){echo $msg;}else{echo 0;}                     
        }else{echo 0;}
        exit;        
                }
    public function actionRemoveCustomField(){
        $flag = 0;
        if(isset($_POST['id']) && is_numeric($_POST['id'])){
            $id = $_POST['id'];
            $removecf = CustomMetadataField::model()->findByPk($id);
            if($removecf){
                $row = CustomMetadataField::model()->deleteByPk($id, 'studio_id=:studio_id', array(':studio_id' => $this->studio->id));
                if($row){
                    CustomMetadataFormField::model()->deleteAll('custom_field_id = '.$id);
                    FilmCustomMetadata::model()->deleteAll('custom_field_id = '.$id);
                    PGCustomMetadata::model()->deleteAll('custom_field_id = '.$id);
                    MovieStreamsCustomMetadata::model()->deleteAll('custom_field_id = '.$id);
                    $flag = 1;
                    TranslateKeyword::model()->DeleteKey($this->studio->id, $removecf->f_id);
                }
            }
        }
        echo $flag;
        exit;
    }
    /**
     * @author MRS <manas@muvi.com>
     * @param type $parent_content_type_id = $formid
     * @return type
     */
    public function actionGetCustomForm($formid=0){
        if(isset($_POST['formid']) && is_numeric($_POST['formid'])){
            $formid = $_POST['formid'];
            $render_flag = 1;
        }else{
            $render_flag = 0;
        }
        if($formid){
            $arg['content_type'] = $_POST['is_multipart'];
            $arg['is_child'] = ($_POST['is_multipart']==0)?0:$_POST['is_child'];
            if($_POST['editid'] && is_numeric($_POST['editid'])){
                $arg['editid'] = $_POST['editid'];
                $column_data = self::actionCustomFormGeneration($this->studio->id,$formid,$arg);
                if(empty($column_data)){
                    //unset($arg['editid']);
                    $column_data = self::actionCustomFormGeneration(0,$formid,$arg);
                }
            }else{
                $column_data = self::actionCustomFormGeneration(0,$formid,$arg);
            }            
            if(@$column_data){
				$array_new = array();
				if($arg['editid']){
					$temp_arg = $arg;
					unset($temp_arg['editid']);
					$default_available_data  =  CHtml::listData(self::actionCustomFormGeneration(0,$formid,$temp_arg), 'f_id', 'f_name');
					$system_availbale_data = CustomMetadataField::model()->getFieldData($this->studio->id);
					foreach($default_available_data as $key){
						if(array_key_exists($key, $system_availbale_data)){
							$array_new[$key] = $system_availbale_data[$key];
						}
					}
				}
                $Allcolumn_data = CustomMetadataField::model()->getFieldData($this->studio->id,1);
                $Allcolumn_data_default = CustomMetadataField::model()->getFieldData(0,1);
                $arr = self::getavailablefield($formid,$arg);
				$Allcolumn_data = !empty($Allcolumn_data)?$Allcolumn_data:array();
                $Allcolumn_data_new = array_merge($Allcolumn_data_default,$Allcolumn_data);
				$Allcolumn_data_new = array_merge($Allcolumn_data_new,$array_new);
                $column_data_ids = CHtml::listData($column_data, 'id', 'id');
				$column_data_f_names = CHtml::listData($column_data , 'f_id', 'f_name');
                $arg['parent_content_type_id'] = $formid;
                $metadata_form_type_id =  Yii::app()->general->getmetadata_form_type_id($arg);
                foreach ($Allcolumn_data_new as $key => $value) {
                    if(!in_array($value['id'], $column_data_ids)){
						if(!in_array($value['f_name'], $column_data_f_names)){
							if(!in_array($value['f_name'], $arr)){
								$template_data[] = $value;
							}
						}
					}
                }
                if($render_flag){
                        $this->renderPartial('content_metadata', array('column_data'=>$column_data,'template_data'=>$template_data,'formid'=>$metadata_form_type_id));
                }else{
                        $output = $this->renderPartial('content_metadata', array('column_data'=>$column_data,'template_data'=>$template_data,'formid'=>$metadata_form_type_id),true);
                    return $output;
                }            
            }else{echo "error";}
        }else{echo "error";}
        }
    function getavailablefield($formid,$arg){
        if($arg['is_child']){
            return array('censor_rating','language','genre');
        }elseif(in_array($formid,array(1,2,3))){
            return array('series_number','episode_number');
        }else{
            return array('censor_rating','language','genre','series_number','episode_number');
        }
    }
    /**
     * @author Manas Ranjan Sahoo <manas@muvi.com>
     * @param type $formid
     * @return string
     */
    function defaultarray($formid=1){
        if($this->studio->id < 3710){$old = 1;}else{$old=0;}
        if(in_array($formid,array(1,3))){//Single part long form & Multi part parent
            $defaultFields = array('name','release_date','story');//,'genre'
            if($old){array_push($defaultFields, 'censor_rating','language');}
        }elseif($formid==2){//Single part short form
            $defaultFields = array('name','story');
            if($old){array_push($defaultFields, 'release_date','genre');}
        }elseif(in_array($formid,array(4,9))){//Multipart child
            $defaultFields = array('content_name','title','story');
            if($old){array_push($defaultFields, 'episode_date');}//'series_number','episode_number',
        }elseif($formid==5){//video live streaming
            $defaultFields = array('name','release_date','story');
            if($old){array_push($defaultFields, 'genre');}
        }elseif($formid==6){//Physical content
            $defaultFields = array('name','description','sale_price','sku','product_type');
        }elseif(in_array($formid,array(7,8))){//Audio Single part & Multi part parent
            $defaultFields = array('name','release_date','story');
        }
        return $defaultFields;
    }
    function checkname($studio_id, $f_name, $f_display_name = false){
        $exists = CustomMetadataField::model()->exists('studio_id=:sid AND f_id=:name',array(":sid"=>$studio_id,":name"=>$f_name));
        $checkarray = self::CheckUniqueArray();
        if($exists || in_array($f_name, $checkarray)){
            $f_name = self::createfname($f_name);
            return self::checkname($studio_id,$f_name,$f_display_name);
        }else{
            if($f_display_name){
                $trans = TranslateKeyword::model()->checkUniqueKey($studio_id, $f_name);
                if(!empty($trans)){
                    if(strtolower(trim($trans['trans_value'])) != strtolower($f_display_name)){
                    $f_name = self::createfname($f_name);
                    return self::checkname($studio_id,$f_name, $f_display_name);
                    }elseif($trans['device_type'] !='2'){
                        $translate = TranslateKeyword::model()->findByPk($trans['id']);
                        $translate -> device_type = '2';
                        $translate->save();
                    }
                }else{
                    TranslateKeyword::model()->insertNewKey($studio_id, $f_name, $f_display_name);
                }
            }
            return $f_name;
        }
    }
    public function createfname($f_name){
        $format_perm = preg_replace("/\d+$/","", $f_name);
        $parts = explode($format_perm, $f_name);
        $end = 1;
        if (count($parts) > 0) {
            $end = (int) $parts[1];
            $end++;
        }
        $f_name = $format_perm.$end;
        return $f_name;
    }
    public function actionCheckUniqueFieldName(){
        $formData = $_POST;
        $formData = trim(preg_replace('/[^a-zA-Z0-9]/', '_', $_POST['fieldname']));
        $f_name = '';
        $studio_id = $this->studio->id;
        if(!empty($formData)){        
            $f_name = self::checkname($studio_id, str_replace(" ", "_", strtolower($formData)));//check name of field exist or not  
        }
        echo $f_name;
    }
    function getlastcustomid($studio_id,$content_type,$custom_metadata_form_id){
        $pdo_cond = 'studio_id=:studio_id AND custom_metadata_form_id=:custom_metadata_form_id';
        $pdo_array = array(':studio_id'=>$studio_id,':custom_metadata_form_id'=>$custom_metadata_form_id);
        if(in_array($content_type, array(4,9))){
            $data = MovieStreamsCustomMetadata::model()->findAll($pdo_cond,$pdo_array);
        }else if($content_type == 6){
            $data = PGCustomMetadata::model()->findAll($pdo_cond,$pdo_array);
        }else{
            $data = FilmCustomMetadata::model()->findAll($pdo_cond,$pdo_array);
        }
        if(!empty($data)){
            $c = CHtml::listData($data, 'custom_field_id', 'field_name');
			$c = str_replace('custom', '', $c);
            $customindex = (int) max($c);
        }else{
            $customindex = 0;
        }
        return $customindex+1;
    }
    function actionGetFormList(){
        $studio_id = $this->studio->id;
        $parent_content_type_data = ParentContentType::model()->findAll();
        $parent_content_name = CHtml::listData($parent_content_type_data, 'id', 'name');
        $formdata = Yii::app()->general->formlist($studio_id); 
        $delids = array_keys($formdata);        
        $content_ids = (!empty($delids))?Yii::app()->general->getIdsOfcontent(implode(',', $delids)):'';
        $content_form_list = $this->renderPartial('content_form_list', array('data'=>$formdata,'parent_content_name' => $parent_content_name,'content_ids'=>$content_ids),true);
        return $content_form_list;
    }
    public function actionAddContentForm(){
        $this->pageTitle = "Muvi | Add Content Format";
        $this->breadcrumbs = array('Manage Content','Manage Metadata'=>array('category/manageCategory'));
        $this->headerinfo = "Add Content Format";
        if((isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))){
            $id = $_REQUEST['id'];
            $cmfdata = CustomMetadataForm::model()->find('id=:id AND studio_id=:studio_id',array(':id'=>$id,':studio_id'=>$this->studio->id));
			if(empty($cmfdata)){
				$cmfdata = CustomMetadataForm::model()->find('id=:id AND studio_id=:studio_id',array(':id'=>$id,':studio_id'=>0));
			}
			if($cmfdata){
				$this->pageTitle = "Muvi | Edit Content Format";
				$this->headerinfo = "Edit Content Format";
			}
        }
        $parent_content_type_data = ParentContentType::model()->findAll();       
        $this->render('AddEditCustomForm', array('cmfdata'=>@$cmfdata,'parent_content_type_data' => $parent_content_type_data,'studio_id'=>$this->studio->id));
    }
    /**
     * @author manas <manas@muvi.com>
     * @return int flag 
     * @param int or array
     * @example delete form
     */
    public function actionDeleteForms() {
        if ($_POST['id']) {
            $formid = is_array($_POST['id']) ? implode(', ', $_POST['id']) : $_POST['id'];
            $alldefaultform = CustomMetadataForm::model()->findAll('id IN (' . $formid . ') AND studio_id = 0');
            $alldefaultformid = CHtml::listData($alldefaultform, 'id', 'id');
            if (is_array($_POST['id'])) {
                $delidarray = array_diff($_POST['id'], $alldefaultformid);                
                $delids = implode(', ', $delidarray);
                $content_ids = Yii::app()->general->getIdsOfcontent($delids);                
                $finalids = array_diff($delidarray, $content_ids);
                $delid = implode(', ', $finalids);
            } else {
                if (!in_array($formid, $alldefaultformid)) {
                    $delid = $formid;
                } else {
                    echo 0;
                    exit;
                }
            }            
            if (CustomMetadataForm::model()->deleteAll('id IN (' . $delid . ') AND studio_id = ' . $this->studio->id)) {
                CustomMetadataFormField::model()->deleteAll('custom_form_id IN (' . $delid . ')');
				FilmCustomMetadata::model()->deleteAll('custom_metadata_form_id IN (' . $delid . ')');
				PGCustomMetadata::model()->deleteAll('custom_metadata_form_id IN (' . $delid . ')');
				MovieStreamsCustomMetadata::model()->deleteAll('custom_metadata_form_id IN (' . $delid . ')');				
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        } else {
            echo 0;
            exit;
        }
    }
    function CheckUniqueArray(){
        return  array('content_name','name','movie_id','is_episode','title','content_title','play_btn','permalink','poster','data_type','is_landscape','release_date','full_release_date','censor_rating','movie_uniq_id','stream_uniq_id','video_duration','video_duration_text','ppv','payment_type','is_converted','movie_stream_id','uniq_id','content_type_id','content_types_id','ppv_plan_id','full_movie','story','short_story','genres','display_name','content_permalink','trailer_url','trailer_is_converted','trailer_player','casts','casting','content_banner','reviewformonly','reviewsummary','reviews','defaultresolution','multiplevideo');
    }
}