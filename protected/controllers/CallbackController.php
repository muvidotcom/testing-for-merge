<?php
require 's3bucket/aws-autoloader.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
use Aws\S3\S3Client;
class CallbackController extends Controller {
    public function actionApiResponse() {
        $this->HandleClickhereResponse(file_get_contents('php://input'));
        $data = file_get_contents('php://input');
         if ($data && (strpos($data, 'msisdn') !== false )) {
            $dataExtract = explode('&', $data); 
            $msisdn = explode('=', $dataExtract[0]); 
            $status = explode('=', $dataExtract[1]);
            $studio_id = Yii::app()->common->getStudioIdOP();    
            $sdkuser = new SdkUser;
            $getSdkUser = $sdkuser->find('mobile_number=:mobile_number and studio_id=:studio_id', array(':mobile_number' => $msisdn[1], ':studio_id'=>$studio_id));
            if($msisdn[1] && $status[1] && !empty($getSdkUser)){
                $command = Yii::app()->db->createCommand();
                if(strcasecmp(trim($status[1]),'activated') == 0){
                    $command->update('sdk_users', array('is_subscribe_inapi'=>1), 'studio_id=:studio_id AND mobile_number=:mobile_number', array(':studio_id'=>$studio_id, ':mobile_number'=>$msisdn[1]));
            }else{
                    $command->update('sdk_users', array('is_subscribe_inapi'=>2), 'studio_id=:studio_id AND mobile_number=:mobile_number', array(':studio_id'=>$studio_id, ':mobile_number'=>$msisdn[1]));
                }
            }
        }
        exit;
    }

    public function HandleClickhereResponse($res) {
        $res = file_get_contents('php://input');
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . SUB_FOLDER . 'protected/runtime/clickhere.log', "a+");
        fwrite($fp, " \n\n\t Response " . date('m-d-Y H:i:s') . "\n\t " . serialize($res));
        fclose($fp);
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

