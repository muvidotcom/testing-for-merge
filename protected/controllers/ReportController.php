<?php
    
class ReportController extends Controller {
    
    public $defaultAction = 'revenue';
    public $headerinfo = '';
    public $layout = '';
        
    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = 'admin';
        if(isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id==4)){
            Yii::app()->layout = 'partners';
        }else{
            Yii::app()->layout = 'admin';
        }
        $actionArr = array('addTrailerBufferLog','addNewTrailerBufferLog','addTrailerLog','addNewBufferLog','addBufferLog');
        if(in_array(Yii::app()->controller->action->id, $actionArr)){
            return true;
        }else{
            if (!(Yii::app()->user->id)) {
                $this->redirect(array('/index.php/'));
            }else{
                 $this->checkPermission();
            }
        }
        return true;
    }
        
    /**
     * @method public revenue() It give all the details of revenue earned by the respective site
     * @author GDR<support@muvi.com>,SKM<support@muvi.com>
     * @return HTML	
     */
    function actionRevenue() {
       $this->pageTitle = Yii::app()->name .' | ' . 'Revenue Reports';
        $this->breadcrumbs = array('Analytics', 'Revenue');
        $this->headerinfo = "Revenue";
        $studio = $this->studio;
        $default_currency = $studio->default_currency_id;
        $currency = Yii::app()->common->getStudioCurrency();
        $c = new Currency();
        $currency_details = $c->findByPk($default_currency);
        $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
        $plan = $plans['plans'];
        $studioPlans = array();
        foreach($plan AS $plank=>$planval ){
            $studioPlans[$planval['id']] = $planval['name'];
        }
        $dbcon = Yii::app()->db;
        if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){
            $content = Yii::app()->general->getPartnersContentIds();
            if(trim($content['movie_id'])){
                $movie_ids = " AND transactions.movie_id IN({$content['movie_id']})";
            }else{
                $movie_ids = '';
            }
           
            $sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id AND user_subscriptions.status = 1) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE  transactions.studio_id = '" . Yii::app()->user->studio_id ."'". $movie_ids ." AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
            $ppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 0 AND ppv_subscriptions.is_ppv_bundle = 0 ".$movie_ids." AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
            $advppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 1 ".$movie_ids." AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
            $ppvbundlessql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_ppv_bundle = 1 ".$movie_ids." AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
          
            
            }else{
            $sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id AND user_subscriptions.status = 1) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE  transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
            $ppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 0 AND ppv_subscriptions.is_ppv_bundle = 0 AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
            $advppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 1 AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
            $ppvbundlessql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_ppv_bundle = 1 AND transactions.currency_id = '".$default_currency."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id "; 
            }
        $data = $dbcon->createCommand($sql)->queryAll();
        $ppvdata = $dbcon->createCommand($ppvsql)->queryAll();
       
        $advppvdata = $dbcon->createCommand($advppvsql)->queryAll();
        $ppvbundlesdata = $dbcon->createCommand($ppvbundlessql)->queryAll();
        $isPpv = 0;
        $data = $this->array_restructure($data);
        if ($data) {
            foreach ($data AS $key => $val) {
                $gdata[$val['trans_month']][$studioPlans[$val['plan_id']]] = (float) $val['tot'];
            }
        }
        if ($ppvdata) {
            foreach ($ppvdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['ppv']))
                        $new_data = $gdata[$val['trans_month']]['ppv'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['ppv'] = $new_data;
                }
            }
        }
        
        if ($advppvdata) {
            foreach ($advppvdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['adv_ppv']))
                        $new_data = $gdata[$val['trans_month']]['adv_ppv'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['adv_ppv'] = $new_data;
                }
            }
        }
        
         if ($ppvbundlesdata) {
            foreach ($ppvbundlesdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['ppv_bundles']))
                        $new_data = $gdata[$val['trans_month']]['ppv_bundles'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['ppv_bundles'] = $new_data;
                }
            }
        }
        
        
        
        $lunchDate = Yii::app()->user->lunch_date;
        $lmonth = date('n', strtotime($lunchDate));
        $lyear = date('Y', strtotime($lunchDate));
        for ($i = date('Y'); $i >= $lyear; $i--) {
            if ($i == date('Y')) {
                $j = date('n');
            } else {
                $j = 12;
            }
            for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                $mont = $j < 10 ? '0' . $j : $j;
                foreach($studioPlans AS $plank =>$planv){
                    $arr[$planv][] = isset($gdata[$i . "-" . $mont][$planv]) ? $gdata[$i . "-" . $mont][$planv] : 0;
                }
                $arr['Ppv'][] = isset($gdata[$i . "-" . $mont]['ppv']) ? $gdata[$i . "-" . $mont]['ppv'] : 0;
                $arr['Advance Purchase'][] = isset($gdata[$i . "-" . $mont]['adv_ppv']) ? $gdata[$i . "-" . $mont]['adv_ppv'] : 0;
                $arr['Ppv Bundles'][] = isset($gdata[$i . "-" . $mont]['ppv_bundles']) ? $gdata[$i . "-" . $mont]['ppv_bundles'] : 0;
                 //$xdata[] = date('F', mktime(0, 0, 0, $j, 10));
            }
        }
        if(!isset(Yii::app()->user->is_partner)){
            foreach($studioPlans AS $plank =>$planv){
                @$totalRevenue += array_sum($arr[$planv]);
                $revenue[] =  array('name' => ucfirst($planv), 'data' => array_reverse($arr[$planv]));
            }
            
              @$totalRevenue += array_sum($arr['Ppv Bundles']);
        $revenue[] =  array('name' => ucfirst('Ppv Bundles'), 'data' => array_reverse($arr['Ppv Bundles']));
        $sumArray = array();
            
        }
        @$totalRevenue += array_sum($arr['Ppv']);
        $revenue[] =  array('name' => ucfirst('Ppv'), 'data' => array_reverse($arr['Ppv']));
        
        @$totalRevenue += array_sum($arr['Advance Purchase']);
        $revenue[] =  array('name' => ucfirst('Advance Purchase'), 'data' => array_reverse($arr['Advance Purchase']));
        $sumArray = array();
         
       
        
        //Total revenue graph data
        /*foreach ($revenue as $k => $subArray) {
            foreach ($subArray as $ke => $suubArray) {
                if(is_array($suubArray)){
                    foreach($suubArray as $key=>$value){
                        $sumArray[$key]+=$value;
                    }
                }
            }
        }
        for($i=0;$i<count($sumArray);$i++){
            if($i == 0){
                $sumArray[$i] = $sumArray[$i];
            }else{
                $sumArray[$i] = $sumArray[$i]+$sumArray[$i-1];
            }
        }
        if(count($revenue) > 1){
            $revenue[] = array('name' => ucfirst('Total'), 'data' => $sumArray);
        }*/
            
        //Total revenue graph data ends
            
        $graphData = json_encode($revenue);
            
        // Getting the Revenue for the Current selected month
        $jdata = $this->actionGetMonthlyRevenue('','',$content);
        $de_jdata = json_decode($jdata, TRUE);
       
        $revenueData = $de_jdata[0];
        $rrevenueData = $de_jdata[1];
        
        $res = $this->actionGetMonthlySubscribers('','','',$content);
        $resPpv = $this->actionGetMonthlyPpvSubscribers('','','',$content);
        $resAdvPpv = $this->actionGetMonthlyAdvPpvSubscribers('','','',$content);
        $resPpvbundles = $this->actionGetMonthlyPpvbundlesSubscribers('','','',$content);

        $page_size = 20;
        $data = $res['data'];
        $count = $res['count'];
            
        $ppvdata = $resPpv['data'];
        $ppvcount = $resPpv['count'];
        $advppvdata = $resAdvPpv['data'];
        $advppvcount = $resAdvPpv['count'];
        
        $ppvbundlesdata = $resPpvbundles['data'];
        $ppvbundlescount = $resPpvbundles['count'];
        $user_start = Yii::app()->common->getStudioUserStartDate();
       
        $this->render('revenue', array('revenueData' => $revenueData,'rrevenueData' => $rrevenueData, 'graphData' => $graphData, 'lmonth' => $lmonth, 'lyear'=>$lyear,'totalRevenue' => $totalRevenue,'studioPlans'=>$studioPlans, 'xdata' => json_encode(array_reverse($xdata)), 'isPpv' => $isPpv,'subscription'=> $data,'subscription_count'=>$count,'page_size' => $page_size,'ppv_subscription'=>$ppvdata,'ppv_subscription_count'=>$ppvcount,'ppvbundles_subscription'=>$ppvbundlesdata,'ppvbundles_subscription_count'=>$ppvbundlescount,'advppvdata'=>$advppvdata,'advppvcount'=>$advppvcount,'lunchDate'=>$user_start,'default_currency'=>$default_currency,'currency'=>$currency,'currency_details'=>$currency_details,'percentageshare'=>$percentageshare));
            
    }
        
    /**
     * @method public usages() It give all the details of subscribers site
     * @author GDR<support@muvi.com>,SKM<support@muvi.com>
     * @return HTML	
     */
    function actionUsages() {
        $this->pageTitle = "Muvi | User Action";
        $this->breadcrumbs = array('Analytics', 'User Action');
        $this->headerinfo = "User Action";
         $dbcon = Yii::app()->db;
        $studio_id = Yii::app()->user->studio_id;
		$loginArr = LoginHistory::model()->getAllLoginHistory($studio_id);
            
        $lunchDate = Yii::app()->user->lunch_date;
        $lmonth = date('n', strtotime($lunchDate));
        $lyear = date('Y', strtotime($lunchDate));
        $arr = array();
        for ($i = date('Y'); $i >= $lyear; $i--) {
            if ($i == date('Y')) {
                $j = date('n');
            } else {
                $j = 12;
            }
            for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                $mont = $j < 10 ? '0' . $j : $j;
                $arr['login'][] = isset($loginArr[$i . "-" . $mont]) ? intval($loginArr[$i . "-" . $mont]) : 0;
                $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
            }
        }
        $logingraph[] =  array('name' => ucfirst('Logins'), 'data' => array_reverse($arr['login']));
        $graphData = json_encode($logingraph, TRUE);
		
        $logins = $this->actionGetMonthlyLogins();
        $results = $this->actionSearchData();
        $page_size = 20;
        $user_start = Yii::app()->common->getStudioUserStartDate();
        
        //views
        //$view_graph_data = VideoLogs::model()->getAllViewHistory($studio_id);
//       foreach ($view_graph_data as $viewrow) {
//            $date = date('Y-m',strtotime($viewrow['created_date']));
//            $x[] = $date;
//            if(in_array($date, $x)){
//                $viewArr[$date] +=1;
//            }else{
//                $viewArr[$date] = 1;
//            }
//        }
		$viewLogs = Yii::app()->db->createCommand("SELECT DATE_FORMAT(created_date,'%Y-%m') as created_date, count(id) as cnt FROM video_logs WHERE studio_id=".$studio_id." GROUP BY DATE_FORMAT(created_date,'%Y-%m')")->queryAll();
		foreach ($viewLogs as $viewrow) {
            $viewArr[$viewrow['created_date']] = (INT)$viewrow['cnt'];
            }
        //echo "<pre>";print_r($viewArr);exit;
        $lunchDate = Yii::app()->user->lunch_date;
        $lmonth = date('n', strtotime($lunchDate));
        $lyear = date('Y', strtotime($lunchDate));
        $arr = array();
        for ($i = date('Y'); $i >= $lyear; $i--) {
            if ($i == date('Y')) {
                $j = date('n');
            } else {
                $j = 12;
            }
            for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                $mont = $j < 10 ? '0' . $j : $j;
                $arr['loginview'][] = isset($viewArr[$i . "-" . $mont]) ? $viewArr[$i . "-" . $mont] : 0;
                $xdataView[] = date('F', mktime(0, 0, 0, $j, 10));
            }
        }
        $viewgraphview[] =  array('name' => ucfirst('Views'), 'data' => array_reverse($arr['loginview']));
        $graphDataview = json_encode($viewgraphview);
        $totalviewssql='select count(*) as totalviewcount,SUM(played_length) as totalduration from video_logs where studio_id='.$studio_id;
        $totalviews= $dbcon->createCommand($totalviewssql)->queryRow();
        $totalsviewcount=$totalviews['totalviewcount'];
        $totalshourscount=$totalviews['totalduration'];
        $lastmondate = date('Y-m', strtotime("first day of last month"));
        $totalviewssqllastmonth='select count(*) as totalviewcounthour,SUM(played_length) as totaldurationhour from video_logs where studio_id='.$studio_id.' AND DATE_FORMAT(created_date, "%Y-%m")="'.$lastmondate.'"';
        $totalhourviewslastmonth= $dbcon->createCommand($totalviewssqllastmonth)->queryRow();
        $totalsviewcountlasthour=$totalhourviewslastmonth['totalviewcounthour'];
        $totalsdurationcounthour=$totalhourviewslastmonth['totaldurationhour'];
        $this->render('usages', array('results' => @$results, 'logins' => $logins,'xdata' => json_encode(array_reverse($xdata)),'graphData' => $graphData,'xdataview' => json_encode(array_reverse($xdataView)),'graphDataview' => $graphDataview,'page_size' => $page_size,'lunchDate'=>$user_start,'totalsviewcount'=>$totalsviewcount,'totalshourscount'=>$totalshourscount,'totalsviewcountlasthour'=>$totalsviewcountlasthour,'totalsdurationcounthour'=>$totalsdurationcounthour));
            
                }
                    
    /**
     * @method public userdata() It give all the details of subscribers site
     * @author GDR<support@muvi.com>
     * @return HTML	
     */
    function actionUserdata() {
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
            
        $type = (isset($_REQUEST['type']) && $_REQUEST['type']) ? $_REQUEST['type'] : 'subscriber';
        $this->pageTitle = "Muvi | Users Report";
        $this->breadcrumbs = array('Analytics' , 'Users Report');
        $this->headerinfo = "Users Report";
        $dbcon = Yii::app()->db;
        $lunchDate = Yii::app()->user->lunch_date;
        if ($type == 'subscriber') {
            //$countSql = "SELECT id FROM user_subscriptions WHERE status=1 AND studio_id ='" . Yii::app()->user->studio_id . "' AND (profile_id !='' OR profile_id IS NOT NULL)";
            $countSql = "SELECT us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=1 AND us.is_success=1  AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC";
            $sql = "SELECT us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name as dname,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=1 AND us.is_success=1 AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC LIMIT  $offset,$page_size";
        } elseif ($type == 'register') {
            //$countSql = "SELECT id FROM sdk_users WHERE studio_id='" . Yii::app()->user->studio_id . "' AND is_developer!= '1' AND is_studio_admin != '1'";
            $countSql = "SELECT u.id  FROM sdk_users u  LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.id DESC";
            $sql = "SELECT u.email,u.created_date AS reg_dt,u.display_name as dname,u.source,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM sdk_users u  LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.id DESC LIMIT $offset, $page_size";
        } elseif ($type == 'cancel') {
            //$countSql = "SELECT id FROM user_subscriptions WHERE status=0 AND studio_id ='" . Yii::app()->user->studio_id . "'";
            $countSql = "SELECT us.id FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=0 AND us.studio_id='" . Yii::app()->user->studio_id . "'  ORDER BY us.id DESC";
            $sql = "SELECT us.*,u.email,u.created_date AS reg_dt,u.display_name as dname,ad.address1,ad.city,ad.state,ad.country,ad.phone FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=0 AND us.studio_id='" . Yii::app()->user->studio_id . "'  ORDER BY us.id DESC LIMIT $offset,$page_size";
        }
            
        $data = $dbcon->createCommand($sql)->queryAll();
        // item count for pagination 
        $item_count = $dbcon->createCommand($countSql)->execute();
        $pages = new CPagination($item_count);
        $pages->setPageSize($page_size);
        // simulate the effect of LIMIT in a sql query
        $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
        $sample = range($pages->offset + 1, $end);
        $frmt = new Format();
        $countries = $frmt->getCountries();
        if ($type == 'cancel') {
            $grpCount = $dbcon->createCommand("SELECT cancel_reason_id,COUNT(cancel_reason_id) AS cnt FROM user_subscriptions  WHERE studio_id='" . Yii::app()->user->studio_id . "' AND status=0 GROUP BY cancel_reason_id")->queryAll();
            foreach ($grpCount AS $key => $val) {
                $reasonCount[$val['cancel_reason_id']] = $val['cnt'];
            }
            $cancelReason = $dbcon->createCommand('SELECT * FROM cancel_reasons WHERE studio_id=' . Yii::app()->user->studio_id)->queryAll();
            if($cancelReason){
				foreach ($cancelReason AS $k => $v) {
					$reasons[$v['id']] = $v['reason'];
					$cnt = @$reasonCount[$v['id']] ? $reasonCount[$v['id']] : 0;
					$per = 0;
					if ($item_count) {
						$per = ($cnt / $item_count) * 100;
					}
					$xdata[] = array($v['reason'], (float) $per);
				}
			}else{
				$xdata[] = array('Other',(float)$item_count);
			}
            $this->render('cancelSubscription', array('data' => $data, 'cancelReason' => $reasons, 'xdata' => json_encode($xdata), 'type' => $type, 'countries' => $countries, 'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample));
        } else {
            $this->render('userdata', array('data' => $data, 'type' => $type, 'countries' => $countries, 'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample));
        }
    }
        
        
     /**
     * @method public userReport() It give all the details of subscribers site
     * @author SKM<support@muvi.com>
     * @return HTML	
     */
    function actionUsersReport() {
        $this->pageTitle = "Muvi | Users Report";
        $this->breadcrumbs = array('Analytics', 'Users');
        $this->headerinfo = "Users";
        $studio_id = Yii::app()->common->getStudiosId();
        $plans = Yii::app()->common->isPaymentGatwayAndPlanExists($studio_id);
        $plan = $plans['plans'];
        $studioPlans = array();
        foreach($plan AS $plank=>$planval ){
            $studioPlans[$planval['id']] = $planval['name'];
        }
        $page_size = 20;
        $offset = 0;
        $res = Report::model()->usersReport($dt = '',$offset,$page_size);
        $data = Report::model()->allUsersReport();
        foreach ($data['registration'] as $row) {
            $date = date('Y-m',strtotime($row['created_date']));
            $x[] = $date;
            if(in_array($date, $x)){
                $reg[$date] +=1;
            }else{
                $reg[$date] = 1;
            }
        }
        foreach ($data['subscription'] as $row) {
            $date = date('Y-m',strtotime($row['reg_dt']));
            $y[] = $date;
            if(in_array($date, $y)){
                $sub[$date] +=1;
            }else{
                $sub[$date] = 1;
            }
        }
       
        foreach ($data['ppv_user'] as $row) {
            $date = date('Y-m',strtotime($row['start_date']));
            $z[] = $date;
            if(in_array($date, $z)){
                $ppv[$date] +=1;
            }else{
                $ppv[$date] = 1;
            }
        }
        
        foreach ($data['adv_ppv_user'] as $row) {
            $date = date('Y-m',strtotime($row['start_date']));
            $z[] = $date;
            if(in_array($date, $z)){
                $advppv[$date] +=1;
            }else{
                $advppv[$date] = 1;
            }
        }
        $lunchDate = Yii::app()->user->lunch_date;
        $lmonth = date('n', strtotime($lunchDate));
        $lyear = date('Y', strtotime($lunchDate));
            
        for ($i = date('Y'); $i >= $lyear; $i--) {
            if ($i == date('Y')) {
                $j = date('n');
            } else {
                $j = 12;
            }
            for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                $mont = $j < 10 ? '0' . $j : $j;
                $arr['registration'][] = isset($reg[$i . "-" . $mont]) ? $reg[$i . "-" . $mont] : 0;
                $arr['subscription'][] = isset($sub[$i . "-" . $mont]) ? $sub[$i . "-" . $mont] : 0;
                $arr['ppv_user'][] = isset($ppv[$i . "-" . $mont]) ? $ppv[$i . "-" . $mont] : 0;
                $arr['adv_ppv_user'][] = isset($advppv[$i . "-" . $mont]) ? $advppv[$i . "-" . $mont] : 0;
                $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
            }
        }
 
        $usergraph[] =  array('name' => ucfirst('registrations'), 'data' => array_reverse($arr['registration']));
        if(isset($studioPlans) && !empty($studioPlans)){
            if(!empty(array_filter($arr['subscription']))){
                $usergraph[] =  array('name' => ucfirst('subscriptions'), 'data' => array_reverse($arr['subscription']));
            }
        }
        if(!empty(array_filter($arr['ppv_user']))){
            $usergraph[] =  array('name' => ucfirst('ppv'), 'data' => array_reverse($arr['ppv_user']));
        }
        
        if(!empty(array_filter($arr['adv_ppv_user']))){
            $usergraph[] =  array('name' => ucfirst('Pre-Order'), 'data' => array_reverse($arr['adv_ppv_user']));
        }
        $graphData = json_encode($usergraph);
        $user_start = Yii::app()->common->getStudioUserStartDate();
        $this->render('userdata',array('data' => $res,'xdata' => json_encode(array_reverse($xdata)),'graphData' => $graphData,'page_size' => $page_size,'studioPlans' => $studioPlans,'ppv_users'=>$arr['ppv_user'],'lunchDate'=>$user_start));
    }
        
    /**
     * @method public getMontlyRevenue($month) Returns the revenue earned for the specified month
     * @author GDR<support@muvi.com>,SKM<support@muvi.com>
     * @return json Array of json result
     * @param char $date A month Year string to specify which month and string e.g 2014-11
     */
    function actionGetMonthlyRevenue($dt = '',$studioPlans='',$content='') {
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        if(!$studioPlans){
            $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
            if(isset($plans) && !empty($plans)){
                $plan = $plans['plans'];
                foreach($plan AS $plank=>$planval ){
                    $studioPlans[$planval['id']] = $planval['name'];
                    $arr[0][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $planval['name']))."_".$planval['id']] = 0;
                    $arr[1][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $planval['name']))."_".$planval['id']] = 0;
                }
            }
        }else{
            foreach($studioPlans AS $pkey =>$pval){
                $arr[0][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $pval))."_".$pkey] = 0;
                $arr[1][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $pval))."_".$pkey] = 0;
            }
        }
        $subdomain = isset(Yii::app()->user->studio_subdomain) ? Yii::app()->user->studio_subdomain : SUB_DOMAIN;
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        
        $dbcon = Yii::app()->db;
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(!isset(Yii::app()->user->is_partner)){
            $sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,transactions.subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id) LEFT JOIN subscription_plans ON (user_subscriptions.plan_id = subscription_plans.id) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d')>= '" . $sDate . "' AND DATE_FORMAT(transaction_date, '%Y-%m-%d')<='".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '".$currency_id."' AND sdk_users.status=1 GROUP BY  DATE_FORMAT(user_subscriptions.created_date, '%Y-%m'),plan_id, movie_id ";
            //$ppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 0 AND ppv_subscriptions.is_ppv_bundle = 0 AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
            $ppvsql= "SELECT SUM(t.amount) as tot,DATE_FORMAT(t.transaction_date, '%Y-%m-%d') as trans_month,t.plan_id, t.movie_id, t.ppv_subscription_id FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE ps.is_voucher=0 AND u.is_developer !='". 1 ."' AND ps.studio_id='". Yii::app()->user->studio_id ."' AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND ps.is_advance_purchase = 0 AND ps.is_ppv_bundle = 0 AND t.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(t.transaction_date, '%Y-%m-%d'),t.plan_id,ps.movie_id";
            $advppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 1 AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
            $ppvbundlessql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_ppv_bundle = 1 AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
             
            
        }else{
            $sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,transactions.subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id) LEFT JOIN subscription_plans ON (user_subscriptions.plan_id = subscription_plans.id) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d')>= '" . $sDate . "' AND DATE_FORMAT(transaction_date, '%Y-%m-%d')<='".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.movie_id IN({$movie_ids}) AND transactions.currency_id = '".$currency_id."'  GROUP BY DATE_FORMAT(user_subscriptions.created_date, '%Y-%m'),plan_id, movie_id ";
            $ppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 0 AND ppv_subscriptions.is_ppv_bundle = 0  AND transactions.movie_id IN({$movie_ids}) AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
            $advppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 1 AND transactions.movie_id IN({$movie_ids}) AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
            $ppvbundlessql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) LEFT JOIN sdk_users ON (ppv_subscriptions.user_id  = sdk_users.id) LEFT JOIN films ON (ppv_subscriptions.movie_id = films.id) WHERE (DATE_FORMAT(transaction_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') AND transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_ppv_bundle =1 AND transactions.movie_id IN({$movie_ids}) AND transactions.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(transaction_date, '%Y-%m-%d'),plan_id, movie_id ";
             
            }
        
        //$sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id AND user_subscriptions.status = 1) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE  transactions.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY DATE_FORMAT(sdk_users.created_date, '%Y-%m'),plan_id, movie_id ";
        $data = $dbcon->createCommand($sql)->queryAll();
        $ppv_payment = 0;
        $monthly_sub = 0;
        $yearly_sub = 0;
        $adv_ppv_payment = 0;
        $ppv_bundles_payment=0;
        $ppvdata = $dbcon->createCommand($ppvsql)->queryAll();
        $advppvdata = $dbcon->createCommand($advppvsql)->queryAll();
        $ppvbundlesdata = $dbcon->createCommand($ppvbundlessql)->queryAll();
        if(!isset(Yii::app()->user->is_partner)){
            if(isset($plans) && !empty($plans)){
                if ($data) {
                    foreach ($data AS $key => $val) {
                        if (isset($val['subscription_id']) && trim($val['subscription_id'])) {
                            $temp_trans = $val['trans_month'];
                            $temp_trans = explode('-', $temp_trans);
                            $temp_create = $val['created_month'];
                            $temp_create = explode('-', $temp_create);
                            $diff = intval($temp_trans[1])-intval($temp_create[1]);
                            if($diff!=0){
                                $arr[1][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $studioPlans[$val['plan_id']])).'_'.$val['plan_id']] += $val['tot'];
                            }else{
                                $ydiff = intval($temp_trans[0])-intval($temp_create[0]);
                                if($ydiff !=0){
                                    $arr[1][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $studioPlans[$val['plan_id']])).'_'.$val['plan_id']] += $val['tot'];
                                }else{
                                    $tot = $val['tot'] ? $val['tot'] : 0;
                                    $arr[0][preg_replace('/[^a-zA-Z]/',str_replace(' ', '', $studioPlans[$val['plan_id']])).'_'.$val['plan_id']] += $val['tot'];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (isset($ppvdata) && !empty($ppvdata)) {
            foreach ($ppvdata AS $key => $val) {
                $ppv_payment += $val['tot'];
            }
        }
        if(!isset(Yii::app()->user->is_partner)){ 
        $arr[0]['ppv'] = $ppv_payment;
        }else{
           
            $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
             $arr[0]['ppv'] = $ppv_payment*($percentageshare/100);

            
            
        }
         if (isset($ppvbundlesdata) && !empty($ppvbundlesdata)) {
            foreach ($ppvbundlesdata AS $key => $val) {
                $ppv_bundles_payment += $val['tot'];
            }
        }
        $arr[0]['ppv_bundles'] = $ppv_bundles_payment;
        
        if (isset($advppvdata) && !empty($advppvdata)) {
            foreach ($advppvdata AS $key => $val) {
                $adv_ppv_payment += $val['tot'];
            }
        }
        $arr[0]['advppv'] = $adv_ppv_payment;
        /* Physical goods @author manas@muvi.com */
        if(Yii::app()->general->getStoreLink()){
            $pg_payment = 0;
            $pgsql = "SELECT SQL_CALC_FOUND_ROWS (0),COUNT(d.id) AS nod, SUM(d.`quantity`) AS quantity, SUM((d.`price` * d.quantity) - o.`discount`) AS revenue 
                     FROM `pg_order_details` AS d,pg_order AS o,pg_product p,transactions t 
                     WHERE d.order_id=o.id AND d.product_id=p.id AND o.transactions_id=t.id AND (DATE_FORMAT(t.transaction_date,'%Y-%m-%d') 
                     BETWEEN '".$sDate."' AND '".$eDate."') AND t.studio_id = '".Yii::app()->user->studio_id."' 
                     AND t.transaction_type=4 AND t.currency_id = '".$currency_id."' GROUP BY d.`product_id` ";
            $pgdata = $dbcon->createCommand($pgsql)->queryAll();
            if (isset($pgdata) && !empty($pgdata)) {
                foreach ($pgdata AS $key => $val) {
                    $pg_payment += $val['revenue'];
                }
            }
            $arr[0]['pg'] = $pg_payment;
        }
        /* END */
        /* Muvi cart order start */
        if(Yii::app()->general->getStoreLink()){
            $pg_payment = 0;
            $cart_sql = "SELECT SQL_CALC_FOUND_ROWS (0),COUNT(d.id) AS nod, SUM(d.`quantity`) AS quantity, SUM(o.`grand_total`) AS revenue 
                     FROM `pg_order_details` AS d,pg_order AS o,pg_product p,transactions t 
                     WHERE d.order_id=o.id AND d.product_id=p.id AND o.transactions_id=t.id AND (DATE_FORMAT(t.transaction_date,'%Y-%m-%d') 
                     BETWEEN '".$sDate."' AND '".$eDate."') AND t.studio_id = '".Yii::app()->user->studio_id."' 
                     AND t.transaction_type=4 AND t.currency_id = '".$currency_id."' GROUP BY d.`product_id` ";
            $ocdata = $dbcon->createCommand($cart_sql)->queryAll();
            if (isset($ocdata) && !empty($ocdata)) {
                foreach ($ocdata AS $key => $val) {
                    $oc_payment += $val['revenue'];
                }
            }
            $arr[0]['oc'] = $oc_payment;
            if($arr[0]['oc'] == ''){
               $arr[0]['oc'] = 0;  
            }
        }
        /* END */        
       /* $arr['monthly_sub'] = $monthly_sub;
        $arr['yearly_sub'] = $yearly_sub;
        $arr['ppv_payment'] = $ppv_payment;*/
        if (isset($_REQUEST['dt'])) {
            echo json_encode($arr);exit;
        } else {
            return json_encode($arr);
        }
    }
        
    /**
     * @method public cancelSubscription() List All cancelled subscriptions
     * @return HTML 
     * @author GDR<support@muvi.com>
     */
    function actionCancelSubscription() {
        
    }
        
        
    function actionDownloadCsv() {
        $type = (isset($_REQUEST['type']) && $_REQUEST['type']) ? $_REQUEST['type'] : 'subscriber';
        //echo $type;exit;
        $this->pageTitle = "Muvi | User Data";
        $this->breadcrumbs = array('Reports' => array('report/revenue'), 'User Data');
        $dbcon = Yii::app()->db;
        $lunchDate = Yii::app()->user->lunch_date;
            
        if ($type == 'subscriber') {
            $countSql = "SELECT id FROM user_subscriptions WHERE status=1 AND studio_id ='" . Yii::app()->user->studio_id . "' AND (profile_id !='' OR profile_id IS NOT NULL)";
            $sql = "SELECT us.*,u.email,u.created_date AS reg_dt,u.display_name as dname,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=1  AND u.is_developer !='1' AND us.studio_id='" . Yii::app()->user->studio_id . "' ORDER BY us.id DESC ";
        } elseif ($type == 'register') {
            $countSql = "SELECT id FROM sdk_users WHERE studio_id='" . Yii::app()->user->studio_id . "' AND is_developer!= '1' AND is_studio_admin != '1'";
            $sql = "SELECT u.email,u.created_date AS reg_dt,u.display_name as dname,u.source,ad.address1,ad.city,ad.state,ad.country,ad.phone  FROM sdk_users u  LEFT JOIN user_profiles AS up ON u.id = up.user_id LEFT JOIN user_addresses AS ad ON u.id=ad.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' ORDER BY u.id DESC ";
        } elseif ($type == 'cancel') {
            $countSql = "SELECT id FROM user_subscriptions WHERE status=0 AND studio_id ='" . Yii::app()->user->studio_id . "'";
            $sql = "SELECT us.*,u.email,u.created_date AS reg_dt,u.display_name as dname,ad.address1,ad.city,ad.state,ad.country,ad.phone FROM user_subscriptions us LEFT JOIN sdk_users u ON us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' LEFT JOIN user_profiles AS up ON us.user_id = up.user_id LEFT JOIN user_addresses AS ad ON us.user_id=ad.user_id WHERE us.status=0 AND us.studio_id='" . Yii::app()->user->studio_id . "'  ORDER BY us.id DESC";
        }

        $data = $dbcon->createCommand($sql)->queryAll();
        $dataCsv='';
        if ($data && $type == 'subscriber') {
            $headings = "Sl no" . "\t" . "Name" . "\t" . "Email" . "\t" . "City" . "\t" . "Country" . "\t" . "Member Since" . "\t" . "Subscription";
        $headings.= "\n";
           // echo Yii::app()->user->studio_id;exit;
            $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
            //echo "<pre>";print_r($plans);exit;
							$plan = $plans['plans'];
							foreach($plan AS $plank=>$planval ){
								$studioPlans[$planval['id']] = $planval['name'];
                                                               // print_r($studioPlans);exit;
							}
            // print_r($data);exit;
            $i = 1;

            for ($k = 0; $k < count($data); $k++) {
                $dataCsv .= $i . "\t" . $data[$k]['dname'] . "\t" . $data[$k]['email'] . "\t" . $data[$k]['city'] . "\t" . $data[$k]['country'] . "\t" .  date('M jS Y',strtotime($data[$k]['reg_dt'])). "\t" . $studioPlans[$data[$k]['plan_id']];
                $dataCsv .= "\n";
                $i++;
            }
        }
       else if ($data && $type == 'register') {
          // echo $type;exit;
           $headings = "Sl no" . "\t" . "Name" . "\t" . "Email" . "\t" . "City" . "\t" . "Country" . "\t" . "Member Since" . "\t" . "Source";
        $headings.= "\n";
           // echo Yii::app()->user->studio_id;exit;
            $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
          //echo "<pre>";print_r($plans);exit;
							$plan = $plans['plans'];
							foreach($plan AS $plank=>$planval ){
								$studioPlans[$planval['id']] = $planval['name'];
                                                               // print_r($studioPlans);exit;
							}
         //    print_r($data);exit;
            $i = 1;

            for ($k = 0; $k < count($data); $k++) {
                $dataCsv .= $i . "\t" . $data[$k]['dname'] . "\t" . $data[$k]['email'] . "\t" . $data[$k]['city'] . "\t" . $data[$k]['country'] . "\t" .  date('M jS Y',strtotime($data[$k]['reg_dt'])). "\t" . ($data[$k]['source']?$data[$k]['source']:'N/A');
                $dataCsv .= "\n";
                $i++;
            }
        }
        else if ($data && $type == 'cancel') {
             $headings = "Sl no" . "\t" . "Name" . "\t" . "Email" . "\t" . "City" . "\t" . "Country" . "\t" . "Cancel Date" . "\t" . "Subscription" ."\t". "Reason"  ."\t". "Comments";
        $headings.= "\n";
          //echo Yii::app()->user->studio_id;exit;
            $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
         //  echo "<pre>";print_r($plans);exit;
							$plan = $plans['plans'];
							foreach($plan AS $plank=>$planval ){
								$studioPlans[$planval['id']] = $planval['name'];
                                                               // print_r($studioPlans);exit;
							}
           //  print_r($data);exit;
            $i = 1;

            for ($k = 0; $k < count($data); $k++) {
                $dataCsv .= $i . "\t" . $data[$k]['dname'] . "\t" . $data[$k]['email'] . "\t" . $data[$k]['city'] . "\t" . $data[$k]['country'] . "\t" .  date('jS M Y',strtotime($data[$k]['cancel_date'])). "\t" .($data[$k]['plan_id']==1?'Montly':'Yearly') . "\t" . $cancelReason[$data[$k]['cancel_reason_id']]  . "\t" . $data[$k]['cancel_note'] ;
                $dataCsv .= "\n";
                $i++;
            }
        }

        $fileName =  $type ."_".date('m-d-Y'). ".xls";
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=" . $fileName);
        header("Content-Transfer-Encoding: binary");
        echo $headings . $dataCsv;
        exit;
    }
   
    /**
     * @method array_restructure() restructure the array for revenue graph data
     * @return array 
     * @author Sanjeev<support@muvi.com>
     */
    function array_restructure($data){
        $new = array();
        $i=0;
        foreach($data as $a){
            $u = $a['trans_month'];
            unset($a['trans_month']);
            $p = $a['plan_id'];
            unset($a['plan_id']);
            $found = false;
            foreach($new as $k=>$v){
                if(isset($new[$k]['trans_month']) && $new[$k]['trans_month'] == $u && isset($new[$k]['plan_id']) && $new[$k]['plan_id'] == $p){
                    $new[$k][] = $a;
                    $found = true;
                    break;
                }
            }
            if(!$found){
                $new[$i]['trans_month']=$u;
                $new[$i]['plan_id']=$p;
                $new[$i][]=$a;
                $i++;
            }
        }
        $abc = array();
        foreach ($new as $key => $value) {
            $trans_month = $value['trans_month'];
            $plan_id = $value['plan_id'];
            unset($value['trans_month']);
            unset($value['plan_id']);
            for($i=0;$i<count($value);$i++){
                $value[$i]['trans_month'] = $trans_month;
                $value[$i]['plan_id'] = $plan_id;
            }
        $abc[] = $value;
        }
        $finalArray = array();
        $i = 0;
        foreach ($abc as $key => $value) {
            $j = count($value)-1;
            $finalArray[$i]['tot'] = array_sum(array_column($value, 'tot'));
            $finalArray[$i]['trans_month']= $value[$j]['trans_month'];
            $finalArray[$i]['created_month']= $value[$j]['created_month'];
            $finalArray[$i]['plan_id']= $value[$j]['plan_id'];
            $finalArray[$i]['movie_id']= $value[$j]['movie_id'];
            $finalArray[$i]['ppv_subscription_id']= $value[$j]['ppv_subscription_id'];
            $i++;
        }
        return $finalArray;
    }
        
    public function actionGetMonthlySubscribers($dt = '',$report = 0,$searchKeySub = '',$content='', $isCSV = 0,$currency_id = 153)
    {
        $page_size = 20;
        $offset = 0;
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }    
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        }else if (isset($_REQUEST['dt']) && $_REQUEST['dt'] == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        } else if (!$dt) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_sub']) && trim($_REQUEST['search_value_sub'])){
            $searchKeySub = $_REQUEST['search_value_sub'];
        }
        $searchStr = '';
        if(trim($searchKeySub)){
            $searchStr = " AND (u.email LIKE '%".$searchKeySub."%' OR u.display_name LIKE '%".$searchKeySub."%' OR sp.name LIKE '%".$searchKeySub."%')";
        }
        
        $limit = " LIMIT ".$offset.",".$page_size;
        if (intval($isCSV)) {
            $limit = '';
        }
        
        if(trim($content['movie_id'])){
            $sql = "SELECT SQL_CALC_FOUND_ROWS us.user_id,us.status,us.start_date,us.end_date,us.cancel_date,t.amount,t.order_number,t.payment_method,t.currency_id,u.email,u.display_name as dname,us.start_date,us.created_date as us_created,u.created_date AS reg_dt,u.display_name,sp.name,sp.recurrence,ad.address1,ad.city,ad.state,ad.country,ad.phone,t.user_id as transaction_user_id  FROM user_subscriptions us LEFT JOIN (sdk_users u, subscription_plans sp) ON (us.user_id = u.id AND u.is_developer !='1' AND us.plan_id=sp.id) LEFT JOIN (user_profiles AS up, user_addresses AS ad) ON (us.user_id = up.user_id AND us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND us.plan_id=sp.id) WHERE (t.transaction_type=1 OR t.transaction_type=7) AND us.studio_id='" . Yii::app()->user->studio_id . "' AND us.currency_id = ".$currency_id." AND movie_id IN({$content['movie_id']}) AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." AND u.status=1 GROUP BY us.id ORDER BY u.email ".$limit;
        }else{
         $sql="select SQL_CALC_FOUND_ROWS us.user_id,us.status,us.start_date,us.end_date,us.cancel_date,t.amount,t.order_number,t.payment_method,t.currency_id,u.email,u.display_name as dname,us.start_date,us.created_date as us_created,u.created_date AS reg_dt,u.display_name,sp.name,sp.recurrence,t.user_id as transaction_user_id from transactions t , user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id = u.id) left join subscription_plans sp on us.plan_id=sp.id where t.user_id=us.user_id and us.plan_id=t.plan_id  and t.subscription_id=us.id and t.currency_id = ".$currency_id." and (t.transaction_type=1 OR t.transaction_type=7) AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." AND u.status=1  GROUP BY us.id ORDER BY u.email ".$limit;
        }
       
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $resArray['count'] = $count; 
        $resArray['page_size'] = $page_size;
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            
            //echo json_encode($resArray);exit;
            $this->renderPartial('subscriber', array('userdata' =>$resArray,'subscription' => $data,'subscription_count'=>$count,'page'=>$_REQUEST['page'],'page_size' => $page_size));exit;
        } else {
            return $resArray;
        }
    }
        
    public function actionGetMonthlyPpvSubscribers($dt = '',$report = 0,$searchKeyPpv = '',$content='', $isCSV = 0,$currency_id = 153)
    {
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_ppv']) && trim($_REQUEST['search_value_ppv'])){
            $searchKeyPpv = $_REQUEST['search_value_ppv'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
            $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR u.display_name LIKE "%'.$searchKeyPpv.'%" OR f.name LIKE "%'.$searchKeyPpv.'%")';
        }
        
        $limit = " LIMIT ".$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        }
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
	    $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
           $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(!isset(Yii::app()->user->is_partner)){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email,u.display_name as dname, ps.start_date,ps.season_id,ps.episode_id,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE ps.is_voucher=0 AND u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 0 AND ps.is_ppv_bundle = 0 AND t.currency_id = '.$currency_id.$searchStr.' ORDER BY u.email '.$limit;
        }else{
             $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE ps.is_voucher=0 AND u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 0 AND ps.is_ppv_bundle = 0 AND  t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC '.$limit;
        
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $resArray['count'] = $count; 
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            //echo json_encode($resArray);exit;
            $this->renderPartial('ppvsubscriber', array('ppv_subscription' => $data,'ppv_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size,'percentageshare'=>$percentageshare ));exit;
        } else {
            return $resArray;
        }
    }
    
    
      public function actionGetMonthlyPpvbundlesSubscribers($dt = '',$report = 0,$searchKeyPpv = '',$content='', $isCSV = 0,$currency_id = 153)
    {
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_ppv']) && trim($_REQUEST['search_value_ppv'])){
            $searchKeyPpv = $_REQUEST['search_value_ppv'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
           $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR u.display_name LIKE "%'.$searchKeyPpv.'%" OR f.name LIKE "%'.$searchKeyPpv.'%")';
     
            }
        
        $limit = " LIMIT ".$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        }
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(!isset(Yii::app()->user->is_partner)){
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email,u.display_name as dname, ps.start_date,p.title as name,t.amount FROM ppv_subscriptions ps,sdk_users u,transactions t,ppv_plans p WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND p.id = ps.ppv_plan_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_ppv_bundle = 1 AND t.currency_id = '.$currency_id.$searchStr.' ORDER BY u.email '.$limit;
       }else{
           $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,p.title as name,t.amount FROM ppv_subscriptions ps,sdk_users u,transactions t,ppv_plans p WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND p.id = ps.ppv_plan_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_ppv_bundle = 1 AND t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC '.$limit;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $resArray['count'] = $count; 
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            //echo json_encode($resArray);exit;
            $this->renderPartial('ppvbundlessubscriber', array('ppvbundles_subscription' => $data,'ppvbundles_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size));exit;
        } else {
            return $resArray;
        }
    }
    
    
       public function actionGetMonthlyVoucher($dt = '',$report = 0,$searchKeyPpv = '',$content='', $isCSV = 0,$currency_id = 153)
    {
         // echo $_REQUEST['currency_id']; 
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_voucher']) && trim($_REQUEST['search_value_voucher'])){
            $searchKeyPpv = $_REQUEST['search_value_voucher'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
           $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR gu.email LIKE "%'.$searchKeyPpv.'%" OR v.voucher_code LIKE "%'.$searchKeyPpv.'%")';
     
            }
        
        $limit = " LIMIT ".$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        }
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(!isset(Yii::app()->user->is_partner)){
        //$sql='SELECT SQL_CALC_FOUND_ROWS  Sv.created_date,Sv.user_id,u.email,v.voucher_code,Sv.movie_id,Sv.season_id,Sv.episode_id from ppv_subscriptions Sv LEFT JOIN sdk_users u on Sv.user_id=u.id LEFT JOIN voucher v on v.id=Sv.ppv_plan_id  where Sv.is_voucher=1 and Sv.studio_id="'.Yii::app()->user->studio_id.'"'. $searchStr .' AND (DATE_FORMAT(Sv.created_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'")';   
             $sql = 'SELECT SQL_CALC_FOUND_ROWS Sv.created_date,Sv.user_id,v.voucher_code,Sv.movie_id,Sv.season_id,Sv.episode_id,Sv.is_register_user, 
            CASE
                WHEN Sv.is_register_user = 1 THEN u.email 
                WHEN Sv.is_register_user = 0 THEN gu.email 
            END AS email
            FROM ppv_subscriptions Sv 
            LEFT JOIN voucher v ON v.id=Sv.ppv_plan_id 
            LEFT JOIN sdk_users u ON Sv.user_id=u.id
            LEFT JOIN guest_user gu ON Sv.user_id=gu.id
            WHERE Sv.is_voucher=1 AND Sv.studio_id="'.Yii::app()->user->studio_id.'"'. $searchStr .' AND (DATE_FORMAT(Sv.created_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'")';
        }else{
           //$sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,p.title as name,t.amount FROM ppv_subscriptions ps,sdk_users u,transactions t,ppv_plans p WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND p.id = ps.ppv_plan_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_ppv_bundle = 1 AND t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC '.$limit;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
       
         foreach ($data as $key => $val) {
            if (intval($val['season_id']) && intval($val['episode_id'])) {
                $content_id = $val['movie_id'] . ':' . $val['season_id'] . ':' . $val['episode_id'];
            } else if (intval($val['season_id']) && intval($val['episode_id']) == 0) {
                $content_id = $val['movie_id'] . ':' . $val['season_id'];
            } else {
                $content_id = $val['movie_id'];
            }
            $contentNamee = CouponOnly::model()->getContentInfo($content_id);
            $data[$key]['content_name'] = $contentNamee;
        }
        
        $resArray['count'] = $count; 
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            //echo json_encode($resArray);exit;
            $this->renderPartial('VoucherReport', array('ppvbundles_subscription' => $data,'ppvbundles_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size));exit;
        } else {
            return $resArray;
        }
    }
    
    public function actionGetMonthlyAdvPpvSubscribers($dt = '',$report = 0,$searchKeyPpv = '',$content='',$currency_id = 153)
    {
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_adv_ppv']) && trim($_REQUEST['search_value_adv_ppv'])){
            $searchKeyPpv = $_REQUEST['search_value_adv_ppv'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
            $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR u.display_name LIKE "%'.$searchKeyPpv.'%" OR f.name LIKE "%'.$searchKeyPpv.'%")';
        }
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(trim($content['movie_id']) && isset(Yii::app()->user->is_partner)){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 1 AND t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC LIMIT '.$offset.','.$page_size;
       
            $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
             $arr[0]['ppv'] = $ppv_payment*($percentageshare/100);
            
            
        }else{
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email,u.display_name as dname, ps.start_date,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 1 AND t.currency_id = '.$currency_id.$searchStr.' ORDER BY u.email DESC LIMIT '.$offset.','.$page_size;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $resArray['count'] = $count; 
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            //echo json_encode($resArray);exit;
            $this->renderPartial('ppvsubscriber', array('userdata' =>$resArray,'ppv_subscription' => $data,'ppv_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size,'percentageshare'=>$percentageshare));exit;
        } else {
            return $resArray;
        }
    }
    public function actionGetMonthlyPhysicalGoods($dt = '',$report = 0,$searchKeyPpv = '',$content='',$currency_id = 153,$isCSV = 0)
    {
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if(isset($_REQUEST['search_value_pg']) && trim($_REQUEST['search_value_pg'])){
            $searchKeyPpv = $_REQUEST['search_value_pg'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
            $searchStr = ' AND (d.name LIKE "%'.$searchKeyPpv.'%")';
        }
        $limit = ' LIMIT '.$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        }
        //$sql = 'SELECT SQL_CALC_FOUND_ROWS (0),count(d.id) as nod,d.`order_id`, d.`product_id`, d.`quantity`, d.`price`, d.`name`, d.`sub_total`,sum(d.price) as revenue,o.`order_number`, o.`studio_id`, o.`buyer_id`, o.`order_status`, o.`created_date`,p.status FROM `pg_order_details` as d,pg_order as o,pg_product p WHERE d.order_id=o.id AND d.product_id=p.id  AND o.studio_id="'.Yii::app()->user->studio_id.'" AND (DATE_FORMAT(o.created_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND p.currency_id = '.$currency_id.$searchStr.' GROUP BY d.`product_id` ORDER BY o.created_date DESC LIMIT '.$offset.','.$page_size;
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),count(d.id) as nod,o.total as item_cost,d.`order_id`,t.order_number,t.payment_method, d.`product_id`,o.shipping_cost AS shipping_charge,d.`price`,d.shipping_cost,o.discount, sum(d.`quantity`) as quantity, sum((d.`price` * d.quantity) - o.`discount`) as revenue, d.`name`,p.status,o.transactions_id,t.currency_id  FROM `pg_order_details` as d,pg_order as o,pg_product p,transactions t WHERE d.order_id=o.id AND d.product_id=p.id AND o.transactions_id=t.id AND (DATE_FORMAT(t.transaction_date,"%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND t.studio_id = "'.Yii::app()->user->studio_id.'"  AND t.transaction_type=4 AND t.currency_id = '.$currency_id.$searchStr.' GROUP BY d.`product_id` ORDER BY o.created_date DESC '. $limit;
        
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        
        for($i=0;$i<$count;$i++){
            $data[$i]['new_revenue'] = ($data[$i]['price']*$data[$i]['quantity'])-$data[$i]['discount'];
        }
        $resArray['count'] = $count; 
        $resArray['data'] = $data;
        if (isset($_REQUEST['dt']) && !$report) {
            $this->renderPartial('physicalrevenue', array('userdata' =>$resArray,'ppv_subscription' => $data,'ppv_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size));exit;
        } else {
            return $resArray;
        }
    }
    public function actionGetMuviCartOrders($dt = '',$report = 0,$searchKeyPpv = '',$content='',$currency_id = 153,$isCSV =0){
        $studio = $this->studio;
         if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $page_size = 20;
        $offset = 0; 
         if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }  
         if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }  
        if(isset($_REQUEST['search_value_order_cart']) && trim($_REQUEST['search_value_order_cart'])){
            $searchKeyPpv = $_REQUEST['search_value_order_cart'];
        }
        $searchStr = '';
        
        if(trim($searchKeyPpv)){
            $searchStr = ' AND (o.order_number LIKE "%'.$searchKeyPpv.'%" OR sdku.email LIKE "%'.$searchKeyPpv.'%" OR t.invoice_id LIKE "%'.$searchKeyPpv.'%")';
        }
        $limit = ' LIMIT '.$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        } 
        $sql = 'SELECT SQL_CALC_FOUND_ROWS (0), o.order_number as onumber,t.order_number,t.payment_method,SUM(od.quantity) AS no_of_items,o.`coupon_code`,o.discount as coupon_discount, o.shipping_cost AS shipping_charge, 
                SUM(od.sub_total) AS sub_total,od.coupon,od.discount,o.grand_total,o.order_reference_number,sdku.email,t.invoice_id
                FROM `pg_order_details` od, pg_order o,transactions t,sdk_users sdku
                WHERE od.order_id=o.id AND o.transactions_id=t.id AND o.buyer_id = sdku.id AND (DATE_FORMAT(t.transaction_date,"%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") 
                AND t.studio_id = "'.Yii::app()->user->studio_id.'"  AND t.transaction_type=4 AND t.currency_id = '.$currency_id.$searchStr.' GROUP BY o.id ORDER By o.id'.$limit;    
        $data = Yii::app()->db->createCommand($sql)->queryAll();  
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $resArray['count'] = $count; 
        $resArray['data'] = $data; 
          if (isset($_REQUEST['dt']) && !$report) {
            //echo json_encode($resArray);exit;
            $this->renderPartial('muvi_cart_order', array('userdata' =>$resArray,'ppv_subscription' => $data,'ppv_subscription_count'=>$count,'page'=>@$_REQUEST['page'],'page_size' => $page_size));exit;
        } else {
            return $resArray;
        }      
        
    }
    public function actionUserDetails()
    {
        $user_id = $_REQUEST['user_id'];
        $studio_id = Yii::app()->user->studio_id;
        $usr = new SdkUser();
        $user = $usr->findByPk($user_id);
        $res = Report::model()->userDetails($user_id);
        $userdata = $res['userdata'];
        $subscriptions = $res['subscriptions'];
        $userdata['signup_location'] = Yii::app()->common->getLocationTextFromData($userdata['signup_location']);        
        $custom_data = Yii::app()->general->getCustomFieldValue($studio_id, 1, $user_id,1);
        $chkAnnoucement = NewsletterSubscribers::model()->findBYAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id));
        $this->renderPartial('userprofile', array('user' => $user, 'studio_id' => $studio_id,'userdata' =>$userdata,'uservideodata' => $res['uservideodata'],'subscriptions' => $subscriptions,'currency_info'=>$res['currency_info'],'user_login_info'=>$res['userlogindata'], 'custom_data' => $custom_data,'chkAnnoucement'=>$chkAnnoucement));
    }
        
    public function actionUsersTypeData()
    {
        $type = $_REQUEST['type'];
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $res = Report::model()->usersTypeData($type,$sDate,$eDate,$offset,$page_size,$searchKey);
        $this->renderPartial('usersTypeData',array('data' => $res,'page_size'=>$page_size,'type'=>$type));
    }
        
    public function actionTypeData()
    {
        $res = array();
        $type = $_REQUEST['type'];
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $res = Report::model()->usersTypeData($type,$sDate,$eDate,$offset,$page_size);
        $res['type'] = $type;
        $this->renderPartial('typeData',array('data' => $res,'page_size'=>$page_size));
    }
        
    public function actionGetMonthlyUsers()
    {
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $res = Report::model()->usersReport($dt,$offset,$page_size,$searchKey);
        echo json_encode($res);
    }
        
    public function actionGetMonthlyLoginsCount()
    {
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value_login']) ? $_REQUEST['search_value_login'] : '';
        $logins = LoginHistory::model()->getLoginHistory($dt,$studio_id,$offset,$page_size,$searchKey);
        echo $logins['count'];
    }
        
    public function actionGetMonthlyLogins()
    {
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value_login']) ? $_REQUEST['search_value_login'] : '';
        $logins = LoginHistory::model()->getLoginHistory($dt,$studio_id,$offset,$page_size,$searchKey);
        //print_r($logins);
        if (isset($_REQUEST['dt'])) {
            $this->renderPartial('loginData', array('logins' => $logins,'page'=>$_REQUEST['page'],'page_size' => $page_size));
        } else {
            return $logins;
        }
    }
        
    public function actionSearchData()
    {
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] !='') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }else {
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value_search']) ? $_REQUEST['search_value_search'] : '';
        $results = SearchLog::model()->getSearchLog($start_date,$end_date,$offset,$page_size,$searchKey);
        if (isset($_REQUEST['dt'])) {
            $this->renderPartial('searchData', array('results' => @$results,'page'=>$_REQUEST['page'],'page_size' => $page_size));
        } else {
            return $results;
        }
    }
    
     public function actionViewData()
    {
        $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        
        $start_date=$dt->start;
        $end_date=$dt->end;
        $studio = $this->studio;
        $searchuser='';
        if(isset($_REQUEST['search_value_view'])&& $_REQUEST['search_value_view']!='')
        {
        
        $searchKey = isset($_REQUEST['search_value_view']) ? $_REQUEST['search_value_view'] : '';
       $searchuser= ' AND (u.email LIKE "%'.$searchKey.'%" OR u.display_name LIKE "%'.$searchKey.'%" OR f.name LIKE "%'.$searchKey.'%")';
        //$searchuser=" AND u.display_name LIKE '$searchKey%'";
        }     
       $sql = 'SELECT SQL_CALC_FOUND_ROWS v.played_length,v.created_date,v.device_type,v.played_from,u.display_name,v.user_id,u.email,f.name as movie_name,v.movie_id,v.trailer_id,m.full_movie,m.is_episode,f.content_types_id,m.series_number,m.episode_title FROM video_logs as v LEFT JOIN sdk_users as u on v.user_id=u.id LEFT JOIN films f on f.id=v.movie_id LEFT JOIN movie_streams as m on v.video_id=m.id WHERE (DATE_FORMAT(v.created_date, "%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") AND v.studio_id='.$studio_id . $searchuser.' LIMIT '.$offset.','.$page_size;
       $data = Yii::app()->db->createCommand($sql)->queryAll();
    
       $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
       $this->renderPartial('viewData',array('userdata'=>$data,'dateRange' => $dt,'studio_id'=>$studio_id,'count'=>$count,'user_id'=>$user_id));
    }
        
    public function actionUsagesDatePickerCounter(){
         $dt = $_REQUEST['dt'];
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $dt = stripcslashes($dt);
            $dt = json_decode($dt);
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        $searchKeyLogin = isset($_REQUEST['search_value_login']) ? $_REQUEST['search_value_login'] : '';
        $searchKeySearch = isset($_REQUEST['search_value_search']) ? $_REQUEST['search_value_search'] : '';
        $searchuser='';
        if(isset($_REQUEST['search_value_view']) && $_REQUEST['search_value_view']!='')
        {
        $searchKeyview = isset($_REQUEST['search_value_view']) ? $_REQUEST['search_value_view'] : '';
        $searchuser=" AND u.display_name LIKE '$searchKeyview%'";
        }     
        $studio_id = $this->studio->id;

        $sql = 'SELECT v.user_id,v.played_length,v.created_date,v.device_type,v.played_from,v.trailer_id,u.display_name,u.email,f.name as movie_name,v.movie_id,m.full_movie,m.is_episode,f.content_types_id,m.series_number,m.episode_title FROM video_logs as v LEFT JOIN sdk_users as u on v.user_id=u.id LEFT JOIN films f on f.id=v.movie_id LEFT JOIN movie_streams as m on v.video_id=m.id WHERE (DATE_FORMAT(v.created_date, "%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") AND v.studio_id='.$studio_id . $searchuser;
        $dataview = Yii::app()->db->createCommand($sql)->queryAll();
        $countdataView=count($dataview);         
        $results = SearchLog::model()->getSearchLogReport($dt,$searchKeySearch); 
        $countdataSearch=count($results);
        $logins = LoginHistory::model()->getLoginHistoryReport($dt,$studio_id,$searchKeyLogin);
        $countdataLogin=count($logins);
        
        $maxCount = max(array($countdataView, $countdataSearch, $countdataLogin));
        
		if ($maxCount > MAX_DOWNLOAD_SIZE) {
			echo 1;
		} else {
			echo 0;
		}
	}

    public function actionUserdataDatePickerCounter(){
        $type = $_REQUEST['type'];
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days')); 
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }       
            switch ($type){
                case 'registrations':
                    $sql = "SELECT tot.* FROM (SELECT DISTINCT u.id as user_id,u.email,u.source,u.created_date,u.display_name,u.signup_location,u.is_free,us.status FROM sdk_users u LEFT JOIN user_subscriptions AS us ON u.id = us.user_id WHERE u.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer != '1' AND is_studio_admin != '1' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.status DESC) AS tot GROUP BY tot.user_id ORDER BY tot.user_id DESC LIMIT ".$offset.",".$page_size;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;
                case 'subscriptions':
                    //$sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) LEFT JOIN transactions AS t ON (t.user_id = us.user_id AND t.studio_id ='" . Yii::app()->user->studio_id . "' AND (t.subscription_id IS NOT NULL OR t.subscription_id <> 0 OR t.subscription_id <> '')) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(u.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
                    $sql = "SELECT SQL_CALC_FOUND_ROWS us.*,u.email,us.start_date,u.created_date AS reg_dt,u.display_name,u.signup_location,ad.address1,ad.city,ad.state,ad.country,ad.phone,u.id as user_id  FROM user_subscriptions us LEFT JOIN sdk_users u ON (us.user_id=u.id AND us.studio_id='" . Yii::app()->user->studio_id . "' AND u.is_developer !='1') LEFT JOIN user_profiles AS up ON (us.user_id = up.user_id) LEFT JOIN user_addresses AS ad ON (us.user_id=ad.user_id) WHERE us.status=1 AND us.studio_id='" . Yii::app()->user->studio_id . "' AND (DATE_FORMAT(us.created_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY us.id DESC LIMIT ".$offset.",".$page_size;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;
                case 'ppvusers':
                    $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 0 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY start_date DESC LIMIT ".$offset.",".$page_size;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;

                case 'advppvusers':
                    $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT ppv_subscriptions.user_id,u.*,u.id as user_id FROM  ppv_subscriptions LEFT JOIN sdk_users AS u ON u.id = ppv_subscriptions.user_id WHERE ppv_subscriptions.studio_id = '" . Yii::app()->user->studio_id . "' AND is_advance_purchase = 1 AND (DATE_FORMAT(start_date, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." ORDER BY start_date DESC LIMIT ".$offset.",".$page_size;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;
                case 'cancelled':
                    $sql = "SELECT SQL_CALC_FOUND_ROWS user.* FROM (SELECT tot.*, u.id, u.email,u.studio_id,u.display_name,u.signup_location,u.created_date FROM (SELECT sub.* FROM (SELECT us.user_id, us.status,us.cancel_reason_id, us.cancel_date,us.cancel_note FROM user_subscriptions AS us  WHERE us.studio_id = '" . Yii::app()->user->studio_id . "' GROUP BY us.user_id ORDER BY us.status DESC) AS sub WHERE sub.status=0) AS tot , sdk_users u  WHERE u.id !='' AND u.id = tot.user_id AND u.is_developer != '1' AND u.is_studio_admin != '1' AND (DATE_FORMAT(tot.cancel_date, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."')) AS user LEFT JOIN cancel_reasons cr ON (user.cancel_reason_id = cr.id) WHERE user.studio_id = '" . Yii::app()->user->studio_id . "' ".$searchStr." ORDER BY user.cancel_date DESC LIMIT ".$offset.",".$page_size;
                     $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;
                case 'deleted':
                    $sql = "SELECT *, status as user_status,id as user_id FROM sdk_users WHERE is_developer != '1' AND is_studio_admin != '1' AND is_deleted=1 AND status=0 AND (DATE_FORMAT(deleted_at, '%Y-%m-%d') BETWEEN '" . $sDate . "' AND '".$eDate."') ".$searchStr." AND studio_id = '".Yii::app()->user->studio_id."'  ORDER BY deleted_at DESC LIMIT ".$offset.",".$page_size;
                    $res = Yii::app()->db->createCommand($sql)->queryAll();
                    break;
            }
            $countRes = count($res);
			
			if ($countRes > MAX_DOWNLOAD_SIZE) {
				echo 1;
			} else {
				echo 0;
			}
	}  
    
    public function actionVideoDatePickerCounter(){
         $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? trim($_REQUEST['device_type']) : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }        
        $is_partner = 0;
            $qstr = '';
            if (intval($is_partner)) {
                if(trim($movie_id)){
                    $qstr = ' AND v.movie_id IN ('.$movie_id.') ';

                } else {
                    return array();
                }
            }
            if(trim($searchKey)){
                //$searchStr = " AND (f.name LIKE '%".$searchKey."%' OR ms.episode_title LIKE '%".$searchKey."%')";
                $searchStr = " AND (f.name LIKE '%".$searchKey."%')";
            }
            $deviceStr = '';
            if(trim($deviceType)){
                $deviceStr = " AND v.device_type=".$deviceType;
            }        

            $sql="SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = ".Yii::app()->common->getStudiosId()." AND (DATE_FORMAT(v.created_date,'%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$qstr.$searchStr.$deviceStr." AND v.content_type = 1 GROUP BY v.video_id LIMIT ".$offset.",".$page_size;
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            $countResults=count($results);         
            if ($countResults > MAX_DOWNLOAD_SIZE) {
				echo 1;
			} else {
				echo 0;
			}
	}
        public function actionRevenuePpvDatePickerCounter(){ 
            $isCSV = 0;
            $studio = $this->studio;
            if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
                $currency_id = $_REQUEST['currency_id'];
            } else {
                $currency_id = $studio->default_currency_id;
            }
            $page_size = 20;
            $offset = 0;
            if (isset($_REQUEST['page'])) {
                $offset = ($_REQUEST['page'] - 1) * $page_size;
            }
            if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
                $dt = stripcslashes($_REQUEST['dt']);
                $dt = json_decode($dt);
                $sDate = $dt->start;
                $eDate = $dt->end;
            } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
                $eDate = date('Y-m-d');
                $daysgo = date('d')-1;
                $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            }
    
    
            if(isset($_REQUEST['search_value_ppv']) && trim($_REQUEST['search_value_ppv'])){
                $searchKeyPpv = $_REQUEST['search_value_ppv'];
            }
            $searchStr = '';
            if(trim($searchKeyPpv)){
                $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR u.display_name LIKE "%'.$searchKeyPpv.'%" OR f.name LIKE "%'.$searchKeyPpv.'%")';
            }

            $limit = " LIMIT ".$offset.','.$page_size;
            if (intval($isCSV)) {
                $limit = "";    
            }
            if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
                $content = Yii::app()->general->getPartnersContentIds();
                    $studio_id=Yii::app()->user->studio_id;
                $partner_id= Yii::app()->user->id;
                $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
                $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
               $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];
            }
            $movie_ids = -1;
            if(trim($content['movie_id'])){
                $movie_ids = $content['movie_id'];
            }

            if(!isset(Yii::app()->user->is_partner)){
                $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email,u.display_name as dname, ps.start_date,ps.season_id,ps.episode_id,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 0 AND ps.is_ppv_bundle = 0 AND t.currency_id = '.$currency_id.$searchStr.' ORDER BY u.email '.$limit;
            }else{
                 $sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,t.order_number,t.payment_method,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,f.name,t.amount FROM ppv_subscriptions ps,sdk_users u,films f,transactions t WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_advance_purchase = 0 AND ps.is_ppv_bundle = 0 AND  t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC '.$limit;
            }	

            $data = Yii::app()->db->createCommand($sql)->queryAll(); 
            $count = count($data);
            if(intval($count) > MAX_DOWNLOAD_SIZE){
				echo 1;
			} else {
				echo 0;
			}
	}	

    public function actionRevenueVoucherDatePickerCounter(){
        $isCSV = 0;
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if ($_REQUEST['dt'] == '' || !$_REQUEST['dt']) {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
		
		
        if(isset($_REQUEST['search_value_voucher']) && trim($_REQUEST['search_value_voucher'])){
            $searchKeyPpv = $_REQUEST['search_value_voucher'];
        }
        $searchStr = '';
        if(trim($searchKeyPpv)){
           $searchStr = ' AND (u.email LIKE "%'.$searchKeyPpv.'%" OR gu.email LIKE "%'.$searchKeyPpv.'%" OR v.voucher_code LIKE "%'.$searchKeyPpv.'%")';  
     
            }
        
        $limit = " LIMIT ".$offset.','.$page_size;
        if (intval($isCSV)) {
            $limit = "";    
        }
        if(isset(Yii::app()->user->is_partner) && !trim($content['movie_id'])){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $movie_ids = -1;
        if(trim($content['movie_id'])){
            $movie_ids = $content['movie_id'];
        }
        
        if(!isset(Yii::app()->user->is_partner)){
      //  $sql='SELECT SQL_CALC_FOUND_ROWS  Sv.created_date,Sv.user_id,u.email,v.voucher_code,Sv.movie_id,Sv.season_id,Sv.episode_id from ppv_subscriptions Sv LEFT JOIN sdk_users u on Sv.user_id=u.id LEFT JOIN voucher v on v.id=Sv.ppv_plan_id  where Sv.is_voucher=1 and Sv.studio_id="'.Yii::app()->user->studio_id.'"'. $searchStr .' AND (DATE_FORMAT(Sv.created_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'")';   
         $sql = 'SELECT SQL_CALC_FOUND_ROWS Sv.created_date,Sv.user_id,v.voucher_code,Sv.movie_id,Sv.season_id,Sv.episode_id,Sv.is_register_user, 
            CASE
                WHEN Sv.is_register_user = 1 THEN u.email 
                WHEN Sv.is_register_user = 0 THEN gu.email   
            END AS email
            FROM ppv_subscriptions Sv 
            LEFT JOIN voucher v ON v.id=Sv.ppv_plan_id 
            LEFT JOIN sdk_users u ON Sv.user_id=u.id
            LEFT JOIN guest_user gu ON Sv.user_id=gu.id
            WHERE Sv.is_voucher=1 AND Sv.studio_id="'.Yii::app()->user->studio_id.'"'. $searchStr .' AND (DATE_FORMAT(Sv.created_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'")';
             
         }else{
           //$sql = 'SELECT SQL_CALC_FOUND_ROWS (0),ps.*,u.id AS user_id,u.email, u.display_name as dname,ps.start_date,p.title as name,t.amount FROM ppv_subscriptions ps,sdk_users u,transactions t,ppv_plans p WHERE u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND p.id = ps.ppv_plan_id AND ps.id = t.ppv_subscription_id AND ps.user_id=u.id AND (DATE_FORMAT(ps.start_date, "%Y-%m-%d") BETWEEN "' . $sDate . '" AND "'.$eDate.'") AND ps.is_ppv_bundle = 1 AND t.currency_id = '.$currency_id.$searchStr.' AND ps.movie_id IN('.$movie_ids.') ORDER BY ps.start_date DESC '.$limit;
        }
        $data = Yii::app()->db->createCommand($sql)->queryAll(); 
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
		if (intval($count) > MAX_DOWNLOAD_SIZE) {
			echo 1;
		} else {
			echo 0;
		}
	}
 
    public function actionVideo() {
        $this->pageTitle = "Muvi | Content Analytics";
        $this->breadcrumbs = array('Analytics', 'Content Analytics');
        $this->headerinfo = "Content Analytics";
        $studio = $this->studio;
        $page_size = 20;
        $ip_address = CHttpRequest::getUserHostAddress();
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $default_currency = $studio->default_currency_id;
        $currency = Yii::app()->common->getStudioCurrency();
 
        $c = new Currency();
        $currency_details = $c->findByPk($default_currency);
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio->id,0,0,$dt,$searchKey,$deviceType);
        $netWatchedHour = VideoLogs::model()->getNetWatchedHour($studio->id,$dt,0,0,$searchKey,$deviceType);
        
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        //$total_bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio->id, $start_date, $end_date, $deviceType, 1);
        $bandwidth = VideoLogs::model()->getTotalBandwidth($studio->id, $dt,0,0,$searchKey,$deviceType);
		
        $json_data['watched_hour'] = $netWatchedHour['watched_hour'];
        $json_data['bandwidth'] = $bandwidth['buffered_size'];
        $json_data['buffer_duration'] = $bufferDuration;
        
        if (isset($_REQUEST['dt'])) {
            echo json_encode($json_data);
        } else {
            $watchedHour = VideoLogs::model()->getTotalWatchedHour($studio->id, '',0,$dt,0,$searchKey,$deviceType);
            
            $bandwidth = VideoLogs::model()->getTotalBandwidth($studio->id, $dt,0,0,$searchKey,$deviceType);
            $allWatchedHour = VideoLogs::model()->getAllWatchedHour($studio->id);
            $allBandwidth = VideoLogs::model()->getAllBandwidth($studio->id);
			$viewsDetails = $this->actionGetMonthlyViews();
            $viewsDetails = '';//$this->actionGetMonthlyViews();
            
            foreach ($allBandwidth as $row) {
                $date = date('Y-m', strtotime($row['created_date']));
                $x[] = $date;
                if (in_array($date, $x)) {
                    $bandwidthArr[$date] +=(float) $row['total_buffered_size'] / 1024 / 1024;
                } else {
                    $bandwidthArr[$date] = (float) $row['total_buffered_size'] / 1024 / 1024;
                }
            }
            foreach ($allWatchedHour as $row) {
                $date = date('Y-m', strtotime($row['created_date']));
                $x[] = $date;
                if (in_array($date, $x)) {
                    $watchArr[$date] +=number_format((float) (($row['played_length'])), 2, '.', '');
                } else {
                    $watchArr[$date] = number_format((float) (($row['played_length'])), 2, '.', '');
                }
            }

            $lunchDate = Yii::app()->user->lunch_date;
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['watch'][] = isset($watchArr[$i . "-" . $mont]) ? $watchArr[$i . "-" . $mont] : 0;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $watchgraph[] = array('name' => ucfirst('Watch Hours'), 'data' => array_reverse($arr['watch']));
            $bandwidthgraph[] = array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $graphData = json_encode($watchgraph);
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $user_start = Yii::app()->common->getStudioUserStartDate();
            $this->render('video', array('watchedHour' => $watchedHour, 'bandwidth' => $bandwidth, 'viewsDetails' => $viewsDetails, 'xdata' => json_encode(array_reverse($xdata)), 'graphData' => $graphData, 'bandwidthGraphData' => $bandwidthGraphData,'bufferDuration'=>$bufferDuration ,'page_size' => $page_size,'lunchDate'=>$user_start,'default_currency'=>$default_currency,'currency'=>$currency,'currency_details'=>$currency_details));
            //$this->render('video', array('watchedHour' => $watchedHour, 'viewsDetails' => $viewsDetails, 'xdata' => json_encode(array_reverse($xdata)), 'graphData' => $graphData, 'page_size' => $page_size,'lunchDate'=>$user_start));
        }
    }

    public function actionGetMonthlyViews() {
         $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
		} else {
            $currency_id = $studio->default_currency_id;
        }
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? trim($_REQUEST['device_type']) : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $views = '';
		$video_data = VideoLogs::model()->getVideoAllViewDetails($dt, $offset, $page_size, '', 0, $searchKey, $deviceType);
		$video_count_data = VideoLogs::model()->getVideoViewDetails($dt, $offset, $page_size, '', 0, $searchKey, $deviceType);
		foreach ($video_data['data'] as $key => $pricedata) {
			$contentTypes = $pricedata['content_type'];
			if ($pricedata['content_types_id'] == 3) {
				$movie_id = isset($pricedata['movie_id']) && trim($pricedata['movie_id']) ? $pricedata['movie_id'] : 0;
				$video_id = $pricedata['video_id'];
				$transaction_data = VideoLogs::model()->getPpvTransactionDetails($dt, $currency_id, $studio_id, $video_id);
				$bandwidth_data = VideoLogs::model()->getVideoBandwidthDetailsvideo($dt, $video_id, $deviceType, $contentTypes, $movie_id,1);
				$bandwidth_data_offline_view = VideoLogs::model()->getVideoBandwidthDetailsvideo($dt, $video_id, $deviceType, $contentTypes, $movie_id,2);
                		$trailer_data = VideoLogs::model()->getTrailerdata($dt, $movie_id, $studio_id,$deviceType);
				$embeded_data = VideoLogs::model()->getEmbededData($dt, $video_id, $studio_id, 3, $pricedata['trailer_id'],$deviceType);
				$get_unique_view = VideoLogs::model()->getgetuniqueview($dt, $movie_id, $video_id, $deviceType, $contentTypes);
			} else {
				$movie_id = isset($pricedata['movie_id']) && trim($pricedata['movie_id']) ? $pricedata['movie_id'] : 0;
				$transaction_data = VideoLogs::model()->getPpvTransactionDetailsmovie($dt, $currency_id, $studio_id, $movie_id);
				$bandwidth_data = VideoLogs::model()->getVideoBandwidthDetails($dt, $movie_id, $deviceType, $contentTypes,1);
				$bandwidth_data_offline_view = VideoLogs::model()->getVideoBandwidthDetails($dt, $movie_id, $deviceType, $contentTypes,2);
                		$trailer_data = VideoLogs::model()->getTrailerdata($dt, $movie_id, $studio_id,$deviceType);
				$embeded_data = VideoLogs::model()->getEmbededData($dt, $movie_id, $studio_id, 1, $pricedata['trailer_id'],$deviceType);
				$get_unique_view = VideoLogs::model()->getgetuniqueview($dt, $movie_id, $pricedata['video_id'], $deviceType, $contentTypes);
         }

			$video_data['data'][$key]['unique_view'] = $get_unique_view;
         $video_data['data'][$key]['u_embededcnt'] = $embeded_data;
         $video_data['data'][$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data['data'][$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];
         $video_data['data'][$key]['bandwidthdata_ofline'] = $bandwidth_data_offline_view['bandwidth'];
         $video_data['data'][$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data['data'][$key]['revenue'] = $transaction_data['transactionamt'];
        
			if ($pricedata['ppv_plan_id'] != 0) {
				if ($pricedata['content_types_id'] != 3) {
					$movie_id = isset($pricedata['movie_id']) && trim($pricedata['movie_id']) ? $pricedata['movie_id'] : 0;
					$price = VideoLogs::model()->videoPriceDetails($movie_id, $pricedata['ppv_plan_id'], $currency_id);
					if (isset($price) && !empty($price)) {
						$video_data['data'][$key]['subscribe_price'] = $price[0]['price_for_subscribed'];
						$video_data['data'][$key]['nonsubscribe_price'] = $price[0]['price_for_unsubscribed'];
						$video_data['data'][$key]['is_advance_purchase'] = $price[0]['is_advance_purchase'];
        }     
				} elseif ($pricedata['content_types_id'] == 3) {
					$movie_id = isset($pricedata['movie_id']) && trim($pricedata['movie_id']) ? $pricedata['movie_id'] : 0;
					$price = VideoLogs::model()->videoPriceDetailsMultipart($movie_id, $pricedata['ppv_plan_id'], $currency_id);
      
					if (isset($price) && !empty($price)) {
						$video_data['data'][$key]['subscribe_price'] = $price[0]['episode_subscribed'];
						$video_data['data'][$key]['nonsubscribe_price'] = $price[0]['episode_unsubscribed'];
						$video_data['data'][$key]['is_advance_purchase'] = $price[0]['is_advance_purchase'];
        }     
        }
			} else {
				$movie_id = isset($pricedata['movie_id']) && trim($pricedata['movie_id']) ? $pricedata['movie_id'] : 0;
				$price = VideoLogs::model()->videoPriceDetailsadvance($movie_id, $currency_id);
				if ((isset($price) && !empty($price)) && (($price[0]['is_advance_purchase'] == 1))) {
					$video_data['data'][$key]['subscribe_price'] = $price[0]['price_for_subscribed'];
					$video_data['data'][$key]['nonsubscribe_price'] = $price[0]['price_for_unsubscribed'];
					$video_data['data'][$key]['is_advance_purchase'] = $price[0]['is_advance_purchase'];
         }
        }    
        }
		
		$trailer_data = BufferLogs::model()->getTrailerVideoDetails($studio_id, '', 0, $dt, $searchKey, $deviceType, $offset, $page_size);
        $datappv = Yii::app()->general->monetizationMenuSetting($studio_id);
        $video_count = array();
       
		foreach ($video_count_data['data'] as $count) {
            $video_count[$count['movie_id']] = $count;
        }
        $bandwidthData = array();
        $transactionData = array();
		foreach ($transaction_data as $transaction) {
            $transactionData[$transaction['movie_id']] = $transaction;
        }
        $trailerData = array();
		foreach ($trailer_data as $trailer) {
            $trailerData[$trailer['movie_id']] = $trailer;
        }
        if (isset($_REQUEST['dt'])) {
            if (isset($_REQUEST['dt'])) {
                $dt = stripcslashes($_REQUEST['dt']);
                $dt = json_decode($dt);
            } else if (!$dt) {
                $dt = '';
            }
			$this->renderPartial('videoViewData', array('viewsDetails' => $video_data['data'], 'transaction_data' => $transactionData, 'trailer_data' => $trailerData, 'video_count' => $video_count, 'bandwidth_data' => $bandwidthData, 'dateRange' => $dt, 'currency_id' => $currency_id, 'studio_id' => $studio_id, 'monetization' => $datappv, 'count' => $video_data['count']));
        } else {
            return $views;
        }
    }
        
    public function actionVideoDetails(){
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if ((isset($_REQUEST['start']) && trim($_REQUEST['start'])) && (isset($_REQUEST['end']) && trim($_REQUEST['end']))) {
            $dt = array('start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']);
            $dt = (object) $dt;
        }else if(!$dt){
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,0,0,$dt,$searchKey,$deviceType,$offset,$page_size);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,0,0,$dt='',$searchKey,$deviceType,$offset,$page_size);
        }
        $std_id = isset($videoTableData['data'][0]['studio_id'])?$videoTableData['data'][0]['studio_id']:$studio_id;
        if($studio_id == $std_id){
            $is_self = 1;
            if(isset($videoTableData['count']) && !$videoTableData['count']){
                $contentDetails = BufferLogs::model()->getContentDetails($studio_id,0,0);
            }
        }else{
            $is_self = 0;
        }
        
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,0,0,$dt,$searchKey,$deviceType);
        $bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio_id,0,0,$dt,$searchKey,$deviceType); 
        
        
        if($dt == ''){
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }else{
            $start_date = $dt->start;
            $end_date = $dt->end;
        }
        
        $total_bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $start_date, $end_date, $deviceType, 1);
        
        $total_bandwidth['is_bandwidth'] = 1;
        $bandwidth_price = Yii::app()->aws->getBandwidthPriceForReport($total_bandwidth);
        $total_bandwidth_cost = (isset($bandwidth_price['price']) && abs($bandwidth_price['price']) >= 0.00001) ? $bandwidth_price['price'] : '';
        
        if (isset($_REQUEST['dt'])) {
            $json_data['bandwidth'] = $total_bandwidth['raw_bandwidth'];//$bandwidth_price['total_bandwidth'];
            $json_data['buffer_duration'] = $bufferDuration;
            $json_data['total_price'] = $total_bandwidth_cost;
            echo json_encode($json_data);
        }else{
            $allBandwidth = VideoLogs::model()->getAllBandwidthMovie($studio_id,0,0,$searchKey,$deviceType);
            foreach ($allBandwidth as $row) {
                $date = date('Y-m',strtotime($row['created_date']));
                $x[] = $date;
                if(in_array($date, $x)){
                    $bandwidthArr[$date] +=(float)$row['total_buffered_size']/1024/1024;
                }else{
                    $bandwidthArr[$date] =(float)$row['total_buffered_size']/1024/1024;
                }
            }
            $lunchDate = Yii::app()->user->lunch_date;
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $bandwidthgraph[] =  array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $user_start = Yii::app()->common->getStudioUserStartDate();
            $this->render('videoDetails',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$hour_bandwidth, 'xdata' => json_encode(array_reverse($xdata)),'bandwidthGraphData' => $bandwidthGraphData,'is_self'=>$is_self,'bufferDuration'=>$bufferDuration,'dateRanges'=>$dt,'lunchDate'=>$user_start));
        }
    }
        
    public function actionGetMonthlyBandwidth(){
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,0,0,$dt,$searchKey,$deviceType,$offset,$page_size);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,0,0,$dt,$searchKey,$deviceType,$offset,$page_size);
        }
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,0,0,$dt,$searchKey,$deviceType);
        $this->renderPartial('bandwidthTable',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$hour_bandwidth));
    }
        
    function secondsToHour($seconds)
    {
        $ret = "";
            
        /*** get the hours ***/
        $hours = (intval($seconds) / 3600) % 24;
        if($hours > 0)
        {
            $ret .= $hours;
        }else{
            $ret .= '00:';
        }
            
        /*/*** get the minutes ***/
        $minutes = (intval($seconds) / 60) % 60;
        if($minutes > 0)
        {
            $ret .= $minutes;
        }else{
            $ret .= ':00';
        }
            
        /*** get the seconds ***/
        $seconds = intval($seconds) % 60;
        if ($seconds > 0) {
            $ret .= $seconds;
        }else{
            $ret .= ':00';
        }
            
        return $ret;
    }
        
    public function actionAddBufferLog() {
		$ip_address = CHttpRequest::getUserHostAddress();
        $drmBandwidthValue = 10 ; 
		if (Yii::app()->aws->isIpAllowed()) {
        if(isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log == 0) {
				return true;
			}
			$user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
			$movie_stream_data = movieStreams::model()->findByPk($_REQUEST['video_id']);
			if (isset($_REQUEST['u_id']) && $_REQUEST['u_id']) {
                     if(strtolower($_REQUEST['request_type']) == 'mped_dash'){
					//mpeg_dash
					$bandwidth_used = $_REQUEST['totDRMBandwidth'];
                    }else{
					//not mped_dash

					$res_size = json_decode($movie_stream_data->resolution_size, true);
					$video_duration = explode(':', $movie_stream_data->video_duration);
					$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
					$res = $_REQUEST['resolution'];
					$size = $res_size[$res];
					$played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
					$bandwidth_used = ($size / $duration) * $played_time;
				}
                //HARD_CODE_TEST_CASE
                //@$bandwidth_used = (50000000000/1024);
                //@$bandwidth_used = (0/1024);
				$unique_id = $_REQUEST['u_id'];
				$buff_log = BufferLogs::model()->findByAttributes(array('unique_id' => $_REQUEST['u_id'],
					'studio_id' => $this->studio->id,
					'user_id' => $user_id,
					'movie_id' => $_REQUEST['movie_id'],
					'resolution' => $_REQUEST['resolution']));
                    $buff_log_id = @$buff_log->id;
                    //Bandwidth exceeds 5gb
                   $bandwidthData = array();
                   $bandwidthData = $_REQUEST;
                   $bandwidthData['bandwidth_used'] = $bandwidth_used;
                    //Bandwidth check
                   $is_log = Yii::app()->aws->sentMailForWrongBandwidthLog($bandwidthData);
                    if (isset($buff_log) && !empty($buff_log) && $is_log == 1) {
                        $buff_log->end_time = $_REQUEST['end_time'];
                        $buff_log->buffer_size = $bandwidth_used;
                        $buff_log->played_time = $_REQUEST['end_time'] - $buff_log->start_time;
                        $buff_log->save();
                        $buff_log_id = $buff_log->id;
                    }
			} else {
                    if(strtolower($_REQUEST['request_type']) == 'mped_dash'){
					//mped_dash
					$bandwidth_used = $_REQUEST['totDRMBandwidth'];
                    }
                    else{
					//not mped_dash
					$res_size = json_decode($movie_stream_data->resolution_size, true);
					$video_duration = explode(':', $movie_stream_data->video_duration);
					$duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
					$res = $_REQUEST['resolution'];
					$size = $res_size[$res];
					$played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
					$bandwidth_used = ($size / $duration) * $played_time;
				}
				if (isset(Yii::app()->session['location']) && Yii::app()->session['location'] == 1) {
                            $buff_log = BufferLogs::model()->findByAttributes(array('studio_id' => $this->studio->id, 'user_id' => $user_id));
					$city = $buff_log->city;
					$region = $buff_log->region;
					$country = $buff_log->country;
					$country_code = $buff_log->country_code;
					$continent_code = $buff_log->continent_code;
					$latitude = $buff_log->latitude;
					$longitude = $buff_log->longitude;
				} else {
					$city = @$_SESSION[$studio_id]['city'];
					$region = @$_SESSION[$studio_id]['region'];
					$country = @$_SESSION[$studio_id]['country_name'];
					$country_code = @$_SESSION[$studio_id]['country'];
					$continent_code = @$_SESSION[$studio_id]['continent_code'];
					$latitude = @$_SESSION[$studio_id]['latitude'];
					$longitude = @$_SESSION[$studio_id]['longitude'];
				}

				$unique_id = md5(uniqid(rand(), true));
				$buff_log = new BufferLogs();
				$buff_log->studio_id = $this->studio->id;
				$buff_log->unique_id = $unique_id;
				$buff_log->user_id = $user_id;
				$buff_log->movie_id = $_REQUEST['movie_id'];
				$buff_log->video_id = $_REQUEST['video_id'];
				$buff_log->resolution = $_REQUEST['resolution'];
				$buff_log->start_time = $_REQUEST['start_time'];
				$buff_log->end_time = $_REQUEST['end_time'];
				$buff_log->played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
				$buff_log->buffer_size = $bandwidth_used;
				$buff_log->city = $city;
				$buff_log->region = $region;
				$buff_log->country = $country;
				$buff_log->country_code = $country_code;
				$buff_log->continent_code = $continent_code;
				$buff_log->latitude = $latitude;
				$buff_log->longitude = $longitude;

				$buff_log->ip = $ip_address;
				$buff_log->created_date = date('Y-m-d H:i:s');
				$buff_log->save();
				$buff_log_id = $buff_log->id;
				Yii::app()->session['location'] = 1;
			}
			$data['id'] = $buff_log_id;
			$data['u_id'] = $unique_id;
			echo json_encode($data);
		} else {
			return TRUE;
		}
    }

	public function actionAddNewBufferLog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed()) {
            if(isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log == 0) {
                return true;
            }              
                $user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                $movie_stream_data = movieStreams::model()->findByPk($_REQUEST['video_id']);
                $buff_log = BufferLogs::model()->findByAttributes(array( 'studio_id' => $this->studio->id,  'user_id' => $user_id));
                if (isset(Yii::app()->session['location']) && Yii::app()->session['location'] == 1) {
                    $city = $buff_log->city;
                    $region = $buff_log->region;
                    $country = $buff_log->country;
                    $country_code = $buff_log->country_code;
                    $continent_code = $buff_log->continent_code;
                    $latitude = $buff_log->latitude;
                    $longitude = $buff_log->longitude;
                } else {
                    $city = @$_SESSION[$studio_id]['city'];
                    $region = @$_SESSION[$studio_id]['region'];
                    $country = @$_SESSION[$studio_id]['country_name'];
                    $country_code = @$_SESSION[$studio_id]['country'];
                    $continent_code = @$_SESSION[$studio_id]['continent_code'];
                    $latitude = @$_SESSION[$studio_id]['latitude'];
                    $longitude = @$_SESSION[$studio_id]['longitude'];
                }
                $unique_id = md5(uniqid(rand(), true));
                if(strtolower($_REQUEST['request_type']) == 'mped_dash'){
                   //IF mped 
                   $bandwidth_used = $_REQUEST['totDRMBandwidth'];
                }
                else
                {
                    //Not mped      
                    $res_size = json_decode($movie_stream_data->resolution_size, true);
                    $video_duration = explode(':', $movie_stream_data->video_duration);
                    $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
                    $res = $_REQUEST['resolution'];
                    $size = $res_size[$res];
                    $played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
                    $bandwidth_used = ($size / $duration) * $played_time;
                }
                
                $buff_log = new BufferLogs();
                $buff_log->studio_id = $this->studio->id;
                $buff_log->unique_id = $unique_id;
                $buff_log->user_id = $user_id;
                $buff_log->movie_id = $_REQUEST['movie_id'];
                $buff_log->video_id = $_REQUEST['video_id'];
                $buff_log->resolution = $_REQUEST['resolution'];
                $buff_log->start_time = $_REQUEST['start_time'];
                $buff_log->end_time = $_REQUEST['end_time'];
                $buff_log->played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
                
                $buff_log->buffer_size = $bandwidth_used;
                
                $buff_log->city = $city;
                $buff_log->region = $region;
                $buff_log->country = $country;
                $buff_log->country_code = $country_code;
                $buff_log->continent_code = $continent_code;
                $buff_log->latitude = $latitude;
                $buff_log->longitude = $longitude;
                $buff_log->ip = $ip_address;
                $buff_log->created_date = date('Y-m-d H:i:s');
                $buff_log->save();
                $buff_log_id = $buff_log->id;
                $data['id'] = $buff_log_id;
                $data['u_id']= $unique_id;
                echo json_encode($data);

            } else {
            echo json_encode(array());
        }
    }

    public function actionGetRevenueReport()
    {
        $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
        if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){
            $content = Yii::app()->general->getPartnersContentIds();
        }
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $plan = $plans['plans'];
        $studioPlans = array();
        foreach($plan AS $plank=>$planval ){
            $studioPlans[$planval['id']] = $planval['name'];
        }
        $dt = $_REQUEST['dt'];
        $searchKeySub = isset($_REQUEST['search_sub']) ? $_REQUEST['search_sub'] : '';
        $searchKeyPpv = isset($_REQUEST['search_ppv']) ? $_REQUEST['search_ppv'] : '';
        $searchKeyAdv = isset($_REQUEST['adv_search']) ? $_REQUEST['adv_search'] : '';
        $searchKeyPpvbundles = isset($_REQUEST['search_ppvbundles']) ? $_REQUEST['search_ppvbundles'] : '';
        $searchKeyVoucher = isset($_REQUEST['searchVoucher']) ? $_REQUEST['searchVoucher'] : '';
        $SearchOrderCart = isset($_REQUEST['SearchOrderCart'])?$_REQUEST['SearchOrderCart']:'';
       
       
        if(!isset(Yii::app()->user->is_partner)){
            $headArr[0] = array('Subscriber','GATEWAY REF','Plan','Last Billed Amount','Last Billing Date','Next Billing Date','Active?','Canceled Date');
            $sheetName[0] = 'Subscription';
            $res = $this->actionGetMonthlySubscribers($dt,1,$searchKeySub, '', 1,$currency_id);
            $data[0] = array();
            $sheet = 1;
            foreach ($res['data'] as $value) {
				$user_email = (isset($row['email']) && trim($row['email']))?$row['email']:'Anonymous ';
				$payment_method = ($row['payment_method'] != '') ? $row['payment_method'].':'.$row['order_number']:'N/A';
                $data[0][] = array(
                    $user_email,
                    $payment_method,
                    $value['name'],
                    $value['amount'],
                    $value['transaction_date'],
                    $value['start_date'],
                    $value['status']?'Yes':'No',
                    $value['status']?'':$value['cancel_date']
                );
            }
        }
        $ppv_user_data = Report::model()->allUsersReport();
         
                $headArr[1] = array('User','GATEWAY REF','Amount','Date & Time','Content');
                $sheetName[1] = 'Pay-per-view';
                $sheet = 2;
                $resPpv = $this->actionGetMonthlyPpvSubscribers($dt,1,'',@$content, 1,$currency_id);
                if(isset(Yii::app()->user->is_partner))
                {
                $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share']; 
                }
              
                $data[1] = array();
                foreach($resPpv['data'] as $row){
                      
                $Moviename="";
                $fnameSeason="";
                $episodeName="";
                if(isset($row['name']) && !empty($row['name'])){
                   $Moviename= $row['name'];
                }
                if((isset($row['season_id']) && !empty($row['season_id']))){
                   $fnameSeason= "->Season #".$row['season_id']; 
                }
                if((isset($row['episode_id']) && !empty($row['episode_id']))){
                  
                   $episode_title=Yii::app()->pdf->getEpisodeName($row['episode_id']);
                   $episodeName= "->".$episode_title;
                }
                $fname=$Moviename.$fnameSeason.$episodeName; 
                
                
                    
               if(!isset(Yii::app()->user->is_partner))
                {
                 $totalamt=$row['amount'];
                }
                else{
                 $totalamt=$row['amount']*($percentageshare/100);  
                }
                
                    $date = new DateTime($row['start_date']);
                    $date->setTimezone(new DateTimeZone('GMT'));
                    $start_date = $date->format('F j, Y, g:ia'). ' GMT';
                    $data[1][] = array(
                        $row['email'],
                        ($row['payment_method'] != '')?$row['payment_method'].':'.$row['order_number']:'N/A',
                        $totalamt,
                        $start_date,
                        $fname
                    );
                }
       
         
        
            
                $headArr[2] = array('User','GATEWAY REF','Amount','Date & Time','Content');
                $sheetName[2] = 'Pre-Order';
                $sheet = 3;
                $resPpv = $this->actionGetMonthlyAdvPpvSubscribers($dt,1,'',@$content, 1,$currency_id);
                $data[2] = array();
                foreach($resPpv['data'] as $row){
                    $date = new DateTime($row['start_date']);
                    $date->setTimezone(new DateTimeZone('GMT'));
                    $start_date = $date->format('F j, Y, g:ia'). ' GMT';
                    $data[2][] = array(
                        $row['email'],
                        ($row['payment_method'] != '')?$row['payment_method'].':'.$row['order_number']:'N/A',
                        $row['amount'],
                        $start_date,
                        $row['name']
                    );
            }
        
          if(!isset(Yii::app()->user->is_partner)){  
                $headArr[3] = array('User','GATEWAY REF','Amount','Date & Time','Bundle Name');
                $sheetName[3] = 'Pay-per-view Bundles';
                $sheet = 4;
                $resPpv = $this->actionGetMonthlyPpvbundlesSubscribers($dt,1,$searchKeyPpvbundles,@$content, 1,$currency_id);
                $data[3] = array();
                foreach($resPpv['data'] as $row){
                    $date = new DateTime($row['start_date']);
                    $date->setTimezone(new DateTimeZone('GMT'));
                    $start_date = $date->format('F j, Y, g:ia'). ' GMT';
                    $data[3][] = array(
                        $row['email'],
                        ($row['payment_method'] != '')?$row['payment_method'].':'.$row['order_number']:'N/A',
                        $row['amount'],
                        $start_date,
                        $row['name']
                    );
                }
                 $headArr[4] = array('Voucher','User','Date & Time','Content Used On');
                $sheetName[4] = 'Voucher';
                $sheet = 5;
                $resPpv = $this->actionGetMonthlyVoucher($dt,1,$searchKeyVoucher,@$content, 1,$currency_id);
                $data[4] = array();
                 
            foreach($resPpv['data'] as $row){
				$user_email = (isset($row['email']) && trim($row['email']))?$row['email']:'Anonymous ';
            if (intval($row['season_id']) && intval($row['episode_id'])) {
                $content_id = $row['movie_id'] . ':' . $row['season_id'] . ':' . $row['episode_id'];
            } else if (intval($row['season_id']) && intval($row['episode_id']) == 0) {
                $content_id = $row['movie_id'] . ':' . $row['season_id'];
            } else {
                $content_id = $row['movie_id'];
            }
            $contentNamee = CouponOnly::model()->getContentInfo($content_id);
               $date = new DateTime($row['created_date']);
                $date->setTimezone(new DateTimeZone('GMT'));
                    $date= $date->format('F j, Y, g:ia'). ' GMT';
                    $data[4][] = array(
                        $row['voucher_code'],
                        $user_email,
                        $date,
                        $contentNamee
                    );
                }    
          }
          
        if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){  
           $headArr[5] = array('Item Name','GATEWAY REF','Active?','Item Price','Number of Orders','Revenue');
                $sheetName[5] = 'Physical Goods';
                $sheet = 6;
                $resPpv = $this->actiongetMonthlyPhysicalGoods($dt,1,$searchKeyPpvbundles,@$content, 1,$currency_id);
                $data[5] = array();
                foreach($resPpv['data'] as $row){
                    if($row['status']==1){$status="YES";}else{$status="NO";}
                    $data[5][] = array(
                        $row['name'],
                        ($row['payment_method'] != '')?$row['payment_method'].':'.$row['order_number']:'N/A',
                        $status,
                        $row['price'],
                        $row['nod'],
                        $row['revenue'] 
                    );
                }
        }
        if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){  
           $headArr[6] = array('ORDER #','GATEWAY REF','EMAIL ID','NO OF ITEMS','SUB TOTAL','COUPON','DISCOUNT','SHIPPING COST','TOTAL');
                $sheetName[6] = 'MuviKart - Orders';
                $sheet = 7;
                $resPpv = $this->actionGetMuviCartOrders($dt,1,$searchKeyPpvbundles,@$content,$currency_id,1);

                $data[6] = array();
                foreach($resPpv['data'] as $row){             
                    $data[6][] = array(
                        $row['onumber'],
                        ($row['payment_method'] != '')?$row['payment_method'].':'.$row['order_number']:'N/A',
                        $row['email'],
                        $row['no_of_items'],
                        $row['sub_total'],
                        $row['coupon'],
                        $row['coupon_discount'],
                        $row['shipping_charge'],
                        $row['grand_total']
                    );
                }
        }        
        $filename = 'revenue_report_'.date('Ymd_His');
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv'){
           $type = 'xls'; 
        }
        elseif(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }
        if(isset(Yii::app()->user->is_partner)){
            $data = array_values($data);
            $sheetName = array_values($sheetName);
            $headArr = array_values($headArr);
            $sheet = count($sheetName);
        }
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,$type);
        exit;
    }
    public function actionGetUserReport()
    {
      
        $studio_id = Yii::app()->user->studio_id;
        $dt = $_REQUEST['dt'];
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $typeofsubscription = isset($_REQUEST['typeofsubscription']) ? $_REQUEST['typeofsubscription'] : '';
        $registeredMobile = isset($_REQUEST['mobile_no']) ? $_REQUEST['mobile_no'] : '';
        $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
        $plan = $plans['plans'];
        $studioPlans = array();
        foreach($plan AS $plank=>$planval ){
            $studioPlans[$planval['id']] = $planval['name'];
        }
        $sheet = 1;
        $fields = CustomField::model()->findAllFields($studio_id, 1);
        $i = 0;
        $result = array();
        $field_ids = array();
        if(count($fields) > 0){
            foreach($fields as $field){
                $result[] = $field->field_name;
                $field_ids[] = $field->id;
            }
        }
        if($typeofsubscription!='cancelled'){
			if($studio_id==3715){
				$cols = array('User','Name','Type','Mobile Number','Country','Address','Registered On','Source');
			}else{
        $cols = array('User','Name','Type','Country','Address','Registered On','Source');
			}
        $cols = array_merge($cols, $result);
        $headArr[0] = $cols;        
        $sheetName[0] = ucfirst($typeofsubscription);
        $resReg = Report::model()->usersReportDataTypes($typeofsubscription,$registeredMobile,$sDate,$eDate,$searchKey);
        $data[0] = array();
        
        foreach ($resReg as $value) {
            $signup_location = Yii::app()->common->getLocationFromData($value['signup_location']);
            $address = $signup_location['city'].', '.$signup_location['region'];
            $country = $signup_location['country_name'];
            
            $type_ofsubscriptions= (isset($value['status']) && $value['status'] == 1) ? 'Subscriber': ((isset($type) && trim($type) == 'cancelled')?'Cancelled':(isset($value['is_free']) && $value['is_free'])?'Free User':'Free registration');
            $mobile_no = isset($value['mobile_number']) ? $value['mobile_number'] : '';
            $custom_field_vals = array();
            if(count($field_ids) > 0){
                foreach($field_ids as $field){
                    $custom_field_vals[] = implode(', ', Yii::app()->general->getCustomFieldsValue($studio_id, 1, $value['user_id'], $field));
                }
            }
           // $display_name=Yii::app()->general->sanitize($value['display_name']);
			if($studio_id==3715){
				$return_data = array(
					$value['email'],
					$value['display_name'],
					$type_ofsubscriptions,
					$mobile_no,
					$country,
					$address,
					$value['created_date'],
					$value['source']
				);
			}else{
				$return_data = array(
					$value['email'],
					$value['display_name'],
					$type_ofsubscriptions,
					$country,
					$address,
					$value['created_date'],
					$value['source']
				);
			}
            $return_data = array_merge($return_data, $custom_field_vals);
            $data[0][] = $return_data;
        }
        }
        else
        {
        $cols = array('User','Name','Country','Address','Registered On','Cancelled On','Cancel Person');
        $cols = array_merge($cols, $result);
        $headArr[0] = $cols;        
        
        $sheetName[0] = ucfirst($typeofsubscription);
        $resReg = Report::model()->usersReportDataTypes($typeofsubscription,$mobile_no,$sDate,$eDate,$searchKey);
        $data[0] = array();
        
        foreach ($resReg as $value) {
            $signup_location = Yii::app()->common->getLocationFromData($value['signup_location']);
            $address = $signup_location['city'].', '.$signup_location['region'];
            $country = $signup_location['country_name'];
           
           // $type_ofsubscriptions= (isset($value['status']) && $value['status'] == 1) ? 'Subscriber': ((isset($type) && trim($type) == 'cancelled')?'Cancelled':(isset($value['is_free']) && $value['is_free'])?'Free User':'Free registration');
            $custom_field_vals = array();
            if(count($field_ids) > 0){
                foreach($field_ids as $field){
                    $custom_field_vals[] = implode(', ', Yii::app()->general->getCustomFieldsValue($studio_id, 1, $value['user_id'], $field));
                }
            }
            $display_name=Yii::app()->general->sanitize($value['display_name']);
            $cancelnote=$value['reason'].$value['cancel_note'];            
            $return_data = array(
                $value['email'],
                $display_name,
                $country,
                $address,
                $value['created_date'],
                date('F d, Y',strtotime($value['cancel_date'])),
                $cancelnote
            );
            $return_data = array_merge($return_data, $custom_field_vals);
            $data[0][] = $return_data;
        }
        }
        $reportname=ucfirst($typeofsubscription);
        $filename = $reportname.'_user Report';
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv'){
           $type = 'xls'; 
        }
        elseif(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,$type);
        exit;
    }
        
    public function actionGetUsagesReport() {
		$dt = $_REQUEST['dt'];
		if ($dt == '') {
			$end_date = date('Y-m-d');
			$daysgo = date('d') - 1;
			$start_date = date('Y-m-d', strtotime('-' . $daysgo . ' days'));
		} else {
			$dt = stripcslashes($dt);
			$dt = json_decode($dt);
			$start_date = $dt->start;
			$end_date = $dt->end;
		}
		$searchKeyLogin = isset($_REQUEST['search_value_login']) ? $_REQUEST['search_value_login'] : '';
		$searchKeySearch = isset($_REQUEST['search_value_search']) ? $_REQUEST['search_value_search'] : '';
		$searchuser = '';
		if (isset($_REQUEST['search_value_view']) && $_REQUEST['search_value_view'] != '') {
			$searchKeyview = isset($_REQUEST['search_value_view']) ? $_REQUEST['search_value_view'] : '';
			$searchuser = " AND u.display_name LIKE '$searchKeyview%'";
		}
		$studio_id = $this->studio->id;
        $headArr[0] = array('User','Email','Content','Date And Time','Duration','Platform','Type');
        $headArr[1] = array('Keywords','Category','Searches');
        $headArr[2] = array('User','Email','Login At','Logout At','IP');
        $sheetName = array('View','Searches','Logins');
		$sheet = 3;
        $sql = 'SELECT v.user_id,v.played_length,v.created_date,v.device_type,v.played_from,v.trailer_id,u.display_name,u.email,f.name as movie_name,v.movie_id,m.full_movie,m.is_episode,f.content_types_id,m.series_number,m.episode_title FROM video_logs as v LEFT JOIN sdk_users as u on v.user_id=u.id LEFT JOIN films f on f.id=v.movie_id LEFT JOIN movie_streams as m on v.video_id=m.id WHERE (DATE_FORMAT(v.created_date, "%Y-%m-%d") BETWEEN "'.$start_date.'" AND "'.$end_date.'") AND v.studio_id='.$studio_id . $searchuser;
		$dataview = Yii::app()->db->createCommand($sql)->queryAll();
		$results = SearchLog::model()->getSearchLogReport($dt, $searchKeySearch);
		$logins = LoginHistory::model()->getLoginHistoryReport($dt, $studio_id, $searchKeyLogin);
		$data[0] = array();
		$data[1] = array();
		$data[2] = array();
        if(isset($dataview) && !empty($dataview)){
         foreach($dataview as $view){
             if($view['device_type']==1){
                      $device= 'Web';
                    }elseif($view['device_type']==2){   $device= 'Android';}elseif($view['device_type']==3){   $device= 'iOS';}elseif($view['device_type']==2){   $device= 'Roku';}
          if($view['content_types_id']!=3){ $moviename= $view['movie_name']; }else{ $moviename= $view['movie_name'].' -> Season '.$view['series_number'].' -> '.$view['episode_title']; }
             $duration=$this->timeFormat($view['played_length']);       
             if($view['played_from'] == 2) { $display_name = "User - Embed";} else if(($view['user_id']==0 && $view['trailer_id'] != '' && $view['movie_id'] > 0 && $view['played_from'] == 1) || ($view['user_id']==0 && $views['trailer_id'] == '' && $view['movie_id'] > 0 && $view['played_from'] == 1)){$display_name = "Anonymous";} else {$display_name = $view['display_name'];}
                if(($view['user_id']==0) || ($view['played_from'] ==2)) { $email = "Not Available";} else {$email = $view['email'];}
                    $type = ($view['trailer_id']!= '') ? 'Trailer' : 'Content';  
				$data[0][] = array(
                $display_name,
                $email,
					$moviename,
					$view['created_date'],
					$duration,
                $device,
                $type
				);
			}
		}
		if (isset($results) && !empty($results)) {
			foreach ($results as $row) {
				$data[1][] = array(
					$row['search_string'],
					ucfirst($row['object_category']),
					$row['search_count']
				);
			}
		}
		if (isset($logins) && !empty($logins)) {
			foreach ($logins as $value) {
				$data[2][] = array(
					$value['display_name'],
					$value['email'],
					$value['login_at'],
					$value['logout_at'],
					$value['ip']
				);
			}
		}
		$filename = 'search_login_report_' . date('Ymd_His');
		if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv') {
			$type = 'xls';
		} elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf') {
			$type = 'pdf';
		}
		
		Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
		exit;
	}

	public function actionGetVideoReport() {
       $dt = $_REQUEST['dt'];
        $monetization=$_REQUEST['monetization'];
        $studio_id = $this->studio->id;
        
        if((isset($monetization) && (($monetization & 2) || ($monetization & 16)))){
        $headArr[0] = array('Content Name','Number of Transactions' ,'Price','Type','Revenue','Duration','Total Views', 'Unique Views','Embed Views','Trailer Views' ,'Total Bandwidth', 'Download');
        }
        else
        {
        $headArr[0] = array('Content Name','Duration','Total Views', 'Unique Views','Embed Views','Total Bandwidth', 'Download');

        }
        $sheetName = array('Video');
        $sheet = 1;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        //$views = VideoLogs::model()->getViewsDetails($dt,$offset,$page_size,'',0,$searchKey,$deviceType);
        $views = '';
        $video_data = VideoLogs::model()->getVideoViewDetailsReport($dt,'',0,$searchKey,$deviceType);
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        //$video_count_data = VideoLogs::model()->getVideoViewDetails($dt,0,20,'',0,$searchKey,$deviceType);
       foreach($video_data as $key=>$pricedata){
         $contentTypes=$pricedata['content_type']; 
         if($pricedata['content_types_id']==3)
         {
         $movie_id=isset($pricedata['movie_id']) && trim($pricedata['movie_id'])?$pricedata['movie_id']:0;    
         $video_id=isset($pricedata['video_id']) && trim($pricedata['video_id'])?$pricedata['video_id']:0;
         $transaction_data = VideoLogs::model()->getPpvTransactionDetails($dt,$currency_id,$studio_id,$video_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetailsvideo($dt,$video_id,$deviceType,$contentTypes,$movie_id,1);
         $bandwidth_data_offline_view = VideoLogs::model()->getVideoBandwidthDetailsvideo($dt,$video_id,$deviceType,$contentTypes,$movie_id,2);
         $trailer_data = VideoLogs::model()->getTrailerdata($dt,$movie_id,$studio_id,$deviceType);
         $embeded_data = VideoLogs::model()->getEmbededData($dt,$video_id,$studio_id,3,$pricedata['trailer_id'],$deviceType);
         $get_unique_view = VideoLogs::model()->getgetuniqueview($dt,$movie_id,$pricedata['video_id'],$deviceType,$contentTypes);
         $trailer_data['viewcount'];
         $video_data[$key]['unique_view'] = $get_unique_view;
         $video_data[$key]['u_embededcnt'] = $embeded_data;
         $video_data[$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data[$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];
         $video_data[$key]['bandwidthdata_ofline'] = $bandwidth_data_offline_view['bandwidth'];
         $video_data[$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data[$key]['revenue'] = $transaction_data['transactionamt'];
         }
         else
         {
         $movie_id=isset($pricedata['movie_id']) && trim($pricedata['movie_id'])?$pricedata['movie_id']:0;   
         $transaction_data = VideoLogs::model()->getPpvTransactionDetailsmovie($dt,$currency_id,$studio_id,$movie_id);
         $bandwidth_data = VideoLogs::model()->getVideoBandwidthDetails($dt,$movie_id,$deviceType,$contentTypes,1);
         $bandwidth_data_offline_view = VideoLogs::model()->getVideoBandwidthDetails($dt,$movie_id,$deviceType,$contentTypes,2);
         $trailer_data = VideoLogs::model()->getTrailerdata($dt,$movie_id,$studio_id,$deviceType);
         $embeded_data = VideoLogs::model()->getEmbededData($dt,$movie_id,$studio_id,1,$pricedata['trailer_id'],$deviceType);
         $get_unique_view = VideoLogs::model()->getgetuniqueview($dt,$movie_id,$pricedata['video_id'],$deviceType,$contentTypes);
         $video_data[$key]['unique_view'] = $get_unique_view;
         $video_data[$key]['u_embededcnt'] = $embeded_data;
         $video_data[$key]['trailercount'] = $trailer_data['viewcount'];
         $video_data[$key]['bandwidthdata'] = $bandwidth_data['bandwidth'];
         $video_data[$key]['bandwidthdata_ofline'] = $bandwidth_data_offline_view['bandwidth'];
         $video_data[$key]['transactions'] = $transaction_data['transactioncount'];
         $video_data[$key]['revenue'] = $transaction_data['transactionamt'];
         }
        if($pricedata['ppv_plan_id']!=0){ 
         if($pricedata['content_types_id']!=3){
         $movie_id=isset($pricedata['movie_id']) && trim($pricedata['movie_id'])?$pricedata['movie_id']:0;   
        $price = VideoLogs::model()->videoPriceDetails($movie_id,$pricedata['ppv_plan_id'],$currency_id);
        if(isset($price) && !empty($price)){
          $video_data[$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }     
        }
        elseif($pricedata['content_types_id']==3){
        $movie_id=isset($pricedata['movie_id']) && trim($pricedata['movie_id'])?$pricedata['movie_id']:0;   
        $price = VideoLogs::model()->videoPriceDetailsMultipart($movie_id,$pricedata['ppv_plan_id'],$currency_id);
        if(isset($price) && !empty($price)){
          $video_data[$key]['subscribe_price']= $price[0]['episode_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['episode_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }     
        }
         }
         else
         {
         $movie_id=isset($pricedata['movie_id']) && trim($pricedata['movie_id'])?$pricedata['movie_id']:0;   
         $price = VideoLogs::model()->videoPriceDetailsadvance($movie_id,$currency_id);
        if(isset($price) && !empty($price)){
          $video_data[$key]['subscribe_price']= $price[0]['price_for_subscribed']; 
          $video_data[$key]['nonsubscribe_price']= $price[0]['price_for_unsubscribed']; 
          $video_data[$key]['is_advance_purchase']= $price[0]['is_advance_purchase']; 
        }    
        }
        }      
        //$bandwidth_data = VideoLogs::model()->getVideoBandwidthDetailsReport($dt);
        $transaction_data = VideoLogs::model()->getPpvTransactionDetailsReport($dt,$currency_id);
        $trailer_data = BufferLogs::model()->getTrailerVideoDetailsReports($studio_id,'',0,$dt,$searchKey,$deviceType,0);
        $video_count = array();
       
        foreach ($video_count_data['data'] as $count){
            $video_count[$count['movie_id']] = $count;
        }
       /* $bandwidthData = array();
        foreach ($bandwidth_data as $bandwidth){
            $bandwidthData[$bandwidth['movie_id']] = $bandwidth;
        }*/
        $transactionData = array();
        foreach ($transaction_data as $transaction){
            $transactionData[$transaction['movie_id']] = $transaction;
        }
        $trailerData = array();
        foreach ($trailer_data as $trailer){
                $trailerData[$trailer['movie_id']] = $trailer;
        }
        $data[0] = array();
        if(isset($video_data) && !empty($video_data)){
            foreach ($video_data as $views){
                    $address = Yii::app()->common->getLocationFromData($bandwidthData[$views['movie_id']]['location']);
                    $country = $address['country_name'];                
                    $bandwidth_consumed = $this->formatKBytes($views['bandwidthdata']*1024,2);
                    $bandwidth_consumed_ofline = $this->formatKBytes($views['bandwidthdata_ofline']*1024,2);
                    $device_type = 'Web';
                    switch ($views['device_type']){
                        case 1:
                            $device_type = 'Web';
                            break;
                        case 2:
                            $device_type = 'Android';
                            break;
                        case 3:
                            $device_type = 'iOS';
                            break;
                        case 4:
                            $device_type = 'Roku';
                            break;
                        Default:
                            $device_type = 'Web';
                            break;
                    }
                     $subscribe=trim($views['subscribe_price'])? Yii::app()->common->formatPrice($views['subscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                    $nonsubscribe=trim($views['nonsubscribe_price'])? Yii::app()->common->formatPrice($views['nonsubscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                   if($views['content_type']==2){
                       $pricedetails= 'N/A';
                   }else{
                   if(isset($views['subscribe_price']) && !empty($views['subscribe_price'])){ $pricedetails= $subscribe.'/'.$nonsubscribe; }else { $pricedetails= 'N/A';}
                   } 
                   if($views['content_type']==2){
                       $typedetails = 'N/A';
                   }else{
                    if(isset($views['is_advance_purchase']) && ($views['is_advance_purchase'])==0){ $typedetails= 'PPV';}elseif(isset($views['is_advance_purchase']) &&($views['is_advance_purchase'])==1){ $typedetails= 'PO';}else { $typedetails= 'N/A';}
                   }
                    if(isset($views['movie_id']) && $views['movie_id']){
                         if($views['content_type']==2){
                                  $trailer='(Trailer)';  
                                }
                                else{
                                   $trailer=''; 
                                }
                        if(@$views['content_types_id']==3){
                                    $content = $views['name']."-> Season ".$views['series_number']." -> ". ($views['episode_title']?$views['episode_title']:'Episode#-'.$views['episode_number']).$trailer;
                            }else{
                                    $content = $views['name'].$trailer;
                            }
                    }else{
                        $content = "Content is removed";
                    }
                    $video_count= (intval($views['viewcount']))?$views['viewcount']:0;
                    $unique_count=$views['unique_view'] ? $views['unique_view'] : 0;
                    $embeded_count = $views['u_embededcnt']? $views['u_embededcnt'] : '0';
                     if((isset($monetization) && (($monetization & 2) || ($monetization & 16)))){
                    //$contentrevenue=trim($transactionData[$views['movie_id']]['amount'])? Yii::app()->common->formatPrice($transactionData[$views['movie_id']]['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                        $transactiondatas=$views['transactions']? $views['transactions'] : 0; 
                         if($views['content_type']==2){$contentrevenue= 'N/A'; }else{ $contentrevenue= $views['revenue']? Yii::app()->common->formatPrice($views['revenue'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);} 
                        $trailercount= $views['trailercount'] ? $views['trailercount'] : 0;
                        $data[0][] = array(
                        $content,
                        $transactiondatas,
                        $pricedetails,
                        $typedetails,
                        $contentrevenue,
                        $this->timeFormat($views['duration']),
                        $video_count,
                        $unique_count,
                        $embeded_count,    
                        $trailercount,
                        $bandwidth_consumed,
                        $bandwidth_consumed_ofline,  
                    );
                     }
                     else
                     {
                      $data[0][] = array(
                        $content,
                        $this->timeFormat($views['duration']),
                        $video_count,
                        $unique_count,
                        $embeded_count,  
                        $bandwidth_consumed,
                        $bandwidth_consumed_ofline,
                          );
                     }
                    
            }
        }
        $filename = 'video_report_' . date('Ymd_His');
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv') {
            $type = 'xls';
        } elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf') {
            $type = 'pdf';
        }
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }

    public function actionGetVideoDetailsReport()
    {
        $dt = (isset($_REQUEST['dt']) && !empty($_REQUEST['dt'])) ? $_REQUEST['dt'] : '';
        $studio_id = $this->studio->id;
        $headArr[0] = array('Date & Time','Video','Resolution','Geography','Bandwidth','User');
        $sheetName = array('Video Details');
        $sheet = 1;
        $studio_id = $this->studio->id;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $videoData = BufferLogs::model()->getVideoDetailsReport($dt,$studio_id,0,0,$searchKey,$deviceType);
        $data[0] = array();
        foreach($videoData as $video){
            $address = Yii::app()->common->getLocationFromData($video['location']);
            $country = $address['country_name'];                         
            if(isset($video['content_type']) && $video['content_type'] == 2){
                $video_name = $video['name'].' - Trailer';
            }else{
                $video_name =  $video['name'];
            }
            $data[0][] = array(
                $video['created_date'],
                $video_name,
                $video['resolution'],
                $country,
                $video['buffer_size'],
                trim($video['display_name'])?$video['display_name']:'N/A',
            );
        }
        $filename = 'video_details_report_'.date('Ymd_His');
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv'){
           $type = 'xls'; 
        }
        elseif(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,$type);
        exit;
    }
    public function formatKBytes($bytes, $precision = 2) { 
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;
            
        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';
                
        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';
                
        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';
                
        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';
                
        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }
        
    public function actionGetSearchedSubscribers()
    {
        $key = $_REQUEST['search_value'];
        $sql = 'SELECT us.*,sp.name,u.email,us.start_date,transaction_date FROM user_subscriptions us,sdk_users u,subscription_plans sp,transactions t WHERE (u.email LIKE "%'.$key.'" OR sp.name LIKE "%'.$key.'") AND us.is_success=1  AND u.is_developer !="1" AND us.studio_id="'.Yii::app()->user->studio_id.'" AND t.studio_id="'.Yii::app()->user->studio_id.'" AND t.plan_id = us.plan_id AND us.user_id=u.id AND us.plan_id=sp.id AND us.user_id=t.user_id ORDER BY t.transaction_date,us.id DESC';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $this->renderPartial('subscriber', array('userdata' =>$data,'subscription' => $data));
        exit;
    }
        
    public function actionGetSearchedPpvSubscribers()
    {
        $key = $_REQUEST['search_value'];
        $sql = 'SELECT ps.*,u.id AS user_id,u.email,ps.start_date,f.name FROM ppv_subscriptions ps,sdk_users u,films f WHERE (u.email LIKE "%'.$key.'" OR f.name LIKE "%'.$key.'") AND u.is_developer !="1" AND ps.studio_id="'.Yii::app()->user->studio_id.'" AND f.id = ps.movie_id AND ps.user_id=u.id ORDER BY ps.id DESC';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $this->renderPartial('ppvsubscriber', array('userdata' =>$data,'ppv_subscription' => $data));
        exit;
    }
        
    public function actionGetSearchedUserTypeData()
    {
        $key = $_REQUEST['search_value'];
        $type = $_REQUEST['type'];
        $res = Report::model()->usersSearchedTypeData($type,$key);
        $this->renderPartial('usersTypeData',array('data' => $res,'type'=>$type));
        exit;
    }
        
    public function actionGetSearchedLogins()
    {
        $key = $_REQUEST['search_value'];
        $studio_id = $this->studio->id;
        $logins = LoginHistory::model()->getSearchedLoginHistory($studio_id,$key);
        $this->renderPartial('loginData', array('logins' => $logins));
        exit;
    }
        
    public function actionGetSearchedData()
    {
        $key = $_REQUEST['search_value'];
        $results = SearchLog::model()->getSearchedLog($key);
        $this->renderPartial('searchData', array('results' => @$results));
        exit;
    }
        
    public function actionGetSearchedViews()
    {
        $key = $_REQUEST['search_value'];
        $views = VideoLogs::model()->getSearchedViewsDetails($key);
        $this->renderPartial('videoViewData',array('viewsDetails' => $views));
        exit;
    }
        
    public function actionGetSearchedVideoDetails()
    {
        $key = $_REQUEST['search_value'];
        $movie_id = $_REQUEST['id'];
        $studio_id = $this->studio->id;
        $videoTableData = BufferLogs::model()->getSearchedVideoDetails($studio_id,$movie_id,$key);
        $this->renderPartial('bandwidthTable',array('videoTableData' => $videoTableData));
        exit;
    }
        
    public function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
            
        return false;
    }
    /* This function is for adding new user to front-end user table */
    public function actionNewUser(){
        if (isset($_REQUEST['data']['Admin']) && !empty($_REQUEST['data']['Admin'])) {
            $studio_id = $this->studio->id;
            $data = $_REQUEST['data']['Admin'];
            $enc = new bCrypt();
            $password = trim($data['password']);
            $encrypted_pass = $enc->hash($password);
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($data['email']),'studio_id'=>$studio_id));
            
            if (isset($users) && !empty($users)) {                
                $users->display_name = htmlspecialchars(trim($data['first_name']));
                $users->encrypted_password = $encrypted_pass;
                $users->is_free = 1;
                $users->is_deleted = 0;
                $users->save();
                $this->sendEmail($users, $password);
            }else{
                $ip_address = CHttpRequest::getUserHostAddress();
                $geo_data = Yii::app()->common->getVisitorLocation($ip_address);
                
                $user->is_studio_admin = 0;
                $user->email = trim($data['email']);
                $user->studio_id = $this->studio->id;
                $user->signup_ip = $ip_address;
                $user->signup_location = serialize($geo_data);
                $user->display_name = htmlspecialchars(trim($data['first_name']));
                $user->encrypted_password = $encrypted_pass;
                $user->status = 1;
                $user->created_date = new CDbExpression("NOW()");
                $user->is_free = 1;
                $user->save();
                $this->sendEmail($user, $password);
            }            
            //Send to email to that user and studio admin            
            Yii::app()->user->setFlash('success', 'New user has been added successfully.');
        }else {
            Yii::app()->user->setFlash('error', 'Invalid user data.');
        }
        $url = $this->createUrl('/monetization/endusersupport');
        $this->redirect($url);
    }
    public function actionCheckEmail(){
        $isExists = 0;
        if (isset($_REQUEST['email']) && $_REQUEST['email']) {
            $studio_id = $this->studio->id;
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']),'studio_id'=>$studio_id));
            if (isset($users) && !empty($users)) {
               $isExists = 1;
            }
        }
        $res = array('isExists' => $isExists);
        echo json_encode($res);exit;
    }
    function sendEmail($user = Null, $password = Null) {
        $studio_id = Yii::app()->user->studio_id;
        $std = new Studio();
        $studio = $this->studio;
        $site_url = 'http://' . $studio->domain;
        $siteLogo = Yii::app()->common->getLogoFavPath($studio_id);
        $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" alt="" /></a>';
        $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
        //Check facebook link given or not
        if ($studio->fb_link != '') {
            $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
        } else {
            $fb_link = '';
        }
        //Check twitter link given or not
        if ($studio->tw_link != '') {
            $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
        } else {
            $twitter_link = '';
        }
        //Check google plus link given or not
        if ($studio->gp_link != '') {
            $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
        } else {
            $gplus_link = '';
        }
        //Set variables for different email types
        $email = $user->email;
        $FirstName = $user->display_name;
        $StudioName = $studio->name;
        $domain = Yii::app()->getBaseUrl(true);  
        $Url = $site_url;
        $UserName = $user->email;
        $Password = $password;
        $Studiodomain = $studio->domain;
        $content = "SELECT subject,content FROM `notification_templates` WHERE type= 'free_user' AND studio_id=" . $studio_id." AND (language_id = ".$this->language_id." OR parent_id=0 AND id NOT IN (SELECT parent_id FROM `notification_templates` WHERE type= 'free_user' AND studio_id=" . $studio_id." AND language_id = ".$this->language_id."))";
        $content_data = Yii::app()->db->createCommand($content)->queryRow();
        if(isset($content_data) && empty($content_data)){
            $content = "SELECT subject,content FROM `notification_templates` WHERE type= 'free_user' AND studio_id= 0";
            $content_data = Yii::app()->db->createCommand($content)->queryRow();
        }
        $subject = (string) $content_data['subject'];
        $subject = htmlspecialchars($subject);
        eval("\$subject = \"$subject\";");
        $subject = htmlspecialchars_decode($subject);
        
        $content = (string) $content_data['content'];
        $breaks = array("<br />", "<br>", "<br/>");
        $content = str_ireplace($breaks, "\r\n", $content);
        $content = htmlspecialchars($content);
        eval("\$content = \"$content\";");
        $content = htmlspecialchars_decode($content); 
        
        $user_model = new User();
        $admin = $user_model->findByAttributes(array('studio_id' => Yii::app()->common->getStudiosId(), 'is_sdk' => 1, 'role_id' => 1));
        if (isset($admin) && !empty($admin)) {
            $from_mail = $admin->email;
        } else {
            $from_mail = 'info@' . $studio->domain;
        }
       
        $params = array(
            'website_name'=> $StudioName,
            'site_link'=> $site_link,
            'logo'=> $logo,
            'name'=> $FirstName,
            'url'=> $Url,
            'username'=> $UserName,
            'password'=> $Password,
            'mailcontent'=> $content
        );
        $message = array(
            'subject' => $subject,
            'from_email' => $from_mail,
            'from_name' => $studio->name,
            'to' => array(
                array(
                    'email' => $email,
                    'name' => $FirstName,
                    'type' => 'to'
                )
            )
        );
        $to=$email;
        $from=$from_mail;
        
        $template_name = 'sdk_user_welcome_new';
        $obj = new Controller();
        
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$to,$from,'','','',$StudioName); 

        /*$adminsubject = "A free user registered to '" . $studio->domain . "' site";
        $admincontent = '<p>Hi,</p>
                    <p>A free user has registered to ' . $studio->domain . ' Below are the details of the user</p>
                    <p style="display:block;margin:0 0 17px">
                        User Name: <strong><span mc:edit="email">' . $user->email . '</span></strong><br/>
                        Password: <strong><span mc:edit="password">' . $password . '</span></strong><br/>
                    </p>
                    <p>&nbsp;</p>
                    <p>Sincerely,</p>
                    <p>Team ' . $StudioName . '</p>';
        $params = array(
            'website_name'=> $StudioName,
            'site_link'=> $site_link,
            'logo'=> $logo,
            'name'=> $FirstName,
            'mailcontent'=> $admincontent
        );
        
        $template_name = 'sdk_user_welcome_new';
        $from_email='studio@muvi.com';
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/sdk_user_welcome_new',array('params'=>$params),true);              
         $returnVal=$this->sendmailViaAmazonsdk($thtml,$adminSubject,$adminGroupEmail,$from_email,'','','','Muvi Studio'); */

    }
    
    public function actionAddTrailerBufferLog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id']));
        $res_size = json_decode($movie_stream_data->resolution_size, true);
        $video_duration = explode(':', $movie_stream_data->video_duration);
        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
        $res = $_REQUEST['resolution'];
        $size = $res_size[$res];
        $played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
        $bandwidth_used = ($size / $duration) * $played_time;
        if (isset($_REQUEST['u_id']) && $_REQUEST['u_id']) {
            $unique_id = $_REQUEST['u_id'];
            $buff_log = BufferLogs::model()->findByAttributes(array('unique_id' => $_REQUEST['u_id'], 'studio_id' => $this->studio->id, 'movie_id' => $_REQUEST['movie_id'], 'resolution' => $_REQUEST['resolution']));
            if (isset($buff_log) && !empty($buff_log)) {
                $buff_log->end_time = $_REQUEST['end_time'];
                $buff_log->buffer_size = $bandwidth_used;
                $buff_log->played_time = $_REQUEST['end_time'] - $buff_log->start_time;
                $buff_log->save();
                $buff_log_id = $buff_log->id;
            }
        } else {
            $city = @$_SESSION['city'];
            $region = @$_SESSION['region'];
            $country = @$_SESSION['country_name'];
            $country_code = @$_SESSION['country'];
            $continent_code = @$_SESSION['continent_code'];
            $latitude = @$_SESSION['latitude'];
            $longitude = @$_SESSION['longitude'];

            $unique_id = md5(uniqid(rand(), true));
            $buff_log = new BufferLogs();
            $buff_log->studio_id = $this->studio->id;
            $buff_log->unique_id = $unique_id;
            $buff_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
            $buff_log->movie_id = $_REQUEST['movie_id'];
            $buff_log->video_id = $movie_stream_data->id;
            $buff_log->resolution = $_REQUEST['resolution'];
            $buff_log->start_time = $_REQUEST['start_time'];
            $buff_log->end_time = $_REQUEST['end_time'];
            $buff_log->played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
            $buff_log->buffer_size = $bandwidth_used;
            $buff_log->content_type = 2; //1 for content and 2 for trailer
            $buff_log->city = $city;
            $buff_log->region = $region;
            $buff_log->country = $country;
            $buff_log->country_code = $country_code;
            $buff_log->continent_code = $continent_code;
            $buff_log->latitude = $latitude;
            $buff_log->longitude = $longitude;
            $buff_log->ip = $ip_address;
            $buff_log->created_date = date('Y-m-d H:i:s');
            $buff_log->save();
            $buff_log_id = $buff_log->id;
            Yii::app()->session['location'] = 1;
        }
        $data['id'] = $buff_log_id;
        $data['u_id'] = $unique_id;
        echo json_encode($data);
    }

    public function actionAddNewTrailerBufferLog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id' => $_REQUEST['movie_id']));
        $res_size = json_decode($movie_stream_data->resolution_size, true);
        $video_duration = explode(':', $movie_stream_data->video_duration);
        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);
        $res = $_REQUEST['resolution'];
        $size = $res_size[$res];
        $played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
        $bandwidth_used = ($size / $duration) * $played_time;
        $unique_id = md5(uniqid(rand(), true));

        $city = @$_SESSION['city'];
        $region = @$_SESSION['region'];
        $country = @$_SESSION['country_name'];
        $country_code = @$_SESSION['country'];
        $continent_code = @$_SESSION['continent_code'];
        $latitude = @$_SESSION['latitude'];
        $longitude = @$_SESSION['longitude'];

        $buff_log = new BufferLogs();
        $buff_log->studio_id = $this->studio->id;
        $buff_log->unique_id = $unique_id;
        $buff_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $buff_log->movie_id = $_REQUEST['movie_id'];
        $buff_log->video_id = $movie_stream_data->id;
        $buff_log->resolution = $_REQUEST['resolution'];
        $buff_log->start_time = $_REQUEST['start_time'];
        $buff_log->end_time = $_REQUEST['end_time'];
        $buff_log->played_time = $_REQUEST['end_time'] - $_REQUEST['start_time'];
        $buff_log->buffer_size = $bandwidth_used;
        $buff_log->city = $city;
        $buff_log->region = $region;
        $buff_log->country = $country;
        $buff_log->country_code = $country_code;
        $buff_log->continent_code = $continent_code;
        $buff_log->latitude = $latitude;
        $buff_log->longitude = $longitude;
        $buff_log->content_type = 2; //1 for content and 2 for trailer
        $buff_log->ip = $ip_address;
        $buff_log->created_date = date('Y-m-d H:i:s');
        $buff_log->save();
        $buff_log_id = $buff_log->id;
        $data['id'] = $buff_log_id;
        $data['u_id'] = $unique_id;
        echo json_encode($data);
    }

    public function actionAddTrailerLog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        $movie_id = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
        $movie_stream_data = movieTrailer::model()->findByAttributes(array('movie_id'=>$_REQUEST['movie_id']));
        $video_log = new VideoLogs();
        $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
        if ($log_id > 0) {
            $video_log = VideoLogs::model()->findByPk($log_id);
            $video_log->updated_date = new CDbExpression("NOW()");
            $video_log->played_length = $_REQUEST['played_length'];
        } else {
            $video_log->created_date = new CDbExpression("NOW()");
            $video_log->ip = $ip_address;
            $video_log->video_length = $_REQUEST['video_length'];
        }
        $video_log->movie_id = $movie_id;
        $video_log->trailer_id = $movie_stream_data->id;
        $video_log->content_type = 2;
        $video_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
        $video_log->studio_id = Yii::app()->common->getStudiosId();
        $video_log->watch_status = $_REQUEST['status'];
        if ($video_log->save()) {
            echo $video_log->id;
            exit;
        } else {
            echo 0;
            exit;
        }
    }
    
    public function actionAnalytics()
    {
        $this->pageTitle = "Muvi | Video Analytics";
        $this->breadcrumbs = array('Video' => array('report/video'), 'Analytics');
        $page_size = 20;
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if ((isset($_REQUEST['start']) && trim($_REQUEST['start'])) && (isset($_REQUEST['end']) && trim($_REQUEST['end']))) {
            $dt = array('start'=>$_REQUEST['start'],'end'=>$_REQUEST['end']);
            $dt = (object) $dt;
        }else if(!$dt){
            $dt = '';
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $videoTableData = BufferLogs::model()->getVideoDetails($studio_id,0,0,$dt,$searchKey,$deviceType,$offset,$page_size);
        if(isset($videoTableData['data']) && empty($videoTableData['data'])){
            $videoTableData = BufferLogs::model()->getVideoDetailsOfDevice($studio_id,0,0,$dt='',$searchKey,$deviceType,$offset,$page_size);
        }
        $std_id = isset($videoTableData['data'][0]['studio_id'])?$videoTableData['data'][0]['studio_id']:$studio_id;
        if($studio_id == $std_id){
            $is_self = 1;
            if(isset($videoTableData['count']) && !$videoTableData['count']){
                $contentDetails = BufferLogs::model()->getContentDetails($studio_id,0,0);
            }
        }else{
            $is_self = 0;
        }
        
        $hour_bandwidth = BufferLogs::model()->getHourBandwidth($studio_id,0,0,$dt,$searchKey,$deviceType);
        $bufferDuration = BufferLogs::model()->getTotalBufferDuration($studio_id,0,0,$dt,$searchKey,$deviceType);    
        $total_bandwidth = Yii::app()->aws->getLocationBandwidthSize($studio_id, $start_date, $end_date, $deviceType, 1);
        
        $total_bandwidth['is_bandwidth'] = 1;
        $bandwidth_price = Yii::app()->aws->getBandwidthPriceForReport($total_bandwidth);
        $total_bandwidth_cost = (isset($bandwidth_price['price']) && abs($bandwidth_price['price']) >= 0.00001) ? $bandwidth_price['price'] : '';
        if (isset($_REQUEST['dt'])) {
            $json_data['bandwidth'] = $bandwidth_price['total_bandwidth'];
            $json_data['buffer_duration'] = $bufferDuration;
            $json_data['total_price'] = $total_bandwidth_cost;
            echo json_encode($json_data);
        }else{
            $allBandwidth = VideoLogs::model()->getAllBandwidthMovie($studio_id,0,0,$searchKey,$deviceType);
            foreach ($allBandwidth as $row) {
                $date = date('Y-m',strtotime($row['created_date']));
                $x[] = $date;
                if(in_array($date, $x)){
                    $bandwidthArr[$date] +=(float)$row['total_buffered_size']/1024/1024;
                }else{
                    $bandwidthArr[$date] =(float)$row['total_buffered_size']/1024/1024;
                }
            }
            $lunchDate = Yii::app()->user->lunch_date;
            $lmonth = date('n', strtotime($lunchDate));
            $lyear = date('Y', strtotime($lunchDate));
            $arr = array();
            for ($i = date('Y'); $i >= $lyear; $i--) {
                if ($i == date('Y')) {
                    $j = date('n');
                } else {
                    $j = 12;
                }
                for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                    $mont = $j < 10 ? '0' . $j : $j;
                    $arr['bandwidth'][] = isset($bandwidthArr[$i . "-" . $mont]) ? $bandwidthArr[$i . "-" . $mont] : 0;
                    $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
                }
            }
            $bandwidthgraph[] =  array('name' => ucfirst('Bandwidth Consumed'), 'data' => array_reverse($arr['bandwidth']));
            $bandwidthGraphData = json_encode($bandwidthgraph);
            $user_start = Yii::app()->common->getStudioUserStartDate();
            $this->render('analytics',array('videoTableData' => $videoTableData,'hour_bandwidth'=>$bandwidth_price['total_bandwidth'],'xdata' => json_encode(array_reverse($xdata)),'bandwidth_cost'=>$total_bandwidth_cost,'bandwidthGraphData' => $bandwidthGraphData,'is_self'=>$is_self,'bufferDuration'=>$bufferDuration,'dateRanges'=>$dt,'lunchDate'=>$user_start,'page_size'=>$page_size));
        }
    }
    
    public function actionRevenueGraph() {
        $this->layout = false;
        $plans = Yii::app()->common->isPaymentGatwayAndPlanExists(Yii::app()->user->studio_id);
        $plan = $plans['plans'];
        $studioPlans = array();
        foreach($plan AS $plank=>$planval ){
            $studioPlans[$planval['id']] = $planval['name'];
        }
        $studio = $this->studio;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        } else {
            $currency_id = $studio->default_currency_id;
        }
        $partner_sql="";
        if(isset(Yii::app()->user->is_partner) && Yii::app()->user->is_partner){
            $content = Yii::app()->general->getPartnersContentIds();
            if(trim($content['movie_id'])){
                $partner_sql = " AND ppv_subscriptions.movie_id IN (".$content['movie_id'].")";
            }else{
                $partner_sql = " AND ppv_subscriptions.movie_id IN (-1)";
            }
            
        }
        
        $dbcon = Yii::app()->db;
        if(!isset(Yii::app()->user->is_partner)){
            $sql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id AND user_subscriptions.status = 1) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE  transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '" . $currency_id . "' AND sdk_users.status=1 GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
            $data = $dbcon->createCommand($sql)->queryAll();
           
            
            
        }
        $ppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 0 AND ppv_subscriptions.is_ppv_bundle = 0 AND transactions.currency_id = '" . $currency_id ."'". $partner_sql . " GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
        $advppvsql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_advance_purchase = 1 AND transactions.currency_id = '" . $currency_id ."'". $partner_sql . " GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";
        $ppvbundlessql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m') as trans_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,is_advance_purchase FROM transactions LEFT JOIN ppv_subscriptions ON (transactions.ppv_subscription_id = ppv_subscriptions.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND ppv_subscription_id <> 0 AND ppv_subscriptions.is_ppv_bundle = 1 AND transactions.currency_id = '" . $currency_id ."'". $partner_sql . " GROUP BY DATE_FORMAT(transaction_date, '%Y-%m'),plan_id, movie_id ";

        $ppvdata = $dbcon->createCommand($ppvsql)->queryAll();
        $advppvdata = $dbcon->createCommand($advppvsql)->queryAll();
        $ppvbundlesdata = $dbcon->createCommand($ppvbundlessql)->queryAll();
        $isPpv = 0;
        $data = $this->array_restructure($data);
        if ($data) {
            foreach ($data AS $key => $val) {
                $gdata[$val['trans_month']][$studioPlans[$val['plan_id']]] = (float) $val['tot'];
            }
        }
        if ($ppvdata && !isset(Yii::app()->user->is_partner)) {
            foreach ($ppvdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['ppv']))
                        $new_data = $gdata[$val['trans_month']]['ppv'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['ppv'] = $new_data;
                }
            }
        }
        else{
                 $studio_id=Yii::app()->user->studio_id;
            $partner_id= Yii::app()->user->id;
            $partnersharing="select percentage_revenue_share from partners_contents where studio_id='".$studio_id."' AND user_id='".$partner_id."' group by user_id";
            $datapartnersharing = Yii::app()->db->createCommand($partnersharing)->queryAll();
            $percentageshare=$datapartnersharing[0]['percentage_revenue_share'];      
             foreach ($ppvdata AS $key => $val) {
                  $totalrevenue=(float) ($val['tot']*($percentageshare/100));
                  
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['ppv']))
                        $new_data = $gdata[$val['trans_month']]['ppv'] +$totalrevenue;
                    else {
                        $new_data = $totalrevenue;
                    }
                    $gdata[$val['trans_month']]['ppv'] = $new_data;
                }
            }
            
            
            
        }

        if ($advppvdata) {
            foreach ($advppvdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['adv_ppv']))
                        $new_data = $gdata[$val['trans_month']]['adv_ppv'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['adv_ppv'] = $new_data;
                }
            }
        }
         if ($ppvbundlesdata) {
            foreach ($ppvbundlesdata AS $key => $val) {
                if ($val['ppv_subscription_id'] > 0) {
                    $isPpv = 1;
                    if (isset($gdata[$val['trans_month']]['ppv_bundles']))
                        $new_data = $gdata[$val['trans_month']]['ppv_bundles'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['ppv_bundles'] = $new_data;
                }
            }
        }

        
        
        /* Physical goods @author manas@muvi.com */
        $storeflag = 0;
        if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){
            $storeflag = 1;
            $pgsql = "SELECT SUM(d.`price` - o.`discount`) AS tot,DATE_FORMAT(o.created_date, '%Y-%m') as trans_month 
                     FROM `pg_order_details` AS d,pg_order AS o,pg_product p,transactions t 
                     WHERE d.order_id=o.id AND d.product_id=p.id AND o.transactions_id=t.id  AND t.studio_id = '".Yii::app()->user->studio_id."' 
                     AND t.transaction_type=4 AND t.currency_id = '".$currency_id."' GROUP BY DATE_FORMAT(o.created_date, '%Y-%m-%d')";
            $pgdata = $dbcon->createCommand($pgsql)->queryAll();
            if (isset($pgdata) && !empty($pgdata)) {
                foreach ($pgdata AS $key => $val) {
                    if (isset($gdata[$val['trans_month']]['pg']))
                        $new_data = $gdata[$val['trans_month']]['pg'] + (float) $val['tot'];
                    else {
                        $new_data = (float) $val['tot'];
                    }
                    $gdata[$val['trans_month']]['pg'] = $new_data;
                }
            }            
        }
        /* END */
        /* muvi cart order */
         if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){
            $cart_sql = "SELECT SQL_CALC_FOUND_ROWS (0),DATE_FORMAT(o.created_date, '%Y-%m') as order_month, SUM(d.`quantity`) AS quantity, SUM(o.`grand_total`) AS revenue 
                        FROM `pg_order_details` AS d,pg_order AS o,pg_product p,transactions t 
                        WHERE d.order_id=o.id AND d.product_id=p.id AND o.transactions_id=t.id AND  t.studio_id = '".Yii::app()->user->studio_id."' 
                        AND t.transaction_type=4 AND t.currency_id = '".$currency_id."'
                        GROUP BY DATE_FORMAT(o.created_date, '%Y-%m-%d')";
            $cart_data = $dbcon->createCommand($cart_sql)->queryAll();
            if (isset($cart_data) && !empty($cart_data)) {
                   foreach($cart_data as $key => $val){
                        if (isset($gdata[$val['order_month']]['oc']))
                           $new_data = $gdata[$val['order_month']]['oc'] + (float) $val['revenue'];
                        else {
                           $new_data = (float) $val['revenue'];
                        }
                      $gdata[$val['order_month']]['oc'] = $new_data;                      
                   }
             }
        }
        /*muvi cart order end */
        $lunchDate = Yii::app()->user->lunch_date;
        $lmonth = date('n', strtotime($lunchDate));
        $lyear = date('Y', strtotime($lunchDate));
        for ($i = date('Y'); $i >= $lyear; $i--) {
            if ($i == date('Y')) {
                $j = date('n');
            } else {
                $j = 12;
            }
            for ($j; (($i != $lyear && $j > 0) || ($i == $lyear && $j >= $lmonth)); $j--) {
                $mont = $j < 10 ? '0' . $j : $j;
                foreach ($studioPlans AS $plank => $planv) {
                    $arr[$planv][] = isset($gdata[$i . "-" . $mont][$planv]) ? $gdata[$i . "-" . $mont][$planv] : 0;
                }
                $arr['Ppv'][] = isset($gdata[$i . "-" . $mont]['ppv']) ? $gdata[$i . "-" . $mont]['ppv'] : 0;
                $arr['Advance Purchase'][] = isset($gdata[$i . "-" . $mont]['adv_ppv']) ? $gdata[$i . "-" . $mont]['adv_ppv'] : 0;
                $arr['Ppv Bundles'][] = isset($gdata[$i . "-" . $mont]['ppv_bundles']) ? $gdata[$i . "-" . $mont]['ppv_bundles'] : 0;
                if($storeflag){
                    $arr['Physical Goods'][] = isset($gdata[$i . "-" . $mont]['pg']) ? $gdata[$i . "-" . $mont]['pg'] : 0;
                }
                if($storeflag){
                    $arr['MuviKart Order'][] = isset($gdata[$i . "-" . $mont]['oc']) ? $gdata[$i . "-" . $mont]['oc'] : 0;
                }                
                $xdata[] = date('F', mktime(0, 0, 0, $j, 10));
            }
        }
        if(in_array($studio_id,array(2663,3057))){
            if($storeflag){
                @$totalRevenue += array_sum($arr['Physical Goods']);
                $revenue[] = array('name' => ucfirst('Physical Goods'), 'data' => array_reverse($arr['Physical Goods']));
            }
        }else{
        foreach ($studioPlans AS $plank => $planv) {
            @$totalRevenue += array_sum($arr[$planv]);
            $revenue[] = array('name' => ucfirst($planv), 'data' => array_reverse($arr[$planv]));
        }
        @$totalRevenue += array_sum($arr['Ppv']);
        $revenue[] = array('name' => ucfirst('Ppv'), 'data' => array_reverse($arr['Ppv']));

        @$totalRevenue += array_sum($arr['Advance Purchase']);
        $revenue[] = array('name' => ucfirst('Pre-Order'), 'data' => array_reverse($arr['Advance Purchase']));
        if(!isset(Yii::app()->user->is_partner)){ 
        @$totalRevenue += array_sum($arr['Ppv Bundles']);
        $revenue[] = array('name' => ucfirst('Ppv Bundles'), 'data' => array_reverse($arr['Ppv Bundles']));
        }
        
        if($storeflag){
            @$totalRevenue += array_sum($arr['Physical Goods']);
            $revenue[] = array('name' => ucfirst('Physical Goods'), 'data' => array_reverse($arr['Physical Goods']));
        }
        if($storeflag){
            @$totalRevenue += array_sum($arr['MuviKart Order']);
            $revenue[] = array('name' => ucfirst('MuviKart Order'), 'data' => array_reverse($arr['MuviKart Order']));
        }        
        }
        $res['graphdata'] = $revenue;
        $res['xdata'] = array_reverse($xdata);
        print json_encode($res);exit;
    }
    
/*This function sorts the video details array*/
    function make_comparer() {
        // Normalize criteria up front so that the comparer finds everything tidy
        $criteria = func_get_args();
        foreach ($criteria as $index => $criterion) {
            $criteria[$index] = is_array($criterion)
                ? array_pad($criterion, 3, null)
                : array($criterion, SORT_ASC, null);
        }
        
        return function($first, $second) use (&$criteria) {
            foreach ($criteria as $criterion) {
                // How will we compare this round?
                list($column, $sortOrder, $projection) = $criterion;
                $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;
                
                // If a projection was defined project the values now
                if ($projection) {
                    $lhs = ucfirst(call_user_func($projection, $first[$column]));
                    $rhs = ucfirst(call_user_func($projection, $second[$column]));
                }
                else {
                    $lhs = ucfirst($first[$column]);
                    $rhs = ucfirst($second[$column]);
                }
                
                // Do the actual comparison; do not return if equal
                if ($lhs < $rhs) {
                    return -1 * $sortOrder;
                }
                else if ($lhs > $rhs) {
                    return 1 * $sortOrder;
                }
            }
            
            return 0; // tiebreakers exhausted, so $first == $second
        };
    }
    
    public function filterArray($array,$condition,$param)
    {
        $resArray = array();
        foreach($array as $subArray){
            foreach($subArray as $nextsubArray){
                if($nextsubArray[$param] == $condition){
                    $resArray[] = $nextsubArray;
                }
            }
        }
    }
    
    public function actionTestVideoReport()
    {
        $dt = '';
        $studio = $this->studio;
        $studio_id = $studio->id;
        if (isset($_REQUEST['currency_id']) && $_REQUEST['currency_id']) {
            $currency_id = $_REQUEST['currency_id'];
        }else{
            $currency_id = $studio->default_currency_id;
        }
        $sql_content = "SELECT f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = 488 AND f.id = ms.movie_id GROUP BY movie_id ORDER BY f.name DESC";
        $content_data = Yii::app()->db->createCommand($sql_content)->queryAll();
        $sql_season = "SELECT f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = 488 AND f.id = ms.movie_id AND f.content_types_id = 3 AND (series_number <> 0 OR series_number IS NOT NULL) GROUP BY series_number, movie_id ORDER BY f.name DESC";
        $season_data = Yii::app()->db->createCommand($sql_season)->queryAll();
        $sql_episode = "SELECT f.id as movie_id,ms.id as stream_id,f.name,f.content_types_id,f.ppv_plan_id,ms.is_converted,ms.series_number,ms.episode_title,ms.episode_number FROM films f, movie_streams ms WHERE f.studio_id = 488 AND f.id = ms.movie_id AND f.content_types_id = 3 AND (series_number <> 0 OR series_number IS NOT NULL) ORDER BY f.name DESC";
        $episode_data = Yii::app()->db->createCommand($sql_episode)->queryAll();
        $finalArray = array();
        $i = 0;
        
        if(isset($content_data) && !empty($content_data)){
            foreach($content_data as $content){
                $j = 1;
                if($content['content_types_id'] == 3){
                    $type_str = '';
                    if($j){
                        $price = $this->getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                        $transaction_data = VideoLogs::model()->getPpvContentTransactionDetails($dt,$currency_id,$content['movie_id'],0,0,0);
                        $content['transactions'] = $transaction_data['count']['transactioncount'];
                        foreach ($transaction_data['type'] as $type){
                            if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                if (!preg_match('PO', $type_str)) {
                                    $type_str .= 'PO-';
                                }
                            }else{
                                if (!preg_match('PPV', $type_str)) {
                                    $type_str .= 'PPV-';
                                }
                            }
                        }
                        $content['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                        $content['price'] = Yii::app()->common->formatPrice($price['show_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['show_subscribed'],$currency_id);
                        $content['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice($transaction_data['count']['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                        $finalArray[$i] = $content;
                        $j = 0;
                        $i++;
                    }
                    if(isset($season_data) && !empty($season_data)){
                        foreach ($season_data as $season) {
                            $type_str = '';
                            $price = $this->getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                            $transaction_data = VideoLogs::model()->getPpvContentTransactionDetails($dt,$currency_id,$content['movie_id'],$season['series_number'],0);
                            foreach ($transaction_data['type'] as $type){
                                if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                    if (!preg_match('PO', $type_str)) {
                                        $type_str .= 'PO-';
                                    }
                                }else{
                                    if (!preg_match('PPV', $type_str)) {
                                        $type_str .= 'PPV-';
                                    }
                                }
                            }
                            $finalArray[$i]['movie_id'] = $content['movie_id'];
                            $finalArray[$i]['name'] = $content['name'].' - Season - '.$season['series_number'];
                            $finalArray[$i]['content_types_id'] = $content['content_types_id'];
                            $finalArray[$i]['series_number'] = $season['series_number'];
                            $finalArray[$i]['episode_title'] = '';
                            $finalArray[$i]['episode_number'] = '';
                            $finalArray[$i]['transactions'] = $transaction_data['count']['transactioncount'];
                            $finalArray[$i]['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                            $finalArray[$i]['price'] = Yii::app()->common->formatPrice($price['season_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['season_subscribed'],$currency_id);
                            $finalArray[$i]['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice($transaction_data['count']['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                            $i++;
                        }
                    }
                    if(isset($episode_data) && !empty($episode_data)){
                        foreach ($episode_data as $episode) {
                            $type_str = '';
                            $price = $this->getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                            $transaction_data = VideoLogs::model()->getPpvContentTransactionDetails($dt,$currency_id,$content['movie_id'],$episode['series_number'],$episode['stream_id']);
                            foreach ($transaction_data['type'] as $type){
                                if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                                    if (!preg_match('PO', $type_str)) {
                                        $type_str .= 'PO-';
                                    }
                                }else{
                                    if (!preg_match('PPV', $type_str)) {
                                        $type_str .= 'PPV-';
                                    }
                                }
                            }
                            $finalArray[$i]['movie_id'] = $content['movie_id'];
                            $finalArray[$i]['name'] = $content['name'].' - Season - '.$episode['series_number'].' - Episode - '.$episode['episode_title'];
                            $finalArray[$i]['content_types_id'] = $content['content_types_id'];
                            $finalArray[$i]['series_number'] = $episode['series_number'];
                            $finalArray[$i]['episode_title'] = $episode['episode_title'];
                            $finalArray[$i]['episode_number'] = $episode['episode_number'];
                            $finalArray[$i]['transactions'] = $transaction_data['count']['transactioncount'];
                            $finalArray[$i]['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                            $finalArray[$i]['price'] = Yii::app()->common->formatPrice($price['episode_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['episode_subscribed'],$currency_id);
                            $finalArray[$i]['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice($transaction_data['count']['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                            $i++;
                        }
                    }
                    
                }else{
                    $type_str = '';
                    $price = $this->getPrice($content['is_converted'],$content['movie_id'],$content['content_types_id'],$content['ppv_plan_id'],$currency_id);
                    $transaction_data = VideoLogs::model()->getPpvContentTransactionDetails($dt,$currency_id,$content['movie_id'],0,0);
                    $content['transactions'] = $transaction_data['count']['transactioncount'];
                    foreach ($transaction_data['type'] as $type){
                        if(isset($type['is_advance_purchase']) && ($type['is_advance_purchase']==1)){
                            if (!preg_match('PO', $type_str)) {
                                $type_str .= 'PO-';
                            }
                        }else{
                            if (!preg_match('PPV', $type_str)) {
                                $type_str .= 'PPV-';
                            }
                        }
                    }
                    $content['transaction_type'] = trim($type_str) ? rtrim($type_str,'-') : 'Other';
                    $content['price'] = Yii::app()->common->formatPrice($price['price_for_unsubscribed'],$currency_id).'/'.Yii::app()->common->formatPrice($price['price_for_subscribed'],$currency_id);
                    $content['revenue'] = trim($transaction_data['count']['amount'])? Yii::app()->common->formatPrice($transaction_data['count']['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                    $finalArray[$i] = $content;
                }
                $i++;
            }
        }
        print "<pre>";
        print_r($finalArray);
        print "</pre>";
        
    }
    
    public function getPrice($isConverted,$movie_id,$content_types_id,$ppv_plan_id,$currency_id)
    {
        $price = array();
        
        if ($isConverted == 1) {
            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id, '', 0);

            if (isset($ppv->id) && intval($ppv->id)) {
                $payment_type = $ppv->id;
            } else {
                $ppv = Yii::app()->common->getContentPaymentType($content_types_id, $ppv_plan_id);
                $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
               
            }
            if (intval($payment_type)) {
                $price = Yii::app()->common->getPPVPrices($payment_type, $currency_id);
            }
        } else {
            $ppv = Yii::app()->common->checkAdvancePurchase($movie_id);
            $adv_payment = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
            if (intval($adv_payment)) {
                $price = Yii::app()->common->getPPVPrices($adv_payment, $currency_id);
            }
        }
        return $price;
    }
    
    function actionLocationTest() {
        $limit = (isset($_REQUEST['limit'])) ? $_REQUEST['limit'] : 10000;
        $offset = (isset($_REQUEST['offset'])) ? $_REQUEST['offset'] : 0;
        $sql = "SELECT id,studio_id, location FROM bandwidth_log WHERE 1 LIMIT {$offset}, {$limit}";
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        
        foreach ($res as $value) {
            if (trim($value['location'])) {
                $signup_location = Yii::app()->common->getLocationFromData(trim($value['location']));
                $id = $value['id'];

                if (trim($signup_location['geoplugin_continentCode'])) {
                    $buff_log = BufferLogs::model()->findByPk($id);
                    $buff_log->city = @$signup_location['city_name'];
                    $buff_log->region = @$signup_location['region_name'];
                    $buff_log->country = @$signup_location['country'];
                    $buff_log->country_code = @$signup_location['country_code'];
                    $buff_log->continent_code = @$signup_location['continent_code'];
                    $buff_log->latitude = @$signup_location['latitude'];
                    $buff_log->longitude = @$signup_location['longitude'];
                    $buff_log->save();
                }
            }
        }
        
        print '<pre>';print_r($res);exit;
    }
    public function actionContentPartners() {
        $this->pageTitle = "Muvi | Content Partners";
        $this->breadcrumbs = array('Analytics', 'Content Partners');
        $this->headerinfo = "Content Partners";
        $studio = $this->studio;
         if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $currency = Yii::app()->common->getStudioCurrency();
        $c = new Currency();
        $default_currency = $studio->default_currency_id;
        $currency_details = $c->findByPk($default_currency);
        $contentpartnersql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,transactions.subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id) LEFT JOIN subscription_plans ON (user_subscriptions.plan_id = subscription_plans.id) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '".$default_currency."' AND subscription_id!='' GROUP BY DATE_FORMAT(user_subscriptions.created_date, '%Y-%m'),plan_id, movie_id";
        //echo $contentpartnersql = "SELECT SUM(transactions.amount) as tot FROM transactions  WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '".$default_currency."' AND transactions.transaction_type = 1 group by studio_id"; 
        $data =Yii::app()->db->createCommand($contentpartnersql)->queryAll();
        $tot=0;
       foreach($data as $val){
           $tot +=$val['tot'];
       }
        $contentpartnerviewssql = "SELECT count(v.id) as totalview_count FROM video_logs v  WHERE v.studio_id = '" . Yii::app()->user->studio_id . "' group by studio_id"; 
        $datatotalviewcount =Yii::app()->db->createCommand($contentpartnerviewssql)->queryRow();
        $totalcount=$datatotalviewcount['totalview_count'];
        $user_start = Yii::app()->common->getStudioUserStartDate();
        $this->render('contentPartners',array('tot_subscriptions' => $tot,'datatotalviewciunt'=>$totalcount,'default_currency'=>$default_currency,'lunchDate'=>$user_start,'currency'=>$currency));
    }
     public function actionGetContentPartnersViews()
    {
         $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $views = '';
        $content_data= User::model()->getPartnersContentViewDetails($dt,$studio_id,$deviceType);
        if (isset($_REQUEST['dt'])) {
            if (isset($_REQUEST['dt'])) {
                $dt = stripcslashes($_REQUEST['dt']);
                $dt = json_decode($dt);
            } else if (!$dt) {
                $dt = '';
            }
            $this->renderPartial('contentPartnersViewData',array('content_data'=>$content_data));
        } else {
            return $views;
        }
    }
     public function actionContentPartnerMultiCurrency(){
        $currency_id= $_REQUEST['currency_id'];
         $currencySymbol = Currency::model()->findByPk($currency_id)->symbol;
        $contentpartnersql = "SELECT SUM(transactions.amount) as tot,DATE_FORMAT(transaction_date, '%Y-%m-%d') as trans_month,DATE_FORMAT(user_subscriptions.created_date, '%Y-%m-%d') AS created_month,transactions.plan_id, transactions.movie_id, transactions.ppv_subscription_id,transactions.subscription_id FROM transactions LEFT JOIN user_subscriptions ON (transactions.subscription_id = user_subscriptions.id) LEFT JOIN subscription_plans ON (user_subscriptions.plan_id = subscription_plans.id) LEFT JOIN sdk_users ON (transactions.user_id = sdk_users.id) WHERE transactions.studio_id = '" . Yii::app()->user->studio_id . "' AND transactions.currency_id = '".$currency_id."' AND subscription_id!='' GROUP BY DATE_FORMAT(user_subscriptions.created_date, '%Y-%m'),plan_id, movie_id";
        $data =Yii::app()->db->createCommand($contentpartnersql)->queryAll();
        $tot=0;
       foreach($data as $val){
           $tot +=$val['tot'];
       }
        $totalSubscriptionrate=Yii::app()->common->formatPrice($tot,$currency_id);
        echo $CurrencyMulti=$totalSubscriptionrate;
        
     }
       public function actiongetContentPartnerReport()
    {
          $studio_id = Yii::app()->user->studio_id;
        if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
        } else if (!$dt) {
            $dt = '';
        }
        $studio = $this->studio;
        $deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $views = '';
            $headArr[0] = array('Content Partner','Views On Content','% Views On Content');
            $sheetName[0] = 'Content Partner';
            $content_data= User::model()->getPartnersContentViewDetails($dt,$studio_id,$deviceType);
            $data[0] = array();
            $sheet = 1;
            foreach ($content_data as $contentviews){
                $viewcountt=$contentviews['viewcount']? $contentviews['viewcount'] : 0;
                $viewcountpercentage=floor($viewcountt * ($contentviews['percentage_revenue_share']/100));
                $data[0][] = array(
                   $contentviews['first_name'],
                   $viewcountt,
                   $viewcountpercentage
                );
            }
       
       
        $filename = 'Contentpartner_report_'.date('Ymd_His');
        if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv'){
           $type = 'xls'; 
        }
        elseif(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf'){
           $type = 'pdf'; 
        }
        if(isset(Yii::app()->user->is_partner)){
            $data = array_values($data);
            $sheetName = array_values($sheetName);
            $headArr = array_values($headArr);
            $sheet = count($sheetName);
        }
        Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,$type);
        exit;
    }
   public function actionAddNewCustomreport(){
       
        $this->pageTitle = "Muvi | Add Report";
        $this->breadcrumbs = array('Analytics', "Custom Reports"=>array('report/CustomReport'),'Add Report');
        $this->headerinfo = "Add Report";
        $studio = $this->studio;
        $template_data= ReportTemplate::model()->getAvailableColumn();
        $column_data= ReportTemplate::model()->getColumnDetails();
        $this->render('customreport',array('template_data'=>$template_data,'column_data'=>$column_data));
    }
     public function actionAddNewColumn(){
         $studio_id = Yii::app()->user->studio_id;
         $customcolsql = "SELECT count(id) as countid  from report_template  WHERE custom_label_id = '".$_REQUEST['data']['col_heading']."' AND studio_id=".$studio_id; 
         $countarr =Yii::app()->db->createCommand($customcolsql)->queryRow();
         $count=$countarr['countid'];
         $static= $_REQUEST['report']['static'];
         if($static!=3){
         if($count==0){
         $ReportTemplateModel = New ReportTemplate;
                   $ReportTemplateModel->custom_label_id = $_REQUEST['data']['col_heading'];
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 1;
                   $ReportTemplateModel->save();
        Yii::app()->user->setFlash('success', 'new Column has been Added successfully');
        $url = $this->createUrl('report/AddNewCustomreport');
        $this->redirect($url);
          }
          else
          {
        Yii::app()->user->setFlash('success', 'not');
        $url = $this->createUrl('report/AddNewCustomreport');
        $this->redirect($url);
          }
         }
         else{
           $CustomLabelModel = New CustomLabelColumn;
                   $CustomLabelModel->original_label_name = $_REQUEST['report']['static_report_title'];
                   $CustomLabelModel->label_name = $_REQUEST['report']['static_report_title'];
                   $CustomLabelModel->label_code = $_REQUEST['report']['static_report_value'];
                   $CustomLabelModel->type = 2;
                   $CustomLabelModel->save();
                   $CustomLabelModel_id = $CustomLabelModel->id; 
          $ReportTemplateModel = New ReportTemplate;
                   $ReportTemplateModel->custom_label_id = $CustomLabelModel_id;
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 2;
                   $ReportTemplateModel->save();
        Yii::app()->user->setFlash('success', 'new Column has been Added successfully');
        $url = $this->createUrl('report/AddNewCustomreport');
        $this->redirect($url);   
         }
          
     }
     public function actionAddNewColumnEdit(){
         $studio_id = Yii::app()->user->studio_id;
         $customcolsql = "SELECT count(id) as countid  from report_template  WHERE custom_label_id = '".$_REQUEST['data']['col_heading']."' AND studio_id=".$studio_id; 
         $countarr =Yii::app()->db->createCommand($customcolsql)->queryRow();
         $count=$countarr['countid'];
         $custom_report_id=$_REQUEST['report']['custom_report_id'];
         $static= $_REQUEST['report']['static'];
         if($static!=3){
         if($count==0){
         $ReportTemplateModel = New ReportTemplate;
                   $ReportTemplateModel->custom_label_id = $_REQUEST['data']['col_heading'];
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 1;
                   $ReportTemplateModel->save();
        Yii::app()->user->setFlash('success', 'new Column has been Added successfully');
        $url = $this->createUrl('report/CustomReportEdit/Report/'.$custom_report_id);
        $this->redirect($url);
          }
          else
          {
        Yii::app()->user->setFlash('success', 'not');
        $url = $this->createUrl('report/CustomReportEdit/Report/'.$custom_report_id);
        $this->redirect($url);
          }
         }
         else{
           $CustomLabelModel = New CustomLabelColumn;
                   $CustomLabelModel->original_label_name = $_REQUEST['report']['static_report_title'];
                   $CustomLabelModel->label_name = $_REQUEST['report']['static_report_title'];
                   $CustomLabelModel->label_code = $_REQUEST['report']['static_report_value'];
                   $CustomLabelModel->type = 2;
                   $CustomLabelModel->save();
                   $CustomLabelModel_id = $CustomLabelModel->id; 
          $ReportTemplateModel = New ReportTemplate;
                   $ReportTemplateModel->custom_label_id = $CustomLabelModel_id;
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 2;
                   $ReportTemplateModel->save();
        Yii::app()->user->setFlash('success', 'new Column has been Added successfully');
        $url = $this->createUrl('report/CustomReportEdit/Report/'.$custom_report_id);
        $this->redirect($url);   
         }
          
     }
     public function actionEditCustomReport(){
         $editid=$_REQUEST['editid'];
         $template_data= ReportTemplate::model()->getReportDetails($editid);
        // print_r($template_data);
         //exit;
         $column_data= ReportTemplate::model()->getColumnDetails();
         $this->renderpartial('editcustomreport',array('report_template'=>$template_data,'column_data'=> $column_data));
     }
      public function actionUpdateCustomReport(){
        // print_r($_REQUEST);
        $editid=$_REQUEST['col_id'];
        $reporttemplatesql = "SELECT r.studio_id,cl.original_label_name  from report_template as r LEFT JOIN custom_lable_column as cl on r.custom_label_id=cl.id  WHERE r.id = '".$editid."'"; 
        $data =Yii::app()->db->createCommand($reporttemplatesql)->queryRow();
        $datastudionid=$data['studio_id'];
        $original_label_name=$data['original_label_name'];
        $studio_id = Yii::app()->user->studio_id; 
        $column_name=1;
        $type=$_REQUEST['type'];
        
         if($original_label_name=='' && $type==1){
         $Customlabelduplicate= CustomLabelColumn::model()->getAllcolumnName($_REQUEST['col_heading_replace']);  
         $count=$Customlabelduplicate[0]['label_namecount'];
         $countidsql = "select count(cl.label_code)as countt,cr.* from custom_lable_column as cl LEFT JOIN report_template as cr on cl.id=cr.custom_label_id where cl.label_code='".$_REQUEST['label_code']."' AND cr.studio_id=".$studio_id; 
         $datacountarr =Yii::app()->db->createCommand($countidsql)->queryRow();
         $datacount=$datacountarr['countt'];
        if(($datacount ==1 || $datacount ==0)){   
        if(($count==0) ){   
           $CustomReportColumnModel=New CustomLabelColumn;
           $CustomReportColumnModel->original_label_name=$_REQUEST['customlabel_name'];
           $CustomReportColumnModel->label_name = $_REQUEST['col_heading_replace'];
           $CustomReportColumnModel->label_code = $_REQUEST['label_code'];
           $CustomReportColumnModel->save(); 
           $customidd=$CustomReportColumnModel->id;
           $ReportTemplateModel = New ReportTemplate;
                   $ReportTemplateModel->custom_label_id = $customidd;
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 2;
                   $ReportTemplateModel->save(); 
                   $id = $ReportTemplateModel->id;
                   $custom_labelid=$ReportTemplateModel->custom_label_id;
        }
         else
        {
          $column_name=2;  
        }
        }
         else
        {
          $column_name=3;  
        }
        
         }
         else{
          $Customlabelduplicate= CustomLabelColumn::model()->getAllcolumnName($_REQUEST['col_heading_replace']);  
          $count=$Customlabelduplicate[0]['label_namecount'];
          $countidsql = "select count(cl.label_code)as countt,cr.* from custom_lable_column as cl LEFT JOIN report_template as cr on cl.id=cr.custom_label_id where cl.label_code='".$_REQUEST['label_code']."' AND cr.studio_id=".$studio_id; 
          $datacountarr =Yii::app()->db->createCommand($countidsql)->queryRow();
          $datacount=$datacountarr['countt'];
        if($_REQUEST['type']==1){
        if(($datacount < 3)){    
        if(($count==0) && ($datacount < 3)){ 
           
        $CustomReportColumnModel= CustomLabelColumn::model()->findByPk($_REQUEST['customlabel_id']);
        $CustomReportColumnModel->label_name = $_REQUEST['col_heading_replace'];
        //$CustomReportColumnModel->original_label_name=$_REQUEST['customlabel_name'];
        if($_REQUEST['type']==2){
         $CustomReportColumnModel->label_code = $_REQUEST['label_code'];    
        }
        if($_REQUEST['type']==1){
         $CustomReportColumnModel->label_code = $_REQUEST['label_code'];    
        }
        $CustomReportColumnModel->save();
        $ReportTemplateModel= ReportTemplate::model()->findByPk($editid);
                   $ReportTemplateModel->custom_label_id = $_REQUEST['customlabel_id'];
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 2;
                   $ReportTemplateModel->save();
                   $id = $ReportTemplateModel->id;
                   $custom_labelid=$ReportTemplateModel->custom_label_id;
         }
        else
        {
          $column_name=2;  
        }
        }
         else
        {
          $column_name=3;  
        }
        }
        if($_REQUEST['type']==2){
        
        if(($datacount < 3)){ 
        $CustomReportColumnModel= CustomLabelColumn::model()->findByPk($_REQUEST['customlabel_id']);
        $CustomReportColumnModel->label_name = $_REQUEST['col_heading_replace'];
        $CustomReportColumnModel->label_code = $_REQUEST['static_val'];    
        $CustomReportColumnModel->save();
        $ReportTemplateModel= ReportTemplate::model()->findByPk($editid);
                   $ReportTemplateModel->custom_label_id = $_REQUEST['customlabel_id'];
                   $ReportTemplateModel->studio_id = $studio_id;
                   $ReportTemplateModel->type = 2;
                   $ReportTemplateModel->save();
                   $id = $ReportTemplateModel->id;
                   $custom_labelid=$ReportTemplateModel->custom_label_id;
         }
        else
        {
          $column_name=2;  
        }
        }
         }
         $customcolsql = "SELECT *  from custom_lable_column  WHERE id = '".$custom_labelid."'"; 
         $datacustom =Yii::app()->db->createCommand($customcolsql)->queryRow();
         $labelname=$datacustom['label_name'];
         $data = array();
         $data['id'] = $id;
         $data['lable_name'] = $labelname;
         $data['col_name'] = $column_name;
         echo json_encode($data);
        
     }
     
     public function actionAddCustomReport(){
         $report_title=$_REQUEST['report']['report_title'];
         $report_type=$_REQUEST['report']['report_template'];
         $studio_id = Yii::app()->user->studio_id; 
         $CustomReport = New CustomReport;
                   $CustomReport->report_title = $report_title;
                   $CustomReport->report_type = $report_type;
                   $CustomReport->studio_id = $studio_id;
                   $CustomReport->save(); 
                   $id = $CustomReport->id;
         foreach($_REQUEST['colvalue'] as $key => $value) {
             $CustomReportColumn = New CustomReportColumn;
                   $CustomReportColumn->custom_report_id = $id;
                   $CustomReportColumn->report_template_id = $value;
                    $CustomReportColumn->position = ($key+1);
                   $CustomReportColumn->studio_id = $studio_id;
                   $CustomReportColumn->save(); 
}
    Yii::app()->user->setFlash('success', 'Custom Report added Successfully..');
        $url = $this->createUrl('report/CustomReport');
        $this->redirect($url);     
     }
     //update custom report 
      public function actionUpdateReport(){
         $report_title=$_REQUEST['report']['report_title'];
         $report_type=$_REQUEST['report']['report_template'];
         $studio_id = Yii::app()->user->studio_id; 
         $CustomReport = New CustomReport;
                   $CustomReport->report_title = $report_title;
                   $CustomReport->report_type = $report_type;
                   $CustomReport->studio_id = $studio_id;
                   $CustomReport->save(); 
                   $id = $CustomReport->id;
         foreach($_REQUEST['colvalue'] as $key => $value) {
             $CustomReportColumn = New CustomReportColumn;
                   $CustomReportColumn->custom_report_id = $id;
                   $CustomReportColumn->report_template_id = $value;
                   $CustomReportColumn->position = ($key+1);
                   $CustomReportColumn->studio_id = $studio_id;
                   $CustomReportColumn->save(); 
}
    Yii::app()->user->setFlash('success', 'Custom Report added Successfully..');
        $url = $this->createUrl('report/CustomReport');
        $this->redirect($url);     
     }
     
     
      public function actionCustomReport(){
       
        $this->pageTitle = "Muvi | Custom Reports";
        $this->breadcrumbs = array('Analytics', 'Custom Reports');
        $this->headerinfo = "Custom Reports";
        $studio = $this->studio;
        $customReport_data= CustomReport::model()->getAvailableReport();
        //$column_data= ReportTemplate::model()->getColumnDetails();
        $this->render('CustomReportList',array('customReport_data'=>$customReport_data));
        
        
    }
      public function actionShowCustomreport(){
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $studio = $this->studio;
        $studio_id = Yii::app()->user->studio_id; 
        $currency = Currency::model()->findByPk($this->studio->default_currency_id)->symbol;
        $requested_id=$_REQUEST['Report'];
        $customReportdetail_data= CustomReport::model()->getReportDetails($requested_id);
        $report_type=$customReportdetail_data[0]['report_type'];
        $report_name=$customReportdetail_data[0]['report_title'];
        $this->pageTitle = "Muvi | Custom Reports";
        $this->breadcrumbs = array('Analytics',  "Custom Reports"=>array('report/CustomReport'),'View Report');
        $this->headerinfo = "Custom Report -".$report_name;
        //echo "<pre>";
        //print_r($customReportdetail_data);
        if($report_type==1){
        $arr='';
        foreach($customReportdetail_data as $customreport){
        $labelcode= $customreport['label_code'];
        $labelcode_param=Yii::app()->common->getCustomValueReportUser($labelcode);
        if($arr!=''){
            $arr.=','.$labelcode_param;
        }else{
            $arr=$labelcode_param;
        }
        }
        $arrr= rtrim($arr,',');
        $name='USER EMAIL';
        //$sql="select u.email,u.id,COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,SUM(v.played_length) as duration ,b.buffer_size,u.display_name,(select sum(amount) as tamt from transactions where transactions.user_id=u.id) as tottransactionamt from sdk_users as u LEFT JOIN video_logs as v on u.id=v.user_id LEFT JOIN bandwidth_log b on b.user_id=u.id LEFT JOIN transactions as t on t.user_id=u.id where status=1 and u.studio_id=331 GROUP BY v.user_id";
        //$sql="select u.email,u.id,COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,SUM(v.played_length) as duration ,b.buffer_size,u.display_name from sdk_users as u LEFT JOIN video_logs as v on u.id=v.user_id LEFT JOIN bandwidth_log b on b.user_id=u.id where status=1 and u.studio_id=$studio_id GROUP BY v.user_id";
     $sql="select SQL_CALC_FOUND_ROWS u.email,". $arrr." ,u.id from sdk_users as u LEFT JOIN video_logs as v on u.id=v.user_id where (u.status=1 and u.studio_id=$studio_id) OR (u.is_free=1 and u.studio_id=$studio_id) GROUP BY v.user_id ORDER BY u.id DESC LIMIT $offset,$page_size";
     $data = Yii::app()->db->createCommand($sql)->queryAll(); 
        $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() AS count')->queryAll();
        $item_count = (isset($item_count[0]['count'])) ? $item_count[0]['count'] : 0; 
        }
         if($report_type==2){
        $arr='';
        foreach($customReportdetail_data as $customreport){
        $labelcode= $customreport['label_code'];
        $labelcode_param=Yii::app()->common->getCustomValueReportContent($labelcode);
        if($arr!=''){
            $arr.=','.$labelcode_param;
        }else{
            $arr=$labelcode_param;
        }
        }
         $arrr= rtrim($arr,',');
         $name='MOVIE NAME';
        //$sql="SELECT COUNT(v.id) as viewcount,COUNT(distinct v.user_id) as u_viewcount,v.movie_id,v.video_id,v.user_id,f.name,SUM(v.played_length) as duration,f.ppv_plan_id,f.content_types_id,m.episode_title,m.episode_number,m.series_number FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id WHERE v.studio_id = $studio_id   GROUP BY v.video_id";
        $sql="SELECT SQL_CALC_FOUND_ROWS f.name,". $arrr." ,v.movie_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id LEFT JOIN bandwidth_log b on b.user_id=v.user_id WHERE v.studio_id = $studio_id   GROUP BY v.video_id ORDER BY v.id DESC LIMIT $offset,$page_size";
       $data = Yii::app()->db->createCommand($sql)->queryAll();
         $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() AS count')->queryAll();
        $item_count = (isset($item_count[0]['count'])) ? $item_count[0]['count'] : 0; 
         }//$column_data= ReportTemplate::model()->getColumnDetails();
        $pages = new CPagination($item_count);
        $pages->setPageSize($page_size);
        $end = ($pages->offset + $pages->limit <= $item_count ? $pages->offset + $pages->limit : $item_count);
        $sample = range($pages->offset + 1, $end);
        $this->render('ShowCustomReport',array('customReportdetail_data'=>$customReportdetail_data,'data'=>$data,'name'=>$name,'page_size' => $page_size, 'items_count' => $item_count, 'pages' => $pages, 'sample' => $sample,'report_name'=>$report_name,'currency'=>$currency));
    }
      public function actionCustomReportEdit(){
        $this->pageTitle = "Muvi | Custom Reports";
        $this->breadcrumbs = array('Analytics',  "Custom Reports"=>array('report/CustomReport'),'Edit Custom report');
        $this->headerinfo = "Custom Reports";
        $studio = $this->studio;
        $requested_id=$_REQUEST['Report'];
        $column_data= ReportTemplate::model()->getTemplateColumnDetails($requested_id);
        $template_data= ReportTemplate::model()->getAvailableColumn();
        $column_data1= ReportTemplate::model()->getColumnDetails();
        $this->render('customReportEdit',array('column_data'=>$column_data,'template_data'=>$template_data,'requested_id'=>$requested_id,'available_column'=>$column_data1));
    }
    
      public function actionPartnerAssign() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $report_title = $_REQUEST['report_title'];
        $custom_report = CustomReport::model()->findByPk($type);
        $contents=explode(',',$custom_report['partner_id']);
         if(!empty($contents[0])){
        $arrayname=array();
        $aarr=array();
        for($i=0;$i<count($contents);$i++){
                  $partners_name= CustomReport::model()->getPartnerName($contents[$i]);
                 $arrayname[]=array_push($arrayname, array('name' => $partners_name['first_name'],'content_id'=>$partners_name['id']));
              }
        }
         $this->render('PartnerAssign', array('report_title' => $report_title,'contents'=>$arrayname));
    }
     public function actionAvailablePartners() {
        $con = Yii::app()->db;
        $studio = $this->studio;
        $studio_id = $studio->id;
      //$csql ="SELECT DISTINCT f.id AS content_id, f.name FROM films f, movie_streams ms WHERE f.studio_id=".$studio_id." AND f.id=ms.movie_id AND (ms.is_episode=0 AND ms.is_converted=1) OR (ms.is_episode=0 AND ms.is_converted=0 AND f.content_types_id=3) ORDER BY f.content_types_id, f.name ASC";
      //$csql="SELECT f.id AS content_id, f.name FROM films f WHERE f.studio_id=".$studio_id." ORDER BY f.content_types_id, f.name ASC";
        $csql="SELECT id as content_id,first_name as name FROM user where studio_id=$studio_id and role_id=4";
        $content = $con->createCommand($csql)->queryAll();
        echo json_encode($content);
        exit;
    }
    
    public function actionPartnerAssignedToCustomReport() {
        $this->layout = false;
        $studio = $this->studio;
        $studio_id = $studio->id;
        $type = $_REQUEST['type'];
        $partnerid = $_REQUEST['data']['content'];
        $partnersidd = implode(",",$partnerid);
        $report_id = $_REQUEST['data']['data-reportid'];
        $custom_report = CustomReport::model()->findByPk($report_id);
        $custom_report->partner_id = $partnersidd;
        $custom_report->save();
         Yii::app()->user->setFlash('success', 'Assigned to customer report successfully');
        $url = $this->createUrl('report/CustomReport');
        $this->redirect($url);
    }
    
    
   public function actionGetVideoCustomReport() {
       
        $studio_id = $this->studio->id;
        // $type=$_REQUEST['type'];
        $searchKey = isset($_REQUEST['search_value']) ? $_REQUEST['search_value'] : '';
        //$deviceType = (isset($_REQUEST['device_type']) && trim($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : '';
        $studio = $this->studio;
        $studio_id = Yii::app()->user->studio_id; 
        $requested_id=4;
       if (isset($_REQUEST['dt'])) {
            $dt = stripcslashes($_REQUEST['dt']);
            $frmdate = json_decode($dt);
            $start_date = $frmdate->start;
            $end_date = $frmdate->end;
            $date=" AND (DATE_FORMAT(u.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "')";  

        } else if (!$dt) {
            $end_date = date('Y-m-d');
            $daysgo = date('d')-1;
            $start_date = date('Y-m-d', strtotime('-'.$daysgo.' days'));
            $date=" AND (DATE_FORMAT(u.created_date,'%Y-%m-%d') BETWEEN '" . $start_date . "' AND '" . $end_date . "')";  

        }
        if(isset($_REQUEST['search_value']) && $_REQUEST['search_value']!=''){
        $searchvalue=$_REQUEST['search_value'];
        $searchKey=" AND u.email LIKE '".$searchvalue."%'";
        }
        else{
         $searchKey='';  
        }
        $customReportdetail_data= CustomReport::model()->getReportDetails($requested_id);
        $report_type=$customReportdetail_data[0]['report_type'];
     
        if($report_type==1){
        $arr='';
        foreach($customReportdetail_data as $customreport){
        $labelcode= $customreport['label_code'];
        $labelcode_param=Yii::app()->common->getCustomValueReportUser($labelcode);
        if($arr!=''){
            $arr.=','.$labelcode_param;
        }else{
            $arr=$labelcode_param;
        }
        }
        $arrr= rtrim($arr,',');
        $name='USER EMAIL';
       $sql="select u.email,". $arrr." ,u.id from sdk_users as u LEFT JOIN video_logs as v on u.id=v.user_id LEFT JOIN bandwidth_log b on b.user_id=u.id where status=1 and u.studio_id=$studio_id". $date. $searchKey." GROUP BY v.user_id";
      $data = Yii::app()->db->createCommand($sql)->queryAll(); 
        }
         if($report_type==2){
        $arr='';
        foreach($customReportdetail_data as $customreport){
        $labelcode= $customreport['label_code'];
        $labelcode_param=Yii::app()->common->getCustomValueReportContent($labelcode);
        if($arr!=''){
            $arr.=','.$labelcode_param;
        }else{
            $arr=$labelcode_param;
        }
        }
         $arrr= rtrim($arr,',');
         $name='MOVIE NAME';
        $sql="SELECT f.name,". $arrr." ,v.movie_id FROM video_logs v LEFT JOIN films f ON (v.movie_id = f.id AND (f.id<>0 OR f.id !='')) LEFT JOIN movie_streams m on m.id=v.video_id LEFT JOIN bandwidth_log b on b.user_id=v.user_id WHERE v.studio_id = $studio_id". $date. $searchKey."  GROUP BY v.video_id";
        $data = Yii::app()->db->createCommand($sql)->queryAll(); 
         }
         $headArr[0] = array();
        foreach($customReportdetail_data as $customReport){ 
            $rr="'".$customReport['label_name']."'";
            array_push($headArr[0] ,$rr);
        }
       $arr=implode(",",$headArr[0]);
       $headArr[0] = array($arr);
        $sheetName = array('Video Custom Report');
        $sheet = 1;
        $data[0] = array();
        if(isset($data) && !empty($data)){
             foreach($data as $customReportdata){
                  $keysofArr=array_keys($customReportdata);
                   for( $i=0;$i<count($keysofArr)-1;$i++){ 
                $data[0][] = array(
                   $customReportdata[$keysofArr[$i]],
                );
            }
        }
        } 
        $filename = 'video_report_' . date('Ymd_His');
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'csv') {
            $type = 'xls';
        } elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pdf') {
            $type = 'pdf';
        }
        $type = 'xls';
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }  
 public function actionDeleteCustomRepoprt(){
      $con = Yii::app()->db;
      $delete_id=$_REQUEST['rec_id']; 
      $csql="delete from custom_report where id=$delete_id";
      $content = $con->createCommand($csql)->execute();
      $csqll="delete from custom_report_column where custom_report_id=$delete_id";
      $contentt = $con->createCommand($csqll)->execute();
      echo 1;
     
  }  
public function actionDeleteSelectedImages() {
        $studio_id = Yii::app()->user->studio_id;
        $allid=$_REQUEST['id'];
        for($i=0; $i<=count($_REQUEST["id"])-1 ; $i++) {
                $query = "delete from `custom_report` where id=" . $allid[$i];
                $command = Yii::app()->db->createCommand($query);
                $command->execute();
                $query1 = "delete from `custom_report_column` where custom_report_id=" . $allid[$i];
                $command1 = Yii::app()->db->createCommand($query1);
                $command1->execute();
            }
     echo 1;
    }
  public function actionEditCustomReportUpdate(){
      $con = Yii::app()->db;
         $report_title=$_REQUEST['report']['report_title'];
         $report_type=$_REQUEST['report']['report_template'];
         $custom_report_id=$_REQUEST['custom_report_id'];
         $studio_id = Yii::app()->user->studio_id; 
         $CustomReport = CustomReport::model()->findByPk($custom_report_id);
                   $CustomReport->report_title = $report_title;
                   $CustomReport->report_type = $report_type;
                   $CustomReport->studio_id = $studio_id;
                   $CustomReport->save(); 
                   $id = $CustomReport->id;
      $csql="delete from custom_report_column where custom_report_id=$custom_report_id";
      $content = $con->createCommand($csql)->execute();          
         foreach($_REQUEST['colvalue'] as $key => $value) {
             $CustomReportColumn = New CustomReportColumn;
                   $CustomReportColumn->custom_report_id = $id;
                   $CustomReportColumn->report_template_id = $value;
                   $CustomReportColumn->position = ($key+1);
                   $CustomReportColumn->studio_id = $studio_id;
                   $CustomReportColumn->save(); 
}
    Yii::app()->user->setFlash('success', 'Custom Report Updated Successfully..');
        $url = $this->createUrl('report/CustomReport');
        $this->redirect($url);  
      
      
  }
  
  public function actionCreditUsed() {
    $this->pageTitle = Yii::app()->name .' | ' . 'Credits Report';
    $this->breadcrumbs = array('Analytics', 'Credits Used');
    $this->headerinfo = "Credits Used";
    $lunchDate = Yii::app()->user->lunch_date;
    $studio = $this->studio;
    $page_size = 20;
    $offset = 0;
    if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
        $dt = stripcslashes($_REQUEST['dt']);
        $dt = json_decode($dt);
        $sDate = $dt->start;
        $eDate = $dt->end;
    } else if (!$dt || $dt == '') {
        $eDate = date('Y-m-d');
        $daysgo = date('d')-1;
        $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
    }
    if(isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax']){
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $total_credit_used = MuviDebitPoints::model()->getTotalDebitOfStudio($studio->id, $sDate, $eDate);
        echo $total_credit_used;
        exit;
    }
    $debitDetails = MuviDebitPoints::model()->getDebitDetailsOfStudio($studio->id,$_REQUEST['searchText'], $sDate, $eDate, $page_size, $offset);
    $this->render('creditused', array('debitDetails'=>$debitDetails,'lunchDate' => $lunchDate,'page_size' =>$page_size,'total_credit_used'=>$total_credit_used));
  }
  
    public function  actioncreditusedlist() {
        $studio = $this->studio;
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $debitDetails = MuviDebitPoints::model()->getDebitDetailsOfStudio($studio->id,$_REQUEST['searchText'], $sDate, $eDate, $page_size, $offset);
        if(isset($debitDetails['data']) && count($debitDetails['data'])){
            for($i=0;$i<count($debitDetails['data']);$i++){
                $debitDetails['data'][$i]['content_title'] = '';
                if($debitDetails['data'][$i]['content_type'] == 3){
                    $str = $debitDetails['data'][$i]['content'];
                    $arr = explode(':', $str);
                    if(count($arr) == 3){
                        $movie_stream_name = movieStreams::model()->findByPk($arr[2])->episode_title;
                        $content_name = $debitDetails['data'][$i]['movie_name'] . ' - Season - ' . $arr[1] . ' - ' . $movie_stream_name;
                    }else{
                        $content_name = $debitDetails['data'][$i]['movie_name'] . ' - Season - ' . $arr[1];
                    }
                }else{
                    $content_name = $debitDetails['data'][$i]['movie_name'];
                }
               $debitDetails['data'][$i]['content_title'] =  $content_name;
            }
        }
        $this->renderPartial('creditlist', array('details'=>$debitDetails));
    }
    
    public function actionCreditGenerated() {
    $this->pageTitle = Yii::app()->name .' | ' . 'Credits Report';
    $this->breadcrumbs = array('Analytics', 'Credits Generated');
    $this->headerinfo = "Credits Generated";
    $lunchDate = Yii::app()->user->lunch_date;
    $studio = $this->studio;
    $page_size = 20;
    $offset = 0;
    if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
        $dt = stripcslashes($_REQUEST['dt']);
        $dt = json_decode($dt);
        $sDate = $dt->start;
        $eDate = $dt->end;
    } else if (!$dt || $dt == '') {
        $eDate = date('Y-m-d');
        $daysgo = date('d')-1;
        $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
    }
    if(isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax']){
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        $total_credit_generated = MuviCreditPoints::model()->getTotalCreditOfStudio($studio->id, $sDate, $eDate);
        echo $total_credit_generated;
        exit;
    }
    $creditDetails = MuviCreditPoints::model()->getCreditDetailsOfStudio($studio->id,$_REQUEST['searchText'], $sDate, $eDate, $page_size, $offset);
    $this->render('creditgenerated', array('creditDetails'=>$creditDetails,'lunchDate' => $lunchDate,'page_size' =>$page_size,'total_credit_generated'=>$total_credit_generated));
  }
  
   public function  actioncreditgeneratedlist() {
        $studio = $this->studio;
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['dt']) && $_REQUEST['dt'] != '') {
            $dt = stripcslashes($_REQUEST['dt']);
            $dt = json_decode($dt);
            $sDate = $dt->start;
            $eDate = $dt->end;
        } else if (!$dt || $dt == '') {
            $eDate = date('Y-m-d');
            $daysgo = date('d')-1;
            $sDate = date('Y-m-d', strtotime('-'.$daysgo.' days'));
        }
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        $creditDetails = MuviCreditPoints::model()->getCreditDetailsOfStudio($studio->id,$_REQUEST['searchText'], $sDate, $eDate, $page_size, $offset);
        if(isset($creditDetails['data']) && count($creditDetails['data'])){
            for($i=0;$i<count($creditDetails['data']);$i++){
                if($creditDetails['data'][$i]['rule_action_type'] == 1){
                    $creditDetails['data'][$i]['rule_action'] = 'Subscription';
                }else{
                    $creditDetails['data'][$i]['rule_action'] = 'Subscription Renewal';
                }
            }
        }
        $this->renderPartial('creditlist', array('details'=>$creditDetails));
    }
}