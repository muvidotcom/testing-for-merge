<?php

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\Ses\SesClient;

require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );

class MultistudioController extends Controller {

    public $defaultAction = 'index';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin';
        if (!(Yii::app()->user->id)) {
            $this->redirect(array('/index.php/'));
        } else if (!Yii::app()->common->checkparentStore($this->studio->id)) {
            $this->redirect(array('/index.php/'));
        } else {
            $this->checkPermission();
        }
        return true;
    }

    function actionindex() {
        if (Yii::app()->user->id) {
            $this->redirect('/multistudio/Sync');
            exit;
        }
        $this->render('index');
    }

    public function actionSync() {
        $studio_id = $this->studio->id;
        $this->pageTitle = "Muvi | Sync DATA";
        $this->breadcrumbs = array('Sync DATA');
        $this->headerinfo = "Sync DATA to Child stores";
        $dbcon = Yii::app()->db;
        $csql = "SELECT * FROM store_mapping_log WHERE studio_id=".$studio_id;
        $content = $dbcon->createCommand($csql)->queryAll();
        $content = CHtml::listData($content, 'id', 'mapping_code');
        $this->render('sync',array('content'=>$content));
    }

    public function actionCategorySync() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $ContentCategories = ContentCategories::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $ContentSubcategory = ContentSubcategory::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $contentModel = new ContentCategories();
        $ContentCategoriesarr = array();
        foreach ($ContentCategories as $t) {
            $ContentCategoriesarr[] = $t->attributes;
        }
        $contentsubModel = new ContentSubcategory();
        if (!empty($ContentSubcategory)) {
            $ContentSubcategoryarr = array();
            foreach ($ContentSubcategory as $s) {
                $ContentSubcategoryarr[] = $s->attributes;
            }
            $StudioConfig = new StudioConfig;
        }
        foreach ($StoreMapping as $key => $value) {
            // delete child store data from content_category & content_subcategory
            ContentCategories::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            ContentSubcategory::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            // then copy from parent studio 
            foreach ($ContentCategoriesarr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $contentModel->$key1 = $value1;
                }
                $contentModel->studio_id = $value['studio_id'];
                $contentModel->isNewRecord = true;
                $contentModel->primaryKey = NULL;
                $contentModel->save();
            }
            if (!empty($ContentSubcategory)) {
                foreach ($ContentSubcategoryarr as $key => $v) {
                    foreach ($v as $key1 => $value1) {
                        $contentsubModel->$key1 = $value1;
                    }
                    $contentsubModel->studio_id = $value['studio_id'];
                    $contentsubModel->isNewRecord = true;
                    $contentsubModel->primaryKey = NULL;
                    $contentsubModel->save();
                }
                $StudioConfig->studio_id = $value['studio_id'];
                $StudioConfig->config_key = 'subcategory_enabled';
                $StudioConfig->config_value = 1;
                $StudioConfig->isNewRecord = true;
                $StudioConfig->primaryKey = NULL;
                $StudioConfig->save();
            }
        }
        $vsql = "INSERT INTO `store_mapping_log`(`studio_id`,`mapping_code`, `status`) VALUES ({$studio_id},'category',1)";
        Yii::app()->db->createCommand($vsql)->execute();
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }

    public function actionMenusSync() {
        $studio_id = $this->studio->id;
        $ip_address = CHttpRequest::getUserHostAddress();
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $MenuItem = MenuItem::model()->findAll('studio_id = :studio_id AND language_id = 20', array(':studio_id' => $studio_id)); // for english only
        $MenuModel = new MenuItem();
        $urlRouting = new UrlRouting();
        $MenuItemarr = array();
        foreach ($MenuItem as $t) {
            $MenuItemarr[] = $t->attributes;
        }
        foreach ($StoreMapping as $key => $value) {
            $Menu = Menu::model()->find('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            MenuItem::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            UrlRouting::model()->deleteAll('studio_id = :studio_id AND mapped_url like "/media/list/menu_item_id/%"', array(':studio_id' => $value['studio_id']));
            
            // then copy from parent studio 
            foreach ($MenuItemarr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $MenuModel->$key1 = $value1;
                }
                $MenuModel->menu_id = $Menu['id'];
                $MenuModel->parent_id = 0;
                $MenuModel->studio_id = $value['studio_id'];
                $MenuModel->isNewRecord = true;
                $MenuModel->primaryKey = NULL;
                $MenuModel->save();
                $menu_item_id = $MenuModel->id;
                
                $urlRouting->permalink = $MenuModel->permalink;
                $urlRouting->mapped_url = '/media/list/menu_item_id/' . $menu_item_id;
                $urlRouting->studio_id = $value['studio_id'];
                $urlRouting->created_date = new CDbExpression("NOW()");
                $urlRouting->ip = $ip_address;
                $urlRouting->isNewRecord = true;
                $urlRouting->primaryKey = NULL;
                $urlRouting->save();
            }
        }
        $vsql = "INSERT INTO `store_mapping_log`(`studio_id`, `mapping_code`, `status`) VALUES ({$studio_id},'menu',1)";
        Yii::app()->db->createCommand($vsql)->execute();
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }

    /**
     * @desc only VOD,AOD and live stream content we sync
     */
    public function actionContentSync() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));

        $film_field = 'F.id as movie_id,F.name,F.content_types_id,F.parent_content_type_id,F.language,F.genre,F.censor_rating,F.story,F.content_category_value,F.content_subcategory_value,F.release_date';
        $fetchcustomfield = 'F.custom1,F.custom2,F.custom3,F.custom4,F.custom5,F.custom6,F.custom7,F.custom8,F.custom9,F.custom10,m.custom1 as cs1,m.custom2 as cs2,m.custom3 as cs3,m.custom4 as cs4,m.custom5 as cs5,m.custom6 as cs6';
        $movie_stream_field = 'm.id AS movie_stream_id,m.episode_title,m.episode_number,m.series_number,m.episode_story,m.episode_date,m.is_episode,m.embed_id';
        $livestream_field = 'l.feed_type,l.feed_url,l.feed_method';

        $sql_data = "SELECT P.*,{$livestream_field} FROM "
                . "( SELECT {$movie_stream_field},{$film_field},{$fetchcustomfield}"
                . " FROM movie_streams m,films F WHERE F.parent_id = 0 AND m.movie_id = F.id AND F.status = 1 AND F.studio_id=" . $studio_id
                . " ) AS P LEFT JOIN livestream l ON P.movie_id = l.movie_id order by P.is_episode ASC";
        $list = Yii::app()->db->createCommand($sql_data)->queryAll();
        
        foreach ($list as $listkey => $listvalue) {
            $flist[$listvalue['movie_id']][] = $listvalue;
        }
        //echo "<pre>";
//        print_r($flist);
//        exit;
        $Films = new Film();
        $solrObj = new SolrFunctions();
        $MovieStreams = new movieStreams();
        $livestream = new Livestream();
        foreach ($StoreMapping as $key => $value) {
            /*delete from film moviestream and livestream*/
            Film::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            movieStreams::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            Livestream::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            UrlRouting::model()->deleteAll('studio_id = :studio_id AND mapped_url like "/movie/show/content_id/%"', array(':studio_id' => $value['studio_id']));
            
            $solrobj->deleteSolrQuery("cat:content AND sku:" . $value['studio_id']);
            
            foreach ($flist as $listkey => $listvalue) {
                foreach ($listvalue as $k => $v) {
                    if ($v['is_episode'] == 0) {
                        /* INSERT into film table */
                        $movie = self::addContent($v, $value['studio_id'], $Films);
                        $movie_id = $movie['id'];
                        /* insert into url routing */
                        $urlRouts['permalink'] = $movie['permalink'];
                        $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
                        $urlRoutObj = new UrlRouting();
                        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $value['studio_id']);
                        /* insert into moviestream */
                        self::saveStream($v, $value['studio_id'], $movie_id,$MovieStreams,0);
                        /* solr insert */
                        if (HOST_IP != '127.0.0.1') {
                            if ($data['genre']) {
                                foreach ($data['genre'] as $key => $val) {
                                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                                    $solrArr['content_id'] = $movie_id;
                                    $solrArr['stream_id'] = $MovieStreams->id;
                                    $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                                    $solrArr['is_episode'] = $MovieStreams->is_episode;
                                    $solrArr['name'] = $movie['name'];
                                    $solrArr['permalink'] = $movie['permalink'];
                                    $solrArr['studio_id'] = $value['studio_id'];
                                    $solrArr['display_name'] = 'content';
                                    $solrArr['content_permalink'] = $movie['permalink'];
                                    $solrArr['genre_val'] = $val;
                                    $solrArr['product_format'] = '';
                                    $ret = $solrObj->addSolrData($solrArr);
                                }
                            } else {
                                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                                $solrArr['content_id'] = $movie_id;
                                $solrArr['stream_id'] = $MovieStreams->id;
                                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                                $solrArr['is_episode'] = $MovieStreams->is_episode;
                                $solrArr['name'] = $movie['name'];
                                $solrArr['permalink'] = $movie['permalink'];
                                $solrArr['studio_id'] = $value['studio_id'];
                                $solrArr['display_name'] = 'content';
                                $solrArr['content_permalink'] = $movie['permalink'];
                                $solrArr['genre_val'] = '';
                                $solrArr['product_format'] = '';
                                $ret = $solrObj->addSolrData($solrArr);
                            }
                        }
                        if ($v['content_types_id'] == 4) {
                            self::saveFeeds($v, $value['studio_id'], $movie_id,$livestream);
                        }
                    } else if ($v['is_episode'] == 1) {
                        self::saveStream($v, $value['studio_id'], $movie_id,$MovieStreams,1);
                    }
                }
            }
        }
        $vsql = "INSERT INTO `store_mapping_log`(`studio_id`,`mapping_code`, `status`) VALUES ({$studio_id},'content',1)";
        Yii::app()->db->createCommand($vsql)->execute();
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }

    function addContent($data, $studio_id, $Films) {
        $permalinkins = ($data['permalink'] != '') ? $data['permalink'] : $data['name'];
        $Films->studio_id = $studio_id;
        $Films->name = $data['name'];
        $uniqid = Yii::app()->common->generateUniqNumber();
        $Films->uniq_id = $uniqid;
        if (isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
            $Films->release_date = date('Y-m-d', strtotime($data['release_date']));
        //$Films->content_type_id = $data['content_type_id'];
        $Films->language = @$data['language'];
        $Films->genre = @$data['genre'];
        $Films->censor_rating = @$data['censer_rating'];
        $Films->story = @$data['story'];
        $Films->created_date = gmdate('Y-m-d H:i:s');
        $Films->last_updated_date = gmdate('Y-m-d H:i:s');

        $Films->content_types_id = $data['content_types_id'];
        $Films->parent_content_type_id = $data['parent_content_type_id'];
        $Films->content_category_value = $data['content_category_value'];
        if (@$data['content_subcategory_value'])
            $Films->content_subcategory_value = array_sum($data['content_subcategory_value']);
        $Films->permalink = Yii::app()->general->generatePermalink(stripslashes($permalinkins));
        $Films->status = 1;

        $Films->custom1 = @$data['custom1'];
        $Films->custom2 = @$data['custom2'];
        $Films->custom3 = @$data['custom3'];
        $Films->custom4 = @$data['custom4'];
        $Films->custom5 = @$data['custom5'];
        $Films->custom6 = @$data['custom6'];
        $Films->custom7 = @$data['custom7'];
        $Films->custom8 = @$data['custom8'];
        $Films->custom9 = @$data['custom9'];
        $Films->custom10 = @$data['custom10'];
        $Films->mapped_id = @$data['movie_id'];
        
        $Films->setIsNewRecord(true);
        $Films->setPrimaryKey(NULL);
        if ($Films->save()) {
            $content_id = $Films->id;
            $film = Film::model()->findByPk($content_id);
            $film->search_parent_id = $content_id;
            $film->save();
            return $Films->attributes;
        } else {
            return '';
        }
    }

    function saveFeeds($data, $studio_id = '', $movie_id = '',$livestream) {
        if (!$studio_id) {
            $studio_id = Yii::app()->user->studio_id;
        }
        if ($data) {
            $livestream->movie_id = $movie_id;
            $livestream->feed_type = $data['feed_type'];
            $livestream->feed_url = $data['feed_url'];
            $livestream->feed_method = $data['feed_method'];
            $livestream->studio_id = $studio_id;
            $livestream->created_by = Yii::app()->user->id;
            $livestream->created_date = gmdate('Y-m-d H:i:s');
            $livestream->ip = Yii::app()->common->getIP();
            /*if (@$data['method_type'] == 'push') {
                $livestream->stream_url = @$data['stream_url'];
                $livestream->stream_key = @$data['stream_key'];
                $livestream->is_recording = @$data['is_recording'];
                if (@$data['is_recording'] == 1 && @$data['delete_content'] == 1) {
                    $livestream->delete_no = @$data['delete_no'];
                    $livestream->delete_span = @$data['delete_span'];
                }
                if (@$data['start_streaming'] != '') {
                    $livestream->start_streaming = @$data['start_streaming'];
                }
            }*/
            $livestream->isNewRecord = true;
            $livestream->primaryKey = NULL;
            $livestream->save();
            return $livestream->id;
        } else {
            return '';
        }
    }
    function saveStream($v,$studio_id,$movie_id,$MovieStreams,$is_episode){
        $epdt = $v['episode_date'] ? date('Y-m-d', strtotime($v['episode_date'])) : gmdate('Y-m-d');
        $MovieStreams->episode_title = $v['episode_title'];
        $MovieStreams->episode_number = $v['episode_number'];
        $MovieStreams->series_number = $v['series_number'];
        $MovieStreams->studio_id = $studio_id;
        $MovieStreams->episode_story = $v['episode_story'];
        $MovieStreams->episode_date = $epdt;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->created_by = $studio_id;
        $MovieStreams->created_date = gmdate('Y-m-d');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        /* Add custom data */
        $MovieStreams->custom1 = @$v['cs1'];
        $MovieStreams->custom2 = @$v['cs2'];
        $MovieStreams->custom3 = @$v['cs3'];
        $MovieStreams->custom4 = @$v['cs4'];
        $MovieStreams->custom5 = @$v['cs5'];
        $MovieStreams->custom6 = @$v['cs6'];
        $MovieStreams->mapped_stream_id = @$v['movie_stream_id'];
        
        $MovieStreams->is_episode = $is_episode;
        $MovieStreams->isNewRecord = true;
        $MovieStreams->primaryKey = NULL;                        
        $MovieStreams->save();
    }
    public function actionFeaturedContentSync() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $FeaturedSection = FeaturedSection::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $FeaturedSectionModel = new FeaturedSection();
        $FeaturedContentModel = new FeaturedContent();
        $FeaturedSectionModelarr = array();
        
        foreach ($FeaturedSection as $t) {
            $FeaturedSectionModelarr[] = $t->attributes;
        }
        foreach ($StoreMapping as $key => $value) {
            FeaturedSection::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            // then copy from parent studio 
            foreach ($FeaturedSectionModelarr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $FeaturedSectionModel->$key1 = $value1;
                }
                $FeaturedSectionModel->studio_id = $value['studio_id'];
                $FeaturedSectionModel->isNewRecord = true;
                $FeaturedSectionModel->primaryKey = NULL;
                $FeaturedSectionModel->save();
                $sectionid = $FeaturedSectionModel->id;
                $FeaturedContent = Yii::app()->db->createCommand()
                    ->select("movie_id,is_episode,id_seq")
                    ->from("featured_content")
                    ->where('studio_id = '.$studio_id.' AND section_id = ('.$v['id'].')')->queryAll();
                foreach ($FeaturedContent as $ck => $cv) {
                    if($cv['is_episode']==1){
                        $tbl = 'movie_streams';
                        $field='mapped_stream_id';
                    }else{
                        $tbl = 'films';
                        $field='mapped_id';
                    }
                    $FeaturedContentmapping = Yii::app()->db->createCommand()
                        ->select("id")
                        ->from($tbl)
                        ->where('studio_id = '.$value['studio_id'].' AND '.$field.' = ('.$cv['movie_id'].')')->queryROW();
                    
                    $FeaturedContentModel->movie_id = $FeaturedContentmapping['id'];
                    $FeaturedContentModel->is_episode = $cv['is_episode'];
                    $FeaturedContentModel->studio_id = $value['studio_id'];
                    $FeaturedContentModel->section_id = $sectionid;
                    $FeaturedContentModel->id_seq = $cv['id_seq'];
                    $FeaturedContentModel->isNewRecord = true;
                    $FeaturedContentModel->primaryKey = NULL;
                    $FeaturedContentModel->save();
                }
            }
        }
        $vsql = "INSERT INTO `store_mapping_log`(`studio_id`, `mapping_code`, `status`) VALUES ({$studio_id},'feature',1)";
        Yii::app()->db->createCommand($vsql)->execute();
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }
    public function actionPaymentDetailsSync() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $StudioPaymentGateways = StudioPaymentGateways::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $StudioPaymentGatewaysModel = new StudioPaymentGateways();
        foreach ($StudioPaymentGateways as $t) {
            $StudioPaymentGatewaysModelarr[] = $t->attributes;
        }
        echo "<pre>";
        print_r($StudioPaymentGatewaysModelarr);
        exit;
        foreach ($StoreMapping as $key => $value) {
            StudioPaymentGateways::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            // then copy from parent studio 
            foreach ($StudioPaymentGatewaysModelarr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $StudioPaymentGatewaysModel->$key1 = $value1;
                }
                $StudioPaymentGatewaysModel->studio_id = $value['studio_id'];
                $MenuModel->isNewRecord = true;
                $MenuModel->primaryKey = NULL;
                $MenuModel->save();
            }
        }
        $vsql = "INSERT INTO `store_mapping_log`(`studio_id`, `mapping_code`, `status`) VALUES ({$studio_id},'payment',1)";
        Yii::app()->db->createCommand($vsql)->execute();
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }
    public function actionCastCrewSync() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $Celebrity = Celebrity::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $CelebrityModel = new Celebrity();
        $MovieCastModel = new MovieCast();
        foreach ($Celebrity as $t) {
            $Celebrityarr[] = $t->attributes;
        }
        foreach ($StoreMapping as $key => $value) {
            Celebrity::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            // then copy from parent studio 
            foreach ($Celebrityarr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $CelebrityModel->$key1 = $value1;
                }
                $CelebrityModel->studio_id = $value['studio_id'];
                $CelebrityModel->mapped_id = $v['id'];
                $CelebrityModel->isNewRecord = true;
                $CelebrityModel->primaryKey = NULL;
                $CelebrityModel->save();
                $cid = $CelebrityModel->id;
                $MovieCast = MovieCast::model()->findAll('celebrity_id = :celebrity_id', array(':celebrity_id' => $v['id']));
                foreach ($MovieCast as $ck => $cv) {
                    if($cv['movie_id']){
                        $MovieCastmapping = Yii::app()->db->createCommand()
                            ->select("id")
                            ->from("films")
                            ->where('studio_id = '.$value['studio_id'].' AND mapped_id = ('.$cv['movie_id'].')')->queryROW();
                        if($MovieCastmapping['id']){
                            $MovieCastModel->movie_id = $MovieCastmapping['id'];
                            $MovieCastModel->celebrity_id = $cid;
                            $MovieCastModel->cast_type = $cv['cast_type'];
                            $MovieCastModel->cast_name = $cv['cast_name'];
                            $MovieCastModel->isNewRecord = true;
                            $MovieCastModel->primaryKey = NULL;
                            $MovieCastModel->save();
                        }
                    }
                }
            }
        }
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }
    public function actionaddExistingCastsToSolr() {
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $solrobj = new SolrFunctions();
        $now = new CDbExpression("NOW()");
        
        foreach ($StoreMapping as $key => $value) {
            $studio_id = $value['studio_id'];    
            $solrobj->deleteSolrQuery("cat:star AND sku:" . $studio_id);
            $data = Yii::app()->db->createCommand()
                    ->select('id,name,permalink,studio_id')
                    ->from('celebrities')
                    ->where('created_date <=:time AND studio_id=:studio_id AND parent_id=:parent_id', array(':time' => $now, ':studio_id' => $studio_id, ':parent_id' => 0))
                    ->queryAll();
            foreach ($data as $dk => $dv) {
                if (HOST_IP != '127.0.0.1') {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $dv['id'];
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = '';
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $dv['name'];
                    $solrArr['permalink'] = $dv['permalink'];
                    $solrArr['studio_id'] = $dv['studio_id'];
                    $solrArr['display_name'] = 'star';
                    $solrArr['content_permalink'] = 'star';
                    $solrObj = new SolrFunctions();
                    $ret_solr = $solrObj->addSolrData($solrArr);
                }
            }
        }
        Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
    }
	public function actionStaticPagesSync() {		
        $studio_id = $this->studio->id;
        $StoreMapping = StoreMapping::model()->findAll('parent_studio_id=:pstudio_id', array(':pstudio_id' => $studio_id));
        $Page = Page::model()->findAll('studio_id = :studio_id', array(':studio_id' => $studio_id));
        $PageModel = new Page();
        $urlRoutObj = new UrlRouting();
        $Pagearr = array();
        foreach ($Page as $t) {
            $Pagearr[] = $t->attributes;
		}
        foreach ($StoreMapping as $key => $value) {
            Page::model()->deleteAll('studio_id = :studio_id', array(':studio_id' => $value['studio_id']));
            UrlRouting::model()->deleteAll('studio_id = :studio_id AND mapped_url like "/page/show/permalink/%"', array(':studio_id' => $value['studio_id']));
            // then copy from parent studio 
            foreach ($Pagearr as $key => $v) {
                foreach ($v as $key1 => $value1) {
                    $PageModel->$key1 = $value1;
                }
				$PageModel->studio_id = $value['studio_id'];
                $PageModel->isNewRecord = true;
                $PageModel->primaryKey = NULL;
                $PageModel->save();
				
                $urlRouts['permalink'] = $v['permalink'];
                $urlRouts['mapped_url'] = '/page/show/permalink/' . $v['permalink'];
                $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $value['studio_id']);
            }
        }
		Yii::app()->user->setFlash('success', 'Sync successfully.');
        $this->redirect('/multistudio/Sync');
        exit;
	}
}
