<?php

class ContactController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public $layout='main';     
    public function actionIndex()
    {
        $this->pageTitle = 'Contact Muvi to create your own Branded Video Streaming Site';
        $this->pageKeywords = 'Muvi, Muvi.com, Muvi Contact, Muvi Contact, Contact Muvi.com';
        $this->pageDescription = 'Contact us and learn how Muvi can help you launch your White label VoD Site similar to a Netflix Clone at Zero Cost!';
        $this->render('index');
    }

    public function actionSend(){
        $response = 'error';
        $msg = 'Sorry, there is some error in sending the email';  
        $contact_email = 'studio@muvi.com';
        
        $ret = Yii::app()->common->sendSampleEmail("ratikanta@muvi.com", "Contact Form Check in Muvi.com", $_REQUEST);
        
         $temporary_email_log = new TemporaryEmailLog();
         $temporary_email_log -> name = $_REQUEST['name'];
         $temporary_email_log -> email = $_REQUEST['email'];
         $temporary_email_log -> form_data = serialize($_REQUEST);
         $temporary_email_log -> page = 'contact_us';
         $temporary_email_log -> save();
         
        if(isset($_REQUEST) && count($_REQUEST) > 0 && ($_REQUEST['ccheck']=='') && ($_REQUEST['submit-btn']=='contact')){
	
            $name = isset($_REQUEST['name'])?trim($_REQUEST['name']):'';
            $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
            $phone = isset($_REQUEST['phone'])?trim($_REQUEST['phone']):'';
            $company = isset($_REQUEST['company'])?trim($_REQUEST['company']):'';
            $message = isset($_REQUEST['message'])?trim($_REQUEST['message']):'';
			if($message == '' || $email == '' || $phone == ''){
				$ret = array('status' => 'error', 'message' => 'Email, Phone, Message  can\'t be left blank!');
				echo json_encode($ret);exit;
			}
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                $source = Yii::app()->request->cookies['REFERRER'];

                //Unset source cookie
                //unset($_COOKIE['REFERRER']);
                //setcookie('REFERRER', '', time() - 60000, '/', DOMAIN_COOKIE, false, false);
            }
            
            $contact_us = new ContactUs();
            $contact_us -> name = $name;
            $contact_us -> email = $email;
            $contact_us -> company = $company;
            $contact_us -> phone = $phone;
            $contact_us -> message = $message;
            $contact_us -> source = $source;
            $contact_us -> created_date = date('Y-m-d H:i:s');
            $contact_us -> ip = $_SERVER["REMOTE_ADDR"];
            $contact_us -> save();
            
            $to = array($contact_email);
            $to_name = 'Muvi';
            $from = 'support@muvi.com';
           // $from='studio@muvi.com';
            $from_name = 'Muvi Support';
            $site_url = Yii::app()->getBaseUrl(true);

            $logo = EMAIL_LOGO;
            $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
            
            $msg = '<p>'.$name.' has fill up the contact request in '.$to_name.'</p>';
            $msg.= '<p>';
            $msg.= '<b>Name :</b> '.$name.'<br />';
            $msg.= '<b>Email :</b> '.$email.'<br />';
            $msg.= '<b>Phone :</b> '.$phone.'<br />';
            $msg.= '<b>Company :</b> '.$company.'<br />';
            $msg.= '<b>Message :</b> '.$message.'<br />';
            $msg.= '</p>';
            
            $params = array(
                'website_name'=> $site_url,
                'logo'=> $logo,
                'msg'=> $msg
            );

            $subject = 'New contact from Muvi';
            $adminGroupEmail = array('sales@muvi.com');
                   
            $mailAddress = array(
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => $from_name,
                'to' => $adminGroupEmail
            );            
            
            $template_name= 'studio_contact_us';
            //$this->mandrilEmail($template_name, $params, $mailAddress);              
         //New code for sending email by Amazon SES    
        Yii::app()->theme = 'bootstrap';
        $thtml = Yii::app()->controller->renderPartial('//email/studio_contact_us',array('params'=>$params),true);
        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from,'','','',$name); 
        //New code for sending email by Amazon SES
            $response = 'success';
            $msg = 'Thank you for contacting us. We will respond to you as soon as possible.';
            $nm = explode(" ", $name, 2);
            Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
            
            $user = array('email' => $email, 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);            
    }
    public function actionSendFromZopim(){
         $temporary_email_log = new TemporaryEmailLog();
         $temporary_email_log -> name = $_REQUEST['name'];
         $temporary_email_log -> email = $_REQUEST['email'];
         $temporary_email_log -> form_data = serialize($_REQUEST);
         $temporary_email_log -> page = 'zopim';
         $temporary_email_log -> save();
                 
        $email = $_POST['email'];
        $name  = $_POST['name'];
        $nm = explode(" ", $name, 2);
        Hubspot::AddToHubspot($email,$nm[0],$nm[1],'');
        $user = array('email' => $email, 'firstName' => $nm[0],'lastName'=>$nm[1],'add_list' => 'Muvi All');
        require('MadMimi.class.php');
        $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
        $mimi->AddUser($user);
    }
    
    public function actionLiveDemoRequest(){
        $ret = Yii::app()->common->sendSampleEmail("ratikanta@muvi.com", "Live Demo Request in Muvi.com", $_REQUEST);
        $temporary_email_log = new TemporaryEmailLog();
        $temporary_email_log -> name = $_REQUEST['name'];
        $temporary_email_log -> email = $_REQUEST['email'];
        $temporary_email_log -> form_data = serialize($_REQUEST);
        $temporary_email_log -> page = 'Live Demo Request';
        $temporary_email_log -> save();
         
        if (isset($_REQUEST) && count($_REQUEST) > 0 ) {
            $name = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : '';
            $company = isset($_REQUEST['company']) ? trim($_REQUEST['company']) : '';
            $phone = isset($_REQUEST['phone']) ? trim($_REQUEST['phone']) : '';
            $email = isset($_REQUEST['email']) ? trim($_REQUEST['email']) : '';
            $date = isset($_REQUEST['datepicker']) ? trim($_REQUEST['datepicker']) : '';
            $time = isset($_REQUEST['timepicker']) ? trim($_REQUEST['timepicker']) : '';
            $timezone = isset($_REQUEST['timezone']) ? trim($_REQUEST['timezone']) : '';
            $country = isset($_REQUEST['country']) ? trim($_REQUEST['country']) : '';
            $ip_address = isset($_REQUEST['ip_address']) ? trim($_REQUEST['ip_address']) : '';
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                    $source = Yii::app()->request->cookies['REFERRER'];
            }
            if($email!='') {
                /*$nm = explode(" ", $name, 2);
                Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
                */
                $user = array('email' => $email,'add_list' => 'MUVI LIVE DEMO REQUEST');
                require('MadMimi.class.php');
                $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                $mimi->AddUser($user);

                $muvi_live_demo_request_form = new LiveDemoRequest();
                $muvi_live_demo_request_form->name = $name;
                $muvi_live_demo_request_form->email = $email;
                $muvi_live_demo_request_form->company = $company;
                $muvi_live_demo_request_form->phone = $phone;
                $muvi_live_demo_request_form->date = $date;
                $muvi_live_demo_request_form->time = $time;
                $muvi_live_demo_request_form->timezone = $timezone;
                $muvi_live_demo_request_form->country = $country;
                $muvi_live_demo_request_form->ip_address = $ip_address;
                $muvi_live_demo_request_form->source = $source;
                $muvi_live_demo_request_form->created_date = date('Y-m-d H:i:s');
                $muvi_live_demo_request_form->save();
                $insert_id = $muvi_live_demo_request_form->id;

                if($insert_id){
                    $from = $email;
                    $from_name = $name;
                    $site_url = Yii::app()->getBaseUrl(true);

                    $logo = EMAIL_LOGO;
                    $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
                    $msg = '<p>';
                    $msg.= '<b>Name :</b> '.$name.'<br />';
                    $msg.= '<b>Email :</b> <a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br />';
                    if($phone!=''){
                        $msg.= '<b>Phone :</b> '.$phone.'<br />';
                    }else{
                        $msg.= '<b>Phone :</b> N/A <br />';
                    }
                    if($company!=''){
                        $msg.= '<b>Company :</b> '.$company.'<br />';
                    }else{
                        $msg.= '<b>Company :</b> N/A <br />';
                    }
                    if($country!=''){
                        $msg.= '<b>Country :</b> '.$country.'<br />';
                    }else{
                        $msg.= '<b>Country :</b> N/A <br />';
                    }
                    $msg.= '<b>Request Date :</b> '.$date.'</a><br />';
                    $msg.= '<b>Request Time :</b> '.$time.'</a><br />';
                    $msg.= '<b>Request Timezone :</b> '.$timezone.'</a><br />';
                    $msg.= '<b>Submission Time :</b> '.$muvi_live_demo_request_form->created_date.'<br />';
                    $msg.= '</p>';

                    $params = array('website_name' => $site_url,'logo'=> $logo,'msg' => $msg);

                    $subject = 'New Live Demo Request';
                    $adminGroupEmail = array('sales@muvi.com');

                    $cc=array();
                    $bcc=array();           
                    Yii::app()->theme = 'bootstrap';
                    $thtml = Yii::app()->controller->renderPartial('//email/Live_Demo_Request',array('params'=>$params),true);
                    $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from,$cc,$bcc,'',$name);    

                    $response = 'success';
                    $msg= 'Data saved successfully';
                    $ret = array('status' => $response, 'message' => $msg, 'code' =>'1');
                    echo json_encode($ret);exit;

                }else{
                        $response = 'error';
                        $msg = 'Something went wrong';
                        $ret = array('status' => $response, 'message' => $msg, 'code' =>'0');
                        echo json_encode($ret);exit;
                }
            }else{
                    $response = 'error';
                    $msg = 'Something went wrong';
                    $ret = array('status' => $response, 'message' => $msg, 'code' =>'0');
                    echo json_encode($ret);exit;
            }
        }           
    }
    public function actionHackathonRegister(){

        $response = 'error';
        $msg = 'Sorry, there is some error in sending the email';  
        $contact_email = 'studio@muvi.com';
        if(isset($_REQUEST) && count($_REQUEST) > 0 && ($_REQUEST['ccheck']=='') && ($_REQUEST['submit-btn']=='hackathon')){
            $name = isset($_REQUEST['name'])?trim($_REQUEST['name']):'';
            $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
            $phone= isset($_REQUEST['phone'])?trim($_REQUEST['phone']):'';
            $why_interested= isset($_REQUEST['why_interested'])?trim($_REQUEST['why_interested']):'';
            $how_hear= isset($_REQUEST['how_hear'])?trim($_REQUEST['how_hear']):'';
            $source = '';
            if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                $source = Yii::app()->request->cookies['REFERRER'];
            }
            $country = isset($_REQUEST['country'])?trim($_REQUEST['country']):'';
            $ip_address = isset($_REQUEST['ip_address'])?trim($_REQUEST['ip_address']):'';
             
            $hackathon_register = new HackathonRegister();
            $hackathon_register->name = $name;
            $hackathon_register->email = $email;
            $hackathon_register->phone = $phone;
            $hackathon_register->why_interested = $why_interested;
            $hackathon_register->how_hear= $how_hear;
            $hackathon_register->source = $source;
            $hackathon_register->country = $country;
            $hackathon_register->created_date = date('Y-m-d H:i:s');
            $hackathon_register->ip = $ip_address;
            $hackathon_register->save();
     
            $response = 'success';
            $msg = '<strong>Thank you for registering on Hackathon 18.</strong>';
        }
        $ret = array('status' => $response, 'message' => $msg);
        echo json_encode($ret);            
        }
        public function actionSavenewsletter(){
           $email = isset($_REQUEST['email_id'])?trim($_REQUEST['email_id']):''; 
           if($email!='') {
            $user = array('email' => $email,'add_list' => 'MUVI NEWSLETTER SUBSCRIBERS');
            require('MadMimi.class.php');
            $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
            $mimi->AddUser($user);
            echo "Succesfully saved to madmimi";
           }    
        }
        public function actionSavemuvinewslettersubscriber(){
           if (isset($_REQUEST) && count($_REQUEST) > 0 ) {
                $email_id = isset($_REQUEST['email_id'])?trim($_REQUEST['email_id']):'';
                $ip_address = isset($_REQUEST['ip_address'])?trim($_REQUEST['ip_address']):'';
                if($email_id!='') {
                    $muvi_newsletter_subscriber_form = new MuviNewsletterSubscriber();
                    if($muvi_newsletter_subscriber_form->findByAttributes(array("email"=>$email_id))){
                        $response1 = 'error';
                        $msg1 = 'Email Address Already Exists';
                        $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'0');
                        echo json_encode($ret1);exit;
                    } else {
                        $user = array('email' => $email_id,'add_list' => 'MUVI NEWSLETTER SUBSCRIBERS');
                        require('MadMimi.class.php');
                        $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                        $mimi->AddUser($user);
                        $muvi_newsletter_subscriber_form = new MuviNewsletterSubscriber();
                        $muvi_newsletter_subscriber_form->ip = $ip_address;
                        $muvi_newsletter_subscriber_form->email = $email_id;
                        $muvi_newsletter_subscriber_form->created_date = date('Y-m-d H:i:s');
                        $muvi_newsletter_subscriber_form->save();
                        $insert_id = $muvi_newsletter_subscriber_form->id;
                        if($insert_id){
                            $response1 = 'success';
                            $msg1= 'Subscribed To Our Newsletter Successfully!...';
                            $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'1');
                            echo json_encode($ret1);exit;
                        }else{
                            $response1 = 'error';
                            $msg1 = 'Oops!... Something Went Wrong';
                            $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'0');
                            echo json_encode($ret1);exit;
                        }
                    }
                } else {
                    $response1 = 'error';
                    $msg1 = 'Oops!... Something Went Wrong';
                    $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'0');
                    echo json_encode($ret1);exit;
                }
           }    
        }
        public function actionWhitepaperuser(){
            if (isset($_REQUEST) && count($_REQUEST) > 0 ) {
                $name = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : '';
                $email = isset($_REQUEST['email']) ? trim($_REQUEST['email']) : '';
                $company = isset($_REQUEST['company']) ? trim($_REQUEST['company']) : '';
                $phone = isset($_REQUEST['phone']) ? trim($_REQUEST['phone']) : '';
                $country = isset($_REQUEST['country']) ? trim($_REQUEST['country']) : '';
                $ip_address = isset($_REQUEST['ip_address']) ? trim($_REQUEST['ip_address']) : '';
                $download_file = isset($_REQUEST['download_file']) ? trim($_REQUEST['download_file']) : '';
                $download_file = addslashes($download_file);
                $post_id= isset($_REQUEST['post_id']) ? trim($_REQUEST['post_id']) : '';
                $pdf_link = get_field('pdf_file',$post_id);
                $source = '';
                if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                    $source = Yii::app()->request->cookies['REFERRER'];
                }   
                if($email!='') {
                    /*$nm = explode(" ", $name, 2);
                    Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
                    */
                    $user = array('email' => $email,'add_list' => 'MUVI PDF DOWNLOADS');
                    require('MadMimi.class.php');
                    $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                    $mimi->AddUser($user);
                    
                    $muvi_pdf_form = new MuviPdfForm();
                    $muvi_pdf_form->name = $name;
                    $muvi_pdf_form->email = $email;
                    $muvi_pdf_form->company = $company;
                    $muvi_pdf_form->phone = $phone;
                    $muvi_pdf_form->country = $country;
                    $muvi_pdf_form->ip_address = $ip_address;
                    $muvi_pdf_form->download_file = $download_file;
                    $muvi_pdf_form->source = $source;
                    $muvi_pdf_form->created_date = date('Y-m-d H:i:s');
                    $muvi_pdf_form->save();
                    $insert_id = $muvi_pdf_form->id;

                    if($insert_id){
                        $from = $email;
                        $from_name = $name;
                        $site_url = Yii::app()->getBaseUrl(true);

                        $logo = EMAIL_LOGO;
                        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
                        $msg = '<p>';
                        $msg.= '<b>Name :</b> '.$name.'<br />';
                        $msg.= '<b>Email :</b> <a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br />';
                        if($phone!=''){
                            $msg.= '<b>Phone :</b> '.$phone.'<br />';
                        }else{
                            $msg.= '<b>Phone :</b> N/A <br />';
                        }
                        if($company!=''){
                            $msg.= '<b>Company :</b> '.$company.'<br />';
                        }else{
                            $msg.= '<b>Company :</b> N/A <br />';
                        }
                        if($country!=''){
                            $msg.= '<b>Country :</b> '.$country.'<br />';
                        }else{
                            $msg.= '<b>Country :</b> N/A <br />';
                        }
                        $msg.= '<b>Downloaded File :</b> <a alt="" href="'.get_permalink($post_id).'">'.$download_file.'</a><br />';
                        $msg.= '<b>Download Time :</b> '.$muvi_pdf_form->created_date.'<br />';
                        $msg.= '</p>';

                        $params = array(
                                'website_name' => $site_url,
                                'logo'=> $logo,
                                'msg' => $msg
                        );

                        $subject = 'New PDF Download';
                        $adminGroupEmail = array('sales@muvi.com');

                        $mailAddress = array(
                                'subject' => $subject,
                                'from_email' => $from,
                                'from_name' => $from_name,
                                'to' => $adminGroupEmail
                        );            
                        $cc=array('marketing@muvi.com');
                        $bcc=array();           
                        Yii::app()->theme = 'bootstrap';
                        $thtml = Yii::app()->controller->renderPartial('//email/pdf_download',array('params'=>$params),true);
                        $returnVal=$this->sendmailViaAmazonsdk($thtml,$subject,$adminGroupEmail,$from,$cc,$bcc,'',$name);

                        $response1 = 'success';
                        $msg1= 'Data saved successfully';
                        $ret1 = array('status' => $response1, 'message' => $msg1,'pdf_link'=>$pdf_link, 'code' =>'1');
                        echo json_encode($ret1);exit;
                    }else{
                        $response1 = 'error';
                        $msg1 = 'Something went wrong';
                        $ret1 = array('status' => $response1, 'message' => $msg1,'pdf_link'=>'', 'code' =>'0');
                        echo json_encode($ret1);exit;
                    }
                }else{
                    $response1 = 'error';
                    $msg1 = 'Something went wrong';
                    $ret1 = array('status' => $response1, 'message' => $msg1,'pdf_link'=>'', 'code' =>'0');
                    echo json_encode($ret1);exit; 
                }
            }
        }
        public function actionMigrateuser(){
            if (isset($_REQUEST) && count($_REQUEST) > 0 ) {
                $name = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : '';
                $company = isset($_REQUEST['company']) ? trim($_REQUEST['company']) : '';
                $phone = isset($_REQUEST['phone']) ? trim($_REQUEST['phone']) : '';
                $email = isset($_REQUEST['email']) ? trim($_REQUEST['email']) : '';
                $platform = isset($_REQUEST['platform']) ? trim($_REQUEST['platform']) : '';
                $link = isset($_REQUEST['link']) ? trim($_REQUEST['link']) : '';
                $country = isset($_REQUEST['country']) ? trim($_REQUEST['country']) : '';
                $ip_address = isset($_REQUEST['ip_address']) ? trim($_REQUEST['ip_address']) : '';
                $source = '';
                if (isset(Yii::app()->request->cookies['REFERRER']) && trim(Yii::app()->request->cookies['REFERRER'])) {
                    $source = Yii::app()->request->cookies['REFERRER'];
                }
                if($email!='') {
                    /*$nm = explode(" ", $name, 2);
                    Hubspot::AddToHubspot($email,$nm[0],$nm[1],$phone);
                    */
                    $user = array('email' => $email,'add_list' => 'MUVI PDF DOWNLOADS');
                    require('MadMimi.class.php');
                    $mimi = new MadMimi(MADMIMI_EMAIL, MADMIMI_KEY);
                    $mimi->AddUser($user);

                    $muvi_migrate_form = new MuviMigrateForm();
                    $muvi_migrate_form->name = $name;
                    $muvi_migrate_form->company = $company;
                    $muvi_migrate_form->phone = $phone;
                    $muvi_migrate_form->email = $email;
                    $muvi_migrate_form->platform = $platform;
                    $muvi_migrate_form->link = $link;
                    $muvi_migrate_form->country = $country;
                    $muvi_migrate_form->ip_address = $ip_address;
                    $muvi_migrate_form->source = $source;
                    $muvi_migrate_form->created_date = date('Y-m-d H:i:s');
                    $muvi_migrate_form->save();
                    $insert_id = $muvi_migrate_form->id;

                    if($insert_id){
                        $from = $email;
                        $from_name = $name;
                        $site_url = Yii::app()->getBaseUrl(true);

                        $logo = EMAIL_LOGO;
                        $logo = '<a href="'.$site_url.'"><img src="'.$logo.'" alt="Muvi" /></a>';
                        $msg = '<p>';
                        $msg.= '<b>Name :</b> '.$name.'<br />';
                        $msg.= '<b>Email :</b> <a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br />';
                        if($phone!=''){
                                $msg.= '<b>Phone :</b> '.$phone.'<br />';
                        }else{
                                $msg.= '<b>Phone :</b> N/A <br />';
                        }
                        if($company!=''){
                                $msg.= '<b>Company :</b> '.$company.'<br />';
                        }else{
                                $msg.= '<b>Company :</b> N/A <br />';
                        }
                        if($country!=''){
                                $msg.= '<b>Country :</b> '.$country.'<br />';
                        }else{
                                $msg.= '<b>Country :</b> N/A <br />';
                        }
                        
                        $msg.= '<b>Platform :</b> '.$platform.'</a><br />';
                        $msg.= '<b>Link To Website / App :</b> '.$link.'</a><br />';
                        $msg.= '<b>Submission Time :</b> '.$muvi_migrate_form->created_date.'<br />';
                        $msg.= '</p>';

                        $params = array('website_name' => $site_url,'logo'=> $logo,'msg' => $msg,'title' =>$name);

                        $subject = 'Migration Request';
                        $adminGroupEmail = array('sales@muvi.com');

                        $cc=array();
                        $bcc=array();           
                        Yii::app()->theme = 'bootstrap';
                        $thtml1 = Yii::app()->controller->renderPartial('//email/migrate_form_admin',array('params'=>$params),true);
                        $thtml2 = Yii::app()->controller->renderPartial('//email/migrate_form_customer',array('params'=>$params),true);
                        $returnVal1=$this->sendmailViaAmazonsdk($thtml1,$subject,$adminGroupEmail,$from,$cc,$bcc,'',$name);
                        $returnVal2=$this->sendmailViaAmazonsdk($thtml2,$subject,$email,'sales@muvi.com',$cc,$bcc,'','Muvi Sales');

                        $response1 = 'success';
                        $msg1= 'Data saved successfully';
                        $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'1');
                        echo json_encode($ret1);exit;
                        
                    }else{
                        $response1 = 'error';
                        $msg1 = 'Something went wrong';
                        $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'0');
                        echo json_encode($ret1);exit;
                    }
                }else{
                    $response1 = 'error';
                    $msg1 = 'Something went wrong';
                    $ret1 = array('status' => $response1, 'message' => $msg1, 'code' =>'0');
                    echo json_encode($ret1);exit;
                }
            }
        }
    }