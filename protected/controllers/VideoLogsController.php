<?php

class VideoLogsController extends Controller {
        
    protected function beforeAction($action) {
        parent::beforeAction($action);
        Yii::app()->theme = false;
        Yii::app()->layout = false;
        $actionArr = array('videoViewlog','videoBandwidthLog');
        if(in_array(Yii::app()->controller->action->id, $actionArr)){
            return true;
        }else{
            if (!(Yii::app()->user->id)) {
                Yii::app()->user->setFlash('error', 'Login to access the page.');
                $this->redirect(Yii::app()->getBaseUrl(TRUE) . '/user/login');
                exit;
            }
        }
        return true;
    }

    public function actionaddDataToRestrictDevice() {
        $data = "";
        if(@$_REQUEST['movie_stream_id'] != '' && @$_REQUEST['id'] != ''){
            $postData = $_REQUEST;
            $postData['studio_id'] = Yii::app()->common->getStudioId();;
            $postData['user_id'] = Yii::app()->user->id;
            $data = RestrictDevice::model()->dataSave($postData);
        }
        echo $data;
    }
    
    public function actiondeleteDataFromRestrictDevice() {
        if(@$_REQUEST['id'] != ''){
            $data = RestrictDevice::model()->deleteData($_REQUEST['id']);
        }
    }
    
    public function actionvideoViewlog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed()) {
            if ((isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log) || (!isset(Yii::app()->user->add_video_log))) {
                $data = array();
                $data = $_REQUEST;
                $data['movie_id'] = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
                $data['stream_id'] = isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0;
                $data['studio_id'] = isset($_REQUEST['studio_id']) ? $_REQUEST['studio_id'] : Yii::app()->common->getStudioId();
                if(@$_REQUEST['user_id_of_viewer'] != 0){
                    $data['user_id'] = $_REQUEST['user_id_of_viewer'];
                } else{
                    $data['user_id'] = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                }
                $data['ip_address'] = $ip_address;
				$data['log_temp_id'] = (isset($_REQUEST['log_id_temp']) && intval($_REQUEST['log_id_temp'])) ? $_REQUEST['log_id_temp'] : 0;
                $video_log_id = VideoLogs::model()->dataSave($data);
                $avail_time = 1;
                if(isset($data['enableWatchDuration']) && $data['enableWatchDuration'] == 1 && $data['user_id']>0){
                    $sum = Yii::app()->db->createCommand()
                        ->select('sum(played_length)')
                        ->from('video_logs t')
                        ->where('t.user_id ='.$data['user_id'] . ' and date(t.created_date )="'. date("Y-m-d").'" and t.is_watch_durationenabled = 1')
                        ->queryScalar();
                    $watch_duration_sec = Yii::app()->custom->restrictDevices($data['studio_id'], 'daily_watch_duration');
                    if($watch_duration_sec <= $sum){
                        $avail_time = 0;
                    }
                }
                if ($video_log_id) {
                   $response = array(
                        'log_id_temp' => $video_log_id[0],
                        'log_id' => $video_log_id[1],
                        'avail_time' => $avail_time  
                    );
                    echo (json_encode($response));
                    exit;
                } else {
                    echo 0;
                    exit;
                }
            }
        }
    }
    public function actionvideoBandwidthLog() {
		$ip_address = CHttpRequest::getUserHostAddress();
		if (Yii::app()->aws->isIpAllowed()) {
			if ((isset(Yii::app()->user->add_video_log) && Yii::app()->user->add_video_log) || (!isset(Yii::app()->user->add_video_log))) {
				$data = array();
				$data = $_REQUEST;
				if (strtolower($_REQUEST['request_type']) == 'mped_dash') {
					//mpeg_dash
					$bandwidth_used = $_REQUEST['totDRMBandwidth'];
                } else if(strtolower($_REQUEST['request_type']) == 'livestreaming'){
                    //livestreaming
                    $bandwidth_used = $_REQUEST['totalBandwidth'];
				} else {
					//not mped_dash
					$movie_stream_data = movieStreams::model()->findByPk($_REQUEST['video_id']);
					$res_size = json_decode($movie_stream_data->resolution_size, true);
					$duration = $_REQUEST['duration'];
                    if(@$movie_stream_data->video_duration){
                        $duration = explode(":",$movie_stream_data->video_duration);
                        $duration = @$duration[0] * 3600 + @$duration[1] * 60 + @$duration[2];
                        $res = $_REQUEST['resolution'];
                        $size = $res_size[$res];
                        $buff_duration = $_REQUEST['buff_dur'];
                        if($size != '' && $duration > 0){
                            $bandwidth_used = ($size / $duration) * round($buff_duration);
                        }
                    }
                }
                if(@$_REQUEST['user_id_of_viewer'] != 0){
                    $data['user_id'] = $_REQUEST['user_id_of_viewer'];
                } else{
                    $data['user_id'] = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
                }
				$data['studio_id'] = isset($_REQUEST['studio_id']) ? $_REQUEST['studio_id'] : Yii::app()->common->getStudioId();
                $data['bandwidth_used'] = @$bandwidth_used;
				$data['ip_address'] = $ip_address;
                $is_log = Yii::app()->aws->sentMailForWrongBandwidthLog($data);
                if(@$data['buff_log_idtemp']){
                    $prev_bandwidth = Yii::app()->db->createCommand()
                            ->select('buffer_size')
                            ->from('bandwidth_log_temp')
                            ->where('id ='.$data['buff_log_idtemp'])
                            ->queryScalar();
                    if(@$prev_bandwidth > $bandwidth_used){
                        $response = array(
                           'buffer_log_id' => $data['buff_log_id'],
                           'u_id' => "",
                           'buff_log_idtemp' => $data['buff_log_idtemp']
                        );
                        echo (json_encode($response));
                        exit;
                    }
                }
                if($is_log){
                    $buffer_log_id = BufferLogs::model()->bufferLogSave($data);
                }
                if (@$buffer_log_id) {
					$response = array(
						'buffer_log_id' => $buffer_log_id[0],
						'u_id' => $buffer_log_id[1],
						'buff_log_idtemp' => $buffer_log_id[2]
					);
					echo (json_encode($response));
					exit;
                }elseif($is_log == 0){
                    $response = array(
						'buffer_log_id' => $data['buff_log_id'],
						'u_id' => "",
						'buff_log_idtemp' => $data['buff_log_idtemp']
					);
					echo (json_encode($response));
					exit;
				}
			}
		}
	}
}

