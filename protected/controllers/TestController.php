<?php

require('MadMimi.class.php');
require 's3bucket/aws-autoloader.php';
Yii::import('ext.EGeoIP');
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class TestController extends Controller {

	public function actionDateAndTimeTest() {
		echo "PHP Date: ".date('Y-m-d H:i:s');
		echo "<br/>PHP GmDate: ".gmdate('Y-m-d H:i:s');
		$sql = "SELECT NOW() AS mydate;";
		$data = Yii::app()->db->createCommand($sql)->queryRow();
		echo "<br/>MySql Date: ".$data['mydate'];
	}
	
    public function actionChangeGeoIP() {
        $this->layout = false;
        $dbcon = Yii::app()->db;
        $option = (isset($_REQUEST['option']))?$_REQUEST['option']:'';
        $offset = (isset($_REQUEST['offset']))?$_REQUEST['offset']:0;
        $limit = (isset($_REQUEST['limit']))?$_REQUEST['limit']:10000;
        $order = (isset($_REQUEST['order']))?$_REQUEST['order']:'DESC';
        
        $total_updated = 0;
        $city = $region = $country_name = $country = $continent_code = $latitude = $longitude = $currency_code = $currency_symbol = '';
        if($option == 'user'){
            $sql = "SELECT signup_location, signup_location_old, signup_ip, id FROM user WHERE signup_location_old = '' ORDER BY id ".$order." LIMIT ".$offset.", ".$limit.";";
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);
            foreach ($datas as $data){
                $ip_address = @$data['signup_ip'];
                $response = $this->getAddress(@$data['signup_location'], @$data['signup_ip']);
                if (count($response) > 0) {  
                    $user = User::model()->findByPk($data['id']);
                    $user->signup_location_old = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }
                }else{
                    echo "<br />Country is blank";
                }
            }
        }
        else if($option == 'user_blank'){
            $sql = 'SELECT signup_location, signup_ip, id FROM user WHERE signup_location = "" OR signup_location like \'%"country_name";s:0%\' ORDER BY id '.$order.' LIMIT '.$offset.', '.$limit.';';
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);
            foreach ($datas as $data){
                $ip_address = @$data['signup_ip'];
                $loc = Yii::app()->common->getLocationFromData($data['signup_location']);
                if(is_array($loc) && $loc['country'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }else if($data['signup_location'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }                
                if (count($response) > 0) {  
                    $user = User::model()->findByPk($data['id']);
                    $user->signup_location = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }
                }else{
                    echo "<br />Country is blank";
                }
            }
        }        
        else if($option == 'seller_application'){               
            $sql = "SELECT seller_location, seller_location_old, ip, id FROM seller_application WHERE seller_location_old = '' ORDER BY id ".$order." LIMIT ".$offset.", ".$limit.";";
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);  

            foreach ($datas as $data){
                $ip_address = @$data['ip'];
                $response = $this->getAddress(@$data['seller_location'], @$data['signup_ip']);
                if (count($response) > 0) { 
                    $user = SellerApplication::model()->findByPk($data['id']);
                    $user->seller_location_old = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }                                                            
                }else{
                    echo "<br />Country is blank";
                }
            }            
        }
        else if($option == 'seller_application_blank'){
            $sql = 'SELECT seller_location, ip, id FROM seller_application WHERE seller_location = "" OR seller_location like \'%"country_name";s:0%\' ORDER BY id '.$order.' LIMIT '.$offset.', '.$limit.';';
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);
            foreach ($datas as $data){
                $ip_address = @$data['ip'];
                $loc = Yii::app()->common->getLocationFromData($data['seller_location']);
                if(is_array($loc) && $loc['country'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }else if($data['seller_location'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }                
                if (count($response) > 0) { 
                    $user = SellerApplication::model()->findByPk($data['id']);
                    $user->seller_location = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }                                                            
                }else{
                    echo "<br />Country is blank";
                }
            }
        }         
        else if($option == 'sdk_users'){
            $sql = "SELECT signup_location, signup_location_old, signup_ip, id FROM sdk_users WHERE signup_location_old = ''  ORDER BY id ".$order." LIMIT ".$offset.", ".$limit.";";
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);
            
            //print_r($data);exit;
            foreach ($datas as $data) {               
                $ip_address = @$data['signup_ip'];
                $response = $this->getAddress(@$data['signup_location'], @$data['signup_ip']);
                if (count($response) > 0) {
                    $user = SdkUser::model()->findByPk($data['id']);
                    $user->signup_location_old = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }                    
                }else{
                    echo "<br />Country is blank";
                }
            }
        }  
        else if($option == 'sdk_users_blank'){
            $sql = 'SELECT signup_location, signup_ip, id FROM sdk_users WHERE signup_location = "" OR signup_location like \'%"country_name";s:0%\' ORDER BY id '.$order.' LIMIT '.$offset.', '.$limit.';';
            $datas = $dbcon->createCommand($sql)->queryAll();
            echo "Total Matched".count($datas);
            foreach ($datas as $data){
                $ip_address = @$data['signup_ip'];
                $loc = Yii::app()->common->getLocationFromData($data['signup_location']);
                if(is_array($loc) && $loc['country'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }else if($data['signup_location'] == ''){
                    $response = $this->getIP2LocationData($ip_address);
                }                
                if (count($response) > 0) {
                    $user = SdkUser::model()->findByPk($data['id']);
                    $user->signup_location = serialize($response);
                    if($user->save())
                        $total_updated++;
                    else{
                        echo 'error:'.$data['id'];
                        exit;
                    }                    
                }else{
                    echo "<br />Country is blank";
                }
            }
        }     
        echo "Total Updated: ".@$total_updated;
        exit;
    } 
    
    public function getAddress($signup_loc, $ip_address){        
        if($ip_address == '')
            $ip_address = CHttpRequest::getUserHostAddress();
        $city = $region = $country_name = $country = $continent_code = $latitude = $longitude = $currency_code = $currency_symbol = '';
        if($signup_loc != '' && $signup_loc != NULL && strpos($signup_loc, 'geoplugin') !== false && strlen($signup_loc) > 10){              
            $full_address = $this->mb_unserialize($signup_loc);            
            $address = @json_decode(json_encode($full_address), TRUE);            
            $signup_location = @$address['\0*\0_data']['\0CMap\0_d'];
            if(!count($signup_location) || empty($signup_location)){
                $signup_location = $address['*_data']['CMap_d'];
                if(!count($signup_location) || empty($signup_location)){
                    $signup_location = $address[' * _data'][' CMap _d'];
                }
            } 
            if(isset($signup_location['geoplugin_status']) && $signup_location['geoplugin_status'] === 200){
                $city = @$signup_location['geoplugin_city'];
                $region = @$signup_location['geoplugin_region']; 
                $country_name = @$signup_location['geoplugin_countryName']; 
                $country = @$signup_location['geoplugin_countryCode']; 
                $continent_code = @$signup_location['geoplugin_continentCode']; 
                $latitude = @$signup_location['geoplugin_latitude']; 
                $longitude = @$signup_location['geoplugin_longitude']; 
                $currency_code = @$signup_location['geoplugin_currencyCode'];
                $currency_symbol = @$signup_location['geoplugin_currencySymbol']; 
                if(strlen(trim($country)) <= 0){
                    $geoIp = new EGeoIP();
                    $geoIp = @unserialize($data['signup_location']);
                    $city = @$geoIp->getCity();
                    $region = @$geoIp->getRegion();
                    $country_name = @$geoIp->getCountryName();
                    $country = @$geoIp->getCountryCode();
                    $continent_code = @$geoIp->getContinentCode();
                    $latitude = @$geoIp->getLatitude();
                    $longitude = @$geoIp->getLongitude();
                    $currency_code = @$geoIp->getCurrencyCode();
                    $currency_symbol = @$geoIp->getCurrencySymbol();
                }
            }
        }
        if(strlen(trim($country)) < 0){
            $location = IP2Location::model()->getLocation($ip_address);
            if (is_array($location) && count($location) > 0) {
                $city = @$location['city_name'];
                $region = @$location['region_name'];
                $country_name = @$location['country_name'];
                $country = @$location['country_code'];
                $continent_code = @$location['continent_code'];
                $latitude = @$location['latitude'];
                $longitude = @$location['longitude'];
                $currency_code = '';
                $currency_symbol = '';
            }            
        }
        $response = array(
            'city' => trim($city), 
            'region' => trim($region), 
            'country' => trim($country),
            'country_name' => trim($country_name),
            'continent_code' => trim($continent_code),
            'ip_address' => trim($ip_address), 
            'currency_code' => trim($currency_code), 
            'currency_symbol' => trim($currency_symbol), 
            'latitude' => trim($latitude), 
            'longitude' => trim($longitude)
        );
        
        return $response;
    }    
    
    public function getIP2LocationData($ip_address){
        $location = array();
        $location = IP2Location::model()->getLocation($ip_address);
        if (is_array($location) && count($location) > 0) {
            $location['currency_code'] = @$location['currency_code'];
            $location['currency_symbol'] = @$location['currency_symbol'];
        }  
        return $location;
    }
    
    function actionIsssl() {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        $REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
        echo $REQUEST_PROTOCOL . "===";

        if (isset($_SERVER['HTTPS'])) {
            if ('on' == strtolower($_SERVER['HTTPS']))
                echo "True";
            if ('1' == $_SERVER['HTTPS'])
                echo "true";
        } elseif (isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] )) {
            echo "true=";
        }
        exit;
    }

    function actionIsSecure() {
        $issecure = isset($_SERVER['HTTPS']) && (strcasecmp($_SERVER['HTTPS'], 'on') === 0 || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        var_dump($issecure);
        exit;
    }

    public function actionupdateTicketMaster() {
        $sql = "SELECT email,studio_id from user WHERE (role_id = 1 and is_admin=0 AND is_sdk=1) OR email='admin@muvi.com'";
        echo $sql;
        $data = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($data as $key => $value) {
            $ticket_master_sql = "insert into `ticket_master`(`studio_id`, `studio_email`) values (" . $value['studio_id'] . ",'" . $value['email'] . "')";
            Yii::app()->db->createCommand($ticket_master_sql)->execute();
        }
        exit;
    }

    public function actionTest() {
        //phpinfo();exit();
        //print "Hi";exit;
        echo '<pre>';
        $sql = "SELECT ms.id,ms.studio_id,ms.full_movie FROM movie_streams ms,studios s WHERE is_converted = 1 AND wiki_data IS NULL AND ms.studio_id = s.id AND ms.full_movie IS NOT NULL";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        //print_r($data);//exit;
        $type = 'movie';
        foreach ($data as $value) {
            // print_r($value);
            $movie_id = $value['id'];
            echo $studio_id = $value['studio_id'];
            $full_movie = $value['full_movie'];
            // get video resolution and size
            $res = Yii::app()->common->getStorageSize($movie_id, $studio_id, $type);
            print_r($res);
            if (isset($res['res_size']) && !empty($res['res_size'])) {
                $res_size = json_encode($res['res_size']);
                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
                print_r($res_size);
                // get video duration
                $bucketName = $bucketInfo['bucket_name'];
                $s3url = $bucketInfo['s3url'];
                $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
                $signedBucketPath = $folderPath['signedFolderPath'];
                $videoUrl = 'http://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_id . '/' . $full_movie;
                $duration = $this->getVideoDuration($videoUrl);
                print_r($res_size);
                print $duration;
                print $movie_id;
                $qry = "UPDATE movie_streams ms SET ms.resolution_size = '" . $res_size . "', ms.video_duration = '" . $duration . "' WHERE ms.id=" . $movie_id;
                Yii::app()->db->createCommand($qry)->execute();
            }
        }
    }

    function actionParseData() {

        $con = Yii::app()->db;
        $data = $con->createCommand('SELECT actor,director FROM xmldata_new')->queryAll();
        foreach ($data AS $key => $val) {
            /* if(strstr($val['actor'],',')){
              $casts = explode(',', $val['actor']);
              foreach($casts AS $k =>$v){
              if(trim($v) && (trim($v) != 'N/A')){
              $arr[] = trim($v);
              }
              }
              }else{
              if(trim($v) && (trim($v) != 'N/A')){
              $arr[] = trim($val['actor']);
              }
              } */
            if (strstr($val['director'], ',')) {
                $director = explode(',', $val['director']);
                foreach ($director AS $k => $v) {
                    if (trim($v) && (trim($v) != 'N/A')) {
                        $arr[] = trim($v);
                    }
                }
            } else {
                if (trim($v) && (trim($v) != 'N/A')) {
                    $arr[] = trim($val['director']);
                }
            }
        }

        $castList = array_unique($arr);
        echo "<pre>Total uniq casts===" . count($castList);
        print_r($castList);
        exit;
        $celeb = new Celebrity();
        foreach ($castList AS $key => $val) {
            $celeb->setIsNewRecord(true);
            $celeb->setPrimaryKey(NULL);
            $celeb->name = $val;
            $celeb->studio_id = Yii::app()->user->studio_id;
            $celeb->created_by = Yii::app()->user->id;
            $celeb->created_date = gmdate('Y-m-d H:i:s');
            $celeb->save();
        }
        echo "Total Celebraty Inserted into DB is:- " . count($castList);
        exit;
    }

    /**
     * @method public addContent(type $paramName) Description
     * @author Gayadhar<support@muvi.com>
     */
    function actionAddContent() {
        if (@Yii::app()->user->studio_id == 2471) {
            $con = Yii::app()->db;
            $jsonData = $con->createCommand('SELECT category_value,content_subcategory_value,name,custom2 FROM sample_data')->queryAll();

            foreach ($jsonData AS $key => $val) {
                $val['content_types_id'] = 3;
                $val['parent_content_type_id'] = 1;
                $val['content_category_value'] = array($val['category_value']);
                $val['content_subcategory_value'] = array($val['content_subcategory_value']);
                $Films = new Film();
                $movie = $Films->addContent($val);

                $movie_id = $movie['id'];
                $studio_id = Yii::app()->user->studio_id;
                //Adding permalink to url routing 
                $urlRouts['permalink'] = $movie['permalink'];
                $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
                $urlRoutObj = new UrlRouting();
                $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $studio_id);

                $movie_id = $movie['id'];
                $arr['uniq_id'] = $movie['uniq_id'];

                //Insert Into Movie streams table

                $MovieStreams = new movieStreams();
                $MovieStreams->setIsNewRecord(true);
                $MovieStreams->setPrimaryKey(NULL);
                $MovieStreams->studio_id = Yii::app()->user->studio_id;
                $MovieStreams->movie_id = $movie_id;
                $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
                $MovieStreams->is_episode = 0;
                $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
                $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
                $MovieStreams->save();




                //if(HOST_IP != '127.0.0.1'){
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $movie_id;
                $solrArr['stream_id'] = $MovieStreams->id;
                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                $solrArr['is_episode'] = $MovieStreams->is_episode;
                $solrArr['name'] = $movie['name'];
                $solrArr['permalink'] = $movie['permalink'];
                $solrArr['studio_id'] = Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'content';
                $solrArr['content_permalink'] = $movie['permalink'];
                $solrObj = new SolrFunctions();
                $ret = $solrObj->addSolrData($solrArr);
            }
        }
        echo "Total Content added to the studio is:- " . count($jsonData);
        exit;
    }

    /* function  actionAddCast(){
      if(@Yii::app()->user->studio_id == 1122){
      $con = Yii::app()->db;
      $data = $con->createCommand('SELECT actor,director,movie_id FROM jsondata_new')->queryAll();
      foreach($data AS $key =>$val){
      if(trim($val['actor'])){
      $casts = explode(',', $val['actor']);
      foreach ($casts AS $k => $v){
      $celeb = Celebrity :: model()-> find('name =:name AND studio_id=:studio_id',array(':name'=>trim($v),':studio_id'=>Yii::app()->user->studio_id));
      if($celeb){
      $movie_cast = new MovieCast();
      $movie_cast -> movie_id = $val['movie_id'];
      $movie_cast -> celebrity_id = $celeb->id;
      $movie_cast -> cast_type = json_encode(['actor']);
      $movie_cast -> cast_name = trim($v);
      $movie_cast -> created_by = Yii::app()->user->id;
      $movie_cast -> created_date = date('Y-m-d H:i:s');
      $movie_cast -> save(false);
      }
      }
      }
      if(trim($val['director'])){
      $director = explode(',', $val['director']);
      foreach ($director AS $k => $v){
      $celeb = Celebrity :: model()-> find('name =:name AND studio_id=:studio_id',array(':name'=>trim($v),':studio_id'=>Yii::app()->user->studio_id));
      if($celeb){
      $movie_cast = new MovieCast();
      $movie_cast -> movie_id = $val['movie_id'];
      $movie_cast -> celebrity_id = $celeb->id;
      $movie_cast -> cast_type = json_encode(['director']);
      $movie_cast -> cast_name = trim($v);
      $movie_cast -> created_by = Yii::app()->user->id;
      $movie_cast -> created_date = date('Y-m-d H:i:s');
      $movie_cast -> save(false);
      }
      }
      }
      }
      }
      echo "Total cast crew mapped is:-". count($data);exit;
      } */




    /* public function actionImportCustomerCSV()
      {
      require_once 'parsecsv.lib.php';
      $csv = new parseCSV();
      $csv->auto('customers.csv');
      //$studio_id = 1;
      $studio_id = 1132;
      $cnt = 1;
      $headArr[0] = array('Email','Password');
      $headArr[1] = array('Email','Password');
      $sheetName = array("Customer's Login","Canceled Customers");
      $sheet = 2;
      $count = count($csv->data);

      $data[1] = array();
      if($count){
      foreach ($csv->data as $row):
      if($cnt>700){
      echo $row['Email'];
      echo '<br>';
      if($cnt<=$count){
      if($row['Status'] == 'active'){
      $key = '';
      $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
      for($i=0; $i < 8; $i++) {
      $key .= $pool[mt_rand(0, count($pool) - 1)];
      }
      $data[0][] = array(
      $row['Email'],
      $key
      );
      //insert user details to sdk_user table
      $temp_name = $row['Email'];
      $name = explode('@', $temp_name);
      $pass = '';
      $enc = new bCrypt();
      $pass = $enc->hash($key);
      $confirm_token = $enc->hash($row['Email']);
      if(strpos($confirm_token, "/") !== FALSE) {
      $confirm_token = str_replace('/', '', $confirm_token);
      }
      $sdkUser = new SdkUser();
      $sdkUser->email = $row['Email'];
      $sdkUser->encrypted_password = $pass;
      $sdkUser->display_name = $name[0];
      $sdkUser->studio_id = $studio_id;
      $sdkUser->confirmation_token = $confirm_token;
      $sdkUser->created_date = date('Y-m-d H:i:s');
      $sdkUser->status = 1;
      $sdkUser->save();
      $user_id = $sdkUser->id;
      //insert card details to sdk_card_info
      $sdkCard = new SdkCardInfos();
      $sdkCard->studio_id = $studio_id;
      $sdkCard->user_id = $user_id;
      $sdkCard->card_holder_name = $name[0];
      $sdkCard->exp_month = $row['Card Exp Month'];
      $sdkCard->exp_year = $row['Card Exp Year'];
      $sdkCard->card_last_fourdigit = '############'.$row['Card Last4'];
      $sdkCard->card_type = $row['Card Brand'];
      $sdkCard->token = $row['Card ID'];
      $sdkCard->status = 'succeeded';
      $sdkCard->created = date('Y-m-d H:i:s');
      $sdkCard->save();
      $card_id = $sdkCard->id;
      //insert user subscription details to user_subscription table
      $plan = SubscriptionPlans::model()->findByAttributes(array('name'=>$row['Plan'],'studio_id'=>$studio_id));

      /*$temp_date = explode('-', $row['Created (UTC)']);
      $temp_date[2] ='20'.$temp_date[2];
      $temp_date = implode('-', $temp_date);/

      $start_date = date('Y-m-d H:i:s',strtotime($row['Created (UTC)']));
      if($plan->recurrence == 'Month' || $plan->recurrence == 'month'){
      $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
      }elseif($plan->recurrence == 'Year' || $plan->recurrence == 'year'){
      $end_date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($start_date)));
      }
      $userSubscription = new UserSubscription();
      $userSubscription->studio_id = $studio_id;
      $userSubscription->user_id = $user_id;
      $userSubscription->studio_payment_gateway_id = 5;
      $userSubscription->plan_id = $plan->id;
      $userSubscription->amount = $plan->price;
      $userSubscription->is_success = 1;
      $userSubscription->card_id = $card_id;
      $userSubscription->profile_id = $row['id'];
      $userSubscription->start_date = $start_date;
      $userSubscription->end_date = $end_date;
      $userSubscription->created_date = date('Y-m-d H:i:s');
      $userSubscription->created_by = $user_id;
      $userSubscription->save();
      }else{
      $key = '';
      $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
      for($i=0; $i < 8; $i++) {
      $key .= $pool[mt_rand(0, count($pool) - 1)];
      }
      $data[1][] = array(
      $row['Email'],
      $key
      );
      //insert user details to sdk_user table
      $temp_name = $row['Email'];
      $name = explode('@', $temp_name);
      $pass = '';
      $enc = new bCrypt();
      $pass = $enc->hash($key);
      $confirm_token = $enc->hash($row['Email']);
      if(strpos($confirm_token, "/") !== FALSE) {
      $confirm_token = str_replace('/', '', $confirm_token);
      }
      $sdkUser = new SdkUser();
      $sdkUser->email = $row['Email'];
      $sdkUser->encrypted_password = $pass;
      $sdkUser->display_name = $name[0];
      $sdkUser->studio_id = $studio_id;
      $sdkUser->confirmation_token = $confirm_token;
      $sdkUser->created_date = date('Y-m-d H:i:s');
      $sdkUser->status = 1;
      $sdkUser->save();
      $user_id = $sdkUser->id;
      //insert card details to sdk_card_info
      $sdkCard = new SdkCardInfos();
      $sdkCard->studio_id = $studio_id;
      $sdkCard->user_id = $user_id;
      $sdkCard->card_holder_name = $name[0];
      $sdkCard->exp_month = $row['Card Exp Month'];
      $sdkCard->exp_year = $row['Card Exp Year'];
      $sdkCard->card_last_fourdigit = '############'.$row['Card Last4'];
      $sdkCard->card_type = $row['Card Brand'];
      $sdkCard->token = $row['Card ID'];
      $sdkCard->status = 'succeeded';
      $sdkCard->created = date('Y-m-d H:i:s');
      $sdkCard->save();
      }
      }
      }
      $cnt++;
      endforeach;
      }
      $filename = 'customer_login_details_'.date('Ymd_His');
      Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,'xls');
      }

      public function actionEncrypt(){
      $enc = new bCrypt();
      echo $enc->hash('shY18WCy');
      }
     */

    public function actionStripeCancelCustomer() {
        require_once 'stripe/init.php';
        //require_once 'parsecsv.lib.php';
        //\Stripe\Stripe::setApiKey('sk_test_J7QceZ04oG973QoKALgaP0mV');
        \Stripe\Stripe::setApiKey('sk_live_b0ciPN8ThjsqdLJiQ578DzmZ');
        /* $headArr[0] = array('Customer ID','Status');
          $sheetName = array("Customer");
          $sheet = 1;
          $csv = new parseCSV();
          $csv->auto('customers.csv'); */
        $customerIDs = array('cus_6zr6gqCr1lclsH', 'cus_6ntPCkdgEI2LXH', 'cus_6Q29wlM6LY2ufm', 'cus_6Fzy1HPAQCgbxd', 'cus_5taeF66NfKOfqh', 'cus_5ePDN2v37TdIbR', 'cus_5cxTMPdlAtcgfO', 'cus_5cnJHetjfgilq0', 'cus_5cflCrXk1OXClG');
        //foreach ($csv->data as $row):
        for ($i = 0; $i < count($customerIDs); $i++) {
            //if($row['Status'] == 'active'){
            //$customer_id = $row['id'];
            $customer_id = $customerIDs[$i];
            $customer = \Stripe\Customer::retrieve($customer_id);
            $subscriptions_id = isset($customer->subscriptions->data[0]->id) ? $customer->subscriptions->data[0]->id : '';
            if (trim($subscriptions_id)) {
                $customer->subscriptions->retrieve($subscriptions_id)->cancel();
                print 'Deleted: ' . $customerIDs[$i];
                // $data[0][] = array($customer_id,'Canceled');
            } else {
                print 'Not Deleted: ' . $customerIDs[$i];
                // $data[0][] = array($customer_id,'Not Canceled');
            }
            print '<br>';
            //}
        }
        //endforeach;
        /* $filename = 'customer_cancel_status_'.date('Ymd_His');
          Yii::app()->general->getCSV($headArr,$sheet,$sheetName,$data,$filename,'xls'); */
    }

    /* public function actionUpdateDate()
      {
      require_once 'parsecsv.lib.php';
      $csv = new parseCSV();
      $csv->auto('customers.csv');
      $studio_id = 1132;
      $plan_id = 112;
      $cnt = 1;
      $count = count($csv->data);
      echo '<pre>';
      //echo'<pre>';print_r($csv->data);exit;
      if($count){
      foreach ($csv->data as $row):
      if($cnt<=$count){
      if(isset($row['Email']) && $row['Email'] != ''){
      $user = SdkUser::model()->findByAttributes(array('email'=>$row['Email'],'studio_id'=>$studio_id));
      //print_r($user);
      $user_id = $user->id;
      $userSubscription = UserSubscription::model()->findByAttributes(array('user_id'=>$user_id,'plan_id'=>$plan_id,'studio_id'=>$studio_id));
      //print_r($userSubscription);
      $today = date('d');
      $date_str = date('d',strtotime($row['Created (UTC)']));
      $time_str = date('H:i:s',strtotime($row['Created (UTC)']));

      if($today > $date_str){
      $start_date = '2016-02-'.$date_str.' '.$time_str;
      $start_date = date('Y-m-d H:i:s', strtotime($start_date));
      $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
      }else{
      $start_date = '2016-01-'.$date_str.' '.$time_str;
      $start_date = date('Y-m-d H:i:s', strtotime($start_date));
      $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
      }
      //echo $start_date.'--'.$end_date;exit;
      $created_date = date('Y-m-d H:i:s', strtotime($row['Created (UTC)']));
      if(isset($userSubscription->id) && intval($userSubscription->id)){
      $dbcon = Yii::app()->db;
      $sql = "UPDATE user_subscriptions SET created_date='".$created_date."',start_date='".$start_date."',end_date='".$end_date."' WHERE id=".$userSubscription->id." AND studio_id=1132;";
      echo $sql;
      echo '<br>';
      //$res = $dbcon->createCommand($sql)->execute();
      }
      }
      }
      $cnt++;
      endforeach;
      }
      } */

    /**
     * 
     */
    function actionAddShows() {
        if (@Yii::app()->user->studio_id == 1122) {
            $con = Yii::app()->db;
            $data = $con->createCommand('SELECT * FROM xmldata_25 ORDER By name,series_number,episode_number')->queryAll();
            //echo "<pre>";print_r($data);exit;
            $epData = new movieStreams();
            foreach ($data AS $key => $val) {
                $epData->episode_title = $val['episode_title'];
                $epData->episode_number = $val['episode_number'];
                $epData->series_number = $val['series_number'];
                $epData->studio_id = Yii::app()->user->studio_id;
                $epData->episode_story = $val['story'];
                $epData->episode_date = $val['date'];
                $epData->movie_id = $val['movie_id'];
                $epData->created_by = Yii::app()->user->id;
                $epData->created_date = gmdate('Y-m-d');
                $epData->last_updated_date = gmdate('Y-m-d H:i:s');
                $epData->is_episode = 1;
                $epData->isNewRecord = true;
                $epData->primaryKey = NULL;
                $return = $epData->save();
            }
            echo "Total Episodes added to the studio is:- " . count($data);
            exit;
        }
    }

    public function actionImportCustomerCSVPaypal() {
        require_once 'parsecsv.lib.php';
        $csv = new parseCSV();
        $csv->auto('UNIFYme.tv_UserList.csv');
        $studio_id = 1122;
        $cnt = 1;
        $headArr[0] = array('Email', 'Password');
        $sheetName = array("Customer's Login");
        $sheet = 1;
        $count = count($csv->data);
        echo '<pre>';
        //print_r($csv->data);exit;
        if ($count) {
            foreach ($csv->data as $row):
                if ($cnt > 0) {
                    if ($cnt <= $count) {
                        $key = '';
                        $pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
                        for ($i = 0; $i < 8; $i++) {
                            $key .= $pool[mt_rand(0, count($pool) - 1)];
                        }
                        $data[0][] = array(
                            $row['Email'],
                            $key
                        );
                        //insert user details to sdk_user table
                        $pass = '';
                        $enc = new bCrypt();
                        $pass = $enc->hash($key);
                        $confirm_token = $enc->hash($row['Email']);
                        if (strpos($confirm_token, "/") !== FALSE) {
                            $confirm_token = str_replace('/', '', $confirm_token);
                        }
                        $sdkUser = new SdkUser();
                        $sdkUser->email = $row['Email'];
                        $sdkUser->encrypted_password = $pass;
                        $sdkUser->display_name = $row['First Name'];
                        $sdkUser->studio_id = $studio_id;
                        $sdkUser->confirmation_token = $confirm_token;
                        $sdkUser->created_date = date('Y-m-d H:i:s');
                        $sdkUser->status = 1;
                        $sdkUser->save();
                        $user_id = $sdkUser->id;

                        $temp_date = explode('-', $row['Start Date']);
                        $temp_date[2] = '20' . $temp_date[2];
                        $temp_date = implode('-', $temp_date);
                        $free_days = 0;
                        $plan_id = 0;
                        if ($row['Plan'] == 'Monthly') {
                            $free_days = 1;
                            $plan_id = 138;
                        } elseif ($row['Plan'] == 'Quaterly') {
                            $free_days = 3;
                            $plan_id = 138;
                        } elseif ($row['Plan'] == 'Half-Yearly') {
                            $free_days = 6;
                            $plan_id = 138;
                        } elseif ($row['Plan'] == 'Yearly') {
                            $free_days = 12;
                            $plan_id = 149;
                        }
                        $start_date = date('Y-m-d H:i:s', strtotime('+' . $free_days . ' month', strtotime($temp_date)));
                        $end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($start_date)));
                        $userSubscription = new UserSubscription();
                        $userSubscription->studio_id = $studio_id;
                        $userSubscription->user_id = $user_id;
                        $userSubscription->studio_payment_gateway_id = 5;
                        $userSubscription->plan_id = $plan_id;
                        $userSubscription->amount = preg_replace("/[^0-9]/", "", $row['Amount']);
                        $userSubscription->is_success = 1;
                        $userSubscription->start_date = $start_date;
                        $userSubscription->end_date = $end_date;
                        $userSubscription->created_date = date('Y-m-d H:i:s');
                        $userSubscription->created_by = $user_id;
                        $userSubscription->save();
                    }
                }
                $cnt++;
            endforeach;
        }
        $filename = 'customer_login_details_' . date('Ymd_His');
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, 'xls');
    }

    public function actionGetVideoReport() {
        $studio_id = $_REQUEST['studio'];
        if (isset($studio_id) && intval($studio_id)) {
            $sql = "SELECT u.email,bl.studio_id,bl.start_time,bl.end_time,bl.played_time,bl.buffer_size,bl.movie_id,bl.video_id,bl.resolution,bl.created_date FROM bandwidth_log bl,sdk_users u WHERE bl.user_id = u.id AND bl.studio_id = " . $studio_id . " LIMIT 20";
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            $headArr[0] = array('Email', 'Studio id', 'start time', 'end time', 'played time', 'buffer size', 'movie id', 'video id', 'resolution', 'created date');
            $sheetName = array("Video Report");
            $sheet = 1;
            $report = array();
            echo '<pre>';

            foreach ($data as $value) {
                print_r($value);
                $report[0][] = array(
                    $value['email'],
                    $value['studio_id'],
                    $value['start_time'],
                    $value['end_time'],
                    $value['played_time'],
                    $value['buffer_size'],
                    $value['movie_id'],
                    $value['video_id'],
                    $value['resolution'],
                    $value['created_date']
                );
            }
            $filename = 'video_report_' . date('Ymd_His');
            Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $report, $filename, 'xls');
        }
    }

    public function actionGetSize() {
        require 's3bucket/aws-autoloader.php';
        $s3 = Yii::app()->common->connectToAwsS3(1132);
        $folderPath = Yii::app()->common->getFolderPath("", 1132);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $prefix = $signedBucketPath . 'uploads/movie_stream/full_movie/6015/';
        $ms = movieStreams::model()->findByPk(6015);
        $video_resolution = $ms->video_resolution;
        $full_movie_name = $ms->full_movie;
        $movie_name = explode('.', $full_movie_name);
        $original_name = $movie_name[0] . '_original';
        $bucketInfo = Yii::app()->common->getBucketInfo('', 1132);
        $response = $s3->getListObjectsIterator(array(
            'Bucket' => $bucketInfo['bucket_name'],
            'Prefix' => $prefix
        ));
        echo '<pre>';
        foreach ($response as $object) {
            print_r($object);
        }
    }

    public function actionDeleteDuplicateLog() {
        echo '<pre>';
        $limit = $_REQUEST['limit'];
        $sql = "SELECT * FROM bandwidth_log WHERE studio_id = 1132 ORDER BY id DESC"; // LIMIT ".$limit.",500";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        //print_r($data);exit;
        $user_id = $data[0]['user_id'];
        $movie_id = $data[0]['movie_id'];
        $video_id = $data[0]['video_id'];
        $resolution = $data[0]['resolution'];
        unset($data[0]);
        //print_r($data);exit;
        foreach ($data as $key => $value) {
            if ($user_id == $value['user_id'] && $movie_id == $value['movie_id'] && $video_id == $value['video_id'] && $resolution == $value['resolution']) {
                echo 'Delete:-' . $value['id'] . '-' . $value['user_id'] . '-' . $value['movie_id'] . '-' . $value['video_id'] . '-' . $value['resolution'];
                echo '<br>';
                $d_sql = "DELETE FROM bandwidth_log WHERE id = " . $value['id'];
                Yii::app()->db->createCommand($d_sql)->execute();
            }

            $user_id = $value['user_id'];
            $movie_id = $value['movie_id'];
            $video_id = $value['video_id'];
            $resolution = $value['resolution'];
        }
        $dl_sql = "DELETE FROM bandwidth_log WHERE buffer_size <= 0";
        Yii::app()->db->createCommand($dl_sql)->execute();
    }

    public function actionValidatePackageCustomCodeTest() {
        $data = array();
        $data['isExists'] = 0;
        if ($_REQUEST['id']) {
            $custom_code = 'test2016';
        } else {
            $custom_code = 'TEST2016';
        }
        (array) $package_codes = PackageCode::model()->find(
                array(
                    'select' => '*',
                    'condition' => 'package_code=:package_code AND BINARY custom_code=:custom_code',
                    'params' => array(':package_code' => trim('muvi_studio_enterprise'), 'custom_code' => trim($custom_code)),
                )
        );

        if (isset($package_codes) && !empty($package_codes)) {
            $data = $package_codes->attributes;
            $data['isExists'] = 1;
        }

        print json_encode($data);
        exit;
    }

    public function actionUpdateIP() {
        //$sql = "SELECT DISTINCT(lh.user_id),lh.ip FROM bandwidth_log bl,login_history lh WHERE bl.user_id = lh.user_id AND DATE_FORMAT(lh.login_at,'%Y-%m-%d') = DATE_FORMAT(bl.created_date,'%Y-%m-%d')";
        $sql = "SELECT DISTINCT(user_id),ip from login_history";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $value) {
            $ip = $value['ip'];
            $user_id = $value['user_id'];
            $u_sql = "UPDATE bandwidth_log SET ip='" . $ip . "' WHERE user_id=" . $user_id . " AND (ip='' OR ip IS NULL)";
            Yii::app()->db->createCommand($u_sql)->execute();
        }
    }

    public function actionUpdateLocation() {
        /* $limit = $_REQUEST['limit'];
          $offset = $_REQUEST['offset'];
          $sql = "SELECT * FROM bandwidth_log WHERE (ip<>'' OR ip IS NOT NULL) AND (location='' OR location IS NULL) LIMIT ".$offset.",".$limit;
          $data = Yii::app()->db->createCommand($sql)->queryAll();
          echo '<pre>';
          //print_r($data);exit;
          foreach ($data as $value){
          $ip = $value['ip'];
          $id = $value['id'];
          $log = new BufferLogs();
          $log_data = $log->findByPk($id);
          $geo_data = new EGeoIP();
          $geo_data->locate($ip);
          $geo_data = serialize($geo_data);
          $log_data->location = $geo_data;
          $log_data->save();
          } */
        $status = 0;
        $limit = $_REQUEST['limit'];
        $offset = $_REQUEST['offset'];
        $status = $_REQUEST['status'];
        $sql = "SELECT id,ip FROM bandwidth_log WHERE LENGTH(location) < 500 AND (ip <> '' OR ip IS NOT NULL)LIMIT " . $offset . "," . $limit;
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        if ($status) {
            echo '<pre>';
            print_r($data);
            exit;
        }

        foreach ($data as $value) {
            $ip = $value['ip'];
            $id = $value['id'];
            $log = new BufferLogs();
            $log_data = $log->findByPk($id);
            $geo_data = new EGeoIP();
            $geo_data->locate($ip);
            $geo_data = serialize($geo_data);
            $log_data->location = $geo_data;
            $log_data->save();
        }
    }

    public function actionGetStorageTest() {
        $studio_id = $_GET['studio'];
        $vl_sql = "SELECT video_properties FROM video_management WHERE studio_id = " . $studio_id;
        $vl_data = Yii::app()->db->createCommand($vl_sql)->queryAll();
        if (isset($vl_data) && !empty($vl_data)) {
            foreach ($vl_data as $value) {
                $video_properties = json_decode($value['video_properties']);
                if (isset($video_properties) && !empty($video_properties)) {
                    $video_size_string = $video_properties->FileSize;
                    $video_size = explode(' ', $video_size_string);
                    echo $video_size[0] . "<br/>";
                }
            }
        }
    }

    public function actionAuthorizeUpdateCustomer() {
        /* $payment_gateway_controller = 'Api'.$this->PAYMENT_GATEWAY.'Controller';
          echo $payment_gateway_controller.'<br/>';
          Yii::import('application.controllers.wrapper.'.$payment_gateway_controller);
          $payment_gateway = new $payment_gateway_controller();
          $res = $payment_gateway::updatePaymentProfile(); */

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {//test case
            $cond = " AND u.id=" . $_REQUEST['user'];

            $sql = "SELECT u.*, u.id AS user_id, us.*, us.id AS user_subscription_id, sci.*, sci.id AS card_info_id, sp.* 
            FROM user_subscriptions us LEFT JOIN sdk_users u ON (u.id=us.user_id AND u.studio_id=us.studio_id AND us.status=1 ),
            sdk_card_infos sci, subscription_plans sp WHERE u.status=1 AND u.is_deleted='0' " . $cond . " AND sci.user_id=us.user_id AND 
            sci.studio_id=us.studio_id AND sci.is_cancelled=0 AND sp.status=1 AND sp.studio_id=us.studio_id AND us.plan_id=sp.id GROUP BY u.id";
        }
        $dbcon = Yii::app()->db;
        $user = $dbcon->createCommand($sql)->queryRow();

        $user['currency_code'] = 'USD';
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $trans_data = $payment_gateway::processTransactions($user);
        print "<pre>";
        print_r($trans_data);
        exit;
    }

    public function actionGetStudioS3Storage() {
        print date('Y-m-d H:i:s:u');
        print "<br />";

        if (isset($_REQUEST['studio']) && $_REQUEST['studio']) {
            $movie_size = 0;
            $trailer_size = 0;
            $raw_size = 0;
            $raw_image = 0;
            $studio_id = $_REQUEST['studio'];
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
            $signedBucketPath = $folderPath['signedFolderPath'];
            $unsignedBucketPath = $folderPath['unsignedFolderPathForVideo'];
            $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);

            $prefix_encoded_movie = $signedBucketPath . 'uploads/movie_stream/full_movie/';
            $prefix_trailer = $signedBucketPath . 'uploads/trailers/';
            $prefix_raw_video = $unsignedBucketPath . 'videogallery/';
            $prefix_raw_image = $unsignedBucketPath . 'videogallery/videogallery-image/';

            $response_movie = $s3->getListObjectsIterator(array(
                'Bucket' => $bucketInfo['bucket_name'],
                'Prefix' => $prefix_encoded_movie
            ));
            $response_trailer = $s3->getListObjectsIterator(array(
                'Bucket' => $bucketInfo['bucket_name'],
                'Prefix' => $prefix_trailer
            ));
            $response_raw_video = $s3->getListObjectsIterator(array(
                'Bucket' => $bucketInfo['bucket_name'],
                'Prefix' => $prefix_raw_video
            ));

            $response_raw_image = $s3->getListObjectsIterator(array(
                'Bucket' => $bucketInfo['bucket_name'],
                'Prefix' => $prefix_raw_image
            ));

            foreach ($response_movie as $object_movie) {
                $movie_size += $object_movie['Size'];
            }
            foreach ($response_trailer as $object_trailer) {
                $trailer_size += $object_trailer['Size'];
            }
            foreach ($response_raw_video as $object_raw_video) {
                $raw_size += $object_raw_video['Size'];
            }
            foreach ($response_raw_image as $object_raw_image) {
                $raw_image += $object_raw_image['Size'];
            }
            $size = $movie_size + $trailer_size + $raw_size - $raw_image;
            $tsize = $movie_size + $trailer_size + $raw_size;
            $total_size = $size / 1024 / 1024 / 1024;
            //return $total_size;
            echo 'Raw Size = ' . ($tsize / 1024 / 1024 / 1024);
            echo '<br>';
            echo 'Final Size = ' . ($size / 1024 / 1024 / 1024);
            print "<br />";
            print date('Y-m-d H:i:s:u');
            exit;
        } else {
            return 0;
        }
    }

    public function actionRemoveGeoblock() {
        $sql = "SELECT g.*,s.studio_id,s.is_episode FROM geoblockcontent g,movie_streams s  WHERE g.movie_id=s.movie_id AND g.movie_stream_id = s.id AND s.studio_id=413";
        $dbcon = Yii::app()->db;
        $user = $dbcon->createCommand($sql)->queryALL();
        /* foreach($user as $k=>$v){
          if($v['is_episode']==1){
          $dbcon->createCommand("DELETE FROM geoblockcontent WHERE id = ".$v['id'])->execute();
          }
          $geo[$v['studio_id']][$v['movie_id']][] = $v;
          } */
        echo "<pre>";
        print_r($user);
        echo "</pre>";
    }

    public function actionBandwidthUses() {
        $studio_id = isset($_REQUEST['studio']) ? $_REQUEST['studio'] : '1132';
        $date = '2016-06-07';
        for ($i = 0; $i < 10; $i++) {
            $sql = "SELECT SUM(total_buffered_size) AS buffered_size FROM bandwidth_buffer_log bbl,films f,movie_streams ms WHERE bbl.studio_id = '" . $studio_id . "' AND (DATE_FORMAT(bbl.created_date,'%Y-%m-%d') BETWEEN '" . $date . "' AND '" . $date . "') AND (bbl.movie_id = f.id AND (f.id<>0 OR f.id !='')) AND (bbl.video_id=ms.id OR (bbl.video_id=0 OR ISNULL(bbl.video_id))) GROUP BY bbl.studio_id";
            $bandwidth = Yii::app()->db->createCommand($sql)->queryRow();
            $Tbandwidth = ($bandwidth['buffered_size']) / 1024 / 1024;
            echo $date . ':  ' . $Tbandwidth . ' GB <br />';
            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }
    }

    /* @author manas@muvi.com */

    public function actionCreateSonyAccount() {
        /* $emails = array(
          'christian.huber@sonydadc.com',
          'ferda.tastan@sonydadc.com',
          'rudolf.ablinger@sonydadc.com',
          'markus.colosio@sonydadc.com',
          'harald.absmann@sonydadc.com',
          'alex.drummond@sonydadc.com',
          'laura.krug@sonydadc.com',
          'Monali.Waghulde@sonydadc.com',
          'russell.dsouza@sonydadc.com',
          'Karin.ploner@sonydadc.com',
          'andreas.lenzenweger-kraml@sonydadc.com',
          'Jitesh.sawant@sonydadc.com',
          'carlo.bellucci@sonydadc.com'
         * 'gerry.kelly@sonydadc.com'
         * 'jonathan.cheesmur@sonydadc.com'
         * 'ray.wheeler@sonydadc.com'
          ); */
        $emails = array(
            'philipp.maccani@sonydadc.com'
        );
        foreach ($emails as $key => $value) {
            $portaluser = PortalUser::model()->exists('email=:email', array(':email' => $value));
            if (!$portaluser) {
                $portal = new PortalUser();
                $password = $this->getRandomPassword();
                $enc = new bCrypt();
                $encrypted_pass = $enc->hash($password);
                $name1 = explode('@', $value);
                $name2 = explode('.', $name1[0]);
                $name = ucfirst($name2[0]) . " " . ucfirst($name2[1]);
                $portal->name = $name;
                $portal->encrypted_password = $encrypted_pass;
                $portal->permission_id = "34,33";
                $portal->email = $value;
                $portal->parent_id = 7;
                $portal->is_customer = 0;
                $portal->seller_app_id = 0;
                $portal->status = 1;
                $portal->save();
                echo "Email: " . $value . "==Password:" . $password . "==UserID:" . $portal->id . "<br>";
            }
        }
        exit();
    }

    public function getRandomPassword() {
        $acceptablePasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@#0123456789";
        $randomPassword = "";

        for ($i = 0; $i < 8; $i++) {
            $randomPassword .= substr($acceptablePasswordChars, rand(0, strlen($acceptablePasswordChars) - 1), 1);
        }
        return $randomPassword;
    }

    public function actionAddtoHubspot() {
        $_POST['name'] = "Manas Sahoo";
        $_POST['email'] = "manas@muvi.com";
        $nm = explode(" ", $_POST['name'], 2);
        $_POST['phone'] = "98989797";
        $hubspot_owner_id = '';
        $hs_lead_status = 'COLD_LEAD';
        $lead_type = 'Partner / Reseller / Affiliate / Referral';
        $res = Hubspot::AddToHubspot($_POST['email'], $nm[0], $nm[1], $_POST['phone'], $hubspot_owner_id, $hs_lead_status, $lead_type);
        var_dump($res);
    }

    public function actiongetVideoProperties() {

        $sql = "select id,video_properties from video_management group by id";
        $res = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($res as $key => $val) {
            $properties = json_decode($val['video_properties'], true);
            $duration = $properties['duration'];
            $fsize = explode(" ", $properties['FileSize']);
            $filesize = $fsize[0];

            $update_query = "update video_management set duration='" . $duration . "', file_size='" . $filesize . "' where id=" . $val['id'];
            Yii::app()->db->createCommand($update_query)->execute();
        }
        exit;
    }

    public function actionTestCurl() {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '1024M');
        $url = "ftp://52.5.45.62/Tune_Maari_Entriyaan.mp4";
        $username = "gaya";
        $password = "REU9FPzmIX";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_USERAGENT, "Proventum Proxy/1.0 (Linux)");
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //times out after 10s 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);
        print curl_error($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        print $code;
        exit;
    }

    public function actionEnablePg() {
        $configvalue = $_GET['pg'];
        $studio_id = $this->studio->id;
        $getStudioConfig = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'pg_enable'));
        if ($getStudioConfig) {
            $getStudioConfig->config_value = $configvalue;
            $getStudioConfig->save();
        } else {
            $studioConfig = new StudioConfig();
            $studioConfig->studio_id = $studio_id;
            $studioConfig->config_key = 'pg_enable';
            $studioConfig->config_value = $configvalue;
            $studioConfig->save();
        }
    }

    public function actiondata() {
        /* ini_set('max_execution_time', 3000);
          $con = Yii::app()->db;
          $studioid = "SELECT DISTINCT studio_id FROM films order by studio_id";
          $studio = $con->createCommand($studioid)->queryAll();

          foreach($studio as $sval)
          {
          $genre = "SELECT genre FROM films WHERE genre LIKE '%[%' AND studio_id = ".$sval['studio_id'];
          $genrename = $con->createCommand($genre)->queryAll();
          $genarr=array();
          $gvarr=array();
          foreach($genrename as $gval)
          {
          $gv = json_decode($gval['genre']);

          foreach($gv as $gvval)
          {
          $rawval = strtolower(trim($gvval));
          $gvarr[] = $rawval;
          }

          $genarr = array_unique(array_merge($genarr,$gvarr));
          }


          $pcModel = New MovieTag;
          foreach($genarr as $genval)
          {
          if ($genval != '') {
          $pcModel->name = ucwords($genval);
          $pcModel->taggable_type = 1;
          $pcModel->studio_id = $sval['studio_id'];
          $pcModel->isNewRecord = TRUE;
          $pcModel->primaryKey = NULL;
          if($pcModel->save())
          {
          echo $sval['studio_id']."-".$genval."-Success<br />";
          }
          }
          }
          } */
    }

    public function actionManualPortaluser() {
        $portaluserid = '72';
        $token = 'a8d6a5bb7732344ea5239d42ce832e53';
        $activelink = 'https://partners.muvi.com/partner/resetpassword?auth=' . $token . '&uid=' . base64_encode($portaluserid);
        echo '<a herf="' . $activelink . '">Click here</a>';
    }

    public function actionPermaLink() {
        $products = PGProduct::model()->findAll(array('condition' => 'permalink=:permalink AND studio_id=:studio_id', 'params' => array(':permalink' => '', ':studio_id' => $this->studio->id)));
        foreach ($products AS $val) {
            $permalink = Yii::app()->general->generatePermalink(stripslashes($val['name']));
            Yii::app()->db->createCommand("UPDATE pg_product SET permalink='$permalink' WHERE id=" . $val['id'])->execute();
            $urlRouts['permalink'] = $permalink;
            $urlRouts['mapped_url'] = '/shop/ProductDetails/id/' . $val['uniqid'];
            $urlRoutObj = new UrlRouting();
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, Yii::app()->user->studio_id);
        }
    }

    public function actionHbl() {
        echo '<pre>';
        echo '<h2>HBL Payment Gateway Testing.</h2>';
        print_r($_REQUEST);
        echo '</pre>';
    }

    public function actionMytubetor() {
        Yii::app()->theme = 'bootstrap';
        $this->layout = 'blank';
        $this->render('mytubetor');
    }

    public function actionRemoveDuplicatePermalink() {
        //fetch all duplicate permalink respect to studio_id
        //2702;//2643 //2665 //2612
        $studio_id = $_GET['id'];
        $fetch_duplicate_link = "SELECT count(id) as cnt,`permalink`, `studio_id` FROM `films` where studio_id=$studio_id group by `permalink`,studio_id having cnt>1 order by cnt desc";
        //print $fetch_duplicate_link;
        $all_records = Yii::app()->db->createCommand($fetch_duplicate_link)->queryAll();
        //fetch all duplicate permalink respect to studio_id
        foreach ($all_records as $key => $val) {
            $fetch_ind_link = "SELECT id,name,permalink,studio_id FROM `films` WHERE `studio_id` =" . $val['studio_id'] . " and `permalink`='" . $val['permalink'] . "' order by id desc";
            //print $fetch_ind_link;
            $all_ind_records = Yii::app()->db->createCommand($fetch_ind_link)->queryAll();
            $i = 0;
            $qry_delete_url = "delete from url_routing where permalink='" . $val['permalink'] . "' and studio_id=" . $val['studio_id'];
            Yii::app()->db->createCommand($qry_delete_url)->execute();
            foreach ($all_ind_records as $k => $value) {
                echo $value['name'];
                $new_permalink = self::generatePermalink($value['name'], 0, $studio_id);
                echo "<br>" . $new_permalink;
                $qry_update_films = "update films set permalink='" . $new_permalink . "' where id=" . $value['id'];
                print "<br>" . $qry_update_films . "<br>";
                Yii::app()->db->createCommand($qry_update_films)->execute();
                $mapped_url = '/movie/show/content_id/' . $value['id'];
                $qry_insert_url = "insert into url_routing(`studio_id`, `permalink`, `mapped_url`, `is_reserved`, `created_date`, `updated_date`) values(" . $val['studio_id'] . ",'" . $new_permalink . "','" . $mapped_url . "',0,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "')";
                print $qry_insert_url . "<br>";
                Yii::app()->db->createCommand($qry_insert_url)->execute();
            }
        }
    }

    public function format_uri($string, $separator = '-') {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and', "'" => '');
        $string = mb_strtolower(trim($string), 'UTF-8');
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }

    public function generatePermalink($title, $id = 0, $sid) {
        //$studio_id = Yii::app()->common->getStudiosId();
        $studio_id = $sid;
        $delimiter = '-';
        $resevredPermalink = array('about', 'insertapplication', 'application', 'partners', 'byod', 'contactus', 'video-streaming-platform', 'live-streaming', 'admin', 'site', 'user', 'adminceleb', 'ads', 'affiliate', 'apidocs', 'awss3', 'blogs', 'category', 'contact', 'content', 'conversion', 'cron', 'dam', 'dashboard', 'embed', 'language', 'livestream', 'login', 'management', 'media', 'mergedata', 'monetization', 'movie', 'mrss', 'muvi', 'notifypaypal', 'page', 'partner', 'partners', 'payment', 'permission', 'player', 'pricing', 'report', 'rest', 'sdk', 'search', 'signup', 'site', 'star', 'template', 'test', 'ticket', 'userfeature', 'video', 'wp');

        $permalink = strtolower(trim($title, $delimiter));
        $permalink = preg_replace('@[\s!:;_\?=\\\+\*/%&#]+@', '-', $permalink);
        $permalink = trim($this->format_uri($permalink));
        $permalink = trim($permalink, $delimiter);
        if (!$permalink) {
            $permalink = 'vod-';
        }
        if (in_array($permalink, $resevredPermalink)) {
            $permalink .="-1";
        }

        $routing = New UrlRouting;
        $res = $routing->findAllByAttributes(array('studio_id' => $studio_id, 'permalink' => $permalink));
        if (count($res) > 0) {

            $format = addcslashes($permalink, '%_');
            $q = new CDbCriteria(array(
                'condition' => "permalink LIKE :match AND studio_id = :studio_id",
                'params' => array(':match' => "$format%", ':studio_id' => $studio_id),
                'order' => 'permalink DESC'
            ));

            $plinks = UrlRouting::model()->findAll($q);
            foreach ($plinks as $key => $value) {
                $pernamlinkarray[] = $value['permalink'];
            }
            $total_matched = count($plinks);
            if ($plinks[0]->id != $id) {
                $perm = $plinks[0]['permalink'];
                $format_perm = preg_replace("/\d+$/", "", $perm);
                $parts = explode($format_perm, $perm);
                $end = 1;
                if (count($parts) > 0) {
                    $end = (int) $parts[1];
                    $end++;
                }
                $permalink.= $end;
            }
            $permalink = self::recursiveSearch($pernamlinkarray, $permalink);
        }
        return $permalink;
    }

    function recursiveSearch($pernamlinkarray, $x) {
        static $permanlinkindex = 1;
        if (in_array($x, $pernamlinkarray)) {
            $x = $x . $permanlinkindex++;
            return self::recursiveSearch($pernamlinkarray, $x);
        } else {
            return $x;
        }
    }

    public function actionPalFlesx() {
        $fetch_ind_link = "SELECT id,name,permalink,studio_id FROM `films` WHERE `studio_id` =1367 and `permalink` like 'vod-10%' order by id desc";
        print $fetch_ind_link;
        $all_ind_records = Yii::app()->db->createCommand($fetch_ind_link)->queryAll();
        $i = 0;
        $qry_delete_url = "delete from url_routing where permalink like 'vod-10%' and studio_id=1367";
        Yii::app()->db->createCommand($qry_delete_url)->execute();
        foreach ($all_ind_records as $k => $value) {
            echo $value['name'];
            $new_permalink = self::generatePermalink($value['name'], 0, 1367);
            echo "<br>" . $new_permalink;
            $qry_update_films = "update films set permalink='" . $new_permalink . "' where id=" . $value['id'];
            print "<br>" . $qry_update_films . "<br>";
            Yii::app()->db->createCommand($qry_update_films)->execute();
            $mapped_url = '/movie/show/content_id/' . $value['id'];
            $qry_insert_url = "insert into url_routing(`studio_id`, `permalink`, `mapped_url`, `is_reserved`, `created_date`, `updated_date`) values(1367,'" . $new_permalink . "','" . $mapped_url . "',0,'" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "')";
            print $qry_insert_url . "<br>";
            Yii::app()->db->createCommand($qry_insert_url)->execute();
        }
    }

    public function actionUpdateObj() {
        if (isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] > 0) {
            $studio_id = $_REQUEST['studio_id'];
            $s3 = Yii::app()->common->connectToAwsS3($studio_id);
            $bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
            $bucket = $bucketInfo['bucket_name'];
            $s3dir = $studio_id . "/public/";
            try {
                $result = $s3->listObjects(array('Bucket' => $bucket, "Prefix" => $s3dir));
                foreach ($result['Contents'] as $object) {
                    $key = $object['Key'];
                    try {
                        $o = $s3->copyObject(array(
                            'Bucket' => $bucket,
                            'Key' => $key,
                            'CopySource' => $bucket . '/' . $key,
                            'MetadataDirective' => 'REPLACE',
                            'ACL' => 'public-read',
                            'command.headers' => array(
                                "Cache-Control" => "max-age=94608000",
                                "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+3 years"))
                            )
                        ));
                    } catch (Exception $e) {
                        echo $objectKey . ': ' . $e->getMessage() . PHP_EOL;
                    }
                }
            } catch (S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
    }

    public function actionCreateInsertSQL() {
        $statement = self::makeRecoverySQL('user', '3364');
        print $statement;
    }

    function makeRecoverySQL($table, $id) {
        // get the record          
        $selectSQL = "SELECT * FROM `" . $table . "` WHERE `id` = " . $id . ';';

        $result = Yii::app()->db->createCommand($selectSQL);
        $row = $result->queryROW();

        $insertSQL = "INSERT INTO `" . $table . "` SET ";
        foreach ($row as $field => $value) {
            $insertSQL .= " `" . $field . "` = '" . $value . "', ";
        }
        $insertSQL = trim($insertSQL, ", ");

        return $insertSQL;
    }

    public function actionWSDL() {
        $site_id = "PBYP_DUK";
        $password = "P1CT_8Y_P05T";
        /*
          $xml_data_checkStock = "
          <app:checkStock>
          <app:siteId>$site_id</app:siteId>
          <app:password>$password</app:password>
          <app:checkStock>
          &lt;SKUList&gt;
          &lt;account&gt;TESTSHOP_D2C&lt;/account&gt;
          &lt;SKU&gt;TESTITEM2&lt;/SKU&gt;
          &lt;/SKUList&gt;
          </app:checkStock>
          </app:checkStock>";
          $value =  CDS::CheckStock($xml_data_checkStock);
          print_r($value);
         */

        $xml_data_createOrder = "
        <app:createOrder>
         <app:siteId>$site_id</app:siteId>
         <app:password>$password</app:password>
         <app:createOrder>
            &lt;OrderBundle&gt;
              &lt;Order&gt;
                &lt;OrderNumber&gt;TS0000007&lt;/OrderNumber&gt;
                &lt;CustomerNumber&gt;100002&lt;/CustomerNumber&gt;
                &lt;SiteID&gt;$site_id&lt;/SiteID&gt;
                &lt;DateCreated&gt;2016-07-30 11:30:00&lt;/DateCreated&gt;
                &lt;BillingName&gt;Peter Schmitzberger&lt;/BillingName&gt;
                &lt;BillingEmail&gt;peter.schmitzberger@hotmail.com&lt;/BillingEmail&gt;
                &lt;BillingAddress1&gt;Grazer Bundesstraße 16&lt;/BillingAddress1&gt;
                &lt;BillingCity&gt;Salzburg&lt;/BillingCity&gt;
                &lt;BillingState&gt;&lt;/BillingState&gt;
                &lt;BillingPostalCode&gt;5023&lt;/BillingPostalCode&gt;
                &lt;BillingCountry&gt;AT&lt;/BillingCountry&gt;
                &lt;ShipName&gt;Peter Schmitzberger&lt;/ShipName&gt;
                &lt;ShippingEmail&gt;peter.schmitzberger@hotmail.com&lt;/ShippingEmail&gt;
                &lt;ShipAddress1&gt;Grazer Bundesstraße 16&lt;/ShipAddress1&gt;
                &lt;ShipCity&gt;Salzburg&lt;/ShipCity&gt;
                &lt;ShipState&gt;&lt;/ShipState&gt;
                &lt;ShipPostalCode&gt;5023&lt;/ShipPostalCode&gt;
                &lt;ShipCountry&gt;AT&lt;/ShipCountry&gt;
                &lt;OrderLanguage&gt;DE&lt;/OrderLanguage&gt;
                &lt;OrderCurrency&gt;EUR&lt;/OrderCurrency&gt;
                &lt;Remark&gt;This is a remark.&lt;/Remark&gt;
                &lt;FreeShipping&gt;N&lt;/FreeShipping&gt;
                &lt;ShippingFlatFeeBT&gt;0&lt;/ShippingFlatFeeBT&gt;
                &lt;OrderLine&gt;
                  &lt;OrderNumber&gt;TS0000007&lt;/OrderNumber&gt;
                  &lt;LineID&gt;7&lt;/LineID&gt;
                  &lt;ProductID&gt;TESTITEM1&lt;/ProductID&gt;
                  &lt;Quantity&gt;1&lt;/Quantity&gt;
                  &lt;UnitPriceBT&gt;11.6723&lt;/UnitPriceBT&gt;
                &lt;/OrderLine&gt;
                &lt;TotalLines&gt;1&lt;/TotalLines&gt;
              &lt;/Order&gt;
            &lt;/OrderBundle&gt;</app:createOrder>
            </app:createOrder>
        ";
        echo $xml_data_createOrder;
        $value = CDS::CreateOrder($xml_data_createOrder);
        echo $value;

        /*
          $xml_data_cancelOrder = "
          <app:cancelOrder>
          <app:siteId>TESTSHOP_D2C</app:siteId>
          <app:password>C2D_POHSTSET</app:password>
          <app:cancelOrder>
          &lt;CancelBundle&gt;
          &lt;Cancel&gt;
          &lt;CancelLines&gt;
          &lt;SiteID&gt;TESTSHOP_D2C&lt;/SiteID&gt;
          &lt;OrderNumber&gt;TS0000004&lt;/OrderNumber&gt;
          &lt;LineID&gt;4&lt;/LineID&gt;
          &lt;ProductID&gt;&lt;/ProductID&gt;
          &lt;/CancelLines&gt;
          &lt;/Cancel&gt;
          &lt;/CancelBundle&gt;
          </app:cancelOrder>
          </app:cancelOrder>";
          $value =  CDS::CancelOrder($xml_data_cancelOrder);
          print_r($value);
         */
        /*
          $xml_data_getOrderStatus = "
          <app:getOrderStatus>
          <app:siteId>TESTSHOP_D2C</app:siteId>
          <app:password>C2D_POHSTSET</app:password>
          <app:getOrderStatus>
          &lt;Orders&gt;
          &lt;Order&gt;
          &lt;SiteID&gt;TESTSHOP_D2C&lt;/SiteID&gt;
          &lt;Type&gt;ORDER_LIST&lt;/Type&gt;
          &lt;OrderNumber&gt;&lt;/OrderNumber&gt;
          &lt;/Order&gt;
          &lt;/Orders&gt;
          </app:getOrderStatus>
          </app:getOrderStatus>";
          //$value =  CDS::OrderStatus($xml_data_getOrderStatus);
          print_r($value);
         */
    }

    public function actionsphePayPal() {
        Yii::app()->theme = 'bootstrap';
        $this->layout = 'blank';
        $this->render('sphepaypal');
    }

    public function actionsphePayment() {
        //print_r($_REQUEST);exit;
        $gateways = array();
        $gateways_var = array();
        $gateways_var['id'] = 1;
        $gateways_var['gateway_id'] = 4;
        $gateways_var['short_code'] = 'paypalpro';
        $gateways_var['api_username'] = 'developer.sanjeev31-facilitator-1_api1.gmail.com';
        $gateways_var['api_password'] = 'EME68W76966NXS5K';
        $gateways_var['api_signature'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AKCC9ZVKxmcbdZQCfGmUlnCte.0x';
        $gateways_var['api_mode'] = 'sandbox';
        $gateways_var['is_primary'] = 1;
        $gateways_var['is_currency_conversion'] = 0;
        $gateways_var['non_3d_secure'] = '';
        $gateways[0] = (object) $gateways_var;
        $this->setPaymentGatwayVariable($gateways);

        $user_data = array();
        $user_data['currency_code'] = 'USD';
        $user_data['havePaypal'] = $_REQUEST['havePaypal'];
        $user_data['amount'] = $_REQUEST['price'];
        $user_data['paymentType'] = "Sale";
        $user_data['paymentDesc'] = 'SPHE-$' . $_REQUEST['price'];
        $user_data['returnURL'] = Yii::app()->getBaseUrl(true) . "/test/spheSuccess";
        $user_data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/test/spheCancel";
        $user_data['email'] = $_REQUEST['email'];
        $user_data['currencyCodeType'] = 'USD';
        $user_data['shipping'] = 0;

        $payment_gateway_controller = 'ApipaypalproController';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $trans_data = $payment_gateway::processTransactionsPCI($user_data);
    }

    public function actionSpheSuccess() {
        echo "<h3 style='color:green'>Success!!!</h3><br> Click <a href='" . Yii::app()->getBaseUrl(true) . "/test/sphePayPal'>here</a> to go to the payment page.";
    }

    public function actionSpheCancel() {
        echo "<h3 style='color:red'>You have canceled the payment.</h3><br> Click <a href='" . Yii::app()->getBaseUrl(true) . "/test/sphePayPal'>here</a> to pay.";
    }

    public function actioncleanCode() {
        exit();
        $studio_id = 2663;
        Yii::app()->db->createCommand("DELETE FROM pg_order WHERE studio_id=" . $studio_id)->execute();
        Yii::app()->db->createCommand("DELETE FROM pg_shipping_address WHERE studio_id=" . $studio_id)->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_order_details")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_order_status_log")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_preorder_category")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_preorder_items")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_preorder_price")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_product_content")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_saved_address")->execute();
        Yii::app()->db->createCommand("TRUNCATE TABLE pg_shipping_address")->execute();
        Yii::app()->db->createCommand("DELETE FROM transactions WHERE studio_id=" . $studio_id . " AND transaction_type=4")->execute();
    }

    public function actionTestImageDemo() {
        $studio_id = Yii::app()->common->getStudioId();
        $folderPath = Yii::app()->common->getFolderPath('', $studio_id);
        $unsignedBucketPath = $folderPath['unsignedFolderPath'];
        echo $studio_id . "==" . $unsignedBucketPath;
        $url = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        echo "<br>" . $url;
    }

    /*
      Please don't delete the following 3 functions
     *      */

    public function actionEmbedBufferLog() {
        $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : CHttpRequest::getUserHostAddress();
        if (isset($ip_address) && isset($_REQUEST['embed_id'])) {
            if (Yii::app()->aws->isIpAllowed($ip_address)) {
                $movie_data = Yii::app()->db->createCommand()
                        ->select('f.id,f.name,f.permalink,f.created_date,f.studio_id,ms.id AS stream_id,ms.wiki_data,ms.full_movie,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.resolution_size,ms.video_duration')
                        ->from('films f ,movie_streams ms ')
                        ->where('f.id = ms.movie_id AND embed_id=:embed_id', array(':embed_id' => $_REQUEST['embed_id']))
                        ->queryRow();
                $movie_id = $movie_data['id'];
                $stream_id = $movie_data['stream_id'];
                $studio_id = $movie_data['studio_id'];
                if (isset($movie_data) && count($movie_data)) {
                    $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                    $isAllowed = 0;
                    $device_id = 0;
                    $device_type = isset($_REQUEST['device_type']) && intval($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 1;
                    if ($device_type) {
                        $isAllowed = 1;
                        $device_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
                        $device_type = $device_type;
                        $resolution = @$_REQUEST['resolution'];
                        $start_time = @$_REQUEST['start_time'];
                        $end_time = @$_REQUEST['end_time'];
                        $log_unique_id = (isset($_REQUEST['u_id']) && trim(($_REQUEST['u_id']))) ? $_REQUEST['u_id'] : '';
                        $log_id = (isset($_REQUEST['buff_log_id']) && trim(($_REQUEST['buff_log_id']))) ? $_REQUEST['buff_log_id'] : '';

                        $res_size = json_decode($movie_data['resolution_size'], true);
                        $video_duration = explode(':', $movie_data['video_duration']);
                        $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);

                        $size = $res_size[$resolution];
                        $played_time = $end_time - $start_time;
                        $bandwidth_used = ($size / $duration) * $played_time;
                        if (isset($log_unique_id) && trim($log_unique_id)) {
                            $buff_log = BufferLogs::model()->findByAttributes(array('unique_id' => $log_unique_id, 'studio_id' => $studio_id, 'movie_id' => $movie_id, 'resolution' => $resolution));
                            if (isset($buff_log) && !empty($buff_log)) {
                                $buff_log->end_time = $end_time;
                                $buff_log->buffer_size = $bandwidth_used;
                                $buff_log->played_time = $end_time - $buff_log->start_time;
                                $buff_log->save();
                                $buff_log_id = $log_id;
                                $unique_id = $log_unique_id;
                            }
                        } else {
                            $city = @$_SESSION['city'];
                            $region = @$_SESSION['region'];
                            $country = @$_SESSION['country_name'];
                            $country_code = @$_SESSION['country'];
                            $continent_code = @$_SESSION['continent_code'];
                            $latitude = @$_SESSION['latitude'];
                            $longitude = @$_SESSION['longitude'];
                            $unique_id = md5(uniqid(rand(), true));
                            $buff_log = new BufferLogs();
                            $buff_log->studio_id = $studio_id;
                            $buff_log->unique_id = $unique_id;
                            $buff_log->user_id = @$user_id;
                            $buff_log->device_id = @$device_id;
                            $buff_log->movie_id = @$movie_id;
                            $buff_log->video_id = @$stream_id;
                            $buff_log->resolution = @$resolution;
                            $buff_log->start_time = @$start_time;
                            $buff_log->end_time = @$end_time;
                            $buff_log->played_time = @$played_time;
                            $buff_log->buffer_size = @$bandwidth_used;
                            $buff_log->city = @$city;
                            $buff_log->region = @$region;
                            $buff_log->country = @$country;
                            $buff_log->country_code = @$country_code;
                            $buff_log->continent_code = @$continent_code;
                            $buff_log->latitude = @$latitude;
                            $buff_log->longitude = @$longitude;
                            $buff_log->device_type = @$device_type;
                            $buff_log->ip = @$ip_address;
                            $buff_log->created_date = date('Y-m-d H:i:s');
                            $buff_log->save();
                            $buff_log_id = $buff_log->id;
                        }
                    } else {
                        $data['code'] = 446;
                        $data['status'] = "Error";
                        $data['msg'] = "Device type not found!";
                    }
                }
            } else {
                $data['code'] = 445;
                $data['status'] = "Error";
                $data['msg'] = "IP address is not allowed!";
            }
        } else {
            $data['code'] = 444;
            $data['status'] = "Error";
            $data['msg'] = "IP address required!";
        }
        $data['id'] = $buff_log_id;
        $data['u_id'] = $unique_id;
        echo json_encode($data);
        exit;
    }

    public function actionEmbedNewBufferLog() {
        $ip_address = (isset($_REQUEST['ip_address']) && trim($_REQUEST['ip_address'])) ? $_REQUEST['ip_address'] : CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed($ip_address)) {
            $movie_data = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.created_date,f.studio_id,ms.id AS stream_id,ms.wiki_data,ms.full_movie,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.resolution_size,ms.video_duration')
                    ->from('films f ,movie_streams ms ')
                    ->where('f.id = ms.movie_id AND embed_id=:embed_id', array(':embed_id' => $_REQUEST['embed_id']))
                    ->queryRow();
            $movie_id = $movie_data['id'];
            $stream_id = $movie_data['stream_id'];
            $studio_id = $movie_data['studio_id'];
            if (isset($movie_data) && count($movie_data)) {
                $user_id = (isset($_REQUEST['user_id']) && $_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
                $isAllowed = 0;
                $device_id = 0;
                $device_type = isset($_REQUEST['device_type']) && intval($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 1;
                if ($device_type) {
                    $device_type = $device_type;
                    $resolution = $_REQUEST['resolution'];
                    $start_time = $_REQUEST['start_time'];
                    $end_time = $_REQUEST['end_time'];
                    $log_unique_id = (isset($_REQUEST['log_unique_id']) && trim(($_REQUEST['log_unique_id']))) ? $_REQUEST['log_unique_id'] : '';
                    $log_id = (isset($_REQUEST['log_id']) && trim(($_REQUEST['log_id']))) ? $_REQUEST['log_id'] : '';

                    $res_size = json_decode($movie_data['resolution_size'], true);
                    $video_duration = explode(':', $movie_data['video_duration']);
                    $duration = ($video_duration[0] * 60 * 60) + ($video_duration[1] * 60) + ($video_duration[2]);

                    $size = $res_size[$resolution];
                    $played_time = $end_time - $start_time;
                    $bandwidth_used = ($size / $duration) * $played_time;
                    $city = @$_SESSION['city'];
                    $region = @$_SESSION['region'];
                    $country = @$_SESSION['country_name'];
                    $country_code = @$_SESSION['country'];
                    $continent_code = @$_SESSION['continent_code'];
                    $latitude = @$_SESSION['latitude'];
                    $longitude = @$_SESSION['longitude'];

                    $unique_id = md5(uniqid(rand(), true));
                    $buff_log = new BufferLogs();
                    $buff_log->studio_id = $studio_id;
                    $buff_log->unique_id = $unique_id;
                    $buff_log->user_id = $user_id;
                    $buff_log->device_id = $device_id;
                    $buff_log->movie_id = $movie_id;
                    $buff_log->video_id = $stream_id;
                    $buff_log->resolution = $resolution;
                    $buff_log->start_time = $start_time;
                    $buff_log->end_time = $end_time;
                    $buff_log->played_time = $played_time;
                    $buff_log->buffer_size = $bandwidth_used;
                    $buff_log->city = $city;
                    $buff_log->region = $region;
                    $buff_log->country = $country;
                    $buff_log->country_code = $country_code;
                    $buff_log->continent_code = $continent_code;
                    $buff_log->latitude = $latitude;
                    $buff_log->longitude = $longitude;
                    $buff_log->device_type = $device_type;
                    $buff_log->ip = $ip_address;
                    $buff_log->created_date = date('Y-m-d H:i:s');
                    $buff_log->save();
                    $buff_log_id = $buff_log->id;
                } else {
                    $data['code'] = 446;
                    $data['status'] = "Error";
                    $data['msg'] = "Device type not found!";
                }
            }
        } else {
            $data['code'] = 445;
            $data['status'] = "Error";
            $data['msg'] = "IP address is not allowed!";
        }
        $data['id'] = $buff_log_id;
        $data['u_id'] = $unique_id;
        echo json_encode($data);
        exit;
    }

    /* public function actionadd_log() {
      $ip_address = CHttpRequest::getUserHostAddress();
      if (Yii::app()->aws->isIpAllowed()) {
      $movie_id = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
      $stream_id = isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0;
      $movie_data = Yii::app()->db->createCommand()
      ->select('f.id,f.name,f.permalink,f.created_date,f.studio_id,ms.id AS stream_id,ms.wiki_data,ms.full_movie,f.content_types_id,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.resolution_size,ms.video_duration')
      ->from('films f ,movie_streams ms ')
      ->where('f.id = ms.movie_id AND ms.movie_id=:movie_id', array(':movie_id'=>$_REQUEST['movie_id']))
      ->queryRow();
      $studio_id = $movie_data['studio_id'];
      $video_log = new VideoLogs();
      $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
      if ($log_id > 0) {
      $video_log = VideoLogs::model()->findByPk($log_id);
      $video_log->updated_date = new CDbExpression("NOW()");
      $video_log->played_length = $_REQUEST['played_length'];
      } else {
      $video_log->created_date = new CDbExpression("NOW()");
      $video_log->ip = $ip_address;
      $video_log->video_length = $_REQUEST['video_length'];
      }

      $video_log->movie_id = $movie_id;
      $video_log->video_id = $stream_id;
      $video_log->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 0;
      $video_log->studio_id = $studio_id;
      $video_log->watch_status = $_REQUEST['status'];

      if ($video_log->save()) {
      echo $video_log->id;
      exit;
      } else {
      echo 0;
      exit;
      }
      }
      }

     */

    public function actionembedVideoAddLog() {
        $ip_address = CHttpRequest::getUserHostAddress();
        if (Yii::app()->aws->isIpAllowed()) {
            $movie_id = isset($_REQUEST['movie_id']) ? $_REQUEST['movie_id'] : 0;
            $stream_id = isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0;
            $device_type = isset($_REQUEST['device_type']) && intval($_REQUEST['device_type']) ? $_REQUEST['device_type'] : 1;
            $video_log = new VideoLogs();
            $log_id = (isset($_REQUEST['log_id']) && intval($_REQUEST['log_id'])) ? $_REQUEST['log_id'] : 0;
            if ($log_id > 0) {
                $video_log = VideoLogs::model()->findByPk($log_id);
                $video_log->updated_date = new CDbExpression("NOW()");
                $video_log->played_length = $_REQUEST['played_length'];
                $video_log->played_percent = $_REQUEST['percent'];
            } else {
                $video_log->created_date = new CDbExpression("NOW()");
                $video_log->ip = $ip_address;
                $video_log->video_length = $_REQUEST['video_length'];
                $video_log->played_percent = $_REQUEST['percent'];
                $video_log->played_length = $_REQUEST['played_length'];
                $video_log->device_type = @$device_type;
            }

            $video_log->movie_id = $movie_id;
            $video_log->video_id = $stream_id;
            $video_log->user_id = $_REQUEST['user_id'];
            $video_log->studio_id = $_REQUEST['studio_id'];
            $video_log->watch_status = $_REQUEST['status'];

            if ($video_log->save()) {
                echo $video_log->id;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }

    public function actionAddCustomFormPhysical() {
        //exit;
        $studio_id = $_GET['sid'];
        $cmf = New CustomMetadataForm;
        $cmf->studio_id = $studio_id;
        $cmf->content_types_id = 7;
        $cmf->form_type_id = 6;
        $cmf->title = 'Add Items';
        $cmf->description = NULL;
        $cmf->created_by = NULL;
        $cmf->created_date = NULL;
        $cmf->updated_date = NULL;
        $cmf->ip = NULL;
        $cmf->save();
        $ins_id = $cmf->id;

        $cmfield = New CustomMetadataField;
        $fieldarray = array('studio_id', 'f_type', 'f_display_name', 'f_name', 'f_id', 'f_value', 'f_is_required', 'created_by', 'created_date', 'updated_by', 'updated_date', 'ip', 'id_seq');
        $valuearray = array(
            array($studio_id, 0, 'Item Name', 'name', 'mname', 'Enter item name here.', 1, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 1, 'Description', 'description', 'mdescription', 'Enter item description here', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            //array($studio_id, 0, 'Alternate Name', 'alternate_name', 'c_alternate_name', 'Enter Alternate Name', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 4, 'Item Type', 'product_type', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 0, 'Price', 'sale_price', NULL, 'Enter amount', 1, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 0, 'SKU Number', 'sku', 'skuno', 'Enter SKU number', 1, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 2, 'Format', 'custom_fields', 'c_dvdtype', '["DVD","BLU-RAY","3D BLU-RAY","4K ULTRA HD"]', 1, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 0, 'Rating', 'rating', 'c_rating', 'Enter rating', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 2, 'Size', 'size', 'c_size', 'Enter size', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            array($studio_id, 3, 'Genre', 'genre', 'c_genre', '["ACTION","FAMILY & KIDS","DRAMA","COMEDY","THRILLER","OTHER"]', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            //array($studio_id, 0, 'Barcode', 'barcode', 'c_barcode', 'Enter barcode', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            //array($studio_id, 1, 'Audio', 'audio', 'c_audio', 'Enter audio', 0, NULL, NULL, NULL, NULL, NULL, NULL),
            //array($studio_id, 1, 'Subtitles', 'subtitles', 'c_subtitles', 'Enter Subtitles', 0, NULL, NULL, NULL, NULL, NULL, NULL)
            array($studio_id, 0, 'Actor', 'actor', 'c_actor', 'Enter actor name', 0, NULL, NULL, NULL, NULL, NULL, NULL)
        );
        $cmff = New CustomMetadataFormField;
        foreach ($valuearray as $key => $value) {
            foreach ($fieldarray as $key1 => $value1) {
                $cmfield->$value1 = $value[$key1];
            }
            $cmfield->isNewRecord = TRUE;
            $cmfield->primaryKey = NULL;
            $cmfield->save();
            $tempid = $cmfield->id;
            //insert into customformfield mapping
            $cmff->custom_form_id = $ins_id;
            $cmff->custom_field_id = $tempid;
            $cmff->created_by = NULL;
            $cmff->created_date = NULL;
            $cmff->updated_date = NULL;
            $cmff->id_seq = ++$key;
            $cmff->isNewRecord = TRUE;
            $cmff->primaryKey = NULL;
            $cmff->save();
        }
    }

    function actionPhpInfo() {
        phpinfo();
        exit;
    }

    public function actionStringTest() {
        $str = 'cus_8swr7jkhpfWTlz,cus_9AgQd0UbXsuDuX,cus_9GEVW0N0pkygri,cus_9GWeSfFqMcdJXs,cus_9InVSGcFfbyzGJ,cus_9KOpIyQnqhSFC9,cus_9NCH8wKyE8YsSz,cus_8swlISgKkZTR69,cus_9AFuyQJvpFuvcE,cus_9O8ivIFOhjF2HB,cus_9J8aXa1z0lzZbH,cus_97ujKNLYJGd9rO,cus_9QI5kjy7lKjxSv,cus_9J8e9n2wfHzGdy,cus_9Bxjmv7vyjCqf3,cus_99WIq61lmDoplt,cus_8swim4h8KUcckO,cus_8swkd3hAScIiYF,cus_9QzE7TapkcooZ5,cus_9OShVIvL5QctIf,cus_9HN3zx2J5JnCcJ,cus_8u0kGWmY9OnRuz,cus_9R1WmXEbUSGFuL,cus_9FkGyheLYFjRsp,cus_8swjTfQBBP36MV,cus_9OYg1xUe7G6sDq,cus_9FMw9hsHjVvcG1,cus_9I6EfN5nXEkon0,cus_9HlnJfKQy2nfK1,cus_97yvvb8NdGlsrb,cus_9SY5dDQlXSuGvu,cus_9LVUCoO5aFzrIT,cus_9OePHVANKArte2,cus_9IoVKQMRatbNmr,cus_9J7m0MmV5jQFDI,cus_9KLoavPM7ivaBM,cus_9ExGfaQ9GKzYLS,cus_8swsi88IKZAWGJ,cus_9T1E4ltQAxmwGk,cus_9M9qk28EPEzeak,cus_9JAuDKeSz6ZItC,cus_9TBlOOCcqKRTCm,cus_9Max5rX1jviuzV,cus_9My8UwAnTKDDPk,cus_9PLc3cS7oxB5Gr,cus_9DRoNi7lUe6mxX,cus_9TQKG2ZBgm3RmH,cus_9NFtbBsc8aISzi,cus_8swrFJ1E6y3eui,cus_9OUDOBsCWP4dWP,cus_9NwuKyDuKbfhPH,cus_9RRM5Yke7kGDUe,cus_8u0N30g8A3ZzJa,cus_9UJlUCioD2mTmr,cus_9TYqaxXujzR9Mq,cus_9RqdgnV9kt3m9a,cus_9O7WQUTPB2Cm6t,cus_9HFhZS9gVGhnGq,cus_9QP0ZHvWOZp9kW,cus_9TItWB7leBqUNz,cus_9GuISFR6fKezE1,cus_9HMhiaJ1NmF3ps,cus_9UMrHZpwMOqZbs,cus_9NyxN3vFloSbQW,cus_9UGbT7nkdjvORb,cus_9OsvtwhHL3KiUg,cus_9UWL0Obf7mlvGH,cus_9Mr6eHJzVSco6p,cus_9VCN4y0yUCeKMU,cus_9PE13RxLFbMCfH,cus_9ITOiaCepPqJ7v,cus_9UPtwRSPcTPPE8,cus_9VvmfT3wkp9rcs,cus_9RTrqYoZio8Hq2,cus_9U2sHscdCtYgd4,cus_9P288V0opjZeb6,cus_9VqBzPAGVsWFQ9,cus_9RlxtiyU0LgUs7,cus_9VHHrmsgpgnxvz,cus_9Kgd3VgpBBDn3Y,cus_9W0uoCheJZ0bk6,cus_9PLeMZklpYHNV7,cus_9TZhIKkdkh7MJJ,cus_9VwegC6yohVGX8,cus_9UWHSq6s5TJUJQ,cus_9VbciYE40yTNnr,cus_9Wwrr5EnFPGtnL,cus_9VRfjKtpRrrL1D,cus_9SeyQnpGkk56zR,cus_9VY655CS67zhHK,cus_9Uu5BarvFquxib,cus_9WPFFck9HgJFfm,cus_9TLteTPMPEmTzh,cus_9WJfk8JRf9LMok,cus_9WhlCeqgiUdl4r,cus_9Vw2jP4CLQnZg9,cus_9X4znGFfKcvi5c,cus_9WGVwM82NP5DLB,cus_9XB5OQQ0rHQKb8,cus_9Vc6BoJT6dj48k,cus_9VC5XEKVchmc3i,cus_9WxD5PK6mdh6w0,cus_9WO2zojNPcxJon,cus_9X4yWBzdUFMkxp';
        $_new = explode(',', $str);
        $i = 0;
        $user_id = array();
        $id = array();
        $k = 0;
        foreach ($_new as $old) {
            $sql = 'SELECT id,user_id FROM user_subscriptions where profile_id = "' . $old[$i] . '"';
            $data = Yii::app()->db->createCommand($sql)->queryRow();
            $user_id[$k] = $data['user_id'];
            $id[$k] = $data['id'];
            $i++;
            $k++;
        }
        print_r($user_id);
        print "<br><br>";
        print_r($id);
    }

    public function actionadyen() {
        Yii::app()->theme = 'bootstrap';
        $this->layout = 'blank';
        $this->render('adyen');
    }

    public function actionAdyenPayment() {
        $hmacKey = 'C99269C79E1BEB15DAC71A20A9C6FB9EF94B4D1BD52EB101DD9B4E93DC87C075';

        $params["merchantReference"] = "Internet Order 12345";
        $params["paymentAmount"] = $_REQUEST['price'] * 100;
        $params["currencyCode"] = "USD";
        $params["shipBeforeDate"] = date('Y-m-d');
        $params["skinCode"] = "PfG3rpYU";
        $params["merchantAccount"] = "MuviCOM";
        $params["sessionValidity"] = date('Y-m-d\TH:i:s\Z', strtotime(date('Y-m-d') . '+1 day'));


        $escapeval = function ($val) {
            return str_replace(':', '\\:', str_replace('\\', '\\\\', $val));
        };
        ksort($params, SORT_STRING);
        $signData = implode(":", array_map($escapeval, array_merge(array_keys($params), array_values($params))));
        $merchantSig = base64_encode(hash_hmac('sha256', $signData, pack("H*", $hmacKey), true));
        $params["merchantSig"] = $merchantSig;
        echo json_encode($params);
        exit;
    }

    public function actionBraintreeCustomerDetails() {
        $varified = array();
        $unvarified = array();
        if (isset($_REQUEST) && trim($_REQUEST['user'])) {
            $id = $_REQUEST['user'];
        } else {
            $ids = 'cus_8u0kGWmY9OnRuz,cus_97ujKNLYJGd9rO,cus_97yvvb8NdGlsrb,cus_9a2IqMgLvxR6O9,cus_9a6k7yV0SGb94F,cus_9a8GukdiIMUAgE,cus_9aiQ2qF5Uej0lb,cus_9aJ3Wei8i7oegK,cus_9aMifrw6HufoWK,cus_9aTy8edPlAtD02,cus_9b570NYaW0EWSi,cus_9b8AwW5zvZnQQY,cus_9bACGHsQpBd59S,cus_9bDuk605CzB6gg,cus_9bFf4GqFeXjjkk,cus_9bFlVzaI4oUUFj,cus_9bnmHT6vDKlofs,cus_9butH2csfPFxtB,cus_9bynd5eCbcOxfe,cus_9c0ckvUz5OnxBI,cus_9c1wDasQDEr3nF,cus_9cAeh9I0Kvs3tL,cus_9cAK0qZ8aS5ZB5,cus_9cCs0zws7eqw5K,cus_9cCWCLWJhTkT53,cus_9cCzlfLqEyKVCU,cus_9cezaXhIfTukib,cus_9cgUQ4U06wNhsU,cus_9cJDrTm1bpItr1,cus_9CKpe2xlV5WxV5,cus_9cMHHkK2Zetug9,cus_9cTn3cO2cnqOTF,cus_9cwhTB8nWq0QU7,cus_9cXdLTMCgYlrV6,cus_9cyP4t65yn4LKU,cus_9cyptXo0bASozw,cus_9cZQZW52dQEwjF,cus_9d50j4U8N0Blij,cus_9d73CzgelESxRW,cus_9diOVHzpY6EVX7,cus_9djrryoK6zyMUo,cus_9dm5ePONltLjM3,cus_9do8Y0OrQRNwuh,cus_9douGRAKZQ3ZPN,cus_9dp9h7v8Il0U2e,cus_9dPbJ5WhPn1S69,cus_9drvlKmJFzVdRP,cus_9dsb3sKyzDvJmK,cus_9Dt2pdgD8WcQpQ,cus_9dWJsaV6LYB8wb,cus_9dXqSNdaCEcRFa,cus_9e5MuNtiuDtNgc,cus_9e77ZBckWNlGiG,cus_9e889pJmg9EepQ,cus_9e99B1sDbKo53C,cus_9ea4GDrl5ghHRj,cus_9eBlvSerV61csV,cus_9eC9y5GQtfvHj4,cus_9enTui7HUZey8r,cus_9epPP1BnI4cQMW,cus_9eqETlOrrC5wCw,cus_9erM8k5Rc29BkU,cus_9esdoQ8piA69Cx,cus_9eTa5bcc49Afsy,cus_9ete9VJYMdj1qF,cus_9eTFpZ92ZZNFmd,cus_9eVeYVdQPAo1f6,cus_9ew7vlifSd5XzO,cus_9exCfUNDnpoMND,cus_9exXWHHXnEsddz,cus_9eYAvaMil6ZLyj,cus_9eYcueppy099ZO,cus_9eykCvJUf4Qtlh,cus_9eZdQ5zSxzkaOA,cus_9f06qrhV4vkWog,cus_9f1L2iqJWODqbe,cus_9f3rfhqLbw8krb,cus_9f4vjXkB3nBa6i,cus_9f8RdCRP6O0361,cus_9fBrlrcsGOr0A6,cus_9fFBCFiskSnKNZ,cus_9fgeFX6cBC4fuj,cus_9fIqINkEytwdpn,cus_9FkGyheLYFjRsp,cus_9flSoGBTfBzjnw,cus_9FMw9hsHjVvcG1,cus_9fmyz4q7TyLzps,cus_9fNRymwVwDWQFH,cus_9fvl7QmzyKYjKl,cus_9fzibfdoZ13tQh,cus_9g2easV7fr1PNo,cus_9g2REqIkjy2nUk,cus_9g3iIsJNBiXU7V,cus_9g6jbGVmYXYKyF,cus_9g7rkCYpoWBPHD,cus_9gCC44FLuIy27M,cus_9GEVW0N0pkygri,cus_9gIpklUWLBS0Wv,cus_9gkjwFDDLok3jz,cus_9gkKV1FyjLAbNH,cus_9gLnVXoQeS4taN,cus_9gMHlfQUO4fNtN,cus_9gmtWBiHP4M1Cz,cus_9gNc76AlO4ZV31,cus_9goiza4XIPVwSo,cus_9gOuut0IImkJOd,cus_9gP45f7xuwdp5K,cus_9gpVgUIoTwQNDf,cus_9gSPa7L5wrebzI,cus_9gsVQOZ9OVjhwh,cus_9gUBEJP9tJYE8S,cus_9GuISFR6fKezE1,cus_9gUPZ3jwojEe9O,cus_9gusv1Rqd0e2No,cus_9gVnQWaH9YuV2D,cus_9GWeSfFqMcdJXs,cus_9h7cphVwVEOeh5,cus_9h8fToSvazkJhj,cus_9h8p3czdBJAWQd,cus_9hA6b4c2ZCMF9G,cus_9haCh3AEGhvw81,cus_9haPhY2wPwZ0Bx,cus_9hCq62R73SSdAP,cus_9hEAs1Ir1GB87w,cus_9hEUMMrzEHqwNX,cus_9hEZL1dgKM1Cpc,cus_9HFhZS9gVGhnGq,cus_9hl0gj1ZEH6Fvu,cus_9hlaqCljK0xctR,cus_9HlnJfKQy2nfK1,cus_9HMhiaJ1NmF3ps,cus_9HN3zx2J5JnCcJ,cus_9hoQkf8AeO9Z2Q,cus_9hoS9r0r2dZVdO,cus_9hr7RDuzUacW5O,cus_9hrl2TP1CfhK9g,cus_9hsMqGVLE20s8J,cus_9hsugBkXr9UDbx,cus_9htGxUV0NZcuN9,cus_9hTITygddTPGEQ,cus_9hTSy8BvYsS1Wh,cus_9hva5XgFZQSm5Q,cus_9hVibKfsoXWVC0,cus_9hXK2vPXhr33Un,cus_9hxqhHstI0OJGP,cus_9hzYoTqL6Jd3aE,cus_9i2PCUQiSfKqeT,cus_9i3KdLbfpMMSDa,cus_9I6EfN5nXEkon0,cus_9i9Wx9MwMB717T,cus_9iAfqj7UOlEiS2,cus_9iAX11KLtKszX9,cus_9ibWTEju3Zdkeu,cus_9ictRjRdVZWrOn,cus_9ieDhl4ZGXLqTy,cus_9iF5dyrH0C5fMD,cus_9iFtHCgkeZshY3,cus_9iIpDvPdnsDmHd,cus_9iIXR9uBowoxSr,cus_9iJWyT70s9vnwM,cus_9iKaOyQhVKFjDC,cus_9InVSGcFfbyzGJ,cus_9IoVKQMRatbNmr,cus_9ITOiaCepPqJ7v,cus_9iuceUk9EPFiua,cus_9iwe6zw7mIH5eK,cus_9iweSJtQoWjqND,cus_9j1ijzGeMGpGUc,cus_9j3u5Zz1YVnmLm,cus_9j505fHLECrBuf,cus_9J7m0MmV5jQFDI,cus_9J8e9n2wfHzGdy,cus_9j9DixsQ4PZkPy,cus_9j9L7R8E85MuZM,cus_9JAuDKeSz6ZItC,cus_9KLoavPM7ivaBM,cus_9KOpIyQnqhSFC9,cus_9M9qk28EPEzeak,cus_9Max5rX1jviuzV,cus_9NFtbBsc8aISzi,cus_9NyxN3vFloSbQW,cus_9O7WQUTPB2Cm6t,cus_9O8ivIFOhjF2HB,cus_9OePHVANKArte2,cus_9Opiy3wgkEeWyG,cus_9OShVIvL5QctIf,cus_9OsvtwhHL3KiUg,cus_9OUDOBsCWP4dWP,cus_9OYg1xUe7G6sDq,cus_9P288V0opjZeb6,cus_9QP0ZHvWOZp9kW,cus_9RqdgnV9kt3m9a,cus_9RRM5Yke7kGDUe,cus_9RTrqYoZio8Hq2,cus_9SeyQnpGkk56zR,cus_9SY5dDQlXSuGvu,cus_9T1E4ltQAxmwGk,cus_9TBlOOCcqKRTCm,cus_9TItWB7leBqUNz,cus_9TQKG2ZBgm3RmH,cus_9TYqaxXujzR9Mq,cus_9U2sHscdCtYgd4,cus_9UJlUCioD2mTmr,cus_9UMrHZpwMOqZbs,cus_9Uu5BarvFquxib,cus_9UWL0Obf7mlvGH,cus_9Vc6BoJT6dj48k,cus_9VHHrmsgpgnxvz,cus_9VRfjKtpRrrL1D,cus_9VwegC6yohVGX8,cus_9XkaBM0GsvND1D,cus_9XTHYs5e6ALLrC,cus_9XtVYmjZKwkif4,cus_9Y2t44j2MhWbuo,cus_9Y9mkbVi5FtnLU,cus_9YCCzo2auQ8BVj,cus_9Yr7tqLEhrC9tP,cus_9YWd4fESRLGmNU,cus_9YXeEUPRjbINuX,cus_9YXqxyrNfs9N8f,cus_9YYNfQbhCKBYYi,cus_9YzsviOcCzFuXR,cus_9ZKuSp4dnEukhr,cus_9ZSBiU9MlRyd5E,cus_9ZvxYxMKeYSOKD,cus_9ZYOt74k6zihEt,cus_9ZZ8FIqLFFhPCG,cus_9ZZBoU52H9VRpe';
        }
        $customer_ids = explode(',', $ids);
        $j = 0;
        $k = 0;

        $headArr[0] = array('Name', 'Email', 'Customer ID');
        $headArr[1] = array('Name', 'Email', 'Customer ID');
        $sheetName[0] = 'Verified Users';
        $sheetName[1] = 'Unverified Users';
        $sheet = count($sheetName);
        $data_excel[0] = array();
        $data_excel[1] = array();
        for ($i = 0; $i < count($customer_ids); $i++) {
            $payment_gateway_controller = 'ApibraintreeController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $user_data = $payment_gateway::getCustomerDetails($customer_ids[$i]);

            $sql = "select u.email,u.display_name,us.profile_id from sdk_users u, user_subscriptions us where u.id = us.user_id and us.studio_id = 1600 and profile_id = '" . $customer_ids[$i] . "'";
            $data = Yii::app()->db->createCommand($sql)->queryRow();
            if (isset($user_data->id) && trim($user_data->id) == $customer_ids[$i]) {
                $varified[$j]['Name'] = $data['display_name'];
                $varified[$j]['Email'] = $data['email'];
                $varified[$j]['Customer_id'] = $customer_ids[$i];
                $j++;

                $data_excel[0][] = array($data['display_name'], $data['email'], $customer_ids[$i]);
            } else {
                $unvarified[$k]['Name'] = $data['display_name'];
                $unvarified[$k]['Email'] = $data['email'];
                $unvarified[$k]['Customer_id'] = $customer_ids[$i];
                $k++;
                $data_excel[1][] = array($data['display_name'], $data['email'], $customer_ids[$i]);
            }
        }
        $type = 'xls';
        $filename = 'eduflix_user_verification_report_' . date('Ymd_His');
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data_excel, $filename, $type);
        print "<pre>";
        print_r($varified);
        print_r($unvarified);
        exit;
    }

    public function actionAddCustomFormForGreta() {
        if (!in_array($_GET['sid'], array(2530))) {
            echo "invalid studio id";
            exit;
        }
        $studio_id = $_GET['sid'];
        $cmf = New CustomMetadataForm;
        $cmf->studio_id = $studio_id;
        $cmf->content_types_id = 1;
        $cmf->form_type_id = 1;
        $cmf->title = 'Video On-demand Single part long Form';
        $cmf->description = NULL;
        $cmf->created_by = NULL;
        $cmf->created_date = NULL;
        $cmf->updated_date = NULL;
        $cmf->ip = NULL;
        $cmf->save();
        $ins_id = $cmf->id;

        $cmfield = New CustomMetadataField;
        $fieldarray = array('studio_id', 'f_type', 'f_display_name', 'f_name', 'f_id', 'f_value', 'f_is_required', 'created_by', 'created_date', 'updated_by', 'updated_date', 'ip', 'id_seq');
        $valuearray = array(
            array($studio_id, 0, 'Content Name', 'name', NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
            array($studio_id, 0, "Rights owner's Content ID", 'c_contentid', 'c_contentid', NULL, 0, NULL, NULL, NULL, NULL, NULL, 2),
            array($studio_id, 0, 'IMDB ID', 'c_imdbid', 'c_imdbid', NULL, 0, NULL, NULL, NULL, NULL, NULL, 2),
            array($studio_id, 0, 'Original title', 'c_original_title', 'c_original_title', NULL, 0, NULL, NULL, NULL, NULL, NULL, 4),
            array($studio_id, 0, 'Item title', 'c_item_title', 'c_item_title', NULL, 0, NULL, NULL, NULL, NULL, NULL, 5),
            array($studio_id, 0, 'Release/Recorded Date', 'release_date', 'release_date', NULL, 0, NULL, NULL, NULL, NULL, NULL, 6),
            array($studio_id, 0, 'Video release date', 'c_video_rdate', 'c_video_rdate', NULL, 0, NULL, NULL, NULL, NULL, NULL, 7),
            array($studio_id, 3, 'Genre', 'genre', 'genre', NULL, 0, NULL, NULL, NULL, NULL, NULL, 8),
            array($studio_id, 0, 'Production year', 'c_production_year', 'c_production_year', NULL, 0, NULL, NULL, NULL, NULL, NULL, 9),
            array($studio_id, 0, 'Production country', 'c_production_country', 'c_production_country', NULL, 0, NULL, NULL, NULL, NULL, NULL, 10),
            array($studio_id, 1, 'Story/Description', 'story', 'story1', NULL, 0, NULL, NULL, NULL, NULL, NULL, 11),
            array($studio_id, 0, 'Short synopsis', 'c_short_synopsis', 'c_short_synopsis', NULL, 0, NULL, NULL, NULL, NULL, NULL, 12),
            array($studio_id, 0, 'Keywords', 'c_keywords', 'c_keywords', NULL, 0, NULL, NULL, NULL, NULL, NULL, 13),
        );
        $relationalf_id = array('custom1' => 'c_contentid', 'custom2' => 'c_imdbid', 'custom3' => 'c_video_rdate', 'custom4' => 'c_original_title', 'custom5' => 'c_item_title', 'custom6' => 'c_production_year', 'custom7' => 'c_production_country', 'custom8' => 'c_short_synopsis', 'custom9' => 'c_keywords');
        $fcm = new FilmCustomMetadata;
        $cmff = New CustomMetadataFormField;
        foreach ($valuearray as $key => $value) {
            foreach ($fieldarray as $key1 => $value1) {
                $cmfield->$value1 = $value[$key1];
            }
            $cmfield->isNewRecord = TRUE;
            $cmfield->primaryKey = NULL;
            $cmfield->save();
            $tempid = $cmfield->id;
            //insert into customformfield mapping
            $cmff->custom_form_id = $ins_id;
            $cmff->custom_field_id = $tempid;
            $cmff->created_by = NULL;
            $cmff->created_date = NULL;
            $cmff->updated_date = NULL;
            $cmff->id_seq = ++$key;
            $cmff->isNewRecord = TRUE;
            $cmff->primaryKey = NULL;
            $cmff->save();

            $customefieldname = array_search($value[4], $relationalf_id);
            if ($customefieldname != "") {
                $fcm->field_name = $customefieldname;
                $fcm->custom_field_id = $tempid;
                $fcm->studio_id = $studio_id;
                $fcm->isNewRecord = TRUE;
                $fcm->primaryKey = NULL;
                $fcm->save();
            }
        }
    }

    function actionGetRawFileSize() {
        $studio_id = Yii::app()->user->studio_id;
        $sql = "SELECT F.id,F.name,F.content_types_id, M.id as stream_id, M.full_movie,M.episode_title, 0 AS bytesize, 0 as size FROM films F, movie_streams M WHERE F.id = M.movie_id AND M.studio_id={$studio_id} AND (M.full_movie IS NOT NULL AND M.full_movie !='')";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
        $folderPath = Yii::app()->common->getFolderPath("", $studio_id);
        $signedBucketPath = $folderPath['signedFolderPath'];
        $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
        $s3dir1 = $signedBucketPath . 'uploads/movie_stream/full_movie/';
        $bucketName = $bucketInfo['bucket_name'];

        $s3 = S3Client::factory(array(
                    'key' => Yii::app()->params->s3_key,
                    'secret' => Yii::app()->params->s3_secret
        ));
        $size = 0;
        if ($data) {
            foreach ($data AS $key => $val) {

                //$s3dir = 'uploads/movie_stream/full_movie/88/Imported_Kamariya_Song_Shanghai.mp4';
                $s3dir = $s3dir1 . $val['stream_id'] . "/";
                $objects = $s3->getIterator('ListObjects', array(
                    "Bucket" => $bucketName,
                    "Prefix" => $s3dir
                ));
                $data[$key]['video_path'] = $s3dir;
                foreach ($objects as $object) {
                    $keyName = array_reverse(explode('/', $object['Key']));
                    if (strstr(@$keyName[0], '_original')) {
                        $data[$key]['video_path'] = $object['Key'];
                        $data[$key]['bytesize'] = $object['Size'];
                        $data[$key]['size'] = number_format($object['Size'] / 1048576, 3);
                        $size +=$object['Size'];
                    }
                }
            }
        }
        Yii::app()->theme = 'admin';
        $totalSize = $this->formatSizeUnits($size);
        $this->layout = 'admin';
        $this->render('rawfile_size', array('data' => @$data, 'totalSize' => $totalSize, 'bucketName' => $bucketName));
    }

    public function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    /**
     * @author:Suraja<suraja@muvi.com>
     * @date:02/02/2017
     * @uses :This method developed for adding the existing cast details to solr.
     * @param: no
     * @return:no
     */
    public function actionaddExistingCastsToSolr() {
        $allstudio = Yii::app()->db->createCommand()
                ->select('id')
                ->from('studios')
                ->where('status=:status', array(':status' => 1))
                ->queryAll();

        foreach ($allstudio as $key => $val) {

            $studio_id = $val['id'];
            $solrobj = new SolrFunctions();
            $solrobj->deleteSolrQuery("cat:star AND sku:" . $studio_id);

            $now = new CDbExpression("NOW()");
            $data = Yii::app()->db->createCommand()
                    ->select('id,name,permalink,studio_id')
                    ->from('celebrities')
                    ->where('created_date <=:time AND studio_id=:studio_id AND parent_id=:parent_id', array(':time' => $now, ':studio_id' => $studio_id, ':parent_id' => 0))
                    ->queryAll();

            foreach ($data as $dk => $dv) {
                if (HOST_IP != '127.0.0.1') {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $dv['id'];
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = '';
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $dv['name'];
                    $solrArr['permalink'] = $dv['permalink'];
                    $solrArr['studio_id'] = $dv['studio_id'];
                    $solrArr['display_name'] = 'star';
                    $solrArr['content_permalink'] = 'star';
                    $solrObj = new SolrFunctions();
                    $ret_solr = $solrObj->addSolrData($solrArr);
                }
            }
        }
    }

    public function actionAddCustomfieldForTHEI() {
        $studio_id = 3063; // for Tarkovsky        
        $ins_id = 14;

        $cmfield = New CustomMetadataField;
        $fieldarray = array('studio_id', 'f_type', 'f_display_name', 'f_name', 'f_id', 'f_value', 'f_is_required', 'created_by', 'created_date', 'updated_by', 'updated_date', 'ip', 'id_seq');
        $valuearray = array(
            array($studio_id, 0, 'Region Code', 'region_code', 'c_region_code', 'Enter Region Code', 0, NULL, NULL, NULL, NULL, NULL, NULL),
        );
        $cmff = New CustomMetadataFormField;
        $abc = 7;
        foreach ($valuearray as $key => $value) {
            foreach ($fieldarray as $key1 => $value1) {
                $cmfield->$value1 = $value[$key1];
            }
            $cmfield->isNewRecord = TRUE;
            $cmfield->primaryKey = NULL;
            $cmfield->save();
            $tempid = $cmfield->id;
            //insert into customformfield mapping
            $cmff->custom_form_id = $ins_id;
            $cmff->custom_field_id = $tempid;
            $cmff->created_by = NULL;
            $cmff->created_date = NULL;
            $cmff->updated_date = NULL;
            $cmff->id_seq = ++$abc;
            $cmff->isNewRecord = TRUE;
            $cmff->primaryKey = NULL;
            $cmff->save();
        }
    }

    function actionDeleteAccountSony() {
        $dbcon = Yii::app()->db;
        //$sql = "SELECT users.* FROM (SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=4 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0) AS users WHERE user_id IS NOT NULL";
        //fetch all studios of sony account and delete those with corresponding users
        /* $portal_sql="SELECT * FROM user_relation WHERE refer_id=7 and relation_type='reseller'";
          $all_reseller=$dbcon->createCommand($portal_sql)->queryAll();
          foreach($all_reseller as $key=>$val){
          $studio_lists[]=$val['studio_id'];
          }
          $all_studio=implode(",",$studio_lists);
         */
        $studio_id = 2626;
        $sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON s.id=u.studio_id WHERE s.id ={$studio_id}";

        if (isset($_REQUEST['studio']) && !empty($_REQUEST['studio'])) {
            // $sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON (s.id=u.studio_id AND u.role_id=1 AND u.is_admin=0 AND u.is_sdk=1 AND u.is_active=1) WHERE s.status=4 AND s.is_subscribed=0 AND s.is_deleted=0 AND s.is_default=0 AND s.id=" . $_REQUEST['studio'];
            $sql = "SELECT s.*, u.id AS user_id, u.first_name, u.email FROM studios s LEFT JOIN user u ON s.id=u.studio_id WHERE s.id=" . $_REQUEST['studio'];
        }

        $studios = $dbcon->createCommand($sql)->queryAll();
        //print '<pre>';print count($studios);print_r($studios);exit;
        if (isset($studios) && !empty($studios)) {
            foreach ($studios as $key => $values) {
                $studio_id = $values['id'];
                $user_id = $values['user_id'];
                $end_date = date("Y-m-d", strtotime($values['end_date']));
                $today_date = gmdate('Y-m-d');
                if (intval($studio_id) && intval($user_id)) {
                    //If onhold period has finished, which has set at the time of cancellation account
                    //Delete Manage Cookie
                    StudioManageCookie::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Manage Coupon
                    StudioManageCoupon::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Studio S3 bucket Details
                    $studioS3BucketDetails = StudioS3buckets::model()->findByPk($values->studio_s3bucket_id);
//                        if (isset($studioS3BucketDetails->pem_file_path) && $studioS3BucketDetails->pem_file_path != '') {
//                            @unlink($pemFilePath . $studioS3BucketDetails->pem_file_path);
//                        }
                    StudioS3buckets::model()->deleteAll('id = :id', array(
                        ':id' => $values->studio_s3bucket_id
                    ));

                    //Delete all cdn details
                    StudioCdnDetails::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all Notification Template
                    NotificationTemplates::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all Studio Pricing Plan
                    StudioPricingPlan::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all Studio Payment Gateways
                    StudioPaymentGateways::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all Celebrity
                    Celebrity::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all StudioBanner
                    StudioBanner::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all BannerText
                    BannerText::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all FeaturedSection
                    $section = new FeaturedSection;
                    $sections = $section->findAllByAttributes(array('studio_id' => $studio_id), array('order' => 'id_seq ASC'));
                    if (count($sections) > 0) {
                        foreach ($sections as $section) {
                            FeaturedContent::model()->deleteAll('studio_id = :studio_id AND section_id=:section_id', array(
                                ':studio_id' => $studio_id, ':section_id' => $section->id
                            ));
                        }
                    }

                    //Delete all sdk users
                    SdkUser::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete card information of studio
                    CardInfos::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete CardErrors
                    CardErrors::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete StudioCancelReason
                    StudioCancelReason::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all studio content
                    StudioContentType::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all studio seo content
                    SeoInfo::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete all studio seo content
                    FreeMonth::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    //Delete Physical cart table
                    PGCart::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    //Delete Physical cart log table 
                    PGCartLog::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    //Delete Physical Multicurrency  table 
                    PGMultiCurrency::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //delete from pg_order_details based on order id of studios
                    $sql_orderdetails = "SELECT GROUP_CONCAT(id) AS ids FROM pg_order where studio_id=" . $studio_id;
                    $order_ids = $dbcon->createCommand($sql_orderdetails)->queryAll();
                    if ($order_ids && $order_ids[0]['ids']) {
                        $delete_order_details = "DELETE FROM pg_order_details WHERE order_id IN(" . @$order_ids[0]['ids'] . ")";
                        $dbcon->createCommand($sql_orderdetails)->execute();
                    }
                    //Delete Physical order  table 
                    PGOrder::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical order  status log table 
                    PGOrderStatusLog::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical Preorder Category  table 
                    PGPreorderCategory::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical product  table 
                    PGProduct::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    //Delete Physical saved address  table 
                    PGSavedAddress::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical settings  table 
                    PGSettings::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical shipping cost  table 
                    PGShippinCost::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical shipping address cost  table 
                    PGShippingAddress::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical Video  table 
                    PGVideo::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    //Delete Physical customer source table  
                    $sql_pg_cust_source = "DELETE from pg_customer_source where studio_id=" . $studio_id;
                    $dbcon->createCommand($sql_pg_cust_source)->execute();

                    //Delete Physical custom metadata table            
                    $sql_pg_cust_meta = "DELETE FROM pg_custom_metadata where studio_id=" . $studio_id;
                    $dbcon->createCommand($sql_pg_cust_meta)->execute();

                    //Delete all studio content
                    $studio = Studios::model()->findByPk($studio_id);



                    $filmsSql = "SELECT GROUP_CONCAT(id) as ids FROM films WHERE studio_id = " . $studio_id;
                    $fdata = $dbcon->createCommand($filmsSql)->queryAll();
                    if ($fdata && $fdata[0]['ids']) {
                        $delPosterSql = "DELETE FROM posters WHERE object_id IN(" . $fdata[0]['ids'] . ") AND object_type='films'";
                        $dbcon->createCommand($delPosterSql)->execute();

                        $delTrailer = "DELETE FROM movie_trailer WHERE movie_id IN(" . $fdata[0]['ids'] . ")";
                        $dbcon->createCommand($delTrailer)->execute();
                    }

                    movieStreams::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    Film::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    Menu::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    UrlRouting::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    ContentCategories::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    MenuItem::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));

                    StudioVisitor::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    StudioLoginHistory::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    Visitorlog::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    SearchLog::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    LoginHistory::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    VideoLogs::model()->deleteAll('studio_id = :studio_id', array(
                        ':studio_id' => $studio_id
                    ));
                    //Delete folder from theme and theme backup
                    $theme = $studio->theme;
                    if (trim($theme)) {
                        $preview_theme = $theme . '_preview';
                        $theme_path = ROOT_DIR . 'themes/' . $theme;
                        $preview_theme_path = ROOT_DIR . 'themes/' . $preview_theme;
                        if (file_exists($theme_path)) {
                            Yii::app()->common->rrmdir($theme_path);
                        }

                        if (file_exists($preview_theme_path)) {
                            Yii::app()->common->rrmdir($preview_theme_path);
                        }
                    }

                    //Deactivate studio user and delete all users like admin and members
                    $sql_usr = "DELETE FROM user WHERE studio_id = " . $studio_id;
                    $dbcon->createCommand($sql_usr)->execute();

                    Studios::model()->delete($studio_id);
                }
            }
        }
    }

    public function actionLoginForAll() {
        if (isset($_REQUEST['email']) && $_REQUEST['email'] != '' && isset($_REQUEST['studio_id']) && $_REQUEST['studio_id'] > 0) {
            $email = $_REQUEST['email'];
            $studio_id = $_REQUEST['studio_id'];
            $_SESSION["login_studio_id"] = $studio_id;
            $type = $_REQUEST['type']; // 1 for partner , blank studio
            $user = User::model()->findAllByAttributes(array('email' => $email, 'studio_id' => $studio_id));
            if ($user) {
                $model = new LoginForm;
                $model->attributes = array('email' => $email, 'password' => $user[0]->encrypted_password, 'rememberMe' => 0, 'studio_id' => $studio_id);
                if ($type == 1) {
                    $flag = $model->dopartnerlogin();
                } else {
                    $flag = $model->dostudiologin();
                }
                if ($flag) {
                    $this->redirect($this->createUrl('admin/dashboard'));
                } else {
                    echo "Invalid email or password";
                }
            } else {
                echo "No user found";
            }
        } else {
            echo "Invalid login";
        }
    }

    public function actiondownloadtoCds() {
        $pgorder = new PGOrder();
        $orders = $pgorder->findAll(array('condition' => 'cds_order_status=:cds_order_status AND studio_id=:studio_id AND id > 408 AND id NOT IN (434,447,454,474,480) LIMIT 10', 'params' => array(':cds_order_status' => '', ':studio_id' => 3063)));
        if ($orders) {
            foreach ($orders AS $ord) {
                $pgorder->CreateCDSOrder(3063, $ord['id']);
            }
        }
    }

    public function actionAddPhysicalmuvikart() {
        /* $studio_id = 3062;
          $solrobj = new SolrFunctions();
          $solrobj->deleteSolrQuery("cat:muvikart AND sku:" . $studio_id);

          $command = Yii::app()->db->createCommand("SELECT * FROM `pg_product` WHERE is_deleted=0 AND status != 2 AND studio_id={$studio_id}");
          $data1 = $command->queryAll();
          $solrobj1 = new SolrFunctions();
          foreach ($data1 as $k => $v) {
          if (!empty($v['genre'])) {
          $genre_array = json_decode($v['genre']);
          foreach ($genre_array as $key => $val) {
          $uniq_id = Yii::app()->common->generateUniqNumber();
          $solrArr['uniq_id'] = $uniq_id;
          $solrArr['content_id'] = $v['id'];
          $solrArr['stream_id'] = '';
          $solrArr['stream_uniq_id'] = $uniq_id;
          $solrArr['is_episode'] = 0;
          $solrArr['name'] = $v['name'];
          $solrArr['permalink'] = $v['permalink'];
          $solrArr['studio_id'] = $studio_id;
          $solrArr['display_name'] = 'muvikart';
          $solrArr['content_permalink'] = $v['permalink'];
          $solrArr['product_sku'] = $v['sku'];
          $solrArr['product_format'] = $v['custom_fields'];
          $solrArr['genre_val'] = $val;

          $solrobj1->addSolrData($solrArr);
          }
          } else {
          $uniq_id = Yii::app()->common->generateUniqNumber();
          $solrArr['uniq_id'] = $uniq_id;
          $solrArr['content_id'] = $v['id'];
          $solrArr['stream_id'] = '';
          $solrArr['stream_uniq_id'] = $uniq_id;
          $solrArr['is_episode'] = 0;
          $solrArr['name'] = $v['name'];
          $solrArr['permalink'] = $v['permalink'];
          $solrArr['studio_id'] = $studio_id;
          $solrArr['display_name'] = 'muvikart';
          $solrArr['content_permalink'] = $v['permalink'];
          $solrArr['product_sku'] = $v['sku'];
          $solrArr['product_format'] = $v['custom_fields'];
          $solrArr['genre_val'] = '';

          $ret = $solrobj1->addSolrData($solrArr);
          }
          }
          echo "success";
          exit; */
    }

    public function actionImportLumiere() {
        $studio_id = Yii::app()->user->studio_id;

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/content_excel';
        if (!is_dir($dir)) {
            mkdir($dir);
            @chmod($dir, 0777);
        }
        $filename = "New_Content_Lumiere.xlsx";
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/content_excel/' . $filename;
        //this is only for multipart child type
        $error_exists = $this->importExcelLumiere($path);

        unlink($path);
        if (is_numeric($error_exists)) {
            Yii::app()->user->setFlash('success', $error_exists . ' rows uploaded successfully');
        }

        echo $error_exists;
    }

    public function importExcelLumiere($path) {
        require 'PHPExcel/PHPExcel/IOFactory.php';
        $studio_id = Yii::app()->user->studio_id;
        //read excel data
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($path);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($path, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        // check whether the type is exists or not, if not exists insert into celebriety type.?
        $celebtity_type = array('Director', 'cast1', 'cast2', 'cast3');
        foreach ($celebtity_type as $k => $v) {
            $duplicate = Yii::app()->db->createCommand("SELECT COUNT(*) AS CNT FROM celebrity_type WHERE celebtity_type='" . $v . "' AND studio_id=" . $studio_id)->queryROW();
            if ($duplicate['CNT'] == 0) {
                $celeb = new CelebrityType();
                $celeb->celebtity_type = $v;
                $celeb->date_created = gmdate('Y-m-d H:i:s');
                $celeb->studio_id = $studio_id;
                $celeb->save();
                $celib_id = $celeb->id;
            }
        }
        /*  steps
         *  insert parent content details
         *  for each rows get the parent content id and insert the episode details 
         *  for each parent content enter the cast crew details.
         */
        //count the records in a sheet    
        $arrayCount = count($allDataInSheet);
        $countnoempty = 0;
        $castcrew = array();
        $director = array();
        $creator = array();

        for ($i = 2; $i <= $arrayCount; $i++) {

            $countnoempty = $countnoempty + 1;

            //check whether the content exists or not ?
            $check_content = "select id from films where name='" . trim($allDataInSheet[$i][A]) . "' and studio_id=" . $studio_id;
            $content_exists = Yii::app()->db->createCommand($check_content)->queryAll();

            if (count($content_exists) == 0) {
                $test_id = $this->insertFilmsLumeire($allDataInSheet[$i]);
            } else {
                $test_id = $content_exists[0]['id'];
            }

            $qry_film = "select * from films where id=" . $test_id;
            $movie = Yii::app()->db->createCommand($qry_film)->queryAll();

            $res = $this->addExcelContentLumiere($movie[0], $allDataInSheet[$i]);

            //add content to cast_crew tables and map to movie_cast table
            $movie_id = $movie[0]['id'];
            $director[$movie_id]['Director'][] = $allDataInSheet[$i]['Q'];

            $castcrew[$movie_id]['cast1'][] = $allDataInSheet[$i]['R'];
            $castcrew[$movie_id]['cast2'][] = $allDataInSheet[$i]['S'];
            $castcrew[$movie_id]['cast3'][] = $allDataInSheet[$i]['T'];
        }

        foreach ($castcrew as $key => $val) {
            foreach ($val as $k => $v) {
                $castcrew[$key][$k] = array_unique($v);
            }
        }

        foreach ($director as $key => $val) {
            foreach ($val as $k => $v) {
                $director[$key][$k] = array_unique($v);
            }
        }



        $this->addCastCrewcanalplus($director, $castcrew);
        $countempty = ($arrayCount - 1) - $countnoempty;
        //delete the uploaded file.
        return $countnoempty;
    }

    public function insertFilmsLumeire($allDataInSheet) {

        $Films = new Film();
        $permalinkins = $allDataInSheet['A'];
        $Films->studio_id = Yii::app()->user->studio_id;

        $uniqid = Yii::app()->common->generateUniqNumber();
        $Films->uniq_id = $uniqid;
        $Films->release_date = NULL;
        $Films->name = stripslashes($allDataInSheet['A']);
        $Films->story = stripslashes($allDataInSheet['G']);
        //$this->content_type_id = $data['content_type_id'];
        $Films->language = '';
        $Films->genre = json_encode(explode(',', $allDataInSheet['C']));
        $Films->censor_rating = json_encode(explode(',', $allDataInSheet['F']));

        $Films->created_date = gmdate('Y-m-d H:i:s');
        $Films->last_updated_date = gmdate('Y-m-d H:i:s');
        $Films->content_types_id = 3; //multipart 
        $Films->parent_content_type_id = 1; //VOD
        $Films->custom1 = $allDataInSheet['E'];
        $Films->custom2 = addslashes($allDataInSheet['D']);


        //if the category values are given in names
        //get the category id and map with the table and update the rows.
        //if not present then insert into table and update the new id.
        $categories = explode(",", $allDataInSheet['H']);
        $language_id = $this->language_id;
        foreach ($categories as $k => $category_name) {
            $chk_cat = "select * from content_category where category_name='" . trim($category_name) . "' and studio_id=" . Yii::app()->user->studio_id . " and language_id=" . $language_id;
            $cat_detail = Yii::app()->db->createCommand($chk_cat)->queryAll();

            if (isset($cat_detail[0]['id']) && $cat_detail[0]['id'] != '') { //if category does not exists
                $cat_id[] = $cat_detail[0]['binary_value'];
            } else { //if category does not exists
                $contentModel = new ContentCategories();
                $sql = " SELECT binary_value FROM content_category WHERE studio_id=" . Yii::app()->user->studio_id . " AND parent_id = 0 ORDER BY binary_value ASC";
                //$sql = 'SELECT MAX(binary_value) maxBvalue FROM content_category WHERE studio_id =' . Yii::app()->user->studio_id;
                $bvalue = Yii::app()->db->createCommand($sql)->queryAll();
                foreach ($bvalue AS $key => $val) {
                    if ($val['binary_value'] != pow(2, $key)) {
                        $binary_value = pow(2, $key);
                        break;
                    }
                }
                if (!@$binary_value) {
                    $binary_value = pow(2, 0 + 1);
                }
                $Menus = Menu::model()->find('studio_id=:studio_id AND position=\'top\'', array(':studio_id' => Yii::app()->user->studio_id));
                $ip_address = CHttpRequest::getUserHostAddress();
                $contentModel->studio_id = Yii::app()->user->studio_id;
                $permalink = Yii::app()->general->generatePermalink(trim($category_name));
                $contentModel->permalink = $permalink;
                $contentModel->binary_value = $binary_value;
                $contentModel->category_name = trim($category_name);
                $contentModel->created_date = gmdate('Y-m-d H:i:s');
                $contentModel->added_by = Yii::app()->user->id;
                $contentModel->ip = $ip_address;
                $contentModel->is_poster = 0;
                $contentModel->save();
                $cat_id[] = $binary_value;

                // Adding to Studio Content Types for menu uses
                $studioContentTypes = new StudioContentType();
                $studioContentTypes->display_name = Yii::app()->common->formatPermalink(trim($category_name));
                $studioContentTypes->content_category_value = $binary_value;
                $studioContentTypes->studio_id = Yii::app()->user->studio_id;
                $studioContentTypes->is_enabled = 1;
                $studioContentTypes->created_at = gmdate('Y-m-d H:i:s');
                $studioContentTypes->save();

                if (@$Menus->auto_categories == '1') {
                    //Adding to Menu Items table by default
                    $menuItems = new MenuItem();
                    $menuItems->title = trim($category_name);
                    $menuItems->menu_id = @$Menus->id;
                    $menuItems->permalink = $permalink;
                    $menuItems->value = $binary_value;
                    $menuItems->studio_id = Yii::app()->user->studio_id;
                    $menuitem->id_seq = Yii::app()->general->findMaxmenuorder();
                    $menuItems->save();

                    //Adding data to Url routing table
                    $urlRouting = new UrlRouting();
                    $urlRouting->permalink = $permalink;
                    $urlRouting->mapped_url = '/media/list/menu_item_id/' . $menuItems->id;
                    $urlRouting->studio_id = Yii::app()->user->studio_id;
                    $urlRouting->created_date = gmdate('Y-m-d H:i:s');
                    $urlRouting->ip = $ip_address;
                    $urlRouting->save();
                }
            }
        }
        $Films->content_category_value = array_sum($cat_id);
        $Films->permalink = Yii::app()->general->generatePermalink(stripslashes($permalinkins));
        $Films->status = 1;

        $Films->setIsNewRecord(true);
        $Films->setPrimaryKey(NULL);

        $Films->save();

        return $Films->id;
    }

    public function addExcelContentLumiere($movie, $allDataInSheet) {
        //Adding permalink to url routing 
        $urlRouts['permalink'] = $movie['permalink'];
        $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie['id'];
        $urlRoutObj = new UrlRouting();
        $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts);

        $movie_id = $movie['id'];
        $arr['uniq_id'] = $movie['uniq_id'];
        //Remove sample data from website
        $studio = $this->studio;
        $studio->show_sample_data = 0;

        $studio->save();
        //check whether parent record is there or not if not insert parent record else ignore the block
        $studio_id = Yii::app()->user->studio_id;
        $check_parent = "select * from movie_streams where studio_id=" . $studio_id . " and  movie_id=" . $movie['id'] . " and is_episode=0";
        $parent_epi = Yii::app()->db->createCommand($check_parent)->queryAll();
        if (count($parent_epi) == 0) {
            $MovieStreamparents = new movieStreams();
            $MovieStreamparents->studio_id = Yii::app()->user->studio_id;
            $MovieStreamparents->movie_id = $movie['id'];
            $MovieStreamparents->embed_id = Yii::app()->common->generateUniqNumber();
            $MovieStreamparents->is_episode = 0;
            $MovieStreamparents->created_date = gmdate('Y-m-d H:i:s');
            $MovieStreamparents->last_updated_date = gmdate('Y-m-d H:i:s');
            $publish_date = NULL;
            $MovieStreamparents->content_publish_date = $publish_date;
            $MovieStreamparents->save();
        }
        //end insert parent record
        //Insert Into Movie streams table
        $MovieStreams = new movieStreams();
        $MovieStreams->studio_id = Yii::app()->user->studio_id;
        $MovieStreams->movie_id = $movie['id'];
        $MovieStreams->embed_id = Yii::app()->common->generateUniqNumber();
        $PartnersContent->created_by = Yii::app()->user->id;
        $MovieStreams->is_episode = 1;
        //for multipart child content mapping

        $MovieStreams->episode_title = $allDataInSheet['M'];
        $MovieStreams->episode_story = $allDataInSheet['N'];
        $MovieStreams->episode_number = $allDataInSheet['L'];
        $MovieStreams->series_number = $allDataInSheet['J'];
        $MovieStreams->episode_date = NULL;
        $MovieStreams->custom1 = $allDataInSheet['B'];

        //end for multipart child content mapping
        $MovieStreams->created_date = gmdate('Y-m-d H:i:s');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        $publish_date = NULL;

        $MovieStreams->content_publish_date = $publish_date;

        $MovieStreams->save();

        //Check and Save new Tags into database
        $movieTags = new MovieTag();
        $data = array();
        $data['language'] = '';
        $data['genre'] = explode(',', $allDataInSheet['C']);
        $data['censor_rating'] = explode(',', $allDataInSheet['F']);

        $tag_ret = $movieTags->addTags($data);
        $studio_id = Yii::app()->user->studio_id;

        if (isset(Yii::app()->user->is_partner)) {
            $PartnersContent = new PartnersContent();
            $PartnersContent->studio_id = Yii::app()->user->studio_id;
            $PartnersContent->user_id = Yii::app()->user->id;
            $PartnersContent->movie_id = $movie['id'];
            $PartnersContent->save();
        }

        $arr = array();
        $arr['succ'] = 1;
        $arr['movie_id'] = $movie['id'];
        $arr['movie_stream_id'] = $MovieStreams->id;
        $arr['content_types_id'] = 3; //for Multipart child
        $arr['name'] = $allDataInSheet['A'];
        $arr['is_episode'] = $MovieStreams->is_episode;
        $arr['poster'] = POSTER_URL . '/no-image.png';


        if (HOST_IP != '127.0.0.1') {
            $solrObj = new SolrFunctions();
            foreach ($data['genre'] as $key => $val) {
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $movie['id'];
                $solrArr['stream_id'] = $arr['movie_stream_id'];
                $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                $solrArr['is_episode'] = $MovieStreams->is_episode;
                $solrArr['name'] = $allDataInSheet['A'];
                $solrArr['permalink'] = $movie['permalink'];
                $solrArr['studio_id'] = Yii::app()->user->studio_id;
                $solrArr['display_name'] = 'content';
                $solrArr['content_permalink'] = $movie['permalink'];
                $solrArr['genre_val'] = $val;
                $solrArr['product_format'] = '';
                $ret = $solrObj->addSolrData($solrArr);
            }
        }
        $arr['poster'] = @$ret['original'];

        return $arr;
    }

    function addCastCrewcanalplus($director, $castcrew) {
        $studio_id = Yii::app()->user->studio_id;
        foreach ($director as $key => $val) {
            foreach ($val as $k1 => $v1) {

                foreach ($v1 as $k => $v) {

                    if ($v != '') {
                        $duplicate = Yii::app()->db->createCommand("SELECT * FROM celebrities WHERE name='" . addslashes(trim($v)) . "' AND studio_id=" . $studio_id)->queryAll();
                        if (count($duplicate) < 1) {
                            $celeb = new Celebrity();
                            $celeb->name = $v;
                            $celeb->studio_id = $studio_id;
                            $celeb->parent_id = 0;
                            $celeb->created_by = Yii::app()->user->id;
                            $celeb->ip = $_SERVER['REMOTE_ADDR'];
                            $celeb->created_date = date('Y-m-d H:i:s');
                            $celeb->language_id = $this->language_id;

                            $celeb->save();
                            $castid = $celeb->id;
                        } else {
                            $castid = $duplicate[0]['id'];
                        }


                        if ($castid != 0 && $k1 != '') {
                            $extdata = Yii::app()->db->createCommand("SELECT * FROM movie_casts WHERE movie_id=" . $key . " AND celebrity_id=" . $castid . " AND cast_type ='" . json_encode([$k1]) . "'")->queryAll();

                            if (count($extdata) == 0) {

                                $movie_cast = new MovieCast();
                                $movie_cast->movie_id = $key;
                                $movie_cast->celebrity_id = $castid;
                                $movie_cast->cast_type = json_encode([$k1]);
                                $movie_cast->cast_name = $v;
                                $movie_cast->ip = $_SERVER["REMOTE_ADDR"];
                                $movie_cast->created_by = Yii::app()->user->id;
                                $movie_cast->created_date = date('Y-m-d H:i:s');
                                $movie_cast->save();
                            }
                        }
                    }
                }
            }
        }


        foreach ($castcrew as $key => $val) {
            foreach ($val as $k1 => $v1) {

                foreach ($v1 as $k => $v) {

                    if ($v != '') {
                        $duplicate = Yii::app()->db->createCommand("SELECT * FROM celebrities WHERE name='" . addslashes(trim($v)) . "' AND studio_id=" . $studio_id)->queryAll();
                        if (count($duplicate) < 1) {
                            $celeb = new Celebrity();
                            $celeb->name = $v;
                            $celeb->studio_id = $studio_id;
                            $celeb->parent_id = 0;
                            $celeb->created_by = Yii::app()->user->id;
                            $celeb->ip = $_SERVER['REMOTE_ADDR'];
                            $celeb->created_date = date('Y-m-d H:i:s');
                            $celeb->language_id = $this->language_id;

                            $celeb->save();
                            $castid = $celeb->id;
                        } else {
                            $castid = $duplicate[0]['id'];
                        }


                        if ($castid != 0 && $k1 != '') {
                            $extdata = Yii::app()->db->createCommand("SELECT * FROM movie_casts WHERE movie_id=" . $key . " AND celebrity_id=" . $castid . " AND cast_type ='" . json_encode([$k1]) . "'")->queryAll();

                            if (count($extdata) == 0) {

                                $movie_cast = new MovieCast();
                                $movie_cast->movie_id = $key;
                                $movie_cast->celebrity_id = $castid;
                                $movie_cast->cast_type = json_encode([$k1]);
                                $movie_cast->cast_name = $v;
                                $movie_cast->ip = $_SERVER["REMOTE_ADDR"];
                                $movie_cast->created_by = Yii::app()->user->id;
                                $movie_cast->created_date = date('Y-m-d H:i:s');
                                $movie_cast->save();
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionUpdateCustomFromIDTOContent() {
        $sql = "select * from custom_metadata_form where studio_id!=0";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $k => $v) {
            if (($v['parent_content_type_id'] == 1) && ($v['content_type'] == 0) && ($v['is_child'] == 0)) {
                $update = "update films set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'] . " and parent_content_type_id=1 and content_types_id=1";
            } else if (($v['parent_content_type_id'] == 1) && ($v['content_type'] == 1) && ($v['is_child'] == 0)) {
                $update = "update films set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'] . " and parent_content_type_id=1 and content_types_id=3";
            } else if (($v['parent_content_type_id'] == 1) && ($v['content_type'] == 1) && ($v['is_child'] == 1)) {
                $update = "update movie_streams set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'];
            } else if (($v['parent_content_type_id'] == 2)) {
                $update = "update films set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'] . " and content_types_id=4";
            } else if (($v['parent_content_type_id'] == 3) && ($v['content_type'] == 0) && ($v['is_child'] == 0)) {
                $update = "update films set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'] . " and parent_content_type_id=3 and content_types_id=5";
            } else if (($v['parent_content_type_id'] == 3) && ($v['content_type'] == 1) && ($v['is_child'] == 0)) {
                $update = "update films set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'] . " and parent_content_type_id=3 and content_types_id=6";
            } else if (($v['parent_content_type_id'] == 3) && ($v['content_type'] == 1) && ($v['is_child'] == 1)) {
                //$update = "update movie_streams set custom_metadata_form_id = ".$v['id']." where studio_id=".$v['studio_id'];
            } else if (($v['parent_content_type_id'] == 5)) {
                $update = "update pg_product set custom_metadata_form_id = " . $v['id'] . " where studio_id=" . $v['studio_id'];
            }
            if ($update) {
                Yii::app()->db->createCommand($update)->execute();
            }
        }
    }

    public function actionUpdateCategoryBinToCommaStudiowise() {
        $command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('studios')
                ->where('is_deleted = 0');
        $data_all = $command->queryAll();
        $data = CHtml::listData($data_all, 'id', 'id');
        foreach ($data as $key => $value) {
            self::UpdateCategoryValue($value);
            self::UpdateURLroutingCategoryValue($value);
            self::UpdateMenuItems($value);
        }
        echo "update succefully";
    }
    function UpdateCategoryValue($studio_id = 0) {
        /* Update films table content category value */
        if ($studio_id) {
            $command = Yii::app()->db->createCommand()
                    ->select('content_category_value,id,studio_id')
                    ->from('films')
                    ->where('studio_id = ' . $studio_id)
                    ->group('content_category_value');
            $data_all = $command->queryAll();
        }
        if (!empty($data_all)) {
            $data = CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id)), 'id', 'binary_value');
            if (!empty($data)) {
                $datav = array_values($data);
                $datak = array_keys($data);
                foreach ($data_all as $key => $value) {
                    foreach ($datav as $k => $v) {
                        if ($v < pow(2, 31)) {
                            if ((int) $v & (int) $value['content_category_value']) {
                                $f[$value['content_category_value']][] = $datak[$k];
                            }
                        }
                    }
                }
                if (!empty($f)) {
                    foreach ($f as $key => $value) {
                        $attr = array('content_category_value' => implode(',', $value));
                        $condition = "studio_id =:studio_id AND content_category_value =:content_category_value";
                        $params = array(':studio_id' => $studio_id, ':content_category_value' => $key);
                        Film::model()->updateAll($attr, $condition, $params);
                        PGProduct::model()->updateAll($attr, $condition, $params);
                    }
                }
            }
        }
    }
    function UpdateURLroutingCategoryValue($studio_id = 0) {
        if ($studio_id) {
            $sql = "SELECT * FROM url_routing WHERE studio_id=" . $studio_id . " AND `mapped_url` LIKE '%category%'";
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            $category = array_flip(CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id)), 'id', 'binary_value'));
            $subcategory = array_flip(CHtml::listData(ContentSubcategory::model()->findAllByAttributes(array('studio_id' => $studio_id)), 'id', 'subcat_binary_value'));
            foreach ($data as $key => $value) {
                preg_match_all('/\d+/', $value['mapped_url'], $matches);
                if ($category[$matches[0][0]] && $subcategory[$matches[0][1]]) {
                    $newmappedurl = preg_replace('/category\/(\d+)\/subcategory\/(\d+)/', "category/" . $category[$matches[0][0]] . "/subcategory/" . $subcategory[$matches[0][1]], $value['mapped_url']);
                    $update = "UPDATE url_routing SET mapped_url = '" . $newmappedurl . "' WHERE studio_id=" . $studio_id . " AND id=" . $value['id'];
                    Yii::app()->db->createCommand($update)->execute();
                }
            }
        }
    }
    function UpdateMenuItems($studio_id = 0) {
        if ($studio_id) {
            $sql = "SELECT * FROM menu_items WHERE studio_id=" . $studio_id . " AND link_type='0'"; //only category
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            $category = array_flip(CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id)), 'id', 'binary_value'));
            foreach ($data as $key => $value) {
                if ($category[$value['value']]) {
					$update = "UPDATE menu_items SET value = '".$category[$value['value']]."' WHERE studio_id=".$studio_id." AND id=".$value['id']." AND link_type='0'";
                    Yii::app()->db->createCommand($update)->execute();
                }
            }
        }
    }
	function UpdateAppMenuItems($studio_id = 0) {
        if ($studio_id) {
            $sql = "SELECT * FROM app_menu_items WHERE studio_id=" . $studio_id . " AND link_type='0'"; //only category
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            $category = array_flip(CHtml::listData(ContentCategories::model()->findAllByAttributes(array('studio_id' => $studio_id)), 'id', 'binary_value'));
            foreach ($data as $key => $value) {
                if ($category[$value['value']]) {
					$update = "UPDATE app_menu_items SET value = '".$category[$value['value']]."' WHERE studio_id=".$studio_id." AND id=".$value['id']." AND link_type='0'";
                    Yii::app()->db->createCommand($update)->execute();
                }
            }
        }
    }
	public function actionAppMymenuUpdate(){
		$command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('studios')
                ->where('is_deleted = 0 AND id NOT IN (3143,6169)');
		$data_all = $command->queryAll();
		$data = CHtml::listData($data_all, 'id', 'id');		
		foreach ($data as $key => $value) {
			self::UpdateAppMenuItems($value);
		}
		echo "update succefully . thank you";
	}
	public function actionMymenuUpdate(){
		$command = Yii::app()->db->createCommand()
                ->select('id')
                ->from('studios')
                ->where('is_deleted = 0');
		$data_all = $command->queryAll();
		$data = CHtml::listData($data_all, 'id', 'id');		
		foreach ($data as $key => $value) {
			self::UpdateMenuItems($value);
		}
		echo "update succefully";
	}
    public function actiondevsuccesipp() {
        print "Success <br><br><pre>";
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            print_r($_REQUEST);
        }

        print_r($_SERVER);
        //print_r($_SESSION);exit;

        echo '<script type="text/javascript">';
        echo 'window.top.location.href = "' . Yii::app()->getbaseUrl(true) . '";';
        echo '</script>';

        /* Yii::app()->theme = 'bootstrap';
          $this->layout = 'blank';
          $this->render('ipp_success'); */
    }

    public function actiondevsuccesserveripp() {
        $logfile = dirname(__FILE__) . '/ipp.txt';
        $msg = "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
        $data_log = serialize($_REQUEST);
        $msg.= $data_log . "\n";
        $msg = "\n----------array --\n";
        $msg.= print_r($_REQUEST, true) . "\n";
        $msg.= $_REQUEST . "\n";
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
    }

    public function actiontransactionprocessing() {
        $soapClient = new SoapClient("https://demo.ippayments.com.au/interface/api/dts.asmx?WSDL", array("trace" => 1));

        $request = <<<XML
                <Transaction>
                    <CustomerStorageNumber>QFX_HO</CustomerStorageNumber> 
                        <CustRef>123456</CustRef> 
                        <Amount>5500</Amount> 
                        <TrnType>2</TrnType> 
                        <CreditCard > 
                            <TokeniseAlgorithmID>2</TokeniseAlgorithmID> 
                            <CardNumber>9222896035903198</CardNumber> 
                        </CreditCard> 
                        <Security> 
                            <UserName>api.user.qflix1</UserName> 
                            <Password>zxcv1234</Password> 
                        </Security> 
                    <UserDefined></UserDefined>
                </Transaction>
XML;


        try {
            $result = $soapClient->__soapCall('SubmitSinglePayment', array(
                new SoapVar($request, XSD_ANYXML)
            ));
            echo '<pre>', htmlentities($result), '</pre>';
            print "<pre>";
            print_r($result);
            exit;
        } catch (SoapFault $sf) {
            $err = $sf->getMessage();
            echo "<pre>";
            print_r($err);
            exit;
        }
    }

    public function actionUpdateCustom15() {
        /* $sql = 'select id,custom6 from movie_streams where studio_id=3755 and is_episode=1 and custom6 like "%custom15%"';
          $data = Yii::app()->db->createCommand($sql)->queryAll();
          foreach ($data as $k=>$v){
          $episode = json_decode($v['custom6'],true);
          foreach ($episode as $key => $value) {
          foreach ($value as $key1 => $value1) {
          if($key1=='custom15'){
          Yii::app()->db->createCommand("UPDATE movie_streams SET series_number=".$value1." WHERE studio_id=3755 AND id=".$v['id'])->execute();
          echo $v['id']."<br>";
          }
          }
          }
          } */
    }

    /*
     * @author: RK<developer@muvi.com>
     * @description: Copy all new files and folders from theme_updates to all templates
     */
    public function actionUpdateTemplate(){
        $this->layout = false;  
        $message = '';
        $criteria = new CDbCriteria;
        $criteria->addCondition("date_added > NOW() AND status = 0");
        $updates = TemplateUpdate::model()->findAll($criteria);            
        foreach($updates as $update){                
            $cond = 't.parent_theme = "'.$update->template_code.'" AND ';
            $cond .= "t.theme != '' AND t.is_deleted = 0 AND t.status = 1";
            if($update->studio_id > 0)
                $cond .= ' AND t.id = '.$update->studio_id;
            $criteria = new CDbCriteria;
            $criteria->select = 'parent_theme, theme, id, domain'; // select fields which you want in output
            $criteria->condition = $cond;
            $criteria->order = 'id asc';
            $studios = Studios::model()->findAll($criteria);       
            echo 'Total Updated:'.count($studios).'<br />';
            foreach($studios as $studio){
                echo 'Studio:'.$studio->name.', theme:'.$studio->theme.', parent:'.$studio->parent_theme.'<br />';
                $themes[] = $studio->theme;
                $source = ROOT_DIR . '/theme_updates/' . $studio->parent_theme . '/';
                $sourcePath = 'themes/' . $studio->theme . '/';
                $ret = Yii::app()->common->recurse_copy($source, $sourcePath);  
                $message.= 'Theme '.$studio->theme.' updated.<br />';
            }
            $update->status = 1;
            $update->release_id = $update->release_id+1;
            $update->date_executed = new CDbExpression("NOW()");
            $update->save();
        }
        echo $message;
        exit;
    }
    function actionAddpublishdateToSolr() {
        //for physical get all product with publish_date IS NOT NULL
        $allproduct = Yii::app()->db->createCommand()
                ->select("*")
                ->from("pg_product")
                ->where("publish_date IS NOT NULL")
                ->queryAll();

        foreach ($allproduct as $key => $val) {
            //delete each from solr and add that one with publish date to solr 
           // if (HOST_IP != '127.0.0.1') {
                $solrobj = new SolrFunctions();
                $solrobj->deleteSolrQuery("cat:muvikart AND content_id:" . $val['id']);

                if ($val['genre'] != '') {
                    $val['genre'] = json_decode($val['genre']);
                    foreach ($val['genre'] as $key1 => $val1) {
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $val['id'];
                        $solrArr['stream_id'] = '';
                        $solrArr['stream_uniq_id'] = $val['uniqid'];
                        $solrArr['is_episode'] = 0;
                        $solrArr['name'] = $val['name'];
                        $solrArr['permalink'] = $val['permalink'];
                        $solrArr['studio_id'] = $val['studio_id'];
                        $solrArr['display_name'] = 'muvikart';
                        $solrArr['content_permalink'] = $val['permalink'];
                        $solrArr['product_sku'] = $val['sku'];
                        $solrArr['product_format'] = $val['custom_fields'];
                        $solrArr['genre_val'] = $val1;
                        $solrArr['publish_date'] = $val['content_publish_date'];
                        $solrObjct = new SolrFunctions();
                        $ret = $solrObjct->addSolrData($solrArr);
}
                } else {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] = $val['id'];
                    $solrArr['stream_id'] = '';
                    $solrArr['stream_uniq_id'] = $val['uniqid'];
                    $solrArr['is_episode'] = 0;
                    $solrArr['name'] = $val['name'];
                    $solrArr['permalink'] = $val['permalink'];
                    $solrArr['studio_id'] = $val['studio_id'];
                    $solrArr['display_name'] = 'muvikart';
                    $solrArr['content_permalink'] = $val['permalink'];
                    $solrArr['product_sku'] = $val['sku'];
                    $solrArr['product_format'] = $val['custom_fields'];
                    $solrArr['genre_val'] = '';
                    $solrArr['publish_date'] = $val['publish_date'];
                    $solrObjct = new SolrFunctions();
                    $ret = $solrObjct->addSolrData($solrArr);
                }
           // }
        }
        //for digital 
        //for digital get all content with publish_date IS NOT NULL
        $allcontent = Yii::app()->db->createCommand()
                ->select("*")
                ->from("movie_streams")
                ->where("content_publish_date IS NOT NULL")
                ->queryAll();

        $cond = 't.content_publish_date IS NOT NULL';
        $allcontent = movieStreams::model()->with(array('film'))->findAll(array('condition' => $cond));

        foreach ($allcontent as $val) {
            //$val->id;  //movie_stream_id
            // $val->film->id  //film id        
          //  if (HOST_IP != '127.0.0.1') {
                $solrobj = new SolrFunctions();
                $solrobj->deleteSolrQuery("cat:content AND sku:" . $val->studio_id . " AND content_id:" . $val->film->id . " AND stream_id:" . $val->id);
                $solrArr = array();
                $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                $solrArr['content_id'] = $val->film->id;
                $solrArr['stream_id'] = $val->id;
                $solrArr['stream_uniq_id'] = $val->embed_id;
                $solrArr['is_episode'] = $val->is_episode;
                $solrArr['name'] = $val->film->name;
                $solrArr['permalink'] = $val->film->permalink;
                $solrArr['studio_id'] = $val->studio_id;
                $solrArr['display_name'] = 'content';
                $solrArr['content_permalink'] = $val->film->permalink;
                $genre = json_decode($val->film->genre);
                $solrArr['genre_val'] = (count($genre) > 1) ? implode(',', $genre) : $genre[0];
                $solrArr['product_format'] = '';
                $solrArr['publish_date'] = $val->content_publish_date;
                $solrobjnew = new SolrFunctions();
                $ret = $solrobjnew->addSolrData($solrArr);
          //  }
        }
        exit;
    }
    protected function escapeSolrValueWithoutSpace($string){
        $match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';','/','%','#','$');
        $replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;','\\/','\\%','\\#','\\$');
        $string = str_replace($match, $replace, $string);
        return $string;
    }
   public function actionAddExistingEpisodesTosolr() {
          //$cat = $this->escapeSolrValueWithoutSpace('content - episode - Episode');
          //  $cat1 = $this->escapeSolrValueWithoutSpace('content - Episode');
            $solrobj = new SolrFunctions();
            $solrobj->deleteSolrQuery("is_episode:1");
            $solrArr = array();
    
      //get all the contents having the episodes    
        $allchildcontent = Yii::app()->db->createCommand()
                ->select("*")
                ->from("movie_streams")
                ->where("is_episode=1")
                ->queryAll();
        // echo "<pre>"; print_r($allchildcontent);exit; 
        foreach ($allchildcontent as $key => $val) {
           $getMovieStream = Yii::app()->db->createCommand('SELECT f.content_type_id,f.name,f.permalink,m.movie_id,m.full_movie,m.id,m.is_episode,m.episode_title,m.content_publish_date,f.content_types_id,m.original_file,m.converted_file  FROM movie_streams m,films f WHERE f.id=m.movie_id AND  m.movie_id=' .  $val['movie_id'] . ' AND m.studio_id=' .  $val['studio_id'] . ' AND m.id=' .  $val['id'])->queryAll();
            // if (HOST_IP != '127.0.0.1') {
                if ($val['is_episode']) {
                    $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                    $solrArr['content_id'] =  $val['movie_id'];
                    $solrArr['stream_id'] =  $val['id'];
                    $solrArr['stream_uniq_id'] =  $val['embed_id'];
                    $solrArr['is_episode'] =  $val['is_episode'];
                    $solrArr['name'] =  $val['episode_title'] ?  $val['episode_title'] : "Episode " .  $val['episode_number'];
                    $solrArr['permalink'] = $getMovieStream[0]['permalink'];
                    $solrArr['display_name'] = 'content';
                    $solrArr['studio_id'] =  $val['studio_id'];
                    $solrArr['content_permalink'] = $getMovieStream[0]['permalink'];
                    $solrArr['genre_val'] = '';
                    $solrArr['product_format'] = '';
                    if ( $val['content_publish_date'] != NULL)
                        $solrArr['publish_date'] =  $val['content_publish_date'];
                    $solrObj = new SolrFunctions();
                    $ret = $solrObj->addSolrData($solrArr);
                }
           // }
        }
        exit;
    }
    public function actionAdyenIdealPayment() {
        $logfile = dirname(__FILE__).'/paypal.txt';
        echo $logfile;
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $data_log = serialize($_REQUEST);
        $msg.= $data_log."\n";
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
    }
    function actionmigrate000(){
        $pgVideo = PGVideo::model()->findAll();
        $i = 0;
        foreach($pgVideo as $v){
            $i++;
            $pgTrailer = new PGMovieTrailer();
            $pgTrailer->trailer_file_name = $v->full_movie;
            $pgTrailer->trailer_content_type = NULL;
            $pgTrailer->movie_id = $v->product_id;
            $pgTrailer->video_remote_url = $v->full_movie_url;
            $pgTrailer->name = $v->original_file;
            $pgTrailer->ip = $_SERVER['REMOTE_ADDR'];
            $pgTrailer->created_by = $v->studio_id;
            $pgTrailer->created_date = $v->upload_start_time;
            $pgTrailer->last_updated_by = $v->studio_id;
            $pgTrailer->is_converted = $v->is_converted;
            $pgTrailer->has_sh = $v->has_sh;
            $pgTrailer->video_resolution = $v->video_resolution;
            $pgTrailer->resolution_size = $v->resolution_size;
            $pgTrailer->mail_sent_for_trailer = $v->mail_sent_for_video;
            $pgTrailer->upload_cancel_time = $v->upload_cancel_time;
            $pgTrailer->encoding_start_time = $v->encoding_start_time;
            $pgTrailer->encoding_end_time = $v->encoding_end_time;
            $pgTrailer->encode_fail_time = $v->encode_fail_time;
            $pgTrailer->video_duration = NULL;
            $pgTrailer->video_management_id = $v->video_management_id;
            $pgTrailer->upload_start_time = $v->upload_start_time;
            $pgTrailer->upload_end_time = $v->upload_end_time;
            $pgTrailer->third_party_url = $v->thirdparty_url;
            $pgTrailer->save();
            echo "Success - ".$i;
            echo "<br>";
        }
    }
    function actionreplaceYouTube(){
        $pgVideo = PGMovieTrailer::model()->findAll();
        $i = 0;
        foreach($pgVideo as $v){$i++;
            $url = $v->third_party_url;
            $rx = '~^(?:https?://)?(?:www[.])?(?:youtube[.]com/watch[?]v=|youtu[.]be/)([^&]{11})~x';
            $has_match = preg_match($rx, $url, $matches);
            if($has_match){
                echo $i.' '.$matches[1];
                $newUrl = '<iframe width="640" height="360" src="https://www.youtube.com/embed/'.$matches[1].'" frameborder="0" allowfullscreen></iframe>';
                
                $con = Yii::app()->db;
$sql_usr = "UPDATE pg_movie_trailer SET third_party_url='$newUrl' WHERE id=" . $v->id;
$con->createCommand($sql_usr)->execute();
                
                echo "<br>";
            }
        }  
    }
     public function actionfilmSensations() {
        include 'PHPExcel/Classes/PHPExcel.php';
        include 'PHPExcel/PHPExcel/IOFactory.php';
      $sql = "SELECT pg.id,pg.name,pg.description,pg.sale_price,cc.category_name,pgt.third_party_url,pg.units,pg.sku,pg.custom_fields,pg.created_date,pg.release_date,pg.genre FROM  pg_product pg LEFT JOIN content_category cc on pg.content_category_value=cc.id LEFT JOIN pg_movie_trailer pgt on pgt.movie_id=pg.id WHERE pg.studio_id=3057";
       $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        // Instantiate a new PHPExcel object 
        $objPHPExcel = new PHPExcel();  
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'TITLE');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'CATEGORY');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'GENRE');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'FORMAT');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, 'SKU');
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, 'TRAILER LINK');;
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, 'DESCRIPTIONS');
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, 'RELEASE DATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, 'PRICE(SEK(kr))');
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, 'PRICE(DKK(kr))');
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, 'PRICE(NOK(kr))');
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, 'PRICE(EUR(€))');
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, 'PRICE(INR(Rs))');
        $rowCount = 2; 
        for ($k = 0; $k < count($data); $k++) {           
           $sqlMulticurrency = "SELECT price,currency_id FROM  pg_multi_currency  where product_id=".$data[$k]['id'];
           $dataMultiCurrency = $dbcon->createCommand($sqlMulticurrency)->queryAll();
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $data[$k]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $data[$k]['category_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $data[$k]['genre']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $data[$k]['custom_fields']);
	    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $data[$k]['sku']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $data[$k]['third_party_url']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $data[$k]['description']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $data[$k]['release_date']);
            for($j=0;$j<count($dataMultiCurrency);$j++){
                if(!empty( $dataMultiCurrency)){
                if($dataMultiCurrency[$j]['currency_id']==131){ $SEK=$dataMultiCurrency[$j]['price'];}
                else if($dataMultiCurrency[$j]['currency_id']==41){ $DKK=$dataMultiCurrency[$j]['price']; }
                else if($dataMultiCurrency[$j]['currency_id']==110){
                    $NOK=$dataMultiCurrency[$j]['price'];
                }else if($dataMultiCurrency[$j]['currency_id']==47){
                    $EUR=$dataMultiCurrency[$j]['price'];
                }else if($dataMultiCurrency[$j]['currency_id']==68){
                    $INR=$dataMultiCurrency[$j]['price'];
                }
                }
           
            }
             $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $SEK );   
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $DKK);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $NOK);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $EUR);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $INR);
            $SEK=$DKK=$NOK=$EUR=$INR='';
            $rowCount++; 
}
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="Film_Sensation_Pgproduct_download.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
        exit;
    }
    //assign all content to ppv plan of the store yotestine go 
    public function actionppvBundlesyotestinego(){
        $sql = "select id from films WHERE studio_id=4751 and parent_id=0";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        //$idres = array(57003, 53307, 50348,43398);
        foreach($data as $value){
            //if(!in_array($value['id'], $idres)){
          $ticket_master_sql = "insert into `ppv_advance_content`(`ppv_plan_id`, `content_id`) values (" . 1407 . ",'" . $value['id'] . "')";
           Yii::app()->db->createCommand($ticket_master_sql)->execute();  
        //}
        }
        }
//news letter subscriber enable of all user of terminator store
 public function actionnewsletterSubscriptionsTerminator(){
     //$limit=$_REQUEST['limit'];
     //$offset=$_REQUEST['offset'];
        //$sql = "select email,id from sdk_users WHERE studio_id=3065  LIMIT " . $limit . "," . $offset;
        $sql = "select email,id from sdk_users WHERE studio_id=3065";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        //$idres = array(57003, 53307, 50348,43398);
        foreach($data as $value){
          $sqlnewsletterduplicate = "select count(id) as countid from newsletter_subscribers WHERE email='". $value['email'] ."' AND user_id=". $value['id'] ." AND studio_id=". 3065; 
          $datacount = $dbcon->createCommand($sqlnewsletterduplicate)->queryAll();
           if($datacount['countid']==0){
          $ticket_master_sql = "insert into `newsletter_subscribers`(`studio_id`,`email`,`user_id`,`date_subscribed`) values (" . 3065 . ",'" . $value['email'] . "','". $value['id'] ."',NOW())";
           Yii::app()->db->createCommand($ticket_master_sql)->execute();  
}
        }
}

	function actionCheckExpiry() {
        $studio_id = $_REQUEST['studio_id'];
        $user_id = $_REQUEST['user_id'];
        $embed_id = $_REQUEST['embed_id'];
        $return = Yii::app()->common->getContentExpiryDate($studio_id, $user_id, $embed_id);
        print '<pre>';print_r($return);exit;
    }
    public function actionDeleteRemovedEpisodesFromSolr() {
        //for newthought channel only ticket #10236
        require_once( $_SERVER["DOCUMENT_ROOT"] . '/SolrPhpClient/Apache/Solr/Service.php' );
        $studio_id = $_REQUEST['studio_id'];
        $solr = new Apache_Solr_Service('localhost', '8983', '/solr');

        if (!$solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }

        $cat = $this->escapeSolrValueWithoutSpace('content - Episode');
        $query = "cat: " . $cat . "* AND sku:" . $studio_id . " AND is_episode:1";
        $response = $solr->search($query);
        //echo "<pre>"; print_r($response->response->docs);exit;
        $solrobj = new SolrFunctions();
        if ($response->getHttpStatus() == 200) {
            if ($response->response->numFound > 0) {
                foreach ($response->response->docs as $doc) {
                    $ids = $doc->stream_id;
                    
                    $movieStreams = movieStreams::model()->findAllByAttributes(array('id' => $ids, 'studio_id' => $studio_id));
                    if (empty($movieStreams)) {
                        //delete those records from solr which not exists in movie stream table of corresponding studio                     
                        $query= "sku:" . $studio_id . " AND is_episode:1 AND stream_id:" . $ids;
                       
                        $solrobj->deleteSolrQuery($query);
                        
                        
                    }
                }
            }
        }
        exit;
    }

     public function actiondownloadLinkurlVideoLibrary() {
        include 'PHPExcel/Classes/PHPExcel.php';
        include 'PHPExcel/PHPExcel/IOFactory.php';
        $sql = "SELECT video_name,video_remote_url from video_management where studio_id=3396 and flag_uploaded=0";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        // Instantiate a new PHPExcel object 
        $objPHPExcel = new PHPExcel();  
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'MOVIE NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'DOWNLOADABLE LINK');
        $rowCount = 2; 
        for ($k = 0; $k < count($data); $k++) {  
             $base_cloud_url = 'http://d7dpyx3dr0fak.cloudfront.net/videogallery/'.utf8_decode($data[$k]['video_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, utf8_decode($data[$k]['video_name']));
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $base_cloud_url);
            $rowCount++; 
}
        ob_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
        header('Content-Disposition: attachment;filename="Dalton_downlodable_link.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
        exit;
    }
     public function actionexportContentlibrary() {
        include 'PHPExcel/Classes/PHPExcel.php';
        include 'PHPExcel/PHPExcel/IOFactory.php';
        $studio_id=$_REQUEST['studio_id'];
        $sql = "SELECT f.id,f.name,f.content_types_id,f.story,f.genre,f.censor_rating,cc.category_name,f.created_date,f.release_date,f.language_id,ms.series_number,ms.episode_number,f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10 FROM  films f LEFT JOIN content_category cc on f.content_category_value=cc.id left join movie_streams ms on f.id=ms.movie_id WHERE f.studio_id=".$studio_id;
       $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        // Instantiate a new PHPExcel object 
        $objPHPExcel = new PHPExcel();  
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $rowCount = 1;
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'CONTENT NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'CONTENT TYPES');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'GENRE');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'CONTENT CATEGORY');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, 'RELEASE DATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, 'LANGUAGE');;
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, 'DESCRIPTIONS');
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, 'CENSOR RATING');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, 'SEASON#)');
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, 'EPISODE#');
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, 'CUSTOM METADATA FORM');
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, 'custom1');
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, 'custom2');
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, 'custom3');
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, 'custom4');
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, 'custom5');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, 'custom6');
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, 'custom7');
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, 'custom8');
        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, 'custom9');
        $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, 'custom10');
    
        $rowCount = 2; 
        for ($k = 0; $k < count($data); $k++) {           
           $sqllanguages = "SELECT name FROM  languages  where id=".$data[$k]['id'];
           $csid = CustomMetadataForm::model()->find("studio_id = :studio_id AND parent_content_type_id=1  AND name NOT LIKE '%UGC%'",array('studio_id' => $studio_id));
	  			if(empty($csid)){
					$csid = CustomMetadataForm::model()->find("studio_id = 0 AND parent_content_type_id=1 AND name NOT LIKE '%UGC%'");
}
           $datalanguages = $dbcon->createCommand($sqllanguages)->queryAll();
           if($data[$k]['content_types_id']==1){
              $contentType='Single Part' ;
           }
           else if($data[$k]['content_types_id']==3){
              $contentType='Multi Part' ; 
           }
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, utf8_decode($data[$k]['name']));
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $contentType);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, utf8_decode($data[$k]['genre']));
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, utf8_decode($data[$k]['category_name']));
	    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $data[$k]['release_date']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, utf8_decode($datalanguages[$k]['name']));
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, utf8_decode($data[$k]['story']));
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, utf8_decode($data[$k]['censor_rating']));
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $data[$k]['series_number']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $data[$k]['episode_number']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $csid->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, utf8_decode($data[$k]['custom1']));
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, utf8_decode($data[$k]['custom2']));
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, utf8_decode($data[$k]['custom3']));
            $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, utf8_decode($data[$k]['custom4']));
            $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, utf8_decode($data[$k]['custom5']));
            $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, utf8_decode($data[$k]['custom6']));
            $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, utf8_decode($data[$k]['custom7']));
            $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, utf8_decode($data[$k]['custom8']));
            $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, utf8_decode($data[$k]['custom9']));
            $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, utf8_decode($data[$k]['custom10']));
            $rowCount++; 
}
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="flixmojo_allcontent_download.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        // ob_clean();
        //$objWriter->save('php://output');
        $objWriter->save("xml/flixmojo_allcontent_download.xls");
        exit;
    }
    
    function  actioncmaxEmbedidUpdate(){
     $sql = "select id,embed_id from  movie_streams  where studio_id=3457 and embed_id is NULL;";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();   
        foreach($data as $val){
        $embedidd=Yii::app()->common->generateUniqNumber();    
        Yii::app()->db->createCommand("UPDATE movie_streams SET embed_id='".$embedidd."' WHERE studio_id=3457 and embed_id is NULL")->execute();
}
    }
    
        public function actiongenerateXmlDoc() {
        include 'PHPExcel/Classes/PHPExcel.php';
        include 'PHPExcel/PHPExcel/IOFactory.php';
        $studio_id = $_REQUEST['studio_id'];
        $all_encoded_url = array();
        $sql = "SELECT f.id as movie_id,f.name ,f.content_types_id as contenttype,f.story ,f.genre ,f.censor_rating ,cc.category_name,f.created_date ,f.release_date ,ms.id as stream_id,ms.full_movie,ms.is_episode,ms.episode_title,ms.episode_story,ms.episode_date , ms.series_number ,ms.episode_number FROM  films f LEFT JOIN content_category cc on f.content_category_value=cc.id left join movie_streams ms on f.id=ms.movie_id WHERE f.studio_id=" . $studio_id . " order by movie_id desc";
        $dbcon = Yii::app()->db;
        $data = $dbcon->createCommand($sql)->queryAll();
        $client = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
        $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);
        $bucketName = $bucketInfo['bucket_name'];

//           $response = $client->getListObjectsIterator(array(
//                        'Bucket' => 'stagingstudio',
//                        'Prefix' => '16/EncodedVideo/uploads/movie_stream/full_movie/4782/'
//                    ));       

        foreach ($data as $key => $val) {
            if ($val['contenttype'] == 3 && $val['is_episode'] == 1) {
                $data[$key]['poster_url'] = $this->getPoster($val['stream_id'], 'moviestream', 'episode');
            } else {
               $data[$key]['poster_url'] = $this->getPoster($val['movie_id'], 'films');
            }
        }       
        $objPHPExcel = new PHPExcel();        
        $objPHPExcel->setActiveSheetIndex(0);     
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Contenttype');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Story');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Genre');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Censor_rating');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'category_name');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Created_date');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Release_date');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'stream_id');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Full_movie');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Is_episode');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Episode_title');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Episode_story');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Episode_date');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Series_number');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Episode_number');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Posterimage');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Video Url');
        
        $rowCount = 2;
        $cloud_url = 'http://ddcy8vycn4mnt.cloudfront.net/';

        foreach ($data as $k => $val) {
            $key = $studio_id . '/EncodedVideo/uploads/movie_stream/full_movie/' . $val['stream_id'] . '/';
            $fileinfo = pathinfo($key . $val['full_movie']);
            $findme = $fileinfo['filename'] . "_original";
            $response = $client->getListObjectsIterator(array(
                'Bucket' => $bucketName, //'stagingstudio',
                'Prefix' => $key //'11/EncodedVideo/uploads/movie_stream/full_movie/105/'
            ));

            foreach ($response as $object) {
                $all_encoded_url[] = $object['Key'];
            }
            foreach ($all_encoded_url as $ke => $urls) {
                if (strpos($urls, $findme)) {
                    $video_original = $urls;
                }
            }
            $original_fileinfo = pathinfo($video_original);
            $original_filename=$original_fileinfo['basename'];
            if ($val['contenttype'] == 1) {
                $val['contenttype'] = 'Single part';
            } else if ($val['contenttype'] == 3) {
                if ($val['is_episode'] == 1) {
                    $val['contenttype'] = 'Multipart Child';
                    
                } else
                    $val['contenttype'] = 'Multipart Parent';
            }
            else if ($val['contenttype'] == 4) {
                $val['contenttype'] = 'Live Stream';
            }           
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, utf8_decode($val['name']));
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $val['contenttype']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, utf8_decode($val['story']));
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, utf8_decode($val['genre']));
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, utf8_decode($val['censor_rating']));
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, utf8_decode($val['category_name']));
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $val['created_date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $val['release_date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $val['stream_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $val['full_movie']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $val['is_episode']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, utf8_decode($val['episode_title']));
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, utf8_decode($val['episode_story']));
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $val['episode_date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $val['series_number']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $val['episode_number']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $val['poster_url']);
            $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount,  $cloud_url.$data[$k]['stream_id']."/".$original_filename); 
        $rowCount++;
        }
              
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');       
     //   header('Content-Disposition: attachment;filename="allcontent.xls"'); 
        header('Cache-Control: max-age=0'); 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save("xml/newthoughtcontent.xls");
        //ob_clean();
        //$objWriter->save('php://output');
       exit;
    }
}
