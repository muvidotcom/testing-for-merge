<?php
class SeasondataController extends Controller{
    public function init() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        parent::init();
    }
    public function actionGetSeasonData(){
        $studio_id = $this->studio->id;
        $season = (isset($_REQUEST['series_no']) && is_numeric($_REQUEST['series_no'])) ? $_REQUEST['series_no'] : 0;
        if($_REQUEST['contentPlink'] !=""){
            $page_size = $limit = Yii::app()->general->itemsPerPage();
            if(isset($_REQUEST['limit']))
                $page_size = $limit = $_REQUEST['limit'];
			$offset = 0;
			if (isset($_REQUEST['page'])) {
				$offset = ($_REQUEST['page'] - 1) * $limit;
			} else if (isset($_REQUEST['p'])) {
				$offset = ($_REQUEST['p'] - 1) * $limit;
				$page_number = $_REQUEST['p'];
			} else {
				$page_number = 1;
			}
            $request_movie_id = Yii::app()->db->createCommand()->SELECT('id,name')->FROM('films')->WHERE('permalink=:permalink',array(':permalink'=>$_REQUEST['contentPlink']))->queryRow();
            if($request_movie_id){
                $req_movie_id = $request_movie_id['id'];
                $cond = 'movie_id=:movie_id AND series_number=:series_number AND is_episode=:is_episode AND is_converted=:is_converted AND episode_parent_id=0 AND ((content_publish_date IS NULL) OR  (UNIX_TIMESTAMP(content_publish_date) = 0) OR (content_publish_date <=NOW()))';
                $params = array(':movie_id' => $req_movie_id, ':is_episode' => 1, ':is_converted' => 1, ':series_number' => $season);
                $streams = movieStreams::model()->findAll(array('condition' => $cond, 'params' => $params, 'offset' => $offset, 'limit' => $limit));
                $total = movieStreams::model()->count(array('condition' => $cond, 'params' => $params));
                $kvs = array();
                if (count($streams) > 0) {
                    foreach ($streams as $data) {
                        $kvs[] = Yii::app()->general->getContentData($data['id'], 1);
                    }
                } 
            }
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $page_url = Yii::app()->general->full_url($url, '?page=');
            $page_url = Yii::app()->general->full_url($page_url, '&page=');
            $pg = new bootPagination();
            $pg->pagenumber = $_REQUEST['page'];
            $pg->pagesize = $page_size;
            $pg->totalrecords = $total;
            $pg->showfirst = true;
            $pg->showlast = true;
            $pg->paginationcss = "pagination-normal";
            $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
            $pg->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1)
                $pg->paginationUrl = $page_url . "&page=[p]";
            else
                $pg->paginationUrl = $page_url . "?page=[p]";
            $pagination = $pg->process();   
            $this->render('index', array('contents' => json_encode($kvs), 'pagination' => $pagination, 'season' => $season, 'parent_name' => $request_movie_id['name'], 'page_size' => $page_size, 'total' => $total));
        }else{
            $redirect_url = Yii::app()->getBaseUrl(true);         
            Yii::app()->user->setFlash('error', 'Oops! wrong url');
            $this->redirect($redirect_url);
            exit(); 
        }
    }
}