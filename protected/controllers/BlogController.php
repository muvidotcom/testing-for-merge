<?php
class BlogController extends Controller {
    public function init() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        parent::init();
    }
    public $defaultAction = 'index';
    public $layout = 'main';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        if (!($this->has_blog)) {
            Yii::app()->user->setFlash('error', $this->ServerMessage['oops_you_have_no_access']);
            $this->redirect($redirect_url);
            exit();
        }
        return true;
    }

    public function actionIndex() {
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $language_id = $this->language_id;
        $BusinessName = $studio->name;
		$content = Yii::app()->general->content_count($studio_id);
		if ((isset($content) && ($content['content_count'] & 4))){
			if ($_SERVER['HTTP_X_PJAX'] == true) {
				$this->layout = false;
			}
		}

        $permalink = str_replace("/",'',$_SERVER['REQUEST_URI']);
        $BusinessTitle = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id,'permalink'=>$permalink), array('select' => 'title'));
        if(!empty($BusinessTitle)):
            $title = utf8_decode($BusinessTitle->title);
        else:
            $title = $this->Language['head_blog'];
        endif;  
        $description = $BusinessName . ' blog';
        $keywords = '';

        $this->pageTitle = $title . ' | ' . $BusinessName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
		if($_SERVER['HTTP_X_PJAX'] == true){
			echo '<title>'.$title . ' | ' . $BusinessName.'</title>';
		}
        $sortby = (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != '') ? $_REQUEST['sortby'] : 'created_date';
        $order = (isset($_REQUEST['order']) && $_REQUEST['order'] != '') ? $_REQUEST['order'] : 'desc';
        $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 6;
        $mobile_url = "";
        if(isset($_REQUEST['mobileview'])){
            $this->is_mobileview =true;
            $mobile_url = "/mobile_view";
        }
        //Find All Blog Post       
        $data = BlogPost::model()->findAllByAttributes(
                array(
                    'studio_id' => $studio_id,
                    'has_published' => 1,
                )
        );
        $count = count($data);

        //Pagination Implimented ..
        $page_size = $page_size;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }

        $data = BlogPost::model()->findAllByAttributes(
                array(
            'studio_id' => $studio_id,
            'has_published' => '1',
            'parent_id' => 0
                ), array(
            'order' => $sortby . ' ' . $order,
            'limit' => $page_size,
            'offset' => $offset
                )
        );
        $translateposts = array();
        if(isset($data) && !empty($data)){
        if($this->language_id != 20){
            $translateposts = BlogPost::model()->getTranslatedPost($studio_id, $this->language_id, $data);
        }
        $blog_data = array();
        foreach ($data as $post){
            if(array_key_exists($post->id, $translateposts)):
                $post->post_title = $translateposts[$post->id]['post_title'];
                $post->post_content = $translateposts[$post->id]['post_content'];
                $post->post_short_content = $translateposts[$post->id]['post_short_content'];
            endif;
            $date_added = date('d M Y',strtotime($post->created_date));
            $short_desc = $post->post_short_content;
            if($short_desc ==""){
            $html_removed_text = Yii::app()->common->strip_html_tags($post->post_content);
                $short_desc = Yii::app()->general->formattedWords($html_removed_text, 300);
            }
            $author = $post->author;
            if($author ==""){
                $author = $studio->name;
            }
            $featured_image = $post->featured_image;
            if($featured_image !=""){
                $featimgUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $thumb   = $featimgUrl. "/system/featured_image/" . $studio_id . "/original/" .$post->featured_image;
                $thumb   = '<img src="'.$thumb.'">'; 
            }else{
            $html = html_entity_decode($post->post_content);
            preg_match('/<img[^>]+>/i', $html, $thumbnail);
            $thumb = "";
            if(!empty($thumbnail)){
                 $thumb = @$thumbnail[0];
            }
            }
            $blog_data[] = array(
                'permalink' => Yii::app()->getBaseUrl(true).'/blog/'.$post->permalink,
                'post_title' => utf8_encode(Yii::app()->common->htmlchars_encode_to_html($post->post_title)),
                'short_desc' => utf8_encode($short_desc),
                'thumb' => $thumb,
                'full_desc' => utf8_encode(Yii::app()->common->htmlchars_encode_to_html($post->post_content)),
                'author' =>$author,
                'date' => $date_added,
            );
        } 
        }
        //Start the pagination
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $page_url = Yii::app()->general->full_url($url, '?page=');
        $page_url = Yii::app()->general->full_url($page_url, '&page=');
        $pg = new bootPagination();
        $pg->pagenumber = $_REQUEST['page'];
        $pg->pagesize = $page_size;
        $pg->totalrecords = $count;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination-normal";
        $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
        $pg->defaultUrl = $page_url;
        if (strpos($page_url, '?') > -1)
            $pg->paginationUrl = $page_url . "&page=[p]";
        else
            $pg->paginationUrl = $page_url . "?page=[p]";
        $pagination = $pg->process();   
        $this->render('index', array('data' => json_encode($blog_data), 'pagination' => $pagination, 'sortby' => @$sortby,'mobile_url'=>$mobile_url));
    }

    public function actionView() {

        $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        if(isset($_REQUEST['mobileview'])){
            $this->is_mobileview =true;
        }
        $BusinessName = $studio->name;

        $title = 'Blog | ' . $BusinessName;
        $description = $BusinessName . ' blog';
        $title = 'Blog';
        $description = 'Blog';
        $keywords = 'Blog';

        $this->pageTitle = $title;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;

        $permalink = $_REQUEST['permalink'];
        $extension_id = $this->has_blog;
        $extension = Yii::app()->common->getExtensionDetails($extension_id);
        $comments = array();

        $data = BlogPost::model()->findByAttributes(
                array(
                    'studio_id' => $studio_id,
                    'has_published' => 1,
                    'permalink' => $permalink,
                    'parent_id' => 0
                )
        );

        if (count($data) > 0) {
            if($this->language_id != 20){
                $translatedData = BlogPost::model()->getTranslatedBlogData($studio_id, $this->language_id, $data->id);
                if(array_key_exists($data->id, $translatedData)){
                    $data->post_title = $translatedData[$data->id]['post_title'];
                    $data->post_content = $translatedData[$data->id]['post_content'];
                    $data->post_short_content = $translatedData[$data->id]['post_short_content'];
                }
            }
            $this->pageTitle = $data->post_title . ' | ' . $BusinessName;
            $this->pageDescription = $data->post_title;
            $this->pageKeywords = $data->post_title;

            if ($extension->comment_status > 0) {
                $comments = BlogComment::model()->findAllByAttributes(
                        array(
                            'studio_id' => $studio_id,
                            'status' => 1,
                            'post_id' => $data->id,
                        )
                );
            }
            $short_desc = $data->post_short_content;
            if($short_desc ==""){
                $html_removed_text = Yii::app()->common->strip_html_tags($data->post_content);
                $short_desc = Yii::app()->general->formattedWords($html_removed_text, 300);
            }
            $author = $data->author;
            if($author ==""){
                $author = $studio->name;
            }
            $featured_image = $data->featured_image;
            $thumb = "";
            if($featured_image !=""){
                $featimgUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $thumb   = $featimgUrl. "/system/featured_image/" . $studio_id . "/original/" .$data->featured_image;
                $thumb   = '<img src="'.$thumb.'">'; 
            }
            $date = date('d M Y',strtotime($data->created_date));
            $blog_data = array(
                'post_id' => Yii::app()->common->htmlchars_encode_to_html($data->id),
                'post_title' => utf8_encode(Yii::app()->common->htmlchars_encode_to_html($data->post_title)),
                'short_desc' => utf8_encode($short_desc),
                'full_desc' => utf8_encode(Yii::app()->common->htmlchars_encode_to_html($data->post_content)),
                'thumb'=>$thumb,
                'author'=>$author,
                'date'=>$date
            );                
            $this->render('view', array('post' => json_encode($blog_data), 'comments' => $comments));             
        } else {
            $redirect_url = Yii::app()->getBaseUrl(true);         
            Yii::app()->user->setFlash('error', 'Oops! You have no access to this page.');
            $this->redirect($redirect_url);
            exit();
        }
    }
    
    public function actionLoadCommentForm() {
        $this->layout = false;
       $post_id = $_POST['post_id'];
        $extension_id = $this->has_blog;
        $extension = Yii::app()->common->getExtensionDetails($extension_id);
        if ($extension->comment_status > 0) {
            $this->render('comment_form', array('post_id' => $post_id));
        }
    }

    function actionsaveUserComment() {
        $studio_id = Yii::app()->common->getStudiosId();
        $this->layout = false;
        if (isset($_POST['post_id']) && $_POST['post_id'] > 0 && isset($_POST['fname']) && $_POST['fname'] != '' && isset($_POST['comment']) && $_POST['comment'] != '') {
            $user_id = Yii::app()->user->id;
            $post = new BlogPost;
            $post = $post->findByPk($_POST['post_id']);
            $redirect_url = Yii::app()->getBaseUrl(true).'/blog/'.$post->permalink;
            if (!empty($user_id)) {
                $email = Yii::app()->user->email;
                $post_id = $_POST['post_id'];
                $fname = $_POST['fname'];
                $user_comment = htmlspecialchars(trim($_POST['comment']));
                

                $comment = new BlogComment();
                $comment->post_id = $post_id;
                $comment->studio_id = $studio_id;
                $comment->user_id = $user_id;
                $comment->created_date = new CDbExpression("NOW()");
                $comment->fullname = $fname;
                $comment->email = $email;
                $comment->comment = $user_comment;
                $comment->save(); 
                
                if($comment->id > 0){
                    Yii::app()->user->setFlash('success', $this->ServerMessage['your_comment_saved']);                    
                }
                else{
                    Yii::app()->user->setFlash('error', $this->ServerMessage['error_saving_comment']);
                }                
            } else {
                Yii::app()->user->setFlash('error', $this->ServerMessage['please_login_to_comment']);
            }
            $this->redirect($redirect_url);
            //echo $ret_val;
            exit();
        }
    }
    function actionsavecomment() {
        $studio_id = Yii::app()->common->getStudiosId();
        $this->layout = false;
        $status = 'error';
        $message = $this->ServerMessage['please_login_to_comment'];
        $user_id = Yii::app()->user->id;        
        if (!empty($user_id) && $user_id > 0 && isset($_POST['post_id']) && $_POST['post_id'] > 0 && isset($_POST['fname']) && $_POST['fname'] != '' && isset($_POST['comment']) && $_POST['comment'] != '') {
            $email = Yii::app()->user->email;
            $post_id = $_POST['post_id'];
            $fname = $_POST['fname'];
            $user_comment = htmlspecialchars(trim($_POST['comment']));            
            $post = new BlogPost;
            $post = $post->findByPk($post_id);            

            $comment = new BlogComment();
            $comment->post_id = $post_id;
            $comment->studio_id = $studio_id;
            $comment->user_id = $user_id;
            $comment->created_date = new CDbExpression("NOW()");
            $comment->fullname = $fname;
            $comment->email = $email;
            $comment->comment = $user_comment;
            $comment->save();

            if ($comment->id > 0) {
                $status = 'success';
                $message = $this->ServerMessage['your_comment_saved'];
                Yii::app()->user->setFlash('success', $this->ServerMessage['your_comment_saved']);                
            } else {
                $message = $this->ServerMessage['error_saving_comment'];
            }
        }
        $ret = array('status' => $status, 'message' => $message);
        echo json_encode($ret);
        exit;
    }
}
?>

