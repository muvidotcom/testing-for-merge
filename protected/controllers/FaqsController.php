<?php
class FaqsController extends Controller {
    public function init() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        parent::init();
    }
    public $defaultAction = 'index';
    public $layout = 'main';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        if (!($this->has_faqs)) {
            Yii::app()->user->setFlash('error', 'Oops! You have no access to this page.');
            $this->redirect($redirect_url);
            exit();
        }
        return true;
    }
    public function actionIndex() {
        $studio_id = Yii::app()->common->getStudiosId();
        $studio = new Studio;
        $studio = $studio->findByPk($studio_id);
        $language_id = $this->language_id;
        $BusinessName = $studio->name;

        $permalink = str_replace("/",'',$_SERVER['REQUEST_URI']);
        $BusinessTitle = MenuItem::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id,'permalink'=>$permalink), array('select' => 'title'));
        if(!empty($BusinessTitle)):
            $title = utf8_decode($BusinessTitle->title);
        else:
            $title = $this->Language['faq'];
        endif; 
        $description = $BusinessName . ' Help Center';
        $keywords = '';

        $this->pageTitle = $title . ' | ' . $BusinessName;
        $this->pageDescription = $description;
        $this->pageKeywords = $keywords;
        
        $sortby    = (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != '') ? $_REQUEST['sortby'] : 'id_seq';
        $order     = (isset($_REQUEST['order']) && $_REQUEST['order'] != '') ? $_REQUEST['order'] : 'asc';
        $page_size = (isset($_REQUEST['page_size']) && $_REQUEST['page_size'] != '') ? $_REQUEST['page_size'] : 10;
        if(isset($_REQUEST['mobileview'])){
            $this->is_mobileview =true;
        }
        $count = Faq::model()->count( 'studio_id=:studio_id', array(':studio_id' => $studio_id));
        $page_size = $page_size;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        //$data = Faq::model()->findAllByAttributes(array('studio_id' => $studio_id), array( 'order' => $sortby . ' ' . $order, 'limit' => $page_size,'offset' => $offset));
        $data = Faq::model()->findAllByAttributes(array('studio_id' => $studio_id), array( 'order' => $sortby . ' ' . $order));

        $faq_data = array();
        if(count($data) > 0){
            foreach ($data as $faq){
                $html_removed_text = Yii::app()->common->strip_html_tags($faq->content);
                $faq_data[] = array(
                    'faq_id'        => $faq->id,
                    'title'         => Yii::app()->common->htmlchars_encode_to_html($faq->title),
                    'content'       => Yii::app()->common->htmlchars_encode_to_html($faq->content),
                    'short_content' => substr($html_removed_text, 0, 500),
                    'content_length'=> strlen($html_removed_text),
                );
            }
        }
        //start pagination
        $url = Yii::app()->getBaseUrl(true).'/';
                    if(@$_REQUEST['sortby']){
                            $url .= '?sortby='.$_REQUEST['sortby'];
                    }
                    if(@$_REQUEST['order']){
                            if(strstr($url, '?')){$url .='&';}else{ $url .='?';}
                            $url .= 'order='.$_REQUEST['order'];
                    }
                    if(@$_REQUEST['page']){
                            if(strstr($url, '?')){$url .='&';}else{ $url .='?';}
                            $url .= 'page='.$_REQUEST['page'];
                    }
                    if(@$_REQUEST['page_size']){
                            if(strstr($url, '?')){$url .='&';}else{ $url .='?';}
                            $url .= 'page_size='.$_REQUEST['page_size'];
                    }

        $page_url = $url;

        $pg = new bootPagination();
        $pg->pagenumber = $page_number;
        $pg->pagesize = $page_size;
        $pg->totalrecords = $count;
        $pg->showfirst = true;
        $pg->showlast = true;
        $pg->paginationcss = "pagination-normal";
        $pg->paginationstyle = 0; // 1: advance advance pagination, 0: normal pagination
        $pg->defaultUrl = $page_url;
        if (strpos($page_url, '?') > -1)
            $pg->paginationUrl = $page_url . "&p=[p]";
        else
            $pg->paginationUrl = $page_url . "?p=[p]";
        $pagination = $pg->process();   
        $this->render('index', array('data' => json_encode($faq_data), 'pagination' => $pagination, 'sortby' => @$sortby));
    }
    public function actionSearch(){
        $studio_id = Yii::app()->common->getStudiosId();
        $searchedfaq = "";
        if(isset($_REQUEST['searchtext'])){
            $searchText = $_REQUEST['searchtext'];
            $match = addcslashes($searchText, '%_'); 
            $q = new CDbCriteria( array(
                'condition' => "(title LIKE :match OR content LIKE :match) AND studio_id = :studio_id", 
                'params'    => array(':match' => "%$match%",':studio_id'=>$studio_id),
                'order'     => "id_seq asc"
            ) );
            $faqs = Faq::model()->findAll( $q );
            if (count($faqs) > 0) {
                foreach($faqs as $faq){ 
                    $searchedfaq .= '<div class="faqs">';
                    $searchedfaq .= '<div>';
                    $searchedfaq .= '<h4><a href="javascript:void(0);" class="showmore" data-id="'.$faq->id.'">'. Yii::app()->common->htmlchars_encode_to_html($faq->title).'</a></h4>';
                    $searchedfaq .= '</div>'; 
                    $html_removed_text = Yii::app()->common->strip_html_tags($faq->content);
                    if (strlen($html_removed_text) > 500) {
                        $searchedfaq .= '<div>'.substr($html_removed_text, 0, 500).'...<a class="link showmore" href="javascript:void(0);" data-id="'.$faq->id.'">'.$this->Language["view_more"].' &raquo;</a>';
                    }else{
                        $searchedfaq .= '<div>'.$html_removed_text;
                    }
                    $searchedfaq .= '</div>'; 
                    $searchedfaq .='</div><div class="clearfix"></div><br />';
                } 
            }else{
                $searchedfaq .= '<div class="faqs">'.$this->Language['not_found'].'</div>';
            }
            $searchedfaq = "";
            if (count($faqs) > 0) {
                foreach($faqs as $faq){
                    $searchedfaq .='<div class="col-md-12">';
                    $searchedfaq .='<h3><a href="javascript:void(0);" class="showmore" data-id="'.$faq->id.'">'.$faq->title.'</a></h3>';
                    $html_removed_text = Yii::app()->common->strip_html_tags($faq->content);
                    if (strlen($html_removed_text) > 500) {
                        $searchedfaq .= '<div class="short_desc">'.substr($html_removed_text, 0, 500).'...<a class="showmore" href="javascript:void(0);" data-id="'.$faq->id.'">'.$this->Language["view_more"].'</a></div>';
                    }else{
                        $searchedfaq .= '<div class="short_desc">'.$html_removed_text.'</div>';
                    }
                    $searchedfaq .= '</div>';
                }
            }else{
                $searchedfaq .= '<div class="col-md-12">'.$this->Language['not_found'].'</div>';
            }
        }elseif(isset($_REQUEST['faq_id'])){
            $faq_id = $_REQUEST['faq_id'];
            $faq   = Faq::model()->findByPk($faq_id);
            if(!empty($faq)){
                $searchedfaq .= '<div class="faqs">';
                $searchedfaq .= '<div>';
                $searchedfaq .= '<h4><a href="javascript:void(0);">'.Yii::app()->common->htmlchars_encode_to_html($faq->title).'</a></h4>';
                $searchedfaq .= '</div>'; 
                $searchedfaq .= '<div>'.Yii::app()->common->htmlchars_encode_to_html($faq->content);
                $searchedfaq .= '</div>'; 
                $searchedfaq .='</div><div class="clearfix"></div><br />';
            }
            $searchedfaq = "";
            $searchedfaq .='<div class="col-md-12">';
            $searchedfaq .='<h3><a href="javascript:void(0);">'.Yii::app()->common->htmlchars_encode_to_html($faq->title).'</a></h3>';
            $searchedfaq .= '<div class="short_desc">'.Yii::app()->common->htmlchars_encode_to_html($faq->content).'</div>';
            $searchedfaq .= '</div>';
        }
        if($searchText !="" || isset($_REQUEST['faq_id'])){
        $searchedfaq .='<div class="back"> <a href="javascript:void(0)" onclick="javascript:location.reload();"> &laquo; '.$this->Language['back'].'</a></div>';
        }
        echo $searchedfaq;
    }
    #### For Lexanet (By Biswajit Das)
    public function actionGetdataofdevice(){
        if($_REQUEST){
            $device   = $_REQUEST['device'];
            $faq_data = $_REQUEST['faq_data'];
            $this->renderPartial('help-videos-'.$device,array('data'=>$faq_data));
        }
    }
}

