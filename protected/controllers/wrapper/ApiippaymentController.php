<?php

class ApiippaymentController extends Controller {

    /**
     * Constructor
     * @author Ashis Barik <ashis@muvi.com>
     */
    public function __construct() {
        
    }
    
    /**
     *
     * Initialize the api
     * @author Ashis Barik <ashis@muvi.com>
     */
    function initializePaymentGateway() {
        $ippayment = array();
        $ippayment['UserName'] = Yii::app()->controller->PAYMENT_GATEWAY_API_USER['ippayment'];
        $ippayment['Password'] = Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['ippayment'];
        $ippayment['DL'] = Yii::app()->controller->PAYMENT_GATEWAY_NON_3D_SECURE['ippayment'];
        $ippayment['AccountNumber'] = Yii::app()->controller->PAYMENT_GATEWAY_API_SIGNATURE['ippayment'];
        $ippayment['CustomerStorageNumber'] = Yii::app()->controller->API_ADDONS_IPP['ippayment']['CustomerStorageNumber'];
        $ippayment['RecurringUser'] = Yii::app()->controller->API_ADDONS_IPP['ippayment']['RecurringUser'];
        $ippayment['RecurringPassword'] = Yii::app()->controller->API_ADDONS_IPP['ippayment']['RecurringPassword'];
        if(Yii::app()->controller->IS_LIVE_API_PAYMENT_GATEWAY['ippayment'] != 'sandbox'){
            $ippayment['tokenURL'] = "https://www.bambora.co.nz/access/index.aspx";
            $ippayment['transactionURL'] = "https://www.bambora.co.nz/interface/api/dts.asmx";
        }else{
            $ippayment['tokenURL'] = "https://demo.ippayments.com.au/Access/index.aspx";
            $ippayment['transactionURL'] = "https://demo.ippayments.com.au/interface/api/dts.asmx";
        }
        return $ippayment;
    }
    
    function generateSST($arg = array()){
        $ippayment = self::initializePaymentGateway();
        $ippayment['Amount'] = str_replace('.', '', $arg['amount']);
        $ippayment['SessionID'] = $arg['session_id'];
        $ippayment['SessionKey'] = $arg['session_key'];
        $ippayment['ServerURL'] = $arg['ServerURL'];
        $ippayment['UserURL'] = $arg['UserURL'];
        $ippayment['CustRef'] = $arg['customer_ref'];
        $ippayment['TrnType'] = 2;
        $ippayment['TokeniseAlgorithmID'] = 2;
        $res = self::hash_call($ippayment);
        return $res;
        
    }
    function processTransaction($arg = array()){
        $ippayment = self::initializePaymentGateway();
        $amount = $arg['Amount']*100;
        $soap_request = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n";  
        $soap_request .= "<soap:Body>\r\n";
        $soap_request .= "<SubmitSinglePayment xmlns=\"http://www.ippayments.com.au/interface/api/dts\">\r\n";
        $soap_request .= "<trnXML>\r\n";
        $soap_request .= "<![CDATA[\r\n";
        $soap_request .= "<Transaction>\r\n";
        $soap_request .= "<CustomerStorageNumber>".$arg['CustomerStorageNumber']."</CustomerStorageNumber> \r\n";
        $soap_request .= "<CustRef>".$arg['CustRef']."</CustRef> \r\n";
        $soap_request .= "<Amount>".$amount."</Amount> \r\n";
        $soap_request .= "<TrnType>".$arg['TrnType']."</TrnType> \r\n";
        $soap_request .= "<CreditCard > \r\n";
        $soap_request .= "<TokeniseAlgorithmID>".$arg['TokeniseAlgorithmID']."</TokeniseAlgorithmID> \r\n";
        $soap_request .= "<CardNumber>".$arg['CardNumber']."</CardNumber> \r\n";
        $soap_request .= "</CreditCard> \r\n";
        $soap_request .= "<Security> \r\n";
        $soap_request .= "<UserName>".$ippayment['RecurringUser']."</UserName> \r\n";
        $soap_request .= "<Password>".$ippayment['RecurringPassword']."</Password> \r\n";
        $soap_request .= "</Security> \r\n";
        $soap_request .= "<UserDefined></UserDefined>\r\n";
        $soap_request .= "</Transaction>\r\n";
        $soap_request .= "]]>\r\n";
        $soap_request .= "</trnXML>\r\n";
        $soap_request .= "</SubmitSinglePayment>\r\n";
        $soap_request .= "</soap:Body>\r\n";
        $soap_request .= "</soap:Envelope>";

        $header = array(
            "cache-control: no-cache",
            "content-type: text/xml"
          );
        $post_url = $ippayment['transactionURL'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $post_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $soap_request,
          CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        $xml = str_replace(['&lt;','&gt;'],['<','>'], $response);
        $response_array = array('ResponseCode', 'Receipt', 'CreditCardToken', 'TruncatedCard', 'CardType', 'DeclinedCode', 'DeclinedMessage');

        $res = array();
        foreach($response_array as $key => $value) {
         $start_pos = strpos($xml, '<'.$value.'>');
         $end_pos = strpos($xml, '</'.$value.'>');
         $len = strlen($value) + 2;

         $res[$value] = substr($xml, ($start_pos+$len), ($end_pos - ($start_pos+$len)));
        }
        
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
			$res['is_success'] = (intval($res['ResponseCode']) == 0)?1:0;
			$res['ResponseCode'] = (intval($res['ResponseCode']) == 0)?1:0;
                        $res['DeclinedCode'] = (intval($res['ResponseCode']))?$res['ResponseCode']:0;
			return $res;
        }
    }
    
    function processTransactions($arg = array()){
        $ippayment = self::initializePaymentGateway();
        $amountSub = $arg['amount']*100;
        $soap_request = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n";  
        $soap_request .= "<soap:Body>\r\n";
        $soap_request .= "<SubmitSinglePayment xmlns=\"http://www.ippayments.com.au/interface/api/dts\">\r\n";
        $soap_request .= "<trnXML>\r\n";
        $soap_request .= "<![CDATA[\r\n";
        $soap_request .= "<Transaction>\r\n";
        $soap_request .= "<CustomerStorageNumber>QFX_HO</CustomerStorageNumber> \r\n";
        $soap_request .= "<CustRef>".$arg['reference_no']."</CustRef> \r\n";
        $soap_request .= "<Amount>".$amountSub."</Amount> \r\n";
        $soap_request .= "<TrnType>1</TrnType> \r\n";
        $soap_request .= "<CreditCard > \r\n";
        $soap_request .= "<TokeniseAlgorithmID>2</TokeniseAlgorithmID> \r\n";
        $soap_request .= "<CardNumber>".$arg['token']."</CardNumber> \r\n";
        $soap_request .= "</CreditCard> \r\n";
        $soap_request .= "<Security> \r\n";
        $soap_request .= "<UserName>".$ippayment['RecurringUser']."</UserName> \r\n";
        $soap_request .= "<Password>".$ippayment['RecurringPassword']."</Password> \r\n";
        $soap_request .= "</Security> \r\n";
        $soap_request .= "<UserDefined></UserDefined>\r\n";
        $soap_request .= "</Transaction>\r\n";
        $soap_request .= "]]>\r\n";
        $soap_request .= "</trnXML>\r\n";
        $soap_request .= "</SubmitSinglePayment>\r\n";
        $soap_request .= "</soap:Body>\r\n";
        $soap_request .= "</soap:Envelope>";

        $header = array(
            "cache-control: no-cache",
            "content-type: text/xml"
          );
        $post_url = $ippayment['transactionURL'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $post_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $soap_request,
          CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        $xml = str_replace(['&lt;','&gt;'],['<','>'], $response);
        $response_array = array('ResponseCode', 'Receipt', 'CreditCardToken', 'TruncatedCard', 'CardType', 'DeclinedCode', 'DeclinedMessage');

        $res = array();
        foreach($response_array as $key => $value) {
         $start_pos = strpos($xml, '<'.$value.'>');
         $end_pos = strpos($xml, '</'.$value.'>');
         $len = strlen($value) + 2;

         $res[$value] = substr($xml, ($start_pos+$len), ($end_pos - ($start_pos+$len)));
        }
        
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
            $res['is_success'] =0;
        if(intval($res['ResponseCode']) == 0 && $res['DeclinedCode'] == ""){
            $res['is_success']=1;
        }
          $res['invoice_id'] = Yii::app()->common->generateUniqNumber();
          $res['order_number'] = $res['Receipt'];
          $res['dollar_amount'] = $arg['amount'];
          $res['paid_amount'] = $arg['amount'];
          $res['response_text'] = $response;
          return $res;
        }
    }
    
        /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function saveCard($usersub, $arg) {
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Ashis Barik <ashis@muvi.com>
     */
    function defaultCard($usersub, $card) {
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Ashis Barik <ashis@muvi.com>
     */
    function deleteCard($usersub, $card = Null) {
        $res['isSuccess'] = 1;
        
        return json_encode($res);
    }
    
    function hash_call($nvp) {
        $ippayment = self::initializePaymentGateway();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ippayment['tokenURL']);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvp);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $sst_response =  str_replace('<html><body><form><input type="hidden" name="SessionStored" value="True" /><input type="hidden" name="SessionStoredError" value="" /><input type="hidden" name="SST" value="',"",$server_output);
        $sst = str_replace('" /></form></body></html>',"",$sst_response);
        $return = array();
        $return['sst'] = $sst;
        $return['session_id'] = $nvp['SessionID'];
        $return['token_url'] = $ippayment['tokenURL'];
        return $return;
    }
    
	function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return true;
    }
	
    function sampleIntegrationTransaction($user = array()) {
        $res = array();
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        return $res;
    }
}