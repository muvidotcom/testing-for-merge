<?php
class ApitestgatewayController extends Controller {
    /**
     * Constructor
     * @author Kajal Kumari <kajal@muvi.com>
     */
    public function __construct() {        
    }

    /*
     * This method is meant for sever side validation for card in stripe
     * Basically this is used when some one use our API (REST)
     * @param array $arg
     * @return json string
     * @author Kajal Kumari <kajal@muvi.com>
     */
    
    function processCard($arg = array()) 
    {
        $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        $res['card']['status'] = 'succeeded';
        $res['card']['profile_id'] = uniqid();
        $res['card']['card_type'] = 'Test'; //Card type
        $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
        $res['card']['token'] = uniqid();
        $res['card']['response_text'] = 'Test gateway transactions';
        return json_encode($res);
   }
    /**
     * 
     * Make transaction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Kajal Kumari <kajal@muvi.com>
     */
    function processTransaction($card_info, $user_sub) {
        $res['is_success'] = 1;
        $res['transaction_status'] = 'Success';
        $res['invoice_id'] = uniqid();
        $res['order_number'] = uniqid() ;
        $res['amount'] =$user_sub->amount;
        $res['paid_amount'] = $user_sub->amount;
        $res['response_text'] =  'Test gateway transactions';
        return $res;
    }
    
    /**
     * 
     * Make transaction
     * @param array $user
     * @return array
     * @author Kajal Kumari <kajal@muvi.com>
     */
    function processTransactions($user) {
            $amount = $user['amount'];
            $res['is_success'] = 1;
            $res['transaction_status'] = 'Success';
            $res['invoice_id'] = uniqid();
            $res['order_number'] =uniqid();
            $res['amount'] = $amount;
            $res['paid_amount'] = $amount;
            $res['dollar_amount'] = $amount;
            $res['currency_code'] = $user['currency_code'];
            $res['currency_symbol'] = $user['currency_symbol'];
            $res['response_text'] = 'Test gateway transactions';
            return $res;
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub
     * @return object
     * @author Kajal Kumari <kajal@muvi.com>
          */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return '';
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Kajal Kumari <kajal@muvi.com>
     */
    function saveCard($usersub, $arg) {
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        $res['card']['status'] = 'succeeded';
        $res['card']['card_type'] = 'Test';
        $res['card']['card_last_fourdigit'] = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);
        $res['card']['token'] = uniqid();
        $res['card']['response_text'] =  'Test gateway transactions';
        return $res;
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Kajal Kumari <kajal@muvi.com>
     */
    function deleteCard($usersub, $card = Null,$gateway_code = 'testgateway') {
                 
        $res['isSuccess'] = 1;
         return json_encode($res);
    }
    
    function defaultCard($usersub, $card , $gateway_code = 'testgateway') {
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Refund tranasction
     * @param array $data
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    
    /*function refundTransaction($data = array()) {
        self::initializePaymentGateway();
        $res = array();
        
        if (isset($data) && !empty($data)) {
            try {
                $refund = \Stripe\Refund::create(array(
                    "charge" => $data['charge'],
                    "amount" => $data['amount']*100
                ));
            
            $res['is_success'] = 1;
            $res['response_text'] = serialize($refund);
            
            } catch (Exception $ex) {
                $res['isSuccess'] = 0;
                $res['response_text'] = serialize($ex);
                $res['Message'] = 'We are not able to refund your amount.';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Message'] = 'We are not able to refund your amount.';
        }
        
        //return json_encode($res);
        return $res;
    }*/
}
?>