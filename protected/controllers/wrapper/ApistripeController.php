<?php
require_once 'stripe/init.php';

class ApistripeController extends Controller {

    /**
     * Constructor
     * @author Sunil Kund <sunil@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @return Stripe Object
     * @author Sunil Kund <sunil@muvi.com>
     */
    function initializePaymentGateway() {
        \Stripe\Stripe::setApiKey(Yii::app()->controller->PAYMENT_GATEWAY_API_USER['stripe']);
    }
    
    function createToken($arg) {
        self::initializePaymentGateway();
        
        try {
            $data = \Stripe\Token::create(
                        array("card" => array(
                            "name" => $arg['card_name'],
                            "number" => $arg['card_number'],
                            "exp_month" => $arg['exp_month'],
                            "exp_year" => $arg['exp_year'],
                            "cvc" => $arg['cvv']
                        ))
                    );
            
            $res['isSuccess'] = 1;
            $res['status'] = 'OK';
            $res['stripeToken'] = $data->id;
            $res['description'] = 'Creation of New customer';
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
            
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($err);
            $res['Message'] = $err['message'];
        }catch (Exception $e) {
            $err = $e->getJsonBody();

            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['status'] = 'Error';
            $res['response_text'] = serialize($e);
            $res['Message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
        }
        return $res;
    }

    /*
     * This method is meant for sever side validation for card in stripe
     * Basically this is used when some one use our API (REST)
     * @param array $arg
     * @return json string
     * @author Sunil Kund <sunil@muvi.com>
     */
    function authenticateCard($arg = array()) {
        self::initializePaymentGateway();
        
        $result = array();
        if (isset($arg['isAPI']) && $arg['isAPI'] == 1) {
            try {
                $res = \Stripe\Token::create(
                    array("card" => array(
                        "name" => $arg['card_name'],
                        "number" => $arg['card_number'],
                        "exp_month" => $arg['exp_month'],
                        "exp_year" => $arg['exp_year'],
                        "cvc" => $arg['cvv']
                    ))
                );
                
                $result['isSuccess'] = 1;
                $result['status'] = 'OK';
                $result['stripeToken'] = $res->id;
                $result['description'] = 'Creation of New customer';
            } catch (\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($e);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            }catch (Exception $e) {
                $err = $e->getJsonBody();

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
            }
        } else {
            $result['isSuccess'] = 0;
            $result['code'] = 55;
            $result['status'] = 'Error';
            $result['Message'] = 'Insufficient data provided.';
        }
        
        return $result;
    }
    
    /*
     * This method is meant for craeting source token in stripe
     * @param array $arg
     * @return json string
     * @author Sanjeev Kumar Malla <sanjeev@muvi.com>
     */
    function createSourceToken($arg = array()) {
        self::initializePaymentGateway();
        $result = array();
        if (isset($arg['iban_number']) && trim($arg['iban_number'])) {
            try {
                $res = \Stripe\Source::create(array(
                    "type" => "sepa_debit",
                    "sepa_debit" => array("iban" => $arg['iban_number']),
                    "currency" => strtolower($arg['currency_code']),
                    "owner" => array(
                        "name" => $arg['account_name'],
                        "address" => array(
                            "line1" => $arg['street_name'],
                            "city" => $arg['city_name'],
                            "postal_code" => $arg['postalcode'],
                            "country" => $arg['country_name'],
                          ),
                    ),
                ));
                
                $result['isSuccess'] = 1;
                $result['status'] = 'OK';
                $result['stripeToken'] = $res->id;
                $result['description'] = 'Creation of New customer';
            } catch (\Stripe\Error\Card $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($e);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = $err['message'];
            }catch (Exception $e) {
                $err = $e->getJsonBody();

                $result['isSuccess'] = 0;
                $result['code'] = 55;
                $result['status'] = 'Error';
                $result['response_text'] = serialize($err);
                $result['Message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
            }
        } else {
            $result['isSuccess'] = 0;
            $result['code'] = 55;
            $result['status'] = 'Error';
            $result['Message'] = 'Insufficient data provided.';
        }
        
        return $result;
    }
    
    /**
     * 
     * Validate the card and make a zero transaction
     * @param array $arg
     * @return json string
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processCard($arg = array()) {
        self::initializePaymentGateway();
        //self::keeplog(json_encode($arg), 'process card', 'Request params');
        if(isset($arg['is_sepa']) && intval($arg['is_sepa'])){
            $data = self::createSourceToken($arg);
            if (isset($data['status']) && $data['status'] == 'OK') {
                $arg['stripeToken'] = $data['stripeToken'];
                $arg['description'] = $data['description'];
            } else {
                return json_encode($data);
            }
            
            if (isset($arg['stripeToken']) && trim($arg['stripeToken'])) {
                self::keeplog($arg, 'process card', 'before create customer');
                try {
                    //Create a customer in stripe
                    $customer = \Stripe\Customer::create(array(
                        "source" => $arg['stripeToken'],
                        "description" => $arg['description'],
                        "email" => $arg['email'])
                    );
                    //Set variables
                    $card_last_fourdigit = str_replace(substr($arg['iban_number'], 0, strlen($arg['iban_number']) - 4), str_repeat("#", strlen($arg['iban_number']) - 4), $arg['iban_number']);
                    self::keeplog($customer, 'process card', 'after create customer');
                    $res['isSuccess'] = 1;
                    $res['card']['code'] = 200;
                    $res['card']['status'] = 'succeeded';
                    $res['card']['profile_id'] = $customer->id;//Stripe customer id which is used for recurring subscription
                    $res['card']['card_type'] = $customer->sources->data[0]->type; //Card type
                    $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                    $res['card']['token'] = $customer->sources->data[0]->id; //Stripe card id which is used to get card detail
                    $res['card']['response_text'] = serialize($customer);

                } catch (Exception $e) {
                    $res['isSuccess'] = 0;
                    $res['code'] = 404;
                    $res['response_text'] = serialize($e);
                    $res['Message'] = 'Invalid Card details entered. Please check again';
                }
            } else {
                $res['isSuccess'] = 0;
                $res['code'] = 55;
                $res['Message'] = 'We are not able to process your credit card. Please try another card.';
            }
            
        }else{
        //This case will handel when card is going to validated in server side
        if (isset($arg['isAPI']) && $arg['isAPI'] == 1) {
            $data = self::authenticateCard($arg);
            if (isset($data['status']) && $data['status'] == 'OK') {
                $arg['stripeToken'] = $data['stripeToken'];
                $arg['description'] = $data['description'];
            } else {
                return json_encode($data);
            }
        }else{
            $data = self::createToken($arg);
            if (isset($data['status']) && $data['status'] == 'OK') {
                $arg['stripeToken'] = $data['stripeToken'];
                $arg['description'] = $data['description'];
            } else {
                return json_encode($data);
            }
        }
        if (isset($arg['stripeToken']) && trim($arg['stripeToken'])) {
            self::keeplog($arg, 'process card', 'before create customer');
            try {
                //Create a customer in stripe
                $customer = \Stripe\Customer::create(array(
                    "card" => $arg['stripeToken'],
                    "description" => $arg['description'],
                    "email" => $arg['email'])
                );
                //Set variables
                $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);
                self::keeplog($customer, 'process card', 'after create customer');
                $res['isSuccess'] = 1;
                $res['card']['code'] = 200;
                $res['card']['status'] = 'succeeded';
                $res['card']['profile_id'] = $customer->id;//Stripe customer id which is used for recurring subscription
                $res['card']['card_type'] = $customer->sources->data[0]->brand; //Card type
                $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                $res['card']['token'] = $customer->sources->data[0]->id; //Stripe card id which is used to get card detail
                $res['card']['response_text'] = serialize($customer);
                
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }
        }

        return json_encode($res);
    }
    
    /**
     * 
     * Make tranasction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransaction($card_info, $user_sub) {
        self::initializePaymentGateway();
        
        $res = array();
        try {
            
            $charge = \Stripe\Charge::create(array(
                "amount" => $user_sub->amount * 100,
                "currency" => "usd",
                "description" => "Subscription Charge of ".date('F d, Y'),
                "customer" => $user_sub->profile_id
            ));
            self::keeplog($charge, 'processTransaction', 'after create Charge');
            $res['is_success'] = 1;
            $res['transaction_status'] = $charge->status;
            $res['invoice_id'] = $charge->id;
            $res['order_number'] = $charge->balance_transaction;
            $res['amount'] = $charge->amount / 100;
            $res['paid_amount'] = $charge->amount/100;
            $res['response_text'] = serialize($charge);
        } catch (Exception $e) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($e);
        }
        
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $user
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransactions($user) {
        self::initializePaymentGateway();
        
        $amount = $user['amount'];
        $currency = strtolower($user['currency_code']);
        $res = array();
        
        //$meta_tag = preg_replace('/\s+/', '-', $user['studio_name']);
        $meta_tag = $user['studio_name'];
        $meta_email = $user['user_email'];
        try {
			if (isset($user['is_save_card']) && intval($user['is_save_card'])) {
            
				$arg['card_name'] = $user['card_name'];
				$arg['card_number'] = $user['card_number'];
				$arg['exp_month'] = $user['exp_month'];
				$arg['exp_year'] = $user['exp_year'];
				$arg['cvv'] = $user['cvv'];
				$data = self::createToken($arg);
				self::keeplog($data, 'processTransactions', 'before create Charge');
				if (isset($data['status']) && $data['status'] == 'OK') {
            $charge = \Stripe\Charge::create(array(
                "amount" => $amount * 100,
                "currency" => $currency,
                "description" => "Subscription Charge of ".date('F d, Y'),
						"source" => $data['stripeToken'],
						"metadata" => array("Studio" => $meta_tag)
					));
				}
			} else {
				$charge = \Stripe\Charge::create(array(
					"amount" => $amount * 100,
					"currency" => $currency,
					"description" => "Subscription Charge of ".date('F d, Y'),
                "customer" => $user['profile_id'],
                "metadata" => array("Studio" => $meta_tag,"Email" => $meta_email)
            ));
			}
            self::keeplog($charge, 'processTransactions', 'after create Charge');
            $res['is_success'] = 1;
            $res['transaction_status'] = isset($charge->status) && trim($charge->status) == 'pending' ? 'Completed' : $charge->status;
            $res['invoice_id'] = $charge->id;
            $res['order_number'] = isset($charge->balance_transaction) && trim($charge->balance_transaction) ? $charge->balance_transaction : $charge->id;
            $res['amount'] = $user['amount'];
            $res['paid_amount'] = $charge->amount/100;
            $res['dollar_amount'] = $user['dollar_amount'];
            $res['currency_code'] = $user['currency_code'];
            $res['currency_symbol'] = $user['currency_symbol'];
            $res['response_text'] = serialize($charge);
        } catch (Exception $e) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($e);
        }
        
        return $res;
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub
     * @return object
     * @author Sunil Kund <sunil@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        self::initializePaymentGateway();

        $data = '';
        if (isset($usersub->profile_id) && !empty($usersub->profile_id)) {
            try {
                $cu = \Stripe\Customer::retrieve($usersub->profile_id);
                $data = $cu->delete();
            } catch (Exception $e) {
                
            }
        }
        
        return $data;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function saveCard($usersub, $arg) {
        self::initializePaymentGateway();
        $data = self::createToken($arg);
        if (isset($data['status']) && $data['status'] == 'OK') {
            $arg['stripeToken'] = $data['stripeToken'];
            $arg['description'] = $data['description'];
        } else {
            return $data;
        }
        
        if (isset($arg['stripeToken']) && !empty($arg['stripeToken'])) {
            try {
                $cu = \Stripe\Customer::retrieve($usersub->profile_id);
                $card = $cu->sources->create(array("source" => $arg['stripeToken']));
                
                //Set variables
                $card_last_fourdigit = str_replace(substr($arg['card_number'], 0, strlen($arg['card_number']) - 4), str_repeat("#", strlen($arg['card_number']) - 4), $arg['card_number']);

                $res['isSuccess'] = 1;
                $res['card']['code'] = 200;
                $res['card']['status'] = 'succeeded';
                $res['card']['card_type'] = $card->brand; //Card type
                $res['card']['card_last_fourdigit'] = $card_last_fourdigit;
                $res['card']['token'] = $card->id; //Stripe card id which is used to get card detail
                $res['card']['response_text'] = serialize($card);
                
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return $res;
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function defaultCard($usersub, $card , $gateway_code = 'stripe') {
        self::initializePaymentGateway();
        
        if (isset($card->token) && !empty($card->token)) {
            try {
                $cu = \Stripe\Customer::retrieve($usersub->profile_id);
                $cu-> default_source = $card->token;
                $cu->save();
                
                $res['isSuccess'] = 1;
                
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['code'] = 55;
            $res['Message'] = 'We are not able to process your credit card. Please try another card.';
        }

        return json_encode($res);
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function deleteCard($usersub, $card = Null,$gateway_code = 'stripe') {
        self::initializePaymentGateway();
        
        if (isset($card) && !empty($card)) {
            try {
                $cu = \Stripe\Customer::retrieve($usersub->profile_id);
                $cu->sources->retrieve($card->token)->delete();
                
                $res['isSuccess'] = 1;
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['code'] = 404;
                $res['response_text'] = serialize($e);
                $res['Message'] = 'Invalid Card details entered. Please check again';
            }
        }
        
        return json_encode($res);
    }
    
    function validatePaymentGatewayCredentials($API_USER = NUll, $API_PASSWORD = NULL, $IS_LIVE_MODE = NULL) {
        return true;
    }
    
    function sampleIntegrationTransaction($user = array()) {
        $res = array();
        $logfile = dirname(__FILE__).'/stripe.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n----------Studio ID: ".$user['studio_id']."----------\n";
        $msg .= "\n----------".Yii::app()->controller->PAYMENT_GATEWAY_API_USER['stripe']."----------\n";
        $msg .= "\n----------".Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['stripe']."----------\n";
        
        $card_last_fourdigit = str_replace(substr($user['card_number'], 0, strlen($user['card_number']) - 4), str_repeat("#", strlen($user['card_number']) - 4), $user['card_number']);
        $data_log = '';
        if (isset($user['card_number']) && !empty($user['card_number'])) {
            self::initializePaymentGateway();
            $msg .= "\n--------------------------Card last 4 digits: {$card_last_fourdigit}---------------------------------------\n";
            
            $user['amount'] = Yii::app()->billing->currencyConversion('USD', $user['currency_code'], $user['amount']);
            
            try {
                $tokres = \Stripe\Token::create(
                    array("card" => array(
                        "name" => $user['card_name'],
                        "number" => $user['card_number'],
                        "exp_month" => $user['exp_month'],
                        "exp_year" => $user['exp_year'],
                        "cvc" => $user['cvv']
                    ))
                );
                $msg .= "\n--------------------------Result : Token Creation---------------------------------------\n";
                $data_log = serialize($tokres);
                $msg.= $data_log."\n";
                $token = ((isset($tokres->id)) && trim($tokres->id)) ? trim($tokres->id) : '';
                
                if ($token) {
                    try {
                        $customer = \Stripe\Customer::create(array(
                            "card" => $token,
                            "description" => 'Payment gateway setup by studio admin',
                            "email" => $user['email'])
                        );
                        $msg .= "\n--------------------------Result : Customer Creation---------------------------------------\n";
                        $data_log = serialize($customer);
                        $msg.= $data_log."\n";
                        if ($customer->id) {
                            $data = array();
                            $data['amount'] = $user['amount'];
                            $currency = strtolower($user['currency_code']);

                            try {
                                $charge = \Stripe\Charge::create(array(
                                    "amount" => $data['amount']*100,
                                    "currency" => $currency,
                                    "description" => "Sample transaction on: ".date('F d, Y'),
                                    "customer" => $customer->id
                                ));
                                
                                $msg .= "\n--------------------------Result : Sample Charge---------------------------------------\n";
                                $data_log = serialize($charge);
                                $msg.= $data_log."\n";
                                $res['isSuccess'] = 1;
                            } catch (\Stripe\Error\Card $err) {
                                $msg .= "\n--------------------------Error : Sample Charge Card---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];

                            } catch (\Stripe\Error\RateLimit $err) {
                                $msg .= "\n--------------------------Error : Sample Charge RateLimit---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                // Too many requests made to the API too quickly
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];
                            } catch (\Stripe\Error\InvalidRequest $err) {
                                $msg .= "\n--------------------------Error : Sample Charge InvalidRequest---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                // Invalid parameters were supplied to Stripe's API
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];
                            } catch (\Stripe\Error\Authentication $err) {
                                $msg .= "\n--------------------------Error : Sample Charge Authentication---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                // Authentication with Stripe's API failed
                                // (maybe you changed API keys recently)
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];
                            } catch (\Stripe\Error\ApiConnection $err) {
                                $msg .= "\n--------------------------Error : Sample Charge ApiConnection---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                // Network communication with Stripe failed
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];
                            } catch (\Stripe\Error\Base $err) {
                                $msg .= "\n--------------------------Error : Sample Charge Base---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $body = $err->getJsonBody();
                                $error  = $body['error'];
                                $res['isSuccess'] = 0;
                                $res['error_message'] = $error['message'];
                            }catch (Exception $err) {
                                $msg .= "\n--------------------------Error : Sample Charge General Exception---------------------------------------\n";
                                $data_log = serialize($err);
                                $msg.= $data_log."\n";
                                $res['isSuccess'] = 0;
                                $res['error_message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
                            }
                        } else {
                            $res['isSuccess'] = 0;
                            $res['error_message'] = "Some internal error found in creating profile";
                        }
                    } catch (\Stripe\Error\Card $err) {
                        $msg .= "\n--------------------------Error : Customer Creation Card---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];

                    } catch (\Stripe\Error\RateLimit $err) {
                        $msg .= "\n--------------------------Error : Customer Creation RateLimit---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        // Too many requests made to the API too quickly
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];
                    } catch (\Stripe\Error\InvalidRequest $err) {
                        $msg .= "\n--------------------------Error : Customer Creation InvalidRequest---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        // Invalid parameters were supplied to Stripe's API
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];
                    } catch (\Stripe\Error\Authentication $err) {
                        $msg .= "\n--------------------------Error : Customer Creation Authentication---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        // Authentication with Stripe's API failed
                        // (maybe you changed API keys recently)
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];
                    } catch (\Stripe\Error\ApiConnection $err) {
                        $msg .= "\n--------------------------Error : Customer Creation ApiConnection---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        // Network communication with Stripe failed
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];
                    } catch (\Stripe\Error\Base $err) {
                        $msg .= "\n--------------------------Error : Customer Creation Base---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $body = $err->getJsonBody();
                        $error  = $body['error'];
                        $res['isSuccess'] = 0;
                        $res['error_message'] = $error['message'];
                    }catch (Exception $err) {
                        $msg .= "\n--------------------------Error : Customer Creation General Exception---------------------------------------\n";
                        $data_log = serialize($err);
                        $msg.= $data_log."\n";
                        $res['isSuccess'] = 0;
                        $res['error_message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
                    }
                } else {
                    $res['isSuccess'] = 0;
                    $res['error_message'] = "We are not able to process your credit card. Please try another card.";
                }
            } catch (\Stripe\Error\Card $err) {
                $msg .= "\n--------------------------Error : Token Creation Card---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
                
            } catch (\Stripe\Error\RateLimit $err) {
                $msg .= "\n--------------------------Error : Token Creation RateLimit---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                // Too many requests made to the API too quickly
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
            } catch (\Stripe\Error\InvalidRequest $err) {
                $msg .= "\n--------------------------Error : Token Creation InvalidRequest---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                // Invalid parameters were supplied to Stripe's API
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
            } catch (\Stripe\Error\Authentication $err) {
                $msg .= "\n--------------------------Error : Token Creation Authentication---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
            } catch (\Stripe\Error\ApiConnection $err) {
                $msg .= "\n--------------------------Error : Token Creation ApiConnection---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                // Network communication with Stripe failed
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
            } catch (\Stripe\Error\Base $err) {
                $msg .= "\n--------------------------Error : Token Creation Base---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $err->getJsonBody();
                $error  = $body['error'];
                $res['isSuccess'] = 0;
                $res['error_message'] = $error['message'];
            }catch (Exception $err) {
                $msg .= "\n--------------------------Error : Token Creation General Exception---------------------------------------\n";
                $data_log = serialize($err);
                $msg.= $data_log."\n";
                $res['isSuccess'] = 0;
                $res['error_message'] = (isset($err['error']['message']) && trim($err['error']['message'])) ? $err['error']['message'] : "Some internal error found";
            }
        } else {
            $res['isSuccess'] = 0;
            $res['error_message'] = 'Invalid credit card number';
        }
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
        return $res;
    }
    /**
     * 
     * Update user profile
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function updatePaymentProfile($user = array(),$sub = array(),$email)
    {
        $res = array();
        if(isset($sub) && !empty($sub)){
            self::initializePaymentGateway();
            try {
                $cu = \Stripe\Customer::retrieve($sub->profile_id);
                $cu->email = $email;
                $cu->save();
                $res['isSuccess'] = 1;
            } catch (Exception $e) {
                $res['isSuccess'] = 0;
                $res['error_message'] = serialize($e);
            }
        }else{
            $res['isSuccess'] = 0;
            $res['error_message'] = 'We are not able to update email now. Please try after sometime.';
        }
        return $res;
    }
    
    /**
     * 
     * Make tranasction
     * @param array $data
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processPCITransactions($data) {
        self::initializePaymentGateway();
         
        $amount = number_format(floor($data['amount']*100)/100,2, '.', '');
        $currency = strtolower($data['currency_code']);
        $res = array();
        try {
            $charge = \Stripe\Charge::create(array(
                "amount" => $amount * 100,
                "currency" => $currency,
                "receipt_email" => $data['email'],
                "description" => $data['desc'],
                "source" => $data['token'],
                "metadata" => array("Studio" => $data['metadata'])
            ));
            
            $res['is_success'] = 1;
            $res['transaction_status'] = $charge->status;
            $res['invoice_id'] = $charge->id;
            $res['order_number'] = $charge->balance_transaction;
            $res['amount'] = $data['amount'];
            $res['paid_amount'] = $charge->amount/100;
            $res['dollar_amount'] = $data['amount'];
            $res['currency_code'] = $data['currency_code'];
            $res['currency_symbol'] = $data['currency_symbol'];
            $res['response_text'] = serialize($charge);
        } catch (Exception $e) {
            $res['is_success'] = 0;
            $res['amount'] = 0;
            $res['response_text'] = serialize($e);
        }
        return $res;
    }
    
    /**
     * 
     * Refund tranasction
     * @param array $data
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    
    /*function refundTransaction($data = array()) {
        self::initializePaymentGateway();
        $res = array();
        
        if (isset($data) && !empty($data)) {
            try {
                $refund = \Stripe\Refund::create(array(
                    "charge" => $data['charge'],
                    "amount" => $data['amount']*100
                ));
            
            $res['is_success'] = 1;
            $res['response_text'] = serialize($refund);
            
            } catch (Exception $ex) {
                $res['isSuccess'] = 0;
                $res['response_text'] = serialize($ex);
                $res['Message'] = 'We are not able to refund your amount.';
            }
        } else {
            $res['isSuccess'] = 0;
            $res['Message'] = 'We are not able to refund your amount.';
        }
        
        //return json_encode($res);
        return $res;
    }*/
    
    function keeplog($data_log, $method, $action){
        $logfile = dirname(__FILE__).'/stripe.txt';
        $msg = "\n----------Log Date: ".date('Y-m-d H:i:s')."----------\n";
        $msg .= "\n----------Method: ".$method."----------\n";
        $msg .= "\n----------Action: ".$action."----------\n";
        $msg .= "\n----------".Yii::app()->controller->PAYMENT_GATEWAY_API_USER['stripe']."----------\n";
        $msg .= "\n----------".Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['stripe']."----------\n";
        $msg.= $data_log."\n";
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
    }
}

?>