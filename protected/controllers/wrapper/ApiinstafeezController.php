<?php

class ApiinstafeezController extends Controller {

    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }

    /**
     *
     * Initialize the api
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        return true;
    }
    
    function processCard($user) {
        $data['isSuccess'] = 1;
        return json_encode($data);
    }
    
}