<?php

class ApisbwController extends Controller {

    /**
     * Constructor
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    public function __construct() {
        
    }
    
    /**
     *
     * Initialize the api
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function initializePaymentGateway() {
        $sbw = array();
        $sbw['auth_username'] = Yii::app()->controller->PAYMENT_GATEWAY_API_USER['sbw'];
        $sbw['auth_password'] = Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['sbw'];
        $sbw['prod_id'] = Yii::app()->controller->PAYMENT_GATEWAY_NON_3D_SECURE['sbw'];
        $sbw['merch_id'] = Yii::app()->controller->PAYMENT_GATEWAY_API_SIGNATURE['sbw'];
        return $sbw;
    }
    
    /**
     * 
     * @param array $arg
     * @return json string
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function processCard($arg = array()) {
        $sbw = self::initializePaymentGateway();
        $arg['isSuccess'] = 1;
        $arg['prod_id'] = $sbw['prod_id'];
        $arg['mid'] = $sbw['merch_id'];
        $arg['success_url'] = Yii::app()->getBaseUrl(true) . "/userPayment/sbwreturn";
        return json_encode($arg);
    }
    
    /**
     * 
     * Make tranasction
     * @param object $card_info, object $user_sub
     * @return array
     * @author Sunil Kund <sunil@muvi.com>
     */
    function processTransaction($user = array()) {
        $sbw = self::initializePaymentGateway();
        $array['auth_username'] = $sbw['auth_username'];
        $array['prod_id'] = $sbw['prod_id'];
        $array['auth_password'] = $sbw['auth_password'];
        $array['service_type'] = 'one_click';
        $array['amount'] = $user['amount'];
        $array['bill_id'] = $user['profile_id'];
        $array['merch_id'] = $sbw['merch_id'];
        $res = self::hash_call($array);
        $customerData = self::customerDetails($user);
        $res['name_on_card'] = $customerData['customers'][0]['first_name'] . ' ' . $customerData['customers'][0]['last_name'];
        $res['card_last_fourdigit'] = $customerData['customers'][0]['orders'][0]['bills'][0]['acct_num'];
        $res['auth_num'] = $customerData['customers'][0]['orders'][0]['bills'][0]['route_num'];
        $res['token'] = $customerData['customers'][0]['orders'][0]['bills'][0]['customer_id'];
        $res['profile_id'] = $customerData['customers'][0]['orders'][0]['bills'][0]['bill_id'];
        $res['status'] = $customerData['result'];
        $res['is_success'] = 1;
        $res['isSuccess'] = 1;
        return $res;
    }
    
    function processTransactions($user = array()) {
        $sbw = self::initializePaymentGateway();
        $array['auth_username'] = $sbw['auth_username'];
        $array['prod_id'] = $sbw['prod_id'];
        $array['auth_password'] = $sbw['auth_password'];
        $array['service_type'] = 'one_click';
        $array['amount'] = $user['amount'];
        $array['amount'] = number_format((float) ($user['amount']), 2, '.', '');
        $array['bill_id'] = $user['profile_id'];
        $array['merch_id'] = $sbw['merch_id'];
        $res = self::hash_call($array);
        $customerData = self::customerDetails($user);
        $res['is_success'] = 1;
        $res['isSuccess'] = 1;
        $res['name_on_card'] = $customerData['customers'][0]['first_name'] . ' ' . $customerData['customers'][0]['last_name'];
        $res['card_last_fourdigit'] = $customerData['customers'][0]['orders'][0]['bills'][0]['acct_num'];
        $res['auth_num'] = $customerData['customers'][0]['orders'][0]['bills'][0]['route_num'];
        $res['token'] = $customerData['customers'][0]['orders'][0]['bills'][0]['customer_id'];
        $res['profile_id'] = $customerData['customers'][0]['orders'][0]['bills'][0]['bill_id'];
        $res['status'] = $customerData['result'];
        $res['transaction_status'] = $customerData['result'];
        $res['invoice_id'] = Yii::app()->common->generateUniqNumber();
        $res['order_number'] = Yii::app()->common->generateUniqNumber();
        $res['dollar_amount'] = $user['amount'];
        $res['paid_amount'] = $user['amount'];
        $res['response_text'] = json_encode($customerData);
        return $res;
    }
    
    function customerDetails($user = array()) {
        $sbw = self::initializePaymentGateway();
        $array['auth_username'] = $sbw['auth_username'];
        $array['prod_id'] = $sbw['prod_id'];
        $array['auth_password'] = $sbw['auth_password'];
        $array['service_type'] = 'customer_search';
        $array['bill_id'] = $user['profile_id'];
        $array['merch_id'] = $sbw['merch_id'];
        $res = self::hash_call($array);
        return $res;
    }
    
    /**
     * 
     * Save Customer's card detail
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function saveCard($usersub = Null, $arg = Null) {
        $sbw = self::initializePaymentGateway();
        $arg['isSuccess'] = 1;
        $arg['prod_id'] = $sbw['prod_id'];
        $arg['mid'] = $sbw['merch_id'];
        
        $success_url = Yii::app()->getBaseUrl(true) . "/userPayment/sbwsavecardreturn";
        $arg['redirect_url'] = "https://secure.sbw.com/v2/join?pay_type=cc&prod_id=" . $arg['prod_id'] . "&mid=" . $arg['mid'] . "&success_url=" . $success_url;

        return $arg;
    }
    
    /**
     * 
     * Delete Customer's card
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function deleteCard($usersub, $card = Null,$gateway_code = 'sbw') {
        self::initializePaymentGateway();
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Make default card where subscription will charge
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function defaultCard($usersub, $card , $gateway_code = 'stripe') {
        self::initializePaymentGateway();
        $res['isSuccess'] = 1;
        return json_encode($res);
    }
    
    /**
     * 
     * Cancel Customer's Account
     * @param object $usersub, $arg
     * @return array
     * @author Sanjeev Malla <sanjeev@muvi.com>
     */
    function cancelCustomerAccount($usersub = Null, $card_info = Null) {
        return null;
    }
    
    function sampleIntegrationTransaction($arg = array()) {
        $res = array();
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        return $res;
    }
    
    function hash_call($nvp) {
        $nvpstr = 'json=' . urlencode(json_encode($nvp));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.sbw.com/json');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpstr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $server_output = curl_exec($ch);
        $tempArray = urldecode($server_output);
        list($doc, $res) = explode("json=", $tempArray);
        $output = json_decode($res, TRUE);
        curl_close($ch);
        
        return $output;
    }
}