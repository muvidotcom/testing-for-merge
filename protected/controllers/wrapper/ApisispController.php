<?php

class ApisispController extends Controller {
   
    public function __construct() {
        
    }
    /**
     * 
    /**
     *
     * Initialize the api
     * @author Sunil kumar Nayak
     */
    function initializePaymentGateway() {
        $data = array();
        $data['posID'] = trim(Yii::app()->controller->PAYMENT_GATEWAY_API_USER['sisp']);
        $data['posAutCode'] = trim(Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['sisp']);
        if(Yii::app()->controller->IS_LIVE_API_PAYMENT_GATEWAY['sisp'] != 'sandbox'){
            $data['endpoint'] = "https://www.vinti4net.cv/sodade/biz_vbv.jsp";
        }else{
            $data['endpoint'] = "https://mc.vinti4net.cv/sodade/biz_vbv.jsp";
        }
        return $data;
    }
    function processCard($arg = array()) {
        $data = self::initializePaymentGateway();
        //card details
        $data['name'] = $arg['card_name'];
        $data['pan'] = $arg['card_number'];
        $data['dateMonth'] = $arg['exp_month'];
        $data['dateYear'] = $arg['exp_year'];
        $data['cvv2'] = $arg['security_code'];
        //card details end
        $data['merchantSession'] = $arg['merchantSession'];
        $data['merchantRef'] = $arg['merchantRef'];
        $data['amount'] = $arg['amount'];
        $data['currency'] = $arg['iso_num'];
        $data['Is3DSec'] = 1;
        $data['EntityCode'] = uniqid();
        $data['isSuccess'] = 1;
        $data['TransactionCode'] = 1;
        $data['urlMerchantResponse'] = Yii::app()->getBaseUrl(true)."/user/SispPayment";
        return $data;
    }
    function sampleIntegrationTransaction($arg = array()) {
        $res = array();
        $res['isSuccess'] = 1;
        $res['card']['code'] = 200;
        return $res;
    }
}