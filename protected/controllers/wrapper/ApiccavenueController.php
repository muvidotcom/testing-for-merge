<?php
error_reporting(0);
class ApiccavenueController extends Controller {

    /**
     * Constructor
     * @author satyajit Rout <satyajit@muvi.com>
     */
    public function __construct() {
        
    }
    /**
     * 
     * Library functions starts
     * @author Satyajit Rout <satyajit@muvi.com>
  */  
     function initializePaymentGateway() {
        $ccavenue = array();
        $ccavenue['merchant_id'] = trim(Yii::app()->controller->PAYMENT_GATEWAY_API_USER['ccavenue']);
        $ccavenue['access_code'] = trim(Yii::app()->controller->PAYMENT_GATEWAY_API_PASSWORD['ccavenue']);
        $ccavenue['working_key'] = trim(Yii::app()->controller->PAYMENT_GATEWAY_API_SIGNATURE['ccavenue']);
        if(Yii::app()->controller->IS_LIVE_API_PAYMENT_GATEWAY['ccavenue'] == trim('sandbox')){
             $ccavenue['endpoint'] = "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction";
           // $ccavenue['endpoint'] = "https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction";
             $ccavenue['mode'] = 0;
        }else{
            $ccavenue['mode'] = 1;
            $ccavenue['endpoint'] = "https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction";
        }
        return $ccavenue;
    }  
    public function generateCCPaymentForm($data = array()){
        $gateway_access = self::initializePaymentGateway();
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/vendor/ccavenue/Crypto.php';
        if(!empty($data)){
            $merchant_id = $gateway_access['merchant_id'];
            $access_code = $gateway_access['access_code'];
            $working_key = $gateway_access['working_key'];
            $url = $gateway_access['endpoint'];
            $name = $data['name'];
            $email = $data['email'];
            $currency = $data['currency'];
            $start_date = date("d-m-Y", strtotime($data['start_date']));
            $amount = $data['amount'];
            $redirect_url = base64_decode($data['redirect_url']);
            $cancel_url = base64_decode($data['cancel_url']);
            $order_id = $data['order_id'];
            $recurrence = strtoupper($data['recurrence']);
            $frequency = $data['frequency'];
            $language = 'EN';
            $si_amount = $data['plan_price'];
            if (isset($data['trail_period']) && (intval($data['trail_period']) == 0)) {
                $si_is_setup_amt = 'Y'; 
            }else{
                 $si_is_setup_amt = 'N';
                 $amount = 1.00;               
                
            }
         $billing_cicyle = 99;
         $merchant_data='';
         $merchant_data.='merchant_id='.$merchant_id;
         $merchant_data.='&order_id='.$order_id;
         //$merchant_data.='&order_id=111';
         $merchant_data.='&amount='.$amount;
         $merchant_data.='&currency='.$currency;
         $merchant_data.='&redirect_url='.$redirect_url;
         $merchant_data.='&cancel_url='.$cancel_url;
         $merchant_data.='&language='.$language;    
         $merchant_data.='&billing_name='.$name; 
         $merchant_data.='&billing_email='.$email; 
         if($data['is_si'] == 1){  
            $merchant_si_data = '&si_type=FIXED&si_is_setup_amt='.$si_is_setup_amt.'&si_amount='.$si_amount.'&si_start_date='.$start_date.'&si_frequency='.$frequency.'&si_frequency_type='.$recurrence.'&si_bill_cycle='.$billing_cicyle;
         } 
         $merchant_data .= $merchant_si_data; 
         $encrypted_data= encrypt($merchant_data,$working_key); // Method for encrypting the data.  
        }
        $form = '<form method="post" name="redirect" action="'.$url.'" id="redirect_ccavenue">';
        $form .= '<input type="hidden" name="encRequest" value="'.$encrypted_data.'" >';
        $form .= '<input type="hidden" name="access_code" value="'.$access_code.'" >';
       // $form .= '<input type="submit">';
        $form .= '</form';
        return $form;
    }
    public function getdcryptresponse($enc_data = ''){
        $gateway_access = self::initializePaymentGateway();
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/vendor/ccavenue/Crypto.php';
        $workingKey = $gateway_access['working_key'];
        $encResponse= $enc_data;			//This is the response sent by the CCAvenue Server
        $rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);
        for($i = 0; $i < $dataSize; $i++)
        {
            $information=explode('=',$decryptValues[$i]);
            if($i==3)	$order_status=$information[1];
        }
        $res = array();
        if($order_status==="Success")
        {
           
            
            for($i = 0; $i < $dataSize; $i++)
            {
                $information=explode('=',$decryptValues[$i]);
                $res[$information[0]] = $information[1];
            }

        }else{
            $res['order_status'] = $order_status;
        }
       $myfile = $_SERVER["DOCUMENT_ROOT"].'/ccavenue_logs/ccavenue_'.$res['tracking_id'].'.txt';
       $fp = fopen($myfile, 'w');
       fwrite($fp, print_r($res, TRUE));
       fclose($fp);
       $res['log_file'] = $myfile;
       return $res;
    }
    public function getsubscriptionStatus($sireferrence_number){
        $gateway_access = self::initializePaymentGateway();
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/vendor/ccavenue/Crypto.php';
        $workingKey = $gateway_access['working_key'];
        $access_code = $gateway_access['access_code'];
        $json_data = "{'si_sub_ref_no': '".$sireferrence_number."'}";
        $encrypted_data=encrypt($json_data,$workingKey); // Method for encrypting the data.
        $responderParameter = 'enc_request='.$encrypted_data.'&access_code='.$access_code.'&command=getSIChargeList&request_type=JSON&response_type=JSON&version=1.1';        
        $url = 'https://login.ccavenue.com/apis/servlet/DoWebTrans';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$responderParameter);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);
       // $server_output = 'status=0&enc_response=fbf24b33d52121c3895133fe06889fdbff266e8644f349fe5e793aa95ce35786167fb711de20a6d589bb1c107c29c2dbcbc9c5ee1b1dcb1bc3e3cdd0ada0c7b2124328228b2054c526d8280ea3880a23e5b116fc46f618e184feb9146605495495b60cd02728f9326ef465aa4442cbc4af7fbd4767cdcf350c24421dc78c3e5b433bd7c702e9a787ae06039d9a6a256f2304ab5f9342c7f147037a895efb746d516345b423a0e5688cd030db2c51583e52af83e14afc34b6f70a184718565d6004d2a1e28dcd0272b4a57fde2c308ecdc0492e666d409c6f2d0975adb8e084655c5eae69354a23774299e11d7b3e4151836cb0a0af99a02bf6bb18c650008037489b239482f3c8d7f437965a458836876fa1385649f13dd60f26698057e525649f588c42c14aae414b77eb364794edc80f1e673e61e1bac59b3bdfe8c734e0a50af27d09621d81f09deebdebba69a7bc71364b2a443697a5e698563f57a20cbb74b8d087104899a33994864ec448d2a77caaa1c98d7c585e02da931a5b71233d58b7ffa9c355c69a92f559fb040205854dd72f291ab93b9e31a3c060bd0589096d1cbf988c1a98bb57a41e73024b492b7421711cc83b004afa02b86bafabdc2d70f6995cf76f76363251d805e8d0b07abfcccc56469362d3db4f1b3cb48cfc8db50db055086d6ab13897d494cb4633af5e5658cbb3f0bcc12ca49e1aca5eedef0957d2cfeaf798e1682d783dc8cc3a2b86c8309d2a2623bb543a7d4b404b42af18896fe1dc11cef15e1d2478f86e055b33494af3e96b6c2f58a37b18cf1ec1e2b504620179567d91255d5a6bd07cfff15c5ef0178653a8e7887abb9d39df8feb6a779aa5b36b53cdd4990858e413ec6ce145e5145f7415a24ff5c6824c8ab977';
        $temp = explode('=',$server_output);
        $status_arr = explode('&',$temp[1]);
        $response = trim(decrypt($temp[2],$workingKey));
        $first_position =  strpos($response, "{");
        $last_position =  strrpos($response, "}");
        // $response =  substr($response, 0,-2);
        $response =  substr($response, $first_position,$last_position+1);
        $arr = json_decode($response,true);   
        $arr['status'] = $status_arr[0];
        return $arr;
    }
    function cancelCustomerAccount($si_reference_id){
        $gateway_access = self::initializePaymentGateway();
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/vendor/ccavenue/Crypto.php';
        $workingKey = $gateway_access['working_key'];
        $access_code = $gateway_access['access_code'];
        $json_data = "{'si_sub_ref_no': '".$si_reference_id."'}";
        $encrypted_data=encrypt($json_data,$workingKey); // Method for encrypting the data.
        $responderParameter = 'enc_request='.$encrypted_data.'&access_code='.$access_code.'&command=cancelSI&request_type=JSON&response_type=JSON&version=1.1';
        $url = 'https://login.ccavenue.com/apis/servlet/DoWebTrans';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$responderParameter);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        $temp = explode('=',$server_output);
        $status_arr = explode('&',$temp[1]);       
        $status = $status_arr[0];
            return 1;
    }    
}
?>
