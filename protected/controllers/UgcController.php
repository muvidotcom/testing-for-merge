<?php

class UgcController extends Controller {

    public $base_url, $custom_metadata_form_id;

    public function init() {
        parent::init();
        if (isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
    }

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $this->base_url = Yii::app()->getBaseUrl(true);
        if (!Yii::app()->user->id) {
            $this->redirect($this->base_url);
            exit;
        }
        return true;
    }

    public $layout = 'main';

	public function actionShowdetails() {
        $config = StudioConfig::model()->getconfigvalue('user_generated_content');
        if ($config['config_value'] != 1) {
            Yii::app()->user->setFlash('error', $this->Language['oops_you_have_no_access']);
            $this->redirect($this->base_url);
            exit;
        }
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';

        $this->pageTitle = "Muvi | UGC List";
        $this->breadcrumbs = array('Manage Content', "Content Library");

        if ($_REQUEST['searchForm']['contenttype'] == 2) {
            $_REQUEST['searchForm']['is_physical'] = 1;
        }
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $user_id = Yii::app()->user->id;
        if (Yii::app()->common->allowedUploadContent($studio_id) > 0)
            $allow_new = 1;
        else
            $allow_new = 0;
        /* $pdo_cond = " f.studio_id=:studio_id AND sdk_user_id=:sdk_user_id";
          $pdo_cond_arr = array(":studio_id" => $studio_id, 'sdk_user_id'=>$user_id); */

        $pdo_cond = " f.studio_id='$studio_id' AND sdk_user_id='$user_id'";
        $pdo_cond_arr = array();
        $con = Yii::app()->db;
        $content_type = Yii::app()->general->content_count($studio_id);
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            if (isset($search_form) && $search_form['update_date'] == '' && $search_form['content_category_value'] == '' && $search_form['search_text'] == '' && $search_form['custom_metadata_form_id'] == '' && $search_form['sortby'] == '') {
                $url = $this->createUrl('admin/managecontent');
                $this->redirect($url);
            } else {
                $searchData = $_REQUEST['searchForm'];
                if ($searchData['update_date']) {
                    $sdate = json_decode(stripslashes($searchData['update_date']), TRUE);
                    $pdo_cond .= " AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') >= :start_dt AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') <= :end_dt ";
                    $pdo_cond_arr[':start_dt'] = $sdate['start'];
                    $pdo_cond_arr[':end_dt'] = $sdate['end'];
                }
                if ($searchData['search_text']) {
                    $pdo_cond .= " AND ((LOWER(f.name) LIKE :search_text) OR (LOWER(ms.episode_title) LIKE :search_text)) ";
                    $pdo_cond_arr[':search_text'] = "%" . strtolower(trim($searchData['search_text'])) . "%";
                }
                if ($searchData['contenttype'] == 1) {//Audio
                    $pdo_cond .= " AND f.content_types_id IN(5,6,8) ";
                } else {
                    $pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
                }
                if ($searchData['filterby'] == 0) {
                    if ($searchData['content_category_value']) {
                        foreach ($searchData['content_category_value'] as $key => $value) {
                            $cat_cond .= "FIND_IN_SET($value,f.content_category_value) OR ";
                        }
                        $cat_cond = rtrim($cat_cond, 'OR ');
                        $pdo_cond .= " AND ($cat_cond) ";
                        /* $pdo_cond .= " AND (f.content_category_value & :content_category_value) ";
                          $pdo_cond_arr[':content_category_value'] = array_sum($searchData['content_category_value']); */
                    }
                } elseif ($searchData['filterby'] == 1) {
                    if ($searchData['custom_metadata_form_id']) {
                        if (count($searchData['custom_metadata_form_id']) == 1) {
                            $get_is_child = Yii::app()->db->createCommand()
                                    ->from('custom_metadata_form')
                                    ->select('content_type,is_child')
                                    ->where('id =' . $searchData['custom_metadata_form_id'][0])
                                    ->queryROW();
                        }
                        if (($get_is_child['content_type'] == 1) && ($get_is_child['is_child'] == 0)) {
                            $andwherecondition = "`f`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . ") AND ms.is_episode=0";
                        } else {
                            $andwherecondition = "(`f`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . ")) OR (`ms`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . "))";
                        }
                        $custom_metadata_form_id_flag = 1;
                    }
                }
            }
        } else {
            if ((isset($content_type) && ($content_type['content_count'] == 4))) {
                $pdo_cond .= " AND f.content_types_id IN(5,6,8) ";
            } else {
                $pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
            }
        }
        $search_text = '';
        $default_lang_id = 20;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id={$studio_id} AND language_id={$language_id}))";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();

        $contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
        $order = ' ms.id DESC ';
        $orderType = 'desc';
        $sort = 'update_date';
        if (isset($_REQUEST['sort'])) {
            if ($_REQUEST['sort'] == 'update_date' && $_REQUEST['sort_type'] == 'asc') {
                $order = ' ms.last_update_date ASC ';
                $orderType = 'asc';
            }
        }

        if (isset($searchData['sortby'])) {
            if ($searchData['sortby'] == 1) {//most viewed
                $mostviewed = 1;
            } elseif ($searchData['sortby'] == 2) {//A-Z
                $order = ' f.name ASC ';
            } elseif ($searchData['sortby'] == 3) {//Z-A
                $order = ' f.name DESC ';
            }
        }
        //Pagination Implimented ..
        $page_size = 20;
        $offset = 0;
        $tab_active = 0;
        if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $page_size;
            $page_number = $_REQUEST['p'];
            $tab_active = 1;
        } else {
            $page_number = 1;
        }
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }
        //PDO Query
        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo,f.parent_content_type_id,f.custom_metadata_form_id,ms.custom_metadata_form_id as cmfid,ms.is_offline')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND f.parent_id=0 AND ms.episode_parent_id=0 AND ' . $pdo_cond, $pdo_cond_arr)
                ->order($order)
                ->limit($page_size, $offset);
        if ($custom_metadata_form_id_flag) {
            $command->andwhere($andwherecondition);
        }
        //$data = $command->getText(); print_r($data); exit;   
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
        //$pages = new CPagination($count);
        //$pages->setPageSize($page_size);
        // simulate the effect of LIMIT in a sql query
        //$end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        //$sample = range($pages->offset + 1, $end);
        $pagination = '';
        if ($count > 0) {
            $page_url = Yii::app()->general->full_url($url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');
            $pages = new bootPagination();
            $pages->pagenumber = $page_number;
            $pages->pagesize = $page_size;
            $pages->totalrecords = $count;
            $pages->showfirst = true;
            $pages->showlast = true;
            $pages->paginationcss = "pagination-normal";
            $pages->paginationstyle = 0;
            $pages->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1) {
                $pages->paginationUrl = $page_url . "&p=[p]";
            } else {
                $pages->paginationUrl = $page_url . "?p=[p]";
            }
            $pagination = $pages->process();
        }

        $form = Yii::app()->general->formlist($studio_id);
        $form = self::unsetForms($form, @$_REQUEST['searchForm']['is_physical']);
        if ($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            $movieIdsForliveStream = '';
            $movieLiveStream = array();
            $videoGalleryIdsForvideoName = '';
            $audioGalleryIdsForaudioName = '';
            $movievideogalleryName = array();
            $movieaudiogalleryName = array();
            $videoEncodingLogarray = array();
            $list = CHtml::listData($form, 'id', 'name');
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .= "'" . $v['stream_id'] . "',";
                } else {
                    $movieids .= "'" . $v['id'] . "',";
                }
                // For live sreaming
                if ($v['content_types_id'] == 4) {
                    $movieIdsForliveStream .= "'" . $v['id'] . "',";
                }
                //video name from video gallery
                if ($v['video_management_id'] != 0) {
                    $videoGalleryIdsForvideoName .= "'" . $v['stream_id'] . "',";
                }
                //audio name from video gallery
                if ($v['video_management_id'] != 0 && ($v['content_types_id'] == 6 || $v['content_types_id'] == 5)) {
                    $audioGalleryIdsForaudioName .= "'" . $v['stream_id'] . "',";
                }
                //video name from video gallery
                if ($v['full_movie'] != '' && $v['is_converted'] == 0) {
                    $videoEncodingLogarray[] = $v['stream_id'];
                }
                if ($v['content_types_id'] == 5 || ( $v['content_types_id'] == 6 && $v['is_episode'] == 1 )) {
                    $playlist = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 1);
                }
                $data[$k]['formname'] = Yii::app()->general->getFormName($v, $studio_id, $form, $list);
            }
            $videoEncodingLog = "";
            $videoEncodingLog = implode(",", $videoEncodingLogarray);
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
            $movieIdsForliveStream = rtrim($movieIdsForliveStream, ',');
            $videoGalleryIdsForvideoName = rtrim($videoGalleryIdsForvideoName, ',');
            $audioGalleryIdsForaudioName = rtrim($audioGalleryIdsForaudioName, ',');
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }
            //Live stream url
            if ($movieIdsForliveStream) {
                $sql = "SELECT movie_id,feed_method,feed_url,stream_url,stream_key,start_streaming FROM livestream WHERE movie_id IN(" . $movieIdsForliveStream . ") and studio_id = " . $studio_id;
                $liveSreamData = $con->createCommand($sql)->queryAll();
                if ($liveSreamData) {
                    foreach ($liveSreamData as $key => $val) {
                        if ($val['feed_url'] != "") {
                            $movieLiveStream[$val['movie_id']] = 1;
                            if ($val['feed_method'] == 'push') {
                                $movieLiveStream['feed_method'][$val['movie_id']] = $val['feed_method'];
                                $movieLiveStream['stream_url'][$val['movie_id']] = $val['stream_url'];
                                $movieLiveStream['stream_key'][$val['movie_id']] = $val['stream_key'];
                                $movieLiveStream['start_streaming'][$val['movie_id']] = $val['start_streaming'];
                            }
                        }
                    }
                }
            }
            //Video name from video gallery
            if ($videoGalleryIdsForvideoName || $audioGalleryIdsForaudioName) {
                $sql = "SELECT ms.id, vm.video_name FROM movie_streams ms, video_management vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $videoGalleryIdsForvideoName . ") and ms.studio_id = " . $studio_id;
                $videoManagement = $con->createCommand($sql)->queryAll();
                if ($videoManagement) {
                    foreach ($videoManagement as $key => $val) {
                        if ($val['video_name'] != "") {
                            $movievideogalleryName[$val['id']] = $val['video_name'];
                        }
                    }
                }
                //Get Audio Name from Audio Gallery
                if ($audioGalleryIdsForaudioName) {
                    $sql = "SELECT ms.id, vm.audio_name FROM movie_streams ms, audio_gallery vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $audioGalleryIdsForaudioName . ") and ms.studio_id = " . $studio_id;
                    $audioManagement = $con->createCommand($sql)->queryAll();
                    if ($audioManagement) {
                        foreach ($audioManagement as $key => $val) {
                            if ($val['audio_name'] != "") {
                                $movieaudiogalleryName[$val['id']] = $val['audio_name'];
                            }
                        }
                    }
                }
            }

            //Get video encoding data
            $encodingData = array();
            if ($videoEncodingLog) {
                $encLogData = Encoding::model()->getEncodingData($videoEncodingLog);
                if ($encLogData) {
                    foreach ($encLogData AS $key => $val) {
                        $now = new DateTime();
                        $post = new DateTime($val['expected_end_time']);
                        $interval = $now->diff($post);
                        if ($interval) {
                            $encodingData[$val['movie_stream_id']] = $interval->h . ':' . $interval->i . ':' . $interval->s;
                        } else {
                            $encodingData[$val['movie_stream_id']] = '0:0:0';
                        }
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }

        $studio = $this->studio;
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $contentid = $pcontent['movie_id'];
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id);
        } else {
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id);
        }
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'embed_watermark');
        $embedWaterMArkEnable = 0;
        if ($getStudioConfig) {
            if (@$getStudioConfig['config_value'] == 1) {
                $embedWaterMArkEnable = 1;
            }
        }
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $notpartner = 0;
        } else {
            $notpartner = 1;
            $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = ' . $studio_id));
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id', array(':studio_id' => $studio_id));
        }
        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $data_enable = Yii::app()->general->monetizationMenuSetting($studio_id);
        $srest = StudioCountryRestriction::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $poster_sizes = Yii::app()->common->getCropDimension($studio_id);
        if ($mostviewed) {
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $cnt = $episodeViewcount[$v['stream_id']] ? $episodeViewcount[$v['stream_id']] : 0;
                } else {
                    $cnt = ($viewcount[$v['id']]) ? $viewcount[$v['id']] : 0;
                }
                $data[$k]['cnt'] = $cnt;
                $num[] = $cnt;
            }
            array_multisort($num, SORT_DESC, $data);
        }
        $this->render('showdetaillist', array('data' => $data, 'movievideogalleryName' => @$movievideogalleryName, 'movieaudiogalleryName' => $movieaudiogalleryName, 'movieLiveStream' => @$movieLiveStream, 'embedWaterMArkEnable' => $embedWaterMArkEnable, 'studio' => $studio, 'content_category_value' => @$searchData['content_category_value'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount, 'item_count' => @$count, 'pagination' => $pagination, 'contentList' => @$contentTypes, 'sort' => @$sort, 'orderType' => @$orderType, 'studio_ads' => @$studio_ads, 'allow_new' => $allow_new, 'geoexist' => $geoexist, 'notpartner' => $notpartner, 'bucketInfo' => $bucketInfo, 'data_enable' => $data_enable, 'srest' => $srest, 'grestcat' => $grestcat, 'form' => $form, 'searchData' => $searchData, 'encodingData' => @$encodingData, 'playlistName' => $playlist, 'poster_size' => $poster_sizes, 'videoEncodingLog' => $videoEncodingLog, 'offline_val' => $offline_val));
    }

    function unsetForms($form, $is_physical = 0) {
        foreach ($form as $key => $value) {
            if ($is_physical) {
                if ($value['parent_content_type_id'] != 5)
                    unset($form[$key]);
            }else {
                if ($value['parent_content_type_id'] == 5)
                    unset($form[$key]);
            }
        }
        return $form;
    }

    /* actioncreateContent function also used for both add and edit Content - Author Chinu */

    public function actioncreateContent($movieStream = null, $data = null) {
        $config = StudioConfig::model()->getconfigvalue('user_generated_content');
        if ($config['config_value'] == 1) {
            $language_id = $this->language_id;
            if (empty($movieStream)) {
                $this->pageTitle = Yii::app()->name . ' | ' . 'Create Content ';
            }
            $studio_id = $this->studio->id;
            $this->custom_metadata_form_id = self::customFormMetaDataId();
 
           $allcategories = ContentCategories::model()->getCategories($studio_id);
            $ugc_poster_dim = Yii::app()->usercontent->getCustomPosterSize($this->custom_metadata_form_id);
            $issubCat_enabled = Yii::app()->custom->getCatImgOption($studio_id, 'subcategory_enabled');

            $custom_content_types_id = 1;
            $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($custom_content_types_id);
			$arg['arg']['editid'] = !empty($movieStream->custom_metadata_form_id) ? $movieStream->custom_metadata_form_id : $movieStream->film->custom_metadata_form_id;
            $customComp = new CustomForms();
            if ($arg['arg']['editid']) {
                $customData = $customComp->getCustomMetadata($studio_id, $movieStream->film->parent_content_type_id, $arg['arg']);
            }else{
                    $fid = Yii::app()->general->getArrayFrommetadata_form_type_id($custom_content_types_id);
                    $fid['arg']['editid'] = $this->custom_metadata_form_id;
                    $customComp = new CustomForms();
                    $customData1[0] = $customComp->getCustomMetadata($studio_id, $fid['parent_content_type_id'], $fid['arg']);
                    $customData1[1] = $fid['arg']['is_child'];
                    $customData1[2] = $fid['parent_content_type_id'];				
                    //$customData1 = Yii::app()->general->getCustomMetadataForm($studio_id,$custom_content_types_id);
                    $customData = $customData1[0];				
            }			
            //$poster_sizes = Yii::app()->common->getCropDimension($studio_id);
            //$customForm = Yii::app()->usercontent->contentForm($movieStream);

            $langcontent = Yii::app()->custom->getTranslatedContent($movieStream->film->id, $movieStream->is_episode, $language_id);
            if (!empty($movieStream)) {
                if (array_key_exists($movieStream->id, @$langcontent['film'])) {
                    $movieStream->film->name = @$langcontent['film'][$mv_id]->name;
                }
                $this->pageTitle = Yii::app()->name . ' | ' . 'Edit Content - ' . $movieStream->film->name;
            }
            $IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);

            //$custom_form_html = $this->renderPartial('custom_form', array('filmData' => $movieStream, 'customData' => $customData, 'langcontent' => $langcontent, 'issubCat_enabled'=>$issubCat_enabled, 'contentCategories'=>$allcategories), true);
            Yii::app()->theme = 'bootstrap';
            $video_data = Yii::app()->usercontent->getUGCMoiveUrl($movieStream->id); /* only for single part video */
            $thirdparty_data = $this->renderPartial('//ugc/player_third_party', array('third_party_url' => $video_data), true);
            $video_html = $this->renderPartial('//ugc/video_upload', array('movieStream' => $movieStream, 'video_data' => $video_data, 'thirdparty_data' => $thirdparty_data), true);
            $custom_form_html = $this->renderPartial('//ugc/custom_form', array("data" => $data, 'contentCategories' => $allcategories, 'content_types_id' => $data[0]['content_types_id'], 'customData' => $customData, 'langcontent' => $langcontent, 'issubCat_enabled' => $issubCat_enabled, 'IsDownloadable' => $IsDownloadable), true);

            Yii::app()->theme = $this->studio->theme;
            $formdata = Yii::app()->general->formlist($studio_id);
            $this->render('create_ugc', array('categories' => $allcategories, 'width' => $ugc_poster_dim['vwidth'], 'height' => $ugc_poster_dim['vheight'], 'issubCat_enabled' => $issubCat_enabled, 'movieStream' => $movieStream, 'formdata' => $formdata, 'video_data' => $video_data, 'video_html' => $video_html, 'custom_form_html' => $custom_form_html, 'hor_poster_studio' => HOR_POSTER_STUDIO));
        } else {
            Yii::app()->user->setFlash('error', $this->Language['oops_you_have_no_access']);
            $this->redirect($this->base_url);
            exit;
        }
        exit;
    }

    public function actioneditContent() {
        $studio_id = $this->studio->id;
        if (!empty($_REQUEST['stream_uniq_id'])) {
            $movieStream = movieStreams::model()
                    ->with(array('film' => array('select' => 'f.*', 'alias' => 'f')))
                    ->find(array('select' => 't.*', 'condition' => 't.studio_id=:studio_id AND t.embed_id=:embed_id AND t.sdk_user_id=:sdk_user_id', 'params' => array(':studio_id' => $studio_id, ':embed_id' => $_REQUEST['stream_uniq_id'], ':sdk_user_id' => $this->user_id)));
            if (!$movieStream) {
                Yii::app()->user->setFlash('error', $this->Language['invalid_content']);
                $url = $this->createUrl('ugc/Showdetails');
                $this->redirect($url);
            } else if ($movieStream->sdk_user_id != $this->user_id) {
                Yii::app()->user->setFlash('error', $this->Language['invalid_data']);
                $url = $this->createUrl('ugc/Showdetails');
                $this->redirect($url);
            }

            $pdo_cond = 'm.id=:stream_id AND m.studio_id=:studio_id';
            $pdo_cond_arr = array(':stream_id' => $movieStream->id, ':studio_id' => $studio_id);
            $fetchcustomfield = 'f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10,m.custom1 as cs1,m.custom2 as cs2,m.custom3 as cs3,m.custom4 as cs4,m.custom5 as cs5,m.custom6 as cs6,f.custom_metadata_form_id,m.custom_metadata_form_id as cmfid,m.is_downloadable';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.content_category_value,f.content_subcategory_value,f.parent_content_type_id as parent_content_type ,m.id as movie_stream_id,f.uniq_id,f.name,f.language,f.censor_rating,f.genre,f.story,f.release_date,f.content_type_id,m.full_movie,m.episode_title,m.episode_date,m.episode_story,m.episode_number,m.series_number,m.is_episode,if(UNIX_TIMESTAMP(m.content_publish_date) = 0,\'\',m.content_publish_date) AS content_publish_date,f.content_types_id,m.is_converted,m.is_download_progress,m.video_management_id,f.language_id,f.parent_id,f.start_time,f.duration,m.episode_language_id,m.episode_parent_id,' . $fetchcustomfield . '')
                    ->from('films f ,movie_streams m ')
                    ->where('f.id = m.movie_id AND  ' . $pdo_cond, $pdo_cond_arr);
            $list = $command->queryAll();

            self::actioncreateContent($movieStream, $list);
        } else {
            Yii::app()->user->setFlash('error', $this->Language['invalid_data']);
            $url = $this->createUrl('ugc/Showdetails');
            $this->redirect($url);
            //$this->redirect($this->base_url);
        }
        exit;
    }

    public function actionsaveUserContent() {
        if ($_POST) {
            $this->custom_metadata_form_id = self::customFormMetaDataId();
            $config = StudioConfig::model()->getconfigvalue('user_generated_content');
            $requestType = !empty($_REQUEST['stream_uniq_id']) ? 'edit' : 'add';
            if ($config && $config['config_value'] == 1) {
                $isAjax = isset($_POST['isAjax']) && $_POST['isAjax'] == 1 ? 1 : '';
                if (empty($_POST['movie']['name']) && empty($_POST['movie']['content_category_value'])) {
                    if ($isAjax) {
                        $res['error'] = 1;
                        $res['message'] = 'Please fill mandatory fields';
                    } else {
                        Yii::app()->user->setFlash('error', 'Please fill required fields');
                        $this->redirect(Yii::app()->user->returnUrl);
                        exit;
                    }
                } else {
                    $studio_id = $this->studio->id;
                    $user_id = Yii::app()->user->id;
                    if (!empty($_REQUEST['stream_uniq_id'])) {
                        $movieStream = movieStreams::model()
                                ->with(array('film' => array('select' => 'f.id', 'alias' => 'f')))
                                ->find(array('select' => 't.id', 'condition' => 't.studio_id=:studio_id AND t.embed_id=:embed_id AND t.sdk_user_id=:sdk_user_id', 'params' => array(':studio_id' => $studio_id, ':embed_id' => $_REQUEST['stream_uniq_id'], ':sdk_user_id' => $this->user_id)));
                        $_REQUEST['movie_id'] = $movieStream->film['id'];
                        $_REQUEST['stream_id'] = $movieStream->id;
                    }
                    $_REQUEST['movie']['name'] = addslashes($_POST['movie']['name']);
                    if (!empty($_REQUEST['movie_id'])) {
                        $response = Yii::app()->usercontent->updateUgc($this->custom_metadata_form_id, $_REQUEST);
                    } else {
                        $response = Yii::app()->usercontent->saveUgc($this->custom_metadata_form_id, $_REQUEST);
                    }
                    if ($isAjax) {
                        $video_html = '';
                        if ($requestType == 'add') {
                            $video_html = self::actionvideoData($response['movie_stream_id']);
                        }
                        $res = $response;
                        $res['video_html'] = $video_html;
                        $res['requestType'] = $requestType;
                        Yii::app()->user->setFlash('success', $response['message']);
                    } else {
                        Yii::app()->user->setFlash('success', $response['message']);
                        if ($requestType == 'add') {
                            $url = $this->createUrl('ugc/editContent/stream_uniq_id/' . $response['movie_stream']['embed_id']);
                        } else {
                            $url = $this->createUrl('ugc/Showdetails');
                        }
                        $this->redirect($url);
                        exit;
                    }
                }
                echo json_encode($res);
            } else {
                Yii::app()->user->setFlash('error', $this->Language['oops_you_have_no_access']);
                $this->redirect($this->base_url);
                exit;
            }
        }
        exit;
    }

    function actionvideoData($stream_id = null) {
        Yii::app()->theme = 'bootstrap';
        $video_html = '';
        $studio_id = $this->studio->id;
        if (!$stream_id) {
            $stream_id = !empty($_REQUEST['movie_stream_id']) ? $_REQUEST['movie_stream_id'] : '';
        }
        if ($stream_id) {
            $movieStream = movieStreams::model()
                    ->with(array('film' => array('select' => 'f.id, f.content_types_id', 'alias' => 'f')))
                    ->find(array('select' => 't.id', 'condition' => 't.studio_id=:studio_id AND t.id=:id AND t.sdk_user_id=:sdk_user_id', 'params' => array(':studio_id' => $studio_id, ':id' => $stream_id, ':sdk_user_id' => $this->user_id)));

            $video_data = Yii::app()->usercontent->getUGCMoiveUrl($stream_id); /* only for single part video */
            $thirdparty_data = $this->renderPartial('player_third_party', array('third_party_url' => $video_data), true);

            $video_html = $this->renderPartial('video_upload', array('movieStream' => $movieStream, 'video_data' => $video_data, 'thirdparty_data' => $thirdparty_data), true);
        }
        return $video_html;
    }
    
    function customFormMetaDataId($type=null){
        /* type UGC VOD or UGC AOD */
        $studio_id = $this->studio->id;
        $ugcSinglePartId = CustomMetadataForm::model()->find(
                array('select'=>'id', 'condition'=>"name LIKE '%UGC%' AND name LIKE '%VOD%' AND content_types_id=1 AND studio_id='$studio_id'")
            )->id;
        if(!$ugcSinglePartId){
            $ugcSinglePartId = CustomMetadataForm::model()->find(
                array('select'=>'id', 'condition'=>"name LIKE '%UGC%' AND name LIKE '%VOD%' AND content_types_id=1 AND studio_id=0")
            )->id;
        }
        return $ugcSinglePartId;
    }

}
