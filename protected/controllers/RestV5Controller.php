<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/billingV5.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/contentV5.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/playerV5.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/webV5.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
class RestV5Controller extends Controller {
	use Player;
    use Billing;
    use Content;
    use Web;
    public $studio_id = '';
    public $items = array();
    public $code = '';
	public $msg_code = '';
	
	/**
	 * @method public Init : This is first method of this controller
	 * @author GDR<support@muvi.com>
	 * @return boolean
	 */
    public function init() {
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
        $headerinfo = getallheaders();
        foreach ($headerinfo AS $key => $val) {
            $_REQUEST[$key] = $val;
        }
        return true;
    }
    /**
	 * @method public beforeaction : This function will execute before action function
	 * @author GDR<support@muvi.com>
	 * @param type $action
	 * @return boolean
	 */
    protected function beforeAction($action) {
        parent::beforeAction($action);
        $currentAction = strtolower($action->id);
        if($currentAction != 'getstudioauthkey'){
            $_REQUEST['currentAction'] = $action->id;
            $success = self::validateOauth();
            if (!$success)
                self::afterAction();
        }
        $redirect_url = Yii::app()->getBaseUrl(true);
        return true;
    }
	/**
	 * @method public afteraction : This function will execute After  actual action function
	 * @author GDR<support@muvi.com>
	 * @param type $action
	 */
    protected function afterAction($action) {
        ob_clean();
        ob_flush();

        $response = array();
        if (empty($this->msg_code)) {			
            $config_message = parse_ini_file($_SERVER["DOCUMENT_ROOT"] . "/protected/config/api_config_message.ini", true);			
            $config_code = $config_message['api_code'][$this->code];        //Use your language here			
        } else {			
            $config_code = $this->msg_code;
        }		
        $response['msg'] =  TranslateKeyword::model()->getKeyValue(@$_REQUEST['lang_code'], $config_code, $this->studio_id);;
        $response['code'] = $this->code;
        $response['status'] = (intval($this->code) == 200) ? 'SUCCESS' : 'FAILURE';
        $response['items'] = (array) $this->items;
        header('HTTP/1.1 ' . $this->code . ' ' . $response['msg']);
        header('Content-type: application/json; charset=utf-8');
        header('X-Powered-By: Muvi <support@muvi.com>');
        echo json_encode($response);
        exit;
    }
	/**	 
	 * @method public gettransdata : This function will return language data
	 * @author Biswajit Parida<support@muvi.com>
	 * @param type String $lang_code : Language Code
	 * @param type Int $studio_id :Studio id
	 * @return type  array()
	 */
	function getTransData($lang_code='en', $studio_id){
        $studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
        $theme = $studio->theme;
        if(!$lang_code)
            $lang_code = 'en';

        if(file_exists(ROOT_DIR."languages/".$theme."/".trim($lang_code).".php")){
           $lang = include( ROOT_DIR."languages/".$theme."/".trim($lang_code).".php");
        }elseif(file_exists(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php')){
            $lang = include(ROOT_DIR . 'languages/studio/'.trim($lang_code).'.php');
        }else{
            $lang = include(ROOT_DIR . 'languages/studio/en.php');
        }
        $language = Yii::app()->controller->getAllTranslation($lang,$studio_id);
        return $language;
    }
	/**
	 * @method public Encrypt : This function will return encrypted string
	 * @param type String $value : String
	 * @return type string
	 */
    public function encrypt($value) {
        $enc = new bCrypt();
        return $enc->hash($value);
    }

    /**
     * @method private ValidateOauth() Validate the Oauth token if its registered to our database or not
     * @author GDR<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String muvi_token : muvi_token 
     * @return Boolean value
     */
    function validateOauth() {
        if (isset($_REQUEST) && (isset($_REQUEST['authToken']) || isset($_REQUEST['authtoken']) || isset($_REQUEST['muvi_token']))) {
            if (isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
                $muvi_token = explode('-', $_REQUEST['muvi_token']);
                $authToken = $muvi_token[1];
            } else {
                $authToken = (isset($_REQUEST['authtoken'])) ? trim($_REQUEST['authtoken']) : $_REQUEST['authToken'];
                $currentActionRequest = (isset($_REQUEST['currentAction'])) ? trim($_REQUEST['currentAction']) : $_REQUEST['currentAction'];
            }
            $referer = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                $referer = $referer['host'];
            }
            $data = Yii::app()->db->createCommand()
                    ->select(' * ')
                    ->from("oauth_registration")
                    ->where('oauth_token=:oauth_token', array(':oauth_token' => $authToken))
                    ->queryRow();
            if ($data) {
                $data = (object) $data;
                if ($data->request_domain && ($data->request_domain != $referer)) {
                    $this->code = 601;
                    return false;
                }
                if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
                    $this->code = 602;
                    return false;
                }
                $this->studio_id = $data->studio_id;
                //Access permission Read|Write purpose
                //If tocken active
                if($data->permission_status==1){
                    $Command = Yii::app()->db->createCommand()
                    ->select('t1.access_mode, t1.token, t1.studio_id, t2.api_name')
                    ->from('oauth_tocken_access_permission t1')
                    ->leftJoin('all_api_list t2','t2.id=t1.api_id')
                    ->where('t2.api_name=:api_name && t1.token=:auth_token && t1.studio_id=:studio_id',array(':api_name' => strtolower($currentActionRequest),':auth_token' => $authToken,':studio_id' => $data->studio_id));
                    $resdata = $Command->queryRow();
                    $res_data = array();
                    if($resdata){
                        if(($resdata['access_mode']==1)){ 
                            return TRUE;
                        }
                        else{
                           $this->code = 501;
                        }
                    }else{
                        $this->code = 502;
                    }
                } //If tocken deactive and permission status is not there...
                else{
                    return TRUE;
                }
            } else {
                $this->code = 601;
            }
        } else {
            $this->code = 600;
        }
        return false;
    }

	 /**
     * @method private login() Get login parameter from API request and Checks login and return response
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     * @param String authToken* : Auth token of the studio
     * @param String email* : Registered Email of user
     * @param String password* : Login password for the user
	 * @param String google_id : Google id 
	 * @param Integer device_type* : Device type(0->web,1->app)
	 * @param String device_id : Unique id of device
	 * @param String lang_code : User language Code	 
     */
    function actionLogin($req = array()) {
		$studio_id = $this->studio_id;
		if ($req) {
			$_REQUEST = $req;
		}
		if (isset($_REQUEST['email']) && $_REQUEST['email'] != '' && isset($_REQUEST['password']) && $_REQUEST['password'] != '') {
			$userData = SdkUser::model()->find(array('select' => 'id, email, encrypted_password, display_name, nick_name, add_video_log, is_free', "condition" => "email=:email AND studio_id =:studio_id AND status=1 AND is_deleted !=1", "order" => "id DESC", 'params' => array(':email' => trim($_REQUEST['email']), ':studio_id' => $studio_id)));
			$lang_code = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
			$translate = $this->getTransData($lang_code, $studio_id);
			if (!empty($userData)) {
				$enc = new bCrypt();
				$pwd = trim(@$_REQUEST['password']);
				$device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
				if (!$enc->verify(@$pwd, $userData->encrypted_password)) {
					$this->code = 751;
				} else {
					$user_id = $userData->id;
					$data['id'] = $user_id;
					$data['email'] = $userData->email;
					$data['display_name'] = $userData->display_name;
					$data['nick_name'] = $userData->nick_name;
					$data['studio_id'] = $studio_id;
					$data['is_free'] = $userData->is_free;
					$data['profile_image'] = $this->getProfilePicture($user_id, 'profilepicture', 'thumb', $studio_id);
					$isSubscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
					$data['is_subscribed'] = (int) $isSubscribed ? 1 : 0;
					$data['is_broadcaster'] = $userData->is_broadcaster;
					$data['language_list'] = Yii::app()->custom->getactiveLanguageList($studio_id);
					$custom_field_details = Yii::app()->custom->getCustomFieldsDetail($studio_id, $user_id);

					if (!empty($custom_field_details)) {
						foreach ($custom_field_details as $key => $val) {
							$data["custom_" . $key] = ($key == "languages") ? $val : $val[0];
						}
					}
					if ($userData->add_video_log) {
						$ip_address = CHttpRequest::getUserHostAddress();
						$user_id = $user_id;
						$login_at = date('Y-m-d H:i:s');
						$login_history = new LoginHistory();
						$login_history->studio_id = $this->studio_id;
						$login_history->user_id = $user_id;
						$login_history->login_at = $login_at;
						$login_history->logout_at = "0000-00-00 00:00:00";
						$login_history->last_login_check = $login_at;
						$login_history->google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
						$login_history->device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
						$login_history->device_type = $device_type;
						$login_history->ip = $ip_address;
						$login_history->save();
						$data['login_history_id'] = $login_history->id;
					}
					$this->code = 200;
					$this->items = $data;
				}
			} else {
				$this->code = 751;
			}
		} else {
			$this->code = 751;
		}
	}

	/**
     * @method LogoutAll : This function is used to logout from all devices with push notification
     * @author Biswajit Parida<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param String email* : Email of login user
	 * @param String lang_code : User language Code
     * @return json data 
     */
    public function actionLogoutAll(){
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $this->studio_id);                
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1));
            if ($userData) {
                $user_id = $userData->id;
				$sql="SELECT google_id FROM login_history WHERE studio_id={$this->studio_id} and user_id={$user_id} AND (device_type='1') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
				$registration_ids = Yii::app()->db->createCommand($sql)->queryAll();
				$sql2="SELECT google_id FROM login_history WHERE studio_id={$this->studio_id} and user_id={$user_id} AND (device_type='2') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
				$registration_ids2 = Yii::app()->db->createCommand($sql2)->queryAll();
				$reg_ids2= array();
				$reg_ids = array();				
				foreach($registration_ids as $ids){
                            $reg_ids [] = $ids['google_id'];
                    }
				foreach($registration_ids2 as $ids2){
					$reg_ids2 [] = $ids2['google_id'];
                }
				$title = $user_id;
				$message = $translate['logged_out_from_all_devices'];
                $push = new Push();
				$push->setTitle($title);
				$push->setMessage($message);
				//json data android
				$jsondata = $push->getPushAndroid();
				//json data ios
				$jsonda = $push->getPushIos();
				$response = $push->sendMultiple($reg_ids, $jsondata);
				$response2 = $push->sendMultipleNotify($reg_ids2, $jsonda);
                LoginHistory::model()->logoutUser($this->studio_id, $user_id);
                $this->code = 200;
                $this->msg_code = 'logged_out_from_all_devices';
            } else {
                $this->code = 752;
            }
        } else {
            $this->code = 757;
        }
    }  
	/**
     * @method Logout :This function is used to update logout_at field of login_history table
     * @author Biswajit Parida<support@muvi.com>
	 * @param String authToken* : Auth token of the studio
	 * @param Integer login_history_id* : Auto-increment id of login history table
	 * @param String lang_code : User language Code	 
     * @return json data 
     */
    public function actionLogout(){
        if(isset($_REQUEST['login_history_id']) && $_REQUEST['login_history_id']>0){
            $login_history = LoginHistory::model()->findByPk($_REQUEST['login_history_id']);
            $login_history->logout_at = date('Y-m-d H:i:s');
            $login_history->save();
            $this->code = 200;
        }else{
            $this->code = 679;
        }
    }

	/**
	 * @method public isRegistrationEnabled: It will check wheather the registration is enabled or not and if yes, then returns with signup step type
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param String lang_code
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionIsRegistrationEnabled() {
		$studioData = Studio::model()->findByPk($this->studio_id, array('select' => 'need_login, mylibrary_activated, rating_activated'));
		$is_registration = $studioData->need_login;
		$chromecast = $offline = 0;
		$app = Yii::app()->general->apps_count($this->studio_id);
		if (empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))) {
			$chromecast_val = StudioConfig::model()->getconfigvalueForStudio($this->studio_id, 'chromecast');
			if ($chromecast_val['config_value'] == 1 || $chromecast_val == '') {
				$chromecast = 1;
			}
			$offline_val = StudioConfig::model()->getconfigvalueForStudio($this->studio_id, 'offline_view');
			if ($offline_val['config_value'] == 1 || $offline_val == '') {
				$offline = 1;
			}
		}
		$data = array();
		if (intval($is_registration)) {
			$this->code = 200;
			$data['isMylibrary'] = (int) $studioData->mylibrary_activated;
			$data['isRating '] = (int) $studioData->rating_activated;
			$data['is_login'] = 1;
			$data['has_favourite'] = self::CheckFavouriteEnable($this->studio_id);
			$general = new GeneralFunction;
			$data['signup_step'] = $general->signupSteps($this->studio_id);
			$device_status = StudioConfig::model()->getConfig($this->studio_id, 'restrict_no_devices');
			$data['isRestrictDevice'] = (isset($device_status['config_value']) && ($device_status['config_value'] != 0)) ? 1 : 0;
			$getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($this->studio_id, 'restrict_streaming_device');
			$getDeviceRestrictionSetByStudio = 0;
			if (@$getDeviceRestrictionSetByStudioData->config_value != "" && @$getDeviceRestrictionSetByStudioData->config_value > 0) {
				$getDeviceRestrictionSetByStudio = 1;
			}
			$data['is_streaming_restriction'] = $getDeviceRestrictionSetByStudio;
		} else {
			$this->code = 646;
			$data['is_login'] = 0;
		}
		$data['chromecast'] = $chromecast;
		$data['is_offline'] = $offline;
		$this->items = $data;
	}
	
    /**
     * @method getstudioAds() It will return the add server details if enabled 
     * @author Gayadhar<support@muvi.com>
     * @return array  of details
     */
    function getStudioAds() {
        //Find the Ad Channel id if enabled for the studio
        $arr = array();
        $studioAds = StudioAds::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($studioAds) {
            if ($studioAds->ad_network_id == 1) {
                $data['Ads']['network'] = 'spotx';
            } elseif ($studioAds->ad_network_id == 2) {
                $arr['network'] = 'yume';
            }
            $arr['channelId'] = $studioAds->channel_id;
        }
        return $arr;
    }

    /**
     * @method public registerUser Register the user's details 
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $name Full Name of the User
     * @param String $email Email address of the User
     * @param String $password Login Password 
     * @param String $plan_id subscription plan id
	 * @param string card_name Name as on Credit card
     * @param String card_number Card Number 
     * @param String cvv CVV as on card
     * @param String expiry_month Expiry month as on Card
     * @param String expiry_year Expiry Year as on Card
     * @param String $card_last_fourdigit Last 4 digits of the card 
     * @param String $auth_num Authentication Number
     * @param String $token Payment Token
     * @param String $card_type Type of the card
     * @param String $reference_no Reference Number if any
     * @param String $response_text Response text as returned by Payment gateways
     * @param String $status Status of the payment gateways
     * @param String $profile_id User Profile id created in the payment gateways
     * @param Int $signup_step 1: Single, 2: 2-step, Default: single step
     * 
     */
    public function actionRegisterUser() {
		$data = array();
        if (isset($_REQUEST) && !empty($_REQUEST)) {
			if ($this->actionCheckEmailExistance($_REQUEST)) {
				$signup_step = (isset($_REQUEST['signup_step']) && intval($_REQUEST['signup_step'])) ? $_REQUEST['signup_step'] : 1;
        $_REQUEST['data'] = $_REQUEST;
				
				if ($signup_step == 1) {
				$is_register_success = 1;
				
				$plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
				if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
					$gateway_code = $plan_payment_gateway['gateways'][0]->short_code;
					$this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
				}
				
				$_REQUEST['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
				$_REQUEST['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
				$_REQUEST['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
				
				if (isset($_REQUEST['plan_id']) && intval($_REQUEST['plan_id'])) {
					if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
						$studio = Studios::model()->findByPk($this->studio_id);
						$default_currency_id = $studio->default_currency_id;
						$plan_details = SubscriptionPlans::model()->getPlanDetails($_REQUEST['plan_id'],$this->studio_id, $default_currency_id, $_REQUEST['country']);
                        if (isset($plan_details) && !empty($plan_details) && isset($plan_details['trial_period']) && intval($plan_details['trial_period']) == 0) {
                            $price = $plan_details['price'];
                            $currency_id = $plan_details['currency_id'];
                            $currency = Currency::model()->findByPk($currency_id);
							
							$card = array();
							$_REQUEST['data']['currency_id'] = $card['currency_id'] = $currency->id;
                            $card['currency_code'] = $currency->code;
                            $card['currency_symbol'] = $currency->symbol;
                            $card['amount'] = $price;
                            $card['token'] = @$_REQUEST['token'];
                            $card['profile_id'] = @$_REQUEST['profile_id'];
                            $card['card_holder_name'] = @$_REQUEST['card_name'];
                            $card['card_type'] = @$_REQUEST['card_type'];
                            $card['exp_month'] = $_REQUEST['exp_month'];
                            $card['exp_year'] = $_REQUEST['exp_year'];
                            $card['studio_name'] = $studio->name;
							
							$payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
							Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
							$payment_gateway = new $payment_gateway_controller();
                            $trans_data = $payment_gateway::processTransactions($card);
							
							if (intval($trans_data['is_success'])) {
								$_REQUEST['data']['transaction_data']['transaction_status'] = $trans_data['transaction_status'];
								$_REQUEST['data']['transaction_data']['invoice_id'] = $trans_data['invoice_id'];
								$_REQUEST['data']['transaction_data']['order_number'] = $trans_data['order_number'];
								$_REQUEST['data']['transaction_data']['dollar_amount'] = $trans_data['dollar_amount'];
								$_REQUEST['data']['transaction_data']['amount'] = $trans_data['amount'];
								$_REQUEST['data']['transaction_data']['response_text'] = $trans_data['response_text'];
								$_REQUEST['data']['transaction_data']['is_success'] = $trans_data['is_success'];
                            } else {
								$is_register_success = 0;
                                $data = $trans_data;
									$this->code = 622;
                            }
						}
					} else {
						$is_register_success = 0;
						$this->code = 612;
					}
				}
				
				if (intval($is_register_success)) {
					$ret = SdkUser::model()->saveSdkUser($_REQUEST, $this->studio_id);
					if ($ret) {
						$is_login = 1;
						if (@$ret['is_subscribed']) {
							$file_name = '';

							if (isset($_REQUEST['data']['transaction_data']['is_success']) && intval($_REQUEST['data']['transaction_data']['is_success'])) {
								$user = array();
								$user['amount'] = Yii::app()->common->formatPrice($_REQUEST['data']['transaction_data']['amount'], $_REQUEST['data']['currency_id'], 1);
								$user['card_holder_name'] = $_REQUEST['data']['card_name'];
								$user['card_last_fourdigit'] = $_REQUEST['data']['card_last_fourdigit'];

								$file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_REQUEST['data']['transaction_data']['invoice_id']);
							}
							$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email_with_subscription', $file_name);
							$admin_welcome_email = $this->sendStudioAdminEmails($this->studio_id, 'admin_new_paying_customer', $ret['user_id'], '', 0);
						} else {
							if (@$ret['error']) {
								$is_login = 0;
								$this->code = 661;
							} else {
								$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email');
							}
						}
						
						if ($is_login) {
							$_REQUEST['sdk_user_id'] = $ret['user_id'];
								/*if ($_REQUEST['livestream'] == 1) {
									$this->registerStreamUser($_REQUEST);
								}*/
							$this->actionLogin($_REQUEST);
						}
					} else {
						$this->code = 661;
					}
				} else {
					$this->code = 661;
				}
				} elseif ($signup_step == 2) {
					$ret = SdkUser::model()->saveUserDetails($_REQUEST, $this->studio_id);
					
					if (@$ret['user_id']) {
						$_REQUEST['sdk_user_id'] = $ret['user_id'];
						$welcome_email = Yii::app()->common->getStudioEmails($this->studio_id, $ret['user_id'], 'welcome_email');
						/*if ($_REQUEST['livestream'] == 1) {
							$this->registerStreamUser($_REQUEST);
						}*/
						$this->actionLogin($_REQUEST);
			} else {
				$this->code = 661;
			}
				}
		} else {
			$this->code = 661;
		}
		} else {
			$this->code = 661;
		}
		
		$this->items = $data;
	}
	
    /**
	 * @method public registerstreamUser($request) It will register the stream user and create a channel id
	 * @return json Return the json array of results
	 * @author Gayadhar<support@muvi.com>	
	 */
	function registerStreamUser($request) {
		$lsuser = new LivestreamUsers;
		$lsuser->name = $request['name'];
		$lsuser->nick_name = @$request['nick_name'] ? @$request['nick_name'] : $request['name'];
		$lsuser->email = $request['email'];
		$lsuser->sdk_user_id = $request['sdk_user_id'];
		$lsuser->studio_id = $this->studio_id;
		$lsuser->ip = Yii::app()->request->getUserHostAddress();
		$lsuser->status = 1;
		$lsuser->created_date = new CDbExpression("NOW()");
		$return = $lsuser->save();
		$lsuser_id = $lsuser->id;
		$push_url = RTMP_URL . $this->studio_id . '/' . $lsuser->channel_id;
		$hls_path = HLS_PATH . $this->studio_id;
		if (!is_dir($hls_path)) {
			mkdir($hls_path, 0777, TRUE);
			chmod($hls_path, 0777);
		}
		if (!is_dir($hls_path . '/' . $lsuser->channel_id)) {
			mkdir($hls_path . '/' . $lsuser->channel_id, 0777, TRUE);
			chmod($hls_path . '/' . $lsuser->channel_id, 0777);
		}
		$hls_path = $hls_path . '/' . $lsuser->channel_id;
		$hls_url = HLS_URL . $this->studio_id . '/' . $lsuser->channel_id . '/' . $lsuser->channel_id . '.m3u8';
		$lsuser->pull_url = $hls_url;
		$lsuser->push_url = $push_url;
		$lsuser->save();
		$_REQUEST['rtmp_path'] = $push_url;
		return true;
	}

	/**
     * @method public checkEmailExistance($email) It will check if an email exists with the studio or not
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $email Email to be validated
     */
    public function actionCheckEmailExistance($req = array()) {
        if ($req)
            $_REQUEST = $req;
        $code = 752;$email_status = 0;       
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id));
            if (isset($users) && !empty($users)) {
                $code = 200;
                $email_status = 1;
            }
        }
        $this->code = $code;
        $this->items = array('isExists' => $email_status);
        return true;
    }

    public function actionPpvpayment() {
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);

        $data = array();
        $data['movie_id'] = @$_REQUEST['movie_id'];
        $data['season_id'] = @$_REQUEST['season_id'];
        $data['episode_id'] = @$_REQUEST['episode_id'];
        $data['card_holder_name'] = @$_REQUEST['card_name'];
        $data['card_number'] = @$_REQUEST['card_number'];
        $data['exp_month'] = @$_REQUEST['exp_month'];
        $data['exp_year'] = @$_REQUEST['exp_year'];
        $data['cvv'] = @$_REQUEST['cvv'];
        $data['profile_id'] = @$_REQUEST['profile_id'];
        $data['card_type'] = @$_REQUEST['card_type'];
        $data['card_last_fourdigit'] = @$_REQUEST['card_last_fourdigit'];
        $data['token'] = @$_REQUEST['token'];
        $data['email'] = @$_REQUEST['email'];
        $data['coupon_code'] = @$_REQUEST['coupon_code'];
        $data['is_advance'] = @$_REQUEST['is_advance'];
        $data['currency_id'] = @$_REQUEST['currency_id'];
        
        $data['is_save_this_card'] = @$_REQUEST['is_save_this_card'];
        $data['existing_card_id'] = @$_REQUEST['existing_card_id'];
        
        $data['studio_id'] = $studio_id;
        $data['user_id'] = $user_id;
        
        $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
        $video_id = $Films->id;
        $plan_id = 0;

        //Check studio has plan or not
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id, $studio_id);

        //Check ppv plan or advance plan set or not by studio admin
        $is_ppv = $is_advance = 0;
        if (intval($data['is_advance']) == 0) {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id, 0);
            if (isset($plan->id) && intval($plan->id)) {
                $is_ppv = 1;
            } else {
                $plan = Yii::app()->common->getContentPaymentType($Films->content_types_id, $Films->ppv_plan_id, $this->studio_id);
                $is_ppv = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
            }
        } else  {
            $plan = Yii::app()->common->checkAdvancePurchase($video_id, $this->studio_id);
            $is_advance = (isset($plan->id) && intval($plan->id)) ? 1 : 0;
        }
        
        if (isset($plan) && !empty($plan)) {
            $plan_id = $plan->id;
            $price = Yii::app()->common->getPPVPrices($plan_id, $data['currency_id'], $_REQUEST['country']);
            $currency = Currency::model()->findByPk($price['currency_id']);
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;

            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');

            $end_date = '';
            if (intval($is_advance) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;
                        
                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period.' '.strtolower($watch_period_access->validity_recurrence)."s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period.' '.strtolower($access_period_access->validity_recurrence)."s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
            }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }

            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $price['season_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $price['show_unsubscribed'];
                        }

                        //$end_date = ''; //Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $price['price_for_unsubscribed'];
                }
            }

            $couponCode = '';
            //Calculate coupon if exists
            if (isset($data['coupon_code']) && $data['coupon_code'] != '') {
                $getCoup = Yii::app()->common->getCouponDiscount($data['coupon_code'], $data['amount'], $studio_id, $user_id, $data['currency_id']);
                $data['amount'] = $getCoup["amount"];
                $couponCode = $getCoup["couponCode"];
            }
                
            $gateway_code = '';
            
            if (isset($data['existing_card_id']) && trim($data['existing_card_id'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('card_uniq_id' => $data['existing_card_id'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;

                $gateway_info = StudioPaymentGateways::model()->findAllByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                $gateway_code = $gateway_info[0]->short_code;
            } else {
                $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                    $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($this->studio_id);
                    $gateway_info = $plan_payment_gateway['gateways'];
                }
            }
            
            $this->setPaymentGatwayVariable($gateway_info);
            
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);

                $VideoDetails = $VideoName;
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($data['episode_id']);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }

                if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypal') {
                    
                } else if (abs($data['amount']) < 0.01) {
                    
                    if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }
                    
                    $data['amount'] = 0;
                    $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                    $ppv_apv_email = Yii::app()->billing->sendPpvEmail(array(), $data, 0, $is_advance, $VideoName, $VideoDetails);
                    
                    $res['code'] = 200;
                    $res['status'] = "OK";
                } else {
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $VideoDetails.' - '.$data['amount'];
                    
                    if($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])){
                        
                    }
                    
                    if(isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez'){
                        //echo json_encode($data);exit;
                    }
                    $data['studio_name'] = Studios::model()->findByPk($studio_id)->name;
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $trans_data = $payment_gateway::processTransactions($data);
                    
                    $is_paid = $trans_data['is_success'];
                    if (intval($is_paid)) {
                        $data['amount'] = $trans_data['amount'];
                        if (isset($data['is_save_this_card']) && intval($data['is_save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }
                        
                        $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan,$data,$video_id,$start_date,$end_date,$data['coupon_code'], $is_advance, $gateway_code);
                        $ppv_subscription_id = $set_ppv_subscription;
                        
                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan,$data,$video_id,$trans_data, $ppv_subscription_id, $gateway_code, $is_advance);
                        $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $is_advance, $VideoName, $VideoDetails);
                        
                        $res['code'] = 200;
                        $res['status'] = "OK";
                    } else {
                        $res['code'] = 411;
                        $res['status'] = "Error";
                        $res['msg'] = $translate['error_transc_process'];
                        $res['response_text'] = $trans_data['response_text'];
                    }
                }
            } else {
                $res['code'] = 411;
                $res['status'] = "Error";
                $res['msg'] = "Studio has no payment gateway";
            }
        } else {
            $res['code'] = 411;
            $res['status'] = "Error";
            $res['msg'] = "Studio has no PPV plan";
        }

        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }


    /**
     * @method converttimetoSec(string $str) convert a string of HH:mm:ss to second
     * @author Gayadhar<support@muvi.com>
     * @return int 
     */
    function convertTimetoSec($str_time) {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }
	
	
	function uploadProfilePics($user_id,$studio_id,$fileinfo){
		if(!$studio_id){$studio_id = $this->studio_id;}

		//Checking for existing object
		$old_poster = new Poster;
		$old_posters = $old_poster->findAllByAttributes(array('object_id' => $user_id, 'object_type' => 'profilepicture'));
		if(count($old_posters) > 0){
			foreach ($old_posters as $oldposter) {
				$oldposter->delete();
			}
		}
		$ip_address = CHttpRequest::getUserHostAddress(); 
		$poster = new Poster();
		$poster->object_id = $user_id;
		$poster->object_type = 'profilepicture';
		$poster->poster_file_name = $fileinfo['name'];
		$poster->ip = $ip_address;
		$poster->created_by = $user_id;
		$poster->created_date = new CDbExpression("NOW()");
		$poster->save();
		$poster_id = $poster->id;                    
		
		$fileinfo['name'] = Yii::app()->common->fileName($fileinfo['name']);
		$_FILES['Filedata'] = $fileinfo;
		$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/profile_images/'.$poster_id;
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		$dir = $dir.'/original';
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		move_uploaded_file($fileinfo['tmp_name'], $dir.'/'.$fileinfo['name']);
		
		//$uid = $_REQUEST['movie_id'];
		require_once "Image.class.php";
		require_once "Config.class.php";
		require_once "ProfileUploader.class.php";
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		require_once "amazon_sdk/sdk.class.php";
		spl_autoload_register(array('YiiBase', 'autoload'));

		define( "BASEPATH",dirname(__FILE__) . "/.." );
		$config = Config::getInstance();
		$config->setUploadDir($_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'images/public/system/profile_images'); //path for images uploaded
		$bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
		$config->setBucketName($bucketInfo['bucket_name']);
		$s3_config = Yii::app()->common->getS3Details($studio_id);
		$config->setS3Key($s3_config['key']);
		$config->setS3Secret($s3_config['secret']);
		$config->setAmount( 250 );  //maximum paralell uploads
		$config->setMimeTypes( array( "jpg" , "gif" , "png",'jpeg' ) ); //allowed extensions
		$config->setDimensions( array('small'=>"150x150",'thumb'=>"100x100") );   //resize to these sizes
		//usage of uploader class - this simple :)
		$uploader = new ProfileUploader($poster_id);
		$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
		$unsignedBucketPath = $folderPath['unsignedFolderPath'];
		$ret = $uploader->uploadprofileThumb($unsignedBucketPath."public/system/profile_images/");
		return true;
	}
/**
* @method GetStatByType() Return the 
* @param String $authToken A authToken
* @param array $filterType filter on specific type like genre, language,rating etc
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
    function actionGetStatByType() {
        if ($_REQUEST['filterType']) {
            if (strtolower(trim($_REQUEST['filterType'])) == 'genre') {
                $genreArr = array();
                $gsql = 'SELECT genre, COUNT(*) AS cnt FROM films WHERE studio_id =' . $this->studio_id . ' AND genre is not NULL AND genre !=\'\' AND genre !=\'null\' GROUP BY genre';
                $res = Yii::app()->db->createCommand($gsql)->queryAll();
                if ($res) {
                    foreach ($res As $key => $val) {
                        $expArr = json_decode($val['genre'], TRUE);
                        if (is_array($expArr)) {
                            foreach ($expArr AS $k => $v) {
                                $mygenre = strtolower(trim($v));
                                $data['content_statistics'][$mygenre] += $val['cnt'];
                            }
                        }
                    }
                    $this->code = 200; 
                    $this->items = @$data; 
                    return true;                    
                }
                $this->code = 642;
                return true;
            }            
        } 
        $this->code = 641;
        return true;        
    }
        


    function getDomainName(){
        $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=".$this->studio_id;
        $stData = Yii::app()->db->createCommand($sql)->queryAll();
        $domainName = Yii::app()->getBaseUrl(TRUE);
        if(@$stData[0]['is_embed_white_labled']){
                 $domainName = @$stData[0]['domain']? 'http://'.@$stData[0]['domain']:$domainName; 
        }
        return $domainName;
    }
    /**
	 * @method getuniqueidencode(string $str) convert a string to encoded string
	 * @author Gayadhar<support@muvi.com>
	 * @param type String $str
	 * @return type String
	 */
    function getUniqueIdEncode($str){
       for($i=0; $i<5;$i++){
            //apply base64 first and then reverse the string
            $str=strrev(base64_encode($str));
        }
            return $str;
    }
	/**
	 * @method getuniqueiddecode(string $str) convert a encoded string to normal string
	 * @author Gayadhar<support@muvi.com>
	 * @param type String $str
	 * @return type String
	 */
    function getUniqueIdDecode($str){
         for($i=0; $i<5;$i++){
            //apply reverse the string first and then base64
            $str=base64_decode(strrev($str));
        }
            return $str;
    }

/**
 * @purpose get the cast and crew
 * @param movie_id Mandatory
 * @Note Applicable only for single part, multipart content. Not for live streams and episodes
 * @Author <ajit@muvi.com>
 */
    function actiongetCelibrity(){
		$movie_uniqueid = @$_REQUEST['movie_id'];
		if ($movie_uniqueid) {
			$studio_id = $this->studio_id;
			$Films = Film::model()->find(array('select' => 'id', 'condition' => 'studio_id=:studio_id and uniq_id=:uniq_id', 'params' => array(':studio_id' => $studio_id, ':uniq_id' => $movie_uniqueid)));
			$movie_id = $Films->id;
			if ($movie_id) {
				$sql = "SELECT C.id,C.name,C.birthdate,C.birthplace,C.summary,C.twitterid,C.permalink,C.alias_name,C.meta_title,C.meta_description,C.meta_keywords FROM celebrities C WHERE C.id IN(SELECT celebrity_id FROM movie_casts MC WHERE MC.movie_id = " . $movie_id . ") AND C.studio_id = " . $studio_id;
				$command = Yii::app()->db->createCommand($sql);
				$list = $command->queryAll();
				if ($list) {
					foreach ($list as $key => $val) {
						//get celebrity type
						$cast_name = MovieCast::model()->find(array('select' => 'cast_type', 'condition' => 'celebrity_id=:celebrity_id', 'params' => array(':celebrity_id' => $val['id'])));
						$celebrity[$key]['name'] = $val['name'];
						$celebrity[$key]['permalink'] = $val['permalink'];
						$celebrity[$key]['summary'] = $val['summary'];
						$celebrity[$key]['cast_type'] = $cast_name->cast_type;
						$celebrity[$key]['celebrity_image'] = $this->getPoster($val['id'], 'celebrity', 'medium', $studio_id);
					}
					$data['code'] = 200;
					$data['status'] = "OK";
					$data['celibrity'] = $celebrity;
				} else {
					$data['code'] = 448;
					$data['status'] = "Failure";
					$data['msg'] = "Celebrity not found.";
				}
			} else {
				$data['code'] = 448;
				$data['status'] = "Failure";
				$data['msg'] = "Records for this id not found";
			}
		} else {
			$data['code'] = 448;
			$data['status'] = "Failure";
			$data['msg'] = "Required parameters not found";
		}
		$this->setHeader($data['code']);
		echo json_encode($data);
		exit;
	}

	function actiongetEmojis(){
        $emo = Emojis::model()->findAll();        
            if($emo){
            foreach($emo as $key => $val){                
                $set[$key]['code'] = $val['code'];
                $set[$key]['name'] = $val['emoji_name'];
                $set[$key]['url'] = $val['emoji_url'];
            }
                $data['code'] = 200;
                $data['status'] = "OK";
            $data['emojis'] = $set;
            }else{
                $data['code'] = 448;
                $data['status'] = "Failure";
                $data['msg'] = "Emoji not found.";            
            }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
        
    /**
 * @method public GetStudioAuthKey() It will validate the login credential & return the authToken for that studio. 
 * @author Gayadhar<support@muvi.com>
 * @param String authToken* : Auth token of the studio
 * @param String email* :Login email address
 * @param String password* : studio login password
 * @param String lang_code : User language Code
 * @return JSON will return the authToken for the respective studio
 */	
function actionGetStudioAuthkey() {
		if (isset($_REQUEST['email']) && $_REQUEST['email'] != '' && isset($_REQUEST['password']) && $_REQUEST['password'] != '') {
			$userData = User::model()->find('email=:email AND role_id =:role_id', array(':email' => $_REQUEST['email'], 'role_id' => 1));
			if ($userData) {
				$enc = new bCrypt();
				if ($enc->verify($_REQUEST['password'], $userData->encrypted_password)) {
					$sql = "SELECT * FROM oauth_registration WHERE studio_id={$userData->studio_id} and status=1";
					$oauthData = Yii::app()->db->createCommand($sql)->queryRow();
					if ($oauthData) {
						$referer = '';
						if (isset($_SERVER['HTTP_REFERER'])) {
							$referer = parse_url($_SERVER['HTTP_REFERER']);
							$referer = $referer['host'];
						}
						$oauthData = (object) $oauthData;
						if ($oauthData->request_domain && ($oauthData->request_domain != $referer)) {
							$this->code = 740;
						} else if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
							$this->code = 602;
						} else {
							$this->code = 200;
							$this->items['authToken'] = $oauthData->oauth_token;
						}
					} else
						$this->code = 739;
				} else
					$this->code = 603;
			} else
				$this->code = 738;
		} else
			$this->code = 603;
	}

	/* APIs for managing favourite contents */

    function CheckFavouriteEnable($studio_id) {
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_favourite_list');
        if (!empty($fav_status)) {
            $result = $fav_status['config_value'];
        } else {
            $result = 0;
        }
        return $result;
    }
    public function actionTestTrans(){
        $trans  = TranslateKeyword::model()->getKeyValue('en', 'btn_login', $this->studio_id);
        echo $trans;exit;
    }
    
    public function getUserData($userData = array(), $custom_fields = false, $send_lang_list = ''){
        $data['id'] = $userData->id;
        $data['email'] = $userData->email;
        $data['display_name'] = $userData->display_name;
        $data['nick_name'] = $userData->nick_name;        
        $data['isFree'] = (int) @$userData->is_free;
        $data['is_newuser'] = 0;
        $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb');            
        $data['isSubscribed'] = (int) Yii::app()->common->isSubscribed($userData->id, $this->studio_id) ? 1 : 0;  
        if(isset($userData->is_broadcaster))
            $data['is_broadcaster'] = (int) $userData->is_broadcaster;
        if(isset($send_lang_list))
            $data['language_list'] = Yii::app()->custom->getactiveLanguageList($this->studio_id);        
        if($custom_fields){
            $custom_field_details = Yii::app()->custom->getCustomFieldsDetail($this->studio_id, $userData->id);
            if(!empty($custom_field_details)){
                foreach($custom_field_details as $key=>$val){
                    if($key == "languages")
                        $data["custom_".$key] = $val;
                    else
                        $data["custom_".$key] = $val[0];
                }
            }            
        }
        return $data;
    }
	/**@method  return scr part from thirdparty url
     * @author SKM<prakash@muvi.com>
     * @return string
     */
    public function getSrcFromThirdPartyUrl($thirdPartyUrl=''){
        if($thirdPartyUrl!=''){
            preg_match('/src="([^"]+)"/',$thirdPartyUrl, $match);
            if(!empty($match[1])){
                return $match[1];
            }else{
                preg_match("/src='([^']+)'/",$thirdPartyUrl, $match);
                if(!empty($match[1])){
                   return $match[1]; 
                }else{
                   return $thirdPartyUrl; 
                }
            }          
        }else{
            return $thirdPartyUrl;
        }
    }  	
	/**
	 * @method public GetImageForDownload : This function will return s3 url of image
	 * @return json Return the json array of results
	 * @author Srutikanta<support@muvi.com>
	 * @param String authToken* : Auth token of the studio	 
	 * @param Integer internet_speed : User internet speed
	 * @param String lang_code : User language Code
	 * 
	*/
	function actionGetImageForDownload() {
        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);		
        $image_url = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/check-download-speed.jpg";
		$this->code = 200;
		$this->items['image_url']=$image_url;        
	}
}