<?php
header("Access-Control-Allow-Origin: *");
header('content-type: application/json; charset=utf-8');
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/PushNotification.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/webV4.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/contentV4.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/billingV4.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/controllers/api/playerV4.php';
class RestController extends Controller {
    use WebV4;
    use ContentV4;
    use BillingV4;
    use PlayerV4;

    public $defaultAction = '';
    public $studio_id = '';
    public function init() {
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
//      parent::init();
        $headerinfo = getallheaders();
        $authstatus = 0;
        foreach ($headerinfo AS $key => $val) {
            if($key == "authToken"){
                $authstatus = 1 ;
            }
            $_REQUEST[$key] = $val;
        }        
        if($authstatus != 1){
            $requestBody = file_get_contents('php://input');
            $requestBody = json_decode($requestBody);
            foreach ($requestBody AS $key => $val) {
                $_REQUEST[$key] = $val;
            }
        }        
        return true;
    }

    //oauth operations with permission checking
    protected function beforeAction($action) {
        parent::beforeAction($action);
		$currentAction = strtolower($action->id);
		if($currentAction != 'getstudioauthkey'){
            $_REQUEST['currentAction'] = $currentAction;
            $resultSet = $this->validateOauth($_REQUEST);
		}
		$redirect_url = Yii::app()->getBaseUrl(true);
        return true;
    }

    public function encrypt($value) {
        $enc = new bCrypt();
        return $enc->hash($value);
    }

    /**
     * @method private ValidateOauth() Validate the Oauth token if its registered to our database or not
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     */
    function validateOauth() {
        if (isset($_REQUEST) && (isset($_REQUEST['authToken']) || isset($_REQUEST['authtoken']) || isset($_REQUEST['muvi_token']))) {
            if(isset($_REQUEST['muvi_token']) && trim($_REQUEST['muvi_token'])) {
                $muvi_token = explode('-',$_REQUEST['muvi_token']);
                $authToken = $muvi_token[1];
            }else{
                $authToken = (isset($_REQUEST['authtoken'])) ? trim($_REQUEST['authtoken']) : $_REQUEST['authToken'];
                $currentActionRequest = (isset($_REQUEST['currentAction'])) ? trim($_REQUEST['currentAction']) : $_REQUEST['currentAction'];
            }
            $referer = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $referer = parse_url($_SERVER['HTTP_REFERER']);
                $referer = $referer['host'];
            }
			$command = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('oauth_registration')
                    ->where('oauth_token=:oauth_token', array(':oauth_token'=>$authToken));
            $data = $command->queryRow();
            if ($data) {
                $data = (object) $data;
                if ($data->request_domain && ($data->request_domain != $referer)) {
                    $data['code'] = 409;
                    $data['status'] = "failure";
                    $data['msg'] = "Domain not registered with Muvi for API calls!";
                    echo json_encode($data);
                    exit;
                }
                if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
                    $data['code'] = 410;
                    $data['status'] = "failure";
                    $data['msg'] = "Oauth Token expired!";
                    echo json_encode($data);
                    exit;
                }
				$flag = self::check_studio($data->studio_id);
				if($flag == 'error'){
					$data1['code'] = 410;
                    $data1['status'] = "failure";
                    $data1['msg'] = "Oauth Token expired!";
                    echo json_encode($data1);
                    exit;
				}
                $this->studio_id = $data->studio_id;
                //Access permission Read|Write purpose
                //If tocken active
                if($data->permission_status==1){
                    $Command = Yii::app()->db->createCommand()
                    ->select('t1.access_mode, t1.token, t1.studio_id, t2.api_name')
                    ->from('oauth_tocken_access_permission t1')
                    ->leftJoin('all_api_list t2','t2.id=t1.api_id')
                    ->where('t2.api_name=:api_name && t1.token=:auth_token && t1.studio_id=:studio_id',array(':api_name' => strtolower($currentActionRequest),':auth_token' => $authToken,':studio_id' => $data->studio_id));
                    $resdata = $Command->queryRow();
                    $res_data = array();
                    if($resdata){
                        if(($resdata['access_mode']==1)){ 
                            return TRUE;
                        }
                        else{
                            $res_data['code'] = 501;
                            $res_data['status'] = "failure";
                            $res_data['msg'] = "Invalid access permission!";
                            echo json_encode($res_data);
                            exit;
                        }
                    }else{
                        $res_data['code'] = 502;
                        $res_data['status'] = "failure";
                        $res_data['msg'] = "Unauthorized to access!";
                        echo json_encode($res_data);
                        exit;
                    }
                } //If tocken deactive and permission status is not there...
                else{
                    return TRUE;
                }
            } else {
                $data['code'] = 408;
                $data['status'] = "failure";
                $data['msg'] = "Invalid Oauth Token!";
                echo json_encode($data);
                exit;
            }
        } else {
            $data['code'] = 407;
            $data['status'] = "failure";
            $data['msg'] = "Ouath Token required!";
            echo json_encode($data);
            exit;
        }
    }

	function check_studio($studio_id){
		if($studio_id){
			$studio = Studio::model()->findByPk($studio_id);
			if(($studio->is_subscribed == 0) && ($studio->status == 4) && ($studio->is_deleted == 0) && ($studio->is_default == 0)){
				return 'error'; // Cancelled
			} else if(($studio->is_subscribed == 0) && ($studio->status == 0) && ($studio->is_deleted == 1) && ($studio->is_default == 0)){
				return 'error'; // Deleted
			} else {
				return 'success';
			}
		}else{
			return 'success';
		}
	}
	 /**
     * @method private login() Get login parameter from API request and Checks login and return response
     * @author GDR<support@muvi.com>
     * @return json Json data with parameters
     * @param string $authToken Ouath Token generated during registartion.
     * @param string $email Registered Email
     * @param string $password Login password for the user
     */
    function actionLogin($req = array()) {
        $studio_id=$this->studio_id;
        if ($req) {
            $_REQUEST = $req;
        }
        if (isset($_REQUEST) && isset($_REQUEST['email']) && isset($_REQUEST['password'])) {
            //$userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1));
            $lang_code   = (isset($_REQUEST['lang_code']) && $_REQUEST['lang_code'] != "") ? $_REQUEST['lang_code'] : 'en';
            $translate   = $this->getTransData($lang_code, $studio_id);
            $sql = "SELECT id, email, encrypted_password, display_name, nick_name, add_video_log, is_free FROM sdk_users WHERE email = '" . trim($_REQUEST['email']) . "' AND studio_id = '" . $studio_id . "' AND status = 1 AND is_deleted != 1 ORDER BY id DESC LIMIT 1 OFFSET 0";
            $connection = Yii::app()->db;
            $userData = (object) $connection->createCommand($sql)->queryRow();
            
            if (!empty($userData)) {
                $enc = new bCrypt();
                $pwd = trim(@$_REQUEST['password']);
                $device_type=$_REQUEST['device_type'];
                if (!$enc->verify(@$pwd, $userData->encrypted_password)) {
                    $data['code'] = 406;
                    $data['status'] = "failure";
                    $data['msg'] = "Email or Password is invalid!";
                } else {
                    $user_id = $userData->id;
                    $data['id'] = $user_id;
                    $data['email'] = $userData->email;
                    $data['display_name'] = $userData->display_name;
                    $data['nick_name'] = $userData->nick_name;
                    $data['studio_id'] = $studio_id;
                    $data['isFree']=$userData->is_free;
                    $data['profile_image'] = $this->getProfilePicture($user_id, 'profilepicture', 'thumb', $studio_id);
                    $isSubscribed = Yii::app()->common->isSubscribed($user_id, $studio_id);
                    $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
                    $data['is_broadcaster'] = $userData->is_broadcaster;
					$data['language_list'] = Yii::app()->custom->getactiveLanguageList($studio_id);
					$custom_field_details = Yii::app()->custom->getCustomFieldsDetail($studio_id,$user_id);

                    if(!empty($custom_field_details)){
                        foreach($custom_field_details as $key=>$val){
                            $data["custom_".$key] = ($key == "languages") ? $val : $val[0];
                        }
                    }
                    if ($userData->add_video_log){
                            $ip_address = CHttpRequest::getUserHostAddress();
                            $user_id = $user_id;
                            $login_at = date('Y-m-d H:i:s');
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $this->studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = $login_at;
                        $login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = $login_at;
                        $login_history->google_id = isset($_REQUEST['google_id']) ? @$_REQUEST['google_id'] : '';
                        $login_history->device_id = isset($_REQUEST['device_id']) ? @$_REQUEST['device_id'] : '';
                        $login_history->device_type = isset($_REQUEST['device_type']) ? @$_REQUEST['device_type'] : '1';
                        $login_history->ip = $ip_address;
                        $login_history->save();
                        $data['login_history_id'] = $login_history->id;
                    }
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Login Success";
                }
            } else {
                $data['code'] = 406;
                $data['status'] = "failure";
                $data['msg'] = "Email or Password is invalid!";
            }
        } else {
            $data['code'] = 406;
            $data['status'] = "failure";
            $data['msg'] = "Email or Password is invalid!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }
    
     /*By Biswajit das(biswajitdas@muvi.com)(for check whether user login or not)*/
    public function actionCheckIfUserLoggedIn(){
        $studio_id=$this->studio_id;
        $user_id=$_REQUEST['user_id'];
        $device_type=$_REQUEST['device_type'];
        $device_id=$_REQUEST['device_id'];
        $sql = "SELECT * FROM `login_history` WHERE `studio_id` = {$studio_id} AND `user_id` = {$user_id} AND `device_type`= '{$device_type}' AND `device_id`= '{$device_id}' AND  (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
        $login_data = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($login_data)){
            $data['code'] = 200;
            $data['is_login'] = '1';
        }
        else{
            $data['code'] = 200;
            $data['is_login'] = '0';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    
     public function actionLogoutAll(){
        $studio_id=$this->studio_id;
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code, $studio_id);
        $json = '';
        $response = '';
        
        if (isset($_REQUEST) && isset($_REQUEST['email_id'])) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND is_deleted !=1', array(':email' => $_REQUEST['email_id'], ':studio_id' => $this->studio_id, ':status' => 1));
            if($userData){   
            $user_id = $userData->id;
            $sql="SELECT google_id FROM login_history WHERE studio_id={$studio_id} and user_id={$user_id} AND (device_type='1') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
            $registration_ids = Yii::app()->db->createCommand($sql)->queryAll();
            $sql2="SELECT google_id FROM login_history WHERE studio_id={$studio_id} and user_id={$user_id} AND (device_type='2') AND (`logout_at`='0000-00-00 00:00:00' OR `logout_at` IS NULL)";
            $registration_ids2 = Yii::app()->db->createCommand($sql2)->queryAll();
            $reg_ids2= array();
            $reg_ids = array();
            if(!empty($registration_ids)){
                foreach($registration_ids as $ids){
                    $reg_ids [] = $ids['google_id'];
                }
            }
            if(!empty($registration_ids2)){
                foreach($registration_ids2 as $ids2){
                    $reg_ids2 [] = $ids2['google_id'];
                }
            }
            $title=$user_id;
            $message=$translate['logged_out_from_all_devices'];
            $push = new Push();
            $push->setTitle($title);
            $push->setMessage($message);
            //json data android
            $jsondata = $push->getPushAndroid();
            //json data ios
            $jsonda = $push->getPushIos();
            $response = $push->sendMultiple($reg_ids, $jsondata);
            $response2 = $push->sendMultipleNotify($reg_ids2, $jsonda);
            LoginHistory::model()->logoutUser($studio_id,$user_id);
            $data['code']=200;
            $data['status'] = 'success';
            $data['msg']=$translate['logged_out_from_all_devices'];
            }else{
                $data['code']=300;
                $data['status'] = 'failure';
                $data['msg']="Email is invalid!";
            }
        }
        else{
                $data['code']=300;
                $data['status'] = 'failure';
                $data['msg']="Email required!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }  
    public function actionUpdateGoogleid(){
        $studio_id=$this->studio_id;
        $user_id=$_REQUEST['user_id'];
        $google_id=$_REQUEST['google_id'];
        $device_id =$_REQUEST['device_id'];
        $loginD = new LoginHistory();                
        $loginData = $loginD->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id ,'device_id' =>$device_id,'logout_at'=>'0000-00-00 00:00:00'));  
        if ($loginData){
            $loginData->google_id = $google_id;
            $loginData->save();
            $data['code']=200;
            $data['status'] = 'success';
            $data['msg']="Successfully update.";
        }
        else{
            $data['code']=300;
            $data['status'] = 'Failure';
            $data['msg']="Invalid UserID OR DeviceID.";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
    /* By Biswajit Parida For updating login history when logout from mobile apps */
    public function actionLogout(){
        if(isset($_REQUEST['login_history_id']) && $_REQUEST['login_history_id']!=""){
            $login_history_id = $_REQUEST['login_history_id'];
            $login_history = LoginHistory::model()->findByPk($login_history_id);
            $login_history->logout_at = date('Y-m-d H:i:s');
            $login_history->save();
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Logout Successfully.";       
        }else{
            $data['code'] = 406;
            $data['status'] = "Failure";
            $data['msg'] = "Requests needed";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
    }
	
	/**
	 * @method public isRegistrationEnabled: It will check wheather the registration is enabled or not and if yes, then returns with signup step type
	 * @return json Return the json array of results
	 * @param String authToken*: Auth token of the studio
	 * @param String lang_code
	 * @author Ashis<ashis@muvi.com>
	 */
	public function actionIsRegistrationEnabled() {
        $studio_id = $this->studio_id;
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        $chromecast = 0;
        $offline = 0;
        $app = Yii::app()->general->apps_count($studio_id);
        if (empty($app) || (isset($app) && ($app['apps_menu'] & 2)) || (isset($app) && ($app['apps_menu'] & 4))) {
            $chromecast_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'chromecast');
            if ($chromecast_val['config_value'] == 1 || $chromecast_val == '') {
                $chromecast = 1;
            }
            $offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
            if ($offline_val['config_value'] == 1 || $offline_val == '') {
                $offline = 1;
            }
        }
        $queue_status = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'user_queue_list');
        $queue_active = (!empty($queue_status)) ? $queue_status['config_value'] : 0;
        $playlist_active = Yii::app()->custom->hasPlaylistenabled($studio_id);
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
            $data['isRating'] = (int) $studioData->rating_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
            $device_status = StudioConfig::model()->getConfig($studio_id, 'restrict_no_devices');
            $data['isRestrictDevice'] = (isset($device_status['config_value']) && ($device_status['config_value'] != 0)) ? 1 : 0;
            $getDeviceRestrictionSetByStudioData = StudioConfig::model()->getConfig($studio_id, 'restrict_streaming_device');
            $getDeviceRestrictionSetByStudio = 0;
            if (@$getDeviceRestrictionSetByStudioData->config_value != "" && @$getDeviceRestrictionSetByStudioData->config_value > 0) {
                $getDeviceRestrictionSetByStudio = 1;
            }
            $data['is_streaming_restriction'] = $getDeviceRestrictionSetByStudio;
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
		$data['facebook'] = array('status'=>0);
		$data['google'] = array('status'=>0);
        $sociallogininfos = SocialLoginInfos::model()->findByAttributes(array('studio_id' => $studio_id));
		if(!empty($sociallogininfos)){
			if($sociallogininfos->status == 1){
				$data['facebook'] = array('status'=>1,'app_id'=>$sociallogininfos->fb_app_id,'app_secret'=>$sociallogininfos->fb_secret,'app_version'=>$sociallogininfos->fb_app_verson);
			}
			if($sociallogininfos->gplus_status == 1){
				$data['google'] = array('status'=>1,'client_id'=>$sociallogininfos->gplus_client_id,'client_secret'=>$sociallogininfos->gplus_client_secret);
			}
		}
        $data['chromecast'] = $chromecast;
        $data['is_offline'] = $offline;
        $data['isPlayList'] = $playlist_active;
        $data['isQueue'] = $queue_active;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	
	/**
     * @method getCustomMetadata It will return the custom metadata values of a content 
     * @author Biswajit<biswajit@muvi.com>
     * @return array array of custom meta field
     */
    public function getCustomMetadata($listdata = array(), $translate= array()){
        $custom_vals = array();
        $fcm = new FilmCustomMetadata;
		$pdo_cond_arr = array("studio_id" => $this->studio_id);
		if($listdata['custom_metadata_form_id']){
			$pdo_cond_arr['custom_metadata_form_id'] = $listdata['custom_metadata_form_id'];
		}
        $fcms = $fcm->findAllByAttributes($pdo_cond_arr, array('order' => 'id desc'));
        if (count($fcms) > 0) {
            foreach ($fcms as $fcm) {
                $fld = CustomMetadataField::model()->findByPk($fcm->custom_field_id);
                if ($fld->f_id != '' && strlen($fld->f_id) > 0) {
                    $arr[$fcm->field_name] = $fld->f_id;
                    $arr1[] = array(
                        'f_id' => trim($fld->f_id),
                        'field_name' => trim($fcm->field_name),
                        'field_display_name' => trim($fld->f_display_name)
                    );
                }
            }
        }

        if (isset($arr1) && count($arr1) > 0) {
            foreach ($arr1 as $ar) {
                $ar_k = trim($ar['f_id']);
                $ar_k1 = trim($ar['field_name']);
                $k = (int) str_replace('custom', '', $ar_k1);

                if ($k > 9) {
                    if (@$list[$key]['custom10']) {
                        $x = json_decode($listdata['custom10'], true);
                        foreach ($x as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                $custom_vals[] = array(
                                    'field_name' => $ar_k,
                                    'field_display_name' => isset($translate[$ar_k])? @$translate[$ar_k] : trim($ar['field_display_name']),
                                    'field_value' => trim($value1)
                                );
                            }
                        }
                    }
                } else {
                    //$custom_vals[] = array(
                        
                        $custom_vals[$ar_k] = trim(is_array(json_decode($listdata[$ar_k1])) ? implode(', ', json_decode($listdata[$ar_k1])) : $listdata[$ar_k1]);
                  
//                        'field_name' => $ar_k,
//                        'field_display_name' => isset($translate[$ar_k])? @$translate[$ar_k] : trim($ar['field_display_name']),
//                        'field_value' => trim(is_array(json_decode($listdata[$ar_k1])) ? implode(', ', json_decode($listdata[$ar_k1])) : $listdata[$ar_k1])
                    //);
                }
            }
        }
        return $custom_vals;
    }
    /**
     * @method getstudioAds() It will return the add server details if enabled 
     * @author Gayadhar<support@muvi.com>
     * @return array array of details
     */
    function getStudioAds() {
        //Find the Ad Channel id if enabled for the studio
        $arr = array();
        $studioAds = StudioAds::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        if ($studioAds) {
            if ($studioAds->ad_network_id == 1) {
                $data['Ads']['network'] = 'spotx';
            } elseif ($studioAds->ad_network_id == 2) {
                $arr['network'] = 'yume';
            }
            $arr['channelId'] = $studioAds->channel_id;
        }
        return $arr;
    }

    /**
     * @method private forgotPassword() Get Content details 
     * @author GDR<support@muvi.com>
     * @return json Returns array of message and response code based on the action success/failure
     * @param string $email	Valid email of the user
     * @param string $oauthToken Auth token
     * 
     */
    public function actionforgotPassword() {
        $studio_id = $this->studio_id;
        $language_id = 20;
        if (isset($_REQUEST) && count($_REQUEST) > 0 && $_REQUEST['email'] && $_REQUEST['email'] != '') {
            $email = trim($_REQUEST['email']);
            $user = SdkUser::model()->find('studio_id=:studio_id AND email=:email', array(':studio_id'=>$this->studio_id, ':email'=>$email));
            $studio = Studio::model()->findByPk($this->studio_id);
            if ($user) {
                $user_id = $user->id;
                $to = array($email);
                $enc = new bCrypt();
                $reset_tok = $enc->hash($email);
                $user->reset_password_token = $reset_tok;
                $user->save();

                $user_name = $user->display_name;
                $to_name = $user_name;
                $from = $studio->contact_us_email;
                $from_name = $studio->name;
                $site_url = 'http://' . $studio->domain;
                $siteLogo = $logo_path = Yii::app()->common->getLogoFavPath($this->studio_id);
                $logo = '<a href="' . $site_url . '"><img src="' . $siteLogo . '" /></a>';
                $site_link = '<a href="' . $site_url . '">' . $studio->name . '</a>';
                if ($studio->fb_link != '')
                    $fb_link = '<a href="' . $studio->fb_link . '" ><img src="' . $site_url . '/images/fb.png" alt="Find Us on Facebook"></a>';
                else
                    $fb_link = '';
                if ($studio->tw_link != '')
                    $twitter_link = '<a href="' . $studio->tw_link . '" ><img src="' . $site_url . '/images/twitter.png" alt="Find Us on Twitter"></a>';
                else
                    $twitter_link = '';
                if ($studio->gp_link != '')
                    $gplus_link = '<a href="' . $studio->gp_link . '" ><img src="' . $site_url . '/images/google_plus.png" alt="Find Us on Google Plus"></a>';
                else
                    $gplus_link = '';

                $reset_link = $site_url . '/user/resetpassword?user_id=' . $user_id . '&auth=' . $reset_tok . '&email=' . $email;
                $rlink = '<a href="' . $reset_link . '">' . $reset_link . '</a>';

                $studio_email = Yii::app()->common->getStudioEmail($this->studio_id);
                $subject = 'Reset password for your ' . $studio->name . ' account';
                $command = Yii::app()->db->createCommand()
                    ->select('display_name,studio_id')
                    ->from('sdk_users u')
                    ->where(' u.status = 1 AND u.email = "' .$email.'" AND studio_id='.$this->studio_id);
                $dataem = $command->queryAll();
                $FirstName = $dataem[0]['display_name'];
                $link = $rlink;
                $StudioName = $studio->name;
                $EmailAddress = $studio_email;
                $email_type = 'forgot_password';
                $content = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id'=>$this->studio_id, ':type'=>$email_type));                
                if ($content) {                
                } else {
                    $content = NotificationTemplates::model()->find('studio_id=:studio_id AND type=:type', array(':studio_id'=>0, ':type'=>$email_type));
                }
                $temps = $content;
                //Subject
                $subject = $temps['subject'];
                eval("\$subject = \"$subject\";");
                $content = (string) $temps["content"];
                $breaks = array("<br />", "<br>", "<br/>");
                $content = str_ireplace($breaks, "\r\n", $content);
                $content = htmlentities($content);
                eval("\$content = \"$content\";");
                $content = htmlspecialchars_decode($content);
                
                $params = array('website_name' => $studio->name,
                    'logo' => $logo,
                    'site_link' => $site_link,
                    'reset_link' => $rlink,
                    'username' => $user_name,
                    'fb_link' => @$fb_link,
                    'twitter_link' => @$twitter_link,
                    'gplus_link' => @$gplus_link,
                    'supportemail' => $studio_email,
                    'website_address' => $studio->address,
                    'content'=> $content
                );
                $template_name = 'sdk_reset_password_user';

                Yii::app()->theme = 'bootstrap';
                $thtml = Yii::app()->controller->renderPartial('//email/' . $template_name, array('params' => $params), true);
                $return_param = $this->sendmailViaAmazonsdk($thtml, $subject, $user->email, $EmailAddress,"","","",$StudioName);                //$return_param = $this->mandrilEmail($template_name, $params, $message);
                $response = 'success';
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['msg'] = "Please check your email to reset your password.";
            }else {

                $data['code'] = 418;
                $data['status'] = "Email didn't exists with us! ";
                $data['msg'] = "Email you have entered is not present!";
            }
        } else {
            $data['code'] = 417;
            $data['status'] = "A valid Email required";
            $data['msg'] = "Please provide a valid Email!";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public registerstreamUser($request) It will register the stream user and create a channel id
     * @return json Return the json array of results
     * @author Gayadhar<support@muvi.com>
     */
    function registerStreamUser($request) {
        $lsuser = new LivestreamUsers;
        $lsuser->name = $request['name'];
        $lsuser->nick_name = @$request['nick_name']?@$request['nick_name']:$request['name'] ;
        $lsuser->email = $request['email'];
        $lsuser->sdk_user_id = $request['sdk_user_id'];
        $lsuser->studio_id = $this->studio_id;
        $lsuser->ip = Yii::app()->request->getUserHostAddress();
        $lsuser->status = 1;
        $lsuser->created_date = new CDbExpression("NOW()");
        $return = $lsuser->save();
        $lsuser_id = $lsuser->id;
        $push_url = RTMP_URL . $this->studio_id . '/' . $lsuser->channel_id;
        $hls_path = HLS_PATH . $this->studio_id;
        if (!is_dir($hls_path)) {
            mkdir($hls_path, 0777, TRUE);
            chmod($hls_path, 0777);
        }
        if (!is_dir($hls_path . '/' . $lsuser->channel_id)) {
            mkdir($hls_path . '/' . $lsuser->channel_id, 0777, TRUE);
            chmod($hls_path . '/' . $lsuser->channel_id, 0777);
        }
        $hls_path = $hls_path . '/' . $lsuser->channel_id;
        $hls_url = HLS_URL . $this->studio_id . '/' . $lsuser->channel_id . '/' . $lsuser->channel_id . '.m3u8';
        $lsuser->pull_url = $hls_url;
        $lsuser->push_url = $push_url;
        $lsuser->save();
        $_REQUEST['rtmp_path'] = $push_url;
        return true;
    }

    /**
     * @method public checkEmailExistance($email) It will check if an email exists with the studio or not
     * @return json Return the json array of results
     * @param String $authToken Auth token of the studio
     * @param String $email Email to be validated
     */
    public function actionCheckEmailExistance($req = array()) {
        if ($req) {
            $_REQUEST = $req;
        }
        $arr['isExists'] = 0;
        $arr['code'] = 200;
        $arr['msg'] = 'OK';
        if (isset($_REQUEST) && isset($_REQUEST['email'])) {
            $user = new SdkUser;
            $users = $user->findByAttributes(array('email' => trim($_REQUEST['email']), 'studio_id' => $this->studio_id));
            if (isset($users) && !empty($users)) {
                $arr['isExists'] = 1;
                $arr['code'] = 422;
                $arr['msg'] = 'Email already exists for the studio';
            }
        } else {
            $arr['isExists'] = 0;
            $arr['code'] = 423;
            $arr['msg'] = '';
        }
        if ($req && $arr['code'] == 200) {
                return TRUE;
            }
            $this->setHeader($arr['code']);
            echo json_encode($arr);
            exit;
        }

	public function actionvalidateCouponCode() {
        $data = array();
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        
        $couponeDetails = Yii::app()->common->couponCodeIsValid($_REQUEST["couponCode"], $user_id, $studio_id, $_REQUEST["currencyId"]);
        if ($couponeDetails != 0) {
            if ($couponeDetails != 1) {
                $data['code'] = 432;
                $data['status'] = 'Valid';
                $data['msg'] = '';
                $data['discount'] = $couponeDetails['discount_amount'];
                if ($couponeDetails["discount_type"] == 1) {
                    $data['discount_type'] = '%';
                } else {
                    if ($_REQUEST["currencyId"] != 0) {
                        $currencyDetails = Currency::model()->findByPk($_REQUEST["currencyId"]);
                        $data['discount_type'] = $currencyDetails['symbol'];
                    } else {
                        $data['discount_type'] = '$';
                    }
                }
            } else {
                $data['code'] = 433;
                $data['status'] = 'Invalid';
                $data['msg'] = $translate['coupon_already_used'];
            }
        } else {
            $data['code'] = 433;
            $data['status'] = 'Invalid';
            $data['msg'] = 'Coupon code not valid.';
            $data['msg'] = $translate['invalid_coupon'];
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }
	
    public function actionvalidateCouponCode_test() {
        $data = array();
        $contArg = array();
        $prodArr = array();
        $studio_id = $this->studio_id;
        $user_id = $_REQUEST['user_id'];
        
        $lang_code   = @$_REQUEST['lang_code'];
        $translate   = $this->getTransData($lang_code,$studio_id);
        
        if(isset($_REQUEST['is_physical_specific']) && intval($_REQUEST['is_physical_specific'])){
            if(isset($_REQUEST['cart_item_id']) && trim($_REQUEST['cart_item_id']) !=""){
                $cart_item = PGCart::model()->findByPk($_REQUEST['cart_item_id']);
                $cart_item_details = json_decode($cart_item->cart_item, true);
                $n=0;
                foreach ($cart_item_details as $key => $val) {
                    $prodArr[$n]['product_id'] = $val['id'];
                    $prodArr[$n]['quantity'] = $val['quantity'];
                    $prodArr[$n]['price'] = $val['price'];
                    $prodArr[$n]['name'] = $val['name'];
                    $prodArr[$n]['is_api'] = 1;
                    $n++;
                }
            }else{
                $data['code'] = 433;
                $data['status'] = 'Invalid';
                $data['msg'] = 'Please Provide Cart Item Id.';
                echo json_encode($data);
                exit;
            }
        }
        
        if(isset($_REQUEST['is_ppv_specific']) && intval($_REQUEST['is_ppv_specific'])){
            $contArg['studio_id'] = $studio_id;
            
            $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $_REQUEST['movie_id'],'parent_id'=>0));
            $contArg['movie_id'] = $Films->id;
            $contArg['season_id'] = @$_REQUEST['season_id'];
            if (isset($_REQUEST['movie_id']) && trim($_REQUEST['movie_id']) && isset($_REQUEST['season_id']) && intval($_REQUEST['season_id']) && isset($_REQUEST['episode_id']) && trim($_REQUEST['episode_id']) != '0') {
                $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $_REQUEST['episode_id']));
                $contArg['episode_id'] = $streams->id;
            }
        }
        
        $couponeDetails = Yii::app()->common->couponCodeIsValid_test($_REQUEST["couponCode"], $user_id, $studio_id, $_REQUEST["currencyId"],$contArg,$prodArr);
        
        if ($couponeDetails != 0) {
            if ($couponeDetails != 1) {
                $data['code'] = 432;
                $data['status'] = 'Valid';
                $data['msg'] = '';
                $data['discount'] = $couponeDetails['discount_amount'];
                if(isset($couponeDetails['is_physicalSepecific']) && intval($couponeDetails['is_physicalSepecific'])){
                    $data['productName'] = $couponeDetails['productName'];
                }                 
                if ($couponeDetails["discount_type"] == 1) {
                    $data['discount_type'] = '%';
                } else {
                    if ($_REQUEST["currencyId"] != 0) {
                        $currencyDetails = Currency::model()->findByPk($_REQUEST["currencyId"]);
                        $data['discount_type'] = $currencyDetails['symbol'];
                    } else {
                        $data['discount_type'] = '$';
                    }
                }
            } else {
                $data['code'] = 433;
                $data['status'] = 'Invalid';
                $data['msg'] = $translate['coupon_already_used'];
            }
        } else {
            $data['code'] = 433;
            $data['status'] = 'Invalid';
            $data['msg'] = 'Coupon code not valid.';
            $data['msg'] = $translate['invalid_coupon'];
        }

        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public socialAuth() It will authenticate the social Login 
     * @author Gayadhar<support@muvi.com>
     * @param type $authToken AuthToken for the Studio
     * @param string $fb_userId The user id return by the User
     * @param string $email Email return by the app
     * @param String $name Name as returned by the application
     * @return json Json string
     */
    function actionSocialAuth() {
        $req = $_REQUEST;
        $studio_id = $this->studio_id;
        if (@$req['fb_userid'] || @$req['gplus_userid']) {
            if (@$req['email'] && filter_var(@$req['email'], FILTER_VALIDATE_EMAIL) != false && @$req['name']) {
                $device_type = trim(@$req['device_type']);
                $google_id = trim(@$req['google_id']);
                $device_id = isset($req['device_type']) ? @trim(@$req['device_id']) : '2'; 
				if(@$req['fb_userid']){
					$userData = SdkUser::model()->find('(email=:email OR fb_userid=:fb_userid) AND studio_id=:studio_id', array(':email' => $req['email'], ':fb_userid' => $req['fb_userid'], ':studio_id' => $studio_id));
				}else if(@$req['gplus_userid']){
					$userData = SdkUser::model()->find('(email=:email OR gplus_userid=:gplus_userid) AND studio_id=:studio_id', array(':email' => $req['email'], ':gplus_userid' => $req['gplus_userid'], ':studio_id' => $studio_id));
				}
                if ($userData && ($userData->is_deleted || !$userData->status)) {
                    $data['code'] = 436;
                    $data['status'] = 'Failure';
                    $data['msg'] = 'Account suspended, Contact admin to login';
                } elseif ($userData) {
                    if (@$req['fb_userid'] && ($userData['fb_userid'] != $req['fb_userid'])) {
                        $userData->fb_userid = $req['fb_userid'];
                        $userData->save();
                    }
					if (@$req['gplus_userid'] && ($userData['gplus_userid'] != $req['gplus_userid'])) {
                        $userData->gplus_userid = $req['gplus_userid'];
                        $userData->save();
                    }
                    if ($userData->add_video_log) {
                        $ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = $userData->id;
                        $login_history = new LoginHistory();
                        $login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = new CDbExpression("NOW()");
                        $login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = new CDbExpression("NOW()");
                        $login_history->google_id = $google_id;
                        $login_history->device_id = $device_id;
                        $login_history->device_type = $device_type;
                        $login_history->ip = $ip_address;
                        $login_history->save();
						$login_history_id = $login_history->id;
                    } 
                    
                    $data['id'] = $userData->id;
                    $data['email'] = $userData->email;
                    $data['display_name'] = $userData->display_name;
                    $data['studio_id'] = $studio_id;
                    $data['is_newuser'] = 0;
                    $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb',$this->studio_id);
                    $isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
                    $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
					$data['login_history_id'] = @$login_history_id;
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Login Success";
                } else {
                    $email = $res['data']['email'] = @$req['email'];
                    $name = $res['data']['name'] = @$req['name'];
                    $res['data']['fb_userid'] = @$req['fb_userid'];
					$res['data']['gplus_userid'] = @$req['gplus_userid'];

                    $res['data']['password'] = @$req['password'] ? $req['password'] : '';
                    
                    $plan_payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio_id);
                    
                    $gateway_code = '';
                    if (isset($plan_payment_gateway) && !empty($plan_payment_gateway)) {
                        $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode($studio_id);
                        $this->setPaymentGatwayVariable($plan_payment_gateway['gateways']);
                    }

                    $res['PAYMENT_GATEWAY'] = @$this->PAYMENT_GATEWAY[$gateway_code];
                    $res['PAYMENT_GATEWAY_ID'] = @$this->PAYMENT_GATEWAY_ID[$gateway_code];
                    $res['GATEWAY_ID'] = @$this->GATEWAY_ID[$gateway_code];
                    
                    $ret = SdkUser::model()->saveSdkUser($res, $studio_id);
                    if ($ret) {
                        if (@$ret['error']) {
                            $data['code'] = 420;
                            $data['status'] = 'Failure';
                            $data['msg'] = 'Invalid Plan!';
                            $this->setHeader($data['code']);
                            echo json_encode($data);
                            exit;
                        }
                        // Send welcome email to user
                        if (@$ret['is_subscribed']) {
                            $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email_with_subscription');
                        } else {
                            $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $ret['user_id'], 'welcome_email');
                        }
                        $data['id'] = $ret['user_id'];
                        $data['email'] = $email;
                        $data['display_name'] = $name;
                        $data['studio_id'] = $studio_id;
                        $data['is_newuser'] = 1;
                        $data['profile_image'] = $this->getProfilePicture($ret['user_id'], 'profilepicture', 'thumb');
                        $isSubscribed = Yii::app()->common->isSubscribed($ret['user_id'], $this->studio_id);
                        $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
						
						$ip_address = CHttpRequest::getUserHostAddress();
                        $user_id = $userData->id;
						$login_history = new LoginHistory();
						$login_history->studio_id = $studio_id;
                        $login_history->user_id = $user_id;
                        $login_history->login_at = new CDbExpression("NOW()");
						$login_history->logout_at = "0000-00-00 00:00:00";
                        $login_history->last_login_check = new CDbExpression("NOW()");
                        $login_history->google_id = $google_id;
                        $login_history->device_id = $device_id;
                        $login_history->device_type = $device_type;
                        $login_history->ip = $ip_address;
						$login_history->save();
						$data['login_history_id'] = $login_history->id;
						
                        $data['code'] = 200;
                        $data['status'] = "OK";
                        $data['msg'] = "Login Success";
                    } else {
                        $data['code'] = 421;
                        $data['status'] = 'Failure';
                        $data['msg'] = 'Error in registration';
                        $this->setHeader($data['code']);
                        echo json_encode($data);
                        exit;
                    }
                }
            } else {
                $data['code'] = 417;
                $data['status'] = 'Failure';
                $data['msg'] = 'A valid Email required';
            }
        } else {
            $data['code'] = 435;
            $data['status'] = 'Failure';
            $data['msg'] = 'Facebook User id required';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public GetImageForDownload() It will give the s3 url image to check internet speed of user
     * @author Sruti kanta<support@muvi.com>
     * @return json Json string
     */
    function actionGetImageForDownload() {
        $bucketInfo = Yii::app()->common->getBucketInfo('', $this->studio_id);
        $image_url = CDN_HTTP . $bucketInfo['bucket_name'] . "." . $bucketInfo['s3url'] . "/check-download-speed.jpg";
        $data['code'] = 200;
        $data['status'] = 'OK';
        $data['image_url'] = $image_url;
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method public getPorfileDetails() It will fetch the profile details of the logged in user
     * @param string $authToken Authtoken
     * @param string $email
     * @param int $user_id
     * @author Gayadhar<support@muvi.com>
     * @return Json string
     */
    function actionGetProfileDetails() {
        if (@$_REQUEST['email'] && $_REQUEST['user_id']) {
            $userData = SdkUser::model()->find('email=:email AND studio_id =:studio_id AND status=:status AND id=:user_id', array(':email' => $_REQUEST['email'], ':studio_id' => $this->studio_id, ':status' => 1, ':user_id' => $_REQUEST['user_id']));
            if ($userData) {
                $data['id'] = $userData->id;
                $data['email'] = $userData->email;
                $data['display_name'] = $userData->display_name;
                $data['studio_id'] = $userData->studio_id;
                $data['isFree']= $userData->is_free;
                $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb', $this->studio_id);
				$data['mobile_number'] = @$userData->mobile_number;
                
				$isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
                $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
                $custom_field_details = Yii::app()->custom->getCustomFieldsDetail($this->studio_id,$userData->id);
				
                if(!empty($custom_field_details)){
                    foreach($custom_field_details as $key=>$val){
                        if($key == "languages"){
                            $data["custom_".$key] = $val;
                        }else{
                            $data["custom_".$key] = $val[0];
                        }
                    }
                }
                $data['code'] = 200;
                $data['status'] = 'OK';
                $data['msg'] = 'Success!';
            } else {
                $data['code'] = 442;
                $data['status'] = 'Failure';
                $data['msg'] = 'No user found!';
            }
        } else {
            $data['code'] = 443;
            $data['status'] = 'Failure';
            $data['msg'] = 'Email & user id required!';
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method getFbUserstatus() Returns the Facebook user status 
     * @param String $authToken A authToken
     * @param String $fbuser_id Facebook User id
     * @author Gayadhar<support@muvi.com>
     * @return Json Facebook user status
     */
    function actionGetFbUserStatus() {
        $userData = SdkUser::model()->find('(fb_userid=:fb_userid) AND studio_id=:studio_id', array(':fb_userid' => $_REQUEST['fb_userid'], ':studio_id' => $this->studio_id));
        if ($userData && ($userData->is_deleted || !$userData->status)) {
            $data['code'] = 436;
            $data['status'] = 'Failure';
            $data['msg'] = 'Account suspended, Contact admin';
        } elseif ($userData) {
            $data['id'] = $userData->id;
            $data['email'] = $userData->email;
            $data['display_name'] = $userData->display_name;
            $data['nick_name'] = $userData->nick_name;
            $data['studio_id'] = $this->studio_id;
            $data['is_newuser'] = 0;
            $data['profile_image'] = $this->getProfilePicture($userData->id, 'profilepicture', 'thumb');
            $isSubscribed = Yii::app()->common->isSubscribed($userData->id, $this->studio_id);
            $data['isSubscribed'] = (int) $isSubscribed ? 1 : 0;
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Success";
        } else {
            $data['is_newuser'] = 1;
            $data['code'] = 200;
            $data['status'] = "OK";
            $data['msg'] = "Success";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    /**
     * @method converttimetoSec(string $str) convert a string of HH:mm:ss to second
     * @author Gayadhar<support@muvi.com>
     * @return int 
     */
    function convertTimetoSec($str_time) {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }
/**
* @method updateUserProfile() Returns the profile details along with profile image
* @param String $authToken A authToken
* @param array $data profile data in array format
* @param array $image optional image info 
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
function actionUpdateUserProfile() {
        if ($_REQUEST['user_id']) {
            if ($_FILES && ($_FILES['error'] == 0)) {
                $ret = $this->uploadProfilePics($_REQUEST['user_id'], $this->studio_id, $_FILES['file']);
            }
            $sdkUsers = SdkUser::model()->findByPk($_REQUEST['user_id']);
            if ($sdkUsers) {
                if (@$_REQUEST['nick_name']) {
                    $sdkUsers->nick_name = $_REQUEST['nick_name'];
                }
                if (@$_REQUEST['name']) {
                    $sdkUsers->display_name = $_REQUEST['name'];
                }
                if (@$_REQUEST['mobile_number'] && is_numeric($_REQUEST['mobile_number'])) {
                    $sdkUsers->mobile_number = @$_REQUEST['mobile_number'];
                }
                if (@$_REQUEST['password']) {
                    $enc = new bCrypt();
                    $pass = $enc->hash($_REQUEST['password']);
                    $sdkUsers->encrypted_password = $pass;
                }
                $sdkUsers->last_updated_date = new CDbExpression("NOW()");
                $sdkUsers->save();
                $is_user = Yii::app()->custom->updateCustomFieldValue($_REQUEST['user_id'], $this->studio_id);

                $data['status'] = 'OK';
                $data['code'] = 200;
                $data['msg'] = 'Success';
                $data['name'] = $sdkUsers->display_name;
                $data['email'] = $sdkUsers->email;
                $data['nick_name'] = $sdkUsers->nick_name;
                $data['mobile_number'] = $sdkUsers->mobile_number;
                $data['profile_image'] = $this->getProfilePicture($_REQUEST['user_id'], 'profilepicture', 'thumb', $this->studio_id);
            } else {
                $data['code'] = 448;
                $data['status'] = "Error";
                $data['msg'] = "User does not exist";
            }
        } else {
            $data['code'] = 448;
            $data['status'] = "Error";
            $data['msg'] = "User id required";
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }

    function uploadProfilePics($user_id,$studio_id,$fileinfo){
		if(!$studio_id){$studio_id = $this->studio_id;}

		//Checking for existing object
		$old_poster = new Poster;
		$old_posters = $old_poster->findAllByAttributes(array('object_id' => $user_id, 'object_type' => 'profilepicture'));
		if(count($old_posters) > 0){
			foreach ($old_posters as $oldposter) {
				$oldposter->delete();
			}
		}
		$ip_address = CHttpRequest::getUserHostAddress(); 
		$poster = new Poster();
		$poster->object_id = $user_id;
		$poster->object_type = 'profilepicture';
		$poster->poster_file_name = $fileinfo['name'];
		$poster->ip = $ip_address;
		$poster->created_by = $user_id;
		$poster->created_date = new CDbExpression("NOW()");
		$poster->save();
		$poster_id = $poster->id;                    
		
		$fileinfo['name'] = Yii::app()->common->fileName($fileinfo['name']);
		$_FILES['Filedata'] = $fileinfo;
		$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/profile_images/'.$poster_id;
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		$dir = $dir.'/original';
		if (!file_exists($dir)) {
			mkdir($dir, 0777);
		}
		move_uploaded_file($fileinfo['tmp_name'], $dir.'/'.$fileinfo['name']);
		
		//$uid = $_REQUEST['movie_id'];
		require_once "Image.class.php";
		require_once "Config.class.php";
		require_once "ProfileUploader.class.php";
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		require_once "amazon_sdk/sdk.class.php";
		spl_autoload_register(array('YiiBase', 'autoload'));

		define( "BASEPATH",dirname(__FILE__) . "/.." );
		$config = Config::getInstance();
		$config->setUploadDir($_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'images/public/system/profile_images'); //path for images uploaded
		$bucketInfo = Yii::app()->common->getBucketInfoForPoster($studio_id);
		$config->setBucketName($bucketInfo['bucket_name']);
		$s3_config = Yii::app()->common->getS3Details($studio_id);
		$config->setS3Key($s3_config['key']);
		$config->setS3Secret($s3_config['secret']);
		$config->setAmount( 250 );  //maximum paralell uploads
		$config->setMimeTypes( array( "jpg" , "gif" , "png",'jpeg' ) ); //allowed extensions
		$config->setDimensions( array('small'=>"150x150",'thumb'=>"100x100") );   //resize to these sizes
		//usage of uploader class - this simple :)
		$uploader = new ProfileUploader($poster_id);
		$folderPath = Yii::app()->common->getFolderPath("",$studio_id);
		$unsignedBucketPath = $folderPath['unsignedFolderPath'];
		$ret = $uploader->uploadprofileThumb($unsignedBucketPath."public/system/profile_images/");
		return true;
	}
/**
* @method GetStatByType() Return the 
* @param String $authToken A authToken
* @param array $filterType filter on specific type like genre, language,rating etc
* @author Gayadhar<support@muvi.com>
* @return Json user details 
*/
function actionGetStatByType() {
		if($_REQUEST['filterType']){
			$filterType =strtolower(trim($_REQUEST['filterType'])); 
			if($filterType == 'genre'){
				$genreArr = array();
            $gsql = 'SELECT genre, COUNT(*) AS cnt FROM films WHERE studio_id ='.$this->studio_id .' AND genre is not NULL AND genre !=\'\' AND genre !=\'null\' GROUP BY genre';
				$con = Yii::app()->db;
				$res = $con->createCommand($gsql)->queryAll();
				if($res){
						$data['status'] = 'OK';
						$data['code'] = 200;
						$data['msg'] = 'Success';
                foreach($res As $key=>$val) {
                    $expArr = json_decode($val['genre'],TRUE);
                    if(is_array($expArr)) {
                        foreach ($expArr AS $k=>$v){
                            $mygenre = strtolower(trim($v));
                            $data['content_statistics'][$mygenre] += $val['cnt'];
							}
						}
					}
            } else {
					$data['code'] = 451;
					$data['status'] = "Error";
					$data['msg'] = "No valid genre found with your data";
				}
        } else {
				$data['code'] = 450;
				$data['status'] = "Error";
				$data['msg'] = "A valid filter type is required";
			}
    } else {
			$data['code'] = 449;
            $data['status'] = "Error";
            $data['msg'] = "Please mention a filter type like genre,language,rating etc";
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
	
	/* Functions to set header with status code. eg: 200 OK ,400 Bad Request etc.. */

    private function setHeader($status) {
		$status = 200;
		ob_clean();
		ob_flush();
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Muvi <support@muvi.com>");
    }

    private function _getStatusCodeMessage($status) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            407 => 'AuthToken Required',
            406 => 'Invalid Login Details',
            408 => 'Invalid authToken',
            409 => 'AuthToken not registerd with Muvi',
            410 => 'Expired authToken',
            411 => 'Content type permalink required!',
            412 => 'Invalid Content type permalink!',
            413 => 'Content permalink is invalid!',
            414 => 'Content permalink required!',
            415 => 'Invalid Content!',
            416 => 'Search Parameter required!',
            417 => 'A valid Email required!',
            418 => 'Invalid Email!',
            419 => 'No active payment gateways!',
            420 => 'Invalid Plan!',
            421 => 'Error in processing payment!',
            422 => 'Email already exists with the studio!',
            423 => 'A valid email address required!',
            424 => 'Payment is not enabled for this studio!',
            425 => 'Please activate your subscription to watch video.',
            426 => 'Please reactivate your subscription to watch video.',
            427 => 'This video is not allowed to play in your country.',
            428 => 'Sorry, you have crossed the maximum limit of watching this content.',
            429 => 'You will be allow to watch this video.',
            430 => 'Please purchase this video to watch it.',
            55 => 'We are not able to process your credit card. Please try another card.',
            432 => 'Coupon Valid',
            433 => 'Coupon Invalid',
            434 => 'No banner uploaded',
            435 => 'Facebook user id required',
            436 => 'Account suspended, Contact admin to login',
            437 => 'Content id and Stream id required',
            438 => 'No content availbe for the information',
            439 => 'PPV not enabled for this content',
            440 => 'A channel id required',
            441 => 'Invalid Channel id',
            442 => 'No user found!',
            443 => 'Email & user id required!',
            444 => 'IP address required!',
            445 => 'IP address is not allowed!',
            446 => 'Adding log is not allowed!',
            447 => 'No menu Found',
            448 => 'User id required to update profile',
            449 => 'Filter type like genre,language,rating etc',
            450 => 'A valid filterType is require',
            451 => 'Genre not found in your data',
            452 => 'Error in saving data',
            453 => 'Movie id is required',
            454 => 'Account is Blocked in this country',
            455 => 'Registration is not Enabled',
            456 => 'Card can not be saved',
            457 => 'No subscription plans found!',
			458 => 'Required params are missing!',
			459 => 'Invalid Login credential',
			460 => 'No studio available with the given credential!',
			461 => 'Invalid Login credential',
			462 => 'AuthToken not available, Contact admin to generate the token',
            463 => 'Your IP looks suspicious',
            464 => 'Cast permalink is invalid!',
            465 => 'Cast permalink required!',
            466 => 'Invalid Cast!',
            467 => 'Insufficient Data',
            468 => 'Banned user',
            469 => 'Rating disabled',
			471 => 'Please ener image key'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
        
    public function actiongetWebsiteSettings() {
        $studioData = Studio::model()->findByPk($this->studio_id);
        $is_registration = $studioData->need_login;
        if (intval($is_registration)) {
            $data['code'] = 200;
            $data['status'] = "Success";
            $data['isMylibrary'] = (int) $studioData->mylibrary_activated;
            $data['is_login'] = 1;
            $data['has_favourite'] = $this->CheckFavouriteEnable($studio_id);
            $general = new GeneralFunction;
            $data['signup_step'] = $general->signupSteps($this->studio_id);
        } else {
            $data['code'] = 455;
            $data['status'] = "failure";
            $data['is_login'] = 0;
        }
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;
    }    

    function getDomainName(){
        $sql = "SELECT is_embed_white_labled, domain FROM studios WHERE id=".$this->studio_id;
        $stData = Yii::app()->db->createCommand($sql)->queryAll();
        $domainName = Yii::app()->getBaseUrl(TRUE);
        if(@$stData[0]['is_embed_white_labled']){
                 $domainName = @$stData[0]['domain']? 'http://'.@$stData[0]['domain']:$domainName; 
        }
        return $domainName;
    }
    
    /**@method  return scr part from thirdparty url
     * @author SKM<prakash@muvi.com>
     * @return string
     */
    public function getSrcFromThirdPartyUrl($thirdPartyUrl=''){
        if($thirdPartyUrl!=''){
            preg_match('/src="([^"]+)"/',$thirdPartyUrl, $match);
            if(!empty($match[1])){
                return $match[1];
            }else{
                preg_match("/src='([^']+)'/",$thirdPartyUrl, $match);
                if(!empty($match[1])){
                   return $match[1]; 
                }else{
                   return $thirdPartyUrl; 
                }
            }          
        }else{
            return $thirdPartyUrl;
        }
    }  
        
    //get movie name by suniln
     public function getFilmName($movie_id){
        $movie_name = Yii::app()->db->createCommand("SELECT name FROM films WHERE id=".$movie_id)->queryROW();
        return $movie_name['name'];
    }
    //get episode name by suniln
     public function getEpisodeName($episode_id){
        $movie_name = Yii::app()->db->createCommand("SELECT episode_title FROM movie_streams WHERE id=".$episode_id)->queryROW();
        return $movie_name['episode_title'];
    }
    function getUniqueIdEncode($str){
       for($i=0; $i<5;$i++){
            //apply base64 first and then reverse the string
            $str=strrev(base64_encode($str));
        }
            return $str;
    }
    function getUniqueIdDecode($str){
         for($i=0; $i<5;$i++){
            //apply reverse the string first and then base64
            $str=base64_decode(strrev($str));
        }
            return $str;
    }

/**
 * @method public GetStudioAuthKey() It will validate the login credential & return the authToken for that studio. 
 * @author Gayadhar<support@muvi.com>
 * @param string $email Loggin email address
 * @param string $password studio login password
 * @return JSON will return the authToken for the respective studio
 */	
	function actionGetStudioAuthkey(){
		if($_REQUEST['email'] && $_REQUEST['password']){
			$userData = User::model()->find('email=:email AND role_id =:role_id',array(':email'=>$_REQUEST['email'],'role_id'=>1));
			if($userData){
				$enc = new bCrypt();
				if($enc->verify($_REQUEST['password'], $userData->encrypted_password)){
					$sql = "SELECT * FROM oauth_registration WHERE studio_id={$userData->studio_id} and status=1";
					$oauthData = Yii::app()->db->createCommand($sql)->queryRow();
					if ($oauthData){
						$referer = '';
						if (isset($_SERVER['HTTP_REFERER'])) {
							$referer = parse_url($_SERVER['HTTP_REFERER']);
							$referer = $referer['host'];
						}
						$oauthData = (object) $oauthData;
						if ($oauthData->request_domain && ($oauthData->request_domain != $referer)) {
							$data['code'] = 409;
							$data['status'] = "failure";
							$data['msg'] = "Domain not registered with Muvi for API calls!";
							echo json_encode($data);exit;
						}
						if ($data->expiry_date && (strtotime($data->expiry_date) < strtotime(date('Y-m-d')))) {
							$data['code'] = 410;
							$data['status'] = "failure";
							$data['msg'] = "Oauth Token expired!";
							echo json_encode($data);exit;
						}
						$data['code'] = 200;
						$data['authToken'] = $oauthData->oauth_token;
						$data['status'] = "OK";
						$data['msg'] = "Success";
					}else{
						$data['code'] = 462;
						$data['status'] = "Failure";
						$data['msg'] = "AuthToken not available, Contact admin to generate the token";   
					}
				}else{
					$data['code'] = 461;
					$data['status'] = "Failure";
					$data['msg'] = "Invalid login credential";   
				}
			}else{
				$data['code'] = 460;
				$data['status'] = "Failure";
				$data['msg'] = "No studio available with the given credential!"; 
			}
		}else{
			$data['code'] = 459;
            $data['status'] = "Failure";
            $data['msg'] = "Invalid Login credential";    
		}
		$this->setHeader($data['code']);
        echo json_encode($data);exit;
	}
    function getTransData($lang_code='en', $studio_id){
        $studio = Studio::model()->findByPk($studio_id,array('select'=>'theme'));
        $theme = $studio->theme;
        if(!$lang_code)
            $lang_code = 'en';
        
        $language_id = Yii::app()->custom->getLanguage_id($lang_code);
        $trans_msg = TranslateKeyword::model()->findAll(array('condition'=>'(studio_id=0 OR (store_keys=:store_keys AND studio_id=:studio_id AND language_id=20)) AND (device_type=:device_type OR device_type=:all_device)','params'=>array(':store_keys'=>'1',':studio_id'=>$studio_id,':device_type'=>'1',':all_device'=>'2')));
        foreach($trans_msg as $msgs):
            $trans_word[$msgs->trans_key] = $msgs->trans_value;
        endforeach;
        $lang_config = StudioLanguageConfig::model()->getconfig($studio_id, 'is_new');
        if((!empty($lang_config)) && ($lang_config->config_value == 1)):
            $enable_lang_msg    = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=:studio_id AND language_id=:language_id','params'=>array(':studio_id'=>$studio_id,':language_id'=>$language_id)));
            $enable_lang_words  = array();
            foreach($enable_lang_msg as $lang_msg):
              $enable_lang_words[$lang_msg->trans_key] = $lang_msg->trans_value;  
            endforeach;
            $trans_val = @$enable_lang_words;
        endif;
        if(empty($trans_val)):
            $file_path    = $path."/".$enable_lang.".php"; 
            if(file_exists($file_path)):
                $trans_val = $this->mergeArray($file_path);
            else:
                $trans_val = @$trans_word;
            endif;
        endif;
        $language = Yii::app()->controller->getAllTranslation($trans_val, $studio_id, $trans_word);
        return $language;
    }
  
    function isCouponExists() {
        $studio_id = $this->studio_id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
        $isCouponExists = 0;
        if(isset($data['menu']) && !empty($data['menu']) && ($data['menu'] & 32)) {
            $default_currency_id = Studio::model()->findByPk($studio_id)->default_currency_id;
            //$sql = "SELECT c.id FROM coupon c, coupon_currency cc WHERE c.studio_id = {$studio_id} AND c.status=1 AND c.id=cc.coupon_id AND cc.currency_id={$default_currency_id}";
            $sql = "SELECT c.id FROM coupon c WHERE c.studio_id = {$studio_id} AND c.status=1";
            $coupon = Yii::app()->db->createCommand($sql)->queryAll();
            
            if (isset($coupon) && !empty($coupon)) {
                $isCouponExists = 1;
            }
        }
        
        return $isCouponExists;
    }
    
    /*APIs for managing favourite contents*/
    function CheckFavouriteEnable($studio_id){
        $fav_status = StudioConfig::model()->getconfigvalueForStudio($studio_id,'user_favourite_list');
        if(!empty($fav_status)){
            $result = $fav_status['config_value'];
        }
        else{
            $result = 0;
        }
        return $result;
    }
    

    public function GetTableName($key){
        $request_array = array('authToken','lang_code','category','sort','order','limit','offset','Host','User-Agent','Accept','Accept-Encoding','Cookie','Content-Type','Content-Length');
        $film_array    = array('name','language','genre','rating','country','censor_rating','release_date');
        $movie_stream_array  = array('episode_title','episode_story','episode_date');
        if(!in_array($key,$request_array)){
            if(in_array($key, $film_array)){
                $get_filter_details = array(
                    'table' => 'films',
                    'field' => $key
                );
            }elseif(in_array($key, $movie_stream_array)){
                $get_filter_details = array(
                    'table' => 'movie_streams',
                    'field' => $key
                );
            }else{
                $get_filter_details = Yii::app()->custom->getFilterTableData($key, $this->studio_id);
            }
        }
        return $get_filter_details;
    }
	/*
     * @purpose Generate Widevine Licence Url
	 * @param string $conentKey , string $encryptionKey
     * @return  string 
     * @author Prakash<support@muvi.com>
    */
	private function generateExplayWideVineToken($conentKey = '', $encryptionKey = '') {
        if ($conentKey && $encryptionKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, WV_URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerAuthenticator=" . DRM_LICENSE_KEY . "&errorFormat=json&kid=" . $encryptionKey . "&contentKey=" . $conentKey . "&securityLevel=1&hdcpOutputControl=0&expirationTime=%2B36000");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);            
            curl_close($ch);
            return ($server_output)?$server_output:false;
        } else {
            return false;
        }
    }
   /**
    * @method loadFeaturedContent() It will load the featured content a store 
    * @param int $limit Limit the number of section
    * @param int $offset Offset
    * @param String $themes Description
    * @author Gayadhar<support@muvi.com>
    * @return Json Json data array. 
     */
        public function actionrules() {
        $user_id = isset($_REQUEST['user_id']) && $_REQUEST['user_id'] ? $_REQUEST['user_id'] : '';
        $stream_uniq_id = isset($_REQUEST['stream_uniq_id']) && $_REQUEST['stream_uniq_id'] ? $_REQUEST['stream_uniq_id'] : '';
        $res = Yii::app()->policy->getRulesAPI($stream_uniq_id, $user_id);
        echo json_encode($res);
        exit;
    }
    public function actionGetAPIServer() {
        if (isset($_REQUEST['ip']) && $_REQUEST['ip'] != "") {
            $ip = $_REQUEST['ip'];
        } else {
            $ip = CHttpRequest::getUserHostAddress();
        }
        $location = IP2Location::model()->getLocation($ip);                            
        $latitude = @$location['latitude'];
        $longitude = @$location['longitude']; 
        $ref = array($latitude, $longitude);
        $command = Yii::app()->db->createCommand()->select('*')->from('api_server');
        $locations = $command->queryAll();
        foreach ($locations AS $key => $val) {
            $apiserver[$val['id_server']][] = $val['url'];
            $apiserver[$val['id_server']][] = $val['latitude'];
            $apiserver[$val['id_server']][] = $val['longitude'];
            $server[$val['id_server']] = $val;
        }
        if ($latitude && $longitude) {
            $distances = array_map(function($apiserver) use($ref) {
                $a = array_slice($apiserver, -2);
                return Yii::app()->common->getDistance($a, $ref);
            }, $apiserver);
            asort($distances);
            $bucketids = array_keys($distances);
            if ($bucketids[0] > 0)
                $s3bucket_id = $bucketids[0];
        } else {
            $s3bucket_id = 5;
        }
        $res['code'] = 200;
        $res['status'] = "OK";
        $res['url'] = $server[$s3bucket_id]['url'];
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
	 //@vi - get HH:MM:SS values to seconds || modified for Ads streaming 
    //Date : 07-2017-21
    public function getHHMMSSToseconds($time = ''){
        if($time!=''){        
             $timeExploded = explode(':', $time);
             return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
		}
    }
	function actiongetImages(){
        if(!empty($_REQUEST['image_key'])){
            $studio_id = $this->studio_id;
            //$bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
            //$image_root_url = CDN_HTTP . $bucketInfo['cloudfront_url'].'/'.$bucketInfo['unsignedFolderPath'];
			$image_root_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
            $image_key = trim($_REQUEST['image_key']);
            $limit = !empty($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
            $offset = 0;
            if (isset($_REQUEST['offset']) && $_REQUEST['offset']) {
                $offset = ($_REQUEST['offset'] - 1) * $limit;
            }
            $command = Yii::app()->db->createCommand()
                    ->select("id,CONCAT('$image_root_url',s3_thumb_name) AS thumb_name, CONCAT('$image_root_url',s3_original_name) AS original_name, image_key")
                    ->from('image_management')
                    ->where("flag_deleted=0 AND studio_id=:studio_id AND image_key !='' AND image_key LIKE :image_key", array(':studio_id' => $studio_id, ':image_key'=>'%'.$image_key.'%'))
                    ->queryAll();
            
            $image_key = array_filter(preg_split( "/( |_)/", $image_key ));
            $command2 = array();
            if(count($image_key)>1){
                $implodeText = ' image_key LIKE \'%'.implode('%\' OR image_key LIKE \'%', $image_key).'%\'';
                $command2 = Yii::app()->db->createCommand()
                    ->select("id,CONCAT('$image_root_url',s3_thumb_name) AS thumb_name, CONCAT('$image_root_url',s3_original_name) AS original_name, image_key")
                    ->from('image_management')
                    ->where("flag_deleted=0 AND studio_id=:studio_id AND image_key !='' AND $implodeText", array(':studio_id' => $studio_id))
                    ->queryAll();
            }
            $mergeArr = array_merge($command, $command2);
            $input = array_map("unserialize", array_unique(array_map("serialize", $mergeArr)));
            $page = $offset < 1 ? 1 : $offset;
            $start = ($offset - 1) * ($limit + 1);
            $offset = $limit + 1;
            $result = array_slice($input, $start, $offset);
             
            $res['code'] = 200;
            $res['data'] = $input;
        } else {
            $res['code'] = 471;
            $res['msg'] = "Please ener image key"; 
        }
        $this->setHeader($data['code']);
        echo json_encode($res);
    }

    /**
     * @method private GetEpisodes() Get the list of contents based on it parent content
     * @param movie_id, movie_stream_id
     * @author Biswajit Parida<support@muvi.com>
     * @return json Returns the list of data in json format success or corrospending error code
     */
    public function actionGetEpisodes() {
        if (isset($_REQUEST['movie_id']) && trim($_REQUEST['movie_id']) != "") {
            $lang_code = @$_REQUEST['lang_code'];
            $translate = $this->getTransData($lang_code, $this->studio_id);
            $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
            $user_id = isset($_REQUEST['user_id']) && trim($_REQUEST['user_id']) > 0 ? $_REQUEST['user_id'] : 0;
            $movie_id = $_REQUEST['movie_id'];
            $movie_stream_id = $_REQUEST['movie_stream_id'];
            $movie_strm = movieStreams::model()->findByPk($movie_stream_id, array('select' => 'series_number'));
            $ser_no = $movie_strm->series_number;
            $command1 = Yii::app()->db->createCommand()
                    ->select('id , embed_id , episode_number ,series_number')
                    ->from('movie_streams')
                    ->where('movie_id = ' . $movie_id . ' AND episode_parent_id = 0 AND id !=' . $movie_stream_id . ' AND  full_movie != "" AND series_number =' . $ser_no)
                    ->order('series_number');
            $eplists = $command1->queryAll();
            $lists = array();
            if (!empty($eplists)) {
                foreach ($eplists as $eplist) {
                    $lists[] = Yii::app()->general->getContentData($eplist['id'], 1, array(), $language_id, $this->studio_id, $user_id, $translate);
                }
            }
            $res['code'] = 200;
            $res['status'] = "Success";
            $res['list'] = $lists;
        } else {
            $res['code'] = 405;
            $res['status'] = "Failure";
            $res['msg'] = $translate['invalid_data'];
        }
        $this->setHeader($res['code']);
        echo json_encode($res);
        exit;
    }
    
        
    /**
     * @purpose for visual designer
     * @Author Sanjib Pradhan<sanjib.p@muvi.com>
     */
    public function actionSaveDraftDataVD() { 
        $studio_id = $this->studio_id;
        if ($_REQUEST['featured_section'] && $_REQUEST['logdata']) {
            $feature = (array) $_REQUEST['featured_section'];
            if (isset($_REQUEST['logdata']) && $_REQUEST['logdata']) {
                $data = $_REQUEST['logdata'];
                $log = Yii::app()->db->createCommand()->select('id')->from('visual_log')->where('studio_id=:studio_id', array(':studio_id' => $studio_id))->queryRow();
                if (!empty($log)) {
                    $model = VisualLog::model()->findByPk($log['id']);
                    $model->log = json_encode($data);
                    $model->loading_style = $feature['loading_style'];
                    $model->section_count = $feature['section_count'];
                    $model->featured_sec_log = json_encode($_REQUEST['featured_section']);
                    $model->banner_style = $_REQUEST['banner_style'];
                    $model->list_item_info = json_encode($_REQUEST['list_item_info']);
                    $model->content_detail_info = json_encode($_REQUEST['content_detail_info']);
                    $model->shop_list_item_info = json_encode($_REQUEST['shop_list_item_info']);
                    $model->product_detail_info = json_encode($_REQUEST['product_detail_info']);
                } else {
                    $model = new VisualLog();
                    $model->studio_id = $studio_id;
                    $model->log = json_encode($data);
                    $model->loading_style = $feature['loading_style'];
                    $model->section_count = $feature['section_count'];
                    $model->featured_sec_log = json_encode($_REQUEST['featured_section']);
                    $model->banner_style = $_REQUEST['banner_style'];
                    $model->list_item_info = json_encode($_REQUEST['list_item_info']);
                    $model->content_detail_info = json_encode($_REQUEST['content_detail_info']);
                    $model->shop_list_item_info = json_encode($_REQUEST['shop_list_item_info']);
                    $model->product_detail_info = json_encode($_REQUEST['product_detail_info']);
                }
                $model->save();
            }
            if ($_REQUEST['featured_section']) {
                VdFeaturedSections::model()->deleteAll('studio_id=:studio_id', array(':studio_id' => $studio_id));

                foreach ($feature['design'] as $key => $value) {
                    $featureredSection = new VdFeaturedSections();
                    $featureredSection->studio_id = $studio_id;
                    $featureredSection->section_id = $value->id;
                    $featureredSection->layout_design = $value->layout_design;
                    $featureredSection->column_count = $value->column_count;
                    $featureredSection->meta_data = json_encode($value->meta_data);
                    $featureredSection->save();
                }
            }
            $res['code'] = 200;
            $res['status'] = "success";
            $res['msg'] = 'Your changes have been saved';
        } else {
            $res['code'] = 204;
            $res['status'] = 'failure';
            $res['msg'] = 'Your changes could not be saved, please try again';
        }
        echo json_encode($res);
        exit;
    }

    public function actionGetDraftDataVD() {
        $studio_id = $this->studio_id;
        $getLog = VisualLog::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));

        if ($getLog['id']) {
            $res['code'] = 200;
            $res['status'] = "success";
            $res['msg'] = 'Success';
            $res['data']['logdata'] = json_decode($getLog['log']);
            $res['data']['featured_section'] = json_decode($getLog['featured_sec_log']);
            $res['data']['banner_style'] = $getLog['banner_style'];
            $res['data']['list_item_info'] = json_decode($getLog['list_item_info']);
            $res['data']['content_detail_info'] = json_decode($getLog['content_detail_info']);
            $res['data']['shop_list_item_info'] = json_decode($getLog['shop_list_item_info']);
            $res['data']['product_detail_info'] = json_decode($getLog['product_detail_info']);
        } else {
            $res['code'] = 204;
            $res['status'] = 'failure';
            $res['msg'] = 'No data found';
        }
        echo json_encode($res);
        exit;
    }

    public function actionPublishTemplateVD() {
            $studio_id = $this->studio_id;
            $getLog = VisualLog::model()->find('studio_id=:studio_id', array(':studio_id' => $studio_id));
            $getTheme = Studio::model()->findByPk($studio_id, array('select' => 'theme'));
            $path = ROOT_DIR . 'themes/' . $getTheme['theme'] . "/css/";
            if ($getLog['id']) {
                    $result = json_decode($getLog['log'], true);
                    $file = file($path . 'default.scss', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                    $index = array_search('/*SCSS variables end*/', $file);
                    $file = array_splice($file, $index); //echo "<pre>"; print_r($file); exit;
                    $outstring = implode("\n", $file);
                    foreach ($result as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                    foreach ($value1 as $key2 => $value2) {
                                            $string .= "$" . $key1 . '-' . $key2 . ':' . $value2 . ";\n";
                                    }
                            }
                    }
                    chmod($path . 'default.scss', 0777);
                    $newstring = $string . $outstring;
                    $filessss = fopen($path . 'default.scss', 'w+');
                    fwrite($filessss, $newstring);
                    fclose($filessss);
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/protected/vendor/scssphp/scss.inc.php';

                    $scss = new scssc();
                    $scss->setImportPaths($path);

                    if (file_exists($path . 'default.css')) {
                            chmod($path . 'default.css', 0777);
                    }

                    $file_content = $scss->compile('@import "default.scss"');

                    $fileopen = fopen($path . 'default.css', "w+");
                    fwrite($fileopen, $file_content);
                    fclose($fileopen);

                    /* banner Style */
                    $command = Yii::app()->db->createCommand();
                    $command->update('studio_banner_style', array('banner_style_code' => $getLog['banner_style']), 'studio_id=:studio_id', array(':studio_id' => $studio_id));

                    $jsfile = $getLog['banner_style'] . "/banner.html";
                    $jsroot = ROOT_DIR . 'bannerStyle/' . $jsfile;
                    $jspath = ROOT_DIR . 'themes/' . $getTheme['theme'] . '/views/layouts/banner.html';
                    copy($jsroot, $jspath);

                    /* get Feature Section from Draft  */
                    $getVdFeaturedSections = VdFeaturedSections::model()->findAll('studio_id=:studio_id', array(':studio_id' => $studio_id));
                    if (!empty($getVdFeaturedSections)) {
                            //print_r($getVdFeaturedSections);
                            foreach ($getVdFeaturedSections as $features) {
                                    $command = Yii::app()->db->createCommand();
                    $command->update('featured_section', array('layout_design' => $features['layout_design'], 'column_count' => $features['column_count'], 'meta_data_info' => $features['meta_data']), 'id=:id AND studio_id=:studio_id', array(':id' => $features['section_id'], ':studio_id' => $studio_id));
                            }
                            VdFeaturedSections::model()->deleteAll('studio_id=:studio_id', array(':studio_id' => $studio_id));
                    }   
                    $configData = Yii::app()->db->createCommand()->select('id')->from('vd_studio_config')->where('studio_id=:studio_id', array(':studio_id' => $studio_id))->queryRow();
                    if (empty($configData['id'])) { 
                            $model = new VdStudioConfig();
                            $model->featured_section_count  = $getLog['section_count'];
                            $model->feature_loading_style = $getLog['loading_style'];
                            $model->list_item_info = $getLog['list_item_info'];
                            $model->content_detail_info = $getLog['content_detail_info'];
                            $model->shop_list_item_info = $getLog['shop_list_item_info'];
                            $model->product_detail_info = $getLog['product_detail_info'];
                            $model->studio_id = $studio_id;
                            $model->save();
                    } else {
                            $vdcommand = Yii::app()->db->createCommand();
                            $vdcommand->update('vd_studio_config', array(
                                'feature_loading_style' => $getLog['loading_style'], 
                                'featured_section_count' => $getLog['section_count'], 
                                'list_item_info' => $getLog['list_item_info'], 
                                'content_detail_info' => $getLog['content_detail_info']), 'studio_id=:studio_id', array(':studio_id' => $studio_id));
                    }
                    
                    $res['code'] = 200;
                    $res['status'] = "success";
                    $res['msg'] = 'Your changes have been published';
            } else {
                    $res['code'] = 203;
                    $res['status'] = 'failure';
                    $res['msg'] = 'Your changes could not be published, Please try again';
            }
            echo json_encode($res);
            exit;
    }

    public function actionSaveLogoVD() {

        $studio_id = $this->studio_id;
        $logo_dimension = Yii::app()->general->getLogoDimension($studio_id);
        $width = $logo_dimension['logo_width'];
        $height = $logo_dimension['logo_height'];
        $cropDimension = array('original' => $width . 'x' . $height);
        $user_id = Yii::app()->user->id;
        $std = Studio::model()->findByPk($this->studio_id);
        $theme_folder = $std->theme;
        $parent_theme = $std->parent_theme;
        if ((isset($_FILES["Filedata"]["name"]) && $_FILES["Filedata"]["name"] != '')) {

            $return = $this->processLogoImage($theme_folder, $cropDimension, $type = 'logos');
            $imgName = $_FILES['Filedata']['name'];
            $std->show_sample_data = 0;
            $std->logo_file = $imgName;
            $std_logo = $std->save();
            if ($std_logo) {
                $res['code'] = 200;
                $res['status'] = "success";
                $res['msg'] = 'Success';
                $logoPath = Yii::app()->common->getLogopathForVisual($this->studio_id, $imgName);
                $res['logo_url'] = $logoPath;
            }
        } else {
            $res['code'] = 203;
            $res['status'] = 'failure';
            $res['msg'] = 'Unable to Upload';
        }
        echo json_encode($res);
        exit;
    }

    private function processLogoImage($theme_folder = '', $cropDimension = '', $type = 'logos') {
        $studio_id = Yii::app()->common->getStudiosId();
        $url = $this->createUrl('/template/homepage');
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/logos/' . $theme_folder;
        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            $file_info = pathinfo($_FILES['Filedata']['name']);
            $extension = array('PNG', 'png', 'JPG', 'jpeg', 'JPEG', 'jpg');
            if (!in_array($file_info['extension'], $extension)) {
                Yii::app()->user->setFlash('error', 'Please upload valid files formats(png,jpg,jpeg).');
                $url = $this->createUrl('/template/homepage');
                $this->redirect($url);
                exit;
            }

            $cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension, '', $file_info);

            $ret = $this->uploadLogo($_FILES['Filedata'], $cropDimension, $theme_folder, $path, $type);

            if (empty($ret)) {
                Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
                $this->redirect($url);
                exit;
            }
            return $ret;
        } else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name']);
            $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage'];
            $image_name = $_REQUEST['g_image_file_name'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
            if (@$path) {
                $fileinfo['name'] = $_REQUEST['g_image_file_name'];
                $fileinfo['error'] = 0;
                $_FILES['Filedata']['name'] = $_REQUEST['g_image_file_name'];
                $ret = $this->uploadLogo($fileinfo, $cropDimension, $theme_folder, $path, $type);
            } else {
                Yii::app()->user->setFlash('error', 'Error in uploading image,Please try another image');
                $this->redirect($url);
                exit;
            }
            Yii::app()->common->rrmdir($dir . '/jcrop');
            return $ret;
        } else {
            return false;
        }
    }

    public function actionGetLogo() {
        $studio_id = $this->studio_id;
        $logoPath = Yii::app()->common->getLogoFavPath($this->studio_id);
        if ($logoPath) {
            $res['code'] = 200;
            $res['status'] = "success";
            $res['msg'] = 'Success';
            $res['path'] = $logoPath;
        } else {
            $res['code'] = 203;
            $res['status'] = 'failure';
            $res['msg'] = 'unable to find logo';
        }
        echo json_encode($res);
        exit;
    }
        
    public function actionTutorialScreen() {
        $studio_id = $this->studio_id; 
        if(isset($_REQUEST['app_type']) && ($_REQUEST['app_type'] == 'android')){
            $app_type = 'android';            
        } else if(isset($_REQUEST['app_type']) && ($_REQUEST['app_type'] == 'ios')) {
            $app_type = 'ios';
		}
        $tscreen = TutorialScreen::model()->getAll($studio_id,$app_type);
        
        if(count($tscreen) > 0) {
            foreach ($tscreen as $key=>$value){
                $screen_image[] = trim($value['screen_img']);
            }
                if (!empty($tscreen)) {
                    $data['code'] = 200;
                    $data['status'] = "OK";
                    $data['msg'] = "Success";
                    $data['screen_img'] = $screen_image;
                } else {
                    $data['code'] = 200;
                    $data['status'] = 'failure';
                    $data['msg'] = 'No data found';
                }         
        } else {
            $data['code'] = 411;
            $data['status'] = "Error";
            $data['msg'] = "Please provide image in Tutorial Screen";            
        }     
        
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;       
        
    }    

    
    public function actionwatchHistory(){
        $studio_id = $this->studio_id;
        $language_id = Yii::app()->custom->getLanguage_id(strtolower($_REQUEST['lang_code']));
        $watch_status = StudioConfig::model()->getConfig($studio_id, 'studio_watch_history');
        if (!empty($watch_status)) {
            $watch_status = $watch_status['config_value'];
}
        if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0 && $watch_status > 0){
                $user_id = $_REQUEST['user_id'];
                $watchlist = VideoLogs::model()->getWatchHistoryList($user_id, $studio_id,'','', $watch_status,$language_id);                
                $data['code'] = 200;
                $data['status'] = "OK";
                $data['msg'] = "Success";
                $data['item_count'] = $watchlist['total'];
                $data['list'] = $watchlist['list'];
        } else {
            $data['code'] = 411;
            $data['status'] = "Error";
            $data['msg'] = "Authentication Error";            
        }
        
        $this->setHeader($data['code']);
        echo json_encode($data);
        exit;          
    }
/*
     * @auther Ashis(ashis@muvi.com)
     * @method private CancelUserSubscription() Cancel the subscription plan of user
     * @param srting authToken*
     * @param string cancel_note*
     * @param int user_id*
     * @param int plan_id* 
     * @param sring cancel_note*
     * @param int is_admin
     * @param Json Data Array
     */
	
    public function actionCancelUserSubscription() {
        $res = array();
        $card = array();
        if (isset($_REQUEST['cancel_note']) && trim($_REQUEST['cancel_note'])) {
            $studio_id = $this->studio_id;
            if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) {
                $user_id = $_REQUEST['user_id'];
                $usersub = new UserSubscription;
                $usersub = $usersub->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1, 'plan_id' => $_REQUEST['plan_id']));
                if (!empty($usersub)) {
                    $gateway_info = StudioPaymentGateways::model()->findByPk($usersub->studio_payment_gateway_id);
                    $payment_gateway_type = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                    
                    //Getting plan detail
                    $plan = new SubscriptionPlans;
                    $plan = $plan->findByPk($usersub->plan_id);
                    //Getting card detail
                    $card = new SdkCardInfos;
                    $card = $card->findByPk($usersub->card_id);
                    
                    $is_paid = 1;
                    $file_name = '';
                    if (isset($plan) && intval($plan->is_post_paid)) {
                        //If trial period expires
                        $start_date = date("Y-m-d", strtotime($usersub->start_date));
                        $today_date = gmdate('Y-m-d');
                        if (strtotime($start_date) < strtotime($today_date)) {
                            $trans_data = $this->processTransaction($card, $usersub);
                            $is_paid = $trans_data['is_success'];
                            //Save a transaction detail
                            if (intval($is_paid)) {
                                //Getting Ip address
                                $ip_address = Yii::app()->request->getUserHostAddress();
                                $transaction = new Transaction;
                                $transaction->user_id = $user_id;
                                $transaction->studio_id = $studio_id;
                                $transaction->plan_id = $usersub->plan_id;
                                $transaction->transaction_date = new CDbExpression("NOW()");
                                $transaction->payment_method = $this->PAYMENT_GATEWAY[$gateway_info->short_code];
                                $transaction->transaction_status = $trans_data['transaction_status'];
                                $transaction->invoice_id = $trans_data['invoice_id'];
                                $transaction->order_number = $trans_data['order_number'];
                                $transaction->amount = $trans_data['amount'];
                                $transaction->response_text = $trans_data['response_text'];
                                $transaction->subscription_id = $usersub->id;
                                $transaction->ip = $ip_address;
                                $transaction->created_by = $user_id;
                                $transaction->created_date = new CDbExpression("NOW()");
                                $transaction->save();
}
                        }
                    }
                    
                    if (intval($is_paid)) {
                        $success = 1;
                        //Cancel customer's account from payment gateway
                        $cancel_acnt = $this->cancelCustomerAccount($usersub, $card);
                        if (isset($payment_gateway_type) && ($payment_gateway_type == 'paypal' || $payment_gateway_type == 'paypalpro') && ($usersub->card_id == '' || !$usersub->card_id)) {
                            if (isset($cancel_acnt) && $cancel_acnt['ACK'] == 'Success') {
                                $success = 1;
                            } else {
                                $success = 0;
                            }
                        } else {
                            //Delete card detail of user
                            if (trim($usersub->profile_id)) {
                                SdkCardInfos::model()->deleteAll('studio_id = :studio_id AND user_id = :user_id AND profile_id = :profile_id', array(
                                    ':studio_id' => $studio_id,
                                    ':user_id' => $user_id,
                                    ':profile_id' => $usersub->profile_id
                                ));
                            }
                        }

                        if ($success) {
                            //Inactivate user subscription
                            $reason_id = isset($_REQUEST['cancelreason']) ? $_REQUEST['cancelreason'] : '';
                            $cancel_note = isset($_REQUEST['cancel_note']) ? $_REQUEST['cancel_note'] : '';

                            $usersub->status = 0;
                            $usersub->cancel_date = gmdate('Y-m-d H:i:s');
                            $usersub->cancel_reason_id = $reason_id;
                            $usersub->cancel_note = htmlspecialchars($cancel_note);
                            $usersub->payment_status = 0;
                            $usersub->partial_failed_date = '';
                            $usersub->partial_failed_due = 0;
                            $usersub->canceled_by = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? 0 : $user_id;
                            $usersub->save();

                            //Set flag for is_deleted in sdk user table, so that it will re-activate for next time
                            if($usersub->is_subscription_bundle==0){
                            $usr = new SdkUser;
                            $usr = $usr->findByPk($user_id);
                            $usr->is_deleted = 1;
                            $usr->deleted_at = gmdate('Y-m-d H:i:s');
                            $usr->will_cancel_on = ((isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin']))) ? '' : $usersub->start_date;
                            $usr->save();
                            }
                            //Send an email to user
                            if (isset($_REQUEST['is_admin']) && intval($_REQUEST['is_admin'])) {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation', $file_name, '', '', '', '', '', $lang_code);
                            } else {
                                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'subscription_cancellation_user', $file_name, '', '', '', '', '', $lang_code);
                            }

                            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('subscription_cancellation', $studio_id);
                            if ($isEmailToStudio) {
                                $admin_email = $this->sendStudioAdminEmails($studio_id, 'admin_subscription_cancelled', $user_id, '', 0);
                            }
                            $data['code'] = 200;
                            $data['status'] = "Success";
                            $data['msg'] = "Subscription has been cancelled successfully";
                        } else {
                            $data['code'] = 400;
                            $data['status'] = "Error";
                            $data['msg'] = "Couldn't able to process transcation";
                        }
                    }else{
                        $data['code'] = 400;
                        $data['status'] = "Error";
                        $data['msg'] = "Couldn't able to process transcation";
                    }
                }else{
                    $data['code'] = 400;
                    $data['status'] = "Error";
                    $data['msg'] = "User does not have any subscription plan or subscription already cancelled";
                }
            }else{
                $data['code'] = 400;
                $data['status'] = "Error";
                $data['msg'] = "Please provide required parameter(s)";
            }
        }else{
            $data['code'] = 400;
            $data['status'] = "Error";
            $data['msg'] = "Please provide required parameter(s)";
        }
        echo json_encode($data);
        exit;
    }    
    public function mergeArray($filepath = "") {
        $lng = include($filepath);
        $trans_word = array();
        if(!empty(@$lng['static_messages'])){
            $static_messages    = is_array($lng['static_messages'])? $lng['static_messages'] : array();
            $js_messages        = is_array($lng['js_messages'])? $lng['js_messages'] : array();
            $server_messages    = is_array($lng['server_messages'])? $lng['server_messages'] : array();
        }else{
            $static_messages    = is_array($lng)? $lng : array();
            $js_messages        = array();
            $server_messages    = array();
        }
        $trans_word = array_merge($static_messages,$js_messages,$server_messages);
        return $trans_word;
    }
}
