<?php

class PolicyController extends Controller {

    public $studio_id;
    public $getSingleRule;
    public $headerinfo = '';
    public $layout = '';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin';
        $this->studio_id = Yii::app()->common->getStudiosId();
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            Yii::app()->layout = 'partners';
        } else {
            Yii::app()->layout = 'admin';
        }
        if (!(Yii::app()->user->id)) {
            $this->redirect($redirect_url);
            exit;
        } else {
            $this->checkPermission();
            if (Yii::app()->controller->action->id != 'test' && Yii::app()->controller->action->id != 'ajaxtest') {
                if (!PolicyRules::model()->getConfigRule($this->studio_id)) {
                    Yii::app()->user->setFlash('error', 'Please enable policy.');
                    $url = $this->createUrl('/management/ContentSetting');
                    /* $url = $this->createUrl('/admin/advanced'); */
                    $this->redirect($url);
                }
            }
        }
        return true;
    }

    public function actionIndex() {
        $this->headerinfo = "Policy Rules";
        $this->pageTitle = Yii::app()->name . ' | ' . $this->headerinfo;
        $this->breadcrumbs = array('Policies');

        $page_size = 20;
        $criteria = new CDbCriteria();
        $criteria->condition = 't.studio_id = :studio_id';
        $criteria->order = 't.id DESC';
        $criteria->params = array(':studio_id' => $this->studio_id);

        $item_count = PolicyRules::model()->count($criteria);

        $pages = new CPagination($item_count);
        $pages->setPageSize($page_size);
        $pages->applyLimit($criteria);



        //$policyRules = PolicyRules::model()->with('policy_rules_mapping')->findAll('t.studio_id=:studio_id', array(':studio_id' => $this->studio_id));
        $policyRules = PolicyRules::model()->with('policy_rules_mapping')->findAll($criteria);
        $this->render('index', array('data_arr' => $policyRules,
            'item_count' => $item_count,
            'page_size' => $page_size,
            'items_count' => $item_count,
            'pages' => $pages));
    }

    public function actionAjaxGetRuleMapping() {
        $policyRulesArr = array();
        if (isset($_POST['rule_id'])) {
            $policyRules = PolicyRules::model()->with('policy_rules_mapping')->find('t.id=:rules_id AND t.studio_id=:studio_id', array(':rules_id' => $_POST['rule_id'], 'studio_id' => $this->studio_id));
            if ($policyRules) {
                $policyRulesArr['name'] = $policyRules['name'];
                foreach ($policyRules['policy_rules_mapping'] as $ruleMapping) {
                    $policyRulesArr['mapping_rules'][] = array(
                        'policy_type' => ucwords($ruleMapping['policy_type']),
                        'policy_value' => $ruleMapping['policy_value'],
                        'duration_type' => $ruleMapping['duration_type'],
                    );
                }
            }
        }
        echo json_encode($policyRulesArr);
    }

    public function actionRulesBuilder() {
        $this->headerinfo = "Add Rules";
        $this->pageTitle = Yii::app()->name . ' | ' . $this->headerinfo;
        $this->breadcrumbs = array('Policies' => array('/policy'), $this->headerinfo);
        $rule = array();
        if (isset($_GET['uniqid']) && $_GET['uniqid']) {
            $rule = PolicyRules::model()->with('policy_rules_mapping')->find('t.uniqid=:uniqid AND t.studio_id=:studio_id', array(':uniqid' => $_REQUEST['uniqid'], ':studio_id' => $this->studio_id));
//            echo "<pre>"; print_r($rule['policy_rules_mapping']); exit;
            $this->headerinfo = "Edit Rules - " . $rule['name'];
            $this->pageTitle = Yii::app()->name . ' | ' . $this->headerinfo;
            $this->breadcrumbs = array('Policies' => array('/policy'), 'Edit Rules');
        }
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['rule']) && !empty($_POST['rule'])) {
                if (PolicyRules::model()->saveRule($_POST, $rule)) {
                    if (!empty($rule)) {
                        Yii::app()->user->setFlash('success', 'Rules updated successfully.');
                    } else {
                        Yii::app()->user->setFlash('success', 'Rules saved successfully.');
                    }
                } else {
                    Yii::app()->user->setFlash('error', 'Unable to save rule.');
                }
                $url = $this->createUrl('/policy');
                $this->redirect($url);
                /* $this->redirect(Yii::app()->request->urlReferrer); */
            }
        }

        $policies = Yii::app()->db->createCommand()
                        ->select('id, policy_name, policy_field_type, policy_option, default_values')
                        ->from('content_policy_master')
                        ->where('status=1')
                        ->order('policy_name ASC')->queryAll();

        $this->render('rulesbuilder', array('policies' => $policies, 'rule' => $rule));
    }

    public function actionDeleteRule() {
        if (!empty($_REQUEST['rule_id'])) {
            $response = PolicyRules::model()->deleteRule($_REQUEST['rule_id']);
            if ($response['error'] == false) {
                Yii::app()->user->setFlash('success', 'Rules deleted successfully.');
            } else {
                Yii::app()->user->setFlash('error', $response['message']);
            }
        } else {
            Yii::app()->user->setFlash('error', "Request data missing. Please try again.");
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionChangeStatus() {
        $response = array();
        if (isset($_POST['id']) && $_POST['id']) {
            $response = PolicyRules::model()->updateStatus($_POST['id']);
        }
        echo json_encode($response);
    }

    function actionTest() {
        //$rules = Yii::app()->policy->subscriptionRule(); echo "<pre>"; print_r($rules);exit;
        Yii::app()->layout = 'main';
        // echo Yii::app()->policy->resolutionByStreamId(2188); exit; /* returns SD/HD or null */
        //echo Yii::app()->policy->resolutionByFilmId(1660); exit; /* returns SD/HD or null */
        //echo Yii::app()->policy->viewByStreamId(2188); exit; /* returns view number or null */
        //echo Yii::app()->policy->viewByFilmId(1720); /* returns view number or null */
        //echo Yii::app()->policy->watchDurationByStreamId(2188); exit; /* returns duration in second or null */
        //echo Yii::app()->policy->watchDurationByFilmId(1720); exit; /* returns duration in second or null */
        //echo Yii::app()->policy->accessDurationByStreamId(2188); exit; /* returns duration in second or null */
        // echo Yii::app()->policy->accessDurationByFilmId(1720); exit; /* returns duration in second or null */
        //echo Yii::app()->policy->userByFilmId(1720); exit; /* returns duration in second or null */
        //$rules = Yii::app()->policy->rules(2188); echo "<pre>"; print_r($rules); exit;
        //$stream = movieStreams::model()->with('film')->findAll('t.studio_id=:studio_id AND t.is_active=:is_active', array(':studio_id' => $this->studio_id, ':is_active' => 1), array('order' => 'id DESC'));
        //$rules = Yii::app()->policy->verifyRules(33745); exit;
        //$rules = Yii::app()->policy->rules(33745); print_r($rules); exit;
        //$rules = Yii::app()->policyrule->chkUesrSubscription(193); print_r($rules); exit;
        $stream = Yii::app()->db->createCommand()
                ->select('a.id, a.full_movie, a.episode_title, a.studio_id, b.name')
                ->from('movie_streams a')
                ->join('films b', 'b.id=a.movie_id')
                ->where('a.studio_id=:studio_id AND a.is_active=1', array(':studio_id' => $this->studio_id))
                ->group('a.id')
                ->order('a.id DESC')
                ->queryAll();

        $this->render('test', array('stream' => $stream));
        exit;
    }

    function actionAjaxtest() {
        $rules = Yii::app()->policy->verifyRules($_REQUEST['id']);
        //$rules = Yii::app()->policy->rules($_REQUEST['id']);
        echo json_encode($rules);
        exit;
    }

}
