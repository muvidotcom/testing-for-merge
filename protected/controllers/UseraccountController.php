<?php

class UseraccountController extends Controller {

    public $defaultAction = 'usergeneratedcontent';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        } else {
            $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        $this->pageTitle = Yii::app()->name . ' | Manage Home';
        return true;
    }

    /**
     * @function to save the settings option of the studio
     * @author kailash@muvi.com
     * @return HTML 
     */
    public function actionSettings() {
        $studio_id = Yii::app()->common->getStudioId();
        $data = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_generated_content'));
        $data2 = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_review_content'));
        if (isset($_POST['usergenbtn'])) {
            //print_r($studio_id);exit;
            if (empty($data)) {
                $is_generate = new StudioConfig();
                $is_generate->studio_id = $studio_id;
                $is_generate->config_key = 'user_generated_content';
            } else {
                $is_generate = StudioConfig::model()->findByPk($data->id);
                $is_generate->id = $data->id;
            }
            $is_generate->config_value = !empty($_POST['autoUserGen']) ? 1 : 0;
            $is_generate->save();
            if ($is_generate == 1) {
                $data2 = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_review_content'));
                if (empty($data2)) {
                    $review_generate = new StudioConfig();
                    $review_generate->studio_id = $studio_id;
                    $review_generate->config_key = 'user_review_content';
                } else {
                    $review_generate = StudioConfig::model()->findByPk($data2->id);
                    $review_generate->id = $data2->id;
                }
                $review_generate->config_value = !empty($_POST['user_review']) ? 1 : 0;
                $review_generate->save();
                Yii::app()->user->setFlash('success', 'updated successfully.');
                $data = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_generated_content'));

                $data2 = StudioConfig::model()->findByAttributes(array('studio_id' => $studio_id, 'config_key' => 'user_review_content'));
            }
        }
//      print_r($data->config_value);exit;
        $this->render('settings', array('is_generate' => $data->config_value, 'review_gen' => $data2->config_value));
    }

    public function actionUserGeneratedContent() {
        
        $this->pageTitle = "Muvi | UGC List";
        $this->breadcrumbs = array('Manage UGC');
        $this->headerinfo = "Manage UGC";
        $cond = " ";
        $contentType = '';
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $user_id = 0;
        $IsEnableUGC = StudioConfig::model()->getConfig($studio_id,'user_generated_content');
        if(empty($IsEnableUGC) || $IsEnableUGC['config_value']==0){
            Yii::app()->user->setFlash('error', 'Oops! Please enable UGC');
            $url = $this->createUrl('userfeature/Settings');
            $this->redirect($url);
        }
        if (Yii::app()->common->allowedUploadContent($studio_id) > 0)
            $allow_new = 1;
        else
            $allow_new = 0;

        $params = array(':studio_id' => $studio_id);
        $pdo_cond_arr = array(":studio_id" => Yii::app()->user->studio_id);
        $pdo_cond = " f.studio_id=:studio_id AND ms.sdk_user_id != 0 ";
        $con = Yii::app()->db;
        $content_type = Yii::app()->general->content_count($studio_id);
        if (isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']) {
            $search_form = $_REQUEST['searchForm'];
            if (isset($search_form) && $search_form['update_date'] == '' && $search_form['content_category_value'] == '' && $search_form['search_text'] == '' && $search_form['custom_metadata_form_id'] == '' && $search_form['sortby'] == '') {
                $url = $this->createUrl('admin/managecontent');
                $this->redirect($url);
            } else {
                $searchData = $_REQUEST['searchForm'];
                if ($searchData['update_date']) {
                    $sdate = json_decode(stripslashes($searchData['update_date']), TRUE);
                    $pdo_cond .= " AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') >= :start_dt AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') <= :end_dt ";
                    $pdo_cond_arr[':start_dt'] = $sdate['start'];
                    $pdo_cond_arr[':end_dt'] = $sdate['end'];
                }
                if ($searchData['search_text']) {
                    $pdo_cond .= " AND ((LOWER(f.name) LIKE :search_text) OR (LOWER(ms.episode_title) LIKE :search_text)) ";
                    $pdo_cond_arr[':search_text'] = "%" . strtolower(trim($searchData['search_text'])) . "%";
                }
                if ($searchData['contenttype'] == 1) {//Audio
                    $pdo_cond .= " AND f.content_types_id IN(5,6,8) ";
                } else {
                    $pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
                }
                if ($searchData['filterby'] == 0) {
                    if ($searchData['content_category_value']) {
                        foreach ($searchData['content_category_value'] as $key => $value) {
                            $cat_cond .= "FIND_IN_SET($value,f.content_category_value) OR ";
                        }
                        $cat_cond = rtrim($cat_cond, 'OR ');
                        $pdo_cond .= " AND ($cat_cond) ";
                        /* $pdo_cond .= " AND (f.content_category_value & :content_category_value) ";
                          $pdo_cond_arr[':content_category_value'] = array_sum($searchData['content_category_value']); */
                    }
                } elseif ($searchData['filterby'] == 1) {
                    if ($searchData['custom_metadata_form_id']) {
                        if (count($searchData['custom_metadata_form_id']) == 1) {
                            $get_is_child = Yii::app()->db->createCommand()
                                    ->from('custom_metadata_form')
                                    ->select('content_type,is_child')
                                    ->where('id =' . $searchData['custom_metadata_form_id'][0])
                                    ->queryROW();
                        }
                        if (($get_is_child['content_type'] == 1) && ($get_is_child['is_child'] == 0)) {
                            $andwherecondition = "`f`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . ") AND ms.is_episode=0";
                        } else {
                            $andwherecondition = "(`f`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . ")) OR (`ms`.`custom_metadata_form_id` IN (" . implode(',', $searchData['custom_metadata_form_id']) . "))";
                        }
                        $custom_metadata_form_id_flag = 1;
                    }
                }
            }
        } else {
            if ((isset($content_type) && ($content_type['content_count'] == 4))) {
                $pdo_cond .= " AND f.content_types_id IN(5,6,8) ";
            } else {
                $pdo_cond .= " AND f.content_types_id IN(1,2,3,4) ";
            }
        }
        //echo $pdo_cond."<pre>";print_r($searchData);print_r($pdo_cond_arr);exit;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id='{$studio_id}' AND (language_id={$language_id} OR parent_id=0 AND id NOT IN (SELECT parent_id FROM content_category WHERE studio_id='{$studio_id}' AND language_id={$language_id}))";
        $contenttypeList = Yii::app()->db->createCommand($sql)->queryAll();

        $contentTypes = CHtml::listData($contenttypeList, 'id', 'category_name');
        $order = ' ms.id DESC ';
        $orderType = 'desc';
        $sort = 'update_date';
        if (isset($_REQUEST['sort'])) {
            if ($_REQUEST['sort'] == 'update_date' && $_REQUEST['sort_type'] == 'asc') {
                $order = ' ms.last_update_date ASC ';
                $orderType = 'asc';
            }
        }

        if (isset($searchData['sortby'])) {
            if ($searchData['sortby'] == 1) {//most viewed
                $mostviewed = 1;
            } elseif ($searchData['sortby'] == 2) {//A-Z
                $order = ' f.name ASC ';
            } elseif ($searchData['sortby'] == 3) {//Z-A
                $order = ' f.name DESC ';
            }
        }
        //Pagination Implimented ..
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size;
        }

        //PDO Query
        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_category_value,f.content_types_id,f.name,f.ppv_plan_id,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,ms.full_movie,ms.thirdparty_url,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode,ms.embed_id,ms.is_episode,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,ms.is_download_progress,ms.video_management_id,f.language_id,f.parent_id,ms.episode_parent_id,ms.episode_language_id,ms.is_demo,f.parent_content_type_id,f.custom_metadata_form_id,ms.custom_metadata_form_id as cmfid,ms.is_offline, ms.review_flag')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND f.parent_id=0 AND ms.episode_parent_id=0 AND ms.review_flag=1 AND ' . $pdo_cond, $pdo_cond_arr)
                ->order($order)
                ->limit($page_size, $offset);
        if ($custom_metadata_form_id_flag) {
            $command->andwhere($andwherecondition);
        }

        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $offline_val = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'offline_view');
        $pages = new CPagination($count);

        $pages->setPageSize($page_size);
        // simulate the effect of LIMIT in a sql query
        $end = ($pages->offset + $pages->limit <= $count ? $pages->offset + $pages->limit : $count);
        $sample = range($pages->offset + 1, $end);
        $form = Yii::app()->general->formlist($studio_id);
        $form = self::unsetForms($form, @$_REQUEST['searchForm']['is_physical']);
        if ($data) {
            $view = array();
            $cnt = 1;
            $movieids = '';
            $stream_ids = '';
            $movieIdsForliveStream = '';
            $movieLiveStream = array();
            $videoGalleryIdsForvideoName = '';
            $audioGalleryIdsForaudioName = '';
            $movievideogalleryName = array();
            $movieaudiogalleryName = array();
            $videoEncodingLogarray = array();
            $list = CHtml::listData($form, 'id', 'name');
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .= "'" . $v['stream_id'] . "',";
                } else {
                    $movieids .= "'" . $v['id'] . "',";
                }
                // For live sreaming
                if ($v['content_types_id'] == 4) {
                    $movieIdsForliveStream .= "'" . $v['id'] . "',";
                }
                //video name from video gallery
                if ($v['video_management_id'] != 0) {
                    $videoGalleryIdsForvideoName .= "'" . $v['stream_id'] . "',";
                }
                //audio name from video gallery
                if ($v['video_management_id'] != 0 && ($v['content_types_id'] == 6 || $v['content_types_id'] == 5)) {
                    $audioGalleryIdsForaudioName .= "'" . $v['stream_id'] . "',";
                }
                //video name from video gallery
                if ($v['full_movie'] != '' && $v['is_converted'] == 0) {
                    $videoEncodingLogarray[] = $v['stream_id'];
                }
                if ($v['content_types_id'] == 5 || ( $v['content_types_id'] == 6 && $v['is_episode'] == 1 )) {
                    $playlist = Yii::app()->common->getAllPlaylistName($studio_id, $user_id, 1);
                }
                $data[$k]['formname'] = Yii::app()->general->getFormName($v, $studio_id, $form, $list);
            }
            $videoEncodingLog = "";
            $videoEncodingLog = implode(",", $videoEncodingLogarray);
            $viewcount = array();
            $movieids = rtrim($movieids, ',');
            $stream_ids = rtrim($stream_ids, ',');
            $movieIdsForliveStream = rtrim($movieIdsForliveStream, ',');
            $videoGalleryIdsForvideoName = rtrim($videoGalleryIdsForvideoName, ',');
            $audioGalleryIdsForaudioName = rtrim($audioGalleryIdsForaudioName, ',');
            // Get the Video view Counts 
            if ($movieids) {
                $sql = "SELECT COUNT(movie_id) AS cnt, movie_id,video_type FROM video_logs WHERE movie_id IN(" . $movieids . ") GROUP BY movie_id ";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $viewcount[$val['movie_id']] = $val['cnt'];
                    }
                }
            }
            if ($stream_ids) {
                $sql = "SELECT COUNT(video_id) AS cnt, video_id,video_type FROM video_logs WHERE video_id IN(" . $stream_ids . ") GROUP BY video_id";
                $videoLog = $con->createCommand($sql)->queryAll();
                if ($videoLog) {
                    foreach ($videoLog as $key => $val) {
                        $episodeViewcount[$val['video_id']] = $val['cnt'];
                    }
                }
            }
            //Live stream url
            if ($movieIdsForliveStream) {
                $sql = "SELECT movie_id,feed_method,feed_url,stream_url,stream_key,start_streaming FROM livestream WHERE movie_id IN(" . $movieIdsForliveStream . ") and studio_id = " . $studio_id;
                $liveSreamData = $con->createCommand($sql)->queryAll();
                if ($liveSreamData) {
                    foreach ($liveSreamData as $key => $val) {
                        if ($val['feed_url'] != "") {
                            $movieLiveStream[$val['movie_id']] = 1;
                            if ($val['feed_method'] == 'push') {
                                $movieLiveStream['feed_method'][$val['movie_id']] = $val['feed_method'];
                                $movieLiveStream['stream_url'][$val['movie_id']] = $val['stream_url'];
                                $movieLiveStream['stream_key'][$val['movie_id']] = $val['stream_key'];
                                $movieLiveStream['start_streaming'][$val['movie_id']] = $val['start_streaming'];
                            }
                        }
                    }
                }
            }
            //Video name from video gallery
            if ($videoGalleryIdsForvideoName || $audioGalleryIdsForaudioName) {
                $sql = "SELECT ms.id, vm.video_name FROM movie_streams ms, video_management vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $videoGalleryIdsForvideoName . ") and ms.studio_id = " . $studio_id;
                $videoManagement = $con->createCommand($sql)->queryAll();
                if ($videoManagement) {
                    foreach ($videoManagement as $key => $val) {
                        if ($val['video_name'] != "") {
                            $movievideogalleryName[$val['id']] = $val['video_name'];
                        }
                    }
                }
                //Get Audio Name from Audio Gallery
                if ($audioGalleryIdsForaudioName) {
                    $sql = "SELECT ms.id, vm.audio_name FROM movie_streams ms, audio_gallery vm WHERE ms.video_management_id=vm.id and ms.id IN(" . $audioGalleryIdsForaudioName . ") and ms.studio_id = " . $studio_id;
                    $audioManagement = $con->createCommand($sql)->queryAll();
                    if ($audioManagement) {
                        foreach ($audioManagement as $key => $val) {
                            if ($val['audio_name'] != "") {
                                $movieaudiogalleryName[$val['id']] = $val['audio_name'];
                            }
                        }
                    }
                }
            }

            //Get video encoding data
            $encodingData = array();
            if ($videoEncodingLog) {
                $encLogData = Encoding::model()->getEncodingData($videoEncodingLog);
                if ($encLogData) {
                    foreach ($encLogData AS $key => $val) {
                        $now = new DateTime();
                        $post = new DateTime($val['expected_end_time']);
                        $interval = $now->diff($post);
                        if ($interval) {
                            $encodingData[$val['movie_stream_id']] = $interval->h . ':' . $interval->i . ':' . $interval->s;
                        } else {
                            $encodingData[$val['movie_stream_id']] = '0:0:0';
                        }
                    }
                }
            }

            //Get Posters for the Movies 
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = $con->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                    }
                }
            }
            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = $con->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }

        $studio = $this->studio;
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $pcontent = Yii::app()->general->getPartnersContentIds();
            $contentid = $pcontent['movie_id'];
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id, "$contentid", Yii::app()->user->id);
        } else {
            $all_videos = VideoManagement::model()->get_videodetails_by_studio_id($studio_id);
        }
        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($studio_id, 'embed_watermark');
        $embedWaterMArkEnable = 0;
        if ($getStudioConfig) {
            if (@$getStudioConfig['config_value'] == 1) {
                $embedWaterMArkEnable = 1;
            }
        }
        if (isset(Yii::app()->user->role_id) && (Yii::app()->user->role_id == 4)) {
            $notpartner = 0;
        } else {
            $notpartner = 1;
            $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = ' . $studio_id));
            $geoexist = StudioContentRestriction::model()->exists('studio_id=:studio_id', array(':studio_id' => $studio_id));
        }
        $bucketInfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $data_enable = Yii::app()->general->monetizationMenuSetting($studio_id);
        $srest = StudioCountryRestriction::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $grestcat = GeoBlockCategory::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $poster_sizes = Yii::app()->common->getCropDimension($studio_id);
        if ($mostviewed) {
            foreach ($data AS $k => $v) {
                if ($v['is_episode']) {
                    $cnt = $episodeViewcount[$v['stream_id']] ? $episodeViewcount[$v['stream_id']] : 0;
                } else {
                    $cnt = ($viewcount[$v['id']]) ? $viewcount[$v['id']] : 0;
                }
                $data[$k]['cnt'] = $cnt;
                $num[] = $cnt;
            }
            array_multisort($num, SORT_DESC, $data);
        }
        $this->render('usergeneratedcontent', array('movievideogalleryName' => @$movievideogalleryName, 'movieaudiogalleryName' => $movieaudiogalleryName, 'movieLiveStream' => @$movieLiveStream, 'embedWaterMArkEnable' => $embedWaterMArkEnable, 'data' => $data, 'studio' => $studio, 'content_category_value' => @$searchData['content_category_value'], 'dateRange' => @$searchData['update_date'], 'searchText' => @$searchData['search_text'], 'posters' => @$posters, 'episodePosters' => @$epsodePosters, 'viewcount' => @$viewcount, 'episodeViewcount' => @$episodeViewcount, 'item_count' => @$count, 'page_size' => $page_size, 'pages' => $pages, 'sample' => @$sample, 'contentList' => @$contentTypes, 'sort' => @$sort, 'orderType' => @$orderType, 'studio_ads' => @$studio_ads, 'allow_new' => $allow_new, 'geoexist' => $geoexist, 'notpartner' => $notpartner, 'bucketInfo' => $bucketInfo, 'data_enable' => $data_enable, 'srest' => $srest, 'grestcat' => $grestcat, 'form' => $form, 'searchData' => $searchData, 'encodingData' => @$encodingData, 'playlistName' => $playlist, 'poster_size' => $poster_sizes, 'videoEncodingLog' => $videoEncodingLog, 'offline_val' => $offline_val));

        //$this->render('usergeneratedcontent', array('films' => $films, 'pagination' => $pagination, 'posters' => $posters, 'episodePosters' => $epsodePosters));
    }

    function unsetForms($form, $is_physical = 0) {
        foreach ($form as $key => $value) {
            if ($is_physical) {
                if ($value['parent_content_type_id'] != 5)
                    unset($form[$key]);
            }else {
                if ($value['parent_content_type_id'] == 5)
                    unset($form[$key]);
            }
        }
        return $form;
    }

    public function actionUserGeneratedContent00() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        $items_per_page = 20;
        $page_size = $limit = $items_per_page;
        $offset = 0;
        $offsetq = 0;
        $tab_active = 0;
        $this->headerinfo = 'Manage UGC';
        $this->breadcrumbs = array("Manage UGC");

        if (isset($_REQUEST['p'])) {
            $offset = ($_REQUEST['p'] - 1) * $limit;
            $page_number = $_REQUEST['p'];
            // print_r($page_number);exit;
            $tab_active = 1;
        } else {
            $page_number = 1;
        }

        $studio_id = Yii::app()->common->getStudioId();
        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),f.id,f.content_types_id,f.uniq_id,f.name,f.ppv_plan_id,f.permalink,ms.id AS stream_id,ms.is_episode,ms.episode_title,ms.sdk_user_id,ms.is_converted,ms.review_flag')
                ->from('films f ,movie_streams ms ')
                ->where('f.id = ms.movie_id AND ms.sdk_user_id!=0 AND ms.studio_id= "' . $studio_id . '"')
                ->limit($limit, $offset);
        $films = $command->queryAll();



        $item_count = Yii::app()->db->createCommand('SELECT FOUND_ROWS() cnt')->queryScalar();

        if ($films) {
            $movieids = '';
            $stream_ids = '';
            foreach ($films AS $k => $v) {
                if ($v['is_episode']) {
                    $stream_ids .= "'" . $v['stream_id'] . "',";
                } else {
                    $movieids .= "'" . $v['id'] . "',";
                }
            }
            $movieids = rtrim($movieids, ',');
            //print_r($movieids);exit;
            $stream_ids = rtrim($stream_ids, ',');
            //print_r($stream_ids);exit;
            //Get Posters for Movies
            if ($movieids) {
                $psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(" . $movieids . ") AND object_type='films' ";
                $posterData = Yii::app()->db->createCommand($psql)->queryAll();
                if ($posterData) {
                    foreach ($posterData AS $key => $val) {
                        $posters[$val['movie_id']] = $val;
                        //print_r($posters);exit;
                    }
                }
            }

            //Get Posters for Episode
            if ($stream_ids) {
                $psql = "SELECT id,poster_file_name,wiki_data,object_id AS movie_id FROM posters WHERE object_id  IN(" . $stream_ids . ") AND object_type='moviestream' ";
                $sposter = Yii::app()->db->createCommand($psql)->queryAll();
                if ($sposter) {
                    foreach ($sposter AS $key => $val) {
                        $epsodePosters[$val['movie_id']] = $val;
                    }
                }
            }
        }
        //print_r($films);exit;

        if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
            $http = 'https://';
        } else {
            $http = 'http://';
        }
        $url = $http . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//            print_r($url);exit;
        if ($item_count > 0) {
            $page_url = Yii::app()->general->full_url($url, '?p=');
            $page_url = Yii::app()->general->full_url($page_url, '&p=');

            $pages = new bootPagination();

            $pages->pagenumber = $page_number;

            $pages->pagesize = $page_size;
            $pages->totalrecords = $item_count;
            $pages->showfirst = true;
            $pages->showlast = true;
            $pages->paginationcss = "pagination-normal";
            $pages->paginationstyle = 0;
            $pages->defaultUrl = $page_url;
            if (strpos($page_url, '?') > -1) {
                $pages->paginationUrl = $page_url . "&p=[p]";
            } else {
                $pages->paginationUrl = $page_url . "?p=[p]";
            }
            $pagination = $pages->process();
        }

//        print_r($films);exit;
        $this->render('usergeneratedcontent', array('films' => $films, 'pagination' => $pagination, 'posters' => $posters, 'episodePosters' => $epsodePosters));
    }

    public function actionEditMovie() {
        $this->pageTitle = Yii::app()->name . ' | ' . 'Edit Content ';
        $this->breadcrumbs = array("Manage Content", "Content Library" => array('useraccount/usergeneratedcontent'), 'Edit Movie Info');
        $language_id = $this->language_id;
        $default_lang_id = 20;
        $studio_id = $this->studio->id;
        if (isset($_REQUEST['movie_id']) && $_REQUEST['movie_id'] && $_REQUEST['movie_stream_id']) {
            $dbcon = Yii::app()->db;
            //PDO Query
            $pdo_cond = 'm.id=:stream_id AND m.studio_id=:studio_id';
            $pdo_cond_arr = array(':stream_id' => $_REQUEST['movie_stream_id'], ':studio_id' => Yii::app()->user->studio_id);
            $fetchcustomfield = 'f.custom1,f.custom2,f.custom3,f.custom4,f.custom5,f.custom6,f.custom7,f.custom8,f.custom9,f.custom10,m.custom1 as cs1,m.custom2 as cs2,m.custom3 as cs3,m.custom4 as cs4,m.custom5 as cs5,m.custom6 as cs6,f.custom_metadata_form_id,m.custom_metadata_form_id as cmfid,m.is_downloadable';
            $command = Yii::app()->db->createCommand()
                    ->select('f.id,f.content_category_value,f.content_subcategory_value,f.parent_content_type_id as parent_content_type ,m.id as movie_stream_id,f.uniq_id,f.name,f.language,f.censor_rating,f.genre,f.story,f.release_date,f.content_type_id,m.full_movie,m.episode_title,m.episode_date,m.episode_story,m.episode_number,m.series_number,m.is_episode,if(UNIX_TIMESTAMP(m.content_publish_date) = 0,\'\',m.content_publish_date) AS content_publish_date,f.content_types_id,m.is_converted,m.is_download_progress,m.video_management_id,f.language_id,f.parent_id,f.start_time,f.duration,m.episode_language_id,m.episode_parent_id,' . $fetchcustomfield . '')
                    ->from('films f ,movie_streams m ')
                    ->where('f.id = m.movie_id AND  ' . $pdo_cond, $pdo_cond_arr);
            $list = $command->queryAll();
            if ($list) {
                $is_episode = @$list[0]['is_episode'];
                if ($is_episode == 1) {
                    $cont_id = $_REQUEST['movie_stream_id'];
                } else {
                    $cont_id = $_REQUEST['movie_id'];
                }
                $langcontent = Yii::app()->custom->getTranslatedContent($cont_id, $is_episode, $language_id);
                if ($list[0]['content_types_id'] == 4) {
                    $command1 = Yii::app()->db->createCommand()
                            ->select('m.id as livestream_id,feed_method,feed_type,feed_url,is_recording,delete_no,delete_span')
                            ->from('livestream m ')
                            ->where('movie_id=:movie_id ', array(':movie_id' => $list[0]['id']));
                    $lsdata = $command1->queryAll();
                    if ($lsdata) {
                        $list[0]['livestream_id'] = $lsdata[0]['livestream_id'];
                        $list[0]['feed_method'] = $lsdata[0]['feed_method'];
                        $list[0]['feed_type'] = $lsdata[0]['feed_type'];
                        $list[0]['feed_url'] = $lsdata[0]['feed_url'];
                        $list[0]['is_recording'] = $lsdata[0]['is_recording'];
                        $list[0]['delete_no'] = $lsdata[0]['delete_no'];
                        $list[0]['delete_span'] = $lsdata[0]['delete_span'];
                    }
                }
                $sql = "SELECT * FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
                $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
                $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
                $movie_casts = Yii::app()->custom->getCastCrew($_REQUEST['movie_id'], $studio_id, $language_id);

                $celeb_types = Celebrity::model()->celeb_type();
                $mv_id = $list[0]['id'];
                if (array_key_exists($mv_id, @$langcontent['film'])) {
                    @$list[0]['name'] = @$langcontent['film'][$mv_id]->name;
                }
                $this->headerinfo = 'Edit Content - ' . $list[0]['name'];
                $all_images = ImageManagement::model()->get_imagedetails_by_studio_id(Yii::app()->user->studio_id);
                //Check for Custom Form 
                $customComp = new CustomForms();
                if ($is_episode == 1) {
                    $custom_content_types_id = (@$list[0]['parent_content_type'] == 3) ? 9 : 4;
                } else {
                    $custom_content_types_id = self::getFormType($list[0]['content_types_id']);
                }
                $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($custom_content_types_id);
                $arg['arg']['editid'] = ($list[0]['cmfid'] != 0) ? $list[0]['cmfid'] : $list[0]['custom_metadata_form_id'];
                if ($arg['arg']['editid']) {
                    $list[0]['metadata_form_id'] = $arg['arg']['editid'];
                    $customData = $customComp->getCustomMetadata($studio_id, $arg['parent_content_type_id'], $arg['arg']);
                }
                $formdata = Yii::app()->general->formlist($studio_id);
                $list = self::getSerializeCustomDate($list);
                $IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
                $checkImageKey = StudioConfig::model()->getConfig($studio_id, 'enable_image_key');
                $this->render('//admin/newcontents', array('data' => $list, 'contentList' => @$contentCategories, 'movie_casts' => $movie_casts, 'celeb_types' => $celeb_types, 'allow_new' => 1, 'all_images' => $all_images, 'customData' => $customData, 'langcontent' => $langcontent, 'formdata' => $formdata, 'IsDownloadable' => $IsDownloadable, 'checkImageKey' => $checkImageKey));
            } else {
                Yii::app()->user->setFlash('error', 'Oops! You don\'t have access to the movie');
                $this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! invalid input');
            $url = $this->createUrl('useraccount/usergeneratedcontent');
            $this->redirect($url);
        }
    }

    function getFormType($form_type) {
        $arr = array(1 => 1, 2 => 2, 3 => 3, 4 => 5, 5 => 7, 6 => 8);
        return $arr[$form_type];
    }

    function getSerializeCustomDate($list) {
        if (@$list[0]['custom10']) {
            $x = json_decode($list[0]['custom10'], true);
            foreach ($x as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $list[0][$key1] = $value1;
                }
            }
        }
        if (@$list[0]['cs6']) {
            $y = json_decode($list[0]['cs6'], true);
            foreach ($y as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $k = str_replace('custom', 'cs', $key1);
                    $list[0][$k] = $value1;
                }
            }
        }
        return $list;
    }

    function actionUpdateMovieDetails() {
        $studio_id = $this->studio->id;
        $language_id = $this->language_id;
        //echo "<pre>";print_r($_REQUEST);print_r($_FILES);exit;
        if ($_REQUEST['movie_id'] && $_REQUEST['uniq_id']) {
            $Films = Film::model()->findByAttributes(array('id' => $_REQUEST['movie_id'], 'uniq_id' => $_REQUEST['uniq_id']));
            $MovieStreams = movieStreams::model()->findByAttributes(array('id' => $_REQUEST['movie_stream_id'], 'movie_id' => $_REQUEST['movie_id'], 'is_episode' => 0));
            if ($Films) {
                //Update content publish date if any
                $publish_date = NULL;
                if (@$_REQUEST['content_publish_date']) {
                    if (@$_REQUEST['publish_date']) {
                        $pdate = explode('/', $_REQUEST['publish_date']);
                        $publish_date = $pdate[2] . '-' . $pdate[0] . '-' . $pdate[1];
                        if (@$_REQUEST['publish_time']) {
                            $publish_date .= ' ' . $_REQUEST['publish_time'];
                        }
                    }
                }
                $film_detail = $Films;
                $film_lang = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'language_id' => $language_id, 'parent_id' => $_REQUEST['movie_id']));
                $data = $_REQUEST['movie'];
                $data['custom10'] = self::createSerializeCustomDate($data);
                $movie_id = $_REQUEST['movie_id'];
                if (!empty($film_lang)) {
                    $Films = Film::model()->findByAttributes(array('id' => $film_lang->id, 'uniq_id' => $_REQUEST['uniq_id']));
                }
                if ($Films->language_id == $this->language_id) {
                    if ($Films->parent_id == 0) {
                        $Films->name = stripslashes($data['name']);
                        if (isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
                            $Films->release_date = date('Y-m-d', strtotime($data['release_date']));
                        else
                            $Films->release_date = null;
                        $Films->content_category_value = implode(',', $data['content_category_value']);
                        if (@$data['content_subcategory_value'])
                            $Films->content_subcategory_value = implode(',', $data['content_subcategory_value']);
                        $data['language'] = is_array(@$data['language']) ? array_map('ucwords', $data['language']) : $data['language'];
                        $Films->language = is_array(@$data['language']) ? json_encode(array_values(array_unique($data['language']))) : @$data['language'];
                        $data['genre'] = is_array(@$data['genre']) ? array_map('ucwords', $data['genre']) : $data['genre'];
                        $Films->genre = is_array(@$data['genre']) ? json_encode(array_values(array_unique($data['genre'])), JSON_UNESCAPED_UNICODE) : @$data['genre'];
                        $data['censer_rating'] = is_array(@$data['censer_rating']) ? array_map('ucwords', $data['censer_rating']) : $data['censer_rating'];
                        $Films->censor_rating = is_array(@$data['censer_rating']) ? json_encode(array_values(array_unique($data['censer_rating']))) : @$data['censer_rating'];
                        $Films->story = stripslashes($data['story']);
                        $Films->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                        $Films->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                        $Films->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                        $Films->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                        $Films->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                        $Films->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                        $Films->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                        $Films->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                        $Films->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                        $Films->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                        $Films->save();
                        $filmChildData = new Film;
                        $attr = array('content_category_value' => implode(',', $data['content_category_value']));
                        $condition = "parent_id =:id";
                        $params = array(':id' => $movie_id);
                        $filmChildData = $filmChildData->updateAll($attr, $condition, $params);
                    }else {
                        $Films->name = stripslashes($data['name']);
                        $Films->language = json_encode($data['language']);
                        $Films->genre = json_encode(array_values(array_unique($data['genre'])));
                        $Films->censor_rating = json_encode($data['censer_rating']);
                        $Films->story = stripslashes($data['story']);
                        $Films->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                        $Films->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                        $Films->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                        $Films->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                        $Films->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                        $Films->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                        $Films->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                        $Films->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                        $Films->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                        $Films->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                        $Films->save();
                    }

                    if (HOST_IP != '127.0.0.1') {
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id . " AND content_id:" . $movie_id . " AND stream_id:" . $MovieStreams->id);
                        $solrArr = array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $MovieStreams->id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $data['name'];
                        $solrArr['permalink'] = $Films->permalink;
                        $solrArr['studio_id'] = Yii::app()->user->studio_id;
                        $solrArr['display_name'] = 'content';
                        $solrArr['content_permalink'] = $Films->permalink;
                        $solrArr['genre_val'] = (count($data['genre']) > 1) ? implode(',', $data['genre']) : $data['genre'][0];
                        $solrArr['product_format'] = '';
                        if ($publish_date != NULL)
                            $solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
                        $ret = $solrobjnew->addSolrData($solrArr);
                    }
                }else {
                    $Film = new Film;
                    $Film->name = stripslashes($data['name']);
                    $Film->content_category_value = $Films->content_category_value;
                    $Film->content_types_id = $Films->content_types_id;
                    $Film->uniq_id = $Films->uniq_id;
                    $Film->language = json_encode($data['language']);
                    $Film->genre = json_encode(array_values(array_unique($data['genre'])));
                    $Film->censor_rating = json_encode($data['censer_rating']);
                    $Film->story = stripslashes($data['story']);
                    $Film->studio_id = $studio_id;
                    $Film->parent_id = $Films->id;
                    $Film->search_parent_id = $Films->id;
                    $Film->permalink = $Films->permalink;
                    $Film->language_id = $this->language_id;
                    $Film->custom1 = @$data['custom1'] ? (is_array(@$data['custom1']) ? json_encode(@$data['custom1']) : @$data['custom1']) : '';
                    $Film->custom2 = @$data['custom2'] ? (is_array(@$data['custom2']) ? json_encode(@$data['custom2']) : @$data['custom2']) : '';
                    $Film->custom3 = @$data['custom3'] ? (is_array(@$data['custom3']) ? json_encode(@$data['custom3']) : @$data['custom3']) : '';
                    $Film->custom4 = @$data['custom4'] ? (is_array(@$data['custom4']) ? json_encode(@$data['custom4']) : @$data['custom4']) : '';
                    $Film->custom5 = @$data['custom5'] ? (is_array(@$data['custom5']) ? json_encode(@$data['custom5']) : @$data['custom5']) : '';
                    $Film->custom6 = @$data['custom6'] ? (is_array(@$data['custom6']) ? json_encode(@$data['custom6']) : @$data['custom6']) : '';
                    $Film->custom7 = @$data['custom7'] ? (is_array(@$data['custom7']) ? json_encode(@$data['custom7']) : @$data['custom7']) : '';
                    $Film->custom8 = @$data['custom8'] ? (is_array(@$data['custom8']) ? json_encode(@$data['custom8']) : @$data['custom8']) : '';
                    $Film->custom9 = @$data['custom9'] ? (is_array(@$data['custom9']) ? json_encode(@$data['custom9']) : @$data['custom9']) : '';
                    $Film->custom10 = @$data['custom10'] ? (is_array(@$data['custom10']) ? json_encode(@$data['custom10']) : @$data['custom10']) : '';
                    $Film->save();
                    if (HOST_IP != '127.0.0.1') {
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery("cat:content AND sku:" . $studio_id . " AND content_id:" . $movie_id . " AND stream_id:" . $MovieStreams->id);
                        $solrArr = array();
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $movie_id;
                        $solrArr['stream_id'] = $MovieStreams->id;
                        $solrArr['stream_uniq_id'] = $MovieStreams->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $data['name'];
                        $solrArr['permalink'] = $Films->permalink;
                        $solrArr['studio_id'] = Yii::app()->user->studio_id;
                        $solrArr['display_name'] = 'content';
                        $solrArr['content_permalink'] = $Films->permalink;
                        $solrArr['genre_val'] = (count($data['genre']) > 1) ? implode(',', $data['genre']) : $data['genre'][0];
                        $solrArr['product_format'] = '';
                        if ($publish_date != NULL)
                            $solrArr['publish_date'] = $publish_date;
                        $solrobjnew = new SolrFunctions();
                        $ret = $solrObjnew->addSolrData($solrArr);
                    }
                    Yii::app()->user->setFlash('success', 'Wow! Your content updated successfully');
                    $this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
                    exit;
                }

                $MovieStreams->is_downloadable = (@$_REQUEST['download'] && @$_REQUEST['stream']) ? 2 : ((@$_REQUEST['download']) ? 1 : 0);
                ;

                $MovieStreams->content_publish_date = $publish_date;
                $MovieStreams->save();

                //Check and Save new Tags into database
                $movieTags = new MovieTag();
                $movieTags->addTags($data);
                $movie_id = $Films->id;
                if (isset($_REQUEST['content_filter_type'])) {
                    $contentFilterCls = new ContentFilter();
                    $addContentFilter = $contentFilterCls->addContentFilter($_REQUEST, $Films->content_type_id);
                }
                $this->processContentImage($data['content_types_id'], $movie_id, $_REQUEST['movie_stream_id']);
                $checkfireappletv = Yii::app()->general->CheckApp($studio_id);
                if ($checkfireappletv) {
                    $this->processAppTVImage($data['content_types_id'], $movie_id, $_REQUEST['movie_stream_id']);
                }

                if (isset($_FILES['topbanner']) && !($_FILES['topbanner']['error'])) {
                    $this->uploadPoster($_FILES['topbanner'], $movie_id, 'topbanner');
                }


                Yii::app()->user->setFlash('success', 'Wow! Your content updated successfully');
                $this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
                exit;
            } else {
                Yii::app()->user->setFlash('error', 'Oops! Sorry error in updating your data');
                $this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
                exit;
            }
        } else {
            Yii::app()->user->setFlash('error', 'Oops! Sorry you are not authorised to access this data');
            $this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
            exit;
        }
    }

    function createSerializeCustomDate($data, $flag = 9) {
        $cus = '';
        foreach ($data as $key => $value) {
            $k = (int) str_replace('custom', '', $key);
            if ($k > $flag) {
                $cus[] = array($key => $value);
            }
        }
        return $cus;
    }

    function processContentImage($content_types_id = 1, $movie_id = '', $stream_id = '', $is_episode = '') {
        $studio_id = Yii::app()->common->getStudiosId();
        if ($movie_id) {
            $cmsql = "SELECT poster_size FROM custom_metadata_form WHERE id = (SELECT custom_metadata_form_id FROM films WHERE id=" . $movie_id . ")";
            $cmfid = Yii::app()->db->createCommand($cmsql)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
        }
        if (isset($cmfid) && !empty($cmfid) && $cmfid->poster_size != '') {
            $vertical = strtolower($cmfid->poster_size);
            $horizontal = strtolower($cmfid->poster_size);
        } else {
            $poster_sizes = Yii::app()->general->getPosterSize($studio_id);
            $vertical = strtolower($poster_sizes['v_poster_dimension']);
            $horizontal = strtolower($poster_sizes['h_poster_dimension']);
        }

        $poster_object_type = 'films';
        $object_id = $movie_id;
        if ($is_episode) {
            $poster_object_type = 'moviestream';
            $object_id = $stream_id;
        }
        $thumb_size = $vertical;
        $standard_size = $vertical;

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
        if ($content_types_id == 2 || $content_types_id == 4 || $is_episode) {
            $cropDimension['episode'] = $horizontal;
        }

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
        if (isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])) {
            $cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata'], $cdimension);

            $path = Yii::app()->common->jcropImage($_FILES['Filedata'], $dir, $_REQUEST['fileimage']);
            $ret = $this->uploadPoster($_FILES['Filedata'], $object_id, $poster_object_type, $cropDimension, $path);
            if ($movie_id) {
                Yii::app()->common->rrmdir($dir);
            }
            if ($content_types_id == 2 || $is_episode) {
                movieStreams::model()->updateByPk($stream_id, array('is_poster' => 1));
            }
            return $ret;
        } else if ($_FILES['Filedata']['name'] == '' && $_REQUEST['g_image_file_name'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name']);
            $_REQUEST['g_image_file_name'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage'];
            $image_name = $_REQUEST['g_image_file_name'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name'], $_REQUEST['g_original_image'], $dir, $dimension);
            $fileinfo['name'] = $_REQUEST['g_image_file_name'];
            $fileinfo['error'] = 0;
            $ret = $this->uploadPoster($fileinfo, $object_id, $poster_object_type, $cropDimension, $path);
            Yii::app()->common->rrmdir($dir);
            if ($content_types_id == 2 || $is_episode) {
                movieStreams::model()->updateByPk($stream_id, array('is_poster' => 1));
            }
            return $ret;
        } else {
            return false;
        }
    }

    function processAppTVImage($content_types_id = 1, $movie_id = '', $stream_id = '', $is_episode = '') {
        $studio_id = Yii::app()->common->getStudiosId();
        $vertical = '800x600';
        $horizontal = '800x600';

        $poster_object_type = 'tvapp';
        $object_id = $movie_id;
        if ($is_episode) {
            $object_id = $stream_id;
        }
        $thumb_size = $vertical;
        $standard_size = $vertical;

        $cropDimension = array('thumb' => $thumb_size, 'standard' => $standard_size);
        if ($content_types_id == 2 || $content_types_id == 4 || $is_episode) {
            $cropDimension['episode'] = $horizontal;
        }

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . '/images/public/system/posters/' . $movie_id;
        if (isset($_FILES['Filedata_fire']) && !($_FILES['Filedata_fire']['error'])) {
            $cdimension = array('thumb' => "64x64");
            $ret1 = $this->uploadToImageGallery($_FILES['Filedata_fire'], $cdimension);

            $path = Yii::app()->common->jcropImage($_FILES['Filedata_fire'], $dir, $_REQUEST['fileimage_fire']);
            $ret = $this->uploadPoster($_FILES['Filedata_fire'], $object_id, $poster_object_type, $cropDimension, $path);
            if ($movie_id) {
                Yii::app()->common->rrmdir($dir);
            }
            return $ret;
        } else if ($_FILES['Filedata_fire']['name'] == '' && $_REQUEST['g_image_file_name_fire'] != '') {

            $file_info = pathinfo($_REQUEST['g_image_file_name_fire']);
            $_REQUEST['g_image_file_name_fire'] = $file_info['filename'] . "_" . strtotime(date("d-m-Y H:i:s")) . '.' . $file_info['extension'];
            $jcrop_allimage = $_REQUEST['jcrop_allimage_fire'];
            $image_name = $_REQUEST['g_image_file_name_fire'];

            $dimension['x1'] = $jcrop_allimage['x13'];
            $dimension['y1'] = $jcrop_allimage['y13'];
            $dimension['x2'] = $jcrop_allimage['x23'];
            $dimension['y2'] = $jcrop_allimage['y23'];
            $dimension['w'] = $jcrop_allimage['w3'];
            $dimension['h'] = $jcrop_allimage['h3'];

            $path = Yii::app()->common->jcroplibraryImage($_REQUEST['g_image_file_name_fire'], $_REQUEST['g_original_image_fire'], $dir, $dimension);
            $fileinfo['name'] = $_REQUEST['g_image_file_name_fire'];
            $fileinfo['error'] = 0;
            $ret = $this->uploadPoster($fileinfo, $object_id, $poster_object_type, $cropDimension, $path);
            Yii::app()->common->rrmdir($dir);
            return $ret;
        } else {
            return false;
        }
    }

    public function actionupdatereview() {
        $res = array();
        if (!empty($_POST['movie_stream_id'])) {
            $id = $_POST['movie_stream_id'];
            $getMovieStream = movieStreams::model()->with(array('film' => array('select' => 't0.name,t0.permalink, t0.genre')))->find(array('select' => '*', 'condition' => 't.id=:id', 'params' => array(':id' => $id)));
            if (!$getMovieStream) {
                $res['error'] = 1;
                $res['message'] = "Content not found.";
            } else {
                $getMovieStream->review_flag = 0;
                $getMovieStream->save();
                //movieStreams::model()->updateByPk($id, array('review_flag' => 1));
                /* aprove notification here */
                $getSdkUser = SdkUser::model()->find(array('select' => 'email, display_name', 'condition' => 'id=:sdk_user_id', 'params' => array(':sdk_user_id' => $getMovieStream->sdk_user_id)));
                
                /* insert to solr */
                if (HOST_IP != '127.0.0.1' && $getMovieStream->review_flag == 0) {
                    $solrObj = new SolrFunctions();
                    $decodeGenre = json_decode($getMovieStream->film->genre, true);
                    if ($decodeGenre) {
                        foreach ($decodeGenre as $key => $val) {
                            $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                            $solrArr['content_id'] = $getMovieStream->film->id;
                            $solrArr['stream_id'] = $getMovieStream->id;
                            $solrArr['stream_uniq_id'] = $getMovieStream->embed_id;
                            $solrArr['is_episode'] = $getMovieStream->is_episode;
                            $solrArr['name'] = $getMovieStream->film->name;
                            $solrArr['permalink'] = $getMovieStream->film->permalink;
                            $solrArr['studio_id'] = Yii::app()->user->studio_id;
                            $solrArr['display_name'] = 'content';
                            $solrArr['content_permalink'] = $getMovieStream->film->permalink;
                            $solrArr['genre_val'] = $val;
                            $solrArr['product_format'] = '';
                            $ret = $solrObj->addSolrData($solrArr);
                        }
                    } else {
                        $solrArr['uniq_id'] = Yii::app()->common->generateUniqNumber();
                        $solrArr['content_id'] = $getMovieStream->film->id;
                        $solrArr['stream_id'] = $getMovieStream->id;
                        $solrArr['stream_uniq_id'] = $getMovieStream->embed_id;
                        $solrArr['is_episode'] = $MovieStreams->is_episode;
                        $solrArr['name'] = $getMovieStream->film->name;
                        $solrArr['permalink'] = $getMovieStream->film->permalink;
                        $solrArr['studio_id'] = Yii::app()->user->studio_id;
                        $solrArr['display_name'] = 'content';
                        $solrArr['content_permalink'] = $getMovieStream->film->permalink;
                        $solrArr['genre_val'] = '';
                        $solrArr['product_format'] = '';
                        $ret = $solrObj->addSolrData($solrArr);
                    }
                }
                /* insert to solr */
                
                if ($getSdkUser) {
                    $studio = $this->studio;
                    $to = $getSdkUser->email;
                    $to_name = $getSdkUser->display_name;
                    $from = !empty($studio->contact_us_email) ? $studio->contact_us_email : Yii::app()->user->email;
                    $from_name = "Muvi";

                    $logo = EMAIL_LOGO;
                    $logo = '<a href="' . $studio->domain . '"><img src="' . $logo . '" alt="studio" /></a>';
                    $movie_url = $studio->domain . '/' . $getMovieStream->film->permalink;
                    $params = array(
                        'studio_name' => $studio->name,
                        'logo' => $logo,
                        'type' => 'approve_content',
                        'name' => $to_name,
                        'content_name' => $getMovieStream->film->name,
                        'movie_url' => $movie_url
                    );

                    $subject = 'Your content has been approved';
                    Yii::app()->theme = 'bootstrap';
                    $thtml = Yii::app()->controller->renderPartial('//email/ugc_email', array('params' => $params), true);
                    $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $from_name);
                }
                /* aprove notification here */
                $res['error'] = 0;
                $res['message'] = "Content approved successfully.";
            }
        } else {
            $res['error'] = 1;
            $res['message'] = "Requested data not found.";
        }
        echo json_encode($res);
        exit;
        //$this->redirect($this->createUrl('useraccount/usergeneratedcontent'));
    }

    public function actionremoveUGC() {
        if (isset($_POST) && !empty($_POST['movie_id'])) {
            $res = array();
            $movie_id = $_POST['movie_id'];
            $getMovieStream = movieStreams::model()->findAll('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
            $getFilm = Film::model()->find("id=$movie_id");
            if ($movie_id && $getMovieStream) {
                $s3 = Yii::app()->common->connectToAwsS3(Yii::app()->user->studio_id);
                $bucketInfo = Yii::app()->common->getBucketInfo("", Yii::app()->user->studio_id);
                $bucket = $bucketInfo['bucket_name'];
                $sub_dir = '';
                foreach ($getMovieStream AS $key => $val) {
                    $streams = $val->attributes;
                    $folderPath = Yii::app()->common->getFolderPath(Yii::app()->user->new_cdn_users);
                    $signedBucketPath = $folderPath['signedFolderPath'];
                    $s3dir = $signedBucketPath . 'uploads/movie_stream/full_movie/' . $streams['id'] . "/";
                    if ($streams['full_movie']) {
                        $response = $s3->getListObjectsIterator(array(
                            'Bucket' => $bucket,
                            'Prefix' => $s3dir
                        ));
                        foreach ($response as $object) {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $object['Key']
                            ));
                        }
                    }
                    if ($streams['is_episode'] != 1) {
                        //Remove Trailer
                        $this->removeTrailer($movie_id);
                        //Remove Poster 
                        $this->removePosters($movie_id, 'films');
                        //Remove Top Banner
                        $this->removePosters($movie_id, 'topbanner');
                        //Remove Data from films table 
                        Film::model()->deleteByPk($movie_id);
                        //Remove All Child contents
                        $params = array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id);
                        Film::model()->deleteAll('parent_id=:movie_id AND studio_id=:studio_id', $params);
                        //Remove Featured Content
                        UserFavouriteList::model()->deleteAll('content_id=:movie_id AND studio_id=:studio_id AND content_type="0"', $params);
                        FeaturedContent::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
                    } else {
                        //Remove Poster
                        $this->removePosters($streams['id'], 'moviestream');
                    }
                    $this->removeTvGuideEvent($streams['id']);

                    movieStreams::model()->deleteByPk($streams['id']);
                    Encoding::model()->deleteAll('movie_stream_id =:movie_stream_id AND studio_id =:studio_id', array(':movie_stream_id' => $streams['id'], ':studio_id' => Yii::app()->user->studio_id));
                    if (HOST_IP != '127.0.0.1') {
                        $solrobj = new SolrFunctions();
                        $solrobj->deleteSolrQuery('content_id:' . $movie_id . " AND stream_id:" . $streams['id']);
                        /* $solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );
                          $solr->deleteByQuery('content_id:'.$movie_id." AND stream_id:".$streams['id']);
                          $solr->commit(); //commit to see the deletes and the document
                          $solr->optimize(); */
                    }
                }

                //Delete live stream content
                $liveStreamData = Livestream::model()->findbyAttributes(array('studio_id' => Yii::app()->user->studio_id, 'movie_id' => $movie_id));
                if ($liveStreamData) {
                    if (@$liveStreamData->feed_method == 'push' && @$liveStreamData->stream_key != '') {
                        $streamName = explode("?", @$liveStreamData->stream_key);
                        $streamUrlExplode  = explode("/",str_replace("rtmp://","",$liveStreamData->stream_url));
                        if (@$streamName[0] != '' && @$streamUrlExplode[0]) {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, 'http://' . $streamUrlExplode[0] . '/auth.php');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, "submit=delete&stream_name=" . @$streamName[0]);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $server_output = trim(curl_exec($ch));
                            curl_close($ch);
                        }
                    }
                    Livestream::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id', array(':movie_id' => $movie_id, ':studio_id' => Yii::app()->user->studio_id));
                }
                //Yii::app()->user->setFlash('success', "Content removed successfully.");
                //$this->redirect($_SERVER['HTTP_REFERER']);
                /* delee notification here */
                $getSdkUser = SdkUser::model()->find(array('select' => 'email, display_name', 'condition' => 'id=:sdk_user_id', 'params' => array(':sdk_user_id' => $getMovieStream[0]->sdk_user_id)));
                if ($getSdkUser) {
                    $studio = $this->studio;
                    $to = $getSdkUser->email;
                    $to_name = $getSdkUser->display_name;
                    $from = !empty($studio->contact_us_email) ? $studio->contact_us_email : Yii::app()->user->email;
                    $from_name = "Muvi";
                    //$logo = EMAIL_LOGO;
                    $logo = '<a href="' . $studio->domain . '"><img src="' . $logo . '" alt="studio" /></a>';
                    $ugc_listing_url = $studio->domain . '/ugc/Showdetails';
                    $params = array(
                        'studio_name' => $studio->name,
                        'logo' => $logo,
                        'type' => 'delete_content',
                        'name' => $to_name,
                        'content_name' => $getFilm->name,
                        'ugc_listing_url' => $ugc_listing_url
                    );

                    $subject = 'Your content deleted';
                    Yii::app()->theme = 'bootstrap';
                    $thtml = Yii::app()->controller->renderPartial('//email/ugc_email', array('params' => $params), true);
                    $this->sendmailViaAmazonsdk($thtml, $subject, $to, $from, '', '', '', $from_name);
                }
                /* delee notification here */
                $res['error'] = 0;
                $res['message'] = 'Content removed successfully.';
            } else {
                $res['error'] = 1;
                $res['message'] = 'Oops! Sorry error in deleting content.';
            }
            echo json_encode($res);
        }
        exit;
    }

}

?>