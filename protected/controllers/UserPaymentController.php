<?php

class UserPaymentController extends Controller {
    
    public function init() {
        require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/pagination.php';
        if (isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == true) {
            $this->layout = false;
        }
        parent::init();
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionTest() {
        echo 'hi';
    }

    function isAjaxRequest() {
        if (!Yii::app()->request->isAjaxRequest) {
            $url = Yii::app()->createAbsoluteUrl();
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: ' . $url);
            exit();
        }
    }
    function setRedirectPpv($Films, $plan, $data, $isadv = 0, $isinstafeez = 0) {
        if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
            if (trim($data['episode_id']) && trim($data['episode_id']) != '0') {
                $str = '&stream=' . $data['episode_uniq_id'];
                $playerParam = '/stream/' . $data['episode_uniq_id'];
            }

            if (intval($data['season_id'])) {
                $str = '&season=' . $data['season_id'];
                $playerParam .='/season/' . $data['season_id'];
            }
        }
        if (intval($isadv)) {
            $movie_name = $Films->name;
            $msgs = $this->ServerMessage['purchased'];
            $msgs = htmlentities($msgs);
            eval("\$msgs = \"$msgs\";");
            $msgs = htmlspecialchars_decode($msgs);
            Yii::app()->user->setFlash("success", $msgs);
            if ($isinstafeez) {
                return $_SERVER['HTTP_REFERER'];
            } else {
                if (isset($data['permalink'])) {
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/' . $data['permalink']);
                    exit;
                } else if (isset($_SERVER['HTTP_REFERER']) && trim($_SERVER['HTTP_REFERER'])) {
                    $this->redirect($_SERVER['HTTP_REFERER']);
                    exit;
                } else {
                    $this->redirect(Yii::app()->session['backbtnlink']);
                    exit;
                }
            }
        } else {
            if ($isinstafeez) {
                return Yii::app()->getbaseUrl(true) . '/'.$this->Player_Page.'/' . $data['permalink'] . $playerParam;
                exit;
            } else {
                $this->redirect(Yii::app()->getbaseUrl(true) . '/'.$this->Player_Page.'/' . $data['permalink'] . $playerParam);
            }
        }
    }

    /* Functions related IPPaymets START */
    
    public function actionGenerateSST() {
        $user = SdkUser::model()->findByPk(Yii::app()->user->id);
        $data = $_POST;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = $data['session_url'];
        Yii::app()->session['backbtnlink'] = $path;

        $gateway_code = $_POST['payment_method'];
        $data['studio_id'] = $studio_id = Yii::app()->common->getStudiosId();
        $data['user_id'] = $user_id = Yii::app()->user->id;
        $plan_id = $data['plan_id'];
        $is_bundle = @$data['is_bundle'];
        $timeframe_id = @$data['timeframe_id'];
        if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
            $isadv = $data['is_advance_purchase'];
        } else {
            $isadv = $data['isadv'];
        }

        $timeframe_days = 0;
        $end_date = '0000-00-00 00:00:00';
        $data['coupon'] = $_POST['coupon'];
        $plan = PpvPlans::model()->findByPk($plan_id);

        $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        if (intval($is_bundle) && intval($timeframe_id)) {
            (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
            $price = $price->attributes;
            $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
        } else {
            $price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id']);
        }
        $currency = Currency::model()->findByPk($price['currency_id']);
        $data['currency_id'] = $currency->id;
        $data['currency_code'] = $currency->code;
        $data['currency_symbol'] = $currency->symbol;

        //Calculate ppv expiry time only for single part or episode only
        $start_date = Date('Y-m-d H:i:s');
        $data['start_date'] = $start_date;
        if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
            $time = strtotime($start_date);
            $expiry = $timeframe_days . ' ' . "days";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
        } else {
            if (intval($isadv) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }
        }
        if ($is_bundle) {//For PPV bundle
            $data['season_id'] = 0;
            $data['episode_id'] = 0;

            if (intval($is_subscribed_user)) {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                }
            } else {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_unsubscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        } else {
            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $data['episode_uniq_id'] = $data['episode_id'];
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;
                        $data['episode_uniq_id'] = $streams->embed_id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['season_unsubscribed'];
                        }

                        if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status']) && intval($isadv) == 0) {
                            $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                            $season_price_data['ppv_pricing_id'] = $price['id'];
                            $season_price_data['studio_id'] = $studio_id;
                            $season_price_data['currency_id'] = $price['currency_id'];
                            $season_price_data['season'] = $data['season_id'];
                            $season_price_data['is_subscribed'] = $is_subscribed_user;

                            $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                            if ($season_price != -1) {
                                $data['amount'] = $amount = $season_price;
                            }
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['show_unsubscribed'];
                        }
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        }
        $data['end_date'] = $end_date;
        $couponCode = '';
        //Calculate coupon if exists
        if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
            $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
            $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $couponCode = $getCoup["couponCode"];
        }

        $name = explode(' ', $user->display_name);
        $data['session_id'] = Yii::app()->common->generateUniqNumber();
        $data['session_key'] = Yii::app()->common->generateUniqNumber();
        $data['customer_ref'] = Yii::app()->common->generateUniqNumber();
        $data['ServerURL'] = base64_encode(Yii::app()->getbaseUrl(true).'/userPayment/ServerURLIPPayment'); 
        $data['UserURL'] = base64_encode(Yii::app()->getbaseUrl(true).'/userPayment/UserURLIPPayment');
        $data['email'] = Yii::app()->user->email;
        $data['first_name'] = isset($name[0]) && trim($name[0]) ? $name[0] : '';
        $data['last_name'] = isset($name[1]) && trim($name[1]) ? $name[1] : '';
        $data['currency_code'] = $currency->code;
        $data['currency_id'] = $currency->id;
        $data['shipping_cost'] = isset($_REQUEST['shipping_cost']) && $_REQUEST['shipping_cost'] ? $_REQUEST['shipping_cost'] : 0;
        $data['shipping'] = 0;
        $data['return'] = Yii::app()->getBaseUrl(true) . "/userPayment/returnHostedData";
        
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $data['ipp_info'] = $payment_gateway::generateSST($data);
        $log = new PciLog();
        $log->unique_id = $data['session_id'];
        $log->log_data = json_encode($data);
        $log->save();
        Yii::app()->session['ppv_data'] = $data;
        echo json_encode($data);
    }
    
    public function actionServerURLIPPayment(){
		$logfile = dirname(__FILE__) . '/ipp.txt';
        $msg = "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
        $data_log = serialize($_REQUEST);
        $msg.= $data_log . "\n";
        $msg = "\n----------array --\n";
        $msg.= print_r($_REQUEST, true) . "\n";
        $msg.= $_REQUEST . "\n";
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
		
        $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['SessionId']));
        //$log_data = PciLog::model()->findByAttributes(array('unique_id' => '63422db34de83a1bb7c4bfb2af34f47c'));
        if(!empty($_REQUEST) && $_REQUEST['Result'] == 1){
        //if(1){
                $data = json_decode($log_data['log_data'], true);
                $studio_id = $data['studio_id'];
                $user_id = $data['user_id'];
                $gateway_code = $data["payment_method"];
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
				
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);
                $isadv = @$data['isadv'];
                $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
                $video_id = $Films->id;
                $VideoName = Yii::app()->common->getVideoname($video_id);
                $VideoName = trim($VideoName);
                $VideoDetails = $VideoName;
                $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                    if ($data['season_id'] == 0) {
                        $VideoDetails .= ', All Seasons';
                    } else {
                        $VideoDetails .= ', Season ' . $data['season_id'];
                        if ($data['episode_id'] == 0) {
                            $VideoDetails .= ', All Episodes';
                        } else {
                            $episodeName = Yii::app()->common->getEpisodename($streams->id);
                            $episodeName = trim($episodeName);
                            $VideoDetails .= ', ' . $episodeName;
                        }
                    }
                }
                $start_date = $data['start_date'];
                $end_date = $data['end_date'];
                //Save ppv subscription detail
                $data['card_name'] = @$_REQUEST['CardHolderName'];
                $data['card_type'] = @$_REQUEST['CardType'];
                $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code);
                $ppv_subscription_id = $set_ppv_subscription;
                $trans_data = array();
                $trans_data['ResponseCode'] = 1;
                $trans_data['is_success'] = 1;
                $trans_data['transaction_status'] = 'Success';
                $trans_data['invoice_id'] = Yii::app()->common->generateUniqNumber();
                $trans_data['order_number'] = @$_REQUEST['Receipt'];
                $trans_data['amount'] = $data['amount'];
                $trans_data['paid_amount'] = $data['amount'];
                $trans_data['dollar_amount'] = $data['amount'];
                $trans_data['currency_code'] = $data['currency_code'];
                $trans_data['currency_symbol'] = $data['currency_symbol'];
                $trans_data['response_text'] = '';
				
                $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
				
        }else {
                $request = json_encode($_REQUEST);
                PciLog::model()->updateErrorLogIPPayment($log_data['id'],$request);
                exit;
            }
    }
    
    public function actionUserURLIPPayment(){
        $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['SessionId']));
        if(!empty($_REQUEST) && $_REQUEST['SST'] != ""){
                $data = json_decode($log_data['log_data'], true);
                if(isset($data['DeclinedCode']) && trim($data['DeclinedCode']) != ""){
                    Yii::app()->user->setFlash("error", $data['DeclinedMessage']);
                    echo '<script type="text/javascript">';
                    echo 'window.top.location.href = "'.Yii::app()->session['backbtnlink'].'";';
                    echo '</script>';
                    exit;
                }else{
                $studio_id = $data['studio_id'];
                $user_id = $data['user_id'];
                $gateway_code = $data["payment_method"];
                $plan_id = $data['plan_id'];
                $ip_address = CHttpRequest::getUserHostAddress();
                $planModel = new PpvPlans();
                $plan = $planModel->findByPk($plan_id);
                $isadv = @$data['isadv'];
                $isippayment = 1;
                $Films = Film::model()->findByAttributes(array('studio_id' => $data['studio_id'], 'uniq_id' => $data['movie_id']));
                
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    if(trim($data['episode_id']) && trim($data['episode_id']) != '0') {
                        $str = '&stream='.$data['episode_uniq_id'];
                        
                        $embedId = $data['episode_uniq_id'];
                        $episodeTitle = '';
                        $time = time() . uniqid();
                        $_SESSION['showconfirmuniqueid'] = $time;
                        $playerParam = '?unique_id=' . $time . '&season_id=' . $data['season_id'] . '&episode_id=' . $data['episode_id']. '&embed_id=' . $embedId . '&episode=' . $episodeTitle;
                        //$playerParam = '/stream/'.$data['episode_uniq_id'];                                    
                    }

                    if (intval($data['season_id']) && $data['episode_id']==0) {
                        $str = '&season='.$data['season_id'];
                        $time = time() . uniqid();
                        $_SESSION['showconfirmuniqueid'] = $time;
                        $playerParam = '?unique_id=' . $time . '&season_id=' . $data['season_id'];
                        //$playerParam .='/season/'. $data['season_id'];
                    }
                }else{
                    $timeunique = time() . uniqid() . uniqid();
                    $_SESSION['showconfirmuniqueid'] = $timeunique;
                    $playerParam = '?unique_id=' . $timeunique;
                }
                    //PciLog::model()->deleteByPk($log_data['id']);
                echo '<script>';
                echo 'window.top.location.href = "'.Yii::app()->getbaseUrl(true) .'/' . $Films->permalink . $playerParam.'";';
                echo '</script>';
                PciLog::model()->deleteByPk($log_data['id']);
                exit;
                    
                }
        } else {
                Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                echo '<script type="text/javascript">';
                echo 'window.top.location.href = "'.Yii::app()->session['backbtnlink'].'";';
                echo '</script>';
                exit;
            }
    }
    
    public function actionServerURLIPPaymentSubscription(){
            //SAVE RESPONSE TO CHECK
            $logfile = dirname(__FILE__) . '/ipp.txt';
            $msg = "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
            $data_log = serialize($_REQUEST);
            $msg.= $data_log . "\n";
            $msg = "\n----------array --\n";
            $msg.= print_r($_REQUEST, true) . "\n";
            $msg.= $_REQUEST . "\n";
            file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
            
            $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['SessionId']));
            if (isset($_REQUEST['Token']) && trim($_REQUEST['Token']) != "" && intval($_REQUEST['Result'])==1){
                $data = json_decode($log_data['log_data'], true);
                $studio_id = $data['studio_id'];
                $user_id = $data['user_id'];
                $plan_id = $data['plan_id'];
                $currency_id = $data['currency_id'];
                $plan_price = $data['plan_price'];
                $discount_price = $data['discount_price'];
                $couponCode = $data['couponCode'];
                $use_discount = $data['use_discount'];
                $start_date = $data['start_date'];
                $end_date = $data['end_date'];
                
                    $planModel = new SubscriptionPlans();
                    $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
                    Yii::app()->billing->addCreditToUser($plan,$studio_id,$user_id);
                    $is_renewal = 1;
                    
                
                
		$number = $_REQUEST['ExpiryDate'];
                $arr = explode('/', $number);
				
                $sciModel = New SdkCardInfos;
                $sciModel->studio_id = $studio_id;
                $sciModel->user_id = $user_id;
                $sciModel->gateway_id = $data['gateway_id'];
                $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
                $sciModel->card_holder_name = $_REQUEST['CardHolderName'];
                $sciModel->exp_month = $arr[0];
                $sciModel->exp_year = 2000+$arr[1];
                $sciModel->profile_id = $_REQUEST['Receipt'];
                $sciModel->token = $_REQUEST['Token'];
                $sciModel->response_text = serialize($_REQUEST);
                $sciModel->card_type = $_REQUEST['CardType'];
                $sciModel->reference_no = $_REQUEST['CustRef'];
                $sciModel->status = $_REQUEST['Result'];
                $sciModel->ip = $data['ip'];
                $sciModel->created = new CDbExpression("NOW()");

                $sciModel->save();
                $card_id = $sciModel->id;
                $arr['card_id'] = $sciModel->id;
            
                $payment_gateway_id = $this->PAYMENT_GATEWAY_ID['ippayment'];
                $ip_address = CHttpRequest::getUserHostAddress();
                
                if (intval($plan_id) && intval($payment_gateway_id)) {
                        //Create user subscription for making transaction
                        $usModel = new UserSubscription;
                        $usModel->studio_id = $studio_id;
                        $usModel->user_id = $user_id;
                        $usModel->plan_id = $plan_id;
                        $usModel->card_id = $card_id;
                        $usModel->studio_payment_gateway_id = $payment_gateway_id;
                        $usModel->currency_id = $currency_id;
                        $usModel->profile_id = $_REQUEST['Receipt'];
                        $usModel->amount = $plan_price;
                        $usModel->discount_amount = @$discount_price;
                        $usModel->coupon_code = @$couponCode;
                        $usModel->use_discount = $use_discount;
                        $usModel->start_date = $start_date;
                        $usModel->end_date = $end_date;
                        $usModel->status = 1;
                        $usModel->is_renewal = $is_renewal;
                        $usModel->ip = $ip_address;
                        $usModel->created_by = $user_id;
                        $usModel->created_date = new CDbExpression("NOW()");
                        $usModel->save();

                        $user_subscription_id = $usModel->id;

                        $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
                        if (isset($couponDetails) && !empty($couponDetails)) {
                            $command = Yii::app()->db->createCommand();
                            if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                                $qry = $command->update('coupon_subscription', array('used_by' => $couponDetails->used_by . "," . $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                            } else {
                                $qry = $command->update('coupon_subscription', array('used_by' => $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                            }
                        }
                    }
                    
                    //Save transaction data
                    if ($data['trail_period'] == 0){
                        $transaction = new Transaction;
                        $transaction->user_id = $user_id;
                        $transaction->studio_id = $studio_id;
                        $transaction->plan_id = $plan_id;
                        $transaction->currency_id = $currency_id;
                        $transaction->transaction_date = new CDbExpression("NOW()");
                        $transaction->payment_method = 'ippayment';
                        $transaction->transaction_status = @$_REQUEST['Result'];
                        $transaction->invoice_id = Yii::app()->common->generateUniqNumber();
                        $transaction->order_number = $_REQUEST['Receipt'];
                        $transaction->dollar_amount = '';
                        $transaction->amount = $data['amount'];
                        $transaction->response_text = '';
                        $transaction->subscription_id = $user_subscription_id;
                        $transaction->ip = $ip_address;
                        $transaction->created_date = new CDbExpression("NOW()");
                        $transaction->save();

                        $file_name = '';
                        //Generate pdf for instant payment when zero free trial
                        
                        $studio = Studios::model()->findByPk($studio_id);
                        $arg['amount'] = Yii::app()->common->formatPrice($trans['Amount'], $currency_id, 1);
                        if (isset($couponCode) && $couponCode != "") {
                            $arg['discount_amount'] = Yii::app()->common->formatPrice($data['discount_amount'], $currency_id);
                            $arg['full_amount'] = Yii::app()->common->formatPrice($data['plan_price'], $currency_id);
                            $arg['coupon'] = $couponCode;
                        }
                        $arg['card_holder_name'] = $_REQUEST['CardHolderName'];
                        $arg['card_last_fourdigit'] = $_REQUEST['MaskedCard'];

                        $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, '');
                        

                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                        }
                        //$admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'welcome_email_with_subscription', $user_id, '', 0);
                    } else {
                        $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', '', '', '', '', '', '', $lang_code);
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                        }
                        //$admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'welcome_email_with_subscription', $user_id, '', 0);
                    }
                    //PciLog::model()->deleteByPk($log_data['id']);
                    //exit;
            } else {
                //PciLog::model()->deleteByPk($log_data['id']);
                $request = json_encode($_REQUEST);
                PciLog::model()->updateErrorLogIPPayment($log_data['id'],$request);
                exit;
            }
        
    }
    
    public function actionUserURLIPPaymentSubscription(){
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                $route->enabled = false;
            }
        }
        $log_data = PciLog::model()->findByAttributes(array('unique_id' => $_REQUEST['SessionId']));
        if(!empty($_REQUEST) && $_REQUEST['SST'] != ""){
            $data = json_decode($log_data['log_data'], true);
            if(isset($data['DeclinedCode']) && trim($data['DeclinedCode']) != ""){
                    Yii::app()->user->setFlash("error", $data['DeclinedMessage']);
                    echo '<script type="text/javascript">';
                    echo 'window.top.location.href = "'.Yii::app()->getbaseUrl(true).'";';
                    echo '</script>';
                    exit;
            }else{
            Yii::app()->user->setFlash('success', $this->ServerMessage['thanks_for_register']);
            echo '<script type="text/javascript">';
            echo 'window.top.location.href = "'.Yii::app()->getbaseUrl(true).'";';
            echo '</script>';
            exit;
            }
        }else{
            Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
            echo '<script type="text/javascript">';
            echo 'window.top.location.href = "'.Yii::app()->session['backbtnlink'].'";';
            echo '</script>';
            exit;
        }
    }
    
    /* Functions related IPPayments End */
    
    public function actionSetHostedData() {
        $user = SdkUser::model()->findByPk(Yii::app()->user->id);
        $data = $_POST;
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = $data['session_url'];
        Yii::app()->session['backbtnlink'] = $path;

        $gateway_code = $_POST['payment_method'];
        $data['studio_id'] = $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $plan_id = $data['plan_id'];
        $is_bundle = @$data['is_bundle'];
        $timeframe_id = @$data['timeframe_id'];
        if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
            $isadv = $data['is_advance_purchase'];
        } else {
            $isadv = $data['isadv'];
        }

        $timeframe_days = 0;
        $end_date = '0000-00-00 00:00:00';
        $data['coupon'] = $_POST['coupon'];
        $plan = PpvPlans::model()->findByPk($plan_id);

        $data['is_bundle'] = $is_bundle = (isset($plan->is_advance_purchase) && intval($plan->is_advance_purchase) == 2) ? 1 : 0;
        $is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        if (intval($is_bundle) && intval($timeframe_id)) {
            (array) $price = Yii::app()->common->getTimeFramePrice($plan_id, $data['timeframe_id'], $studio_id);
            $price = $price->attributes;

            $timeframe_days = PpvTimeframeLable::model()->findByPk($price['ppv_timeframelable_id'])->days;
        } else {
            $price = Yii::app()->common->getPPVPrices($plan->id, $data['currency_id']);
        }
        $currency = Currency::model()->findByPk($price['currency_id']);
        $data['currency_id'] = $currency->id;
        $data['currency_code'] = $currency->code;
        $data['currency_symbol'] = $currency->symbol;

        //Calculate ppv expiry time only for single part or episode only
        $start_date = Date('Y-m-d H:i:s');
        $data['start_date'] = $start_date;
        if (intval($is_bundle) && intval($timeframe_id) && intval($timeframe_days)) {
            $time = strtotime($start_date);
            $expiry = $timeframe_days . ' ' . "days";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$expiry}", $time));
        } else {
            if (intval($isadv) == 0) {
                $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'limit_video');
                $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'watch_period');
                $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'ppv', 'access_period');
                $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                $time = strtotime($start_date);
                if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                }

                $data['view_restriction'] = $limit_video;
                $data['watch_period'] = $watch_period;
                $data['access_period'] = $access_period;
            }
        }

        if ($is_bundle) {//For PPV bundle
            $data['season_id'] = 0;
            $data['episode_id'] = 0;

            if (intval($is_subscribed_user)) {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                }
            } else {
                if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {
                    $data['amount'] = $amount = $price['show_unsubscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        } else {
            //Set different prices according to schemes
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multi part videos
                //Check which part wants to sell by studio admin
                $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                $is_show = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                $is_season = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                $is_episode = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) != '0') { //Find episode amount
                    if (intval($is_episode)) {
                        $data['episode_uniq_id'] = $data['episode_id'];
                        $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
                        $data['episode_id'] = $streams->id;
                        $data['episode_uniq_id'] = $streams->embed_id;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['episode_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['episode_unsubscribed'];
                        }
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && intval($data['season_id']) && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find season amount
                    if (intval($is_season)) {
                        $data['episode_id'] = 0;

                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['season_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['season_unsubscribed'];
                        }

                        if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($price['ppv_season_status']) && intval($price['ppv_season_status']) && intval($isadv) == 0) {
                            $season_price_data['ppv_plan_id'] = $price['ppv_plan_id'];
                            $season_price_data['ppv_pricing_id'] = $price['id'];
                            $season_price_data['studio_id'] = $studio_id;
                            $season_price_data['currency_id'] = $price['currency_id'];
                            $season_price_data['season'] = $data['season_id'];
                            $season_price_data['is_subscribed'] = $is_subscribed_user;

                            $season_price = Yii::app()->common->getPPVSeasonPrice($season_price_data);
                            if ($season_price != -1) {
                                $data['amount'] = $amount = $season_price;
                            }
                        }

                        //$end_date = '0000-00-00 00:00:00';//Make unlimited time limit for season
                    }
                } else if (isset($data['movie_id']) && trim($data['movie_id']) && isset($data['season_id']) && trim($data['season_id']) == 0 && isset($data['episode_id']) && trim($data['episode_id']) == 0) { //Find show amount
                    if (intval($is_show)) {
                        $data['season_id'] = 0;
                        $data['episode_id'] = 0;
                        if (intval($is_subscribed_user)) {
                            $data['amount'] = $amount = $price['show_subscribed'];
                        } else {
                            $data['amount'] = $amount = $price['show_unsubscribed'];
                        }

                        //$end_date = '0000-00-00 00:00:00';//Make unlimited time limit for show
                    }
                }
            } else {//Single part videos
                $data['season_id'] = 0;
                $data['episode_id'] = 0;

                if (intval($is_subscribed_user)) {
                    $data['amount'] = $amount = $price['price_for_subscribed'];
                } else {
                    $data['amount'] = $amount = $price['price_for_unsubscribed'];
                }
            }
        }
        $data['end_date'] = $end_date;
        $couponCode = '';
        //Calculate coupon if exists
        if ((isset($data['coupon']) && $data['coupon'] != '') || (isset($data['coupon_instafeez']) && $data['coupon_instafeez'] != '')) {
            $coupon = (isset($data['coupon']) && $data['coupon'] != '') ? $data['coupon'] : $data['coupon_instafeez'];
            $getCoup = Yii::app()->common->getCouponDiscount($coupon, $amount, $studio_id, $user_id, $data['currency_id']);
            $data['amount'] = $amount = $getCoup["amount"];
            $couponCode = $getCoup["couponCode"];
        }

        $name = explode(' ', $user->display_name);
        $data['email'] = Yii::app()->user->email;
        $data['first_name'] = isset($name[0]) && trim($name[0]) ? $name[0] : '';
        $data['last_name'] = isset($name[1]) && trim($name[1]) ? $name[1] : '';
        $data['currency_code'] = $currency->code;
        $data['currency_id'] = $currency->id;
        $data['shipping_cost'] = isset($_REQUEST['shipping_cost']) && $_REQUEST['shipping_cost'] ? $_REQUEST['shipping_cost'] : 0;
        $data['shipping'] = 0;
        $data['return'] = Yii::app()->getBaseUrl(true) . "/userPayment/returnHostedData";

        $timeparts = explode(" ", microtime());
        $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
        $reference_number = explode('.', $currenttime);
        $data['unique_id'] = $reference_number[0];
        Yii::app()->session['ppv_data'] = $data;
        echo json_encode($data);
    }

    public function actionReturnHostedData() {
        if (Yii::app()->user->id) {
            $res = $_REQUEST;
            $data = $pay = Yii::app()->session['ppv_data'];
            $data['email'] = Yii::app()->user->email;
            $trans_data['bill_amount'] = $data['amount'];
            $paypal_transaction_id = isset($res['tx']) && trim($res['tx']) ? $res['tx'] : $res['txn_id'];
            $payment_gateway_controller = 'ApipaypalproController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::getPCICardTransactionDetails($paypal_transaction_id);
            $trans_data['paid_amount'] = $trans_data['dollar_amount'] = $trans_data['amount'] = $resArray["AMT"];
            $trans_data['transaction_status'] = $resArray["PAYMENTSTATUS"];
            $trans_data['transaction_status_reason'] = isset($resArray["PAYMENTSTATUS"]) && trim(strtolower($resArray["PAYMENTSTATUS"])) == 'pending' ? $resArray["PENDINGREASON"] : '';
            $trans_data['invoice_id'] = $resArray['TRANSACTIONID'];
            $trans_data['order_number'] = strtoupper($resArray['CORRELATIONID']);
            ;
            $trans_data['PAYERID'] = $resArray['PAYERID'];
            $trans_data['SHIPTONAME'] = $resArray['SHIPTONAME'];
            $trans_data['SHIPTOSTREET'] = $resArray['SHIPTOSTREET'];
            $trans_data['SHIPTOCITY'] = $resArray['SHIPTOCITY'];
            $trans_data['SHIPTOSTATE'] = $resArray['SHIPTOSTATE'];
            $trans_data['COUNTRYCODE'] = $resArray['COUNTRYCODE'];
            $trans_data['SHIPTOZIP'] = $resArray['SHIPTOZIP'];
            $trans_data['response_text'] = json_encode($resArray);
            $data['order_reference_number'] = $data['unique_id'];
            $studio_id = Yii::app()->common->getStudiosId();
            $gateway_code = $data["payment_method"];
            $plan_id = $data['plan_id'];
            $planModel = new PpvPlans();
            $plan = $planModel->findByPk($plan_id);
            if (isset($data['is_advance_purchase']) && intval($data['is_advance_purchase'])) {
                $isadv = $data['is_advance_purchase'];
            } else {
                $isadv = $data['isadv'];
            }
            $Films = Film::model()->findByAttributes(array('studio_id' => $data['studio_id'], 'uniq_id' => $data['movie_id']));
            $video_id = $Films->id;
            $VideoName = Yii::app()->common->getVideoname($video_id);
            $VideoName = trim($VideoName);
            $VideoDetails = $VideoName;
            $streams = movieStreams::model()->findByAttributes(array('studio_id' => $studio_id, 'embed_id' => $data['episode_id']));
            if (isset($plan->content_types_id) && trim($plan->content_types_id) == 3) {//Multipart Content
                if ($data['season_id'] == 0) {
                    $VideoDetails .= ', All Seasons';
                } else {
                    $VideoDetails .= ', Season ' . $data['season_id'];
                    if ($data['episode_id'] == 0) {
                        $VideoDetails .= ', All Episodes';
                    } else {
                        $episodeName = Yii::app()->common->getEpisodename($streams->id);
                        $episodeName = trim($episodeName);
                        $VideoDetails .= ', ' . $episodeName;
                    }
                }
            }
            $trans_data['amount'] = $data['amount'];
            $set_ppv_subscription = Yii::app()->billing->setPpvSubscription($plan, $data, $video_id, $data['start_date'], $data['end_date'], $data['coupon'], $isadv, $gateway_code);
            $ppv_subscription_id = $set_ppv_subscription;
            $transaction_id = Yii::app()->billing->setPpvTransaction($plan, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
            $ppv_apv_email = Yii::app()->billing->sendPpvEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails);
            self::setRedirectPpv($Films, $plan, $data, $isadv);
            exit;
        } else {
            $this->redirect($data['session_url']);
        }
    }

    public function actionProcessCard() {
        parse_str($_REQUEST['data'], $pay);
        if (isset($pay) && !empty($pay)) {
            $studio_id = Yii::app()->common->getStudiosId();
            $ip = CHttpRequest::getUserHostAddress();
            $datap = $pay['pay'];
            if (isset(Yii::app()->user->id) && intval(Yii::app()->user->id)) {
                $user = SdkUser::model()->findByPk(Yii::app()->user->id)->email;
                $_REQUEST['email'] = $user->email;
            }
            $_REQUEST['user_id'] = Yii::app()->user->id ? Yii::app()->user->id : 0;
            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio->id, 'status' => 1, 'is_primary' => 1));
			if (empty($gateway_info)) {
				$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => 0, 'status' => 1, 'is_primary' => 1));
			}
            if ($this->PAYMENT_GATEWAY[$gateway_info->short_code] != '') {
                /* coupon integration */
                if ((isset($_REQUEST['coupon']) && $_REQUEST['coupon'] != '')) {
                    $coupon = $_REQUEST['coupon'];
                    $getCoup = Yii::app()->common->getCouponDiscount($coupon, $datap['amount'], $this->studio->id, Yii::app()->user->id, $datap['currency_id']);
                    $datap['amount'] = $amount = $getCoup["amount"];
                    $couponCode = $getCoup["couponCode"];
                    $datap['coupon_code'] = $couponCode;
                    $datap['discount_type'] = 0; //$getCoup["discount_type"];
                    $datap['discount'] = $getCoup["coupon_amount"];
                }
                $datap['amount'] = (float) $datap['amount'] + (float) $datap['shipping_cost'];
                $datap['email'] = Yii::app()->user->email;
                $datap['cvv'] = $datap['security'];
                $datap['gateway_code'] = $gateway_info->short_code;
                if ($datap['amount'] > 0) {
                    if (isset($gateway_info->short_code) && $gateway_info->short_code == 'paygate') {
                        $timeparts = explode(" ", microtime());
                        $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                        $reference_number = explode('.', $currenttime);
                        $datap['returnURL'] = Yii::app()->getBaseUrl(true) . "/userPayment/payGateReturn";
                        $datap['cancelURL'] = Yii::app()->getBaseUrl(true) . "/userPayment/cancel";
                        $datap['plan_desc'] = 'Physical goods - ' . $reference_number[0];
                        $datap['isSuccess'] = 1;
                        $datap['payment_method'] = 'paygate';
                    }
                    $datap['is_physical'] = 1;
                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $data = $payment_gateway::processCard($datap); //Authenticate Card
                    $res = json_decode($data, true);
                    $datap['gateway_response'] = $res;
                } else {
                    $datap['isSuccess'] = 1;
                }
                echo json_encode($datap);
                exit;
            }
        }
    }
    
    public function actionprocessCoupon(){
        $data = '';
        if (isset($_POST) && !empty($_POST)) {
            $studio_id = Yii::app()->common->getStudiosId();
            $gateway_code = Yii::app()->common->getPrimaryPaymentGatewayCode();
            $_POST['gateway_code'] = $gateway_code;
            $PATH_TO_LIB = Studio::model()->findByPk($studio_id)->permalink;
            $this->PATH_TO_LIB = $_SERVER["DOCUMENT_ROOT"] .'/protected/vendor/Coupon/' . $PATH_TO_LIB . '/';
            $payment_gateway_controller = 'ApicouponController';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $data = $payment_gateway::processCoupon($_POST);
            $res = json_encode($data, true);
            echo $res;exit;
        }
    }
    
    public function actioncouponCreateUserSubscription(){
        if (isset(Yii::app()->session['backbtnlink']) && trim(Yii::app()->session['backbtnlink'])) {
            unset(Yii::app()->session['backbtnlink']);
        }
        $path = Yii::app()->getBaseUrl(true) . Yii::app()->request->requestUri;
        Yii::app()->session['backbtnlink'] = $path;
        $studio_id = Yii::app()->common->getStudiosId();
        if (isset($_POST['data']) && !empty($_POST['data'])) {
            $user_id = Yii::app()->user->id;
            $data = $_POST['data'];
            $gateway_code = $data['payment_method'];
            $start_date = $end_date = '0000-00-00 00:00:00';
            $plan_id = $plan_price = $payment_gateway_id = 0;
            if ((isset($data['plan_id']) && intval($data['plan_id']))) {
                $plan_id = $data['plan_id'];
                $planModel = new SubscriptionPlans();
                $plan = $planModel->findByPk($plan_id);
                $price_list = Yii::app()->common->getUserSubscriptionPrice($plan_id, $currency_id);
                $plan_price = $price_list['price'];
                $trail_period = $plan->trial_period;
                if (isset($trail_period) && (intval($trail_period) == 0)) {
                    $start_date = Date('Y-m-d H:i:s');
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                    $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
                }else {
                    $trial_period = $trail_period . ' ' . strtolower($plan->trial_recurrence) . "s";
                    $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                    $time = strtotime($start_date);
                    $recurrence_frequency = $plan->frequency . ' ' . strtolower($plan->recurrence) . "s";
                    $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                }
                $ip_address = Yii::app()->request->getUserHostAddress();
                $payment_gateway_id = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                //Create user subscription 
                $usModel = new UserSubscription;
                $usModel->studio_id = $studio_id;
                $usModel->user_id = $user_id;
                $usModel->plan_id = $plan_id;
                $usModel->studio_payment_gateway_id = $payment_gateway_id;
                $usModel->currency_id = $data['currency_id'];
                $usModel->amount = $plan_price;
                $usModel->coupon_code = $data['voucher_code'];
                $usModel->is_3rd_party_coupon = 1;
                $usModel->start_date = $start_date;
                $usModel->end_date = $end_date;
                $usModel->is_success = 1;
                $usModel->status = 1;
                $usModel->ip = $ip_address;
                $usModel->created_by = $user_id;
                $usModel->created_date = new CDbExpression("NOW()");
                $usModel->save();
                
                $user_subscription_id = $usModel->id;
                $invoice_id = Yii::app()->common->generateUniqNumber();
                
                $transaction = new Transaction;
                $transaction->user_id = $user_id;
                $transaction->studio_id = $studio_id;
                $transaction->plan_id = $plan_id;
                $transaction->currency_id = $data['currency_id'];
                $transaction->transaction_date = new CDbExpression("NOW()");
                $transaction->payment_method = $data['payment_method'];
                $transaction->transaction_status = $data['transaction_status'];
                $transaction->invoice_id = $invoice_id;
                $transaction->order_number = Yii::app()->common->generateUniqNumber();
                $transaction->dollar_amount = 0;
                $transaction->amount = 0;
                $transaction->response_text = $data['response_text'];
                $transaction->subscription_id = $user_subscription_id;
                $transaction->ip = $ip_address;
                $transaction->created_date = new CDbExpression("NOW()");
                $transaction->save();
                
                //Invoice and email 
                $lang_code = $this->language_code;
                
                $discount_price = $price_list['price'];
                $arg['discount_amount'] = Yii::app()->common->formatPrice($discount_price, $data['currency_id']);
                $arg['full_amount'] = Yii::app()->common->formatPrice($price_list['price'], $data['currency_id']);
                $arg['coupon'] = $data['voucher_code'];
                $arg['amount'] = $arg['full_amount'];
                $file_name = Yii::app()->pdf->invoiceDetial($this->studio, $arg, $invoice_id);
                
                $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $lang_code);
                $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                if ($isEmailToStudio) {
                    $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                }
            }
            Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit;
        }
    }

    public function actionSaveOrder() {
        if (Yii::app()->user->id) {
            $ip = CHttpRequest::getUserHostAddress();
            $res = Yii::app()->general->checkFinalPGprice($this->studio->id, $_REQUEST['data']['coupon_code'], $_SESSION['check_smethod']);
            $pay = $_REQUEST['data'];
            $pay['amount'] = $res['item_total'];
            $pay['discount'] = $res['discount'];
            $pay['currency_id'] = $res['currency_id'];
            $pay['shipping_cost'] = $res['shipping_cost'];

            $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $this->studio->id, 'status' => 1, 'is_primary' => 1));
			if (empty($gateway_info)) {
				$gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => 0, 'status' => 1, 'is_primary' => 1));
			}
            if ($this->PAYMENT_GATEWAY[$gateway_info->short_code] != '') {//check Payment Gatway Exists
                $datap = $_REQUEST['data'];
                $datap['is_physical'] = 1;
                $datap['cvv'] = $datap['security'];
                $datap['security_code'] = $datap['security'];
                $datap['gateway_code'] = $gateway_info->short_code;

                $datap['amount'] = $res['total_amount'];
                $datap['discount'] = $res['discount'];
                $datap['currency_id'] = $res['currency_id'];
                $datap['shipping_cost'] = $res['shipping_cost'];

                if ($datap['amount'] > 0) {
                    $currency = Currency::model()->findByPk($datap['currency_id']);
                    $datap['currency_id'] = $currency->id;
                    $datap['currency_code'] = $currency->code;
                    $datap['currency_symbol'] = $currency->symbol;

                    $timeparts = explode(" ", microtime());
                    $currenttime = bcadd(($timeparts[0] * 1000), bcmul($timeparts[1], 1000));
                    $reference_number = explode('.', $currenttime);
                    $datap['order_reference_number'] = $reference_number[0];

                    $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_info->short_code] . 'Controller';
                    Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                    $payment_gateway = new $payment_gateway_controller();
                    $datap['gateway_code'] = $gateway_info->short_code;
                    $datap['studio_name'] = Studios::model()->findByPk($this->studio->id)->name;
                    $datap['user_email'] = Yii::app()->user->email;
                    $trans_data = $payment_gateway::processTransactions($datap); //charge the card
                    if (intval($trans_data['is_success'])) {
                        if ($pay['savecard'] == 1) {
                            $sciModel = New SdkCardInfos;
                            $card = $datap;
                            $card['card_last_fourdigit'] = $res['card']['card_last_fourdigit'];
                            $card['token'] = $res['card']['token'];
                            $card['card_type'] = $res['card']['card_type'];
                            $card['auth_num'] = $res['card']['auth_num'];
                            $card['profile_id'] = $res['card']['profile_id'];
                            $card['reference_no'] = $res['card']['reference_no'];
                            $card['response_text'] = $res['card']['response_text'];
                            $card['status'] = $res['card']['status'];
                            $sciModel->insertCardInfos($this->studio->id, Yii::app()->user->id, $card, $ip);
                        }
                        //Save a transaction detail
                        $transaction = new Transaction;
                        $transaction_id = $transaction->insertTrasactions($this->studio->id, Yii::app()->user->id, $datap['currency_id'], $trans_data, 4, $ip, $this->PAYMENT_GATEWAY[$gateway_info->short_code]);
                        $datap['transactions_id'] = $transaction_id;
                        /* Save to order table */
                        $datap['hear_source'] = $pay['hear_source'];
                        $pgorder = new PGOrder();
                        $orderid = $pgorder->insertOrder($this->studio->id, Yii::app()->user->id, $datap, $_SESSION["cart_item"], $_SESSION['ship'], $ip);
                        /* Save to shipping address */
                        $pgshippingaddr = new PGShippingAddress();
                        $pgshippingaddr->insertAddress($this->studio->id, Yii::app()->user->id, $orderid, $_SESSION['ship'], $ip);
                        /* Save to Order Details */
                        $pgorderdetails = new PGOrderDetails();
                        $pgorderdetails->insertOrderDetails($orderid, $_SESSION["cart_item"]);
                        if ($pay['card_options'] != '') {
                            $data = array('isSuccess' => 1);
                            $data = json_encode($data);
                        }
                        //send email
                        $req['orderid'] = $orderid;
                        $req['emailtype'] = 'orderconfirm';
                        $req['studio_id'] = $this->studio->id;
                        $req['studio_name'] = $this->studio->name;
                        $req['currency_id'] = $datap['currency_id'];
                        // Send email to user
                        Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);
                        if ($isEmailToStudio) {
                            Yii::app()->email->pgEmailTriggers($req);
                        }

                        self::emptyCart(); // Now empty the cart
                        //create order in cds
                        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));
                        if ($webservice) {
                            if ($webservice->inventory_type == 'CDS') {
                                $pgorder->CreateCDSOrder($this->studio->id, $orderid);
                                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
                            }
                        }
                    } else {
                        Yii::app()->user->setFlash('error',$this->Language['transaction_error']);
                        $this->redirect(Yii::app()->getBaseUrl(true) . '/shop/cart');
                        exit;
                    }
                } else {
                    $data = array('isSuccess' => 1);
                    $data = json_encode($data);
                    $physical = self::physicalDataInsert($datap, $pay, $trans_data, $ip, $this->PAYMENT_GATEWAY[$gateway_info->short_code]);
                }
            } else {
                Yii::app()->user->setFlash('error', $this->Language['checkout_not_allowed']);
                $this->redirect(Yii::app()->getBaseUrl(true));
                exit;
            }
            /* End Payment */
        } else {
            Yii::app()->user->setFlash('error', $this->Language['login_to_save_order']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/user/login');
            exit;
        }
        Yii::app()->user->setFlash('success', $this->Language['order_placed_successfully']);
        $this->redirect(Yii::app()->getBaseUrl(true) . '/shop/success');
        exit;
    }

    public function physicalDataInsert($datap, $pay, $trans_data, $ip, $short_code) {
        //Save a transaction detail
        $transaction = new Transaction;
        $transaction_id = $transaction->insertTrasactions($this->studio->id, Yii::app()->user->id, $datap['currency_id'], $trans_data, 4, $ip, $short_code);
        $datap['transactions_id'] = $transaction_id;
        /* Save to order table */
        $datap['hear_source'] = $pay['hear_source'];
        $pgorder = new PGOrder();
        $orderid = $pgorder->insertOrder($this->studio->id, Yii::app()->user->id, $datap, $_SESSION["cart_item"], $_SESSION['ship'], $ip);
        /* Save to shipping address */
        $pgshippingaddr = new PGShippingAddress();
        $pgshippingaddr->insertAddress($this->studio->id, Yii::app()->user->id, $orderid, $_SESSION['ship'], $ip);
        /* Save to Order Details */
        $pgorderdetails = new PGOrderDetails();
        $pgorderdetails->insertOrderDetails($orderid, $_SESSION["cart_item"]);
        if ($pay['card_options'] != '') {
            $data = array('isSuccess' => 1);
            $data = json_encode($data);
        }
        //send email
        $req['orderid'] = $orderid;
        $req['emailtype'] = 'orderconfirm';
        $req['studio_id'] = $this->studio->id;
        $req['studio_name'] = $this->studio->name;
        $req['currency_id'] = $datap['currency_id'];
        // Send email to user
        Yii::app()->email->getMuviKartEmails($req, 'mk_after_order_placed');
        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('muvi_kart_admin_notifications', $this->studio->id);
        if ($isEmailToStudio) {
            Yii::app()->email->pgEmailTriggers($req);
        }

        $this->emptyCart(); // Now empty the cart
        //create order in cds
        $webservice = StudioWebservices::model()->find('studio_id=:studio_id', array(':studio_id' => $this->studio->id));
        if ($webservice) {
            if ($webservice->inventory_type == 'CDS') {
                $pgorder->CreateCDSOrder($this->studio->id, $orderid);
                //PGOrderDetails::CancelCDSOrder('$order_number',$item_number);
            }
        }
        return 1;
    }

    public function emptyCart() {
        unset($_SESSION["cart_item"]);
        $_SESSION['totalqnt'] = 0;
        $_SESSION['item_total'] = 0;
        $_SESSION['address_details'] = 0;
        $_SESSION['payment_details'] = 0;
        $_SESSION["freeoffer"] = '';
        $_SESSION['couponCode'] = '';
        unset($_SESSION['ship']);
    }

    public function actionGetContentPriceDetail() {
        $contentPrice = array();

        if (isset($_POST) && !empty($_POST)) {
            $arg['studio_id'] = $_POST['studio_id'];
            $arg['movie_id'] = $_POST['movie_id'];
            $arg['default_currency_id'] = (isset($_POST['default_currency_id'])) ? $_POST['default_currency_id'] : $this->studio->default_currency_id;

            $contentPrice = Yii::app()->common->GetContentPriceDetail($arg);
        } else {
            $contentPrice['isSuccess'] = 0;
        }

        print '<pre>';
        print_r($contentPrice);
        exit;
    }

    //subscription bundles By sunil nayak
    //card payment(F)
    public function actionSaveuserSubscriptionBundles() {
        $studio_id = $this->studio->id;
        $data = $_POST['data'];
        if ($this->studio->is_csrf_enabled) {
            if ($_POST['csrfToken'] === @$_SESSION['csrfToken']) {
                unset($_SESSION['csrfToken']);
            } else {
                if (!Yii::app()->request->isAjaxRequest) {
                    $url = Yii::app()->createAbsoluteUrl('user/register');
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: ' . $url);
                    exit();
                } else {
                    echo '';
                    exit;
                }
            }
        }
       
        $is_subscriptionbundles = @$data['is_subscriptionbundle'];
        if ($is_subscriptionbundles) {
            $plandetailbundles_id = $data['plandetailbundles_id'];
            $plan_subscriptionbundles = SubscriptionPlans::model()->findByPk($plandetailbundles_id);
        }
        if (isset($_POST['data']) && !empty($_POST['data']) && isset($_POST['data']['email']) && filter_var($_POST['data']['email'], FILTER_VALIDATE_EMAIL) != false) {
            $name = trim($_POST['data']['name']);
            if (isset(Yii::app()->request->cookies['SITE_REFERRER']) && trim(Yii::app()->request->cookies['SITE_REFERRER'])) {
                $source = Yii::app()->request->cookies['SITE_REFERRER'];
            }
            $gateway_code = $_POST['data']['payment_method'];
            $_POST['PAYMENT_GATEWAY'] = $this->PAYMENT_GATEWAY[$gateway_code];
            $_POST['GATEWAY_ID'] = $this->GATEWAY_ID[$gateway_code];
            $_POST['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
            $_POST['source'] = $source;
            $default_currency_id = $this->studio->default_currency_id;
            $ret = SdkUser::model()->saveSdkUserSubscriptionBundles($_POST, $studio_id, $default_currency_id);
            if (intval($is_subscriptionbundles)) {
                $VideoName = $plan_subscriptionbundles->name;
            }
            if ($ret) {
                if (@$ret['error']) {
                    Yii::app()->user->setFlash('error', $ret['error']);
                    $this->redirect($this->createUrl('register'));
                    exit;
                }
                if (!$is_paypal) {
                    // Send welcome email to user
                    if (@$ret['is_subscribed']) {
                        $permalink = $_POST['data']['permalink'];
                        $file_name = '';
                        //Generate pdf for instant payment when zero free trial
                        if (isset($_POST['data']['transaction_data']['is_success']) && intval($_POST['data']['transaction_data']['is_success'])) {
                            $studio = $this->studio;
                            $user['amount'] = Yii::app()->common->formatPrice($_POST['data']['transaction_data']['amount'], $_POST['data']['currency_id']);
                            $user['card_holder_name'] = $_POST['data']['card_name'];
                            $user['card_last_fourdigit'] = $_POST['data']['card_last_fourdigit'];
                            $file_name = Yii::app()->pdf->invoiceDetial($studio, $user, $_POST['data']['transaction_data']['invoice_id']);
                        }
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($ret, $data, $ret['transaction_id'], $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                        }
                        if($data['is_subscription_bundle_checked']==1){
                            $this->redirect(Yii::app()->getbaseUrl(true) . '/'.$this->Player_Page.'/' . $permalink);
                            exit;
                        }else{
                            Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
                            $this->redirect(Yii::app()->getbaseUrl(true));
                            exit;  
                        }
                    } else {
                        $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                        if ($isEmailToStudio) {
                            $ppv_apv_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $ret['transaction_id'], $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                        }
                        Yii::app()->user->setFlash('error', $this->ServerMessage['error_transc_process']);
                        $this->redirect(Yii::app()->getbaseUrl(true) . '/' . $permalink);
                        exit;
                    }
                }
            }
        } else {
            $err_msg = $this->ServerMessage['error_process_payment'];
            $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register?error=' . urlencode($err_msg));
        }
    }

    //get bundles content(F)
    function actionGetsubscriptionBundledContents() {
        $contents = '';
        if (isset($_POST['plan']) && intval($_POST['plan'])) {
            $studio_id = $this->studio->id;
            $data = Yii::app()->db->createCommand()
                    ->select('f.name ')
                    ->from("films  f")
                    ->join('subscriptionbundles_content  pac', 'f.id=pac.content_id')
                    ->where('pac.subscriptionbundles_plan_id=:subscriptionbundles_plan_id AND f.studio_id =:studio_id ', array(':subscriptionbundles_plan_id' => $_POST['plan'], ':studio_id' => $studio_id))
                    ->queryAll();
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $contents.=ucfirst((strlen($value['name']) > 20) ? substr($value['name'], 0, 20) . '...' : $value['name']) . "<br/>";
                }
            }
        }
        print $contents;
        exit;
    }

    //get subscription bundle content(F)
    function actionGetsubscriptionBundlePrice() {
        $subscriptionbundles_plan_id = @$_POST['plan_id'];
        $studio_id = $this->studio->id;
        $price = array();        
        $default_currency_id = $this->studio->default_currency_id;
        if (isset($subscriptionbundles_plan_id) && intval($subscriptionbundles_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '');
            if (isset($country) && trim($country)) {
                
        $price = Yii::app()->db->createCommand()
                       ->select(' sp.* ')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('cu.country_code=:country_code AND sp.subscription_plan_id=:subscription_plan_id AND sp.status=1',array(':country_code'=>$country,':subscription_plan_id'=>$subscriptionbundles_plan_id))
                ->queryRow();
                if (empty($price)) {
                    $is_default_currency = 1;
    }
            } else {
                $is_default_currency = 1;
            }
            if (intval($is_default_currency)) {
                  $price = Yii::app()->db->createCommand()
                       ->select(' sp.*')
                       ->from("subscription_pricing  sp")
                       ->leftJoin('currency cu' , 'sp.currency_id=cu.id')
                       ->where('sp.subscription_plan_id=:subscription_plan_id AND sp.status=1 AND sp.currency_id=:currency_id',array(':subscription_plan_id'=>$subscriptionbundles_plan_id ,':currency_id'=>$default_currency_id))
                       ->queryRow();
            }
        }
        $currencySymbol=Currency::model()->findByPk($price['currency_id']);
        $price['symbol']=$currencySymbol->symbol ;
        echo json_encode($price);exit;
       
    }
    function actiongetppvprice() {
        $ppv_plan_id = @$_POST['plan_id'];
        $studio_id = $this->studio->id;
        $price = array();        
        $default_currency_id = $this->studio->default_currency_id;  
        if (isset($ppv_plan_id) && intval($ppv_plan_id)) {
            $is_default_currency = 0;
            $con = Yii::app()->db;
            $country = (isset($country) && trim($country)) ? $country : ((isset($_SESSION[$studio_id]['country']) && trim($_SESSION[$studio_id]['country'])) ? $_SESSION[$studio_id]['country'] : '');
            if (isset($country) && trim($country)) {
                
                 $price = Yii::app()->db->createCommand()
                       ->select(' pp.* ')
                       ->from("ppv_pricing  pp")
                       ->leftJoin('currency cu' , 'pp.currency_id=cu.id')
                       ->where('cu.country_code=:country_code AND pp.ppv_plan_id=:ppv_plan_id AND pp.status=1',array(':country_code'=>$country,':ppv_plan_id'=>$ppv_plan_id))
                       ->queryRow();
                 
                if (empty($price)) {
                    $is_default_currency = 1;
                }
            } else {
                $is_default_currency = 1;
            }
            if (intval($is_default_currency)) {
                  $price = Yii::app()->db->createCommand()
                       ->select('pp.*')
                       ->from("ppv_pricing  pp")
                       ->leftJoin('currency cu' , 'pp.currency_id=cu.id')
                       ->where('pp.ppv_plan_id=:ppv_plan_id AND pp.status=1 AND pp.currency_id=:currency_id',array(':ppv_plan_id'=>$ppv_plan_id,':currency_id'=>$default_currency_id))
                       ->queryRow();
            }
        }
        $currencySymbol=Currency::model()->findByPk($price['currency_id']);
        $des=new PpvPlans();
        $ppv_plan = $des->findByAttributes(array('id' => $ppv_plan_id, 'studio_id' => $studio_id, 'status' => 1, 'is_all' => 0, 'is_advance_purchase' => 0));
        
        $price['description']=$ppv_plan['description'];
        $price['symbol']=$currencySymbol->symbol ;
        echo json_encode($price);exit;
       
    }
    //subscription bundles payment(F)
       public function actionSubscriptionBundlesPayment(){
        $gateway_code = isset($_POST['data']['payment_method']) && trim($_POST['data']['payment_method'])?$_POST['data']['payment_method']:'';
        $studio_id = Yii::app()->common->getStudiosId();
        $user_id = Yii::app()->user->id;
        $VideoDetails = '';
        $data = @$_POST['data'];
                        $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$data['movie_id']));
        $video_id = $Films->id;
        $plan_id = 0;
                        $isadv='';
        if ((isset($data['plandetailbundles_id']) && intval($data['plandetailbundles_id']))) {
            $end_date = '0000-00-00 00:00:00';
            $is_subscriptionbundles = @$data['is_subscriptionbundle'];
            if (intval($is_subscriptionbundles)) {
                $plandetailbundles_id = $data['plandetailbundles_id'];
                $plan_subscriptionbundles = SubscriptionPlans::model()->findByPk($plandetailbundles_id);
                //$pricebundles = Yii::app()->common->getSubscriptionBundlesPrice($plandetailbundles_id, $this->studio->default_currency_id,$studio_id);
                $pricebundles = Yii::app()->common->getAllSubscriptionBundlesPrices($plandetailbundles_id, $this->studio->default_currency_id, $studio_id);
                $currency_id = (isset($pricebundles['currency_id']) && trim($pricebundles['currency_id'])) ? $pricebundles['currency_id'] : $this->studio->default_currency_id;
                $currency = Currency::model()->findByPk($currency_id);
            }

            $data['currency_id'] = $currency->id;
            $data['currency_code'] = $currency->code;
            $data['currency_symbol'] = $currency->symbol;
            $data['is_subscription_bundles'] = $data['is_subscription_bundle_checked'];
            //Calculate ppv expiry time only for single part or episode only
            $start_date = Date('Y-m-d H:i:s');
            $trail_period = $plan_subscriptionbundles->trial_period;
            $plans = UserSubscription::model()->UserSubscriptionBundlesPlanExist($studio_id, $user_id, 1);
            $Countplans = count($plans);
            if ($Countplans >= 1) {
                $trail_period = 0;
            }
            if ($_REQUEST['data']['coupon'] != '') {
                $couponCode = $_REQUEST['data']['coupon'];
                //$trail_period = 0;
                if ($_REQUEST['data']['subscriptionBundle_coupon_freeTrial'] > 0) {
                    $trail_period = $_REQUEST['data']['subscriptionBundle_coupon_freeTrial'];
                }
                if (intval($trail_period) > 0) {
                    $use_discount = 1;
                }
            }
            $data['trailPeriod'] = $trail_period;
            $is_transaction = 0;
            if (isset($trail_period) && (intval($trail_period) == 0)) {
                $is_transaction = 1;
                $start_date = Date('Y-m-d H:i:s');
                $time = strtotime($start_date);
                $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
                $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
            } else {
                $trail_recurrence = 'day';
                if ($plan_subscriptionbundles->trial_recurrence != '') {
                    $trail_recurrence = $plan_subscriptionbundles->trial_recurrence;
                }
                $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
                $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
                $time = strtotime($start_date);
                $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
                $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
            }
            $data['trialperiod'] = $plan_subscriptionbundles->trial_recurrence;
            $data['trialfrequency'] = $plan_subscriptionbundles->trial_period;
            $data['trialtotalcysles'] = '1';
            $data['trial_amount'] = '0.00';
            $data['recurrence'] = $plan_subscriptionbundles->recurrence;
            $data['frequency'] = $plan_subscriptionbundles->frequency;
            if ($is_subscriptionbundles) {
                $data['season_id'] = 0;
                $data['episode_id'] = 0;
                $data['amount'] = $amount = $pricebundles['price'];
            }
            if (isset($_POST["data"]["coupon"]) && trim($_POST["data"]["coupon"])) {
                $datasubscriptionBundles = array();
                $datasubscriptionBundles['studio_id'] = Yii::app()->common->getStudiosId();
                $datasubscriptionBundles['coupon'] = $_POST["data"]["coupon"];
                $datasubscriptionBundles['plan'] = $data['plandetailbundles_id'];
                $user_id = Yii::app()->user->id;
                $datasubscriptionBundles['user_id'] = $user_id;
                //getting the subscription bundles plan details
                //$planDetails = SubscriptionPlans::model()->getPlanDetails($_POST["data"]["plandetailbundles_id"],$studio_id);
                $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($data['plandetailbundles_id'], $default_currency_id, $studio_id);
                $currency_id = (isset($planDetails['currency_id']) && trim($planDetails['currency_id'])) ? $planDetails['currency_id'] : $default_currency_id;
                $currency = Currency::model()->findByPk($currency_id);
                $planDetails['currency_id'] = $datasubscriptionBundles['currency'] = $currency_id;
                $planDetails['couponCode'] = $_POST["data"]["coupon"];
                $planDetails['physical'] = $_POST["physical"];
                $couponDetails = Yii::app()->billing->isValidCouponForSubscription($datasubscriptionBundles);
                
                if ($couponDetails != 0) {
                    $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails, $planDetails);
                } else {
                    $res['isError'] = 1;
                }
                //extend_free_trail
                $data['amount'] = $data['full_amount'] = $amount;
                if(!intval($res['isError'])){
                    $data['discount_amount']  = $data['amount'] = $res['discount_amount'];
                    $data['use_discount'] = 0;
                    if(isset($trail_period) && intval($trail_period)){
                        $data['use_discount'] = 1;
            }
                }
            }
            $data['card_holder_name'] = @$data['card_name'];
            if (isset($data['creditcard']) && trim($data['creditcard'])) {
                $card = SdkCardInfos::model()->findByAttributes(array('id' => $data['creditcard'], 'studio_id' => $studio_id, 'user_id' => $user_id));
                $data['card_id'] = @$card->id;
                $data['token'] = @$card->token;
                $data['profile_id'] = @$card->profile_id;
                $data['card_holder_name'] = @$card->card_holder_name;
                $data['card_type'] = @$card->card_type;
                $data['exp_month'] = @$card->exp_month;
                $data['exp_year'] = @$card->exp_year;
                if ($gateway_code == 'testgateway') {
                    $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => 0, 'gateway_id' => $card->gateway_id, 'status' => 1));
                } else {
                    $gateway_info = StudioPaymentGateways::model()->findByAttributes(array('studio_id' => $studio_id, 'gateway_id' => $card->gateway_id, 'status' => 1));
                }

                $gateway_code = $gateway_info->short_code;
            } else {
                $data['save_this_card'] = 1;
            }
            if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
                if (intval($is_subscriptionbundles)) {
                    $VideoName = $plan_subscriptionbundles->name;
                }
                if (abs($data['amount']) < 0.01) {
                    if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                        $data['gateway_code'] = $gateway_code;
                        $crd = Yii::app()->billing->setCardInfo($data);
                        $data['card_id'] = $crd;
                    }
                    $data['amount'] = 0;
                    $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code, $trail_period);
                    $ppv_subscription_id = $set_user_subscription_bundles;
                    if ($is_transaction) {
                        $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                    }
                    $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                        echo json_encode($data);
                        exit;
                    }
                    self::setRedirectPpv($Films, $plan_subscriptionbundles, $data, $isadv);
                } else {
                    $trial_text = '';
                    if(isset($data['trailPeriod']) && intval($data['trailPeriod']) >0){
                        $trial_text = $this->Language['triel_desc'].$data['trailPeriod'] . ' ' .$data['trialperiod'].'(s).';
                    }                   
                    $data['studio_id'] = $studio_id;
                    $data['gateway_code'] = $gateway_code;
                    $data['paymentDesc'] = $trial_text.' '.$plan_subscriptionbundles->name . ':  ' . $currency->symbol . $data['amount'] . '/' . $plan_subscriptionbundles->recurrence;
                    if ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && intval($data['havePaypal'])) {
                        $data['returnURL'] = Yii::app()->getBaseUrl(true) . "/userPayment/subscriptionConfirm?plandetailbundles_id=" . $plandetailbundles_id;
                        $data['cancelURL'] = Yii::app()->getBaseUrl(true) . "/userPayment/cancel";
                        $data['session_data'] = array(
                            'gateway_code' => $gateway_code,
                            'uniq_id' => $data['movie_id'],
                            'episode_id' => $data['episode_id'],
                            'episode_uniq_id' => $data['episode_uniq_id'],
                            'season_id' => $data['season_id'],
                            'permalink' => $data['permalink'],
                            'currency_id' => $data['currency_id'],
                            'amount' => $data['amount'],
                            'discount_amount' => $data['discount_amount'],
                            'couponCode' => $data['coupon'],
                            'is_bundle' => @$data['is_bundle'],
                            'timeframe_id' => @$data['timeframe_id'],
                            'isadv' => $isadv
                        );
                        Yii::app()->session['session_data'] = $data;
                    }
                    
                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' && isset($data['havePaypal']) && intval($data['havePaypal'])) {
                        self::paynow($data);
                        exit;
                    }
                    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] == 'instafeez') {
                        echo json_encode($data);
                        exit;
                    }
                    if ($trail_period == 0 && $_POST['data']['payment_method'] != 'paypalpro') {
                        $data['user_id'] = Yii::app()->user->id;
                        $data['studio_name'] = Studio::model()->findByPk($studio_id)->name;
                        $data['user_email'] = Yii::app()->user->email;
                        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
                        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
                        $payment_gateway = new $payment_gateway_controller();
                        $trans_data = $payment_gateway::processTransactions($data);
                        $payment_error_msg = '';
                        $logfile = dirname(__FILE__) . '/ppvpayment.txt';
                        $payment_error_msg .= "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
                        $payment_error_msg .= "\n-----Studio ID: " . $studio_id . "-----\n";
                        $payment_error_msg .= "\n-----User ID: " . $user_id . "-----\n";
                        $payment_error_msg.= json_encode($trans_data) . "\n";
                        //Write into the log file
                        file_put_contents($logfile, json_encode($trans_data), FILE_APPEND | LOCK_EX);
                        $is_paid = $trans_data['is_success'];
                        if (intval($is_paid)) {
                            $data['amount'] = $trans_data['amount'];
                            if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                                $crd = Yii::app()->billing->setCardInfo($data);
                                $data['card_id'] = $crd;
                            }
                            if (intval($is_subscriptionbundles)) {
                                $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                                $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code, $trail_period);
                                $ppv_subscription_id = $set_user_subscription_bundles;
                                if ($is_transaction) {
                                    $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
                                }
                                $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
                            }
                            if ($data['is_subscription_bundle_checked'] == 1) {
                                self::setRedirectPpv($Films, $plan_subscriptionbundles, $data, $isadv);
                            } else {
                                Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
                                $this->redirect(Yii::app()->getbaseUrl(true));
                                exit;
                            }
                        } else {
                            $logfile = dirname(__FILE__) . '/ppvpayment.txt';
                            $payment_error_msg .= "\n----------Transaction Error----------\n";
                            $payment_error_msg .= "\n----------Log Date: " . date('Y-m-d H:i:s') . "----------\n";
                            $payment_error_msg .= "\n-----Studio ID: " . $studio_id . "-----\n";
                            $payment_error_msg .= "\n-----User ID: " . $user_id . "-----\n";
                            $payment_error_msg.= json_encode($trans_data) . "\n";
                            //Write into the log file
                            file_put_contents($logfile, json_encode($trans_data), FILE_APPEND | LOCK_EX);
                            $teModel = New TransactionErrors;
                            $teModel->studio_id = $studio_id;
                            $teModel->user_id = $user_id;
                            $teModel->plan_id = $plan_id;
                            $teModel->payment_method = $this->PAYMENT_GATEWAY[$gateway_code];
                            $teModel->response_text = $trans_data['response_text'];
                            $teModel->created_date = new CDbExpression("NOW()");
                            $teModel->save();
                            Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
                            $this->redirect($_SERVER['HTTP_REFERER']);
                            exit;
                        }
                    } else {
                        if (isset($data['save_this_card']) && intval($data['save_this_card'])) {
                            $crd = Yii::app()->billing->setCardInfo($data);
                            $data['card_id'] = $crd;
                        }
                        $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
                        $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code, $trail_period);
                        //$subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, $transaction_id, $isadv, $VideoName, $VideoDetails,$data['plandetailbundles_id']);
                        if ($data['is_subscription_bundle_checked'] == 1 && $_POST['data']['payment_method'] != 'paypalpro') {
                            self::setRedirectPpv($Films, $plan_subscriptionbundles, $data, $isadv);
                        } else {
                            Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
                            $this->redirect(Yii::app()->getbaseUrl(true));
                            exit;
                        }
                    }
                }
            } else {
                
            }
        }
    }

    public function actionValidateCouponForSubscription() {
        $res = array();
        $res['isError'] = 0;
        if (isset($_POST["coupon"]) && trim($_POST["coupon"])) {
            $data = array();
    
            $data['studio_id'] = Yii::app()->common->getStudiosId();
            $data['coupon'] = $_POST["coupon"];
            $data['plan'] = $_POST["plan"];
            $data['currency'] = $_POST["currency"];
            $user_id = Yii::app()->user->id;
            if (isset($_POST["email"]) && $_POST["email"] != "") {
                $user_id = 0;
                $user = SdkUser::model()->findByAttributes(array('email' => $_POST["email"], 'studio_id' => $data['studio_id']));
                if (isset($user->id) && $user->id != '') {
                    $user_id = $user->id;
        }
    }
            $data['user_id'] = $user_id;
    
            $country = (isset($_SESSION[$data['studio_id']]['country']) && trim($_SESSION[$data['studio_id']]['country'])) ? $_SESSION[$data['studio_id']]['country'] : '';
            $planDetails = SubscriptionPlans::model()->getPlanDetails($_POST["plan"], $data['studio_id'], '', $country);
                
            $couponDetails = Yii::app()->billing->isValidCouponForSubscription($data);
            if ($couponDetails != 0) {
                if ($couponDetails == 1) {
                    $res['isError'] = 2;
                } else {
                    if ($couponDetails["discount_type"] == 1) {//calculate for percentage
                        $final_price = $planDetails['price'] - ($planDetails['price'] * $couponDetails['discount_amount'] / 100);
                        $new_final_price = number_format($final_price, 2, '.', '');
                        $res['full_amount'] = $planDetails['price'];
                        $res['discount_amount'] = $new_final_price;
                        $res['discount_price'] = $couponDetails['discount_amount'];
                        $res['extend_free_trail'] = $couponDetails['extend_free_trail'];
                        $res['symbol'] = $couponDetails['symbol'];
                        $res['is_cash'] = 0;
                    }else{//calculate for cash
                        if($_POST["currency"] != 0){
                            $currencyDetails  = CouponCurrencySubscription::model()->findByPk($_REQUEST["currency_id"]);
                
                            $discount_amount = number_format((float) ($couponDetails['discount_amount']), 2, '.', '');
                            $plan_amount = number_format((float) ($planDetails['price']), 2, '.', '');
                            $final_amount = $plan_amount;
                
                            if (($plan_amount - $discount_amount) > 0.1) {
                                $final_amount = $plan_amount - $discount_amount;
                            } else {
                                $discount_amount = $plan_amount;
                                $final_amount = number_format(0, 2, '.', '');;
                            }
                
                            $res['discount_price'] = $discount_amount;
                            $res['full_amount'] = $plan_amount;
                            $res['discount_amount'] = $final_amount;
                            $res['extend_free_trail'] = $couponDetails['extend_free_trail'];
                            $res['symbol'] = $couponDetails['symbol'];
                            $res['is_cash'] = 1;
                        } else {
                            $res['discount_amount'] = $couponDetails['discount_amount'];
                            $res['is_cash'] = 1;
                }
            }
                    if (isset($_REQUEST['physical']) && $_REQUEST['physical']) {
                        $_SESSION['couponCode'] = $_REQUEST["couponCode"];
                    }
                }
            } else {
                $res['isError'] = 1;
            }
        } else {
            $res['isError'] = 1;
        }
        echo json_encode($res);
        exit;
    }
    
    //Validate Coupon For SubscriptionBundles by sunilN
   public function actionValidateCouponForSubscriptionBundles() {
        $res = array();
        $res['isError'] = 0;
        if (isset($_POST["coupon"]) && trim($_POST["coupon"])) {
            $data = array();
            $data['studio_id'] = Yii::app()->common->getStudiosId();
            $data['coupon'] = $_POST["coupon"];
            $data['plan'] = $_POST["plan"];
            $user_id = Yii::app()->user->id;
            $data['user_id'] = $user_id;
            $default_currency_id = isset(Yii::app()->controller->studio->default_currency_id) ? Yii::app()->controller->studio->default_currency_id : 0;
            $planDetails = Yii::app()->common->getAllSubscriptionBundlesPrices($_POST["plan"], $default_currency_id, $data['studio_id']);
            $currency_id = (isset($planDetails['currency_id']) && trim($planDetails['currency_id'])) ? $planDetails['currency_id'] : $default_currency_id;
            $currency = Currency::model()->findByPk($currency_id);
            $planDetails['currency_id'] = $data['currency'] =  $currency_id;
            $planDetails['couponCode'] = $_REQUEST["coupon"];
            $planDetails['physical'] = $_REQUEST["physical"];
            $couponDetails = Yii::app()->billing->isValidCouponForSubscription($data);
            if ($couponDetails != 0) {
                $res = Yii::app()->billing->CouponSubscriptionBundlesCalculation($couponDetails, $planDetails);
            } else {
                $res['isError'] = 1;
    }
        } else {
            $res['isError'] = 1;
        }
         echo json_encode($res);exit;
    }
    
    public function actionsbwreturn() {
        if(isset($_REQUEST) && trim($_REQUEST['result']) == 'success'){
            $studio_id = Yii::app()->common->getStudiosId();
            $from_data['data'] = Yii::app()->session['redirect_form_data'];
            $from_data['PAYMENT_GATEWAY'] = 'sbw';
            $from_data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID['sbw'];
            $gateway_code = 'sbw';
            $user['profile_id'] = $_REQUEST['bill_id'];
            $price_list = Yii::app()->common->getUserSubscriptionPrice($from_data['data']['plan_id'], $from_data['data']['currency_id']);
            $user['amount'] = $price_list['price'];
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::processTransaction($user);
            
            $from_data['data']['card_name'] = $resArray['name_on_card'];
            $from_data['data']['card_last_fourdigit'] = $resArray['card_last_fourdigit'];
            $from_data['data']['exp_month'] = '';
            $from_data['data']['exp_year'] = '';
            $from_data['data']['auth_num'] = $resArray['auth_num'];
            $from_data['data']['token'] = $resArray['token'];
            $from_data['data']['profile_id'] = $user['profile_id'];
            $from_data['data']['reference_no'] = @$resArray['reference_no'];
            $from_data['data']['response_text'] = json_encode($resArray);
            $from_data['data']['status'] = $resArray['status'];
            
            
            $transaction_data = array(
                    'transaction_status'=> $resArray['result'],
                    'invoice_id'=> Yii::app()->common->generateUniqNumber(),
                    'order_number'=> Yii::app()->common->generateUniqNumber(),
                    'dollar_amount'=> $price_list['price'],
                    'amount'=> $price_list['price'],
                    'response_text'=> json_encode($resArray)
                
            );
            $from_data['data']['transaction_data'] = $transaction_data;
            if(isset(Yii::app()->user->id) && Yii::app()->user->id){
                $user_id = Yii::app()->user->id;
                $from_data['data']['user_id'] = $user_id;
                $user_details = SdkUser::model()->saveUserPayment($from_data, $studio_id);
            }else{
                $user_details = SdkUser::model()->saveSdkUser($from_data, $studio_id);
                $user_id = $user_details['user_id'];
            }
            
            $welcome_email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'welcome_email_with_subscription', '', '', '', '', '', '', $this->language_code);
            $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('welcome_email_with_subscription', $studio_id);
            if ($isEmailToStudio) {
                $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
            }
            if(isset(Yii::app()->user->id) && Yii::app()->user->id){
                Yii::app()->user->setFlash("success", $this->ServerMessage['subscribed_success']);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
                exit;
            }else if (isset($from_data) && !empty($from_data)) {
                $email = $from_data['data']['email'];
                $password = $from_data['data']['password'];
                $identity = new UserIdentity($email, $password);
                $identity->validate();
                Yii::app()->user->login($identity);
                unset(Yii::app()->session['back_btn']);
                Yii::app()->user->setFlash("success", $this->ServerMessage['subscribed_success']);
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/profile');
            }

        }else {
            Yii::app()->user->setFlash("error", $this->ServerMessage['error_transc_process']);
            $this->redirect(Yii::app()->getbaseUrl(true));
        }
        
    }
    
    public function actionsbwsavecardreturn() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $gateway_code = 'sbw';
        $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));

        $currency = Currency::model()->findByPk($usersub->currency_id);
        $card_data['currency_id'] = $currency->id;
        $card_data['currency_code'] = $currency->code;
        $card_data['currency_symbol'] = $currency->symbol;
        $card_data['email'] = Yii::app()->user->email;

        $data = $_REQUEST;
        $data['profile_id'] = $data['bill_id'];
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $resArray = $payment_gateway::customerDetails($data);
        
        
        if (isset($_REQUEST['result']) && trim($_REQUEST['result']) == 'success') {
            $cards = SdkCardInfos::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'is_cancelled' => 0));
            $is_primary = 0;
            if (isset($cards) && !empty($cards)) {
                $is_primary = 1;
            }
            $ip_address = Yii::app()->request->getUserHostAddress();

            $sciModel = New SdkCardInfos;
            $sciModel->studio_id = $studio_id;
            $sciModel->user_id = $user_id;
            $sciModel->gateway_id = $this->GATEWAY_ID[$gateway_code];
            $sciModel->card_uniq_id = Yii::app()->common->generateUniqNumber();
            $sciModel->card_holder_name = $data['first_name'] . ' ' . $data['last_name'];
            $sciModel->exp_month = '';
            $sciModel->exp_year = '';
            $sciModel->card_last_fourdigit = $resArray['customers'][0]['orders'][0]['bills'][0]['acct_num'];
            $sciModel->auth_num = $resArray['customers'][0]['orders'][0]['bills'][0]['route_num'];
            $sciModel->token = $resArray['customers'][0]['orders'][0]['bills'][0]['customer_id'];
            $sciModel->profile_id = $data['profile_id'];
            $sciModel->card_type = '';
            $sciModel->reference_no = @$data['reference_no'];
            $sciModel->response_text = json_encode($resArray['response_text']);
            $sciModel->status = $resArray['result'];
            $sciModel->is_cancelled = $is_primary;
            $sciModel->ip = $ip_address;
            $sciModel->created = new CDbExpression("NOW()");
            $sciModel->save();
            $sdk_card_info_id = $sciModel->id;

            //Check if partial or failed payment record exists for user only
            $usersub = UserSubscription::model()->findByAttributes(array('studio_id' => $studio_id, 'user_id' => $user_id, 'status' => 1));
            if (isset($usersub->status) && ($usersub->status == 1) && isset($usersub->payment_status) && ($usersub->payment_status != 0)) {
                if (abs($usersub->partial_failed_due) >= 0.01) {
                    //Prepare an array to charge from given card
                    $arg = array();
                    $currency = Currency::model()->findByPk($usersub->currency_id);
                    $arg['currency_id'] = $currency->id;
                    $arg['currency_code'] = $currency->code;
                    $arg['currency_symbol'] = $currency->symbol;

                    $arg['amount'] = $usersub->partial_failed_due;
                    $arg['token'] = $sciModel->token;
                    $arg['profile_id'] = $usersub->profile_id;
                    $arg['card_holder_name'] = $sciModel->card_holder_name;
                    $arg['card_last_fourdigit'] = $sciModel->card_last_fourdigit;
                    $arg['card_type'] = $sciModel->card_type;
                    $arg['exp_month'] = $sciModel->exp_month;
                    $arg['exp_year'] = $sciModel->exp_year;
                    $arg['email'] = Yii::app()->user->email;
                    $arg['user_id'] = Yii::app()->user->id;
                    $res = $this->chargeDueOnPartialOrFailedTransaction($usersub, $arg, $gateway_code);
                }
            }
            if ($sdk_card_info_id > 0) {
                Yii::app()->user->setFlash('success', $this->ServerMessage["credit_card_saved_success"]);
                $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
            } else {
                Yii::app()->user->setFlash('error', $this->Language['unable_to_process_try_another_card']);
                $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
            }
        } else {
            Yii::app()->user->setFlash('error', $this->Language['unable_to_process_try_another_card']);
            $this->redirect(Yii::app()->baseUrl . '/user/cardInformation');
        }
        
    }
    
    function actionGetCreditPlan() {
        self::isAjaxRequest();
        $purifier = new CHtmlPurifier();
        $_POST = $purifier->purify($_POST);

        $this->layout = false;
        $plan = '';
        $data = array();

        if (isset(Yii::app()->user->id) && Yii::app()->user->id > 0 && isset($_POST['movie_id']) && !empty($_POST['movie_id'])) {
            $studio_id = Yii::app()->user->studio_id;
            $user_id = Yii::app()->user->id;

            $data['movie_id'] = $movie_code = isset($_POST['movie_id']) ? $_POST['movie_id'] : '0';
            $data['stream_id'] = $stream_code = isset($_POST['stream_id']) ? $_POST['stream_id'] : '0';
            $season = isset($_POST['season']) ? $_POST['season'] : 0;

            $cond = '';
            $condArr = array(':uniq_id' => $movie_code, ':studio_id' => $studio_id);
            if ($stream_code) {
                $cond = " AND m.embed_id=:embed_id";
                $condArr[":embed_id"] = $stream_code;
            }
            if ($season) {
                $cond .= " AND m.series_number=:series_number";
                $condArr[":series_number"] = $season;
            }

            $command = Yii::app()->db->createCommand()
                    ->select('f.id, m.id as stream_id, f.uniq_id,f.name,f.content_type_id,f.ppv_plan_id,f.name,f.permalink,f.content_types_id')
                    ->from('films f, movie_streams m')
                    ->where('f.id=m.movie_id AND f.uniq_id=:uniq_id AND f.parent_id=0 AND f.studio_id=:studio_id ' . $cond, $condArr);
            $films = $command->queryRow();
            $movie_id = $films['id'];
            $stream_id = @$films['stream_id'];
            $content_types_id = $data['content_types_id'] = $films['content_types_id'];

            //check free content
            $free_data = Yii::app()->db->createCommand()
                        ->select('season_id, episode_id, movie_id')->from('free_contents')
                        ->where('studio_id=:studio_id AND movie_id=:movie_id', array(':studio_id'=>$studio_id,':movie_id'=>$movie_id))
                        ->queryAll();
            $new_array_show = array();
            $new_array_season = array();
            $new_array_episode = array();
            if(isset($free_data) && !empty($free_data)){
                for($i = 0; $i < count($free_data);$i++){
                    if($free_data[$i]['season_id'] == 0 && $free_data[$i]['episode_id'] == 0){
                        $new_array_show = $free_data[$i]['movie_id'];
                    }else if($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] == 0){
                        $new_array_season[] = $free_data[$i]['season_id'];
                    }else if($free_data[$i]['season_id'] != 0 && $free_data[$i]['episode_id'] != 0){
                       $new_array_episode[] = $free_data[$i]['episode_id']; 
                    }
                }
            }
            
            if (intval($stream_id)) {

                if (intval($content_types_id) == 3) {
                    $ppv_buy = PpvBuy::model()->findByAttributes(array('studio_id' => $studio_id));
                    $data['is_show'] = (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) ? 1 : 0;
                    $data['is_season'] = (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) ? 1 : 0;
                    $data['is_episode'] = (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) ? 1 : 0;

                    $EpDetails = $this->getEpisodeToPlay($movie_id);

                    if (isset($EpDetails) && !empty($EpDetails) && isset($EpDetails->total_series) && $EpDetails->total_series > 0) {
                        $series = explode(',', $EpDetails->series_number);
                        sort($series);

                        $data['max_season'] = $series[count($series) - 1];
                        $data['min_season'] = $series[0];

                        $data['series_number'] = intval($season) ? $season : $series[0];

                        $condEpi = array(':studio_id' => $studio_id, ':movie_id' => $movie_id, ':series_number' => $data['series_number']);
                        $episql = Yii::app()->db->createCommand()
                                ->select('m.*')
                                ->from('movie_streams m ')
                                ->where('m.studio_id=:studio_id AND m.is_episode=1 AND m.movie_id=:movie_id AND m.series_number=:series_number', $condEpi, array('order' => 'episode_number ASC'));
                        $episodes = $episql->queryAll();

                        if (isset($episodes) && !empty($episodes)) {
                            $data['episodes'] = $episodes;
                        }
                    }
                }

                $sql_mstream = Yii::app()->db->createCommand()
                        ->select('ms.id,ms.movie_id,ms.episode_number,ms.series_number,ms.episode_title')
                        ->from('movie_streams ms ')
                        ->where(' ms.id=:ms_id AND ms.movie_id=:movie_id', array(':ms_id' => $stream_id, ':movie_id' => $movie_id));
                $stream_data = $sql_mstream->queryAll();

                $content_name = "";
                $search_str = "";
                $con_type = "";
                if ($stream_data[0]['movie_id'] != "") {
                    $search_str .= $stream_data[0]['movie_id'];
                    $content_name .= $films['name'];
                }if ($stream_data[0]['series_number'] != 0 && intval($content_types_id) == 3) {
                    $search_str .= ":" . $stream_data[0]['series_number'];
                    $content_name .= "-Season-" . $stream_data[0]['series_number'];
                }if ($stream_data[0]['episode_number'] != "" && intval($content_types_id) == 3) {
                    if ($stream_data[0]['series_number'] == 0) {
                        $search_str .= ":0:" . $stream_data[0]['id'];
                    } else {
                        $search_str .= ":" . $stream_data[0]['id'];
                        $content_name .= "-Season-" . $stream_data[0]['episode_title'];
                    }
                }

                $content = "'" . $search_str . "'";
                $total_credit = Yii::app()->billing->getUserTotalCredit();
                $content_credit = Yii::app()->common->getContentInCredits($studio_id, $content,1);
            }
            Yii::app()->theme = 'bootstrap';
            $this->render('//user/credit', array('studio_id' => $studio_id,'free_data_show'=>$new_array_show, 'free_data_season'=>$new_array_season, 'free_data_episode'=>$new_array_episode,'user_id' => $user_id, 'content' => $search_str, 'cnt_name' => $content_name, 'films' => @$films, 'data' => @$data, 'season' => @$season,'total_credit'=>$total_credit,'content_credit' => $content_credit));
        }
    }
    
    public function actionCreditSubscription(){
        $studio_id = Yii::app()->user->studio_id;
        $user_id = Yii::app()->user->id;
        $user_credits = Yii::app()->billing->getUserCredits();
        $total_credit = Yii::app()->billing->getUserTotalCredit();
        $content_id_str = $content_id = $_POST['content_id'];
        $is_multi = substr_count($_POST['content_id'], ":");
        $content_type = 1;
        if ($is_multi >= 1) {
            $content_type = 3;
            $content_id_str = "'" . $_POST['content_id'] . "'";
        }
        $content_credit = Yii::app()->common->getContentInCredits($studio_id, $content_id_str,1);
        $user_has_credit = 0;
        if(isset($total_credit) && intval($total_credit)){
            if($content_credit['credit_value'] <= $total_credit){
                $user_has_credit = 1;
            }
        }
        if($user_has_credit){
            $content_credit_value =  $content_credit['credit_value'];
            $isadv = (isset($_POST['is_preorder_content'])) ? intval($_POST['is_preorder_content']) : 0;

            $start_date = Date('Y-m-d H:i:s');
            $end_date = '0000-00-00 00:00:00';

                if (intval($isadv) == 0) {
                    $limit_video_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'credit', 'limit_video');
                    $limit_video = (isset($limit_video_access->validity_period) && intval($limit_video_access->validity_period)) ? $limit_video_access->validity_period : 0;

                    $watch_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'credit', 'watch_period');
                    $watch_period = (isset($watch_period_access->validity_period) && intval($watch_period_access->validity_period)) ? $watch_period_access->validity_period . ' ' . strtolower($watch_period_access->validity_recurrence) . "s" : '';

                    $access_period_access = PpvValidity::model()->getPlaybackAccessForStudio($studio_id, 'credit', 'access_period');
                    $access_period = (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) ? $access_period_access->validity_period . ' ' . strtolower($access_period_access->validity_recurrence) . "s" : '';

                    $time = strtotime($start_date);
                    if (isset($access_period_access->validity_period) && intval($access_period_access->validity_period)) {
                        $end_date = date("Y-m-d H:i:s", strtotime("+{$access_period}", $time));
                    }

                    $data['view_restriction'] = $limit_video;
                    $data['watch_period'] = $watch_period;
                    $data['access_period'] = $access_period;
                }

                $data['credit_id'] = $content_credit['id'];
                $data['content_id'] = $content_id;
                $data['content_type'] = $content_type;
                $data['user_id'] = $user_id;
                $data['studio_id'] = $studio_id;
                $data['isadv'] = $isadv;
                $data['start_date'] = @$start_date;
                $data['end_date'] = @$end_date;
                $data['credit_value'] = $content_credit_value;
                $res = Yii::app()->billing->setCreditSubscription($data);
                $content_arr = explode(":", $content_id);
                $data['movie_id'] = $content_arr[0];
                $data['transaction_id'] = $res['transaction_id'];
                $resArr = Yii::app()->billing->redeemCredit($data, $user_credits, $content_credit_value);
                if ($isadv) {
                    $movie_name = CouponOnly::model()->getContentInfo($content_id);
                    $msgs = $this->ServerMessage['purchased'];
                    $msgs = htmlentities($msgs);
                    eval("\$msgs = \"$msgs\";");
                    $msgs = htmlspecialchars_decode($msgs);
                    Yii::app()->user->setFlash("success", $msgs);
                }

            if (isset($res['creditSubId']) && $res['creditSubId']) {
                $resArray['isSuccess'] = 1;
                echo json_encode($resArray);
                exit;
            } else {
                $resArray['isError'] = 1;
                echo json_encode($resArray);
                exit;
            }
        } else {
            $resArray['isError'] = 1;
            echo json_encode($resArray);
            exit;
        }
    }
    
    public function paynow($data) {
        $gateway_code = $data['gateway_code'];
        $data['have_paypal'] = $data['havePaypal'];
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $payment_gateway::processCard($data);
    }
    
    public function actionSubscriptionConfirm() {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $data = $from_data = Yii::app()->session['session_data'];
        $gateway_code = $from_data['gateway_code'];
        $user['token'] = $_REQUEST['token'];
        $user['gateway_code'] = $gateway_code;
        
        if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();
            $resArray = $payment_gateway::paymentTransaction($user);
        }
        
        $plan_subscriptionbundles = SubscriptionPlans::model()->findByPk($data['plandetailbundles_id']);
        if (isset($resArray) && !empty($resArray) &&  (strtoupper($resArray["ACK"]) == "SUCCESS" || strtoupper($resArray["ACK"]) == "SUCESSWITHWARNING")) {
            $user['payerid'] = $payer_id = $resArray['PAYERID'];
            $user['email'] = $payer_email = $resArray['EMAIL'];
            $first_name = @$resArray['SHIPTONAME'];
            $address1 = @$resArray['SHIPTOSTREET'];
            $city = @$resArray['SHIPTOCITY'];
            $state = @$resArray['SHIPTOSTATE'];
            $country = @$resArray['SHIPTOCOUNTRYCODE'];
            $zip = @$resArray['SHIPTOZIP'];
            $invoice_id = $resArray['CORRELATIONID'];
            $full_name = $resArray['FIRSTNAME'] . ' ' . $resArray['LASTNAME'];
        }
        $user['currency_id'] = $data['currency_id'];
        $user['currency_code'] = $data['currency_code'];
        $user['currency_symbol'] = $data['currency_symbol'];
        
        $used_trial = Yii::app()->common->usedTrial();
        
        $user['trialperiod'] = '';
        $user['trialfrequency'] = '';
        $user['trialtotalcysles'] = '';
        $user['trial_amount'] = '';
        
         if ($used_trial == 0) {
            $user['trialperiod'] = $plan_subscriptionbundles->trial_recurrence;
            $user['trialfrequency'] = $plan_subscriptionbundles->trial_period;
            $user['trialtotalcysles'] = $data['trialtotalcysles'];
            $user['trial_amount'] = $data['trial_amount'];
            
        }
        $user['plan_desc'] = $data['paymentDesc'];
        $user['charged_amount'] = $user['amount'] = $data['amount'];
        $user['recurrence'] = $data['recurrence'];
        $user['frequency'] = $data['frequency'];
        
        $start_date = Date('Y-m-d H:i:s');
        $trail_period = $user['trialfrequency'];
         if (isset($trail_period) && (intval($trail_period) == 0)) {
            $is_transaction = 1;
            $start_date = Date('Y-m-d H:i:s');
            $time = strtotime($start_date);
            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
            $start_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
            $end_date = Date('Y-m-d H:i:s', strtotime($start_date . "+{$recurrence_frequency}-1days"));
        } else {
            $trail_recurrence = 'day';
            if ($user['trialperiod'] != '') {
                $trail_recurrence = $user['trialperiod'];
            }
            $trial_period = $trail_period . ' ' . strtolower($trail_recurrence) . "s";
            $start_date = Date('Y-m-d H:i:s', strtotime("+{$trial_period}"));
            $time = strtotime($start_date);
            $recurrence_frequency = $plan_subscriptionbundles->frequency . ' ' . strtolower($plan_subscriptionbundles->recurrence) . "s";
            $end_date = date("Y-m-d H:i:s", strtotime("+{$recurrence_frequency}", $time));
        }
        
        
        $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY[$gateway_code] . 'Controller';
        Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
        $payment_gateway = new $payment_gateway_controller();
        $resArr = $payment_gateway::paymentTransactions($user);
        $ack = strtoupper($resArr["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCESSWITHWARNING") {
            $is_transaction = 1;
            $data['profile_id'] = $resArr['PROFILEID'];
            $data['save_this_card'] = 0;
            $Films = Film::model()->findByAttributes(array('studio_id' => $studio_id, 'uniq_id' => $data['movie_id']));
            $video_id = $Films->id;
            $trans_data['transaction_status'] = $resArr['ACK'];
            $trans_data['invoice_id'] = $invoice_id;
            $trans_data['order_number'] = $resArr['CORRELATIONID'];
            $trans_data['amount'] = $user['amount'];
            $trans_data['dollar_amount'] = $user['amount'];
            $trans_data['response_text'] = json_encode($resArr);
            $trans_data['PAYERID'] = $payer_id;
            $trans_data['SHIPTONAME'] = $full_name;
            $trans_data['SHIPTOSTREET'] = $address1;
            $trans_data['SHIPTOCITY'] = $city;
            $trans_data['SHIPTOSTATE'] = $state;
            $trans_data['COUNTRYCODE'] = $country;
            $trans_data['SHIPTOZIP'] = $zip;
            $isadv = $data['session_data']['isadv'];
            $trail_period = $data['trialperiod'];
            $VideoDetails = '';
            
            $VideoName = $plan_subscriptionbundles->name;
            $data['PAYMENT_GATEWAY_ID'] = $this->PAYMENT_GATEWAY_ID[$gateway_code];
            $set_user_subscription_bundles = Yii::app()->billing->setUserSubscriptionBundles($plan_subscriptionbundles, $data, $video_id, $start_date, $end_date, $data['coupon'], $isadv, $gateway_code, $trail_period);
            $ppv_subscription_id = $set_user_subscription_bundles;
//            if ($is_transaction) {
//                $transaction_id = Yii::app()->billing->setPpvTransaction($plan_subscriptionbundles, $data, $video_id, $trans_data, $ppv_subscription_id, $gateway_code, $isadv);
//            }
            $subscription_bundles_email = Yii::app()->billing->sendSubscriptionBundlesEmail($trans_data, $data, @$transaction_id, $isadv, $VideoName, $VideoDetails, $data['plandetailbundles_id']);
        }
        if ($data['is_subscription_bundle_checked'] == 1) {
            self::setRedirectPpv($Films, $plan_subscriptionbundles, $data, $isadv);
        } else {
            Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit;
        }
    }
	public function actionCancel() {
        if (isset($_GET['token']) && trim($_GET['token'])) {
            Yii::app()->user->setFlash('error', $this->ServerMessage['canceled_transaction']);
            $this->redirect(Yii::app()->session['backbtnlink']);
            exit;
        }
        //$this->render('cancel');
    }
	function actionCCavenueSubscription(){
        if(isset($_POST['encResp'])){
            $encResponse=$_POST["encResp"];	
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY['ccavenue'] . 'Controller';            
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();          
            $res = $payment_gateway::getdcryptresponse($encResponse);
            if($res['order_status'] == 'Success'){
                $log_data = PciLog::model()->findByAttributes(array('unique_id' => $res['order_id']));
                 $data = json_decode($log_data['log_data'], true);
                $plan_id = $data['plan_id'];
                $currency_id = $data['currency_id'];
                $plan_price = $data['amount'];
                $discount_price = $data['discount_amount'];
                $couponCode = $data['coupon_code'];
                $use_discount = @$data['use_discount'];
                $start_date = $data['start_date'];
                $end_date = $data['end_date'];               
                $signup_step = Yii::app()->general->signupSteps();
                if($signup_step == 1){
                    $name = trim($data['name']);
                    $email = trim($data['email']);
                    $password = @$data['password'];
                    $is_subscription_bundles = @$data['is_subscription_bundles'];
                    $ip_address =  $data['ip'];
                    $geo_data = Yii::app()->common->getVisitorLocation($ip_address);
                    $pass = '';
                    $enc = new bCrypt();
                    if ($password) {
                        $pass = $enc->hash($password);
                    }
                    $confirm_token = $enc->hash($email);
                    if (strpos($confirm_token, "/") !== FALSE) {
                        $confirm_token = str_replace('/', '', $confirm_token);
                    }
                
                    //Save SDK user detail
                    $user = new SdkUser;
                    $user->email = $email;
                    $user->studio_id = $studio_id;
                    $user->signup_ip = $ip_address;
                    $user->display_name = $name;
                    $user->nick_name = $name;
                    $user->mobile_number = @$data['mobile_number'];
                            if(@$data['livestream'] &&  $data['livestream']== 1){
                                    $user->is_broadcaster = 1;
                            }
                    $user->encrypted_password = $pass;
                    $user->source = $data['source'];
                    $user->status = 1;
                    $user->confirmation_token = $confirm_token;
                    $user->signup_location = serialize($geo_data);
                    $user->created_date = new CDbExpression("NOW()");
                    $user->user_language =  "";
                    if (@$data['data']['fb_access_token']) {
                        $user->fb_access_token = $data['data']['fb_access_token'];
                    }
                    if (@$data['data']['fb_userid']) {
                        $user->fb_userid = $data['data']['fb_userid'];
                    }
                    if (@$data['data']['gplus_userid']) {
                        $user->gplus_userid = $data['data']['gplus_userid'];
                    }
                    $return = $user->save();
                    $user_id = $user->id;
                    $data['user_id'] = $user_id;
                    $is_saved = Yii::app()->custom->saveCustomFieldValues($studio_id, $user_id);  
                    
                    $model = new LoginForm;
                    $model->attributes = $data;
                    if ($model->loginuser()) {
                        if (isset($user_id) && intval($user_id)) {
                            $usr = Yii::app()->common->getSDKUserInfo($user_id);
                            if ($usr->add_video_log) {
                                $login_at = date('Y-m-d H:i:s');
                                $login_history = new LoginHistory();
                                $login_history->studio_id = $studio_id;
                                $login_history->user_id = $user_id;
                                $login_history->login_at = $login_at;
                                $login_history->logout_at = '0000-00-00 00:00:00';
                                $login_history->ip = $ip_address;
                                $login_history->last_login_check = $login_at;
                                $login_history->save();
                                Yii::app()->session['login_history_id'] = $login_history->id;
                                $this->setLoginHistoryCookie($login_history->id);
                            }
                        }
                    } 
                       
                }
                    $trans['CustRef'] = $res['order_id'];
                    $trans['Amount'] = ($data['discount_amount']!= "")?$data['discount_amount']:$data['plan_price'];
                    $trans['TrnType'] = 1;  
                    $planModel = new SubscriptionPlans();
                    $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
                    Yii::app()->billing->addCreditToUser($plan,$studio_id,$user_id);
                    $is_renewal = 1;     
                    $payment_gateway_id = $this->PAYMENT_GATEWAY_ID['ccavenue'];
                    //Create user subscription for making transaction
                    $usModel = new UserSubscription;
                    $usModel->studio_id = $studio_id;
                    $usModel->user_id = $user_id;
                    $usModel->plan_id = $plan_id;
                    $usModel->studio_payment_gateway_id = $payment_gateway_id;
                    $usModel->currency_id = $currency_id;
                    $usModel->profile_id = @$res['si_ref_no']; //Si referrence number for check status
                    $usModel->amount = $data['plan_price'];
                    $usModel->discount_amount = @$discount_price;
                    $usModel->coupon_code = @$couponCode;
                    $usModel->use_discount = $use_discount;
                    $usModel->start_date = $start_date;
                    $usModel->end_date = $end_date;
                    $usModel->is_subscription_bundle = @$data['is_subscription_bundles'];
                    $usModel->status = 1;
                    $usModel->is_renewal = $is_renewal;
                    $usModel->ip = $ip_address;
                    $usModel->created_by = $user_id;
                    $usModel->created_date = new CDbExpression("NOW()");
                    $usModel->save();

                    $user_subscription_id = $usModel->id;

                    $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
                    if (isset($couponDetails) && !empty($couponDetails)) {
                        $command = Yii::app()->db->createCommand();
                        if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                            $qry = $command->update('coupon_subscription', array('used_by' => $couponDetails->used_by . "," . $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                        } else {
                            $qry = $command->update('coupon_subscription', array('used_by' => $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                        }
                    }
                //Save transaction data
                   $transaction = new Transaction;
                   $transaction->user_id = $user_id;
                   $transaction->studio_id = $studio_id;
                   $transaction->plan_id = $plan_id;
                   $transaction->currency_id = $currency_id;
                   $transaction->transaction_date = new CDbExpression("NOW()");
                   $transaction->payment_method = 'ccavenue';
                   $transaction->transaction_status = $res['order_status'];
                   $transaction->invoice_id = $res['order_id'];
                   $transaction->order_number = $res['order_id'];
                   $transaction->dollar_amount = $data['amount'];
                   $transaction->amount = $data['amount'];
                   $transaction->response_text = json_encode($res);
                   $transaction->subscription_id = $user_subscription_id;
                   $transaction->ip = $ip_address;
                   $transaction->created_date = new CDbExpression("NOW()");
                   $transaction->save();

                   $file_name = '';
                   //Generate pdf for instant payment when zero free trial

                   $studio = Studios::model()->findByPk($studio_id);
                   $arg['amount'] = Yii::app()->common->formatPrice($data['amount'], $currency_id, 1);
                   if (isset($couponCode) && $couponCode != "") {
                       $arg['discount_amount'] = Yii::app()->common->formatPrice($data['discount_amount'], $currency_id);
                       $arg['full_amount'] = Yii::app()->common->formatPrice($data['amount'], $currency_id);
                       $arg['coupon'] = $couponCode;
                   }
      

                   $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, '');


                   $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $lang_code);
                   $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
                   if ($isEmailToStudio) {
                       $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
                   }
                   Yii::app()->user->setFlash('success', $this->ServerMessage['suc_accnt_activate']);
                   $this->redirect(Yii::app()->getbaseUrl(true));
                   //$admin_welcome_email = $this->sendStudioAdminEmails($studio_id, 'welcome_email_with_subscription', $user_id, '', 0);

            }else{
                $signup_step = Yii::app()->general->signupSteps();
                if($signup_step == 1){
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register');
                }else if($signup_step == 2){
                    $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
                }                
            }

       }else{
           $signup_step = Yii::app()->general->signupSteps();
            if($signup_step == 1){
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/register');
            }else if($signup_step == 2){
                $this->redirect(Yii::app()->getbaseUrl(true) . '/user/activate');
            }
       }
    } 
     public function setLoginHistoryCookie($history_id) {
        $user_id = Yii::app()->user->id;
        $studio_id = Yii::app()->common->getStudiosId();
        $domain = Yii::app()->getBaseUrl(true);
        $domain = explode("//", $domain);
        $domain = $domain[1];
        $login_history_cookie_name = "login_history_id_" . $studio_id . "_" . $user_id;
        if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
            setcookie($login_history_cookie_name, $history_id, time() + (60 * 60 * 24 * 30), '/', $domain, isset($_SERVER["HTTPS"]), TRUE);
        }
    } 
    public function actionCCavenueSubscriptionBundle(){
        if(isset($_POST['encResp'])){
            $encResponse=$_POST["encResp"];	
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $payment_gateway_controller = 'Api' . $this->PAYMENT_GATEWAY['ccavenue'] . 'Controller';            
            Yii::import('application.controllers.wrapper.' . $payment_gateway_controller);
            $payment_gateway = new $payment_gateway_controller();          
            $res = $payment_gateway::getdcryptresponse($encResponse);
            if($res['order_status'] == 'Success'){
                $log_data = PciLog::model()->findByAttributes(array('unique_id' => $res['order_id']));
                $data = json_decode($log_data['log_data'], true);
                $Films = Film::model()->findByAttributes(array('studio_id'=>$studio_id, 'uniq_id'=>$data['movie_id']));
                $video_id = $Films->id;                     
                $plan_id = $data['plan_id'];
                $currency_id = $data['currency_id'];
                $plan_price = $data['amount'];
                $discount_price = $data['discount_amount'];
                $couponCode = $data['coupon_code'];
                $use_discount = @$data['use_discount'];
                $start_date = $data['start_date'];
                $end_date = $data['end_date'];
                $trans['CustRef'] = $res['order_id'];
                $trans['Amount'] = ($data['discount_amount']!= "")?$data['discount_amount']:$data['plan_price'];
                $trans['TrnType'] = 1;  
                $planModel = new SubscriptionPlans();
                $plan = $planModel->find('id=:plan_id AND studio_id=:studio_id', array(':plan_id' => $plan_id, ':studio_id' => $studio_id));
                Yii::app()->billing->addCreditToUser($plan,$studio_id,$user_id);
                $is_renewal = 1;     
                $payment_gateway_id = $this->PAYMENT_GATEWAY_ID['ccavenue'];
                $exist_subscription = Transaction::model()->findByAttributes(array('order_number' => $res['order_id']));
                if(empty($exist_subscription)){
                //Create user subscription for making transaction
                $usModel = new UserSubscription;
                $usModel->studio_id = $studio_id;
                $usModel->user_id = $user_id;
                $usModel->plan_id = $plan_id;
                $usModel->studio_payment_gateway_id = $payment_gateway_id;
                $usModel->currency_id = $currency_id;
                $usModel->profile_id = @$res['si_ref_no']; //Si referrence number for check status
                $usModel->amount = $data['plan_price'];
                $usModel->discount_amount = @$discount_price;
                $usModel->coupon_code = @$couponCode;
                $usModel->use_discount = $use_discount;
                $usModel->start_date = $start_date;
                $usModel->end_date = $end_date;
                $usModel->status = 1;
                $usModel->device_type = 1;
                $usModel->is_renewal = $is_renewal;
                $usModel->is_subscription_bundle = $data['is_subscriptionbundle'];
                $usModel->ip = $ip_address;
                $usModel->created_by = $user_id;
                $usModel->created_date = new CDbExpression("NOW()");
                $usModel->save();

                $user_subscription_id = $usModel->id;

                $couponDetails = CouponSubscription::model()->findByAttributes(array('coupon_code' => @$couponCode));
                if (isset($couponDetails) && !empty($couponDetails)) {
                    $command = Yii::app()->db->createCommand();
                    if ($couponDetails->coupon_type == 1 && $couponDetails->used_by != 0) {
                        $qry = $command->update('coupon_subscription', array('used_by' => $couponDetails->used_by . "," . $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                    } else {
                        $qry = $command->update('coupon_subscription', array('used_by' => $user_id, 'used_date' => new CDbExpression('NOW()')), 'coupon_code=:coupon_code', array(':coupon_code' => $couponCode));
                    }
                }
            //Save transaction data
               $transaction = new Transaction;
               $transaction->user_id = $user_id;
               $transaction->studio_id = $studio_id;
               $transaction->plan_id = $plan_id;
               $transaction->currency_id = $currency_id;
               $transaction->transaction_date = new CDbExpression("NOW()");
               $transaction->payment_method = 'ccavenue';
               $transaction->transaction_status = $res['order_status'];
               $transaction->invoice_id = $res['order_id'];
               $transaction->order_number = $res['order_id'];
               $transaction->dollar_amount = $data['amount'];
               $transaction->amount = $data['amount'];
               $transaction->bill_amount = @$res['amount'];
               
               $transaction->fullname = @$res['billing_name'];
               $transaction->address1 = @$res['billing_address'];
               $transaction->city = @$res['billing_city'];
               $transaction->state = @$res['billing_state'];
               $transaction->phone = @$res['billing_tel'];
               $transaction->country = @$res['billing_country'];
               $transaction->zip = @$res['billing_zip'];
               
               $transaction->response_text = json_encode($res);
               $transaction->subscription_id = $user_subscription_id;
               $transaction->ip = $ip_address;
               $transaction->movie_id = $video_id;
               $transaction->created_date = new CDbExpression("NOW()");
               $transaction->save();

               $file_name = '';
               //Generate pdf for instant payment when zero free trial

               $studio = Studios::model()->findByPk($studio_id);
               $arg['amount'] = Yii::app()->common->formatPrice($data['amount'], $currency_id, 1);
               if (isset($couponCode) && $couponCode != "") {
                   $arg['discount_amount'] = Yii::app()->common->formatPrice($data['discount_amount'], $currency_id);
                   $arg['full_amount'] = Yii::app()->common->formatPrice($data['amount'], $currency_id);
                   $arg['coupon'] = $couponCode;
               }


               $file_name = Yii::app()->pdf->invoiceDetial($studio, $arg, '');


               $email = Yii::app()->common->getStudioEmails($studio_id, $user_id, 'membership_subscription', $file_name, '', '', '', '', '', $lang_code);
               $isEmailToStudio = NotificationSetting::model()->isEmailNotificationSettingsToStudio('membership_subscription', $studio_id);
               if ($isEmailToStudio) {
                   $this->sendStudioAdminEmails($studio_id, 'admin_new_paying_customer', $user_id, '', 0);
               }

               }
               
               self::setRedirectPpv($Films, $plan, $data, $data['is_advance_purchase']);              

            }else{
                 $this->redirect(Yii::app()->getbaseUrl(true)); 
            }
        }else{
            $this->redirect(Yii::app()->getbaseUrl(true));     
        }
    }   
}
