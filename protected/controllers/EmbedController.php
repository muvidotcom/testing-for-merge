<?php
require 's3bucket/aws-autoloader.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/Mobile_Detect.php';
use Aws\CloudFront\CloudFrontClient;
class EmbedController extends Controller {
    public function actionIndex() {
        $this->layout = false;
        $mov_can_see = 'allowed';
        if ($mov_can_see == "unsubscribed") {
            $this->redirect("/user/process");
        } else if ($mov_can_see == 'unpaid') {
            Yii::app()->user->setFlash('error', 'This is a PPV movie. <a href="javascript:void(0);" id="watch_now">Click here</a> to watch this movie now.');
            $red = Yii::app()->common->getMoviePermalink($_REQUEST['movie']);
            $this->redirect($red);
        } else if ($mov_can_see == 'allowed') {
            $data = Yii::app()->db->createCommand()
                    ->select('f.id,f.name,f.permalink,f.created_date,f.studio_id,ms.id AS stream_id,ms.wiki_data,ms.full_movie,ms.thirdparty_url,f.content_types_id,f.meta_description,ms.episode_title,ms.episode_number,ms.series_number,ms.episode_date,ms.episode_story,ms.is_episode')
                    ->from('films f ,movie_streams ms ')
                    ->where('f.id = ms.movie_id AND embed_id=:embed_id', array(':embed_id' => $_REQUEST['embed_id']))
                    ->queryAll();
            //echo  "<pre>";print_r($data);exit;
            if ($data) {
                if (Yii::app()->common->isGeoBlockContent(@$data[0]['id'], @$data[0]['stream_id'],@$data[0]['studio_id'])) {
                    Yii::app()->theme = 'admin';
                    $studio_id = $data[0]['studio_id'];
                    $movie_id = $data[0]['id'];
                    $stream_id = $data[0]['stream_id'];
                    $page_title=!empty($data[0]['episode_title'])?$data[0]['episode_title']:$data[0]['name'];
                    $page_desc =!empty($data[0]['episode_story'])?$data[0]['episode_story']:$data[0]['meta_description'];
                       /*
                        * functionality : Device Platform Restriction iOS || Android || 
                        * author : aravind@muvi.com || Date : 15-09-2017
                        */
                     $this->restrict_platforms_embed(@$page_title,@$data);
                            
                    //embeded url restriction part will not work for share
                    if(!isset($_REQUEST['isShare'])){
                        $og_video=false;
                    if ((!isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] != 1) || ($_REQUEST['isApp'] != 2))) {
                        $getEmbedUrlRestriction = StudioConfig::model()->getconfigvalueForStudio($data[0]['studio_id'], 'embed_url_restriction');
                        if (@$getEmbedUrlRestriction['config_value'] == 1) {
                            $websiteUrl = $_SERVER['HTTP_REFERER'];
                            $websiteUrl = str_replace("http://", "", $websiteUrl);
                            $websiteUrl = str_replace("https://", "", $websiteUrl);
                            $websiteUrl = str_replace("www.", "", $websiteUrl);
                            $websiteUrl = explode("/", $websiteUrl);
                            if (@$websiteUrl[0]) {
                                $embedUrlRestriction = EmbedUrlRestriction::model()->findAllByAttributes(array('studio_id' => $data[0]['studio_id']));
                                if ($embedUrlRestriction) {
                                    $embedWebUrl = array();
                                    foreach ($embedUrlRestriction as $embedUrlRestrictionkey => $embedUrlRestrictionvalue) {
                                        $embedWebUrl[] = $embedUrlRestrictionvalue['website_url'];
                                    }
                                    if (in_array(@$websiteUrl[0], $embedWebUrl)) {

                                    } else {
                                        echo '"Access denied" you are not authorized to access this content';
                                        exit;
                                    }
                                } else {
                                    echo '"Access denied" you are not authorized to access this content';
                                    exit;
                                }
                            } else {
                                echo '"Access denied" you are not authorized to access this content';
                                exit;
                            }
                        }
                    }
                    }else{
                        $og_video=true;
                        //Check the shared url is free or not
                        $data_enable = Yii::app()->general->monetizationMenuSetting($data['0']['studio_id']);
                        $arg=array();
                        $arg['studio_id']  = $data['0']['studio_id'];
                        $arg['movie_id']   = $data['0']['id'];
                        $arg['season_id']  = $data['0']['series_number'];
                        $arg['episode_id'] = $data['0']['stream_id'];
                        $arg['content_types_id'] = $data['0']['content_types_id'];                    
                        $isFreeContent = Yii::app()->common->isFreeContent($arg);                                             
                       if($isFreeContent!=1 || !isset($data_enable) || !($data_enable['menu'] & 8)){                       
                            $this->render('404');                        
                            exit; 
                        }
                    }
                    if ($data[0]['thirdparty_url'] != '') {
                        $info = pathinfo($data[0]['thirdparty_url']);
                        if ($info["extension"] == "m3u8") {
                            if ($data[0]['content_types_id'] == 3) {
                                $item_poster = $this->getPoster($data[0]['stream_id'], 'moviestream', 'original', $data[0]['studio_id']);
                            } else {
                                $item_poster = $this->getPoster($data[0]['id'], 'films', 'original', $data[0]['studio_id']);
                            }
                            $this->render('play_m3u8', array('thirdparty_url' => $data[0]['thirdparty_url'], 'item_poster' => $item_poster,'page_title' => $page_title,'page_desc' => $page_desc,'og_video' =>false));
                        } else {
                            echo $data[0]['thirdparty_url'];
                        }
                        $ip_address = CHttpRequest::getUserHostAddress();
                        if (Yii::app()->aws->isIpAllowed()) {
                                $video_log = new VideoLogs();                
                                $video_log->created_date = new CDbExpression("NOW()");
                                $video_log->ip = $ip_address;
                                $video_log->video_length = 0;
                                $video_log->movie_id = $movie_id;
                                $video_log->video_id = $stream_id;
                                $video_log->user_id = 0;
                                $video_log->studio_id = $studio_id;
                                $video_log->played_from = 2;
                                $video_log->watch_status = 'start';
                                $video_log->save();
                        }
                        exit;
                    } else {
                        $v_logo = Yii::app()->general->getPlayerLogo($data[0]['studio_id']);
                        $can_see = true;
                        define('S3BUCKET_ID', $studio->s3bucket_id);
                        $bucketInfo = Yii::app()->common->getBucketInfo("", $data[0]['studio_id']);
                        $movie_stream = new movieStreams();
                        $movie_strm = $movie_stream->findByPk($data['0']['stream_id']);
                        $drmMobileDisable = @$movie_strm->is_mobile_drm_disable;
                        if (@$movie_strm->content_key && @$movie_strm->encryption_key && ($drmMobileDisable == 0 || (Yii::app()->common->isMobile() == 0 && $drmMobileDisable == 1))) {
                            $folderPath = Yii::app()->common->getFolderPath("", $data[0]['studio_id']);
                            $signedBucketPath = $folderPath['signedFolderPath'];
                            $fullmovie_path = 'https://' . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/stream.mpd';
                            $fullmovie_path_hls = 'https://' . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $movie_strm->id . '/hls/master.m3u8';
                            if (HOST_IP != '127.0.0.1' || HOST_IP != '52.0.64.95') {
                                $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($data[0]['studio_id'], 'drm_cloudfront_url');
                                if ($getStudioConfig) {
                                    if (@$getStudioConfig['config_value'] != '') {
                                        $fullmovie_path = 'https://' . $getStudioConfig['config_value'] . '/uploads/movie_stream/full_movie/' . $movie_strm->id . '/stream.mpd';
                                        $fullmovie_path_hls = 'https://' . $getStudioConfig['config_value'] . '/uploads/movie_stream/full_movie/' . $movie_strm->id . '/hls/master.m3u8';
                                    }
                                }
                            }
                            $tokens = Yii::app()->aws->generateExplayToken($movie_strm->content_key, $movie_strm->encryption_key);
                        } else {
                            $fullmovie_path = $this->getFullVideoPath($data['0']['stream_id'], 0, $data[0]['studio_id']);
                            $share_fullmovie_path="";                        
                            //for getting mp4 filepath for social share
                            if(isset($_REQUEST['isShare']) && $_REQUEST['isShare']==true){
                                $studio_id=$data['0']['studio_id'];
                                $bucketInfo = Yii::app()->common->getBucketInfo("", $studio_id);                       
                                $bucketName = $bucketInfo['bucket_name'];
                                $s3url = $bucketInfo['s3url'];
                                $newQuery="Select new_cdn_users FROM studios WHERE id=".$studio_id;
                                $getCdnUserData=Yii::app()->db->createCommand($newQuery)->queryRow();                            
                                $folderPath = Yii::app()->common->getFolderPath($getCdnUserData['new_cdn_users'], $studio_id);                        
                                $signedBucketPath = $folderPath['signedFolderPath'];
                                $bucketHttpUrl = 'https://' . $bucketName . '.' . $s3url . '/' . $signedBucketPath . 'uploads/movie_stream/full_movie/' . $data['0']['stream_id'] . '/';
                                $share_fullmovie_path = $bucketHttpUrl . $data['0']['full_movie'];
                            }                        
                           // $fullmovie_path = 'https://d2gx0xinochgze.cloudfront.net/707/EncodedVideo/uploads/movie_stream/full_movie/8969/1920_LONDON__OFFICIAL_THEATRICAL_TRAILER__06_May_2016__HD__720p_.mp4';                        
                            $multipleVideo = array();
                            $defaultResolution = 144;
                            $multipleVideo = $this->getvideoResolution($movie_strm->video_resolution, $fullmovie_path);
                            if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)) {
                                $userintSpeed = 10;
                                if (isset($_REQUEST['user_internet_speed']) && ($_REQUEST['user_internet_speed'] != 0)) {
                                    $userintSpeed = $_REQUEST['user_internet_speed'];
                                }
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $userintSpeed);
                            } else if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)) {
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path, $_REQUEST['user_internet_speed']);
                            } else {
                                $videoToBeplayed = $this->getVideoToBePlayed($multipleVideo, $fullmovie_path);
                            }
                            $videoToBeplayed12 = explode(",", $videoToBeplayed);
                            if ((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))) {
                                $fullmovie_path = $videoToBeplayed12[0];
                                $defaultResolution = $videoToBeplayed12[1];
                            } else {
                                $fullmovie_path = $multipleVideo[144];
                                $defaultResolution = 144;
                            }
                            $wiki_data = $data[0]['wiki_data'];

                            $internetSpeedImage = CDN_HTTP . $bucketInfo['bucket_name'] . '.' . $bucketInfo['s3url'] . '/check-download-speed.jpg';
                            $clfntUrl = CDN_HTTP . $bucketInfo['cloudfront_url'];
                            $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $fullmovie_path));
                            if ((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))) {
                                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 0, "", 6000000, $data[0]['studio_id']);
                            } else {
                                $fullmovie_path = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 0, "", 0, $data[0]['studio_id']);
                            }
                        }
                        $studio_language = $this->studio_language($data[0]['studio_id']);
                        $this->enable_laguages = $studio_language['studio_enable_languages'];
                        $this->all_languages = $studio_language['studio_languages'];
                        $this->language_code = $studio_language['language_code'];
                        foreach ($studio_language['studio_languages'] as $key => $value) {
                            if ($this->language_code == $value['code']) {
                                $this->language_id = $value['languageid'];
                            }
                        }
                        $this->default_language_code = $studio_language['studio_languages'][0]['default_language'];
                        $studio = Studio::model()->getStudioBucketId($data[0]['studio_id']);
                        $theme = $studio['theme'];
                        $origin_lang = include(ROOT_DIR . 'languages/studio/en.php');
                        if (file_exists(ROOT_DIR . "languages/" . $theme . "/" . $this->language_code . ".php")) {
                            $lang = include( ROOT_DIR . "languages/" . $theme . "/" . $this->language_code . ".php");
                        } elseif (file_exists(ROOT_DIR . 'languages/studio/' . $this->language_code . '.php')) {
                            $lang = include(ROOT_DIR . 'languages/studio/' . $this->language_code . '.php');
                        } else {
                            $lang = include(ROOT_DIR . 'languages/studio/en.php');
                        }
                        $lang = $this->getFullLangTranslation($lang);
                        $translateLanguage = $lang['all'];

                        $subTitlesql = "select vs.id as subId, vs.filename,vs.display_name as subtitle_name,ls.* from video_subtitle vs,language_subtitle ls where vs.language_subtitle_id = ls.id and vs.video_gallery_id=" . $movie_strm->video_management_id . " and vs.studio_id = " . $data[0]['studio_id'] . " ORDER BY ( ls.name != 'English' ), ls.name ASC";
                        $subTitleData = Yii::app()->db->createCommand($subTitlesql)->queryAll();
                        $subtitleFiles = array();
                        if ($subTitleData) {
                            $i = 1;
                            foreach ($subTitleData as $subTitleDatakey => $subTitleDatavalue) {
                                $clfntUrl = HTTP . $bucketInfo['cloudfront_url'];
                                $filePth = $clfntUrl . "/" . $bucketInfo['signedFolderPath'] . "subtitle/" . $subTitleDatavalue['subId'] . "/" . $subTitleDatavalue['filename'];
                                $rel_path = str_replace(" ", "%20", str_replace($clfntUrl . "/", "", $filePth));
                                $subtitleFiles[$i]['url'] = $this->getnew_secure_urlForEmbeded($rel_path, $clfntUrl, 1, "", 60000);
                                $subtitleFiles[$i]['code'] = $subTitleDatavalue['code'];
								if ($subTitleDatavalue['subtitle_name'] != '') {
									$subtitileName = $subTitleDatavalue['subtitle_name'];
								} else {
									$subtitileName = $subTitleDatavalue['name'];
								}
                                if (@$translateLanguage[$subTitleDatavalue['name']]!=''){
                                    $subtitleFiles[$i]['language'] = $translateLanguage[$subtitileName];  
                                } else {
                                    $subtitleFiles[$i]['language'] = $subtitileName;
                                }
                                $i ++;
                            }
                        }
                        if ($data[0]['content_types_id'] == 3) {
                            $item_poster = $this->getPoster($data[0]['stream_id'], 'moviestream', 'original', $data[0]['studio_id']);
                        } else {
                            $item_poster = $this->getPoster($data[0]['id'], 'films', 'original', $data[0]['studio_id']);
                        }
                        $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($data[0]['studio_id'], 'embed_watermark');
                        $embedWaterMArk = 0;
                        if ($getStudioConfig) {
                            if (@$getStudioConfig['config_value'] == 1) {
                                $embedWaterMArk = 1;
                            }
                        }
                        $waterMarkOnPlayer = Yii::app()->general->playerWatermark($data[0]['studio_id'], @$_REQUEST['watermark_value']);
                        $per_link = @$data[0]['permalink'];
                        $video_log_data = array();
                        $video_log_data['movie_id'] = $data[0]['id'];
                        $video_log_data['stream_id'] = $data[0]['stream_id'];
                        $user_id = 0;
                        $durationPlayed = array();
                        if (@$_REQUEST['watermark_value'] != "") {
                            $userData = Yii::app()->db->createCommand()
                                    ->select('id')
                                    ->from('sdk_users')
                                    ->where('email="' . $_REQUEST['watermark_value'] . '" and studio_id=' . $data[0]['studio_id'])
                                    ->queryAll();
                            if (@$userData[0]['id']) {
                                $user_id = $userData[0]['id'];
                                $durationPlayed = VideoLogs::model()->getvideoDurationPlayed($data[0]['studio_id'], $user_id, $data[0]['id'], $data[0]['stream_id']);
                                if (!empty($durationPlayed)) {
                                        }
                                    }
                                    }

                        $studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = ' . $data[0]['studio_id']));                   
                        $MonetizationMenuSettings = MonetizationMenuSettings::model()->getStudioAdsWithMonitizationDetails($studio_id);
                        $movie_strm_midroll_val ="";
                        //spotx activated
                        $bit_pre_roll = false;$bit_post_roll = false;  
                        if((strlen($movie_strm->roll_after) > 0) && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0]['menu'] & 4 )){
                                $roll_after = explode(',',$movie_strm->roll_after);
                                //check post-roll
                                if (in_array("end", $roll_after)) {
                                    $bit_post_roll = "end";
                                    array_pop($roll_after);
                                }
                                //check pre-roll
                                if (in_array("0", $roll_after)) {
                                    $bit_pre_roll = "start";
                                    $roll_after = array_reverse($roll_after);
                                    array_pop($roll_after);
                                    $roll_after = array_reverse($roll_after);
                                }
                                unset($movie_strm->roll_after);
                                //convert ads streaming time to seconds
                                $time_seconds ="";$i =0;
                                $midRollvalues = array();
                                while($i<count($roll_after)){
                                    $midRollvalues[] = Yii::app()->general->getHHMMSSToseconds($roll_after[$i++]);
                                    }
                                $movie_strm->roll_after = implode(",",$midRollvalues);
                                    }
                        //echo $movie_strm->roll_after;exit;
                        if (@$movie_strm->content_key && @$movie_strm->encryption_key && ($drmMobileDisable == 0 || (Yii::app()->common->isMobile() == 0 && $drmMobileDisable == 1))) {
                            if(Yii::app()->common->isMobile() == 1){
                                $this->render('store_restrict_embed_pad', array('studio_id'=>$studio_id,'page_title' => $movie[0]['name'],'device_content_type'=>'drm','ispad'=>Yii::app()->common->isMobile())); 
                                exit;  
                            }else{ 
                                $this->render('mpeg_dash', array('subtitleFiles' => $subtitleFiles,'page_title' => $page_title,'page_desc' => $page_desc,'embedWaterMArk' => $embedWaterMArk, 'defaultResolution' => $defaultResolution, 'internetSpeedImage' => $internetSpeedImage, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'videoLoaderImage' => $videoLoaderImage, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio' => $studio, 'embed_id' => $_REQUEST['embed_id'], 'studio_id' => $data[0]['studio_id'], 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'Authtoken' => $tokens,'og_video' =>false,'fullmovie_path_hls' => $fullmovie_path_hls, 'translateLanguage' => $translateLanguage));
                                exit;
                            } 
                        } 
                        //for SPOTX Integration   playAds_video 
                        else if($movie_strm->enable_ad == 1 && $MonetizationMenuSettings[1]['ad_network_id'] == 1 && ($MonetizationMenuSettings[0]['menu'] & 4 )){                            
                            //$this->render('embed_playAds_video', array('subtitleFiles' => $subtitleFiles,'page_title' => $page_title,'page_desc' => $page_desc,'embedWaterMArk' => $embedWaterMArk, 'defaultResolution' => $defaultResolution, 'internetSpeedImage' => $internetSpeedImage, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'videoLoaderImage' => $videoLoaderImage, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio' => $studio, 'embed_id' => $_REQUEST['embed_id'], 'studio_id' => $data[0]['studio_id'], 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'translateLanguage' => $translateLanguage, 'user_id' => $user_id, 'video_log_data' => $video_log_data,'studio_ad' => $studio_ads,'share_fullmovie_path' => $share_fullmovie_path,'og_video' =>$og_video,'bit_pre_roll'=>$bit_pre_roll,'bit_post_roll'=>$bit_post_roll,'mvstream' => $movie_strm,'MonetizationMenuSettings'=>$MonetizationMenuSettings));
                            $this->render('index_spotx', array('subtitleFiles' => $subtitleFiles,'page_title' => $page_title,'page_desc' => $page_desc,'embedWaterMArk' => $embedWaterMArk, 'defaultResolution' => $defaultResolution, 'internetSpeedImage' => $internetSpeedImage, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'videoLoaderImage' => $videoLoaderImage, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio' => $studio, 'embed_id' => $_REQUEST['embed_id'], 'studio_id' => $data[0]['studio_id'], 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'translateLanguage' => $translateLanguage, 'user_id' => $user_id, 'video_log_data' => $video_log_data,'studio_ad' => $studio_ads,'share_fullmovie_path' => $share_fullmovie_path,'og_video' =>$og_video,'bit_pre_roll'=>$bit_pre_roll,'bit_post_roll'=>$bit_post_roll,'mvstream' => $movie_strm,'MonetizationMenuSettings'=>$MonetizationMenuSettings));
                            exit;
                        }
                        //for DFP ads
                        else if($movie_strm->enable_ad == 1 && $MonetizationMenuSettings[1]['ad_network_id'] == 3 && ($MonetizationMenuSettings[0]['menu'] & 4 )){
                            $this->render('index_dfp', array('mvstream' => $movie_strm,'subtitleFiles' => $subtitleFiles,'page_title' => $page_title,'page_desc' => $page_desc,'embedWaterMArk' => $embedWaterMArk, 'defaultResolution' => $defaultResolution, 'internetSpeedImage' => $internetSpeedImage, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'videoLoaderImage' => $videoLoaderImage, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio' => $studio, 'embed_id' => $_REQUEST['embed_id'], 'studio_id' => $data[0]['studio_id'], 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'translateLanguage' => $translateLanguage, 'user_id' => $user_id, 'video_log_data' => $video_log_data,'studio_ad' => $studio_ads,'share_fullmovie_path' => $share_fullmovie_path,'og_video' =>$og_video,'MonetizationMenuSettings'=>$MonetizationMenuSettings));
                            exit;
                        }
                        elseif($data[0]['content_types_id'] == '5' || $data[0]['content_types_id'] == '6' || $data[0]['content_types_id'] == '8'){
                            $this->render('audio_embed_play', array('studio_id' => $data[0]['studio_id'],'movie_id'=>$data[0]['id'], 'stream_id'=>$data[0]['stream_id'], 'content_type'=>$data[0]['content_types_id']));
                            exit;
                        }
                        else{
                            $this->render('index', array('mvstream' => $movie_strm,'subtitleFiles' => $subtitleFiles,'page_title' => $page_title,'page_desc' => $page_desc,'embedWaterMArk' => $embedWaterMArk, 'defaultResolution' => $defaultResolution, 'internetSpeedImage' => $internetSpeedImage, 'multipleVideo' => $multipleVideo, 'wiki_data' => $wiki_data, 'can_see' => $mov_can_see, 'fullmovie_path' => $fullmovie_path, 'v_logo' => $v_logo, 'videoLoaderImage' => $videoLoaderImage, 'is_restrict' => @$is_restrict, 'movieData' => @$data[0], 'per_link' => $per_link, 'item_poster' => $item_poster, 'studio' => $studio, 'embed_id' => $_REQUEST['embed_id'], 'studio_id' => $data[0]['studio_id'], 'waterMarkOnPlayer' => $waterMarkOnPlayer, 'durationPlayed' => $durationPlayed, 'translateLanguage' => $translateLanguage, 'user_id' => $user_id, 'video_log_data' => $video_log_data,'studio_ad' => $studio_ads,'share_fullmovie_path' => $share_fullmovie_path,'og_video' =>$og_video,'MonetizationMenuSettings'=>$MonetizationMenuSettings));
                            exit;
                        }
                    }
                }else{
                    $content = '<div style="height:250px;text-align:center; color:red;"><h3>This content is not available to stream in your country</h3></div>';
                    $this->render('//layouts/geoblock_error',array('content'=>$content));
                    exit;
                }
            }
        }

        $this->render('404');
    }

    public function actionlivestream() {
        $this->layout = FALSE;

        $this->pageTitle = 'Live Streaming';
        $this->pageDescription = 'Watch live streaming';

        $studio_id = Yii::app()->common->getStudioId();
        $uniq_id = $_REQUEST['uniq_id'];
        $sql = "SELECT m.id,m.studio_id,m.name,m.permalink,m.studio_id, ms.id as stream_id FROM films m, movie_streams ms  WHERE m.id=ms.movie_id and m.uniq_id='" . $uniq_id . "'";
        $movie = Yii::app()->db->createCommand($sql)->queryAll();
        if($movie){
            if (Yii::app()->common->isGeoBlockContent(@$movie[0]['id'], 0,@$movie[0]['studio_id'])) {
                $liveStream = Livestream::model()->find('movie_id=:movie_id AND studio_id=:studio_id', array(':movie_id' => $movie[0]['id'], ':studio_id' => $movie[0]['studio_id']));
                if ($liveStream) {
                    Yii::app()->theme = 'admin';
                    /*
                    * functionality : Device Platform Restriction iOS || Android || 
                    * author : aravind@muvi.com || Date : 15-09-2017
                    */
                    $this->restrict_platforms_embed(@$page_title,@$movie);
                            
                    $item_poster = $this->getPoster($movie[0]['id'], 'films', 'original', $movie[0]['studio_id']);
                    $getStudioConfig = StudioConfig::model()->getconfigvalueForStudio($liveStream->studio_id, 'embed_watermark');
                    $embedWaterMArk = 0;
                    if ($getStudioConfig) {
                        if (@$getStudioConfig['config_value'] == 1) {
                            $embedWaterMArk = 1;
                        }
                    }
                    $waterMarkOnPlayer = Yii::app()->general->playerWatermark($liveStream->studio_id, @$_REQUEST['watermark_value']);
                    if ($liveStream->feed_type == 2) {
                        $streamUrlExplode  = explode("/",str_replace("rtmp://","",$liveStream->stream_url));
                        $nginxServerIp = @$streamUrlExplode[0];
                        $nginxServerDetails = Yii::app()->aws->getNginxServerDetails($nginxServerIp); 
                        $nginxHttpAddr = @$nginxServerDetails['NGINX_IP_HTTP'];
                        if (strpos($liveStream->feed_url, 'rtmp://'.$nginxServerIp.'/live') !== false) {
                            $feedWithIp = str_replace( "rtmp://".$nginxServerIp, $nginxHttpAddr, $liveStream->feed_url) . "/index.m3u8";
                            $liveStream->feed_url = str_replace("rtmp://".$nginxServerIp."/live", @$nginxServerDetails['nginxserverlivecloudfronturl'], $liveStream->feed_url) . "/index.m3u8";
                            $this->renderPartial('livefeed_streaming_app', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster, 'embedWaterMArk' => $embedWaterMArk, 'waterMarkOnPlayer' => $waterMarkOnPlayer,'feedWithIp' => $feedWithIp,"feed_method" =>$liveStream->feed_method));
                        } else if (strpos($liveStream->feed_url, 'rtmp://'.$nginxServerIp.'/record') !== false) {
                            $feedWithIp = str_replace( "rtmp://".$nginxServerIp, $nginxHttpAddr, $liveStream->feed_url) . "/index.m3u8";
                            $liveStream->feed_url = str_replace("rtmp://".$nginxServerIp."/record", @$nginxServerDetails['nginxserverrecordcloudfronturl'], $liveStream->feed_url) . "/index.m3u8";
                            $this->renderPartial('livefeed_streaming_app', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster, 'embedWaterMArk' => $embedWaterMArk, 'waterMarkOnPlayer' => $waterMarkOnPlayer,'feedWithIp' => $feedWithIp,"feed_method" =>$liveStream->feed_method));
                        } else {
                            $this->renderPartial('live_rtmpstreaming', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster));
                        }
                    } else {
                        //Added by prakash on 8th dec 2016
                        if (isset($_REQUEST['isApp']) && ($_REQUEST['isApp'] == '1' || $_REQUEST['isApp'] == '2')) {
                            $this->renderPartial('livefeed_streaming_app', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster, 'embedWaterMArk' => $embedWaterMArk, 'waterMarkOnPlayer' => $waterMarkOnPlayer));
                        } else {
                            //Check hls feed url is http or https.For http use flash otherwise contrib-hls					
                            if (strtolower(parse_url($liveStream->feed_url, PHP_URL_SCHEME)) == 'https' || $liveStream->studio_id == 4185) {
                                $this->renderPartial('livefeed_streaming_app', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster));
                            } else {
                                $this->renderPartial('livefeed_streaming', array('livestream' => $liveStream, 'movie' => @$movie[0], 'item_poster' => $item_poster));
                            }
                        }
                    }
                } else {
                    $this->render('404');
                    Yii::app()->user->setFlash('error', 'Oops! Sorry no feeds available for streaming.');
                    $this->redirect(Yii::app()->getBaseUrl(TRUE));
                    exit;
                }
            }else{
                    $content = '<div style="height:250px;text-align:center; color:red;"><h3>This content is not available to stream in your country</h3></div>';
                    $this->render('//layouts/geoblock_error',array('content'=>$content));
                    exit;
            }
        } else {
            $this->render('404');
            Yii::app()->user->setFlash('error', 'Oops! Sorry no feeds available for streaming.');
            $this->redirect(Yii::app()->getBaseUrl(TRUE));
            exit;
        }
    }
    
    //restrict_PAD_functionality :: 10-17-2017
    function restrict_platforms_embed($page_title,$data){
        $detect = new Mobile_Detect;
        //iOS || Android restriction
        if(@$data[0]['studio_id']){
            $studio_getres = 'SELECT config_key,config_value FROM `studio_config` WHERE `studio_id` = '.@$data[0]['studio_id'].' and (config_key ="restrict_platform_ios" or config_key="restrict_platform_android")';
            $studio_config_dbres = Yii::app()->db->createCommand($studio_getres)->queryAll();
            if(count($studio_config_dbres)>0){
                foreach($studio_config_dbres as $resdata){
                    switch (strtolower($resdata['config_key'])){
                        case 'restrict_platform_ios':
                            if(count($resdata)>1){
                                $restrict_platform_ios = $resdata['config_value'];
                                if($restrict_platform_Android==0)
                                    $restrict_platform_Android = 0;
                        }
                        break;
                        case 'restrict_platform_android':
                            if(count($resdata)>1){
                                $restrict_platform_Android = $resdata['config_value'];
                                if($restrict_platform_ios==0)
                                    $restrict_platform_ios = 0;
                            }
                        break;
                        default :
                            $restrict_platform_Android =0;
                            $restrict_platform_ios =0;
                        break;
                    }
                }
            }else{
                //If store has no restrictions
                $restrict_platform_Android =0;
                $restrict_platform_ios =0;
            }
            Yii::app()->theme = 'admin';
            //echo $restrict_platform_Android."===========".$restrict_platform_ios;exit;
            if(count($detect)>0){
               if($detect->is('iOS') && $restrict_platform_ios == 1){
                   $this->render('store_restrict_platforms_embed', array("studio_id"=>$data[0]['studio_id'],'page_title' => $page_title,'device_os'=>'iOS','restrict_platform_Android'=>$restrict_platform_Android,'restrict_platform_ios'=>$restrict_platform_ios)); 
                } else if($detect->is('AndroidOS') && $restrict_platform_Android == 1){
                   $this->render('store_restrict_platforms_embed', array("studio_id"=>$data[0]['studio_id'],'page_title' => $page_title,'device_os'=>'AndroidOS','restrict_platform_Android'=>$restrict_platform_Android,'restrict_platform_ios'=>$restrict_platform_ios)); 
               }
            }
        }        
    }
}
