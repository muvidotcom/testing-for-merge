<?php
class CustomFormController extends Controller {
	 protected function beforeAction($action) {
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin';
		Yii::app()->layout = 'admin';
        return true;
    }
	
/**
 * @method public createCustomFrom() It will create the custom form based on the form type
 * @author Gayadhar<support@muvi.com>
 * @param int $studio_id 
 * @param int $form_type 
 * @param array $arg array of data
 * @return HTML HTML Form
 */	
	function actionCreateCustomForm($studio_id='',$form_type=1,$arg=array()){
		if(!$studio_id){
			$studio_id = Yii::app()->common->getStudioId();
		}
		if(@$_REQUEST['form_type']){
			$form_type = $_REQUEST['form_type'];
		}
		$customComp = new CustomForms();
		$data = $customComp->getCustomMetadata($studio_id,$form_type,$arg);
		if($data){
			
		}else{
			
		}
	}
/**
* @method public getContentForm() Generate a add episode form based on the content types id
* @author GDR<support@muvi.com>
* @return html Returns the HTML form of add episode
*/
    function actionGetContentForm() {
        $contentTypeId = $_REQUEST['contentTypes_id'];
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        if (@$_REQUEST['contentTypesId']) {
			$form_type = $_REQUEST['contentTypesId'];
		}
        $issubCat_enabled = Yii::app()->custom->getCatImgOption($studio_id,'subcategory_enabled');
        $viewfile = in_array($form_type, array(7, 8)) ? 'audio_content' : 'basic_content';
        $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($form_type);
        $arg['arg']['editid'] = @$_REQUEST['custom_form_id'];       
		$customComp = new CustomForms();
        $customData = $customComp->getCustomMetadata($studio_id, $arg['parent_content_type_id'], $arg['arg']);
		$IsDownloadable = Yii::app()->general->IsDownloadable($studio_id);
        $output = $this->renderPartial('//admin/' . $viewfile, array('content_type_id' => $_REQUEST['contnet_type_id'], 'content_types_id' => $_REQUEST['content_types_id'], 'contentCategories' => $contentCategories, 'customData' => @$customData,'issubCat_enabled'=>$issubCat_enabled,'IsDownloadable'=>$IsDownloadable), true);
        echo $output;exit;
    }
/**
* @method public getEpisodeForm() Generate a add episode form based on the content types id
* @author GDR<support@muvi.com>
* @return html Returns the HTML form of add episode
*/
    function actionGetEpisodeForm() {
        $contentTypeId = $_REQUEST['contentType_id'];
        if (@$_REQUEST['contentTypesId'] == 9) {
            $content_types_id = 6;
            $form_type = 9;
            $viewfile = 'audio_episode';
        } else {
            $content_types_id = 3;
            $form_type = 4;
            $viewfile = 'episode';
        }
        $movies = Film::model()->findAll('content_types_id =:content_types_id AND studio_id =:studio_id', array(':content_types_id' => $content_types_id, ':studio_id' => Yii::app()->user->studio_id));
        if ($movies) {
            $shows = CHtml::listData($movies, 'id', 'name');
			$customComp = new CustomForms();
            $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($form_type);
            $arg['arg']['editid'] = @$_REQUEST['custom_form_id'];  
            $customData = $customComp->getCustomMetadata(Yii::app()->user->studio_id, $arg['parent_content_type_id'], $arg['arg']); //4 is for multipart child
			$IsDownloadable = Yii::app()->general->IsDownloadable(Yii::app()->user->studio_id);
            $output = $this->renderPartial('//admin/' . $viewfile, array('shows' => $shows, 'customData' => @$customData,'IsDownloadable'=>$IsDownloadable), true);
            echo $output;exit;
        } else {
				echo 'error';exit;
        }
    }
 /**
 * @method public getLiveStreamForm() Generate a add episode form based on the content types id
 * @author GDR<support@muvi.com>
 * @return html Returns the HTML form of add episode
 */
    function actionGetLiveStreamForm() {
		$contentTypeId = $_REQUEST['contentTypesId'];
        if (@$contentTypeId == 10) {
            $content_types_id = 8;
            $form_type = 10;
            $viewfile = 'audio_channel_form';
        } else {
            $content_types_id = 4;
            $form_type = 5;
            $viewfile = 'channel_form';
        }
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        $customComp = new CustomForms();
        $arg = Yii::app()->general->getArrayFrommetadata_form_type_id($form_type);
        $customData = $customComp->getCustomMetadata($studio_id,$arg['parent_content_type_id'], $arg['arg']);
		$issubCat_enabled = Yii::app()->custom->getCatImgOption($studio_id,'subcategory_enabled');
        $output = $this->renderPartial('//admin/'.$viewfile, array('contentCategories' => $contentCategories,'customData'=>@$customData,'issubCat_enabled'=>$issubCat_enabled), true);
        echo $output;
        exit;
    }
 /**
 * @method public getSubCategory() Get the list of subcategory based on the category binary value
 * @author GDR<support@muvi.com>
 * @return html Returns the HTML form of add episode
 */
	function actionGetSubCategory(){
		if(@$_REQUEST['category_value']){
            $category_value = implode(',',$_REQUEST['category_value']);
            //if(count($_REQUEST['category_value'])>1){
                $cond = " category_value IN(:category_value)";
            /*}else{
				$cond = " category_value & :category_value";
            }*/
            //$subCatData = ContentSubcategory::model()->findAll('studio_id=:studio_id AND '.$cond,array(':studio_id'=>Yii::app()->user->studio_id,':category_value'=>$category_value));
			foreach($_REQUEST['category_value'] as $k=>$v){
				$subCatData1[] = Yii::app()->db->createCommand("SELECT id,subcat_name FROM content_subcategory WHERE studio_id=".Yii::app()->user->studio_id." AND FIND_IN_SET ('".$v."', category_value)")->setFetchMode(PDO::FETCH_OBJ)->queryAll();
			}
			foreach ($subCatData1 as $key => $value) {
				foreach ($value as $k1 => $v1) {
					$subCatData[$v1->id]=$v1;
				}
			}
			$output = '<select  name="movie[content_subcategory_value][]" id="content_subcategory_value" multiple  class="form-control input-sm checkInput">
					<option value="">-Select-</option>';
			if($subCatData){
				foreach ($subCatData as $key => $value) {
					$output .='<option value="'.$value->id.'">'.$value->subcat_name.'</option>';
				}
			}
			$output .='</select>';
			echo $output;exit;
		}
	}
	/* mapping with metadata_form_type and content_types
     * parameter content_types id
     * return custom_form_id
     */

    function getFormType($form_type) {
        $arr = array(1 => 1, 2 => 2, 3 => 3, 5 => 7, 6 => 8);
        return $arr[$form_type];
    }
    
    public function actionGetPosterDimention(){
        $flag = 0;        
        if(@$_POST['custom_form_id'] && is_numeric($_POST['custom_form_id'])){
            $posters  = CustomMetadataForm::model()->find(array('select'=>'poster_size','condition'=>'id=:id','params'=>array(':id'=>$_POST['custom_form_id'])));
            $dmn = $posters['poster_size'];            
            if($dmn){
                $flag = 1;
                $expl = explode('x', strtolower($dmn));            
                $poster['vwidth'] = @$expl[0];
                $poster['vheight'] = @$expl[1];
                $poster['hwidth'] = @$expl[0];
                $poster['hheight'] = @$expl[1];
            }else{
                $flag = 0;
			}
        }
        if($flag == 0){
            $poster_sizes = $this->poster_sizes;
            $horizontal = $poster_sizes['horizontal'];
            $vertical = $poster_sizes['vertical'];
            $poster['vwidth'] = $vertical['width'];
            $poster['vheight'] = $vertical['height'];
            $poster['hwidth'] = $horizontal['width'];
            $poster['hheight'] = $horizontal['height'];
            if(Yii::app()->general->getStoreLink()){
                $pg = Yii::app()->common->getPgDimension();
                $poster['pwidth'] = $pg['width'];
                $poster['pheight'] = $pg['height'];
        }        
        }        
        echo json_encode($poster);exit;
    }
    public function actionGetPhysicalForm(){
        $studio_id = Yii::app()->user->studio_id;
        $language_id = $this->language_id;
        $sql = "SELECT id,category_name FROM content_category WHERE studio_id={$studio_id} AND parent_id=0";
        $contentCategory = Yii::app()->db->createCommand($sql)->queryAll();
        $contentCategories = CHtml::listData($contentCategory, 'id', 'category_name');
        $customComp = new CustomForms();
        $arg = Yii::app()->general->getArrayFrommetadata_form_type_id(6);
		$arg['arg']['editid'] = @$_REQUEST['custom_form_id'];
        $customData = $customComp->getCustomMetadata($studio_id,$arg['parent_content_type_id'], $arg['arg']);//6 is for physical
        $pg['currency_code'] = PGProduct::getStudioCurrencyCode($this->studio);
        $settings = Yii::app()->general->getPgSettings();
        $pg['enable_category'] = $settings['enable_category'];
        $sizes = ShippingSize::model()->findAll("studio_id=:studio_id", array(':studio_id' => $studio_id));
        $pg['shipping_size'] = CHtml::listData($sizes, 'size_unique_name', 'size');
		$productize = Yii::app()->general->checkProductizeExtension($studio_id);
        $output = $this->renderPartial('//admin/physical_content', array('contentCategories' => $contentCategories,'customData'=>@$customData,'pg'=>$pg,'productize'=>$productize), true);
        echo $output;
        exit;
    }
}

