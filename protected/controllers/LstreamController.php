<?php
class LstreamController extends Controller{
	public $defaultAction = 'manageChannel';
	public $headerinfo='';
    public $layout = 'admin';
    protected function beforeAction($action){
        parent::beforeAction($action);
        $redirect_url = Yii::app()->getBaseUrl(true);
        Yii::app()->theme = 'admin'; 
        if(!(Yii::app()->user->id) || (!isset(Yii::app()->user->is_sdk))) {
            $this->redirect(array('/index.php/'));
        }else{
             $this->checkPermission();
        }
        return true;
    } 
/**
 * @method public manageChannel() All the contnent will be managed over here 
 * @author GDR<support@muvi.com>
 * @return html List of Contents
 */
	 public function actionmanageChannel(){
		$cond =  " ";$contentType=''; 
		$studio_id = Yii::app()->user->studio_id;
		$params = array(':studio_id'=> $studio_id);
		$pdo_cond_arr =array(":studio_id"=>Yii::app()->user->studio_id,':content_types_id'=>4);
		$pdo_cond = " f.studio_id=:studio_id AND c.content_types_id=:content_types_id";
		$con = Yii::app()->db;
		if(isset($_REQUEST['searchForm']) && $_REQUEST['searchForm']){
			$search_form = $_REQUEST['searchForm'];
			if(isset($search_form) && $search_form['update_date'] == '' && $search_form['content_type_id'] == '' && $search_form['search_text'] == ''){
				$url=$this->createUrl('lstream/manageChannel');
				$this->redirect($url);
			}else{
				$searchData = $_REQUEST['searchForm'];
				if($searchData['update_date']){
					$sdate = json_decode(stripslashes($searchData['update_date']),TRUE);
					$pdo_cond .= " AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') >= :start_dt AND STR_TO_DATE(ms.last_updated_date,'%Y-%m-%d') <= :end_dt ";
					$pdo_cond_arr[':start_dt'] = $sdate['start'];
					$pdo_cond_arr[':end_dt'] = $sdate['end'];
				}
				if($searchData['search_text']){
					$pdo_cond .= " AND ((LOWER(f.name) LIKE :search_text) OR (LOWER(ms.episode_title) LIKE :search_text)) ";
					$pdo_cond_arr[':search_text'] = strtolower(trim($searchData['search_text']))."%";
				}
				if($searchData['content_type_id']){
					$content_type = explode('-',$searchData['content_type_id']);
					if(count($content_type)>1){
						$pdo_cond .= " AND f.content_type_id = :content_type_id AND ms.is_episode = :is_episode ";
						$pdo_cond_arr[':content_type_id'] = $content_type[0];
						$pdo_cond_arr[':is_episode'] = 1;

					}else{
						$pdo_cond .= " AND f.content_type_id = :content_type_id AND ms.is_episode = :is_episode ";
						$pdo_cond_arr[':content_type_id'] = $searchData['content_type_id'];
						$pdo_cond_arr[':is_episode'] = 0;
					}
				}
			}
		}
        $this->pageTitle="Muvi | Channel List";
        $search_text ='';
		$contenttypeList = StudioContentType::model()->findAll(
				array(
					'select'=>'display_name,id,content_types_id',
					'condition' => 'studio_id=:studio_id AND is_enabled=:is_enabled AND content_types_id=:content_types_id',
					'params' =>array(':studio_id'=> $studio_id,':is_enabled'=>1,':content_types_id'=>4)
				));
		$contentTypes = CHtml::listData( $contenttypeList, 'id' , 'display_name','content_types_id');
		$order = ' ms.last_update_date DESC ';
		$orderType = 'desc';
		$sort = 'update_date';
		if(isset($_REQUEST['sort'])){
			if($_REQUEST['sort'] =='update_date' && $_REQUEST['sort_type']=='asc'){
				$order = ' ms.last_update_date ASC ';
				$orderType = 'asc';
			}
		}
       
		//Pagination Implimented ..
		$page_size =20;
		$offset = 0;
		if(isset($_REQUEST['page'])){
			$offset = ($_REQUEST['page']-1)*$page_size;
		}
		
		$this->breadcrumbs=array('Content List');
		
		//PDO Query
		$command = Yii::app()->db->createCommand()
		->select('SQL_CALC_FOUND_ROWS (0),f.id,f.name,ms.is_converted,ms.last_updated_date as update_date,f.uniq_id,f.permalink,f.created_date,ms.id AS stream_id,f.content_types_id, ms.episode_title,ms.embed_id, ms.enable_ad, ms.rolltype, ms.roll_after,l.id as livestream_id, l.feed_method,.l.feed_type,l.feed_url')
		->from('films f ,movie_streams ms,livestream l')
		->where('f.id = ms.movie_id AND f.id = l.movie_id AND '.$pdo_cond, $pdo_cond_arr)
		->order('ms.last_updated_date DESC')		
		->limit($page_size,$offset);
		
		$data = $command->queryAll();
			
		$count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
		
			
		$pages=new CPagination($count);
		$pages->setPageSize($page_size);

		// simulate the effect of LIMIT in a sql query
		$end =($pages->offset+$pages->limit <= $count ? $pages->offset+$pages->limit : $count);
		$sample =range($pages->offset+1, $end);
                
		if($data){
			$view=array(); $cnt=1;$movieids='';$stream_ids ='';
			foreach($data AS $k=>$v){
				if($v['is_episode']){
					$stream_ids .="'".$v['stream_id']."',";
				}else{
					$movieids .="'".$v['id']."',";
				}
			}
			$viewcount = array(); 
			$movieids = rtrim($movieids,',');
			$stream_ids = rtrim($stream_ids,',');
			
			//Get Posters for the Movies 
			if($movieids){
				$psql = "SELECT id,poster_file_name,object_id AS movie_id FROM posters WHERE object_id  IN(".$movieids.") AND object_type='films' ";
				$posterData = $con->createCommand($psql)->queryAll();
				if($posterData){
					foreach($posterData AS $key=>$val){
						$posters[$val['movie_id']] = $val;
					}
				}
			}
		}     
		//$studio_ads = StudioAds::model()->findAll(array('condition' => 'studio_id = '.Yii::app()->user->studio_id)); 
		$this->render('managechannel',array('data'=>$data,'content_type_id'=>@$searchData['content_type_id'],'dateRange'=>@$searchData['update_date'],'searchText'=>@$searchData['search_text'],'posters'=>@$posters,'item_count'=>@$count,'page_size'=>$page_size,'pages'=>$pages,'sample'=>@$sample,'contentList'=>@$contentTypes,'sort'=>@$sort,'orderType'=>@$orderType));	
    }
/**
 * @method public addChannel() add/edit the chanels for live streaming
 * @return HTML 
 * @author GDR<support@muvi.com>
 */		
	function actionAddChannel(){
		if(isset($_REQUEST['movie']) && $_REQUEST['movie']){
			$arr['succ']=0;
			$Films = new Film();
			$data = $_REQUEST['movie'];
			$movie = $Films->addContent($data);
			$movie_id = $movie['id'];
			$arr['uniq_id'] = $movie['uniq_id'];
			//Remove sample data from website
			$studio = $this->studio;
			$studio->show_sample_data = 0;
			$studio->save();   
			//Insert Into Movie streams table
			$MovieStreams = new movieStreams();
			$MovieStreams->studio_id = Yii::app()->user->studio_id;
			$MovieStreams->movie_id = $movie_id;
			$MovieStreams->is_episode = 0;
			$MovieStreams->created_date = gmdate('Y-m-d H:i:s');
			$MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
			$embed_uniqid = Yii::app()->common->generateUniqNumber();
			$MovieStreams->embed_id = $embed_uniqid; 
			$publish_date = NULL;
			/*if(@$_REQUEST['content_publish_date']){
				if(@$_REQUEST['publish_date']){
					$pdate = explode('/', $_REQUEST['publish_date']);
					$publish_date = $pdate[2].'-'.$pdate[0].'-'.$pdate[1];
					if(@$_REQUEST['publish_time']){
						$publish_date .= ' '.$_REQUEST['publish_time'];
					}
				}
			}
			$MovieStreams->content_publish_date = $publish_date;*/
			$MovieStreams->save();

			//Check and Save new Tags into database
			$movieTags = new MovieTag();
			$movieTags->addTags($data);
			if(isset($_REQUEST['content_filter_type'])){
				$contentFilterCls = new ContentFilter();
				$addContentFilter = $contentFilterCls->addContentFilter($_REQUEST,$data['content_type_id']);
			}
			//First Crop the image with specified size
			if(isset($_FILES['Filedata']) && !$_FILES['Filedata']['error']){
				$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/posters/'.$movie_id;
				$path = Yii::app()->common->jcropImage($_FILES['Filedata'],$dir,$_REQUEST['jcrop']);
				$cropDimension = array('episode'=>'280x156','thumb'=>'185x256','standard'=>'280x400');
				$ret = $this->uploadPoster($_FILES['Filedata'],$movie_id,'films',$cropDimension,$path);
				if($movie_id){
					Yii::app()->common->rrmdir($dir);
				}
				$arr['poster'] = $ret['original'];
			}
			//Adding the feed infos into live Stream table
			$livestream = new Livestream();
			$livestream->saveFeeds($_REQUEST,Yii::app()->user->studio_id,$movie_id);
			if(HOST_IP !='127.0.0.1'){
				require_once( $_SERVER["DOCUMENT_ROOT"].'/SolrPhpClient/Apache/Solr/Service.php' );
				$solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );                                             
				$part = new Apache_Solr_Document();
				$part -> id = Yii::app()->common->generateUniqNumber();
				$part -> content_id = $movie_id;
				$part -> stream_id = $MovieStreams->id;
				$part -> stream_uniq_id = Yii::app()->common->generateUniqNumber();;
				$part -> is_episode = 0;
				$part -> name = $movie['name'];
				$part -> title = $movie['permalink'];
				$ContentTypesDeteails = StudioContentType::model()->findByPk($movie['content_type_id']);
				$part -> cat = 'Content';
				$part -> sku = Yii::app()->user->studio_id;
				$part -> features = $movie['permalink'];
				$return = $solr->addDocument( $part );
				$ret1 = $solr->commit();
				$ret2= $solr->optimize();
			}  
			
			Yii::app()->user->setFlash('success','Wow! Channel added successfully.');
			$this->redirect($this->createAbsoluteUrl('lstream/manageChannel'));exit;
		}else{
			$studio_id = Yii::app()->user->studio_id;
			$this->headerinfo = 'Add Channel';
			$this->breadcrumbs = array('Add Channel');
			$sql = "SELECT s.id,s.content_types_id,s.display_name  FROM studio_content_types s WHERE  s.studio_id=".$studio_id." AND s.is_enabled=1 AND s.content_types_id=4 GROUP BY s.id";
			$dbcon = Yii::app()->db;
			$contenttypeList = $dbcon->createCommand($sql)->queryAll();
			$contentTypes= array();
			foreach($contenttypeList AS $key=>$val){
				$contentTypes[$val['id']]['content_types_id'] = $val['content_types_id']; 
				$contentTypes[$val['id']]['display_name'] = $val['display_name']; 
			}
			$this->render('addchannel',array('contentList'=>@$contentTypes));
		}
	}
/**
 *@method public editChannel() Edit Movie Details for sdk users
 * @param int $movie_id Movie id to be edited 
 * @return HTML 
 * @author GDR<info@muvi.in>
 */
	function actionEditChannel(){
        $this->pageTitle = Yii::app()->name .' | ' . 'Edit Channel';
		$this->breadcrumbs=array('Channel List'=>array('lstream/manageChannel'), 'Edit Channel Info',);
		if(isset($_REQUEST['movie_id']) && $_REQUEST['movie_id']){
			$dbcon = Yii::app()->db;
			//PDO Query
			$pdo_cond = 'm.id=:stream_id AND m.studio_id=:studio_id';
			$pdo_cond_arr = array(':stream_id'=>$_REQUEST['livestream_id'],':studio_id'=>Yii::app()->user->studio_id);
			$command = Yii::app()->db->createCommand()
			->select('f.id,m.id as livestream_id,f.uniq_id,f.name,f.language,f.censor_rating,f.genre,f.story,f.release_date,f.content_type_id,m.feed_method,m.feed_type,m.feed_url,c.content_types_id')
			->from('films f ,studio_content_types c,livestream m ')
			->where('f.id = m.movie_id AND f.content_type_id=c.id AND '.$pdo_cond, $pdo_cond_arr);
			$list = $command->queryAll();
			if($list){
				$contenttypeList = StudioContentType::model()->findAll(
				array(
					'select'=>'display_name,id,content_types_id',
					'condition' => 'studio_id=:studio_id AND is_enabled=:is_enabled AND content_types_id=:content_types_id',
					'params' =>array(':studio_id'=>Yii::app()->user->studio_id,':is_enabled'=>1,':content_types_id'=>$list[0]['content_types_id'])
				));
				$contentTypes= array();
				foreach($contenttypeList AS $key=>$val){
					$ctype = $val->attributes;
					$contentTypes[$ctype['id']]['display_name'] = $ctype['display_name']; 
					$contentTypes[$ctype['id']]['content_types_id'] = $ctype['content_types_id']; 
				}
				//echo "<pre>";print_r($contentTypes);print_r($list);exit;
				$this->headerinfo='Edit Channel - '.$list[0]['name'];
				$this->render('addchannel',array('data'=> $list[0],'contentList'=>@$contentTypes)); 		 
			}else{
				Yii::app()->user->setFlash('error','Oops! You don\'t have access to the channel');
				$this->redirect($this->createUrl('lstream/manageChannel'));
			}	
		}else{
			Yii::app()->user->setFlash('error','Oops! You don\'t have access to the channel');
			$url=$this->createUrl('lstream/manageChannel');
			$this->redirect($url);
		}
	}
/**
* @method public updateChannelDetails() It will update a Channel details
* @return HTML 
* @author Gayadhar<support@muvi.com>
*/
	function actionUpdateChannel(){
		if($_REQUEST['movie_id'] && $_REQUEST['uniq_id']){
			$Films = Film::model()->findByAttributes(array('id'=>$_REQUEST['movie_id'],'uniq_id'=>$_REQUEST['uniq_id']));
			if($Films){
				$data = $_REQUEST['movie'];
				$ctype = explode('-', $data['content_type']);
				$Films->name = stripslashes($data['name']);
				if(isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
					$Films->release_date = date('Y-m-d',strtotime($data['release_date']));
				else
					$Films->release_date = null;
				$Films->content_type_id = $ctype[0];				
				$Films->genre = json_encode(array_values(array_unique($data['genre'])));
				$Films->story = stripslashes($data['story']);
				$Films->save();

				//Check and Save new Tags into database
				$movieTags = new MovieTag();
				$movieTags->addTags($data);
				$movie_id = $Films->id;
				if(isset($_REQUEST['content_filter_type'])){
					$contentFilterCls = new ContentFilter();
					$addContentFilter = $contentFilterCls->addContentFilter($_REQUEST,$Films->content_type_id);
				}
				// Update Livestream Feeds data
				$liveFeeds['feed_method'] = $_REQUEST['method_type'];
				$liveFeeds['feed_url'] = $_REQUEST['feed_url'];
				$liveFeeds['feed_type'] = $_REQUEST['feed_type'];
				Livestream::model()->updateAll($liveFeeds,'id='.$_REQUEST['livestream_id'].' AND studio_id='.Yii::app()->user->studio_id);
				if(isset($_FILES['Filedata']) && !($_FILES['Filedata']['error'])){
					$dir = $_SERVER['DOCUMENT_ROOT'].'/'.SUB_FOLDER.'/images/public/system/posters/'.$movie_id;
					$path = Yii::app()->common->jcropImage($_FILES['Filedata'],$dir,$_REQUEST['jcrop']);
					$cropDimension = array('episode'=>'280x156','thumb'=>'185x256','standard'=>'280x400');
					$ret = $this->uploadPoster($_FILES['Filedata'],$movie_id,'films',$cropDimension,$path);
					Yii::app()->common->rrmdir($dir);
				}
				Yii::app()->user->setFlash('success','Wow! Your channel updated successfully');
				$this->redirect($this->createUrl('lstream/managechannel'));exit;
			}else{
				Yii::app()->user->setFlash('error','Oops! Sorry error in updating your data');
				$this->redirect($this->createUrl('lstream/managechannel'));exit;
			}	
		}else{
			Yii::app()->user->setFlash('error','Oops! Sorry you are not authorised to access this data');
			$this->redirect($this->createUrl('lstream/managechannel'));exit;
		}
	}
/**
 * @method public removeChannel() Remove complete channel details
 * @author GDR<support@muvi.com>
 * @return Json 
 */	
	function actionRemoveChannel(){
		$movie_id = $_REQUEST['movie_id'];
		
		$Films = Film::model()->with('movie_streams')->findByAttributes(array('id'=>$_REQUEST['movie_id'],'uniq_id'=>$_REQUEST['uniq_id']));
		$stream_id = $Films->movie_streams[0]->id;
		if($Films){
			//Remove Poster 
			$this->removePosters($movie_id,'films');
			//Remove Data from films table 
			Film::model()->deleteByPk($movie_id);
			//Remove Featured Content 
			FeaturedContent::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id',array(':movie_id'=>$movie_id,':studio_id'=>Yii::app()->user->studio_id));
			movieStreams::model()->deleteByPk($stream_id);
			Livestream::model()->deleteAll('movie_id =:movie_id AND studio_id =:studio_id',array(':movie_id'=>$movie_id,':studio_id'=>Yii::app()->user->studio_id));
			if(HOST_IP !='127.0.0.1'){
				require_once( $_SERVER["DOCUMENT_ROOT"].'/SolrPhpClient/Apache/Solr/Service.php' );
				$solr = new Apache_Solr_Service( 'localhost', '8983', '/solr' );
				$solr->deleteByQuery('content_id:'.$movie_id." AND stream_id:".$stream_id);
				$solr->commit(); //commit to see the deletes and the document
				$solr->optimize();
			}
			Yii::app()->user->setFlash('success',"Channel removed successfully.");
			$this->redirect($this->createUrl('lstream/manageChannel'));
		}else{
			Yii::app()->user->setFlash('error',"Oops! Sorry error in deleting content.");
			$this->redirect($this->createUrl('lstream/manageChannel'));
		}
	}
}