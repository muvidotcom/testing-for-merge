<?php

require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;

class MrssController extends Controller {

    public $defaultAction = 'index';
    public $headerinfo = '';
    public $layout = 'admin';

    protected function beforeAction($action) {
        parent::beforeAction($action);
        if (!(Yii::app()->user->id)) {
            Yii::app()->user->setFlash('error', 'Login to access the page.');
            $this->redirect(Yii::app()->getbaseUrl(true));
            exit();
        }else{
             $this->checkPermission();
        }
        Yii::app()->theme = 'admin';
        //$title_page = Yii::app()->controller->action->id;
        // $title_page = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_page);
        //echo $Word; exit;
        $this->pageTitle = Yii::app()->name . ' | Manage MRSS Feed';
        return true;
    }

    /**
     * @method public home() Default action
     * @author GDR<support@muvi.in>
     * @return HTML 
     */
    public function actionindex() {
        $studio_id = Yii::app()->common->getStudioId();
        $this->breadcrumbs = array('Manage Content','Manage MRSS Feed');
        $this->headerinfo = "Manage MRSS Feed";
        $this->pageTitle = Yii::app()->name .' | ' . 'Manage MRSS Feed';
        $studioMrssCode = Studio::model()->getStudioMrssCode($studio_id);
        $page_size = 20;
        $offset = 0;
        if (isset($_REQUEST['page'])) {
            $offset = ($_REQUEST['page'] - 1) * $page_size + 1;
        }
        $command = Yii::app()->db->createCommand()
                ->select('SQL_CALC_FOUND_ROWS (0),mf.*')
                ->from('mrss_feed mf')
                ->where('studio_id='.$studio_id)
                ->order('id DESC')		
                ->limit($page_size,$offset);
        $data = $command->queryAll();
        $count = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();
        $pages=new CPagination($count);
        $pages->setPageSize($page_size);
        $end =($pages->offset+$pages->limit <= $count ? $pages->offset+$pages->limit : $count);
        $sample =range($pages->offset+1, $end);
        $mrssfeedExport = MrssFeedExport::model()->findAllByAttributes(array('studio_id' => $studio_id));
        $contenttypeList = StudioContentType::model()->findAll(
                array(
                    'select' => 'display_name,id,content_types_id',
                    'condition' => 'studio_id=:studio_id AND is_enabled=:is_enabled AND content_types_id !=:content_types_id',
                    'params' => array(':studio_id' => $studio_id, ':is_enabled' => 1, ':content_types_id' => 4)
        ));
        $this->render('index',array('contenttypeList' => $contenttypeList,'mrssFeedDetails'=>@$data,'item_count'=>@$count,'page_size'=>@$page_size,'pages'=>@$pages,'sample'=>@$sample,'studioMrssCode'=>@$studioMrssCode,'mrssfeedExport' => @$mrssfeedExport));
    }
    public function actionshowFeedData(){
        $this->layout = false;
        if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
            $feedDetails = MrssFeed::model()->findByPk($_REQUEST['id']);
            echo json_encode(json_decode($feedDetails->data,true)); exit;
        }
        echo ""; exit;
    }
    function actionremove(){
        $studio_id = Yii::app()->common->getStudioId();
        if(isset($_REQUEST['mrrs_id_delete']) && intval($_REQUEST['mrrs_id_delete'])){
            $mrssFeed = Yii::app()->db->createCommand()
                            ->delete('mrss_feed', 'id=:id AND studio_id=:studio_id', array(':id' => $_REQUEST['mrrs_id_delete'], ':studio_id' => $studio_id));
            //$mrssFeed = MrssFeed :: model()-> findByPk($_REQUEST['mrrs_id']);
            //$delete = $mrssFeed->delete();
            if($mrssFeed){
                Yii::app()->user->setFlash('success', 'MRSS Feed is deleted successfully');
            } else{
                Yii::app()->user->setFlash('error', 'MRSS Feed could not be deleted');
            }
        } else{
            Yii::app()->user->setFlash('error', 'MRSS Feed could not be deleted');
        }
        $url = $this->createUrl("/mrss/index");
        $this->redirect($url);exit;
    }
    function actionnewMrssFeed(){
        $studio_id = Yii::app()->user->studio_id;
        $command = Yii::app()->db->createCommand()
		->select('f.name as fileName,f.content_types_id,ms.episode_number,ms.episode_title,ms.is_episode,ms.series_number,ms.id as streamId')
		->from('movie_streams ms, films f,studio_content_types sct')
		->where('ms.movie_id=f.id and ms.studio_id ='.$studio_id)
                ->order('f.name ASC');
        $data = $command->queryAll();
        $this->breadcrumbs = array('Manage MRSS Feed'=>array('mrss/index'),'New MRSS Feed');
        $this->pageTitle = Yii::app()->name .' | ' . 'New MRSS Feed';
        $exportData = '';
        $selectedStreamId = array();
        if(isset($_REQUEST['feed_id']) && intval($_REQUEST['feed_id'])){
            $this->breadcrumbs = array('Manage MRSS Feed'=>array('mrss/index'),'Update MRSS Feed');
            $this->pageTitle = Yii::app()->name .' | ' . 'Update MRSS Feed';
            $exportData = MrssFeedExport::model()->findByAttributes(array('studio_id' => $studio_id,'id' => $_REQUEST['feed_id']));
            if(isset($exportData->stream_ids) && $exportData->stream_ids != ''){
                $selectedStreamId = explode(",",$exportData->stream_ids);
            }
        }
        
        $this->render('new_mrss_feed',array('selectedStreamId' => $selectedStreamId, 'data' => $data,'exportData' => $exportData));
    }
    function actionshowFilterVideo(){
        $this->layout = FALSE;
        $studio_id = Yii::app()->user->studio_id;
        $str = '';
        $condition = '';
        if(!empty($_REQUEST['streamId'])){
            $streamId = $_REQUEST['streamId'];
            if(isset($_REQUEST['showSelected']) && $_REQUEST['showSelected'] == 1){
                $condition = ' and ms.id IN ('.$streamId.')';
            } else{
                $condition = ' and ms.id NOT IN ('.$streamId.')';
            }
        }
        $filterData = $_REQUEST['filterData'];
        $command = Yii::app()->db->createCommand()
		->select('f.name as fileName,ms.episode_number,ms.episode_title,ms.is_episode,ms.series_number,ms.id as streamId')
		->from('movie_streams ms, films f')
		->where('ms.movie_id=f.id and ms.studio_id ='.$studio_id.$condition.' and (f.name like "%'.$filterData.'%" or ms.episode_title like "%'.$filterData.'%")')
                ->order('f.name ASC');
        $data = $command->queryAll();
        $this->render('show_filter_video',array('restricted_countries' => $restricted_countries, 'data' => $data));
    }
    
    function actioncheckTitleExist(){
        $studio_id = Yii::app()->user->studio_id;
        $arr = array();
        if($_REQUEST['title'] != ''){
            $con = Yii::app()->db;
            if($_REQUEST['feed_id'] != ''){
                $sql = "select count(*) as countData from mrss_feed_export where studio_id = ".$studio_id." and title='".$_REQUEST['title']."' and id !=".$_REQUEST['feed_id'];
                $titleExist = $con->createCommand($sql)->queryAll();
            } else{
                $sql = "select count(*) as countData from mrss_feed_export where studio_id = ".$studio_id." and title='".$_REQUEST['title']."'";
                $titleExist = $con->createCommand($sql)->queryAll();
            }
            if($titleExist['countData'] == 0){
                $arr['succ'] = 1;
            }else{
                $arr['succ'] = 0;
            }
        }else{
           $arr['succ'] = 0;
        }
        echo json_encode($arr);
        exit;
    }
    
    function actionsaveMrssFeed(){
        $this->layout = FALSE;
        $studio_id = Yii::app()->user->studio_id;
        $str = '';
        if($_REQUEST['selected_stream_back']){
            $str = $_REQUEST['selected_stream_back'];
            if(@$_REQUEST['title'] != '' && $str != ''){
                if(isset($_REQUEST['feed_id']) && intval($_REQUEST['feed_id'])){
                    $mrssFeedExport = MrssFeedExport::model()->findByAttributes(array('studio_id' => $studio_id,'id' => $_REQUEST['feed_id']));
                    if($mrssFeedExport){
                        $mrssFeedExport->title = $_REQUEST['title'];
                        $mrssFeedExport->stream_ids = $str;
                        $mrssFeedExport->updated_date = new CDbExpression("NOW()");
                        $mrssFeedExport->save();
                        Yii::app()->user->setFlash('success', 'New Mrss Feed is updated successfully.');
                    } else{
                        Yii::app()->user->setFlash('error', 'You are not authorised to update the data.'); 
                    }
                } else{
                    $mrssFeedExport = new MrssFeedExport();
                    $mrssFeedExport->studio_id = $studio_id;
                    $mrssFeedExport->title = $_REQUEST['title'];
                    $mrssFeedExport->stream_ids = $str;
                    $mrssFeedExport->created_date = new CDbExpression("NOW()");
                    $mrssFeedExport->updated_date = new CDbExpression("NOW()");
                    $mrssFeedExport->save();
                    Yii::app()->user->setFlash('success', 'New Mrss Feed is saved successfully.');
                }
                $url = $this->createUrl("/mrss/index");
            } else{
                Yii::app()->user->setFlash('error', 'Please fill all the data required');
                $url = $this->createUrl("/mrss/newMrssFeed");
            }
        } else{
            Yii::app()->user->setFlash('error', 'Please fill all the data required');
            $url = $this->createUrl("/mrss/newMrssFeed");
        }
        $this->redirect($url);exit;
    }
    
    function actiondeleteExportData(){
        $studio_id = Yii::app()->common->getStudioId();
        if(isset($_REQUEST['mrrs_export_id_delete']) && intval($_REQUEST['mrrs_export_id_delete'])){
            $mrssFeed = Yii::app()->db->createCommand()
                            ->delete('mrss_feed_export', 'id=:id AND studio_id=:studio_id', array(':id' => $_REQUEST['mrrs_export_id_delete'], ':studio_id' => $studio_id));
            //$mrssFeed = MrssFeed :: model()-> findByPk($_REQUEST['mrrs_id']);
            //$delete = $mrssFeed->delete();
            if($mrssFeed){
                Yii::app()->user->setFlash('success', 'MRSS Feed Export Data is deleted successfully');
            } else{
                Yii::app()->user->setFlash('error', 'MRSS Feed Export Data could not be deleted');
            }
        } else{
            Yii::app()->user->setFlash('error', 'MRSS Feed Export Data could not be deleted');
        }
        $url = $this->createUrl("/mrss/index");
        $this->redirect($url);exit;
    }
    
    public function actionshowFilterContentTitle() {
        $this->layout = false;
        $command = Yii::app()->db->createCommand()
                ->select('distinct(series_number) as season,f.id,f.name')
                ->from('movie_streams ms,films f ')
                ->where('ms.movie_id = f.id and f.content_type_id='. @$_REQUEST['contentTypeId'])
                ->order('f.name ASC');
        $data = $command->queryAll();
        $this->render('show_filter_content_title',array('data' => $data));
    }
    public function actionshowMrssMetadata() {
        $this->layout = false;
        if(isset($_REQUEST['mrss_feed_url']) && ($_REQUEST['mrss_feed_url'] != '') && isset($_REQUEST['content_type']) && ($_REQUEST['content_type'] != '')){
            $xml = simplexml_load_file($_REQUEST['mrss_feed_url'], 'SimpleXMLElement', LIBXML_NOCDATA);
            if($xml != ''){
                $getNameSpace =$xml->getNamespaces(true);
                $mediaAttributes = array();
                $a = array();
                foreach($xml->children() as $child)
                {
                     $ai = 0;
                    foreach ($child->children() as $node) {
                        $a[$ai]['data'] = json_decode(json_encode($node), true);
                        foreach($node->children() as $nodeChildKey => $nodeChildKeyVal){
                            $mediaAttributes[$nodeChildKey] = '';
                        }
                        $attr = $node->children($getNameSpace["media"]);
                        $attr12= json_decode(json_encode($attr, true));
                        foreach($attr12 as $attr12key => $attr12Val){
                            $content = json_decode(json_encode($attr->$attr12key->attributes()), true);
                            if($content){
                                $a[$ai][$attr12key] = @$content['@attributes'];
                            } else{
                                $a[$ai][$attr12key] = $attr12Val;
                            }
                            
                        }
                        $attr1 = $attr->children($getNameSpace["media"]);
                            $attr13 =json_decode(json_encode($attr1), true);
                            foreach($attr13 as $attr13Key => $attr13Val){
                                $content = json_decode(json_encode($attr1->$attr13Key->attributes()), true);
                                if($content){
                                    $a[$ai][$attr13Key] = @$content['@attributes'];
                                } else{
                                    $a[$ai][$attr13Key] = $attr13Val;
                                }
                            }
                        $ai++;
                    }
                }
                $this->render('show_metadata',array('mediaAttributes'=>$mediaAttributes,'content_type'=>$_REQUEST['content_type'],'content_name'=>$_REQUEST['content_name'],'mrss_feed_url' => $_REQUEST['mrss_feed_url']));
            } else{
                echo 0;
            }
        }
    }
}
