<?php

if ((strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/saves3bucketdetails') ||
        (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/movefileasperstudios') ||
        (strtolower(Yii::app()->urlManager->parseUrl(Yii::app()->request)) == 'awss3/createstudiocdnmanualy')) {
    require 'aws/aws-autoloader.php';
}
require 's3bucket/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class CopystoreController extends Controller {

    public function actionindex() {
        //echo "test";exit;
    }

    public function actioncopyStoreData() {
        ini_set('memory_limit', '-1');
        $copied_studio = $_REQUEST['studio_id'];
        $source_studio = $this->studio->id;
       
        
        $this->copyCast($copied_studio);
        $this->copyContentCategories($copied_studio);
        $this->copystaticPages($copied_studio);
        $this->copyMenus($copied_studio);
        $this->copyCustomTables($copied_studio);
        $this->copyTranslation($copied_studio);
        
        //get source studio details      
        $sourcestudioDetails = Studios::model()->findByPk($source_studio);
        $sourcebucketInfo = Yii::app()->common->getBucketInfo("", $source_studio);
        $sourcebucketName = $sourcebucketInfo['bucket_name'];
        $sources3url = $sourcebucketInfo['s3url'];
        $s3cfg = $sources3cfg = $sourcebucketInfo['s3cmd_file_name'];
        $SourcesignedBucketPathForview = $sourcebucketInfo['signedFolderPath'];       
        $sourcefolderPath = Yii::app()->common->getFolderPath("", $source_studio);       
        $sourcesignedBucketPath = $sourcefolderPath['signedFolderPath'];
        $sourceunsignedFolderPath = $sourcefolderPath['unsignedFolderPath'];
        $sourceunsignedFolderPathForVideo=$sourcefolderPath['unsignedFolderPathForVideo'];
        
        //get dest studio details
        $deststudioDetails = Studios::model()->findByPk($copied_studio);
        $destbucketInfo = Yii::app()->common->getBucketInfo("", $copied_studio);
        $destbucketName = $destbucketInfo['bucket_name'];
        $dest3url = $destbucketInfo['s3url'];
        $dests3cfg = $destbucketInfo['s3cmd_file_name'];
        $destsignedBucketPathForview = $destbucketInfo['signedFolderPath'];
        $destbucketnewCloudfrontUrl = CDN_HTTP . $destbucketInfo['cloudfront_url']."/".$destsignedBucketPathForview;   
        $destfolderPath = Yii::app()->common->getFolderPath("", $copied_studio);   
        $destsignedBucketPath = $destfolderPath['signedFolderPath'];
        $destunsignedFolderPath = $destfolderPath['unsignedFolderPath'];
        $destunsignedFolderPathForVideo = $destfolderPath['unsignedFolderPathForVideo'];
        
        $client = Yii::app()->common->connectToAwsS3($copied_studio);
        //source url
        $bucketHttpOldUrl = 'http://' . $sourcebucketName . '.' . $sources3url . '/' . $sourcesignedBucketPath;
        $bucket_oldurl = "s3://" . $sourcebucketName . "/" . $sourcesignedBucketPath;
        $bucket_oldurlunsigned = "s3://" . $sourcebucketName . "/" . $sourceunsignedFolderPath;
        $bucket_oldurlunsignedvideo = "s3://" . $sourcebucketName . "/" . $sourceunsignedFolderPathForVideo;
        
        //dest url
        $bucketHttpNewUrl = 'http://' . $destbucketName . '.' . $dest3url . '/' . $destsignedBucketPath;
        $bucket_newurl = "s3://" . $destbucketName . "/" . $destsignedBucketPath;
        $bucket_newurlForUnsigned = "s3://" . $destbucketName . "/" . $destunsignedFolderPath;
        $bucket_newurlForUnsignedvideo = "s3://" . $destbucketName . "/" . $destunsignedFolderPathForVideo;
    
        $this->copyContents($copied_studio, $destbucketnewCloudfrontUrl);
        $this->copymovieCast($copied_studio);
        $moveData = '';
        $this->copyImageGallery($copied_studio);
        //$moveData = '';//For ImageGallery copy all images from source folder to dest folder
       $imageGalleryDetails = ImageManagement::model()->findByAttributes(array('studio_id' => $copied_studio));
       
        if ($imageGalleryDetails) {
            $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "imagegallery/".$source_studio."/ ". $bucket_newurlunsigned . "imagegallery/".$copied_studio." \n";
       }
               
//        $this->copyVideoGallery($copied_studio);
//        For video Gallery all videos from source folder to dest folder
//        $videoGalleryDetails = VideoManagement::model()->findAllByAttributes(array('studio_id' => $copied_studio));
//        if ($videoGalleryDetails) {
//            $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . "  --acl-public " . $bucket_oldurlunsignedvideo . "videogallery/videogallery-image/ " . $bucket_newurlForUnsignedvideo . "videogallery/videogallery-image/ \n";
//            $moveData .= "mkdir /var/www/html/moveData/" . $source_studio . "/videogallery \n";
//            $moveData .= "chmod 0777 /var/www/html/moveData/" . $source_studio . "/videogallery \n";
//            $moveData .= "cd /var/www/html/moveData/" . $source_studio . "/videogallery \n";
//            foreach ($videoGalleryDetails as $videoGalleryDetailsKey => $videoGalleryDetailsVal) {
//                $moveData .= "/usr/local/bin/s3cmd get --config /usr/local/bin/." . $s3cfg . " " . $bucket_oldurlunsignedvideo . "videogallery/" . $videoGalleryDetailsVal['video_name'] . " \n";
//                $moveData .= "/usr/local/bin/s3cmd sync --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/moveData/" . $source_studio . "/videogallery/" . $videoGalleryDetailsVal['video_name'] . " " . $bucket_newurlForUnsignedvideo . "videogallery/" . $videoGalleryDetailsVal['video_name'] . " \n";
//                $moveData .= "rm /var/www/html/moveData/" . $source_studio . "/videogallery/" . $videoGalleryDetailsVal['video_name'] . " \n";
//            }
//        }
       $contentmoveData=$this->copyContents($s3cfg,$copied_studio, $bucket_oldurlunsigned, $bucket_oldurl,$bucket_newurlForUnsigned, $bucket_newurl, $destbucketnewCloudfrontUrl);
       $moveData=$moveData.$contentmoveData; 
       $trailormovedata=$this->addTrailor($s3cfg, $copied_studio, $bucket_oldurl, $bucket_newurl, $destbucketnewCloudfrontUrl);
       
       $moveData=$moveData.$trailormovedata; 
       $this->copymovieCast($copied_studio);
       //Sh file creation 
       
        if ($moveData != '') {
            $file = 'moveFiles_' . $source_studio . '.sh';
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file, 'w') or die('Cannot open file:  ' . $file);
            $cf = 'echo "${file%???}"';
            $file_data = "file=`echo $0`\n" .
                    "cf='" . $file . "'\n\n" .
                    "# create directory\n" .
                    "mkdir /var/www/html/moveData/$source_studio\n" .
                    "chmod 0777 /var/www/html/moveData/$source_studio\n\n" .
                    $moveData .
                    "# remove directory\n" .
                    "rm -rf /var/www/html/moveData/$source_studio\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/moveData/$file\n";
            fwrite($handle, $file_data);
            fclose($handle);
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file;
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            
            if ($s3->upload('muvistudio', "moveStudioData/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                echo "Sh file created successfully";
                unlink($filePath);
            }
        }
     exit;
    }

    
    public function copyContentCategories($dest_studio) {
        $source_studio = $this->studio->id;
        $ContentCategories = ContentCategories::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        
        $ContentCategoriesarr = array();
        //copy content categories
        
        foreach ($ContentCategories as $t) {
           
          //check the category present or not in dest studio??
            $destCategories = ContentCategories::model()->findAll('studio_id = :studio_id AND category_name=:category_name', array(':studio_id' => $dest_studio,':category_name'=>$t->category_name));
            if(empty($destCategories))// add source category if not exists in dest studio
            $ContentCategoriesarr[] = $t->attributes;
            
        }
       
        $contentModel = new ContentCategories();
        foreach ($ContentCategoriesarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $contentModel->$key1 = $value1;
            }
            $contentModel->studio_id = $dest_studio;
            $contentModel->isNewRecord = true;
            $contentModel->primaryKey = NULL;
            $contentModel->save();
        }
        //copy content sub categories
        $ContentSubcategoryarr = array();
        $StudioConfig = new StudioConfig;
        $contentsubModel = new ContentSubcategory();
        $ContentSubcategory = ContentSubcategory::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        foreach ($ContentSubcategory as $s) {
            //check if the content sub category already exists in dest sudio
            $ContentSubcategory = ContentSubcategory::model()->findAll('studio_id = :studio_id AND permalink= :permalink', array(':studio_id' => $dest_studio,':permalink'=>$s->permalink));
           if(empty($ContentSubcategory))
            $ContentSubcategoryarr[] = $s->attributes;
        }
       
        if (!empty($ContentSubcategory)) {
            foreach ($ContentSubcategoryarr as $key => $v) {
             
                foreach ($v as $key1 => $value1) {
                    $contentsubModel->$key1 = $value1;
                }
                $contentsubModel->studio_id = $dest_studio;
                $contentsubModel->isNewRecord = true;
                $contentsubModel->primaryKey = NULL;
                $contentsubModel->save();
            }
            $StudioConfig->studio_id = $dest_studio;
            $StudioConfig->config_key = 'subcategory_enabled';
            $StudioConfig->config_value = 1;
            $StudioConfig->isNewRecord = true;
            $StudioConfig->primaryKey = NULL;
            $StudioConfig->save();
        }
    }

    public function copycontents($s3cfg,$dest_studio, $bucket_oldurlunsigned, $bucket_oldurl, $bucket_newurlForUnsigned, $signedBucketPath, $bucketCloudfrontUrl) {
        $Films = new Film();
        $MovieStreams = new movieStreams();
        $livestream = new Livestream();
        $movietrailor = new movieTrailer();
        //$dest_studio=$_REQUEST['studio_id'];
        $source_studio = $this->studio->id;
        $film_field = 'F.id as movie_id,F.name,F.content_types_id,F.parent_content_type_id,F.language,F.genre,F.censor_rating,F.story,F.content_category_value,F.content_subcategory_value,F.release_date';
        $fetchcustomfield = 'F.custom1,F.custom2,F.custom3,F.custom4,F.custom5,F.custom6,F.custom7,F.custom8,F.custom9,F.custom10,m.custom1 as cs1,m.custom2 as cs2,m.custom3 as cs3,m.custom4 as cs4,m.custom5 as cs5,m.custom6 as cs6';
        $movie_stream_field = 'm.`id` AS movie_stream_id,m.`movie_id` AS stream_movie_id , m.`video_quality`, m.`full_movie`, m.`full_movie_url`, m.`thirdparty_url`, m.`episode_number`, m.`episode_title`, m.`episode_story`, m.`episode_date`, m.`is_episode`, m.`series_number`, m.`is_popular`, m.`id_seq`, m.`is_poster`, m.`is_active`,    m.`is_converted`, m.`embed_id`, m.`has_sh`,  m.`is_download_progress`, m.`video_resolution`, m.`resolution_size`,  m.`mail_sent_for_video`, m.`upload_start_time`, m.`upload_end_time`, m.`upload_cancel_time`, m.`encoding_start_time`, m.`encoding_end_time`, m.`encode_fail_time`,  m.`original_file`, m.`converted_file`, m.`video_duration`, m.`is_demo`, m.`video_management_id`, m.`encryption_key`, m.`content_key`,  m.`episode_language_id`, m.`is_offline`, m.`has_hls_feed`';
        //$movie_trailor_field ='mt.`id` AS trailor_id, mt.`trailer_file_name`, mt.`trailer_content_type`, mt.`movie_id` AS trailor_movie_id, mt.`video_remote_url`, mt.`name` AS trailor_name, mt.`ip` AS trailor_ip ,  mt.`is_converted` AS trailor_is_converted, mt.`has_sh` AS trailor_has_sh, mt.`video_resolution` AS trailor_video_resolution, mt.`resolution_size`  AS trailor_resolution_size, mt.`mail_sent_for_trailer`, mt.`upload_cancel_time` AS trailor_upload_cancel_time, mt.`encoding_start_time` AS trailor_encoding_start_time, mt.`encoding_end_time` AS trailor_encoding_end_time , mt.`encode_fail_time` AS trailor_encode_fail_time, mt.`upload_start_time` AS trailor_upload_start_time, mt.`upload_end_time` AS trailor_upload_end_time, mt.`video_management_id` AS trailor_video_management_id, mt.`third_party_url`';

        $sql_data = "SELECT P.* FROM "
                . "( SELECT {$movie_stream_field},{$film_field},{$fetchcustomfield}"
                . " FROM movie_streams m,films F  WHERE F.parent_id = 0 AND m.movie_id = F.id AND F.status = 1 AND F.studio_id=" . $source_studio
                . " ) AS P order by P.is_episode ASC";
       
       
                $list = Yii::app()->db->createCommand($sql_data)->queryAll();
       
        foreach ($list as $listkey => $listvalue) {
            $flist[$listvalue['movie_id']][] = $listvalue;
        }
        
        $moveData='';
        $moveData .= "mkdir /var/www/html/moveData/" . $source_studio . "/movie \n";
        $moveData .= "chmod 0777 /var/www/html/moveData/" . $source_studio . "/movie \n";
        $moveData .= "cd /var/www/html/moveData/" . $source_studio . "/movie \n";
        foreach ($flist as $listkey => $listvalue) {
            foreach ($listvalue as $k => $v) {
                if ($v['is_episode'] == 0) {
                    /* INSERT into film table */
                    $movie = $this->addContent($s3cfg,$v, $dest_studio,$Films, $bucket_oldurlunsigned, $bucket_newurlForUnsigned);
                    $movie_id = $movie['id'];   
                    //$moveData1 = $movie['moveData'];
                    /* insert into url routing */
                    $urlRouts['permalink'] = $movie['permalink'];
                    $urlRouts['mapped_url'] = '/movie/show/content_id/' . $movie_id;
                    $urlRoutObj = new UrlRouting();
                    $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $dest_studio);
                    /* insert into moviestream */
                    $moveData2=$this->addStream($s3cfg,$v, $dest_studio, $movie_id, $MovieStreams, 0,$bucket_oldurlunsigned, $bucket_newurlForUnsigned,$bucket_oldurl,$signedBucketPath,'films');
                    $moveData=$moveData.$moveData2;
                    
                } else if ($v['is_episode'] == 1) {
                    $moveData4=$this->addStream($s3cfg,$v, $dest_studio, $movie_id, $MovieStreams, 1,$bucket_oldurlunsigned, $bucket_newurlForUnsigned,$bucket_oldurl,$signedBucketPath,'moviestream');
                
                   $moveData=$moveData.$moveData4;
                }
            }
        }
        
        return $moveData;
    }

    function addContent($s3cfg,$data, $studio_id, $Films,$bucket_oldurlunsigned, $bucket_newurlForUnsigned) {
       /* 
        for copying content 
        * 1.delete all the existing custom form from dest studio.
        * 2.copy all custom form from source studio and add to dest studio
        * 3.map copied content as per the new content form id inserted in dest studio
        */
        $permalinkins = ($data['permalink'] != '') ? $data['permalink'] : $data['name'];
        $Films->studio_id = $studio_id;
        $Films->name = $data['name'];
        $uniqid = Yii::app()->common->generateUniqNumber();
        $Films->uniq_id = $uniqid;
        if (isset($data['release_date']) && $data['release_date'] != '1970-01-01' && strlen(trim($data['release_date'])) > 6)
            $Films->release_date = date('Y-m-d', strtotime($data['release_date']));
        //$Films->content_type_id = $data['content_type_id'];
        $Films->language = @$data['language'];
        $Films->genre = @$data['genre'];
        $Films->censor_rating = @$data['censer_rating'];
        $Films->story = @$data['story'];
        $Films->created_date = gmdate('Y-m-d H:i:s');
        $Films->last_updated_date = gmdate('Y-m-d H:i:s');

        $Films->content_types_id = $data['content_types_id'];
        $Films->parent_content_type_id = $data['parent_content_type_id'];
        
        //get custom form id for dest studio
        $source_metadata_form=$sourcecatdetails = Yii::app()->db->createCommand()->select('id,name')->from('custom_metadata_form')->where('id=:id', array(':id' => @$data['custom_metadata_form_id']))->queryRow();
        $destmetadatadetails = CustomMetadataForm::model()->findByAttributes(array('name' => $source_metadata_form['name'], 'studio_id' => $studio_id));
        $Films->custom_metadata_form_id = $destmetadatadetails->id;
        //get category id for dest studio
        $sourcecatdetails = Yii::app()->db->createCommand()->select('id,category_name')->from('content_category')->where('id=:id', array(':id' => @$data['content_category_value']))->queryRow();
        $destcatdetails = ContentCategories::model()->findByAttributes(array('category_name' => $sourcecatdetails['category_name'], 'studio_id' => $studio_id));
        $Films->content_category_value = $destcatdetails->id;
        
        $Films->permalink = Yii::app()->general->generatePermalink(stripslashes($permalinkins));
        $Films->status = 1;

        $Films->custom1 = @$data['custom1'];
        $Films->custom2 = @$data['custom2'];
        $Films->custom3 = @$data['custom3'];
        $Films->custom4 = @$data['custom4'];
        $Films->custom5 = @$data['custom5'];
        $Films->custom6 = @$data['custom6'];
        $Films->custom7 = @$data['custom7'];
        $Films->custom8 = @$data['custom8'];
        $Films->custom9 = @$data['custom9'];
        $Films->custom10 = @$data['custom10'];


        $Films->isNewRecord = true;
        $Films->primaryKey = NULL;
        $Films->save();    
        
        // $movie['moveData']=$this->copyPosters($s3cfg,$dest_studio, $bucket_oldurlunsigned,$bucket_newurlForUnsigned, $Films->id, 'films');
        $movie['permalink']=$Films->permalink;
        $movie['id']=$Films->id;
        return $movie; 
    }

    function addStream($s3cfg,$v, $studio_id, $movie_id, $MovieStreams, $is_episode,$bucket_oldurlunsigned, $bucket_newurlForUnsigned,$bucket_oldurl,$signedBucketPath,$object_type) {
        $source_studio = $this->studio->id;
        $epdt = $v['episode_date'] ? date('Y-m-d', strtotime($v['episode_date'])) : gmdate('Y-m-d');
        $MovieStreams->episode_title = @$v['episode_title'];
        $MovieStreams->episode_number = @$v['episode_number'];
        $MovieStreams->series_number = @$v['series_number'];
        $MovieStreams->studio_id = $studio_id;
        $MovieStreams->episode_story = @$v['episode_story'];
        $MovieStreams->episode_date = $epdt;
        $MovieStreams->movie_id = $movie_id;
        $MovieStreams->video_quality = @$v['video_quality'];
        //$MovieStreams->full_movie = @$v['full_movie'];
        //$MovieStreams->full_movie_url = @$v['full_movie_url'];
        $MovieStreams->thirdparty_url = @$v['thirdparty_url'];
        //$MovieStreams->description = @$v['description'];
       
        //$MovieStreams->review_flag = @$v['review_flag'];
        
        $MovieStreams->is_popular = @$v['is_popular'];
        $MovieStreams->id_seq = @$v['id_seq'];

        $MovieStreams->is_poster = @$v['is_poster'];
        $MovieStreams->is_active = @$v['is_active'];
        
        $MovieStreams->is_converted = @$v['is_converted'];
        
        $MovieStreams->has_sh = @$v['has_sh'];
        //$MovieStreams->has_upload_sh = @$v['has_upload_sh'];
        $MovieStreams->is_download_progress = @$v['is_download_progress'];

        $MovieStreams->video_resolution = @$v['video_resolution'];
        $MovieStreams->resolution_size = @$v['resolution_size'];
        
        
        $MovieStreams->mail_sent_for_video = @$v['mail_sent_for_video'];
        $MovieStreams->upload_start_time = @$v['upload_start_time'];
        $MovieStreams->upload_end_time = @$v['upload_end_time'];


        $MovieStreams->upload_cancel_time = @$v['upload_cancel_time'];
        $MovieStreams->encoding_start_time = @$v['encoding_start_time'];
        $MovieStreams->encoding_end_time = @$v['encoding_end_time'];
        $MovieStreams->encode_fail_time = @$v['encode_fail_time'];
        
        $MovieStreams->original_file = @$v['original_file'];
        $MovieStreams->converted_file = @$v['converted_file'];
        $MovieStreams->video_duration = @$v['video_duration'];
        $MovieStreams->is_demo = @$v['is_demo'];

        $sourcevideodetails = Yii::app()->db->createCommand()->select('id,video_name,video_remote_url')->from('video_management')->where('id=:id', array(':id' => @$v['video_management_id']))->queryRow();
        $destvideodetails = VideoManagement::model()->findByAttributes(array('video_name' => $sourcevideodetails['video_name'], 'studio_id' => $studio_id));

        $MovieStreams->video_management_id = $destvideodetails->id;
        $MovieStreams->encryption_key = @$v['encryption_key'];
        $MovieStreams->content_key = @$v['content_key'];
      //  $MovieStreams->original_file_name = @$v['original_file_name'];
        $MovieStreams->episode_language_id = @$v['episode_language_id'];
        $MovieStreams->is_offline = @$v['is_offline'];

        $MovieStreams->has_hls_feed = @$v['has_hls_feed'];
        $MovieStreams->is_downloadable = @$v['is_downloadable'];


        $MovieStreams->created_date = gmdate('Y-m-d');
        $MovieStreams->last_updated_date = gmdate('Y-m-d H:i:s');
        /* Add custom data */
        $MovieStreams->custom1 = @$v['cs1'];
        $MovieStreams->custom2 = @$v['cs2'];
        $MovieStreams->custom3 = @$v['cs3'];
        $MovieStreams->custom4 = @$v['cs4'];
        $MovieStreams->custom5 = @$v['cs5'];
        $MovieStreams->custom6 = @$v['cs6'];
        $MovieStreams->is_episode = $is_episode;
        $MovieStreams->isNewRecord = true;
        $MovieStreams->primaryKey = NULL;
        $MovieStreams->save();
        
        $MovieStreams->embed_id = md5($MovieStreams->id);
        $MovieStreams->save();
        
        $object_id=($object_type='films')?$movie_id:$MovieStreams->id;
        
       	$moveData='';
//        if (@$v['full_movie'] != '') {
//            
//            $moveData .= "mkdir /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . " \n";
//            $moveData .= "chmod 0777 /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . " \n";
//            $moveData .= "cd /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . " \n";
//
//            $moveData .= "/usr/local/bin/s3cmd get --recursive --config /usr/local/bin/.". $s3cfg." " . $bucket_oldurl . "uploads/movie_stream/full_movie/" . $v['movie_stream_id'] . "/ \n";
//            if(@$v['encryption_key'] != '' && @$v['content_key'] != ''){
//                $moveData .="if [ -s /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/stream.mpd]\n\nthen\n\n";
//                $moveData .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/dash+xml' /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/stream.mpd " . $signedBucketPath . "uploads/movie_stream/full_movie/" . $MovieStreams->id . "/ \n";
//                $moveData .= "rm /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/stream.mpd \n";
//                $moveData .= "fi \n";
//                $moveData .="if [ -s /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/hls/master.m3u8]\n\nthen\n\n";
//                $moveData .= "/usr/local/bin/s3cmd put --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'application/x-mpegURL' /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/hls/master.m3u8 " . $signedBucketPath . "uploads/movie_stream/full_movie/" . $MovieStreams->id . "/hls/ \n";
//                $moveData .= "rm /var/www/html/moveData/".$source_studio."/movie/".$v['movie_stream_id']."/hls/master.m3u8 \n";
//                $moveData .= "fi \n";
//                $moveData .= "/usr/local/bin/s3cmd put --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . "/ " . $signedBucketPath . "uploads/movie_stream/full_movie/" . $MovieStreams->id . "/ \n";
//            } else{
//                $moveData .= "/usr/local/bin/s3cmd put --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public --add-header='content-type':'video/mp4' /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . "/ " . $signedBucketPath . "uploads/movie_stream/full_movie/" . $MovieStreams->id . "/ \n";
//            }
//            $moveData .= "rm -rf /var/www/html/moveData/" . $source_studio . "/movie/" . $v['movie_stream_id'] . "/ \n";
//        }
        
        return $moveData;
    }
    
    public function copyMenus($dest_studio) {
        $source_studio = $this->studio->id;
        $ip_address = CHttpRequest::getUserHostAddress();

        $MenuItem = MenuItem::model()->findAll('studio_id = :studio_id AND language_id = 20', array(':studio_id' => $source_studio)); // for english only
        $MenuModel = new MenuItem();
        $urlRouting = new UrlRouting();
        $MenuItemarr = array();
        foreach ($MenuItem as $t) {
            $MenuItemarr[] = $t->attributes;
        }
        foreach ($MenuItemarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $MenuModel->$key1 = $value1;
            }
            $MenuModel->menu_id = $Menu['id'];
            $MenuModel->parent_id = 0;
            $MenuModel->studio_id = $dest_studio;
            $MenuModel->isNewRecord = true;
            $MenuModel->primaryKey = NULL;
            $MenuModel->save();
            $menu_item_id = $MenuModel->id;

            $urlRouting->permalink = $MenuModel->permalink;
            $urlRouting->mapped_url = '/media/list/menu_item_id/' . $menu_item_id;
            $urlRouting->studio_id = $dest_studio;
            $urlRouting->created_date = new CDbExpression("NOW()");
            $urlRouting->ip = $ip_address;
            $urlRouting->isNewRecord = true;
            $urlRouting->primaryKey = NULL;
            $urlRouting->save();
        }
    }

    public function copystaticPages($dest_studio) {
        $source_studio = $this->studio->id;
       
        $Page = Page::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
       // echo "<pre>"; print_r($Page);exit;
        $PageModel = new Page();
        $urlRoutObj = new UrlRouting();
        $Pagearr = array();
        foreach ($Page as $t) {
             $destPage = Page::model()->findAll('studio_id = :studio_id AND permalink=:permalink', array(':studio_id' => $dest_studio,':permalink'=>$t->permalink));      
           if(empty($destPage))
             $Pagearr[] = $t->attributes;
        }
       // echo "<pre>"; print_r($Pagearr);exit;
        foreach ($Pagearr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $PageModel->$key1 = $value1;
            }
            $PageModel->studio_id = $dest_studio;
            $PageModel->isNewRecord = true;
            $PageModel->primaryKey = NULL;
            $PageModel->save();

            $urlRouts['permalink'] = $v['permalink'];
            $urlRouts['mapped_url'] = '/page/show/permalink/' . $v['permalink'];
            $urlRoutings = $urlRoutObj->addUrlRouting($urlRouts, $dest_studio);
        }
    }

    public function copyTranslation($dest_studio) {
        $source_studio = $this->studio->id;
        $StudioLanguage = StudioLanguage::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $StudioLanguageModel = new StudioLanguage();

        $Languagearr = array();
        foreach ($StudioLanguage as $t) {
            $Languagearr[] = $t->attributes;
        }
        foreach ($Languagearr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $StudioLanguageModel->$key1 = $value1;
            }
            $StudioLanguageModel->studio_id = $dest_studio;
            $StudioLanguageModel->isNewRecord = true;
            $StudioLanguageModel->primaryKey = NULL;
            $StudioLanguageModel->save();
        }

        //copy the translated keyword
        $TranslateKeyword = TranslateKeyword::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $TranslateKeywordModel = new TranslateKeyword();
        $Keyworarr = array();
        foreach ($TranslateKeyword as $s) {
            $Keyworarr[] = $s->attributes;
        }
        foreach ($TranslateKeyword as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $TranslateKeywordModel->$key1 = $value1;
            }
            $TranslateKeywordModel->studio_id = $dest_studio;
            $TranslateKeywordModel->isNewRecord = true;
            $TranslateKeywordModel->primaryKey = NULL;
            $TranslateKeywordModel->save();
        }
    }

    public function copyCast($dest_studio) {
        $source_studio = $this->studio->id;
        //celebrity        
        $Celebrity = Celebrity::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $CelebrityModel = new Celebrity();

        $Celebrityarr = array();
        foreach ($Celebrity as $t) {
            $Celebrityarr[] = $t->attributes;
        }
        foreach ($Celebrityarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $CelebrityModel->$key1 = $value1;
            }
            $CelebrityModel->studio_id = $dest_studio;
            $CelebrityModel->isNewRecord = true;
            $CelebrityModel->primaryKey = NULL;
            $CelebrityModel->save();
        }
        
         // $moveData=$this->copyPosters($s3cfg,$dest_studio,$bucket_oldurlunsigned, $bucket_newurlForUnsigned, $CelebrityModel->id, 'celebrity');
        //celebrity_type
        $CelebrityType = CelebrityType::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $CelebrityTypeModel = new CelebrityType();

        $CelebrityTypearr = array();
        foreach ($CelebrityType as $c) {
            $CelebrityTypearr[] = $c->attributes;
        }
        foreach ($CelebrityTypearr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $CelebrityTypeModel->$key1 = $value1;
            }
            $CelebrityTypeModel->studio_id = $dest_studio;
            $CelebrityTypeModel->isNewRecord = true;
            $CelebrityTypeModel->primaryKey = NULL;
            $CelebrityTypeModel->save();
        }
     
        }
        public function copymovieCast($dest_studio) {
         $source_studio = $this->studio->id;
        //celebrity        
        $MovieCast= Yii::app()->db->createCommand('select * from movie_casts where movie_id in (select id from films where studio_id='.$source_studio.')')->queryAll();
        $MovieCastModel = new MovieCast();

        $MovieCastarr = array();
        foreach ($MovieCast as $t) {
            $MovieCastarr[] = $t->attributes;
        }
        foreach ($MovieCastarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $MovieCastModel->$key1 = $value1;
                
                 if ($key1 == 'movie_id') {
                    $sourcefilmdetails = Yii::app()->db->createCommand()->select('id,name')->from('films')->where('id=:id', array(':id' => @$v['movie_id']))->queryRow();
                    $destfilmdetails = CustomMetadataForm::model()->findByAttributes(array('name' => $sourcefilmdetails['name'], 'studio_id' => $dest_studio));

                    $MovieCastModel->movie_id = $destfilmdetails->id;
                }
                if ($key1 == 'celebrity_id') {
                    $sourcecelebdetails = Yii::app()->db->createCommand()->select('id,name,permalink')->from('celebrities')->where('id=:id', array(':id' => @$v['celebrity_id']))->queryRow();
                    $destcelebdetails = Celebrity::model()->findByAttributes(array('name' => $sourcecelebdetails['name'],'permalink' => $sourcecelebdetails['permalink'], 'studio_id' => $dest_studio));

                    $MovieCastModel->celebrity_id = $destcelebdetails->id;
                }
            }
            
            $MovieCastModel->isNewRecord = true;
            $MovieCastModel->primaryKey = NULL;
            $MovieCastModel->save();
        }
        
        }
    public function copyPosters($s3cfg,$dest_studio,$bucket_oldurlunsigned, $bucket_newurlForUnsigned, $object_id, $object_type) {
        
        $source_studio = $this->studio->id;
        //content poster,celebrity poster,playlist poster
        $ContentPoster = Poster::model()->findAll('object_type=:object_type AND object_id=:object_id', array(':object_type'=>$object_type,':object_id'=>$object_id));
       
        $ContentPosterModel = new Poster();

        $ContentPosterarr = array();
        foreach ($ContentPoster as $t) {
            $ContentPosterarr[] = $t->attributes;
        }
        foreach ($ContentPosterarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $ContentPosterModel->$key1 = $value1;
            }
            $ContentPosterModel->studio_id = $dest_studio;
            $ContentPosterModel->object_id = $object_id;
            $ContentPosterModel->object_type = $object_type;
            $ContentPosterModel->isNewRecord = true;
            $ContentPosterModel->primaryKey = NULL;
            $ContentPosterModel->save();
            $moveData= $moveData."/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "/public/system/posters/" . $v['id'] . "/ " . $bucket_newurlForUnsigned . "public/system/posters/" . $ContentPosterModel->id . "/ \n";
        }
        
         return $moveData;
    }

    public function copyImageGallery($dest_studio) {
        $source_studio = $this->studio->id;
        //Image Gallery
        $ImageManagement = ImageManagement::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $ImageManagementModel = new ImageManagement();
        $ImageManagementarr = array();

        foreach ($ImageManagement as $t) {
            $ImageManagementarr[] = $t->attributes;
        }
        foreach ($ImageManagementarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $ImageManagementModel->$key1 = $value1;
            }
            $s3_thumb_name='imagegallery/'.$dest_studio.'/thumb/'.$v['image_name'];
            $s3_original_name='imagegallery/'.$dest_studio.'/original/'.$v['image_name'];
            $ImageManagementModel->s3_thumb_name = $s3_thumb_name;
            $ImageManagementModel->s3_original_name = $s3_original_name;
            $ImageManagementModel->studio_id = $dest_studio;
            $ImageManagementModel->isNewRecord = true;
            $ImageManagementModel->primaryKey = NULL;
            $ImageManagementModel->save();
        }
    }

    public function copyVideoGallery($dest_studio) {
        $source_studio = $this->studio->id;
        //Image Gallery
        $VideoManagement = VideoManagement::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $VideoManagementModel = new VideoManagement();
        $VideoManagementarr = array();

        foreach ($VideoManagement as $t) {
            $VideoManagementarr[] = $t->attributes;
        }
        foreach ($VideoManagementarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $VideoManagementModel->$key1 = $value1;
            }
            $VideoManagementModel->studio_id = $dest_studio;
            $VideoManagementModel->isNewRecord = true;
            $VideoManagementModel->primaryKey = NULL;
            $VideoManagementModel->save();
        }
    }
    public function addTrailor($s3cfg, $dest_studio,$bucket_oldurl,$signedBucketPath, $bucketCloudfrontUrl) {
        $source_studio = $this->studio->id;
         
        $movieTrailer = movieTrailer::model()->findAll('movie_id in (select id from films where studio_id=' . $source_studio . ')');
        $movieTrailerModel = new movieTrailer();
        $movieTrailerModelarr = array();
        $moveData='';
        foreach ($movieTrailer as $t) {
            $movieTrailerModelarr[] = $t->attributes;
        }
        foreach ($movieTrailerModelarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $movieTrailerModel->$key1 = $value1;
                if ($key1 == 'movie_id') {                  
                    $sourcefilmdetails = Yii::app()->db->createCommand()->select('id,name')->from('films')->where('id=:id', array(':id' => @$v['movie_id']))->queryRow();
                    $destfilmdetails = Film::model()->findByAttributes(array('name' => $sourcefilmdetails['name'], 'studio_id' => $dest_studio));
                    $movieTrailerModel->movie_id = $destfilmdetails->id;
                }
                
                if ($key1 == 'video_management_id') {                  
                    $sourcevideodetails = Yii::app()->db->createCommand()->select('id,video_name,video_remote_url')->from('video_management')->where('id=:id', array(':id' => @$v['video_management_id']))->queryRow();
                    $destvideodetails = VideoManagement::model()->findByAttributes(array('video_name' => $sourcevideodetails['video_name'], 'studio_id' => $dest_studio));
                    $movieTrailerModel->video_management_id = $destvideodetails->id;
                }
                }
               
                $movieTrailerModel->isNewRecord = true;
                $movieTrailerModel->primaryKey = NULL;
                $movieTrailerModel->save();
                $trailor_id=$movieTrailerModel->id;
                $trailer_file_name=$movieTrailerModel->trailer_file_name;
                $videoFullPath = 'uploads/trailers/' . $trailor_id. '/' . $v['trailer_file_name'];

                $movieTrailerModel->video_remote_url = $bucketCloudfrontUrl .  $videoFullPath;
                $movieTrailerModel->save();
                if($trailer_file_name !='')
                $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurl . "uploads/trailers/" . $v['id'] . "/ " . $signedBucketPath . "uploads/trailers/" . $movieTrailerModel->id . "/ \n";
    
        }
        return $moveData;
        
    }
    
    public function copyCustomTables($dest_studio)
    {
        //copy custom_metadataform
        
        $source_studio = $this->studio->id;
        
        $CustomMetadataForm = CustomMetadataForm::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $CustomMetadataFormModel = new CustomMetadataForm();
        $CustomMetadataFormarr = array();

        foreach ($CustomMetadataForm as $t) {
            $CustomMetadataFormarr[] = $t->attributes;
        }
        foreach ($CustomMetadataFormarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $CustomMetadataFormModel->$key1 = $value1;
            }
            $CustomMetadataFormModel->studio_id = $dest_studio;
            $CustomMetadataFormModel->isNewRecord = true;
            $CustomMetadataFormModel->primaryKey = NULL;
            $CustomMetadataFormModel->save();
        }
        
        //copy custom_metadata field
        
        $CustomMetadataField = CustomMetadataField::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        $CustomMetadataFieldModel = new CustomMetadataField();
        $CustomMetadataFieldModelarr = array();

        foreach ($CustomMetadataField as $t) {
            $CustomMetadataFieldarr[] = $t->attributes;
        }
        foreach ($CustomMetadataFieldarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $CustomMetadataFieldModel->$key1 = $value1;
                if ($key1 == 'metadata_form_type_id') {
                    $sourceformdetails = Yii::app()->db->createCommand()->select('id,name')->from('custom_metadata_form')->where('id=:id', array(':id' => $v['metadata_form_type_id']))->queryRow();
                    $CustomMetadataForm = CustomMetadataForm::model()->findByAttributes(array('name' => $sourceformdetails['name'], 'studio_id' => $dest_studio));

                    $CustomMetadataFieldModel->metadata_form_type_id = $CustomMetadataForm->id;
                }
                }
                $CustomMetadataFieldModel->studio_id = $dest_studio;
                $CustomMetadataFieldModel->isNewRecord = true;
                $CustomMetadataFieldModel->primaryKey = NULL;
                $CustomMetadataFieldModel->save();
            
        }
        
        
        //copy custom_metadata form field mapping table  
        //1.get content from the mapping table based on the source table form id.
         
        $CustomMetadataFormField = Yii::app()->db->createCommand('select * from custom_metadata_form_field where custom_form_id in (select id from custom_metadata_form where studio_id='.$source_studio.')')->queryAll();
        
        $CustomMetadataFormFieldModel = new CustomMetadataFormField();
        $CustomMetadataFormFieldModelarr = array();
        
        foreach ($CustomMetadataFormField as $k=>$t) {
            $CustomMetadataFormFieldModelarr[] = $t;
        }
       
        foreach ($CustomMetadataFormFieldModelarr as $key => $value) {
           foreach ($value as $key1 => $value1) {  
                $CustomMetadataFormFieldModel->$key1 = $value1;
                if ($key1 == 'custom_form_id') {
                    $sourceformdetails = Yii::app()->db->createCommand()->select('id,name')->from('custom_metadata_form')->where('id=:id', array(':id' => $value['custom_form_id']))->queryRow();
                    $CustomMetadataForm = CustomMetadataForm::model()->findByAttributes(array('name' => $sourceformdetails['name'], 'studio_id' => $dest_studio));

                    $CustomMetadataFormFieldModel->custom_form_id = $CustomMetadataForm->id;
                }
                if ($key1 == 'custom_field_id') {
                    $sourcefielddetails = Yii::app()->db->createCommand()->select('id,f_name')->from('custom_metadata_field')->where('id=:id', array(':id' => $value['custom_field_id']))->queryRow();
                    $CustomMetadataField = CustomMetadataField::model()->findByAttributes(array('f_name' => $sourcefielddetails['f_name'], 'studio_id' => $dest_studio));

                    $CustomMetadataFormFieldModel->custom_field_id = $CustomMetadataField->id;
                }
               }
                $CustomMetadataFormFieldModel->isNewRecord = true;
                $CustomMetadataFormFieldModel->primaryKey = NULL;
                $CustomMetadataFormFieldModel->save();
            
        }
        
        
    }
    
    public function actionifilmstore() {
        $dest_studio = $_REQUEST['studio_id'];
        //copy content categories
        $source_studio = $this->studio->id;
        $ContentCategories = ContentCategories::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));
        
        $ContentCategoriesarr = array();
        foreach ($ContentCategories as $t) {
           
          //check the category present or not in dest studio??
            $destCategories = ContentCategories::model()->findAll('studio_id = :studio_id AND category_name=:category_name', array(':studio_id' => $dest_studio,':category_name'=>$t->category_name));
            if(empty($destCategories))// add source category if not exists in dest studio
            $ContentCategoriesarr[] = $t->attributes;
            
        }
       
        $contentModel = new ContentCategories();
        foreach ($ContentCategoriesarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $contentModel->$key1 = $value1;
            }
            $contentModel->studio_id = $dest_studio;
            $contentModel->isNewRecord = true;
            $contentModel->primaryKey = NULL;
            $contentModel->save();
        }
        
        
        $PGProduct = PGProduct::model()->findAll(array('select' => '*', 'condition' => 'studio_id=:studio_id', 'params' => array(':studio_id' => $source_studio)));
        $PGProductModel = new PGProduct();
        $PGProductarr = array();

        foreach ($PGProduct as $t) {
            $PGProductarr[] = $t->attributes;
        }
        foreach ($PGProductarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $PGProductModel->$key1 = $value1;
                if($key1=='category_id')
                {
                $sourcecatdetails = Yii::app()->db->createCommand()->select('id,category_name')->from('content_category')->where('id=:id', array(':id' => $v['category_id']))->queryRow();
                $destcatdetails = ContentCategories::model()->findByAttributes(array('category_name' => $sourcecatdetails['category_name'], 'studio_id' => $dest_studio));
                $PGProductModel->category_id=$destcatdetails->id;
                }
                $PGProductModel->custom_metadata_form_id=52; //manually added the custom form id.
                
            }
            $PGProductModel->studio_id = $dest_studio;
            $PGProductModel->isNewRecord = true;
            $PGProductModel->primaryKey = NULL;
            $PGProductModel->save();
        }
        $this->copyImageGallery($dest_studio);
        $moveData = '';
         
        //get source studio details      
        
        $sourcebucketInfo = Yii::app()->common->getBucketInfo("", $source_studio);
        
        $sourcebucketName = $sourcebucketInfo['bucket_name'];
        $sources3url = $sourcebucketInfo['s3url'];
        $s3cfg = $sources3cfg = $sourcebucketInfo['s3cmd_file_name'];
        $SourcesignedBucketPathForview = $sourcebucketInfo['signedFolderPath'];
        
        $sourcefolderPath = Yii::app()->common->getFolderPath("", $source_studio);       
        
        $sourcesignedBucketPath = $sourcefolderPath['signedFolderPath'];
        $sourceunsignedFolderPath = $sourcefolderPath['unsignedFolderPath'];
        $sourceunsignedFolderPathForVideo=$sourcefolderPath['unsignedFolderPathForVideo'];
        
       //get dest studio details       
        $destbucketInfo = Yii::app()->common->getBucketInfo("", $dest_studio);
        
        $destbucketName = $destbucketInfo['bucket_name'];
        $dest3url = $destbucketInfo['s3url'];
        $dests3cfg = $destbucketInfo['s3cmd_file_name'];
        $destsignedBucketPathForview = $destbucketInfo['signedFolderPath'];
        $destbucketnewCloudfrontUrl = CDN_HTTP . $destbucketInfo['cloudfront_url']."/".$destsignedBucketPathForview;   
        
        $destfolderPath = Yii::app()->common->getFolderPath("", $dest_studio);   
        
        $destsignedBucketPath = $destfolderPath['signedFolderPath'];
        $destunsignedFolderPath = $destfolderPath['unsignedFolderPath'];
        $destunsignedFolderPathForVideo = $destfolderPath['unsignedFolderPathForVideo'];
        
        
        $client = Yii::app()->common->connectToAwsS3($dest_studio);
        //source url
        $bucketHttpOldUrl = 'http://' . $sourcebucketName . '.' . $sources3url . '/' . $sourcesignedBucketPath;
        $bucket_oldurl = "s3://" . $sourcebucketName . "/" . $sourcesignedBucketPath;
        $bucket_oldurlunsigned = "s3://" . $sourcebucketName . "/" . $sourceunsignedFolderPath;
        
        //dest url
        $bucketHttpNewUrl = 'http://' . $destbucketName . '.' . $dest3url . '/' . $destsignedBucketPath;
        $bucket_newurl = "s3://" . $destbucketName . "/" . $destsignedBucketPath;
        $bucket_newurlunsigned = "s3://" . $destbucketName . "/" . $destunsignedFolderPath;
        
        $moveData = '';//For ImageGallery copy all images from source folder to dest folder
        $imageGalleryDetails = ImageManagement::model()->findByAttributes(array('studio_id' => $dest_studio));
        if ($imageGalleryDetails) {
            $moveData .= "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "imagegallery/".$source_studio."/ ". $bucket_newurlunsigned . "imagegallery/".$dest_studio." \n";
        }
        
        //Sh file creation 
       
        if ($moveData != '') {
            $file = 'moveFiles_' . $source_studio . '.sh';
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file, 'w') or die('Cannot open file:  ' . $file);
            $cf = 'echo "${file%???}"';
            $file_data = "file=`echo $0`\n" .
                    "cf='" . $file . "'\n\n" .
                    "# create directory\n" .
                    "mkdir /var/www/html/moveData/$source_studio\n" .
                    "chmod 0777 /var/www/html/moveData/$source_studio\n\n" .
                    $moveData .
                    "# remove directory\n" .
                    "rm -rf /var/www/html/moveData/$source_studio\n" .
                    "# remove the process copy file\n" .
                    "rm /var/www/html/moveData/$file\n";
            fwrite($handle, $file_data);
            fclose($handle);
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file;
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));
            
            if ($s3->upload('muvistudio', "moveStudioData/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                echo "Sh file created successfully";
                unlink($filePath);
            }
        }
     exit;
    }
    
    public function actioncopyStorePoster() {
        $copied_studio = $_REQUEST['studio_id'];
        $source_studio = $this->studio->id;
        $sourcebucketInfo = Yii::app()->common->getBucketInfo("", $source_studio);
        $sourcebucketName = $sourcebucketInfo['bucket_name'];
        $sources3url = $sourcebucketInfo['s3url'];
        $s3cfg = $sources3cfg = $sourcebucketInfo['s3cmd_file_name'];
        $sourcefolderPath = Yii::app()->common->getFolderPath("", $source_studio);
        $sourcesignedBucketPath = $sourcefolderPath['signedFolderPath'];
        $sourceunsignedFolderPath = $sourcefolderPath['unsignedFolderPath'];
        $client = Yii::app()->common->connectToAwsS3($copied_studio);
        $bucketHttpOldUrl = 'http://' . $sourcebucketName . '.' . $sources3url . '/' . $sourcesignedBucketPath;
        $bucket_oldurl = "s3://" . $sourcebucketName . "/" . $sourcesignedBucketPath;
        $bucket_oldurlunsigned = "s3://" . $sourcebucketName . "/" . $sourceunsignedFolderPath;
        $destbucketInfo = Yii::app()->common->getBucketInfo("", $copied_studio);
        $destbucketName = $destbucketInfo['bucket_name'];
        $dest3url = $destbucketInfo['s3url'];
        $dests3cfg = $destbucketInfo['s3cmd_file_name'];
        $destsignedBucketPathForview = $destbucketInfo['signedFolderPath'];
        $destbucketnewCloudfrontUrl = CDN_HTTP . $destbucketInfo['cloudfront_url'] . "/" . $destsignedBucketPathForview;
        $destfolderPath = Yii::app()->common->getFolderPath("", $copied_studio);
        $destsignedBucketPath = $destfolderPath['signedFolderPath'];
        $destunsignedFolderPath = $destfolderPath['unsignedFolderPath'];
        $destunsignedFolderPathForVideo = $destfolderPath['unsignedFolderPathForVideo'];
        $bucketHttpNewUrl = 'http://' . $destbucketName . '.' . $dest3url . '/' . $destsignedBucketPath;
        $bucket_newurl = "s3://" . $destbucketName . "/" . $destsignedBucketPath;
        $bucket_newurlForUnsigned = "s3://" . $destbucketName . "/" . $destunsignedFolderPath;
        $bucket_newurlForUnsignedvideo = "s3://" . $destbucketName . "/" . $destunsignedFolderPathForVideo;

        $moveData = '';
        //for films
        $SourcePoster = Poster::model()->findAll('object_id in(select id from films where studio_id="' . $source_studio . '") and object_type="films"');
        $PosterModel = new Poster();
        $Posterarr = array();
        foreach ($SourcePoster as $t) {
            $Posterarr[] = $t->attributes;
        }       
        foreach ($Posterarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $PosterModel->$key1 = $value1;
                if ($key1 == 'object_id') {
                    $sourcefilm = Yii::app()->db->createCommand()->select('id,name')->from('films')->where('id=:id', array(':id' => @$v['object_id']))->queryRow();
                    $destfilm = Film::model()->findByAttributes(array('name' => $sourcefilm['name'], 'studio_id' => $copied_studio));
                    $PosterModel->object_id = $destfilm->id;
                }
            }
            $PosterModel->isNewRecord = true;
            $PosterModel->primaryKey = NULL;
            $PosterModel->save();
            $moveData = $moveData . "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "public/system/posters/" . $v['id'] . "/ " . $bucket_newurlForUnsigned . "public/system/posters/" . $PosterModel->id . "/ \n";
        }       
        $SourcestreamPoster = Poster::model()->findAll('object_id in(select id from movie_streams where studio_id="' . $source_studio . '") and object_type="moviestream"');
        $PosterstreamModel = new Poster();
        $Postestreamrarr = array();
        foreach ($SourcestreamPoster as $t) {
            $Postestreamrarr[] = $t->attributes;
        }
        foreach ($Postestreamrarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                   $PosterstreamModel->$key1 = $value1;
                if ($key1 == 'object_id') {
                    $sourcestream = Yii::app()->db->createCommand()->select('id,episode_title')->from('movie_streams')->where('id=:id', array(':id' => @$v['object_id']))->queryRow();
                    $deststream = movieStreams::model()->findByAttributes(array('episode_title' => $sourcestream['episode_title'], 'studio_id' => $copied_studio));
                    $PosterstreamModel->object_id = $deststream->id;
                }
            }
            $PosterstreamModel->isNewRecord = true;
            $PosterstreamModel->primaryKey = NULL;
            $PosterstreamModel->save();
            $moveData = $moveData . "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "public/system/posters/" . $v['id'] . "/ " . $bucket_newurlForUnsigned . "public/system/posters/" . $PosterstreamModel->id . "/ \n";
        }
        $SourcecelebPoster = Poster::model()->findAll('object_id in(select id from celebrities where studio_id="' . $source_studio . '") and object_type="celebrity"');
        $celebPosterModel = new Poster();
        $celebPosterarr = array();
        foreach ($SourcecelebPoster as $t) {
            $celebPosterarr[] = $t->attributes;
        }
        foreach ($SourcecelebPoster as $key => $v) {
            foreach ($v as $key1 => $value1) {
                $celebPosterModel->$key1 = $value1;
                if ($key1 == 'object_id') {
                    $sourceceleb = Yii::app()->db->createCommand()->select('id,name')->from('celebrities')->where('id=:id', array(':id' => @$v['object_id']))->queryRow();
                    $destceleb = Celebrity::model()->findByAttributes(array('name' => $sourceceleb['name'], 'studio_id' => $copied_studio));
                    $celebPosterModel->object_id = $destceleb->id;
                }
            }
            $celebPosterModel->object_id = $destceleb->id;
            $celebPosterModel->isNewRecord = true;
            $celebPosterModel->primaryKey = NULL;
            $celebPosterModel->save();
            $moveData = $moveData . "/usr/local/bin/s3cmd cp --recursive --config /usr/local/bin/." . $s3cfg . " --acl-public " . $bucket_oldurlunsigned . "public/system/posters/" . $v['id'] . "/ " . $bucket_newurlForUnsigned . "public/system/posters/" . $celebPosterModel->id . "/ \n";
        }

        if ($moveData != '') {
            $file = 'moveposterFiles_' . $source_studio . '.sh';
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file, 'w') or die('Cannot open file:  ' . $file);
            $cf = 'echo "${file%???}"';
            $file_data =$moveData .                  
                    "# remove the process copy file\n" .
                    "rm /var/www/html/moveData/$file\n";
            fwrite($handle, $file_data);
            fclose($handle);
            $filePath = $_SERVER['DOCUMENT_ROOT'] . "/moveStudioData/" . $file;
            $s3 = S3Client::factory(array(
                        'key' => Yii::app()->params->s3_key,
                        'secret' => Yii::app()->params->s3_secret,
            ));

            if ($s3->upload('muvistudio', "moveStudioData/" . $file, fopen($filePath, 'rb'), 'public-read')) {
                echo "Sh file created successfully";
                unlink($filePath);
            }
        }
        exit;
    }
    
    public function actioncopymultiCurrency() {
        $copied_studio = $_REQUEST['studio_id'];
        $source_studio = $this->studio->id;
        $Sourcecur = PGMultiCurrency::model()->findAll('studio_id = :studio_id', array(':studio_id' => $source_studio));

        $Sourcecurrency = new PGMultiCurrency();
        $Sourcecurrencyrarr = array();

        foreach ($Sourcecur as $t) {
            $Sourcecurrencyrarr[] = $t->attributes;
        }
       
        foreach ($Sourcecurrencyrarr as $key => $v) {
            foreach ($v as $key1 => $value1) {
                   $Sourcecurrency->$key1 = $value1;
                if ($key1 == 'product_id') {
                    $sourcepg = Yii::app()->db->createCommand()->select('id,permalink')->from('pg_product')->where('id=:id', array(':id' => @$v['product_id']))->queryRow();
                    $destpg = PGProduct::model()->findByAttributes(array('permalink' => $sourcepg['permalink'], 'studio_id' => $copied_studio));
                    $Sourcecurrency->product_id = $destpg->id;
                }
            }
            $Sourcecurrency->studio_id = $copied_studio;
            $Sourcecurrency->isNewRecord = true;
            $Sourcecurrency->primaryKey = NULL;
            $Sourcecurrency->save();
            }  
    } 

}
?>