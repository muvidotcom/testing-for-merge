<?php
require 's3bucket/aws-autoloader.php';
use Aws\S3\S3Client;
class LanguageController extends Controller
{
    public $layout = 'admin';
    public $headerinfo='';
    protected function beforeAction($action){
        parent::beforeAction($action);
        Yii::app()->theme = 'admin'; 
        if(!(Yii::app()->user->id))
        {
            $this->redirect(array('/index.php/'));
        }else{
             $this->checkPermission();
        }
                
        return true;
    }
    function actionManageLanguage() {
        $this->breadcrumbs=array('Settings','Language', 'Manage Language');
        $this->pageTitle="Muvi | Manage Language";
        $this->headerinfo = "Manage Language";
        $this->layout='admin';
        
        $studio_id = Yii::app()->common->getStudiosId();
        
        $con = Yii::app()->db;
        $sql = "SELECT a.*, s.id AS studio_id, s.default_language FROM 
        (SELECT l.id AS languageid, l.name, l.code, sl.* FROM languages l LEFT JOIN studio_languages sl 
        ON (l.id = sl.language_id AND sl.studio_id={$studio_id}) WHERE l.code='en' OR (sl.studio_id={$studio_id})) AS a, studios s WHERE s.id={$studio_id} 
        ORDER BY FIND_IN_SET(a.code,s.default_language) DESC, FIND_IN_SET(a.code,'en') DESC, a.status DESC, a.name ASC";
        
        $studio_languages = $con->createCommand($sql)->queryAll();
        $this->render('managelanguage', array('studio_languages' => $studio_languages));
    }
    
    function actionGetLanguages() {
        $this->layout=false;
        
        $q = trim($_REQUEST['term']);
        $studio_id = Yii::app()->common->getStudiosId();
        
        $sql = "SELECT * FROM languages WHERE LOWER(code) != 'en' AND id NOT IN
        (SELECT language_id FROM studio_languages WHERE studio_id={$studio_id}) 
        AND (LOWER(name) LIKE '%".strtolower($q)."%' )";
        
        $con = Yii::app()->db;
        $list = $con->createCommand($sql)->queryAll();
        if (isset($list) && empty($list)) {
            $list[0]['id'] = '';
            $list[0]['name'] = 'No Language found!';
            $list[0]['code'] = '';
        }
        
        echo json_encode($list);exit;
    }
    
    function actionSaveLanguage() {
        if (isset($_REQUEST['language_id']) && trim($_REQUEST['language_id'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = New StudioLanguage;
            $slModel->studio_id = $studio_id;
            $slModel->language_id = trim($_REQUEST['language_id']);
            $slModel->status = 1;
            $slModel->created_by = $user_id;
            $slModel->created_date = date('Y-m-d H:i:s');
            $slModel->save();
            
            Yii::app()->user->setFlash('success','Language has been added successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while adding the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionEnableLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->status = 1;
            $slModel->updated_by = $user_id;
            $slModel->updated_date = date('Y-m-d H:i:s');
            $slModel->save();
            Yii::app()->user->setFlash('success','Language has been enabled successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while enabling the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionDisableLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//",$domain);
            $domain = $domain[1];
            if($_COOKIE['Language'] == $code){
                if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                    setcookie('Language', "en", time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            }
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->status = 0;
            $slModel->updated_by = $user_id;
            $slModel->updated_date = date('Y-m-d H:i:s');
            $slModel->save();
            Yii::app()->user->setFlash('success','Language has been disabled successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while disabling the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    function actionFrontendShowStatusOfLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            if(!empty($slModel)){
                $studioLanguage = StudioLanguage::model()->findByPk($slModel->id);
                $studioLanguage->frontend_show_status = $slModel->frontend_show_status == '1' ? '0' : '1';
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }else{
                $studioLanguage = new StudioLanguage;
                $studioLanguage->studio_id = $studio_id;
                $studioLanguage->language_id = 20;
                $studioLanguage->status = 1;
                $studioLanguage->created_by = $user_id;
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->frontend_show_status = '0';
                $studioLanguage->created_date = date('Y-m-d H:i:s');
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }
            Yii::app()->user->setFlash('success','Language show hide status updated.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while disabling the language.');
        }
    
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    function actionMakeprimaryLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language']) && isset($_REQUEST['code']) && trim($_REQUEST['code'])) {
            $user_id = Yii::app()->user->id;
            $studio_id = Yii::app()->common->getStudiosId();
            
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            if (isset($slModel) && !empty($slModel)) {
                $slModel->status = 1;
                $slModel->frontend_show_status = '1';
                $slModel->updated_by = $user_id;
                $slModel->updated_date = date('Y-m-d H:i:s');
                $slModel->save();
            }
            
            $studio = Studio::model()->findByPk($studio_id);
            $studio->default_language = trim($_REQUEST['code']);
            $studio->save();
            
            Yii::app()->user->setFlash('success','Language has been set as primary successfully');
        } else {
            Yii::app()->user->setFlash('error','Oops! Sorry, Language can not be made as primary!');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    }
    
    function actionDeleteLanguage() {
        if (isset($_REQUEST['id_language']) && trim($_REQUEST['id_language'])) {
            $studio_id = Yii::app()->common->getStudiosId();
            $code = $_REQUEST['code'];
            $domain = Yii::app()->getBaseUrl(true);
            $domain = explode("//",$domain);
            $domain = $domain[1];
            if($_COOKIE['Language'] == $code){
                if (strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE) {
                    setcookie('Language', "en", time() + (60 * 60 * 24 * 90), '/', $domain, false, false);
                }
            }
            $slModel = StudioLanguage::model()->findByAttributes(array('language_id'=>$_REQUEST['id_language'],'studio_id'=>$studio_id));
            $slModel->delete();
            Yii::app()->user->setFlash('success','Language has been deleted successfully.');
        } else {
            Yii::app()->user->setFlash('error','Oops! Error while deleting the language.');
        }
        
        $url=$this->createUrl('language/manageLanguage');
        $this->redirect($url);
    } 
    public function updateMainLang(){
        $allkeys        = TranslateKeyword::model()->getAllMessages();
        $file_content   = '<?php return '.var_export($allkeys, true).' ?>';
        $main_language_path = ROOT_DIR.'languages/studio/mainlang.php';
        chmod($main_language_path,0777);
        $file = fopen($main_language_path, "w+");
        chmod(ROOT_DIR."languages",0777);
        chmod(ROOT_DIR."languages/studio",0777);
        chmod($main_language_path,0777);
        file_put_contents($main_language_path, $file_content);
    }
    public function actionTranslation(){
        $this->breadcrumbs  = array('Settings','Language','Translation');
        $this->pageTitle    = "Muvi | Translation";
        $this->headerinfo   = "Translation";
        $this->layout       = 'admin';
        $studio_details     = $this->studio;
        $studio_id          = $studio_details->id;
        $enable_lang        = $this->language_code;
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang))->name;
        $trans_categories   = CHtml::listData(TranslationCategory::model()->findAll(),'id','feature_name');
        $trans_subcats      = TranslationCategory::model()->getAllSubCategories();
        $trans_msg          = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=0 OR (store_keys=:store_keys AND studio_id=:studio_id AND language_id=20)','params'=>array(':store_keys'=>'1',':studio_id'=>$studio_id),'order'=>'trans_key asc'));
        $last_update_date   = TranslationLog::model()->lastUpdateDate($studio_id, $this->language_id);
        $all_msgs = array();
        $theme    = $studio_details->parent_theme;
        $template = Yii::app()->common->getTemplateDetails($theme);
        $features = TranslateKeyword::model()->languageCategory($studio_id);
        $active_features = $features['active_features'];
        $active_sub_features = $features['active_sub_features'];
        foreach($trans_msg as $msgs):
            $trans_word[$msgs->trans_key] = $msgs->trans_value;
            if($msgs->studio_id == 0):
                if(array_key_exists($msgs->trans_category, $trans_categories)):
                    if(in_array($msgs->trans_category, $active_features) && ($msgs->trans_subcategory == 0 || in_array($msgs->trans_category.",".$msgs->trans_subcategory ,$active_sub_features))):
                        $all_msgs[$trans_categories[$msgs->trans_category]][$trans_subcats[$msgs->trans_category][$msgs->trans_subcategory]][] = $msgs;
                    endif;
                else:
                    $all_msgs['General']['Common'][] = $msgs;  
                endif;
            else:
                $all_msgs['Store']['Common'][] = $msgs; 
            endif;
        endforeach;
        $theme = $studio_details->theme;
        $path  = ROOT_DIR."languages/".$theme;
        $lang_config = StudioLanguageConfig::model()->getconfig($studio_id, 'is_new');
        if((!empty($lang_config)) && ($lang_config->config_value == 1)):
            $total_lang_key = 0;
            $enable_lang_words  = array();
            if($this->language_id == 20):
                $total_lang_key = TranslateKeyword::model()->countByAttributes(array('studio_id' => $studio_id, 'language_id' => 20, 'store_keys' => '0'));
            endif;
            if(($this->language_id != 20) || ($total_lang_key)):
                $enable_lang_msg    = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=:studio_id AND language_id=:language_id','params'=>array(':studio_id'=>$studio_id,':language_id'=>$this->language_id)));
                foreach($enable_lang_msg as $lang_msg):
                  $enable_lang_words[$lang_msg->trans_key] = $lang_msg->trans_value;  
                endforeach;
            endif;
            $trans_val = @$enable_lang_words;
        endif;
        if(empty($trans_val)):
            $file_path    = $path."/".$enable_lang.".php"; 
            if(file_exists($file_path)):
                $trans_val = $this->mergeArray($file_path);
            else:
                $trans_val = @$trans_word;
            endif;
        endif;
        $this->render('translation',array('trans_key' => $trans_msg,'trans_val' => $trans_val,'select_lang' => $enable_lang_name,'all_msgs' => $all_msgs,'last_update_date'=>$last_update_date));
    }
    public function actionmakeTranslation(){
        $enable_lang        = $this->language_code;
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang));
        $enable_lang_name   = $enable_lang_name['name'];
        $studio_id          = Yii::app()->user->studio_id;
        $studio_details     = $this->studio;
        $theme              = $studio_details->theme;
        $name               = $studio_details->name;
        $main_language_path = ROOT_DIR.'languages/studio/'.$enable_lang.'.php';
        $path               = ROOT_DIR."languages/".$theme;
        $file_path          = $path."/".$enable_lang.".php";
        if(isset($_POST)){
            $lang_config = StudioLanguageConfig::model()->getconfig($studio_id, 'is_new');
            if(empty($lang_config)):
                $lang_config = new StudioLanguageConfig;
                $lang_config->studio_id = $studio_id;
                $lang_config->config_key = 'is_new';
                $lang_config->config_value = 1;
                $lang_config->save();
            endif;
            if(!@is_dir($path)) {
                chmod(ROOT_DIR."languages",0777);
                @mkdir($path,0777,true);           
                chmod($path,0777);
            }
            $static_message_key   = $_POST['static_message_key'];
            $static_message_value = $_POST['static_message_value'];
            $js_message_key       = $_POST['js_message_key'];
            $js_message_value     = $_POST['js_message_value'];
            $server_message_key   = $_POST['server_message_key'];
            $server_message_value = $_POST['server_message_value'];
            $static_msg       = array_filter(array_map('trim',array_combine($static_message_key, $static_message_value)));
            $static_msg       = is_array($static_msg) ? $static_msg : array();
            $js_msg           = array_filter(array_map('trim',array_combine($js_message_key, $js_message_value)));
            $js_msg           = is_array($js_msg) ? $js_msg : array();
            $server_msg       = array_filter(array_map('trim',array_combine($server_message_key, $server_message_value)));
            $server_msg       = is_array($server_msg) ? $server_msg : array();
            $static_message   = '$static_messages=' . var_export($static_msg, true) . ';';
            $js_message       = '$js_messages=' . var_export($js_msg, true) . ';';
            $server_message   = '$server_messages=' . var_export($server_msg, true) . ';';
            $file_content     = '<?php '.$static_message.$js_message.$server_message.'$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
            chmod($path, 0777);
            if (file_exists($file_path)) {
                chmod($file_path, 0777);
            }
            $file = fopen($file_path, "w+");
            if (fwrite($file, $file_content) === FALSE) {
                echo "Cannot write to the file";
            }else{
                echo "Succesfully write to the file";
                fclose($file);
                chmod($file_path, 0777);
            }
            $trans_word = array_merge($static_msg,$js_msg,$server_msg);
            if(!empty($trans_word)){
                $translate_keyword = new TranslateKeyword;
                foreach($trans_word as $key => $val){
                    $trans_type = '0';
                    if(array_key_exists($key, $js_msg)){
                        $trans_type = '1';
                    }else if(array_key_exists($key, $server_msg)){
                        $trans_type = '2';
                    }
                    $trans_key = $translate_keyword->find('trans_key=:trans_key AND language_id =:language_id AND studio_id = :studio_id', array(':trans_key'=>$key,':language_id'=>$this->language_id,':studio_id'=>$studio_id));
                    if(!empty($trans_key)){
                        $trans_key->trans_value = $val;
                        $trans_key->trans_type = (string)$trans_type;
                        $trans_key->save();
                    }else{
                        $translate_keyword->trans_type = (string)$trans_type;
                        $translate_keyword->trans_key = $key;
                        $translate_keyword->trans_value = $val;
                        $translate_keyword->studio_id = $studio_id;
                        $translate_keyword->language_id = $this->language_id;
                        $translate_keyword->setIsNewRecord(true);
                        $translate_keyword->setPrimaryKey(NULL);
                        $translate_keyword->save();
                    }
                }
                $this->logupdate($path,$file_path,$enable_lang);
            }
        }
    }
    public function logupdate($path,$file_path,$lang_code='en',$action=false){
        if(!$lang_code){
            $lang_code = 'en';
        }
        if(!$action){
            $status = "Added";
            if(file_exists($file_path)){
                $status = "Updated";
            }
        }else{
            $status = $action;
        }
        TranslationLog::model()->removeOldestBackup();
        $backup_file = "backup_".$lang_code."_".date('Ymd-His').".php";
        $backup_folder = $path."/backups";
        if(!file_exists($backup_folder)){
            mkdir($backup_folder,0777,true);
            chmod($backup_folder,0777);
        }
        $backup_lang_folder = $backup_folder."/".$lang_code;
        if(!file_exists($backup_lang_folder)){
            mkdir($backup_lang_folder,0777,true);
            chmod($backup_lang_folder,0777);
        }
        $backup_file_path = $backup_lang_folder."/".$backup_file;
        fopen($backup_file_path, "w+");
        chmod($backup_file_path,0777);
        chmod($file_path,0777);
        copy($file_path, $backup_file_path);
        
        $log = new TranslationLog;
        $log->studio_id = $this->studio->id;
        $log->language_id = $this->language_id;
        $log->action = $status;
        $log->backup_file_name = $backup_file;
        $log->save();
        return true;
    }
    public function actionDeletelangugefile(){
        $language       = $_POST['language_code'];
        $studio_id      = Yii::app()->user->studio_id;
        $studio_details = Studio::model()->findbyPk($studio_id);
        $theme          = $studio_details['theme'];
        $name           = $studio_details['name'];
        $path           = ROOT_DIR."languages/".$theme."/";
        $file_path      = $path."/".$language.".php";
        $log_path       = $path."/log.php";
        $current_file   = $language.".php";
        //$logfile        = $this->logupdate($log_path,$path,$current_file,$name,"Delete");
        if(file_exists($file_path)){
            if(unlink($file_path)){
                echo "This language file deleted from your theme";
            }
        }else{
            echo "File not exist";
        }
    }
    public function mergeArray($filepath = "") {
        $lng = include($filepath);
        $trans_word = array();
        if(!empty(@$lng['static_messages'])){
            $static_messages    = is_array($lng['static_messages'])? $lng['static_messages'] : array();
            $js_messages        = is_array($lng['js_messages'])? $lng['js_messages'] : array();
            $server_messages    = is_array($lng['server_messages'])? $lng['server_messages'] : array();
        }else{
            $static_messages    = is_array($lng)? $lng : array();
            $js_messages        = array();
            $server_messages    = array();
        }
        $trans_word = array_merge($static_messages,$js_messages,$server_messages);
        return $trans_word;
    }
    public function UpdateMainLanguageFile() {
        $studio_id = $this->studio->id;
        $static_msg = TranslateKeyword::model()->getMessages('0');
        $js_msg = TranslateKeyword::model()->getMessages('1');
        $server_msg = TranslateKeyword::model()->getMessages('2');

        $static_message   = '$static_messages=' . var_export($static_msg, true) . ';';
        $js_message       = '$js_messages=' . var_export($js_msg, true) . ';';
        $server_message   = '$server_messages=' . var_export($server_msg, true) . ';';

        $file_content     = '<?php '.$static_message.$js_message.$server_message.'$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
        $main_language_path = ROOT_DIR.'languages/studio/en.php';
        $file = fopen($main_language_path, "w+");
        chmod(ROOT_DIR."languages",0777);
        chmod(ROOT_DIR."languages/studio",0777);
        chmod($main_language_path,0777);
        file_put_contents($main_language_path, $file_content);
    }
    public function actioncheckUniqueKey(){
        $studio_id = $this->studio->id;
        if(isset($_REQUEST['key'])){
            $key = $_REQUEST['key'];
            $keyword = TranslateKeyword::model()->find(array('condition'=>"trans_key='".$key."' AND (studio_id=".$studio_id." OR studio_id=0)"));
            if(!empty($keyword)){
                echo "not unique";exit;
            }else{
                echo "unique";exit;
            }
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionnewKeyword(){
        if($_REQUEST){
            $trans_key  = $_REQUEST['translate_key'];
            $trans_word = $_REQUEST['translate_word'];
            $studio_id  = $this->studio->id;
            $trans =  new TranslateKeyword;
            $trans->trans_key   = $trans_key;
            $trans->trans_value = $trans_word;
            $trans->trans_type  = '0';
            $trans->studio_id   = $studio_id;
            $trans->store_keys  = '1';
            $trans->save();
            echo "success";exit;
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionupdateLanguageName(){
        if(isset($_POST['language_id'])){
            $language_id     = $_POST['language_id'];
            $studio_id       = $this->studio->id;
            $translated_name = $_POST['translated_name'];
            $studio_language = StudioLanguage::model()->findByAttributes(array('studio_id'=>$studio_id,'language_id'=>$language_id));
            if(!empty($studio_language)){
                $studio_language->translated_name = $translated_name;
                $studio_language->save();
            }else{
                $studioLanguage = new StudioLanguage;
                $studioLanguage->studio_id = $studio_id;
                $studioLanguage->language_id = 20;
                $studioLanguage->status = 1;
                $studioLanguage->translated_name = $translated_name;
                $studioLanguage->created_by = $user_id;
                $studioLanguage->updated_by = $user_id;
                $studioLanguage->frontend_show_status = '1';
                $studioLanguage->created_date = date('Y-m-d H:i:s');
                $studioLanguage->updated_date = date('Y-m-d H:i:s');
                $studioLanguage->save();
            }
        }else{
            $url=$this->createUrl('admin/dashboard');
            $this->redirect($url);
        }
    }
    public function actionGetLanguageList() {
        $studio = $this->studio;
        $theme = $studio->theme;
        $studio_id = $studio->id;
        $language_id = $this->language_id;
        $enable_lang = $this->language_code;
        $enable_lang_name   = Language::model()->findByAttributes(array('code'=>$enable_lang))->name;
        $trans_word = array();
        $trans_msg        = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=0 OR (store_keys=:store_keys AND studio_id=:studio_id AND language_id=20)','params'=>array(':store_keys'=>'1',':studio_id'=>$studio_id),'order'=>'trans_key asc'));
        $trans_categories = CHtml::listData(TranslationCategory::model()->findAll(),'id','feature_name');
        $trans_subcats    = TranslationCategory::model()->getAllSubCategories();
        $features = TranslateKeyword::model()->languageCategory($studio_id);
        $active_features = $features['active_features'];
        foreach($trans_msg as $msgs):
            $trans_word[$msgs->trans_key] = $msgs->trans_value;
            if($msgs->studio_id == 0):
                if(array_key_exists($msgs->trans_category, $trans_categories)):
                    if(in_array($msgs->trans_category, $active_features)):
                        $all_msgs[$trans_categories[$msgs->trans_category]][] = $msgs;
                    endif;
                else:
                    $all_msgs['General'][] = $msgs;  
                endif;
            else:
                $all_msgs['Store'][] = $msgs; 
            endif;
        endforeach;
        $list = array();
        $count = 0;
        $lang_config = StudioLanguageConfig::model()->getconfig($studio_id, 'is_new');
        $path = ROOT_DIR . "languages/" . $theme;
        $file_path = $path . "/" . $enable_lang . ".php";
        if((!empty($lang_config)) && ($lang_config->config_value == 1)):
            $enable_lang_msg    = TranslateKeyword::model()->findAll(array('condition'=>'studio_id=:studio_id AND language_id=:language_id','params'=>array(':studio_id'=>$studio_id,':language_id'=>$this->language_id)));
            $enable_lang_words  = array();
            foreach($enable_lang_msg as $lang_msg):
                $enable_lang_words[$lang_msg->trans_key] = $lang_msg->trans_value;  
            endforeach;
            $trans_val = @$enable_lang_words;
        endif;
        if(empty($trans_val)):
            if(file_exists($file_path)):
                $trans_val = $this->mergeArray($file_path);
            else:
                $trans_val = @$trans_word;
            endif;
        endif;
        foreach ($all_msgs as $code => $msgs) {
            $sheetName[$count] = $code;
            $headArr[$count] = array('Language Key', 'Original(English)', 'Translate To '.$enable_lang_name);
            if (!empty($trans_val)) {
                $i = 0;
                foreach ($msgs as $msg) {
                    $translated_value = "";
                    if(array_key_exists($msg->trans_key, $trans_val)){
                        $translated_value = $trans_val[$msg->trans_key];
                    }
                    $list[$count][$i][0] = $msg->trans_key;
                    $list[$count][$i][2] = $msg->trans_value;
                    $list[$count][$i][1] = $translated_value;
                    $i++;
                }
            }
            $count++;
        }
        $type = 'xls';
        $filename = $enable_lang_name.'_'.$enable_lang .'_'. date('Y-m-d-H-i-s');
        $headArr = array_values($headArr);
        $sheetName = array_values($sheetName);
        $data = array_values($list);
        $sheet = count($sheetName);
        Yii::app()->general->getCSV($headArr, $sheet, $sheetName, $data, $filename, $type);
        exit;
    }
    public function csvImportTranslation($source, $enable_lang){
        $studio = $this->studio;
        $theme = $studio->theme;
        $studio_id = $studio->id;
        $path = ROOT_DIR . "languages/" . $theme;
        $file_path = $path . "/" . $enable_lang . ".php";
        $features = TranslateKeyword::model()->languageCategory($studio_id);
        $active_features = $features['active_features'];
        $lists = Yii::app()->general->csvImportWithMultiSheet($source);
        if(!empty($lists)){
            foreach ($lists as $cat_name => $langmsg) {
                foreach ($langmsg as $key => $val) {
                    if($val[2]){
                        $trans_key = $val[0];
                        $trans_type = TranslateKeyword::model()->getLanguageType($trans_key, $studio_id);
                        if ($trans_type == 0) {
                            $static_msg[$val[0]] = $val[2];
                        } elseif ($trans_type == 1) {
                            $js_msg[$val[0]] = $val[2];
                        } elseif ($trans_type == 2) {
                            $server_msg[$val[0]] = $val[2];
                        }else{
                            return "extra_keys";
                        }
                    }
                }
            }
            $static_msg   = is_array($static_msg) ? $static_msg : array();
            $js_msg       = is_array($js_msg) ? $js_msg : array();
            $server_msg   = is_array($server_msg) ? $server_msg : array();
            $writetofile  = $this->WriteToLangFile($static_msg,$js_msg,$server_msg,$path,$file_path,$studio_id);
            $this->logupdate($path,$file_path,$enable_lang,'import');
            $trans_word   = array_merge($static_msg,$js_msg,$server_msg);
            if(!empty($trans_word)){
                $translate_keyword = new TranslateKeyword;
                foreach($trans_word as $key => $val){
                    $trans_type = '0';
                    if(array_key_exists($key, $js_msg)){
                        $trans_type = '1';
                    }else if(array_key_exists($key, $server_msg)){
                        $trans_type = '2';
                    }
                    $trans_key = $translate_keyword->find('trans_key=:trans_key AND language_id =:language_id AND studio_id = :studio_id', array(':trans_key'=>$key,':language_id'=>$this->language_id,':studio_id'=>$studio_id));
                    if(!empty($trans_key)){
                        $trans_key->trans_value = $val;
                        $trans_key->trans_type = $trans_type;
                        $trans_key->save();
                    }else{
                        $translate_keyword->trans_type = $trans_type;
                        $translate_keyword->trans_key = $key;
                        $translate_keyword->trans_value = $val;
                        $translate_keyword->studio_id = $studio_id;
                        $translate_keyword->language_id = $this->language_id;
                        $translate_keyword->setIsNewRecord(true);
                        $translate_keyword->setPrimaryKey(NULL);
                        $translate_keyword->save();
                    }
                }
            }
            if(!$writetofile){
                return false;
            }
       }else{
           return "not_valid"; 
        }
        return true;
    }
    function WriteToLangFile($static_msg,$js_msg,$server_msg,$path,$file_path,$studio_id){
        if(!@is_dir($path)) {
            chmod(ROOT_DIR."languages",0777);
            @mkdir($path,0777,true);           
            chmod($path,0777);
        }
        $static_message = '$static_messages=' . var_export($static_msg, true) . ';';
        $js_message = '$js_messages=' . var_export($js_msg, true) . ';';
        $server_message = '$server_messages=' . var_export($server_msg, true) . ';';

        $file_content = '<?php ' . $static_message . $js_message . $server_message . '$results = array();$results["static_messages"] = $static_messages;$results["js_messages"] = $js_messages;$results["server_messages"] = $server_messages; return $results;';
        chmod($path, 0777);
        if (file_exists($file_path)) {
            chmod($file_path, 0777);
        }
        $file = fopen($file_path, "w+");
        if (fwrite($file, $file_content) === FALSE) {
            return false;
        } else {
            fclose($file);
            chmod($file_path, 0777);
            return true;
        }
    }
    public function actionUploadLangfile() {
        $action = 'error';
        $max_template_size = 20971520; //20MB=20971520B
        $enable_lang = $this->language_code;
        $restricted_files = array('mp4', 'mp3', 'sql', 'php', 'xml', 'doc', 'docx', 'pdf','zip','jpg','png','gif','bmp');
        $message = '<span class="error">Error in uploading</span>';
        if (count($_FILES) && $_FILES['lang_file']['size'] > 0 && !($_FILES['lang_file']['error'])) {
            if ($_FILES['lang_file']['size'] > $max_template_size) {
                $action = 'error';
                $message = '<span class="error">File size must be less than 20MB.</span>';
            } else {
                $original_name  = $_FILES['lang_file']['name'];
                $check_file_ext = explode('.', $original_name);
                foreach($check_file_ext as $ext){
                    if(in_array($ext,$restricted_files)){
                        $ret = array('action' => "error", 'message' => 'Please upload a valid xls file.');
                        echo json_encode($ret);exit;
                    }
                }
                $get_lang_code = explode('_', $original_name);
                if($get_lang_code[1] == $enable_lang){
                    $source  = $_FILES['lang_file']['tmp_name'];  
                    $import  = $this->csvImportTranslation($source, $enable_lang);
                }else{
                    $import  = 'lang_file_error';
                }
                if($import == 1){
                    $action  = "success";
                    $message = "File imported successfully";
                }elseif($import == "worksheet_error"){
                    $action = "error";
                    $message = "Oops! Worksheet name is incorrect.";
                }elseif($import == "notexist"){
                    $action = "error";
                    $message = "Oops! Please make sure all languages in the worksheet are enabled.";
                }elseif($import == "extra_keys"){
                    $action = "error";
                    $message = "Oops! Please remove extra keys from sheet.";
                }elseif($import == "not_valid"){
                    $action = "error";
                    $message = "Oops! Invalid Data";
                }elseif($import == "lang_file_error"){
                    $action = "error";
                    $message = "Oops! Please make sure you are uploading correct language file";
                }else{
                    $action = "error";
                    $message = "Oops! Error while importing translation message.";
                }
            }
        }
        $ret = array('action' => $action, 'message' => $message);
        echo json_encode($ret);
    }
    public function actionImportLangCatFile(){
        $src = ROOT_DIR."translate_keyword.xlsx";
        $lists = Yii::app()->general->csvImportWithMultiSheet($src);
        foreach($lists['translate_keyword'] as $list):
            $trans_key = TranslateKeyword::model()->find('trans_key=:trans_key AND language_id =:language_id AND studio_id = :studio_id', array(':trans_key'=>$list[1],':language_id'=>20,':studio_id'=>0));
            if(!empty($trans_key)){
                echo $list[1]."_".$list[6]."_".$list[7]."<br/>";
                $trans_key->trans_category = trim($list[6]) !="" ? $list[6] : 0;
                $trans_key->trans_subcategory = trim($list[7]) !="" ? $list[7] : 0;
                $trans_key->save();
            }
        endforeach;
    }
}

