<?php
class CustomizeController extends Controller {

    public function init() {
        parent::init();
    }

    public $defaultAction = 'index';
    public $layout = 'main';
    protected function beforeAction($action) {
        parent::beforeAction($action);
        return true;
    }
    public function actionIndex(){
        $this->render("src/index");
    }
}