<?php

//class Policy extends CWidget {

class Policy extends CApplicationComponent {

    protected $response = array();
    protected $stream_id, $studio_id, $user_id, $cur_date, $enableQuery, $ppvSubscripton;

    public function init() {
        //$this->studio_id = Yii::app()->user->studio_id;
        $this->stream_id = '';
        $this->studio_id = Yii::app()->common->getStudiosId();
        $this->user_id = Yii::app()->user->id;
        $this->cur_date = date("Y-m-d H:i:s");
        $this->enableQuery = true;
    }

    /* ############################# START CODING By STREAM ID ############################ */

    function getResolution($stream_id = null) {
        if ($stream_id) {
            if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                }
                return;
            }
        }
        return;
    }

    function resolutionByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                        return self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                    }
                    return;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                }
                return;
            }
        }
        return;
    }

    function viewByStreamId($stream_id = null, $rules_id = null) {
        $no_of_views = '';
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    $no_of_views = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                $no_of_views = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
            }
        }
        $chkSubscription = self::checkUserSubscription();
        if (!empty($chkSubscription['views'])) {
            $no_of_views += $chkSubscription['views'];
        }
        return $no_of_views;
    }

    function watchDurationByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
            }
        }
    }

    function accessDurationByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
                    exit;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
            }
            return;
        }
    }

    function userByStreamId($stream_id = null, $rules_id = null) {
        if ($this->enableQuery) {
            $isEnable = self::isEnabledPolicy($this->studio_id);
            if ($isEnable == 0)
                return;
            if ($stream_id) {
                $checkMonetization = self::checkMonetization($stream_id);
                $this->ppvSubscripton = $checkMonetization;
                if (!$checkMonetization) {
                    return $this->response = self::checkUserSubscription();
                }
                if ($checkMonetization == 'free') {
                    return $this->response;
                } else if ($checkMonetization == 'unpaid') {
                    $response['error'] = true;
                    $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                    return $response;
                }
                $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
                if ($rules = self::getRuleDetails($getPpvPlanDetail['rules_id'])) {
                    return self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                    exit;
                }
            }
        } else {
            if (!$rules_id)
                return;
            if ($rules = self::getRuleDetails($rules_id)) {
                return self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                exit;
            }
            return;
        }
        return;
    }

    function checkMonetization($stream_id = null) {
        $is_ppv_bundle = 0;
        $purchase_type = (isset($_POST['purchase_type'])) ? $_POST['purchase_type'] : Null;
        $season = isset($_POST['season']) ? $_POST['season'] : 0;
        $response = array();
        $is_free = SdkUser::model()->findByPk($this->user_id)->is_free;
        if ((isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin) || (@Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038 || $is_free == 1)) {
            return 'free';
        }
        $movieStream = movieStreams::model()->findByPk($stream_id);
        $film = Film::model()->findByPk($movieStream->movie_id);
        if ($stream_id == 0) {
            $stream_id = self::getStreamId($movie_id, '', '', $studio_id);
        }

        $ppv_plan = Yii::app()->common->getPPVBundle($film->id, $this->studio_id);
        if (isset($ppv_plan->id) && intval($ppv_plan->id)) {
            $is_ppv_bundle = 1;
            $payment_type = $ppv_plan->id;
        } else {
            $ppv = Yii::app()->common->getContentPaymentType($film->content_types_id, $film->ppv_plan_id, $this->studio_id);
            $payment_type = (isset($ppv->id) && intval($ppv->id)) ? $ppv->id : 0;
        }

        /* if no ppv set */
        if ($payment_type == 0) {
            return;
        }
        /* if no ppv set */
        $season_id = $season;
        if (intval($film->content_types_id) == 3 && $purchase_type == 'episode' && $season == 0) {
            $season_id = $movieStream->series_number;
        }

        $freearg['studio_id'] = $this->studio_id;
        $freearg['movie_id'] = $film->id;
        $freearg['season_id'] = $season_id;
        $freearg['episode_id'] = (isset($purchase_type) && ($purchase_type == 'show' || $purchase_type == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
        $freearg['content_types_id'] = $film->content_types_id;
        $monetizations = Yii::app()->general->monetizationMenuSetting($this->studio_id);
        $isFreeContent = 0;
        if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
            $isFreeContent = Yii::app()->common->isFreeContent($freearg);
        }
        if (intval($isFreeContent)) {
            $response['result'] = 'free';
            return $response;
        } else {
            if (trim($film['permalink']) != '0' && trim($movieStream->embed_id) != '0') {
                $purchase_type = 'episode';
            } else if (trim($film->permalink) != '0' && trim($movieStream->embed_id) == '0' && intval($movieStream->series_number)) {
                $purchase_type = 'episode';
            }

            if (intval($film->content_types_id) == 3) {
                $content = "'" . $film->id . ":" . $season_id . ":" . $stream_id . "'";
            } else {
                $content = $film->id;
            }

            $return = 'unpaid';
            $temp_return = 1;
            $expiredAccessability = '';

            $monitization_data = array();
            $monitization_data['studio_id'] = $studio_id = $this->studio_id;
            $monitization_data['user_id'] = $user_id = $this->user_id;
            $monitization_data['movie_id'] = $movie_id = $film->id;
            $monitization_data['season'] = $season_id;
            $monitization_data['stream_id'] = $stream_id;
            $monitization_data['content'] = $content;
            $monitization_data['film'] = $film;
            $monitization_data['purchase_type'] = $purchase_type;

            $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
            $ppv_subscription_data = array();
            /* if (isset($monetization['is_play']) && $monetization['is_play']) {
              return 'allowed';
              } */
            $is_pre_order_content = 0;
            /* if (isset($monetization['monetization']['pre_order']) && trim($monetization['monetization']['pre_order'])) {
              //Is content subscribed from pre-order
              $pre_order_plan = $monetization['monetization_plans']['pre_order'];
              $content_types_id = $pre_order_plan->content_types_id;
              $is_adv_subscribed = self::IsAdvPaid($movie_id, $stream_id, $season_id, $content_types_id, $purchase_type, $studio_id, $user_id);

              if (!empty($is_adv_subscribed)) {
              return $is_adv_subscribed;
              }
              } */
            /* if (isset($monetization['monetization']['subscription_bundles']) && trim($monetization['monetization']['subscription_bundles'])) {
              //Is content subscribed from subscription bundle
              $is_subscription_bundle_subscribed = self::IsBundledsubscriptionPaid($movie_id, $studio_id, $user_id);
              if (!empty($is_subscription_bundle_subscribed)) {
              $is_subscription_bundle_subscribed['ppv_plan_type'] = 'subscription_bundles';
              $is_subscription_bundle_subscribed['monetization'] = $monetization['monetization'];
              return $is_subscription_bundle_subscribed;
              }
              } */
            if (isset($monetization['monetization']['voucher']) && trim($monetization['monetization']['voucher'])) {
                //Is content subscribed by implementing voucher
                $voucher = Yii::app()->common->isVoucherExists($studio_id, $content);
                if ($voucher != 0) {
                    $content_types_id = $film['content_types_id'];
                    $voucher_subscription_data = Yii::app()->common->IsVoucherSubscribed($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);

                    if (!empty($voucher_subscription_data)) {
                        $lastRecord = self::getLastRecord($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);
                        if ($lastRecord) {
                            $lastRecord['monetization'] = $monetization['monetization'];
                            return $lastRecord;
                        }
                    }
                }
            }

            if (isset($monetization['monetization']['ppv_bundle']) && trim($monetization['monetization']['ppv_bundle'])) {
                //Is content subscribed from ppv bundle
                $is_ppv_bundle_subscribed = self::IsBundledPpvPaid($movie_id, $studio_id, $user_id);
                if ($is_ppv_bundle_subscribed) {
                    $is_ppv_bundle_subscribed['monetization'] = $monetization['monetization'];
                    return $is_ppv_bundle_subscribed;
                }
            }

            if (isset($monetization['monetization']['ppv']) && trim($monetization['monetization']['ppv'])) {
                //Is content subscribed from ppv
                $ppv_plan = $monetization['monetization_plans']['ppv'];

                $content_types_id = $ppv_plan->content_types_id;
                $ppv_subscription_data = Yii::app()->common->IsPpvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);
                if (!empty($ppv_subscription_data)) {
                    $lastRecord = self::getLastRecord($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);
                    if ($lastRecord) {
                        $lastRecord['monetization'] = $monetization['monetization'];
                        return $lastRecord;
                    }
                }
            }


            if (empty($ppv_subscription_data)) {
                $response['result'] = 'unpaid';
                $response['monetization'] = $monetization['monetization'];
                return $response;
            }
        }
    }

    ############################ MAIN FUNCTION #################################

    function rules($stream_id = null, $user_id = null) {
        if ($user_id) {
            $this->user_id = $user_id;
        }
        $this->enableQuery = false;
        $this->stream_id = $stream_id;
        $isEnable = self::isEnabledPolicy($this->studio_id);
        if ($isEnable == 0) {
            return $this->response;
        } if ($stream_id) {
            $checkMonetization = self::checkMonetization($stream_id);
            $this->ppvSubscripton = $checkMonetization;
            if (!$checkMonetization) {
                return $this->response = self::checkUserSubscription();
            }
            if (!empty($checkMonetization['result']) && $checkMonetization['result'] == 'free') {
                return $this->response;
            } else if (!empty($checkMonetization['result']) && $checkMonetization['result'] == 'unpaid') {
                $response['error'] = true;
                $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                return $response;
            }
            $getPpvPlanDetail = self::getPpvSubscriptoinPlan($checkMonetization);
            if (empty($getPpvPlanDetail['rules_id'])) {
                return $this->response = self::checkUserSubscription();
            }
            $resolution = self::resolutionByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($resolution) {
                $this->response['resolution'] = $resolution;
            }
            $views = self::viewByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($views) {
                $this->response['views'] = $views;
            }
            $user = self::userByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($user) {
                $this->response['user'] = $user;
            }
            $watchDuration = self::watchDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($watchDuration) {
                $this->response['watch_duration'] = $watchDuration;
            }
            $accessDuration = self::accessDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
            if ($accessDuration) {
                $this->response['access_duration'] = $accessDuration;
            }
            return $this->response;
        }
        return;
    }

    function verifyRules($stream_id, $user_id = null) {
        if ($user_id) {
            $this->user_id = $user_id;
        }
        $countUser = Yii::app()->Controller->getUserDetail($this->user_id);
        if (!$countUser) {
            $response['user']['error'] = true;
            $response['user']['user_id'] = $this->user_id;
            $response['user']['message'] = Yii::app()->Controller->ServerMessage['user_information_not_found'];
            return $response;
        }
        $rulesArr = self::rules($stream_id);
        $response = array();
        if (!empty($rulesArr)) {
            if ($rulesArr['error'] == true) {
                $response = $rulesArr;
            } if (!empty($rulesArr['access_duration'])) {
                $response['access_duration'] = self::validateAccessDuration($rulesArr['access_duration']);
                //$response['access_duration'] = '';
            } if (!empty($rulesArr['watch_duration']) && empty($response['access_duration'])) {
                $response['watch_duration'] = self::validateWatchDuration($rulesArr['watch_duration']);
            } if (!empty($rulesArr['views']) && empty($response['access_duration'])) {
                $response['views'] = self::validateViews($rulesArr['views']);
            } if (!empty($rulesArr['resolution'])) {
                $response['resolution'] = self::validateResolution($rulesArr['resolution']);
            } if (!empty($rulesArr['user'])) {
                $response['user'] = self::validateUser($rulesArr['user']);
            }
        }
        return $response;
    }

    ############################# Rules Validation Start #############################

    function validateResolution($resolution) {
        return $resolution;
    }

    function validateViews($getView) {
        if (!$getView) {
            return;
        }
        $subscription = $this->ppvSubscripton;
        if(!$this->user_id){
            $this->user_id = $subscription['user_id'];
        }
        /* $ppvSubscription['view_restriction'] = $getView;
          $isView = Yii::app()->common->isNumberOfViewsOnContentExceeds($ppvSubscription, $this->stream_id);
          if ($isView == 0) {
          $response['error'] = true;
          $response['totalView'] = $getView;
          $response['message'] = Yii::app()->Controller->ServerMessage['crossed_max_limit_of_watching'];
          return $response;
          } */
        $getData = Yii::app()->db->createCommand()
                /* ->select('IF(watch_status="halfplay" OR watch_status="complete", COUNT(watch_status), 0) AS total_views') */
                ->select('COUNT(a.watch_status) AS total_views')
                ->from('video_logs a')
                /* ->where('a.video_id=:video_id AND a.created_date >= :created_date', array(':video_id' => $stream_id, ':created_date' => $subscription['created_date'])) */
                ->where('a.video_id=:video_id AND a.user_id=:user_id AND a.studio_id=:studio_id AND a.created_date >= :created_date', array(':video_id' => $this->stream_id, ':user_id' => $this->user_id, ':studio_id' => $this->studio_id, ':created_date' => $subscription['created_date']))
                ->queryRow();
        if ($getData) {
            if ($getView <= $getData['total_views']) {
                $response['error'] = true;
                $response['totalView'] = $getView;
                $response['myView'] = $getData['total_views'];
                $response['message'] = Yii::app()->Controller->ServerMessage['crossed_max_limit_of_watching'];
                return $response;
            }
            return;
        }
    }

    function validateUser($userType) {
        if (!$userType) {
            return;
        }
        /* will do later after discuss with gaya */
        //$countUser = SdkUser::model()->count('id=:id AND studio_id=:studio_id AND is_deleted=0');
        if (!$this->user_id) {
            $response['error'] = true;
            $response['userType'] = $userType;
            $response['message'] = 'This user neither registered nor subscribed';
            return $response;
        } if ($userType == 'subscribed') {
            $chkUser = Yii::app()->common->isSubscribed($this->user_id);
            if (!$chkUser) {
                $response['error'] = true;
                $response['userType'] = $userType;
                $response['message'] = Yii::app()->Controller->ServerMessage['activate_subscription_watch_video'];
                return $response;
            }
        }
    }

    function validateWatchDuration($watchDuraion) {
        if (!$watchDuraion) {
            return;
        }
        $subscription = $this->ppvSubscripton;
        $criteria = new CDbCriteria;
        $criteria->select = 't.created_date';
        $criteria->condition = 't.video_id =:stream_id AND user_id=:user_id AND t.created_date >= :created_date';
        $criteria->params = (array(':stream_id' => $this->stream_id, ':user_id' => $this->user_id, ':created_date' => $subscription['created_date']));
        $criteria->order = 't.id ASC';

        $getFirstRow = VideoLogs::model()->find($criteria);
        if (count($getFirstRow)) {
            $seconds = strtotime($getFirstRow['created_date']) + $watchDuraion;
            if ($seconds <= strtotime($this->cur_date)) {
                $response['error'] = true;
                $response['expiryDate'] = date('Y-m-d H:i:s', $seconds);
                $response['watchDuraion'] = $watchDuraion;
                $response['message'] = Yii::app()->Controller->ServerMessage['watch_period_expired'];
                return $response;
            }
            return;
        }
    }

    function validateAccessDuration($accessDuration) {
        if (!$accessDuration) {
            return;
        }
        $getData = $this->ppvSubscripton;
        $seconds = strtotime($getData['created_date']) + $accessDuration;
        if ($seconds <= strtotime($this->cur_date)) {
            $response['error'] = true;
            $response['expiryDate'] = date('Y-m-d H:i:s', $seconds);
            $response['accessDuration'] = $accessDuration;
            $response['message'] = Yii::app()->Controller->ServerMessage['access_period_expired'];
            return $response;
        }
        return;
    }

    ###################### Subscription Start##########################

    function checkUserSubscription($user_id = null) {
        if (!$user_id) {
            $user_id = $this->user_id;
        }
        //$is_subscribed_user = Yii::app()->common->isSubscribed($user_id);
        $userPlan = UserSubscription::model()->with(array('subscription_plan' => array('select' => 'rules_id')))->find(array("select" => 't.id', 'condition' => 't.user_id=:user_id AND t.studio_id=:studio_id AND t.status=1 AND t.is_subscription_bundle != 1', "params" => array(':user_id' => $user_id, ':studio_id' => $this->studio_id)));
        if (empty($userPlan)) {
            return;
        } else {
            if ($userPlan['subscription_plan']['rules_id'] == '' || $userPlan['subscription_plan']['rules_id'] == 0) {
                return;
            } else {
                if ($rules = self::getRuleDetails($userPlan['subscription_plan']['rules_id'])) {
                    if (!empty($rules['policy_rules_mapping']) && $rules['policy_rules_mapping']) {
                        $response = array();
                        $resolution = self::checkPolicyMapping($rules['policy_rules_mapping'], 2);
                        if ($resolution) {
                            $response['resolution'] = $resolution;
                        }
                        $view = self::checkPolicyMapping($rules['policy_rules_mapping'], 1);
                        if ($view) {
                            $response['views'] = $view;
                        }
                        $wd = self::checkPolicyMapping($rules['policy_rules_mapping'], 3);
                        if ($wd) {
                            $response['watch_duration'] = $wd;
                        }
                        $ad = self::checkPolicyMapping($rules['policy_rules_mapping'], 4);
                        if ($ad) {
                            $response['access_duration'] = $ad;
                        }
                        $user = self::checkPolicyMapping($rules['policy_rules_mapping'], 5);
                        if ($user) {
                            $response['user'] = $user;
                        }
                        return $response;
                    }
                    return;
                }
            }
        }
    }

    ###################### Subscription End ##########################

    /* ############################# common function start ####################### */
      function isStudioPolicyPresent($studio_id = null) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $PolicyRulesPresent = PolicyRules::model()->count('studio_id=:studio_id AND status=1', array(':studio_id' => $studio_id));
        return $PolicyRulesPresent;
    }

    function isEnabledPolicy($studio_id = null) {
        if (!$studio_id) {
            $studio_id = Yii::app()->common->getStudiosId();
        }
        $studioConfig = StudioConfig::model()->count('studio_id=:studio_id AND config_key="enable_policy" AND config_value=1', array(':studio_id' => $studio_id));
        return $studioConfig;
    }

    function getRuleDetails($rule_id = null) {
        if (!$rule_id)
            return;
        $rule = PolicyRules::model()->with('policy_rules_mapping')->find('t.id=:id AND t.studio_id=:studio_id AND t.status=1', array(':id' => $rule_id, ':studio_id' => $this->studio_id));
        return $rule;
    }

    function checkPolicyMapping($mappingArr = array(), $master_id = 2) {
        foreach ($mappingArr as $val) {
            if (($master_id == 3 || $master_id == 4) && $val['policy_rules_master_id'] == $master_id) {
                /* 1=Hour, 2=Day, 3=Month, 4=Minute */
                $duration_type = 0;
                if ($val['duration_type'] == 1) {
                    $duration_type = 60 * 60; /* hour */
                } else if ($val['duration_type'] == 2) {
                    $duration_type = 60 * 60 * 24; /* day */
                } else if ($val['duration_type'] == 3) {
                    $duration_type = 60 * 60 * 24 * 30; /* day */
                } else if ($val['duration_type'] == 4) {
                    $duration_type = 60; /* minute */
                }
                //$duration_type = $val['duration_type'] == 1 ? 60 * 60 : ($val['duration_type'] == 2 ? 60 * 60 * 24 : 60 * 60 * 24 * 30); /* convert in second */
                return $val['policy_value'] * $duration_type;
                exit;
            }
            if ($val['policy_rules_master_id'] == $master_id) {
                return $val['policy_value'];
                exit;
            }
        }
        return;
    }

    public function IsBundledsubscriptionPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in subscription Bundled 
        $bundled_content = Yii::app()->db->createCommand()
                ->select('GROUP_CONCAT(subscriptionbundles_plan_id) AS plan_ids')
                ->from("subscriptionbundles_content")
                ->where('content_id =:content_id', array(':content_id' => $content_id))
                ->queryRow();
        $now = Date('Y-m-d H:i:s');
        $command = array();
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {

            $pid = explode(',', $bundled_content['plan_ids']);
            $command = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("user_subscriptions")
                    ->where('user_id =:user_id AND studio_id=:studio_id AND status=1 AND is_subscription_bundle=1', array(':user_id' => $user_id, ':studio_id' => $studio_id))
                    ->andWhere(array('IN', 'plan_id', $pid))
                    ->andWhere('end_date > :end_date', array(':end_date' => $now))
                    ->queryRow();
        }
        return $command;
    }

    public function IsAdvPaid($movie_id = 0, $stream_id = 0, $season = 0, $content_types_id = 1, $purchase_type = Null, $studio_id, $user_id) {
        //Explicitily pass from Rest controller otherwise default
        if (!isset($studio_id)) {
            $studio_id = $this->studio_id;
        }

        if (!isset($user_id)) {
            $user_id = $this->user_id;
        }

        $isPlay = 0;
        $response = array();
        if (isset($content_types_id) && $content_types_id == 3) {
            $stream = new movieStreams();
            $series_number = $stream->findByPk($stream_id)->series_number;
            $season = $series_number ? $series_number : 0;

            $cond = '';
            $condArr = array();

            if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
            } else if (isset($purchase_type) && $purchase_type == "season") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
                $cond = "(season_id=0) AND (episode_id=0)";
            }

            $conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
            $allcondarr = array_merge($conditions, $condArr);

            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND ' . $cond, $allcondarr, array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();

            if (isset($row) && !empty($row)) {
                if ($row['season_id'] == 0 && $row['episode_id'] == 0) {//Purchase entire show before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] == 0) {//Purchase entire season before
                    $isPlay = 1;
                } else if ($row['season_id'] != 0 && $row['episode_id'] != 0) {//Purchase entire episode before
                    $isPlay = 1;
                }
            }
            if (isset($row) && !empty($row) && $isPlay == 1) {
                $response = $row;
            }
        } else {
            $command = Yii::app()->db->createCommand()
                    ->select('season_id, episode_id')
                    ->from('ppv_subscriptions')
                    ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND is_advance_purchase=1 AND season_id=0 AND episode_id=0', array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id), array('order' => 'movie_id, season_id, episode_id'));
            $row = $command->queryRow();

            if (isset($row) && !empty($row) && $isPlay == 1) {
                $response = $row;
            }
        }

        return $row;
    }

    function IsBundledPpvPaid($content_id, $studio_id, $user_id) {
        //Find out the content is in Bundled PPV
        $bundled_content = Yii::app()->db->createCommand()
                ->select('GROUP_CONCAT(ppv_plan_id) AS plan_ids')
                ->from("ppv_advance_content")
                ->where('content_id =:content_id', array(':content_id' => $content_id))
                ->queryRow();

        $now = Date('Y-m-d H:i:s');
        $is_ppv_bundle_subscribed = array();
        if (isset($bundled_content['plan_ids']) && trim($bundled_content['plan_ids'])) {
            $is_ppv_bundle_subscribed = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from("ppv_subscriptions")
                    ->where('ppv_plan_id IN (:ppv_plan_id) AND user_id =:user_id AND studio_id=:studio_id AND status=1  AND end_date > :end_date', array(':ppv_plan_id' => $bundled_content['plan_ids'], ':user_id' => $user_id, ':studio_id' => $studio_id, ':end_date' => $now))
                    ->queryRow();
        }

        return $is_ppv_bundle_subscribed;
    }

    function getPpvSubscriptoinPlan($ppv_subscription = null) {
        $query = array();
        if ($ppv_subscription) {
            if (!empty($ppv_subscription['ppv_plan_type']) && $ppv_subscription['ppv_plan_type'] == 'subscription_bundles') {
                $query = Yii::app()->db->createCommand()
                        ->select('b.rules_id')
                        ->from('user_subscriptions a, subscription_plans b')
                        ->where('a.plan_id=b.id AND a.id=:id', array(':id' => $ppv_subscription['id']))
                        ->order('a.created_date DESC')
                        ->queryRow();
            } else if ($ppv_subscription['is_voucher'] == 1) {
                $query = Yii::app()->db->createCommand()
                        ->select('b.rules_id')
                        ->from('ppv_subscriptions a, voucher b')
                        ->where('a.ppv_plan_id=b.id AND a.id=:id', array(':id' => $ppv_subscription['id']))
                        ->order('a.created_date DESC')
                        ->queryRow();
            } else {
                $query = Yii::app()->db->createCommand()
                        ->select('b.rules_id')
                        ->from('ppv_subscriptions a, ppv_plans b')
                        ->where('a.ppv_plan_id=b.id AND a.id=:id', array(':id' => $ppv_subscription['id']))
                        ->order('a.created_date DESC')
                        ->queryRow();
            }
        }
        return $query;
    }

    function getLastRecord($movie_id = 0, $stream_id = 0, $season = 0, $purchase_type = Null, $studio_id, $user_id, $content_types_id, $is_pre_order_content = 0) {
        if (!$studio_id) {
            $studio_id = $this->studio_id;
        }
        $cond = '';
        $condArr = array();
        if (isset($content_types_id) && $content_types_id == 3) {
            if (isset($purchase_type) && $purchase_type == "episode") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=:episode_id OR episode_id=0)";
                $condArr = array(':season' => $season, ':episode_id' => $stream_id);
            } else if (isset($purchase_type) && $purchase_type == "season") {
                $cond = "(season_id =:season OR season_id=0) AND (episode_id=0)";
                $condArr = array(':season' => $season);
            } else {
                $cond = "(season_id=0) AND (episode_id=0)";
            }
        } else {
            $cond = "season_id=0 AND episode_id=0";
        }

        $conditions = array(':user_id' => $user_id, ':studio_id' => $studio_id, ':movie_id' => $movie_id);
        $allcondarr = array_merge($conditions, $condArr);

        $command = Yii::app()->db->createCommand()
                ->select('*')
                ->from('ppv_subscriptions')
                ->where('studio_id=:studio_id AND user_id=:user_id AND movie_id=:movie_id AND status=1 AND ' . $cond, $allcondarr)
                ->order('created_date DESC, movie_id, season_id, episode_id');
        $vdata = $command->queryRow();

        return $vdata;
    }

    /* ##################### common function end ############################## */

    function getRulesAPI($stream_uniq_id, $user_id, $studio_id = null) {
        if ($studio_id) {
            $this->studio_id = $studio_id;
        }
        if ($user_id && $stream_uniq_id) {
            //$movie_stream = movieStreams::model()->find(array('select'=>'id', 'condition'=>'embed_id=:embed_id', 'params'=>array(':embed_id'=>$stream_uniq_id)));
            $movie_stream = Yii::app()->db->createCommand()
                    ->select('a.id AS stream_id, a.is_episode, a.episode_title,a.episode_number, a.series_number, b.name')
                    ->from('movie_streams a, films b')
                    ->where('a.movie_id=b.id AND a.embed_id=:embed_id', array(':embed_id' => $stream_uniq_id))
                    ->queryRow();
            if (empty($movie_stream)) {
                $res['code'] = 656;
                $res['message'] = 'Incorrect stream id';
                $res['status'] = 'FAILURE';
            } else {
                $stream_id = $movie_stream['stream_id'];
                $rules = self::verifyRules($stream_id, $user_id);
                if (!empty($rules['error']) && $rules['error'] == true) {
                    $rule = $rules;
                    $res['code'] = 657;
                } else if (!empty($rules['access_duration'])) {
                    $rule = $rules['access_duration'];
                    $res['code'] = 658;
                } else if (!empty($rules['watch_duration'])) {
                    $rule = $rules['watch_duration'];
                    $res['code'] = 659;
                } else if (!empty($rules['views'])) {
                    $rule = $rules['views'];
                    $res['code'] = 660;
                } else if (!empty($rules['user'])) {
                    $rule = $rules['user'];
                    $res['code'] = 661;
                } else {
                    $rule = "";
                    $res['code'] = 200;
                    $res['status'] = 'OK';
                }
                $res['name'] = $movie_stream['name'];
                if ($movie_stream['is_episode'] == 1) {
                    if ($movie_stream['episode_title']) {
                        $res['name'] .= ' - ' . $movie_stream['episode_title'];
                    }if ($movie_stream['episode_number']) {
                        $res['episode_number'] = $movie_stream['episode_number'];
                    }if ($movie_stream['episode_number']) {
                        $res['episode_number'] = $movie_stream['episode_number'];
                    }if ($movie_stream['series_number']) {
                        $res['season'] = $movie_stream['series_number'];
                    }
                }
                $res['monetization'] = $this->ppvSubscripton['monetization'];
                $res['rules'] = $rule;
                $res['message'] = "Policy Rules";
            }
        } else {
            $res['code'] = 662;
            $res['message'] = 'User id or Stream id is required';
            $res['status'] = 'FAILURE';
        }
        return $res;
    }

    public function canSeeMovie($arg = array()) {
        //Initialize parameters
        $movie_id = (isset($arg['movie_id'])) ? $arg['movie_id'] : 0;
        $stream_id = (isset($arg['stream_id'])) ? $arg['stream_id'] : 0;
        $season = (isset($arg['season'])) ? $arg['season'] : 0;
        $purchase_type = (isset($arg['purchase_type'])) ? $arg['purchase_type'] : Null;
        $studio_id = (isset($arg['studio_id'])) ? $arg['studio_id'] : Yii::app()->common->getStudiosId();
        if (self::isEnabledPolicy($studio_id) == 0) {
            return;
        }
        if (self::isStudioPolicyPresent($studio_id) == 0) {
            return;
        }
        
        $country = (isset($arg['country'])) ? $arg['country'] : 0;
        $is_monetizations_menu = (isset($arg['is_monetizations_menu'])) ? $arg['is_monetizations_menu'] : 0;
        $user_id = (isset($arg['user_id'])) ? $arg['user_id'] : (isset(Yii::app()->user->id) ? Yii::app()->user->id : 0);
        $title = (isset($arg['title'])) ? $arg['title'] : Null;
        $api_perimission = (isset($arg['api_perimission'])) ? $arg['api_perimission'] : 0;
        $content_type = (isset($arg['content_type'])) ? $arg['content_type'] : 0;
        $film = Film::model()->findByPk($movie_id);
        if ($stream_id == 0) {
            $stream_id = Yii::app()->common->getStreamId($movie_id, '', '', $studio_id);
        }
        $this->stream_id = $stream_id;

        if (isset($user_id) && $user_id > 0) {
            $is_free = SdkUser::model()->findByPk($user_id)->is_free;
            if ($api_perimission) {
                $getApiDetails = Yii::app()->db->createCommand()
                        ->select('e.endpoint as endpoint, e.access_user as access_user,e.access_password access_password, e.api_name api_name, e.api_key api_key, s.studio_id studio_id, s.module module, s.api_type_id api_type_id, s.api_classname classname, s.api_callback api_callback')
                        ->from('external_api_keys e')
                        ->join('studio_api_details s', 'e.id=s.api_type_id')
                        ->where('e.studio_id=:studio_id AND s.module=:module', array(':studio_id' => $studio_id, ':module' => 'playPerimissionChk'))
                        ->queryRow();
                if (!empty($getApiDetails)) {
                    require_once $_SERVER["DOCUMENT_ROOT"] . '/protected/components/clickHereApi.php';
                    $chk = new clickHereApi();
                    $getApiDetails['movie_id'] = $movie_id;
                    $getApiDetails['stream_id'] = $stream_id;
                    $getApiDetails['title'] = $title;
                    $mobile_number = Yii::app()->user->mobile_number;
                    $message = Yii::app()->controller->Language['permission_api_body_msg'];
                    $uniqueID = $chk->randomUniqueKey();
                    if ($purchase_type === 'episode') {
                        $command = Yii::app()->db->createCommand()
                                ->select('episode_title')
                                ->from('movie_streams')
                                ->where('id=:stream_id AND movie_id=:movie_id', array(':stream_id' => $stream_id, ':movie_id' => $movie_id));
                        $getEpisodeTitle = $command->queryRow();
                        $getApiDetails['title'] = $getEpisodeTitle['episode_title'];
                    }
                    $is_play = $chk->doCheckPermissionToPlay($mobile_number, $message, $uniqueID, $getApiDetails);

                    $response = json_decode(substr($is_play['response'], strpos($is_play['response'], '{'), strlen($is_play['response'])), true);
                    //$response['StatusCode'] = 200;
                    if ($response['StatusCode'] == '403') {
                        return 'noaccess';
                    } else if ($response['StatusCode'] == '200') {
                        return 'allowed';
                    }
                    exit;
                }
            }
            //if (isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin && @Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038) {
            if ((isset(Yii::app()->user->is_studio_admin) && Yii::app()->user->is_studio_admin) || (@Yii::app()->user->id == STUDIO_USER_ID || Yii::app()->user->id == 1038 || $is_free == 1)) {
                return 'allowed';
            } else {
                $allwed_in_country = Yii::app()->common->isAllowedInCountry($movie_id, $studio_id, $country);
                if ($allwed_in_country) {

                    $season_id = $season;

                    if (intval($film->content_types_id) == 3 && $purchase_type == 'episode' && $season == 0) {
                        $season_id = movieStreams::model()->findByPk($stream_id)->series_number;
                    }

                    $freearg['studio_id'] = $studio_id;
                    $freearg['movie_id'] = $movie_id;
                    $freearg['season_id'] = $season_id;
                    $freearg['episode_id'] = (isset($arg['purchase_type']) && ($arg['purchase_type'] == 'show' || $arg['purchase_type'] == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
                    $freearg['content_types_id'] = $film->content_types_id;

                    $monetizations = Yii::app()->general->monetizationMenuSetting($studio_id);
                    $isFreeContent = 0;
                    if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 8)) {
                        $isFreeContent = Yii::app()->common->isFreeContent($freearg);
                    }

                    if (intval($isFreeContent)) {
                        //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                        return 'allowed';
                    } else {
                        if (intval($film->content_types_id) == 3) {
                            $content = "'" . $movie_id . ":" . $season_id . ":" . $stream_id . "'";
                        } else {
                            $content = $movie_id;
                        }

                        $return = 'unpaid';
                        $temp_return = 1;
                        $expiredAccessability = '';

                        $monitization_data = array();
                        $monitization_data['studio_id'] = $studio_id;
                        $monitization_data['user_id'] = $user_id;
                        $monitization_data['movie_id'] = $movie_id;
                        $monitization_data['season'] = @$season_id;
                        $monitization_data['stream_id'] = @$stream_id;
                        $monitization_data['content'] = @$content;
                        $monitization_data['film'] = $film;
                        $monitization_data['purchase_type'] = @$purchase_type;

                        $monetization = Yii::app()->common->getMonetizationsForContent($monitization_data);
                        if (isset($monetization['is_play']) && $monetization['is_play']) {
                            //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                            return 'allowed';
                        }
                        $is_pre_order_content = 0;
                        if (isset($monetization['monetization']['pre_order']) && trim($monetization['monetization']['pre_order'])) {
                            //Is content subscribed from pre-order
                            $pre_order_plan = $monetization['monetization_plans']['pre_order'];
                            $content_types_id = $pre_order_plan->content_types_id;
                            $is_adv_subscribed = Yii::app()->common->IsAdvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);

                            if ($is_adv_subscribed > 0) {
                                return 'advancedpurchased';
                            }

                            $temp_return = 0;
                            $is_pre_order_content = 1;
                        }
                        if (isset($monetization['monetization']['subscription_bundles']) && trim($monetization['monetization']['subscription_bundles'])) {
                            //Is content subscribed from subscription bundle
                            $is_subscription_bundle_subscribed = self::IsBundledsubscriptionPaid($movie_id, $studio_id, $user_id);
                            if(!empty($is_subscription_bundle_subscribed)) {
                            //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                              
                              return 'allowed';
                            }
                            $temp_return = 0;
                          }
                        
                        if (isset($monetization['monetization']['voucher']) && trim($monetization['monetization']['voucher'])) {
                            //Is content subscribed by implementing voucher
                            $voucher = Yii::app()->common->isVoucherExists($studio_id, $content);
                            if ($voucher != 0) {
                                $content_types_id = $film->content_types_id;
                                $voucher_subscription_data = Yii::app()->common->IsVoucherSubscribed($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);

                                if (!empty($voucher_subscription_data)) {
                                    if ($is_pre_order_content) {
                                        return 'advancedpurchased';
                                    } else {
                                        $isPlaybackAccess = self::isPlaybackAccessOnContent($voucher_subscription_data, $content_types_id, $stream_id, $purchase_type);
                                        if (!$isPlaybackAccess) {
                                            return;
                                        }
                                        if ($isPlaybackAccess['isContinue']) {
                                            //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                                            return 'allowed';
                                        } else {
                                            if (intval($is_monetizations_menu)) {
                                                $return = 'unpaid';
                                                $expiredAccessability = $isPlaybackAccess['expiredAccessability'];
                                            } else {
                                                return $isPlaybackAccess['expiredAccessability'];
                                            }
                                        }
                                    }
                                }
                            }

                            $temp_return = 0;
                        }
                        
                        /*
                          if (isset($monetization['monetization']['credit']) && trim($monetization['monetization']['credit'])) {
                          //Is content subscribed by implementing voucher
                          $credit = Yii::app()->common->isCreditExists($studio_id,$content);
                          if($credit != 0){
                          $content_types_id = $film->content_types_id;
                          $credit_subscription_data = Yii::app()->common->IsCreditSubscribed($movie_id, $stream_id, $season_id, $purchase_type, $studio_id, $user_id, $content_types_id, $is_pre_order_content);

                          if (!empty($credit_subscription_data)) {
                          if ($is_pre_order_content) {
                          return 'advancedpurchased';
                          } else {
                          $isPlaybackAccess = self::isPlaybackAccessOnContent($credit_subscription_data, $content_types_id, $stream_id, $purchase_type);
                         *               if(!$isPlaybackAccess){
                          return;
                          }

                          if ($isPlaybackAccess['isContinue']) {
                          return 'allowed';
                          } else {
                          if (intval($is_monetizations_menu)) {
                          $return = 'unpaid';
                          $expiredAccessability = $isPlaybackAccess['expiredAccessability'];
                          } else {
                          return $isPlaybackAccess['expiredAccessability'];
                          }
                          }
                          }
                          }
                          }

                          $temp_return = 0;
                          } */
                         
                        if (isset($monetization['monetization']['ppv_bundle']) && trim($monetization['monetization']['ppv_bundle'])) {
                            //Is content subscribed from ppv bundle
                            $is_ppv_bundle_subscribed = self::IsBundledPpvPaid($movie_id, $studio_id, $user_id);
                            if ($is_ppv_bundle_subscribed) {
                                $is_ppv_bundle_subscribed['monetization'] = $monetization['monetization'];
                                return $is_ppv_bundle_subscribed;
                            }
                            $temp_return = 0; 
                        }

                        if (isset($monetization['monetization']['ppv']) && trim($monetization['monetization']['ppv'])) {
                            //Is content subscribed from ppv
                            $ppv_plan = $monetization['monetization_plans']['ppv'];

                            $content_types_id = $ppv_plan->content_types_id;
                            $ppv_subscription_data = Yii::app()->common->IsPpvPaid($movie_id, $stream_id, $season, $content_types_id, $purchase_type, $studio_id, $user_id);

                            if (!empty($ppv_subscription_data)) {
                                $isPlaybackAccess = self::isPlaybackAccessOnContent($ppv_subscription_data, $content_types_id, $stream_id, $purchase_type);
                                if (!$isPlaybackAccess) {
                                    return;
                                }
                                if ($isPlaybackAccess['isContinue']) {
                                    //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                                    return 'allowed';
                                } else {
                                    if (intval($is_monetizations_menu)) {
                                        $return = 'unpaid';
                                        $expiredAccessability = $isPlaybackAccess['expiredAccessability'];
                                    } else {
                                        return $isPlaybackAccess['expiredAccessability'];
                                    }
                                }
                            }
                            $temp_return = 0;
                        }

                        if (intval($temp_return)) {
                            $return = "";
                        }

                        if ($return == 'unpaid') {
                            if (intval($is_monetizations_menu)) {
                                $res = $monetization['monetization'];
                                if (trim($expiredAccessability)) {
                                    $res['playble_error_msg'] = $expiredAccessability;
                                }
                                return $res;
                            } else {
                                return $return;
                            }
                        } else {
                            if (isset($monetizations['menu']) && !empty($monetizations['menu']) && ($monetizations['menu'] & 1)) {
                                $isSubscribed = Yii::app()->common->isSubscribedContent($studio_id, $user_id, $stream_id);
                                if ($isSubscribed == 'allowed') {
                                    //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                                }
                                return $isSubscribed;
                            } else {
                                //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                                return 'allowed';
                            }
                        }
                    }
                } else {
                    return 'limitedcountry';
                }
            }
        } else {
            $std_config = StudioConfig::model()->getConfig($studio_id, 'free_content_login');
            if (isset($std_config) && !empty($std_config)) {
                $free_content_login = $std_config->config_value;
            } else {
                $free_content_login = 1;
            }
            $freearg['studio_id'] = $studio_id;
            $freearg['movie_id'] = $movie_id;
            $freearg['season_id'] = $season_id;
            $freearg['episode_id'] = (isset($arg['purchase_type']) && ($arg['purchase_type'] == 'show' || $arg['purchase_type'] == 'season') && intval($film->content_types_id) == 3) ? 0 : $stream_id;
            $freearg['content_types_id'] = $film->content_types_id;

            $isFreeContent = Yii::app()->common->isFreeContent($freearg);
            if (intval($user_id) > 0) {
                if (intval($isFreeContent) == 1) {
                    //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                    return 'allowed';
                } else {
                    return 'noaccess';
                }
            } else {
                if ($free_content_login == 0 && intval($isFreeContent) == 1) {
                    //Yii::app()->common->setSessionForContentAuthorization($studio_id, $stream_id);
                    return 'allowed';
                } else {
                    return 'noaccess';
                }
            }
        }
    }

    function isPlaybackAccessOnContent($ppv_subscription_data, $content_types_id = 0, $stream_id = 0, $purchase_type = null) {
        $this->stream_id = $stream_id;
        $this->enableQuery = false;
        $return = true;
        $expiredAccessability = "";
        $result = array();

        $isEnable = self::isEnabledPolicy($this->studio_id);
        if ($isEnable == 0) {
            return;
        }

        $getPpvPlanDetail = self::getPpvSubscriptoinPlan($ppv_subscription_data);
        if (empty($getPpvPlanDetail['rules_id'])) {
            return;
        }
        $limit_video = self::viewByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $watch_period = self::watchDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $access_period = self::accessDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);

        if (trim($access_period) && trim($watch_period) && intval($limit_video)) {//111
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $watch_period);
                    $expiredAccessability = 'watch_period';
                    if ($return) {
                        $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $limit_video);
                        $expiredAccessability = 'maximum';
                    }
                }
            }
        } else if (trim($access_period) && trim($watch_period) && intval($limit_video) == 0) {//110
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $watch_period);
                    $expiredAccessability = 'watch_period';
                }
            }
        } else if (trim($access_period) && trim($watch_period) == '' && intval($limit_video)) {//101
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = 0;
                    $expiredAccessability = 'already_purchased';
                }
            } else {
                $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
                $expiredAccessability = 'access_period';
                if ($return) {
                    $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $limit_video);
                    $expiredAccessability = 'maximum';
                }
            }
        } else if (trim($access_period) && trim($watch_period) == '' && intval($limit_video) == 0) {//100
            $return = self::isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period);
            $expiredAccessability = 'access_period';
        } else if (trim($access_period) == '' && trim($watch_period) && intval($limit_video)) {//011
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $watch_period);
                $expiredAccessability = 'watch_period';
                if ($return) {
                    $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $limit_video);
                    $expiredAccessability = 'maximum';
                }
            }
        } else if (trim($access_period) == '' && trim($watch_period) && intval($limit_video) == 0) {//010
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isWatchPeriodOnContentExceeds($ppv_subscription_data, $watch_period);
                $expiredAccessability = 'watch_period';
            }
        } else if (trim($access_period) == '' && trim($watch_period) == '' && intval($limit_video)) {//001
            if ((isset($content_types_id) && $content_types_id == 3) && isset($purchase_type) && ($purchase_type == "show" || $purchase_type == "season")) {
                $return = 0;
                $expiredAccessability = 'already_purchased';
            } else {
                $return = self::isNumberOfViewsOnContentExceeds($ppv_subscription_data, $limit_video);
                $expiredAccessability = 'maximum';
            }
        }
        $result['isContinue'] = $return;
        $result['expiredAccessability'] = $expiredAccessability;
        return $result;
    }

    public function isAccessPeriodOnContentExceeds($ppv_subscription_data, $access_period = 0) {
        $purchased_date = strtotime($ppv_subscription_data['created_date']);
        $content_access_period = $purchased_date + $access_period;
        if ($content_access_period <= strtotime($this->cur_date)) {
            return false;
        } else {
            return true;
        }
    }

    public function isWatchPeriodOnContentExceeds($ppv_subscription_data, $watch_period = 0, $stream_id=null) {
        $return = true;
        if($stream_id){
            $this->stream_id=$stream_id;
        }
        if (trim($watch_period)) {
            $data = $ppv_subscription_data;
            $data['stream_id'] = $this->stream_id;
            $first_watched_date = self::getFirstWatchedDateAndTimeOfContent($data);
            if (isset($first_watched_date) && trim($first_watched_date) && date('Y-m-d', $first_watched_date) != '0000-00-00' && date('Y-m-d', $first_watched_date) != '1970-01-01') {
                $watch_period_expiry = $first_watched_date + $watch_period;
                if ($watch_period_expiry <= strtotime($this->cur_date)) {
                    $return = false;
                }
            }
        }
        return $return;
    }

    public function isNumberOfViewsOnContentExceeds($ppv_subscription_data, $view_restriction, $stream_id=null) {
        $studio_id = @$ppv_subscription_data['studio_id'];
        $user_id = @$ppv_subscription_data['user_id'];
        $movie_id = @$ppv_subscription_data['movie_id'];

        $purchased_date = $ppv_subscription_data['created_date'];
        $todayDate = $this->cur_date;
        if($stream_id){
            $this->stream_id=$stream_id;
        }

        $connection = Yii::app()->db;
        $cond = "studio_id= {$studio_id} AND user_id = {$user_id} AND movie_id = {$movie_id} AND video_id = '{$this->stream_id}' AND (created_date >= '" . $purchased_date . "' AND created_date <= '" . $todayDate . "')";
        $sql = "SELECT COUNT(id) AS tot FROM video_logs WHERE {$cond} ";
        $log_obj = $connection->createCommand($sql);
        $log_data = $log_obj->queryRow();
        $watched = @$log_data['tot'];
        /* $chkSubscription = self::checkUserSubscription();
          if (!empty($chkSubscription['views'])) {
          $view_restriction += $chkSubscription['views'];
          } */
        if ($view_restriction > 0 && $watched >= $view_restriction) {
            return false;
        } else {
            return true;
        }
    }

    function getFirstWatchedDateAndTimeOfContent($arg = array()) {
        $first_watched_date = '';

        if (isset($arg) && !empty($arg)) {
            $studio_id = @$arg['studio_id'];
            $user_id = @$arg['user_id'];
            $movie_id = @$arg['movie_id'];
            $stream_id = @$arg['stream_id'];

            $purchased_date = $arg['created_date'];
            $todayDate = $this->cur_date;
            $connection = Yii::app()->db;
            $cond = "studio_id= {$studio_id} AND user_id = {$user_id} AND movie_id = {$movie_id} AND video_id = {$stream_id} AND created_date >= '" . $purchased_date . "' AND created_date <= '" . $todayDate . "'";
            $sql = "SELECT MIN(created_date) AS created_date FROM video_logs WHERE {$cond}"; //Get first watch date and time
            $log_obj = $connection->createCommand($sql);
            $log_data = $log_obj->queryRow();

            $first_watched_date = strtotime(@$log_data['created_date']);
        }

        return $first_watched_date;
    }

    /* ####################### FOR CRON JOB ################################### */
    public function check_validity_for_single_part_content($subscription_data = array(), $stream_id = 0) {
        if($stream_id){
            $this->stream_id = $stream_id;
        }
        $this->enableQuery = false;
        $this->studio_id = $subscription_data['studio_id'];
        $this->user_id = $subscription_data['user_id'];
        $isEnable = self::isEnabledPolicy($this->studio_id);
        if ($isEnable == 0) {
            return;
        }

        $getPpvPlanDetail = self::getPpvSubscriptoinPlan($subscription_data);
        if (empty($getPpvPlanDetail['rules_id'])) {
            return;
        }
        $data['id'] = $subscription_data['id'];
        $data['view_restriction'] = self::viewByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $data['watch_period'] = self::watchDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $data['access_period'] = self::accessDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        
        if ($data['access_period'] != '' && $data['watch_period'] != '' && $data['view_restriction'] != 0) {//111
            $return = self::isAccessPeriodOnContentExceeds($subscription_data, $data['access_period']);
            if ($return) {
                $return = self::isWatchPeriodOnContentExceeds($subscription_data, $data['watch_period']);
                if ($return) {
                    $return = self::isNumberOfViewsOnContentExceeds($subscription_data, $data['view_restriction']);
                    if (!$return) {
                        PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
                    }
                } else {
                    PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
                }
            } else {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] != '' && $data['watch_period'] != '' && $data['view_restriction'] == 0) {//110
            $return = self::isAccessPeriodOnContentExceeds($subscription_data, $data['access_period']);
            if ($return) {
                $return = self::isWatchPeriodOnContentExceeds($subscription_data, $data['watch_period']);
                if (!$return) {
                    PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
                }
            } else {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] != '' && $data['watch_period'] == '' && $data['view_restriction'] != 0) {//101
            $return = self::isAccessPeriodOnContentExceeds($subscription_data, $data['access_period']);
            if ($return) {
                $return = self::isNumberOfViewsOnContentExceeds($subscription_data, $data['view_restriction']);
                if (!$return) {
                    PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
                }
            } else {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] != '' && $data['watch_period'] == '' && $data['view_restriction'] == 0) {//100
            $return = self::isAccessPeriodOnContentExceeds($subscription_data, $data['access_period']);
            if (!$return) {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] == '' && $data['watch_period'] != '' && $data['view_restriction'] != 0) {//011
            $return = self::isWatchPeriodOnContentExceeds($subscription_data, $data['watch_period']);
            if ($return) {
                $return = self::isNumberOfViewsOnContentExceeds($subscription_data, $data['view_restriction']);
                if (!$return) {
                    PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
                }
            } else {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] == '' && $data['watch_period'] != '' && $data['view_restriction'] == 0) {//010
            $return = self::isWatchPeriodOnContentExceeds($subscription_data, $data['watch_period']);
            if (!$return) {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        } else if ($data['access_period'] == '' && $data['watch_period'] == '' && $data['view_restriction'] != 0) {//001
            $return = self::isNumberOfViewsOnContentExceeds($subscription_data, $data['view_restriction']);
            if (!$return) {
                PpvSubscription::model()->update_status_of_ppv_subscription_multi($data['id']);
            }
        }
    }
    public function setppv_data($data, $stream_id=null) {
        
        if($stream_id){
            $this->stream_id = $stream_id;
        }
        $this->enableQuery = false;
        $this->studio_id = $data['studio_id'];
        $this->user_id = $data['user_id'];
        $isEnable = self::isEnabledPolicy($this->studio_id);
        if ($isEnable == 0) {
            return;
        }

        $getPpvPlanDetail = self::getPpvSubscriptoinPlan($data);
        if (empty($getPpvPlanDetail['rules_id'])) {
            return;
        }
        $data['view_restriction'] = self::viewByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $data['watch_period'] = self::watchDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        $data['access_period'] = self::accessDurationByStreamId($stream_id, $getPpvPlanDetail['rules_id']);
        
        $subscription_data['user_id'] = $data['user_id'];
        $subscription_data['movie_id'] = $data['movie_id'];
        $subscription_data['studio_id'] = $data['studio_id'];
        $subscription_data['created_date'] = $data['created_date'];
        $subscription_data['access_period'] = $data['access_period'];
        $subscription_data['watch_period'] = $data['watch_period'];
        $subscription_data['view_restriction'] = $data['view_restriction'];
        //echo "<pre>"; print_r($subscription_data); exit;
        return $subscription_data;
    }

}
