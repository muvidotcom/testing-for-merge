<?php

class DefaultController extends Controller{
  
	/**
	 * Displays sitemap in XML or HTML format,
	 * depending on the value of $format parameter
	 * @param string $format
	 */
	public function actionIndex($format = 'xml'){
          
		if ($this->getModule()->actions){        
            $urls = $this->getModule()->getSpecifiedUrls();
		}
                 
		if(SUB_DOMAIN!='studio' && SUB_DOMAIN!='devstudio' ){
			$con = Yii::app()->db;
			$studio_id = Yii::app()->common->getStudiosId();
						
			//Login, Register, forgot password urls
			$this->getModule()->addUrl($urls,'user/login');
			$this->getModule()->addUrl($urls,'user/forgot');
			$this->getModule()->addUrl($urls,'user/register');
			
			$urlRouts = UrlRouting::model()->findAll('studio_id='.$studio_id);
			foreach($urlRouts AS $key=>$val){
				$this->getModule()->addUrl($urls,$val['permalink'],array(),'','1',@$val['created_date']);
			}
			
			$sql = "SELECT m.permalink,ms.movie_id,m.content_types_id,DATE(m.created_date) AS created_date FROM films m, movie_streams ms WHERE ms.movie_id=m.id AND ms.studio_id=".Yii::app()->common->getStudiosId()."  AND is_episode=0 ";
			$data = $con->createCommand($sql)->queryAll();
			
			if($data){
				$movie_ids ='';
				foreach($data AS $key=>$val){
					//$this->getModule()->addUrl($urls,$val['permalink'],array(),'','1',$val['created_date']);
					$movie_ids .=$val['movie_id'].","; 
				}
				$movie_ids = trim($movie_ids,',');
				$castsql = "SELECT celeb.permalink,MovieCast.celebrity_id,DATE(MovieCast.created_date) AS  created_date FROM movie_casts MovieCast,celebrities celeb WHERE MovieCast.celebrity_id= celeb.id AND MovieCast.movie_id IN(".$movie_ids.")  GROUP BY MovieCast.celebrity_id,celeb.permalink ";
				$castList = $con->createCommand($castsql)->queryAll();
				if($castList){
					foreach($castList AS $k=>$v){
						$createdDt = date('Y-m-d',strtotime($v['created_date']));
						$this->getModule()->addUrl($urls,'star/'.$v['permalink'],array(),'','1',$createdDt);
					}
				}
			}
			
		}else{
            $this->getModule()->addUrl($urls);
            $this->getModule()->addUrl($urls, 'signup');
            $this->getModule()->addUrl($urls, 'signup/signupTerms');
           // $this->getModule()->addUrl($urls, 'sdk/pricing_calculator');
            spl_autoload_unregister(array('YiiBase', 'autoload'));
            //define('WP_USE_THEMES', false);
            //global $wp, $wp_query, $wp_the_query;
            //require('wordpress/wp-blog-header.php');

            require('wpstudio/wp-load.php');

            spl_autoload_register(array('YiiBase', 'autoload'));
            // Get all category url and add them to the sitemap 
            $format_cat = array();
            $args = array(
                'taxonomy' => 'category',
                'orderby' => 'name',
                'pad_counts' => false,
                'exclude' => array(1353,1289),
                'hide_empty' => 0
            );
            $all_cat = get_categories($args);
            foreach ($all_cat as $ck => $cv) {
                $format_cat[$cv->category_parent][$cv->cat_ID] = $cv;
            }
            foreach ($format_cat[0] as $ck => $cv) :
                if (isset($format_cat[$ck])):
                    $parent_info = $format_cat[0][$ck];
                    $this->getModule()->addUrl($urls, 'category/' . $parent_info->category_nicename);
                    foreach ($format_cat[$ck] as $cik => $civ) :
                        $this->getModule()->addUrl($urls, 'category/'. $parent_info->category_nicename.'/'. $civ->category_nicename);
                    endforeach;
                else:
                    $this->getModule()->addUrl($urls, 'category/' . $cv->category_nicename);
                endif;
            endforeach;
            
            // Get all post url and add them to the sitemap 
            //$posts = new WP_Query('post_type=any&posts_per_page=-1&post_status=publish');
            $posts = new WP_Query( array(
                'post_type' => array('post','page','help','byod','agreements','partneragreement'),
                'orderby' => 'title',
                'order' => 'ASC', 
                'posts_per_page' => -1,
                'post_status' => 'publish'
            )
            );
            $posts = $posts->posts;
            foreach ($posts as $post) {
                switch ($post->post_type) {
                    case 'revision':
                    case 'nav_menu_item':
                        break;
                    case 'page':
                        $permalink = get_page_link($post->ID);
                        break;
                    case 'post':
                        $permalink = get_permalink($post->ID);
                        break;
                    case 'attachment':
                        $permalink = get_attachment_link($post->ID);
                        break;
                    default:
                        $permalink = get_post_permalink($post->ID);
                        break;
                }
                $dt = get_the_date('Y-m-d', $post->ID);
                $this->getModule()->addUrl($urls, $permalink, '', '', 0, $dt);
            }
            //Get all tags 
            $tags = get_tags($arg);
            foreach ($tags AS $k => $v) {
                $this->getModule()->addUrl($urls, 'tag/' . $v->slug);
            }
        }
        if ($format == 'xml') {
            if (!headers_sent()) {
                ob_clean();
                ob_flush();
                header('Content-Type: text/xml');
            }
            $this->renderPartial('xml', array('urls' => $urls));
        } else {
            $this->render('html', array('urls' => $urls));
        }
    }

    public function actionNewsSitemap($format = 'xml') {

        $this->layout = false;
        $postArrya = array();
        $qry = "select distinct WP.id,WP.post_title,WP.post_modified from wp_posts WP join wp_term_relationships WTR join wp_term_taxonomy WTT join wp_terms WT where WTR.term_taxonomy_id=WTT.term_taxonomy_id and WP.id=WTR.object_id and WTT.taxonomy = 'category' and WT.term_id=WTT.term_id and WP.post_status = 'publish'";
        $postArrya = Yii::app()->db->createCommand($qry)->queryAll();
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        //define('WP_USE_THEMES', false);
        //global $wp, $wp_query, $wp_the_query;
        //require('wordpress/wp-blog-header.php');
        require('wordpress/wp-load.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
        $i = 0;
        $siteDetails = array();
        foreach ($postArrya as $key => $value) {
            $siteDetails[$i]['url'] = get_permalink($value['id']);
            $siteDetails[$i]['lastmod'] = date("Y-m-d", strtotime($value["post_modified"]));
            $siteDetails[$i]['title'] = $value["post_title"];
            $i++;
        }
        header('Content-Type: text/xml');
        $this->renderPartial('newssitemap', array('urls' => $siteDetails));
    }

    public function actionMrssFeed() {
        $this->layout = false;
        $error = 0;
        if (isset($_REQUEST['mrss_id']) && $_REQUEST['mrss_id'] != '') {
            $studio_id = Studio::model()->getStudioDetailsFromMrssCode($_REQUEST['mrss_id']);
        } else {
            $error++;
        }
        if (isset($_REQUEST['feed_id']) && intval($_REQUEST['feed_id'])) {
            $mrssFeedExport = MrssFeedExport::model()->findByAttributes(array('studio_id' => $studio_id, 'id' => $_REQUEST['feed_id']));
            if ($mrssFeedExport) {
                $title = $mrssFeedExport['title'];
                $streamIds = $mrssFeedExport['stream_ids'];
            } else {
                $error++;
            }
        } else {
            $error++;
        }
        if ($error != 0) {
            Yii::app()->user->setFlash('error', 'Invalid Url.');
            $redirect_url = Yii::app()->getBaseUrl(true);
            $this->redirect($redirect_url);
        }
        $studioName = Studio::model()->getStudioName($studio_id);
        $bucketinfo = Yii::app()->common->getBucketInfo('', $studio_id);
        $folderpath = Yii::app()->common->getFolderPath('', $studio_id);
        $videoUrl = 'http://' . $bucketinfo['bucket_name'] . '.' . $bucketinfo['s3url'] . '/' . $folderpath['signedFolderPath'] . 'uploads/movie_stream/full_movie/';
        $postUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
        $movieDetails = array();
        $qry = "select f.id as film_id,f.name,f.story,ms.id,ms.full_movie,ms.episode_number,ms.episode_title,ms.is_episode,ms.episode_story,ms.series_number,ms.full_movie from films f, movie_streams ms where f.id=ms.movie_id and f.studio_id=" . $studio_id . " and ms.id IN (" . $streamIds . ") order by f.id desc";
        $movieDetails = Yii::app()->db->createCommand($qry)->queryAll();
        if ($movieDetails) {
            foreach ($movieDetails as $movieDetailsKey => $movieDetailsVal) {
                //For Episode
                if ($movieDetailsVal['is_episode'] == 1) {
                    $psql = "SELECT id,poster_file_name FROM posters WHERE object_id =" . $movieDetailsVal['id'] . " AND object_type='moviestream' ";
                } else {
                    $psql = "SELECT id,poster_file_name FROM posters WHERE object_id =" . $movieDetailsVal['film_id'] . " AND object_type='films' ";
                }
                $sposter = Yii::app()->db->createCommand($psql)->queryAll();
                if (isset($sposter[0])) {
                    $movieDetails[$movieDetailsKey]['poster'] = $sposter[0];
                } else {
                    $movieDetails[$movieDetailsKey]['poster'] = "";
                }
            }
        }
        //print_r($movieDetails);
        if (!headers_sent()) {
            ob_clean();
            ob_flush();
            header('Content-Type: text/xml');
        } else {
            header('Content-Type: text/xml');
        }
        $this->renderPartial('mrssfeed', array('movieDetails' => $movieDetails, 'studioName' => $studioName, 'videoUrl' => $videoUrl, 'postUrl' => $postUrl, 'title' => $title));
    }

}