<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
<?php foreach ($urls as $url => $data) : ?>
<url>
	<loc><?php echo htmlspecialchars($data['url']) ?></loc>
        <news:news>
                <news:publication>
                    <news:name>Muvi</news:name>
                    <news:language>en</news:language>
                </news:publication>
<?php if (isset($data['lastmod'])) : ?>
	<news:publication_date><?php echo $data['lastmod'] ?></news:publication_date>
<?php endif; ?>
<?php if (isset($data['title'])) : ?>
	<news:title><?php echo htmlspecialchars($data['title']); ?></news:title>
<?php endif; ?>
        </news:news>
</url>
<?php endforeach; ?>
</urlset> 

