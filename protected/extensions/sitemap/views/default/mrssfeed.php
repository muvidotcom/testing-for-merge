<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<rss version = "2.0" xmlns:media = "http://search.yahoo.com/mrss/">
    <channel>
        <title><?php echo htmlspecialchars($title);?></title>
        <link><?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?></link>
        <description><?php echo htmlspecialchars($studioName);?> Media RSS</description>
        <?php foreach ($movieDetails as $movieDetailsKey => $movieDetailsVal) { ?>
<item>
        <?php
            $title = '';
            if($movieDetailsVal['is_episode'] == 1){
                $title = $movieDetailsVal['name'].'- Series:'.$movieDetailsVal['series_number'].'- Episode:'.$movieDetailsVal['episode_number'].'- Title:'.$movieDetailsVal['episode_title'];
            } else{
                $title = $movieDetailsVal['name'];
            }
            $description = '';
            if($movieDetailsVal['is_episode'] == 1  && $movieDetailsVal['episode_story'] != ''){
                    $description = htmlspecialchars($movieDetailsVal['episode_story']);
            } else{
                if($movieDetailsVal['story'] != ''){
                    $description = $movieDetailsVal['story'];
                }
            }
        ?>
        <title><?php echo htmlspecialchars($title);?></title>
        <?php if($description != ''){?>
        <description><?php echo htmlspecialchars($description);?></description>
        <?php } 
        if($movieDetailsVal['poster'] != ''){
        if($movieDetailsVal['full_movie'] != ''){
        ?>
        <media:content url="<?php echo htmlspecialchars($videoUrl.$movieDetailsVal['id'].'/'.$movieDetailsVal['full_movie']);?>" fileSize="0" type="video/mp4" medium="video" bitrate="0" framerate="0" duration="0" height="0" width="0" isDefault="true">
        <?php 
        if($movieDetailsVal['is_episode'] == 1){
        ?>
            <media:thumbnail url="<?php echo htmlspecialchars($postUrl.'/system/posters/'.$movieDetailsVal['poster']['id'].'/episode/'.$movieDetailsVal['poster']['poster_file_name']);?>"/>
        <?php
        } else{
        ?>
            <media:thumbnail url="<?php echo htmlspecialchars($postUrl.'/system/posters/'.$movieDetailsVal['poster']['id'].'/thumb/'.$movieDetailsVal['poster']['poster_file_name']);?>"/>
        <?php        
        }
        ?>
        </media:content>
        <?php
        } else{
        if($movieDetailsVal['is_episode'] == 1){
        ?>
        <media:content url="<?php echo htmlspecialchars($postUrl.'/system/posters/'.$movieDetailsVal['poster']['id'].'/episode/'.$movieDetailsVal['poster']['poster_file_name']);?>" isDefault="true" medium = "image"></media:content>
        <?php
        } else{
        ?>
        <media:content url="<?php echo htmlspecialchars($postUrl.'/system/posters/'.$movieDetailsVal['poster']['id'].'/thumb/'.$movieDetailsVal['poster']['poster_file_name']);?>" isDefault="true" medium = "image"></media:content>
        <?php        
        }
        }
        } else if($movieDetailsVal['full_movie'] != ''){
        ?>
        <media:content url="<?php echo htmlspecialchars($videoUrl.$movieDetailsVal['id'].'/'.$movieDetailsVal['full_movie']);?>" fileSize="0" type="video/mp4" medium="video" bitrate="0" framerate="0" duration="0" height="0" width="0" isDefault="true"></media:content>
        <?php 
        } 
        ?>  
        </item>
        <?php } ?>
</channel>
</rss>

