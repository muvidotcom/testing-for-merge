(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                var f = new Error("Cannot find module '" + o + "'");
                throw f.code = "MODULE_NOT_FOUND", f
            }
            var l = n[o] = {
                exports: {}
            };
            t[o][0].call(l.exports, function(e) {
                var n = t[o][1][e];
                return s(n ? n : e)
            }, l, l.exports, e, t, n, r)
        }
        return n[o].exports
    }
    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s
})({
    1: [function(require, module, exports) {
        'use strict';

        //simple representation of the API

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        var IVPAIDAdUnit = exports.IVPAIDAdUnit = function() {
            function IVPAIDAdUnit() {
                _classCallCheck(this, IVPAIDAdUnit);
            }

            _createClass(IVPAIDAdUnit, [{
                key: 'handshakeVersion',

                //all methods below
                //are async methods
                value: function handshakeVersion() {
                    var playerVPAIDVersion = arguments.length <= 0 || arguments[0] === undefined ? '2.0' : arguments[0];
                    var callback = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];
                }

                //creativeData is an object to be consistent with VPAIDHTML

            }, {
                key: 'initAd',
                value: function initAd(width, height, viewMode, desiredBitrate) {
                    var creativeData = arguments.length <= 4 || arguments[4] === undefined ? {
                        AdParameters: ''
                    } : arguments[4];
                    var environmentVars = arguments.length <= 5 || arguments[5] === undefined ? {
                        flashVars: ''
                    } : arguments[5];
                    var callback = arguments.length <= 6 || arguments[6] === undefined ? undefined : arguments[6];
                }
            }, {
                key: 'resizeAd',
                value: function resizeAd(width, height, viewMode) {
                    var callback = arguments.length <= 3 || arguments[3] === undefined ? undefined : arguments[3];
                }
            }, {
                key: 'startAd',
                value: function startAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'stopAd',
                value: function stopAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'pauseAd',
                value: function pauseAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'resumeAd',
                value: function resumeAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'expandAd',
                value: function expandAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'collapseAd',
                value: function collapseAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }
            }, {
                key: 'skipAd',
                value: function skipAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];
                }

                //properties that will be treat as async methods

            }, {
                key: 'getAdLinear',
                value: function getAdLinear(callback) {}
            }, {
                key: 'getAdWidth',
                value: function getAdWidth(callback) {}
            }, {
                key: 'getAdHeight',
                value: function getAdHeight(callback) {}
            }, {
                key: 'getAdExpanded',
                value: function getAdExpanded(callback) {}
            }, {
                key: 'getAdSkippableState',
                value: function getAdSkippableState(callback) {}
            }, {
                key: 'getAdRemainingTime',
                value: function getAdRemainingTime(callback) {}
            }, {
                key: 'getAdDuration',
                value: function getAdDuration(callback) {}
            }, {
                key: 'setAdVolume',
                value: function setAdVolume(soundVolume) {
                    var callback = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];
                }
            }, {
                key: 'getAdVolume',
                value: function getAdVolume(callback) {}
            }, {
                key: 'getAdCompanions',
                value: function getAdCompanions(callback) {}
            }, {
                key: 'getAdIcons',
                value: function getAdIcons(callback) {}
            }]);

            return IVPAIDAdUnit;
        }();

        Object.defineProperty(IVPAIDAdUnit, 'EVENTS', {
            writable: false,
            configurable: false,
            value: ['AdLoaded', 'AdStarted', 'AdStopped', 'AdSkipped', 'AdSkippableStateChange', // VPAID 2.0 new event
                'AdSizeChange', // VPAID 2.0 new event
                'AdLinearChange', 'AdDurationChange', // VPAID 2.0 new event
                'AdExpandedChange', 'AdRemainingTimeChange', // [Deprecated in 2.0] but will be still fired for backwards compatibility
                'AdVolumeChange', 'AdImpression', 'AdVideoStart', 'AdVideoFirstQuartile', 'AdVideoMidpoint', 'AdVideoThirdQuartile', 'AdVideoComplete', 'AdClickThru', 'AdInteraction', // VPAID 2.0 new event
                'AdUserAcceptInvitation', 'AdUserMinimize', 'AdUserClose', 'AdPaused', 'AdPlaying', 'AdLog', 'AdError'
            ]
        });

    }, {}],
    2: [function(require, module, exports) {
        'use strict';

        var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) {
            return typeof obj;
        } : function(obj) {
            return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
        };

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        function _possibleConstructorReturn(self, call) {
            if (!self) {
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            }
            return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
        }

        function _inherits(subClass, superClass) {
            if (typeof superClass !== "function" && superClass !== null) {
                throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
            }
            subClass.prototype = Object.create(superClass && superClass.prototype, {
                constructor: {
                    value: subClass,
                    enumerable: false,
                    writable: true,
                    configurable: true
                }
            });
            if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
        }

        var IVPAIDAdUnit = require('./IVPAIDAdUnit').IVPAIDAdUnit;
        var ALL_VPAID_METHODS = Object.getOwnPropertyNames(IVPAIDAdUnit.prototype).filter(function(property) {
            return ['constructor'].indexOf(property) === -1;
        });

        var VPAIDAdUnit = exports.VPAIDAdUnit = function(_IVPAIDAdUnit) {
            _inherits(VPAIDAdUnit, _IVPAIDAdUnit);

            function VPAIDAdUnit(flash) {
                _classCallCheck(this, VPAIDAdUnit);

                var _this = _possibleConstructorReturn(this, (VPAIDAdUnit.__proto__ || Object.getPrototypeOf(VPAIDAdUnit)).call(this));

                _this._destroyed = false;
                _this._flash = flash;
                return _this;
            }

            _createClass(VPAIDAdUnit, [{
                key: '_destroy',
                value: function _destroy() {
                    var _this2 = this;

                    this._destroyed = true;
                    ALL_VPAID_METHODS.forEach(function(methodName) {
                        _this2._flash.removeCallbackByMethodName(methodName);
                    });
                    IVPAIDAdUnit.EVENTS.forEach(function(event) {
                        _this2._flash.offEvent(event);
                    });

                    this._flash = null;
                }
            }, {
                key: 'isDestroyed',
                value: function isDestroyed() {
                    return this._destroyed;
                }
            }, {
                key: 'on',
                value: function on(eventName, callback) {
                    this._flash.on(eventName, callback);
                }
            }, {
                key: 'off',
                value: function off(eventName, callback) {
                    this._flash.off(eventName, callback);
                }

                //VPAID interface

            }, {
                key: 'handshakeVersion',
                value: function handshakeVersion() {
                    var playerVPAIDVersion = arguments.length <= 0 || arguments[0] === undefined ? '2.0' : arguments[0];
                    var callback = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];

                    this._flash.callFlashMethod('handshakeVersion', [playerVPAIDVersion], callback);
                }
            }, {
                key: 'initAd',
                value: function initAd(width, height, viewMode, desiredBitrate) {
                    var creativeData = arguments.length <= 4 || arguments[4] === undefined ? {
                        AdParameters: ''
                    } : arguments[4];
                    var environmentVars = arguments.length <= 5 || arguments[5] === undefined ? {
                        flashVars: ''
                    } : arguments[5];
                    var callback = arguments.length <= 6 || arguments[6] === undefined ? undefined : arguments[6];

                    //resize element that has the flash object
                    this._flash.setSize(width, height);
                    creativeData = creativeData || {
                        AdParameters: ''
                    };
                    environmentVars = environmentVars || {
                        flashVars: ''
                    };

                    this._flash.callFlashMethod('initAd', [this._flash.getWidth(), this._flash.getHeight(), viewMode, desiredBitrate, creativeData.AdParameters || '', environmentVars.flashVars || ''], callback);
                }
            }, {
                key: 'resizeAd',
                value: function resizeAd(width, height, viewMode) {
                    var callback = arguments.length <= 3 || arguments[3] === undefined ? undefined : arguments[3];

                    //resize element that has the flash object
                    this._flash.setSize(width, height);

                    //resize ad inside the flash
                    this._flash.callFlashMethod('resizeAd', [this._flash.getWidth(), this._flash.getHeight(), viewMode], callback);
                }
            }, {
                key: 'startAd',
                value: function startAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('startAd', [], callback);
                }
            }, {
                key: 'stopAd',
                value: function stopAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('stopAd', [], callback);
                }
            }, {
                key: 'pauseAd',
                value: function pauseAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('pauseAd', [], callback);
                }
            }, {
                key: 'resumeAd',
                value: function resumeAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('resumeAd', [], callback);
                }
            }, {
                key: 'expandAd',
                value: function expandAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('expandAd', [], callback);
                }
            }, {
                key: 'collapseAd',
                value: function collapseAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('collapseAd', [], callback);
                }
            }, {
                key: 'skipAd',
                value: function skipAd() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    this._flash.callFlashMethod('skipAd', [], callback);
                }

                //properties that will be treat as async methods

            }, {
                key: 'getAdLinear',
                value: function getAdLinear(callback) {
                    this._flash.callFlashMethod('getAdLinear', [], callback);
                }
            }, {
                key: 'getAdWidth',
                value: function getAdWidth(callback) {
                    this._flash.callFlashMethod('getAdWidth', [], callback);
                }
            }, {
                key: 'getAdHeight',
                value: function getAdHeight(callback) {
                    this._flash.callFlashMethod('getAdHeight', [], callback);
                }
            }, {
                key: 'getAdExpanded',
                value: function getAdExpanded(callback) {
                    this._flash.callFlashMethod('getAdExpanded', [], callback);
                }
            }, {
                key: 'getAdSkippableState',
                value: function getAdSkippableState(callback) {
                    this._flash.callFlashMethod('getAdSkippableState', [], callback);
                }
            }, {
                key: 'getAdRemainingTime',
                value: function getAdRemainingTime(callback) {
                    this._flash.callFlashMethod('getAdRemainingTime', [], callback);
                }
            }, {
                key: 'getAdDuration',
                value: function getAdDuration(callback) {
                    this._flash.callFlashMethod('getAdDuration', [], callback);
                }
            }, {
                key: 'setAdVolume',
                value: function setAdVolume(volume) {
                    var callback = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];

                    this._flash.callFlashMethod('setAdVolume', [volume], callback);
                }
            }, {
                key: 'getAdVolume',
                value: function getAdVolume(callback) {
                    this._flash.callFlashMethod('getAdVolume', [], callback);
                }
            }, {
                key: 'getAdCompanions',
                value: function getAdCompanions(callback) {
                    this._flash.callFlashMethod('getAdCompanions', [], callback);
                }
            }, {
                key: 'getAdIcons',
                value: function getAdIcons(callback) {
                    this._flash.callFlashMethod('getAdIcons', [], callback);
                }
            }]);

            return VPAIDAdUnit;
        }(IVPAIDAdUnit);

    }, {
        "./IVPAIDAdUnit": 1
    }],
    3: [function(require, module, exports) {
        'use strict';

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        var swfobject = require('swfobject');

        var JSFlashBridge = require('./jsFlashBridge').JSFlashBridge;
        var VPAIDAdUnit = require('./VPAIDAdUnit').VPAIDAdUnit;

        var noop = require('./utils').noop;
        var callbackTimeout = require('./utils').callbackTimeout;
        var isPositiveInt = require('./utils').isPositiveInt;
        var createElementWithID = require('./utils').createElementWithID;
        var uniqueVPAID = require('./utils').unique('vpaid');
        var createFlashTester = require('./flashTester.js').createFlashTester;

        var ERROR = 'error';
        var FLASH_VERSION = '10.1.0';

        var flashTester = {
            isSupported: function isSupported() {
                return true;
            }
        }; // if the runFlashTest is not run the flashTester will always return true

        var VPAIDFLASHClient = function() {
            function VPAIDFLASHClient(vpaidParentEl, callback) {
                var swfConfig = arguments.length <= 2 || arguments[2] === undefined ? {
                    data: 'VPAIDFlash.swf',
                    width: 800,
                    height: 400
                } : arguments[2];

                var _this = this;

                var params = arguments.length <= 3 || arguments[3] === undefined ? {
                    wmode: 'transparent',
                    salign: 'tl',
                    align: 'left',
                    allowScriptAccess: 'always',
                    scale: 'noScale',
                    allowFullScreen: 'true',
                    quality: 'high'
                } : arguments[3];
                var vpaidOptions = arguments.length <= 4 || arguments[4] === undefined ? {
                    debug: false,
                    timeout: 10000
                } : arguments[4];

                _classCallCheck(this, VPAIDFLASHClient);

                var me = this;

                this._vpaidParentEl = vpaidParentEl;
                this._flashID = uniqueVPAID();
                this._destroyed = false;
                callback = callback || noop;

                swfConfig.width = isPositiveInt(swfConfig.width, 800);
                alert(swfConfig.width)
                swfConfig.height = isPositiveInt(swfConfig.height, 400);
                alert(swfConfig.height)

                createElementWithID(vpaidParentEl, this._flashID, true);

                params.movie = swfConfig.data;
                params.FlashVars = 'flashid=' + this._flashID + '&handler=' + JSFlashBridge.VPAID_FLASH_HANDLER + '&debug=' + vpaidOptions.debug + '&salign=' + params.salign;

                if (!VPAIDFLASHClient.isSupported()) {
                    return onError('user don\'t support flash or doesn\'t have the minimum required version of flash ' + FLASH_VERSION);
                }

                this.el = swfobject.createSWF(swfConfig, params, this._flashID);

                if (!this.el) {
                    return onError('swfobject failed to create object in element');
                }

                var handler = callbackTimeout(vpaidOptions.timeout, function(err, data) {
                    $loadPendedAdUnit.call(_this);
                    callback(err, data);
                }, function() {
                    callback('vpaid flash load timeout ' + vpaidOptions.timeout);
                });

                /*
                 * @avi - modified
                 * date : 16-03-2017
                 */
                this._flash = new JSFlashBridge(this.el, swfConfig.data, this._flashID, swfConfig.width, swfConfig.height, handler);



                function onError(error) {
                    setTimeout(function() {
                        callback(new Error(error));
                    }, 0);
                    return me;
                }
            }

            _createClass(VPAIDFLASHClient, [{
                key: 'destroy',
                value: function destroy() {
                    this._destroyAdUnit();

                    if (this._flash) {
                        this._flash.destroy();
                        this._flash = null;
                    }
                    this.el = null;
                    this._destroyed = true;
                }
            }, {
                key: 'isDestroyed',
                value: function isDestroyed() {
                    return this._destroyed;
                }
            }, {
                key: '_destroyAdUnit',
                value: function _destroyAdUnit() {
                    delete this._loadLater;

                    if (this._adUnitLoad) {
                        this._adUnitLoad = null;
                        this._flash.removeCallback(this._adUnitLoad);
                    }

                    if (this._adUnit) {
                        this._adUnit._destroy();
                        this._adUnit = null;
                    }
                }
            }, {
                key: 'loadAdUnit',
                value: function loadAdUnit(adURL, callback) {
                    var _this2 = this;

                    $throwIfDestroyed.call(this);

                    if (this._adUnit) {
                        this._destroyAdUnit();
                    }

                    if (this._flash.isReady()) {
                        this._adUnitLoad = function(err, message) {
                            if (!err) {
                                _this2._adUnit = new VPAIDAdUnit(_this2._flash);
                            }
                            _this2._adUnitLoad = null;
                            callback(err, _this2._adUnit);
                        };

                        this._flash.callFlashMethod('loadAdUnit', [adURL], this._adUnitLoad);
                    } else {
                        this._loadLater = {
                            url: adURL,
                            callback: callback
                        };
                    }
                }
            }, {
                key: 'unloadAdUnit',
                value: function unloadAdUnit() {
                    var callback = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

                    $throwIfDestroyed.call(this);

                    this._destroyAdUnit();
                    this._flash.callFlashMethod('unloadAdUnit', [], callback);
                }
            }, {
                key: 'getFlashID',
                value: function getFlashID() {
                    $throwIfDestroyed.call(this);
                    return this._flash.getFlashID();
                }
            }, {
                key: 'getFlashURL',
                value: function getFlashURL() {
                    $throwIfDestroyed.call(this);
                    return this._flash.getFlashURL();
                }
            }]);

            return VPAIDFLASHClient;
        }();

        setStaticProperty('isSupported', function() {
            return swfobject.hasFlashPlayerVersion(FLASH_VERSION) && flashTester.isSupported();
        }, true);

        setStaticProperty('runFlashTest', function(swfConfig) {
            flashTester = createFlashTester(document.body, swfConfig);
        });

        function $throwIfDestroyed() {
            if (this._destroyed) {
                throw new Error('VPAIDFlashToJS is destroyed!');
            }
        }

        function $loadPendedAdUnit() {
            if (this._loadLater) {
                this.loadAdUnit(this._loadLater.url, this._loadLater.callback);
                delete this._loadLater;
            }
        }

        function setStaticProperty(propertyName, value) {
            var writable = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

            Object.defineProperty(VPAIDFLASHClient, propertyName, {
                writable: writable,
                configurable: false,
                value: value
            });
        }

        VPAIDFLASHClient.swfobject = swfobject;

        module.exports = VPAIDFLASHClient;

    }, {
        "./VPAIDAdUnit": 2,
        "./flashTester.js": 4,
        "./jsFlashBridge": 5,
        "./utils": 8,
        "swfobject": 14
    }],
    4: [function(require, module, exports) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        var swfobject = require('swfobject');

        var FLASH_TEST = 'vpaid_video_flash_tester';
        var FLASH_TEST_EL = 'vpaid_video_flash_tester_el';
        var JSFlashBridge = require('./jsFlashBridge').JSFlashBridge;
        var utils = require('./utils');
        var MultipleValuesRegistry = require('./registry').MultipleValuesRegistry;

        var FlashTester = function() {
            function FlashTester(parent) {
                var _this = this;

                var swfConfig = arguments.length <= 1 || arguments[1] === undefined ? {
                    data: 'VPAIDFlash.swf',
                    width: 800,
                    height: 400
                } : arguments[1];

                _classCallCheck(this, FlashTester);

                this.parentEl = utils.createElementWithID(parent, FLASH_TEST_EL); // some browsers create global variables using the element id http://stackoverflow.com/questions/3434278/do-dom-tree-elements-with-ids-become-global-variables
                utils.hideFlashEl(this.parentEl);
                var params = {};
                params.movie = swfConfig.data;
                params.FlashVars = 'flashid=' + FLASH_TEST_EL + '&handler=' + JSFlashBridge.VPAID_FLASH_HANDLER;
                params.allowScriptAccess = 'always';

                this.el = swfobject.createSWF(swfConfig, params, FLASH_TEST_EL);
                this._handlers = new MultipleValuesRegistry();
                this._isSupported = false;
                if (this.el) {
                    utils.hideFlashEl(this.el);
                    this._flash = new JSFlashBridge(this.el, swfConfig.data, FLASH_TEST_EL, swfConfig.width, swfConfig.height, function() {
                        var support = true;
                        _this._isSupported = support;
                        _this._handlers.get('change').forEach(function(callback) {
                            setTimeout(function() {
                                callback('change', support);
                            }, 0);
                        });
                    });
                }
            }

            _createClass(FlashTester, [{
                key: 'isSupported',
                value: function isSupported() {
                    return this._isSupported;
                }
            }, {
                key: 'on',
                value: function on(eventName, callback) {
                    this._handlers.add(eventName, callback);
                }
            }]);

            return FlashTester;
        }();

        var createFlashTester = exports.createFlashTester = function createFlashTester(el, swfConfig) {
            if (!window[FLASH_TEST]) {
                window[FLASH_TEST] = new FlashTester(el, swfConfig);
            }
            return window[FLASH_TEST];
        };

    }, {
        "./jsFlashBridge": 5,
        "./registry": 7,
        "./utils": 8,
        "swfobject": 14
    }],
    5: [function(require, module, exports) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        var unique = require('./utils').unique;
        var isPositiveInt = require('./utils').isPositiveInt;
        var stringEndsWith = require('./utils').stringEndsWith;
        var SingleValueRegistry = require('./registry').SingleValueRegistry;
        var MultipleValuesRegistry = require('./registry').MultipleValuesRegistry;
        var registry = require('./jsFlashBridgeRegistry');
        var VPAID_FLASH_HANDLER = 'vpaid_video_flash_handler';
        var ERROR = 'AdError';

        var JSFlashBridge = exports.JSFlashBridge = function() {
            function JSFlashBridge(el, flashURL, flashID, width, height, loadHandShake) {
                _classCallCheck(this, JSFlashBridge);

                this._el = el;
                this._flashID = flashID;
                this._flashURL = flashURL;
                this._width = width;
                this._height = height;
                this._handlers = new MultipleValuesRegistry();
                this._callbacks = new SingleValueRegistry();
                this._uniqueMethodIdentifier = unique(this._flashID);
                this._ready = false;
                this._handShakeHandler = loadHandShake;

                registry.addInstance(this._flashID, this);
            }

            _createClass(JSFlashBridge, [{
                key: 'on',
                value: function on(eventName, callback) {
                    this._handlers.add(eventName, callback);
                }
            }, {
                key: 'off',
                value: function off(eventName, callback) {
                    return this._handlers.remove(eventName, callback);
                }
            }, {
                key: 'offEvent',
                value: function offEvent(eventName) {
                    return this._handlers.removeByKey(eventName);
                }
            }, {
                key: 'offAll',
                value: function offAll() {
                    return this._handlers.removeAll();
                }
            }, {
                key: 'callFlashMethod',
                value: function callFlashMethod(methodName) {
                    var args = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];
                    var callback = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

                    var callbackID = '';
                    // if no callback, some methods the return is void so they don't need callback
                    if (callback) {
                        callbackID = this._uniqueMethodIdentifier() + '_' + methodName;
                        this._callbacks.add(callbackID, callback);
                    }

                    try {
                        //methods are created by ExternalInterface.addCallback in as3 code, if for some reason it failed
                        //this code will throw an error
                        this._el[methodName]([callbackID].concat(args));
                    } catch (e) {
                        if (callback) {
                            $asyncCallback.call(this, callbackID, e);
                        } else {

                            //if there isn't any callback to return error use error event handler
                            this._trigger(ERROR, e);
                        }
                    }
                }
            }, {
                key: 'removeCallback',
                value: function removeCallback(callback) {
                    return this._callbacks.removeByValue(callback);
                }
            }, {
                key: 'removeCallbackByMethodName',
                value: function removeCallbackByMethodName(suffix) {
                    var _this = this;

                    this._callbacks.filterKeys(function(key) {
                        return stringEndsWith(key, suffix);
                    }).forEach(function(key) {
                        _this._callbacks.remove(key);
                    });
                }
            }, {
                key: 'removeAllCallbacks',
                value: function removeAllCallbacks() {
                    return this._callbacks.removeAll();
                }
            }, {
                key: '_trigger',
                value: function _trigger(eventName, event) {
                    var _this2 = this;

                    this._handlers.get(eventName).forEach(function(callback) {
                        //clickThru has to be sync, if not will be block by the popupblocker
                        if (eventName === 'AdClickThru') {
                            callback(event);
                        } else {
                            setTimeout(function() {
                                if (_this2._handlers.get(eventName).length > 0) {
                                    callback(event);
                                }
                            }, 0);
                        }
                    });
                }
            }, {
                key: '_callCallback',
                value: function _callCallback(methodName, callbackID, err, result) {

                    var callback = this._callbacks.get(callbackID);

                    //not all methods callback's are mandatory
                    //but if there exist an error, fire the error event
                    if (!callback) {
                        if (err && callbackID === '') {
                            this.trigger(ERROR, err);
                        }
                        return;
                    }

                    $asyncCallback.call(this, callbackID, err, result);
                }
            }, {
                key: '_handShake',
                value: function _handShake(err, data) {
                    this._ready = true;
                    if (this._handShakeHandler) {
                        this._handShakeHandler(err, data);
                        delete this._handShakeHandler;
                    }
                }

                //methods like properties specific to this implementation of VPAID

            }, {
                key: 'getSize',
                value: function getSize() {
                    return {
                        width: this._width,
                        height: this._height
                    };
                }
            }, {
                key: 'setSize',
                value: function setSize(newWidth, newHeight) {
                    this._width = isPositiveInt(newWidth, this._width);
                    this._height = isPositiveInt(newHeight, this._height);
                    this._el.setAttribute('width', this._width);
                    this._el.setAttribute('height', this._height);
                }
            }, {
                key: 'getWidth',
                value: function getWidth() {
                    return this._width;
                }
            }, {
                key: 'setWidth',
                value: function setWidth(newWidth) {
                    this.setSize(newWidth, this._height);
                }
            }, {
                key: 'getHeight',
                value: function getHeight() {
                    return this._height;
                }
            }, {
                key: 'setHeight',
                value: function setHeight(newHeight) {
                    this.setSize(this._width, newHeight);
                }
            }, {
                key: 'getFlashID',
                value: function getFlashID() {
                    return this._flashID;
                }
            }, {
                key: 'getFlashURL',
                value: function getFlashURL() {
                    return this._flashURL;
                }
            }, {
                key: 'isReady',
                value: function isReady() {
                    return this._ready;
                }
            }, {
                key: 'destroy',
                value: function destroy() {
                    this.offAll();
                    this.removeAllCallbacks();
                    registry.removeInstanceByID(this._flashID);
                    if (this._el.parentElement) {
                        this._el.parentElement.removeChild(this._el);
                    }
                }
            }]);

            return JSFlashBridge;
        }();

        function $asyncCallback(callbackID, err, result) {
            var _this3 = this;

            setTimeout(function() {
                var callback = _this3._callbacks.get(callbackID);
                if (callback) {
                    _this3._callbacks.remove(callbackID);
                    callback(err, result);
                }
            }, 0);
        }

        Object.defineProperty(JSFlashBridge, 'VPAID_FLASH_HANDLER', {
            writable: false,
            configurable: false,
            value: VPAID_FLASH_HANDLER
        });

        /**
         * External interface handler
         *
         * @param {string} flashID identifier of the flash who call this
         * @param {string} typeID what type of message is, can be 'event' or 'callback'
         * @param {string} typeName if the typeID is a event the typeName will be the eventName, if is a callback the typeID is the methodName that is related this callback
         * @param {string} callbackID only applies when the typeID is 'callback', identifier of the callback to call
         * @param {object} error error object
         * @param {object} data
         */
        window[VPAID_FLASH_HANDLER] = function(flashID, typeID, typeName, callbackID, error, data) {
            var instance = registry.getInstanceByID(flashID);
            if (!instance) return;
            if (typeName === 'handShake') {
                instance._handShake(error, data);
            } else {
                if (typeID !== 'event') {
                    instance._callCallback(typeName, callbackID, error, data);
                } else {
                    instance._trigger(typeName, data);
                }
            }
        };

    }, {
        "./jsFlashBridgeRegistry": 6,
        "./registry": 7,
        "./utils": 8
    }],
    6: [function(require, module, exports) {
        'use strict';

        var SingleValueRegistry = require('./registry').SingleValueRegistry;
        var instances = new SingleValueRegistry();

        var JSFlashBridgeRegistry = {};
        Object.defineProperty(JSFlashBridgeRegistry, 'addInstance', {
            writable: false,
            configurable: false,
            value: function value(id, instance) {
                instances.add(id, instance);
            }
        });

        Object.defineProperty(JSFlashBridgeRegistry, 'getInstanceByID', {
            writable: false,
            configurable: false,
            value: function value(id) {
                return instances.get(id);
            }
        });

        Object.defineProperty(JSFlashBridgeRegistry, 'removeInstanceByID', {
            writable: false,
            configurable: false,
            value: function value(id) {
                return instances.remove(id);
            }
        });

        module.exports = JSFlashBridgeRegistry;

    }, {
        "./registry": 7
    }],
    7: [function(require, module, exports) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
            value: true
        });

        var _createClass = function() {
            function defineProperties(target, props) {
                for (var i = 0; i < props.length; i++) {
                    var descriptor = props[i];
                    descriptor.enumerable = descriptor.enumerable || false;
                    descriptor.configurable = true;
                    if ("value" in descriptor) descriptor.writable = true;
                    Object.defineProperty(target, descriptor.key, descriptor);
                }
            }
            return function(Constructor, protoProps, staticProps) {
                if (protoProps) defineProperties(Constructor.prototype, protoProps);
                if (staticProps) defineProperties(Constructor, staticProps);
                return Constructor;
            };
        }();

        function _classCallCheck(instance, Constructor) {
            if (!(instance instanceof Constructor)) {
                throw new TypeError("Cannot call a class as a function");
            }
        }

        var MultipleValuesRegistry = exports.MultipleValuesRegistry = function() {
            function MultipleValuesRegistry() {
                _classCallCheck(this, MultipleValuesRegistry);

                this._registries = {};
            }

            _createClass(MultipleValuesRegistry, [{
                key: 'add',
                value: function add(id, value) {
                    if (!this._registries[id]) {
                        this._registries[id] = [];
                    }
                    if (this._registries[id].indexOf(value) === -1) {
                        this._registries[id].push(value);
                    }
                }
            }, {
                key: 'get',
                value: function get(id) {
                    return this._registries[id] || [];
                }
            }, {
                key: 'filterKeys',
                value: function filterKeys(handler) {
                    return Object.keys(this._registries).filter(handler);
                }
            }, {
                key: 'findByValue',
                value: function findByValue(value) {
                    var _this = this;

                    var keys = Object.keys(this._registries).filter(function(key) {
                        return _this._registries[key].indexOf(value) !== -1;
                    });

                    return keys;
                }
            }, {
                key: 'remove',
                value: function remove(key, value) {
                    if (!this._registries[key]) {
                        return;
                    }

                    var index = this._registries[key].indexOf(value);

                    if (index < 0) {
                        return;
                    }
                    return this._registries[key].splice(index, 1);
                }
            }, {
                key: 'removeByKey',
                value: function removeByKey(id) {
                    var old = this._registries[id];
                    delete this._registries[id];
                    return old;
                }
            }, {
                key: 'removeByValue',
                value: function removeByValue(value) {
                    var _this2 = this;

                    var keys = this.findByValue(value);
                    return keys.map(function(key) {
                        return _this2.remove(key, value);
                    });
                }
            }, {
                key: 'removeAll',
                value: function removeAll() {
                    var old = this._registries;
                    this._registries = {};
                    return old;
                }
            }, {
                key: 'size',
                value: function size() {
                    return Object.keys(this._registries).length;
                }
            }]);

            return MultipleValuesRegistry;
        }();

        var SingleValueRegistry = exports.SingleValueRegistry = function() {
            function SingleValueRegistry() {
                _classCallCheck(this, SingleValueRegistry);

                this._registries = {};
            }

            _createClass(SingleValueRegistry, [{
                key: 'add',
                value: function add(id, value) {
                    this._registries[id] = value;
                }
            }, {
                key: 'get',
                value: function get(id) {
                    return this._registries[id];
                }
            }, {
                key: 'filterKeys',
                value: function filterKeys(handler) {
                    return Object.keys(this._registries).filter(handler);
                }
            }, {
                key: 'findByValue',
                value: function findByValue(value) {
                    var _this3 = this;

                    var keys = Object.keys(this._registries).filter(function(key) {
                        return _this3._registries[key] === value;
                    });

                    return keys;
                }
            }, {
                key: 'remove',
                value: function remove(id) {
                    var old = this._registries[id];
                    delete this._registries[id];
                    return old;
                }
            }, {
                key: 'removeByValue',
                value: function removeByValue(value) {
                    var _this4 = this;

                    var keys = this.findByValue(value);
                    return keys.map(function(key) {
                        return _this4.remove(key);
                    });
                }
            }, {
                key: 'removeAll',
                value: function removeAll() {
                    var old = this._registries;
                    this._registries = {};
                    return old;
                }
            }, {
                key: 'size',
                value: function size() {
                    return Object.keys(this._registries).length;
                }
            }]);

            return SingleValueRegistry;
        }();

    }, {}],
    8: [function(require, module, exports) {
        'use strict';

        Object.defineProperty(exports, "__esModule", {
            value: true
        });
        exports.unique = unique;
        exports.noop = noop;
        exports.callbackTimeout = callbackTimeout;
        exports.createElementWithID = createElementWithID;
        exports.isPositiveInt = isPositiveInt;
        exports.stringEndsWith = stringEndsWith;
        exports.hideFlashEl = hideFlashEl;

        function unique(prefix) {
            var count = -1;
            return function(f) {
                return prefix + '_' + ++count;
            };
        }

        function noop() {}

        function callbackTimeout(timer, onSuccess, onTimeout) {

            var timeout = setTimeout(function() {

                onSuccess = noop;
                onTimeout();
            }, timer);

            return function() {
                clearTimeout(timeout);
                onSuccess.apply(this, arguments);
            };
        }

        function createElementWithID(parent, id) {
            var cleanContent = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

            var nEl = document.createElement('div');
            nEl.id = id;
            if (cleanContent) {
                parent.innerHTML = '';
            }
            parent.appendChild(nEl);
            return nEl;
        }

        function isPositiveInt(newVal, oldVal) {
            return !isNaN(parseFloat(newVal)) && isFinite(newVal) && newVal > 0 ? newVal : oldVal;
        }

        var endsWith = function() {
            if (String.prototype.endsWith) return String.prototype.endsWith;
            return function endsWith(searchString, position) {
                var subjectString = this.toString();
                if (position === undefined || position > subjectString.length) {
                    position = subjectString.length;
                }
                position -= searchString.length;
                var lastIndex = subjectString.indexOf(searchString, position);
                return lastIndex !== -1 && lastIndex === position;
            };
        }();

        function stringEndsWith(string, search) {
            return endsWith.call(string, search);
        }

        function hideFlashEl(el) {
            // can't use display none or visibility none because will block flash in some browsers
            el.style.position = 'absolute';
            el.style.left = '-1px';
            el.style.top = '-1px';
            el.style.width = '1px';
            el.style.height = '1px';
        }

    }, {}],
    9: [function(require, module, exports) {
        'use strict';

        var METHODS = [
            'handshakeVersion',
            'initAd',
            'startAd',
            'stopAd',
            'skipAd', // VPAID 2.0 new method
            'resizeAd',
            'pauseAd',
            'resumeAd',
            'expandAd',
            'collapseAd',
            'subscribe',
            'unsubscribe'
        ];

        var EVENTS = [
            'AdLoaded',
            'AdStarted',
            'AdStopped',
            'AdSkipped',
            'AdSkippableStateChange', // VPAID 2.0 new event
            'AdSizeChange', // VPAID 2.0 new event
            'AdLinearChange',
            'AdDurationChange', // VPAID 2.0 new event
            'AdExpandedChange',
            'AdRemainingTimeChange', // [Deprecated in 2.0] but will be still fired for backwards compatibility
            'AdVolumeChange',
            'AdImpression',
            'AdVideoStart',
            'AdVideoFirstQuartile',
            'AdVideoMidpoint',
            'AdVideoThirdQuartile',
            'AdVideoComplete',
            'AdClickThru',
            'AdInteraction', // VPAID 2.0 new event
            'AdUserAcceptInvitation',
            'AdUserMinimize',
            'AdUserClose',
            'AdPaused',
            'AdPlaying',
            'AdLog',
            'AdError'
        ];

        var GETTERS = [
            'getAdLinear',
            'getAdWidth', // VPAID 2.0 new getter
            'getAdHeight', // VPAID 2.0 new getter
            'getAdExpanded',
            'getAdSkippableState', // VPAID 2.0 new getter
            'getAdRemainingTime',
            'getAdDuration', // VPAID 2.0 new getter
            'getAdVolume',
            'getAdCompanions', // VPAID 2.0 new getter
            'getAdIcons' // VPAID 2.0 new getter
        ];

        var SETTERS = [
            'setAdVolume'
        ];


        /**
         * This callback is displayed as global member. The callback use nodejs error-first callback style
         * @callback NodeStyleCallback
         * @param {string|null}
         * @param {undefined|object}
         */


        /**
         * IVPAIDAdUnit
         *
         * @class
         *
         * @param {object} creative
         * @param {HTMLElement} el
         * @param {HTMLVideoElement} video
         */
        function IVPAIDAdUnit(creative, el, video) {}


        /**
         * handshakeVersion
         *
         * @param {string} VPAIDVersion
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.handshakeVersion = function(VPAIDVersion, callback) {};

        /**
         * initAd
         *
         * @param {number} width
         * @param {number} height
         * @param {string} viewMode can be 'normal', 'thumbnail' or 'fullscreen'
         * @param {number} desiredBitrate indicates the desired bitrate in kbps
         * @param {object} [creativeData] used for additional initialization data
         * @param {object} [environmentVars] used for passing implementation-specific of js version
         * @param {NodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.initAd = function(width, height, viewMode, desiredBitrate, creativeData, environmentVars, callback) {};

        /**
         * startAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.startAd = function(callback) {};

        /**
         * stopAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.stopAd = function(callback) {};

        /**
         * skipAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.skipAd = function(callback) {};

        /**
         * resizeAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.resizeAd = function(width, height, viewMode, callback) {};

        /**
         * pauseAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.pauseAd = function(callback) {};

        /**
         * resumeAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.resumeAd = function(callback) {};

        /**
         * expandAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.expandAd = function(callback) {};

        /**
         * collapseAd
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.collapseAd = function(callback) {};

        /**
         * subscribe
         *
         * @param {string} event
         * @param {nodeStyleCallback} handler
         * @param {object} context
         */
        IVPAIDAdUnit.prototype.subscribe = function(event, handler, context) {};

        /**
         * startAd
         *
         * @param {string} event
         * @param {function} handler
         */
        IVPAIDAdUnit.prototype.unsubscribe = function(event, handler) {};



        /**
         * getAdLinear
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdLinear = function(callback) {};

        /**
         * getAdWidth
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdWidth = function(callback) {};

        /**
         * getAdHeight
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdHeight = function(callback) {};

        /**
         * getAdExpanded
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdExpanded = function(callback) {};

        /**
         * getAdSkippableState
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdSkippableState = function(callback) {};

        /**
         * getAdRemainingTime
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdRemainingTime = function(callback) {};

        /**
         * getAdDuration
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdDuration = function(callback) {};

        /**
         * getAdVolume
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdVolume = function(callback) {};

        /**
         * getAdCompanions
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdCompanions = function(callback) {};

        /**
         * getAdIcons
         *
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.getAdIcons = function(callback) {};

        /**
         * setAdVolume
         *
         * @param {number} volume
         * @param {nodeStyleCallback} callback
         */
        IVPAIDAdUnit.prototype.setAdVolume = function(volume, callback) {};

        addStaticToInterface(IVPAIDAdUnit, 'METHODS', METHODS);
        addStaticToInterface(IVPAIDAdUnit, 'GETTERS', GETTERS);
        addStaticToInterface(IVPAIDAdUnit, 'SETTERS', SETTERS);
        addStaticToInterface(IVPAIDAdUnit, 'EVENTS', EVENTS);


        var VPAID1_METHODS = METHODS.filter(function(method) {
            return ['skipAd'].indexOf(method) === -1;
        });

        addStaticToInterface(IVPAIDAdUnit, 'checkVPAIDInterface', function checkVPAIDInterface(creative) {
            var result = VPAID1_METHODS.every(function(key) {
                return typeof creative[key] === 'function';
            });
            return result;
        });

        module.exports = IVPAIDAdUnit;

        function addStaticToInterface(Interface, name, value) {
            Object.defineProperty(Interface, name, {
                writable: false,
                configurable: false,
                value: value
            });
        }


    }, {}],
    10: [function(require, module, exports) {
        'use strict';

        var IVPAIDAdUnit = require('./IVPAIDAdUnit');
        var Subscriber = require('./subscriber');
        var checkVPAIDInterface = IVPAIDAdUnit.checkVPAIDInterface;
        var utils = require('./utils');
        var METHODS = IVPAIDAdUnit.METHODS;
        var ERROR = 'AdError';
        var AD_CLICK = 'AdClickThru';
        var FILTERED_EVENTS = IVPAIDAdUnit.EVENTS.filter(function(event) {
            return event != AD_CLICK;
        });

        /**
         * This callback is displayed as global member. The callback use nodejs error-first callback style
         * @callback NodeStyleCallback
         * @param {string|null}
         * @param {undefined|object}
         */


        /**
         * VPAIDAdUnit
         * @class
         *
         * @param VPAIDCreative
         * @param {HTMLElement} [el] this will be used in initAd environmentVars.slot if defined
         * @param {HTMLVideoElement} [video] this will be used in initAd environmentVars.videoSlot if defined
         */
        function VPAIDAdUnit(VPAIDCreative, el, video, iframe) {
            this._isValid = checkVPAIDInterface(VPAIDCreative);
            if (this._isValid) {
                this._creative = VPAIDCreative;
                this._el = el;
                this._videoEl = video;
                this._iframe = iframe;
                this._subscribers = new Subscriber();
                utils.setFullSizeStyle(el);
                $addEventsSubscribers.call(this);
            }
        }

        VPAIDAdUnit.prototype = Object.create(IVPAIDAdUnit.prototype);

        /**
         * isValidVPAIDAd will return if the VPAIDCreative passed in constructor is valid or not
         *
         * @return {boolean}
         */
        VPAIDAdUnit.prototype.isValidVPAIDAd = function isValidVPAIDAd() {
            return this._isValid;
        };

        IVPAIDAdUnit.METHODS.forEach(function(method) {
            //NOTE: this methods arguments order are implemented differently from the spec
            var ignores = [
                'subscribe',
                'unsubscribe',
                'initAd'
            ];

            if (ignores.indexOf(method) !== -1) return;

            VPAIDAdUnit.prototype[method] = function() {
                var ariaty = IVPAIDAdUnit.prototype[method].length;
                // TODO avoid leaking arguments
                // https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments
                var args = Array.prototype.slice.call(arguments);
                var callback = (ariaty === args.length) ? args.pop() : undefined;

                setTimeout(function() {
                    var result, error = null;
                    try {
                        result = this._creative[method].apply(this._creative, args);
                    } catch (e) {
                        error = e;
                    }

                    callOrTriggerEvent(callback, this._subscribers, error, result);
                }.bind(this), 0);
            };
        });


        /**
         * initAd concreate implementation
         *
         * @param {number} width
         * @param {number} height
         * @param {string} viewMode can be 'normal', 'thumbnail' or 'fullscreen'
         * @param {number} desiredBitrate indicates the desired bitrate in kbps
         * @param {object} [creativeData] used for additional initialization data
         * @param {object} [environmentVars] used for passing implementation-specific of js version, if el & video was used in constructor slot & videoSlot will be added to the object
         * @param {NodeStyleCallback} callback
         */
        VPAIDAdUnit.prototype.initAd = function initAd(width, height, viewMode, desiredBitrate, creativeData, environmentVars, callback) {
            creativeData = creativeData || {};
            environmentVars = utils.extend({
                slot: this._el,
                videoSlot: this._videoEl
            }, environmentVars || {});

            setTimeout(function() {
                var error;
                try {
                    this._creative.initAd(width, height, viewMode, desiredBitrate, creativeData, environmentVars);
                } catch (e) {
                    error = e;
                }

                callOrTriggerEvent(callback, this._subscribers, error);
            }.bind(this), 0);
        };

        /**
         * subscribe
         *
         * @param {string} event
         * @param {nodeStyleCallback} handler
         * @param {object} context
         */
        VPAIDAdUnit.prototype.subscribe = function subscribe(event, handler, context) {
            this._subscribers.subscribe(handler, event, context);
        };


        /**
         * unsubscribe
         *
         * @param {string} event
         * @param {nodeStyleCallback} handler
         */
        VPAIDAdUnit.prototype.unsubscribe = function unsubscribe(event, handler) {
            this._subscribers.unsubscribe(handler, event);
        };

        //alias
        VPAIDAdUnit.prototype.on = VPAIDAdUnit.prototype.subscribe;
        VPAIDAdUnit.prototype.off = VPAIDAdUnit.prototype.unsubscribe;

        IVPAIDAdUnit.GETTERS.forEach(function(getter) {
            VPAIDAdUnit.prototype[getter] = function(callback) {
                setTimeout(function() {

                    var result, error = null;
                    try {
                        result = this._creative[getter]();
                    } catch (e) {
                        error = e;
                    }

                    callOrTriggerEvent(callback, this._subscribers, error, result);
                }.bind(this), 0);
            };
        });

        /**
         * setAdVolume
         *
         * @param volume
         * @param {nodeStyleCallback} callback
         */
        VPAIDAdUnit.prototype.setAdVolume = function setAdVolume(volume, callback) {
            setTimeout(function() {

                var result, error = null;
                try {
                    this._creative.setAdVolume(volume);
                    result = this._creative.getAdVolume();
                } catch (e) {
                    error = e;
                }

                if (!error) {
                    error = utils.validate(result === volume, 'failed to apply volume: ' + volume);
                }
                callOrTriggerEvent(callback, this._subscribers, error, result);
            }.bind(this), 0);
        };

        VPAIDAdUnit.prototype._destroy = function destroy() {
            this.stopAd();
            this._subscribers.unsubscribeAll();
        };

        function $addEventsSubscribers() {
            // some ads implement
            // so they only handle one subscriber
            // to handle this we create our one
            FILTERED_EVENTS.forEach(function(event) {
                this._creative.subscribe($trigger.bind(this, event), event);
            }.bind(this));

            // map the click event to be an object instead of depending of the order of the arguments
            // and to be consistent with the flash
            this._creative.subscribe($clickThruHook.bind(this), AD_CLICK);

            // because we are adding the element inside the iframe
            // the user is not able to click in the video
            if (this._videoEl) {
                var documentElement = this._iframe.contentDocument.documentElement;
                var videoEl = this._videoEl;
                documentElement.addEventListener('click', function(e) {
                    if (e.target === documentElement) {
                        videoEl.click();
                    }
                });
            }
        }

        function $clickThruHook(url, id, playerHandles) {
            this._subscribers.triggerSync(AD_CLICK, {
                url: url,
                id: id,
                playerHandles: playerHandles
            });
        }

        function $trigger(event) {
            // TODO avoid leaking arguments
            // https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments
            this._subscribers.trigger(event, Array.prototype.slice(arguments, 1));
        }

        function callOrTriggerEvent(callback, subscribers, error, result) {
            if (callback) {
                callback(error, result);
            } else if (error) {
                subscribers.trigger(ERROR, error);
            }
        }

        module.exports = VPAIDAdUnit;


    }, {
        "./IVPAIDAdUnit": 9,
        "./subscriber": 12,
        "./utils": 13
    }],
    11: [function(require, module, exports) {
        'use strict';

        var utils = require('./utils');
        var unique = utils.unique('vpaidIframe');
        var VPAIDAdUnit = require('./VPAIDAdUnit');

        var defaultTemplate = '<!DOCTYPE html>' +
            '<html lang="en">' +
            '<head><meta charset="UTF-8"></head>' +
            '<body style="margin:0;padding:0"><div class="ad-element"></div>' +
            '<script type="text/javascript" src="{{iframeURL_JS}}"></script>' +
            '<script type="text/javascript">' +
            'window.parent.postMessage(\'{"event": "ready", "id": "{{iframeID}}"}\', \'{{origin}}\');' +
            '</script>' +
            '</body>' +
            '</html>';

        var AD_STOPPED = 'AdStopped';

        /**
         * This callback is displayed as global member. The callback use nodejs error-first callback style
         * @callback NodeStyleCallback
         * @param {string|null}
         * @param {undefined|object}
         */

        /**
         * VPAIDHTML5Client
         * @class
         *
         * @param {HTMLElement} el that will contain the iframe to load adUnit and a el to add to adUnit slot
         * @param {HTMLVideoElement} video default video element to be used by adUnit
         * @param {object} [templateConfig] template: html template to be used instead of the default, extraOptions: to be used when rendering the template
         * @param {object} [vpaidOptions] timeout: when loading adUnit
         */
        function VPAIDHTML5Client(el, video, templateConfig, vpaidOptions) {
            templateConfig = templateConfig || {};

            this._id = unique();
            this._destroyed = false;

            this._frameContainer = utils.createElementInEl(el, 'div');
            this._videoEl = video;
            this._vpaidOptions = vpaidOptions || {
                timeout: 10000
            };

            this._templateConfig = {
                template: templateConfig.template || defaultTemplate,
                extraOptions: templateConfig.extraOptions || {}
            };
        }

        /**
         * destroy
         *
         */
        VPAIDHTML5Client.prototype.destroy = function destroy() {
            if (this._destroyed) {
                return;
            }
            this._destroyed = true;
            $unloadPreviousAdUnit.call(this);
        };

        /**
         * isDestroyed
         *
         * @return {boolean}
         */
        VPAIDHTML5Client.prototype.isDestroyed = function isDestroyed() {
            return this._destroyed;
        };

        /**
         * loadAdUnit
         *
         * @param {string} adURL url of the js of the adUnit
         * @param {nodeStyleCallback} callback
         */
        VPAIDHTML5Client.prototype.loadAdUnit = function loadAdUnit(adURL, callback) {
            $throwIfDestroyed.call(this);
            $unloadPreviousAdUnit.call(this);
            var that = this;

            var frame = utils.createIframeWithContent(
                this._frameContainer,
                this._templateConfig.template,
                utils.extend({
                    iframeURL_JS: adURL,
                    iframeID: this.getID(),
                    origin: getOrigin()
                }, this._templateConfig.extraOptions)
            );

            this._frame = frame;

            this._onLoad = utils.callbackTimeout(
                this._vpaidOptions.timeout,
                onLoad.bind(this),
                onTimeout.bind(this)
            );

            window.addEventListener('message', this._onLoad);

            function onLoad(e) {
                /*jshint validthis: false */
                //don't clear timeout
                if (e.origin !== getOrigin()) return;
                var result = JSON.parse(e.data);

                //don't clear timeout
                if (result.id !== that.getID()) return;

                var adUnit, error, createAd;
                if (!that._frame.contentWindow) {

                    error = 'the iframe is not anymore in the DOM tree';

                } else {
                    createAd = that._frame.contentWindow.getVPAIDAd;
                    error = utils.validate(typeof createAd === 'function', 'the ad didn\'t return a function to create an ad');
                }

                if (!error) {
                    var adEl = that._frame.contentWindow.document.querySelector('.ad-element');
                    adUnit = new VPAIDAdUnit(createAd(), adEl, that._videoEl, that._frame);
                    adUnit.subscribe(AD_STOPPED, $adDestroyed.bind(that));
                    error = utils.validate(adUnit.isValidVPAIDAd(), 'the add is not fully complaint with VPAID specification');
                }

                that._adUnit = adUnit;
                $destroyLoadListener.call(that);
                callback(error, error ? null : adUnit);

                //clear timeout
                return true;
            }

            function onTimeout() {
                callback('timeout', null);
            }
        };

        /**
         * unloadAdUnit
         *
         */
        VPAIDHTML5Client.prototype.unloadAdUnit = function unloadAdUnit() {
            $unloadPreviousAdUnit.call(this);
        };

        /**
         * getID will return the unique id
         *
         * @return {string}
         */
        VPAIDHTML5Client.prototype.getID = function() {
            return this._id;
        };


        /**
         * $removeEl
         *
         * @param {string} key
         */
        function $removeEl(key) {
            var el = this[key];
            if (el) {
                el.remove();
                delete this[key];
            }
        }

        function $adDestroyed() {
            $removeAdElements.call(this);
            delete this._adUnit;
        }

        function $unloadPreviousAdUnit() {
            $removeAdElements.call(this);
            $destroyAdUnit.call(this);
        }

        function $removeAdElements() {
            $removeEl.call(this, '_frame');
            $destroyLoadListener.call(this);
        }

        /**
         * $destroyLoadListener
         *
         */
        function $destroyLoadListener() {
            if (this._onLoad) {
                window.removeEventListener('message', this._onLoad);
                utils.clearCallbackTimeout(this._onLoad);
                delete this._onLoad;
            }
        }


        function $destroyAdUnit() {
            if (this._adUnit) {
                this._adUnit.stopAd();
                delete this._adUnit;
            }
        }

        /**
         * $throwIfDestroyed
         *
         */
        function $throwIfDestroyed() {
            if (this._destroyed) {
                throw new Error('VPAIDHTML5Client already destroyed!');
            }
        }

        function getOrigin() {
            if (window.location.origin) {
                return window.location.origin;
            } else {
                return window.location.protocol + "//" +
                    window.location.hostname +
                    (window.location.port ? ':' + window.location.port : '');
            }
        }

        module.exports = VPAIDHTML5Client;


    }, {
        "./VPAIDAdUnit": 10,
        "./utils": 13
    }],
    12: [function(require, module, exports) {
        'use strict';

        function Subscriber() {
            this._subscribers = {};
        }

        Subscriber.prototype.subscribe = function subscribe(handler, eventName, context) {
            if (!this.isHandlerAttached(handler, eventName)) {
                this.get(eventName).push({
                    handler: handler,
                    context: context,
                    eventName: eventName
                });
            }
        };

        Subscriber.prototype.unsubscribe = function unsubscribe(handler, eventName) {
            this._subscribers[eventName] = this.get(eventName).filter(function(subscriber) {
                return handler !== subscriber.handler;
            });
        };

        Subscriber.prototype.unsubscribeAll = function unsubscribeAll() {
            this._subscribers = {};
        };

        Subscriber.prototype.trigger = function(eventName, data) {
            var that = this;
            var subscribers = this.get(eventName)
                .concat(this.get('*'));

            subscribers.forEach(function(subscriber) {
                setTimeout(function() {
                    if (that.isHandlerAttached(subscriber.handler, subscriber.eventName)) {
                        subscriber.handler.call(subscriber.context, data);
                    }
                }, 0);
            });
        };

        Subscriber.prototype.triggerSync = function(eventName, data) {
            var subscribers = this.get(eventName)
                .concat(this.get('*'));

            subscribers.forEach(function(subscriber) {
                subscriber.handler.call(subscriber.context, data);
            });
        };

        Subscriber.prototype.get = function get(eventName) {
            if (!this._subscribers[eventName]) {
                this._subscribers[eventName] = [];
            }
            return this._subscribers[eventName];
        };

        Subscriber.prototype.isHandlerAttached = function isHandlerAttached(handler, eventName) {
            return this.get(eventName).some(function(subscriber) {
                return handler === subscriber.handler;
            })
        };

        module.exports = Subscriber;


    }, {}],
    13: [function(require, module, exports) {
        'use strict';

        /**
         * noop a empty function
         */
        function noop() {}

        /**
         * validate if is not validate will return an Error with the message
         *
         * @param {boolean} isValid
         * @param {string} message
         */
        function validate(isValid, message) {
            return isValid ? null : new Error(message);
        }

        var timeouts = {};
        /**
         * clearCallbackTimeout
         *
         * @param {function} func handler to remove
         */
        function clearCallbackTimeout(func) {
            var timeout = timeouts[func];
            if (timeout) {
                clearTimeout(timeout);
                delete timeouts[func];
            }
        }

        /**
         * callbackTimeout if the onSuccess is not called and returns true in the timelimit then onTimeout will be called
         *
         * @param {number} timer
         * @param {function} onSuccess
         * @param {function} onTimeout
         */
        function callbackTimeout(timer, onSuccess, onTimeout) {
            var callback, timeout;

            timeout = setTimeout(function() {
                onSuccess = noop;
                delete timeout[callback];
                onTimeout();
            }, timer);

            callback = function() {
                // TODO avoid leaking arguments
                // https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments
                if (onSuccess.apply(this, arguments)) {
                    clearCallbackTimeout(callback);
                }
            };

            timeouts[callback] = timeout;

            return callback;
        }


        /**
         * createElementInEl
         *
         * @param {HTMLElement} parent
         * @param {string} tagName
         * @param {string} id
         */
        function createElementInEl(parent, tagName, id) {
            var nEl = document.createElement(tagName);
            if (id) nEl.id = id;
            parent.appendChild(nEl);
            return nEl;
        }

        /**
         * createIframeWithContent
         *
         * @param {HTMLElement} parent
         * @param {string} template simple template using {{var}}
         * @param {object} data
         */
        function createIframeWithContent(parent, template, data) {
            var iframe = createIframe(parent, null, data.zIndex);
            if (!setIframeContent(iframe, simpleTemplate(template, data))) return;
            return iframe;
        }

        /**
         * createIframe
         *
         * @param {HTMLElement} parent
         * @param {string} url
         */
        function createIframe(parent, url, zIndex) {
            var nEl = document.createElement('iframe');
            nEl.src = url || 'about:blank';
            nEl.marginWidth = '0';
            nEl.marginHeight = '0';
            nEl.frameBorder = '0';
            nEl.width = '100%';
            nEl.height = '100%';
            setFullSizeStyle(nEl);

            if (zIndex) {
                nEl.style.zIndex = zIndex;
            }

            nEl.setAttribute('SCROLLING', 'NO');
            parent.innerHTML = '';
            parent.appendChild(nEl);
            return nEl;
        }

        function setFullSizeStyle(element) {
            element.style.position = 'absolute';
            element.style.left = '0';
            element.style.top = '0';
            element.style.margin = '0px';
            element.style.padding = '0px';
            element.style.border = 'none';
            element.style.width = '100%';
            element.style.height = '100%';
        }

        /**
         * simpleTemplate
         *
         * @param {string} template
         * @param {object} data
         */
        function simpleTemplate(template, data) {
            Object.keys(data).forEach(function(key) {
                var value = (typeof value === 'object') ? JSON.stringify(data[key]) : data[key];
                template = template.replace(new RegExp('{{' + key + '}}', 'g'), value);
            });
            return template;
        }

        /**
         * setIframeContent
         *
         * @param {HTMLIframeElement} iframeEl
         * @param content
         */
        function setIframeContent(iframeEl, content) {
            var iframeDoc = iframeEl.contentWindow && iframeEl.contentWindow.document;
            if (!iframeDoc) return false;

            iframeDoc.write(content);

            return true;
        }


        /**
         * extend object with keys from another object
         *
         * @param {object} toExtend
         * @param {object} fromSource
         */
        function extend(toExtend, fromSource) {
            Object.keys(fromSource).forEach(function(key) {
                toExtend[key] = fromSource[key];
            });
            return toExtend;
        }


        /**
         * unique will create a unique string everytime is called, sequentially and prefixed
         *
         * @param {string} prefix
         */
        function unique(prefix) {
            var count = -1;
            return function() {
                return prefix + '_' + (++count);
            };
        }

        module.exports = {
            noop: noop,
            validate: validate,
            clearCallbackTimeout: clearCallbackTimeout,
            callbackTimeout: callbackTimeout,
            createElementInEl: createElementInEl,
            createIframeWithContent: createIframeWithContent,
            createIframe: createIframe,
            setFullSizeStyle: setFullSizeStyle,
            simpleTemplate: simpleTemplate,
            setIframeContent: setIframeContent,
            extend: extend,
            unique: unique
        };


    }, {}],
    14: [function(require, module, exports) {
        /*!    SWFObject v2.3.20130521 <http://github.com/swfobject/swfobject>
            is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
        */

        /* global ActiveXObject: false */

        (function(root, factory) {
            if (typeof define === 'function' && define.amd) {
                // AMD
                define(factory);
            } else if (typeof module === 'object' && module.exports) {
                // Node, CommonJS-like
                module.exports = factory();
            } else {
                // Browser globals (root is window)
                root.swfobject = factory();
            }
        }(this, function() {

            var UNDEF = "undefined",
                OBJECT = "object",
                SHOCKWAVE_FLASH = "Shockwave Flash",
                SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
                FLASH_MIME_TYPE = "application/x-shockwave-flash",
                EXPRESS_INSTALL_ID = "SWFObjectExprInst",
                ON_READY_STATE_CHANGE = "onreadystatechange",

                win = window,
                doc = document,
                nav = navigator,

                plugin = false,
                domLoadFnArr = [],
                regObjArr = [],
                objIdArr = [],
                listenersArr = [],
                storedFbContent,
                storedFbContentId,
                storedCallbackFn,
                storedCallbackObj,
                isDomLoaded = false,
                isExpressInstallActive = false,
                dynamicStylesheet,
                dynamicStylesheetMedia,
                autoHideShow = true,
                encodeURIEnabled = false,

                /* Centralized function for browser feature detection
                    - User agent string detection is only used when no good alternative is possible
                    - Is executed directly for optimal performance
                */
                ua = function() {
                    var w3cdom = typeof doc.getElementById !== UNDEF && typeof doc.getElementsByTagName !== UNDEF && typeof doc.createElement !== UNDEF,
                        u = nav.userAgent.toLowerCase(),
                        p = nav.platform.toLowerCase(),
                        windows = p ? /win/.test(p) : /win/.test(u),
                        mac = p ? /mac/.test(p) : /mac/.test(u),
                        webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, // returns either the webkit version or false if not webkit
                        ie = nav.appName === "Microsoft Internet Explorer",
                        playerVersion = [0, 0, 0],
                        d = null;
                    if (typeof nav.plugins !== UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] === OBJECT) {
                        d = nav.plugins[SHOCKWAVE_FLASH].description;
                        // nav.mimeTypes["application/x-shockwave-flash"].enabledPlugin indicates whether plug-ins are enabled or disabled in Safari 3+
                        if (d && (typeof nav.mimeTypes !== UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) {
                            plugin = true;
                            ie = false; // cascaded feature detection for Internet Explorer
                            d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
                            playerVersion[0] = toInt(d.replace(/^(.*)\..*$/, "$1"));
                            playerVersion[1] = toInt(d.replace(/^.*\.(.*)\s.*$/, "$1"));
                            playerVersion[2] = /[a-zA-Z]/.test(d) ? toInt(d.replace(/^.*[a-zA-Z]+(.*)$/, "$1")) : 0;
                        }
                    } else if (typeof win.ActiveXObject !== UNDEF) {
                        try {
                            var a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
                            if (a) { // a will return null when ActiveX is disabled
                                d = a.GetVariable("$version");
                                if (d) {
                                    ie = true; // cascaded feature detection for Internet Explorer
                                    d = d.split(" ")[1].split(",");
                                    playerVersion = [toInt(d[0]), toInt(d[1]), toInt(d[2])];
                                }
                            }
                        } catch (e) {}
                    }
                    return {
                        w3: w3cdom,
                        pv: playerVersion,
                        wk: webkit,
                        ie: ie,
                        win: windows,
                        mac: mac
                    };
                }(),

                /* Cross-browser onDomLoad
                    - Will fire an event as soon as the DOM of a web page is loaded
                    - Internet Explorer workaround based on Diego Perini's solution: http://javascript.nwbox.com/IEContentLoaded/
                    - Regular onload serves as fallback
                */
                onDomLoad = function() {
                    if (!ua.w3) {
                        return;
                    }
                    if ((typeof doc.readyState !== UNDEF && (doc.readyState === "complete" || doc.readyState === "interactive")) || (typeof doc.readyState === UNDEF && (doc.getElementsByTagName("body")[0] || doc.body))) { // function is fired after onload, e.g. when script is inserted dynamically
                        callDomLoadFunctions();
                    }
                    if (!isDomLoaded) {
                        if (typeof doc.addEventListener !== UNDEF) {
                            doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, false);
                        }
                        if (ua.ie) {
                            doc.attachEvent(ON_READY_STATE_CHANGE, function detach() {
                                if (doc.readyState === "complete") {
                                    doc.detachEvent(ON_READY_STATE_CHANGE, detach);
                                    callDomLoadFunctions();
                                }
                            });
                            if (win == top) { // if not inside an iframe
                                (function checkDomLoadedIE() {
                                    if (isDomLoaded) {
                                        return;
                                    }
                                    try {
                                        doc.documentElement.doScroll("left");
                                    } catch (e) {
                                        setTimeout(checkDomLoadedIE, 0);
                                        return;
                                    }
                                    callDomLoadFunctions();
                                }());
                            }
                        }
                        if (ua.wk) {
                            (function checkDomLoadedWK() {
                                if (isDomLoaded) {
                                    return;
                                }
                                if (!/loaded|complete/.test(doc.readyState)) {
                                    setTimeout(checkDomLoadedWK, 0);
                                    return;
                                }
                                callDomLoadFunctions();
                            }());
                        }
                    }
                }();

            function callDomLoadFunctions() {
                if (isDomLoaded || !document.getElementsByTagName("body")[0]) {
                    return;
                }
                try { // test if we can really add/remove elements to/from the DOM; we don't want to fire it too early
                    var t, span = createElement("span");
                    span.style.display = "none"; //hide the span in case someone has styled spans via CSS
                    t = doc.getElementsByTagName("body")[0].appendChild(span);
                    t.parentNode.removeChild(t);
                    t = null; //clear the variables
                    span = null;
                } catch (e) {
                    return;
                }
                isDomLoaded = true;
                var dl = domLoadFnArr.length;
                for (var i = 0; i < dl; i++) {
                    domLoadFnArr[i]();
                }
            }

            function addDomLoadEvent(fn) {
                if (isDomLoaded) {
                    fn();
                } else {
                    domLoadFnArr[domLoadFnArr.length] = fn; // Array.push() is only available in IE5.5+
                }
            }

            /* Cross-browser onload
                - Based on James Edwards' solution: http://brothercake.com/site/resources/scripts/onload/
                - Will fire an event as soon as a web page including all of its assets are loaded
             */
            function addLoadEvent(fn) {
                if (typeof win.addEventListener !== UNDEF) {
                    win.addEventListener("load", fn, false);
                } else if (typeof doc.addEventListener !== UNDEF) {
                    doc.addEventListener("load", fn, false);
                } else if (typeof win.attachEvent !== UNDEF) {
                    addListener(win, "onload", fn);
                } else if (typeof win.onload === "function") {
                    var fnOld = win.onload;
                    win.onload = function() {
                        fnOld();
                        fn();
                    };
                } else {
                    win.onload = fn;
                }
            }

            /* Detect the Flash Player version for non-Internet Explorer browsers
                - Detecting the plug-in version via the object element is more precise than using the plugins collection item's description:
                  a. Both release and build numbers can be detected
                  b. Avoid wrong descriptions by corrupt installers provided by Adobe
                  c. Avoid wrong descriptions by multiple Flash Player entries in the plugin Array, caused by incorrect browser imports
                - Disadvantage of this method is that it depends on the availability of the DOM, while the plugins collection is immediately available
            */
            function testPlayerVersion() {
                var b = doc.getElementsByTagName("body")[0];
                var o = createElement(OBJECT);
                o.setAttribute("style", "visibility: hidden;");
                o.setAttribute("type", FLASH_MIME_TYPE);
                var t = b.appendChild(o);
                if (t) {
                    var counter = 0;
                    (function checkGetVariable() {
                        if (typeof t.GetVariable !== UNDEF) {
                            try {
                                var d = t.GetVariable("$version");
                                if (d) {
                                    d = d.split(" ")[1].split(",");
                                    ua.pv = [toInt(d[0]), toInt(d[1]), toInt(d[2])];
                                }
                            } catch (e) {
                                //t.GetVariable("$version") is known to fail in Flash Player 8 on Firefox
                                //If this error is encountered, assume FP8 or lower. Time to upgrade.
                                ua.pv = [8, 0, 0];
                            }
                        } else if (counter < 10) {
                            counter++;
                            setTimeout(checkGetVariable, 10);
                            return;
                        }
                        b.removeChild(o);
                        t = null;
                        matchVersions();
                    }());
                } else {
                    matchVersions();
                }
            }

            /* Perform Flash Player and SWF version matching; static publishing only
             */
            function matchVersions() {
                var rl = regObjArr.length;
                if (rl > 0) {
                    for (var i = 0; i < rl; i++) { // for each registered object element
                        var id = regObjArr[i].id;
                        var cb = regObjArr[i].callbackFn;
                        var cbObj = {
                            success: false,
                            id: id
                        };
                        if (ua.pv[0] > 0) {
                            var obj = getElementById(id);
                            if (obj) {
                                if (hasPlayerVersion(regObjArr[i].swfVersion) && !(ua.wk && ua.wk < 312)) { // Flash Player version >= published SWF version: Houston, we have a match!
                                    setVisibility(id, true);
                                    if (cb) {
                                        cbObj.success = true;
                                        cbObj.ref = getObjectById(id);
                                        cbObj.id = id;
                                        cb(cbObj);
                                    }
                                } else if (regObjArr[i].expressInstall && canExpressInstall()) { // show the Adobe Express Install dialog if set by the web page author and if supported
                                    var att = {};
                                    att.data = regObjArr[i].expressInstall;
                                    att.width = obj.getAttribute("width") || "0";
                                    att.height = obj.getAttribute("height") || "0";
                                    if (obj.getAttribute("class")) {
                                        att.styleclass = obj.getAttribute("class");
                                    }
                                    if (obj.getAttribute("align")) {
                                        att.align = obj.getAttribute("align");
                                    }
                                    // parse HTML object param element's name-value pairs
                                    var par = {};
                                    var p = obj.getElementsByTagName("param");
                                    var pl = p.length;
                                    for (var j = 0; j < pl; j++) {
                                        if (p[j].getAttribute("name").toLowerCase() !== "movie") {
                                            par[p[j].getAttribute("name")] = p[j].getAttribute("value");
                                        }
                                    }
                                    showExpressInstall(att, par, id, cb);
                                } else { // Flash Player and SWF version mismatch or an older Webkit engine that ignores the HTML object element's nested param elements: display fallback content instead of SWF
                                    displayFbContent(obj);
                                    if (cb) {
                                        cb(cbObj);
                                    }
                                }
                            }
                        } else { // if no Flash Player is installed or the fp version cannot be detected we let the HTML object element do its job (either show a SWF or fallback content)
                            setVisibility(id, true);
                            if (cb) {
                                var o = getObjectById(id); // test whether there is an HTML object element or not
                                if (o && typeof o.SetVariable !== UNDEF) {
                                    cbObj.success = true;
                                    cbObj.ref = o;
                                    cbObj.id = o.id;
                                }
                                cb(cbObj);
                            }
                        }
                    }
                }
            }

            /* Main function
                - Will preferably execute onDomLoad, otherwise onload (as a fallback)
            */
            domLoadFnArr[0] = function() {
                if (plugin) {
                    testPlayerVersion();
                } else {
                    matchVersions();
                }
            };

            function getObjectById(objectIdStr) {
                var r = null,
                    o = getElementById(objectIdStr);

                if (o && o.nodeName.toUpperCase() === "OBJECT") {
                    //If targeted object is valid Flash file
                    if (typeof o.SetVariable !== UNDEF) {
                        r = o;
                    } else {
                        //If SetVariable is not working on targeted object but a nested object is
                        //available, assume classic nested object markup. Return nested object.

                        //If SetVariable is not working on targeted object and there is no nested object,
                        //return the original object anyway. This is probably new simplified markup.

                        r = o.getElementsByTagName(OBJECT)[0] || o;
                    }
                }

                return r;
            }

            /* Requirements for Adobe Express Install
                - only one instance can be active at a time
                - fp 6.0.65 or higher
                - Win/Mac OS only
                - no Webkit engines older than version 312
            */
            function canExpressInstall() {
                return !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac) && !(ua.wk && ua.wk < 312);
            }

            /* Show the Adobe Express Install dialog
                - Reference: http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=6a253b75
            */
            function showExpressInstall(att, par, replaceElemIdStr, callbackFn) {

                var obj = getElementById(replaceElemIdStr);

                //Ensure that replaceElemIdStr is really a string and not an element
                replaceElemIdStr = getId(replaceElemIdStr);

                isExpressInstallActive = true;
                storedCallbackFn = callbackFn || null;
                storedCallbackObj = {
                    success: false,
                    id: replaceElemIdStr
                };

                if (obj) {
                    if (obj.nodeName.toUpperCase() === "OBJECT") { // static publishing
                        storedFbContent = abstractFbContent(obj);
                        storedFbContentId = null;
                    } else { // dynamic publishing
                        storedFbContent = obj;
                        storedFbContentId = replaceElemIdStr;
                    }
                    att.id = EXPRESS_INSTALL_ID;
                    if (typeof att.width === UNDEF || (!/%$/.test(att.width) && toInt(att.width) < 310)) {
                        att.width = "310";
                    }
                    if (typeof att.height === UNDEF || (!/%$/.test(att.height) && toInt(att.height) < 137)) {
                        att.height = "137";
                    }
                    var pt = ua.ie ? "ActiveX" : "PlugIn",
                        fv = "MMredirectURL=" + encodeURIComponent(win.location.toString().replace(/&/g, "%26")) + "&MMplayerType=" + pt + "&MMdoctitle=" + encodeURIComponent(doc.title.slice(0, 47) + " - Flash Player Installation");
                    if (typeof par.flashvars !== UNDEF) {
                        par.flashvars += "&" + fv;
                    } else {
                        par.flashvars = fv;
                    }
                    // IE only: when a SWF is loading (AND: not available in cache) wait for the readyState of the object element to become 4 before removing it,
                    // because you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                    if (ua.ie && obj.readyState != 4) {
                        var newObj = createElement("div");
                        replaceElemIdStr += "SWFObjectNew";
                        newObj.setAttribute("id", replaceElemIdStr);
                        obj.parentNode.insertBefore(newObj, obj); // insert placeholder div that will be replaced by the object element that loads expressinstall.swf
                        obj.style.display = "none";
                        removeSWF(obj); //removeSWF accepts elements now
                    }
                    createSWF(att, par, replaceElemIdStr);
                }
            }

            /* Functions to abstract and display fallback content
             */
            function displayFbContent(obj) {
                if (ua.ie && obj.readyState != 4) {
                    // IE only: when a SWF is loading (AND: not available in cache) wait for the readyState of the object element to become 4 before removing it,
                    // because you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                    obj.style.display = "none";
                    var el = createElement("div");
                    obj.parentNode.insertBefore(el, obj); // insert placeholder div that will be replaced by the fallback content
                    el.parentNode.replaceChild(abstractFbContent(obj), el);
                    removeSWF(obj); //removeSWF accepts elements now
                } else {
                    obj.parentNode.replaceChild(abstractFbContent(obj), obj);
                }
            }

            function abstractFbContent(obj) {
                var ac = createElement("div");
                if (ua.win && ua.ie) {
                    ac.innerHTML = obj.innerHTML;
                } else {
                    var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                    if (nestedObj) {
                        var c = nestedObj.childNodes;
                        if (c) {
                            var cl = c.length;
                            for (var i = 0; i < cl; i++) {
                                if (!(c[i].nodeType == 1 && c[i].nodeName === "PARAM") && !(c[i].nodeType == 8)) {
                                    ac.appendChild(c[i].cloneNode(true));
                                }
                            }
                        }
                    }
                }
                return ac;
            }

            function createIeObject(url, paramStr) {
                var div = createElement("div");
                div.innerHTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'><param name='movie' value='" + url + "'>" + paramStr + "</object>";
                return div.firstChild;
            }

            /* Cross-browser dynamic SWF creation
             */
            function createSWF(attObj, parObj, id) {
                var r, el = getElementById(id);
                id = getId(id); // ensure id is truly an ID and not an element

                if (ua.wk && ua.wk < 312) {
                    return r;
                }

                if (el) {
                    var o = (ua.ie) ? createElement("div") : createElement(OBJECT),
                        attr,
                        attrLower,
                        param;

                    if (typeof attObj.id === UNDEF) { // if no 'id' is defined for the object element, it will inherit the 'id' from the fallback content
                        attObj.id = id;
                    }

                    //Add params
                    for (param in parObj) {
                        //filter out prototype additions from other potential libraries and IE specific param element
                        if (parObj.hasOwnProperty(param) && param.toLowerCase() !== "movie") {
                            createObjParam(o, param, parObj[param]);
                        }
                    }

                    //Create IE object, complete with param nodes
                    if (ua.ie) {
                        o = createIeObject(attObj.data, o.innerHTML);
                    }

                    //Add attributes to object
                    for (attr in attObj) {
                        if (attObj.hasOwnProperty(attr)) { // filter out prototype additions from other potential libraries
                            attrLower = attr.toLowerCase();

                            // 'class' is an ECMA4 reserved keyword
                            if (attrLower === "styleclass") {
                                o.setAttribute("class", attObj[attr]);
                            } else if (attrLower !== "classid" && attrLower !== "data") {
                                o.setAttribute(attr, attObj[attr]);
                            }
                        }
                    }

                    if (ua.ie) {
                        objIdArr[objIdArr.length] = attObj.id; // stored to fix object 'leaks' on unload (dynamic publishing only)
                    } else {
                        o.setAttribute("type", FLASH_MIME_TYPE);
                        o.setAttribute("data", attObj.data);
                    }

                    el.parentNode.replaceChild(o, el);
                    r = o;
                }

                return r;
            }

            function createObjParam(el, pName, pValue) {
                var p = createElement("param");
                p.setAttribute("name", pName);
                p.setAttribute("value", pValue);
                el.appendChild(p);
            }

            /* Cross-browser SWF removal
                - Especially needed to safely and completely remove a SWF in Internet Explorer
            */
            function removeSWF(id) {
                var obj = getElementById(id);
                if (obj && obj.nodeName.toUpperCase() === "OBJECT") {
                    if (ua.ie) {
                        obj.style.display = "none";
                        (function removeSWFInIE() {
                            if (obj.readyState == 4) {
                                //This step prevents memory leaks in Internet Explorer
                                for (var i in obj) {
                                    if (typeof obj[i] === "function") {
                                        obj[i] = null;
                                    }
                                }
                                obj.parentNode.removeChild(obj);
                            } else {
                                setTimeout(removeSWFInIE, 10);
                            }
                        }());
                    } else {
                        obj.parentNode.removeChild(obj);
                    }
                }
            }

            function isElement(id) {
                return (id && id.nodeType && id.nodeType === 1);
            }

            function getId(thing) {
                return (isElement(thing)) ? thing.id : thing;
            }

            /* Functions to optimize JavaScript compression
             */
            function getElementById(id) {

                //Allow users to pass an element OR an element's ID
                if (isElement(id)) {
                    return id;
                }

                var el = null;
                try {
                    el = doc.getElementById(id);
                } catch (e) {}
                return el;
            }

            function createElement(el) {
                return doc.createElement(el);
            }

            //To aid compression; replaces 14 instances of pareseInt with radix
            function toInt(str) {
                return parseInt(str, 10);
            }

            /* Updated attachEvent function for Internet Explorer
                - Stores attachEvent information in an Array, so on unload the detachEvent functions can be called to avoid memory leaks
            */
            function addListener(target, eventType, fn) {
                target.attachEvent(eventType, fn);
                listenersArr[listenersArr.length] = [target, eventType, fn];
            }

            /* Flash Player and SWF content version matching
             */
            function hasPlayerVersion(rv) {
                rv += ""; //Coerce number to string, if needed.
                var pv = ua.pv,
                    v = rv.split(".");
                v[0] = toInt(v[0]);
                v[1] = toInt(v[1]) || 0; // supports short notation, e.g. "9" instead of "9.0.0"
                v[2] = toInt(v[2]) || 0;
                return (pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2])) ? true : false;
            }

            /* Cross-browser dynamic CSS creation
                - Based on Bobby van der Sluis' solution: http://www.bobbyvandersluis.com/articles/dynamicCSS.php
            */
            function createCSS(sel, decl, media, newStyle) {
                var h = doc.getElementsByTagName("head")[0];
                if (!h) {
                    return;
                } // to also support badly authored HTML pages that lack a head element
                var m = (typeof media === "string") ? media : "screen";
                if (newStyle) {
                    dynamicStylesheet = null;
                    dynamicStylesheetMedia = null;
                }
                if (!dynamicStylesheet || dynamicStylesheetMedia != m) {
                    // create dynamic stylesheet + get a global reference to it
                    var s = createElement("style");
                    s.setAttribute("type", "text/css");
                    s.setAttribute("media", m);
                    dynamicStylesheet = h.appendChild(s);
                    if (ua.ie && typeof doc.styleSheets !== UNDEF && doc.styleSheets.length > 0) {
                        dynamicStylesheet = doc.styleSheets[doc.styleSheets.length - 1];
                    }
                    dynamicStylesheetMedia = m;
                }
                // add style rule
                if (dynamicStylesheet) {
                    if (typeof dynamicStylesheet.addRule !== UNDEF) {
                        dynamicStylesheet.addRule(sel, decl);
                    } else if (typeof doc.createTextNode !== UNDEF) {
                        dynamicStylesheet.appendChild(doc.createTextNode(sel + " {" + decl + "}"));
                    }
                }
            }

            function setVisibility(id, isVisible) {
                if (!autoHideShow) {
                    return;
                }
                var v = isVisible ? "visible" : "hidden",
                    el = getElementById(id);
                if (isDomLoaded && el) {
                    el.style.visibility = v;
                } else if (typeof id === "string") {
                    createCSS("#" + id, "visibility:" + v);
                }
            }

            /* Filter to avoid XSS attacks
             */
            function urlEncodeIfNecessary(s) {
                var regex = /[\\\"<>\.;]/;
                var hasBadChars = regex.exec(s) !== null;
                return hasBadChars && typeof encodeURIComponent !== UNDEF ? encodeURIComponent(s) : s;
            }

            /* Release memory to avoid memory leaks caused by closures, fix hanging audio/video threads and force open sockets/NetConnections to disconnect (Internet Explorer only)
             */
            var cleanup = function() {
                if (ua.ie) {
                    window.attachEvent("onunload", function() {
                        // remove listeners to avoid memory leaks
                        var ll = listenersArr.length;
                        for (var i = 0; i < ll; i++) {
                            listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
                        }
                        // cleanup dynamically embedded objects to fix audio/video threads and force open sockets and NetConnections to disconnect
                        var il = objIdArr.length;
                        for (var j = 0; j < il; j++) {
                            removeSWF(objIdArr[j]);
                        }
                        // cleanup library's main closures to avoid memory leaks
                        for (var k in ua) {
                            ua[k] = null;
                        }
                        ua = null;
                        for (var l in swfobject) {
                            swfobject[l] = null;
                        }
                        swfobject = null;
                    });
                }
            }();

            return {
                /* Public API
                    - Reference: http://code.google.com/p/swfobject/wiki/documentation
                */
                registerObject: function(objectIdStr, swfVersionStr, xiSwfUrlStr, callbackFn) {
                    if (ua.w3 && objectIdStr && swfVersionStr) {
                        var regObj = {};
                        regObj.id = objectIdStr;
                        regObj.swfVersion = swfVersionStr;
                        regObj.expressInstall = xiSwfUrlStr;
                        regObj.callbackFn = callbackFn;
                        regObjArr[regObjArr.length] = regObj;
                        setVisibility(objectIdStr, false);
                    } else if (callbackFn) {
                        callbackFn({
                            success: false,
                            id: objectIdStr
                        });
                    }
                },

                getObjectById: function(objectIdStr) {
                    if (ua.w3) {
                        return getObjectById(objectIdStr);
                    }
                },

                embedSWF: function(swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj, callbackFn) {

                    var id = getId(replaceElemIdStr),
                        callbackObj = {
                            success: false,
                            id: id
                        };

                    if (ua.w3 && !(ua.wk && ua.wk < 312) && swfUrlStr && replaceElemIdStr && widthStr && heightStr && swfVersionStr) {
                        setVisibility(id, false);
                        addDomLoadEvent(function() {
                            widthStr += ""; // auto-convert to string
                            heightStr += "";
                            var att = {};
                            if (attObj && typeof attObj === OBJECT) {
                                for (var i in attObj) { // copy object to avoid the use of references, because web authors often reuse attObj for multiple SWFs
                                    att[i] = attObj[i];
                                }
                            }
                            att.data = swfUrlStr;
                            att.width = widthStr;
                            att.height = heightStr;
                            var par = {};
                            if (parObj && typeof parObj === OBJECT) {
                                for (var j in parObj) { // copy object to avoid the use of references, because web authors often reuse parObj for multiple SWFs
                                    par[j] = parObj[j];
                                }
                            }
                            if (flashvarsObj && typeof flashvarsObj === OBJECT) {
                                for (var k in flashvarsObj) { // copy object to avoid the use of references, because web authors often reuse flashvarsObj for multiple SWFs
                                    if (flashvarsObj.hasOwnProperty(k)) {

                                        var key = (encodeURIEnabled) ? encodeURIComponent(k) : k,
                                            value = (encodeURIEnabled) ? encodeURIComponent(flashvarsObj[k]) : flashvarsObj[k];

                                        if (typeof par.flashvars !== UNDEF) {
                                            par.flashvars += "&" + key + "=" + value;
                                        } else {
                                            par.flashvars = key + "=" + value;
                                        }

                                    }
                                }
                            }
                            if (hasPlayerVersion(swfVersionStr)) { // create SWF
                                var obj = createSWF(att, par, replaceElemIdStr);
                                if (att.id == id) {
                                    setVisibility(id, true);
                                }
                                callbackObj.success = true;
                                callbackObj.ref = obj;
                                callbackObj.id = obj.id;
                            } else if (xiSwfUrlStr && canExpressInstall()) { // show Adobe Express Install
                                att.data = xiSwfUrlStr;
                                showExpressInstall(att, par, replaceElemIdStr, callbackFn);
                                return;
                            } else { // show fallback content
                                setVisibility(id, true);
                            }
                            if (callbackFn) {
                                callbackFn(callbackObj);
                            }
                        });
                    } else if (callbackFn) {
                        callbackFn(callbackObj);
                    }
                },

                switchOffAutoHideShow: function() {
                    autoHideShow = false;
                },

                enableUriEncoding: function(bool) {
                    encodeURIEnabled = (typeof bool === UNDEF) ? true : bool;
                },

                ua: ua,

                getFlashPlayerVersion: function() {
                    return {
                        major: ua.pv[0],
                        minor: ua.pv[1],
                        release: ua.pv[2]
                    };
                },

                hasFlashPlayerVersion: hasPlayerVersion,

                createSWF: function(attObj, parObj, replaceElemIdStr) {
                    if (ua.w3) {
                        return createSWF(attObj, parObj, replaceElemIdStr);
                    } else {
                        return undefined;
                    }
                },

                showExpressInstall: function(att, par, replaceElemIdStr, callbackFn) {
                    if (ua.w3 && canExpressInstall()) {
                        showExpressInstall(att, par, replaceElemIdStr, callbackFn);
                    }
                },

                removeSWF: function(objElemIdStr) {
                    if (ua.w3) {
                        removeSWF(objElemIdStr);
                    }
                },

                createCSS: function(selStr, declStr, mediaStr, newStyleBoolean) {
                    if (ua.w3) {
                        createCSS(selStr, declStr, mediaStr, newStyleBoolean);
                    }
                },

                addDomLoadEvent: addDomLoadEvent,

                addLoadEvent: addLoadEvent,

                getQueryParamValue: function(param) {
                    var q = doc.location.search || doc.location.hash;
                    if (q) {
                        if (/\?/.test(q)) {
                            q = q.split("?")[1];
                        } // strip question mark
                        if (!param) {
                            return urlEncodeIfNecessary(q);
                        }
                        var pairs = q.split("&");
                        for (var i = 0; i < pairs.length; i++) {
                            if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                                return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                            }
                        }
                    }
                    return "";
                },

                // For internal usage only
                expressInstallCallback: function() {
                    if (isExpressInstallActive) {
                        var obj = getElementById(EXPRESS_INSTALL_ID);
                        if (obj && storedFbContent) {
                            obj.parentNode.replaceChild(storedFbContent, obj);
                            if (storedFbContentId) {
                                setVisibility(storedFbContentId, true);
                                if (ua.ie) {
                                    storedFbContent.style.display = "block";
                                }
                            }
                            if (storedCallbackFn) {
                                storedCallbackFn(storedCallbackObj);
                            }
                        }
                        isExpressInstallActive = false;
                    }
                },

                version: "2.3"

            };
        }));

    }, {}],
    15: [function(require, module, exports) {
        'use strict';

        var InLine = require('./InLine');
        var Wrapper = require('./Wrapper');

        function Ad(adJTree) {
            if (!(this instanceof Ad)) {
                return new Ad(adJTree);
            }
            this.initialize(adJTree);
        }

        Ad.prototype.initialize = function(adJTree) {
            this.id = adJTree.attr('id');
            this.sequence = adJTree.attr('sequence');

            if (adJTree.inLine) {
                this.inLine = new InLine(adJTree.inLine);
            }

            if (adJTree.wrapper) {
                this.wrapper = new Wrapper(adJTree.wrapper);
            }
        };

        module.exports = Ad;
    }, {
        "./InLine": 18,
        "./Wrapper": 28
    }],
    16: [function(require, module, exports) {
        'use strict';

        var TrackingEvent = require('./TrackingEvent');

        var utilities = require('../../utils/utilityFunctions');

        var xml = require('../../utils/xml');

        var logger = require('../../utils/consoleLogger');


        function Companion(companionJTree) {
            if (!(this instanceof Companion)) {
                return new Companion(companionJTree);
            }

            logger.info("<Companion> found companion ad");
            logger.debug("<Companion>  companionJTree:", companionJTree);

            //Required Elements
            this.creativeType = xml.attr(companionJTree.staticResource, 'creativeType');
            this.staticResource = xml.keyValue(companionJTree.staticResource);

            logger.info("<Companion>  creativeType: " + this.creativeType);
            logger.info("<Companion>  staticResource: " + this.staticResource);

            // Weird bug when the JXON tree is built it doesn't handle casing properly in this situation...
            var htmlResource = null;
            if (xml.keyValue(companionJTree.HTMLResource)) {
                htmlResource = xml.keyValue(companionJTree.HTMLResource);
            } else if (xml.keyValue(companionJTree.hTMLResource)) {
                htmlResource = xml.keyValue(companionJTree.hTMLResource);
            }

            if (htmlResource !== null) {
                logger.info("<Companion> found html resource", htmlResource);
            }

            this.htmlResource = htmlResource;

            var iframeResource = null;
            if (xml.keyValue(companionJTree.IFrameResource)) {
                iframeResource = xml.keyValue(companionJTree.IFrameResource);
            } else if (xml.keyValue(companionJTree.iFrameresource)) {
                iframeResource = xml.keyValue(companionJTree.iFrameresource);
            }

            if (iframeResource !== null) {
                logger.info("<Companion> found iframe resource", iframeResource);
            }

            this.iframeResource = iframeResource;

            //Optional fields
            this.id = xml.attr(companionJTree, 'id');
            this.width = xml.attr(companionJTree, 'width');
            this.height = xml.attr(companionJTree, 'height');
            this.expandedWidth = xml.attr(companionJTree, 'expandedWidth');
            this.expandedHeight = xml.attr(companionJTree, 'expandedHeight');
            this.scalable = xml.attr(companionJTree, 'scalable');
            this.maintainAspectRatio = xml.attr(companionJTree, 'maintainAspectRatio');
            this.minSuggestedDuration = xml.attr(companionJTree, 'minSuggestedDuration');
            this.apiFramework = xml.attr(companionJTree, 'apiFramework');
            this.companionClickThrough = xml.keyValue(companionJTree.companionClickThrough);
            this.trackingEvents = parseTrackingEvents(companionJTree.trackingEvents && companionJTree.trackingEvents.tracking);

            logger.info("<Companion>  companionClickThrough: " + this.companionClickThrough);


            /*** Local functions ***/
            function parseTrackingEvents(trackingEvents) {
                var trackings = [];
                if (utilities.isDefined(trackingEvents)) {
                    trackingEvents = utilities.isArray(trackingEvents) ? trackingEvents : [trackingEvents];
                    trackingEvents.forEach(function(trackingData) {
                        trackings.push(new TrackingEvent(trackingData));
                    });
                }
                return trackings;
            }
        }

        module.exports = Companion;
    }, {
        "../../utils/consoleLogger": 41,
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./TrackingEvent": 21
    }],
    17: [function(require, module, exports) {
        'use strict';

        var Linear = require('./Linear');
        var Companion = require('./Companion');
        var utilities = require('../../utils/utilityFunctions');

        function Creative(creativeJTree) {
            if (!(this instanceof Creative)) {
                return new Creative(creativeJTree);
            }

            this.id = creativeJTree.attr('id');
            this.sequence = creativeJTree.attr('sequence');
            this.adId = creativeJTree.attr('adId');
            this.apiFramework = creativeJTree.attr('apiFramework');

            if (creativeJTree.linear) {
                this.linear = new Linear(creativeJTree.linear);
            }

            if (creativeJTree.companionAds) {
                var companions = [];
                var companionAds = creativeJTree.companionAds && creativeJTree.companionAds.companion;
                if (utilities.isDefined(companionAds)) {
                    companionAds = utilities.isArray(companionAds) ? companionAds : [companionAds];
                    companionAds.forEach(function(companionData) {
                        companions.push(new Companion(companionData));
                    });
                }
                this.companionAds = companions;
            }
        }

        /**
         * Returns true if the browser supports at the creative.
         */
        Creative.prototype.isSupported = function() {
            if (this.linear) {
                return this.linear.isSupported();
            }

            return true;
        };

        Creative.parseCreatives = function parseCreatives(creativesJTree) {
            var creatives = [];
            var creativesData;
            if (utilities.isDefined(creativesJTree) && utilities.isDefined(creativesJTree.creative)) {
                creativesData = utilities.isArray(creativesJTree.creative) ? creativesJTree.creative : [creativesJTree.creative];
                creativesData.forEach(function(creative) {
                    creatives.push(new Creative(creative));
                });
            }
            return creatives;
        };

        module.exports = Creative;

    }, {
        "../../utils/utilityFunctions": 47,
        "./Companion": 16,
        "./Linear": 19
    }],
    18: [function(require, module, exports) {
        'use strict';

        var vastUtil = require('./vastUtil');
        var Creative = require('./Creative');

        var utilities = require('../../utils/utilityFunctions');
        var xml = require('../../utils/xml');

        function InLine(inlineJTree) {
            if (!(this instanceof InLine)) {
                return new InLine(inlineJTree);
            }

            //Required Fields
            this.adTitle = xml.keyValue(inlineJTree.adTitle);
            this.adSystem = xml.keyValue(inlineJTree.adSystem);
            this.impressions = vastUtil.parseImpressions(inlineJTree.impression);
            this.creatives = Creative.parseCreatives(inlineJTree.creatives);

            //Optional Fields
            this.description = xml.keyValue(inlineJTree.description);
            this.advertiser = xml.keyValue(inlineJTree.advertiser);
            this.surveys = parseSurveys(inlineJTree.survey);
            this.error = xml.keyValue(inlineJTree.error);
            this.pricing = xml.keyValue(inlineJTree.pricing);
            this.extensions = inlineJTree.extensions;

            /*** Local Functions ***/
            function parseSurveys(inlineSurveys) {
                if (inlineSurveys) {
                    return utilities.transformArray(utilities.isArray(inlineSurveys) ? inlineSurveys : [inlineSurveys], function(survey) {
                        if (utilities.isNotEmptyString(survey.keyValue)) {
                            return {
                                uri: survey.keyValue,
                                type: survey.attr('type')
                            };
                        }

                        return undefined;
                    });
                }
                return [];
            }
        }


        /**
         * Returns true if the browser supports all the creatives.
         */
        InLine.prototype.isSupported = function() {
            var i, len;

            if (this.creatives.length === 0) {
                return false;
            }

            for (i = 0, len = this.creatives.length; i < len; i += 1) {
                if (!this.creatives[i].isSupported()) {
                    return false;
                }
            }
            return true;
        };

        module.exports = InLine;

    }, {
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./Creative": 17,
        "./vastUtil": 30
    }],
    19: [function(require, module, exports) {
        'use strict';

        var TrackingEvent = require('./TrackingEvent');
        var MediaFile = require('./MediaFile');
        var VideoClicks = require('./VideoClicks');

        var utilities = require('../../utils/utilityFunctions');
        var parsers = require('./parsers');

        var xml = require('../../utils/xml');


        function Linear(linearJTree) {
            if (!(this instanceof Linear)) {
                return new Linear(linearJTree);
            }

            //Required Elements
            this.duration = parsers.duration(xml.keyValue(linearJTree.duration));
            this.mediaFiles = parseMediaFiles(linearJTree.mediaFiles && linearJTree.mediaFiles.mediaFile);

            //Optional fields
            this.trackingEvents = parseTrackingEvents(linearJTree.trackingEvents && linearJTree.trackingEvents.tracking, this.duration);
            this.skipoffset = parsers.offset(xml.attr(linearJTree, 'skipoffset'), this.duration);

            if (linearJTree.videoClicks) {
                this.videoClicks = new VideoClicks(linearJTree.videoClicks);
            }

            if (linearJTree.adParameters) {
                this.adParameters = xml.keyValue(linearJTree.adParameters);

                if (xml.attr(linearJTree.adParameters, 'xmlEncoded')) {
                    this.adParameters = xml.decode(this.adParameters);
                }
            }

            /*** Local functions ***/
            function parseTrackingEvents(trackingEvents, duration) {
                var trackings = [];
                if (utilities.isDefined(trackingEvents)) {
                    trackingEvents = utilities.isArray(trackingEvents) ? trackingEvents : [trackingEvents];
                    trackingEvents.forEach(function(trackingData) {
                        trackings.push(new TrackingEvent(trackingData, duration));
                    });
                }
                return trackings;
            }

            function parseMediaFiles(mediaFilesJxonTree) {
                var mediaFiles = [];
                if (utilities.isDefined(mediaFilesJxonTree)) {
                    mediaFilesJxonTree = utilities.isArray(mediaFilesJxonTree) ? mediaFilesJxonTree : [mediaFilesJxonTree];

                    mediaFilesJxonTree.forEach(function(mfData) {
                        mediaFiles.push(new MediaFile(mfData));
                    });
                }
                return mediaFiles;
            }
        }

        /**
         * Must return true if at least one of the MediaFiles' type is supported
         */
        Linear.prototype.isSupported = function() {
            var i, len;
            for (i = 0, len = this.mediaFiles.length; i < len; i += 1) {
                if (this.mediaFiles[i].isSupported()) {
                    return true;
                }
            }

            return false;
        };

        module.exports = Linear;

    }, {
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./MediaFile": 20,
        "./TrackingEvent": 21,
        "./VideoClicks": 27,
        "./parsers": 29
    }],
    20: [function(require, module, exports) {
        'use strict';

        var xml = require('../../utils/xml');
        var vastUtil = require('./vastUtil');

        var attributesList = [
            //Required attributes
            'delivery',
            'type',
            'width',
            'height',
            //Optional attributes
            'codec',
            'id',
            'bitrate',
            'minBitrate',
            'maxBitrate',
            'scalable',
            'maintainAspectRatio',
            'apiFramework'
        ];

        function MediaFile(mediaFileJTree) {
            if (!(this instanceof MediaFile)) {
                return new MediaFile(mediaFileJTree);
            }

            //Required attributes
            this.src = xml.keyValue(mediaFileJTree);

            for (var x = 0; x < attributesList.length; x++) {
                var attribute = attributesList[x];
                this[attribute] = mediaFileJTree.attr(attribute);
            }
        }

        MediaFile.prototype.isSupported = function() {
            if (vastUtil.isVPAID(this)) {
                return !!vastUtil.findSupportedVPAIDTech(this.type);
            }

            if (this.type === 'video/x-flv') {
                return vastUtil.isFlashSupported();
            }

            return true;
        };

        module.exports = MediaFile;

    }, {
        "../../utils/xml": 48,
        "./vastUtil": 30
    }],
    21: [function(require, module, exports) {
        'use strict';

        var parsers = require('./parsers');

        var xml = require('../../utils/xml');

        function TrackingEvent(trackingJTree, duration) {
            if (!(this instanceof TrackingEvent)) {
                return new TrackingEvent(trackingJTree, duration);
            }

            this.name = trackingJTree.attr('event');
            this.uri = xml.keyValue(trackingJTree);

            if ('progress' === this.name) {
                this.offset = parsers.offset(trackingJTree.attr('offset'), duration);
            }
        }

        module.exports = TrackingEvent;
    }, {
        "../../utils/xml": 48,
        "./parsers": 29
    }],
    22: [function(require, module, exports) {
        'use strict';

        var Ad = require('./Ad');
        var VASTError = require('./VASTError');
        var VASTResponse = require('./VASTResponse');
        var vastUtil = require('./vastUtil');

        var async = require('../../utils/async');
        var http = require('../../utils/http').http;
        var utilities = require('../../utils/utilityFunctions');
        var xml = require('../../utils/xml');

        var logger = require('../../utils/consoleLogger');

        function VASTClient(options) {
            if (!(this instanceof VASTClient)) {
                return new VASTClient(options);
            }
            var defaultOptions = {
                WRAPPER_LIMIT: 5
            };

            options = options || {};
            this.settings = utilities.extend({}, options, defaultOptions);
            this.errorURLMacros = [];
        }

        VASTClient.prototype.getVASTResponse = function getVASTResponse(adTagUrl, callback) {
            var that = this;

            var error = sanityCheck(adTagUrl, callback);
            if (error) {
                if (utilities.isFunction(callback)) {
                    return callback(error);
                }
                throw error;
            }

            async.waterfall([
                    this._getVASTAd.bind(this, adTagUrl),
                    buildVASTResponse
                ],
                callback);

            /*** Local functions ***/
            function buildVASTResponse(adsChain, cb) {
                try {
                    var response = that._buildVASTResponse(adsChain);
                    cb(null, response);
                } catch (e) {
                    cb(e);
                }
            }

            function sanityCheck(adTagUrl, cb) {
                if (!adTagUrl) {
                    return new VASTError('on VASTClient.getVASTResponse, missing ad tag URL');
                }

                if (!utilities.isFunction(cb)) {
                    return new VASTError('on VASTClient.getVASTResponse, missing callback function');
                }
            }
        };

        VASTClient.prototype._getVASTAd = function(adTagUrl, callback) {
            var that = this;

            getAdWaterfall(adTagUrl, function(error, vastTree) {
                var waterfallAds = vastTree && utilities.isArray(vastTree.ads) ? vastTree.ads : null;
                if (error) {
                    that._trackError(error, waterfallAds);
                    return callback(error, waterfallAds);
                }

                getAd(waterfallAds.shift(), [], waterfallHandler);

                /*** Local functions ***/
                function waterfallHandler(error, adChain) {
                    if (error) {
                        that._trackError(error, adChain);
                        if (waterfallAds.length > 0) {
                            getAd(waterfallAds.shift(), [], waterfallHandler);
                        } else {
                            callback(error, adChain);
                        }
                    } else {
                        callback(null, adChain);
                    }
                }
            });

            /*** Local functions ***/
            function getAdWaterfall(adTagUrl, callback) {
                var requestVastXML = that._requestVASTXml.bind(that, adTagUrl);
                async.waterfall([
                    requestVastXML,
                    buildVastWaterfall
                ], callback);
            }

            function buildVastWaterfall(xmlStr, callback) {
                var vastTree;
                try {
                    vastTree = xml.toJXONTree(xmlStr);
                    logger.debug("built JXONTree from VAST response:", vastTree);

                    if (utilities.isArray(vastTree.ad)) {
                        vastTree.ads = vastTree.ad;
                    } else if (vastTree.ad) {
                        vastTree.ads = [vastTree.ad];
                    } else {
                        vastTree.ads = [];
                    }
                    callback(validateVASTTree(vastTree), vastTree);

                } catch (e) {
                    callback(new VASTError("on VASTClient.getVASTAd.buildVastWaterfall, error parsing xml", 100), null);
                }
            }

            function validateVASTTree(vastTree) {
                var vastVersion = xml.attr(vastTree, 'version');

                if (!vastTree.ad) {
                    return new VASTError('on VASTClient.getVASTAd.validateVASTTree, no Ad in VAST tree', 303);
                }

                if (vastVersion && (vastVersion != 3 && vastVersion != 2)) {
                    return new VASTError('on VASTClient.getVASTAd.validateVASTTree, not supported VAST version "' + vastVersion + '"', 102);
                }

                return null;
            }

            function getAd(adTagUrl, adChain, callback) {
                if (adChain.length >= that.WRAPPER_LIMIT) {
                    return callback(new VASTError("on VASTClient.getVASTAd.getAd, players wrapper limit reached (the limit is " + that.WRAPPER_LIMIT + ")", 302), adChain);
                }

                async.waterfall([
                    function(next) {
                        if (utilities.isString(adTagUrl)) {
                            requestVASTAd(adTagUrl, next);
                        } else {
                            next(null, adTagUrl);
                        }
                    },
                    buildAd
                ], function(error, ad) {
                    if (ad) {
                        adChain.push(ad);
                    }

                    if (error) {
                        return callback(error, adChain);
                    }

                    if (ad.wrapper) {
                        return getAd(ad.wrapper.VASTAdTagURI, adChain, callback);
                    }

                    return callback(null, adChain);
                });
            }

            function buildAd(adJxonTree, callback) {
                try {
                    var ad = new Ad(adJxonTree);
                    callback(validateAd(ad), ad);
                } catch (e) {
                    callback(new VASTError('on VASTClient.getVASTAd.buildAd, error parsing xml', 100), null);
                }
            }

            function validateAd(ad) {
                var wrapper = ad.wrapper;
                var inLine = ad.inLine;
                var errMsgPrefix = 'on VASTClient.getVASTAd.validateAd, ';

                if (inLine && wrapper) {
                    return new VASTError(errMsgPrefix + "InLine and Wrapper both found on the same Ad", 101);
                }

                if (!inLine && !wrapper) {
                    return new VASTError(errMsgPrefix + "nor wrapper nor inline elements found on the Ad", 101);
                }

                if (inLine && !inLine.isSupported()) {
                    return new VASTError(errMsgPrefix + "could not find MediaFile that is supported by this video player", 403);
                }

                if (wrapper && !wrapper.VASTAdTagURI) {
                    return new VASTError(errMsgPrefix + "missing 'VASTAdTagURI' in wrapper", 101);
                }

                return null;
            }

            function requestVASTAd(adTagUrl, callback) {
                that._requestVASTXml(adTagUrl, function(error, xmlStr) {
                    if (error) {
                        return callback(error);
                    }
                    try {
                        var vastTree = xml.toJXONTree(xmlStr);
                        callback(validateVASTTree(vastTree), vastTree.ad);
                    } catch (e) {
                        callback(new VASTError("on VASTClient.getVASTAd.requestVASTAd, error parsing xml", 100));
                    }
                });
            }
        };

        VASTClient.prototype._requestVASTXml = function requestVASTXml(adTagUrl, callback) {
            try {
                if (utilities.isFunction(adTagUrl)) {
                    adTagUrl(requestHandler);
                } else {
                    logger.info("requesting adTagUrl: " + adTagUrl);
                    http.get(adTagUrl, requestHandler, {
                        withCredentials: true
                    });
                }
            } catch (e) {
                callback(e);
            }

            /*** Local functions ***/
            function requestHandler(error, response, status) {
                if (error) {
                    var errMsg = utilities.isDefined(status) ?
                        "on VASTClient.requestVastXML, HTTP request error with status '" + status + "'" :
                        "on VASTClient.requestVastXML, Error getting the the VAST XML with he passed adTagXML fn";
                    return callback(new VASTError(errMsg, 301), null);
                }

                callback(null, response);
            }
        };

        VASTClient.prototype._buildVASTResponse = function buildVASTResponse(adsChain) {
            var response = new VASTResponse();
            addAdsToResponse(response, adsChain);
            validateResponse(response);

            return response;

            //*** Local function ****
            function addAdsToResponse(response, ads) {
                ads.forEach(function(ad) {
                    response.addAd(ad);
                });
            }

            function validateResponse(response) {
                var progressEvents = response.trackingEvents.progress;

                if (!response.hasLinear()) {
                    throw new VASTError("on VASTClient._buildVASTResponse, Received an Ad type that is not supported", 200);
                }

                if (response.duration === undefined) {
                    throw new VASTError("on VASTClient._buildVASTResponse, Missing duration field in VAST response", 101);
                }

                if (progressEvents) {
                    progressEvents.forEach(function(progressEvent) {
                        if (!utilities.isNumber(progressEvent.offset)) {
                            throw new VASTError("on VASTClient._buildVASTResponse, missing or wrong offset attribute on progress tracking event", 101);
                        }
                    });
                }
            }
        };

        VASTClient.prototype._trackError = function(error, adChain) {
            if (!utilities.isArray(adChain) || adChain.length === 0) { //There is nothing to track
                return;
            }

            var errorURLMacros = [];
            adChain.forEach(addErrorUrlMacros);
            vastUtil.track(errorURLMacros, {
                ERRORCODE: error.code || 900
            }); //900 <== Undefined error

            /*** Local functions  ***/
            function addErrorUrlMacros(ad) {
                if (ad.wrapper && ad.wrapper.error) {
                    errorURLMacros.push(ad.wrapper.error);
                }

                if (ad.inLine && ad.inLine.error) {
                    errorURLMacros.push(ad.inLine.error);
                }
            }
        };

        module.exports = VASTClient;

    }, {
        "../../utils/async": 40,
        "../../utils/consoleLogger": 41,
        "../../utils/http": 43,
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./Ad": 15,
        "./VASTError": 23,
        "./VASTResponse": 25,
        "./vastUtil": 30
    }],
    23: [function(require, module, exports) {
        'use strict';

        function VASTError(message, code) {
            this.message = 'VAST Error: ' + (message || '');
            if (code) {
                this.code = code;
            }
        }

        VASTError.prototype = new Error();
        VASTError.prototype.name = "VAST Error";

        module.exports = VASTError;
    }, {}],
    24: [function(require, module, exports) {
        'use strict';

        /**
         * Inner helper class that deals with the logic of the individual steps needed to setup an ad in the player.
         *
         * @param player {object} instance of the player that will play the ad. It assumes that the videojs-contrib-ads plugin
         *                        has been initialized when you use its utility functions.
         *
         * @constructor
         */

        var VASTResponse = require('./VASTResponse');
        var VASTError = require('./VASTError');
        var VASTTracker = require('./VASTTracker');
        var vastUtil = require('./vastUtil');

        var async = require('../../utils/async');
        var dom = require('../../utils/dom');
        var playerUtils = require('../../utils/playerUtils');
        var utilities = require('../../utils/utilityFunctions');

        var logger = require('../../utils/consoleLogger');

        function VASTIntegrator(player) {
            if (!(this instanceof VASTIntegrator)) {
                return new VASTIntegrator(player);
            }

            this.player = player;
        }

        VASTIntegrator.prototype.playAd = function playAd(vastResponse, callback) {
            var that = this;
            callback = callback || utilities.noop;

            if (!(vastResponse instanceof VASTResponse)) {
                return callback(new VASTError('On VASTIntegrator, missing required VASTResponse'));
            }

            async.waterfall([
                function(next) {
                    next(null, vastResponse);
                },
                this._selectAdSource.bind(this),
                this._createVASTTracker.bind(this),
                this._addClickThrough.bind(this),
                this._addSkipButton.bind(this),
                this._setupEvents.bind(this),
                this._playSelectedAd.bind(this)
            ], function(error, response) {
                if (error && response) {
                    that._trackError(error, response);
                }
                callback(error, response);
            });

            this._adUnit = {
                _src: null,
                type: 'VAST',
                pauseAd: function() {
                    that.player.pause(true);
                },

                resumeAd: function() {
                    that.player.play(true);
                },

                isPaused: function() {
                    return that.player.paused(true);
                },

                getSrc: function() {
                    return this._src;
                }
            };

            return this._adUnit;
        };

        VASTIntegrator.prototype._selectAdSource = function selectAdSource(response, callback) {
            var source;

            var playerWidth = dom.getDimension(this.player.el()).width;
            response.mediaFiles.sort(function compareTo(a, b) {
                var deltaA = Math.abs(playerWidth - a.width);
                var deltaB = Math.abs(playerWidth - b.width);
                return deltaA - deltaB;
            });

            source = this.player.selectSource(response.mediaFiles).source;

            if (source) {
                logger.info("selected source: ", source);
                if (this._adUnit) {
                    this._adUnit._src = source;
                }
                return callback(null, source, response);
            }

            // code 403 <== Couldn't find MediaFile that is supported by this video player
            callback(new VASTError("Could not find Ad mediafile supported by this player", 403), response);
        };

        VASTIntegrator.prototype._createVASTTracker = function createVASTTracker(adMediaFile, response, callback) {
            try {
                callback(null, adMediaFile, new VASTTracker(adMediaFile.src, response), response);
            } catch (e) {
                callback(e, response);
            }
        };

        VASTIntegrator.prototype._setupEvents = function setupEvents(adMediaFile, tracker, response, callback) {
            var previouslyMuted;
            var player = this.player;
            player.on('fullscreenchange', trackFullscreenChange);
            player.on('vast.adStart', trackImpressions);
            player.on('pause', trackPause);
            player.on('timeupdate', trackProgress);
            player.on('volumechange', trackVolumeChange);

            playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel'], unbindEvents);
            playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel', 'vast.adSkip'], function(evt) {
                if (evt.type === 'vast.adEnd') {
                    tracker.trackComplete();
                }
            });

            return callback(null, adMediaFile, response);

            /*** Local Functions ***/
            function unbindEvents() {
                player.off('fullscreenchange', trackFullscreenChange);
                player.off('vast.adStart', trackImpressions);
                player.off('pause', trackPause);
                player.off('timeupdate', trackProgress);
                player.off('volumechange', trackVolumeChange);
            }

            function trackFullscreenChange() {
                if (player.isFullscreen()) {
                    tracker.trackFullscreen();
                } else {
                    tracker.trackExitFullscreen();
                }
            }

            function trackPause() {
                //NOTE: whenever a video ends the video Element triggers a 'pause' event before the 'ended' event.
                //      We should not track this pause event because it makes the VAST tracking confusing again we use a
                //      Threshold of 2 seconds to prevent false positives on IOS.
                if (Math.abs(player.duration() - player.currentTime()) < 2) {
                    return;
                }

                tracker.trackPause();
                playerUtils.once(player, ['play', 'vast.adEnd', 'vast.adsCancel'], function(evt) {
                    if (evt.type === 'play') {
                        tracker.trackResume();
                    }
                });
            }

            function trackProgress() {
                var currentTimeInMs = player.currentTime() * 1000;
                tracker.trackProgress(currentTimeInMs);
            }

            function trackImpressions() {
                tracker.trackImpressions();
                tracker.trackCreativeView();
            }

            function trackVolumeChange() {
                var muted = player.muted();
                if (muted) {
                    tracker.trackMute();
                } else if (previouslyMuted) {
                    tracker.trackUnmute();
                }
                previouslyMuted = muted;
            }
        };

        VASTIntegrator.prototype._addSkipButton = function addSkipButton(source, tracker, response, callback) {
            var skipOffsetInSec;
            var that = this;

            if (utilities.isNumber(response.skipoffset)) {
                skipOffsetInSec = response.skipoffset / 1000;
                addSkipButtonToPlayer(this.player, skipOffsetInSec);
            }
            callback(null, source, tracker, response);

            /*** Local function ***/
            function addSkipButtonToPlayer(player, skipOffset) {
                var skipButton = createSkipButton(player);
                var updateSkipButton = updateSkipButtonState.bind(that, skipButton, skipOffset, player);

                player.el().appendChild(skipButton);
                player.on('timeupdate', updateSkipButton);

                playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel'], removeSkipButton);

                function removeSkipButton() {
                    player.off('timeupdate', updateSkipButton);
                    dom.remove(skipButton);
                }
            }

            function createSkipButton(player) {
                var skipButton = window.document.createElement("div");
                dom.addClass(skipButton, "vast-skip-button");

                skipButton.onclick = function(e) {
                    if (dom.hasClass(skipButton, 'enabled')) {
                        tracker.trackSkip();
                        player.trigger('vast.adSkip');
                    }

                    //We prevent event propagation to avoid problems with the clickThrough and so on
                    if (window.Event.prototype.stopPropagation !== undefined) {
                        e.stopPropagation();
                    } else {
                        return false;
                    }
                };

                return skipButton;
            }

            function updateSkipButtonState(skipButton, skipOffset, player) {
                var timeLeft = Math.ceil(skipOffset - player.currentTime());
                if (timeLeft > 0) {
                    skipButton.innerHTML = "Skip in " + utilities.toFixedDigits(timeLeft, 2) + "...";
                } else {
                    if (!dom.hasClass(skipButton, 'enabled')) {
                        dom.addClass(skipButton, 'enabled');
                        skipButton.innerHTML = "Skip ad";
                    }
                }
            }
        };

        VASTIntegrator.prototype._addClickThrough = function addClickThrough(mediaFile, tracker, response, callback) {
            var player = this.player;
            var blocker = createClickThroughBlocker(player, tracker, response);
            var updateBlocker = updateBlockerURL.bind(this, blocker, response, player);

            player.el().insertBefore(blocker, player.controlBar.el());
            player.on('timeupdate', updateBlocker);
            playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel'], removeBlocker);

            return callback(null, mediaFile, tracker, response);

            /*** Local Functions ***/

            function createClickThroughBlocker(player, tracker, response) {
                var blocker = window.document.createElement("a");
                var clickThroughMacro = response.clickThrough;

                dom.addClass(blocker, 'vast-blocker');
                blocker.href = generateClickThroughURL(clickThroughMacro, player);

                if (utilities.isString(clickThroughMacro)) {
                    blocker.target = "_blank";
                }

                blocker.onclick = function(e) {
                    if (player.paused()) {
                        player.play();

                        //We prevent event propagation to avoid problems with the player's normal pause mechanism
                        if (window.Event.prototype.stopPropagation !== undefined) {
                            e.stopPropagation();
                        }
                        return false;
                    }

                    player.pause();
                    tracker.trackClick();
                };

                return blocker;
            }

            function updateBlockerURL(blocker, response, player) {
                blocker.href = generateClickThroughURL(response.clickThrough, player);
            }

            function generateClickThroughURL(clickThroughMacro, player) {
                var variables = {
                    ASSETURI: mediaFile.src,
                    CONTENTPLAYHEAD: vastUtil.formatProgress(player.currentTime() * 1000)
                };

                return clickThroughMacro ? vastUtil.parseURLMacro(clickThroughMacro, variables) : '#';
            }

            function removeBlocker() {
                player.off('timeupdate', updateBlocker);
                dom.remove(blocker);
            }
        };

        VASTIntegrator.prototype._playSelectedAd = function playSelectedAd(source, response, callback) {
            var player = this.player;

            player.preload("auto"); //without preload=auto the durationchange event is never fired
            player.src(source);

            logger.debug("<VASTIntegrator._playSelectedAd> waiting for durationchange to play the ad...");

            playerUtils.once(player, ['durationchange', 'error', 'vast.adsCancel'], function(evt) {
                if (evt.type === 'durationchange') {
                    logger.debug("<VASTIntegrator._playSelectedAd> got durationchange; calling playAd()");
                    playAd();
                } else if (evt.type === 'error') {
                    callback(new VASTError("on VASTIntegrator, Player is unable to play the Ad", 400), response);
                }
                //NOTE: If the ads get canceled we do nothing/
            });

            /**** local functions ******/
            function playAd() {

                playerUtils.once(player, ['playing', 'vast.adsCancel'], function(evt) {
                    if (evt.type === 'vast.adsCancel') {
                        return;
                    }

                    logger.debug("<VASTIntegrator._playSelectedAd/playAd> got playing event; triggering vast.adStart...");

                    player.trigger('vast.adStart');

                    player.on('ended', proceed);
                    player.on('vast.adsCancel', proceed);
                    player.on('vast.adSkip', proceed);

                    function proceed(evt) {

                        if (evt.type === 'ended' && (player.duration() - player.currentTime()) > 3) {
                            // Ignore ended event if the Ad time was not 'near' the end
                            // avoids issues where IOS controls could skip the Ad
                            return;
                        }

                        player.off('ended', proceed);
                        player.off('vast.adsCancel', proceed);
                        player.off('vast.adSkip', proceed);

                        //NOTE: if the ads get cancel we do nothing apart removing the listners
                        if (evt.type === 'ended' || evt.type === 'vast.adSkip') {
                            callback(null, response);
                        }
                    }
                });

                logger.debug("<VASTIntegrator._playSelectedAd/playAd> calling player.play()...");

                player.play();
            }
        };

        VASTIntegrator.prototype._trackError = function trackError(error, response) {
            vastUtil.track(response.errorURLMacros, {
                ERRORCODE: error.code || 900
            });
        };

        module.exports = VASTIntegrator;
    }, {
        "../../utils/async": 40,
        "../../utils/consoleLogger": 41,
        "../../utils/dom": 42,
        "../../utils/playerUtils": 45,
        "../../utils/utilityFunctions": 47,
        "./VASTError": 23,
        "./VASTResponse": 25,
        "./VASTTracker": 26,
        "./vastUtil": 30
    }],
    25: [function(require, module, exports) {
        'use strict';

        var Ad = require('./Ad');
        var VideoClicks = require('./VideoClicks');
        var Linear = require('./Linear');
        var InLine = require('./InLine');
        var Wrapper = require('./Wrapper');

        var utilities = require('../../utils/utilityFunctions');
        var xml = require('../../utils/xml');

        window.InLine__A = InLine;

        function VASTResponse() {
            if (!(this instanceof VASTResponse)) {
                return new VASTResponse();
            }

            this._linearAdded = false;
            this.ads = [];
            this.errorURLMacros = [];
            this.impressions = [];
            this.clickTrackings = [];
            this.customClicks = [];
            this.trackingEvents = {};
            this.mediaFiles = [];
            this.clickThrough = undefined;
            this.adTitle = '';
            this.duration = undefined;
            this.skipoffset = undefined;
        }

        VASTResponse.prototype.addAd = function(ad) {
            var inLine, wrapper;
            if (ad instanceof Ad) {
                inLine = ad.inLine;
                wrapper = ad.wrapper;

                this.ads.push(ad);

                if (inLine) {
                    this._addInLine(inLine);
                }

                if (wrapper) {
                    this._addWrapper(wrapper);
                }
            }
        };

        VASTResponse.prototype._addErrorTrackUrl = function(error) {
            var errorURL = error instanceof xml.JXONTree ? xml.keyValue(error) : error;
            if (errorURL) {
                this.errorURLMacros.push(errorURL);
            }
        };

        VASTResponse.prototype._addImpressions = function(impressions) {
            utilities.isArray(impressions) && appendToArray(this.impressions, impressions);
        };

        VASTResponse.prototype._addClickThrough = function(clickThrough) {
            if (utilities.isNotEmptyString(clickThrough)) {
                this.clickThrough = clickThrough;
            }
        };

        VASTResponse.prototype._addClickTrackings = function(clickTrackings) {
            utilities.isArray(clickTrackings) && appendToArray(this.clickTrackings, clickTrackings);
        };

        VASTResponse.prototype._addCustomClicks = function(customClicks) {
            utilities.isArray(customClicks) && appendToArray(this.customClicks, customClicks);
        };

        VASTResponse.prototype._addTrackingEvents = function(trackingEvents) {
            var eventsMap = this.trackingEvents;

            if (trackingEvents) {
                trackingEvents = utilities.isArray(trackingEvents) ? trackingEvents : [trackingEvents];
                trackingEvents.forEach(function(trackingEvent) {
                    if (!eventsMap[trackingEvent.name]) {
                        eventsMap[trackingEvent.name] = [];
                    }
                    eventsMap[trackingEvent.name].push(trackingEvent);
                });
            }
        };

        VASTResponse.prototype._addTitle = function(title) {
            if (utilities.isNotEmptyString(title)) {
                this.adTitle = title;
            }
        };

        VASTResponse.prototype._addDuration = function(duration) {
            if (utilities.isNumber(duration)) {
                this.duration = duration;
            }
        };

        VASTResponse.prototype._addVideoClicks = function(videoClicks) {
            if (videoClicks instanceof VideoClicks) {
                this._addClickThrough(videoClicks.clickThrough);
                this._addClickTrackings(videoClicks.clickTrackings);
                this._addCustomClicks(videoClicks.customClicks);
            }
        };

        VASTResponse.prototype._addMediaFiles = function(mediaFiles) {
            utilities.isArray(mediaFiles) && appendToArray(this.mediaFiles, mediaFiles);
        };

        VASTResponse.prototype._addSkipoffset = function(offset) {
            if (offset) {
                this.skipoffset = offset;
            }
        };

        VASTResponse.prototype._addAdParameters = function(adParameters) {
            if (adParameters) {
                this.adParameters = adParameters;
            }
        };

        VASTResponse.prototype._addLinear = function(linear) {
            if (linear instanceof Linear) {
                this._addDuration(linear.duration);
                this._addTrackingEvents(linear.trackingEvents);
                this._addVideoClicks(linear.videoClicks);
                this._addMediaFiles(linear.mediaFiles);
                this._addSkipoffset(linear.skipoffset);
                this._addAdParameters(linear.adParameters);
                this._linearAdded = true;
            }
        };

        VASTResponse.prototype._addInLine = function(inLine) {
            var that = this;

            if (inLine instanceof InLine) {
                this._addTitle(inLine.adTitle);
                this._addErrorTrackUrl(inLine.error);
                this._addImpressions(inLine.impressions);

                inLine.creatives.forEach(function(creative) {
                    if (creative.linear) {
                        that._addLinear(creative.linear);
                    }
                });
            }
        };

        VASTResponse.prototype._addWrapper = function(wrapper) {
            var that = this;

            if (wrapper instanceof Wrapper) {
                this._addErrorTrackUrl(wrapper.error);
                this._addImpressions(wrapper.impressions);

                wrapper.creatives.forEach(function(creative) {
                    var linear = creative.linear;
                    if (linear) {
                        that._addVideoClicks(linear.videoClicks);
                        that.clickThrough = undefined; //We ensure that no clickThrough has been added
                        that._addTrackingEvents(linear.trackingEvents);
                    }
                });
            }
        };

        VASTResponse.prototype.hasLinear = function() {
            return this._linearAdded;
        };

        function appendToArray(array, items) {
            items.forEach(function(item) {
                array.push(item);
            });
        }

        module.exports = VASTResponse;


    }, {
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./Ad": 15,
        "./InLine": 18,
        "./Linear": 19,
        "./VideoClicks": 27,
        "./Wrapper": 28
    }],
    26: [function(require, module, exports) {
        'use strict';

        var VASTError = require('./VASTError');
        var VASTResponse = require('./VASTResponse');
        var vastUtil = require('./vastUtil');
        var utilities = require('../../utils/utilityFunctions');

        function VASTTracker(assetURI, vastResponse) {
            if (!(this instanceof VASTTracker)) {
                return new VASTTracker(assetURI, vastResponse);
            }

            this.sanityCheck(assetURI, vastResponse);
            this.initialize(assetURI, vastResponse);

        }

        VASTTracker.prototype.initialize = function(assetURI, vastResponse) {
            this.response = vastResponse;
            this.assetURI = assetURI;
            this.progress = 0;
            this.quartiles = {
                firstQuartile: {
                    tracked: false,
                    time: Math.round(25 * vastResponse.duration) / 100
                },
                midpoint: {
                    tracked: false,
                    time: Math.round(50 * vastResponse.duration) / 100
                },
                thirdQuartile: {
                    tracked: false,
                    time: Math.round(75 * vastResponse.duration) / 100
                }
            };
        };

        VASTTracker.prototype.sanityCheck = function(assetURI, vastResponse) {
            if (!utilities.isString(assetURI) || utilities.isEmptyString(assetURI)) {
                throw new VASTError('on VASTTracker constructor, missing required the URI of the ad asset being played');
            }

            if (!(vastResponse instanceof VASTResponse)) {
                throw new VASTError('on VASTTracker constructor, missing required VAST response');
            }
        };

        VASTTracker.prototype.trackURLs = function trackURLs(urls, variables) {
            if (utilities.isArray(urls) && urls.length > 0) {
                variables = utilities.extend({
                    ASSETURI: this.assetURI,
                    CONTENTPLAYHEAD: vastUtil.formatProgress(this.progress)
                }, variables || {});

                vastUtil.track(urls, variables);
            }
        };

        VASTTracker.prototype.trackEvent = function trackEvent(eventName, trackOnce) {
            this.trackURLs(getEventUris(this.response.trackingEvents[eventName]));
            if (trackOnce) {
                this.response.trackingEvents[eventName] = undefined;
            }

            /*** Local function ***/
            function getEventUris(trackingEvents) {
                var uris;

                if (trackingEvents) {
                    uris = [];
                    trackingEvents.forEach(function(event) {
                        if (!event.uri) {
                            return;
                        }

                        uris.push(event.uri);
                    });
                }
                return uris;
            }
        };

        VASTTracker.prototype.trackProgress = function trackProgress(newProgressInMs) {
            var that = this;
            var events = [];
            var ONCE = true;
            var ALWAYS = false;
            var trackingEvents = this.response.trackingEvents;

            if (utilities.isNumber(newProgressInMs)) {
                addTrackEvent('start', ONCE, newProgressInMs > 0);
                addTrackEvent('rewind', ALWAYS, hasRewound(this.progress, newProgressInMs));
                addQuartileEvents(newProgressInMs);
                trackProgressEvents(newProgressInMs);
                trackEvents();
                this.progress = newProgressInMs;
            }

            /*** Local function ***/
            function hasRewound(currentProgress, newProgress) {
                var REWIND_THRESHOLD = 3000; //IOS video clock is very unreliable and we need a 3 seconds threshold to ensure that there was a rewind an that it was on purpose.
                return currentProgress > newProgressInMs && Math.abs(newProgress - currentProgress) > REWIND_THRESHOLD;
            }

            function addTrackEvent(eventName, trackOnce, canBeAdded) {
                if (trackingEvents[eventName] && canBeAdded) {
                    events.push({
                        name: eventName,
                        trackOnce: !!trackOnce
                    });
                }
            }

            function addQuartileEvents(progress) {
                var quartiles = that.quartiles;
                var firstQuartile = that.quartiles.firstQuartile;
                var midpoint = that.quartiles.midpoint;
                var thirdQuartile = that.quartiles.thirdQuartile;

                if (!firstQuartile.tracked) {
                    trackQuartile('firstQuartile', progress);
                } else if (!midpoint.tracked) {
                    trackQuartile('midpoint', progress);
                } else if (!thirdQuartile.tracked) {
                    trackQuartile('thirdQuartile', progress);
                }

                /*** Local function ***/
                function trackQuartile(quartileName, progress) {
                    var quartile = quartiles[quartileName];
                    if (canBeTracked(quartile, progress)) {
                        quartile.tracked = true;
                        addTrackEvent(quartileName, ONCE, true);
                    }
                }
            }

            function canBeTracked(quartile, progress) {
                var quartileTime = quartile.time;
                //We only fire the quartile event if the progress is bigger than the quartile time by 5 seconds at most.
                return progress >= quartileTime && progress <= (quartileTime + 5000);
            }

            function trackProgressEvents(progress) {
                if (!utilities.isArray(trackingEvents.progress)) {
                    return; //Nothing to track
                }

                var pendingProgressEvts = [];

                trackingEvents.progress.forEach(function(evt) {
                    if (evt.offset <= progress) {
                        that.trackURLs([evt.uri]);
                    } else {
                        pendingProgressEvts.push(evt);
                    }
                });
                trackingEvents.progress = pendingProgressEvts;
            }

            function trackEvents() {
                events.forEach(function(event) {
                    that.trackEvent(event.name, event.trackOnce);
                });
            }
        };

        [
            'rewind',
            'fullscreen',
            'exitFullscreen',
            'pause',
            'resume',
            'mute',
            'unmute',
            'acceptInvitation',
            'acceptInvitationLinear',
            'collapse',
            'expand'
        ].forEach(function(eventName) {
            VASTTracker.prototype['track' + utilities.capitalize(eventName)] = function() {
                this.trackEvent(eventName);
            };
        });

        [
            'start',
            'skip',
            'close',
            'closeLinear'
        ].forEach(function(eventName) {
            VASTTracker.prototype['track' + utilities.capitalize(eventName)] = function() {
                this.trackEvent(eventName, true);
            };
        });

        [
            'firstQuartile',
            'midpoint',
            'thirdQuartile'
        ].forEach(function(quartile) {
            VASTTracker.prototype['track' + utilities.capitalize(quartile)] = function() {
                this.quartiles[quartile].tracked = true;
                this.trackEvent(quartile, true);
            };
        });

        VASTTracker.prototype.trackComplete = function() {
            if (this.quartiles.thirdQuartile.tracked) {
                this.trackEvent('complete', true);
            }
        };

        VASTTracker.prototype.trackErrorWithCode = function trackErrorWithCode(errorcode) {
            if (utilities.isNumber(errorcode)) {
                this.trackURLs(this.response.errorURLMacros, {
                    ERRORCODE: errorcode
                });
            }
        };

        VASTTracker.prototype.trackImpressions = function trackImpressions() {
            this.trackURLs(this.response.impressions);
        };

        VASTTracker.prototype.trackCreativeView = function trackCreativeView() {
            this.trackEvent('creativeView');
        };

        VASTTracker.prototype.trackClick = function trackClick() {
            this.trackURLs(this.response.clickTrackings);
        };

        module.exports = VASTTracker;

    }, {
        "../../utils/utilityFunctions": 47,
        "./VASTError": 23,
        "./VASTResponse": 25,
        "./vastUtil": 30
    }],
    27: [function(require, module, exports) {
        'use strict';

        var utilities = require('../../utils/utilityFunctions');
        var xml = require('../../utils/xml');

        function VideoClicks(videoClickJTree) {
            if (!(this instanceof VideoClicks)) {
                return new VideoClicks(videoClickJTree);
            }

            this.clickThrough = xml.keyValue(videoClickJTree.clickThrough);
            this.clickTrackings = parseClickTrackings(videoClickJTree.clickTracking);
            this.customClicks = parseClickTrackings(videoClickJTree.customClick);

            /*** Local functions ***/
            function parseClickTrackings(trackingData) {
                var clickTrackings = [];
                if (trackingData) {
                    trackingData = utilities.isArray(trackingData) ? trackingData : [trackingData];
                    trackingData.forEach(function(clickTrackingData) {
                        clickTrackings.push(xml.keyValue(clickTrackingData));
                    });
                }
                return clickTrackings;
            }
        }

        module.exports = VideoClicks;
    }, {
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48
    }],
    28: [function(require, module, exports) {
        'use strict';

        var vastUtil = require('./vastUtil');
        var Creative = require('./Creative');

        var utilities = require('../../utils/utilityFunctions');
        var xml = require('../../utils/xml');

        function Wrapper(wrapperJTree) {
            if (!(this instanceof Wrapper)) {
                return new Wrapper(wrapperJTree);
            }

            //Required elements
            this.adSystem = xml.keyValue(wrapperJTree.adSystem);
            this.impressions = vastUtil.parseImpressions(wrapperJTree.impression);
            this.VASTAdTagURI = xml.keyValue(wrapperJTree.vASTAdTagURI);

            //Optional elements
            this.creatives = Creative.parseCreatives(wrapperJTree.creatives);
            this.error = xml.keyValue(wrapperJTree.error);
            this.extensions = wrapperJTree.extensions;

            //Optional attrs
            this.followAdditionalWrappers = utilities.isDefined(xml.attr(wrapperJTree, 'followAdditionalWrappers')) ? xml.attr(wrapperJTree, 'followAdditionalWrappers') : true;
            this.allowMultipleAds = xml.attr(wrapperJTree, 'allowMultipleAds');
            this.fallbackOnNoAd = xml.attr(wrapperJTree, 'fallbackOnNoAd');
        }

        module.exports = Wrapper;

    }, {
        "../../utils/utilityFunctions": 47,
        "../../utils/xml": 48,
        "./Creative": 17,
        "./vastUtil": 30
    }],
    29: [function(require, module, exports) {
        'use strict';

        var utilities = require('../../utils/utilityFunctions');

        var durationRegex = /(\d\d):(\d\d):(\d\d)(\.(\d\d\d))?/;

        var parsers = {

            duration: function parseDuration(durationStr) {

                var match, durationInMs;

                if (utilities.isString(durationStr)) {
                    match = durationStr.match(durationRegex);
                    if (match) {
                        durationInMs = parseHoursToMs(match[1]) + parseMinToMs(match[2]) + parseSecToMs(match[3]) + parseInt(match[5] || 0);
                    }
                }

                return isNaN(durationInMs) ? null : durationInMs;

                /*** local functions ***/
                function parseHoursToMs(hourStr) {
                    return parseInt(hourStr, 10) * 60 * 60 * 1000;
                }

                function parseMinToMs(minStr) {
                    return parseInt(minStr, 10) * 60 * 1000;
                }

                function parseSecToMs(secStr) {
                    return parseInt(secStr, 10) * 1000;
                }
            },

            offset: function parseOffset(offset, duration) {
                if (isPercentage(offset)) {
                    return calculatePercentage(offset, duration);
                }
                return parsers.duration(offset);

                /*** Local function ***/
                function isPercentage(offset) {
                    var percentageRegex = /^\d+(\.\d+)?%$/g;
                    return percentageRegex.test(offset);
                }

                function calculatePercentage(percentStr, duration) {
                    if (duration) {
                        return calcPercent(duration, parseFloat(percentStr.replace('%', '')));
                    }
                    return null;
                }

                function calcPercent(quantity, percent) {
                    return quantity * percent / 100;
                }
            }

        };


        module.exports = parsers;
    }, {
        "../../utils/utilityFunctions": 47
    }],
    30: [function(require, module, exports) {
        'use strict';

        var utilities = require('../../utils/utilityFunctions');
        var VPAIDHTML5Tech = require('../vpaid/VPAIDHTML5Tech');
        var VPAIDFlashTech = require('../vpaid/VPAIDFlashTech');
        var VPAIDFLASHClient = require('VPAIDFLASHClient/js/VPAIDFLASHClient');

        var vastUtil = {

            track: function track(URLMacros, variables) {
                var sources = vastUtil.parseURLMacros(URLMacros, variables);
                var trackImgs = [];
                sources.forEach(function(src) {
                    var img = new Image();
                    img.src = src;
                    trackImgs.push(img);
                });
                return trackImgs;
            },

            parseURLMacros: function parseMacros(URLMacros, variables) {
                var parsedURLs = [];

                variables = variables || {};

                if (!(variables["CACHEBUSTING"])) {
                    variables["CACHEBUSTING"] = Math.round(Math.random() * 1.0e+10);
                }

                URLMacros.forEach(function(URLMacro) {
                    parsedURLs.push(vastUtil._parseURLMacro(URLMacro, variables));
                });

                return parsedURLs;
            },

            parseURLMacro: function parseMacro(URLMacro, variables) {
                variables = variables || {};

                if (!(variables["CACHEBUSTING"])) {
                    variables["CACHEBUSTING"] = Math.round(Math.random() * 1.0e+10);
                }

                return vastUtil._parseURLMacro(URLMacro, variables);
            },

            _parseURLMacro: function parseMacro(URLMacro, variables) {
                variables = variables || {};

                utilities.forEach(variables, function(value, key) {
                    URLMacro = URLMacro.replace(new RegExp("\\[" + key + "\\\]", 'gm'), value);
                });

                return URLMacro;
            },

            parseDuration: function parseDuration(durationStr) {
                var durationRegex = /(\d\d):(\d\d):(\d\d)(\.(\d\d\d))?/;
                var match, durationInMs;

                if (utilities.isString(durationStr)) {
                    match = durationStr.match(durationRegex);
                    if (match) {
                        durationInMs = parseHoursToMs(match[1]) + parseMinToMs(match[2]) + parseSecToMs(match[3]) + parseInt(match[5] || 0);
                    }
                }

                return isNaN(durationInMs) ? null : durationInMs;

                /*** local functions ***/
                function parseHoursToMs(hourStr) {
                    return parseInt(hourStr, 10) * 60 * 60 * 1000;
                }

                function parseMinToMs(minStr) {
                    return parseInt(minStr, 10) * 60 * 1000;
                }

                function parseSecToMs(secStr) {
                    return parseInt(secStr, 10) * 1000;
                }
            },

            parseImpressions: function parseImpressions(impressions) {
                if (impressions) {
                    impressions = utilities.isArray(impressions) ? impressions : [impressions];
                    return utilities.transformArray(impressions, function(impression) {
                        if (utilities.isNotEmptyString(impression.keyValue)) {
                            return impression.keyValue;
                        }
                        return undefined;
                    });
                }
                return [];
            },


            //We assume that the progress is going to arrive in milliseconds
            formatProgress: function formatProgress(progress) {
                var hours, minutes, seconds, milliseconds;
                hours = progress / (60 * 60 * 1000);
                hours = Math.floor(hours);
                minutes = (progress / (60 * 1000)) % 60;
                minutes = Math.floor(minutes);
                seconds = (progress / 1000) % 60;
                seconds = Math.floor(seconds);
                milliseconds = progress % 1000;
                return utilities.toFixedDigits(hours, 2) + ':' + utilities.toFixedDigits(minutes, 2) + ':' + utilities.toFixedDigits(seconds, 2) + '.' + utilities.toFixedDigits(milliseconds, 3);
            },

            parseOffset: function parseOffset(offset, duration) {
                if (isPercentage(offset)) {
                    return calculatePercentage(offset, duration);
                }
                return vastUtil.parseDuration(offset);

                /*** Local function ***/
                function isPercentage(offset) {
                    var percentageRegex = /^\d+(\.\d+)?%$/g;
                    return percentageRegex.test(offset);
                }

                function calculatePercentage(percentStr, duration) {
                    if (duration) {
                        return calcPercent(duration, parseFloat(percentStr.replace('%', '')));
                    }
                    return null;
                }

                function calcPercent(quantity, percent) {
                    return quantity * percent / 100;
                }
            },


            //List of supported VPAID technologies
            VPAID_techs: [
                VPAIDFlashTech,
                VPAIDHTML5Tech
            ],

            isVPAID: function isVPAIDMediaFile(mediaFile) {
                return !!mediaFile && mediaFile.apiFramework === 'VPAID';
            },

            findSupportedVPAIDTech: function findSupportedVPAIDTech(mimeType) {
                var i, len, VPAIDTech;

                for (i = 0, len = this.VPAID_techs.length; i < len; i += 1) {
                    VPAIDTech = this.VPAID_techs[i];
                    if (VPAIDTech.supports(mimeType)) {
                        return VPAIDTech;
                    }
                }
                return null;
            },

            isFlashSupported: function isFlashSupported() {
                return VPAIDFLASHClient.isSupported();
            },

            /**
             * Necessary step for VPAIDFLAShClient to know if flash is supported and not blocked.
             * IMPORTANT NOTE: This is an async test and needs to be run as soon as possible.
             *
             * @param vpaidFlashLoaderPath the path to the vpaidFlashLoader swf obj.
             */
            runFlashSupportCheck: function runFlashSupportCheck(vpaidFlashLoaderPath) {
                VPAIDFLASHClient.runFlashTest({
                    data: vpaidFlashLoaderPath
                });
            }

        };

        module.exports = vastUtil;

    }, {
        "../../utils/utilityFunctions": 47,
        "../vpaid/VPAIDFlashTech": 32,
        "../vpaid/VPAIDHTML5Tech": 33,
        "VPAIDFLASHClient/js/VPAIDFLASHClient": 3
    }],
    31: [function(require, module, exports) {
        'use strict';

        var VASTError = require('../vast/VASTError');

        var utilities = require('../../utils/utilityFunctions');

        function VPAIDAdUnitWrapper(vpaidAdUnit, opts) {
            if (!(this instanceof VPAIDAdUnitWrapper)) {
                return new VPAIDAdUnitWrapper(vpaidAdUnit, opts);
            }
            sanityCheck(vpaidAdUnit, opts);

            this.options = utilities.extend({}, opts);

            this._adUnit = vpaidAdUnit;

            /*** Local Functions ***/
            function sanityCheck(adUnit, opts) {
                if (!adUnit || !VPAIDAdUnitWrapper.checkVPAIDInterface(adUnit)) {
                    throw new VASTError('on VPAIDAdUnitWrapper, the passed VPAID adUnit does not fully implement the VPAID interface');
                }

                if (!utilities.isObject(opts)) {
                    throw new VASTError("on VPAIDAdUnitWrapper, expected options hash  but got '" + opts + "'");
                }

                if (!("responseTimeout" in opts) || !utilities.isNumber(opts.responseTimeout)) {
                    throw new VASTError("on VPAIDAdUnitWrapper, expected responseTimeout in options");
                }
            }
        }

        VPAIDAdUnitWrapper.checkVPAIDInterface = function checkVPAIDInterface(VPAIDAdUnit) {
            //NOTE: skipAd is not part of the method list because it only appears in VPAID 2.0 and we support VPAID 1.0
            var VPAIDInterfaceMethods = [
                'handshakeVersion', 'initAd', 'startAd', 'stopAd', 'resizeAd', 'pauseAd', 'expandAd', 'collapseAd'
            ];

            for (var i = 0, len = VPAIDInterfaceMethods.length; i < len; i++) {
                if (!VPAIDAdUnit || !utilities.isFunction(VPAIDAdUnit[VPAIDInterfaceMethods[i]])) {
                    return false;
                }
            }


            return canSubscribeToEvents(VPAIDAdUnit) && canUnsubscribeFromEvents(VPAIDAdUnit);

            /*** Local Functions ***/

            function canSubscribeToEvents(adUnit) {
                return utilities.isFunction(adUnit.subscribe) || utilities.isFunction(adUnit.addEventListener) || utilities.isFunction(adUnit.on);
            }

            function canUnsubscribeFromEvents(adUnit) {
                return utilities.isFunction(adUnit.unsubscribe) || utilities.isFunction(adUnit.removeEventListener) || utilities.isFunction(adUnit.off);

            }
        };

        VPAIDAdUnitWrapper.prototype.adUnitAsyncCall = function() {
            var args = utilities.arrayLikeObjToArray(arguments);
            var method = args.shift();
            var cb = args.pop();
            var timeoutId;

            sanityCheck(method, cb, this._adUnit);
            args.push(wrapCallback());

            this._adUnit[method].apply(this._adUnit, args);
            timeoutId = setTimeout(function() {
                timeoutId = null;
                cb(new VASTError("on VPAIDAdUnitWrapper, timeout while waiting for a response on call '" + method + "'"));
                cb = utilities.noop;
            }, this.options.responseTimeout);

            /*** Local functions ***/
            function sanityCheck(method, cb, adUnit) {
                if (!utilities.isString(method) || !utilities.isFunction(adUnit[method])) {
                    throw new VASTError("on VPAIDAdUnitWrapper.adUnitAsyncCall, invalid method name");
                }

                if (!utilities.isFunction(cb)) {
                    throw new VASTError("on VPAIDAdUnitWrapper.adUnitAsyncCall, missing callback");
                }
            }

            function wrapCallback() {
                return function() {
                    if (timeoutId) {
                        clearTimeout(timeoutId);
                    }
                    cb.apply(this, arguments);
                };
            }
        };

        VPAIDAdUnitWrapper.prototype.on = function(evtName, handler) {
            var addEventListener = this._adUnit.addEventListener || this._adUnit.subscribe || this._adUnit.on;
            addEventListener.call(this._adUnit, evtName, handler);
        };

        VPAIDAdUnitWrapper.prototype.off = function(evtName, handler) {
            var removeEventListener = this._adUnit.removeEventListener || this._adUnit.unsubscribe || this._adUnit.off;
            removeEventListener.call(this._adUnit, evtName, handler);
        };

        VPAIDAdUnitWrapper.prototype.waitForEvent = function(evtName, cb, context) {
            var timeoutId;
            sanityCheck(evtName, cb);
            context = context || null;

            this.on(evtName, responseListener);

            timeoutId = setTimeout(function() {
                cb(new VASTError("on VPAIDAdUnitWrapper.waitForEvent, timeout while waiting for event '" + evtName + "'"));
                timeoutId = null;
                cb = utilities.noop;
            }, this.options.responseTimeout);

            /*** Local functions ***/
            function sanityCheck(evtName, cb) {
                if (!utilities.isString(evtName)) {
                    throw new VASTError("on VPAIDAdUnitWrapper.waitForEvent, missing evt name");
                }

                if (!utilities.isFunction(cb)) {
                    throw new VASTError("on VPAIDAdUnitWrapper.waitForEvent, missing callback");
                }
            }

            function responseListener() {
                var args = utilities.arrayLikeObjToArray(arguments);

                if (timeoutId) {
                    clearTimeout(timeoutId);
                    timeoutId = null;
                }

                args.unshift(null);
                cb.apply(context, args);
            }
        };

        // VPAID METHODS
        VPAIDAdUnitWrapper.prototype.handshakeVersion = function(version, cb) {
            this.adUnitAsyncCall('handshakeVersion', version, cb);
        };

        /* jshint maxparams:6 */
        VPAIDAdUnitWrapper.prototype.initAd = function(width, height, viewMode, desiredBitrate, adUnitData, cb) {
            this.waitForEvent('AdLoaded', cb);
            this._adUnit.initAd(width, height, viewMode, desiredBitrate, adUnitData);
        };

        VPAIDAdUnitWrapper.prototype.resizeAd = function(width, height, viewMode, cb) {
            // NOTE: AdSizeChange event is only supported on VPAID 2.0 so for the moment we are not going to use it
            // and will assume that everything is fine after the async call
            this.adUnitAsyncCall('resizeAd', width, height, viewMode, cb);
        };

        VPAIDAdUnitWrapper.prototype.startAd = function(cb) {
            this.waitForEvent('AdStarted', cb);
            this._adUnit.startAd();
        };

        VPAIDAdUnitWrapper.prototype.stopAd = function(cb) {
            this.waitForEvent('AdStopped', cb);
            this._adUnit.stopAd();
        };

        VPAIDAdUnitWrapper.prototype.pauseAd = function(cb) {
            this.waitForEvent('AdPaused', cb);
            this._adUnit.pauseAd();
        };

        VPAIDAdUnitWrapper.prototype.resumeAd = function(cb) {
            this.waitForEvent('AdPlaying', cb);
            this._adUnit.resumeAd();
        };

        VPAIDAdUnitWrapper.prototype.expandAd = function(cb) {
            this.waitForEvent('AdExpandedChange', cb);
            this._adUnit.expandAd();
        };

        VPAIDAdUnitWrapper.prototype.collapseAd = function(cb) {
            this.waitForEvent('AdExpandedChange', cb);
            this._adUnit.collapseAd();
        };

        VPAIDAdUnitWrapper.prototype.skipAd = function(cb) {
            this.waitForEvent('AdSkipped', cb);
            this._adUnit.skipAd();
        };

        //VPAID property getters
        [
            'adLinear',
            'adWidth',
            'adHeight',
            'adExpanded',
            'adSkippableState',
            'adRemainingTime',
            'adDuration',
            'adVolume',
            'adCompanions',
            'adIcons'
        ].forEach(function(property) {
            var getterName = 'get' + utilities.capitalize(property);

            VPAIDAdUnitWrapper.prototype[getterName] = function(cb) {
                this.adUnitAsyncCall(getterName, cb);
            };
        });

        //VPAID property setters
        VPAIDAdUnitWrapper.prototype.setAdVolume = function(volume, cb) {
            this.adUnitAsyncCall('setAdVolume', volume, cb);
        };

        module.exports = VPAIDAdUnitWrapper;

    }, {
        "../../utils/utilityFunctions": 47,
        "../vast/VASTError": 23
    }],
    32: [function(require, module, exports) {
        'use strict';

        var MimeTypes = require('../../utils/mimetypes');

        var VASTError = require('../vast/VASTError');

        var VPAIDFLASHClient = require('VPAIDFLASHClient/js/VPAIDFLASHClient');

        var utilities = require('../../utils/utilityFunctions');
        var dom = require('../../utils/dom');

        var logger = require('../../utils/consoleLogger');

        function VPAIDFlashTech(mediaFile, settings) {
            if (!(this instanceof VPAIDFlashTech)) {
                return new VPAIDFlashTech(mediaFile);
            }
            sanityCheck(mediaFile);
            this.name = 'vpaid-flash';
            this.mediaFile = mediaFile;
            this.containerEl = null;
            this.vpaidFlashClient = null;
            this.settings = settings;

            /*** local functions ***/
            function sanityCheck(mediaFile) {
                if (!mediaFile || !utilities.isString(mediaFile.src)) {
                    throw new VASTError('on VPAIDFlashTech, invalid MediaFile');
                }
            }
        }

        VPAIDFlashTech.VPAIDFLASHClient = VPAIDFLASHClient;

        VPAIDFlashTech.supports = function(type) {
            return (MimeTypes.flash.indexOf(type) > -1) && VPAIDFlashTech.VPAIDFLASHClient.isSupported();
        };

        VPAIDFlashTech.prototype.loadAdUnit = function loadFlashCreative(containerEl, objectEl, callback) {
            var that = this;
            var flashClientOpts = this.settings && this.settings.vpaidFlashLoaderPath ? {
                data: this.settings.vpaidFlashLoaderPath
            } : undefined;
            sanityCheck(containerEl, callback);

            this.containerEl = containerEl;

            logger.debug("<VPAIDFlashTech.loadAdUnit> loading VPAIDFLASHClient with opts:", flashClientOpts);

            this.vpaidFlashClient = new VPAIDFlashTech.VPAIDFLASHClient(containerEl, function(error) {
                if (error) {
                    return callback(error);
                }

                logger.info("<VPAIDFlashTech.loadAdUnit> calling VPAIDFLASHClient.loadAdUnit(); that.mediaFile:", that.mediaFile);
                that.vpaidFlashClient.loadAdUnit(that.mediaFile.src, callback);
            }, flashClientOpts);

            /*** Local Functions ***/
            function sanityCheck(container, cb) {

                if (!dom.isDomElement(container)) {
                    throw new VASTError('on VPAIDFlashTech.loadAdUnit, invalid dom container element');
                }

                if (!utilities.isFunction(cb)) {
                    throw new VASTError('on VPAIDFlashTech.loadAdUnit, missing valid callback');
                }
            }
        };

        VPAIDFlashTech.prototype.unloadAdUnit = function() {
            if (this.vpaidFlashClient) {
                try {
                    this.vpaidFlashClient.destroy();
                } catch (e) {
                    logger.error('VAST ERROR: trying to unload the VPAID adunit');
                }
                this.vpaidFlashClient = null;
            }

            if (this.containerEl) {
                dom.remove(this.containerEl);
                this.containerEl = null;
            }
        };

        module.exports = VPAIDFlashTech;

    }, {
        "../../utils/consoleLogger": 41,
        "../../utils/dom": 42,
        "../../utils/mimetypes": 44,
        "../../utils/utilityFunctions": 47,
        "../vast/VASTError": 23,
        "VPAIDFLASHClient/js/VPAIDFLASHClient": 3
    }],
    33: [function(require, module, exports) {
        'use strict';

        var MimeTypes = require('../../utils/mimetypes');

        var VASTError = require('../vast/VASTError');

        var VPAIDHTML5Client = require('VPAIDHTML5Client/js/VPAIDHTML5Client');

        var utilities = require('../../utils/utilityFunctions');
        var dom = require('../../utils/dom');

        var logger = require('../../utils/consoleLogger');

        function VPAIDHTML5Tech(mediaFile) {

            if (!(this instanceof VPAIDHTML5Tech)) {
                return new VPAIDHTML5Tech(mediaFile);
            }

            sanityCheck(mediaFile);

            this.name = 'vpaid-html5';
            this.containerEl = null;
            this.videoEl = null;
            this.vpaidHTMLClient = null;

            this.mediaFile = mediaFile;

            function sanityCheck(mediaFile) {
                if (!mediaFile || !utilities.isString(mediaFile.src)) {
                    throw new VASTError(VPAIDHTML5Tech.INVALID_MEDIA_FILE);
                }
            }
        }

        VPAIDHTML5Tech.VPAIDHTML5Client = VPAIDHTML5Client;

        VPAIDHTML5Tech.supports = function(type) {
            return !utilities.isOldIE() && MimeTypes.html5.indexOf(type) > -1;
        };

        VPAIDHTML5Tech.prototype.loadAdUnit = function loadAdUnit(containerEl, videoEl, callback) {
            sanityCheck(containerEl, videoEl, callback);

            this.containerEl = containerEl;
            this.videoEl = videoEl;
            this.vpaidHTMLClient = new VPAIDHTML5Tech.VPAIDHTML5Client(containerEl, videoEl, {});
            this.vpaidHTMLClient.loadAdUnit(this.mediaFile.src, callback);

            function sanityCheck(container, video, cb) {
                if (!dom.isDomElement(container)) {
                    throw new VASTError(VPAIDHTML5Tech.INVALID_DOM_CONTAINER_EL);
                }

                if (!dom.isDomElement(video) || video.tagName.toLowerCase() !== 'video') {
                    throw new VASTError(VPAIDHTML5Tech.INVALID_DOM_CONTAINER_EL);
                }

                if (!utilities.isFunction(cb)) {
                    throw new VASTError(VPAIDHTML5Tech.MISSING_CALLBACK);
                }
            }
        };

        VPAIDHTML5Tech.prototype.unloadAdUnit = function unloadAdUnit() {
            if (this.vpaidHTMLClient) {
                try {
                    this.vpaidHTMLClient.destroy();
                } catch (e) {
                    logger.error('VAST ERROR: trying to unload the VPAID adunit');
                }

                this.vpaidHTMLClient = null;
            }

            if (this.containerEl) {
                dom.remove(this.containerEl);
                this.containerEl = null;
            }
        };

        var PREFIX = 'on VPAIDHTML5Tech';
        VPAIDHTML5Tech.INVALID_MEDIA_FILE = PREFIX + ', invalid MediaFile';
        VPAIDHTML5Tech.INVALID_DOM_CONTAINER_EL = PREFIX + ', invalid container HtmlElement';
        VPAIDHTML5Tech.INVALID_DOM_VIDEO_EL = PREFIX + ', invalid HTMLVideoElement';
        VPAIDHTML5Tech.MISSING_CALLBACK = PREFIX + ', missing valid callback';

        module.exports = VPAIDHTML5Tech;

    }, {
        "../../utils/consoleLogger": 41,
        "../../utils/dom": 42,
        "../../utils/mimetypes": 44,
        "../../utils/utilityFunctions": 47,
        "../vast/VASTError": 23,
        "VPAIDHTML5Client/js/VPAIDHTML5Client": 11
    }],
    34: [function(require, module, exports) {
        'use strict';

        var MimeTypes = require('../../utils/mimetypes');
        var VASTError = require('../vast/VASTError');
        var VASTResponse = require('../vast/VASTResponse');
        var VASTTracker = require('../vast/VASTTracker');
        var vastUtil = require('../vast/vastUtil');

        var VPAIDAdUnitWrapper = require('./VPAIDAdUnitWrapper');

        var async = require('../../utils/async');
        var dom = require('../../utils/dom');
        var playerUtils = require('../../utils/playerUtils');
        var utilities = require('../../utils/utilityFunctions');

        var logger = require('../../utils/consoleLogger');

        function VPAIDIntegrator(player, settings) {
            if (!(this instanceof VPAIDIntegrator)) {
                return new VPAIDIntegrator(player);
            }

            this.VIEW_MODE = {
                NORMAL: 'normal',
                FULLSCREEN: "fullscreen",
                THUMBNAIL: "thumbnail"
            };
            this.player = player;
            this.containerEl = createVPAIDContainerEl(player);
            this.options = {
                responseTimeout: 5000,
                VPAID_VERSION: '2.0'
            };
            this.settings = settings;

            /*** Local functions ***/

            function createVPAIDContainerEl() {
                var containerEl = document.createElement('div');
                dom.addClass(containerEl, 'VPAID-container');
                player.el().insertBefore(containerEl, player.controlBar.el());
                return containerEl;

            }
        }

        VPAIDIntegrator.prototype.playAd = function playVPaidAd(vastResponse, callback) {
            if (!(vastResponse instanceof VASTResponse)) {
                return callback(new VASTError('on VASTIntegrator.playAd, missing required VASTResponse'));
            }

            var that = this;
            var player = this.player;
            logger.debug("<VPAIDIntegrator.playAd> looking for supported tech...");
            var tech = this._findSupportedTech(vastResponse, this.settings);

            callback = callback || utilities.noop;

            this._adUnit = null;

            dom.addClass(player.el(), 'vjs-vpaid-ad');

            player.on('vast.adsCancel', triggerVpaidAdEnd);
            player.one('vpaid.adEnd', function() {
                player.off('vast.adsCancel', triggerVpaidAdEnd);
                removeAdUnit();
            });

            if (tech) {
                logger.info("<VPAIDIntegrator.playAd> found tech: ", tech);

                async.waterfall([
                    function(next) {
                        next(null, tech, vastResponse);
                    },
                    this._loadAdUnit.bind(this),
                    this._playAdUnit.bind(this),
                    this._finishPlaying.bind(this)

                ], adComplete);

                this._adUnit = {
                    _paused: true,
                    type: 'VPAID',
                    pauseAd: function() {
                        player.trigger('vpaid.pauseAd');
                        player.pause(true); //we make sure that the video content gets stopped.
                    },
                    resumeAd: function() {
                        player.trigger('vpaid.resumeAd');
                    },
                    isPaused: function() {
                        return this._paused;
                    },
                    getSrc: function() {
                        return tech.mediaFile;
                    }
                };

            } else {
                logger.debug("<VPAIDIntegrator.playAd> could not find suitable tech");
                var error = new VASTError('on VPAIDIntegrator.playAd, could not find a supported mediaFile', 403);
                adComplete(error, this._adUnit, vastResponse);
            }

            return this._adUnit;

            /*** Local functions ***/
            function adComplete(error, adUnit, vastResponse) {
                if (error && vastResponse) {
                    that._trackError(vastResponse, error.code);
                }
                player.trigger('vpaid.adEnd');
                callback(error, vastResponse);
            }

            function triggerVpaidAdEnd() {
                player.trigger('vpaid.adEnd');
            }

            function removeAdUnit() {
                if (tech) {
                    tech.unloadAdUnit();
                }
                dom.removeClass(player.el(), 'vjs-vpaid-ad');
            }
        };

        VPAIDIntegrator.prototype._findSupportedTech = function(vastResponse, settings) {
            if (!(vastResponse instanceof VASTResponse)) {
                return null;
            }

            var vpaidMediaFiles = vastResponse.mediaFiles.filter(vastUtil.isVPAID);
            var preferredTech = settings && settings.preferredTech;
            var skippedSupportTechs = [];
            var i, len, mediaFile, VPAIDTech, isPreferedTech;

            for (i = 0, len = vpaidMediaFiles.length; i < len; i += 1) {
                mediaFile = vpaidMediaFiles[i];
                VPAIDTech = vastUtil.findSupportedVPAIDTech(mediaFile.type);

                // no supported VPAID tech found, skip mediafile
                if (!VPAIDTech) {
                    continue;
                }

                // do we have a prefered tech, does it play this media file ?
                isPreferedTech = preferredTech ?
                    (mediaFile.type === preferredTech || (MimeTypes[preferredTech] && MimeTypes[preferredTech].indexOf(mediaFile.type) > -1)) :
                    false;

                // our prefered tech can read this mediafile, defaulting to it.
                if (isPreferedTech) {
                    return new VPAIDTech(mediaFile, settings);
                }

                skippedSupportTechs.push({
                    mediaFile: mediaFile,
                    tech: VPAIDTech
                });
            }

            if (skippedSupportTechs.length) {
                var firstTech = skippedSupportTechs[0];
                return new firstTech.tech(firstTech.mediaFile, settings);
            }

            return null;
        };

        VPAIDIntegrator.prototype._createVPAIDAdUnitWrapper = function(adUnit, src, responseTimeout) {
            return new VPAIDAdUnitWrapper(adUnit, {
                src: src,
                responseTimeout: responseTimeout
            });
        };

        VPAIDIntegrator.prototype._loadAdUnit = function(tech, vastResponse, next) {
            var that = this;
            var player = this.player;
            var vjsTechEl = player.el().querySelector('.vjs-tech');
            var responseTimeout = this.settings.responseTimeout || this.options.responseTimeout;
            tech.loadAdUnit(this.containerEl, vjsTechEl, function(error, adUnit) {
                if (error) {
                    return next(error, adUnit, vastResponse);
                }

                try {
                    var WrappedAdUnit = that._createVPAIDAdUnitWrapper(adUnit, tech.mediaFile.src, responseTimeout);
                    var techClass = 'vjs-' + tech.name + '-ad';
                    dom.addClass(player.el(), techClass);
                    player.one('vpaid.adEnd', function() {
                        dom.removeClass(player.el(), techClass);
                    });
                    next(null, WrappedAdUnit, vastResponse);
                } catch (e) {
                    next(e, adUnit, vastResponse);
                }
            });
        };

        VPAIDIntegrator.prototype._playAdUnit = function(adUnit, vastResponse, callback) {
            async.waterfall([
                function(next) {
                    next(null, adUnit, vastResponse);
                },
                this._handshake.bind(this),
                this._initAd.bind(this),
                this._setupEvents.bind(this),
                this._addSkipButton.bind(this),
                this._linkPlayerControls.bind(this),
                this._startAd.bind(this)
            ], callback);
        };

        VPAIDIntegrator.prototype._handshake = function handshake(adUnit, vastResponse, next) {
            adUnit.handshakeVersion(this.options.VPAID_VERSION, function(error, version) {
                if (error) {
                    return next(error, adUnit, vastResponse);
                }

                if (version && isSupportedVersion(version)) {
                    return next(null, adUnit, vastResponse);
                }

                return next(new VASTError('on VPAIDIntegrator._handshake, unsupported version "' + version + '"'), adUnit, vastResponse);
            });

            function isSupportedVersion(version) {
                var majorNum = major(version);
                return majorNum >= 1 && majorNum <= 2;
            }

            function major(version) {
                var parts = version.split('.');
                return parseInt(parts[0], 10);
            }
        };

        VPAIDIntegrator.prototype._initAd = function(adUnit, vastResponse, next) {
            var tech = this.player.el().querySelector('.vjs-tech');
            var dimension = dom.getDimension(tech);
            adUnit.initAd(dimension.width, dimension.height, this.VIEW_MODE.NORMAL, -1, {
                AdParameters: vastResponse.adParameters || ''
            }, function(error) {
                next(error, adUnit, vastResponse);
            });
        };

        VPAIDIntegrator.prototype._createVASTTracker = function(adUnitSrc, vastResponse) {
            return new VASTTracker(adUnitSrc, vastResponse);
        };

        VPAIDIntegrator.prototype._setupEvents = function(adUnit, vastResponse, next) {
            var adUnitSrc = adUnit.options.src;
            var tracker = this._createVASTTracker(adUnitSrc, vastResponse);
            var player = this.player;
            var that = this;

            adUnit.on('AdSkipped', function() {
                player.trigger('vpaid.AdSkipped');
                tracker.trackSkip();
            });

            adUnit.on('AdImpression', function() {
                player.trigger('vpaid.AdImpression');
                tracker.trackImpressions();
            });

            adUnit.on('AdStarted', function() {
                player.trigger('vpaid.AdStarted');
                tracker.trackCreativeView();
                notifyPlayToPlayer();
            });

            adUnit.on('AdVideoStart', function() {
                player.trigger('vpaid.AdVideoStart');
                tracker.trackStart();
                notifyPlayToPlayer();
            });

            adUnit.on('AdPlaying', function() {
                player.trigger('vpaid.AdPlaying');
                tracker.trackResume();
                notifyPlayToPlayer();
            });

            adUnit.on('AdPaused', function() {
                player.trigger('vpaid.AdPaused');
                tracker.trackPause();
                notifyPauseToPlayer();
            });

            function notifyPlayToPlayer() {
                if (that._adUnit && that._adUnit.isPaused()) {
                    that._adUnit._paused = false;
                }
                player.trigger('play');

            }

            function notifyPauseToPlayer() {
                if (that._adUnit) {
                    that._adUnit._paused = true;
                }
                player.trigger('pause');
            }

            adUnit.on('AdVideoFirstQuartile', function() {
                player.trigger('vpaid.AdVideoFirstQuartile');
                tracker.trackFirstQuartile();
            });

            adUnit.on('AdVideoMidpoint', function() {
                player.trigger('vpaid.AdVideoMidpoint');
                tracker.trackMidpoint();
            });

            adUnit.on('AdVideoThirdQuartile', function() {
                player.trigger('vpaid.AdVideoThirdQuartile');
                tracker.trackThirdQuartile();
            });

            adUnit.on('AdVideoComplete', function() {
                player.trigger('vpaid.AdVideoComplete');
                tracker.trackComplete();
            });

            adUnit.on('AdClickThru', function(data) {
                player.trigger('vpaid.AdClickThru');
                var url = data.url;
                var playerHandles = data.playerHandles;
                var clickThruUrl = utilities.isNotEmptyString(url) ? url : generateClickThroughURL(vastResponse.clickThrough);

                tracker.trackClick();
                if (playerHandles && clickThruUrl) {
                    window.open(clickThruUrl, '_blank');
                }

                function generateClickThroughURL(clickThroughMacro) {
                    var variables = {
                        ASSETURI: adUnit.options.src,
                        CONTENTPLAYHEAD: 0 //In VPAID there is no method to know the current time from the adUnit
                    };

                    return clickThroughMacro ? vastUtil.parseURLMacro(clickThroughMacro, variables) : null;
                }
            });

            adUnit.on('AdUserAcceptInvitation', function() {
                player.trigger('vpaid.AdUserAcceptInvitation');
                tracker.trackAcceptInvitation();
                tracker.trackAcceptInvitationLinear();
            });

            adUnit.on('AdUserClose', function() {
                player.trigger('vpaid.AdUserClose');
                tracker.trackClose();
                tracker.trackCloseLinear();
            });

            adUnit.on('AdUserMinimize', function() {
                player.trigger('vpaid.AdUserMinimize');
                tracker.trackCollapse();
            });

            adUnit.on('AdError', function() {
                player.trigger('vpaid.AdError');
                //NOTE: we track errors code 901, as noted in VAST 3.0
                tracker.trackErrorWithCode(901);
            });

            adUnit.on('AdVolumeChange', function() {
                player.trigger('vpaid.AdVolumeChange');
                var lastVolume = player.volume();
                adUnit.getAdVolume(function(error, currentVolume) {
                    if (lastVolume !== currentVolume) {
                        if (currentVolume === 0 && lastVolume > 0) {
                            tracker.trackMute();
                        }

                        if (currentVolume > 0 && lastVolume === 0) {
                            tracker.trackUnmute();
                        }

                        player.volume(currentVolume);
                    }
                });
            });

            var updateViewSize = resizeAd.bind(this, player, adUnit, this.VIEW_MODE);
            var updateViewSizeThrottled = utilities.throttle(updateViewSize, 100);
            var autoResize = this.settings.autoResize;

            if (autoResize) {
                dom.addEventListener(window, 'resize', updateViewSizeThrottled);
                dom.addEventListener(window, 'orientationchange', updateViewSizeThrottled);
            }

            player.on('vast.resize', updateViewSize);
            player.on('vpaid.pauseAd', pauseAdUnit);
            player.on('vpaid.resumeAd', resumeAdUnit);

            player.one('vpaid.adEnd', function() {
                player.off('vast.resize', updateViewSize);
                player.off('vpaid.pauseAd', pauseAdUnit);
                player.off('vpaid.resumeAd', resumeAdUnit);

                if (autoResize) {
                    dom.removeEventListener(window, 'resize', updateViewSizeThrottled);
                    dom.removeEventListener(window, 'orientationchange', updateViewSizeThrottled);
                }
            });

            next(null, adUnit, vastResponse);

            /*** Local Functions ***/
            function pauseAdUnit() {
                adUnit.pauseAd(utilities.noop);
            }

            function resumeAdUnit() {
                adUnit.resumeAd(utilities.noop);
            }
        };

        VPAIDIntegrator.prototype._addSkipButton = function(adUnit, vastResponse, next) {
            var skipButton;
            var player = this.player;

            adUnit.on('AdSkippableStateChange', updateSkipButtonState);

            playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel'], removeSkipButton);

            next(null, adUnit, vastResponse);

            /*** Local function ***/
            function updateSkipButtonState() {
                player.trigger('vpaid.AdSkippableStateChange');
                adUnit.getAdSkippableState(function(error, isSkippable) {
                    if (isSkippable) {
                        if (!skipButton) {
                            addSkipButton(player);
                        }
                    } else {
                        removeSkipButton(player);
                    }
                });
            }

            function addSkipButton(player) {
                skipButton = createSkipButton(player);
                player.el().appendChild(skipButton);
            }

            function removeSkipButton() {
                dom.remove(skipButton);
                skipButton = null;
            }

            function createSkipButton() {
                var skipButton = window.document.createElement("div");
                dom.addClass(skipButton, "vast-skip-button");
                dom.addClass(skipButton, "enabled");
                skipButton.innerHTML = "Skip ad";

                skipButton.onclick = function(e) {
                    adUnit.skipAd(utilities.noop); //We skip the adUnit

                    //We prevent event propagation to avoid problems with the clickThrough and so on
                    if (window.Event.prototype.stopPropagation !== undefined) {
                        e.stopPropagation();
                    } else {
                        return false;
                    }
                };

                return skipButton;
            }
        };

        VPAIDIntegrator.prototype._linkPlayerControls = function(adUnit, vastResponse, next) {
            var that = this;
            linkVolumeControl(this.player, adUnit);
            linkFullScreenControl(this.player, adUnit, this.VIEW_MODE);

            next(null, adUnit, vastResponse);

            /*** Local functions ***/
            function linkVolumeControl(player, adUnit) {
                player.on('volumechange', updateAdUnitVolume);
                adUnit.on('AdVolumeChange', updatePlayerVolume);

                player.one('vpaid.adEnd', function() {
                    player.off('volumechange', updateAdUnitVolume);
                });


                /*** local functions ***/
                function updateAdUnitVolume() {
                    var vol = player.muted() ? 0 : player.volume();
                    adUnit.setAdVolume(vol, logError);
                }

                function updatePlayerVolume() {
                    player.trigger('vpaid.AdVolumeChange');
                    var lastVolume = player.volume();
                    adUnit.getAdVolume(function(error, vol) {
                        if (error) {
                            logError(error);
                        } else {
                            if (lastVolume !== vol) {
                                player.volume(vol);
                            }
                        }
                    });
                }
            }

            function linkFullScreenControl(player, adUnit, VIEW_MODE) {
                var updateViewSize = resizeAd.bind(that, player, adUnit, VIEW_MODE);

                player.on('fullscreenchange', updateViewSize);

                player.one('vpaid.adEnd', function() {
                    player.off('fullscreenchange', updateViewSize);
                });
            }
        };

        VPAIDIntegrator.prototype._startAd = function(adUnit, vastResponse, next) {
            var player = this.player;

            adUnit.startAd(function(error) {
                if (!error) {
                    player.trigger('vast.adStart');
                }
                next(error, adUnit, vastResponse);
            });
        };

        VPAIDIntegrator.prototype._finishPlaying = function(adUnit, vastResponse, next) {
            var player = this.player;
            adUnit.on('AdStopped', function() {
                player.trigger('vpaid.AdStopped');
                finishPlayingAd(null);
            });

            adUnit.on('AdError', function(error) {
                var errMsg = error ? error.message : 'on VPAIDIntegrator, error while waiting for the adUnit to finish playing';
                finishPlayingAd(new VASTError(errMsg));
            });

            /*** local functions ***/
            function finishPlayingAd(error) {
                next(error, adUnit, vastResponse);
            }
        };

        VPAIDIntegrator.prototype._trackError = function trackError(response, errorCode) {
            vastUtil.track(response.errorURLMacros, {
                ERRORCODE: errorCode || 901
            });
        };

        function resizeAd(player, adUnit, VIEW_MODE) {
            var tech = player.el().querySelector('.vjs-tech');
            var dimension = dom.getDimension(tech);
            var MODE = player.isFullscreen() ? VIEW_MODE.FULLSCREEN : VIEW_MODE.NORMAL;
            adUnit.resizeAd(dimension.width, dimension.height, MODE, logError);
        }

        function logError(error) {
            if (error) {
                logger.error('ERROR: ' + error.message, error);
            }
        }

        module.exports = VPAIDIntegrator;

    }, {
        "../../utils/async": 40,
        "../../utils/consoleLogger": 41,
        "../../utils/dom": 42,
        "../../utils/mimetypes": 44,
        "../../utils/playerUtils": 45,
        "../../utils/utilityFunctions": 47,
        "../vast/VASTError": 23,
        "../vast/VASTResponse": 25,
        "../vast/VASTTracker": 26,
        "../vast/vastUtil": 30,
        "./VPAIDAdUnitWrapper": 31
    }],
    35: [function(require, module, exports) {
        'use strict';

        var dom = require('../../utils/dom');

        var element = document.createElement('div');
        element.className = 'vjs-ads-label vjs-control vjs-label-hidden';
        element.innerHTML = 'Advertisement';

        var AdsLabelFactory = function(baseComponent) {
            return {
                /** @constructor */
                init: function init(player, options) {
                    options.el = element;
                    baseComponent.call(this, player, options);

                    // We asynchronously reposition the ads label element
                    setTimeout(function() {
                        var currentTimeComp = player.controlBar && (player.controlBar.getChild("timerControls") || player.controlBar.getChild("currentTimeDisplay"));
                        if (currentTimeComp) {
                            player.controlBar.el().insertBefore(element, currentTimeComp.el());
                        }
                        dom.removeClass(element, 'vjs-label-hidden');
                    }, 0);
                },

                el: function getElement() {
                    return element;
                }
            };
        };

        module.exports = AdsLabelFactory;
    }, {
        "../../utils/dom": 42
    }],
    36: [function(require, module, exports) {
        'use strict';

        var baseVideoJsComponent = videojs.getComponent('Component');

        var AdsLabel = require('./ads-label')(baseVideoJsComponent);

        videojs.registerComponent('AdsLabel', videojs.extend(baseVideoJsComponent, AdsLabel));

    }, {
        "./ads-label": 35
    }],
    37: [function(require, module, exports) {
        'use strict';

        /**
         * The component that shows a black screen until the ads plugin has decided if it can or it can not play the ad.
         *
         * Note: In case you wonder why instead of this black poster we don't just show the spinner loader.
         *       IOS devices do not work well with animations and the browser chrashes from time to time That is why we chose to
         *       have a secondary black poster.
         *
         *       It also makes it much more easier for the users of the plugin since it does not change the default behaviour of the
         *       spinner and the player works the same way with and without the plugin.
         *
         * @param {vjs.Player|Object} player
         * @param {Object=} options
         * @constructor
         */
        var element = document.createElement('div');

        var BlackPosterFactory = function(baseComponent) {
            return {
                /** @constructor */
                init: function init(player, options) {
                    options.el = element;
                    element.className = 'vjs-black-poster';
                    baseComponent.call(this, player, options);

                    var posterImg = player.getChild('posterImage');

                    //We need to do it asynchronously to be sure that the black poster el is on the dom.
                    setTimeout(function() {
                        if (posterImg && player && player.el()) {
                            player.el().insertBefore(element, posterImg.el());
                        }
                    }, 0);
                },
                el: function getElement() {
                    return element;
                }
            };
        };

        module.exports = BlackPosterFactory;
    }, {}],
    38: [function(require, module, exports) {
        'use strict';

        var baseVideoJsComponent = videojs.getComponent('Component');

        var BlackPoster = require('./black-poster')(baseVideoJsComponent);

        videojs.registerComponent('BlackPoster', videojs.extend(baseVideoJsComponent, BlackPoster));

    }, {
        "./black-poster": 37
    }],
    39: [function(require, module, exports) {
        'use strict';

        var VASTClient = require('../ads/vast/VASTClient');
        var VASTError = require('../ads/vast/VASTError');
        var vastUtil = require('../ads/vast/vastUtil');

        var VASTIntegrator = require('../ads/vast/VASTIntegrator');
        var VPAIDIntegrator = require('../ads/vpaid/VPAIDIntegrator');

        var async = require('../utils/async');
        var dom = require('../utils/dom');
        var playerUtils = require('../utils/playerUtils');
        var utilities = require('../utils/utilityFunctions');

        var logger = require('../utils/consoleLogger');

        module.exports = function VASTPlugin(options) {
            var snapshot;
            var player = this;
            var vast = new VASTClient();
            var adsCanceled = false;
            var defaultOpts = {
                // maximum amount of time in ms to wait to receive `adsready` from the ad
                // implementation after play has been requested. Ad implementations are
                // expected to load any dynamic libraries and make any requests to determine
                // ad policies for a video during this time.
                timeout: 500,

                //TODO:finish this IOS FIX
                //Whenever you play an add on IOS, the native player kicks in and we loose control of it. On very heavy pages the 'play' event
                // May occur after the video content has already started. This is wrong if you want to play a preroll ad that needs to happen before the user
                // starts watching the content. To prevent this usec
                iosPrerollCancelTimeout: 2000,

                // maximun amount of time for the ad to actually start playing. If this timeout gets
                // triggered the ads will be cancelled
                adCancelTimeout: 3000,

                // Boolean flag that configures the player to play a new ad before the user sees the video again
                // the current video
                playAdAlways: false,

                // Flag to enable or disable the ads by default.
                adsEnabled: true,

                // Boolean flag to enable or disable the resize with window.resize or orientationchange
                autoResize: true,

                // Path to the VPAID flash ad's loader
                vpaidFlashLoaderPath: '/VPAIDFlash.swf',

                // verbosity of console logging:
                // 0 - error
                // 1 - error, warn
                // 2 - error, warn, info
                // 3 - error, warn, info, log
                // 4 - error, warn, info, log, debug
                verbosity: 0
            };

            var settings = utilities.extend({}, defaultOpts, options || {});

            if (utilities.isUndefined(settings.adTagUrl) && utilities.isDefined(settings.url)) {
                settings.adTagUrl = settings.url;
            }

            if (utilities.isString(settings.adTagUrl)) {
                settings.adTagUrl = utilities.echoFn(settings.adTagUrl);
            }

            if (utilities.isDefined(settings.adTagXML) && !utilities.isFunction(settings.adTagXML)) {
                return trackAdError(new VASTError('on VideoJS VAST plugin, the passed adTagXML option does not contain a function'));
            }

            if (!utilities.isDefined(settings.adTagUrl) && !utilities.isFunction(settings.adTagXML)) {
                return trackAdError(new VASTError('on VideoJS VAST plugin, missing adTagUrl on options object'));
            }

            logger.setVerbosity(settings.verbosity);

            vastUtil.runFlashSupportCheck(settings.vpaidFlashLoaderPath); // Necessary step for VPAIDFLASHClient to work.

            playerUtils.prepareForAds(player);

            if (settings.playAdAlways) {
                // No matter what happens we play a new ad before the user sees the video again.
                player.on('vast.contentEnd', function() {
                    setTimeout(function() {
                        player.trigger('vast.reset');
                    }, 0);
                });
            }

            player.on('vast.firstPlay', tryToPlayPrerollAd);

            player.on('vast.reset', function() {
                //If we are reseting the plugin, we don't want to restore the content
                snapshot = null;
                cancelAds();
            });

            player.vast = {
                isEnabled: function() {
                    return settings.adsEnabled;
                },

                enable: function() {
                    settings.adsEnabled = true;
                },

                disable: function() {
                    settings.adsEnabled = false;
                }
            };

            return player.vast;

            /**** Local functions ****/
            function tryToPlayPrerollAd() {
                //We remove the poster to prevent flickering whenever the content starts playing
                playerUtils.removeNativePoster(player);

                playerUtils.once(player, ['vast.adsCancel', 'vast.adEnd'], function() {
                    removeAdUnit();
                    restoreVideoContent();
                });

                async.waterfall([
                    checkAdsEnabled,
                    preparePlayerForAd,
                    startAdCancelTimeout,
                    playPrerollAd
                ], function(error, response) {
                    if (error) {
                        trackAdError(error, response);
                    } else {
                        player.trigger('vast.adEnd');
                    }
                });

                /*** Local functions ***/

                function removeAdUnit() {
                    if (player.vast && player.vast.adUnit) {
                        player.vast.adUnit = null; //We remove the adUnit
                    }
                }

                function restoreVideoContent() {
                    setupContentEvents();
                    if (snapshot) {
                        playerUtils.restorePlayerSnapshot(player, snapshot);
                        snapshot = null;
                    }
                }

                function setupContentEvents() {
                    playerUtils.once(player, ['playing', 'vast.reset', 'vast.firstPlay'], function(evt) {
                        if (evt.type !== 'playing') {
                            return;
                        }

                        player.trigger('vast.contentStart');

                        playerUtils.once(player, ['ended', 'vast.reset', 'vast.firstPlay'], function(evt) {
                            if (evt.type === 'ended') {
                                player.trigger('vast.contentEnd');
                            }
                        });
                    });
                }

                function checkAdsEnabled(next) {
                    if (settings.adsEnabled) {
                        return next(null);
                    }
                    next(new VASTError('Ads are not enabled'));
                }

                function preparePlayerForAd(next) {
                    if (canPlayPrerollAd()) {
                        snapshot = playerUtils.getPlayerSnapshot(player);
                        player.pause();
                        addSpinnerIcon();

                        if (player.paused()) {
                            next(null);
                        } else {
                            playerUtils.once(player, ['playing'], function() {
                                player.pause();
                                next(null);
                            });
                        }
                    } else {
                        next(new VASTError('video content has been playing before preroll ad'));
                    }
                }

                function canPlayPrerollAd() {
                    return !utilities.isIPhone() || player.currentTime() <= settings.iosPrerollCancelTimeout;
                }

                function startAdCancelTimeout(next) {
                    var adCancelTimeoutId;
                    adsCanceled = false;

                    adCancelTimeoutId = setTimeout(function() {
                        trackAdError(new VASTError('timeout while waiting for the video to start playing', 402));
                    }, settings.adCancelTimeout);

                    playerUtils.once(player, ['vast.adStart', 'vast.adsCancel'], clearAdCancelTimeout);

                    /*** local functions ***/
                    function clearAdCancelTimeout() {
                        if (adCancelTimeoutId) {
                            clearTimeout(adCancelTimeoutId);
                            adCancelTimeoutId = null;
                        }
                    }

                    next(null);
                }

                function addSpinnerIcon() {
                    dom.addClass(player.el(), 'vjs-vast-ad-loading');
                    playerUtils.once(player, ['vast.adStart', 'vast.adsCancel'], removeSpinnerIcon);
                }

                function removeSpinnerIcon() {
                    //IMPORTANT NOTE: We remove the spinnerIcon asynchronously to give time to the browser to start the video.
                    // If we remove it synchronously we see a flash of the content video before the ad starts playing.
                    setTimeout(function() {
                        dom.removeClass(player.el(), 'vjs-vast-ad-loading');
                    }, 100);
                }

            }

            function cancelAds() {
                player.trigger('vast.adsCancel');
                adsCanceled = true;
            }

            function playPrerollAd(callback) {
                async.waterfall([
                    getVastResponse,
                    playAd
                ], callback);
            }

            function getVastResponse(callback) {
                vast.getVASTResponse(settings.adTagUrl ? settings.adTagUrl() : settings.adTagXML, callback);
            }

            function playAd(vastResponse, callback) {
                //TODO: Find a better way to stop the play. The 'playPrerollWaterfall' ends in an inconsistent situation
                //If the state is not 'preroll?' it means the ads were canceled therefore, we break the waterfall
                if (adsCanceled) {
                    return;
                }

                var adIntegrator = isVPAID(vastResponse) ? new VPAIDIntegrator(player, settings) : new VASTIntegrator(player);
                var adFinished = false;

                playerUtils.once(player, ['vast.adStart', 'vast.adsCancel'], function(evt) {
                    if (evt.type === 'vast.adStart') {
                        addAdsLabel();
                    }
                });

                playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel'], removeAdsLabel);

                if (utilities.isIDevice()) {
                    preventManualProgress();
                }

                player.vast.vastResponse = vastResponse;
                logger.debug("calling adIntegrator.playAd() with vastResponse:", vastResponse);
                player.vast.adUnit = adIntegrator.playAd(vastResponse, callback);

                /*** Local functions ****/
                function addAdsLabel() {
                    if (adFinished || player.controlBar.getChild('AdsLabel')) {
                        return;
                    }

                    player.controlBar.addChild('AdsLabel');
                }

                function removeAdsLabel() {
                    player.controlBar.removeChild('AdsLabel');
                    adFinished = true;
                }

                function preventManualProgress() {
                    //IOS video clock is very unreliable and we need a 3 seconds threshold to ensure that the user forwarded/rewound the ad
                    var PROGRESS_THRESHOLD = 3;
                    var previousTime = 0;
                    var skipad_attempts = 0;

                    player.on('timeupdate', preventAdSeek);
                    player.on('ended', preventAdSkip);

                    playerUtils.once(player, ['vast.adEnd', 'vast.adsCancel', 'vast.adError'], stopPreventManualProgress);

                    /*** Local functions ***/
                    function preventAdSkip() {
                        // Ignore ended event if the Ad time was not 'near' the end
                        // and revert time to the previous 'valid' time
                        if ((player.duration() - previousTime) > PROGRESS_THRESHOLD) {
                            player.pause(true); // this reduce the video jitter if the IOS skip button is pressed
                            player.play(true); // we need to trigger the play to put the video element back in a valid state
                            player.currentTime(previousTime);
                        }
                    }

                    function preventAdSeek() {
                        var currentTime = player.currentTime();
                        var progressDelta = Math.abs(currentTime - previousTime);
                        if (progressDelta > PROGRESS_THRESHOLD) {
                            skipad_attempts += 1;
                            if (skipad_attempts >= 2) {
                                player.pause(true);
                            }
                            player.currentTime(previousTime);
                        } else {
                            previousTime = currentTime;
                        }
                    }

                    function stopPreventManualProgress() {
                        player.off('timeupdate', preventAdSeek);
                        player.off('ended', preventAdSkip);
                    }
                }
            }

            function trackAdError(error, vastResponse) {
                player.trigger({
                    type: 'vast.adError',
                    error: error
                });
                cancelAds();
                logger.error('AD ERROR:', error.message, error, vastResponse);
            }

            function isVPAID(vastResponse) {
                var i, len;
                var mediaFiles = vastResponse.mediaFiles;
                for (i = 0, len = mediaFiles.length; i < len; i++) {
                    if (vastUtil.isVPAID(mediaFiles[i])) {
                        return true;
                    }
                }
                return false;
            }
        };

    }, {
        "../ads/vast/VASTClient": 22,
        "../ads/vast/VASTError": 23,
        "../ads/vast/VASTIntegrator": 24,
        "../ads/vast/vastUtil": 30,
        "../ads/vpaid/VPAIDIntegrator": 34,
        "../utils/async": 40,
        "../utils/consoleLogger": 41,
        "../utils/dom": 42,
        "../utils/playerUtils": 45,
        "../utils/utilityFunctions": 47
    }],
    40: [function(require, module, exports) {
        //Small subset of async

        var utilities = require('./utilityFunctions');

        var async = {};

        async.setImmediate = function(fn) {
            setTimeout(fn, 0);
        };

        async.iterator = function(tasks) {
            var makeCallback = function(index) {
                var fn = function() {
                    if (tasks.length) {
                        tasks[index].apply(null, arguments);
                    }
                    return fn.next();
                };
                fn.next = function() {
                    return (index < tasks.length - 1) ? makeCallback(index + 1) : null;
                };
                return fn;
            };
            return makeCallback(0);
        };


        async.waterfall = function(tasks, callback) {
            callback = callback || function() {};
            if (!utilities.isArray(tasks)) {
                var err = new Error('First argument to waterfall must be an array of functions');
                return callback(err);
            }
            if (!tasks.length) {
                return callback();
            }
            var wrapIterator = function(iterator) {
                return function(err) {
                    if (err) {
                        callback.apply(null, arguments);
                        callback = function() {};
                    } else {
                        var args = Array.prototype.slice.call(arguments, 1);
                        var next = iterator.next();
                        if (next) {
                            args.push(wrapIterator(next));
                        } else {
                            args.push(callback);
                        }
                        async.setImmediate(function() {
                            iterator.apply(null, args);
                        });
                    }
                };
            };
            wrapIterator(async.iterator(tasks))();
        };

        async.when = function(condition, callback) {
            if (!utilities.isFunction(callback)) {
                throw new Error("async.when error: missing callback argument");
            }

            var isAllowed = utilities.isFunction(condition) ? condition : function() {
                return !!condition;
            };

            return function() {
                var args = utilities.arrayLikeObjToArray(arguments);
                var next = args.pop();

                if (isAllowed.apply(null, args)) {
                    return callback.apply(this, arguments);
                }

                args.unshift(null);
                return next.apply(null, args);
            };
        };

        module.exports = async;


    }, {
        "./utilityFunctions": 47
    }],
    41: [function(require, module, exports) {
        /*jshint unused:false */
        "use strict";

        var _verbosity = 0;
        var _prefix = "[videojs-vast-vpaid] ";

        function setVerbosity(v) {
            _verbosity = v;
        }

        function handleMsg(method, args) {
            if ((args.length) > 0 && (typeof args[0] === 'string')) {
                args[0] = _prefix + args[0];
            }

            if (method.apply) {
                method.apply(console, Array.prototype.slice.call(args));
            } else {
                method(Array.prototype.slice.call(args));
            }
        }

        function debug() {
            if (_verbosity < 4) {
                return;
            }

            if (typeof console.debug === 'undefined') {
                // IE 10 doesn't have a console.debug() function
                handleMsg(console.log, arguments);
            } else {
                handleMsg(console.debug, arguments);
            }
        }

        function log() {
            if (_verbosity < 3) {
                return;
            }

            handleMsg(console.log, arguments);
        }

        function info() {
            if (_verbosity < 2) {
                return;
            }

            handleMsg(console.info, arguments);
        }


        function warn() {
            if (_verbosity < 1) {
                return;
            }

            handleMsg(console.warn, arguments);
        }

        function error() {
            handleMsg(console.error, arguments);
        }

        var consoleLogger = {
            setVerbosity: setVerbosity,
            debug: debug,
            log: log,
            info: info,
            warn: warn,
            error: error
        };

        if ((typeof(console) === 'undefined') || !console.log) {
            // no console available; make functions no-op
            consoleLogger.debug = function() {};
            consoleLogger.log = function() {};
            consoleLogger.info = function() {};
            consoleLogger.warn = function() {};
            consoleLogger.error = function() {};
        }

        module.exports = consoleLogger;
    }, {}],
    42: [function(require, module, exports) {
        'use strict';

        var utilities = require('./utilityFunctions');

        var dom = {};

        dom.isVisible = function isVisible(el) {
            var style = window.getComputedStyle(el);
            return style.visibility !== 'hidden';
        };

        dom.isHidden = function isHidden(el) {
            var style = window.getComputedStyle(el);
            return style.display === 'none';
        };

        dom.isShown = function isShown(el) {
            return !dom.isHidden(el);
        };

        dom.hide = function hide(el) {
            el.__prev_style_display_ = el.style.display;
            el.style.display = 'none';
        };

        dom.show = function show(el) {
            if (dom.isHidden(el)) {
                el.style.display = el.__prev_style_display_;
            }
            el.__prev_style_display_ = undefined;
        };

        dom.hasClass = function hasClass(el, cssClass) {
            var classes, i, len;

            if (utilities.isNotEmptyString(cssClass)) {
                if (el.classList) {
                    return el.classList.contains(cssClass);
                }

                classes = utilities.isString(el.getAttribute('class')) ? el.getAttribute('class').split(/\s+/) : [];
                cssClass = (cssClass || '');

                for (i = 0, len = classes.length; i < len; i += 1) {
                    if (classes[i] === cssClass) {
                        return true;
                    }
                }
            }
            return false;
        };

        dom.addClass = function(el, cssClass) {
            var classes;

            if (utilities.isNotEmptyString(cssClass)) {
                if (el.classList) {
                    return el.classList.add(cssClass);
                }

                classes = utilities.isString(el.getAttribute('class')) ? el.getAttribute('class').split(/\s+/) : [];
                if (utilities.isString(cssClass) && utilities.isNotEmptyString(cssClass.replace(/\s+/, ''))) {
                    classes.push(cssClass);
                    el.setAttribute('class', classes.join(' '));
                }
            }
        };

        dom.removeClass = function(el, cssClass) {
            var classes;

            if (utilities.isNotEmptyString(cssClass)) {
                if (el.classList) {
                    return el.classList.remove(cssClass);
                }

                classes = utilities.isString(el.getAttribute('class')) ? el.getAttribute('class').split(/\s+/) : [];
                var newClasses = [];
                var i, len;
                if (utilities.isString(cssClass) && utilities.isNotEmptyString(cssClass.replace(/\s+/, ''))) {

                    for (i = 0, len = classes.length; i < len; i += 1) {
                        if (cssClass !== classes[i]) {
                            newClasses.push(classes[i]);
                        }
                    }
                    el.setAttribute('class', newClasses.join(' '));
                }
            }
        };

        dom.addEventListener = function addEventListener(el, type, handler) {
            if (utilities.isArray(el)) {
                utilities.forEach(el, function(e) {
                    dom.addEventListener(e, type, handler);
                });
                return;
            }

            if (utilities.isArray(type)) {
                utilities.forEach(type, function(t) {
                    dom.addEventListener(el, t, handler);
                });
                return;
            }

            if (el.addEventListener) {
                el.addEventListener(type, handler, false);
            } else if (el.attachEvent) {
                // WARNING!!! this is a very naive implementation !
                // the event object that should be passed to the handler
                // would not be there for IE8
                // we should use "window.event" and then "event.srcElement"
                // instead of "event.target"
                el.attachEvent("on" + type, handler);
            }
        };

        dom.removeEventListener = function removeEventListener(el, type, handler) {
            if (utilities.isArray(el)) {
                utilities.forEach(el, function(e) {
                    dom.removeEventListener(e, type, handler);
                });
                return;
            }

            if (utilities.isArray(type)) {
                utilities.forEach(type, function(t) {
                    dom.removeEventListener(el, t, handler);
                });
                return;
            }

            if (el.removeEventListener) {
                el.removeEventListener(type, handler, false);
            } else if (el.detachEvent) {
                el.detachEvent("on" + type, handler);
            } else {
                el["on" + type] = null;
            }
        };

        dom.dispatchEvent = function dispatchEvent(el, event) {
            if (el.dispatchEvent) {
                el.dispatchEvent(event);
            } else {
                el.fireEvent("on" + event.eventType, event);
            }
        };

        dom.isDescendant = function isDescendant(parent, child) {
            var node = child.parentNode;
            while (node !== null) {
                if (node === parent) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        };

        dom.getTextContent = function getTextContent(el) {
            return el.textContent || el.text;
        };

        dom.prependChild = function prependChild(parent, child) {
            if (child.parentNode) {
                child.parentNode.removeChild(child);
            }
            return parent.insertBefore(child, parent.firstChild);
        };

        dom.remove = function removeNode(node) {
            if (node && node.parentNode) {
                node.parentNode.removeChild(node);
            }
        };

        dom.isDomElement = function isDomElement(o) {
            return o instanceof Element;
        };

        dom.click = function(el, handler) {
            dom.addEventListener(el, 'click', handler);
        };

        dom.once = function(el, type, handler) {
            function handlerWrap() {
                handler.apply(null, arguments);
                dom.removeEventListener(el, type, handlerWrap);
            }

            dom.addEventListener(el, type, handlerWrap);
        };

        //Note: there is no getBoundingClientRect on iPad so we need a fallback
        dom.getDimension = function getDimension(element) {
            var rect;

            //On IE9 and below getBoundingClientRect does not work consistently
            if (!utilities.isOldIE() && element.getBoundingClientRect) {
                rect = element.getBoundingClientRect();
                return {
                    width: rect.width,
                    height: rect.height
                };
            }

            return {
                width: element.offsetWidth,
                height: element.offsetHeight
            };
        };

        module.exports = dom;
    }, {
        "./utilityFunctions": 47
    }],
    43: [function(require, module, exports) {
        'use strict';

        var urlUtils = require('./urlUtils');
        var utilities = require('./utilityFunctions');

        function HttpRequestError(message) {
            this.message = 'HttpRequest Error: ' + (message || '');
        }
        HttpRequestError.prototype = new Error();
        HttpRequestError.prototype.name = "HttpRequest Error";

        function HttpRequest(createXhr) {
            if (!utilities.isFunction(createXhr)) {
                throw new HttpRequestError('Missing XMLHttpRequest factory method');
            }

            this.createXhr = createXhr;
        }

        HttpRequest.prototype.run = function(method, url, callback, options) {
            sanityCheck(url, callback, options);
            var timeout, timeoutId;
            var xhr = this.createXhr();
            options = options || {};
            timeout = utilities.isNumber(options.timeout) ? options.timeout : 0;

            xhr.open(method, urlUtils.urlParts(url).href, true);

            if (options.headers) {
                setHeaders(xhr, options.headers);
            }

            if (options.withCredentials) {
                xhr.withCredentials = true;
            }

            xhr.onload = function() {
                var statusText, response, status;

                /**
                 * The only way to do a secure request on IE8 and IE9 is with the XDomainRequest object. Unfortunately, microsoft is
                 * so nice that decided that the status property and the 'getAllResponseHeaders' method where not needed so we have to
                 * fake them. If the request gets done with an XDomainRequest instance, we will assume that there are no headers and
                 * the status will always be 200. If you don't like it, DO NOT USE ANCIENT BROWSERS!!!
                 *
                 * For mor info go to: https://msdn.microsoft.com/en-us/library/cc288060(v=vs.85).aspx
                 */
                if (!xhr.getAllResponseHeaders) {
                    xhr.getAllResponseHeaders = function() {
                        return null;
                    };
                }

                if (!xhr.status) {
                    xhr.status = 200;
                }

                if (utilities.isDefined(timeoutId)) {
                    clearTimeout(timeoutId);
                    timeoutId = undefined;
                }

                statusText = xhr.statusText || '';

                // responseText is the old-school way of retrieving response (supported by IE8 & 9)
                // response/responseType properties were introduced in XHR Level2 spec (supported by IE10)
                response = ('response' in xhr) ? xhr.response : xhr.responseText;

                // normalize IE9 bug (http://bugs.jquery.com/ticket/1450)
                status = xhr.status === 1223 ? 204 : xhr.status;

                callback(
                    status,
                    response,
                    xhr.getAllResponseHeaders(),
                    statusText);
            };

            xhr.onerror = requestError;
            xhr.onabort = requestError;

            xhr.send();

            if (timeout > 0) {
                timeoutId = setTimeout(function() {
                    xhr && xhr.abort();
                }, timeout);
            }

            function sanityCheck(url, callback, options) {
                if (!utilities.isString(url) || utilities.isEmptyString(url)) {
                    throw new HttpRequestError("Invalid url '" + url + "'");
                }

                if (!utilities.isFunction(callback)) {
                    throw new HttpRequestError("Invalid handler '" + callback + "' for the http request");
                }

                if (utilities.isDefined(options) && !utilities.isObject(options)) {
                    throw new HttpRequestError("Invalid options map '" + options + "'");
                }
            }

            function setHeaders(xhr, headers) {
                utilities.forEach(headers, function(value, key) {
                    if (utilities.isDefined(value)) {
                        xhr.setRequestHeader(key, value);
                    }
                });
            }

            function requestError() {
                callback(-1, null, null, '');
            }
        };

        HttpRequest.prototype.get = function(url, callback, options) {
            this.run('GET', url, processResponse, options);

            function processResponse(status, response, headersString, statusText) {
                if (isSuccess(status)) {
                    callback(null, response, status, headersString, statusText);
                } else {
                    callback(new HttpRequestError(statusText), response, status, headersString, statusText);
                }
            }

            function isSuccess(status) {
                return 200 <= status && status < 300;
            }
        };

        function createXhr() {
            var xhr = new XMLHttpRequest();
            if (!("withCredentials" in xhr)) {
                // XDomainRequest for IE.
                xhr = new XDomainRequest();
            }
            return xhr;
        }

        var http = new HttpRequest(createXhr);

        module.exports = {
            http: http,
            HttpRequest: HttpRequest,
            HttpRequestError: HttpRequestError,
            createXhr: createXhr
        };

    }, {
        "./urlUtils": 46,
        "./utilityFunctions": 47
    }],
    44: [function(require, module, exports) {
        'use strict';

        module.exports = {
            html5: [
                'text/javascript',
                'text/javascript1.0',
                'text/javascript1.2',
                'text/javascript1.4',
                'text/jscript',
                'application/javascript',
                'application/x-javascript',
                'text/ecmascript',
                'text/ecmascript1.0',
                'text/ecmascript1.2',
                'text/ecmascript1.4',
                'text/livescript',
                'application/ecmascript',
                'application/x-ecmascript',
            ],

            flash: [
                'application/x-shockwave-flash'
            ],
        };

    }, {}],
    45: [function(require, module, exports) {
        'use strict';

        var dom = require('./dom');
        var utilities = require('./utilityFunctions');

        var playerUtils = {};

        /**
         * Returns an object that captures the portions of player state relevant to
         * video playback. The result of this function can be passed to
         * restorePlayerSnapshot with a player to return the player to the state it
         * was in when this function was invoked.
         * @param {object} player The videojs player object
         */
        playerUtils.getPlayerSnapshot = function getPlayerSnapshot(player) {
            var tech = player.el().querySelector('.vjs-tech');

            var snapshot = {
                ended: player.ended(),
                src: player.currentSrc(),
                currentTime: player.currentTime(),
                type: player.currentType(),
                playing: !player.paused(),
                suppressedTracks: getSuppressedTracks(player)
            };

            if (tech) {
                snapshot.nativePoster = tech.poster;
                snapshot.style = tech.getAttribute('style');
            }
            return snapshot;

            /**** Local Functions ****/
            function getSuppressedTracks(player) {
                var tracks = player.remoteTextTracks ? player.remoteTextTracks() : [];

                if (tracks && utilities.isArray(tracks.tracks_)) {
                    tracks = tracks.tracks_;
                }

                if (!utilities.isArray(tracks)) {
                    tracks = [];
                }

                var suppressedTracks = [];
                tracks.forEach(function(track) {
                    suppressedTracks.push({
                        track: track,
                        mode: track.mode
                    });
                    track.mode = 'disabled';
                });

                return suppressedTracks;
            }
        };

        /**
         * Attempts to modify the specified player so that its state is equivalent to
         * the state of the snapshot.
         * @param {object} snapshot - the player state to apply
         */
        playerUtils.restorePlayerSnapshot = function restorePlayerSnapshot(player, snapshot) {
            var tech = player.el().querySelector('.vjs-tech');
            var attempts = 20; // the number of remaining attempts to restore the snapshot

            if (snapshot.nativePoster) {
                tech.poster = snapshot.nativePoster;
            }

            if ('style' in snapshot) {
                // overwrite all css style properties to restore state precisely
                tech.setAttribute('style', snapshot.style || '');
            }

            if (hasSrcChanged(player, snapshot)) {

                // on ios7, fiddling with textTracks too early will cause safari to crash
                player.one('contentloadedmetadata', restoreTracks);

                player.one('canplay', tryToResume);
                ensureCanplayEvtGetsFired();

                // if the src changed for ad playback, reset it
                player.src({
                    src: snapshot.src,
                    type: snapshot.type
                });

                // safari requires a call to `load` to pick up a changed source
                player.load();

            } else {
                restoreTracks();

                if (snapshot.playing) {
                    player.play();
                }
            }

            /*** Local Functions ***/

            /**
             * Sometimes firefox does not trigger the 'canplay' evt.
             * This code ensure that it always gets triggered triggered.
             */
            function ensureCanplayEvtGetsFired() {
                var timeoutId = setTimeout(function() {
                    player.trigger('canplay');
                }, 1000);

                player.one('canplay', function() {
                    clearTimeout(timeoutId);
                });
            }

            /**
             * Determine whether the player needs to be restored to its state
             * before ad playback began. With a custom ad display or burned-in
             * ads, the content player state hasn't been modified and so no
             * restoration is required
             */
            function hasSrcChanged(player, snapshot) {
                if (player.src()) {
                    return player.src() !== snapshot.src;
                }
                // the player was configured through source element children
                return player.currentSrc() !== snapshot.src;
            }

            function restoreTracks() {
                var suppressedTracks = snapshot.suppressedTracks;
                suppressedTracks.forEach(function(trackSnapshot) {
                    trackSnapshot.track.mode = trackSnapshot.mode;
                });
            }

            /**
             * Determine if the video element has loaded enough of the snapshot source
             * to be ready to apply the rest of the state
             */
            function tryToResume() {

                // if some period of the video is seekable, resume playback
                // otherwise delay a bit and then check again unless we're out of attempts

                if (!playerUtils.isReadyToResume(player) && attempts--) {
                    setTimeout(tryToResume, 50);
                } else {
                    try {
                        if (player.currentTime() !== snapshot.currentTime) {
                            if (snapshot.playing) { // if needed restore playing status after seek completes
                                player.one('seeked', function() {
                                    player.play();
                                });
                            }
                            player.currentTime(snapshot.currentTime);

                        } else if (snapshot.playing) {
                            // if needed and no seek has been performed, restore playing status immediately
                            player.play();
                        }

                    } catch (e) {
                        videojs.log.warn('Failed to resume the content after an advertisement', e);
                    }
                }
            }
        };

        playerUtils.isReadyToResume = function(player) {

            if (player.readyState() > 1) {
                // some browsers and media aren't "seekable".
                // readyState greater than 1 allows for seeking without exceptions
                return true;
            }

            if (player.seekable() === undefined) {
                // if the player doesn't expose the seekable time ranges, try to
                // resume playback immediately
                return true;
            }

            if (player.seekable().length > 0) {
                // if some period of the video is seekable, resume playback
                return true;
            }

            return false;
        };

        /**
         * This function prepares the player to display ads.
         * Adding convenience events like the 'vast.firsPlay' that gets fired when the video is first played
         * and ads the blackPoster to the player to prevent content from being displayed before the preroll ad.
         *
         * @param player
         */
        playerUtils.prepareForAds = function(player) {
            var blackPoster = player.addChild('blackPoster');
            var _firstPlay = true;
            var volumeSnapshot;


            monkeyPatchPlayerApi();

            player.on('play', tryToTriggerFirstPlay);
            player.on('vast.reset', resetFirstPlay); //Every time we change the sources we reset the first play.
            player.on('vast.firstPlay', restoreContentVolume);
            player.on('error', hideBlackPoster); //If there is an error in the player we remove the blackposter to show the err msg
            player.on('vast.adStart', hideBlackPoster);
            player.on('vast.adsCancel', hideBlackPoster);
            player.on('vast.adError', hideBlackPoster);
            player.on('vast.adStart', addStyles);
            player.on('vast.adEnd', removeStyles);
            player.on('vast.adsCancel', removeStyles);

            /*** Local Functions ***/

            /**
             What this function does is ugly and horrible and I should think twice before calling myself a good developer. With that said,
             it is the best solution I could find to mute the video until the 'play' event happens (on mobile devices) and the plugin can decide whether
             to play the ad or not.

             We also need this monkeypatch to be able to pause and resume an ad using the player's API

             If you have a better solution please do tell me.
             */
            function monkeyPatchPlayerApi() {

                /**
                 * Monkey patch needed to handle firstPlay and resume of playing ad.
                 *
                 * @param callOrigPlay necessary flag to prevent infinite loop when you are restoring a VAST ad.
                 * @returns {player}
                 */
                var origPlay = player.play;
                player.play = function(callOrigPlay) {
                    var that = this;

                    if (isFirstPlay()) {
                        firstPlay();
                    } else {
                        resume(callOrigPlay);
                    }

                    return this;

                    /*** local functions ***/
                    function firstPlay() {
                        if (!utilities.isIPhone()) {
                            volumeSnapshot = saveVolumeSnapshot();
                            player.muted(true);
                        }

                        origPlay.apply(that, arguments);
                    }

                    function resume(callOrigPlay) {
                        if (isAdPlaying() && !callOrigPlay) {
                            player.vast.adUnit.resumeAd();
                        } else {
                            origPlay.apply(that, arguments);
                        }
                    }
                };


                /**
                 * Needed monkey patch to handle pause of playing ad.
                 *
                 * @param callOrigPlay necessary flag to prevent infinite loop when you are pausing a VAST ad.
                 * @returns {player}
                 */
                var origPause = player.pause;
                player.pause = function(callOrigPause) {
                    if (isAdPlaying() && !callOrigPause) {
                        player.vast.adUnit.pauseAd();
                    } else {
                        origPause.apply(this, arguments);
                    }
                    return this;
                };


                /**
                 * Needed monkey patch to handle paused state of the player when ads are playing.
                 *
                 * @param callOrigPlay necessary flag to prevent infinite loop when you are pausing a VAST ad.
                 * @returns {player}
                 */
                var origPaused = player.paused;
                player.paused = function(callOrigPaused) {
                    if (isAdPlaying() && !callOrigPaused) {
                        return player.vast.adUnit.isPaused();
                    }
                    return origPaused.apply(this, arguments);
                };
            }

            function isAdPlaying() {
                return player.vast && player.vast.adUnit;
            }

            function tryToTriggerFirstPlay() {
                if (isFirstPlay()) {
                    _firstPlay = false;
                    player.trigger('vast.firstPlay');
                }
            }

            function resetFirstPlay() {
                _firstPlay = true;
                blackPoster.show();
                restoreContentVolume();
            }

            function isFirstPlay() {
                return _firstPlay;
            }

            function saveVolumeSnapshot() {
                return {
                    muted: player.muted(),
                    volume: player.volume()
                };
            }

            function restoreContentVolume() {
                if (volumeSnapshot) {
                    player.currentTime(0);
                    restoreVolumeSnapshot(volumeSnapshot);
                    volumeSnapshot = null;
                }
            }

            function restoreVolumeSnapshot(snapshot) {
                if (utilities.isObject(snapshot)) {
                    player.volume(snapshot.volume);
                    player.muted(snapshot.muted);
                }
            }

            function hideBlackPoster() {
                if (!dom.hasClass(blackPoster.el(), 'vjs-hidden')) {
                    blackPoster.hide();
                }
            }

            function addStyles() {
                dom.addClass(player.el(), 'vjs-ad-playing');
            }

            function removeStyles() {
                dom.removeClass(player.el(), 'vjs-ad-playing');
            }
        };

        /**
         * Remove the poster attribute from the video element tech, if present. When
         * reusing a video element for multiple videos, the poster image will briefly
         * reappear while the new source loads. Removing the attribute ahead of time
         * prevents the poster from showing up between videos.
         * @param {object} player The videojs player object
         */
        playerUtils.removeNativePoster = function(player) {
            var tech = player.el().querySelector('.vjs-tech');
            if (tech) {
                tech.removeAttribute('poster');
            }
        };

        /**
         * Helper function to listen to many events until one of them gets fired, then we
         * execute the handler and unsubscribe all the event listeners;
         *
         * @param player specific player from where to listen for the events
         * @param events array of events
         * @param handler function to execute once one of the events fires
         */
        playerUtils.once = function once(player, events, handler) {
            function listener() {
                handler.apply(null, arguments);

                events.forEach(function(event) {
                    player.off(event, listener);
                });
            }

            events.forEach(function(event) {
                player.on(event, listener);
            });
        };


        module.exports = playerUtils;
    }, {
        "./dom": 42,
        "./utilityFunctions": 47
    }],
    46: [function(require, module, exports) {
        'use strict';

        var utilities = require('./utilityFunctions');

        /**
         *
         * IMPORTANT NOTE: This function comes from angularJs and was originally called urlResolve
         *                 you can take a look at the original code here https://github.com/angular/angular.js/blob/master/src/ng/urlUtils.js
         *
         * Implementation Notes for non-IE browsers
         * ----------------------------------------
         * Assigning a URL to the href property of an anchor DOM node, even one attached to the DOM,
         * results both in the normalizing and parsing of the URL.  Normalizing means that a relative
         * URL will be resolved into an absolute URL in the context of the application document.
         * Parsing means that the anchor node's host, hostname, protocol, port, pathname and related
         * properties are all populated to reflect the normalized URL.  This approach has wide
         * compatibility - Safari 1+, Mozilla 1+, Opera 7+,e etc.  See
         * http://www.aptana.com/reference/html/api/HTMLAnchorElement.html
         *
         * Implementation Notes for IE
         * ---------------------------
         * IE >= 8 and <= 10 normalizes the URL when assigned to the anchor node similar to the other
         * browsers.  However, the parsed components will not be set if the URL assigned did not specify
         * them.  (e.g. if you assign a.href = "foo", then a.protocol, a.host, etc. will be empty.)  We
         * work around that by performing the parsing in a 2nd step by taking a previously normalized
         * URL (e.g. by assigning to a.href) and assigning it a.href again.  This correctly populates the
         * properties such as protocol, hostname, port, etc.
         *
         * IE7 does not normalize the URL when assigned to an anchor node.  (Apparently, it does, if one
         * uses the inner HTML approach to assign the URL as part of an HTML snippet -
         * http://stackoverflow.com/a/472729)  However, setting img[src] does normalize the URL.
         * Unfortunately, setting img[src] to something like "javascript:foo" on IE throws an exception.
         * Since the primary usage for normalizing URLs is to sanitize such URLs, we can't use that
         * method and IE < 8 is unsupported.
         *
         * References:
         *   http://developer.mozilla.org/en-US/docs/Web/API/HTMLAnchorElement
         *   http://www.aptana.com/reference/html/api/HTMLAnchorElement.html
         *   http://url.spec.whatwg.org/#urlutils
         *   https://github.com/angular/angular.js/pull/2902
         *   http://james.padolsey.com/javascript/parsing-urls-with-the-dom/
         *
         * @kind function
         * @param {string} url The URL to be parsed.
         * @description Normalizes and parses a URL.
         * @returns {object} Returns the normalized URL as a dictionary.
         *
         *   | member name   | Description    |
         *   |---------------|----------------|
         *   | href          | A normalized version of the provided URL if it was not an absolute URL |
         *   | protocol      | The protocol including the trailing colon                              |
         *   | host          | The host and port (if the port is non-default) of the normalizedUrl    |
         *   | search        | The search params, minus the question mark                             |
         *   | hash          | The hash string, minus the hash symbol
         *   | hostname      | The hostname
         *   | port          | The port, without ":"
         *   | pathname      | The pathname, beginning with "/"
         *
         */

        var urlParsingNode = document.createElement("a");
        /**
         * documentMode is an IE-only property
         * http://msdn.microsoft.com/en-us/library/ie/cc196988(v=vs.85).aspx
         */
        var msie = document.documentMode;

        function urlParts(url) {
            var href = url;

            if (msie) {
                // Normalize before parse.  Refer Implementation Notes on why this is
                // done in two steps on IE.
                urlParsingNode.setAttribute("href", href);
                href = urlParsingNode.href;
            }

            urlParsingNode.setAttribute('href', href);

            // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
            return {
                href: urlParsingNode.href,
                protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
                host: urlParsingNode.host,
                search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
                hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
                hostname: urlParsingNode.hostname,
                port: utilities.isNotEmptyString(urlParsingNode.port) ? urlParsingNode.port : 80,
                pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                    urlParsingNode.pathname :
                    '/' + urlParsingNode.pathname
            };
        }


        /**
         * This function accepts a query string (search part of a url) and returns a dictionary with
         * the different key value pairs
         * @param {string} qs queryString
         */
        function queryStringToObj(qs, cond) {
            var pairs, qsObj;

            cond = utilities.isFunction(cond) ? cond : function() {
                return true;
            };

            qs = qs.trim().replace(/^\?/, '');
            pairs = qs.split('&');
            qsObj = {};

            utilities.forEach(pairs, function(pair) {
                var keyValue, key, value;
                if (pair !== '') {
                    keyValue = pair.split('=');
                    key = keyValue[0];
                    value = keyValue[1];
                    if (cond(key, value)) {
                        qsObj[key] = value;
                    }
                }
            });

            return qsObj;
        }

        /**
         * This function accepts an object and serializes it into a query string without the leading '?'
         * @param obj
         * @returns {string}
         */
        function objToQueryString(obj) {
            var pairs = [];
            utilities.forEach(obj, function(value, key) {
                pairs.push(key + '=' + value);
            });
            return pairs.join('&');
        }

        module.exports = {
            urlParts: urlParts,
            queryStringToObj: queryStringToObj,
            objToQueryString: objToQueryString
        };

    }, {
        "./utilityFunctions": 47
    }],
    47: [function(require, module, exports) {
        /*jshint unused:false */
        "use strict";

        var NODE_TYPE_ELEMENT = 1;
        var SNAKE_CASE_REGEXP = /[A-Z]/g;
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)+$/i;
        /*jslint maxlen: 500 */
        var ISO8086_REGEXP = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;


        function noop() {}

        function isNull(o) {
            return o === null;
        }

        function isDefined(o) {
            return o !== undefined;
        }

        function isUndefined(o) {
            return o === undefined;
        }

        function isObject(obj) {
            return typeof obj === 'object';
        }

        function isFunction(str) {
            return typeof str === 'function';
        }

        function isNumber(num) {
            return typeof num === 'number';
        }

        function isWindow(obj) {
            return utilities.isObject(obj) && obj.window === obj;
        }

        function isArray(array) {
            return Object.prototype.toString.call(array) === '[object Array]';
        }

        function isArrayLike(obj) {
            if (obj === null || utilities.isWindow(obj) || utilities.isFunction(obj) || utilities.isUndefined(obj)) {
                return false;
            }

            var length = obj.length;

            if (obj.nodeType === NODE_TYPE_ELEMENT && length) {
                return true;
            }

            return utilities.isString(obj) || utilities.isArray(obj) || length === 0 ||
                typeof length === 'number' && length > 0 && (length - 1) in obj;
        }

        function isString(str) {
            return typeof str === 'string';
        }

        function isEmptyString(str) {
            return utilities.isString(str) && str.length === 0;
        }

        function isNotEmptyString(str) {
            return utilities.isString(str) && str.length !== 0;
        }

        function arrayLikeObjToArray(args) {
            return Array.prototype.slice.call(args);
        }

        function forEach(obj, iterator, context) {
            var key, length;
            if (obj) {
                if (isFunction(obj)) {
                    for (key in obj) {
                        // Need to check if hasOwnProperty exists,
                        // as on IE8 the result of querySelectorAll is an object without a hasOwnProperty function
                        if (key !== 'prototype' && key !== 'length' && key !== 'name' && (!obj.hasOwnProperty || obj.hasOwnProperty(key))) {
                            iterator.call(context, obj[key], key, obj);
                        }
                    }
                } else if (isArray(obj)) {
                    var isPrimitive = typeof obj !== 'object';
                    for (key = 0, length = obj.length; key < length; key++) {
                        if (isPrimitive || key in obj) {
                            iterator.call(context, obj[key], key, obj);
                        }
                    }
                } else if (obj.forEach && obj.forEach !== forEach) {
                    obj.forEach(iterator, context, obj);
                } else {
                    for (key in obj) {
                        if (obj.hasOwnProperty(key)) {
                            iterator.call(context, obj[key], key, obj);
                        }
                    }
                }
            }
            return obj;
        }

        function snake_case(name, separator) {
            separator = separator || '_';
            return name.replace(SNAKE_CASE_REGEXP, function(letter, pos) {
                return (pos ? separator : '') + letter.toLowerCase();
            });
        }

        function isValidEmail(email) {
            if (!utilities.isString(email)) {
                return false;
            }

            return EMAIL_REGEXP.test(email.trim());
        }

        function extend(obj) {
            var arg, i, k;
            for (i = 1; i < arguments.length; i++) {
                arg = arguments[i];
                for (k in arg) {
                    if (arg.hasOwnProperty(k)) {
                        if (isObject(obj[k]) && !isNull(obj[k]) && isObject(arg[k])) {
                            obj[k] = extend({}, obj[k], arg[k]);
                        } else {
                            obj[k] = arg[k];
                        }
                    }
                }
            }
            return obj;
        }

        function capitalize(s) {
            return s.charAt(0).toUpperCase() + s.slice(1);
        }

        function decapitalize(s) {
            return s.charAt(0).toLowerCase() + s.slice(1);
        }

        /**
         * This method works the same way array.prototype.map works but if the transformer returns undefine, then
         * it won't be added to the transformed Array.
         */
        function transformArray(array, transformer) {
            var transformedArray = [];

            array.forEach(function(item, index) {
                var transformedItem = transformer(item, index);
                if (utilities.isDefined(transformedItem)) {
                    transformedArray.push(transformedItem);
                }
            });

            return transformedArray;
        }

        function toFixedDigits(num, digits) {
            var formattedNum = num + '';
            digits = utilities.isNumber(digits) ? digits : 0;
            num = utilities.isNumber(num) ? num : parseInt(num, 10);
            if (utilities.isNumber(num) && !isNaN(num)) {
                formattedNum = num + '';
                while (formattedNum.length < digits) {
                    formattedNum = '0' + formattedNum;
                }
                return formattedNum;
            }
            return NaN + '';
        }

        function throttle(callback, delay) {
            var previousCall = new Date().getTime() - (delay + 1);
            return function() {
                var time = new Date().getTime();
                if ((time - previousCall) >= delay) {
                    previousCall = time;
                    callback.apply(this, arguments);
                }
            };
        }

        function debounce(callback, wait) {
            var timeoutId;

            return function() {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }
                timeoutId = setTimeout(function() {
                    callback.apply(this, arguments);
                    timeoutId = undefined;
                }, wait);
            };
        }

        // a function designed to blow up the stack in a naive way
        // but it is ok for videoJs children components
        function treeSearch(root, getChildren, found) {
            var children = getChildren(root);
            for (var i = 0; i < children.length; i++) {
                if (found(children[i])) {
                    return children[i];
                } else {
                    var el = treeSearch(children[i], getChildren, found);
                    if (el) {
                        return el;
                    }
                }
            }
        }

        function echoFn(val) {
            return function() {
                return val;
            };
        }

        //Note: Supported formats come from http://www.w3.org/TR/NOTE-datetime
        // and the iso8601 regex comes from http://www.pelagodesign.com/blog/2009/05/20/iso-8601-date-validation-that-doesnt-suck/
        function isISO8601(value) {
            if (utilities.isNumber(value)) {
                value = value + ''; //we make sure that we are working with strings
            }

            if (!utilities.isString(value)) {
                return false;
            }

            return ISO8086_REGEXP.test(value.trim());
        }

        /**
         * Checks if the Browser is IE9 and below
         * @returns {boolean}
         */
        function isOldIE() {
            var version = utilities.getInternetExplorerVersion(navigator);
            if (version === -1) {
                return false;
            }

            return version < 10;
        }

        /**
         * Returns the version of Internet Explorer or a -1 (indicating the use of another browser).
         * Source: https://msdn.microsoft.com/en-us/library/ms537509(v=vs.85).aspx
         * @returns {number} the version of Internet Explorer or a -1 (indicating the use of another browser).
         */
        function getInternetExplorerVersion(navigator) {
            var rv = -1;

            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                var res = re.exec(ua);
                if (res !== null) {
                    rv = parseFloat(res[1]);
                }
            }

            return rv;
        }

        /*** Mobile Utility functions ***/
        function isIDevice() {
            return /iP(hone|ad)/.test(utilities._UA);
        }

        function isMobile() {
            return /iP(hone|ad|od)|Android|Windows Phone/.test(utilities._UA);
        }

        function isIPhone() {
            return /iP(hone|od)/.test(utilities._UA);
        }

        function isAndroid() {
            return /Android/.test(utilities._UA);
        }

        var utilities = {
            _UA: navigator.userAgent,
            noop: noop,
            isNull: isNull,
            isDefined: isDefined,
            isUndefined: isUndefined,
            isObject: isObject,
            isFunction: isFunction,
            isNumber: isNumber,
            isWindow: isWindow,
            isArray: isArray,
            isArrayLike: isArrayLike,
            isString: isString,
            isEmptyString: isEmptyString,
            isNotEmptyString: isNotEmptyString,
            arrayLikeObjToArray: arrayLikeObjToArray,
            forEach: forEach,
            snake_case: snake_case,
            isValidEmail: isValidEmail,
            extend: extend,
            capitalize: capitalize,
            decapitalize: decapitalize,
            transformArray: transformArray,
            toFixedDigits: toFixedDigits,
            throttle: throttle,
            debounce: debounce,
            treeSearch: treeSearch,
            echoFn: echoFn,
            isISO8601: isISO8601,
            isOldIE: isOldIE,
            getInternetExplorerVersion: getInternetExplorerVersion,
            isIDevice: isIDevice,
            isMobile: isMobile,
            isIPhone: isIPhone,
            isAndroid: isAndroid
        };

        module.exports = utilities;

    }, {}],
    48: [function(require, module, exports) {
        'use strict';

        var utilities = require('./utilityFunctions');

        var xml = {};

        xml.strToXMLDoc = function strToXMLDoc(stringContainingXMLSource) {
            //IE 8
            if (typeof window.DOMParser === 'undefined') {
                var xmlDocument = new ActiveXObject('Microsoft.XMLDOM');
                xmlDocument.async = false;
                xmlDocument.loadXML(stringContainingXMLSource);
                return xmlDocument;
            }

            return parseString(stringContainingXMLSource);

            function parseString(stringContainingXMLSource) {
                var parser = new DOMParser();
                var parsedDocument;

                //Note: This try catch is to deal with the fact that on IE parser.parseFromString does throw an error but the rest of the browsers don't.
                try {
                    parsedDocument = parser.parseFromString(stringContainingXMLSource, "application/xml");

                    if (isParseError(parsedDocument) || utilities.isEmptyString(stringContainingXMLSource)) {
                        throw new Error();
                    }
                } catch (e) {
                    throw new Error("xml.strToXMLDOC: Error parsing the string: '" + stringContainingXMLSource + "'");
                }

                return parsedDocument;
            }

            function isParseError(parsedDocument) {
                try { // parser and parsererrorNS could be cached on startup for efficiency
                    var parser = new DOMParser(),
                        erroneousParse = parser.parseFromString('INVALID', 'text/xml'),
                        parsererrorNS = erroneousParse.getElementsByTagName("parsererror")[0].namespaceURI;

                    if (parsererrorNS === 'http://www.w3.org/1999/xhtml') {
                        // In PhantomJS the parseerror element doesn't seem to have a special namespace, so we are just guessing here :(
                        return parsedDocument.getElementsByTagName("parsererror").length > 0;
                    }

                    return parsedDocument.getElementsByTagNameNS(parsererrorNS, 'parsererror').length > 0;
                } catch (e) {
                    //Note on IE parseString throws an error by itself and it will never reach this code. Because it will have failed before
                }
            }
        };

        xml.parseText = function parseText(sValue) {
            if (/^\s*$/.test(sValue)) {
                return null;
            }
            if (/^(?:true|false)$/i.test(sValue)) {
                return sValue.toLowerCase() === "true";
            }
            if (isFinite(sValue)) {
                return parseFloat(sValue);
            }
            if (utilities.isISO8601(sValue)) {
                return new Date(sValue);
            }
            return sValue.trim();
        };

        xml.JXONTree = function JXONTree(oXMLParent) {
            var parseText = xml.parseText;

            //The document object is an especial object that it may miss some functions or attrs depending on the browser.
            //To prevent this problem with create the JXONTree using the root childNode which is a fully fleshed node on all supported
            //browsers.
            if (oXMLParent.documentElement) {
                return new xml.JXONTree(oXMLParent.documentElement);
            }

            if (oXMLParent.hasChildNodes()) {
                var sCollectedTxt = "";
                for (var oNode, sProp, vContent, nItem = 0; nItem < oXMLParent.childNodes.length; nItem++) {
                    oNode = oXMLParent.childNodes.item(nItem);
                    /*jshint bitwise: false*/
                    if ((oNode.nodeType - 1 | 1) === 3) {
                        sCollectedTxt += oNode.nodeType === 3 ? oNode.nodeValue.trim() : oNode.nodeValue;
                    } else if (oNode.nodeType === 1 && !oNode.prefix) {
                        sProp = utilities.decapitalize(oNode.nodeName);
                        vContent = new xml.JXONTree(oNode);
                        if (this.hasOwnProperty(sProp)) {
                            if (this[sProp].constructor !== Array) {
                                this[sProp] = [this[sProp]];
                            }
                            this[sProp].push(vContent);
                        } else {
                            this[sProp] = vContent;
                        }
                    }
                }
                if (sCollectedTxt) {
                    this.keyValue = parseText(sCollectedTxt);
                }
            }

            //IE8 Stupid fix
            var hasAttr = typeof oXMLParent.hasAttributes === 'undefined' ? oXMLParent.attributes.length > 0 : oXMLParent.hasAttributes();
            if (hasAttr) {
                var oAttrib;
                for (var nAttrib = 0; nAttrib < oXMLParent.attributes.length; nAttrib++) {
                    oAttrib = oXMLParent.attributes.item(nAttrib);
                    this["@" + utilities.decapitalize(oAttrib.name)] = parseText(oAttrib.value.trim());
                }
            }
        };

        xml.JXONTree.prototype.attr = function(attr) {
            return this['@' + utilities.decapitalize(attr)];
        };

        xml.toJXONTree = function toJXONTree(xmlString) {
            var xmlDoc = xml.strToXMLDoc(xmlString);
            return new xml.JXONTree(xmlDoc);
        };

        /**
         * Helper function to extract the keyvalue of a JXONTree obj
         *
         * @param xmlObj {JXONTree}
         * return the key value or undefined;
         */
        xml.keyValue = function getKeyValue(xmlObj) {
            if (xmlObj) {
                return xmlObj.keyValue;
            }
            return undefined;
        };

        xml.attr = function getAttrValue(xmlObj, attr) {
            if (xmlObj) {
                return xmlObj['@' + utilities.decapitalize(attr)];
            }
            return undefined;
        };

        xml.encode = function encodeXML(str) {
            if (!utilities.isString(str)) return undefined;

            return str.replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&apos;');
        };

        xml.decode = function decodeXML(str) {
            if (!utilities.isString(str)) return undefined;

            return str.replace(/&apos;/g, "'")
                .replace(/&quot;/g, '"')
                .replace(/&gt;/g, '>')
                .replace(/&lt;/g, '<')
                .replace(/&amp;/g, '&');
        };

        module.exports = xml;

    }, {
        "./utilityFunctions": 47
    }],
    49: [function(require, module, exports) {
        'use strict';

        require('./plugin/components/ads-label_5');
        require('./plugin/components/black-poster_5');

        var videoJsVAST = require('./plugin/videojs.vast.vpaid');

        videojs.plugin('vastClient', videoJsVAST);

    }, {
        "./plugin/components/ads-label_5": 36,
        "./plugin/components/black-poster_5": 38,
        "./plugin/videojs.vast.vpaid": 39
    }]
}, {}, [49])