<div class="row">
    <div class="col-md-12">
        <h2><?php echo $this->Language['faq']; ?></h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-6" style="margin-bottom:20px;">
                <div class="input-group">
                    <input type="text" name="search" id="search" placeholder="<?php echo $this->Language['what_are_you_searching']; ?>" class="form-control" />
                    <span class="input-group-addon" style="border-radius:0;"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="loader" style="display:none;"></div>
        <div class="faq_data">
        <?php
        if (count($data) > 0) {
            $c = 0;
            foreach ($data as $faq) {
                $html = html_entity_decode($faq->content);
        ?> 
        
        <div class="faqs">
            <div>
                <h4><a href="javascript:void(0);" class="showmore" data-id="<?php echo $faq->id; ?>";"><?php echo Yii::app()->common->htmlchars_encode_to_html($faq->title); ?></a></h4>
            </div>   
            <div>
                <?php
                    $html_removed_text = Yii::app()->common->strip_html_tags($faq->content);
                    echo strlen($html_removed_text)>500 ? substr($html_removed_text, 0, 500).'...<a class="link showmore" href="javascript:void(0);" data-id="'.$faq->id.'">'.$this->Language['view_more'].' &raquo;</a>' : $html_removed_text;
                ?>
            </div>
        </div>    
        <div class="clearfix"></div><br />
        <?php
            }
        }else{ ?>
           <div class="faqs"><?php echo $this->Language['no_content_added_yet']; ?></div> 
       <?php } ?>
            <!--div class="pull-right">
                <?php/*
                if ($data) {
                    $opts = array('class' => 'pagination');
                    $this->widget('CLinkPager', array(
                        'currentPage' => $pages->getCurrentPage(),
                        'itemCount' => $item_count,
                        'pageSize' => $page_size,
                        'maxButtonCount' => 6,
                        'nextPageLabel' => 'Next &gt;',
                        "htmlOptions" => $opts,
                        'header' => '',
                        'selectedPageCssClass' => 'active',
                        'lastPageLabel' => 'Last',
                        'firstPageLabel' => 'First',
                    ));
                }*/
                ?>                            
            </div-->    
        </div>
    </div>
</div>
<script type="text/javascript">
    var typingTimer;
    var doneTypingInterval = 1000;
    $(document).ready(function(){
        $('#search').keyup(function(){
            $('.loader').show();
            clearTimeout(typingTimer);
            if ($('#search').val) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        }); 
        $('.faq_data').on('click','.showmore',function(){
            $('.loader').show();
            var faq_id = $(this).data('id');
            var url = "/faqs/search";
            $.ajax({
                url: url,
                data:{"faq_id":faq_id},
                method: "post",
                success: function (result) {
                   $('.faq_data').html(result);
                   $('.loader').hide();
                }
            });
        });
    });
    function doneTyping () {
        var searchtext = $('#search').val();
        var url = "/faqs/search";
        $.ajax({
            url: url,
            data:{"searchtext":searchtext},
            method: "post",
            success: function (result) {
               $('.faq_data').html(result);
               $('.loader').hide();
            }
        });
    }
</script>
