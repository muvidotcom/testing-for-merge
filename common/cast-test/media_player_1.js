// Copyright Google Inc. All Rights Reserved.
(function() {
    'use strict';
    var h, m = function(a, b) {
            function c() {}
            c.prototype = b.prototype;
            a.Le = b.prototype;
            a.prototype = new c;
            a.prototype.constructor = a;
            for (var d in b)
                if ("prototype" != d)
                    if (Object.defineProperties) {
                        var e = Object.getOwnPropertyDescriptor(b, d);
                        e && Object.defineProperty(a, d, e)
                    } else a[d] = b[d]
        },
        aa = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, c) {
            a != Array.prototype && a != Object.prototype && (a[b] = c.value)
        },
        ba = "undefined" != typeof window && window === this ? this : "undefined" != typeof global &&
        null != global ? global : this,
        ca = function() {
            ca = function() {};
            ba.Symbol || (ba.Symbol = da)
        },
        ea = 0,
        da = function(a) {
            return "jscomp_symbol_" + (a || "") + ea++
        },
        ha = function() {
            ca();
            var a = ba.Symbol.iterator;
            a || (a = ba.Symbol.iterator = ba.Symbol("iterator"));
            "function" != typeof Array.prototype[a] && aa(Array.prototype, a, {
                configurable: !0,
                writable: !0,
                value: function() {
                    return fa(this)
                }
            });
            ha = function() {}
        },
        fa = function(a) {
            var b = 0;
            return ia(function() {
                return b < a.length ? {
                    done: !1,
                    value: a[b++]
                } : {
                    done: !0
                }
            })
        },
        ia = function(a) {
            ha();
            a = {
                next: a
            };
            a[ba.Symbol.iterator] = function() {
                return this
            };
            return a
        },
        p = function(a) {
            ha();
            var b = a[Symbol.iterator];
            return b ? b.call(a) : fa(a)
        },
        ja = function(a) {
            if (!(a instanceof Array)) {
                a = p(a);
                for (var b, c = []; !(b = a.next()).done;) c.push(b.value);
                a = c
            }
            return a
        },
        ka = ka || {},
        t = this,
        u = function(a) {
            return void 0 !== a
        },
        w = function(a) {
            return "string" == typeof a
        },
        x = function(a) {
            return "number" == typeof a
        },
        la = function() {},
        ma = function(a) {
            a.Rg = void 0;
            a.kf = function() {
                return a.Rg ? a.Rg : a.Rg = new a
            }
        },
        na = function(a) {
            var b = typeof a;
            if ("object" ==
                b)
                if (a) {
                    if (a instanceof Array) return "array";
                    if (a instanceof Object) return b;
                    var c = Object.prototype.toString.call(a);
                    if ("[object Window]" == c) return "object";
                    if ("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) return "array";
                    if ("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) return "function"
                } else return "null";
            else if ("function" ==
                b && "undefined" == typeof a.call) return "object";
            return b
        },
        oa = function(a) {
            return "array" == na(a)
        },
        ra = function(a) {
            var b = na(a);
            return "array" == b || "object" == b && "number" == typeof a.length
        },
        sa = function(a) {
            return "function" == na(a)
        },
        ta = function(a) {
            var b = typeof a;
            return "object" == b && null != a || "function" == b
        },
        ua = function(a, b, c) {
            return a.call.apply(a.bind, arguments)
        },
        va = function(a, b, c) {
            if (!a) throw Error();
            if (2 < arguments.length) {
                var d = Array.prototype.slice.call(arguments, 2);
                return function() {
                    var c = Array.prototype.slice.call(arguments);
                    Array.prototype.unshift.apply(c, d);
                    return a.apply(b, c)
                }
            }
            return function() {
                return a.apply(b, arguments)
            }
        },
        wa = function(a, b, c) {
            wa = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? ua : va;
            return wa.apply(null, arguments)
        },
        xa = function(a, b) {
            var c = Array.prototype.slice.call(arguments, 1);
            return function() {
                var b = c.slice();
                b.push.apply(b, arguments);
                return a.apply(this, b)
            }
        },
        y = Date.now || function() {
            return +new Date
        },
        z = function(a, b) {
            a = a.split(".");
            var c = t;
            a[0] in c || !c.execScript ||
                c.execScript("var " + a[0]);
            for (var d; a.length && (d = a.shift());) !a.length && u(b) ? c[d] = b : c = c[d] && c[d] !== Object.prototype[d] ? c[d] : c[d] = {}
        },
        ya = function(a, b) {
            function c() {}
            c.prototype = b.prototype;
            a.Le = b.prototype;
            a.prototype = new c;
            a.prototype.constructor = a;
            a.tl = function(a, c, f) {
                for (var d = Array(arguments.length - 2), e = 2; e < arguments.length; e++) d[e - 2] = arguments[e];
                return b.prototype[c].apply(a, d)
            }
        };
    var cast = t.cast || {};
    var za = function() {
            return !(!cast.__platform__ || !cast.__platform__.metrics)
        },
        Aa = function(a, b) {
            za() && cast.__platform__.metrics.logBoolToUma(a, b)
        },
        Ba = function(a) {
            za() && cast.__platform__.metrics.logEventToUma(a)
        },
        A = function(a, b) {
            za() && cast.__platform__.metrics.logIntToUma(a, b)
        };
    var Ca = function(a) {
        if (Error.captureStackTrace) Error.captureStackTrace(this, Ca);
        else {
            var b = Error().stack;
            b && (this.stack = b)
        }
        a && (this.message = String(a))
    };
    ya(Ca, Error);
    Ca.prototype.name = "CustomError";
    var Da = function(a, b) {
            var c = String(b).toLowerCase();
            a = String(a.substr(0, b.length)).toLowerCase();
            return 0 == (c < a ? -1 : c == a ? 0 : 1)
        },
        Ea = function(a, b) {
            for (var c = a.split("%s"), d = "", e = Array.prototype.slice.call(arguments, 1); e.length && 1 < c.length;) d += c.shift() + e.shift();
            return d + c.join("%s")
        },
        Fa = String.prototype.trim ? function(a) {
            return a.trim()
        } : function(a) {
            return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
        },
        Ga = function(a, b) {
            return a < b ? -1 : a > b ? 1 : 0
        };
    var Ha = function(a, b) {
        b.unshift(a);
        Ca.call(this, Ea.apply(null, b));
        b.shift()
    };
    ya(Ha, Ca);
    Ha.prototype.name = "AssertionError";
    var Ia = function(a, b) {
        throw new Ha("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1));
    };
    var Ja = function(a, b, c) {
        b = b || new Uint8Array(a.length);
        var d = 0;
        for (c = c || 0; d < a.length; d++) b[d + c] = a.charCodeAt(d);
        return b
    };
    var Ka = Array.prototype.indexOf ? function(a, b, c) {
            return Array.prototype.indexOf.call(a, b, c)
        } : function(a, b, c) {
            c = null == c ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
            if (w(a)) return w(b) && 1 == b.length ? a.indexOf(b, c) : -1;
            for (; c < a.length; c++)
                if (c in a && a[c] === b) return c;
            return -1
        },
        La = Array.prototype.lastIndexOf ? function(a, b, c) {
            return Array.prototype.lastIndexOf.call(a, b, null == c ? a.length - 1 : c)
        } : function(a, b, c) {
            c = null == c ? a.length - 1 : c;
            0 > c && (c = Math.max(0, a.length + c));
            if (w(a)) return w(b) && 1 == b.length ? a.lastIndexOf(b, c) : -1;
            for (; 0 <=
                c; c--)
                if (c in a && a[c] === b) return c;
            return -1
        },
        Ma = Array.prototype.forEach ? function(a, b, c) {
            Array.prototype.forEach.call(a, b, c)
        } : function(a, b, c) {
            for (var d = a.length, e = w(a) ? a.split("") : a, f = 0; f < d; f++) f in e && b.call(c, e[f], f, a)
        },
        Na = Array.prototype.map ? function(a, b, c) {
            return Array.prototype.map.call(a, b, c)
        } : function(a, b, c) {
            for (var d = a.length, e = Array(d), f = w(a) ? a.split("") : a, g = 0; g < d; g++) g in f && (e[g] = b.call(c, f[g], g, a));
            return e
        },
        Oa = Array.prototype.some ? function(a, b, c) {
            return Array.prototype.some.call(a, b, c)
        } :
        function(a, b, c) {
            for (var d = a.length, e = w(a) ? a.split("") : a, f = 0; f < d; f++)
                if (f in e && b.call(c, e[f], f, a)) return !0;
            return !1
        },
        Qa = function(a) {
            a: {
                var b = Pa;
                for (var c = a.length, d = w(a) ? a.split("") : a, e = 0; e < c; e++)
                    if (e in d && b.call(void 0, d[e], e, a)) {
                        b = e;
                        break a
                    }
                b = -1
            }
            return 0 > b ? null : w(a) ? a.charAt(b) : a[b]
        },
        Ra = function(a) {
            if (!oa(a))
                for (var b = a.length - 1; 0 <= b; b--) delete a[b];
            a.length = 0
        },
        Sa = function(a, b) {
            b = Ka(a, b);
            var c;
            (c = 0 <= b) && Array.prototype.splice.call(a, b, 1);
            return c
        },
        Ta = function(a) {
            return Array.prototype.concat.apply([],
                arguments)
        },
        Ua = function(a) {
            var b = a.length;
            if (0 < b) {
                for (var c = Array(b), d = 0; d < b; d++) c[d] = a[d];
                return c
            }
            return []
        };
    var Va = function(a, b, c) {
        this.zk = c;
        this.Yj = a;
        this.Yk = b;
        this.Gf = 0;
        this.rf = null
    };
    Va.prototype.get = function() {
        if (0 < this.Gf) {
            this.Gf--;
            var a = this.rf;
            this.rf = a.next;
            a.next = null
        } else a = this.Yj();
        return a
    };
    Va.prototype.put = function(a) {
        this.Yk(a);
        this.Gf < this.zk && (this.Gf++, a.next = this.rf, this.rf = a)
    };
    var Wa;
    a: {
        var Xa = t.navigator;
        if (Xa) {
            var Ya = Xa.userAgent;
            if (Ya) {
                Wa = Ya;
                break a
            }
        }
        Wa = ""
    }
    var Za = function(a) {
        return -1 != Wa.indexOf(a)
    };
    var $a = function(a, b) {
            for (var c in a)
                if (b.call(void 0, a[c], c, a)) return !0;
            return !1
        },
        ab = function(a) {
            var b = [],
                c = 0,
                d;
            for (d in a) b[c++] = a[d];
            return b
        },
        bb = function(a) {
            var b = [],
                c = 0,
                d;
            for (d in a) b[c++] = d;
            return b
        },
        cb = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" "),
        db = function(a, b) {
            for (var c, d, e = 1; e < arguments.length; e++) {
                d = arguments[e];
                for (c in d) a[c] = d[c];
                for (var f = 0; f < cb.length; f++) c = cb[f], Object.prototype.hasOwnProperty.call(d, c) && (a[c] = d[c])
            }
        };
    var eb = function(a) {
            t.setTimeout(function() {
                throw a;
            }, 0)
        },
        fb, gb = function() {
            var a = t.MessageChannel;
            "undefined" === typeof a && "undefined" !== typeof window && window.postMessage && window.addEventListener && !Za("Presto") && (a = function() {
                var a = document.createElement("IFRAME");
                a.style.display = "none";
                a.src = "";
                document.documentElement.appendChild(a);
                var b = a.contentWindow;
                a = b.document;
                a.open();
                a.write("");
                a.close();
                var c = "callImmediate" + Math.random(),
                    d = "file:" == b.location.protocol ? "*" : b.location.protocol + "//" + b.location.host;
                a = wa(function(a) {
                    if (("*" == d || a.origin == d) && a.data == c) this.port1.onmessage()
                }, this);
                b.addEventListener("message", a, !1);
                this.port1 = {};
                this.port2 = {
                    postMessage: function() {
                        b.postMessage(c, d)
                    }
                }
            });
            if ("undefined" !== typeof a && !Za("Trident") && !Za("MSIE")) {
                var b = new a,
                    c = {},
                    d = c;
                b.port1.onmessage = function() {
                    if (u(c.next)) {
                        c = c.next;
                        var a = c.Vh;
                        c.Vh = null;
                        a()
                    }
                };
                return function(a) {
                    d.next = {
                        Vh: a
                    };
                    d = d.next;
                    b.port2.postMessage(0)
                }
            }
            return "undefined" !== typeof document && "onreadystatechange" in document.createElement("SCRIPT") ?
                function(a) {
                    var b = document.createElement("SCRIPT");
                    b.onreadystatechange = function() {
                        b.onreadystatechange = null;
                        b.parentNode.removeChild(b);
                        b = null;
                        a();
                        a = null
                    };
                    document.documentElement.appendChild(b)
                } : function(a) {
                    t.setTimeout(a, 0)
                }
        };
    var hb = function() {
            this.fg = this.Ld = null
        },
        jb = new Va(function() {
            return new ib
        }, function(a) {
            a.reset()
        }, 100);
    hb.prototype.add = function(a, b) {
        var c = jb.get();
        c.set(a, b);
        this.fg ? this.fg.next = c : this.Ld = c;
        this.fg = c
    };
    hb.prototype.remove = function() {
        var a = null;
        this.Ld && (a = this.Ld, this.Ld = this.Ld.next, this.Ld || (this.fg = null), a.next = null);
        return a
    };
    var ib = function() {
        this.next = this.scope = this.md = null
    };
    ib.prototype.set = function(a, b) {
        this.md = a;
        this.scope = b;
        this.next = null
    };
    ib.prototype.reset = function() {
        this.next = this.scope = this.md = null
    };
    var ob = function(a, b) {
            kb || lb();
            mb || (kb(), mb = !0);
            nb.add(a, b)
        },
        kb, lb = function() {
            if (-1 != String(t.Promise).indexOf("[native code]")) {
                var a = t.Promise.resolve(void 0);
                kb = function() {
                    a.then(qb)
                }
            } else kb = function() {
                var a = qb;
                !sa(t.setImmediate) || t.Window && t.Window.prototype && !Za("Edge") && t.Window.prototype.setImmediate == t.setImmediate ? (fb || (fb = gb()), fb(a)) : t.setImmediate(a)
            }
        },
        mb = !1,
        nb = new hb,
        qb = function() {
            for (var a; a = nb.remove();) {
                try {
                    a.md.call(a.scope)
                } catch (b) {
                    eb(b)
                }
                jb.put(a)
            }
            mb = !1
        };
    var rb = function(a) {
        rb[" "](a);
        return a
    };
    rb[" "] = la;
    var tb = function(a, b) {
        var c = sb;
        return Object.prototype.hasOwnProperty.call(c, a) ? c[a] : c[a] = b(a)
    };
    var ub = Za("Opera"),
        vb = Za("Trident") || Za("MSIE"),
        wb = Za("Edge"),
        xb = Za("Gecko") && !(-1 != Wa.toLowerCase().indexOf("webkit") && !Za("Edge")) && !(Za("Trident") || Za("MSIE")) && !Za("Edge"),
        yb = -1 != Wa.toLowerCase().indexOf("webkit") && !Za("Edge"),
        zb = function() {
            var a = t.document;
            return a ? a.documentMode : void 0
        },
        Ab;
    a: {
        var Bb = "",
            Cb = function() {
                var a = Wa;
                if (xb) return /rv\:([^\);]+)(\)|;)/.exec(a);
                if (wb) return /Edge\/([\d\.]+)/.exec(a);
                if (vb) return /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);
                if (yb) return /WebKit\/(\S+)/.exec(a);
                if (ub) return /(?:Version)[ \/]?(\S+)/.exec(a)
            }();Cb && (Bb = Cb ? Cb[1] : "");
        if (vb) {
            var Db = zb();
            if (null != Db && Db > parseFloat(Bb)) {
                Ab = String(Db);
                break a
            }
        }
        Ab = Bb
    }
    var Eb = Ab,
        sb = {},
        Fb = function(a) {
            return tb(a, function() {
                for (var b = 0, c = Fa(String(Eb)).split("."), d = Fa(String(a)).split("."), e = Math.max(c.length, d.length), f = 0; 0 == b && f < e; f++) {
                    var g = c[f] || "",
                        k = d[f] || "";
                    do {
                        g = /(\d*)(\D*)(.*)/.exec(g) || ["", "", "", ""];
                        k = /(\d*)(\D*)(.*)/.exec(k) || ["", "", "", ""];
                        if (0 == g[0].length && 0 == k[0].length) break;
                        b = Ga(0 == g[1].length ? 0 : parseInt(g[1], 10), 0 == k[1].length ? 0 : parseInt(k[1], 10)) || Ga(0 == g[2].length, 0 == k[2].length) || Ga(g[2], k[2]);
                        g = g[3];
                        k = k[3]
                    } while (0 == b)
                }
                return 0 <= b
            })
        },
        Gb;
    var Hb = t.document;
    Gb = Hb && vb ? zb() || ("CSS1Compat" == Hb.compatMode ? parseInt(Eb, 10) : 5) : void 0;
    var Ib = function(a, b, c, d, e) {
        this.reset(a, b, c, d, e)
    };
    Ib.prototype.oc = 0;
    Ib.prototype.Eg = null;
    var Jb = 0;
    Ib.prototype.reset = function(a, b, c, d, e) {
        this.oc = "number" == typeof e ? e : Jb++;
        this.Hd = d || y();
        this.Jc = a;
        this.Pi = b;
        this.Li = c;
        delete this.Eg
    };
    Ib.prototype.Ah = function(a) {
        this.Jc = a
    };
    var Kb = function(a) {
            this.Mc = a;
            this.Wd = this.sg = this.Jc = this.Pa = null
        },
        Lb = function(a, b) {
            this.name = a;
            this.value = b
        };
    Lb.prototype.toString = function() {
        return this.name
    };
    var Mb = new Lb("SHOUT", 1200),
        Nb = new Lb("SEVERE", 1E3),
        Ob = new Lb("WARNING", 900),
        Pb = new Lb("INFO", 800),
        Qb = new Lb("CONFIG", 700),
        Rb = new Lb("FINE", 500),
        Sb = [new Lb("OFF", Infinity), Mb, Nb, Ob, Pb, Qb, Rb, new Lb("FINER", 400), new Lb("FINEST", 300), new Lb("ALL", 0)],
        Tb = null,
        Ub = function(a) {
            if (!Tb) {
                Tb = {};
                for (var b = 0, c; c = Sb[b]; b++) Tb[c.value] = c, Tb[c.name] = c
            }
            if (a in Tb) return Tb[a];
            for (b = 0; b < Sb.length; ++b)
                if (c = Sb[b], c.value <= a) return c;
            return null
        };
    Kb.prototype.getName = function() {
        return this.Mc
    };
    Kb.prototype.getParent = function() {
        return this.Pa
    };
    Kb.prototype.Ah = function(a) {
        this.Jc = a
    };
    var Vb = function(a) {
        if (a.Jc) return a.Jc;
        if (a.Pa) return Vb(a.Pa);
        Ia("Root logger has no level set.");
        return null
    };
    Kb.prototype.log = function(a, b, c) {
        if (a.value >= Vb(this).value)
            for (sa(b) && (b = b()), a = new Ib(a, String(b), this.Mc), c && (a.Eg = c), c = "log:" + a.Pi, t.console && (t.console.timeStamp ? t.console.timeStamp(c) : t.console.markTimeline && t.console.markTimeline(c)), t.msWriteProfilerMark && t.msWriteProfilerMark(c), c = this; c;) {
                var d = c,
                    e = a;
                if (d.Wd)
                    for (var f = 0; b = d.Wd[f]; f++) b(e);
                c = c.getParent()
            }
    };
    Kb.prototype.info = function(a, b) {
        this.log(Pb, a, b)
    };
    var Wb = {},
        Xb = null,
        Yb = function() {
            Xb || (Xb = new Kb(""), Wb[""] = Xb, Xb.Ah(Qb))
        },
        Zb = function() {
            Yb();
            return Xb
        },
        B = function(a) {
            Yb();
            var b;
            if (!(b = Wb[a])) {
                b = new Kb(a);
                var c = a.lastIndexOf("."),
                    d = a.substr(c + 1);
                c = B(a.substr(0, c));
                c.sg || (c.sg = {});
                c.sg[d] = b;
                b.Pa = c;
                Wb[a] = b
            }
            return b
        };
    var C = function() {
        this.Rd = this.Rd;
        this.Hf = this.Hf
    };
    C.prototype.Rd = !1;
    C.prototype.M = function() {
        this.Rd || (this.Rd = !0, this.G())
    };
    C.prototype.G = function() {
        if (this.Hf)
            for (; this.Hf.length;) this.Hf.shift()()
    };
    var $b;
    ($b = !vb) || ($b = 9 <= Number(Gb));
    var ac = $b,
        bc = vb && !Fb("9");
    !yb || Fb("528");
    xb && Fb("1.9b") || vb && Fb("8") || ub && Fb("9.5") || yb && Fb("528");
    xb && !Fb("8") || vb && Fb("9");
    var cc = function() {
        if (!t.addEventListener || !Object.defineProperty) return !1;
        var a = !1,
            b = Object.defineProperty({}, "passive", {
                get: function() {
                    a = !0
                }
            });
        t.addEventListener("test", la, b);
        t.removeEventListener("test", la, b);
        return a
    }();
    var dc = function(a, b) {
        this.type = a;
        this.currentTarget = this.target = b;
        this.defaultPrevented = this.Pc = !1;
        this.tj = !0
    };
    dc.prototype.stopPropagation = function() {
        this.Pc = !0
    };
    dc.prototype.preventDefault = function() {
        this.defaultPrevented = !0;
        this.tj = !1
    };
    var ec = function(a, b) {
        dc.call(this, a ? a.type : "");
        this.relatedTarget = this.currentTarget = this.target = null;
        this.button = this.screenY = this.screenX = this.clientY = this.clientX = this.offsetY = this.offsetX = 0;
        this.key = "";
        this.charCode = this.keyCode = 0;
        this.metaKey = this.shiftKey = this.altKey = this.ctrlKey = !1;
        this.Bc = this.state = null;
        a && this.da(a, b)
    };
    ya(ec, dc);
    ec.prototype.da = function(a, b) {
        var c = this.type = a.type,
            d = a.changedTouches ? a.changedTouches[0] : null;
        this.target = a.target || a.srcElement;
        this.currentTarget = b;
        if (b = a.relatedTarget) {
            if (xb) {
                a: {
                    try {
                        rb(b.nodeName);
                        var e = !0;
                        break a
                    } catch (f) {}
                    e = !1
                }
                e || (b = null)
            }
        } else "mouseover" == c ? b = a.fromElement : "mouseout" == c && (b = a.toElement);
        this.relatedTarget = b;
        null === d ? (this.offsetX = yb || void 0 !== a.offsetX ? a.offsetX : a.layerX, this.offsetY = yb || void 0 !== a.offsetY ? a.offsetY : a.layerY, this.clientX = void 0 !== a.clientX ? a.clientX : a.pageX,
            this.clientY = void 0 !== a.clientY ? a.clientY : a.pageY, this.screenX = a.screenX || 0, this.screenY = a.screenY || 0) : (this.clientX = void 0 !== d.clientX ? d.clientX : d.pageX, this.clientY = void 0 !== d.clientY ? d.clientY : d.pageY, this.screenX = d.screenX || 0, this.screenY = d.screenY || 0);
        this.button = a.button;
        this.keyCode = a.keyCode || 0;
        this.key = a.key || "";
        this.charCode = a.charCode || ("keypress" == c ? a.keyCode : 0);
        this.ctrlKey = a.ctrlKey;
        this.altKey = a.altKey;
        this.shiftKey = a.shiftKey;
        this.metaKey = a.metaKey;
        this.state = a.state;
        this.Bc = a;
        a.defaultPrevented &&
            this.preventDefault()
    };
    ec.prototype.stopPropagation = function() {
        ec.Le.stopPropagation.call(this);
        this.Bc.stopPropagation ? this.Bc.stopPropagation() : this.Bc.cancelBubble = !0
    };
    ec.prototype.preventDefault = function() {
        ec.Le.preventDefault.call(this);
        var a = this.Bc;
        if (a.preventDefault) a.preventDefault();
        else if (a.returnValue = !1, bc) try {
            if (a.ctrlKey || 112 <= a.keyCode && 123 >= a.keyCode) a.keyCode = -1
        } catch (b) {}
    };
    var fc = "closure_listenable_" + (1E6 * Math.random() | 0),
        gc = 0;
    var hc = function(a, b, c, d, e) {
            this.listener = a;
            this.Lf = null;
            this.src = b;
            this.type = c;
            this.capture = !!d;
            this.qf = e;
            this.key = ++gc;
            this.Ad = this.df = !1
        },
        ic = function(a) {
            a.Ad = !0;
            a.listener = null;
            a.Lf = null;
            a.src = null;
            a.qf = null
        };
    var jc = function(a) {
        this.src = a;
        this.ma = {};
        this.Se = 0
    };
    jc.prototype.add = function(a, b, c, d, e) {
        var f = a.toString();
        a = this.ma[f];
        a || (a = this.ma[f] = [], this.Se++);
        var g = kc(a, b, d, e); - 1 < g ? (b = a[g], c || (b.df = !1)) : (b = new hc(b, this.src, f, !!d, e), b.df = c, a.push(b));
        return b
    };
    jc.prototype.remove = function(a, b, c, d) {
        a = a.toString();
        if (!(a in this.ma)) return !1;
        var e = this.ma[a];
        b = kc(e, b, c, d);
        return -1 < b ? (ic(e[b]), Array.prototype.splice.call(e, b, 1), 0 == e.length && (delete this.ma[a], this.Se--), !0) : !1
    };
    var lc = function(a, b) {
        var c = b.type;
        c in a.ma && Sa(a.ma[c], b) && (ic(b), 0 == a.ma[c].length && (delete a.ma[c], a.Se--))
    };
    jc.prototype.Jg = function(a, b, c, d) {
        a = this.ma[a.toString()];
        var e = -1;
        a && (e = kc(a, b, c, d));
        return -1 < e ? a[e] : null
    };
    jc.prototype.hasListener = function(a, b) {
        var c = u(a),
            d = c ? a.toString() : "",
            e = u(b);
        return $a(this.ma, function(a) {
            for (var f = 0; f < a.length; ++f)
                if (!(c && a[f].type != d || e && a[f].capture != b)) return !0;
            return !1
        })
    };
    var kc = function(a, b, c, d) {
        for (var e = 0; e < a.length; ++e) {
            var f = a[e];
            if (!f.Ad && f.listener == b && f.capture == !!c && f.qf == d) return e
        }
        return -1
    };
    var mc = "closure_lm_" + (1E6 * Math.random() | 0),
        nc = {},
        oc = 0,
        D = function(a, b, c, d, e) {
            if (d && d.once) pc(a, b, c, d, e);
            else if (oa(b))
                for (var f = 0; f < b.length; f++) D(a, b[f], c, d, e);
            else c = qc(c), a && a[fc] ? a.fb.add(String(b), c, !1, ta(d) ? !!d.capture : !!d, e) : rc(a, b, c, !1, d, e)
        },
        rc = function(a, b, c, d, e, f) {
            if (!b) throw Error("Invalid event type");
            var g = ta(e) ? !!e.capture : !!e,
                k = sc(a);
            k || (a[mc] = k = new jc(a));
            c = k.add(b, c, d, g, f);
            if (!c.Lf) {
                d = tc();
                c.Lf = d;
                d.src = a;
                d.listener = c;
                if (a.addEventListener) cc || (e = g), void 0 === e && (e = !1), a.addEventListener(b.toString(),
                    d, e);
                else if (a.attachEvent) a.attachEvent(uc(b.toString()), d);
                else throw Error("addEventListener and attachEvent are unavailable.");
                oc++
            }
        },
        tc = function() {
            var a = vc,
                b = ac ? function(c) {
                    return a.call(b.src, b.listener, c)
                } : function(c) {
                    c = a.call(b.src, b.listener, c);
                    if (!c) return c
                };
            return b
        },
        pc = function(a, b, c, d, e) {
            if (oa(b))
                for (var f = 0; f < b.length; f++) pc(a, b[f], c, d, e);
            else c = qc(c), a && a[fc] ? a.fb.add(String(b), c, !0, ta(d) ? !!d.capture : !!d, e) : rc(a, b, c, !0, d, e)
        },
        wc = function(a, b, c, d, e) {
            if (oa(b))
                for (var f = 0; f < b.length; f++) wc(a,
                    b[f], c, d, e);
            else d = ta(d) ? !!d.capture : !!d, c = qc(c), a && a[fc] ? a.fb.remove(String(b), c, d, e) : a && (a = sc(a)) && (b = a.Jg(b, c, d, e)) && xc(b)
        },
        xc = function(a) {
            if (!x(a) && a && !a.Ad) {
                var b = a.src;
                if (b && b[fc]) lc(b.fb, a);
                else {
                    var c = a.type,
                        d = a.Lf;
                    b.removeEventListener ? b.removeEventListener(c, d, a.capture) : b.detachEvent && b.detachEvent(uc(c), d);
                    oc--;
                    (c = sc(b)) ? (lc(c, a), 0 == c.Se && (c.src = null, b[mc] = null)) : ic(a)
                }
            }
        },
        uc = function(a) {
            return a in nc ? nc[a] : nc[a] = "on" + a
        },
        zc = function(a, b, c, d) {
            var e = !0;
            if (a = sc(a))
                if (b = a.ma[b.toString()])
                    for (b =
                        b.concat(), a = 0; a < b.length; a++) {
                        var f = b[a];
                        f && f.capture == c && !f.Ad && (f = yc(f, d), e = e && !1 !== f)
                    }
            return e
        },
        yc = function(a, b) {
            var c = a.listener,
                d = a.qf || a.src;
            a.df && xc(a);
            return c.call(d, b)
        },
        vc = function(a, b) {
            if (a.Ad) return !0;
            if (!ac) {
                if (!b) a: {
                    b = ["window", "event"];
                    for (var c = t, d; d = b.shift();)
                        if (null != c[d]) c = c[d];
                        else {
                            b = null;
                            break a
                        }
                    b = c
                }
                d = b;
                b = new ec(d, this);
                c = !0;
                if (!(0 > d.keyCode || void 0 != d.returnValue)) {
                    a: {
                        var e = !1;
                        if (0 == d.keyCode) try {
                            d.keyCode = -1;
                            break a
                        } catch (g) {
                            e = !0
                        }
                        if (e || void 0 == d.returnValue) d.returnValue = !0
                    }
                    d = [];
                    for (e = b.currentTarget; e; e = e.parentNode) d.push(e);e = a.type;
                    for (var f = d.length - 1; !b.Pc && 0 <= f; f--) b.currentTarget = d[f],
                    a = zc(d[f], e, !0, b),
                    c = c && a;
                    for (f = 0; !b.Pc && f < d.length; f++) b.currentTarget = d[f],
                    a = zc(d[f], e, !1, b),
                    c = c && a
                }
                return c
            }
            return yc(a, new ec(b, this))
        },
        sc = function(a) {
            a = a[mc];
            return a instanceof jc ? a : null
        },
        Ac = "__closure_events_fn_" + (1E9 * Math.random() >>> 0),
        qc = function(a) {
            if (sa(a)) return a;
            a[Ac] || (a[Ac] = function(b) {
                return a.handleEvent(b)
            });
            return a[Ac]
        };
    var Bc = function() {
        C.call(this);
        this.fb = new jc(this);
        this.Mj = this;
        this.lh = null
    };
    ya(Bc, C);
    Bc.prototype[fc] = !0;
    Bc.prototype.addEventListener = function(a, b, c, d) {
        D(this, a, b, c, d)
    };
    Bc.prototype.removeEventListener = function(a, b, c, d) {
        wc(this, a, b, c, d)
    };
    Bc.prototype.dispatchEvent = function(a) {
        var b, c = this.lh;
        if (c)
            for (b = []; c; c = c.lh) b.push(c);
        c = this.Mj;
        var d = a.type || a;
        if (w(a)) a = new dc(a, c);
        else if (a instanceof dc) a.target = a.target || c;
        else {
            var e = a;
            a = new dc(d, c);
            db(a, e)
        }
        e = !0;
        if (b)
            for (var f = b.length - 1; !a.Pc && 0 <= f; f--) {
                var g = a.currentTarget = b[f];
                e = Cc(g, d, !0, a) && e
            }
        a.Pc || (g = a.currentTarget = c, e = Cc(g, d, !0, a) && e, a.Pc || (e = Cc(g, d, !1, a) && e));
        if (b)
            for (f = 0; !a.Pc && f < b.length; f++) g = a.currentTarget = b[f], e = Cc(g, d, !1, a) && e;
        return e
    };
    Bc.prototype.G = function() {
        Bc.Le.G.call(this);
        if (this.fb) {
            var a = this.fb,
                b = 0,
                c;
            for (c in a.ma) {
                for (var d = a.ma[c], e = 0; e < d.length; e++) ++b, ic(d[e]);
                delete a.ma[c];
                a.Se--
            }
        }
        this.lh = null
    };
    var Cc = function(a, b, c, d) {
        b = a.fb.ma[String(b)];
        if (!b) return !0;
        b = b.concat();
        for (var e = !0, f = 0; f < b.length; ++f) {
            var g = b[f];
            if (g && !g.Ad && g.capture == c) {
                var k = g.listener,
                    l = g.qf || g.src;
                g.df && lc(a.fb, g);
                e = !1 !== k.call(l, d) && e
            }
        }
        return e && 0 != d.tj
    };
    Bc.prototype.Jg = function(a, b, c, d) {
        return this.fb.Jg(String(a), b, c, d)
    };
    Bc.prototype.hasListener = function(a, b) {
        return this.fb.hasListener(u(a) ? String(a) : void 0, b)
    };
    var Dc = "StopIteration" in t ? t.StopIteration : {
            message: "StopIteration",
            stack: ""
        },
        Ec = function() {};
    Ec.prototype.next = function() {
        throw Dc;
    };
    Ec.prototype.Lj = function() {
        return this
    };
    var Fc = function(a) {
            return /^\s*$/.test(a) ? !1 : /^[\],:{}\s\u2028\u2029]*$/.test(a.replace(/\\["\\\/bfnrtu]/g, "@").replace(/(?:"[^"\\\n\r\u2028\u2029\x00-\x08\x0a-\x1f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)[\s\u2028\u2029]*(?=:|,|]|}|$)/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""))
        },
        Gc = function(a) {
            a = String(a);
            if (Fc(a)) try {
                return eval("(" + a + ")")
            } catch (b) {}
            throw Error("Invalid JSON string: " + a);
        };
    var Hc = function(a, b) {
            a && a.log(Nb, b, void 0)
        },
        E = function(a, b) {
            a && a.log(Ob, b, void 0)
        },
        F = function(a, b) {
            a && a.info(b, void 0)
        },
        Ic = function(a, b) {
            a && a.log(Rb, b, void 0)
        };
    var Jc = function() {};
    Jc.prototype.Uh = null;
    var Lc = function(a) {
        var b;
        (b = a.Uh) || (b = {}, Kc(a) && (b[0] = !0, b[1] = !0), b = a.Uh = b);
        return b
    };
    var Mc, Nc = function() {};
    ya(Nc, Jc);
    var Oc = function(a) {
            return (a = Kc(a)) ? new ActiveXObject(a) : new XMLHttpRequest
        },
        Kc = function(a) {
            if (!a.ti && "undefined" == typeof XMLHttpRequest && "undefined" != typeof ActiveXObject) {
                for (var b = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"], c = 0; c < b.length; c++) {
                    var d = b[c];
                    try {
                        return new ActiveXObject(d), a.ti = d
                    } catch (e) {}
                }
                throw Error("Could not create ActiveXObject. ActiveX might be disabled, or MSXML might not be installed");
            }
            return a.ti
        };
    Mc = new Nc;
    var Pc = function(a, b) {
        this.Ta = {};
        this.aa = [];
        this.Da = this.K = 0;
        var c = arguments.length;
        if (1 < c) {
            if (c % 2) throw Error("Uneven number of arguments");
            for (var d = 0; d < c; d += 2) this.set(arguments[d], arguments[d + 1])
        } else a && this.addAll(a)
    };
    h = Pc.prototype;
    h.tb = function() {
        return this.K
    };
    h.xa = function() {
        Qc(this);
        for (var a = [], b = 0; b < this.aa.length; b++) a.push(this.Ta[this.aa[b]]);
        return a
    };
    h.za = function() {
        Qc(this);
        return this.aa.concat()
    };
    h.Xb = function(a) {
        return Rc(this.Ta, a)
    };
    h.sb = function(a, b) {
        if (this === a) return !0;
        if (this.K != a.tb()) return !1;
        b = b || Sc;
        Qc(this);
        for (var c, d = 0; c = this.aa[d]; d++)
            if (!b(this.get(c), a.get(c))) return !1;
        return !0
    };
    var Sc = function(a, b) {
        return a === b
    };
    Pc.prototype.pa = function() {
        return 0 == this.K
    };
    Pc.prototype.clear = function() {
        this.Ta = {};
        this.Da = this.K = this.aa.length = 0
    };
    Pc.prototype.remove = function(a) {
        return Rc(this.Ta, a) ? (delete this.Ta[a], this.K--, this.Da++, this.aa.length > 2 * this.K && Qc(this), !0) : !1
    };
    var Qc = function(a) {
        var b, c;
        if (a.K != a.aa.length) {
            for (b = c = 0; c < a.aa.length;) {
                var d = a.aa[c];
                Rc(a.Ta, d) && (a.aa[b++] = d);
                c++
            }
            a.aa.length = b
        }
        if (a.K != a.aa.length) {
            var e = {};
            for (b = c = 0; c < a.aa.length;) d = a.aa[c], Rc(e, d) || (a.aa[b++] = d, e[d] = 1), c++;
            a.aa.length = b
        }
    };
    h = Pc.prototype;
    h.get = function(a, b) {
        return Rc(this.Ta, a) ? this.Ta[a] : b
    };
    h.set = function(a, b) {
        Rc(this.Ta, a) || (this.K++, this.aa.push(a), this.Da++);
        this.Ta[a] = b
    };
    h.addAll = function(a) {
        if (a instanceof Pc) {
            var b = a.za();
            a = a.xa()
        } else b = bb(a), a = ab(a);
        for (var c = 0; c < b.length; c++) this.set(b[c], a[c])
    };
    h.forEach = function(a, b) {
        for (var c = this.za(), d = 0; d < c.length; d++) {
            var e = c[d],
                f = this.get(e);
            a.call(b, f, e, this)
        }
    };
    h.clone = function() {
        return new Pc(this)
    };
    h.Lj = function(a) {
        Qc(this);
        var b = 0,
            c = this.Da,
            d = this,
            e = new Ec;
        e.next = function() {
            if (c != d.Da) throw Error("The map has changed since the iterator was created");
            if (b >= d.aa.length) throw Dc;
            var e = d.aa[b++];
            return a ? e : d.Ta[e]
        };
        return e
    };
    var Rc = function(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b)
    };
    var Tc = function(a) {
            if (a.xa && "function" == typeof a.xa) return a.xa();
            if (w(a)) return a.split("");
            if (ra(a)) {
                for (var b = [], c = a.length, d = 0; d < c; d++) b.push(a[d]);
                return b
            }
            return ab(a)
        },
        Uc = function(a, b, c) {
            if (a.forEach && "function" == typeof a.forEach) a.forEach(b, c);
            else if (ra(a) || w(a)) Ma(a, b, c);
            else {
                if (a.za && "function" == typeof a.za) var d = a.za();
                else if (a.xa && "function" == typeof a.xa) d = void 0;
                else if (ra(a) || w(a)) {
                    d = [];
                    for (var e = a.length, f = 0; f < e; f++) d.push(f)
                } else d = bb(a);
                e = Tc(a);
                f = e.length;
                for (var g = 0; g < f; g++) b.call(c,
                    e[g], d && d[g], a)
            }
        };
    var Xc = function(a, b) {
            this.g = 0;
            this.sj = void 0;
            this.ed = this.Vb = this.Pa = null;
            this.nf = this.Fg = !1;
            if (a != la) try {
                var c = this;
                a.call(b, function(a) {
                    Vc(c, 2, a)
                }, function(a) {
                    if (!(a instanceof Wc)) try {
                        if (a instanceof Error) throw a;
                        throw Error("Promise rejected.");
                    } catch (e) {}
                    Vc(c, 3, a)
                })
            } catch (d) {
                Vc(this, 3, d)
            }
        },
        Yc = function() {
            this.next = this.context = this.sd = this.oe = this.zc = null;
            this.Ye = !1
        };
    Yc.prototype.reset = function() {
        this.context = this.sd = this.oe = this.zc = null;
        this.Ye = !1
    };
    var Zc = new Va(function() {
            return new Yc
        }, function(a) {
            a.reset()
        }, 100),
        $c = function(a, b, c) {
            var d = Zc.get();
            d.oe = a;
            d.sd = b;
            d.context = c;
            return d
        },
        ad = function(a) {
            return new Xc(function(b, c) {
                c(a)
            })
        },
        cd = function() {
            var a, b, c = new Xc(function(c, e) {
                a = c;
                b = e
            });
            return new bd(c, a, b)
        };
    Xc.prototype.then = function(a, b, c) {
        return dd(this, sa(a) ? a : null, sa(b) ? b : null, c)
    };
    Xc.prototype.then = Xc.prototype.then;
    Xc.prototype.$goog_Thenable = !0;
    var ed = function(a, b) {
        dd(a, null, b, void 0)
    };
    Xc.prototype.cancel = function(a) {
        0 == this.g && ob(function() {
            var b = new Wc(a);
            fd(this, b)
        }, this)
    };
    var fd = function(a, b) {
            if (0 == a.g)
                if (a.Pa) {
                    var c = a.Pa;
                    if (c.Vb) {
                        for (var d = 0, e = null, f = null, g = c.Vb; g && (g.Ye || (d++, g.zc == a && (e = g), !(e && 1 < d))); g = g.next) e || (f = g);
                        e && (0 == c.g && 1 == d ? fd(c, b) : (f ? (d = f, d.next == c.ed && (c.ed = d), d.next = d.next.next) : gd(c), hd(c, e, 3, b)))
                    }
                    a.Pa = null
                } else Vc(a, 3, b)
        },
        jd = function(a, b) {
            a.Vb || 2 != a.g && 3 != a.g || id(a);
            a.ed ? a.ed.next = b : a.Vb = b;
            a.ed = b
        },
        dd = function(a, b, c, d) {
            var e = $c(null, null, null);
            e.zc = new Xc(function(a, g) {
                e.oe = b ? function(c) {
                    try {
                        var e = b.call(d, c);
                        a(e)
                    } catch (n) {
                        g(n)
                    }
                } : a;
                e.sd = c ? function(b) {
                    try {
                        var e =
                            c.call(d, b);
                        !u(e) && b instanceof Wc ? g(b) : a(e)
                    } catch (n) {
                        g(n)
                    }
                } : g
            });
            e.zc.Pa = a;
            jd(a, e);
            return e.zc
        };
    Xc.prototype.ml = function(a) {
        this.g = 0;
        Vc(this, 2, a)
    };
    Xc.prototype.nl = function(a) {
        this.g = 0;
        Vc(this, 3, a)
    };
    var Vc = function(a, b, c) {
            if (0 == a.g) {
                a === c && (b = 3, c = new TypeError("Promise cannot resolve to itself"));
                a.g = 1;
                a: {
                    var d = c,
                        e = a.ml,
                        f = a.nl;
                    if (d instanceof Xc) {
                        jd(d, $c(e || la, f || null, a));
                        var g = !0
                    } else {
                        if (d) try {
                            var k = !!d.$goog_Thenable
                        } catch (n) {
                            k = !1
                        } else k = !1;
                        if (k) d.then(e, f, a), g = !0;
                        else {
                            if (ta(d)) try {
                                var l = d.then;
                                if (sa(l)) {
                                    kd(d, l, e, f, a);
                                    g = !0;
                                    break a
                                }
                            } catch (n) {
                                f.call(a, n);
                                g = !0;
                                break a
                            }
                            g = !1
                        }
                    }
                }
                g || (a.sj = c, a.g = b, a.Pa = null, id(a), 3 != b || c instanceof Wc || ld(a, c))
            }
        },
        kd = function(a, b, c, d, e) {
            var f = !1,
                g = function(a) {
                    f || (f = !0, c.call(e, a))
                },
                k = function(a) {
                    f || (f = !0, d.call(e, a))
                };
            try {
                b.call(a, g, k)
            } catch (l) {
                k(l)
            }
        },
        id = function(a) {
            a.Fg || (a.Fg = !0, ob(a.jk, a))
        },
        gd = function(a) {
            var b = null;
            a.Vb && (b = a.Vb, a.Vb = b.next, b.next = null);
            a.Vb || (a.ed = null);
            return b
        };
    Xc.prototype.jk = function() {
        for (var a; a = gd(this);) hd(this, a, this.g, this.sj);
        this.Fg = !1
    };
    var hd = function(a, b, c, d) {
            if (3 == c && b.sd && !b.Ye)
                for (; a && a.nf; a = a.Pa) a.nf = !1;
            if (b.zc) b.zc.Pa = null, md(b, c, d);
            else try {
                b.Ye ? b.oe.call(b.context) : md(b, c, d)
            } catch (e) {
                nd.call(null, e)
            }
            Zc.put(b)
        },
        md = function(a, b, c) {
            2 == b ? a.oe.call(a.context, c) : a.sd && a.sd.call(a.context, c)
        },
        ld = function(a, b) {
            a.nf = !0;
            ob(function() {
                a.nf && nd.call(null, b)
            })
        },
        nd = eb,
        Wc = function(a) {
            Ca.call(this, a)
        };
    ya(Wc, Ca);
    Wc.prototype.name = "cancel";
    var bd = function(a, b, c) {
        this.mj = a;
        this.resolve = b;
        this.reject = c
    };
    var od = function(a, b, c) {
        if (sa(a)) c && (a = wa(a, c));
        else if (a && "function" == typeof a.handleEvent) a = wa(a.handleEvent, a);
        else throw Error("Invalid listener argument");
        return 2147483647 < Number(b) ? -1 : t.setTimeout(a, b || 0)
    };
    var pd = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/,
        qd = function(a, b) {
            if (a) {
                a = a.split("&");
                for (var c = 0; c < a.length; c++) {
                    var d = a[c].indexOf("="),
                        e = null;
                    if (0 <= d) {
                        var f = a[c].substring(0, d);
                        e = a[c].substring(d + 1)
                    } else f = a[c];
                    b(f, e ? decodeURIComponent(e.replace(/\+/g, " ")) : "")
                }
            }
        };
    var G = function(a) {
        Bc.call(this);
        this.headers = new Pc;
        this.ig = a || null;
        this.Tb = !1;
        this.hg = this.s = null;
        this.Ji = this.ke = "";
        this.zb = 0;
        this.je = "";
        this.Dc = this.Og = this.tf = this.Bg = !1;
        this.Ob = 0;
        this.fa = null;
        this.Je = "";
        this.dg = this.Vk = this.Kd = !1
    };
    ya(G, Bc);
    G.prototype.na = B("goog.net.XhrIo");
    var rd = /^https?$/i,
        sd = ["POST", "PUT"],
        td = function(a) {
            a.Je = "arraybuffer"
        };
    G.prototype.send = function(a, b, c, d) {
        if (this.s) throw Error("[goog.net.XhrIo] Object is active with another request=" + this.ke + "; newUri=" + a);
        b = b ? b.toUpperCase() : "GET";
        this.ke = a;
        this.je = "";
        this.zb = 0;
        this.Ji = b;
        this.Bg = !1;
        this.Tb = !0;
        this.s = this.xg();
        this.hg = this.ig ? Lc(this.ig) : Lc(Mc);
        this.s.onreadystatechange = wa(this.bj, this);
        this.Vk && "onprogress" in this.s && (this.s.onprogress = wa(function(a) {
            this.aj(a, !0)
        }, this), this.s.upload && (this.s.upload.onprogress = wa(this.aj, this)));
        try {
            Ic(this.na, ud(this, "Opening Xhr")),
                this.Og = !0, this.s.open(b, String(a), !0), this.Og = !1
        } catch (f) {
            Ic(this.na, ud(this, "Error opening Xhr: " + f.message));
            vd(this, f);
            return
        }
        a = c || "";
        var e = this.headers.clone();
        d && Uc(d, function(a, b) {
            e.set(b, a)
        });
        d = Qa(e.za());
        c = t.FormData && a instanceof t.FormData;
        !(0 <= Ka(sd, b)) || d || c || e.set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        e.forEach(function(a, b) {
            this.s.setRequestHeader(b, a)
        }, this);
        this.Je && (this.s.responseType = this.Je);
        "withCredentials" in this.s && this.s.withCredentials !== this.Kd &&
            (this.s.withCredentials = this.Kd);
        try {
            wd(this), 0 < this.Ob && (this.dg = xd(this.s), Ic(this.na, ud(this, "Will abort after " + this.Ob + "ms if incomplete, xhr2 " + this.dg)), this.dg ? (this.s.timeout = this.Ob, this.s.ontimeout = wa(this.Fj, this)) : this.fa = od(this.Fj, this.Ob, this)), Ic(this.na, ud(this, "Sending request")), this.tf = !0, this.s.send(a), this.tf = !1
        } catch (f) {
            Ic(this.na, ud(this, "Send error: " + f.message)), vd(this, f)
        }
    };
    var xd = function(a) {
            return vb && Fb(9) && x(a.timeout) && u(a.ontimeout)
        },
        Pa = function(a) {
            return "content-type" == a.toLowerCase()
        };
    G.prototype.xg = function() {
        return this.ig ? Oc(this.ig) : Oc(Mc)
    };
    G.prototype.Fj = function() {
        "undefined" != typeof ka && this.s && (this.je = "Timed out after " + this.Ob + "ms, aborting", this.zb = 8, Ic(this.na, ud(this, this.je)), this.dispatchEvent("timeout"), this.abort(8))
    };
    var vd = function(a, b) {
            a.Tb = !1;
            a.s && (a.Dc = !0, a.s.abort(), a.Dc = !1);
            a.je = b;
            a.zb = 5;
            yd(a);
            zd(a)
        },
        yd = function(a) {
            a.Bg || (a.Bg = !0, a.dispatchEvent("complete"), a.dispatchEvent("error"))
        };
    G.prototype.abort = function(a) {
        this.s && this.Tb && (Ic(this.na, ud(this, "Aborting")), this.Tb = !1, this.Dc = !0, this.s.abort(), this.Dc = !1, this.zb = a || 7, this.dispatchEvent("complete"), this.dispatchEvent("abort"), zd(this))
    };
    G.prototype.G = function() {
        this.s && (this.Tb && (this.Tb = !1, this.Dc = !0, this.s.abort(), this.Dc = !1), zd(this, !0));
        G.Le.G.call(this)
    };
    G.prototype.bj = function() {
        this.Rd || (this.Og || this.tf || this.Dc ? Ad(this) : this.Jk())
    };
    G.prototype.Jk = function() {
        Ad(this)
    };
    var Ad = function(a) {
        if (a.Tb && "undefined" != typeof ka)
            if (a.hg[1] && 4 == Bd(a) && 2 == Cd(a)) Ic(a.na, ud(a, "Local request error detected and ignored"));
            else if (a.tf && 4 == Bd(a)) od(a.bj, 0, a);
        else if (a.dispatchEvent("readystatechange"), 4 == Bd(a)) {
            Ic(a.na, ud(a, "Request complete"));
            a.Tb = !1;
            try {
                var b = Cd(a);
                a: switch (b) {
                    case 200:
                    case 201:
                    case 202:
                    case 204:
                    case 206:
                    case 304:
                    case 1223:
                        var c = !0;
                        break a;
                    default:
                        c = !1
                }
                var d;
                if (!(d = c)) {
                    var e;
                    if (e = 0 === b) {
                        var f = String(a.ke).match(pd)[1] || null;
                        if (!f && t.self && t.self.location) {
                            var g =
                                t.self.location.protocol;
                            f = g.substr(0, g.length - 1)
                        }
                        e = !rd.test(f ? f.toLowerCase() : "")
                    }
                    d = e
                }
                if (d) a.dispatchEvent("complete"), a.dispatchEvent("success");
                else {
                    a.zb = 6;
                    try {
                        var k = 2 < Bd(a) ? a.s.statusText : ""
                    } catch (l) {
                        Ic(a.na, "Can not get status: " + l.message), k = ""
                    }
                    a.je = k + " [" + Cd(a) + "]";
                    yd(a)
                }
            } finally {
                zd(a)
            }
        }
    };
    G.prototype.aj = function(a, b) {
        this.dispatchEvent(Dd(a, "progress"));
        this.dispatchEvent(Dd(a, b ? "downloadprogress" : "uploadprogress"))
    };
    var Dd = function(a, b) {
            return {
                type: b,
                lengthComputable: a.lengthComputable,
                loaded: a.loaded,
                total: a.total
            }
        },
        zd = function(a, b) {
            if (a.s) {
                wd(a);
                var c = a.s,
                    d = a.hg[0] ? la : null;
                a.s = null;
                a.hg = null;
                b || a.dispatchEvent("ready");
                try {
                    c.onreadystatechange = d
                } catch (e) {
                    Hc(a.na, "Problem encountered resetting onreadystatechange: " + e.message)
                }
            }
        },
        wd = function(a) {
            a.s && a.dg && (a.s.ontimeout = null);
            x(a.fa) && (t.clearTimeout(a.fa), a.fa = null)
        };
    G.prototype.ce = function() {
        return !!this.s
    };
    var Bd = function(a) {
            return a.s ? a.s.readyState : 0
        },
        Cd = function(a) {
            try {
                return 2 < Bd(a) ? a.s.status : -1
            } catch (b) {
                return -1
            }
        },
        Ed = function(a) {
            try {
                if (!a.s) return null;
                if ("response" in a.s) return a.s.response;
                switch (a.Je) {
                    case "":
                    case "text":
                        return a.s.responseText;
                    case "arraybuffer":
                        if ("mozResponseArrayBuffer" in a.s) return a.s.mozResponseArrayBuffer
                }
                Hc(a.na, "Response type " + a.Je + " is not supported on this browser");
                return null
            } catch (b) {
                return Ic(a.na, "Can not get response: " + b.message), null
            }
        };
    G.prototype.getResponseHeader = function(a) {
        if (this.s && 4 == Bd(this)) return a = this.s.getResponseHeader(a), null === a ? void 0 : a
    };
    G.prototype.getAllResponseHeaders = function() {
        return this.s && 4 == Bd(this) ? this.s.getAllResponseHeaders() : ""
    };
    var ud = function(a, b) {
        return b + " [" + a.Ji + " " + a.ke + " " + Cd(a) + "]"
    };
    var Fd = function(a) {
            return Da(a, "audio/")
        },
        Gd = function(a) {
            return Da(a, "video/")
        },
        Hd = /dv(he|av).[s|d|p][e|t|w][n|r|h|b][a|h]?[e|t|w]?/,
        Id = function(a) {
            var b = !1;
            a = a.split(",");
            for (var c = 0; c < a.length; c++) {
                if (a[c].match(Hd)) return !1;
                0 === a[c].indexOf("hev1.2") && (b = !0)
            }
            return b
        };

    function Jd(a) {
        if (a) {
            if (Gd(a)) return "Video";
            if (Fd(a)) return "Audio"
        }
    }

    function Kd(a) {
        switch (a) {
            case "webvtt":
                return 1;
            case "ttml":
                return 2;
            case "cea608":
                return 3
        }
        return 0
    }

    function Ld(a) {
        switch (a) {
            case "mp4a.a6":
                return 1;
            case "ec-3":
                return 2;
            case "mp4a.40.2":
                return 3;
            case "mp4a.40.5":
                return 4;
            case "mp4a.67":
                return 5;
            case "avc1.4D40":
                return 6;
            case "avc1.4D401E":
                return 7;
            case "mp4a.a5":
                return 8;
            case "ac-3":
                return 9;
            case "vorbis":
                return 10
        }
        return 0 == a.lastIndexOf("avc1", 0) ? 11 : 0 == a.lastIndexOf("mp4a.40", 0) ? 12 : 0
    }

    function Md(a, b) {
        switch (b) {
            case 1:
                switch (a) {
                    case "clearkey":
                        return 101;
                    case "widevine":
                        return 102;
                    case "playready":
                        return 103
                }
                break;
            case 2:
                switch (a) {
                    case "widevine":
                        return 201;
                    case "aes_128":
                        return 202;
                    case "aes_128_ckp":
                        return 203
                }
                break;
            case 3:
                switch (a) {
                    case "playready":
                        return 301
                }
        }
        return 0
    }

    function Nd(a) {
        switch (a) {
            case "application/ttml+xml":
                return 1;
            case "text/vtt":
                return 2;
            case "text/mp4":
                return 3;
            case "audio/mp4":
                return 4;
            case "video/mp4":
                return 5;
            case "video/mp2t":
                return 6
        }
        return 0
    }
    var Od = function(a) {
            a.split(",").forEach(function(a) {
                A("Cast.MPL.Codec", Ld(a))
            })
        },
        Pd = function(a) {
            A("Cast.MPL.StreamingProtocolType", a)
        },
        Qd = function(a, b) {
            A("Cast.MPL.ProtocolProtection", Md(a, b))
        },
        Rd = function(a) {
            A("Cast.MPL.Live", a ? 1 : 0)
        };
    var Sd = function() {
            this.qj = y()
        },
        Td = null;
    Sd.prototype.set = function(a) {
        this.qj = a
    };
    Sd.prototype.reset = function() {
        this.set(y())
    };
    Sd.prototype.get = function() {
        return this.qj
    };
    var Ud = function(a) {
        this.Rk = a || "";
        Td || (Td = new Sd);
        this.kl = Td
    };
    h = Ud.prototype;
    h.Oh = !0;
    h.yj = !0;
    h.gl = !0;
    h.fl = !0;
    h.zj = !1;
    h.hl = !1;
    var Vd = function(a) {
            return 10 > a ? "0" + a : String(a)
        },
        Wd = function(a, b) {
            a = (a.Hd - b) / 1E3;
            b = a.toFixed(3);
            var c = 0;
            if (1 > a) c = 2;
            else
                for (; 100 > a;) c++, a *= 10;
            for (; 0 < c--;) b = " " + b;
            return b
        },
        Xd = function(a) {
            Ud.call(this, a)
        };
    ya(Xd, Ud);
    var Yd = function() {
        this.Xk = wa(this.Nj, this);
        this.hf = new Xd;
        this.hf.yj = !1;
        this.hf.zj = !1;
        this.vi = this.hf.Oh = !1;
        this.kk = {}
    };
    Yd.prototype.Nj = function(a) {
        if (!this.kk[a.Li]) {
            var b = this.hf;
            var c = [];
            c.push(b.Rk, " ");
            if (b.yj) {
                var d = new Date(a.Hd);
                c.push("[", Vd(d.getFullYear() - 2E3) + Vd(d.getMonth() + 1) + Vd(d.getDate()) + " " + Vd(d.getHours()) + ":" + Vd(d.getMinutes()) + ":" + Vd(d.getSeconds()) + "." + Vd(Math.floor(d.getMilliseconds() / 10)), "] ")
            }
            b.gl && c.push("[", Wd(a, b.kl.get()), "s] ");
            b.fl && c.push("[", a.Li, "] ");
            b.hl && c.push("[", a.Jc.name, "] ");
            c.push(a.Pi);
            b.zj && (d = a.Eg) && c.push("\n", d instanceof Error ? d.message : d.toString());
            b.Oh && c.push("\n");
            b = c.join("");
            if (c = Zd) switch (a.Jc) {
                case Mb:
                    $d(c, "info", b);
                    break;
                case Nb:
                    $d(c, "error", b);
                    break;
                case Ob:
                    $d(c, "warn", b);
                    break;
                default:
                    $d(c, "debug", b)
            }
        }
    };
    var ae = null,
        Zd = t.console,
        $d = function(a, b, c) {
            if (a[b]) a[b](c);
            else a.log(c)
        };
    var be = /^(-)?P(?:(\d+)Y)?(?:(\d+)M)?(?:(\d+)D)?(T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:\.\d+)?)S)?)?$/,
        ce = function(a, b, c, d, e, f) {
            w(a) ? (this.bd = "y" == a ? b : 0, this.Lc = "m" == a ? b : 0, this.Yb = "d" == a ? b : 0, this.cc = "h" == a ? b : 0, this.ic = "n" == a ? b : 0, this.nc = "s" == a ? b : 0) : (this.bd = a || 0, this.Lc = b || 0, this.Yb = c || 0, this.cc = d || 0, this.ic = e || 0, this.nc = f || 0)
        };
    ce.prototype.sb = function(a) {
        return a.bd == this.bd && a.Lc == this.Lc && a.Yb == this.Yb && a.cc == this.cc && a.ic == this.ic && a.nc == this.nc
    };
    ce.prototype.clone = function() {
        return new ce(this.bd, this.Lc, this.Yb, this.cc, this.ic, this.nc)
    };
    ce.prototype.add = function(a) {
        this.bd += a.bd;
        this.Lc += a.Lc;
        this.Yb += a.Yb;
        this.cc += a.cc;
        this.ic += a.ic;
        this.nc += a.nc
    };
    var de = function(a, b) {
        this.Gi = a;
        this.eg = b
    };
    de.prototype.getKey = function() {
        return this.Gi
    };
    de.prototype.clone = function() {
        return new de(this.Gi, this.eg)
    };
    var ee = function(a) {
        this.Oa = [];
        if (a) a: {
            if (a instanceof ee) {
                var b = a.za();
                a = a.xa();
                if (0 >= this.tb()) {
                    for (var c = this.Oa, d = 0; d < b.length; d++) c.push(new de(b[d], a[d]));
                    break a
                }
            } else b = bb(a),
            a = ab(a);
            for (d = 0; d < b.length; d++) this.insert(b[d], a[d])
        }
    };
    h = ee.prototype;
    h.insert = function(a, b) {
        var c = this.Oa;
        c.push(new de(a, b));
        a = c.length - 1;
        b = this.Oa;
        for (c = b[a]; 0 < a;) {
            var d = a - 1 >> 1;
            if (b[d].getKey() > c.getKey()) b[a] = b[d], a = d;
            else break
        }
        b[a] = c
    };
    h.remove = function() {
        var a = this.Oa,
            b = a.length,
            c = a[0];
        if (!(0 >= b)) {
            if (1 == b) Ra(a);
            else {
                a[0] = a.pop();
                a = 0;
                b = this.Oa;
                for (var d = b.length, e = b[a]; a < d >> 1;) {
                    var f = 2 * a + 1,
                        g = 2 * a + 2;
                    f = g < d && b[g].getKey() < b[f].getKey() ? g : f;
                    if (b[f].getKey() > e.getKey()) break;
                    b[a] = b[f];
                    a = f
                }
                b[a] = e
            }
            return c.eg
        }
    };
    h.oh = function() {
        var a = this.Oa;
        if (0 != a.length) return a[0].eg
    };
    h.xa = function() {
        for (var a = this.Oa, b = [], c = a.length, d = 0; d < c; d++) b.push(a[d].eg);
        return b
    };
    h.za = function() {
        for (var a = this.Oa, b = [], c = a.length, d = 0; d < c; d++) b.push(a[d].getKey());
        return b
    };
    h.Xb = function(a) {
        return Oa(this.Oa, function(b) {
            return b.getKey() == a
        })
    };
    h.clone = function() {
        return new ee(this)
    };
    h.tb = function() {
        return this.Oa.length
    };
    h.pa = function() {
        return 0 == this.Oa.length
    };
    h.clear = function() {
        Ra(this.Oa)
    };
    var fe = function() {
        ee.call(this)
    };
    ya(fe, ee);
    fe.prototype.enqueue = function(a, b) {
        this.insert(a, b)
    };
    fe.prototype.jd = function() {
        return this.remove()
    };
    var ge = function() {
            this.Ia = [];
            this.Ya = []
        },
        he = function(a) {
            0 == a.Ia.length && (a.Ia = a.Ya, a.Ia.reverse(), a.Ya = [])
        };
    h = ge.prototype;
    h.enqueue = function(a) {
        this.Ya.push(a)
    };
    h.jd = function() {
        he(this);
        return this.Ia.pop()
    };
    h.oh = function() {
        he(this);
        var a = this.Ia;
        return a[a.length - 1]
    };
    h.tb = function() {
        return this.Ia.length + this.Ya.length
    };
    h.pa = function() {
        return 0 == this.Ia.length && 0 == this.Ya.length
    };
    h.clear = function() {
        this.Ia = [];
        this.Ya = []
    };
    h.contains = function(a) {
        return 0 <= Ka(this.Ia, a) || 0 <= Ka(this.Ya, a)
    };
    h.remove = function(a) {
        var b = this.Ia;
        var c = La(b, a);
        0 <= c ? (Array.prototype.splice.call(b, c, 1), b = !0) : b = !1;
        return b || Sa(this.Ya, a)
    };
    h.xa = function() {
        for (var a = [], b = this.Ia.length - 1; 0 <= b; --b) a.push(this.Ia[b]);
        var c = this.Ya.length;
        for (b = 0; b < c; ++b) a.push(this.Ya[b]);
        return a
    };
    var I = function(a, b) {
        this.rb = this.$c = this.Fb = "";
        this.xd = null;
        this.Cc = this.jb = "";
        this.Ja = this.xk = !1;
        if (a instanceof I) {
            this.Ja = u(b) ? b : a.Ja;
            ie(this, a.Fb);
            var c = a.$c;
            J(this);
            this.$c = c;
            c = a.rb;
            J(this);
            this.rb = c;
            je(this, a.xd);
            c = a.jb;
            J(this);
            this.jb = c;
            ke(this, a.lb.clone());
            a = a.Cc;
            J(this);
            this.Cc = a
        } else a && (c = String(a).match(pd)) ? (this.Ja = !!b, ie(this, c[1] || "", !0), a = c[2] || "", J(this), this.$c = le(a), a = c[3] || "", J(this), this.rb = le(a, !0), je(this, c[4]), a = c[5] || "", J(this), this.jb = le(a, !0), ke(this, c[6] || "", !0), a = c[7] ||
            "", J(this), this.Cc = le(a)) : (this.Ja = !!b, this.lb = new me(null, 0, this.Ja))
    };
    I.prototype.toString = function() {
        var a = [],
            b = this.Fb;
        b && a.push(ne(b, oe, !0), ":");
        var c = this.rb;
        if (c || "file" == b) a.push("//"), (b = this.$c) && a.push(ne(b, oe, !0), "@"), a.push(encodeURIComponent(String(c)).replace(/%25([0-9a-fA-F]{2})/g, "%$1")), c = this.xd, null != c && a.push(":", String(c));
        if (c = this.jb) this.rb && "/" != c.charAt(0) && a.push("/"), a.push(ne(c, "/" == c.charAt(0) ? pe : qe, !0));
        (c = this.lb.toString()) && a.push("?", c);
        (c = this.Cc) && a.push("#", ne(c, re));
        return a.join("")
    };
    I.prototype.resolve = function(a) {
        var b = this.clone(),
            c = !!a.Fb;
        c ? ie(b, a.Fb) : c = !!a.$c;
        if (c) {
            var d = a.$c;
            J(b);
            b.$c = d
        } else c = !!a.rb;
        c ? (d = a.rb, J(b), b.rb = d) : c = null != a.xd;
        d = a.jb;
        if (c) je(b, a.xd);
        else if (c = !!a.jb) {
            if ("/" != d.charAt(0))
                if (this.rb && !this.jb) d = "/" + d;
                else {
                    var e = b.jb.lastIndexOf("/"); - 1 != e && (d = b.jb.substr(0, e + 1) + d)
                }
            e = d;
            if (".." == e || "." == e) d = "";
            else if (-1 != e.indexOf("./") || -1 != e.indexOf("/.")) {
                d = 0 == e.lastIndexOf("/", 0);
                e = e.split("/");
                for (var f = [], g = 0; g < e.length;) {
                    var k = e[g++];
                    "." == k ? d && g == e.length &&
                        f.push("") : ".." == k ? ((1 < f.length || 1 == f.length && "" != f[0]) && f.pop(), d && g == e.length && f.push("")) : (f.push(k), d = !0)
                }
                d = f.join("/")
            } else d = e
        }
        c ? (J(b), b.jb = d) : c = "" !== a.lb.toString();
        c ? ke(b, a.lb.clone()) : c = !!a.Cc;
        c && (a = a.Cc, J(b), b.Cc = a);
        return b
    };
    I.prototype.clone = function() {
        return new I(this)
    };
    var ie = function(a, b, c) {
            J(a);
            a.Fb = c ? le(b, !0) : b;
            a.Fb && (a.Fb = a.Fb.replace(/:$/, ""))
        },
        je = function(a, b) {
            J(a);
            if (b) {
                b = Number(b);
                if (isNaN(b) || 0 > b) throw Error("Bad port number " + b);
                a.xd = b
            } else a.xd = null
        },
        ke = function(a, b, c) {
            J(a);
            b instanceof me ? (a.lb = b, a.lb.zh(a.Ja)) : (c || (b = ne(b, se)), a.lb = new me(b, 0, a.Ja))
        };
    I.prototype.removeParameter = function(a) {
        J(this);
        this.lb.remove(a);
        return this
    };
    var J = function(a) {
        if (a.xk) throw Error("Tried to modify a read-only Uri");
    };
    I.prototype.zh = function(a) {
        this.Ja = a;
        this.lb && this.lb.zh(a);
        return this
    };
    var te = function(a) {
            return a instanceof I ? a.clone() : new I(a, void 0)
        },
        le = function(a, b) {
            return a ? b ? decodeURI(a.replace(/%25/g, "%2525")) : decodeURIComponent(a) : ""
        },
        ne = function(a, b, c) {
            return w(a) ? (a = encodeURI(a).replace(b, ue), c && (a = a.replace(/%25([0-9a-fA-F]{2})/g, "%$1")), a) : null
        },
        ue = function(a) {
            a = a.charCodeAt(0);
            return "%" + (a >> 4 & 15).toString(16) + (a & 15).toString(16)
        },
        oe = /[#\/\?@]/g,
        qe = /[\#\?:]/g,
        pe = /[\#\?]/g,
        se = /[\#\?@]/g,
        re = /#/g,
        me = function(a, b, c) {
            this.K = this.T = null;
            this.ya = a || null;
            this.Ja = !!c
        },
        ve = function(a) {
            a.T ||
                (a.T = new Pc, a.K = 0, a.ya && qd(a.ya, function(b, c) {
                    a.add(decodeURIComponent(b.replace(/\+/g, " ")), c)
                }))
        };
    h = me.prototype;
    h.tb = function() {
        ve(this);
        return this.K
    };
    h.add = function(a, b) {
        ve(this);
        this.ya = null;
        a = we(this, a);
        var c = this.T.get(a);
        c || this.T.set(a, c = []);
        c.push(b);
        this.K += 1;
        return this
    };
    h.remove = function(a) {
        ve(this);
        a = we(this, a);
        return this.T.Xb(a) ? (this.ya = null, this.K -= this.T.get(a).length, this.T.remove(a)) : !1
    };
    h.clear = function() {
        this.T = this.ya = null;
        this.K = 0
    };
    h.pa = function() {
        ve(this);
        return 0 == this.K
    };
    h.Xb = function(a) {
        ve(this);
        a = we(this, a);
        return this.T.Xb(a)
    };
    h.forEach = function(a, b) {
        ve(this);
        this.T.forEach(function(c, d) {
            Ma(c, function(c) {
                a.call(b, c, d, this)
            }, this)
        }, this)
    };
    h.za = function() {
        ve(this);
        for (var a = this.T.xa(), b = this.T.za(), c = [], d = 0; d < b.length; d++)
            for (var e = a[d], f = 0; f < e.length; f++) c.push(b[d]);
        return c
    };
    h.xa = function(a) {
        ve(this);
        var b = [];
        if (w(a)) this.Xb(a) && (b = Ta(b, this.T.get(we(this, a))));
        else {
            a = this.T.xa();
            for (var c = 0; c < a.length; c++) b = Ta(b, a[c])
        }
        return b
    };
    h.set = function(a, b) {
        ve(this);
        this.ya = null;
        a = we(this, a);
        this.Xb(a) && (this.K -= this.T.get(a).length);
        this.T.set(a, [b]);
        this.K += 1;
        return this
    };
    h.get = function(a, b) {
        a = a ? this.xa(a) : [];
        return 0 < a.length ? String(a[0]) : b
    };
    h.toString = function() {
        if (this.ya) return this.ya;
        if (!this.T) return "";
        for (var a = [], b = this.T.za(), c = 0; c < b.length; c++) {
            var d = b[c],
                e = encodeURIComponent(String(d));
            d = this.xa(d);
            for (var f = 0; f < d.length; f++) {
                var g = e;
                "" !== d[f] && (g += "=" + encodeURIComponent(String(d[f])));
                a.push(g)
            }
        }
        return this.ya = a.join("&")
    };
    h.clone = function() {
        var a = new me;
        a.ya = this.ya;
        this.T && (a.T = this.T.clone(), a.K = this.K);
        return a
    };
    var we = function(a, b) {
        b = String(b);
        a.Ja && (b = b.toLowerCase());
        return b
    };
    me.prototype.zh = function(a) {
        a && !this.Ja && (ve(this), this.ya = null, this.T.forEach(function(a, c) {
            var b = c.toLowerCase();
            c != b && (this.remove(c), this.remove(b), 0 < a.length && (this.ya = null, this.T.set(we(this, b), Ua(a)), this.K += a.length))
        }, this));
        this.Ja = a
    };
    me.prototype.extend = function(a) {
        for (var b = 0; b < arguments.length; b++) Uc(arguments[b], function(a, b) {
            this.add(b, a)
        }, this)
    };
    var xe = null,
        ye = null,
        Ae = function(a) {
            var b = new Uint8Array(Math.ceil(3 * a.length / 4)),
                c = 0;
            ze(a, function(a) {
                b[c++] = a
            });
            return b.subarray(0, c)
        },
        ze = function(a, b) {
            function c(b) {
                for (; d < a.length;) {
                    var c = a.charAt(d++),
                        e = ye[c];
                    if (null != e) return e;
                    if (!/^[\s\xa0]*$/.test(c)) throw Error("Unknown base64 encoding at char: " + c);
                }
                return b
            }
            Be();
            for (var d = 0;;) {
                var e = c(-1),
                    f = c(0),
                    g = c(64),
                    k = c(64);
                if (64 === k && -1 === e) break;
                b(e << 2 | f >> 4);
                64 != g && (b(f << 4 & 240 | g >> 2), 64 != k && b(g << 6 & 192 | k))
            }
        },
        Be = function() {
            if (!xe) {
                xe = {};
                ye = {};
                for (var a =
                        0; 65 > a; a++) xe[a] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(a), ye[xe[a]] = a, 62 <= a && (ye["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.".charAt(a)] = a)
            }
        };
    var Ce = function() {},
        De = "function" == typeof Uint8Array,
        Ge = function(a, b, c, d, e) {
            a.$ = null;
            b || (b = c ? [c] : []);
            a.yl = c ? String(c) : void 0;
            a.Md = 0 === c ? -1 : 0;
            a.Ea = b;
            a: {
                if (a.Ea.length && (b = a.Ea.length - 1, (c = a.Ea[b]) && "object" == typeof c && !oa(c) && !(De && c instanceof Uint8Array))) {
                    a.te = b - a.Md;
                    a.Ha = c;
                    break a
                }
                a.te = Number.MAX_VALUE
            }
            a.ul = {};
            if (d)
                for (b = 0; b < d.length; b++) c = d[b], c < a.te ? (c += a.Md, a.Ea[c] = a.Ea[c] || Ee) : a.Ha[c] = a.Ha[c] || Ee;
            e && e.length && Ma(e, xa(Fe, a))
        },
        Ee = Object.freeze ? Object.freeze([]) : [],
        K = function(a, b) {
            if (b < a.te) {
                b +=
                    a.Md;
                var c = a.Ea[b];
                return c === Ee ? a.Ea[b] = [] : c
            }
            c = a.Ha[b];
            return c === Ee ? a.Ha[b] = [] : c
        },
        He = function(a, b) {
            if (b < a.te) {
                b += a.Md;
                var c = a.Ea[b];
                return c === Ee ? a.Ea[b] = [] : c
            }
            c = a.Ha[b];
            return c === Ee ? a.Ha[b] = [] : c
        },
        Ie = function(a) {
            if (null == a || a instanceof Uint8Array) return a;
            if (w(a)) return Ae(a);
            Ia("Cannot coerce to Uint8Array: " + na(a));
            return null
        },
        Je = function(a) {
            if (a && 1 < a.length) {
                var b = na(a[0]);
                Ma(a, function(a) {
                    na(a) != b && Ia("Inconsistent type in JSPB repeated field array. Got " + na(a) + " expected " + b)
                })
            }
        },
        Ke = function(a,
            b, c) {
            b < a.te ? a.Ea[b + a.Md] = c : a.Ha[b] = c
        },
        Me = function(a, b, c) {
            var d = Fe(a, Le[0]);
            d && d !== b && void 0 !== c && (a.$ && d in a.$ && (a.$[d] = void 0), Ke(a, d, void 0));
            Ke(a, b, c)
        },
        Fe = function(a, b) {
            var c, d;
            Ma(b, function(b) {
                var e = K(a, b);
                null != e && (c = b, d = e, Ke(a, b, void 0))
            });
            return c ? (Ke(a, c, d), c) : 0
        },
        Ne = function(a, b, c) {
            a.$ || (a.$ = {});
            if (!a.$[c]) {
                for (var d = He(a, c), e = [], f = 0; f < d.length; f++) e[f] = new b(d[f]);
                a.$[c] = e
            }
            b = a.$[c];
            b == Ee && (b = a.$[c] = []);
            return b
        },
        Oe = function(a) {
            if (a.$)
                for (var b in a.$) {
                    var c = a.$[b];
                    if (oa(c))
                        for (var d = 0; d <
                            c.length; d++) c[d] && c[d].Yc();
                    else c && c.Yc()
                }
        };
    Ce.prototype.Yc = function() {
        Oe(this);
        return this.Ea
    };
    Ce.prototype.toString = function() {
        Oe(this);
        return this.Ea.toString()
    };
    Ce.prototype.getExtension = function(a) {
        if (this.Ha) {
            this.$ || (this.$ = {});
            var b = a.vl;
            if (a.wl) {
                if (a.wk()) return this.$[b] || (this.$[b] = Na(this.Ha[b] || [], function(b) {
                    return new a.Zj(b)
                })), this.$[b]
            } else if (a.wk()) return !this.$[b] && this.Ha[b] && (this.$[b] = new a.Zj(this.Ha[b])), this.$[b];
            return this.Ha[b]
        }
    };
    Ce.prototype.clone = function() {
        return new this.constructor(Pe(this.Yc()))
    };
    var Pe = function(a) {
        var b;
        if (oa(a)) {
            for (var c = Array(a.length), d = 0; d < a.length; d++) null != (b = a[d]) && (c[d] = "object" == typeof b ? Pe(b) : b);
            return c
        }
        if (De && a instanceof Uint8Array) return new Uint8Array(a);
        c = {};
        for (d in a) null != (b = a[d]) && (c[d] = "object" == typeof b ? Pe(b) : b);
        return c
    };
    var Qe = 0,
        Re = 0,
        Se = function(a) {
            if (a.constructor === Uint8Array) return a;
            if (a.constructor === ArrayBuffer) return new Uint8Array(a);
            if (a.constructor === Array) return new Uint8Array(a);
            if (a.constructor === String) return Ae(a);
            Ia("Type not convertible to Uint8Array.");
            return new Uint8Array(0)
        };
    var Te = function() {
        this.B = []
    };
    Te.prototype.length = function() {
        return this.B.length
    };
    Te.prototype.end = function() {
        var a = this.B;
        this.B = [];
        return a
    };
    var Ue = function(a, b) {
            for (; 127 < b;) a.B.push(b & 127 | 128), b >>>= 7;
            a.B.push(b)
        },
        Ve = function(a, b) {
            if (0 <= b) Ue(a, b);
            else {
                for (var c = 0; 9 > c; c++) a.B.push(b & 127 | 128), b >>= 7;
                a.B.push(1)
            }
        };
    h = Te.prototype;
    h.gg = function(a) {
        this.B.push(a >>> 0 & 255)
    };
    h.ad = function(a) {
        this.B.push(a >>> 0 & 255);
        this.B.push(a >>> 8 & 255)
    };
    h.j = function(a) {
        this.B.push(a >>> 0 & 255);
        this.B.push(a >>> 8 & 255);
        this.B.push(a >>> 16 & 255);
        this.B.push(a >>> 24 & 255)
    };
    h.Lh = function(a) {
        var b = a >>> 0;
        a = Math.floor((a - b) / 4294967296) >>> 0;
        Qe = b;
        Re = a;
        this.j(Qe);
        this.j(Re)
    };
    h.Kj = function(a) {
        this.B.push(a >>> 0 & 255);
        this.B.push(a >>> 8 & 255);
        this.B.push(a >>> 16 & 255);
        this.B.push(a >>> 24 & 255)
    };
    h.Jh = function(a) {
        var b = a;
        b = (a = 0 > b ? 1 : 0) ? -b : b;
        if (0 === b) 0 < 1 / b ? Qe = Re = 0 : (Re = 0, Qe = 2147483648);
        else if (isNaN(b)) Re = 0, Qe = 2147483647;
        else if (3.4028234663852886E38 < b) Re = 0, Qe = (a << 31 | 2139095040) >>> 0;
        else if (1.1754943508222875E-38 > b) b = Math.round(b / Math.pow(2, -149)), Re = 0, Qe = (a << 31 | b) >>> 0;
        else {
            var c = Math.floor(Math.log(b) / Math.LN2);
            b *= Math.pow(2, -c);
            b = Math.round(8388608 * b) & 8388607;
            Re = 0;
            Qe = (a << 31 | c + 127 << 23 | b) >>> 0
        }
        this.j(Qe)
    };
    h.Ih = function(a) {
        this.B.push(a ? 1 : 0)
    };
    h.Jj = function(a) {
        Ve(this, a)
    };
    h.Xe = function(a) {
        this.B.push.apply(this.B, a)
    };
    h.uc = function(a) {
        for (var b = this.B.length, c = 0; c < a.length; c++) {
            var d = a.charCodeAt(c);
            if (128 > d) this.B.push(d);
            else if (2048 > d) this.B.push(d >> 6 | 192), this.B.push(d & 63 | 128);
            else if (65536 > d)
                if (55296 <= d && 56319 >= d && c + 1 < a.length) {
                    var e = a.charCodeAt(c + 1);
                    56320 <= e && 57343 >= e && (d = 1024 * (d - 55296) + e - 56320 + 65536, this.B.push(d >> 18 | 240), this.B.push(d >> 12 & 63 | 128), this.B.push(d >> 6 & 63 | 128), this.B.push(d & 63 | 128), c++)
                } else this.B.push(d >> 12 | 224), this.B.push(d >> 6 & 63 | 128), this.B.push(d & 63 | 128)
        }
        return this.B.length - b
    };
    var We = function() {
            this.cd = [];
            this.ob = 0;
            this.la = new Te
        },
        Ye = function(a, b) {
            Xe(a, b, 2);
            b = a.la.end();
            a.cd.push(b);
            a.ob += b.length;
            b.push(a.ob);
            return b
        },
        Ze = function(a, b) {
            var c = b.pop();
            for (c = a.ob + a.la.length() - c; 127 < c;) b.push(c & 127 | 128), c >>>= 7, a.ob++;
            b.push(c);
            a.ob++
        };
    We.prototype.reset = function() {
        this.cd = [];
        this.la.end();
        this.ob = 0
    };
    var $e = function(a) {
            for (var b = new Uint8Array(a.ob + a.la.length()), c = a.cd, d = c.length, e = 0, f = 0; f < d; f++) {
                var g = c[f];
                b.set(g, e);
                e += g.length
            }
            c = a.la.end();
            b.set(c, e);
            a.cd = [b];
            return b
        },
        Xe = function(a, b, c) {
            Ue(a.la, 8 * b + c)
        };
    h = We.prototype;
    h.Kj = function(a, b) {
        null != b && null != b && (Xe(this, a, 0), Ve(this.la, b))
    };
    h.j = function(a, b) {
        null != b && null != b && (Xe(this, a, 0), Ue(this.la, b))
    };
    h.Lh = function(a, b) {
        if (null != b && null != b) {
            Xe(this, a, 0);
            a = this.la;
            var c = b;
            b = 0 > c;
            c = Math.abs(c);
            var d = c >>> 0;
            c = Math.floor((c - d) / 4294967296);
            c >>>= 0;
            b && (c = ~c >>> 0, d = (~d >>> 0) + 1, 4294967295 < d && (d = 0, c++, 4294967295 < c && (c = 0)));
            Qe = d;
            Re = c;
            b = Qe;
            for (d = Re; 0 < d || 127 < b;) a.B.push(b & 127 | 128), b = (b >>> 7 | d << 25) >>> 0, d >>>= 7;
            a.B.push(b)
        }
    };
    h.Jh = function(a, b) {
        null != b && (Xe(this, a, 5), this.la.Jh(b))
    };
    h.Ih = function(a, b) {
        null != b && (Xe(this, a, 0), this.la.Ih(b))
    };
    h.Jj = function(a, b) {
        null != b && (Xe(this, a, 0), Ve(this.la, b))
    };
    h.uc = function(a, b) {
        null != b && (a = Ye(this, a), this.la.uc(b), Ze(this, a))
    };
    h.Xe = function(a, b) {
        null != b && (b = Se(b), Xe(this, a, 2), Ue(this.la, b.length), a = this.la.end(), this.cd.push(a), this.cd.push(b), this.ob += a.length + b.length)
    };
    var af = function(a, b, c, d) {
        if (null != c)
            for (var e = 0; e < c.length; e++) {
                var f = Ye(a, b);
                d(c[e], a);
                Ze(a, f)
            }
    };
    var bf = function(a) {
        Ge(this, a, 0, null, null)
    };
    ya(bf, Ce);
    bf.prototype.Pf = function() {
        var a = new We;
        cf(this, a);
        return $e(a)
    };
    var cf = function(a, b) {
            var c = K(a, 1);
            null != c && b.uc(1, c);
            c = K(a, 2);
            null != c && b.Xe(2, c)
        },
        ef = function(a) {
            Ge(this, a, 0, df, null)
        };
    ya(ef, Ce);
    var df = [2, 11];
    ef.prototype.Pf = function() {
        var a = new We;
        var b = K(this, 1);
        null != b && a.Jj(1, b);
        b = He(this, 2);
        Je(b);
        b = !b.length || b[0] instanceof Uint8Array ? b : Na(b, Ie);
        if (0 < b.length && null != b)
            for (var c = 0; c < b.length; c++) a.Xe(2, b[c]);
        b = K(this, 3);
        null != b && a.uc(3, b);
        b = K(this, 4);
        null != b && a.Xe(4, b);
        b = K(this, 5);
        null != b && a.uc(5, b);
        b = K(this, 6);
        null != b && a.uc(6, b);
        b = K(this, 7);
        null != b && a.j(7, b);
        b = K(this, 8);
        null != b && a.Xe(8, b);
        b = K(this, 9);
        null != b && a.j(9, b);
        b = K(this, 10);
        null != b && a.j(10, b);
        b = Ne(this, bf, 11);
        0 < b.length && af(a, 11, b, cf);
        return $e(a)
    };
    var gf = function(a) {
        Ge(this, a, "scs.sc", ff, null)
    };
    ya(gf, Ce);
    var ff = [1, 2];
    gf.prototype.Pf = function() {
        var a = new We;
        var b = He(this, 1);
        if (0 < b.length && null != b)
            for (var c = 0; c < b.length; c++) {
                var d = a,
                    e = b[c];
                null != e && (Xe(d, 1, 0), Ve(d.la, e))
            }
        b = Ne(this, hf, 2);
        0 < b.length && af(a, 2, b, jf);
        return $e(a)
    };
    gf.xl = "scs.sc";
    var hf = function(a) {
        Ge(this, a, 0, null, Le)
    };
    ya(hf, Ce);
    var Le = [
        [2, 3, 4, 5]
    ];
    hf.prototype.Pf = function() {
        var a = new We;
        jf(this, a);
        return $e(a)
    };
    var jf = function(a, b) {
        var c = K(a, 1);
        null != c && b.uc(1, c);
        c = K(a, 2);
        null != c && b.uc(2, c);
        c = K(a, 3);
        null != c && b.Kj(3, c);
        c = K(a, 4);
        null != c && b.Jh(4, c);
        c = K(a, 5);
        null != c && b.Ih(5, c)
    };
    h = hf.prototype;
    h.getName = function() {
        return K(this, 1)
    };
    h.getStringValue = function() {
        return K(this, 2)
    };
    h.setStringValue = function(a) {
        Me(this, 2, a)
    };
    h.getFloatValue = function() {
        var a = K(this, 4);
        return null == a ? a : +a
    };
    h.setFloatValue = function(a) {
        Me(this, 4, a)
    };
    z("cast.player.api.VERSION", "1.0.0");
    z("cast.player.api.ErrorCode", {
        UNKNOWN: 0,
        PLAYBACK: 1,
        MEDIAKEYS: 2,
        NETWORK: 3,
        MANIFEST: 4
    });
    var kf = ["cast.player.api.ErrorCode.UNKNOWN", "cast.player.api.ErrorCode.PLAYBACK", "cast.player.api.ErrorCode.MEDIAKEYS", "cast.player.api.ErrorCode.NETWORK", "cast.player.api.ErrorCode.MANIFEST"];
    z("cast.player.api.HlsSegmentFormat", {
        MPEG2_TS: 0,
        MPEG_AUDIO_ES: 1,
        PACKED_AUDIO_AC3: 2,
        sl: 3,
        MPEG_LAYER_3: 4
    });
    z("cast.player.api.CaptionsType", {
        UNKNOWN: "unknown",
        WEBVTT: "webvtt",
        TTML: "ttml",
        CEA608: "cea608",
        STPP: "stpp"
    });
    z("cast.player.api.ContentProtection", {
        NONE: "none",
        CLEARKEY: "clearkey",
        PLAYREADY: "playready",
        WIDEVINE: "widevine",
        AES_128: "aes_128",
        AES_128_CKP: "aes_128_ckp"
    });
    z("cast.player.api.LoggerLevel", {
        DEBUG: 0,
        INFO: 800,
        WARNING: 900,
        ERROR: 1E3,
        NONE: Infinity
    });
    z("cast.player.api.StreamingProtocolType", {
        UNKNOWN: 0,
        MPEG_DASH: 1,
        HLS: 2,
        SMOOTH_STREAMING: 3
    });
    var lf = function() {
        this.timeoutInterval = 3E4;
        this.headers = this.url = null;
        this.withCredentials = !1;
        this.protectionSystem = this.content = null;
        this.skipRequest = !1;
        this.setResponse = null;
        this.interval = {
            time: 0,
            duration: 0
        };
        this.mc = null
    };
    z("cast.player.api.RequestInfo", lf);
    var mf = function(a, b, c, d, e) {
        this.url = a;
        this.errorCode = b;
        this.status = c;
        this.responseHeaders = d;
        this.response = e
    };
    z("cast.player.api.RequestStatus", mf);
    var nf = function(a, b, c, d, e, f) {
        this.codecs = a;
        this.mimeType = b;
        this.bitrates = c;
        this.language = d;
        this.name = e;
        this.role = f
    };
    z("cast.player.api.StreamInfo", nf);
    window.VTTCue = window.VTTCue || window.TextTrackCue;
    var of = function(a) {
        Zb().Ah(Ub(a))
    };
    z("cast.player.api.setLoggerLevel", of );
    ae || (ae = new Yd);
    if (ae) {
        var pf = ae;
        if (1 != pf.vi) {
            var qf = Zb(),
                rf = pf.Xk;
            qf.Wd || (qf.Wd = []);
            qf.Wd.push(rf);
            pf.vi = !0
        }
    };
    var sf = function(a) {
        this.url = a;
        this.updateCaptionsRequestInfo = this.updateSegmentRequestInfo = this.updateManifestRequestInfo = null
    };
    z("cast.player.api.HostBase", sf);
    var tf = {
            clearkey: "org.w3.clearkey",
            playready: "com.chromecast.playready",
            widevine: "com.widevine.alpha"
        },
        uf = B("cast.player.common"),
        vf = function(a, b, c) {
            return "caption" == a || Da(b, "text/") || "application/mp4" == b && "stpp" == c
        },
        xf = function(a, b) {
            b && (b = new Uint8Array(new ArrayBuffer(16)), b.set(a), wf(b.subarray(0, 4)), wf(b.subarray(4, 6)), wf(b.subarray(6, 8)), a = b);
            this.Ue = a
        };
    xf.prototype.sb = function(a) {
        if (!a) return !1;
        var b = this.Ue;
        a = a.Ue;
        for (var c = 0; 16 > c; c++)
            if (b[c] != a[c]) return !1;
        return !0
    };
    xf.prototype.Yc = function() {
        return this.Ue
    };
    xf.prototype.toString = function() {
        for (var a = "", b = 0; b < this.Ue.length; b++) a += ("0" + this.Ue[b].toString(16)).slice(-2);
        return a
    };
    var wf = function(a) {
            for (var b = 0, c = a.length - 1; b < c; b++, c--) {
                var d = a[b];
                a[b] = a[c];
                a[c] = d
            }
        },
        yf = new xf([154, 4, 240, 121, 152, 64, 66, 134, 171, 146, 230, 91, 224, 136, 95, 149]),
        zf = new xf([237, 239, 139, 169, 121, 214, 74, 206, 163, 200, 39, 220, 213, 29, 33, 237]),
        Af = new xf([16, 119, 239, 236, 192, 178, 77, 2, 172, 227, 60, 30, 82, 226, 251, 75]),
        Bf = function(a, b) {
            return MediaSource.isTypeSupported(a + '; codecs="' + b + '"')
        },
        Cf = function(a) {
            return "mp4a.a6" == a || "ec-3" == a
        },
        Df = function(a, b, c) {
            for (a = a.toString(b); a.length < c;) a = "0" + a;
            return a
        },
        Ef = function(a) {
            for (var b =
                    a.length / 2, c = new Uint8Array(b), d = 0; d < b; d++) c[d] = parseInt(a.substr(2 * d, 2), 16);
            return c
        },
        Ff = function(a) {
            if (cast.__platform__ && cast.__platform__.queryPlatformValue) return cast.__platform__.queryPlatformValue(a)
        },
        Gf = function(a) {
            return 2 == a ? "mp4a.a5" : 4 == a ? "" : "mp4a.40.2"
        },
        Hf = function(a) {
            a = a.toLowerCase();
            return !a.includes(",") && !!a.match(/^(mp4a|[ae]c-3)/)
        },
        If = function(a) {
            a = a.split(",");
            for (var b = 0; b < a.length; b++) {
                var c = a[b].match(Hd);
                if (c && !Bf("video/mp4", c[0])) {
                    F(uf, c[0] + " removed");
                    a.splice(b, 1);
                    break
                }
                if (c =
                    a[b]) {
                    var d = c.split(".");
                    2 != d.length || "avc1" != d[0] && "avc3" != d[0] || (c = d[0] + "." + d[1].toUpperCase())
                }
                a[b] = c
            }
            return a.join(",")
        },
        Jf = cast.__platform__ && cast.__platform__.display,
        Kf = cast.__platform__ && cast.__platform__.canDisplayType,
        Lf = Ff("log-level-mpl"); of (u(Lf) ? parseInt(Lf, 10) : 0);
    var Mf = {
            "Cast.MPL.SegmentStats.Size": {
                min: 0,
                max: 1E6,
                Qi: 100
            },
            "Cast.MPL.SegmentStats.Time": {
                min: 0,
                max: 2E4,
                Qi: 20
            }
        },
        Nf = function(a, b) {
            Mf[a] ? za() && cast.__platform__.metrics.logHistogramValueToUma(a, b, Mf[a].min, Mf[a].max, Mf[a].Qi) : E(uf, "Invalid histogram name")
        };
    var Of = function(a) {
        sf.call(this, a.url);
        this.initialBandwidth = 3145728;
        this.autoResumeDuration = 10;
        this.autoResumeNumberOfSegments = 3;
        this.autoPauseDuration = 1;
        this.segmentRequestRetryLimit = 3;
        this.useSingleKeySession = !1;
        this.mediaElement = a.mediaElement;
        this.url = a.url;
        this.licenseUrl = a.licenseUrl || null;
        this.protectionSystem = a.protectionSystem || null;
        this.licenseCustomData = a.licenseCustomData || null;
        this.Nh = 1 == a.aggressiveStallHandling;
        this.hk = 1 == a.disableSourceBufferTimeAdjust;
        this.Ng = 1 == a.hlsContentKeyProtection;
        this.il = 1 == a.skipSegmentOnNetworkFailure;
        this.preferSequenceNumberForPlaylistMatching = 1 == a.preferSequenceNumberForPlaylistMatching;
        this.ignoreTtmlPositionInfo = 1 == a.ignoreTtmlPositionInfo;
        this.useRelativeCueTimestamps = 1 == a.useRelativeCueTimestamps;
        this.onCue = this.onAutoPause = this.onMediaDownloadEnded = this.onManifestReady = this.processLicense = this.processSegment = this.processManifest = this.processMetadata = this.prepareLicenseRequest = this.getQualityLevel = this.trackBandwidth = this.updateCaptionsRequestInfo =
            this.updateSegmentRequestInfo = this.updateLicenseRequestInfo = this.updateManifestRequestInfo = this.onError = null
    };
    m(Of, sf);
    z("cast.player.api.Host", Of);
    Of.prototype.R = function(a, b, c, d) {
        var e = a;
        if (b) {
            e = b.errorCode;
            if (6 == b.errorCode && 0 <= b.status) var f = b.status;
            var g = a;
            null != e && (g = 10 * g + e % 10, null != f && (g = 1E3 * g + f % 1E3));
            e = g
        }
        A("Cast.MPL.Error", e);
        a = Math.floor(a / 100);
        Hc(d || Pf, "error: " + kf[a] + "/" + e + (c ? " (" + c + ")" : ""));
        if (this.onError) this.onError(a, b)
    };
    Of.prototype.lj = function(a) {
        return this.processManifest ? this.processManifest(a) : a
    };
    var Pf = B("cast.player.api.Host");
    var Qf = function() {
        this.dd = cast.__platform__ && cast.__platform__.cache;
        this.zd = this.isEnabled() ? this.dd.capacity() : 0;
        this.He = new Pc
    };
    Qf.prototype.isEnabled = function() {
        return u(this.dd)
    };
    var Sf = function(a, b, c) {
            var d = cd();
            a.dd.insert(b, c).then(function() {
                a.zd -= c.byteLength;
                d.resolve()
            }, function(a) {
                E(Rf, "Failed to cache (" + b + ") because of " + a);
                d.reject(a)
            });
            return d.mj
        },
        Uf = function(a, b, c) {
            if (a.isEnabled()) {
                for (var d = new ArrayBuffer(2 * c.length), e = new Uint16Array(d), f = 0; f < c.length; f++) e[f] = c.charCodeAt(f);
                a = Tf(a, b, d)
            } else a = ad("Cache is not enabled.");
            return a
        },
        Tf = function(a, b, c) {
            if (!a.isEnabled()) return ad("Cache is not enabled.");
            var d = Vf(b);
            F(Rf, "Store data into cache with key: " +
                d);
            return a.zd < c.byteLength ? (a = "Insufficient cache space available for " + b.url, E(Rf, a), ad(a)) : Sf(a, d, c)
        },
        Vf = function(a) {
            var b = a.url;
            a.headers && a.headers.Range && (b += "\b" + a.headers.Range);
            return b
        },
        Wf = function(a, b) {
            return a.isEnabled() && a.dd.contains(Vf(b))
        },
        Xf = function(a, b) {
            a.isEnabled() ? (a = a.dd.get(Vf(b)), a instanceof Xc || (b = new Xc(la), Vc(b, 2, a), a = b)) : a = ad("Cache is not enabled.");
            return a
        },
        Yf = function(a) {
            return a.isEnabled() ? 512E3 > a.zd : !0
        };
    Qf.prototype.reset = function() {
        this.isEnabled() && (this.zd = this.dd.capacity())
    };
    ma(Qf);
    var Rf = B("cast.player.cache.Store");
    var Zf = function(a) {
            this.buffer = a;
            this.bi = new DataView(a.buffer, a.byteOffset);
            this.offset = 0
        },
        $f = function(a) {
            return a.buffer[a.offset++]
        },
        ag = function(a) {
            var b = a.bi.getUint16(a.offset);
            a.offset += 2;
            return b
        },
        L = function(a) {
            var b = a.bi.getUint32(a.offset);
            a.offset += 4;
            return b
        },
        bg = function(a) {
            var b = L(a);
            a = L(a);
            return 4294967296 * b + a
        },
        cg = function(a, b) {
            var c = a.buffer.subarray(a.offset, a.offset + b);
            a.offset += b;
            return c
        },
        dg = function(a) {
            return a.buffer.subarray(a.offset)
        },
        M = function(a, b) {
            a.offset += b
        };
    var eg = function() {};
    eg.prototype.mh = function(a, b) {
        a = new Uint8Array(a);
        239 == a[0] && 187 == a[1] && 191 == a[2] && (a = a.subarray(3));
        a = escape(String.fromCharCode.apply(null, a));
        try {
            var c = decodeURIComponent(a)
        } catch (d) {
            throw Error("Decoding Error.");
        }
        return this.parse(c, b)
    };
    var fg = function(a) {
        this.jf = 30;
        this.tk = a
    };
    m(fg, eg);
    var gg = function(a, b) {
            a = p(a);
            for (var c = a.next(); !c.done; c = a.next())
                if (c = c.value, c.localName == b) return c;
            return null
        },
        jg = function(a, b) {
            var c = b.split(":");
            if (1 == c.length) {
                var d = 0;
                if (c = c[0].match(hg)) switch (b = parseFloat(c[1]), c[2]) {
                    case "h":
                        d = 3600 * b;
                        break;
                    case "m":
                        d = 60 * b;
                        break;
                    case "s":
                        d = b;
                        break;
                    case "ms":
                        d = b / 1E3;
                        break;
                    case "f":
                        d = b / a.jf
                } else E(ig, "unsupported time expression: " + b);
                return d
            }
            d = 3600 * parseInt(c[0], 10) + 60 * parseInt(c[1], 10) + parseFloat(c[2]);
            4 == c.length && (d += parseInt(c[3], 10) / a.jf);
            return d
        },
        kg = function(a) {
            var b = "";
            a = p(a.childNodes);
            for (var c = a.next(); !c.done; c = a.next()) c = c.value, c.nodeType == Node.TEXT_NODE ? (c = c.textContent.trim()) && (b += c) : "span" == c.localName ? (b += kg(c), (c = c.attributes.getNamedItem("tts:fontStyle")) && "italic" == c.value && (b = "<i>" + b + "</i>")) : "br" == c.localName && (b += "\n");
            return b
        };
    fg.prototype.parse = function(a, b) {
        var c = [],
            d = (new DOMParser).parseFromString(a, "text/xml"),
            e = gg(d.childNodes, "tt");
        if (!e) return E(ig, "missing tt"), c;
        var f = d = null,
            g = gg(e.childNodes, "head");
        if (g) {
            if (f = gg(g.childNodes, "styling")) {
                d = null;
                f = p(f.childNodes);
                for (var k = f.next(); !k.done; k = f.next())
                    if (k = k.value, "style" === k.nodeName && (k = lg(k))) d = d || new Map, d.set(k.id, k.origin)
            } else d = null;
            f = d;
            if (k = gg(g.childNodes, "layout")) {
                g = null;
                k = p(k.childNodes);
                for (var l = k.next(); !l.done; l = k.next()) {
                    var n = l.value;
                    if ("region" ==
                        n.nodeName && (l = n.attributes.getNamedItem("xml:id"))) {
                        var q = f,
                            r = null,
                            v = lg(n);
                        v ? r = v.origin : q && (v = n.attributes.getNamedItem("style")) && (r = q.get(v.value));
                        if (q = r) g = g || new Map, n = n.attributes.getNamedItem("tts:textAlign"), g.set(l.value, {
                            align: n && n.value || "left",
                            origin: q
                        })
                    }
                }
                f = g
            } else f = null
        }
        g = gg(e.childNodes, "body");
        if (!g) return E(ig, "missing body"), c;
        (k = e.attributes.getNamedItem("ttp:frameRate")) ? this.jf = parseInt(k.value, 10): (E(ig, "defaulting frameRate to30"), this.jf = 30);
        k = gg(g.childNodes, "div");
        if (!k) return E(ig,
            "missing div"), c;
        g = {};
        if (e = gg(e.childNodes, "head"))
            if (e = gg(e.childNodes, "metadata"))
                for (e = p(e.childNodes), l = e.next(); !l.done; l = e.next()) l = l.value, "image" == l.localName && (g["#" + l.attributes.getNamedItem("xml:id").value] = kg(l));
        e = null;
        k.attributes && d && (l = k.attributes.getNamedItem("style")) && (e = d.get(l.value));
        k = p(k.childNodes);
        for (l = k.next(); !l.done; l = k.next()) {
            var H = l.value;
            if ("p" == H.localName && H.attributes) {
                var pa = v = null;
                q = n = r = l = null;
                var pb = kg(H);
                H = p(H.attributes);
                for (var qa = H.next(); !qa.done; qa = H.next()) switch (qa =
                    qa.value, qa.localName) {
                    case "begin":
                        v = jg(this, qa.value) + b;
                        break;
                    case "end":
                        pa = jg(this, qa.value) + b;
                        break;
                    case "backgroundImage":
                        g[qa.value] && (l = g[qa.value]);
                        break;
                    case "origin":
                        r = mg(qa.value);
                        break;
                    case "region":
                        n = qa.value;
                        break;
                    case "style":
                        q = qa.value
                }!pb && !l || null === v || null === pa ? E(ig, "skipped cue begin=" + v + ", end=" + pa + ", text=" + a) : (v = new VTTCue(v, pa, pb), this.tk || (pa = d, pb = e, H = null, n && f && (H = f.get(n)) && H.align && (v.align = H.align), !r && q && pa && (r = pa.get(q)), r || (r = H && H.origin || pb), r && (v.line = r.y, v.position =
                    r.x, v.snapToLines = !1, "left" != v.align && "right" != v.align && (v.align = "left"))), c.push({
                    Wf: v,
                    uk: l
                }))
            }
        }
        return c
    };
    var mg = function(a) {
            if ("auto" === a) return null;
            a = a.split(" ");
            return 2 !== a.length ? (E(ig, "Two numbers are expected in tts:origin"), null) : a.find(function(a) {
                return !a.includes("%")
            }) ? (E(ig, "Only percentage value is supported in tts:origin"), null) : {
                x: parseInt(a[0], 10),
                y: parseInt(a[1], 10)
            }
        },
        lg = function(a) {
            var b = a.attributes;
            if (!b) return null;
            var c = b.getNamedItem("xml:id");
            if (!c) return null;
            b = b.getNamedItem("tts:origin");
            if (!b) {
                a = p(a.childNodes);
                for (var d = a.next(); !d.done; d = a.next()) d = d.value, d.attributes &&
                    (b = d.attributes.getNamedItem("tts:origin"));
                if (!b) return null
            }
            return (b = mg(b.value)) ? {
                id: c.value,
                origin: b
            } : null
        },
        ig = B("cast.player.core.TtmlParser"),
        hg = /([0-9]*\.?[0-9]+)?(h|ms|m|s|f)/;
    var og = function(a) {
        this.X = ng(a)
    };
    og.prototype.reset = function(a) {
        this.X = ng(a)
    };
    var ng = function(a) {
        for (var b = -1, c = 0; c < a.length; c++) {
            var d = parseInt(a.charAt(c), 10);
            if (isNaN(d)) throw Error("Invalid positive base 10 integer string");
            0 > b && d && (b = c)
        }
        return a.substr(b, a.length)
    };
    og.prototype.toString = function() {
        return this.X
    };
    og.prototype.add = function(a) {
        if (0 > a || Math.floor(a) != a) throw Error("Value must be a positive integer");
        var b = a + "",
            c = Math.max(b.length, this.X.length);
        a = [];
        for (var d = 0, e = 0; e < c; e++) {
            var f = d + parseInt(e < b.length ? b.charAt(b.length - 1 - e) : "0", 10) + parseInt(e < this.X.length ? this.X.charAt(this.X.length - 1 - e) : "0", 10);
            10 <= f ? (d = 1, f -= 10) : d = 0;
            a.push(f)
        }
        b = 0 < d ? "1" : "";
        for (c = a.length - 1; 0 <= c; c--) b += a[c];
        this.X = b
    };
    var pg = function(a, b) {
        if (b.X.length < a.X.length) return !0;
        if (b.X.length > a.X.length) return !1;
        for (var c = 0; c < b.X.length; c++) {
            var d = parseInt(a.X.charAt(c), 10),
                e = parseInt(b.X.charAt(c), 10);
            if (d < e) return !1;
            if (d > e) break
        }
        return !0
    };
    var qg = function(a) {
        Zf.call(this, a)
    };
    m(qg, Zf);
    var rg = function(a) {
            this.buffer = new Uint8Array(new ArrayBuffer(a ? a : 2E4));
            this.offset = 0
        },
        sg = function(a) {
            return a.buffer.subarray(0, a.offset)
        };
    rg.prototype.gg = function(a) {
        N(this, [a])
    };
    rg.prototype.ad = function(a) {
        N(this, [a >> 8, a])
    };
    rg.prototype.j = function(a) {
        N(this, [a >> 24, a >> 16, a >> 8, a])
    };
    rg.prototype.Lh = function(a) {
        this.j(a / 4294967296);
        this.j(a % 4294967296)
    };
    var N = function(a, b) {
        var c = b.length;
        if (!(a.offset + c <= a.buffer.length)) {
            for (var d = a.buffer.length; d < a.offset + c;) d *= 2;
            d = new Uint8Array(new ArrayBuffer(d));
            d.set(a.buffer, 0);
            a.buffer = d
        }
        a.buffer.set(b, a.offset);
        a.offset += c
    };
    var tg = function(a) {
        rg.call(this, a)
    };
    m(tg, rg);
    var ug = function(a, b, c) {
        (new DataView(a.buffer, a.byteOffset, a.byteLength)).setUint32(b, c)
    };
    var vg = [96E3, 88200, 64E3, 48E3, 44100, 32E3, 24E3, 22050, 16E3, 12E3, 11025, 8E3, 7350],
        wg = function(a) {
            Hc(B("cast.player.mp4.base"), a)
        },
        xg = function(a, b, c) {
            if (0 > a || 32 <= a) return wg("Invalid object type id: " + a), null;
            var d = vg.indexOf(b);
            return 0 > d ? (wg("Invalid sample rate: " + b), null) : 0 > c || 16 < c ? (wg("Invalid channels: " + c), null) : new Uint8Array([a << 3 | d >> 1, d << 7 | c << 3])
        };
    var yg = function(a) {
            this.J = new qg(a);
            this.pc = 0;
            this.Jd = this.Mc = null;
            this.If = a;
            this.gb = 0;
            this.pc = L(this.J);
            this.gb = 4;
            this.Mc = L(this.J);
            this.gb += 4;
            1 == this.pc && (this.pc = bg(this.J), this.gb += 8);
            1970628964 == this.Mc && (this.Jd = new xf(cg(this.J, 16)), this.gb += 16)
        },
        zg = function(a) {
            switch (a.Mc) {
                case 1836019574:
                case 1836019558:
                case 1836475768:
                case 1953653099:
                case 1953653094:
                case 1701082227:
                case 1835297121:
                case 1835626086:
                case 1684631142:
                case 1937007212:
                    return !0;
                default:
                    return !1
            }
        };
    yg.prototype.getName = function() {
        return this.Mc
    };
    var Ag = function(a) {
            return a.If.subarray(0, a.pc)
        },
        O = function(a) {
            return Ag(a).subarray(a.gb)
        };
    yg.prototype.h = function(a) {
        N(a, this.If.subarray(0, this.pc))
    };
    var P = function(a) {
        yg.call(this, a);
        this.$b = this.Da = 0;
        a = L(this.J);
        this.gb += 4;
        this.Da = a >>> 24;
        this.$b = a & 16777215
    };
    m(P, yg);
    var Bg = function(a) {
        P.call(this, a)
    };
    m(Bg, P);
    Bg.prototype.Kg = function(a) {
        for (var b = this.Da, c = $f(this.J), d = Array(c), e = 0; e < c; e++) {
            if (1 == b) {
                var f = L(this.J);
                var g = L(this.J);
                var k = L(this.J);
                var l = L(this.J);
                var n = new og(Math.floor(4294967296 * f / 1E4).toString() + "0000");
                n.add(f % 1E4 * 7296 % 1E4);
                n.add(g);
                f = 4294967296 / a * f + g / a;
                k = 4294967296 / a * k + l / a
            } else l = L(this.J), k = L(this.J), n = new og(l.toString()), f = l / a, k /= a;
            d[e] = {
                time: f,
                duration: k,
                Eb: n,
                offset: 0,
                size: 0,
                url: null
            }
        }
        return d
    };
    var Cg = new xf([212, 128, 126, 242, 202, 57, 70, 149, 142, 84, 38, 203, 158, 70, 167, 159]),
        Dg = function(a) {
            P.call(this, a)
        };
    m(Dg, P);
    Dg.prototype.Kg = function(a) {
        var b = this.Da;
        M(this.J, 4);
        var c = L(this.J),
            d = 0 == b ? L(this.J) : bg(this.J),
            e = 0 == b ? L(this.J) : bg(this.J),
            f = a + Ag(this).byteOffset,
            g = this.pc;
        M(this.J, 2);
        a = ag(this.J);
        b = Array(a);
        e = f + g + e;
        for (f = 0; f < a; f++) {
            g = L(this.J) & 2147483647;
            var k = L(this.J);
            M(this.J, 4);
            b[f] = {
                time: d / c,
                duration: k / c,
                Eb: null,
                offset: e,
                size: g,
                url: null
            };
            d += k;
            e += g
        }
        return b
    };
    var Eg = function(a) {
        P.call(this, a);
        this.Of = new Uint8Array([]);
        this.uh = null;
        this.Wg = !1;
        this.qe()
    };
    m(Eg, P);
    Eg.prototype.qe = function() {
        var a = new qg(O(this)),
            b = this.$b;
        b & 1 && M(a, 20);
        var c = L(a);
        this.uh = dg(a);
        this.Wg = b & 2 ? !0 : !1;
        this.Of = new Uint8Array(new ArrayBuffer(c));
        for (b = 0; b < c; b++)
            if (this.Wg) {
                M(a, 8);
                var d = 6 * ag(a);
                this.Of[b] = 10 + d;
                M(a, d)
            } else this.Of[b] = 8, M(a, 8)
    };
    var Fg = new xf([162, 57, 79, 82, 90, 155, 79, 20, 162, 68, 108, 66, 124, 100, 141, 244]),
        Gg = function(a) {
            P.call(this, a);
            this.Dd = 0;
            this.Tc = [];
            this.Cd = [];
            this.qe()
        };
    m(Gg, P);
    Gg.prototype.qe = function() {
        var a = new qg(O(this));
        this.Dd = L(a);
        var b = this.$b;
        b & 1 && M(a, 4);
        b & 4 && M(a, 4);
        var c = !!(b & 256),
            d = !!(b & 512),
            e = !!(b & 1024);
        b = !!(b & 2048);
        for (var f = 0; f < this.Dd; f++) c && M(a, 4), d && this.Tc.push(L(a)), e && M(a, 4), b && this.Cd.push(L(a))
    };
    Gg.prototype.Qf = function(a) {
        ug(O(this), 4, a)
    };
    var Hg = function(a) {
        P.call(this, a);
        this.Pe = 9E4;
        this.qe()
    };
    m(Hg, P);
    Hg.prototype.qe = function() {
        var a = new qg(O(this));
        M(a, 4);
        M(a, 4);
        this.Pe = L(a)
    };
    var Ig = function(a) {
        P.call(this, a)
    };
    m(Ig, P);
    var Jg = function(a) {
        P.call(this, a);
        this.Dh = new xf(cg(this.J, 16));
        a = L(this.J);
        this.ff = cg(this.J, a)
    };
    m(Jg, P);
    Jg.prototype.getData = function() {
        return this.ff
    };
    var Kg = function(a) {
        P.call(this, a)
    };
    m(Kg, P);
    var Lg = function(a) {
        yg.call(this, a)
    };
    m(Lg, yg);
    Lg.prototype.Qf = function(a, b) {
        var c = Q(O(this), 1953653094, void 0, void 0);
        if (c) {
            var d = Q(O(c), 1953658222, void 0, void 0);
            d && d.Qf(a - this.If.byteOffset);
            b && (a = Q(O(c), 1935763823, void 0, void 0)) && (b -= this.If.byteOffset, c = 0, a.$b & 1 && (c += 8), c += 4, d = O(a), 0 != a.Da && (ug(d, c, Math.floor(b / 4294967296)), c += 4), ug(d, c, Math.floor(b % 4294967296)))
        }
    };
    var Mg = function(a, b) {
            var c = Q(a, 1836019558);
            a = O(Q(a, 1835295092)).byteOffset;
            b ? c.Qf(a + b, a) : c.Qf(a)
        },
        Ng = function(a) {
            switch (a.getName()) {
                case 1836019558:
                    return new Lg(Ag(a));
                case 1835296868:
                    return new Hg(Ag(a));
                case 1953658222:
                    return new Gg(Ag(a));
                case 1935763823:
                    return new Kg(Ag(a));
                case 1936286840:
                    return new Dg(Ag(a));
                case 1952868452:
                    return new Ig(Ag(a));
                case 1970628964:
                    var b = a.Jd;
                    return Fg.sb(b) ? new Eg(Ag(a)) : Cg.sb(b) ? new Bg(Ag(a)) : a;
                case 1886614376:
                    return new Jg(Ag(a));
                case 1702061171:
                case 1751411826:
                case 1835427940:
                case 1836476516:
                case 1935763834:
                case 1935894637:
                case 1937011571:
                case 1952804451:
                case 1952867444:
                case 1953196132:
                case 1953654136:
                    return new P(Ag(a));
                default:
                    return a
            }
        },
        Q = function(a, b, c, d) {
            for (var e = 0; e < a.length;) {
                var f = new yg(a.subarray(e));
                e += f.pc;
                if (f.getName() == b && (1970628964 != b || c && c.sb(f.Jd))) return Ng(f);
                if (d && zg(f) && (f = Q(O(f), b, c, d))) return f
            }
            return null
        },
        Og = function(a, b) {
            for (var c = 0, d = []; c < a.length;) {
                var e = new yg(a.subarray(c));
                c += e.pc;
                d.push(Ng(e));
                b && zg(e) && (d = d.concat(Og(O(e), !0)))
            }
            return d
        };
    var Pg = function(a) {
        fg.apply(this, arguments)
    };
    m(Pg, fg);
    Pg.prototype.mh = function(a, b) {
        a = Q(a, 1835295092);
        if (!a) return [];
        a = dg(new Zf(O(a)));
        return fg.prototype.mh.call(this, a, b)
    };
    var Qg = function() {};
    m(Qg, eg);
    Qg.prototype.parse = function(a, b) {
        for (var c = a.trim().split(Rg), d = a = 0, e = p(c[0].split("\n")), f = e.next(); !f.done; f = e.next())
            if (f = f.value, 0 == f.indexOf("X-TIMESTAMP-MAP")) {
                (e = f.match(Sg)) && (a = Tg(e[1]));
                (e = f.match(Ug)) && (d = parseInt(e[1], 10) / 9E4);
                break
            }
        b += d - a;
        if (isNaN(b)) return E(Vg, "invalid time offset"), [];
        a = [];
        c = p(c);
        for (d = c.next(); !d.done; d = c.next()) {
            var g = d.value;
            if (g) {
                var k = b,
                    l = null;
                f = e = d = null;
                g = g.split("\n");
                for (var n = 0, q = p(g), r = q.next(); !r.done; r = q.next()) {
                    r = r.value;
                    if (l = r.match(Wg)) {
                        d = r.match(Xg);
                        e = r.match(Yg);
                        f = r.match(Zg);
                        break
                    }
                    n++
                }
                l ? (q = Tg(l[1]), l = Tg(l[2]), g = g.slice(n + 1).join("\n"), isNaN(q) || isNaN(l) || !g ? (E(Vg, "skipped cue, begin=" + q + ", end=" + l + ", text=" + g), d = null) : (k = new VTTCue(q + k, l + k, g), d && (k.align = d[1]), e && (k.position = parseInt(e[1], 10)), f && (k.size = parseInt(f[1], 10)), d = k)) : d = null;
                d && a.push({
                    Wf: d
                })
            }
        }
        return a
    };
    var Tg = function(a) {
            var b = a.split(":");
            if (3 < b.length) return E(Vg, "unexpected time format=" + a), 0;
            a = 0;
            b = p(b);
            for (var c = b.next(); !c.done; c = b.next()) a = 60 * a + parseFloat(c.value);
            return a
        },
        Vg = B("cast.player.core.WebvttParser"),
        Rg = /\n\s*\n/,
        Ug = /MPEGTS:([\d]*)/,
        Xg = /align:(start|middle|end|left|center|right)/,
        Yg = /position:(\d*)%/,
        Zg = /size:(\d*)%/,
        Sg = /LOCAL:((?:[\d]{2}:)?[\d]{2}:[\d]{2}.[\d]{3})/,
        Wg = /((?:[\d]{2}:)?[\d]{2}:[\d]{2}.[\d]{3})[\t ]+--\x3e[\t ]+((?:[\d]{2}:)?[\d]{2}:[\d]{2}.[\d]{3})/;
    var $g = function(a, b, c, d) {
        C.call(this);
        this.a = b;
        this.Qe = document.createElement("track");
        this.Qe.src = "";
        this.Qe.kind = "captions";
        this.a.mediaElement.appendChild(this.Qe);
        this.Xc = this.Qe.track;
        this.Xc.mode = "showing";
        this.ak = a;
        this.Pb = c;
        A("Cast.MPL.Captions", Kd(this.Pb));
        this.ud = null;
        "ttml" == c ? this.ud = new fg(1 == this.a.ignoreTtmlPositionInfo) : "webvtt" == c ? this.ud = new Qg : d && "application/mp4" == d && "stpp" == c && (this.ud = new Pg(1 == this.a.ignoreTtmlPositionInfo));
        this.La = null;
        this.Gj = 0;
        this.Sd = []
    };
    m($g, C);
    $g.prototype.G = function() {
        ah(this);
        this.Xc.mode = "disabled";
        this.Qe.remove();
        C.prototype.G.call(this)
    };
    var ah = function(a) {
        var b = a.Xc.cues;
        if (b)
            for (; 0 < b.length;) a.Xc.removeCue(b[0]);
        a.Sd.forEach(function(a) {
            a.remove()
        });
        a.Sd.length = 0
    };
    h = $g.prototype;
    h.createBuffer = function() {};
    h.reset = function() {
        ah(this);
        this.La = null
    };
    h.ee = function() {
        return !0
    };
    h.ki = function() {
        return Infinity
    };
    h.append = function(a, b, c, d) {
        d && (this.Gj = b.time - c);
        if (this.ud) {
            var e = this.a.useRelativeCueTimestamps || "ttml" == this.Pb ? b.time : this.Gj;
            c = bh(this);
            d = 0 < c.length ? c[c.length - 1].startTime : -Infinity;
            for (a = this.ud.mh(a, e); 0 < a.length;) {
                e = a.pop();
                var f = e.Wf,
                    g = !1;
                if (f.startTime < d || .1 >= Math.abs(f.startTime - d))
                    for (var k = c.length - 1; 0 <= k; k--)
                        if (c[k].text == f.text && .1 >= Math.abs(c[k].startTime - f.startTime) && .1 >= Math.abs(c[k].endTime - f.endTime)) {
                            g = !0;
                            break
                        }
                g || this.addCue(e)
            }
        }
        this.La = b
    };
    h.va = function(a) {
        return this.La ? this.La.time + this.La.duration - a : 0
    };
    h.addCue = function(a) {
        if (this.a.onCue) this.a.onCue(this.Pb);
        var b = a.Wf;
        this.Xc.addCue(b);
        var c = a.uk;
        if (c) {
            var d = null;
            b.zl = function() {
                var a = document.createElement("img");
                a.src = "data:image/png;base64," + c;
                a.style.position = "absolute";
                a.style.left = "0";
                a.style.bottom = "0";
                a.style.right = "0";
                a.style.margin = "0 auto";
                a.style.zIndex = 5;
                this.a.mediaElement.parentNode.appendChild(a);
                d = a;
                this.Sd.push(d)
            }.bind(this);
            b.Al = function() {
                d && (d.remove(), this.Sd.splice(this.Sd.indexOf(d), 1))
            }.bind(this)
        }
    };
    var bh = function(a) {
        for (var b = [], c = a.Xc.cues, d = a.ak.ac(), e = 0; e < c.length;) {
            var f = c[e];
            f.endTime < d ? a.Xc.removeCue(f) : (b.push(f), e++)
        }
        return b
    };
    $g.prototype.parse = function(a, b) {
        for (a = this.ud.parse(a, b); 0 < a.length;) this.addCue(a.pop())
    };
    var ch = function(a) {
        G.call(this);
        this.pb = null;
        this.jh = a
    };
    m(ch, G);
    ch.prototype.xg = function() {
        this.pb && (this.pb.onprogress = null);
        this.pb = G.prototype.xg.call(this);
        this.pb.onprogress = this.jh;
        return this.pb
    };
    ch.prototype.G = function() {
        this.pb && (this.pb.onprogress = null);
        G.prototype.G.call(this)
    };
    var eh = function() {
        var a = dh,
            b = this;
        return new Xc(function(c, d) {
            var e = new ch(function() {});
            D(e, "success", function(a) {
                c(Ed(a.target))
            }, !1, b);
            D(e, "error", d, !1, b);
            D(e, "timeout", d, !1, b);
            e.send(a)
        })
    };
    var fh = function(a, b, c) {
            var d = y();
            this.clientId = a;
            this.blockIds = b;
            this.flags = c;
            this.timestamp = d
        },
        hh = function() {
            var a = gh();
            this.ug = a && a.clientId || Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)
        };
    hh.prototype.Nc = function(a) {
        Aa("Cast.MPL.ExperimentationFetchResult", !0);
        try {
            var b = Gc(a.substring(5));
            var c = new gf(b[0])
        } catch (g) {
            ih(this, "Failed to parse experimentation flags.");
            return
        }
        a = He(c, 1);
        b = Ne(c, hf, 2);
        c = {};
        b = p(b);
        for (var d = b.next(); !d.done; d = b.next()) {
            d = d.value;
            var e = d.getName(),
                f = void 0;
            switch (Fe(d, Le[0])) {
                case 2:
                    f = K(d, 5);
                    break;
                case 3:
                    f = K(d, 3);
                    break;
                case 4:
                    f = d.getFloatValue();
                    break;
                case 5:
                    f = K(d, 5)
            }
            c[e] = f
        }
        jh("Cast.MPL.ExperimentationDownloadedBlockId", a);
        window.localStorage.setItem("cast.player.common.Experimentation",
            JSON.stringify(new fh(this.ug, a, c)));
        F(kh, "Flags: " + c.toString())
    };
    hh.prototype.Db = function() {
        Aa("Cast.MPL.ExperimentationFetchResult", !1);
        ih(this, "Failed to download experimentation flags.")
    };
    var ih = function(a, b) {
            E(kh, b);
            window.localStorage.setItem("cast.player.common.Experimentation", JSON.stringify(new fh(a.ug, [], {})))
        },
        jh = function(a, b) {
            if (u(b) && 0 < b.length) {
                b = p(b);
                for (var c = b.next(); !c.done; c = b.next()) A(a, c.value)
            }
        },
        mh = function(a) {
            if (!lh()) {
                var b = gh();
                if (u(b) && u(b.flags)) return b.flags[a]
            }
        },
        lh = function() {
            var a = gh();
            if (!u(a) || !u(a.timestamp)) return !0;
            var b = y() - a.timestamp;
            a = a.flags.expirationHrs;
            x(a) || (a = 3);
            return b > 36E5 * a
        },
        gh = function() {
            var a = window.localStorage.getItem("cast.player.common.Experimentation");
            return a ? JSON.parse(a) : void 0
        };
    ma(hh);
    var kh = B("cast.player.common.Experimentation"),
        nh = hh.kf(),
        oh = lh();
    Aa("Cast.MPL.ExperimentationCheckExpired", oh);
    if (oh) {
        var dh = "https://$Env$.google.com/cast/senderconfig/config?hw=mpl&id=$ClientId$".replace(/\$Env\$/g, "clients3").replace(/\$ClientId\$/g, nh.ug.toString());
        eh().then(nh.Nc.bind(nh), nh.Db.bind(nh))
    };
    var ph = function(a, b) {
        this.ca = a;
        this.Yd = [];
        for (var c = 0; c < a.length; c++) this.Yd.push(c);
        this.Dk = u(b) ? b : 3;
        this.Ge = {};
        this.gd = this.Yd.slice()
    };
    ph.prototype.li = function() {
        return 0
    };
    ph.prototype.Gh = function() {
        this.Ge = {};
        this.gd = this.Yd.slice()
    };
    ph.prototype.Re = function(a) {
        this.Ge[a] || (this.Ge[a] = 0);
        this.Ge[a]++;
        this.Ge[a] > (1 == this.gd.length ? this.Dk : this.li()) && (a = this.gd.indexOf(a), -1 < a && this.gd.splice(a, 1));
        return this.gd
    };
    var qh = function(a, b) {
        ph.call(this, a, b)
    };
    m(qh, ph);
    qh.prototype.Re = function(a, b) {
        for (var c = 0, d = this.ca[0], e = 1; e < this.ca.length; e++) this.ca[e] < d && (d = this.ca[e], c = e);
        if (!b || b && a == c) this.gd = [c];
        return ph.prototype.Re.call(this, a, b)
    };
    var rh = function(a, b) {
        ph.call(this, a, b);
        this.Fe = Array(this.ca.length);
        this.Fe.fill(0);
        a = mh("numRetriesPerQualityLevel");
        this.Ek = x(a) ? a : 1;
        a = mh("blacklistThreshold");
        this.gk = x(a) ? a : 6
    };
    m(rh, ph);
    rh.prototype.li = function() {
        return this.Ek
    };
    rh.prototype.Gh = function(a) {
        this.Fe[a] = 0;
        ph.prototype.Gh.call(this, a)
    };
    rh.prototype.Re = function(a, b) {
        this.Fe[a] += 1;
        if (this.Fe[a] >= this.gk) {
            var c = this.Yd.indexOf(a); - 1 < c && this.Yd.splice(c, 1)
        }
        return ph.prototype.Re.call(this, a, b)
    };
    var uh = function(a, b, c, d) {
        this.a = a;
        this.N = b;
        this.ca = c;
        a = this.a.initialBandwidth;
        3145728 == this.a.initialBandwidth && (a = parseFloat(window.localStorage.getItem("cast.player.core.QualityManager.currentBandwidth")), a = x(a) && a > this.a.initialBandwidth ? a : this.a.initialBandwidth);
        this.Ub = a;
        this.Ac = sh(this, this.Ub);
        F(th, this.N + ": initial " + this.ca[this.Ac]);
        this.Ae = 0;
        this.ag = null;
        this.Ii = y();
        this.Ck = d;
        a = this.ca;
        d = Jd(d);
        if (u(d))
            for (A("Cast.MPL.Available" + d + "Bitrates", a.length), b = 0; b < a.length; b++) A("Cast.MPL.Available" +
                d + "Bitrate" + b, a[b])
    };
    uh.prototype.M = function() {
        vh(this)
    };
    var sh = function(a, b, c) {
            for (var d = -1, e = Number.MAX_VALUE, f = u(c) ? c[0] : 0, g = 0; g < a.ca.length; g++)
                if (!u(c) || -1 !== c.indexOf(g)) {
                    if (b >= a.ca[g]) {
                        var k = b - a.ca[g];
                        k < e && (d = g, e = k)
                    }
                    a.ca[f] > a.ca[g] && (f = g)
                }
            return 0 > d ? f : d
        },
        wh = function(a, b, c, d) {
            var e = y();
            if (!b && null !== a.ag && e - a.ag < 2E3 * (c || 0)) return a.Ac;
            a.ag = e;
            b = sh(a, .7 * a.Ub, d);
            a.a.getQualityLevel && (b = a.a.getQualityLevel(a.N, b), b >= a.ca.length && (E(th, a.N + ": Host provided an invalid quality level!"), b = a.ca.length - 1));
            b != a.Ac && (F(th, a.N + ": from " + a.ca[a.Ac] + " to " + a.ca[b]),
                vh(a), a.Ac = b);
            return a.Ac
        },
        yh = function(a, b) {
            3E5 > y() - a.Ii || (A("Cast.MPL.Bandwidth", b), xh || window.localStorage.setItem("cast.player.core.QualityManager.currentBandwidth", "" + b), vh(a))
        },
        vh = function(a) {
            var b = y(),
                c = Jd(a.Ck);
            u(c) && A("Cast.MPL." + c + "Bitrate", a.ca[a.Ac]);
            a.Ii = b
        },
        th = B("cast.player.core.QualityManager"),
        xh = document.domain.includes("gstatic.com");
    var zh = function() {};
    z("cast.player.core.ProcessSourceDataCallback", zh);
    zh.prototype.ze = function() {};
    var Ah = function(a, b, c, d, e) {
        C.call(this);
        this.a = a;
        this.Uk = e;
        this.rc = c;
        this.ba = d;
        this.N = b;
        this.Te = !1;
        this.ja = this.La = this.lc = null;
        this.Zb = new ge;
        this.hd = 0;
        this.createBuffer();
        this.vc = null
    };
    m(Ah, C);
    h = Ah.prototype;
    h.G = function() {
        C.prototype.G.call(this);
        this.Zb.clear();
        this.ja && (wc(this.ja, "updateend", this.gj, !1, this), "closed" != this.ba.readyState && this.ba.removeSourceBuffer(this.ja))
    };
    h.createBuffer = function() {
        if (null === this.ja && "open" == this.ba.readyState) {
            var a = this.rc.mimeType + '; codecs="' + If(this.rc.codecs) + '"';
            F(R, "buffer type: " + a);
            try {
                this.ja = this.ba.addSourceBuffer(a)
            } catch (b) {
                this.a.R(110, void 0, b.message);
                return
            }
            D(this.ja, "updateend", this.gj, !1, this);
            Bh(this)
        }
    };
    h.va = function(a) {
        var b = this.hd + (this.lc ? this.lc.duration : 0);
        return this.La ? this.La.time + this.La.duration - a + b : b
    };
    h.reset = function() {
        this.lc = this.La = null;
        this.Zb.clear();
        this.hd = 0;
        this.ee() ? Ch(this) : (F(R, this.N + ": queue remove"), this.Zb.enqueue({
            kh: 1,
            kg: null
        }));
        this.ee() ? Dh(this) : (F(R, this.N + ": queue abort"), this.Zb.enqueue({
            kh: 0,
            kg: null
        }))
    };
    h.ee = function() {
        return null !== this.ja && !this.Te && this.Zb.pa()
    };
    h.ki = function() {
        return Fd(this.rc.mimeType) ? 40 : 20
    };
    var Bh = function(a) {
        for (; !a.Zb.pa();) {
            var b = a.Zb.jd(),
                c = b.kg;
            switch (b.kh) {
                case 2:
                    F(R, a.N + ": dequeue append");
                    a.hd -= c.interval.duration;
                    Eh(a, c.data, c.interval, c.O, c.U);
                    return;
                case 1:
                    F(R, a.N + ": dequeue remove");
                    Ch(a);
                    return;
                case 0:
                    F(R, a.N + ": dequeue abort"), Dh(a)
            }
        }
    };
    Ah.prototype.gj = function() {
        for (var a = this.ja.buffered, b = 0; b < a.length; b++) F(R, this.N + ": " + a.start(b) + " - " + a.end(b));
        F(R, this.N + ": updateend");
        null !== this.lc && 0 != this.lc.duration && (this.La = this.lc);
        this.lc = null;
        this.Te = !1;
        Bh(this)
    };
    var Eh = function(a, b, c, d, e) {
            var f = a.ja.timestampOffset;
            if (e) {
                Dh(a);
                var g = Fh(a, c.time);
                a.ja.timestampOffset = g - d;
                F(R, a.N + ": timestampOffset = " + a.ja.timestampOffset)
            } else g = Fh(a, c.time);
            a.Te = !0;
            try {
                a.ja.appendBuffer(b)
            } catch (k) {
                F(R, a.N + ": append failed " + k);
                a.Te = !1;
                e && (a.ja.timestampOffset = f);
                Gh(a, b, c, d, e, k);
                return
            }
            F(R, a.N + ": successfully append " + b.length + " bytes");
            null !== a.vc && (F(R, a.N + ": cleared interval " + a.vc), clearInterval(a.vc), a.vc = null);
            a.lc = {
                time: g,
                duration: c.duration
            };
            a.Uk.ze(a.rc.mimeType,
                b, a.ja.timestampOffset, g, c.duration)
        },
        Dh = function(a) {
            "open" == a.ba.readyState ? (F(R, a.N + ": abort"), a.ja.abort()) : E(R, a.N + ": unable to abort")
        },
        Ch = function(a) {
            F(R, a.N + ": remove");
            a.Te = !0;
            a.ja.remove(0, Infinity)
        };
    Ah.prototype.append = function(a, b, c, d) {
        this.ee() ? Eh(this, a, b, c, d) : Gh(this, a, b, c, d)
    };
    var Gh = function(a, b, c, d, e, f) {
            F(R, a.N + ": delay append, append to deferred queue");
            a.hd += c.duration;
            a.Zb.enqueue({
                kh: 2,
                kg: {
                    data: b,
                    interval: {
                        time: c.time,
                        duration: c.duration
                    },
                    O: d,
                    U: e
                }
            });
            f && 22 === f.code && null === a.vc && (a.vc = setInterval(function() {
                Bh(a)
            }, 2E3), F(R, a.N + ": set up interval to processDeferred_ " + a.vc))
        },
        Fh = function(a, b) {
            if (!a.La || a.a.hk) return b;
            var c = b,
                d = a.ja.buffered;
            0 < d.length && (b = d.end(d.length - 1));
            F(R, a.N + ": adjustTime: " + c + " : " + b);
            return b
        },
        R = B("cast.player.core.SourceBufferManager");
    var Hh = function(a, b, c, d, e, f) {
        C.call(this);
        this.Aa = a;
        this.host = b;
        this.protocol = c;
        this.I = d;
        c = c.getStreamInfo(d);
        this.wc = vf(c.role, c.mimeType, c.codecs) ? new $g(a, b, c.codecs, c.mimeType) : new Ah(b, d, c, e, a);
        Od(c.codecs);
        A("Cast.MPL.MimeType", Nd(c.mimeType));
        A("Cast.MPL.StreamType", Fd(c.mimeType) ? 1 : Gd(c.mimeType) ? 2 : vf(c.role, c.mimeType, c.codecs) ? 3 : 0);
        this.xf = !1;
        this.g = 1;
        this.Nf = 0;
        this.Gd = !1;
        this.Mh = this.mb = 0;
        this.qb = 400;
        this.ec = this.Gc = this.nd = this.yf = !1;
        this.Hi = this.lg = 0;
        this.cb = new ge;
        this.u = new lf;
        this.u.setResponse =
            this.Zk.bind(this);
        this.f = new ch(this.jh.bind(this));
        td(this.f);
        D(this.f, "success", this.Nc, !1, this);
        D(this.f, "error", this.Db.bind(this, !1));
        D(this.f, "timeout", this.Db.bind(this, !0));
        D(this.f, "ready", this.Lk, !1, this);
        a = this.protocol.getStreamInfo(this.I).bitrates;
        this.Sc = new uh(b, d, a, c.mimeType);
        this.bf = Qf.kf();
        this.Fd = null;
        (b = f) || (b = this.host.segmentRequestRetryLimit, b = mh("simpleFailOver") ? new rh(a, b) : new qh(a, b));
        this.gi = b
    };
    m(Hh, C);
    Hh.prototype.G = function() {
        clearTimeout(this.Fd);
        this.Fd = null;
        this.wc.M();
        this.Sc.M();
        this.f.M();
        C.prototype.G.call(this)
    };
    Hh.prototype.Zk = function(a, b) {
        a ? this.pf({
            Fc: this.ec,
            data: new Uint8Array(a),
            interval: this.u.interval,
            mc: this.u.mc
        }, this.u, b) : this.Db(!1);
        this.yf = !1;
        this.nd && (Ih(this), this.nd = !1)
    };
    var Kh = function(a) {
            a: {
                var b = a.bf;
                var c = a.host.url,
                    d = a.I;
                if (b.isEnabled() && b.He.Xb(c) && (b = b.He.get(c)[d], x(b))) break a;b = -1
            } - 1 < b ? Jh(a, b) : Jh(a, wh(a.Sc, !0, void 0, void 0))
        },
        Jh = function(a, b) {
            var c = a.protocol.getQualityLevel(a.I);
            if (b == c) return !1;
            a.xf = !0;
            a.protocol.Rf(a.I, b, a);
            return !0
        },
        Mh = function(a, b, c, d, e) {
            if (a.Gc) {
                F(Lh, a.I + ": segment processed");
                a.Gc = !1;
                if (a.host.processSegment) {
                    var f = a.host.processSegment(a.I, b);
                    f instanceof Uint8Array && (b = f)
                }
                a.wc.append(b, c, d, e)
            } else F(Lh, a.I + ": not processing");
            a.qh()
        },
        Ph = function(a, b) {
            a.Gc ? Nh(a, b, "processing previous segment") : a.cb.pa() ? Oh(a, b) ? a.ye(b) : (Nh(a, b, "segment duration will go beyond buffer limit"), a.Fd = setTimeout(a.qh.bind(a), 1E3)) : Nh(a, b, "queue has " + a.cb.tb() + " segments")
        },
        Nh = function(a, b, c) {
            F(Lh, a.I + ": queue segment (" + b.data.length + ") as " + c);
            a.cb.enqueue(b)
        };
    Hh.prototype.qh = function() {
        if (!this.Gc && !this.cb.pa()) {
            var a = this.cb.oh();
            Oh(this, a) ? (F(Lh, this.I + ": dequeue segment"), this.cb.jd(), this.ye(a)) : this.Fd = setTimeout(this.qh.bind(this), 1E3)
        }
    };
    var Oh = function(a, b) {
        var c = a.va(a.Aa.ac());
        return c <= a.host.autoPauseDuration ? !0 : c + b.interval.duration <= a.wc.ki()
    };
    Hh.prototype.ye = function(a) {
        F(Lh, this.I + ": process segment");
        this.Gc = !0;
        this.protocol.processSegment(this.I, a, this)
    };
    Hh.prototype.reset = function() {
        F(Lh, this.I + ": reset");
        this.g = 1;
        this.Nf = 0;
        this.Gd = !1;
        Qh(this);
        this.Gc = this.nd = !1;
        this.cb.clear();
        clearTimeout(this.Fd);
        this.Fd = null;
        this.ec || this.f.abort();
        this.protocol.reset(this.I);
        this.wc.reset();
        this.Aa.jc()
    };
    Hh.prototype.Nc = function(a) {
        var b = this.protocol.getQualityLevel(this.I);
        this.gi.Gh(b);
        this.pf({
            Fc: this.ec,
            headers: this.f.getAllResponseHeaders(),
            data: new Uint8Array(Ed(a.target)),
            interval: this.u.interval,
            mc: this.u.mc
        }, this.u)
    };
    Hh.prototype.pf = function(a, b, c) {
        if (!a.ji) {
            c = u(c) ? c : y() - this.Nf;
            b = this.Sc;
            var d = a.data.length;
			totalDRMBandWidth = parseInt(parseInt(d)+totalDRMBandWidth);
			console.log("Prakash  length is="+d+"Total Bandwidth="+totalDRMBandWidth);
            0 >= c || (b.a.trackBandwidth && b.a.trackBandwidth(b.N, c, d), Nf("Cast.MPL.SegmentStats.Size", d), Nf("Cast.MPL.SegmentStats.Time", c), c = 0 < c ? 8 * d / (c / 1E3) : 0, b.Ub = .8 * c + (1 - .8) * b.Ub, F(th, b.N + ": current=" + c.toFixed(2) + ", average=" + b.Ub.toFixed(2)), yh(b, c), b.Ae = 0)
        }
        Qh(this);
        Ph(this, a);
        if (!Rh(this, a) && (b = this.va(this.Aa.ac()), a.ji || Jh(this, wh(this.Sc, 10 > b, a.interval.duration, void 0)), this.lg = .8 * b + (1 - .8) * this.lg, 3E5 < y() - this.Hi &&
                (A("Cast.MPL.AverageBufferDuration", parseInt(this.lg, 10)), this.Hi = y()), this.xf)) return;
        this.Aa.jc()
    };
    var Rh = function(a, b) {
        return a.ec ? (a.ec = !1, a.protocol.xj(a.I, b.data), !0) : a.Gd = !1
    };
    h = Hh.prototype;
    h.Lk = function() {
        this.nd && (Ih(this), this.nd = !1)
    };
    h.Db = function(a) {
        var b = this.Sc,
            c = y() - this.Nf;
        b.a.trackBandwidth && b.a.trackBandwidth(b.N, c, b.Ae);
        a && (b.Ub = 0 < c ? 8 * b.Ae / (c / 1E3) : 0);
        b.Ae = 0;
        b.ag = null;
        F(th, b.N + ": current=" + b.Ub.toFixed(2));
        yh(b, b.Ub);
        b = this.protocol.getQualityLevel(this.I);
        a = this.gi.Re(b, a);
        0 == a.length ? this.host.il ? (Qh(this), this.Aa.jc()) : (this.g = -1, a = this.yf ? void 0 : new mf(this.u.url, this.f.zb, Cd(this.f), this.f.getAllResponseHeaders(), Ed(this.f)), this.host.R(301, a)) : Jh(this, wh(this.Sc, !0, void 0, a)) ? (this.Gd = !0, this.Aa.jc()) : (this.mb++,
            this.Mh = y(), this.Aa.jc(this.qb))
    };
    h.jh = function(a) {
        this.Sc.Ae = a.loaded
    };
    h.va = function(a) {
        return this.wc.va(a)
    };
    h.createBuffer = function() {
        this.wc.createBuffer()
    };
    var Th = function(a) {
            a.xf = !1;
            a.Aa.dh();
            Sh(a, !1);
            a.ec ? Ih(a) : a.Aa.jc()
        },
        Ih = function(a) {
            if (Uh(a)) a.nd = !0;
            else {
                a.Nf = y();
                a.protocol.updateSegmentRequestInfo(a.I, a.u);
                var b = a.u.interval;
                a.u.timeoutInterval = Math.max(2E3 * b.duration, 1E4);
                a.u.skipRequest = !1;
                if (a.host.updateSegmentRequestInfo && (a.host.updateSegmentRequestInfo(a.u, a.I), a.u.skipRequest)) {
                    a.yf = !0;
                    return
                }
                Wf(a.bf, a.u) ? Xf(a.bf, a.u).then(function(c) {
                    a.pf({
                        Fc: a.ec,
                        data: new Uint8Array(c),
                        interval: b,
                        mc: a.u.mc,
                        ji: !0
                    }, a.u)
                }, function() {
                    Vh(a)
                }) : Vh(a)
            }
        },
        Vh =
        function(a) {
            a.f.Kd = a.u.withCredentials;
            a.f.Ob = Math.max(0, a.u.timeoutInterval);
            a.f.send(a.u.url, void 0, void 0, a.u.headers)
        };
    Hh.prototype.ce = function() {
        return 0 < this.g || this.Gc || Uh(this) || !this.wc.ee() || !this.cb.pa()
    };
    Hh.prototype.Vg = function() {
        var a;
        if (a = !this.xf && 2 > this.cb.tb() && !Uh(this)) a = this.cb.oh(), a = !(a && !Oh(this, a));
        return a
    };
    var Uh = function(a) {
            return a.f.ce() || a.yf
        },
        Sh = function(a, b) {
            if (a.protocol.qd(a.I)) {
                var c = a.protocol.Ud(a.I);
                null === c ? (a.ec = !0, a.Gd = a.Gd || b) : Ph(a, {
                    Fc: !0,
                    data: c,
                    interval: {
                        time: a.u.interval.time,
                        duration: 0
                    }
                })
            }
        };
    Hh.prototype.Ie = function() {
        if (0 < this.mb || this.Gd) {
            if (0 < this.mb) {
                if (y() - this.Mh < this.qb) return;
                this.qb *= 2
            }
            Ih(this)
        } else if (1 == this.g) {
            var a = this.Aa.ac(),
                b = this.protocol.seek(this.I, a);
            x(b) ? (F(Lh, this.I + ": seek success " + a), this.g = 2, this.Aa.gh(this.I, b), Sh(this, !0), Ih(this)) : F(Lh, this.I + ": seek failure " + a)
        } else this.protocol.fc(this.I) && (Sh(this, !0), Ih(this)), this.protocol.vb(this.I) && (this.g = 0)
    };
    var Qh = function(a) {
            a.mb = 0;
            a.Mh = 0;
            a.qb = 400
        },
        Lh = B("cast.player.core.SegmentManager");
    var Wh = function(a, b, c, d, e) {
        Hh.call(this, a, b, c, d, new MediaSource);
        this.Zh = e;
        this.qg = 0
    };
    m(Wh, Hh);
    Wh.prototype.pf = function(a, b) {
        var c = this,
            d = Rh(this, a) ? 0 : this.protocol.getSegmentInterval(this.I).duration;
        Tf(this.bf, b, a.data.buffer).then(function() {
            c.qg += d;
            var b = c.Zh,
                f = c.I,
                g = a.data.buffer.byteLength;
            b.w.vb(f) ? (F(Xh, "Cached the entire stream " + f), Yh(b, f)) : b.Ed.va(b.bb.Qg) >= b.a.autoResumeDuration ? (F(Xh, "Successfully cached stream " + f), Yh(b, f)) : b.Kb.zd < g ? (E(Xh, "Cache likely can not fit the next segement for stream " + f), Yh(b, f)) : b.Ed.Ie()
        }, function(a) {
            E(Zh, "Unable to cache segment (" + b.url + ") because of " +
                a);
            Yh(c.Zh, c.I)
        })
    };
    Wh.prototype.va = function() {
        return this.qg
    };
    var Zh = B("cast.player.cache.SegmentManager");
    var $h = function() {};
    z("cast.player.core.CurrentTimeCallback", $h);
    $h.prototype.ac = function() {};
    var ai = function() {};
    z("cast.player.core.PlayerCallbacks", ai);
    ai.prototype.jc = function() {};
    ai.prototype.gh = function() {};
    ai.prototype.dh = function() {};
    var bi = function(a) {
        C.call(this);
        this.ll = a;
        this.Mg = this.Zc = null;
        this.mb = 0;
        this.sh = null;
        this.qb = 400;
        this.f = new ch(function() {});
        D(this.f, "success", this.Nc.bind(this));
        D(this.f, "error", this.Db.bind(this));
        D(this.f, "timeout", this.Mk.bind(this))
    };
    m(bi, C);
    bi.prototype.G = function() {
        this.f.M();
        C.prototype.G.call(this)
    };
    bi.prototype.load = function(a) {
        var b = this;
        if (this.Zc = a.url) {
            var c = Qf.kf();
            Wf(c, a) ? Xf(c, a).then(function(a) {
                b.Bd(String.fromCharCode.apply(String, [].concat(ja(new Uint16Array(a)))))
            }, function() {
                ci(b, a)
            }) : ci(this, a)
        } else E(uf, "No url provided for request")
    };
    var ci = function(a, b) {
        a.Mg = b.headers;
        a.f.Kd = b.withCredentials;
        a.f.Ob = Math.max(0, b.timeoutInterval);
        a.mb = 0;
        a.f.send(a.Zc, void 0, void 0, a.Mg)
    };
    bi.prototype.abort = function() {
        this.f.abort()
    };
    var di = function(a) {
        return new mf(a.Zc, a.f.zb, Cd(a.f), a.f.getAllResponseHeaders(), Ed(a.f))
    };
    bi.prototype.Nc = function(a) {
        clearTimeout(this.sh);
        this.qb = 400;
        this.Bd(Ed(a.target))
    };
    bi.prototype.Db = function() {
        ei(this, 408 == Cd(this.f) ? 0 : this.qb)
    };
    bi.prototype.Mk = function() {
        ei(this, 0)
    };
    var ei = function(a, b) {
        a.mb++;
        clearTimeout(a.sh);
        3 < a.mb ? a.Bd(null) : (1 < a.mb && (a.qb = 2 * b), a.sh = setTimeout(function() {
            a.f.send(a.Zc, void 0, void 0, a.Mg)
        }, b))
    };
    bi.prototype.Bd = function(a) {
        this.mb = 0;
        var b = this.f;
        this.ll.kc(a, b.pb && b.pb.responseURL ? b.pb.responseURL : null)
    };
    var fi = function(a) {
        bi.call(this, a);
        this.u = this.a = null
    };
    m(fi, bi);
    fi.prototype.od = function(a, b) {
        this.a = a;
        this.u = new lf;
        this.u.url = this.Zc = b;
        this.u.setResponse = this.Bd.bind(this);
        a.updateManifestRequestInfo && a.updateManifestRequestInfo(this.u);
        this.u.skipRequest || this.load(this.u)
    };
    fi.prototype.Bd = function(a) {
        this.a && (a = this.a.lj(a, this.u));
        bi.prototype.Bd.call(this, a)
    };
    var gi = function(a) {
            if (a[7] << 8 | 1 != a[6]) return E(uf, "PlayReady header object is not valid"), null;
            var b = a[9] << 8 | a[8];
            b += 10;
            for (var c = "", d = 10; d < b; d += 2) c += String.fromCharCode(a[d + 1] << 8 | a[d]);
            d = b = null;
            c = (new DOMParser).parseFromString(c.trim(), "text/xml").childNodes[0].firstElementChild;
            c = p(c.children);
            for (var e = c.next(); !e.done; e = c.next()) e = e.value, "LA_URL" == e.nodeName ? d = e.textContent : "KID" == e.nodeName && (b = Ja(window.atob(e.textContent)));
            return b ? {
                systemId: yf,
                Xd: a,
                ie: new xf(b, !0),
                url: d,
                Bf: 8
            } : (E(uf, "PlayReady rights management header does not have KID"),
                null)
        },
        S = function(a) {
            this.host = a;
            this.Qc = this.Kc = this.uri = null;
            this.Z = void 0;
            this.duration = -1;
            this.l = [];
            this.Pg = []
        },
        ii = function(a) {
            var b = hi(a.l);
            b || (b = a.l.find(function(a) {
                return 1 == a.type && !Cf(a.codecs)
            }));
            b && (b.enabled = !0);
            a = p(a.l);
            for (b = a.next(); !b.done; b = a.next())
                if (b = b.value, 2 == b.type) {
                    b.enabled = !0;
                    break
                }
        },
        hi = function(a) {
            return a.find(function(a) {
                return 1 == a.type && Cf(a.codecs) && Bf("audio/mp4", a.codecs)
            })
        };
    S.prototype.getDefaultAudioStreamIndex = function() {
        for (var a = -1, b = 0; b < this.l.length; b++) {
            var c = this.l[b];
            if (1 == c.type && Bf("audio/mp4", c.codecs)) {
                if (Cf(c.codecs)) return b;
                0 > a && (a = b)
            }
        }
        return a
    };
    S.prototype.getDefaultAudioStreamIndex = S.prototype.getDefaultAudioStreamIndex;
    S.prototype.getStreamCount = function() {
        return this.l.length
    };
    S.prototype.getStreamCount = S.prototype.getStreamCount;
    S.prototype.enableStream = function(a, b) {
        var c = this.l[a];
        c.index = -1;
        c.S = -1;
        c.U = !0;
        c.enabled = b;
        c.Na = !1;
        this.Pg[a] = []
    };
    S.prototype.enableStream = S.prototype.enableStream;
    S.prototype.isStreamEnabled = function(a) {
        return this.l[a].enabled
    };
    S.prototype.isStreamEnabled = S.prototype.isStreamEnabled;
    S.prototype.getQualityLevel = function(a) {
        return this.l[a].S
    };
    S.prototype.getQualityLevel = S.prototype.getQualityLevel;
    S.prototype.getStreamInfo = function(a) {
        a = this.l[a];
        for (var b = [], c = p(a.v), d = c.next(); !d.done; d = c.next()) b.push(d.value.ka);
        return new nf(a.codecs, a.mimeType, b, a.language, a.name, a.role)
    };
    S.prototype.getStreamInfo = S.prototype.getStreamInfo;
    h = S.prototype;
    h.kc = function() {
        ii(this);
        if (this.host.onManifestReady) this.host.onManifestReady();
        this.Qc.onManifestReady()
    };
    h.load = function(a) {
        this.Qc = a;
        this.od()
    };
    h.od = function() {
        var a = this.host.url;
        this.uri = new I(a);
        this.Kc = new fi(this);
        this.Kc.od(this.host, a)
    };
    h.Qb = function() {
        this.Kc && (this.Kc.M(), this.Kc = null)
    };
    h.Rf = function(a, b, c) {
        a = this.l[a];
        a.S = b;
        a.Na = !0;
        Th(c)
    };
    h.updateLicenseRequestInfo = function() {};
    h.getDuration = function() {
        return this.Z ? -1 : this.duration
    };
    h.wa = function(a) {
        a = this.l[a];
        var b = a.S;
        if (0 > b) return null;
        a = a.v[b];
        var c = a.c;
        if (0 == c.length) return null;
        b = c[0].time;
        var d = c.length - 1;
        c = c[d].time + c[d].duration;
        this.Z && (c -= 20, c < b && (c = b));
        return {
            start: b - a.O,
            end: c - a.O
        }
    };
    h.seek = function(a, b) {
        var c = this.l[a],
            d = c.v[c.S],
            e = d.c;
        a = this.wa(a);
        if (!a) return null;
        b < a.start && (b = a.start);
        b > a.end && (b = a.end);
        b += d.O;
        for (a = e.length - 1; 0 <= a; a--)
            if (b >= e[a].time) return c.index = a, c.Na = !0, b - d.O;
        return null
    };
    h.fc = function(a) {
        a = this.l[a];
        var b = a.index + 1;
        return b < a.v[a.S].c.length ? (a.index = b, !0) : !1
    };
    h.vb = function(a) {
        var b = this.l[a];
        a = b.index;
        b = b.v[b.S].c;
        return !this.Z && a == b.length - 1
    };
    h.getSegmentInterval = function(a) {
        var b = this.l[a],
            c = b.S;
        return 0 <= c && (a = b.index, 0 <= a && (b = b.v[c], c = b.c, a < c.length)) ? {
            time: c[a].time - b.O,
            duration: c[a].duration
        } : {
            time: 0,
            duration: 0
        }
    };
    h.reset = function(a) {
        this.l[a].index = -1
    };
    h.qd = function(a) {
        a = this.l[a];
        var b = a.v[a.S];
        null !== b.ib || null !== b.F && null !== b.F.da || (a.Na = !1);
        return a.Na
    };
    h.xj = function(a, b) {
        this.l[a].Na = !1;
        262144 < b.length ? E(ji, "init data (" + b.length + " bytes) is too large to cache") : this.Pg[a][this.l[a].S] = b
    };
    h.Ud = function(a) {
        var b = this.l[a],
            c = this.Pg[a];
        return (b = b && c ? c[b.S] || null : null) ? (this.l[a].Na = !1, b) : null
    };
    h.updateSegmentRequestInfo = function(a, b) {
        b.interval = this.getSegmentInterval(a)
    };
    h.processSegment = function() {};
    h.lf = function() {
        return 0
    };
    var ji = B("cast.player.comon.StreamingProtocolBase");
    var T = function(a) {
        this.name = a;
        this.zi = !1
    };
    T.prototype.parse = function() {
        this.zi = !0
    };
    var U = function(a) {
        T.call(this, a);
        this.value = null
    };
    m(U, T);
    U.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        u(a) && -1 < a.indexOf("/") ? (a = a.split("/"), this.value = parseInt(a[0], 10) / (1 < a.length ? parseInt(a[1], 10) : 1)) : this.value = parseInt(a, 10)
    };
    var V = function(a, b) {
        T.call(this, a);
        this.value = b || null
    };
    m(V, T);
    V.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        this.value = a
    };
    var ki = function(a) {
        T.call(this, a);
        this.value = null
    };
    m(ki, T);
    ki.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        this.value = "true" == a.toLowerCase()
    };
    var li = function(a) {
        T.call(this, a);
        this.value = null
    };
    m(li, T);
    li.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        if (a = a.match(be)) {
            var b = !(a[6] || a[7] || a[8]);
            if (b && !(a[2] || a[3] || a[4]) || b && a[5]) a = null;
            else {
                b = parseInt(a[2], 10) || 0;
                var c = parseInt(a[3], 10) || 0,
                    d = parseInt(a[4], 10) || 0,
                    e = parseInt(a[6], 10) || 0,
                    f = parseInt(a[7], 10) || 0,
                    g = parseFloat(a[8]) || 0;
                a = a[1] ? new ce(-b, -c, -d, -e, -f, -g) : new ce(b, c, d, e, f, g)
            }
        } else a = null;
        this.value = 60 * (60 * (24 * a.Yb + a.cc) + a.ic) + a.nc
    };
    var mi = function(a) {
        T.call(this, a);
        this.value = null
    };
    m(mi, T);
    mi.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        a = a.toUpperCase();
        for (var b in ni)
            if (ni.hasOwnProperty(b) && a.includes(ni[b])) {
                this.value = ni[b];
                break
            }
    };
    var ni = {
            PLAYREADY: "9A04F079-9840-4286-AB92-E65BE0885F95",
            WIDEVINE: "EDEF8BA9-79D6-4ACE-A3C8-27DCD51D21ED",
            CLEARKEY: "1077EFEC-C0B2-4D02-ACE3-3C1E52E2FB4B",
            rl: "URN:MPEG:DASH:MP4PROTECTION:2011"
        },
        oi = function(a) {
            T.call(this, a);
            this.value = null
        };
    m(oi, T);
    oi.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        a = a.toUpperCase();
        a.includes("9A04F079-9840-4286-AB92-E65BE0885F95") ? this.value = yf : a.includes("EDEF8BA9-79D6-4ACE-A3C8-27DCD51D21ED") ? this.value = zf : a.includes("1077EFEC-C0B2-4D02-ACE3-3C1E52E2FB4B") && (this.value = Af)
    };
    var pi = function(a, b) {
        for (var c in b)
            if (b.hasOwnProperty(c) && b[c] instanceof T) {
                var d = b[c];
                if (!d.zi) {
                    var e = a.getNamedItem(d.name);
                    e && d.parse(e.value)
                }
            }
    };
    var ri = function(a) {
            this.duration = new li("mediaPresentationDuration");
            this.type = new V("type");
            this.Ij = new li("minimumUpdatePeriod");
            for (this.url = null; a;) {
                if ("MPD" == a.nodeName) {
                    pi(a.attributes, this);
                    break
                }
                a = a.nextElementSibling
            }
            this.wd = [];
            if (a) {
                for (var b = p(a.children), c = b.next(); !c.done; c = b.next())
                    if (c = c.value, "BaseURL" == c.nodeName) {
                        this.url = c.textContent;
                        break
                    }
                a = p(a.children);
                for (c = a.next(); !c.done; c = a.next()) b = c.value, "Period" == b.nodeName && this.wd.push(new qi(b, this))
            }
        },
        si = function(a) {
            T.call(this,
                a);
            this.end = this.start = null
        };
    m(si, T);
    si.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        a = a.split("-");
        this.start = parseInt(a[0], 10);
        this.end = parseInt(a[1], 10)
    };
    var ti = function(a) {
            this.media = new V("media");
            pi(a.attributes, this)
        },
        ui = function(a) {
            this.Aj = new V("sourceURL");
            this.ta = new si("range");
            pi(a.attributes, this)
        },
        vi = function(a) {
            this.duration = new U("duration");
            this.Oe = new U("timescale");
            this.O = new V("presentationTimeOffset");
            this.H = new U("startNumber");
            this.Ec = new si("indexRange");
            pi(a.attributes, this);
            this.Uc = this.ae = null;
            a = p(a.children);
            for (var b = a.next(); !b.done; b = a.next()) switch (b = b.value, b.nodeName) {
                case "Initialization":
                    this.ae = new ui(b);
                    break;
                case "SegmentTimeline":
                    this.Uc = [];
                    b = p(b.children);
                    for (var c = b.next(); !c.done; c = b.next()) {
                        var d = c.value;
                        c = 0;
                        var e = d.attributes.getNamedItem("r");
                        e && (c = parseInt(e.value, 10));
                        e = (e = d.attributes.getNamedItem("t")) ? new og(e.value) : null;
                        if (d = d.attributes.getNamedItem("d"))
                            for (d = parseInt(d.value, 10), this.Uc.push({
                                    time: e,
                                    duration: d
                                }), e = 0; e < c; e++) this.Uc.push({
                                time: null,
                                duration: d
                            })
                    }
            }
        };
    vi.prototype.Ma = function(a) {
        null === a.duration.value && (a.duration = this.duration);
        null === a.Oe.value && (a.Oe = this.Oe);
        null === a.O.value && (a.O = this.O);
        null === a.H.value && (a.H = this.H);
        null === a.Ec.start && (a.Ec = this.Ec);
        null === a.ae && (a.ae = this.ae);
        null === a.Uc && (a.Uc = this.Uc)
    };
    var wi = function(a) {
        vi.call(this, a);
        this.c = [];
        a = p(a.children);
        for (var b = a.next(); !b.done; b = a.next()) switch (b = b.value, b.nodeName) {
            case "SegmentURL":
                this.c.push(new ti(b))
        }
    };
    m(wi, vi);
    wi.prototype.Ma = function(a) {
        vi.prototype.Ma.call(this, a);
        0 == a.c.length && (a.c = this.c)
    };
    var xi = function(a) {
        vi.call(this, a);
        this.media = new V("media");
        this.da = new V("initialization");
        pi(a.attributes, this)
    };
    m(xi, vi);
    xi.prototype.Ma = function(a) {
        vi.prototype.Ma.call(this, a);
        null === a.media.value && (a.media = this.media);
        null === a.da.value && (a.da = this.da)
    };
    var zi = function(a) {
            this.bl = new mi("schemeIdUri");
            pi(a.attributes, this);
            var b = this.bl.value;
            this.Be = yi(b, a);
            this.bc = "URN:MPEG:DASH:MP4PROTECTION:2011" == b
        },
        yi = function(a, b) {
            switch (a) {
                case "9A04F079-9840-4286-AB92-E65BE0885F95":
                    a = p(b.children);
                    for (b = a.next(); !b.done; b = a.next()) switch (b = b.value, b.nodeName) {
                        case "mspr:pro":
                            return gi(Ja(window.atob(b.childNodes[0].nodeValue.trim())));
                        case "cenc:pssh":
                            return a = Ja(window.atob(b.childNodes[0].nodeValue)), a = new Jg(a), gi(a.getData())
                    }
                    return Ai(yf);
                case "EDEF8BA9-79D6-4ACE-A3C8-27DCD51D21ED":
                    return Ai(zf);
                case "1077EFEC-C0B2-4D02-ACE3-3C1E52E2FB4B":
                    return Ai(Af);
                default:
                    return null
            }
        },
        Ai = function(a) {
            return {
                systemId: a,
                Xd: null,
                ie: null,
                url: null,
                Bf: 0
            }
        },
        Bi = function(a) {
            this.De = [];
            this.bc = !1;
            this.Gb = this.F = this.Hb = this.url = null;
            a = p(a.children);
            for (var b = a.next(); !b.done; b = a.next()) switch (b = b.value, b.nodeName) {
                case "BaseURL":
                    this.url = b.textContent;
                    break;
                case "ContentProtection":
                    b = new zi(b);
                    b.Be && this.De.push(b.Be);
                    this.bc = this.bc || b.bc;
                    break;
                case "SegmentBase":
                    this.Gb = new vi(b);
                    break;
                case "SegmentTemplate":
                    this.F =
                        new xi(b);
                    break;
                case "SegmentList":
                    this.Hb = new wi(b)
            }
        };
    Bi.prototype.Ma = function(a) {
        a.F ? this.F && this.F.Ma(a.F) : a.F = this.F;
        a.Gb ? this.Gb && this.Gb.Ma(a.Gb) : a.Gb = this.Gb;
        a.Hb ? this.Hb && this.Hb.Ma(a.Hb) : a.Hb = this.Hb;
        if (a.url && this.url) {
            var b = this.url;
            var c = a.url;
            b instanceof I || (b = te(b));
            c instanceof I || (c = te(c));
            b = b.resolve(c).toString()
        } else b = a.url || this.url;
        a.url = b;
        0 == a.De.length && (a.De = this.De);
        a.bc = a.bc || this.bc
    };
    var Ci = function(a) {
        Bi.call(this, a);
        this.id = new V("id");
        this.mimeType = new V("mimeType");
        this.codecs = new V("codecs");
        pi(a.attributes, this)
    };
    m(Ci, Bi);
    Ci.prototype.Ma = function(a) {
        Bi.prototype.Ma.call(this, a);
        a.mimeType.value = a.mimeType.value || this.mimeType.value;
        a.codecs.value = a.codecs.value || this.codecs.value
    };
    var Di = function(a, b) {
        Ci.call(this, a);
        this.Qj = new U("bandwidth");
        this.height = new U("height");
        this.width = new U("width");
        pi(a.attributes, this);
        b.Ma(this);
        if (this.mimeType.value) switch (this.mimeType.value.toLowerCase()) {
            case "application/ttml+xml":
                this.codecs.value = "ttml";
                this.mimeType.value = "text/mp4";
                break;
            case "text/vtt":
                this.codecs.value = "webvtt"
        }
        "avc1" == this.codecs.value && (this.codecs.value = "avc1.4D401E")
    };
    m(Di, Ci);
    var Ei = function(a, b) {
        Ci.call(this, a);
        this.language = new V("lang");
        this.frameRate = new U("frameRate");
        pi(a.attributes, this);
        this.role = null;
        b.Ma(this);
        this.v = [];
        a = p(a.children);
        for (b = a.next(); !b.done; b = a.next()) b = b.value, "Representation" == b.nodeName ? this.v.push(new Di(b, this)) : "Role" == b.nodeName && (this.role = b.getAttribute("value"))
    };
    m(Ei, Ci);
    var qi = function(a, b) {
        Bi.call(this, a);
        this.duration = new li("duration");
        this.start = new li("start");
        pi(a.attributes, this);
        this.url = this.url || b.url;
        this.l = [];
        a = p(a.children);
        for (b = a.next(); !b.done; b = a.next()) switch (b = b.value, b.nodeName) {
            case "AdaptationSet":
                this.l.push(new Ei(b, this))
        }
    };
    m(qi, Bi);
    var Fi = function(a) {
        S.call(this, a);
        this.Ee = new Pc(5);
        this.Sf = !1;
        this.td = this.od.bind(this);
        this.fa = null;
        this.Oc = [];
        this.$h = []
    };
    m(Fi, S);
    Fi.prototype.Qb = function() {
        S.prototype.Qb.call(this);
        null !== this.fa && (clearTimeout(this.fa), this.fa = null)
    };
    Fi.prototype.updateLicenseRequestInfo = function(a) {
        var b = this.Ee.get(a.protectionSystem);
        b && (a.headers = {}, a.headers["content-type"] = "text/xml;charset=utf-8", a.url = b.url)
    };
    Fi.prototype.bh = function(a) {
        if (a.wd && 0 != a.wd.length) {
            var b = "dynamic" == a.type.value;
            u(this.Z) || Rd(b);
            this.Z = b;
            a.duration.value && (this.duration = a.duration.value);
            a: {
                var c = a.wd;b = c[0];
                if (b.l[0].F) {
                    var d = [],
                        e = [];
                    c = p(c);
                    for (var f = c.next(); !f.done; f = c.next()) {
                        f = f.value;
                        for (var g = f.duration.value, k = 0, l = [], n = p(f.l), q = n.next(); !q.done; q = n.next()) {
                            var r = q.value;
                            q = r.F;
                            if (!q) {
                                E(Gi, "Multiple periods not using SegmentTemplate is not supported");
                                b = [{
                                    start: b.start.value,
                                    duration: b.duration.value,
                                    se: null
                                }];
                                break a
                            }
                            r =
                                this.uri.resolve(new I(r.v[0].url)).toString();
                            var v = new I(r);
                            e[k] = e[k] || 0;
                            r = e[k];
                            var H = {
                                ka: 0,
                                O: 0,
                                c: [],
                                url: null,
                                F: null,
                                H: null,
                                ib: null
                            };
                            Hi(v, q, H);
                            q = Ii(v, q);
                            q = {
                                H: H.H,
                                O: H.O,
                                ui: r,
                                F: q
                            };
                            l.push(q);
                            e[k] += Math.round(g / q.F.duration);
                            k++
                        }
                        d.push({
                            start: f.start.value,
                            duration: f.duration.value,
                            se: l
                        })
                    }
                    b = d
                } else b = [{
                    start: b.start.value,
                    duration: b.duration.value,
                    se: null
                }]
            }
            this.Oc = b;
            a = p(a.wd[0].l);
            for (b = a.next(); !b.done; b = a.next()) {
                d = b.value;
                b = d.v;
                e = b[0].mimeType.value;
                if (null === e) {
                    this.host.R(422, void 0, "No mime type in manifest",
                        Gi);
                    break
                }
                if (Fd(e)) c = 1;
                else if (Gd(e)) c = 2;
                else if (vf(d.role, e, d.codecs.value)) c = 3;
                else continue;
                b = {
                    name: d.id.value,
                    type: c,
                    enabled: !1,
                    Na: !1,
                    U: !0,
                    index: -1,
                    S: -1,
                    v: [],
                    language: d.language.value,
                    mimeType: e,
                    codecs: b[0].codecs.value || "",
                    role: d.role
                };
                d = p(d.v);
                for (e = d.next(); !e.done; e = d.next()) {
                    e = e.value;
                    c = p(e.De);
                    for (f = c.next(); !f.done; f = c.next()) f = f.value, (g = Ji(f.systemId)) && this.Ee.set(g, f);
                    e.bc && (this.Sf = !0);
                    f = this.uri.resolve(new I(e.url)).toString();
                    c = {
                        ka: e.Qj.value || 0,
                        id: e.id.value,
                        O: 0,
                        c: [],
                        url: f,
                        H: null,
                        F: null,
                        ib: null
                    };
                    e.Gb || e.Hb || e.F ? (e.url && "/" != f[f.length - 1] && (f += "/"), f = new I(f), Hi(f, e.Gb, c), (g = e.Hb) && Hi(f, g, c), e = e.F, g = c, e && (Hi(f, e, g), g.F = Ii(f, e))) : (e = f, f = this.duration, c.c.push({
                        time: 0,
                        Eb: new og("0"),
                        duration: f,
                        url: e
                    }));
                    b.v.push(c)
                }
                b.Na = !0;
                this.l.push(b)
            }
        } else this.host.R(421, void 0, "No periods found in manifest", Gi)
    };
    var Ki = function(a) {
            if (Jf && Jf.updateOutputMode) {
                var b = a.frameRate.value;
                if (x(b)) {
                    a = p(a.v);
                    for (var c = a.next(); !c.done; c = a.next()) {
                        var d = c.value;
                        c = d.width.value;
                        var e = d.height.value;
                        if (x(c) && x(e) && 2160 <= e) {
                            a = d.codecs.value;
                            F(Gi, "Update output mode with frameRate=" + b + " codecs=" + a);
                            Jf.updateOutputMode(c, e, b, a);
                            break
                        }
                    }
                }
            }
        },
        Li = function(a) {
            a = p(a.wd);
            for (var b = a.next(); !b.done; b = a.next()) {
                var c = {};
                b = p(b.value.l);
                for (var d = b.next(); !d.done; c = {
                        frameRate: c.frameRate
                    }, d = b.next()) d = d.value, c.frameRate = d.frameRate.value,
                    d.v = d.v.filter(function(a) {
                        return function(b) {
                            var c = a.frameRate;
                            if (Kf) {
                                var d = b.codecs.value,
                                    e = b.mimeType.value,
                                    f = b.width.value;
                                b = b.height.value;
                                x(f) && x(b) && 2160 <= b && d && e ? (e = e + "; codecs=" + If(d), f && b && (e += "; width=" + f + "; height=" + b), x(c) && (e += "; output_refresh_rate=" + c), Id(d) && (e += "; eotf=smpte2084"), c = Kf(e), F(Gi, "canDisplay(" + e + "): " + !!c)) : c = !0
                            } else c = !0;
                            return c
                        }
                    }(c)), Ki(d)
            }
        };
    Fi.prototype.kc = function(a, b) {
        if (a) {
            b && (this.uri = new I(b));
            var c = null !== this.fa,
                d = this.l;
            this.l = [];
            var e = (new DOMParser).parseFromString(a, "text/xml");
            if (e.firstChild)
                if (e = new ri(e.firstChild), Li(e), this.bh(e), this.Sf || this.Qc.ne(this.Ee.za()), this.Z && x(e.Ij.value) && (this.fa = setTimeout(this.td, 1E3 * (e.Ij.value || 10))), c) {
                    for (a = 0; a < d.length; a++) {
                        b = this.l[a];
                        c = d[a];
                        b.enabled = c.enabled;
                        b.S = c.S;
                        b.Na = c.Na;
                        e = [];
                        for (var f = !0, g = c.index, k = 0; k < b.v.length; k++) {
                            var l = c.v[k],
                                n = b.v[k];
                            n.O = l.O;
                            var q = k;
                            if (x(l.H) &&
                                x(n.H)) {
                                var r = l.H + g - n.H;
                                r = 0 < r ? {
                                    c: n.c,
                                    index: r,
                                    H: n.H
                                } : {
                                    c: l.c.slice(g, n.H - l.H).concat(n.c),
                                    index: 0,
                                    H: l.H + g
                                }
                            } else r = null;
                            if (r) l = r;
                            else {
                                r = {
                                    c: [],
                                    index: 0,
                                    H: n.H
                                };
                                var v = n.c[0];
                                0 <= g && v.time < l.c[g].time ? (r.c = n.c, r.index = Mi(l.c[g].time, n.c), l = r) : (v = Mi(v.time, l.c), -1 == v ? l = null : (g > v ? (r.c = n.c, r.index = g - v) : (r.c = l.c.slice(g, v).concat(n.c), r.index = 0, x(n.H) && (r.H -= v - g)), l = r))
                            }
                            e[q] = l;
                            if (!e[k]) {
                                f = !1;
                                E(Gi, "Old and new representations are out of sync.");
                                break
                            }
                        }
                        if (!(0 > g))
                            if (f) {
                                for (f = 0; f < b.v.length; f++) b.v[f].c = e[f].c, b.v[f].H =
                                    e[f].H;
                                b.U = c.U;
                                b.index = e[0].index
                            } else b.U = !0, b.index = 0
                    }
                    this.Qc.fh()
                } else S.prototype.kc.call(this, a, b)
        } else this.host.R(321, di(this.Kc))
    };
    Fi.prototype.wa = function(a) {
        var b = this.l[a];
        b = b.v[b.S];
        return 0 == b.c.length && b.F ? (a = this.duration, this.Z && (a -= 20), {
            start: 0,
            end: a
        }) : S.prototype.wa.call(this, a)
    };
    var Oi = function(a, b, c) {
        c = Ni(a, b, c);
        a.$h[b] != c && (a.l[b].U = !0);
        a.$h[b] = c
    };
    Fi.prototype.seek = function(a, b) {
        var c = this.l[a],
            d = c.v[c.S];
        if (0 == d.c.length && d.F) {
            var e = this.wa(a);
            b < e.start && (b = e.start);
            b > e.end && (b = e.end);
            e = Math.floor(b / d.F.duration);
            d = Math.ceil(this.duration / d.F.duration);
            c.index = e < d ? e : d - 1;
            Oi(this, a, c.index);
            return b
        }
        return S.prototype.seek.call(this, a, b)
    };
    Fi.prototype.fc = function(a) {
        var b = this.l[a],
            c = b.v[b.S];
        if (0 == c.c.length && c.F) {
            var d = b.index + 1;
            return d < Math.ceil(this.duration / c.F.duration) ? (b.index = d, Oi(this, a, b.index), !0) : !1
        }
        return S.prototype.fc.call(this, a)
    };
    var Ni = function(a, b, c) {
        a = a.Oc;
        for (var d = 0; d < a.length - 1 && !(c < a[d + 1].se[b].ui); d++);
        return d
    };
    h = Fi.prototype;
    h.vb = function(a) {
        var b = this.l[a],
            c = b.v[b.S];
        return 0 == c.c.length && c.F ? b.index == Math.ceil(this.duration / c.F.duration) - 1 : S.prototype.vb.call(this, a)
    };
    h.getSegmentInterval = function(a) {
        if (this.qd(a)) return {
            time: 0,
            duration: 0
        };
        var b = this.l[a],
            c = b.v[b.S];
        if (c.F) {
            b = b.index;
            if (0 > b) return {
                time: 0,
                duration: 0
            };
            0 < c.c.length ? (a = c.c[b].duration, c = c.c[b].time - c.O) : (a = c.F.duration, c = b * a);
            return {
                time: c,
                duration: a
            }
        }
        return S.prototype.getSegmentInterval.call(this, a)
    };
    h.updateSegmentRequestInfo = function(a, b) {
        S.prototype.updateSegmentRequestInfo.call(this, a, b);
        var c = this.l[a],
            d = c.v[c.S];
        c = c.index;
        var e = this.qd(a);
        if (d.F) {
            var f = d.F,
                g = null;
            1 < this.Oc.length && (g = this.Oc[Ni(this, a, c)].se[a], f = g.F);
            a = e ? f.da : f.url;
            a = a.replace(/\$RepresentationID\$/g, d.id);
            a = a.replace(/\$Bandwidth\$/g, d.ka.toString());
            e || (e = c + (d.H || 0), 1 < this.Oc.length && (e = c + (g.H || 0) - g.ui), a = Pi(a, e), a = a.replace("$Time$", 0 < d.c.length ? d.c[c].Eb.toString() : (c * f.duration).toString()));
            b.url = a
        } else {
            g = f = null;
            if (e) c = d.ib, d = c.url ? c.url : d.url, c.ta && (f = c.ta.start, g = c.ta.end);
            else if (c < d.c.length) c = d.c[c], c.url ? d = c.url : (d = d.url, f = c.offset, g = c.offset + c.size - 1);
            else {
                this.host.R(423, void 0, "Invalid segment info in manifest.", Gi);
                return
            }
            b.url = d;
            b.headers = {};
            null === f || null === g || (b.headers.Range = "bytes=" + f + "-" + g)
        }
    };
    h.processSegment = function(a, b, c) {
        var d = this.l[a],
            e = d.v[d.S],
            f = b.data;
        if (this.Sf && this.Qc) {
            var g = Q(f, 1886614376, void 0, !0);
            if (g = null === g ? null : {
                    systemId: g.Dh,
                    url: null,
                    Xd: null,
                    ie: null,
                    Bf: 0
                }) {
                var k = Ji(g.systemId);
                k && (this.Ee.set(k, g), this.Qc.ne(this.Ee.za()), this.Sf = !1)
            }
        }
        if (b.Fc)
            if (d = (d = Q(f, 1836019574)) ? Ag(d) : null, null === d) this.host.R(322, void 0, "no init", Gi);
            else {
                if (!e.F && 0 == e.c.length) {
                    a = 0;
                    b.Fc && e.ib && e.ib.ta && (a = e.ib.ta.start);
                    f = (f = Q(f, 1936286840)) ? f.Kg(a) : null;
                    if (!f) {
                        E(Gi, "no segments");
                        return
                    }
                    e.c =
                        f
                }
                Mh(c, d, {
                    time: b.interval.time,
                    duration: 0
                }, 0, !1)
            }
        else g = b.interval, e = e.O, 1 < this.Oc.length && (e = this.Oc[Ni(this, a, d.index)], e = e.se[a].O - e.start), this.host.processMetadata && (a = b.data, Q(a, 1701671783, void 0, !0) && this.host.processMetadata("EMSG", a, b.interval.time)), Mh(c, f, g, g.time + e, d.U), d.U = !1
    };
    h.lf = function() {
        return 1
    };
    var Ji = function(a) {
            return yf.sb(a) ? "playready" : zf.sb(a) ? "widevine" : Af.sb(a) ? "clearkey" : null
        },
        Hi = function(a, b, c) {
            if (b) {
                null !== b.H.value && (c.H = b.H.value);
                var d = b.Oe.value || 1,
                    e = new og(b.O.value || "0");
                c.O = parseInt(e.toString(), 10) / d;
                e = new og(e.toString());
                var f = b.c,
                    g = b.Uc,
                    k = 0;
                g ? k = g.length : f && 0 < f.length && (k = f.length);
                for (var l = 0; l < k; l++) {
                    var n = g ? g[l].duration : b.duration.value || 0;
                    g && g[l].time && (e = new og(g[l].time.toString()));
                    var q = parseInt(e.toString(), 10) / d,
                        r = new og(e.toString());
                    if (!f || 0 >= f.length) var v =
                        null;
                    else {
                        v = f[l].media.value;
                        var H = a.resolve(new I(v)).toString();
                        v = !v && H.endsWith("/") ? H.slice(0, -1) : H
                    }
                    c.c.push({
                        time: q,
                        Eb: r,
                        duration: n / d,
                        url: v
                    });
                    e.add(n)
                }(d = c.ib) || (f = e = d = null, g = !0, (k = b.ae) && k.Aj.value ? (g = !1, d = k.Aj.value, k.ta.start && k.ta.end && (e = k.ta.start, f = k.ta.end)) : (e = 0, k && k.ta.start && k.ta.end && (e = k.ta.start, f = k.ta.end, g = !1), b.Ec.start && b.Ec.end && (e = Math.min(e, b.Ec.start), f = Math.max(f, b.Ec.end), g = !1), f || (f = 2048)), d = g ? null : {
                    url: d ? a.resolve(new I(d)).toString() : null,
                    ta: null === e || null === f ? null : {
                        start: e,
                        end: f
                    },
                    data: null
                });
                c.ib = d
            }
        },
        Ii = function(a, b) {
            return {
                duration: (b.duration.value || 0) / (b.Oe.value || 1),
                url: a.resolve(new I(b.media.value)).toString(),
                da: b.da.value ? a.resolve(new I(b.da.value)).toString() : null
            }
        },
        Mi = function(a, b) {
            for (var c = .5 * b[0].duration, d = 0; d < b.length; d++)
                if (Math.abs(b[d].time - a) <= c) return d;
            return -1
        },
        Pi = function(a, b) {
            return a.replace(/\$Number(%0\d*[diuxXo]){0,1}\$/g, function(a) {
                var c = a.indexOf("%0");
                if (0 > c) return b;
                c = parseInt(a.substring(c + 2, a.length - 2), 10);
                switch (a.substr(a.length -
                    2, 1)) {
                    case "x":
                        return Df(b, 16, c);
                    case "X":
                        return Df(b, 16, c).toUpperCase();
                    case "o":
                        return Df(b, 8, c);
                    default:
                        return Df(b, 10, c)
                }
            })
        },
        Gi = B("cast.player.dash.Protocol");
    var Qi = function(a) {
            this.Df = a;
            this.Zd = null;
            this.Eh = this.xc = 0
        },
        Ri = function(a, b) {
            b = b.length > a.xc ? b.subarray(0, a.xc) : b;
            N(a.Zd, b);
            a.xc -= b.length
        };
    Qi.prototype.append = function(a, b, c, d) {
        b = b.subarray(c, d);
        if (this.Zd) return Ri(this, b), 0 == this.xc && (this.Df && this.Df("ID3", sg(this.Zd), this.Eh), this.xc = this.Eh = this.Zd = null), 0;
        c = 0;
        73 != b[c++] || 68 != b[c++] || 51 != b[c++] ? c = null : (c += 3, d = b[c++] << 21 | b[c++] << 14 | b[c++] << 7 | b[c++], c += d);
        if (null === c) return 0;
        if (c <= b.length) return this.Df && this.Df("ID3", b.subarray(0, c), a), c;
        this.Zd = new rg(c);
        this.Eh = a;
        this.xc = c;
        Ri(this, b);
        return 0
    };
    var Si = function() {
        this.o = null;
        this.kd = this.Rc = this.si = this.jg = this.mf = this.ve = this.D = 0
    };
    Si.prototype.pa = function() {
        return null === this.o
    };
    Si.prototype.uf = function() {
        return this.D >= this.o.length
    };
    Si.prototype.da = function(a) {
        this.o = a;
        this.kd = this.Rc = this.jg = this.mf = this.ve = this.D = 0
    };
    Si.prototype.parse = function() {
        for (var a = this.D; this.D < this.o.length && !(524288 < this.D - a);) {
            for (; this.D < this.o.length && 71 != this.o[this.D];) this.D++;
            if (this.D + 188 > this.o.length) break;
            var b = this.D + 188,
                c = b - 1;
            this.D++;
            var d = this.o[this.D] & 64,
                e = this.o[this.D] & 31;
            this.D++;
            e = e << 8 | this.o[this.D];
            this.D++;
            var f = (this.o[this.D] & 48) >> 4;
            this.D++;
            f & 2 && (this.D += this.o[this.D] + 1);
            if (0 == e || e == this.ve)
                if (d && this.D++, c = this.o[this.D], this.D++, 0 == c)
                    for (c = this.D, d = this.o[c] & 15, c++, d = d << 8 | this.o[c], c++, d = c + d - 4, c += 5; c < d;)
                        if (e =
                            this.o[c], c++, e = (e << 8) + this.o[c], c++, 0 == e) c += 2;
                        else {
                            this.ve = this.o[c] & 31;
                            c++;
                            this.ve = (this.ve << 8) + this.o[c];
                            break
                        }
            else {
                if (2 == c)
                    for (c = this.D, d = this.o[c] & 15, c++, d = d << 8 | this.o[c], c++, d = c + d - 4, c = c + 5 + 2, e = this.o[c] & 15, c++, e = e << 8 | this.o[c], c += e + 1; c < d;) {
                        e = this.o[c];
                        c++;
                        f = this.o[c] & 31;
                        c++;
                        f = f << 8 | this.o[c];
                        c++;
                        switch (e) {
                            case 27:
                            case 219:
                                this.mf = f;
                                break;
                            case 15:
                            case 207:
                                this.jg = f;
                                break;
                            case 21:
                                this.si = f
                        }
                        e = this.o[c] & 15;
                        c++;
                        e = e << 8 | this.o[c];
                        c += e + 1
                    }
            } else if (e == this.si) this.ij(this.D, c, !!d);
            else if (e == this.mf || e ==
                this.jg) d && this.nh(this.D), e == this.mf && this.hj(this.D, c);
            this.D = b
        }
    };
    var Ti = function(a, b) {
        var c = ((a.o[b] & 14) << 13) / 1.373291015625;
        c += (a.o[b + 1] << 6) / 1.373291015625;
        c += (a.o[b + 2] & 254) / 5.4931640625;
        c += a.o[b + 3] / 703.125;
        return c += (a.o[b + 4] & 254) / 18E4
    };
    Si.prototype.nh = function(a) {
        if (0 == this.o[a] && 0 == this.o[a + 1] && 1 == this.o[a + 2]) {
            a += 7;
            var b = this.o[a] >> 6 & 3;
            a++;
            var c = this.o[a];
            a++;
            this.D = a + c;
            2 == b ? this.kd = this.Rc = Ti(this, a) : 3 == b && (this.Rc = Ti(this, a), this.kd = Ti(this, a + 5))
        }
    };
    Si.prototype.ij = function() {};
    Si.prototype.hj = function() {};
    var Ui = function(a, b, c) {
        Si.call(this);
        this.Va = null;
        this.sf = b ? new Qi(b) : null;
        this.ah = Infinity;
        this.jl = c;
        for (this.da(a); !this.uf();) this.parse()
    };
    m(Ui, Si);
    Ui.prototype.mi = function() {
        return this.ah
    };
    Ui.prototype.nh = function(a) {
        Si.prototype.nh.call(this, a);
        null === this.Va && (this.Va = this.kd);
        a = Vi(this.Va, this.kd);
        a < this.ah && (this.ah = a)
    };
    Ui.prototype.ij = function(a, b, c) {
        if (this.sf) {
            a = new Zf(this.o.subarray(a, b + 1));
            if (c) {
                if (1 != L(a) >> 8) return;
                M(a, 2);
                M(a, 2);
                c = $f(a);
                M(a, c)
            }
            null === this.Va && (this.Va = this.kd);
            this.sf.append(this.jl + (Vi(this.Va, this.Rc) - this.Va), dg(a), 0)
        }
    };
    var Vi = function(a, b) {
        var c = Math.floor(a / 95443.7176888889),
            d = 95443.7176888889 * (c - 1) + b,
            e = 95443.7176888889 * (c + 0) + b;
        b = 95443.7176888889 * (c + 1) + b;
        c = Math.abs(d - a);
        var f = Math.abs(e - a);
        a = Math.abs(b - a);
        var g = c;
        f < c && (d = e, g = f);
        a < g && (d = b);
        return d
    };
    var W = function(a, b) {
        this.Tj = a;
        this.Jd = b ? b : null;
        this.b = null
    };
    W.prototype.h = function(a) {
        var b = sg(a).length;
        this.b = a;
        this.Kh();
        this.A();
        a = sg(a).length - b;
        this.b.buffer.set([a >> 24, a >> 16, a >> 8, a], b)
    };
    W.prototype.Kh = function() {
        this.b.j(0);
        this.b.j(this.Tj);
        this.Jd && N(this.b, this.Jd.Yc())
    };
    W.prototype.A = function() {
        throw Error("writeBody() should be  overriden.");
    };
    var X = function(a, b, c) {
        W.call(this, a);
        this.Da = b;
        this.$b = c
    };
    m(X, W);
    X.prototype.Kh = function() {
        W.prototype.Kh.call(this);
        this.b.j(this.Da << 24 | this.$b & 16777215)
    };
    var Wi = function(a) {
        X.call(this, 1935763823, 0, 0);
        this.D = a
    };
    m(Wi, X);
    Wi.prototype.A = function() {
        this.b.j(1);
        this.b.j(this.D)
    };
    var Xi = function(a) {
        X.call(this, 1952867444, 1, 0);
        this.Od = a
    };
    m(Xi, X);
    Xi.prototype.A = function() {
        this.b.Lh(this.Od)
    };
    var Yi = function(a, b) {
        X.call(this, 1935763834, 0, 0);
        this.fi = a;
        this.vj = b
    };
    m(Yi, X);
    Yi.prototype.A = function() {
        this.b.gg(this.fi);
        this.b.j(this.vj.length);
        0 == this.fi && N(this.b, this.vj)
    };
    var Zi = function(a, b) {
        X.call(this, 1702061171, 0, 0);
        this.Gk = a;
        this.Pd = b
    };
    m(Zi, X);
    Zi.prototype.A = function() {
        N(this.b, [3, 25, 0, 1, 0, 4, 17, this.Gk, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2, this.Pd[0], this.Pd[1]])
    };
    var $i = function(a, b) {
        W.call(this, 1635148611);
        this.Bj = a;
        this.kj = b
    };
    m($i, W);
    $i.prototype.A = function() {
        N(this.b, [1, 77, 64, 30, 255, 225]);
        this.b.ad(this.Bj.length);
        N(this.b, this.Bj);
        this.b.gg(1);
        this.b.ad(this.kj.length);
        N(this.b, this.kj)
    };
    var aj = function() {
        X.call(this, 1937011571, 0, 0)
    };
    m(aj, X);
    aj.prototype.A = function() {
        this.b.j(0)
    };
    var bj = function(a, b, c) {
        X.call(this, 1952804451, 0, 1);
        this.ek = c ? 0 : a;
        this.dk = b;
        this.ei = c
    };
    m(bj, X);
    bj.prototype.A = function() {
        this.b.j(256 | this.ek);
        N(this.b, this.dk.Yc());
        this.ei && (this.b.gg(16), N(this.b, this.ei))
    };
    var cj = function(a) {
        X.call(this, 1936027235, 0, 2);
        this.th = a
    };
    m(cj, X);
    cj.prototype.A = function() {
        this.b.j(this.th.length);
        for (var a = 0; a < this.th.length; a++) {
            var b = this.th[a].Cj;
            this.b.ad(b.length);
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                this.b.ad(d.og);
                this.b.j(d.pg)
            }
        }
    };
    var dj = function(a, b) {
        X.call(this, 1935894637, 0, 0);
        this.cl = a;
        this.dl = b
    };
    m(dj, X);
    dj.prototype.A = function() {
        this.b.j(this.cl);
        this.b.j(this.dl)
    };
    var ej = function(a) {
        W.call(this, 1718775137);
        this.bk = a
    };
    m(ej, W);
    ej.prototype.A = function() {
        this.b.j(this.bk)
    };
    var fj = function(a) {
        X.call(this, 1751411826, 0, 0);
        this.qk = a
    };
    m(fj, X);
    fj.prototype.A = function() {
        this.b.j(0);
        this.b.j(this.qk);
        N(this.b, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    };
    var gj = function(a) {
        X.call(this, 1835296868, 0, 0);
        this.Xa = a
    };
    m(gj, X);
    gj.prototype.A = function() {
        this.b.j(0);
        this.b.j(0);
        this.b.j(this.Xa);
        this.b.j(0);
        N(this.b, [85, 196, 0, 0])
    };
    var hj = function(a, b) {
        X.call(this, 1953196132, 0, 3);
        this.ql = a;
        this.rk = b
    };
    m(hj, X);
    hj.prototype.A = function() {
        this.b.j(0);
        this.b.j(0);
        N(this.b, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0]);
        this.b.j(this.ql << 16);
        this.b.j(this.rk << 16)
    };
    var ij = function(a) {
        X.call(this, 1953654136, 0, 0);
        this.vk = a
    };
    m(ij, X);
    ij.prototype.A = function() {
        var a = 65536;
        this.vk && (a &= -65537);
        N(this.b, [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]);
        this.b.j(a)
    };
    var jj = function(a, b) {
        X.call(this, 1886614376, 0, 0);
        this.Dh = a;
        this.hb = b
    };
    m(jj, X);
    jj.prototype.A = function() {
        N(this.b, this.Dh.Yc());
        this.b.j(this.hb.length);
        N(this.b, this.hb)
    };
    var kj = function(a) {
        X.call(this, 1836476516, 0, 0);
        this.Xa = a
    };
    m(kj, X);
    kj.prototype.A = function() {
        this.b.j(0);
        this.b.j(0);
        this.b.j(this.Xa);
        N(this.b, [0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2])
    };
    var lj = function(a) {
        X.call(this, 1952868452, 0, 8);
        this.fk = a
    };
    m(lj, X);
    lj.prototype.A = function() {
        this.b.j(1);
        this.b.j(this.fk)
    };
    var mj = function(a) {
        X.call(this, 1953658222, 0, 513);
        this.wh = a
    };
    m(mj, X);
    mj.prototype.A = function() {
        this.b.j(this.wh.length);
        this.b.j(0);
        for (var a = 0; a < this.wh.length; a++) this.b.j(this.wh[a].length)
    };
    var nj = function(a) {
        X.call(this, 1835427940, 0, 0);
        this.oc = a
    };
    m(nj, X);
    nj.prototype.A = function() {
        this.b.j(this.oc)
    };
    var oj = function(a) {
        W.call(this, 1835295092);
        this.ai = a
    };
    m(oj, W);
    oj.prototype.A = function() {
        for (var a = 0; a < this.ai.length; a++) N(this.b, this.ai[a])
    };
    var qj = function(a) {
        var b = new Uint8Array(2 + a.length);
        b[1] = 1;
        Ja(a, b, 2);
        jj.call(this, pj, b)
    };
    m(qj, jj);
    var pj = new xf([43, 248, 102, 128, 198, 229, 78, 36, 190, 35, 15, 129, 90, 96, 110, 178]);
    var sj = function(a) {
            var b = new tg;
            (new jj(zf, rj(a))).h(b);
            return sg(b)
        },
        rj = function(a) {
            var b = new ef;
            Ke(b, 1, 1);
            var c = Ef(a.ge);
            He(b, 2).push(c);
            Ke(b, 3, a.Wk);
            c = [];
            for (var d = 0; d < a.Yh.length; d++) c.push(a.Yh.charCodeAt(d));
            a = new Uint8Array(c);
            Ke(b, 4, a);
            Ke(b, 9, 1667392371);
            return b.Pf()
        },
        tj = [71, 64, 1, 16, 0, 1, 176, 45, 255, 255, 193, 0, 0, 9, 34, 99, 101, 224, 16, 99, 98, 99, 115, 0, 1, 0, 0, 1, 0, 16, 1, 237, 239, 139, 169, 121, 214, 74, 206, 163, 200, 39, 220, 213, 29, 33, 237, 0, 143, 19, 198, 145, 164, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 71, 64, 17, 16, 127,
            255, 255, 255, 0, 0, 0, 32, 112, 115, 115, 104, 0, 0, 0, 0, 237, 239, 139, 169, 121, 214, 74, 206, 163, 200, 39, 220, 213, 29, 33, 237, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 71, 64, 16, 48, 147, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 79, 16, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 1, 112, 213, 251, 214, 184, 46, 217, 62, 78, 249, 138, 228, 9, 49, 238, 51, 183
        ];
    var uj = function(a, b, c) {
            this.Fk = a;
            this.sampleRate = b;
            this.ab = c;
            this.Hc = void 0
        },
        vj = function(a, b, c, d) {
            this.width = a;
            this.height = b;
            this.Wc = c;
            this.we = d
        },
        wj = function(a, b, c, d, e, f) {
            this.kb = a;
            this.tc = b;
            this.Vj = c;
            this.pk = d;
            this.audio = e;
            this.video = f
        },
        xj = function(a) {
            W.call(this, 1836019574);
            this.i = a
        };
    m(xj, W);
    xj.prototype.A = function() {
        (new kj(this.i.tc)).h(this.b);
        var a = this.i.kb;
        a && a.Xd && (new jj(a.systemId, a.Xd)).h(this.b);
        (new yj(this.i)).h(this.b);
        (new zj(this.i)).h(this.b)
    };
    var zj = function(a) {
        W.call(this, 1953653099);
        this.i = a
    };
    m(zj, W);
    zj.prototype.A = function() {
        var a = 0,
            b = 0,
            c = this.i.video;
        c && (a = c.width, b = c.height);
        (new hj(a, b)).h(this.b);
        (new Aj(this.i)).h(this.b)
    };
    var Aj = function(a) {
        W.call(this, 1835297121);
        this.i = a
    };
    m(Aj, W);
    Aj.prototype.A = function() {
        (new gj(this.i.tc)).h(this.b);
        (new fj(this.i.pk)).h(this.b);
        (new Bj(this.i)).h(this.b)
    };
    var Bj = function(a) {
        W.call(this, 1835626086);
        this.i = a
    };
    m(Bj, W);
    Bj.prototype.A = function() {
        (new Cj(this.i)).h(this.b)
    };
    var Cj = function(a) {
        W.call(this, 1937007212);
        this.i = a
    };
    m(Cj, W);
    Cj.prototype.A = function() {
        (new Dj(this.i)).h(this.b);
        this.i.video && (new aj).h(this.b)
    };
    var Dj = function(a) {
        X.call(this, 1937011556, 0, 0);
        this.i = a
    };
    m(Dj, X);
    Dj.prototype.A = function() {
        this.b.j(1);
        (this.i.video ? new Ej(this.i) : new Fj(this.i)).h(this.b)
    };
    var Fj = function(a) {
        W.call(this, a.kb ? 1701733217 : 1836069985);
        this.i = a
    };
    m(Fj, W);
    Fj.prototype.A = function() {
        N(this.b, [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 16, 0, 0, 0, 0]);
        this.b.j(this.i.audio.sampleRate << 16);
        (new Zi(this.i.audio.Fk, this.i.audio.ab)).h(this.b);
        this.i.kb && (new Gj(this.i)).h(this.b)
    };
    var Ej = function(a) {
        W.call(this, a.kb ? 1701733238 : 1635148593);
        this.i = a
    };
    m(Ej, W);
    Ej.prototype.A = function() {
        N(this.b, [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        this.b.ad(this.i.video.width);
        this.b.ad(this.i.video.height);
        N(this.b, [0, 72, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 255, 255]);
        (new $i(this.i.video.Wc, this.i.video.we)).h(this.b);
        this.i.kb && (new Gj(this.i)).h(this.b)
    };
    var yj = function(a) {
        W.call(this, 1836475768);
        this.i = a
    };
    m(yj, W);
    yj.prototype.A = function() {
        (new ij(!this.i.video)).h(this.b)
    };
    var Gj = function(a) {
        W.call(this, 1936289382);
        this.i = a
    };
    m(Gj, W);
    Gj.prototype.A = function() {
        (new ej(this.i.Vj)).h(this.b);
        (new dj(this.i.kb.scheme || 1667591779, 0)).h(this.b);
        (new Hj(this.i)).h(this.b)
    };
    var Hj = function(a) {
        W.call(this, 1935894633);
        this.i = a
    };
    m(Hj, W);
    Hj.prototype.A = function() {
        var a = this.i.kb;
        a && a.ie && (new bj(a.Bf, a.ie, a.Wj)).h(this.b)
    };
    var Ij = function(a, b, c, d, e) {
        xj.call(this, new wj(a, b, 1836069985, 1936684398, new uj(c, d, e), null))
    };
    m(Ij, xj);
    var Jj = function(a, b, c, d, e, f) {
        xj.call(this, new wj(a, b, 1635148593, 1986618469, null, new vj(c, d, e, f)))
    };
    m(Jj, xj);
    var Kj = function(a, b, c, d) {
        W.call(this, 1836019558);
        this.i = {
            $k: a,
            Rj: b,
            al: c,
            uj: d
        }
    };
    m(Kj, W);
    Kj.prototype.A = function() {
        (new nj(0)).h(this.b);
        (new Lj(this.i)).h(this.b)
    };
    var Lj = function(a) {
        W.call(this, 1953653094);
        this.i = a
    };
    m(Lj, W);
    Lj.prototype.A = function() {
        this.i.uj && (new cj(this.i.uj)).h(this.b);
        (new lj(this.i.$k)).h(this.b);
        (new Xi(this.i.Rj)).h(this.b);
        (new mj(this.i.al)).h(this.b)
    };
    var Mj = function(a, b, c) {
        W.call(this, 1836019558);
        this.Od = b;
        this.Nk = a;
        this.Tf = c
    };
    m(Mj, W);
    Mj.prototype.A = function() {
        for (var a = Og(O(this.Nk), void 0); 0 < a.length;) {
            var b = a.shift();
            switch (b.getName()) {
                case 1953653094:
                    (new Nj(b, this.Od, this.Tf)).h(this.b);
                    break;
                default:
                    b.h(this.b)
            }
        }
    };
    var Nj = function(a, b, c) {
        W.call(this, 1953653094);
        this.Od = b;
        this.Ok = a;
        this.Tf = c
    };
    m(Nj, W);
    Nj.prototype.A = function() {
        for (var a = Og(O(this.Ok), void 0), b = null, c = !1, d = !1; 0 < a.length;) {
            var e = a.shift();
            switch (e.getName()) {
                case 1970628964:
                    e instanceof Eg && (b = e);
                    break;
                case 1952868452:
                    var f = e;
                    f.$b & 32 && (this.Tf = !1);
                    ug(O(f), 0, 1);
                    e.h(this.b);
                    break;
                case 1953658222:
                    (new Oj(e, this.Tf)).h(this.b);
                    break;
                case 1935763823:
                    c = !0;
                    e.h(this.b);
                    break;
                case 1935763834:
                    d = !0;
                    e.h(this.b);
                    break;
                default:
                    e.h(this.b)
            }
        }(new Xi(this.Od)).h(this.b);
        b && (d || (new Yi(b.Wg ? 0 : 8, b.Of)).h(this.b), c || (new Wi(0)).h(this.b))
    };
    var Oj = function(a, b) {
        var c = a.$b,
            d = !1,
            e = null;
        c & 1 || (d = !0, c |= 1);
        !b || c & 1024 || c & 4 || (c |= 4, e = 0);
        b = d;
        X.call(this, 1953658222, a.Da, c);
        this.el = b;
        this.hi = e;
        this.Pk = a
    };
    m(Oj, X);
    Oj.prototype.A = function() {
        var a = new qg(O(this.Pk));
        this.b.j(L(a));
        this.el && this.b.j(0);
        null === this.hi || this.b.j(this.hi);
        N(this.b, dg(a))
    };
    var Pj = function(a, b, c) {
        Zf.call(this, a);
        this.sf = new Qi(b);
        this.Cg = c
    };
    m(Pj, Zf);
    Pj.prototype.rh = function() {
        throw Error("readAudioHeader is not implemented");
    };
    var Qj = function(a, b, c, d, e, f, g) {
        this.Ph = a;
        this.ff = c;
        this.Va = d;
        this.Oi = f;
        this.Cg = g;
        this.Td = [];
        this.sampleRate = 0;
        this.vh = e;
        this.Pd = new Uint8Array([0, 0])
    };
    Qj.prototype.parse = function(a) {
        for (var b = this.wg(this.ff), c = [], d = null; b.offset < b.buffer.length;) {
            var e = b.sf.append(a, b.buffer, b.offset);
            b.offset += e;
            if (!e)
                if (d = b.rh()) c.push(cg(b, d.ii)), a += this.vh / d.sampleRate;
                else return Hc(Rj, "Neither ID3 nor ADTS header was found at " + dg(b).byteOffset), !1
        }
        if (null === d) return Hc(Rj, "No ADTS header was found."), !1;
        b = xg(d.profile, d.sampleRate, d.Ri);
        if (null === b) return Hc(Rj, "Cannot generate audio codec private data."), !1;
        this.Td = c;
        this.sampleRate = d.sampleRate;
        this.Pd =
            b;
        return !0
    };
    Qj.prototype.wg = function() {
        throw Error("createdataReader is not implemented");
    };
    Qj.prototype.mi = function() {
        return this.Va
    };
    Qj.prototype.Vd = function() {
        throw Error("getTimeScale is not implemented");
    };
    var Rj = B("cast.player.hls.PackedAudioParser");
    var Sj = function(a, b) {
        Pj.call(this, a, b)
    };
    m(Sj, Pj);
    Sj.prototype.rh = function() {
        var a = this.buffer.subarray(this.offset);
        if (255 != a[0] || 240 != (a[1] & 240)) return null;
        var b = ((a[3] & 3) << 11) + (a[4] << 3) + ((a[5] & 224) >> 5),
            c = a[2] >> 2 & 15,
            d = a[1] & 1 ? 7 : 9;
        this.offset += d;
        return {
            profile: (a[2] >> 6 & 3) + 1,
            sampleRate: vg[c],
            Ri: (a[2] << 2 & 4) + (a[3] >> 6 & 3),
            ii: b - d
        }
    };
    var Tj = function(a, b, c, d, e) {
        a = a.match("mp4a.67") ? 103 : 64;
        Qj.call(this, a, 0, b, c, 1024, d, e)
    };
    m(Tj, Qj);
    Tj.prototype.wg = function(a) {
        return new Sj(a, this.Oi)
    };
    Tj.prototype.Vd = function() {
        return 1E6
    };
    var Uj = [48E3, 44100, 32E3],
        Vj = [
            [64, 69, 96],
            [64, 70, 96],
            [80, 87, 120],
            [80, 88, 120],
            [96, 104, 144],
            [96, 105, 144],
            [112, 121, 168],
            [112, 122, 168],
            [128, 139, 192],
            [128, 140, 192],
            [160, 174, 240],
            [160, 175, 240],
            [192, 208, 288],
            [192, 209, 288],
            [224, 243, 336],
            [224, 244, 336],
            [256, 278, 384],
            [256, 279, 384],
            [320, 348, 480],
            [320, 349, 480],
            [384, 417, 576],
            [384, 418, 576],
            [448, 487, 672],
            [448, 488, 672],
            [512, 557, 768],
            [512, 558, 768],
            [640, 696, 960],
            [640, 697, 960],
            [768, 835, 1152],
            [768, 836, 1152],
            [896, 975, 1344],
            [896, 976, 1344],
            [1024, 1114, 1536],
            [1024, 1115, 1536],
            [1152, 1253, 1728],
            [1152, 1254, 1728],
            [1280, 1393, 1920],
            [1280, 1394, 1920]
        ],
        Wj = function(a, b, c) {
            Pj.call(this, a, b, c)
        };
    m(Wj, Pj);
    Wj.prototype.rh = function() {
        var a = this.buffer.subarray(this.offset),
            b = 0;
        if (11 != a[b++] || 119 != a[b++]) return null;
        b += 2;
        var c = a[b++];
        b = c >> 6 & 3;
        c = Vj[c & 63];
        return u(c) ? {
            profile: (a[2] >> 6 & 3) + 1,
            sampleRate: Uj[b],
            Ri: 6,
            ii: 2 * c[b]
        } : (this.Cg(316), null)
    };
    var Xj = function(a, b, c, d, e) {
        Qj.call(this, 165, 0, b, c, 1536, d, e)
    };
    m(Xj, Qj);
    Xj.prototype.wg = function(a) {
        return new Wj(a, this.Oi, this.Cg)
    };
    Xj.prototype.Vd = function() {
        return this.sampleRate
    };
    var Yj = function(a, b) {
        this.a = a;
        this.Sg = !1;
        this.rg = b;
        this.hh = this.Kk.bind(this);
        this.eh = this.rd.bind(this)
    };
    Yj.prototype.cancel = function() {
        this.Sg = !0
    };
    Yj.prototype.rd = function(a) {
        if (!this.Sg) {
            var b = void 0;
            a && a.name && a.message && (b = a.name + ": " + a.message);
            this.a.R(203, void 0, b)
        }
    };
    Yj.prototype.Kk = function(a) {
        this.Sg || this.rg(a)
    };
    var Zj = function(a, b, c, d, e) {
        Yj.call(this, a, b);
        window.crypto.subtle.decrypt({
            name: "AES-CBC",
            iv: d
        }, c, e).then(this.hh, this.eh)
    };
    m(Zj, Yj);
    var ak = function(a, b, c) {
        Yj.call(this, a, b);
        window.crypto.subtle.importKey("raw", c, {
            name: "AES-CBC"
        }, !0, ["decrypt"]).then(this.hh, this.eh)
    };
    m(ak, Yj);
    var bk = function(a) {
        this.Ra = -1;
        this.C = null;
        this.Bk = this.Ak.bind(this);
        this.Qk = a
    };
    bk.prototype.Ak = function(a, b) {
        for (var c = 0, d = null, e = 0; e < b.length; e++)
            if (null !== b[e].Jf) {
                c = e;
                d = b[e].Jf;
                break
            }
        if (null === d) return null;
        b = -1;
        e = Infinity;
        for (var f = 0; f < a.length; f++) {
            var g = Math.abs(a[f].Jf - d);
            if (100 > g) return f - c;
            g < e && (b = f, e = g)
        }
        return 0 > b || e > 500 * this.C.Vf ? -1 : b - c
    };
    var ck = function(a, b) {
        b = b[0].yh;
        for (var c = 0; c < a.length; c++)
            if (a[c].yh == b) return c;
        return null
    };
    h = bk.prototype;
    h.update = function(a) {
        if (0 > this.Ra) this.C = a;
        else if (0 == a.c.length) E(dk, "new manifest has no segment");
        else {
            var b = this.C.c,
                c = b[this.Ra],
                d = a.c,
                e = {
                    md: ck,
                    Ki: "sequence number"
                },
                f = {
                    md: this.Bk,
                    Ki: "program date time"
                };
            a: {
                e = p(this.C.Lg && a.Lg ? this.Qk ? [e, f] : [f, e] : [e]);
                for (f = e.next(); !f.done; f = e.next()) {
                    f = f.value;
                    b: {
                        var g = this.Ra;
                        var k = f.md,
                            l = k(b, d);
                        if (null === l) {
                            l = k(d, b);
                            if (null === l) {
                                g = -1;
                                break b
                            }
                            l *= -1
                        }
                        k = g - l;g < l ? (g = b[g], 0 < d.length && (d[0].gf = !0), d.splice(0, 0, g), k = 0) : k >= d.length && (d.length = 0, Array.prototype.push.apply(d,
                            b), k = g);g = k
                    }
                    if (0 > g) E(dk, "No match was found using " + f.Ki);
                    else {
                        b = g;
                        break a
                    }
                }
                b = -1
            }
            0 > b && (E(dk, "New segments are appended after current"), 0 < d.length && (d[0].gf = !0), d.splice(0, 0, c), b = 0);
            c = c.Ga - d[b].Ga;
            d = p(d);
            for (e = d.next(); !e.done; e = d.next()) e.value.Ga += c;
            this.Ra = b;
            this.C = a
        }
    };
    h.next = function() {
        var a = this.Ra + 1;
        return a < this.C.c.length ? (this.Ra = a, !0) : !1
    };
    h.vb = function() {
        return !this.C.Z && this.Ra == this.C.c.length - 1
    };
    h.wa = function() {
        var a = this.C.c,
            b = a[0].Ga,
            c = a.length - 1;
        a = a[c].Ga + a[c].duration;
        this.C.Z && (a -= 3 * this.C.Vf, a < b && (a = b));
        return {
            start: b,
            end: a
        }
    };
    h.seek = function(a) {
        var b = this.wa();
        if (!b) return null;
        for (var c = this.C.c, d = p(c), e = d.next(); !e.done; e = d.next()) e.value.U = !1;
        a < b.start && (a = b.start);
        a > b.end && (a = b.end);
        for (b = c.length - 1; 0 <= b; b--)
            if (a >= c[b].Ga) return this.Ra = b, c[this.Ra].U = !0, a;
        return null
    };
    var ek = function(a) {
            return 0 > a.Ra ? null : a.C.c[a.Ra]
        },
        dk = B("cast.player.hls.Iterator");
    var fk = function(a, b, c, d) {
        Yj.call(this, a, b);
        cast.__platform__.crypto.unwrapKey("raw", c, d, {
            name: "RSA-OAEP",
            hash: {
                name: "SHA-256"
            }
        }, {
            name: "AES-CBC",
            length: 128
        }, !1, ["decrypt"]).then(this.hh, this.eh)
    };
    m(fk, Yj);
    var gk = function(a, b, c) {
        C.call(this);
        this.a = a;
        this.Ke = c;
        this.Hj = b;
        this.sa = null;
        this.wb = new bk(1 == this.a.preferSequenceNumberForPlaylistMatching);
        this.$i = this.Ik.bind(this);
        this.di = this.ck.bind(this);
        this.xh = this.Mf = null;
        this.pj = -1;
        this.re = null;
        this.f = new G;
        td(this.f);
        D(this.f, "success", this.Hk, !1, this);
        D(this.f, "error", this.Wi, !1, this);
        D(this.f, "timeout", this.Wi, !1, this);
        this.u = new lf;
        this.u.setResponse = this.ni.bind(this);
        this.he = this.xe = this.Di = this.yb = this.Qd = this.Hh = null;
        this.Ff = !1;
        this.Id = !0;
        this.ea =
            null;
        this.Pj = {};
        this.Ca = this.dc = null
    };
    m(gk, C);
    h = gk.prototype;
    h.G = function() {
        this.reset();
        this.f.M()
    };
    h.reset = function() {
        this.Id = !0;
        this.dc = this.ea = this.re = null;
        this.f.abort();
        this.xe && (this.xe.cancel(), this.xe = null);
        this.Qd && (this.Qd.cancel(), this.Qd = null);
        null === this.Ca || clearTimeout(this.Ca)
    };
    h.Ik = function(a) {
        this.xe = null;
        this.he = a;
        this.Ca = setTimeout(this.di, 0)
    };
    h.Hk = function(a) {
        this.ni(Ed(a.target))
    };
    h.ni = function(a) {
        a ? (a = new Uint8Array(a), this.a.processLicense && (a = this.a.processLicense(a)), this.xe = this.a.Ng ? new fk(this.a, this.$i, a, this.Hh) : new ak(this.a, this.$i, a)) : this.a.R(313)
    };
    h.Wi = function() {
        this.a.R(314, new mf(this.u.url, this.f.zb, Cd(this.f), this.f.getAllResponseHeaders(), Ed(this.f)))
    };
    h.ck = function() {
        var a = this.re.info,
            b = this.re.data;
        a && a.Hc && this.he && b && (F(hk, "decrypt segment"), this.Qd = new Zj(this.a, this.Tk.bind(this, a), this.he, a.Hc, b), this.Ca = this.re = null)
    };
    h.Tk = function(a, b) {
        this.ye(a, new Uint8Array(b))
    };
    h.ye = function(a, b) {
        F(hk, "process segment");
        this.Qd = null;
        switch (this.Ke) {
            case 2:
            case 1:
                b = new(2 == this.Ke ? Xj : Tj)(this.sa.Qa.codecs, b, a.Ga, this.a.processMetadata, this.a.R.bind(this.a));
                var c = a.gf || a.U;
                var d = this.Pj;
                var e = a.Sb || void 0,
                    f = a.Hc || void 0,
                    g = d.endOfLastSegment;
                g = c || !u(g) ? b.Va : g;
                if (b.parse(g)) {
                    b.Va = g;
                    d.endOfLastSegment = g + b.vh / b.sampleRate * b.Td.length;
                    d = new tg(2 * b.ff.length);
                    c && (c = null, e && (c = {
                        systemId: zf,
                        url: null,
                        Xd: rj(e),
                        ie: new xf(Ef(e.ge), !1),
                        Wj: f,
                        Bf: f ? f.length : 16,
                        scheme: 1667392371
                    }), F(Rj, "Built AudioMoov with objectType(0x" +
                        b.Ph.toString(16) + ") and sampleRate(" + b.sampleRate + ")"), (new Ij(c, b.Vd(), b.Ph, b.sampleRate, b.Pd)).h(d));
                    f = g * b.Vd();
                    c = b.vh / b.sampleRate * b.Vd();
                    g = b.Td;
                    if (e) {
                        e = b.Td;
                        for (var k = [], l = 0; l < e.length; l++) {
                            var n = e[l].length;
                            if (0 == n % 16) k.push({
                                Cj: [{
                                    og: 16,
                                    pg: n - 16
                                }]
                            });
                            else {
                                var q = n % 16;
                                k.push({
                                    Cj: [{
                                        og: 16,
                                        pg: n - 16 - q
                                    }, {
                                        og: q,
                                        pg: 0
                                    }]
                                })
                            }
                        }
                        e = k
                    } else e = void 0;
                    (new Kj(c, f, g, e)).h(d);
                    (new oj(b.Td)).h(d);
                    d = sg(d);
                    Mg(d)
                } else d = null;
                e = d;
                if (!e) {
                    this.a.R(315);
                    return
                }
                d = b;
                b = e;
                break;
            case 4:
                break;
            default:
                d = new Ui(b, this.a.processMetadata,
                    a.Ga)
        }
        if (a.gf || a.U) this.ea = null, a.U = !1;
        a = {
            time: a.Ga,
            duration: a.duration
        };
        e = d ? d.mi() : a.time;
        if (d = null === this.ea && Infinity != e) F(hk, "start: " + e), this.ea = 4 == this.Ke ? 0 : e, this.dc = a, this.Hj && (f = this.Hj, f.Y && f.Y.U(e, a));
        e = this.xh;
        this.xh = null;
        Mh(e, b, a, this.ea || 0, d)
    };
    var jk = function(a) {
            a.a.Ng && !a.Hh ? cast.__platform__.cryptokeys.getKeyByName("CKP").then(function(b) {
                a.Hh = b;
                ik(a)
            }) : ik(a)
        },
        ik = function(a) {
            a.he = null;
            a.u.url = a.yb;
            a.u.skipRequest = !1;
            if (a.a.updateLicenseRequestInfo && (a.a.updateLicenseRequestInfo(a.u), a.u.skipRequest)) return;
            a.f.Kd = a.u.withCredentials;
            a.f.Ob = Math.max(0, a.u.timeoutInterval);
            a.f.send(a.u.url, void 0, void 0, a.u.headers)
        };
    h = gk.prototype;
    h.processSegment = function(a, b, c) {
        this.xh = c;
        this.re = {
            data: b,
            info: a
        };
        (c = a.Fi) ? this.yb != c || null === this.he ? (this.yb = c, jk(this)) : this.Ca = setTimeout(this.di, 0): this.ye(a, b)
    };
    h.updateSegmentRequestInfo = function(a) {
        var b = ek(this.wb);
        if (b) {
            a.mc = b;
            this.Id && (b.U = !0, this.Id = !1);
            var c = b.Sh;
            c && (a.headers = {}, a.headers.Range = "bytes=" + c.start + "-" + c.end);
            a.url = b.url;
            a.interval = this.getSegmentInterval()
        }
    };
    h.getSegmentInterval = function() {
        var a = ek(this.wb);
        return a ? {
            time: a.Ga,
            duration: a.duration
        } : {
            time: 0,
            duration: 0
        }
    };
    h.wa = function() {
        return this.wb.wa()
    };
    h.vb = function() {
        return this.wb.vb()
    };
    var kk = function(a) {
        var b = ek(a.wb);
        if (b.Bh || b.Sb && b.Sb.ge != a.Di && 0 == a.Ke) a.Ff = !0
    };
    gk.prototype.fc = function() {
        var a = this.wb.next();
        a && kk(this);
        return a
    };
    gk.prototype.seek = function(a) {
        a = this.wb.seek(a);
        x(a) && kk(this);
        return a
    };
    gk.prototype.pe = function() {
        lk(this, this.sa.qa)
    };
    var lk = function(a, b) {
        a.wb.update(b);
        a.Mf && (Th(a.Mf), a.Mf = null)
    };
    h = gk.prototype;
    h.Rf = function(a, b, c) {
        this.pj = a;
        this.Mf = b;
        this.Id = !0;
        this.sa && this.sa.qa && this.sa.qa.Z && this.sa.Qb();
        this.sa = c;
        c.qa ? lk(this, c.qa) : c.load()
    };
    h.getQualityLevel = function() {
        return this.pj
    };
    h.U = function(a, b) {
        this.Id = !0;
        this.ea = a;
        this.dc = b
    };
    h.Ud = function() {
        var a = ek(this.wb);
        if (a.Bh) {
            this.Ff = !1;
            var b = Ff("hls-sample-aes-init-segment");
            return b && b.clear_leader ? Ja(window.atob(b.clear_leader)) : null
        }
        if (!a.Sb || !a.Hc) return null;
        this.Ff = !1;
        this.Di = a.Sb.ge;
        b = a.Sb;
        a = a.Hc;
        var c = Ff("hls-sample-aes-init-segment");
        c = c ? {
            oj: c.pssh_offset,
            Ci: c.key_id_offset,
            Bi: c.iv_offset,
            Dj: Ja(window.atob(c.template))
        } : {
            oj: 196,
            Ci: 530,
            Bi: 548,
            Dj: new Uint8Array(tj)
        };
        var d = c.Dj;
        d.set(sj(b), c.oj);
        d.set(Ef(b.ge), c.Ci);
        d.set(a, c.Bi);
        return d
    };
    h.qd = function() {
        return this.Ff
    };
    var hk = B("cast.player.hls.Adaptation"),
        mk = function(a) {
            gk.call(this, a, null, 3)
        };
    m(mk, gk);
    mk.prototype.fc = function() {
        return null === this.ea || null === this.dc ? !1 : gk.prototype.fc.call(this)
    };
    mk.prototype.seek = function(a) {
        return null === this.ea || null === this.dc ? null : gk.prototype.seek.call(this, a)
    };
    mk.prototype.processSegment = function(a, b, c) {
        var d = {
                time: a.Ga,
                duration: a.duration
            },
            e = !1;
        if (null === this.ea || null === this.dc) b = new Uint8Array(0);
        else if (this.Id || a.U) d = this.dc, e = !0;
        Mh(c, b, d, this.ea || 0, e)
    };
    var nk = function(a, b) {
        this.Sj = new I(a);
        this.ga = b;
        this.g = 0;
        this.fe = this.We = this.yb = this.rc = this.nb = null;
        this.Dg = this.oc = 0;
        this.zg = !1;
        this.yd = null;
        this.ri = this.pi = !1;
        this.C = {
            yi: !1,
            Z: !0,
            Kf: "none",
            Lg: !1,
            Vf: 0,
            Fh: 0,
            c: [],
            Lb: [],
            Ni: []
        }
    };
    nk.prototype.parse = function(a) {
        a = p(a.split("\n"));
        for (var b = a.next(); !b.done; b = a.next())
            if (!ok(this, b.value)) return E(pk, "failed to parse HLS playlist"), this.C = null;
        if (this.pi && this.ri)
            for (a = 0; a < this.C.Lb.length;) b = this.C.Lb[a].codecs, qk(this.C.Lb[a].mimeType, b) ? (E(pk, "filtered out " + b + " stream"), this.C.Lb.splice(a, 1)) : a++;
        if (0 < this.C.c.length && (a = this.C.c[0], !a.Sb))
            for (b = 1; b < this.C.c.length; b++) this.C.c[b].Sb && (a.Bh = !0);
        return this.C
    };
    var qk = function(a, b) {
            return 0 <= a.indexOf("audio") || 0 > b.indexOf(",") && (0 == b.indexOf("mp4a.") || "ac-3" == b) ? !0 : !1
        },
        rk = function(a, b) {
            var c = new I(b);
            c.Fb || (b = a.Sj.resolve(c).toString());
            return b
        },
        ok = function(a, b) {
            b = b.trim();
            if (!b) return !0;
            if ("#" != b[0]) {
                if (3 != a.g && 4 != a.g) sk(a, "URI"), a = !1;
                else {
                    b = rk(a, b);
                    if (3 == a.g) {
                        a.nb.Jf = a.yd;
                        null !== a.yd && (a.yd += 1E3 * a.nb.duration);
                        if (a.yb || a.We) {
                            a.nb.Fi = a.yb;
                            a.nb.Sb = a.We;
                            var c = a.nb;
                            if (a.fe) {
                                var d = a.fe;
                                for (var e = new Uint8Array(16), f = 15, g = d.length; 0 < g; g -= 2, f--) e[f] = parseInt(1 <
                                    g ? d.substr(g - 2, 2) : d.substr(0, 1), 16)
                            } else
                                for (d = a.nb.yh, e = new Uint8Array(16), f = 15; 0 <= f; f--) e[f] = d & 255, d >>= 8;
                            d = e;
                            c.Hc = d
                        }
                        a.nb.url = b;
                        a.C.c.push(a.nb)
                    } else a.rc.url = b, a.C.Lb.push(a.rc);
                    a.g = 2;
                    a = !0
                }
                return a
            }
            c = "#EXTINF:";
            if (0 == b.indexOf(c)) return d = b.indexOf(","), b = b.substr(c.length, (0 <= d ? d : b.length) - c.length), 2 != a.g && 1 != a.g ? (sk(a, "EXTINF"), a = !1) : (b = parseFloat(b), a.nb = {
                    yh: a.oc,
                    url: "",
                    Sh: null,
                    Ga: a.Dg,
                    duration: b,
                    gf: a.zg,
                    Jf: a.yd,
                    Fi: null,
                    Sb: null,
                    Hc: null,
                    U: !1,
                    Bh: !1
                }, a.C.Fh += b, a.Dg += b, a.zg = !1, a.oc += 1, a.g = 3, a = !0),
                a;
            c = "#EXT-X-KEY:";
            if (0 == b.indexOf(c)) return tk(a, b.substr(c.length));
            if (0 == b.indexOf("#EXT-X-DISCONTINUITY-SEQUENCE")) return 1 != a.g ? (sk(a, "EXT-X-DISCONTINUITY-SEQUENCE"), a = !1) : a = !0, a;
            if (0 == b.indexOf("#EXT-X-DISCONTINUITY")) return 2 != a.g && 1 != a.g ? (sk(a, "EXT-X-DISCONTINUITY"), a = !1) : (a.zg = !0, a.g = 2, a = !0), a;
            c = "#EXT-X-PROGRAM-DATE-TIME:";
            if (0 == b.indexOf(c)) return b = b.substr(c.length), 2 != a.g && 3 != a.g && 1 != a.g ? (sk(a, "EXT-X-PROGRAM-DATE-TIME"), a = !1) : (c = Date.parse(b), isNaN(c) ? (E(pk, "cannot parse #EXT-X-PROGRAM-DATE-TIME: " +
                b), a = !1) : (a.yd = c, a = a.C.Lg = !0)), a;
            c = "#EXT-X-BYTERANGE:";
            if (0 == b.indexOf(c)) return b = b.substr(c.length), 3 != a.g ? (sk(a, "EXT-X-BYTERANGE"), a = !1) : (b = b.split("@"), c = parseInt(b[1], 10), a.nb.Sh = {
                start: c,
                end: c + parseInt(b[0], 10) - 1
            }, a = !0), a;
            if (0 == b.indexOf("#EXTM3U")) return 0 != a.g ? (sk(a, "EXTM3U"), a = !1) : (a.g = 1, a = !0), a;
            if (0 == b.indexOf("#EXT-X-ENDLIST")) return 2 != a.g && 1 != a.g ? (sk(a, "EXT-X-ENDLIST"), a = !1) : (a.C.Z = !1, a = !0), a;
            c = "#EXT-X-STREAM-INF:";
            if (0 == b.indexOf(c)) {
                e = b.substr(c.length);
                if (1 != a.g && 2 != a.g) sk(a, "EXT-X-STREAM-INF"),
                    a = !1;
                else if (g = e.match("BANDWIDTH=([0-9]+)")) {
                    b = parseInt(g[1], 10);
                    c = "video/mp2t";
                    (g = e.match('CODECS="([^"]*)"')) ? (d = g[1], qk(c, d) ? (a.pi = !0, f = a.ga, 1 == f || 2 == f ? c = "audio/mp4" : 4 == f && (c = "audio/mpeg", d = "")) : (a.ri = !0, d = d.replace("mp4a.40.34", "mp4a.6B"))) : d = "avc1.4D401E,mp4a.40.2";
                    f = {
                        width: null,
                        height: null
                    };
                    if (g = e.match("RESOLUTION=(\\d+x\\d+)")) e = g[1].split("x"), 2 === e.length && (f.width = parseInt(e[0], 10), f.height = parseInt(e[1], 10));
                    a.rc = {
                        name: null,
                        url: "",
                        ka: b,
                        mimeType: c,
                        codecs: d,
                        Tg: !0,
                        language: null,
                        rj: f
                    };
                    a.C.yi = !0;
                    a.g = 4;
                    a = !0
                } else E(pk, "no BANDWIDTH attribute"), a = !1;
                return a
            }
            c = "#EXT-X-TARGETDURATION:";
            if (0 == b.indexOf(c)) return b = b.substr(c.length), 1 != a.g ? (sk(a, "EXT-X-TARGETDURATION"), a = !1) : (a.C.Vf = parseInt(b, 10), a = !0), a;
            c = "#EXT-X-MEDIA-SEQUENCE:";
            if (0 == b.indexOf(c)) return b = b.substr(c.length), 1 != a.g ? (sk(a, "EXT-X-MEDIA-SEQUENCE"), a = !1) : (a.oc = parseInt(b, 10), a = !0), a;
            c = "#EXT-X-MEDIA:";
            return 0 == b.indexOf(c) ? uk(a, b.substr(c.length)) : !0
        },
        sk = function(a, b) {
            E(pk, "unexpected " + b + ": state " + a.g)
        },
        uk = function(a, b) {
            if (2 !=
                a.g && 1 != a.g) return sk(a, "EXT-X-MEDIA"), !1;
            var c = null,
                d = null,
                e = null,
                f = !1,
                g = b.match("TYPE=(AUDIO|VIDEO|SUBTITLES|CLOSED-CAPTIONS)");
            if (null === g) return !0;
            switch (g[1]) {
                case "SUBTITLES":
                    var k = "webvtt";
                    var l = "text/vtt";
                    break;
                case "AUDIO":
                    k = Gf(a.ga);
                    l = "audio/mp4";
                    break;
                default:
                    return !0
            }
            g = b.match('URI="([^"]*)"');
            null === g || (c = rk(a, g[1]));
            g = b.match('LANGUAGE="([^"]*)"');
            null === g || (d = g[1]);
            g = b.match('NAME="([^"]*)"');
            null === g || (e = g[1]);
            g = b.match('DEFAULT="?(YES|NO)"?');
            null === g || (f = "YES" == g[1]);
            c && a.C.Ni.push({
                name: e,
                url: c,
                ka: 0,
                mimeType: l,
                codecs: k,
                Tg: f,
                language: d
            });
            return !0
        },
        tk = function(a, b) {
            if (2 != a.g && 3 != a.g && 1 != a.g) return sk(a, "EXT-X-KEY"), !1;
            var c;
            if (0 <= b.indexOf("METHOD=AES-128")) {
                if (c = b.match('URI="([^"]*)"')) a.yb = rk(a, c[1]);
                c = b.match("IV=0[x|X]([0-9a-fA-F]+)");
                a.fe = c ? c[1] : null;
                a.We = null;
                a.C.Kf = "aes_128";
                return !0
            }
            if (0 <= b.indexOf("METHOD=SAMPLE-AES")) {
                c = b.match('KEYFORMAT="([^"]*)"');
                if (!c) return E(pk, "Missing KEYFORMAT for SAMPLE-AES"), !0;
                c = c[1];
                if ("com.widevine" != c) return E(pk, "KEYFORMAT " + c + " is not supported for SAMPLE-AES"), !0;
                if (1 != Ff("enable-hls-sample-aes")) return E(pk, "SAMPLE-AES not support by Cast platform"), !1;
                if (c = b.match('URI="data:.*?,(.*?)"')) {
                    try {
                        var d = JSON.parse(window.atob(c[1]))
                    } catch (e) {
                        return !1
                    }
                    c = d.key_ids;
                    if (!c || !Array.isArray(c) || 0 == c.length) return E(pk, "no Widevine key ID"), !1;
                    a.We = {
                        Wk: d.provider,
                        Yh: d.content_id,
                        ge: c[0]
                    }
                }
                c = b.match("IV=0[x|X]([0-9a-fA-F]+)");
                a.fe = c ? c[1] : null;
                a.yb = null;
                a.C.Kf = "widevine";
                return !0
            }
            if (0 <= b.indexOf("METHOD=NONE")) return a.yb = null, a.We = null, a.fe = null, !0;
            E(pk, "unsupported KEY");
            return !1
        },
        pk = B("cast.player.hls.Parser");
    var vk = function(a, b, c, d) {
        C.call(this);
        this.a = a;
        this.ue = b;
        this.Ab = new fi(this);
        this.fa = null;
        this.ol = this.bg.bind(this);
        this.Qa = c;
        this.duration = -1;
        this.qa = null;
        this.ga = d
    };
    m(vk, C);
    h = vk.prototype;
    h.G = function() {
        this.Ab.M();
        null !== this.fa && (clearTimeout(this.fa), this.fa = null);
        C.prototype.G.call(this)
    };
    h.load = function() {
        this.bg()
    };
    h.Qb = function() {
        this.duration = -1;
        this.qa = null;
        this.Ab.abort();
        null !== this.fa && (clearTimeout(this.fa), this.fa = null)
    };
    h.bg = function() {
        F(wk, "update: " + this.Qa.url);
        this.Ab.od(this.a, this.Qa.url)
    };
    h.kc = function(a) {
        a ? (this.qa = (new nk(this.Qa.url, this.ga)).parse(a)) ? (this.qa.Z ? (a = 1E3 * this.qa.Vf, this.fa = setTimeout(this.ol, a), F(wk, "update in: " + a)) : this.duration = this.qa.Fh, this.ue.pe(this)) : this.a.R(412) : this.a.R(312, di(this.Ab))
    };
    var wk = B("cast.player.hls.Playlist");
    var xk = function(a, b, c, d) {
        C.call(this);
        this.a = a;
        this.Ab = new fi(this);
        this.le = b;
        this.ue = c;
        this.ga = d;
        this.sc = [];
        this.Cb = []
    };
    m(xk, C);
    xk.prototype.G = function() {
        this.Ab.M();
        for (var a = p(this.Cb), b = a.next(); !b.done; b = a.next()) b.value.M();
        this.Cb.length = 0;
        a = p(this.sc);
        for (b = a.next(); !b.done; b = a.next()) b.value.M();
        this.sc.length = 0;
        C.prototype.G.call(this)
    };
    xk.prototype.load = function() {
        this.Ab.od(this.a, this.a.url)
    };
    xk.prototype.kc = function(a) {
        if (a) {
            var b = this.Ab.Zc;
            if (b)
                if (a = (new nk(b, this.ga)).parse(a)) {
                    if (a.yi) {
                        yk(a);
                        if (0 === a.Lb.length) {
                            this.a.R(411);
                            return
                        }
                        b = p(a.Lb);
                        for (var c = b.next(); !c.done; c = b.next()) c = new vk(this.a, this.ue, c.value, this.ga), this.sc.push(c)
                    } else {
                        switch (this.ga) {
                            case 1:
                                c = "mp4a.40.2";
                                var d = "audio/mp4";
                                break;
                            case 2:
                                c = "ac-3";
                                d = "audio/mp4";
                                break;
                            case 4:
                                c = "";
                                d = "audio/mpeg";
                                break;
                            default:
                                c = "avc1.4D401E,mp4a.40.2", d = "video/mp2t"
                        }
                        b = new vk(this.a, this.ue, {
                            name: null,
                            url: b,
                            ka: 0,
                            mimeType: d,
                            codecs: c,
                            Tg: !0,
                            language: null
                        }, this.ga);
                        this.sc.push(b)
                    }
                    a = p(a.Ni);
                    for (b = a.next(); !b.done; b = a.next()) b = b.value, c = 0, d = this.ga, Fd(b.mimeType) && u(d) && (c = d, 0 == d && (b.mimeType = "video/mp2t")), b = new vk(this.a, this.ue, b, c), this.Cb.push(b);
                    this.le.onManifestReady()
                } else this.a.R(411)
        } else this.a.R(311, di(this.Ab))
    };
    var yk = function(a) {
            a.Lb = a.Lb.filter(function(a) {
                if (Kf) {
                    var b = a.codecs;
                    var d = a.mimeType,
                        e = a.rj.width;
                    a = a.rj.height;
                    x(e) && x(a) && 2160 <= a && (b || "audio/mpeg" != d) ? (d = d + "; codecs=" + b, e && a && (d += "; width=" + e + "; height=" + a), Id(b) && (d += "; eotf=smpte2084"), b = Kf(d), F(zk, "canDisplayType(" + d + "): " + b)) : b = !0
                } else b = !0;
                return b
            })
        },
        zk = B("cast.player.hls.MasterPlaylist");
    var Y = function(a, b) {
        this.a = a;
        this.ga = b;
        this.ra = this.ef = this.Mi = this.le = null;
        this.hc = new gk(a, this, 0);
        this.Y = this.oa = null;
        this.wi = void 0
    };
    Y.prototype.load = function(a) {
        this.ef = this.Mi = this.le = a;
        this.ra = new xk(this.a, this, this, this.ga);
        this.ra.load()
    };
    Y.prototype.Qb = function() {
        this.hc.M();
        this.oa && (this.oa.M(), this.oa = null);
        this.Y && (this.Y.M(), this.Y = null);
        this.ra && (this.ra.M(), this.ra = null)
    };
    Y.prototype.onManifestReady = function() {
        var a = this.getDefaultAudioStreamIndex();
        this.enableStream(a, !0);
        a = !0;
        for (var b = p(this.ra.sc), c = b.next(); !c.done; c = b.next()) c = c.value, qk(c.Qa.mimeType, c.Qa.codecs) || (a = !1);
        a && u(this.ga) && (this.hc.Ke = this.ga);
        if (this.a.onManifestReady) this.a.onManifestReady();
        this.le.onManifestReady();
        this.le = null
    };
    Y.prototype.getDefaultAudioStreamIndex = function() {
        for (var a = this.ra.Cb, b = -1, c = 0; c < a.length; c++) {
            var d = a[c].Qa;
            if (Hf(d.codecs) && d.Tg)
                if ("mp4a.a5" == d.codecs) {
                    b = c;
                    break
                } else -1 == b && (b = c)
        }
        return -1 < b ? b + 1 : b
    };
    Y.prototype.getDefaultAudioStreamIndex = Y.prototype.getDefaultAudioStreamIndex;
    Y.prototype.getStreamCount = function() {
        return this.ra.Cb.length + 1
    };
    Y.prototype.getStreamCount = Y.prototype.getStreamCount;
    Y.prototype.enableStream = function(a, b) {
        if (0 < a) {
            a = this.ra.Cb[a - 1];
            var c = a.Qa.mimeType;
            Fd(c) || Hf(a.Qa.codecs) ? (this.oa && (this.oa.M(), this.oa = null), b && Bf("audio/mp4", Gf(this.ga)) && (this.oa = new gk(this.a, this, u(this.ga) ? this.ga : 1), this.oa.sa = a)) : Da(c, "text/") && (this.Y && (this.Y.M(), this.Y = null), b && (this.Y = new mk(this.a), this.Y.sa = a, this.Y.U(this.hc.ea, this.hc.dc)))
        }
    };
    Y.prototype.enableStream = Y.prototype.enableStream;
    Y.prototype.isStreamEnabled = function(a) {
        return null !== Ak(this, a)
    };
    Y.prototype.isStreamEnabled = Y.prototype.isStreamEnabled;
    var Ak = function(a, b) {
        0 == b ? a = a.hc : (b = a.ra.Cb[b - 1], a = null === a.oa || a.oa.sa != b ? null === a.Y || a.Y.sa != b ? null : a.Y : a.oa);
        return a
    };
    Y.prototype.getQualityLevel = function(a) {
        return Ak(this, a).getQualityLevel()
    };
    Y.prototype.getQualityLevel = Y.prototype.getQualityLevel;
    Y.prototype.getStreamInfo = function(a) {
        var b = [],
            c;
        if (0 == a) {
            b = [];
            a = this.ra.sc;
            for (c = 0; c < a.length; c++) b.push(a[c].Qa.ka);
            a = this.ra.sc[0].Qa;
            c = a.codecs;
            if (this.oa)
                for (var d = a.codecs.split(","), e = 0; e < d.length; e++)
                    if (0 == d[e].indexOf("avc1.")) {
                        c = d[e];
                        break
                    }
        } else a = this.ra.Cb[a - 1].Qa, b.push(a.ka), c = a.codecs;
        return new nf(c, a.mimeType, b, a.language, a.name, null)
    };
    Y.prototype.getStreamInfo = Y.prototype.getStreamInfo;
    h = Y.prototype;
    h.Rf = function(a, b, c) {
        Ak(this, a).Rf(b, c, 0 == a ? this.ra.sc[b] : this.ra.Cb[a - 1])
    };
    h.reset = function(a) {
        Ak(this, a).reset()
    };
    h.wa = function(a) {
        return Ak(this, a).wa()
    };
    h.pe = function(a) {
        this.hc.sa == a ? this.hc.pe() : null !== this.oa && this.oa.sa == a ? this.oa.pe() : null !== this.Y && this.Y.sa == a && this.Y.pe();
        if (this.ef) {
            var b = a.qa.Kf;
            switch (b) {
                case "widevine":
                    this.ef.ne([b]);
                    break;
                case "aes_128":
                    this.a.Ng && (b = "aes_128_ckp", a.qa.Kf = b);
                    Qd(b, 2);
                    break;
                case "none":
                    break;
                default:
                    E(uf, "Unexpected HLS protection type")
            }
            this.ef = null
        }
        this.Mi.fh();
        u(this.wi) || (Rd(a.qa.Z), this.wi = a.qa.Z)
    };
    h.updateLicenseRequestInfo = function() {};
    h.getDuration = function() {
        var a = this.hc.sa;
        return a ? a.duration : -1
    };
    h.seek = function(a, b) {
        return Ak(this, a).seek(b)
    };
    h.fc = function(a) {
        return Ak(this, a).fc()
    };
    h.vb = function(a) {
        return Ak(this, a).vb()
    };
    h.getSegmentInterval = function(a) {
        return Ak(this, a).getSegmentInterval()
    };
    h.qd = function(a) {
        return Ak(this, a).qd()
    };
    h.xj = function() {};
    h.Ud = function(a) {
        return Ak(this, a).Ud()
    };
    h.updateSegmentRequestInfo = function(a, b) {
        Ak(this, a).updateSegmentRequestInfo(b)
    };
    h.processSegment = function(a, b, c) {
        b.Fc ? Mh(c, b.data, {
            time: 0,
            duration: 0
        }, 0, !0) : Ak(this, a).processSegment(b.mc, b.data, c)
    };
    h.lf = function() {
        return 2
    };
    var Dk = function(a) {
            this.Z = new ki("IsLive");
            this.tc = new U("TimeScale");
            for (this.duration = new U("Duration"); a;) {
                if ("SmoothStreamingMedia" == a.nodeName) {
                    pi(a.attributes, this);
                    break
                }
                a = a.nextElementSibling
            }
            this.streams = [];
            this.kb = null;
            if (a)
                for (this.tc.value || (this.tc.value = 1E7), a = a.firstElementChild; null !== a; a = a.nextElementSibling)
                    if ("StreamIndex" == a.nodeName) {
                        var b = new Bk(a, this.tc.value);
                        0 < b.Ua.length && this.streams.push(b)
                    } else "Protection" == a.nodeName && (this.kb = new Ck(a.firstElementChild))
        },
        Ek = function(a,
            b) {
            V.call(this, a, b)
        };
    m(Ek, V);
    Ek.prototype.parse = function(a) {
        switch (a) {
            case "H264":
            case "AVC1":
                this.value = "avc1.4D40";
                break;
            case "AACL":
                this.value = "mp4a.40.2";
                break;
            case "EC-3":
                this.value = "mp4a.a6";
                break;
            case "AACH":
                this.value = "mp4a.40.5";
                break;
            case "DFXP":
            case "TTML":
                this.value = "ttml";
                break;
            default:
                this.value = null
        }
    };
    var Fk = function() {
        T.call(this, "CodecPrivateData");
        this.we = this.Wc = null
    };
    m(Fk, T);
    Fk.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        a = a.split("00000001");
        3 == a.length && (this.Wc = Ef(a[1]), this.we = Ef(a[2]))
    };
    var Gk = function() {
        T.call(this, "CodecPrivateData");
        this.value = null
    };
    m(Gk, T);
    Gk.prototype.parse = function(a) {
        T.prototype.parse.call(this, a);
        a && (this.value = Ef(a))
    };
    var Hk = function(a) {
            this.ka = new U("Bitrate");
            this.format = new Ek("FourCC", a)
        },
        Ik = function(a) {
            Hk.call(this, "ttml");
            pi(a.attributes, this)
        };
    m(Ik, Hk);
    var Jk = function(a) {
        Hk.call(this, "avc1.4D401E");
        this.width = new U("MaxWidth");
        this.height = new U("MaxHeight");
        this.ab = new Fk;
        pi(a.attributes, this)
    };
    m(Jk, Hk);
    var Kk = function(a) {
        Hk.call(this, "mp4a.40.2");
        this.sampleRate = new U("SamplingRate");
        this.Uj = new U("Channels");
        this.ab = new Gk;
        pi(a.attributes, this)
    };
    m(Kk, Hk);
    var Bk = function(a, b) {
            this.type = new V("Type");
            this.url = new V("Url");
            this.name = new V("Name");
            this.language = new V("Language");
            pi(a.attributes, this);
            this.Mb = 0;
            switch (this.type.value) {
                case "video":
                    this.Mb = 2;
                    break;
                case "audio":
                    this.Mb = 1;
                    break;
                case "text":
                    this.Mb = 3
            }
            this.c = [];
            this.Ua = [];
            var c = new og("0");
            for (a = a.firstElementChild; null !== a; a = a.nextElementSibling)
                if ("QualityLevel" == a.nodeName) {
                    var d = a;
                    switch (this.Mb) {
                        case 2:
                            d = new Jk(d);
                            break;
                        case 1:
                            d = new Kk(d);
                            break;
                        case 3:
                            d = new Ik(d);
                            break;
                        default:
                            d = null
                    }
                    d &&
                        d.format.value && this.Ua.push(d)
                } else if ("c" == a.nodeName) {
                var e = a;
                d = b;
                var f = e.attributes.getNamedItem("t");
                f && c.reset(f.value);
                f = null;
                var g = -1;
                e.attributes.getNamedItem("d") && (f = parseInt(e.attributes.d.value, 10), g = f / d);
                e = (e = e.attributes.getNamedItem("r")) ? parseInt(e.value, 10) : 1;
                for (var k = 0; k < e; k++) this.c.push({
                    time: parseInt(c.toString(), 10) / d,
                    duration: g,
                    Eb: new og(c.toString()),
                    offset: 0,
                    size: 0,
                    url: null
                }), null === f || c.add(f)
            }
            c = this.c.length - 1;
            for (a = 0; a < c; a++)
                if (d = this.c[a], f = this.c[a + 1], 0 > d.duration) {
                    f =
                        f.Eb;
                    g = d.Eb;
                    if (f.X == g.X) f = 0;
                    else {
                        if (!pg(f, g)) throw Error("Value must be smaller than the current value");
                        k = e = 0;
                        for (var l = 1, n = 0; n < f.X.length; n++) {
                            var q = parseInt(f.X.charAt(f.X.length - 1 - n), 10) - (n < g.X.length ? parseInt(g.X.charAt(g.X.length - 1 - n), 10) : 0) - k;
                            k = 0 > q ? 1 : 0;
                            e += (k ? 10 + q : q) * l;
                            l *= 10
                        }
                        if (k) throw Error("Value must be smaller than the current value");
                        if (e > Number.MAX_SAFE_INTEGER) throw Error("Difference lost precision");
                        f = e
                    }
                    d.duration = f / b
                }
        },
        Ck = function(a) {
            this.systemId = new oi("SystemID");
            pi(a.attributes,
                this);
            this.Be = null;
            this.systemId.value && yf.sb(this.systemId.value) && (this.Be = gi(Ja(window.atob(a.textContent.trim()))))
        };
    B("cast.player.smooth");
    var Lk = function(a) {
        S.call(this, a);
        this.Ce = null;
        this.Xa = 1E7
    };
    m(Lk, S);
    Lk.prototype.bh = function(a) {
        var b = a.kb;
        if (b) {
            b = b.Be;
            if (null === b) {
                Mk(this, "invalid protection info");
                return
            }
            this.Ce = b
        }
        a.tc.value && (this.Xa = a.tc.value);
        a.duration.value && (this.duration = a.duration.value / this.Xa);
        b = 1 == a.Z.value;
        u(this.Z) || Rd(b);
        this.Z = b;
        b = Infinity;
        for (var c = p(a.streams), d = c.next(); !d.done; d = c.next()) d = d.value, (2 == d.Mb || 1 == d.Mb) && d.c[0].time < b && (b = d.c[0].time);
        c = [];
        a = p(a.streams);
        for (d = a.next(); !d.done; d = a.next()) {
            var e = d.value;
            if (0 == e.Ua.length) {
                Mk(this, "no quality levels");
                return
            }
            d =
                e.Ua[0].format.value;
            if (null === d) {
                Mk(this, "unknown media format");
                return
            }
            var f = null;
            if (2 == e.Mb) a: {
                f = b;
                var g = e.Ua[0];
                if (null === g.ab.Wc) Mk(this, "no sps"),
                f = null;
                else {
                    g = g.ab.Wc[3].toString(16);
                    1 == g.length && (g = "0" + g);
                    for (var k = Nk(2, "video/mp4", d + g.toLowerCase(), e.language.value, e.name.value), l = 0; l < e.Ua.length; l++) {
                        g = e.Ua[l];
                        if (null === g.ka.value || null === g.width.value || null === g.height.value || null === g.ab.Wc || null === g.ab.we) {
                            Mk(this, "invalid video quality");
                            f = null;
                            break a
                        }
                        var n = this.Ce,
                            q = this.Xa,
                            r = g.width.value,
                            v = g.height.value,
                            H = g.ab.Wc,
                            pa = g.ab.we,
                            pb = new tg;
                        (new Jj(n, q, r, v, H, pa)).h(pb);
                        k.v.push(Ok(e.url.value, g.ka.value, f, e.c, sg(pb)))
                    }
                    f = k
                }
            }
            else if (1 == e.Mb) a: {
                f = b;g = Nk(1, "audio/mp4", d, e.language.value, e.name.value);
                for (k = 0; k < e.Ua.length; k++) {
                    l = e.Ua[k];
                    if (null === l.ka.value || null === l.sampleRate.value) {
                        Mk(this, "invalid audio quality");
                        f = null;
                        break a
                    }(n = l.ab.value) || (n = l.Uj.value, n = xg(2, l.sampleRate.value, null === n ? 2 : n));
                    v = n;
                    if (null === v) {
                        Mk(this, "invalid audio codec private data");
                        f = null;
                        break a
                    }
                    n = this.Ce;
                    q = this.Xa;
                    r = l.sampleRate.value;
                    H = "mp4a.a6" == d ? 166 : 64;
                    pa = new tg;
                    (new Ij(n, q, H, r, v)).h(pa);
                    g.v.push(Ok(e.url.value, l.ka.value, f, e.c, sg(pa)))
                }
                f = g
            }
            else if (3 == e.Mb) {
                f = b;
                g = Nk(3, "text/mp4", d, e.language.value, e.name.value, !1);
                for (k = 0; k < e.Ua.length; k++) g.v.push(Ok(e.url.value, e.Ua[k].ka.value || 0, f, e.c, null));
                f = g
            }
            f && ("mp4a.a6" == d ? c.push(f) : this.l.push(f))
        }
        Array.prototype.push.apply(this.l, c)
    };
    var Mk = function(a, b) {
        a.host.R(431, void 0, b, Pk)
    };
    h = Lk.prototype;
    h.kc = function(a, b) {
        if (a) {
            b && (this.uri = new I(b));
            var c = (new DOMParser).parseFromString(a, "text/xml");
            c = new Dk(c.firstChild);
            this.bh(c);
            S.prototype.kc.call(this, a, b);
            this.Ce && this.Qc.ne(["playready"])
        } else this.host.R(331, di(this.Kc))
    };
    h.updateLicenseRequestInfo = function(a) {
        a.headers = {};
        a.headers["content-type"] = "text/xml;charset=utf-8";
        a.url = this.Ce.url
    };
    h.Ud = function(a) {
        a = this.l[a];
        a.Na = !1;
        return a.v[a.S].ib.data
    };
    h.updateSegmentRequestInfo = function(a, b) {
        S.prototype.updateSegmentRequestInfo.call(this, a, b);
        var c = this.l[a];
        a = c.index;
        c = c.v[c.S];
        var d = c.url;
        d = d.replace(Qk, c.ka.toString());
        d = d.replace(Rk, c.c[a].Eb.toString());
        b.url = this.uri.resolve(new I(d)).toString().toString()
    };
    h.processSegment = function(a, b, c) {
        var d = this.l[a],
            e = b.data,
            f = b.interval;
        if (b.Fc) Mh(c, e, {
            time: f.time,
            duration: 0
        }, 0, !1);
        else {
            if (this.Z) {
                var g = this.Xa,
                    k = Q(e, 1970628964, Cg, !0);
                k = k ? k.Kg(g) : null;
                if (null === k || 0 == k.length) E(Pk, "no new segments");
                else {
                    a = this.l[a];
                    g = a.v[a.S].c;
                    if (0 != k.length) {
                        for (var l = k[0].time, n = 0; n < g.length; n++) {
                            var q = g[n].time;
                            if (q > l || .3 > Math.abs(q - l)) {
                                g.splice(n, g.length - n);
                                break
                            }
                        }
                        k = p(k);
                        for (l = k.next(); !l.done; l = k.next()) g.push(l.value)
                    }
                    Sk(a)
                }
                if (b.headers && (b = b.headers.match('ChildTrack="([^"]*)"'))) {
                    b =
                        p(b[1].split(/,|;/));
                    for (a = b.next(); !a.done; a = b.next())
                        if (a = a.value.match("([^=]*)=([0-9]*)"))
                            for (g = a[1], k = p(this.l), l = k.next(); !l.done; l = k.next())
                                if (l = l.value, g == l.name) {
                                    g = l.v[0].c;
                                    a = a[2];
                                    k = parseInt(a, 10) / this.Xa;
                                    (0 == g.length || g[g.length - 1].time < k) && g.push({
                                        time: k,
                                        duration: 0,
                                        Eb: new og(a),
                                        offset: 0,
                                        size: 0,
                                        url: null
                                    });
                                    break
                                }
                    b = p(this.l);
                    for (l = b.next(); !l.done; l = b.next()) Sk(l.value)
                }
            }
            b = f.time + d.v[d.S].O;
            if (3 == d.type) e = (e = Q(e, 1835295092)) ? O(e) : null;
            else {
                a = b * this.Xa;
                g = 2 == d.type;
                k = Og(e);
                l = new tg(e.byteLength +
                    1024);
                n = null;
                for (e = 0; 0 < k.length;) switch (q = k.shift(), q.getName()) {
                    case 1836019558:
                        n = Q(O(q), 1970628964, Fg, !0);
                        (new Mj(q, a, g)).h(l);
                        break;
                    case 1835295092:
                        var r = null;
                        n && (r = n.uh ? n.uh : new Uint8Array([]));
                        e = r ? r.length : 0;
                        (new oj(r ? [r, O(q)] : [O(q)])).h(l);
                        break;
                    default:
                        q.h(l)
                }
                a = sg(l);
                Mg(a, e);
                e = a
            }
            null === e ? this.host.R(332, void 0, "no media data", Pk) : (Mh(c, e, f, b, d.U), d.U = !1)
        }
    };
    h.lf = function() {
        return 3
    };
    var Nk = function(a, b, c, d, e, f) {
            return {
                name: e,
                type: a,
                enabled: !1,
                mimeType: b,
                U: !0,
                v: [],
                language: d,
                Na: u(f) ? f : !0,
                codecs: c,
                index: -1,
                S: -1
            }
        },
        Ok = function(a, b, c, d, e) {
            return {
                url: a || "",
                ka: b,
                O: c,
                c: d,
                F: null,
                H: null,
                ib: e ? {
                    url: null,
                    ta: null,
                    data: e
                } : null
            }
        },
        Sk = function(a) {
            var b = a.v[0].c,
                c = b.length - 1E4;
            0 < c && a.index >= c && (b.splice(0, c), a.index -= c)
        },
        Pk = B("cast.player.smooth.Protocol"),
        Qk = /{bitrate}|{Bitrate}/,
        Rk = /{start time}|{start_time}/;
    var Tk = function(a, b, c) {
        this.Kb = a;
        this.bb = b;
        this.Xj = c;
        this.Ed = this.w = null;
        this.Ch = new fe;
        this.a = new Of({
            mediaElement: null,
            url: b.url
        });
        this.a.lj = this.Sk.bind(this);
        this.a.onError = this.rd.bind(this);
        this.a.updateManifestRequestInfo = this.bb.updateManifestRequestInfo;
        this.a.updateSegmentRequestInfo = this.bb.updateSegmentRequestInfo;
        this.a.updateCaptionsRequestInfo = this.bb.updateCaptionsRequestInfo
    };
    Tk.prototype.load = function() {
        switch (this.bb.nj) {
            case 1:
                this.w = new Fi(this.a);
                break;
            case 2:
                this.w = new Y(this.a, this.bb.sk);
                break;
            case 3:
                this.w = new Lk(this.a);
                break;
            default:
                E(Xh, "Unrecognized streaming protocol " + this.bb.nj);
                return
        }
        F(Xh, "Pre caching " + this.bb.url);
        this.w.load(this)
    };
    Tk.prototype.Sk = function(a, b) {
        var c = this;
        if (null === a) return null;
        ed(Uf(this.Kb, b, a), function(a) {
            E(Xh, a);
            Uk(c)
        });
        return a
    };
    Tk.prototype.onManifestReady = function() {
        for (var a = this.w.getStreamCount(), b = 0; b < a; b++)
            if (this.w.isStreamEnabled(b)) {
                var c = this.w.getStreamInfo(b).mimeType;
                this.Ch.enqueue(b, Gd(c) ? 0 : Fd(c) ? 1 : 2)
            }
        Vk(this)
    };
    Tk.prototype.fh = function() {};
    var Vk = function(a) {
        if (a.Ch.pa()) Uk(a);
        else if (Yf(a.Kb)) E(Xh, "Cache is full. Notifies Content Cache to move to next content."), Uk(a);
        else {
            var b = a.Ch.jd();
            a.w.enableStream(b, !0);
            a.Ed = new Wh(a, a.a, a.w, b, a);
            Kh(a.Ed)
        }
    };
    Tk.prototype.ac = function() {
        return this.bb.Qg
    };
    var Yh = function(a, b) {
        if (0 < a.Ed.va(a.bb.Qg)) {
            var c = a.Kb,
                d = a.a.url,
                e = a.w.getQualityLevel(b);
            c.isEnabled() && (c.He.Xb(d) || c.He.set(d, []), c.He.get(d)[b] = e)
        }
        Vk(a)
    };
    Tk.prototype.jc = function() {
        this.Ed.Ie()
    };
    Tk.prototype.rd = function() {
        Uk(this)
    };
    var Uk = function(a) {
        a.w.Qb();
        a.Xj.Si()
    };
    Tk.prototype.ne = function() {};
    Tk.prototype.dh = function() {};
    Tk.prototype.gh = function() {};
    Tk.prototype.ze = function() {};
    var Xh = B("cast.player.cache.ContentLoader");
    var Wk = function() {};
    z("cast.player.cache.ContentCacheCallbacks", Wk);
    Wk.prototype.Si = function() {};
    var Xk = function(a) {
        sf.call(this, a.url);
        this.nj = a.protocolType || 0;
        this.Qg = a.initialTime || 0;
        this.sk = a.hlsSegmentFormat
    };
    m(Xk, sf);
    z("cast.player.api.ContentCacheHost", Xk);
    var Yk = function() {
        this.Kb = Qf.kf();
        this.vg = new ge;
        this.cf = null
    };
    z("cast.player.api.ContentCache", Yk);
    Yk.prototype.load = function(a) {
        if (!this.Kb.isEnabled()) return E(Zk, "Platform cache is not enabled"), Promise.reject(Error("Platform cache is not enabled"));
        this.Kb.reset();
        a = p(a);
        for (var b = a.next(); !b.done; b = a.next()) b = new Tk(this.Kb, b.value, this), this.vg.enqueue(b);
        $k(this);
        this.cf = cd();
        return Promise.resolve(this.cf.mj)
    };
    Yk.prototype.load = Yk.prototype.load;
    var $k = function(a) {
        Yf(a.Kb) || a.vg.pa() ? (a.cf.resolve(), a.cf = null) : a.vg.jd().load()
    };
    Yk.prototype.Si = function() {
        $k(this)
    };
    var Zk = B("cast.player.api.ContentCache");
    var bl = function(a) {
            this.ci = a;
            this.ng = this.ea = this.Hd = this.mode = this.Ef = 0;
            this.Gg = new al(this);
            this.Hg = new al(this);
            this.yc = [];
            this.reset()
        },
        cl = function(a) {
            return a.Hd + a.ea
        };
    bl.prototype.clear = function() {
        this.ng = this.Hd = this.mode = 0;
        this.yc = [];
        this.reset()
    };
    bl.prototype.reset = function() {
        this.mode = 0;
        this.Gg.reset(0);
        this.Hg.reset(1)
    };
    var el = function(a, b, c) {
            if (255 == a && 255 == b || !a && !b) return {
                Ze: a,
                $e: b,
                result: 0
            };
            a = dl[a];
            b = dl[b];
            if (a & 128) {
                if (!(b & 128) && 0 != c.g && c.mg == b) return {
                    Ze: a,
                    $e: b,
                    result: 1
                }
            } else if (b & 128 && 1 <= a && 31 >= a) return {
                Ze: a,
                $e: b,
                result: 2
            };
            return {
                Ze: a,
                $e: b,
                result: 3
            }
        },
        gl = function(a, b, c) {
            255 == b && 255 == c || !b && !c ? (45 == ++a.ng && a.reset(), a.Gg.Ic.clear(), a.Hg.Ic.clear()) : (a.ng = 0, fl(a.Gg, b, c))
        };
    bl.prototype.decode = function() {
        this.yc.sort(function(a, b) {
            var c = a.time - b.time;
            return 0 == c ? a.order - b.order : c
        });
        for (var a = 0; a < this.yc.length; a++) {
            var b = this.yc[a];
            this.Hd = b.time;
            0 == b.type ? gl(this, b.Wh, b.Xh) : 1 == b.type && this.Ef & 496 && fl(this.Hg, b.Wh, b.Xh)
        }
        this.yc.length = 0
    };
    var dl = [128, 1, 2, 131, 4, 133, 134, 7, 8, 137, 138, 11, 140, 13, 14, 143, 16, 145, 146, 19, 148, 21, 22, 151, 152, 25, 26, 155, 28, 157, 158, 31, 32, 161, 162, 35, 164, 37, 38, 167, 168, 41, 42, 171, 44, 173, 174, 47, 176, 49, 50, 179, 52, 181, 182, 55, 56, 185, 186, 59, 188, 61, 62, 191, 64, 193, 194, 67, 196, 69, 70, 199, 200, 73, 74, 203, 76, 205, 206, 79, 208, 81, 82, 211, 84, 213, 214, 87, 88, 217, 218, 91, 220, 93, 94, 223, 224, 97, 98, 227, 100, 229, 230, 103, 104, 233, 234, 107, 236, 109, 110, 239, 112, 241, 242, 115, 244, 117, 118, 247, 248, 121, 122, 251, 124, 253, 254, 127, 0, 129, 130, 3, 132, 5, 6, 135, 136, 9, 10, 139,
            12, 141, 142, 15, 144, 17, 18, 147, 20, 149, 150, 23, 24, 153, 154, 27, 156, 29, 30, 159, 160, 33, 34, 163, 36, 165, 166, 39, 40, 169, 170, 43, 172, 45, 46, 175, 48, 177, 178, 51, 180, 53, 54, 183, 184, 57, 58, 187, 60, 189, 190, 63, 192, 65, 66, 195, 68, 197, 198, 71, 72, 201, 202, 75, 204, 77, 78, 207, 80, 209, 210, 83, 212, 85, 86, 215, 216, 89, 90, 219, 92, 221, 222, 95, 96, 225, 226, 99, 228, 101, 102, 231, 232, 105, 106, 235, 108, 237, 238, 111, 240, 113, 114, 243, 116, 245, 246, 119, 120, 249, 250, 123, 252, 125, 126, 255
        ],
        hl = function() {
            this.Pb = 0
        };
    hl.prototype.set = function(a) {
        this.Pb = a
    };
    hl.prototype.get = function() {
        return this.Pb
    };
    var il = function() {
        this.mg = this.Rh = this.g = 0
    };
    il.prototype.clear = function() {
        this.g = 0
    };
    il.prototype.update = function() {
        this.g = 2 == this.g ? 1 : 0
    };
    il.prototype.matches = function(a, b) {
        return 0 != this.g && a == this.Rh && b == this.mg
    };
    var jl = function() {
        this.timestamp = this.Rb = 0;
        this.de = !1
    };
    jl.prototype.reset = function() {
        this.timestamp = this.Rb = 0;
        this.de = !1
    };
    var kl = function(a) {
            this.Za = [];
            for (var b = 0; 15 >= b; b++) {
                this.Za[b] = [];
                for (var c = 0; 32 >= c; c++) this.Za[b][c] = new jl
            }
            this.P = this.ha = this.ia = 0;
            this.style = new hl;
            this.V = a;
            this.me = 0
        },
        ml = function(a) {
            for (var b = "", c = cl(a.V), d = c, e = 1; 15 >= e; ++e) {
                for (var f = "", g = !1, k = 1; 32 >= k; ++k) {
                    var l = a.Za[e][k];
                    if (0 != l.Rb) {
                        var n = String.fromCharCode(l.Rb);
                        " " != n && (g = !0);
                        f += n;
                        var q = l.timestamp;
                        q < c && (c = q);
                        q > d && (d = q);
                        3 == a.style.Pb ? (!l.de && " " == n && g && 32 > k && ll(a.V.ci, c, c, b ? b + "\n" + f : f), l.de = !0) : l.reset()
                    }
                }
                f && (b = b ? b + "\n" + f : f)
            }
            b && ll(a.V.ci,
                c, d, b)
        };
    kl.prototype.reset = function(a) {
        for (var b = 0; 15 >= b; b++)
            for (var c = 0; 32 >= c; c++) this.Za[b][c].reset();
        this.me = a;
        this.P = 0;
        this.ha = this.ia = 1
    };
    var nl = function(a) {
            return a.Za[a.ia][a.ha]
        },
        ql = function(a, b, c) {
            2 <= b && 1 < a.ha && (--a.ha, nl(a).Rb = 0);
            var d = nl(a);
            d.timestamp = cl(a.V);
            a: {
                switch (b) {
                    case 0:
                        b = ol[(c & 127) - 32];
                        break a;
                    case 1:
                        b = pl[c & 15];
                        break a
                }
                b = 0
            }
            d.Rb = b;
            32 > a.ha && a.ha++
        },
        rl = function(a, b, c, d) {
            for (var e = 0; e < d; e++)
                for (var f = 0; 32 >= f; f++) {
                    var g = a.Za[b + e][f],
                        k = a.Za[c + e][f];
                    g.Rb = k.Rb;
                    g.timestamp = k.timestamp;
                    g.de = k.de
                }
        },
        sl = function(a, b, c) {
            for (var d = 0; d < c; d++)
                for (var e = 0; 32 >= e; e++) a.Za[b + d][e].reset()
        },
        tl = function(a) {
            a.ia = 0 < a.P ? a.P : 1;
            a.ha = 1;
            sl(a, 0,
                15)
        },
        ol = [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 225, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 233, 93, 237, 243, 250, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 231, 247, 209, 241, 9632],
        pl = [174, 176, 189, 191, 8482, 162, 163, 9834, 224, 32, 232, 226, 234, 238, 244, 251],
        ul = function(a) {
            this.V = a;
            this.tg = 0;
            this.style = new hl;
            this.Zg = new kl(a);
            this.$g = new kl(a);
            this.Y = new kl(a);
            this.eb = this.Zg;
            this.pd = this.$g;
            this.L = this.eb
        };
    ul.prototype.reset = function(a, b) {
        this.tg = b;
        this.style.set(2);
        this.eb = this.Zg;
        this.pd = this.$g;
        this.L = this.eb;
        var c = (a << 2) + (b << 1) + 0;
        this.Zg.reset(c);
        this.$g.reset(c);
        this.Y.reset((a << 2) + (b << 1) + 1)
    };
    var vl = function(a, b) {
            var c = a.eb;
            switch (a.style.get()) {
                case 4:
                    if (0 < c.P) break;
                case 1:
                case 2:
                    ml(c), tl(a.eb), tl(a.pd), c.ia = 15, c.P = b
            }
            a.style.set(3);
            a.L = c;
            a.L.style = a.style;
            a.V.mode = 1 << c.me;
            c.ha = 1;
            c.P != b && (c.P > b ? (ml(c), sl(c, c.ia - c.P, b)) : c.ia < b && (b = c.P), c.P = b)
        },
        wl = function(a) {
            a.style.set(1);
            a.L = a.pd;
            a.L.P = 0;
            a.L.style = a.style;
            a.V.mode = 1 << a.L.me
        },
        xl = function(a) {
            a.style.set(4);
            a.L = a.Y;
            a.L.style = a.style;
            a.V.mode = 1 << a.L.me
        },
        al = function(a) {
            this.V = a;
            this.Ic = new il;
            this.Ig = 0;
            this.af = new ul(a);
            this.Th = new ul(a);
            this.yg = this.af
        };
    al.prototype.reset = function(a) {
        this.Ig = a;
        this.Ic.clear();
        this.yg = this.af;
        this.af.reset(a, 0);
        this.Th.reset(a, 1)
    };
    var fl = function(a, b, c) {
        a.Ic.update();
        b = el(b, c, a.Ic);
        switch (b.result) {
            case 0:
                return;
            case 1:
            case 2:
                return
        }
        var d = b.Ze;
        c = b.$e;
        if (32 <= d || !d) a.V.mode & a.V.Ef && (b = d, b & 128 && (b = 127), c & 128 && (c = 127), a = a.yg.L, b & 96 && ql(a, 0, b), c & 96 && ql(a, 0, c));
        else if (d & 16) a: if (!a.Ic.matches(d, c) && (b = a.Ic, b.Rh = d, b.mg = c, b.g = 2, b = d & 8 ? a.Th : a.af, a.yg = b, a.V.mode = 1 << (a.Ig << 2) + (b.tg << 1) + (4 == b.style.Pb ? 1 : 0), (a.V.mode | 1 << (a.Ig << 2) + (b.tg << 1) + (4 != b.style.Pb ? 1 : 0)) & a.V.Ef))
            if (c & 64) {
                a = [11, 11, 1, 2, 3, 4, 12, 13, 14, 15, 5, 6, 7, 8, 9, 10][(d & 7) << 1 | c >> 5 & 1];
                c = c & 16 ? 4 * ((c & 14) >> 1) : 0;
                d = b.L;
                switch (b.style.get()) {
                    case 4:
                        a = d.ia;
                        break;
                    case 3:
                        if (a != d.ia) {
                            if (a < d.P && (a = d.P, a == d.ia)) break;
                            var e = 1 + d.ia - d.P,
                                f = 1 + a - d.P;
                            rl(d, f, e, d.P);
                            b = e;
                            var g = d.P;
                            f < e ? (e = f + g - e, 0 < e && (b += e, g -= e)) : (e = e + g - f, 0 < e && (g -= e));
                            sl(d, b, g)
                        }
                }
                d.ia = a;
                d.ha = c + 1
            } else switch (d & 7) {
                case 1:
                    switch (c & 112) {
                        case 32:
                            ql(b.L, 0, 32);
                            break a;
                        case 48:
                            57 == c ? (b = b.L, nl(b).Rb = 0, 32 > b.ha && b.ha++) : ql(b.L, 1, c & 15)
                    }
                    break;
                case 2:
                    c & 32 && ql(b.L, 2, c & 31);
                    break;
                case 3:
                    c & 32 && ql(b.L, 3, c & 31);
                    break;
                case 4:
                case 5:
                    if (32 <= c && 47 >= c) switch (c) {
                        case 32:
                            wl(b);
                            break;
                        case 33:
                            b = b.L;
                            1 < b.ha && (--b.ha, nl(b).Rb = 0);
                            break;
                        case 36:
                            b = b.L;
                            a = nl(b);
                            for (c = 0; 15 >= c; c++)
                                for (d = 0; 32 >= d; d++)
                                    if (b.Za[c][d] == a) {
                                        for (; 32 >= d; d++) b.Za[c][d].reset();
                                        break
                                    }
                            break;
                        case 37:
                            vl(b, 2);
                            break;
                        case 38:
                            vl(b, 3);
                            break;
                        case 39:
                            vl(b, 4);
                            break;
                        case 40:
                            ql(b.L, 0, 32);
                            break;
                        case 41:
                            b.style.set(2);
                            b.L = b.eb;
                            b.L.P = 0;
                            b.L.style = b.style;
                            b.V.mode = 1 << b.L.me;
                            break;
                        case 42:
                            a = b.Y;
                            a.P = 15;
                            a.style.set(4);
                            tl(a);
                            xl(b);
                            break;
                        case 43:
                            xl(b);
                            break;
                        case 44:
                            a = b.eb;
                            switch (b.style.get()) {
                                case 1:
                                case 2:
                                case 3:
                                    ml(a)
                            }
                            sl(a, 0, 15);
                            break;
                        case 45:
                            b: {
                                a = b.L;
                                switch (b.style.get()) {
                                    default:
                                        case 2:
                                        case 1:
                                        break b;
                                    case 4:
                                            if (15 > a.ia) {
                                            ++a.ia;
                                            a.ha = 1;
                                            break b
                                        }
                                    case 3:
                                }
                                2 > a.P && (a.P = 2, a.ia < a.P && (a.ia = a.P));b = a.ia - a.P + 1;ml(a);rl(a, b, b + 1, a.P - 1);sl(a, a.ia, 1)
                            }
                            break;
                        case 46:
                            sl(b.pd, 0, 15);
                            break;
                        case 47:
                            ml(b.eb), a = b.pd, b.pd = b.eb, b.eb = a, wl(b)
                    }
                    break;
                case 7:
                    switch (c) {
                        case 33:
                        case 34:
                        case 35:
                            b = b.L, 32 < (b.ha += c & 3) && (b.ha = 32)
                    }
            }
    };
    var yl = function(a) {
        this.rg = a;
        this.Ib = 0;
        this.Wa = new Uint8Array(5120)
    };
    yl.prototype.clear = function() {
        this.Ib = 0
    };
    yl.prototype.append = function(a) {
        this.Wa.set(a, this.Ib);
        this.Ib += a.length
    };
    var zl = function(a, b) {
        for (var c = 0, d = 0, e = 0; d < a.Ib;) 2 == c && 3 == a.Wa[d] ? c = 0 : (0 == a.Wa[d] ? c++ : c = 0, a.Wa[e] = a.Wa[d], e++), d++;
        c = d - e;
        for (d = 0; d + c < a.Ib;) {
            for (var f = 0; 255 == a.Wa[d];) f += 255, d++;
            f += a.Wa[d++];
            if (45 < f) break;
            for (e = 0; 255 == a.Wa[d];) e += 255, d++;
            e += a.Wa[d++];
            if (4 == f) {
                f = a.rg.V;
                var g = b,
                    k = new Zf(a.Wa.subarray(d, d + e));
                if (181 == $f(k) && 49 == ag(k) && 1195456820 == L(k) && 3 == $f(k)) {
                    var l = $f(k);
                    if (0 != (l & 64)) {
                        l &= 31;
                        M(k, 1);
                        for (var n = 0; n < l; n++) {
                            var q = $f(k),
                                r = (q & 4) >> 2,
                                v = $f(k),
                                H = $f(k);
                            r && f.yc.push({
                                time: g,
                                type: q & 3,
                                Wh: v,
                                Xh: H,
                                order: f.yc.length
                            })
                        }
                    }
                }
            }
            d +=
                e
        }
        a.Ib = 0
    };
    var Al = function(a) {
        this.Jb = new yl(a);
        this.$a = null;
        this.Ag = 0;
        this.Pe = 9E4;
        this.Dd = 0;
        this.Tc = [];
        this.Cd = []
    };
    h = Al.prototype;
    h.da = function(a) {
        if (a) {
            var b = Q(a, 1835296868, void 0, !0);
            b && (this.Pe = b.Pe);
            if (b = Q(a, 1953658222, void 0, !0))
                if (this.Tc = b.Tc, 0 != this.Tc.length && (this.Cd = b.Cd, this.Dd = b.Dd, a = Q(a, 1835295092))) this.$a = new Zf(O(a))
        } else this.Ag = 0, this.$a = null
    };
    h.wj = function(a) {
        this.Ag = a
    };
    h.pa = function() {
        return null === this.$a
    };
    h.uf = function() {
        var a;
        (a = null === this.$a) || (a = this.$a, a = !(a.offset < a.buffer.length));
        return a
    };
    h.parse = function() {
        if (this.$a)
            for (var a = this.Ag / this.Dd, b = 0, c = 0, d = this.Tc[0]; !this.uf();) {
                var e = L(this.$a);
                6 == ($f(this.$a) & 31) ? (this.Jb.append(cg(this.$a, e - 1)), zl(this.Jb, 0 == this.Cd.length ? b : b + this.Cd[c] / this.Pe)) : M(this.$a, e - 1);
                d -= e + 4;
                0 == d && (b += a, c++, d = this.Tc[c])
            }
    };
    var Bl = function(a) {
        Si.call(this);
        this.Jb = new yl(a)
    };
    m(Bl, Si);
    Bl.prototype.da = function(a) {
        Si.prototype.da.call(this, a);
        this.Jb.clear()
    };
    Bl.prototype.wj = function() {};
    Bl.prototype.hj = function(a, b) {
        for (var c = 0;;) {
            a: {
                var d = a;
                var e = b;
                for (e -= 3; d <= e;) {
                    if (0 == this.o[d] && 0 == this.o[d + 1]) {
                        if (0 == this.o[d + 2] && 1 == this.o[d + 3]) {
                            d += 4;
                            break a
                        }
                        if (1 == this.o[d + 2]) {
                            d += 3;
                            break a
                        }
                    }
                    d++
                }
                d = -1
            }
            e = 0 <= d;
            if (0 < this.Jb.Ib || 6 == c) {
                c = e ? d - 1 : b;
                var f = this.Jb;
                f.Ib + (c - a + 1) <= f.Wa.length || zl(this.Jb, this.Rc);
                this.Jb.append(this.o.subarray(a, c + 1))
            }
            if (!e) break;zl(this.Jb, this.Rc);a = d;c = this.o[a++] & 31
        }
    };
    var Cl = function(a, b) {
        $g.call(this, a, b, "webvtt");
        this.na = B("cast.player.cea608.InbandCaptionsManager");
        this.V = new bl(this);
        this.V.Ef = 1;
        this.Fa = null;
        this.ea = 0;
        this.ld = null;
        this.fd = [];
        this.td = this.ih.bind(this);
        this.Ca = null;
        this.vd = new ge
    };
    m(Cl, $g);
    Cl.prototype.G = function() {
        this.reset();
        $g.prototype.G.call(this)
    };
    Cl.prototype.ih = function() {
        if (this.Fa.pa()) {
            var a = this.vd.jd();
            this.ea != a.O && (this.V.clear(), this.ld = null);
            this.ea = a.O;
            this.V.ea = a.$j;
            this.Fa.da(a.data);
            this.Fa.wj(a.duration)
        }
        this.Fa.parse();
        if (this.Fa.uf()) {
            this.V.decode();
            if (0 < this.fd.length) {
                for (bh(this); 0 < this.fd.length;) a = this.fd.pop(), a = new VTTCue(a.start, a.end, a.text), a.position = 20, a.align = "start", this.addCue({
                    Wf: a
                });
                this.fd.length = 0
            }
            this.Fa.da(null)
        }
        this.vd.pa() && this.Fa.pa() ? this.Ca = null : this.Ca = setTimeout(this.td, 20)
    };
    var ll = function(a, b, c, d) {
        null !== a.ld && b < a.ld && (b = a.ld);
        .1 > c - b && (c = b + .1);
        a.ld = c;
        a.fd.push({
            start: b,
            end: c,
            text: d
        })
    };
    Cl.prototype.ze = function(a, b, c, d, e) {
        if (Gd(a)) {
            if (!this.Fa) switch (a) {
                case "video/mp2t":
                    this.Fa = new Bl(this);
                    break;
                case "video/mp4":
                    this.Fa = new Al(this);
                    break;
                default:
                    return
            }
            1 < this.vd.tb() ? E(this.na, "Dropped segment") : (this.vd.enqueue({
                data: b,
                duration: e,
                O: c,
                $j: "video/mp4" == a ? d : c
            }), F(this.na, "Pending " + this.vd.tb()), null === this.Ca && (this.Ca = setTimeout(this.td, 20)))
        }
    };
    Cl.prototype.reset = function() {
        $g.prototype.reset.call(this);
        this.ea = 0;
        this.ld = null;
        this.fd.length = 0;
        this.Fa && this.Fa.da(null);
        this.V.clear();
        this.vd.clear();
        null !== this.Ca && (clearTimeout(this.Ca), this.Ca = null)
    };
    var Dl = function(a, b, c, d) {
        C.call(this);
        this.a = a;
        this.w = b;
        this.yk = d;
        this.hb = c;
        this.m = this.Ei = null;
        this.xb = "none";
        this.$f = this.Zf = this.gc = null;
        this.cg = !1;
        this.f = new G;
        td(this.f);
        D(this.f, "success", this.Nc, !1, this);
        D(this.f, "error", this.Db, !1, this)
    };
    m(Dl, C);
    h = Dl.prototype;
    h.G = function() {
        this.m && (this.gc && wc(this.gc, "message", this.Xi, !1, this), this.m = null);
        this.gc && (this.gc.close(), this.gc = null);
        this.f.M();
        C.prototype.G.call(this)
    };
    h.createSession = function(a) {
        F(El, "create session");
        this.$f = y();
        var b = this.a.licenseCustomData;
        if (b) {
            var c = new tg(this.hb.length + b.length + 34);
            N(c, this.hb);
            (new qj(b)).h(c);
            b = sg(c)
        } else b = this.hb;
        this.m = this.a.mediaElement;
        this.xb = a;
        this.gc = a = this.m.mediaKeys.createSession();
        D(this.gc, "message", this.Xi, !1, this);
        a.generateRequest("cenc", b.buffer).catch(this.Vi.bind(this))
    };
    h.Nc = function(a) {
        F(El, "xhr success");
        var b = this.Zf;
        null !== b && (b = y() - b, A("Cast.MPL.LicenseReq.ServerLatency", b));
        this.Zf = null;
        this.oi(Ed(a.target))
    };
    h.oi = function(a) {
        var b = this;
        a ? (a = new Uint8Array(a), this.a.processLicense && (a = this.a.processLicense(a)), this.cg = !0, this.gc.update(a.buffer).then(function() {
            b.cg = !1;
            var a = b.yk;
            F(Fl, "key session ended: " + a.Vc);
            8 < a.Ba.length ? a.Ba.shift().M() : a.Vc++;
            a.Vc < a.Ba.length && a.Ba[a.Vc].createSession(a.xb)
        }, function(a) {
            b.cg = !1;
            b.Vi(a)
        })) : this.a.R(201, void 0)
    };
    h.Db = function() {
        F(El, "xhr error");
        var a = new mf(String(this.f.ke), this.f.zb, Cd(this.f), this.f.getAllResponseHeaders(), Ed(this.f));
        this.a.R(201, a)
    };
    h.Xi = function(a) {
        F(El, "message");
        this.Ei = new Uint8Array(a.Bc.message);
        this.a.prepareLicenseRequest && !this.a.prepareLicenseRequest() || this.qc()
    };
    h.Vi = function(a) {
        F(El, "keyerror " + a);
        this.a.R(202)
    };
    h.qc = function() {
        var a = new lf;
        a.timeoutInterval = 18E4;
        a.protectionSystem = this.xb;
        a.content = this.Ei;
        a.setResponse = this.oi.bind(this);
        this.w.updateLicenseRequestInfo(a);
        this.a.licenseUrl && (a.url = this.a.licenseUrl);
        if (!a.url && this.hb && "playready" == this.xb) {
            var b = new Jg(this.hb);
            if (b = gi(b.getData())) a.url = b.url
        }
        if (this.a.updateLicenseRequestInfo && (this.a.updateLicenseRequestInfo(a), a.skipRequest)) return;
        this.f.Kd = a.withCredentials;
        this.f.Ob = Math.max(0, a.timeoutInterval);
        this.f.send(a.url, "POST", a.content,
            a.headers);
        a = this.$f;
        null !== a && (a = y() - a, A("Cast.MPL.LicenseReq.GenLatency", a));
        this.$f = null;
        this.Zf = y()
    };
    h.be = function() {
        return null !== this.$f || null !== this.Zf || this.cg
    };
    var El = B("cast.player.core.MediaKeySession");
    var Hl = function(a, b, c, d, e) {
        C.call(this);
        this.a = a;
        this.w = b;
        this.Ba = [];
        this.Vc = 0;
        this.xb = null;
        D(this.a.mediaElement, "encrypted", this.Ti, !1, this);
        c && 0 < c.length && (a.protectionSystem && (c.includes(a.protectionSystem) ? c.splice(0, 0, c.splice(c.indexOf(a.protectionSystem), 1)[0]) : E(Fl, "Preferred protection system not found")), Gl(this, {
            Xg: c,
            Cf: 0,
            Oj: d,
            pl: e
        }))
    };
    m(Hl, C);
    Hl.prototype.G = function() {
        wc(this.a.mediaElement, "encrypted", this.Ti, !1, this);
        for (var a = p(this.Ba), b = a.next(); !b.done; b = a.next()) b.value.M();
        this.Ba.length = 0;
        C.prototype.G.call(this)
    };
    var Gl = function(a, b) {
            Il(a, b).catch(function(c) {
                b.Cf++;
                b.Cf < b.Xg.length ? Gl(a, b) : (Hc(Fl, c.toString()), E(Fl, "unsupported protection system"), a.a.R(202))
            })
        },
        Il = function(a, b) {
            var c = b.Xg[b.Cf];
            b = Jl(b);
            return navigator.requestMediaKeySystemAccess(tf[c], [b]).then(function(a) {
                return a.createMediaKeys()
            }).then(function(b) {
                return a.a.mediaElement.setMediaKeys(b)
            }).then(function() {
                a.xb = c;
                Qd(a.xb, a.w.lf());
                0 < a.Ba.length && a.Ba[0].createSession(c);
                return Promise.resolve()
            })
        };
    Hl.prototype.Ti = function(a) {
        a = a.Bc;
        F(Fl, "onencrypted: " + this.Vc);
        (a = a.initData) ? (a = new Uint8Array(a), Kl(this, a) || (a = new Dl(this.a, this.w, a, this), this.xb && this.Vc == this.Ba.length && a.createSession(this.xb), this.Ba.push(a))) : F(Fl, "invalid init data")
    };
    var Kl = function(a, b) {
        return a.Ba.some(function(a) {
            a: if (b.length != a.hb.length) a = !1;
                else {
                    for (var c = b.length, e = 0; e < c; e++)
                        if (b[e] != a.hb[e]) {
                            a = !1;
                            break a
                        }
                    a = !0
                }return a
        })
    };
    Hl.prototype.qc = function() {
        this.Ba[this.Vc].qc()
    };
    Hl.prototype.be = function() {
        return this.Ba.some(function(a) {
            return a.be()
        })
    };
    var Jl = function(a) {
            var b = a.Xg[a.Cf],
                c = {
                    initDataTypes: ["cenc"]
                },
                d = a.Oj || "";
            a = a.pl || "";
            a = a.replace("video/mp2t", "video/mp4");
            d && (c.audioCapabilities = [{
                contentType: d,
                robustness: Ll[b] || ""
            }]);
            a && (c.videoCapabilities = [{
                contentType: a,
                robustness: Ml[b] || ""
            }]);
            return c
        },
        Nl = {},
        Ll = (Nl.widevine = "HW_SECURE_CRYPTO", Nl),
        Ol = {},
        Ml = (Ol.widevine = "HW_SECURE_ALL", Ol),
        Fl = B("cast.player.core.MediaKeysManager");
    var Pl = function(a, b, c) {
        C.call(this);
        this.a = a;
        this.w = b;
        this.Aa = c;
        this.ua = [];
        this.Bb = null;
        this.ba = new MediaSource;
        this.xi = this.Ai = this.vf = !1;
        this.Yg = this.Ve = this.Nd = null;
        D(this.ba, "sourceopen", this.ej, !1, this)
    };
    m(Pl, C);
    Pl.prototype.G = function() {
        this.w.Qb();
        Ql(this);
        this.Ve = this.Nd = null;
        this.Bb && (this.Bb.M(), this.Bb = null);
        wc(this.ba, "sourceopen", this.ej, !1, this);
        C.prototype.G.call(this)
    };
    Pl.prototype.ej = function() {
        F(Rl, "sourceopen");
        if (this.xi) {
            this.update();
            Sl(this);
            for (var a = p(this.ua), b = a.next(); !b.done; b = a.next())(b = b.value) && b.createBuffer()
        }
    };
    var Tl = function(a) {
            !a.Bb && a.Yg && a.Ai && (a.Nd || a.Ve) && (a.Bb = new Hl(a.a, a.w, a.Yg, a.Nd, a.Ve))
        },
        Ul = function(a) {
            if (u(a.ba.setLiveSeekableRange) && "open" == a.ba.readyState && Infinity == a.ba.duration) {
                var b = a.wa();
                b && a.ba.setLiveSeekableRange(b.start, b.end)
            }
        },
        Vl = function(a) {
            return a.split(",").find(function(a) {
                return !Hf(a)
            })
        },
        Wl = function(a) {
            return a.split(",").find(function(a) {
                return Hf(a)
            })
        };
    h = Pl.prototype;
    h.wa = function() {
        for (var a = -Infinity, b = Infinity, c = this.w.getStreamCount(), d = 0; d < c; d++)
            if (this.w.isStreamEnabled(d)) {
                var e = this.w.wa(d);
                if (!e) return null;
                e.start > a && (a = e.start);
                e.end < b && (b = e.end)
            }
        a > b && (a = b);
        return {
            start: a,
            end: b
        }
    };
    h.onManifestReady = function() {
        this.xi = !0;
        this.update();
        Ul(this)
    };
    h.fh = function() {
        Ul(this)
    };
    h.ne = function(a) {
        this.Yg = a;
        Tl(this)
    };
    h.endOfStream = function() {
        "open" == this.ba.readyState && this.ba.endOfStream()
    };
    h.load = function() {
        Ql(this);
        this.open()
    };
    h.open = function() {
        this.vf || (this.w.load(this), this.vf = !0);
        this.a.mediaElement.src ? (pc(this.a.mediaElement, "emptied", function() {
            Xl(this)
        }, !1, this), this.a.mediaElement.src = "") : Xl(this)
    };
    var Xl = function(a) {
            F(Rl, "open");
            !a.Bb && a.a.mediaElement.setMediaKeys ? a.a.mediaElement.setMediaKeys(null).catch(function(a) {
                Hc(Rl, a.toString())
            }).then(function() {
                Yl(a)
            }) : Yl(a)
        },
        Yl = function(a) {
            a.a.mediaElement.src = window.URL.createObjectURL(a.ba);
            a.Ai = !0;
            Tl(a)
        };
    Pl.prototype.ph = function() {
        this.vf ? E(Rl, "protocol already loaded") : (this.w.load(this), this.vf = !0)
    };
    var Ql = function(a) {
            for (var b = p(a.ua), c = b.next(); !c.done; c = b.next())(c = c.value) && c.M();
            a.ua.length = 0
        },
        Sl = function(a) {
            if ("open" == a.ba.readyState) {
                var b = a.w.getDuration();
                0 < b && !a.ba.duration && (a.ba.duration = parseFloat((b - 1E-4).toFixed(4)))
            }
        };
    h = Pl.prototype;
    h.reset = function() {
        for (var a = p(this.ua), b = a.next(); !b.done; b = a.next())(b = b.value) && b.reset()
    };
    h.update = function() {
        for (var a = this.w.getStreamCount(), b = null, c = null, d = 0; d < a; d++)
            if (this.w.isStreamEnabled(d)) {
                var e = this.w.getStreamInfo(d),
                    f = e.mimeType;
                e = e.codecs;
                Fd(f) ? b ? E(Rl, "more than one audio stream enabled") : (b = Wl(e), b = f + ';codecs="' + b + '"') : Gd(f) && (c ? E(Rl, "more than one video stream enabled") : (c = Vl(e), c = f + ';codecs="' + c + '"'));
                this.ua[d] || (this.ua[d] = new Hh(this.Aa, this.a, this.w, d, this.ba), this.w.enableStream(d, !0), Kh(this.ua[d]))
            } else this.ua[d] && (this.ua[d].M(), this.ua[d] = null);
        b || c ? this.Nd ||
            this.Ve || (this.Nd = b, this.Ve = c, Tl(this)) : E(Rl, "no enabled audio or video stream")
    };
    h.Vg = function(a) {
        return this.ua[a].Vg()
    };
    h.Ie = function(a) {
        this.ua[a].Ie()
    };
    h.ce = function(a) {
        return this.ua[a].ce()
    };
    h.va = function(a, b) {
        return this.ua[a].va(b)
    };
    h.qc = function() {
        this.Bb.qc()
    };
    h.be = function() {
        return null !== this.Bb && this.Bb.be()
    };
    h.getStreamCount = function() {
        return this.ua.length
    };
    var Rl = B("cast.player.core.MediaSourceManager");
    var Zl = function(a, b, c, d) {
        $g.call(this, a, b, c);
        a = new lf;
        a.url = d;
        b.updateCaptionsRequestInfo && b.updateCaptionsRequestInfo(a);
        this.Ej = new bi(this);
        this.Ej.load(a)
    };
    m(Zl, $g);
    Zl.prototype.G = function() {
        this.Ej.M();
        $g.prototype.G.call(this)
    };
    Zl.prototype.reset = function() {};
    Zl.prototype.kc = function(a) {
        a && this.parse(a, 0)
    };
    var Z = function(a) {
        F($l, "Version: 1.0.0.43");
        za() && cast.__platform__.metrics.setMplVersion("1.0.0.43");
        this.a = a;
        this.w = null;
        this.ub = 0;
        this.Nb = null;
        this.Qh = this.Ka = this.Ug = this.Sa = !1;
        this.Af = !0;
        this.zf = !1;
        this.Wb = this.W = this.m = null;
        this.$d = !1;
        this.Ne = null;
        this.td = this.ih.bind(this);
        this.Yf = this.Xf = this.Me = null;
        this.Uf = this.wf = !1
    };
    z("cast.player.api.Player", Z);
    var bm = function(a, b) {
        am(a);
        a.Ne = setTimeout(a.td, b || 0)
    };
    h = Z.prototype;
    h.jc = function(a) {
        bm(this, a)
    };
    h.ze = function(a, b, c, d, e) {
        this.Ug && this.Wb.ze(a, b, c, d, e);
        bm(this)
    };
    h.gh = function(a, b) {
        this.Sa && (Infinity == this.ub || this.ub < b) && (this.ub = b)
    };
    h.dh = function() {
        Sl(this.W)
    };
    h.ac = function() {
        return this.Sa ? this.ub : this.m.currentTime
    };
    var am = function(a) {
        null !== a.Ne && (clearTimeout(a.Ne), a.Ne = null)
    };
    h = Z.prototype;
    h.dj = function() {
        F($l, "seeking");
        this.$d ? this.$d = !1 : (this.zf = this.Sa = !1, this.Me = null, this.Wb && this.Wb.reset(), this.W.reset())
    };
    h.cj = function() {
        F($l, "seeked");
        this.Nb = y()
    };
    h.rd = function() {
        Hc($l, "error");
        var a = 100;
        if (this.m.error) switch (this.m.error.code) {
            case MediaError.MEDIA_ERR_ABORTED:
                a = 101;
                break;
            case MediaError.MEDIA_ERR_NETWORK:
                a = 103;
                break;
            case MediaError.MEDIA_ERR_DECODE:
                a = 102;
                break;
            case MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED:
                a = 104
        }
        this.a.R(a)
    };
    h.Zi = function() {
        this.Me = y();
        Ba("Cast.MPL.Playing");
        if (this.Xf) {
            var a = y() - this.Xf;
            A("Cast.MPL.AutoPauseTime", a);
            this.Xf = null
        }
    };
    h.Yi = function() {
        var a = y();
        this.Ka && !this.Sa && null !== this.Me && (this.Xf = a, A("Cast.MPL.PlayTimeBeforeAutoPause", a - this.Me));
        Ba("Cast.MPL.Pause");
        this.Nb = this.Me = null
    };
    h.Ui = function() {
        Ba("Cast.MPL.Ended");
        this.Nb = null
    };
    h.bg = function() {
        if (this.Sa && this.m && !isNaN(this.m.duration) && 0 != this.m.buffered.length && (this.Sa = !1, this.Nb = y(), A("Cast.MPL.MediaDuration", this.m.duration), 0 != this.ub && this.ub != this.m.currentTime)) {
            var a = this.ub;
            this.$d = !0;
            this.m.currentTime = a
        }
        this.Af = !1;
        var b = 0,
            c = 0,
            d = !0;
        a = this.ac();
        for (var e = this.W.getStreamCount(), f = 0; f < e; f++)
            if (this.w.isStreamEnabled(f) && (c++, this.W.ce(f))) {
                b++;
                var g = this.W.va(f, a);
                if (this.Ka && d) {
                    var k = this.w.getSegmentInterval(f).duration * this.a.autoResumeNumberOfSegments;
                    if (0 ==
                        k || k > this.a.autoResumeDuration) k = this.a.autoResumeDuration;
                    g < k && (d = !1)
                }
                k = f;
                20 <= g || (g < this.a.autoPauseDuration && (g = this.w.getStreamInfo(k), vf(g.role, g.mimeType, g.codecs) || (this.Af = !0)), this.W.Vg(k) && this.W.Ie(k))
            }
        if (this.m && 0 != c) {
            this.Af ? !this.m.paused && this.m.duration - a > this.a.autoPauseDuration && (F($l, "auto pause " + a), this.Ka = !0, this.m.pause(), this.a.onAutoPause && this.a.onAutoPause(this.Ka)) : !this.Sa && this.Ka && d && (this.Ka = !1, this.m.paused && (F($l, "auto resume " + a), this.m.play(), this.a.onAutoPause &&
                this.a.onAutoPause(this.Ka)));
            if (0 == b) {
                if (!this.zf && (F($l, "endOfStream " + a), this.W.endOfStream(), this.a.onMediaDownloadEnded)) this.a.onMediaDownloadEnded();
                this.zf = !0
            }
            if (!(this.Sa || this.Ka || this.m.paused || !this.W || this.W.be()) && (F($l, "time=" + a), this.Nb && (b = y(), c = 2500 < b - this.Nb, this.a.Nh || c))) {
                a: {
                    d = this.m.currentTime;e = this.m.buffered;
                    for (f = e.length - 1; 0 <= f; f--) {
                        g = e.start(f);
                        if (d >= g) break;
                        if (0 == f || d > e.end(f - 1) - .15) {
                            E($l, "stall jump to " + g);
                            Ba("Cast.MPL.PlaybackStallGap");
                            this.$d = this.Uf = !0;
                            this.m.currentTime =
                                g;
                            d = !0;
                            break a
                        }
                    }
                    d = !1
                }
                d ? this.Nb = null : c && (this.Nb = this.a.Nh ? b : null, E($l, "playback stalled in buffered region"), Ba("Cast.MPL.PlaybackStall"), this.$d = this.Uf = !0, this.m.currentTime = a + .5)
            }
        }
        bm(this, 400)
    };
    h.fj = function() {
        this.Nb = y();
        this.Uf && (A("Cast.MPL.StallPrevented", 1), this.Uf = !1);
        if (this.Yf) {
            var a = y() - this.Yf;
            A(this.wf ? "Cast.MPL.PreloadAutoplayStartupLatency" : "Cast.MPL.AutoplayStartupLatency", a);
            this.Yf = null
        }
    };
    h.ih = function() {
        this.Ne = null;
        this.bg()
    };
    var cm = function(a, b, c) {
        a.w = b;
        a.Sa = !0;
        a.ub = c || 0;
        a.W = new Pl(a.a, a.w, a)
    };
    Z.prototype.load = function(a, b) {
        F($l, "load");
        Ba("Cast.MPL.Load");
        var c = gh();
        u(c) && jh("Cast.MPL.ExperimentationBlockId", c.blockIds);
        this.m = this.a.mediaElement;
        D(this.m, "error", this.rd, !1, this);
        D(this.m, "seeking", this.dj, !1, this);
        D(this.m, "seeked", this.cj, !1, this);
        D(this.m, "pause", this.Yi, !1, this);
        D(this.m, "playing", this.Zi, !1, this);
        D(this.m, "timeupdate", this.fj, !1, this);
        D(this.m, "ended", this.Ui, !1, this);
        this.m.autoplay && (this.Yf = y(), this.m.autoplay = !1, this.Qh = !0, this.jj());
        this.W ? this.W.open() : a ? (this.wf = !1, cm(this, a, b), this.W.load()) : Hc($l, "no protocol")
    };
    Z.prototype.load = Z.prototype.load;
    Z.prototype.ph = function(a, b) {
        F($l, "preload");
        Ba("Cast.MPL.Preload");
        cm(this, a, b);
        this.W.ph();
        this.wf = !0
    };
    Z.prototype.preload = Z.prototype.ph;
    Z.prototype.Qb = function() {
        F($l, "unload");
        dm(this);
        this.m && (this.Qh && (this.m.autoplay = !0), this.W.M(), this.W = null, am(this), this.zf = this.Ka = !1, wc(this.m, "error", this.rd, !1, this), wc(this.m, "seeking", this.dj, !1, this), wc(this.m, "seeked", this.cj, !1, this), wc(this.m, "pause", this.Yi, !1, this), wc(this.m, "playing", this.Zi, !1, this), wc(this.m, "timeupdate", this.fj, !1, this), wc(this.m, "ended", this.Ui, !1, this), this.m = null, this.wf = !1)
    };
    Z.prototype.unload = Z.prototype.Qb;
    Z.prototype.reload = function() {
        this.Sa || (this.ub = this.m.currentTime);
        this.Sa = !0;
        this.m.paused || (this.Ka = !0);
        am(this);
        this.W.load()
    };
    Z.prototype.reload = Z.prototype.reload;
    Z.prototype.jj = function() {
        this.Ka = !0
    };
    Z.prototype.playWhenHaveEnoughData = Z.prototype.jj;
    Z.prototype.getState = function(a) {
        var b = this.W.wa();
        u(a) || (a = 1);
        var c = {};
        a & 1 && (c.underflow = this.Af || this.Ka);
        a & 2 && (c.seekable = b ? {
            start: b.start,
            end: b.end
        } : null);
        return c
    };
    Z.prototype.getState = Z.prototype.getState;
    Z.prototype.va = function(a) {
        return this.W.va(a, this.ac())
    };
    Z.prototype.getBufferDuration = Z.prototype.va;
    Z.prototype.mk = function() {
        return 20
    };
    Z.prototype.getMaxBufferDuration = Z.prototype.mk;
    Z.prototype.qc = function() {
        this.W.qc()
    };
    Z.prototype.startLicenseRequest = Z.prototype.qc;
    var dm = function(a) {
        a.Wb && (a.Ug = !1, a.Wb.M(), a.Wb = null)
    };
    Z.prototype.ik = function(a, b, c) {
        b ? a ? "cea608" == b ? (this.Ug = !0, this.Wb = new Cl(this, this.a)) : c && (this.Wb = new Zl(this, this.a, b, c)) : dm(this) : this.W.update()
    };
    Z.prototype.enableCaptions = Z.prototype.ik;
    Z.prototype.nk = function() {
        return this.w
    };
    Z.prototype.getStreamingProtocol = Z.prototype.nk;
    Z.prototype.lk = function() {
        return this.a
    };
    Z.prototype.getHost = Z.prototype.lk;
    var $l = B("cast.player.api.Player");
    Z.State = {
        UNDERFLOW: 1,
        SEEKABLE: 2
    };
    z("cast.player.api.CreateDashStreamingProtocol", function(a) {
        Pd(1);
        return new Fi(a)
    });
    z("cast.player.api.CreateHlsStreamingProtocol", function(a, b) {
        Pd(2);
        return new Y(a, b)
    });
    z("cast.player.api.CreateSmoothStreamingProtocol", function(a) {
        Pd(3);
        return new Lk(a)
    });
}).call(window);