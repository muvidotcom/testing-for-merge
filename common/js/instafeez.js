var imported = document.createElement('script');
imported.src = 'https://api.instafeez.com/public/erpgs.js';
document.head.appendChild(imported);
$(document).ready(function(){
    var _html = '<div style="display: none;" class="loader" id="payment-wait-loading"></div>';
    $("body").append(_html);
});

function instafeezIntegration (name,email,contact,access_token) {
    var options = { 
            "key":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc",
            "amount":1*100,
            "image":"https://api.instafeez.com/public/if-white-logo-icon.png",
            "name": "Test transaction",
            "merchant_details": {
                "access_token" : access_token
            },
            "autofill": {
                "name": name,
                "email": email,
                "contact": contact
            },
            "handler" : function (result) {
                $('#integrate-payment-gateway').html(JSLANGUAGE.wait);
                $('#integrate-payment-gateway').attr('disabled', 'disabled');
                var url = "payment";
                document.payment_gateway_form.action = url;
                document.payment_gateway_form.submit();
            }
    };
    var dop = new DynamoPay(options);
    dop.open();
}

function doPaymentStepTwo(movie_id,user_data,permalink,amount) {
        
        if(parseFloat(amount) > 0.001){
            $.post("user/paymentUserDeatils", {movie_uniq_id: movie_id,gateway_code:'instafeez'}, function(res){
                var result = $.parseJSON(res);
                $('#loader-ppv').hide();
                $('#ppvModal').modal('hide');
                    var options = { 
                        "key":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc",
                        "amount":amount*100,
                        "image":result.studio_logo,
                        "name": result.studio_name,
                        "merchant_details": {
                            "access_token" : result.api_user
                        },
                        "callbackurl" : "YOUR CALLBACK URL ",
                        "autofill": {
                            "name": result.user_name,
                            "email": result.email,
                            "contact": result.contact
                        },
                        "handler" : function (data) {
                            var key_id = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc";
                            var payment_id = data.payment_id;
                            var access_token = result.api_user;
                            $.post("https://api.instafeez.com/erp/payments/id/", {key_id: key_id,payment_id:payment_id,access_token:access_token}, function(res){
                                $('#payment-wait-loading').show();
                                $.post("user/instafeezPpvTransaction", {user_data:user_data,transaction_data:res}, function(resData){
                                    //var purl = "player/" + permalink;
                                    window.location.href = resData;
                                });
                            });
                        },
                        "notes" : {
                            "student_id":result.user_id,
                            "student_name":result.studio_name,
                            "school_id":result.studio_id
                        }
                    };
                    var dop = new DynamoPay(options);
                    dop.open();

            });
        }else{
                var purl = "player/" + permalink;
                window.location.href = purl;
            }
    };

function instafeez () {
    this.processCard = function(res) {
        $('#payment-wait-loading').show();
        try {
            var data = $.parseJSON(res);
            doPayment(data.movie_id,data,data.permalink,data.price_single);
        } catch(e) {
            $('#payment-wait-loading').hide();
            $("#ppvModal").html(res).modal('show');
        }
    };
    
    this.doPayment = function(movie_id,user_data,permalink,amount) {
        
        if($.trim(movie_id) && $.trim(permalink) && user_data && $.trim(amount)) {
            if(parseFloat(amount) > 0.001){
                $.post("user/paymentUserDeatils", {movie_uniq_id: movie_id,gateway_code:'instafeez'}, function(res){
                    $('#ppvModal').modal('hide');
                    var result = $.parseJSON(res);
                    $('#loader-ppv').hide();
                    $('#payment-wait-loading').hide();
                        var options = { 
                            "key":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc",
                            "amount":amount*100,
                            "image":result.studio_logo,
                            "name": result.studio_name,
                            "merchant_details": {
                                "access_token" : result.api_user
                            },
                            "callbackurl" : "YOUR CALLBACK URL ",
                            "autofill": {
                                "name": result.user_name,
                                "email": result.email,
                                "contact": result.contact
                            },
                            "handler" : function (data) {
                                var key_id = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc";
                                var payment_id = data.payment_id;
                                var access_token = result.api_user;
                                $.post("https://api.instafeez.com/erp/payments/id/", {key_id: key_id,payment_id:payment_id,access_token:access_token}, function(res){
                                    $('#payment-wait-loading').show();
                                    $.post("user/instafeezPpvTransaction", {user_data:user_data,transaction_data:res}, function(resData){
                                        //var purl = "player/" + permalink;
                                        
                                        window.location.href = resData;
                                    });
                                });
                            },
                            "notes" : {
                                "student_id":result.user_id,
                                "student_name":result.studio_name,
                                "school_id":result.studio_id
                            }
                        };
                        var dop = new DynamoPay(options);
                        dop.open();

                });
            }else{
                var purl = "player/" + permalink;
                window.location.href = purl;
            }
        }else{
            var url = 'user/ppvpayment';
            var user_data = $('#membership_form').serialize();
            $.post(url, {data:user_data}, function(resData){
                var data = $.parseJSON(resData);
                doPaymentStepTwo(data.movie_id,data,data.permalink,data.amount);
                
            });
        }
    };
    
    
}