function addtocart(id, flag){
    $('.loader_cart').show();
    var url = HTTP_ROOT+'/shop/addtocart';
    $.post(url, {'quantity':1, 'id':id}, function(res){
        if (flag == 2){
            window.location.href = HTTP_ROOT+'/shop/addtocart';
        } else{
            $('.loader_cart').hide();
            $('.round-cart').html(eval($('.round-cart').html()) + 1);
            $('#cartpopup').html(res);
        }
    })
}