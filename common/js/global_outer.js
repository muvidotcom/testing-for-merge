$(document).ready(function() {

    $('#loginModal').on('hidden', function() {
        $('#username').val("");
        $("#password").val('');
        $("#join_password").val('');
        $("#email").val('');
    });

    $('#login-loading').hide();
    $("#login-errors").on('click', '#session_reset', function() {
        var user_id = $("#session_reset_user_id").val();
        var studio_id = $("#session_reset_studio_id").val();
        $.ajax({
            url: HTTP_ROOT + "/user/logoutAll",
            data: {user_id: user_id, studio_id: studio_id},
            type: 'POST',
            beforeSend: function() {
                $("#reset_session_loader").show();
            },
            success: function(data) {
                $("#reset_session_loader").hide();
                $("#login-errors #logout_all").modal('hide');
                $("#login-errors").html(JSLANGUAGE.logged_out_from_all_devices);
            }, complete: function() {
                window.location.href = window.location.href;
            }
        });
    });

    $('#login-btn').click(function() {
        $("#login_form").validate({
            rules: {
                "LoginForm[email]": {
                    required: true,
                    email: true
                },
                "LoginForm[password]": {
                    required: true,
                }
            },
            messages: {
                "LoginForm[email]": {
                    required: JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "LoginForm[password]": {
                    required: JSLANGUAGE.password_required
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: HTTP_ROOT + "/user/dologin",
                    data: $('#login_form').serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function() {
                        $('#login-loading').show();
                    },
                    success: function(data) {
                        $('#login-loading').hide();
                        if (data.login == 'success') {
                            if ($('#play_url').val()) {
                                window.location = $('#play_url').val();
                            } else {
                                if (data.action == 'home')
                                    window.location = HTTP_ROOT + '/';
                                else
                                    window.location = HTTP_ROOT + '/user/' + data.action;
                            }
                        } else if ($.trim(data.login) == 'error_limit_login') {
                            $('#login-errors').html(data.msg);
                            $('#login-errors').show();
                            $("#logout_all").modal('show');
                        } else {
                            $('#login-errors').html(JSLANGUAGE.incorrect_email_or_password);
                            $('#login-errors').show()
                        }
                    }
                });
            }
        });
    });
}); 