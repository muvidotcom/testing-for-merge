
function testgateway() {
    this.processCard = function(isAuthenticateOnly) {      
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        // Processing popup
        $("#loadingPopup").modal('show');
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
         if($('#plandetailbundles_id').val()!=''){
            subscriptionbundles_plan_id=$('#plandetailbundles_id').val();
        }
        if (parseInt(isAuthenticateOnly)) {
            testgatewayAuthResponse();
        } else {
            testgatewayResponseHandler();
        }
    };
    
    
    testgatewayAuthResponse = function() {
            document.membership_form.action = HTTP_ROOT + "/user/" + action;
            document.membership_form.submit();
    };

   testgatewayResponseHandler = function() {
        var card_number = $('#card_number').val();
        var card_name = $('#card_name').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        var description = 'Creation of New customer';
        
        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        var subscriptionbundles_plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
           
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }
        if($('#plandetailbundles_id').val()!=''){
            subscriptionbundles_plan_id=$('#plandetailbundles_id').val();
        }
        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }
        
        var discount_amount = $('#discount_amount').val();
        var new_free_trail = $('#free_trail_charged').val();
        var coupon = $('#coupon').val();
        
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='testgateway' />");
        var url = HTTP_ROOT + "/user/processCard";
       
        var cardInfo = { discount_amount:discount_amount,email: email, description: description, card_number: card_number,card_name: card_name,exp_month: exp_month,exp_year: exp_year,plan_id: plan_id, currency_id: currency_id,subscriptionbundles_plan_id:subscriptionbundles_plan_id};
        cardPayment(url,cardInfo);      
    };
    cardPayment = function(url, cardInfo){
        
        if(cardInfo.subscriptionbundles_plan_id!=0 && (typeof(cardInfo.subscriptionbundles_plan_id)!='undefined')){
            if (parseInt($("#creditcard option:selected").val())) {
              action='SaveuserSubscriptionBundles';   
            }else{
              action = 'SubscriptionBundlesPayment';
            }
           controllerRoot="userPayment";
        }
        else{
         controllerRoot="user";   
        }
        
        $.post(url, {'email': cardInfo.email,'description': cardInfo.description, 'card_number': cardInfo.card_number,'card_name': cardInfo.card_name,'exp_month': cardInfo.exp_month,'exp_year': cardInfo.exp_year,'plan_id': cardInfo.plan_id, 'currency_id': cardInfo.currency_id,subscriptionbundles_plan_id:cardInfo.subscriptionbundles_plan_id,'discount_amount': cardInfo.discount_amount}, function (data) {
            if (parseInt(data.isSuccess) === 1) {
                $("#successPopup").find('.success-popup-payment').html(JSLANGUAGE.thanks_card_auth_sucess_js);
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                if (data.card) {
                    for (var i in data.card) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                    }
                }
                
                if (data.transaction_data) {
                    for (var i in data.transaction_data) {
                        $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.membership_form.action = HTTP_ROOT + "/"+controllerRoot+"/" + action;
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalMain").length) {
                    $("#ppvModalMain").removeClass('fade');
                }
                if ($("#membership_loading").length) {
                    $("#membership_loading").hide();
                }                 
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if($("#paypal").length){
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    };
    this.physicalPayment = function() {
        var url = HTTP_ROOT+"/userPayment/processCard";
        var data = $('#payform').serialize();
        $.post(url, {'data': data,'coupon':$('#coupon').val()}, function (res) {
            var data = JSON.parse(res);
            if (data) {
                for (var i in data) {
                    $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data[i] + "' />");
				}
            }

            if (data.gateway_response.card) {
                for (var i in data.gateway_response.card) {
                    $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data.gateway_response.card[i] + "' />");
                }
            }
                
            if (data.gateway_response.transaction_data) {
                for (var i in data.gateway_response.transaction_data) {
                    $("#payform").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.gateway_response.transaction_data[i] + "' />");
                }
            }
            setTimeout(function () {
                document.payform.action = HTTP_ROOT + "/userPayment/saveOrder";
                document.payform.submit();
                return false;
            }, 5000);
        });
    };
}