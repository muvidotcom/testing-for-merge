function paygate () {
    
    this.processCard = function() {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        var url = HTTP_ROOT+"/user/processCard";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        
        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }

        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }
        var ppv_plan = $('#ppv_plan').val();
        if(ppv_plan){
            $('#paynowbtn').html(JSLANGUAGE.wait);
            $('#paynowbtn').attr('disabled', 'disabled');
        }
        //$("#paypalproPopup").modal('show');
        $("#loadingPopup").modal('show');
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='paygate' />");
        
        $.post(url, {'email': email, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv,'plan_id': plan_id, 'currency_id': currency_id}, function (data) {
            if (parseInt(data.isSuccess) === 1) {
                    $('#payment_method').val('paygate');
                    $("#card_div").append("<input type='hidden' name='data[returnURL]' value='"+data.returnURL+"' />");
                    $("#card_div").append("<input type='hidden' name='data[cancelURL]' value='"+data.cancelURL+"' />");
                    $("#card_div").append("<input type='hidden' name='data[plan_desc]' value='"+data.plan_desc+"' />");
                    $("#card_div").append("<input type='hidden' name='data[payment_method]' value='paygate' />");
                    setTimeout(function () {
                        document.membership_form.action = HTTP_ROOT + "/user/" + action;
                        document.membership_form.submit();
                        return false;
                }, 5000);
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalPricingMain").length) {
                    $("#ppvModalPricingMain").removeClass('fade');
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    }
    
    this.physicalPayment = function() {
        var url = HTTP_ROOT+"/userPayment/processCard";
        var data = $('#payform').serialize();
        $.post(url, {'data': data,'coupon':$('#coupon').val()}, function (res) {
            var data = JSON.parse(res);
            if (data) {
                for (var i in data) {
                    $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data[i] + "' />");
}
            }
            setTimeout(function () {
                document.payform.action = HTTP_ROOT + "/userPayment/saveOrder";
                document.payform.submit();
                return false;
            }, 5000);
        });
    };
}