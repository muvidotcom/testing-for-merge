
function stripe() {
    this.processCard = function (isAuthenticateOnly) {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        var is_sepa = $('#is_sepa').val();
        if (parseInt(is_sepa)) {
            $("#loadingPopup").find('.auth-msg').html(JSLANGUAGE.auth_your_account);
        } else {
            $("#loadingPopup").find('.auth-msg').html(JSLANGUAGE.auth_your_card_js);
        }
        // Processing popup
        $("#loadingPopup").modal('show');
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
        if ($('#plandetailbundles_id').val() != '') {
            subscriptionbundles_plan_id = $('#plandetailbundles_id').val();
        }
        if (parseInt(isAuthenticateOnly)) {
            stripeAuthResponse();
        } else {
            stripeResponseHandler();
        }
    };
    accountPayment = function (url, accountInfo) {
        $.post(url, {'email': accountInfo.email, 'discount_amount': accountInfo.discount_amount, 'new_free_trail': accountInfo.new_free_trail, 'coupon': accountInfo.coupon, 'description': accountInfo.description, 'iban_number': accountInfo.iban_number, 'account_name': accountInfo.account_name, 'street_name': accountInfo.street_name, 'city_name': accountInfo.city_name, 'postalcode': accountInfo.postalcode, 'country_name': accountInfo.country_name, 'plan_id': accountInfo.plan_id, 'currency_id': accountInfo.currency_id, 'is_sepa': accountInfo.is_sepa}, function (data) {
            if (parseInt(data.isSuccess) === 1 && parseInt(data.card.code)==200 && $.trim(data.card.status)=='succeeded') {
                $("#successPopup").find('.success-popup-payment').html(JSLANGUAGE.thanks_account_auth_sucess);
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                if (data.card) {
                    for (var i in data.card) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                    }
                }

                if (data.transaction_data) {
                    for (var i in data.transaction_data) {
                        $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.membership_form.action = HTTP_ROOT + "/user/" + action;
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalMain").length) {
                    $("#ppvModalMain").removeClass('fade');
                }
                if ($("#membership_loading").length) {
                    $("#membership_loading").hide();
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if ($("#paypal").length) {
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    };

    cardPayment = function (url, cardInfo) {
        is_subscription_buldle = 0;
        if (cardInfo.subscriptionbundles_plan_id != 0 && (typeof (cardInfo.subscriptionbundles_plan_id) != 'undefined')) {
            if (parseInt($("#creditcard option:selected").val())) {
                action = 'SaveuserSubscriptionBundles';
            } else {
                action = 'SubscriptionBundlesPayment';
            }
            controllerRoot = "userPayment";
            is_subscription_buldle = 1;
        }
        else {
            controllerRoot = "user";
        }
        $.post(url, {'email': cardInfo.email, 'discount_amount': cardInfo.discount_amount, 'new_free_trail': cardInfo.new_free_trail, 'coupon': cardInfo.coupon, 'description': cardInfo.description, 'card_number': cardInfo.card_number, 'card_name': cardInfo.card_name, 'exp_month': cardInfo.exp_month, 'exp_year': cardInfo.exp_year, 'cvv': cardInfo.cvv, 'plan_id': cardInfo.plan_id, 'currency_id': cardInfo.currency_id, is_sepa: cardInfo.is_sepa, subscriptionbundles_plan_id: cardInfo.subscriptionbundles_plan_id, is_subscription_buldle: is_subscription_buldle}, function (data) {
            if (parseInt(data.isSuccess) === 1  && parseInt(data.card.code)==200 && $.trim(data.card.status)=='succeeded') {
                $("#successPopup").find('.success-popup-payment').html(JSLANGUAGE.thanks_card_auth_sucess_js);
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                if (data.card) {
                    for (var i in data.card) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                    }
                }

                if (data.transaction_data) {
                    for (var i in data.transaction_data) {
                        $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.membership_form.action = HTTP_ROOT + "/" + controllerRoot + "/" + action;
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalMain").length) {
                    $("#ppvModalMain").removeClass('fade');
                }
                if ($("#membership_loading").length) {
                    $("#membership_loading").hide();
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if ($("#paypal").length) {
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    };

    stripeAuthResponse = function () {
        document.membership_form.action = HTTP_ROOT + "/user/" + action;
        document.membership_form.submit();
    };

    stripeResponseHandler = function () {
        var is_sepa = $('#is_sepa').val();
        if (parseInt(is_sepa)) {
            var iban_number = $('#iban').val();
            var account_name = $('#account_name').val();
            var street_name = $('#street').val();
            var city_name = $('#city').val();
            var postalcode = $('#postalcode').val();
            var country_name = $('#country').val();
        } else {
            var card_number = $('#card_number').val();
            var card_name = $('#card_name').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
        }
        var description = 'Creation of New customer';

        if ($.trim($('#email_address').val())) {
            var email = $('#email_address').val();
        } else {
            var email = $('#email').val();
        }
        var plan_id = 0;
        var subscriptionbundles_plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        } else if ($('#plandetail_id').length) {
            plan_id = $('#plandetail_id').val();
        }
        if ($('#plandetailbundles_id').val() != '') {
            subscriptionbundles_plan_id = $('#plandetailbundles_id').val();
        }
        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }

        var discount_amount = $('#discount_amount').val();
        var new_free_trail = $('#free_trail_charged').val();
        var coupon = $('#coupon').val();

        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='stripe' />");
        var url = HTTP_ROOT + "/user/processCard";

        if (parseInt(is_sepa)) {
            var accountInfo = {email: email, 'discount_amount': discount_amount, 'new_free_trail': new_free_trail, 'coupon': coupon, description: description, iban_number: iban_number, account_name: account_name, street_name: street_name, city_name: city_name, postalcode: postalcode, country_name: country_name, plan_id: plan_id, currency_id: currency_id, is_sepa: is_sepa};
            accountPayment(url, accountInfo);
        } else {
            var cardInfo = {email: email, 'discount_amount': discount_amount, 'new_free_trail': new_free_trail, 'coupon': coupon, description: description, card_number: card_number, card_name: card_name, exp_month: exp_month, exp_year: exp_year, cvv: cvv, plan_id: plan_id, currency_id: currency_id, is_sepa: is_sepa, subscriptionbundles_plan_id: subscriptionbundles_plan_id};
            cardPayment(url, cardInfo);
        }
    };

    loadScript = function (url, callback) {
        var script = document.createElement("script")
        script.type = "text/javascript";
        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    };

    this.pciPayment = function () {
        loadScript("https://checkout.stripe.com/checkout.js", function () {
            $.post(HTTP_ROOT + "/user/paymentUserDeatils", {gateway_code: 'stripe'}, function (res) {
                var result = $.parseJSON(res);
                var pg_amount = $('#pg_amount').val();
                var coupon = $('#coupon').val();
                var prodIds = $('#physical_specific_ids').val();
                var pg_currency_id = $('#pg_currency_id').val();
                var pg_shipping_cost = $('#pg_shipping_cost').val();
                var url = HTTP_ROOT + '/shop/getCurrency';
                $.post(url, {'currency_id': pg_currency_id, 'coupon': coupon, 'amount': pg_amount, 'shipping_cost': pg_shipping_cost, 'ProdIds': prodIds}, function (data) {
                    var data = $.parseJSON(data);
                    var amount = Math.round(data.amount * 100) / 100;

                    setTimeout(function () {
                        $('#pci_click_event').trigger('click');
                    }, 2000);
                    token_triggered = false;
                    var handler = StripeCheckout.configure({
                        key: result.api_password,
                        image: result.studio_logo,
                        locale: 'auto',
                        token: function (token) {
                            token_triggered = true;
                            var pci_url = HTTP_ROOT + '/shop/ProcessPCITransaction';
                            var pay_data = $('#payform').serialize();
                            var coupon_id = $.trim($('#coupon').val()) ? $('#coupon').val() : '';
                            var token_id = token.id;
                            $.post(pci_url, {'data': pay_data, 'coupon': coupon_id, 'token': token_id, 'prodsID': prodIds}, function (resArray) {
                                $("#loadingPopup").modal('hide');
                                if (resArray.is_success) {
                                    $("#successPopup").modal('show');
                                    $('.loader_cart').hide();
                                    setTimeout(function () {
                                        window.location.href = HTTP_ROOT + '/shop/success';
                                        return false;
                                    }, 5000);
                                } else {
                                    window.location.href = HTTP_ROOT + '/shop/cancel';
                                    return false;
                                }
                            }, 'json');
                        },
                        closed: function () {
                            if (!token_triggered) {
                                window.location.href = HTTP_ROOT + '/shop/cancel';
                                return false;
                            }
                        }
                    });

                    document.getElementById('pci_click_event').addEventListener('click', function (e) {
                        handler.open({
                            name: result.studio_name,
                            amount: amount * 100,
                            currency: data.currency_code
                        });
                        e.preventDefault();
                    });

                    window.addEventListener('popstate', function () {
                        handler.close();
                    });

                });
            });
        });
    };
    this.physicalPayment = function () {
        var url = HTTP_ROOT + "/userPayment/processCard";
        var data = $('#payform').serialize();
        $.post(url, {'data': data, 'coupon': $('#coupon').val()}, function (res) {
            var data = JSON.parse(res);
            if (data) {
                for (var i in data) {
                    $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data[i] + "' />");
                }
            }

            if (data.gateway_response.card) {
                for (var i in data.gateway_response.card) {
                    $("#payform").append("<input type='hidden' name='data[" + i + "]' value='" + data.gateway_response.card[i] + "' />");
                }
            }

            if (data.gateway_response.transaction_data) {
                for (var i in data.gateway_response.transaction_data) {
                    $("#payform").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.gateway_response.transaction_data[i] + "' />");
                }
            }
            setTimeout(function () {
                document.payform.action = HTTP_ROOT + "/userPayment/saveOrder";
                document.payform.submit();
                return false;
            }, 5000);
        });
    };
}

$('#credit_card_control').click(function () {
    $('#is_sepa').val(0);
});

$('#sepa_control').click(function () {
    $('#is_sepa').val(1);
});
