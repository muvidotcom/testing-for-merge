var imported = document.createElement('script');
imported.src = '../common/js/jquery.creditCardValidator.js';
document.head.appendChild(imported);

function hbl () {
    
    this.processCard = function (isAuthenticateOnly) {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');

        // Processing popup
        $("#loadingPopup").modal('show');
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
        if (parseInt(isAuthenticateOnly)) {
            hblAuthResponse();
        } else {
            hblResponseHandler();
        }
    }
    
    hblAuthResponse = function () {
        $('#card_number').validateCreditCard(function(result) {
            card_type = result.card_type === null ? '-' : result.card_type.name;
            $("#card_div").append("<input type='hidden' name='data[card_type]' value='" + card_type + "' />");
        });
        document.membership_form.action = HTTP_ROOT + "/user/" + action;
        document.membership_form.submit();
    }
    
    hblResponseHandler = function() {
        
        var url = HTTP_ROOT+"/user/processCard";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }

        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }

        var ppv_plan = $('#ppv_plan').val();
        if(ppv_plan){
           $('#paynowbtn').html(JSLANGUAGE.wait);
            $('#paynowbtn').attr('disabled', 'disabled');
        }
        var reference_number = new Date().getTime();
        
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='hbl' />");
        $.post(url, {'email': email, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv,'plan_id': plan_id,'ppv':ppv_plan,'currency_id': currency_id,'reference_number':reference_number}, function (data) {

            if (parseInt(data.isSuccess) === 1) {
                $("#loadingPopup").modal('hide');
                $("#successPopup").modal('show');
                $('#card_number').validateCreditCard(function(result) {
                    data.card.card_type = result.card_type === null ? '-' : result.card_type.name;
                });
                if (data.card) {
                    for (var i in data.card) {
                        $("#card_div").append("<input type='hidden' name='data[" + i + "]' value='" + data.card[i] + "' />");
                    }
                }
                if (data.transaction_data) {
                    for (var i in data.transaction_data) {
                        $("#card_div").append("<input type='hidden' name='data[transaction_data][" + i + "]' value='" + data.transaction_data[i] + "' />");
                    }
                }
                setTimeout(function () {
                    document.membership_form.action = HTTP_ROOT+"/user/" + action;
                    document.membership_form.submit();
                    return false;
                }, 5000);
            } else {                
                $("#loadingPopup").modal('hide');
                if ($("#membership_loading").length)
                    $('#membership_loading').hide();
                if ($("#ppvModalPricingMain").length) {
                    $("#ppvModalPricingMain").removeClass('fade');
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if($("#paypal").length){
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    };
    
};