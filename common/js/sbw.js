function sbw () {
    
    this.processCard = function(isAuthenticateOnly) {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        if(parseInt(isAuthenticateOnly)){
        }else{
             $("#loadingPopup").modal('show');
        }
        // Processing popup
       
        if ($("#ppvModalMain").length) {
            $("#ppvModalMain").addClass('fade');
        }
         if($('#plandetailbundles_id').val()!=''){
            subscriptionbundles_plan_id=$('#plandetailbundles_id').val();
        }
      
        if (parseInt(isAuthenticateOnly)) {
            sbwAuthResponse();
        } else {
            sbwResponseHandler();
        }
    };
    
    sbwAuthResponse = function() {
            document.membership_form.action = HTTP_ROOT + "/user/" + action;
            document.membership_form.submit();
    };
    
    
    
    sbwResponseHandler = function() {
        $('#register_membership').html(JSLANGUAGE.wait);
        $('#register_membership').attr('disabled', 'disabled');
        var url = HTTP_ROOT+"/user/processCard";

        if($.trim($('#email_address').val())){
            var email = $('#email_address').val();
        }else{
            var email = $('#email').val();
        }
        var plan_id = 0;
        if ($('#plan_id').length) {
            plan_id = $('#plan_id').val();
        }else if($('#plandetail_id').length){
            plan_id = $('#plandetail_id').val();
        }

        var currency_id = 0;
        if ($('#currency_id').length) {
            currency_id = $('#currency_id').val();
        }

        var ppv_plan = $('#ppv_plan').val();
        if(ppv_plan){
           $('#paynowbtn').html(JSLANGUAGE.wait);
            $('#paynowbtn').attr('disabled', 'disabled');
        }
        var form_data = $("#membership_form").serialize();
        $("#card_div").append("<input type='hidden' name='data[payment_method]' value='sbw' />");
        $.post(url, {'form_data': form_data}, function (data) {
            if (parseInt(data.isSuccess) === 1) {
                window.location = 'https://secure.sbw.com/v2/join?pay_type=cc&prod_id=' + data.prod_id + '&mid=' + data.mid + '&success_url=' + data.success_url;
            } else {
                $("#loadingPopup").modal('hide');
                if ($("#ppvModalPricingMain").length) {
                    $("#ppvModalPricingMain").removeClass('fade');
                }
                $('#register_membership').html(btn);
                $('#register_membership').removeAttr('disabled');
                if($("#paypal").length){
                    $("#paypal").removeAttr("disabled");
                }
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    }
}