function ippayment() {
    
    this.generateSST = function() {
        var _url = HTTP_ROOT + '/userPayment/GenerateSST';
        var session_url = (window.location != window.parent.location) ? document.referrer : document.location.href;
        var use_coupon = $.trim($("#coupon_use").val());
                var coupon_code = '';
                var coupon_currency_id = 0;
                if(use_coupon === "1"){
                    coupon_code =  $.trim($("#coupon").val());
                    coupon_currency_id =  $("#currency_id").val();
                }
                var payment_method =  $("#payment_method").val();
                
                var movie_id = $("#ppvmovie_id").val();
                var season_id = 0;
                var episode_id = 0;
                var isadv = 0;
                
                var is_show = $("#is_show").val();
                var is_season = $("#is_season").val();
                var is_episode = $("#is_episode").val();
                var content_types_id = $("#content_types_id").val();
                var timeframe_id = $("#timeframe_id").val();
                if($.trim($("#isadv").val())){
                    isadv = $("#isadv").val();
                }else if($.trim($("#is_adv_plan").val())){
                     isadv = $("#is_adv_plan").val();
                }
                var purchase_type = "";
                
                var season_title = '';
                var episode_title = '';
                
                if ($("#showtext").is(':checked')) {
                     purchase_type = "show";
                } else if (parseInt(is_season) && $(".seasontext").is(':checked')) {
                    season_id = $.trim($("#temp_season_id").val());
                    episode_id = 0;
                    purchase_type = "season";
                    
                    season_title = $("#temp_season_title").text();
                } else if (parseInt(is_episode) && $(".episodetext").is(':checked')) {
                    season_id = $.trim($("#temp_season_id").val());
                    episode_id = $.trim($("#temp_episode_id").val());
                    purchase_type = "episode";
                    
                    season_title = $("#temp_season_title").text();
                    episode_title = $("#temp_episode_title").text();
                }
        
        season_id = $.trim($("#temp_season_id").val());
        episode_id = $.trim($("#temp_episode_id").val());
        season_title = $("#temp_season_title").text();
        episode_title = $("#temp_episode_title").text();
        var plan_id = $("#plandetail_id").val();
        var permalink = $("#permalink").val();
        var coupon = $("#coupon").val();
        $('#other-payment-gateway').hide();
        
        $.post(_url, {'session_url':session_url,'permalink':permalink,'plan_id':plan_id,'movie_id': movie_id, 'season_id': season_id, 'episode_id': episode_id, 'purchase_type': purchase_type, 'content_types_id': content_types_id, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'isadv': isadv,'coupon':coupon, 'timeframe_id':timeframe_id}, function (data) {
            
            $('#loader-ppv').hide();
            $('#other-payment-gateway').show();
            $('#paynowbtn').parent().parent().parent().hide();
            $('#iframeContainer').show();
            var url = data.ipp_info.token_url;
            var site = url+'?SessionID='+data.ipp_info.session_id+'&SST='+data.ipp_info.sst;
            $('#iframeContainer_ipp').attr("src",site);
            $('#iframeContainer_ipp').show();
        }, 'json');
    };
}