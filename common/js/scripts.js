/* 
 * All javascript methods are added here
 * @author - RK
 */


$(document).ready(function() {
    $('#loader-login').show();
    $('#login-loading').hide();
    $('#register-loading').hide();
    $('#login-form').validate({
        rules: {
            "LoginForm[email]": {
                required: true,
                mail: true
            },
            "LoginForm[password]": {
                required: true,
            },
        },
        message: {
            "LoginForm[email]": {
                required: "Please enter email address",
                mail: "Please enter a valid email address"
            },
            "LoginForm[password]": {
                required: "Please enter your password",
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxlogin",
                data: $('#login-form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#loader_login').show();
                    $('#login-loading').show();
                },
                success: function(data) {
					$('input[name=csrfToken]').val(data.csrfToken);
                    $('#loader').hide();
                    $('#login-loading').hide();
                    if (data.login == 'success') {
                        if ($.trim($("#movie_id").val())) {
                            if (parseInt($("#isppv").val())) {
                                $('#loader_login').hide();
                                getPpvPlans();
                            } else {
                                playMovie();
                            }
                        } else {
                            location.reload();
                        }
                    }
                    else
                        $('#login-errors').html("You have entered incorrect email or password.");
                },
                complete: function() {
                    $('#loader_login').hide();
                    $('#login-loading').hide();
                }
            });
        }
    });

    $('#register-form').validate({
        rules: {
            "data[name]": {
                required: true,
                minlength: 2
            },
            "data[email]": {
                required: true,
                mail: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
        },
        message: {
            "data[name]": {
                required: "Please enter your full name",
                minlength: "Name must be atleast of 2 characters long"
            },
            "data[email]": {
                required: "Please enter email address",
                mail: "Please enter a valid email address"
            },
            "data[password]": {
                required: "Please enter your password",
                minlength: "Your password must be atleast 6 characters long"
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxregister",
                data: $('#register-form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#loader_register').show();
                    $('#register-loading').show();
                },
                success: function(data) {
                    $('#register-loading').hide();
                    $('#loader_register').show();
                    if (data.login == 'success') {
                        if ($.trim($("#movie_id").val())) {
                            if (parseInt($("#isppv").val())) {
                                $('#loader_register').show();
                                getPpvPlans();
                            } else {
                                playMovie();
                            }
                        } else {
                            location.reload();
                        }
                    }
                    else
                        $('#register-errors').html("Oops! there is some error in saving the data.");
                },
                complete: function() {
                     $('#loader_register').hide();
                    $('#register-loading').hide();
                }
            });
        }
    });

    $(".close").click(function() {
        $("#movie_id").val('');
        $("#stream_id").val('');
        $(".modal-backdrop").remove();
        showLogin();
    });
    
});


function getPpvPlans() {
    var movie_id = $("#movie_id").val();
    var stream_id = $.trim($("#stream_id").val());
    var dataname = $("#videotype").val();
    var purchase_type = "show";
    $('#loader_purchase').show();
    if (stream_id !== '') {
        purchase_type = "episode";
    }
    var content_permalinks = $('#content-permalink').val();
    var url = HTTP_ROOT + "/user/getPpvPlans";
    var content_type_permalinks = $('#content-type-permalink').val();
         $('#loader_purchase').hide();
    $.post(url, {'movie_id': movie_id, 'stream_id': stream_id, 'purchase_type': purchase_type}, function(res) {
        if (parseInt(res) === 1) {
            if (stream_id !== '') {
                window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + content_type_permalinks + "/" + content_permalinks + "/stream/" + stream_id;
            } else {
                window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + content_type_permalinks + "/" + content_permalinks;
            }
        } else {
            reload = 1;
            $("#loginModal").hide();
            $("#ppvModal").html(res).modal('hide');

            if (parseInt(is_plan_error)) {
            $("#ppvModal").html(res).modal('show');
                $('#loader-ppv').hide();
            }
            $("#movitype").html(dataname);
        }
    });
}

function playMovie() {
    var movie_id = $("#movie_id").val();
    var stream_id = $.trim($("#stream_id").val());
    var content_permalinks = $('#content-permalink').val();
    var content_type_permalinks = $('#content-type-permalink').val();
    if (stream_id !== '' && stream_id !== '0') {
        window.location.href = HTTP_ROOT + "/player/" + content_type_permalinks + "/" + content_permalinks + "/stream/" + stream_id;
    } else {
        window.location.href = HTTP_ROOT + "/player/" + content_type_permalinks + "/" + content_permalinks;
    }
}