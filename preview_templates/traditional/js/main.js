/* Filter Popup code */
$(function () {
    var button = $('#loginButton');
    var box = $('#loginBox');
    var form = $('#filterFormPopup');
    button.removeAttr('href');
    button.mouseup(function (login) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function () {
        return false;
    });
    $(this).mouseup(function (login) {
        if (!($(login.target).parent('#loginButton').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});
/* Filter Popup code end */
function show_block(block) {
    if (block == "tv") {
        $("#tvres_block").show();
        $("#movieres_block").hide();
        $("#tv_header").addClass("active_block");
        $("#movie_header").removeClass("active_block");
        $("#tv_header .block_header").addClass("active_text");
        $("#movie_header .block_header").removeClass("active_text");
    } else {
        $("#movieres_block").show();
        $("#tvres_block").hide();
        $("#movie_header").addClass("active_block");
        $("#tv_header").removeClass("active_block");
        $("#movie_header .block_header").addClass("active_text");
        $("#tv_header .block_header").removeClass("active_text");
    }
}

function play_movie(url) {
    if ($('#to_play').length)
    {
        $('#to_play').val("play");
    }
    if ($('#play_url').length)
    {
        $('#play_url').val(url);
    }
    var is_login = check_login();
    if (is_login) {
        window.location.href = url;
    }
}

//Added for PPV
function ppv_play_movie(link) {
    var is_login = check_login();
    if (is_login) {
        window.location.href = link;
    }
    else {
        return false;
    }
}

function show_warning(message) {
    if ($("#alert-area").html() == "") {
        var type = "error";
        $("#alert-area").append($("<div class='alert-message " + type + " fade in' data-alert><p> " + message + " </p></div>"));
        $("#alert-area").show();//$("#alert-area").hide();
        $(".alert-message").delay(5000).fadeOut("slow", function () {
            $(this).remove();
            $("#alert-area").hide();
        });
    }
}