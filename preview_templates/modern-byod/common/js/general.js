/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author : RK
 */
$(document).ready(function () {
    $("form").each(function() {
        if($(this).find('input:password')){
            if($(this).attr('id') !="login_form"){
                if($(this).attr('id')=="membership_form"){
                    $(this).find('input#card_number').eq(0).val("");
                    $(this).find('input#email_address').eq(0).val("");
                }
                $(this).find('input:password').eq(0).val("");
            }
        }
    });
    $('.apply').click(function (event) {
        $("#confirmApply").modal('show');
    });

    $('.applyconfirm').click(function (event) {
        $(".change_template").text('Apply...')
        $('.revert').prop("disabled", true);
        $.ajax({
            type: "post",
            url: "/site/applytemplatepreview",
            data: "",
            success: function (data) {
                $(".change_template").html('WOW ! New template is applied successfully');
                location.href = website_url + "/?preview=2";
            }

        });
    });
    $('.revert').click(function (event) {
        $('.revert').text('Wait...');
        $('.apply').prop("disabled", true);
        $.ajax({
            type: "post",
            url: "/site/reverttemplatepreview",
            data: "",
            success: function (data) {
                location.href = website_url + "/?preview=2";
            }
        });
    });
    $('#browsefile').click(function(){
        $('#avatarInput').click();
    });
    $('#avatarInput').change(function(e){
        var fileName = e.target.files[0].name;
        $(this).next().html(fileName);
    });
    $('#contact_us').on('click', function () {
        $('#submit-btn').val('contact');
         $("#contactus_form").validate({ 
            rules: {    
                "ContactForm[name]": {
                        required: true,
                },
                "ContactForm[email]": {
                    required: true,
                    email: true
                }  ,
                "ContactForm[message]":{
                    required: true
                }
            },messages: {
                "ContactForm[name]": {
                    required:  JSLANGUAGE.full_name_required,
                },
                "ContactForm[email]": {
                    required:  JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "ContactForm[message]": {
                    required: JSLANGUAGE.please_enter_message
                }
            },         

        });
    });
    updateLoginHistory();
});
function updateLoginHistory(){
    $.ajax({
        url: HTTP_ROOT + '/site/updateLoginHistory/',
        type: "POST",
        data: {login_count:'login_count'},          
        success: function (data) {
            console.log(data);
        }
    }); 
}
function toggleNewsletter() {
    $("#newsletter-form-block").toggle();
}
$(function () {
    $('#myTrailer').hide();
    $('#newsletter-form-block').hide();
    $('#newsletter-loader').hide();

    //Newsletter form
    $('#newsletter-form').validate({
        rules: {
            "newsletter-email": {
                required: true,
                email: true
            },
        },
        messages: {
            "newsletter-email": {
                required:  JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
        },
        errorLabelContainer: "#newsletter-message",
        submitHandler: function (form) {
            $.ajax({
                url: HTTP_ROOT + "/content/savenewsletter",
                data: $('#newsletter-form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function (data) {
                    $('#newsletter-loader').show();
                },
                success: function (data) {
                    $('#newsletter-loader').hide();
                    if (data.status == 'success') {
                        $('#newsletter-form').html('');
                        $('#newsletter-message').html(data.message).removeClass('error').addClass('success').show();
                    }
                    else {
                        $('#newsletter-message').html(data.message).addClass('error').show();
                    }
                },
            });
        }
    });
    //autocomplete
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this, currentCategory = "";
            $.each(items, function (index, item) {
                /*for(var i in item){
                 alert(item[i]);
                 }*/
                if (item.category != currentCategory) {
                    //alert(item.category);
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }

                self._renderItemData(ul, item);
            });
        }
    });
    var category = "";
    $("#siteSearch").catcomplete({
        source: HTTP_ROOT + "/search/search/",
        minLength: 1,
        select: function (event, ui) {
            console.log(HTTP_ROOT);
            var I = ui.item;
            var searchCat = I.category;
            searchCat = searchCat.toLowerCase();
            var logurl = HTTP_ROOT + '/search/siteSearchLog';
            $.post(logurl, {'object_category': I.content_types, 'search_string': I.value, 'search_url': window.location.href, 'object_id': I.id}, function (res) {
                if (parseInt(searchCat.search(" - episode")) >= 0) {
                    window.location = HTTP_ROOT + '/player/' + I.title + "/stream/" + I.stream_uniq_id;
                }else if(searchCat == 'star'){
					window.location = HTTP_ROOT +'/star/' + I.title;
				}else{
                    window.location = HTTP_ROOT + '/' + I.title;
                }

            });
        }
    });

});

function check_submit() {
    $("#search-form").submit();
}
function validate_search() {
    var search_val = $("input[name='search_field']").val();
    if (search_val == "") {
        return false;
    }
}
var popupCenter = function (url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 3) - (h / 3)) + dualScreenTop;

    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (newWindow && newWindow.focus) {
        newWindow.focus();
    }
};
$(document).ready(function () {
    try {
        $(document).on('click', 'a.social-share-popup', {}, function popUp(e) {
            var self = $(this);
            popupCenter(self.attr('href'), self.attr('data-title'), 580, 470);
            e.preventDefault();
        });
    } catch (e) {
    }
});
function getvideodtls(obj){
    var movie_id = $(obj).attr('data-movie_id');
    var start_time = $(obj).attr('data-start_time');
    var end_time = $(obj).attr('data-end_time');
    $('#player-loading').show();
    $('#poster-loading').show();    
    $.ajax({
        url: HTTP_ROOT + "/tvguide/getvideodtls",
        data: {movie_id:movie_id,start_time:start_time,end_time:end_time},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if(data.permalink != ''){                
                $('#poster-loading').hide();                
                $('#content_story').html(data.content_short_description);
                $('#content_poster').attr('src', data.content_poster);
                $('#content_title').html(data.content_title);
                $.ajax({
                    type: 'POST',
                    url: HTTP_ROOT + '/TvGuidePlayer/'+data.permalink+'?start_time='+data.start_time+'&old_value=1',
                    success: function(res){
                        $('#player-loading').hide();
                        $("#video_player").html("");
                        $('#video_player').html(res);
                    }
                }); 
            }else{
            /*    $('#poster-loading').hide();  
                $('#player-loading').hide();
                $('#content_story').html("");
                $('#content_poster').attr('src', THEME_URL+"/images/livetv-thumb.jpg");
                $("#video_player").html('<img src="'+default_image+'" class="img-responsive default_image"/>');
            */
           
            var vjsscript = '<script data-cfasync="false" type="text/javascript" src="'+HTTP_ROOT+'/vast/js/video.js"></script><link href="'+HTTP_ROOT+'/vast/css/videojs.vast.css" rel="stylesheet" type="text/css"><link href="'+HTTP_ROOT+'/vast/css/video.js.css" rel="stylesheet" type="text/css"><script data-cfasync="false" type="text/javascript">    videojs.options.flash.swf = "'+HTTP_ROOT+'/js/video-js.swf";</script>';
            $("#video_player").html(vjsscript+'<video id="video_block1" class="video-js vjs-default-skin vjs-16-9 " preload="none" width="auto" height="auto" ></video>');
            $("#video_player").html("");
                var playerSetup = videojs('video_block1');
            playerSetup.ready(function () {
                var player = this;
                $('.vjs-loading-spinner').hide();
                $('.vjs-control-bar').show();
                $('.vjs-control-bar').css('display','block');
                $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
                $('.vjs-play-control').css('pointer-events','none');
                $('.vjs-progress-control ').css('pointer-events','none');
                $("#video_block1_html5_api").attr('poster', image_path);
                $("#video_block1").attr('poster', image_path);
                $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                player = this;
                player.load();
                 $(".vjs-mute-control").css("display","none"); 
                 $(".vjs-volume-control").css("display","none");
                 $(".vjs-fullscreen-control").css("display","none");
            });
            }
        },
    });
}
function getvideoposterdtls(obj){
    var movie_id = $(obj).attr('data-movie_id');
    var start_time = $(obj).attr('data-start_time');
    var end_time = $(obj).attr('data-end_time');
    $('#poster-loading').show();    
    $.ajax({
        url: HTTP_ROOT + "/tvguide/getvideodtls",
        data: {movie_id:movie_id,start_time:start_time,end_time:end_time},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#poster-loading').hide();                
            $('#content_story').html(data.content_short_description);
            $('#content_poster').attr('src', data.content_poster);
            $('#content_title').html(data.content_title);
        },
    });
}
function noprogram(){
    $('#poster-loading').hide();  
    $('#player-loading').hide();
    $("#content_title").html("");
    $('#content_story').html("");
    $('#content_poster').attr('src', THEME_URL+"/images/livetv-thumb.jpg");
   // $("#video_player").html('<img src="'+default_image+'" class="img-responsive default_image"/>');
     var vjsscript = '<script data-cfasync="false" type="text/javascript" src="'+HTTP_ROOT+'/vast/js/video.js"></script><link href="'+HTTP_ROOT+'/vast/css/videojs.vast.css" rel="stylesheet" type="text/css"><link href="'+HTTP_ROOT+'/vast/css/video.js.css" rel="stylesheet" type="text/css"><script data-cfasync="false" type="text/javascript">    videojs.options.flash.swf = "'+HTTP_ROOT+'/js/video-js.swf";</script>';
         $("#video_player").html("");    
    $("#video_player").html(vjsscript+'<video id="video_block1" class="video-js vjs-default-skin vjs-16-9 " preload="none" width="auto" height="auto" ></video>');
            var playerSetup = videojs('video_block1');
            playerSetup.ready(function () {
                var player = this;
                player = this;
                player.load();
                $('.vjs-loading-spinner').hide();
                $('.vjs-control-bar').show();
                $('.vjs-control-bar').css('display','block');
                $('.vjs-default-skin .vjs-control-bar').css('z-index','100');
                $('.vjs-play-control').css('pointer-events','none');
                $('.vjs-progress-control ').css('pointer-events','none');
                $("#video_block1_html5_api").attr('poster', default_image);
                $("#video_block1").attr('poster', default_image);
                $( ".video-js" ).attr('style','padding-top:47%; height:50%;');
                $(".vjs-mute-control").css("display","none"); 
                 $(".vjs-volume-control").css("display","none");
                 $(".vjs-fullscreen-control").css("display","none");
            });

}
$(document).ready(function(){
    $('.time_slide').click(function(){
        $('#program-loading').show();
        var timeguide =  $(this).attr('data-id');
        var action_url = HTTP_ROOT + '/tvguide/gettime';
        var current_program =  $("#current_program").val();
        var programs_to_start =  $("#programs_to_start").val();
        var current_channel= $("#current_channel").val();
        $.ajax({
            url: action_url,
            type: "POST",
            dataType: 'json',
            data: {timeguide:timeguide,current_program:current_program,programs_to_start:programs_to_start},
            success: function (data) {
               //$('#program-loading').hide();
               $("#timeguide").html(data.time_slide); 
               $(".left").attr('data-id','before_'+data.left_right);
               $(".right").attr('data-id','after_'+data.left_right);
               //$("#program-lists .mCSB_container").html(data.response);
               //upload guide as per the time slide time moved.
               $.ajax({
                        url: HTTP_ROOT + '/tvguide/Gettimewiseguide',
                        type: "POST",
                        data: {current_program:current_program,programs_to_start:programs_to_start,nowtime:data.nowtime,start_time:data.start_time,end_time:data.end_time},          
                        success: function (data) {
                            $('#program-loading').hide();
                            if(data != ''){
                                
                                $("#program-lists .mCSB_container").html(data);
                                $('.noprogram:first-child').trigger('click');
                                $('.cursor_pointer:first').trigger('click');
                            }else{
                                $('#player-loading').hide();
                                $('#poster-loading').hide();
                            }
                        }
                    });
            }
        });
    });
    setInterval(function(){
       updateLoginHistory();
    }, 600000);
});

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

function redirect_url(id)
{
 var old_url= HTTP_ROOT+'/tvguide/getOldGuide/id/'+id;
             window.open(old_url);    
}

function changeLang(lang_code) {
    var cname = "Language";
    var cvalue = lang_code;
    $.ajax({
        url: HTTP_ROOT + '/site/setLanguageCookie/',
        type: "POST",
        data: {lang_code:lang_code}, 
        async:true,
        success: function (data) {
            location.reload();
        }
    });
}
function setCookie(cname, cvalue, exdays) {
    var domain = window.location.hostname;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=." + domain + ";path=/";
} 
function isMobile(){
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(navigator.userAgent||navigator.vendor||window.opera)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test((navigator.userAgent||navigator.vendor||window.opera).substr(0,4)))
}