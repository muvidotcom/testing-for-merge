$(document).ready(function () {
      // main.html javascript start
        $(".listpos").mouseover(function(){
            $(this).find( ".genre_list span" ).last().html('');
        });
        
    $('.playbtn').click(function () {
        var mov_id = $(this).prop('id');
        $('#to_play').val('play');
        $('#play_url').val(mov_id);
    });
    
      
//    $('#my-tooltip').tooltipster({
//        interactive: true,
//        position: 'right',
//        content: $('<div class=""> <img src="images/tooptip-img.jpg"  class="img-responsive"><h4 class="gray">The 100</h4><img src="images/star.jpg"><h4 class="gray">Story</h4><p class="gray">A nuclear conflict has decimated civilisation. Ninety-seven years later, a spaceship.</p></div>')
//    });


//    (function($){
//        $(window).on("load",function(){
//        $(".content").mCustomScrollbar();
//        });
//    })(jQuery);

    $('.carousel').carousel({
        pause: true,
        interval: 2000
    });
   
    
    $("#frmRT").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
            }
        },
        messages: {
            password: {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength,
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/reset_user_password",
                data: $('#frmRT').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#reset-loading').show();
                },
                success: function(data) {
                    $('#reset-loading').hide();
                    if (data.err == 0)
                    {
                        $('#myModal').modal('show');
                        window.location = HTTP_ROOT + '/user/login';
                    }
                    else
                    {
                        $("#reset-errors").show();
                        $("#reset-errors").html(data.message);
                    }
                }
            });
        }
    });    
    
    // forgot password(forgot.html) javascript start
     $("#frmRP").validate({
            rules: {    
                email: {
                    required: true,
                    mail: true,
                }
            },
            messages: {             
                email: {
                    required: JSLANGUAGE.registered_email,
                    mail: JSLANGUAGE.valid_email,
                }
            },             
            submitHandler: function(form) {
                console.log($('#frmRP').serialize());
                $.ajax({
                    url: HTTP_ROOT+'/user/forgotpassword',
                    data: $('#frmRP').serialize(),
                    type:'POST',
                    dataType: "json",
                    beforeSend:function(){
                        $('#reset-loading').show();
                    },
                    complete:function(){
                        $('#reset-loading').hide();
                    },
                    success: function(data){
                        if(data.status == 'success')
                        {
                            window.location = HTTP_ROOT+'/user/login';
                        }
                        else
                        {
                            $('#reset-errors').html(data.message);
                            return false;
                        }
                    }                  
                });                  
            }
        });
        
          // profile (profile.html) javascript start  
    $('#profile-loading').hide();
    $('#address-loading').hide();
    
    $('#update-btn').click(function(){
        $("#user-account-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5,
                },
                new_password: {
                    minlength: 6,
                },
                confirm_password: {
                    passwordExist: true,
                    equalTo: '#new_password'
                },
            },
             messages: {
                name: {
                    required: JSLANGUAGE.full_name_required,
                    minlength: JSLANGUAGE.name_atleast_5_char,
                },
                new_password: {
                    minlength:JSLANGUAGE.password_atleast_6_char,
                },
                confirm_password: {
                    passwordExist: JSLANGUAGE.valid_confirm_password,
                    equalTo: JSLANGUAGE.password_donot_match
                }
            },
            submitHandler: function(form) {
                $('#profile-loading').show();           
                $.ajax({
                    url: HTTP_ROOT+"/user/update_user_profile",
                    data: $('#user-account-form').serialize(),
                    type:'POST',
                    dataType: "json",
                    beforeSend:function(){
                        $('#profile-loading').show();
                    },
                    success:function(data){
                        $('#profile-loading').hide();
                        if(parseInt(data.err) === 0) {
                            $("#new_password").val('');
                            $("#confirm_password").val('');
                            $('#success_message').html(JSLANGUAGE.profile_updated).show();
                            $(".alert-msg").fadeTo(5000, 0).slideUp("slow", function(){
                                $('#success_message').html('').css({ opacity: 1 });
                            });        
                        } else if(parseInt(data.err) === 1) {
                            $('#profile-errors').html("Passwords do not match.");
                        } else if(parseInt(data.err) === 2) {
                            $('#profile-errors').html("Error in updating profile.");
                        }
                    }
                });             
            }            
        });
    });     
    
    jQuery.validator.addMethod("passwordExist", function(value, element) {
        if($("#new_password").val()) {
            if($("#confirm_password").val()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, 'Please enter confirm password.');

});

// this code for filter search in list page
function getParam(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : '';	
	}
	return vars;
    }

    function getsortlist(str)
    {
        var genretag = getParam('genretag');
        var sort = getParam('orderby');
        if(genretag != '') {
            genretag = genretag+",";
        }
        if(sort != '') {
            sort = "&orderby="+sort;
        }
        location.href="?genretag="+genretag+str+sort;
    }
    function sortby(sort)
    {
        var genretag = getParam('genretag');
        location.href="?genretag="+genretag+"&orderby="+sort;
    }
    function removetaglist(str)
    {
        var j;
        var genretag_array = [];
        //console.log(str);
        var genretag = getParam('genretag');
         //console.log(genretag);
        var genre_array = genretag.split(",");
        
        for (j = 0; j < genre_array.length; j++) {
            genretag_array.push(genre_array[j].replace("%20", " "));
        }
        
        var i = genretag_array.indexOf(str);
	if(i != -1) {
		genretag_array.splice(i, 1);
	}
       
       var sort = getParam('orderby');
         if(sort != '') {
            sort = "&orderby="+sort;
        }
         console.log(genretag);
       location.href="?genretag="+genretag_array+sort;
    }
// end ///

 
