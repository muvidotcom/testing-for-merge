var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

$(document).ready(function () {
    $('input').attr('autocomplete', 'false');
    $('form').attr('autocomplete', 'false');

    $('.plan-box').click(function () {
        $('.plan-box').removeClass('choosen');
        $(this).addClass('choosen');
        $('.tick-icon').hide();
        $(this).find('.tick-icon').show();

        $('#plan_id').val($(this).attr('data-id'));
        $('#currency_id').val($(this).attr('data-currency_id'));
    });

    $('#loading').hide();
});

function validatePaymentForm() {
    $("#membership_form").validate({
        rules: {
            "data[card_name]": {
                required: true,
                textonly: true
            },
            "data[card_number]": {
                required: true,
                cc: true
            },
            "data[exp_month]": {
                required: true
            },
            "data[exp_year]": {
                required: true
            },
            "data[security_code]": {
                required: true
            },
            "data[over_18]": {
                required: true
            }
        },
        messages: {
            "data[card_name]": {
                required: JSLANGUAGE.card_name_required,
                textonly: JSLANGUAGE.valid_text
            },
            "data[card_number]": {
                required: JSLANGUAGE.card_number_required,
                cc: JSLANGUAGE.card_number_required
            },
            "data[exp_month]": {
                required: JSLANGUAGE.expiry_month_required
            },
            "data[exp_year]": {
                required: JSLANGUAGE.expiry_year_required
            },
            "data[security_code]": {
                required: JSLANGUAGE.security_code_required
            },
            "data[over_18]": {
                required: JSLANGUAGE.term_privacy_policy_required
            }
        },
        submitHandler: function (form) {
            $('#membership_loading').show();
            if ($("#payment_gateway").val() !== 'manual') {
                var payment_gateway = $("#payment_gateway").val();
                        if($.trim(payment_gateway)){
                            var have_paypal_pro = 0;
                            if($('#have_paypal_pro').is(":checked")){
                                have_paypal_pro = 1;
                            }
                           var payment_gateway_str = $('#payment_gateway_str').val();
                           if(!have_paypal_pro && payment_gateway != 'paypalpro'){
                                payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                           }else if(have_paypal_pro){
                               if(payment_gateway = 'paypalpro'){
                                   payment_gateway_str = payment_gateway;
                               }else{
                                    payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                                }
                            }else{
                                payment_gateway_str = payment_gateway;
                            }
                            payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                            
                           var class_name = payment_gateway_str+'()';
                           
                           eval ("var obj = new "+class_name);
                            if (payment_gateway_str == 'paypal') {
                                document.membership_form.action = HTTP_ROOT + "/user/" + action;
                                document.membership_form.submit();
                                return false;
                            } else {
                                obj.processCard();
                            }
                        }
            } else {
                document.membership_form.action = HTTP_ROOT + "/user/" + action;
                document.membership_form.submit();
                return false;
            }
        }
    });
}

function validateSignupStep1Form() {
    $("#membership_form").validate({
        rules: {
            "data[name]": {
                required: true,
                textonly: true,
                minlength: 5
            },
            "data[email]": {
                required: true,
                email: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
            "data[confirm_password]": {
                required: true,
                equalTo: '#join_password'
            },
            "data[over_18]": {
                required: true
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                textonly: JSLANGUAGE.valid_text,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required:  JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "data[password]": {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength
            },
            "data[confirm_password]": {
                required: JSLANGUAGE.valid_confirm_password,
                equalTo: JSLANGUAGE.password_donot_match
            },
            "data[over_18]": {
                required: JSLANGUAGE.term_privacy_policy_required
            }
        },
        submitHandler: function (form) {
            //6307: Hitting Register button twice in the Registration page of Customer websites, creates duplicate registration
            //ajit@muvi.com
            $("#register_membership").prop("disabled", true);
            checkDuplicateEmail(1);
        }
    });
}

function validateUserSignupForm() {
    $("#membership_form").validate({
        rules: {
            "data[name]": {
                required: true,
                textonly: true,
                minlength: 5
            },
            "data[email]": {
                required: true,
                email: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
            "data[confirm_password]": {
                required: true,
                equalTo: '#join_password'
            },
            "data[card_name]": {
                required: true
            },
            "data[card_number]": {
                required: true,
                number: true
            },
            "data[exp_month]": {
                required: true
            },
            "data[exp_year]": {
                required: true
            },
            "data[security_code]": {
                required: true
            },
            "data[over_18]": {
                required: true
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                textonly: JSLANGUAGE.valid_text,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required:  JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "data[password]": {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength
            },
            "data[confirm_password]": {
                required: JSLANGUAGE.valid_confirm_password,
                equalTo: JSLANGUAGE.password_donot_match
            },
            "data[card_name]": {
                required: JSLANGUAGE.card_name_required
            },
            "data[card_number]": {
                required: JSLANGUAGE.card_number_required,
                number: JSLANGUAGE.card_number_required
            },
            "data[exp_month]": {
                required: JSLANGUAGE.expiry_month_required
            },
            "data[exp_year]": {
                required: JSLANGUAGE.expiry_year_required
            },
            "data[security_code]": {
                required: JSLANGUAGE.security_code_required
            },
            "data[over_18]": {
                required: JSLANGUAGE.term_privacy_policy_required
            }
        },
        submitHandler: function (form) {
            //6307: Hitting Register button twice in the Registration page of Customer websites, creates duplicate registration
            //ajit@muvi.com
            $("#register_membership").prop("disabled", true);
            //Check unique email exists or not
            checkDuplicateEmail(1);
        }
    });
}

function checkDuplicateEmail(arg) {
    var email = $.trim($('#membership_form').find("input[type='email']" ).val());
    $('#membership_loading').show();
    $('#membership_form').find('.errordiv').html('').hide();
    $.post(HTTP_ROOT + "/user/checkemail", {'email': email}, function (res) {
        if (parseInt(res.isExists) === 1) {
            $('#membership_loading').hide();
            var msg = JSLANGUAGE.email_exists_us;
            $('#errors').html(msg);
            $('#errors').show();
            $('#membership_form').find('.errordiv').html(msg).show();
            //6307: Hitting Register button twice in the Registration page of Customer websites, creates duplicate registration
            //ajit@muvi.com
            $("#register_membership").prop("disabled", false);
            return false;
        } else {
            $('#errors').html('').hide();
            $('#membership_loading').hide();
            if (parseInt(arg)) {
                if (parseInt($("#payment_gateway").length)) {
                    if ($("#payment_gateway").val() !== 'manual') {
                        var payment_gateway = $("#payment_gateway").val();
                        if($.trim(payment_gateway)){
                            var have_paypal_pro = 0;
                            if($('#have_paypal_pro').is(":checked")){
                                have_paypal_pro = 1;
                            }
                           var payment_gateway_str = $('#payment_gateway_str').val();
                           if(!have_paypal_pro && payment_gateway != 'paypalpro'){
                                payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                           }else if(have_paypal_pro){
                               if(payment_gateway = 'paypalpro'){
                                   payment_gateway_str = payment_gateway;
                               }else{
                                    payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                                }
                            }else{
                                payment_gateway_str = payment_gateway;
                            }
                            payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                           var class_name = payment_gateway_str+'()';
                           eval ("var obj = new "+class_name);
                            
                            if (payment_gateway_str == 'paypal') {
                                document.membership_form.action = HTTP_ROOT + "/user/" + action;
                                document.membership_form.submit();
                                return false;
                            } else {
                                obj.processCard();
                            }
                        }
                    }
                } else {
                    document.membership_form.action = HTTP_ROOT + "/user/" + action;
                    document.membership_form.submit();
                    return false;
                }
            }
        }
    }, 'json');
}

function getMonthList() {
    var d = new Date();
    var curyr = d.getFullYear();
    var selyr = parseInt($('#exp_year').val());
    var curmonth = d.getMonth() + 1;
    var sel_month = $.trim($("#exp_month").val());
    var startindex = 1;

    if (curyr === selyr) {
        startindex = curmonth;
    }

    var month_opt = '<option value="">'+JSLANGUAGE.expiry_month+'</option>';
    for (var i = startindex; i <= 12; i++) {
        var selected = '';
        if (i === parseInt(sel_month)) {
            selected = 'selected="selected"';
        }
        month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
    }
    $('#exp_month').html(month_opt);
}

$('#have_paypal_pro').click(function () {
    if ($(this).is(":checked")) {
        $('#card-section').hide(1500);
    } else {
        $('#card-section').show(1500);
    }
});