jQuery(document).ready(function($){  
        
         $('.plan-box').each(function(i) {
        if (i === 0) {
             $(this).find(".tick-icon").css("display","block");
        } else {
            $(this).find(".tick-icon").css("display","none");
        }
    });
    $('.close').click(function(){
    $('.avatar-wrapper').html("");
    $('.avatar-input').html("");
    });
$('.joinnow').addClass('btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold');
$('.joinnow').attr('data-animation','animated fadeInLeft');
    $('#avatar-modal').on('hidden.bs.modal', function () {
  $('.avatar-wrapper').html("");
$('#avatarInput').val("");

}); 
$('#cancel_now').addClass('btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold');
    $('.review_form h2').addClass('c-font-uppercase c-font-bold h3');
   
    $( '<div class="c-line-left"></div>' ).insertAfter(".review_form h2");
    $('.review_form p .btn').addClass('c-font-white c-theme-btn c-btn-square c-font-uppercase btn_view_trailer_inside pull-right');
  $('<div class="clearfix"></div>').insertAfter('.casting .col-md-6:nth-child(2n)');
    
    
    // jQuery sticky Menu
    
	$(".mainmenu-area").sticky({topSpacing:0});
    
    $('.product-carousel').owlCarousel({
        loop:true,
        navigation:true,
        navigationText : ['<i class="fa fa-3x fa-chevron-left"></i>','<i class="fa fa-3x fa-chevron-right"></i>'],
        margin:20,
        responsiveClass:true,
        rewindNav : false,	
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:5,
            }
        }
    });  
    
    $('.related-products-carousel').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:2,
            },
            1000:{
                items:2,
            },
            1200:{
                items:3,
            }
        }
    });  
    
    $('.brand-list').owlCarousel({
        loop:true,
        nav:true,
        margin:20,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });    
    
    $('[rel="popover"]').popover({
        container: 'body',
        html: true,
        content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
                return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });    
    
});
$(window).bind("load", function () {

        var footerHeight = 0,
                footerTop = 0,
                $footer = $(".c-layout-footer");

        positionFooter();

        function positionFooter() {

            footerHeight = $footer.height();
            footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";

            if (($(document.body).height() + footerHeight) < $(window).height()) {
                $footer.css({
                    position: "absolute",
                    width: "100%"
                }).animate({
                    top: footerTop
                })
            } else {
                $footer.css({
                    position: "static"
                })
            }

        }

        $(window)
                .scroll(positionFooter)
                .resize(positionFooter);
    });
    
function resizeCol(){
    var maxHeight = 0;    
    var parentWidth = $('.cust-row').innerWidth();
    var colWidthSum = 0;
    var colArray = [];
    
    var colWidthSum1 = 0;
    var maxHeight1 = 0;
    var colArray1 = [];
    

    $('.cust-row > div').each(function() {

        colArray1.push($(this));
        colWidthSum1 += Math.ceil($(this).outerWidth());
        var newWidth1 = colWidthSum1 + Math.ceil($(this).outerWidth());
        var currentHeight1 = $(this).find('.c-content-overlay').outerHeight();

        if (currentHeight1 > maxHeight1)
            maxHeight1 = currentHeight1;

        if (newWidth1 >= parentWidth) {
            for (var i = 0; i < colArray1.length; i++) {
                colArray1[i].find('.c-content-overlay').height(maxHeight1);
            }
            maxHeight1 = 0;
            colWidthSum1 = 0;
            colArray1 = [];
        }

        colArray.push($(this));
        colWidthSum += Math.ceil($(this).outerWidth())
        var currentHeight = $(this).outerHeight();

        if(currentHeight > maxHeight)
            maxHeight = currentHeight;
        
        if(colWidthSum >= parentWidth){
            for(var i=0; i<colArray.length; i++){
                colArray[i].height(maxHeight);
            }
            maxHeight = 0;
            colWidthSum = 0;
            colArray = [];
        }              
        
    });
}

$(window).on('load', function(){
	resizeCol();
});
$(window).on('resize', function(){
	$('.cust-row > div').css('height', '');
	resizeCol();
});    