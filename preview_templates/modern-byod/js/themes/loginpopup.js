var reload = 0;
function playMovie(obj) {
    if (typeof (obj) == "undefined") {
        var permalink = $('#content-permalink').val();
    } else {
        var permalink = $(obj).attr("data-content-permalink");
    }
    var url = HTTP_ROOT + "/player/" + permalink;
    window.location.href = url;
}
function showPpvPlans(obj) {
    var purchase_type = "show";
    var movie_id = $(obj).attr("data-movie_id");
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;

    isadv = $(obj).attr('data-isadv');
    if (typeof isadv !== typeof undefined && isadv !== false) {

    } else {
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
        isadv = 0;
    }

    var url = HTTP_ROOT + "/user/getPpvPlans";
    $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv}, function (res) {
        if (parseInt(res) === 1) {
            var permalink = $(obj).attr("data-content-permalink");
            var purl = HTTP_ROOT + "/player/"+permalink;
            window.location.href = purl;
        } else {
            reload = 1;
            $("#ppvModal").html(res).modal('show');
        }
    });
}
function getPpvPlans(obj) {
    console.log('inside');
    $('#loader').show();
    var purchase_type = "show";
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;    
    var url = HTTP_ROOT + "/user/getPpvPlans";

    if (typeof (obj) == "undefined") {
        var movie_id = $('#movie_id').val();
        var permalink = $('#content-permalink').val();
        
        if($("#isadv").length) {
            isadv = $("#isadv").val();
        }
        if($("#is_ppv_bundle").length) {
            is_ppv_bundle = $("#is_ppv_bundle").val();
        }
    } else {
        var movie_id = $(obj).attr("data-movie_id");
        var permalink = $(obj).attr("data-content-permalink");
        isadv = $(obj).attr('data-isadv');
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
    }
    var content_type = $(obj).attr('data-ctype');
    var is_instafeez = 0;
    
    if(PAYMENT_GATEWAY === 'instafeez' && parseInt(content_type) !==3){
        is_instafeez = 1;
    }
    
    var purl = HTTP_ROOT + "/player/" + permalink;
    $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type, 'instafeez':is_instafeez, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv}, function (res) {
        if (parseInt(res) === 1) {

            window.location.href = purl;
        } else {
            if ($("#loginModal").length > 0) {
                $("#loginModal").modal('hide');
                $('#loader').hide();
            }
            if(PAYMENT_GATEWAY === 'instafeez' && parseInt(content_type) !==3){
                var obj = new instafeez();
                obj.processCard(res);
            }else{
                reload = 1;
                $("#ppvModal").html(res).modal('show');
                $('#loader').hide();
            }
        }
    });
}

function episodeplayMovie(obj) {
    $('#loader').hide();
    if (typeof (obj) == "undefined") {
        var movie_id = $('#movie_id').val();
        var stream_id = $('#stream_id').val();
        var permalink = $('#content-permalink').val();
    } else {
        var movie_id = $(obj).attr("data-movie_id");
        var stream_id = $.trim($(obj).attr("data-stream_id"));
        var permalink = $(obj).attr("data-content-permalink");
    }

    if (stream_id !== '') {
        window.location.href = HTTP_ROOT + "/player/" + permalink + '/stream/' + stream_id;
    } else {
        window.location.href = HTTP_ROOT + "/player/" + permalink;
    }
}

function episodegetPpvPlans(obj) {
    var purchase_type = "episode";
    var url = HTTP_ROOT + "/user/getPpvPlans";
    var isppv = 0;
    var is_ppv_bundle = 0;
    
    if (typeof (obj) === "undefined") {
        var movie_id = $('#movie_id').val();
        var stream_id = $('#stream_id').val();
        var permalink = $('#content-permalink').val();
        if($("#is_ppv_bundle").length) {
            is_ppv_bundle = $("#is_ppv_bundle").val();
        }
    } else {
        var movie_id = $(obj).attr("data-movie_id");
        var stream_id = $(obj).attr("data-stream_id");
        var permalink = $(obj).attr("data-content-permalink");
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
    }

    $.post(url, {'movie_id': movie_id, 'stream_id': stream_id, 'purchase_type': purchase_type, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle}, function (res) {
        $('#loader').show();
        if (parseInt(res) === 1) {
            if (stream_id !== '') {
                window.location.href = HTTP_ROOT + "/player/" + permalink + '/stream/' + stream_id;
            } else {
                window.location.href = HTTP_ROOT + "/player/" + permalink;
            }
        } else {
            $('#loader').hide();
            reload = 1;
            $("#ppvModal").html(res).modal('show');
        }
    });
}
$(document).ready(function () {
    $(".playbtn, .playnowbtn").click(function () {
        $('#loader').show();
        var movie_id = $(this).attr('data-movie_id');
        var stream_id = $(this).attr('data-stream_id');
        var isppv = $(this).attr('data-isppv');
        var is_ppv_bundle = $(this).attr('data-is_ppv_bundle');
        var isadv = $(this).attr('data-isadv');
    
        if (typeof isadv !== typeof undefined && isadv !== false) {

        } else {
            isadv = 0;
        }
    
        var permalink = $(this).attr("data-content-permalink");
        var contentTypePermalink = $(this).attr("data-content-type-permalink");
       
        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loader').hide();
            $("#movie_id").val(movie_id);
            $("#stream_id").val(stream_id);
            $("#isppv").val(isppv);
            $("#is_ppv_bundle").val(is_ppv_bundle);
            $("#isadv").val(isadv);
            $('#content-permalink').val(permalink);
            $('#content-type-permalink').val(contentTypePermalink);
        });
    });

    $('#login_form').validate({
        rules: {
            "LoginForm[email]": {
                required: true,
                email: true
            },
            "LoginForm[password]": {
                required: true,
            },
        },
        messages: {
            "LoginForm[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "LoginForm[password]": {
                required: JSLANGUAGE.password_required,
            },
        },
        submitHandler: function (form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxlogin",
                data: $('#login_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function () {
                    $('#login_loading').show();
                      $('#loader_login').show();
                },
                success: function (data) {
                    $('#login_loading').hide();
                      $('#loader_login').hide();
                    if (data.login == 'success') {
                        $('#login_errors').html("");
                        if ($.trim($("#movie_id").val())) {
                            $('#loginModal').hide();
                            if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val()) || parseInt($("#isadv").val())) {
                                getPpvPlans();
                            } else {
                                playMovie();
                            }
                        } else {
                            if ($('#loginModal').length && $('#loginModal').css('display') == 'block') {
                                location.reload();
                            }
                            else {
                                if($.trim(data.action) == 'allchannels'){
                                    window.location = HTTP_ROOT + '/media/allcontents';
                                }
                                else{
                                    window.location = HTTP_ROOT + '/';
                                }
                            }
                        }
                    }else if ($.trim(data.login) == 'error_limit_login') {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();  
                    }
                    else {
                        $('#login_errors').html(JSLANGUAGE.incorrect_email_or_password);
                        $('#login_errors').show();
                    }
                },
                complete: function () {
                     
                    $('#login_loading').hide();
                }
            });
        }
    });

    $('#register_form').validate({
        rules: {
            "data[name]": {
                required: true,
                minlength: 5
            },
            "data[email]": {
                required: true,
                email: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
            "data[confirm_password]": {
                required: true,
                equalTo: "#join_password"
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "data[password]": {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength
            },
            "data[confirm_password]": {
                    required: JSLANGUAGE.valid_confirm_password,
                    equalTo: JSLANGUAGE.password_donot_match,
                },
        },
        submitHandler: function (form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxregister",
                data: $('#register_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function () {
                    $('#register_loading').show();
                    $('#loader_register').show();
                },
                success: function (data) {
                    $('#register_loading').hide();
                    $('#loader_register').hide();
                    if ($.trim(data.login) == 'success') {
                        if ($.trim($("#movie_id").val())) {
                            if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val())) {
                                getPpvPlans();
                                $('#loader_register').hide();
                                $('#register_loading').hide();
                            } else {
                                playMovie();
                                $('#register_loading').hide();
                            }
                        } else {
                            location.reload();
                        }
                    }
                    else if ($.trim(data.login) == 'error') {
                        $('#register_errors').show();
                        $('#register_errors').html(JSLANGUAGE.email_exists_us);
                    }
                    else {
                        $('#register_errors').show();
                        $('#register_errors').html(JSLANGUAGE.saving_error);
                    }
                },
                complete: function () {
                    $('#loader_register').hide();
                    $('#register_loading').hide();
                }
            });
        }
    });

    $('#login-loading').hide();
    $('.loginbtn').click(function () {
        var frm = $(this).closest('form');
        frm.validate({
            rules: {
                "LoginForm[email]": {
                    required: true,
                    email: true
                },
                "LoginForm[password]": {
                    required: true,
                },
            },
            messages: {
                "LoginForm[email]": {
                    required: JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "LoginForm[password]": {
                    required: JSLANGUAGE.password_required,
                },
            },
            submitHandler: function (form) {
                $.ajax({
                    url: HTTP_ROOT + "/user/ajaxlogin",
                    data: frm.serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function () {
                        $('#login-loading').show();
                        $('#loader_login').show();
                    },
                    success: function (data) {
                        $('#login-loading').hide();
                        $('#loader').hide();
                        if (data.login == 'success') {
                            location.reload();
                        }
                        else {
                            frm.find('.loginerror').html(JSLANGUAGE.incorrect_email_or_password).show();
                            frm.find('.loginerror').show();
                        }
                    },
                    complete: function () {
                        $('#login-loading').hide();

                    }
                });
            }
        });
    });

    $('.registerbtn').click(function () {
        var frm = $(this).closest('form');
        frm.validate({
            rules: {
                "data[name]": {
                    required: true,
                    minlength: 2
                },
                "data[email]": {
                    required: true,
                    email: true
                },
                "data[password]": {
                    required: true,
                    minlength: 6
                },
            },
            messages: {
                "data[name]": {
                    required: JSLANGUAGE.full_name_required,
                    minlength: JSLANGUAGE.name_minlength
                },
                "data[email]": {
                    required: JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "data[password]": {
                    required: JSLANGUAGE.password_required,
                    minlength: JSLANGUAGE.password_minlength
                },             
            },
            submitHandler: function (form) {
                $.ajax({
                    url: HTTP_ROOT + "/user/ajaxregister",
                    data: frm.serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function () {
                        $('#register-loading').show();
                        $('#loader_register').show();
                    },
                    success: function (data) {
                        $('#register-loading').hide();
                        $('#loader_register').hide();
                        if (data.login == 'success') {
                            if ($.trim($("#movie_id").val())) {
                                if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val())) {
                                    $('#loader_register').hide();
                                    getPpvPlans();
                                } else {
                                    $('#loader_register').hide();
                                    playMovie();
                                }
                            } else {
                                location.reload();
                            }
                        }
                        else{
                            frm.find('.registererror').html(data.message);
                            frm.find('.registererror').show();
                        }
                    },
                    complete: function () {
                        $('#loader_register').hide();
                        $('#register-loading').hide();
                    }
                });
            }
        });
    });

    $(".close").click(function () {
        $("#movie_id").val('');
        $("#stream_id").val('');
        $(".modal-backdrop").remove();
        showLogin();
    });
});

function showLogin() {
    $("#register_form_div").hide();
    $("#login_form_div").show();
}

function showRegister() {
    $("#login_form_div").hide();
    $("#register_form_div").show();
}