/*!
 * Bootstrap v3.3.4 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

function change_color(str)
{
    var template_name = str.split("_")[0];
    var template_color = str.split("_")[1];
    $.cookie('css_file', template_color, {path: '/'});
    $.cookie('template_name', template_name, {path: '/'});
    $.cookie('template_color', template_color, {path: '/'});
    if ($.cookie('is_color') == $.cookie('template_color'))
    {
        $.cookie('template_status', '1', {path: '/'});
    }
    else {
        $.cookie('template_status', '0', {path: '/'});
    }
    location.reload();

}
var color = "css/" + $.cookie("css_file");
var cssId = 'myCss';  // you could encode the css path itself to generate id..
if (!document.getElementById(cssId))
{
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.id = cssId;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = color + '.css';
    link.media = 'all';
    head.appendChild(link);
}
$(document).ready(function () {
    $('body').prepend('<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color:#222 !important;min-height:0;z-index:99999"><div class="container"><div id="navbar"><div class="navbar-form navbar-right"><label style="font-size: 14px;color: rgb(255, 255, 255);padding: 0px 5px;">Choose Color</label><select class="form-control" id="gate" onchange="change_color(this.value);" ><option value="modern_orange-purple">Orange Purple</option><option value="modern_blue-white">Blue White</option><option value="modern_red-green">Red Green</option></select><button class="btn btn-primary lunch preview" style="margin-left:5px;"> Apply Template </button></div></div></div></nav>');
    $('body').css('margin-top', '6px');
    $('#slideout').css('display', 'none');
    $('#gate').val($.cookie('template_name') + '_' + $.cookie('template_color')).prop('selected', true);
    var studio_id = $.cookie('studio_id');
    var user_id = $.cookie('user_id');
    var ipaddress = $.cookie('ipaddress');
    var template_name = $.cookie('template_name');
    var template_color = $.cookie('template_color');
    var admin_domain = $.cookie('admin_domain');
    var template_status = $.cookie('template_status');
    var draft_template = $.cookie('draft_template_name');
    var host_url = 'https://' + window.location.host;
    $('.preview').click(function (event) {
        var in_preview_theme = 1;
        $('.preview').text('Wait....');
        $('.preview').prop("disabled", true);
        $.ajax({
            type: "post",
            url: "/site/savetemplatepreview",
            data: "studio_id=" + studio_id + "&user_id=" + user_id + "&in_preview_theme=" + in_preview_theme + "&template_name=" + template_name + "&template_color=" + template_color + "&admin_domain=" + admin_domain + "&template_status=" + template_status,
            success: function (data) {
                $(".change_template").html('<div class="top_msg_bar"><div class="alert alert-success alert-dismissable flash-msg" style="margin-bottom:0;border-radius:0;"><div class="container text-center">Your site will look like this preview page</div></div></div>');
                location.href = "http://" + $.cookie('domain') + "/?preview=1";
            }
        });
    });
});
