$(function(){
    $('.menu-toggle-butn').click(function(){
        $('body').toggleClass('_nav-on');
    });
    $('body').css('paddingTop', $('header').outerHeight());
    $('body.style_3._home').css('paddingTop', 0);

    $(window).scroll(function(){
        if($(window).scrollTop() > $('header').height()){
            $('header').addClass('_scrolled');
        }else{
            $('header').removeClass('_scrolled');
        }
    })
    
    $('header .srch-butn').click(function(){
        $('header').addClass('_search-on');
        $('header .sitesearch').focus();
    })
    $('header .search-bar .close-search').click(function(){
        $('header').removeClass('_search-on');
        $('header .sitesearch').val('');
    })
    
    if (is_audio_enable == 1){
        $('a').attr('data-pjax', '#main');
        $('.playbtn').removeAttr('data-pjax');
        $('.logout').removeAttr('data-pjax');
        $('.language-list').removeAttr('data-pjax');
        getPlaylistName(sdk_user_id);
    }
    
    $('.qty-spinner button._btn').click(function(){
        var qtyFld = $(this).siblings('._fld');
        var qtyFldVal = parseInt(qtyFld.val());
        var itemID = qtyFld.data('itemid');
        if($(this).data('action') == "plus"){
            qtyFld.val(qtyFldVal+1);
            updatecart(itemID);
        }
        if($(this).data('action') == "minus"){
            if(qtyFldVal > 1){
                qtyFld.val(qtyFldVal-1);
                updatecart(itemID);
            }
        }
    });
    var currentQty = 0;
    $('.qty-spinner input._fld').focus(function(){
        currentQty =  $(this).val();
    });
    $('.qty-spinner input._fld').blur(function(){
        if(parseInt($(this).val()) <= 0){
            $(this).val(1);
        }
        if(parseInt($(this).val()) != currentQty){
            updatecart($(this).data('itemid'));
        }
    });
    
    $('#lang-popover-butn[rel="popover"]').popover({
    container: 'body',
        html: true,
        content: function () {
        var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
                return clone;
        }
    }).click(function(e) {
        e.preventDefault();
    });
    
});

$(window).on('resize', function(){
    $('body').css('paddingTop', $('header').outerHeight());
    $('body.style_3._home').css('paddingTop', 0);
})


$(window).on('scroll', function(){
    $('.popover').remove();
    $('#lang-popover-butn').removeAttr('aria-describedby');
})
 

function initSlickSlider(currentObj, columnCount){ //currently using for featured section
    if(columnCount == "2"){
        $(currentObj).not('.slick-initialized').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            customPadding: '80px',
            autoplay: false,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        infinite: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
    if(columnCount == "4"){
        $(currentObj).not('.slick-initialized').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            customPadding: '80px',
            autoplay: false,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        infinite: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
    if(columnCount == "6"){
        $(currentObj).not('.slick-initialized').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 6,
            customPadding: '80px',
            autoplay: false,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        infinite: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
}
function initSlickSliderSingle(currentObj){ 
    $(currentObj).not('.slick-initialized').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        customPadding: '80px',
        autoplay: false,
        autoplaySpeed: 3000
    });
}


function episodeContent() {
    var movie_id = $('#content_id').val();
    var movie_type = $('#episode_type').val();
    if (movie_type == '6') {
        $(".loader_episode").show();
        var action_url = HTTP_ROOT + "/content/findepisodes";
        $.ajax({
            method: "POST",
            url: action_url,
            dataType: "json",
            data: {'movie_id': movie_id, 'limit': 9999}
        }).done(function (res) {
            $('.loader_episode').hide();
            $(".tracklist-episode").html(res.msg);
            var total_track = '';
			var trackLang;
            if (res.status != 'failure') {
                total_track = $('.episode_total').val();
            }
            var date = $('.release_date').html();
            var cast = $('.cast_data').html();
            var cast_symbol = '';
            var release_symbol = '';

            if (cast) {
                cast_symbol = '<span class="symbol">&nbsp;-</span>&nbsp;&nbsp;';
            }
            if (date) {
                var release_symbol = '<span class="symbol">&nbsp;-</span>&nbsp;&nbsp;';
            }
			if(total_track == 1){
				trackLang = track;
			}else{
				trackLang = tracks;
			}
            if (total_track != '') {
                $('.symbol-release').addClass('hide');
                $('.tracks').html(cast_symbol + total_track + '&nbsp;' + trackLang + release_symbol);
            }
        });
    }
}


function showcart(id, flag,checkquant){        
    var url = HTTP_ROOT+'/shop/addtocart';
    if(typeof checkquant !== typeof undefined){
       var qnt = eval($('#product_qnt').val());
    }else{
       var qnt = 1;
    }
    var reloadUrl = HTTP_ROOT +'/shop/cart';
    if(qnt>0){
        if(flag == 2){
            $('.loader._full-page').show();
        }else{
            $('.loader._item-'+id).show();
        }
        $.post(url, {'quantity':qnt, 'id':id}, function(res){
            if(flag == 2){	
                $('.loader_cart').hide();
                $('#cart-popup-counter').html(eval($('#cart-popup-counter').html()) + 1);
                $('#cartpopup').html(res);
                window.location.href = reloadUrl;
            }else{
                $('.loader_cart').hide();
                $('html,body').animate({scrollTop:0}, 500);
                $('#cart-popup-counter').html(eval($('#cart-popup-counter').html()) + qnt);
                $('#cartpopup').html(res);
                
                $('.loader._item-'+id).hide();
            }
        })
    }else{
        alert(JSLANGUAGE.enter_quantity);
    }        
}

function removefromcart(id, cartid, qnty) {
    $('#'+cartid+' .loader').show();
    var url = HTTP_ROOT+'/shop/RemoveCart';
    $.post(url, {'id': id,'cartpopup':1}, function (res) {
        $('#'+cartid+' .loader').hide();
        $('#cart-popup-counter').html(eval($('#cart-popup-counter').html()) - eval(qnty));
        $('#cartpopup').html(res);
    });

}

function goKart(){
   window.location.href=HTTP_ROOT+"/shop/cart";
}
function soundOn(){
    $(".bar1").animate({height:'15px'}, 80, soundOn);
    $(".bar2").animate({height:'15px'}, 100);
    $(".bar3").animate({height:'15px'}, 100);
    $(".bar1").animate({height:'1px'}, 100);
    $(".bar2").animate({height:'15px'}, 100);
    $(".bar3").animate({height:'2px'}, 100);
    $(".bar1").animate({height:'3px'}, 100);
    $(".bar2").animate({height:'2px'}, 100);
    $(".bar3").animate({height:'1px'}, 100);
    $(".bar1").animate({height:'15px'}, 100);
    $(".bar2").animate({height:'2px'}, 100);
    $(".bar3").animate({height:'10px'}, 100);
    $(".bar1").animate({height:'12px'}, 100);
    $(".bar2").animate({height:'8px'}, 100);
    $(".bar3").animate({height:'12px'}, 100);
    $(".bar1").animate({height:'9px'}, 100);
    $(".bar2").animate({height:'12.5px'}, 100);
    $(".bar3").animate({height:'3px'}, 100);
}

function ConfirmFavPopup(obj) {
    $("#deleteFav").modal('show');
    var header = $(obj).attr('data-header');
    var action_btn = $(obj).attr('data-button');
    $("#deleteFav").find("#Favbodymodal").text(header);
    $("#deleteFav").find('.action_btn').addClass(action_btn);
    $("." + action_btn).attr('data-content_id', $(obj).attr('data-content_id'));
    $("." + action_btn).attr('data-content_type', $(obj).attr('data-content_type'));
    $("#deleteFav").find('.action_btn').attr('onclick','deleteFavContent(this)');
}
function deleteFavContent(obj) {
    var content_id = $(obj).attr('data-content_id');
    var content_type = $(obj).attr('data-content_type');
    var url = HTTP_ROOT + "/user/deletefromfavlist/";
    $('#favlist_loading').show();
    $('#deleteFav').modal('hide');
    var action = 0;
    if ($.trim(content_type) == "") {
         content_type = 0;
    }
    $.post(url, {'content_id': content_id, 'content_type': content_type, 'login_status': true, 'action': action}, function (res) {
        location.reload();
    });
}

function addFav(obj){
    var login_status  = $(obj).attr('data-login_status'); 
    var content_id    = $(obj).attr('data-content_id'); 
    var content_type  = $(obj).attr('data-content_type'); 
    var fav_status    = $(obj).attr('data-fav_status'); 
    if(login_status == 1){
         $('#loader_fav').show();
        if($(obj).find('.fa').hasClass('fa-heart-o')){
            $(obj).find('.fa').removeClass('fa-heart-o');
            $(obj).find('.fa').addClass('fa-heart');
            $(obj).attr('data-fav_status',0);
            $('.favourite__content').html(added_fav);
        }else{
            $(obj).find('.fa').removeClass('fa-heart');
            $(obj).find('.fa').addClass('fa-heart-o');
            $(obj).attr('data-fav_status',1);
            $('.favourite__content').html(add_fav);
        } 
        addFavContent(content_id,content_type,true, fav_status);
    }else{
        $("#loginModal").modal('show');
        var input_field = '<div id="fav_input"><input type="hidden" name="add_to_fav" id="add_to_fav" value="1" /><input type="hidden" name="content_type"  id="content_type" value="'+content_type+'" /></div>';
        $("#loginModal .popup_bottom").append(input_field);
        if(content_type == 1){
            $("#stream_id").val(content_id);
        }else{
            $("#movie_id").val(content_id);
        }
    }
}
function addFavContent(content_id,content_type,login_status,action){
    var url = HTTP_ROOT+"/user/addtofavlist/";
    $.ajax({
        url: url,
        data: {'content_id': content_id, 'content_type': content_type,'login_status':login_status,'action':action},
        type: 'POST',
        headers: {"X-PJAX":"true","X-PJAX-Container":"#main"},
        success: function (res) {
                $('#loader_fav').hide();
        }
    });
}