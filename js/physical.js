function openPGImageModal(obj){
    var jcrop_api, jcrop_api2, boundx, boundy;
}
function openPGVariantModal(obj){
    var jcrop_api, jcrop_api2, boundx, boundy;
}

function pg_clearInfo() {
    $('#pg_x1').val('');
    $('#pg_y1').val('');
    $('#pg_x2').val('');
    $('#pg_y2').val('');
    $('#pg_w').val('');
    $('#pg_h').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
function pg_var_clearInfo() {
    $('#pg_x18').val('');
    $('#pg_y18').val('');
    $('#pg_x28').val('');
    $('#pg_y28').val('');
    $('#pg_w8').val('');
    $('#pg_h8').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
function pg_clearInfoallImage() {
    $('#pg_x13').val('');
    $('#pg_y13').val('');
    $('#pg_x23').val('');
    $('#pg_y23').val('');
    $('#pg_w3').val('');
    $('#pg_h3').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
function pg_var_clearInfoallImage() {
    $('#pg_x19').val('');
    $('#pg_y19').val('');
    $('#pg_x29').val('');
    $('#pg_y29').val('');
    $('#pg_w9').val('');
    $('#pg_h9').val('');
    $('button[type="submit"]').removeAttr('disabled');
}
function pg_updateInfo(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#pg_x1').val(x);
    $('#pg_y1').val(y);
    $('#pg_x2').val(x2);
    $('#pg_y2').val(y2);
    $('#pg_w').val(e.w);
    $('#pg_h').val(e.h);
}
function pg_var_updateInfo(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#pg_x18').val(x);
    $('#pg_y18').val(y);
    $('#pg_x28').val(x2);
    $('#pg_y28').val(y2);
    $('#pg_w8').val(e.w);
    $('#pg_h8').val(e.h);
}
function pg_updateInfoallImage(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#pg_x13').val(x);
    $('#pg_y13').val(y);
    $('#pg_x23').val(x2);
    $('#pg_y23').val(y2);
    $('#pg_w3').val(e.w);
    $('#pg_h3').val(e.h);

}
function pg_var_updateInfoallImage(e) {
    var x = (e.x <= 0) ? 0 : e.x;
    var y = (e.y <= 0) ? 0 : e.y;
    var x2 = (e.x2 <= 0) ? 0 : e.x2;
    var y2 = (e.y2 <= 0) ? 0 : e.y2;
    $('#pg_x19').val(x);
    $('#pg_y19').val(y);
    $('#pg_x29').val(x2);
    $('#pg_y29').val(y2);
    $('#pg_w9').val(e.w);
    $('#pg_h9').val(e.h);

}
function pg_seepreview(obj) {
    if ($("#pg_x13").val() != "") {
        $(obj).html("Please Wait");
        $('#myPGLargeModalLabel').modal({backdrop: 'static', keyboard: false});
        pg_posterpreview(obj);
    } else {
        if ($("#pg_celeb_preview").hasClass("hide")) {
            $('#myPGLargeModalLabel').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#myPGLargeModalLabel').modal({backdrop: 'static', keyboard: false});
            if ($("#pg_x13").val() != "") {
                pg_posterpreview(obj);
            } else if ($('#pg_x1').val() != "") {
                pg_posterpreview(obj);
            } else {
                $('#myPGLargeModalLabel').modal('hide');
                $(obj).html("Next");
            }
        }
    }
    if ($("#pg_x13").val() != "") {
        if (typeof jcrop_api != 'undefined') {
            //jcrop_api = $('#pg_glry_preview').data('Jcrop');
            jcrop_api.destroy();
        }
        document.getElementById('pg_glry_preview').removeAttribute('src');
        document.getElementById('pg_glry_preview').removeAttribute('style');
    }
    if ($('#pg_x1').val() != "") {
        if (typeof jcrop_api2 != 'undefined') {
            //jcrop_api2 = $('#pg_preview').data('Jcrop');
            jcrop_api2.destroy();
        }
        document.getElementById('pg_preview').removeAttribute('src');
        document.getElementById('pg_preview').removeAttribute('style');
    }
    $('#pg_glry_preview').css("display", "none");
    $('#pg_preview').css("display", "none");
}
var tainted = false;
function pg_posterpreview(obj) {
    var canvaswidth = $("#reqwidth").val();
    var canvasheight = $("#reqheight").val();
    if ($('#pg_image_file_name').val() == '') {
        var x1 = $('#pg_x1').val();
        var y1 = $('#pg_y1').val();
        var width = $('#pg_w').val();
        var height = $('#pg_h').val();
    } else {
        var x1 = $('#pg_x13').val();
        var y1 = $('#pg_y13').val();
        var width = $('#pg_w3').val();
        var height = $('#pg_h3').val();
    }
    //document.getElementById("previewcanvas").style.display = "none";
    
    /* for direct upload */
    if ($('#pg_image_file_name').val() == '') {
        $("#previewcanvas").show();
    document.getElementById("avatar_preview_div").style.display = "none";
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);

    var img = new Image();
    img.crossOrigin = 'anonymous';
    var load_handler = function () {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;

        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        // for browsers supporting the crossOrigin attribute
        if (tainted) {
            console.log(tainted);
        } else {
            // for others
            try {
                var indexLen = $('.pg-thumb-img').length;
                    var inputFields = '<input type="hidden" name="pg_fileCrop[' + indexLen + '][x1]" value="' + $('#pg_x1').val() + '" />\n\
                           <input type="hidden" name="pg_fileCrop[' + indexLen + '][y1]" value="' + $('#pg_y1').val() + '" />\n\
                           <input type="hidden" name="pg_fileCrop[' + indexLen + '][x2]" value="' + $('#pg_x2').val() + '" />\n\
                           <input type="hidden" name="pg_fileCrop[' + indexLen + '][y2]" value="' + $('#pg_y2').val() + '" />\n\
                           <input type="hidden" name="pg_fileCrop[' + indexLen + '][w]" value="' + $('#pg_w').val() + '" />\n\
                           <input type="hidden" name="pg_fileCrop[' + indexLen + '][h]" value="' + $('#pg_h').val() + '" />';
                    var thumbHtml = '<div class="displayInline pg-thumb-img relative">\n\
                        ' + inputFields + '\n\
                        <a href="javascript:;" data-poster-id="" onclick="return removePGposter(\'\', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>\n\
                        <a href="javascript:void(0);" data-poster-id="" data-product-id="" onclick="changePgPoster(this)" class="btn upload-Image relative" style="padding: 0;">\n\
                            <img src="' + canvas.toDataURL() + '" width="100%">\n\
                        </a>\n\
                    </div>';
                    $("#add-more-pg-image").closest('.displayInline').before(thumbHtml);
                    $('#remove-poster-text').attr('data-poster-id', '');

                    var file1 = document.querySelector('#pg_celeb_pic');
                    $clone = file1.cloneNode(true)
                    $clone.removeAttribute('id');
                    $clone.removeAttribute('onchange');
                    $clone.setAttribute('name', 'pg_Filedata[' + indexLen + ']');
                    $(".pg-thumb-img:eq(" + (indexLen) + ")").append($clone);
                    $(file1).val('');
            } catch (e) {
                tainted = true;
            }
        }
    };
    var error_handler = function () {
        this.onerror = function () {
            return false
        };
        tainted = true;
        this.removeAttribute('crossorigin');
        this.src = this.src;
    };
    
    img.onload = load_handler;
    img.onerror = error_handler;
    if ($('#pg_image_file_name').val() == '') {
        img.src = $('#pg_preview').attr("src");
    } else {
        img.src = $('#pg_glry_preview').attr("src");
    }
    } else {
    
        $('#avatar_preview_div img').before('<div class="faded-area" style="display: block"><div class="loading-img">&nbsp;</div></div>');
    /* for gallery section */
    var tempHtml = '<div id="temp_preview_original_image" style="display:block; float:left; width:auto; position:absolute; right: -100%;"><img src="'+$('#pg_glry_preview').attr("src")+'" /></div>';
    $('body').append(tempHtml);
    var previewGalleryCanvas = function(div,myCallback){
        html2canvas(div, {
            proxy: HTTP_ROOT + '/protected/components/html2canvasproxy.php',
            'logging': true,
            "onrendered": function (canvas) {
                try{
                    var context = canvas.getContext('2d');
                    var tmpCanvas = document.createElement('canvas');
                    tmpCanvas.width = canvas.width;
                    tmpCanvas.height = canvas.height;
                    var context2 = canvas.getContext('2d');
                    var imageObj = new Image();

                    imageObj.onload = function () {
                        context2.drawImage(imageObj, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
                        var data = context2.getImageData(x1, y1, width, height);
                        //context.clearRect(0, 0, canvas.width, canvas.height);//clear originalCanvas
                        canvas.width = width;
                        canvas.height = height;

                        context2.putImageData(data, 0, 0);
                        this.src = canvas.toDataURL();
                        myCallback(this);
                        //context.clearRect(0, 0, width, height);//clear originalCanvas
                        //context2.clearRect(0, 0, width, height);//clear tmpCanvas
                        data = null;
                        tmpCanvas = null;
                        canvas = null;
                        imageObj = null;
                    };
                    imageObj.src = tmpCanvas.toDataURL("image/png");
                } catch (e) {
                    tainted = true;
                }
            }
        });
    };
    
    previewGalleryCanvas(document.getElementById("temp_preview_original_image"),function(imgData){
        if(window.location.href == HTTP_ROOT + '/admin/newContents'){
            Holder.addImage('', "#avatar_preview_div");
            Holder.run();
            $('#avatar_preview_div').html('<img src="'+imgData.src+'" id="preview_content_img" width="'+HOR_POSTER_WIDTH+'" height="'+HOR_POSTER_HEIGHT+'" />');
            
        } else {
            document.getElementById("preview_content_img").src = imgData.src;
        }
        $('#avatar_preview_div').show();
        $('#previewcanvas').hide();
        $('#temp_preview_original_image').remove();
        var indexLen = $('.pg-thumb-img').length;
        var inputFields = '<input type="hidden" name="pg_image_file_names[' + indexLen + ']" value="' + $('#pg_image_file_name').val() + '" />\n\
                            <input type="hidden" name="pg_original_images[' + indexLen + ']" value="' + $('#pg_original_image').val() + '" />\n\
                            <input type="hidden" name="pg_imageCrop[' + indexLen + '][x13]" value="' + $('#pg_x13').val() + '" />\n\
                           <input type="hidden" name="pg_imageCrop[' + indexLen + '][y13]" value="' + $('#pg_y13').val() + '" />\n\
                           <input type="hidden" name="pg_imageCrop[' + indexLen + '][x23]" value="' + $('#pg_x23').val() + '" />\n\
                           <input type="hidden" name="pg_imageCrop[' + indexLen + '][y23]" value="' + $('#pg_y23').val() + '" />\n\
                           <input type="hidden" name="pg_imageCrop[' + indexLen + '][w3]" value="' + $('#pg_w3').val() + '" />\n\
                           <input type="hidden" name="pg_imageCrop[' + indexLen + '][h3]" value="' + $('#pg_h3').val() + '" />';


        var thumbHtml = '<div class="displayInline pg-thumb-img relative">\n\
                        ' + inputFields + '\n\
                        <a href="javascript:;" data-poster-id="" onclick="return removePGposter(\'\', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>\n\
                        <a href="javascript:void(0);" data-poster-id="" data-product-id="" onclick="changePgPoster(this)" class="btn upload-Image relative" style="padding: 0;">\n\
                            <img src="' + imgData.src + '" width="100%">\n\
                        </a>\n\
                    </div>';
        $("#add-more-pg-image").closest('.displayInline').before(thumbHtml);
        $('#remove-poster-text').attr('data-poster-id', '');
        
        $('#avatar_preview_div img').on('load', function(){
            $('#avatar_preview_div .faded-area').remove();
        });
        
    });
    }
    $('#myPGLargeModalLabel').modal('hide');
    $(obj).html("Next");
}

function pg_click_browse(file_name) {
    $("#" + file_name).click();
}

function pg_fileSelectHandler() {
    $('#pg_celeb_preview').css("display", "block");
    $("#pg_image_file_name").val('');
    $("#pg_original_image").val('');
    //$("#pg_glry_preview").addClass("hide");
    $('#pg_glry_preview').css("display", "none");
    pg_clearInfo();
    //pg_clearInfoallImage();
    $(".jcrop-keymgr").css("display", "none");
    $("#pg_editceleb_preview").hide();
    $("#pg_celeb_preview").removeClass("hide");
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    var oFile = $('#pg_celeb_pic')[0].files[0];
    var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#pg_celeb_preview").addClass("hide");
        document.getElementById("pg_celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        $("#pg_celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('File names with symbols such as \' , - are not supported');
        return;
    }
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (!rFilter.test(oFile.type)) {
        $("#pg_celeb_preview").addClass("hide");
        document.getElementById("pg_celeb_pic").value = "";

        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);

        if (width < reqwidth || height < reqheight) {
            $("#pg_celeb_preview").addClass("hide");
            document.getElementById("pg_celeb_pic").value = "";
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);

            return;
        }
        var oImage = document.getElementById('pg_preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () {
                if (typeof jcrop_api2 != 'undefined') {
                    jcrop_api2.destroy();
                    jcrop_api2 = null;
                    $('#pg_preview').width(oImage.naturalWidth);
                    $('#pg_preview').height(oImage.naturalHeight);
                    $('#pg_glry_preview').width("450");
                    $('#pg_glry_preview').height("250");
                }
                $('#pg_preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: pg_updateInfo,
                    onSelect: pg_updateInfo,
                    onRelease: pg_clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api2 = this;
                    jcrop_api2.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    }
}

function pg_showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('#myPGLargeModalLabel button,input[type="text"]').attr('disabled', 'disabled');
        $('#profile button').attr('disabled', 'disabled');

    } else {
        $('.loaderDiv').hide();
        $('#myPGLargeModalLabel button,input[type="text"]').removeAttr('disabled');
        $('#profile button').removeAttr('disabled');
    }
}

function pg_toggle_preview(id, img_src, name_of_image) {

    $('#pg_gallery_preview').css("display", "block");
    $("#pg_celeb_preview").addClass("hide");
    pg_clearInfo();
    //pg_clearInfoallImage();
    $('#pg_gallery_preview').removeClass("hide");
    document.getElementById("pg_celeb_pic").value = "";
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    pg_showLoader();
    var image_file_name = name_of_image;
    var image_src = img_src;
    $("#pg_image_file_name").val(image_file_name);
    $("#pg_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < reqwidth || height < reqheight) {
            pg_showLoader(1);
            $("#pg_gallery_preview").addClass("hide");
            $("#pg_image_file_name").val("");
            $("#pg_original_image").val("");
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
            return;
        }
        var oImage = document.getElementById('pg_glry_preview');
        pg_showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#pg_glry_preview').width(oImage.naturalWidth);
                $('#pg_glry_preview').height(oImage.naturalHeight);
                $('#pg_preview').width("800");
                $('#pg_preview').height("300");
            }
            $('#pg_glry_preview').Jcrop({
                minSize: [reqwidth, reqheight],
                aspectRatio: aspectRatio,
                boxWidth: 400,
                boxHeight: 150,
                bgFade: true,
                bgOpacity: .3,
                onChange: pg_updateInfoallImage,
                onSelect: pg_updateInfoallImage,
                onRelease: pg_clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
            });
        };
    };
}

function changePgPoster(obj, size) {
    if(!size){
        var size = 'standard';
    }
    /* frontend */
    $('#avatar_preview_div .faded-area').show();
    $('#avatar_preview_div img').on('load', function(){
        $('#avatar_preview_div .faded-area').hide();
    });
    /* frontend */
    
    $('#previewcanvas').css('display','none');
    var product_id = $(obj).attr('data-product-id');
    var poster_id = $(obj).attr('data-poster-id');
    var poster = $(obj).find('img').attr('src');
    $('#remove-poster-text').attr('data-poster-id', poster_id);
    var bigPoster = poster.replace("thumb/", size + "/");
    $('#avatar_preview_div').find('img').attr('src', bigPoster);
    var index = $( ".pg-thumb-img" ).index( obj );
    $('.pg-thumb-img img.active').removeClass('active');
    $(obj).children('img').addClass('active');
    
    /* frontend */
    $('.lslide.active').removeClass('active');
    $(obj).closest('.lslide').addClass('active');
    /* frontend */
    $('#avatar_preview_div').show();
}

function removePGposter(product_id, obj) {
    var poster_id = $(obj).attr('data-poster-id');
    swal({
        title: "Remove Poster?",
        text: "Are you sure to remove this poster?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        if (poster_id) {
            var url = HTTP_ROOT + "/admin/removepgposter";
            $('#remove-poster-text').text('Removing...');
            $('#remove-poster-text').attr('disabled', 'disabled');
            $.post(url, {'poster_id': poster_id, 'product_id': product_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#remove-poster-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting poster.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Poster removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('');
                    $('#avatar_preview_div').children('img').remove();
                    window.location.reload(1);
                }
            }, 'json');
        } else {
            $(obj).closest('.pg-thumb-img').remove();
        }
    });

}

//pg variant js 
function pg_variant_seepreview(obj) {
    if ($("#pg_x19").val() != "") {
        $(obj).html("Please Wait");
        $('#myPGLargeVariantModal').modal({backdrop: 'static', keyboard: false});
        pg_var_posterpreview(obj);
    } else {
        if ($("#pg_var_celeb_preview").hasClass("hide")) {
            $('#myPGLargeVariantModal').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#myPGLargeVariantModal').modal({backdrop: 'static', keyboard: false});
            if ($("#pg_x19").val() != "") {
                pg_var_posterpreview(obj);
            } else if ($('#pg_x18').val() != "") {
                pg_var_posterpreview(obj);
            } else {
                $('#myPGLargeVariantModal').modal('hide');
                $(obj).html("Next");
            }
        }
    }
    if ($("#pg_x19").val() != "") {
        if (typeof jcrop_api != 'undefined') {
            //jcrop_api = $('#pg_glry_preview').data('Jcrop');
            jcrop_api.destroy();
        }
        document.getElementById('pg_var_glry_preview').removeAttribute('src');
        document.getElementById('pg_var_glry_preview').removeAttribute('style');
    }
    if ($('#pg_x18').val() != "") {
        if (typeof jcrop_api2 != 'undefined') {
            //jcrop_api2 = $('#pg_preview').data('Jcrop');
            jcrop_api2.destroy();
        }
        document.getElementById('pg_var_preview').removeAttribute('src');
        document.getElementById('pg_var_preview').removeAttribute('style');
    }
    $('#pg_var_glry_preview').css("display", "none");
    $('#pg_var_preview').css("display", "none");
}

var tainted = false;
function pg_var_posterpreview(obj) {
    var canvaswidth = $("#reqwidth").val();
    var canvasheight = $("#reqheight").val();
    if ($('#pg_var_image_file_name').val() == '') {
        var x1 = $('#pg_x18').val();
        var y1 = $('#pg_y18').val();
        var width = $('#pg_w8').val();
        var height = $('#pg_h8').val();
    } else {
        var x1 = $('#pg_x19').val();
        var y1 = $('#pg_y19').val();
        var width = $('#pg_w9').val();
        var height = $('#pg_h9').val();
    }
    //document.getElementById("previewcanvas").style.display = "none";
    
    /* for direct upload */
    if ($('#pg_var_image_file_name').val() == '') {
        $("#previewvariantcanvas").show();
        document.getElementById("avatar_var_preview_div").style.display = "none";
        var canvas = $("#previewvariantcanvas")[0];
        var context = canvas.getContext('2d');
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.clearRect(0, 0, canvas.width, canvas.height);

        var img = new Image();
        img.crossOrigin = 'anonymous';
        var load_handler = function () {
            canvas.height = canvasheight;
            canvas.width = canvaswidth;

            context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
            // for browsers supporting the crossOrigin attribute
            if (tainted) {
                console.log(tainted);
            } else {
                // for others
                try {
                    var varindexLen = $('.pg-thumb-img').length;
                        var inputFields = '<input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][x18]" value="' + $('#pg_x18').val() + '" />\n\
                               <input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][y18]" value="' + $('#pg_y18').val() + '" />\n\
                               <input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][x28]" value="' + $('#pg_x28').val() + '" />\n\
                               <input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][y28]" value="' + $('#pg_y28').val() + '" />\n\
                               <input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][w8]" value="' + $('#pg_w8').val() + '" />\n\
                               <input type="hidden" name="pg_var_fileCrop[' + varindexLen + '][h8]" value="' + $('#pg_h8').val() + '" />';
                        var thumbHtml = '<div class="displayInline pg-thumb-img relative">\n\
                            ' + inputFields + '\n\
                            <a href="javascript:;" data-poster-id="" onclick="return removePGVariantposter(\'\', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>\n\
                            <a href="javascript:void(0);" data-poster-id="" data-product-id="" onclick="changePGVariantPoster(this)" class="btn upload-Image relative" style="padding: 0;">\n\
                                <img src="' + canvas.toDataURL() + '" width="100%">\n\
                            </a>\n\
                        </div>';
                        $("#add-more-pg-var-image").closest('.displayInline').before(thumbHtml);
                        $('#remove-poster-text').attr('data-poster-id', '');

                        var file1 = document.querySelector('#pg_vceleb_pic');
                        $clone = file1.cloneNode(true)
                        $clone.removeAttribute('id');
                        $clone.removeAttribute('onchange');
                        $clone.setAttribute('name', 'pg_var_Filedata[' + varindexLen + ']');
                        //console.log($clone);
                        $(".pg-thumb-img:eq(" + (varindexLen) + ")").append($clone);
                        $(file1).val('');
                } catch (e) {
                    tainted = true;
                }
            }
        };
        var error_handler = function () {
            this.onerror = function () {
                return false
            };
            tainted = true;
            this.removeAttribute('crossorigin');
            this.src = this.src;
        };

        img.onload = load_handler;
        img.onerror = error_handler;
        if ($('#pg_var_image_file_name').val() == '') {
            img.src = $('#pg_var_preview').attr("src");
        } else {
            img.src = $('#pg_var_glry_preview').attr("src");
        }
    } else {
        
        $('#avatar_var_preview_div img').before('<div class="faded-area" style="display: block"><div class="loading-img">&nbsp;</div></div>');
    /* for gallery section */
    var tempHtml = '<div id="temp_preview_original_image" style="display:block; float:left; width:auto; position:absolute; right: -100%;"><img src="'+$('#pg_var_glry_preview').attr("src")+'" /></div>';
    $('body').append(tempHtml);
    var previewGalleryCanvas = function(div,myCallback){
        html2canvas(div, {
            proxy: HTTP_ROOT + '/protected/components/html2canvasproxy.php',
            'logging': true,
            "onrendered": function (canvas) {
                try{
                    var context = canvas.getContext('2d');
                    var tmpCanvas = document.createElement('canvas');
                    tmpCanvas.width = canvas.width;
                    tmpCanvas.height = canvas.height;
                    var context2 = canvas.getContext('2d');
                    var imageObj = new Image();

                    imageObj.onload = function () {
                        context2.drawImage(imageObj, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
                        var data = context2.getImageData(x1, y1, width, height);
                        //context.clearRect(0, 0, canvas.width, canvas.height);//clear originalCanvas
                        canvas.width = width;
                        canvas.height = height;

                        context2.putImageData(data, 0, 0);
                        this.src = canvas.toDataURL();
                        myCallback(this);
                        //context.clearRect(0, 0, width, height);//clear originalCanvas
                        //context2.clearRect(0, 0, width, height);//clear tmpCanvas
                        data = null;
                        tmpCanvas = null;
                        canvas = null;
                        imageObj = null;
                    };
                    imageObj.src = tmpCanvas.toDataURL("image/png");
                } catch (e) {
                    tainted = true;
                }
            }
        });
    };  
    
    previewGalleryCanvas(document.getElementById("temp_preview_original_image"),function(imgData){
        if(window.location.href == HTTP_ROOT + '/admin/newContents'){
            Holder.addImage('', "#avatar_var_preview_div");
            Holder.run();
            $('#avatar_var_preview_div').html('<img src="'+imgData.src+'" id="preview_var_content_img" width="'+HOR_POSTER_WIDTH+'" height="'+HOR_POSTER_HEIGHT+'" />');
            
        } else {
            document.getElementById("preview_var_content_img").src = imgData.src;
        }
        $('#avatar_var_preview_div').show();
        $('#previewvariantcanvas').hide();
        $('#temp_preview_original_image').remove();
        var varindexLen = $('.pg-thumb-img').length;
        var inputFields = '<input type="hidden" name="pg_var_image_file_names[' + varindexLen + ']" value="' + $('#pg_var_image_file_name').val() + '" />\n\
                            <input type="hidden" name="pg_var_original_images[' + varindexLen + ']" value="' + $('#pg_var_original_image').val() + '" />\n\
                            <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][x19]" value="' + $('#pg_x19').val() + '" />\n\
                           <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][y19]" value="' + $('#pg_y19').val() + '" />\n\
                           <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][x29]" value="' + $('#pg_x29').val() + '" />\n\
                           <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][y29]" value="' + $('#pg_y29').val() + '" />\n\
                           <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][w9]" value="' + $('#pg_w9').val() + '" />\n\
                           <input type="hidden" name="pg_var_imageCrop[' + varindexLen + '][h9]" value="' + $('#pg_h9').val() + '" />';


        var thumbHtml = '<div class="displayInline pg-thumb-img relative">\n\
                        ' + inputFields + '\n\
                        <a href="javascript:;" data-poster-id="" onclick="return removePGVariantposter(\'\', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>\n\
                        <a href="javascript:void(0);" data-poster-id="" data-product-id="" onclick="changePGVariantPoster(this)" class="btn upload-Image relative" style="padding: 0;">\n\
                            <img src="' + imgData.src + '" width="100%">\n\
                        </a>\n\
                    </div>';
        $("#add-more-pg-var-image").closest('.displayInline').before(thumbHtml);
        $('#remove-poster-text').attr('data-poster-id', '');
        
        $('#avatar_var_preview_div img').on('load', function(){
            $('#avatar_var_preview_div .faded-area').remove();
        });
        
    });
    }
    $('#myPGLargeVariantModal').modal('hide');
    $(obj).html("Next");
}


function pg_variant_click_browse(file_name) {
    $("#" + file_name).click();
}

function pg_variant_fileSelectHandler() {
    $('#pg_var_celeb_preview').css("display", "block");
    $("#pg_var_image_file_name").val('');
    $("#pg_var_original_image").val('');
    //$("#pg_glry_preview").addClass("hide");
    $('#pg_var_glry_preview').css("display", "none");
    pg_var_clearInfo();
    //pg_clearInfoallImage();
    $(".jcrop-keymgr").css("display", "none");
    $("#pg_var_editceleb_preview").hide();
    $("#pg_var_celeb_preview").removeClass("hide");
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    var oFile = $('#pg_vceleb_pic')[0].files[0];
    var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#pg_var_celeb_preview").addClass("hide");
        document.getElementById("pg_vceleb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        $("#pg_var_celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        swal('File names with symbols such as \' , - are not supported');
        return;
    }
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (!rFilter.test(oFile.type)) {
        $("#pg_var_celeb_preview").addClass("hide");
        document.getElementById("pg_vceleb_pic").value = "";

        swal('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);

        if (width < reqwidth || height < reqheight) {
            $("#pg_var_celeb_preview").addClass("hide");
            document.getElementById("pg_vceleb_pic").value = "";
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);

            return;
        }
        var oImage = document.getElementById('pg_var_preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () {
                if (typeof jcrop_api2 != 'undefined') {
                    jcrop_api2.destroy();
                    jcrop_api2 = null;
                    $('#pg_var_preview').width(oImage.naturalWidth);
                    $('#pg_var_preview').height(oImage.naturalHeight);
                    $('#pg_var_glry_preview').width("450");
                    $('#pg_var_glry_preview').height("250");
                }
                $('#pg_var_preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: pg_var_updateInfo,
                    onSelect: pg_var_updateInfo,
                    onRelease: pg_var_clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api2 = this;
                    jcrop_api2.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    }
}

function pg_var_showLoader(isShow) { 
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('#myPGLargeVariantModal button,input[type="text"]').attr('disabled', 'disabled');
        $('#profile button').attr('disabled', 'disabled');

    } else {
        $('.loaderDiv').hide();
        $('#myPGLargeVariantModal button,input[type="text"]').removeAttr('disabled');
        $('#profile button').removeAttr('disabled');
    }
}

function pg_variant_toggle_preview(id, img_src, name_of_image) {

    $('#pg_var_gallery_preview').css("display", "block");
    $("#pg_var_celeb_preview").addClass("hide");
    pg_var_clearInfo();
    //pg_clearInfoallImage();
    $('#pg_var_gallery_preview').removeClass("hide");
    document.getElementById("pg_vceleb_pic").value = "";
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    pg_var_showLoader();
    var image_file_name = name_of_image;
    var image_src = img_src;
    $("#pg_var_image_file_name").val(image_file_name);
    $("#pg_var_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < reqwidth || height < reqheight) {
            pg_var_showLoader(1);
            $("#pg_var_gallery_preview").addClass("hide");
            $("#pg_var_image_file_name").val("");
            $("#pg_var_original_image").val("");
            swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
            return;
        }
        var oImage = document.getElementById('pg_var_glry_preview');
        pg_var_showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#pg_var_glry_preview').width(oImage.naturalWidth);
                $('#pg_var_glry_preview').height(oImage.naturalHeight);
                $('#pg_var_preview').width("800");
                $('#pg_var_preview').height("300");
            }
            $('#pg_var_glry_preview').Jcrop({
                minSize: [reqwidth, reqheight],
                aspectRatio: aspectRatio,
                boxWidth: 400,
                boxHeight: 150,
                bgFade: true,
                bgOpacity: .3,
                onChange: pg_var_updateInfoallImage,
                onSelect: pg_var_updateInfoallImage,
                onRelease: pg_var_clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
            });
        };
    };
}

function changePGVariantPoster(obj, size) {
    if(!size){
        var size = 'standard';
    }
    /* frontend */
    $('#avatar_var_preview_div .faded-area').show();
    $('#avatar_var_preview_div img').on('load', function(){
        $('#avatar_var_preview_div .faded-area').hide();
    });
    /* frontend */
    
    $('#previewvariantcanvas').css('display','none');
    var product_id = $(obj).attr('data-product-id');
    var poster_id = $(obj).attr('data-poster-id');
    var poster = $(obj).find('img').attr('src');
    $('#remove-poster-text').attr('data-poster-id', poster_id);
    var bigPoster = poster.replace("thumb/", size + "/");
    $('#avatar_var_preview_div').find('img').attr('src', bigPoster);
    var index = $( ".pg-thumb-img" ).index( obj );
    $('.pg-thumb-img img.active').removeClass('active');
    $(obj).children('img').addClass('active');
    
    /* frontend */
    $('.lslide.active').removeClass('active');
    $(obj).closest('.lslide').addClass('active');
    /* frontend */
    $('#avatar_var_preview_div').show();
}

function removePGVariantposter(product_id, obj) {
    var poster_id = $(obj).attr('data-poster-id');
    swal({
        title: "Remove Poster?",
        text: "Are you sure to remove this poster?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        if (poster_id) {
            var url = HTTP_ROOT + "/admin/removepgposter";
            $('#remove-poster-text').text('Removing...');
            $('#remove-poster-text').attr('disabled', 'disabled');
            $.post(url, {'poster_id': poster_id, 'product_id': product_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#remove-poster-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>&nbsp;Error in deleting poster.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>&nbsp;Poster removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('');
                    //window.location.reload(1);                   
                    $(obj).closest('.pg-thumb-img').remove();   
                }
            }, 'json');
        } else {
            $(obj).closest('.pg-thumb-img').remove();
        }
    });
//        $('#myvariantmodal').on('hidden.bs.modal', function () {
//            location.reload();
//       });      

}

function changeAllPgPoster(obj, size) {        
    if(!size){
        var size = 'standard';
    }
    /* frontend */
    var defval = $(obj).attr('data-variant-id');  
    $('#avatar_preview_div_'+defval+' .faded-area').show();
    $('#avatar_preview_div_'+defval+' img').on('load', function(){
        $('#avatar_preview_div_'+defval+' .faded-area').hide();
    });
    /* frontend */
    
    $('#previewcanvas').css('display','none');
    var product_id = $(obj).attr('data-product-id');
    var poster_id = $(obj).attr('data-poster-id');
    var poster = $(obj).find('img').attr('src');
    $('#remove-poster-text').attr('data-poster-id', poster_id);
    var bigPoster = poster.replace("thumb/", size + "/");
    $('#avatar_preview_div_'+defval).find('img').attr('src', bigPoster);
    var index = $( ".pg-thumb-img" ).index( obj );
    $('.pg-thumb-img img.active').removeClass('active');
    $(obj).children('img').addClass('active');
    
    /* frontend */
    $('.lslide.active').removeClass('active');
    $(obj).closest('.lslide').addClass('active');
    /* frontend */
    $('#avatar_preview_div_'+defval).show();
}