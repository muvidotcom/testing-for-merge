$(document).ready(function () {
    player.ready(function () {
        //Back button Show and hide
        $('.videocontent').mouseover(function() {
                $('#backButton').show();				
        });
        $('.videocontent').mouseout(function() {
                $('#backButton').hide();				
        });
        // BACK BUTTON OF VIDEO PLAYER DOESN'T HIDE IN FULLSCREEN MODE.  
        setInterval(function() {
            var user_active = player.userActive();
            if(user_active === true){
                $('#backButton').show();
            } else {
                $('#backButton').hide();						
            }
        }, 1000);
        if(is_mobile === 1){
            //Back button secction
            $('#backButton').bind('touchstart', function(){								
                    backButtonClick();
            });
        } else {					
            //Back button secction
            $('#backButton').click(function(){								
                    backButtonClick();
            });
        }
        //BackButton Click
        function backButtonClick(){
            if($('#backbtnlink').val() !='')
                    window.location.href = $('#backbtnlink').val();
            else
                    parent.history.back();
            return false;
        }
    });
});


