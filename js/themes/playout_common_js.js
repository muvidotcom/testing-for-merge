$(document).ready(function () {
      var current_program='';
      var programs_to_start='';
      var firsttimeinslide='';
      var start_time='';
      var end_time='';
     $(".vjs-tvguide-button").css("display","none");   
     $("#program-lists").mCustomScrollbar({
          scrollButtons:{
              enable:true
          },
          scrollInertia:0,
          advanced:{
           autoScrollOnFocus: false
          },
      });
      $('#player-loading').show();
      $('#poster-loading').show();
      $('#program-loading').show();
      var action_url = HTTP_ROOT + '/tvguide/getData';
      var action_url_channel_list = HTTP_ROOT + '/tvguide/Gettimewiseguide';
      $.ajax({
          url: action_url,
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var dataString = "current_program=" + current_program + "programs_to_start=" + programs_to_start + "nowtime=" + data.nowtime + "start_time=" + data.start_time + "end_time=" + data.end_time;
              $.ajax({
                  url: action_url_channel_list,
                  type: "POST",
                  data: dataString,
                  success: function (data) {
                      //alert(data);
                      $('#program-loading').hide();
                      if(data!= ''){
                          //if(current_program=='' && programs_to_start=='' && firsttimeinslide=='' && start_time=='' && end_time==''){ 
                          if(current_program=='' && programs_to_start=='' && firsttimeinslide=='' && data.start_time=='' && data.end_time==''){
                               $("#program-lists .mCSB_container").html(data);
                              noprogram();              
                          }else{
                              $("#program-lists .mCSB_container").html(data);
                              //$("#program-lists").mCustomScrollbar("update");                           
                              $('.noprogram:first-child').trigger('click');
                              $('#cursor_pointer').click(); 
                          }    
                      }else{
                          $('#player-loading').hide();
                          $('#poster-loading').hide();
                      }
                  }
              });
          },
          complete: function () { 
                      $("#program-lists").mCustomScrollbar({
                          theme: "dark-thick",
                          scrollButtons: {
                              enable: true
                          }
                      }); 
                  }
      });
      setInterval(function(){
          if($("#video_block1").attr("poster")){
             console.log("#####1",current_program,programs_to_start,firsttimeinslide,start_time,end_time);
              loadtimeguide(current_program,programs_to_start,firsttimeinslide,start_time,end_time); // this will run after every 1 minute
             console.log("#####3",current_program,programs_to_start,firsttimeinslide,start_time,end_time);
          }
      }, 30000);
      setInterval(function(){
          var action_url = HTTP_ROOT + '/tvguide/getCurrentTime';
          $.ajax({
              url: action_url,
              type: "POST",
              success: function (data) {
                  $("#day-date").html(data);
              }
          }); 
      }, 30000);
      $('#program-lists').on("click",".cursor_pointer",function(){
          $('.cursor_pointer').removeClass('green_bg');
          $(this).addClass('green_bg');
          var current_movie = $(this).attr('id');
          $("#current_program").val(current_movie);
      });
      $('#program-lists').on("click",".cursor_pointer_program_tostart",function(){
          $('.cursor_pointer_program_tostart').removeClass('light-bg');
          $(this).addClass('light-bg');
          var movie_to_start = $(this).attr('id');
          $("#programs_to_start").val(movie_to_start);
      });
  });
  function loadtimeguide(current_program,programs_to_start,firsttimeinslide,start_time,end_time){
      var action_url = HTTP_ROOT + '/tvguide/getData';
      var dataString = "current_program="+current_program+"programs_to_start="+programs_to_start+"nowtime=" +firsttimeinslide+ "start_time="+start_time+ "end_time="+end_time;
      $.ajax({
          url: action_url,
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $("#timeguide").html(data.time_slide); 
              $(".left").attr('data-id','before_'+data.left_right);
              $(".right").attr('data-id','after_'+data.left_right);
              var dataString = "current_program=" + current_program + "programs_to_start=" + programs_to_start + "nowtime=" + data.nowtime + "start_time=" + data.start_time + "end_time=" + data.end_time;
              $.ajax({
                  url: HTTP_ROOT + '/tvguide/Gettimewiseguide',
                  type: "POST",
                  data: dataString,
                  success: function (data) {
                      console.log("data",data);
                      $('#program-loading').hide();
                      if(data!= ''){
                          if(current_program=='' && programs_to_start=='' && firsttimeinslide=='' && data.start_time=='' && data.end_time==''){ 
                              $("#program-lists .mCSB_container").html(data);
                              noprogram(); 
                          } else {
                              $("#program-lists .mCSB_container").html(data);
                              //$("#program-lists").mCustomScrollbar("update");                           
                              $('.noprogram:first-child').trigger('click');
                              $('#cursor_pointer').click(); 
                          }
                      }else{
                          $('#player-loading').hide();
                          $('#poster-loading').hide();
                      }
                  }
              });
          }
      }); 
  }
