/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $(".frmBlog").find('.loader').hide();
    $('.blog-comment-submit').click(function(){
        var blogcommentfrm = $(this).closest('form');
        blogcommentfrm.validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 2
                },
                comment: {
                    required: true,
                    minlength: 2
                },
            }, messages: {
                fname: {
                    required: JSLANGUAGE.full_name_required,
                    minlength: JSLANGUAGE.atleast_2_char,
                },
                comment: {
                    required: JSLANGUAGE.please_enter_your_comment,
                    minlength: JSLANGUAGE.atleast_2_char,
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    url: HTTP_ROOT+"/blog/savecomment",
                    data: $(blogcommentfrm).serialize(),
                    dataType: 'json',
                    method: 'post',
                    beforeSend: function() {
                        $(blogcommentfrm).find('.loader').show();
                    },
                    success: function(result) {
                        if (result.status == 'success')
                        {
                            location.reload();
                        }
                        else
                        {
                            $(blogcommentfrm).find('.loader').hide();
                            if ($("#loginModal").length > 0) {
                                $("#loginModal").modal('show');
                            }
                            return false;
                        }
                    }
                });
            }
        });        
    });
});

