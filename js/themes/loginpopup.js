var reload = 0;
function playMovie(obj) {
    if (typeof (obj) == "undefined") {
        var permalink = $('#content-permalink').val();
        var stream_id = $('#stream_id').val();
        if (stream_id !== '') {
            permalink =  permalink + '/stream/' + stream_id;
        }
    } else {
        var permalink = $(obj).attr("data-content-permalink");
    }
    var url = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
    window.location.href = url;
}
function download_content(obj) {
    if (typeof (obj) == "undefined") {
        var permalink = $('#content-permalink').val();
    } else {
        var permalink = $(obj).attr("data-content-permalink");
    }
    var url = HTTP_ROOT + "/user/DownloadContent/vlink/" + permalink;
    window.location.href = url;
}
function showPpvPlans(obj, onCloseNotRefresh) {
    if(typeof (onCloseNotRefresh) === "undefined"){
        onCloseNotRefresh = 0;
    }else{
        onCloseNotRefresh = 1;
    }
    var purchase_type = "show";
    var movie_id = $(obj).attr("data-movie_id");
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;

    isadv = $(obj).attr('data-isadv');
    if (typeof isadv !== typeof undefined && isadv !== false) {

    } else {
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
        isadv = 0;
    }

    var url = HTTP_ROOT + "/user/getPpvPlans";
    $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv,'onCloseNotRefresh':onCloseNotRefresh}, function (res) {
        if (parseInt(res) === 1) {
            var permalink = $(obj).attr("data-content-permalink");
            var purl = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
            window.location.href = purl;
        } else {
            reload = 1;
            $("#ppvModal").html(res).modal('hide');
            $('#loader').hide();

            if (parseInt(is_plan_error)) {
                $("#ppvModal").html(res).modal('show');
                $('#loader-ppv').hide();
            }
        }
    });
}

function getVoucherGeneralInfoGuest(obj, onCloseNotRefresh){
   if (typeof (onCloseNotRefresh) === "undefined") {
        onCloseNotRefresh = 0;
    } else {
        onCloseNotRefresh = 1;
    }
    $('#loader').show();
    var purchase_type = "show";
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;
    var movie_id;
    var season;
    var permalink;
    var url = HTTP_ROOT + "/user/GetVoucherDownloadContentGuest";
        
    movie_id = $(obj).attr("data-movie_id");
    permalink = $(obj).attr("data-content-permalink");
    isadv = $(obj).attr('data-isadv');
    isppv = $(obj).attr('data-isppv');
    is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
    purchase_type = $(obj).attr('data-purchase_type');
    if (typeof purchase_type !== typeof undefined && purchase_type !== false) {
        if (purchase_type === "season" && $("#series").length) {
            season = $("#series").val();
            if (parseInt(season)) {
                permalink += "/season/" + season;
            }
        }
    }
    var purl = HTTP_ROOT + "/user/DownloadContent/vlink/" + permalink;
    $.post(url, {'movie_id': movie_id, 'season': season, 'purchase_type': purchase_type,'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv, 'onCloseNotRefresh': 1}, function (res) {
        if (parseInt(res) === 1) {
            if ($("#generalInfoModal").length > 0) { 
                $("#generalInfoModal").modal('hide');
                $('#loader').hide();
            } 
            window.location.href = purl;
        } else {
            if ($("#generalInfoModal").length > 0) { 
                $("#generalInfoModal").modal('hide');
                $('#loader').hide();
            } 
            $("#ppvModal").html(res).modal('show');
            $('#loader').hide();
        }
    });
}

function getVoucherGeneralInfo(guest_id){
   if (typeof (onCloseNotRefresh) === "undefined") {
        onCloseNotRefresh = 0;
    } else {
        onCloseNotRefresh = 1;
    }
    $('#loader').show();
    var purchase_type = "show";
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;
    var movie_id;
    var season;
    var permalink;
    var url = HTTP_ROOT + "/user/GetVoucherDownloadContent";
        var movie_id = $('#movie_id').val();
        var permalink = $('#content-permalink').val();
        if ($('#season_id').length) {
            purchase_type = "season";
            season = $('#season_id').val();
            if (parseInt(season)) {
                permalink += "/season/" + season;
            }
        }
        if ($("#isadv").length) {
            isadv = $("#isadv").val();
        }
        if ($("#is_ppv_bundle").length) {
            is_ppv_bundle = $("#is_ppv_bundle").val();
        }
    var purl = HTTP_ROOT + "/user/DownloadContent/vlink/" + permalink;
    $.post(url, {'guest_id': guest_id, 'movie_id': movie_id, 'season': season, 'purchase_type': purchase_type,'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv, 'onCloseNotRefresh': 1}, function (res) {
        if (parseInt(res) === 1) {
            if ($("#generalInfoModal").length > 0) { 
                $("#generalInfoModal").modal('hide');
                $('#loader').hide();
            } 
            window.location.href = purl;
        } else {
            if ($("#generalInfoModal").length > 0) { 
                $("#generalInfoModal").modal('hide');
                $('#loader').hide();
            } 
            $("#ppvModal").html(res).modal('show');
            $('#loader').hide();
        }
    });
}

function getPpvPlans(obj, onCloseNotRefresh) {
    if(typeof (onCloseNotRefresh) === "undefined"){
        onCloseNotRefresh = 0;
    }else{
        onCloseNotRefresh = 1;
    }
    $('#loader').show();
    var purchase_type = "show";
    var isppv = 0;
    var is_ppv_bundle = 0;
    var isadv = 0;
    var movie_id;
    var season;
    var permalink;
    var download_type = 0;
    var url = HTTP_ROOT + "/user/getPpvPlans";
    if (typeof (obj) === "undefined") {
        var movie_id = $('#movie_id').val();
        var permalink = $('#content-permalink').val();
        if ($('#season_id').length) {
            purchase_type = "season";
            season = $('#season_id').val();
            if (parseInt(season)) {
                permalink += "/season/" + season;
            }
        }
        if ($("#isadv").length) {
            isadv = $("#isadv").val();
        }
        if ($("#is_ppv_bundle").length) {
            is_ppv_bundle = $("#is_ppv_bundle").val();
        }
    } else {
        movie_id = $(obj).attr("data-movie_id");
        permalink = $(obj).attr("data-content-permalink");
        isadv = $(obj).attr('data-isadv');
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
            purchase_type = $(obj).attr('data-purchase_type');
        if (typeof purchase_type !== typeof undefined && purchase_type !== false) {
            if(purchase_type === "season" && $("#series").length){
                season = $("#series").val();
                if(parseInt(season)){
                    permalink+="/season/"+season;
                }
            }
        }
    }
    var content_type = $(obj).attr('data-ctype');
    var is_instafeez = 0;

    if (PAYMENT_GATEWAY === 'instafeez' && parseInt(content_type) !== 3) {
        is_instafeez = 1;
    }

    if (typeof ($(obj).attr('data-download')) !== typeof undefined && ($(obj).attr('data-download')) !== false) {
        download_type = $(obj).attr('data-download');
        var purl = HTTP_ROOT + "/user/DownloadContent/vlink/" + permalink;
    }else{
        var purl = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
    }
    $.post(url, {'download_type':download_type, 'movie_id': movie_id, 'season': season, 'purchase_type': purchase_type, 'instafeez': is_instafeez, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'isadv': isadv, 'onCloseNotRefresh': 1}, function (res) {
        if (parseInt(res) === 1) {
            window.location.href = purl;
        } else {
            if ($("#loginModal").length > 0) {
                $("#loginModal").modal('hide');
                $('#loader').hide();
            }
            if (PAYMENT_GATEWAY === 'instafeez' && parseInt(content_type) !== 3) {
                var obj = new instafeez();
                obj.processCard(res);
            } else {
                reload = 1;
                $("#ppvModal").html(res).modal('hide');
                $('#loader').hide();

                if (parseInt(is_plan_error)) {
                    $("#ppvModal").html(res).modal('show');
                    $('#loader-ppv').hide();
                }
            }
        }
    });
}

function episodeplayMovie(obj) {
    $('#loader').hide();
    if (typeof (obj) == "undefined") {
        var movie_id = $('#movie_id').val();
        var stream_id = $('#stream_id').val();
        var permalink = $('#content-permalink').val();
    } else {
        var movie_id = $(obj).attr("data-movie_id");
        var stream_id = $.trim($(obj).attr("data-stream_id"));
        var permalink = $(obj).attr("data-content-permalink");
    }
    var fseasonID = $(obj).attr("data-season_id");
    var SeasonID='';
    if($("#series").val()){
       var season = $("#series").val();
       SeasonID='/season/' +season;
   }
   else{
      SeasonID='/season/' +fseasonID; 
   }
    if (stream_id !== '') {
        window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + permalink + '/stream/' + stream_id + SeasonID;
    } else {
        window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
    }
}

function episodegetPpvPlans(obj, onCloseNotRefresh) {
    if(typeof (onCloseNotRefresh) === "undefined"){
        onCloseNotRefresh = 0;
    }else{
        onCloseNotRefresh = 1;
    }
    var purchase_type = "episode";
    var url = HTTP_ROOT + "/user/getPpvPlans";
    var isppv = 0;
    var is_ppv_bundle = 0;
    var movie_id;
    var stream_id;
    var permalink;

    if (typeof (obj) === "undefined") {
        movie_id = $('#movie_id').val();
        stream_id = $('#stream_id').val();
        permalink = $('#content-permalink').val();
        if ($("#is_ppv_bundle").length) {
            is_ppv_bundle = $("#is_ppv_bundle").val();
        }
    } else {
        movie_id = $(obj).attr("data-movie_id");
        stream_id = $(obj).attr("data-stream_id");
        permalink = $(obj).attr("data-content-permalink");
        isppv = $(obj).attr('data-isppv');
        is_ppv_bundle = $(obj).attr('data-is_ppv_bundle');
    }

    $.post(url, {'movie_id': movie_id, 'stream_id': stream_id, 'purchase_type': purchase_type, 'isppv': isppv, 'is_ppv_bundle': is_ppv_bundle, 'onCloseNotRefresh':onCloseNotRefresh}, function (res) {
        $('#loader').show();
        if (parseInt(res) === 1) {
            if (stream_id !== '') {
                window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + permalink + '/stream/' + stream_id;
            } else {
                window.location.href = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
            }
        } else {
            reload = 1;
            $("#ppvModal").html(res).modal('hide');
            $('#loader').hide();

            if (parseInt(is_plan_error)) {
                $("#ppvModal").html(res).modal('show');
                $('#loader-ppv').hide();
            }
        }
    });
}
$(document).ready(function () {
    var showconfirm = '';
    var showconfirmsessionid = '';
    var showconfirm = $('#show_season_confirm').val();
    var showconfirmsessionid = $('#show_season_confirm_sessionid').val();
    var seasonId = $('#seasonId').val();
    var episodeId = $('#episodeId').val();
    var embedId = $('#embedId').val();
    var episode = $('#episode').val();
    var permalink = $('#permalink').val();
    var timeInterval = '5000';
    if($('#time_interval_set').length > 0){
     timeInterval = $('#time_interval_set').val();
    }
    var content_name = $('#content_name').val();
    if (showconfirm != '' && showconfirmsessionid != '' && typeof (showconfirm) != 'undefined' && typeof (showconfirmsessionid) != 'undefined' && showconfirm === showconfirmsessionid) {
        var contName = content_name;
         if (parseInt(seasonId)) { 
            contName = contName + '-' + JSLANGUAGE.season + seasonId;
    }
        if (parseInt(episodeId)) {
            contName = contName + '-' + episode;
        }
        
        $('#modalmsg').html(contName);
        
        $("#showseasonconfirmpopup").modal('show');
        var initial;
        initial = window.setTimeout(function(){ 
            uri = '';
            if (parseInt(seasonId)) {
                uri = permalink + '/season/' + seasonId;
    } 
            
            if (parseInt(episodeId)) {
                uri = uri + '/stream/'+embedId;
            }
            var content_id = $('#content_id').val();

            var url = HTTP_ROOT + "/user/SeasonShowConfirm/";
            $('.loader_confirm').show();
            $.post(url, {'showconfirmsessionid': showconfirmsessionid}, function (res) {
                if (res) {
                    $('.loader_confirm').hide();
                    if (uri !== "") {
                        window.location.href = HTTP_ROOT + '/' + Player_Page + '/' + uri;
                    } else {
                        window.location.href = HTTP_ROOT + '/' + Player_Page + '/' + permalink;
                    }
                }
            });
        }, timeInterval);
        
    }
      $("#dismisShowseasonconfirm").click(function () {
        clearTimeout( initial );
        var url = HTTP_ROOT + "/user/SeasonShowConfirm/";
       $('.loader_confirm').show();
       $.post(url, {'showconfirmsessionid': showconfirmsessionid}, function (res) {
            if (res == 1) {
            $('.loader_confirm').hide();
      $("#showseasonconfirmpopup").modal('hide');
        }
      });
      });
        $("#watchnowShowseasonconfirm").click(function () {
        uri = '';
             if (parseInt(seasonId)) {
            uri = permalink + '/season/' + seasonId;
        }
         
        if (parseInt(episodeId)) {
                uri = uri + '/stream/'+embedId;
            }
        var content_id = $('#content_id').val();

        var url = HTTP_ROOT + "/user/SeasonShowConfirm/";
          $('.loader_confirm').show();
       $.post(url, {'showconfirmsessionid': showconfirmsessionid}, function (res) {
            if (res) {
             $('.loader_confirm').hide();
                if (uri !== "") {
                    window.location.href = HTTP_ROOT + '/' + Player_Page + '/' + uri;
                } else {
                    window.location.href = HTTP_ROOT + '/' + Player_Page + '/' + permalink;
                }
            }
        });
    });

    $('body').on('click', '.playbtn, .playnowbtn', function (e) {
        $('#loader').show();
        var chk_login = $(this).attr('data-api_available');
        var chk_registeration = $(this).attr('data-chk_register');
        var content_title = $(this).attr('data-content_title');
        var movie_id = $(this).attr('data-movie_id');
        var stream_id = $(this).attr('data-stream_id');
        var season_id;
        var purchase_type = $(this).attr('data-purchase_type');
        if (typeof purchase_type !== typeof undefined && purchase_type !== false) {
            if( purchase_type === 'season' && $("#series").length){
            season_id = $("#series").val();
        }
        }
        var isppv = $(this).attr('data-isppv');
        var is_ppv_bundle = $(this).attr('data-is_ppv_bundle');
        var isadv = $(this).attr('data-isadv');
        $.cookie("movie_id",movie_id);
        $.cookie("is_ppv",isppv);
        if (typeof isadv !== typeof undefined && isadv !== false) {

        } else {
            isadv = 0;
        }

        var permalink = $(this).attr("data-content-permalink");
        var contentTypePermalink = $(this).attr("data-content-type-permalink");
        var isDownload = $(this).attr("data-download");

        $('#loginModal').on('show.bs.modal', function(e) {
            $('#loader').hide();
            $('#chk_register').val(chk_registeration);
            $("#movie_id").val(movie_id);
            $("#content_title").val(content_title);
            if ($("#season_id").length) {
                $("#season_id").val(season_id);
            }
            $("#stream_id").val(stream_id);
            $("#isppv").val(isppv);
            $("#is_ppv_bundle").val(is_ppv_bundle);
            $("#isadv").val(isadv);
            $('#content-permalink').val(permalink);
            $('#content-type-permalink').val(contentTypePermalink);
            if (typeof isDownload !== typeof undefined && isDownload !== false) {
                $('#isDownload').val(isDownload);
            } else {
                $('#isDownload').val('');
            }
        });
		
		$('#generalInfoModal').on('show.bs.modal', function (e) {
			$('#loader').hide();
			$('#chk_register').val(chk_registeration);
			$("#movie_id").val(movie_id);
			$("#content_title").val(content_title);
			if ($("#season_id").length) {
				$("#season_id").val(season_id);
			}
			$("#stream_id").val(stream_id);
			$("#isppv").val(isppv);
			$("#is_ppv_bundle").val(is_ppv_bundle);
			$("#isadv").val(isadv);
			$("#general_info").val(1);
			$('#content-permalink').val(permalink);
			$('#content-type-permalink').val(contentTypePermalink);
			if (typeof isDownload !== typeof undefined && isDownload !== false) {
				$('#isDownload').val(isDownload);
			} else {
				$('#isDownload').val('');
			}
		});
    });

    $('#login_form').validate({
        rules: {
            "LoginForm[email]": {
                required: true,
                email: true
            },
            "LoginForm[password]": {
                required: true,
            },
            "LoginForm[mobile_number]": {
                required: true,
                number: true
            }
        },
        messages: {
            "LoginForm[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "LoginForm[mobile_number]": {
                required: JSLANGUAGE.mobile_number_required,
            },
            "LoginForm[password]": {
                required: JSLANGUAGE.password_required,
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxlogin",
                data: $('#login_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#login_loading').show();
                    $('#loader_login').show();
                },
                success: function (data) {
                    $('input[name=csrfToken]').val(data.csrfToken);
                    $('#login_loading').hide();
                    $('#loader_login').hide();
                    if (data.login == 'success') {
                        if ($.trim($("#add_to_fav").val())) {
                            $('#login_loading').show();
                            $('#loader_login').show();
                            var content_type = $.trim($("#content_type").val());
                            if (content_type == 1) {
                                var content_id = $.trim($("#stream_id").val());
                            } else {
                                var content_id = $.trim($("#movie_id").val());
                            }
                            addToFavList(content_id, content_type, false, 1);
                        } else {
                            $('#login_errors').html("");
                            if ($.trim($("#movie_id").val())) {
                                $('#loginModal').hide();
								if ($.trim($("#isDownload").val())) {
                                    download_content();
                                } else {
                                    if ($("#chkPlay").val()) {
                                        chkPlayPerimission();
                                    } else if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val()) || parseInt($("#isadv").val())) {
                                        if ($.trim($("#stream_id").val()) === 0) {
                                            getPpvPlans();
                                        } else {
                                            episodegetPpvPlans();
                                        }
                                    } else {
                                        playMovie();
                                    }
                                }
                            } else {
                                if ($('#loginModal').length && $('#loginModal').css('display') == 'block') {
                                    location.reload();
                                }
                                else {
                                    if ($.trim(data.action) != '') {
                                        window.location = data.action;
                                    }
                                    else {
                                        window.location = HTTP_ROOT + '/';
                                    }
                                }
                            }
                        }
                    } else if ($.trim(data.login) == 'error_limit_login') {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();
                        $("#logout_all").modal('show');
                    }else if ($.trim(data.login) == 'account_not_activated') {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();
                    }
                    else {
                        $('#login_errors').html(JSLANGUAGE.incorrect_email_or_password);
                        $('#login_errors').show();
                    }
                },
                complete: function() {
                    $('#loader_login').hide();
                    $('#login_loading').hide();
                }
            });
        }
    });
    $("#login_errors").on('click', '#session_reset', function() {
        var user_id = $("#session_reset_user_id").val();
        var studio_id = $("#session_reset_studio_id").val();
        $.ajax({
            url: HTTP_ROOT + "/user/logoutAll",
            data: {user_id: user_id, studio_id: studio_id},
            type: 'POST',
            beforeSend: function() {
                $("#reset_session_loader").show();
            },
            success: function() {
                $("#reset_session_loader").hide();
                $("#login_errors #logout_all").modal('hide');
                $("#login_errors").html(JSLANGUAGE.logged_out_from_all_devices);
            },
            complete: function() {
                window.location.href = window.location.href;
            }
        });
    });

    $('#generalinfo_form').validate({
        rules: {
            "data[name]": {
                required: true,
                minlength: 1
            },
            "data[email]": {
                required: true,
                email: true
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
        },
        submitHandler: function (form) {
            $.ajax({
                url: HTTP_ROOT + "/user/SaveGeneralInfoUser",
                data: $('#generalinfo_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function () {
                    $('#loader_general').show();
                },
                success: function (data) {
                    $('#loader_general').hide();
                    if ($.trim(data.login) == 'success') {
                        if ($.trim($("#movie_id").val())) {
                            if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val())) {
                                var guest_id = $.trim(data.guest_id);
                                getVoucherGeneralInfo(guest_id);
                                $('#loader_general').hide();
                            } else {
                                playMovie();
                                $('#loader_general').hide();
                            }
                        } else {
                            location.reload();
                        }
                        
                    }
                },
                complete: function () {
                    $('#loader_general').hide();
                    $('#loader_general').hide();
                }
            });
        }
    });
    
    $('#register_form').validate({
        rules: {
            "data[name]": {
                required: true,
                minlength: 1
            },
            "data[email]": {
                required: true,
                email: true
            },
            "data[mobile_number]": {
                required: true,
                number: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
            "data[confirm_password]": {
                required: true,
                equalTo: "#join_password"
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "data[mobile_number]": {
                required: JSLANGUAGE.mobile_number_required
            },
            "data[password]": {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength
            },
            "data[confirm_password]": {
                required: JSLANGUAGE.valid_confirm_password,
                equalTo: JSLANGUAGE.password_donot_match,
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxregister",
                data: $('#register_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#register_loading').show();
                    $('#loader_register').show();
                },
                success: function(data) {
                    $('#register_loading').hide();
                    $('#loader_register').hide();
                    if ($.trim(data.login) == 'success') {
                        if ($.trim($("#add_to_fav").val())) {
                            $('#login_loading').show();
                            $('#loader_login').show();
                            var content_type = $.trim($("#content_type").val());
                            if (content_type == 1) {
                                var content_id = $.trim($("#stream_id").val());
                            } else {
                                var content_id = $.trim($("#movie_id").val());
                            }
                            addToFavList(content_id, content_type, false, 1);
                        } else {
                            $('#register-btn').attr("disabled", "disabled");
                            if ($.trim($("#movie_id").val())) {
                                if (parseInt($("#isadv").val()) || parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val())) {
                                    if ($.trim($("#stream_id").val()) === 0) {
                                        getPpvPlans();
                                    } else {
                                        episodegetPpvPlans();
                                    }
                                    $('#loader_register').hide();
                                    $('#register_loading').hide();
                                } else {
                                    playMovie();
                                    $('#register_loading').hide();
                                }
                            } else {
                                location.reload();
                            }
                        }
                    } else if ($.trim(data.register) == 'success') {
                        if (data.code != 202) {
                            $('#register_errors').show();
                            $('#register_errors').html(JSLANGUAGE.api_subscribe_failed);
                        } else {
                            var url = HTTP_ROOT + "/user/register";
                            window.location.href = url;
                        }
                    } else if ($.trim(data.register) == 'error') {
                        $('#register_errors').show();
                        $('#register_errors').html(data.message);
                    } else if ($.trim(data.login) == 'error') {
                        $('#register_errors').show();
                        $('#register_errors').html(JSLANGUAGE.email_exists_us);
                    }else if ($.trim(data.login) == 'account_not_activated') {
                        $('#register_errors').html(data.msg);
                        $('#register_errors').show();
                    }
                    else {
                        $('#register_errors').show();
                        $('#register_errors').html(JSLANGUAGE.saving_error);
                    }
                },
                complete: function() {
                    $('#loader_register').hide();
                    $('#register_loading').hide();
                }
            });
        }
    });

    $('#login-loading').hide();
    $('.loginbtn').click(function () {
        var frm = $(this).closest('form');
        frm.validate({
            rules: {
                "LoginForm[email]": {
                    required: true,
                    email: true
                },
                "LoginForm[mobile_number]": {
                    required: true,
                    number: true,
                },
                "LoginForm[password]": {
                    required: true,
                },
            },
            messages: {
                "LoginForm[email]": {
                    required: JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "LoginForm[mobile_number]": {
                    required: JSLANGUAGE.mobile_number_required,
                },
                "LoginForm[password]": {
                    required: JSLANGUAGE.password_required,
                },
            },
            submitHandler: function (form) {
                $.ajax({
                    url: HTTP_ROOT + "/user/ajaxlogin",
                    data: frm.serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function () {
                        $('#login-loading').show();
                        $('#loader_login').show();
                    },
                    success: function (data) {
                        $('#login-loading').hide();
                        $('#loader').hide();
                        if (data.login == 'success') {
                            if($.trim(data.action) != ''){
                                    window.location = data.action;
                                }
                                else{
                                    location.reload();
                            }
                        }
                        else {
                            frm.find('.loginerror').html(JSLANGUAGE.incorrect_email_or_password).show();
                            frm.find('.loginerror').show();
                        }
                    },
                    complete: function () {
                        $('#login-loading').hide();

                    }
                });
            }
        });
    });

    $('.registerbtn').click(function() {
        var frm = $(this).closest('form');
        frm.validate({
            rules: {
                "data[name]": {
                    required: true,
                    minlength: 1
                },
                "data[email]": {
                    required: true,
                    email: true
                },
                "data[password]": {
                    required: true,
                    minlength: 6
                },
            },
            messages: {
                "data[name]": {
                    required: JSLANGUAGE.full_name_required,
                    minlength: JSLANGUAGE.name_minlength
                },
                "data[email]": {
                    required: JSLANGUAGE.email_required,
                    email: JSLANGUAGE.valid_email
                },
                "data[password]": {
                    required: JSLANGUAGE.password_required,
                    minlength: JSLANGUAGE.password_minlength
                },
            },
            submitHandler: function(form) {
                $.ajax({
                    url: HTTP_ROOT + "/user/ajaxregister",
                    data: frm.serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function() {
                        $('#register-loading').show();
                        $('#loader_register').show();
                    },
                    success: function(data) {
                        $('#register-loading').hide();
                        $('#loader_register').hide();
                        if (data.login == 'success') {
                            if ($.trim($("#movie_id").val())) {
                                if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val())) {
                                    $('#loader_register').hide();
                                    if ($.trim($("#stream_id").val()) === 0) {
                                        getPpvPlans();
                                    } else {
                                        episodegetPpvPlans();
                                    }
                                } else {
                                    $('#loader_register').hide();
                                    playMovie();
                                }
                            } else {
                                location.reload();
                            }
                        }
                        else {
                            frm.find('.registererror').html(data.message);
                            frm.find('.registererror').show();
                        }
                    },
                    complete: function() {
                        $('#loader_register').hide();
                        $('#register-loading').hide();
                    }
                });
            }
        });
    });
    //for register popup 
    $('#register_step').validate({
        rules: {
            "data[name]": {
                required: true,
                minlength: 1
            },
            "data[email]": {
                required: true,
                email: true
            },
            "data[password]": {
                required: true,
                minlength: 6
            },
            "data[confirm_password]": {
                required: true,
                equalTo: "#join_passwords"
            }
        },
        messages: {
            "data[name]": {
                required: JSLANGUAGE.full_name_required,
                minlength: JSLANGUAGE.name_minlength
            },
            "data[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "data[password]": {
                required: JSLANGUAGE.password_required,
                minlength: JSLANGUAGE.password_minlength
            },
            "data[confirm_password]": {
                required: JSLANGUAGE.valid_confirm_password,
                equalTo: JSLANGUAGE.password_donot_match,
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: HTTP_ROOT + "/user/ajaxregister",
                data: $('#register_step').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function() {
                    $('#register_loading').show();
                    $('#loader_register').show();
                },
                success: function(data) {
                    $('#register_loading').hide();
                    $('#loader_register').hide();
                    if ($.trim(data.login) == 'success') {
                        if($.trim(data.action) != ''){
                             window.location = data.action;
                        }
                    }else if ($.trim(data.login) == 'error') {
                        $('#register_error').show();
                        $('#register_error').html(JSLANGUAGE.email_exists_us);
                    }else {
                        $('#register_error').show();
                        $('#register_error').html(JSLANGUAGE.saving_error);
                    }
                },
                complete: function() {
                    $('#loader_register').hide();
                    $('#register_loading').hide();
                }
            });
        }
    });
    $(".close").click(function() {
        $("#movie_id").val('');
        $("#stream_id").val('');
        $("#add_to_fav").val('');
        $("#content_type").val('');
        $("#fav_input").remove();
        $(".modal-backdrop").remove();
        showLogin();
    });
   $('body').on('click', '.addtofav', function (e) {
        var fav_status    = $(this).attr('data-fav_status'); 
        var login_status  = $(this).attr('data-login_status'); 
        var content_id    = $(this).attr('data-content_id'); 
        var content_type  = $(this).attr('data-content_type'); 
        var action = 1;
        if(login_status == 1){
           if($(this).find('.fa').hasClass('fa-heart-o')){
                $(this).find('.fa').removeClass('fa-heart-o');
                $(this).find('.fa').addClass('fa-heart');
                $('#favtext').html(JSLANGUAGE.added_to_fav);
            } else {
                $(this).find('.fa').removeClass('fa-heart');
                $(this).find('.fa').addClass('fa-heart-o');
                $('#favtext').html(JSLANGUAGE.add_to_fav);
            }
            addToFavList(content_id, content_type, true, fav_status);
        } else {
            $("#loginModal").modal('show');
            var input_field = '<div id="fav_input"><input type="hidden" name="add_to_fav" id="add_to_fav" value="1" /><input type="hidden" name="content_type" id="content_type" value="' + content_type + '" /></div>';
            $("#loginModal .popup_bottom").append(input_field);
            if (content_type == 1) {
                $("#stream_id").val(content_id);
            } else {
                $("#movie_id").val(content_id);
            }
        }
    });
   $('body').on('click', '.delete-fab', function (e) {
        var url = HTTP_ROOT+"/user/deletefromfavlist/";
        var content_id = $(this).attr('data-content_id');
        var content_type = $(this).attr('data-content_type');
        var action = 0;
        if ($.trim(content_type) == "") {
            content_type = 0;
        }
        $.post(url, {'content_id': content_id, 'content_type': content_type, 'login_status': true, 'action': action}, function(res) {
            location.reload();
        });
        $(this).parent().parent().remove();
    });
	$('body').on('click', '.facebook_login', function (e) {
	 var w_left = (screen.width/4);
	 var w_top = (screen.height/4);
        var url =$(this).attr('data-url');
        var is_popup=$(this).attr('data-login');
        $.cookie("is_popup",is_popup);
		$.cookie("current_url",''+window.location);
        window.open(''+ url +'','targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=900,height=500,top='+w_top+',left='+w_left+'');
    });

    $('#otp_form').validate({
        rules: {
            "LoginForm[email]": {
                required: true,
                email: true
            },
            "LoginForm[password]": {
                required: true,
            },
            "LoginForm[mobile_number]": {
                required: true,
                number: true
            }
        },
        messages: {
            "LoginForm[email]": {
                required: JSLANGUAGE.email_required,
                email: JSLANGUAGE.valid_email
            },
            "LoginForm[mobile_number]": {
                required: JSLANGUAGE.mobile_number_required,
            },
            "LoginForm[password]": {
                required: JSLANGUAGE.password_required,
            }
        },
        submitHandler: function (form) {
            var url = "/user/ajaxlogin";
            $.ajax({
                url: HTTP_ROOT + url,
                data: $('#otp_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function () {
                    $('#login_loading').show();
                    $('#loader_login').show();
                },
                success: function (data) {
                    $('input[name=csrfToken]').val(data.csrfToken);
                    $('#login_loading').hide();
                    $('#loader_login').hide();
                    if (data.login == 'success') {
                        if ($.trim($("#add_to_fav").val())) {
                            $('#login_loading').show();
                            $('#loader_login').show();
                            var content_type = $.trim($("#content_type").val());
                            if (content_type == 1) {
                                var content_id = $.trim($("#stream_id").val());
                            } else {
                                var content_id = $.trim($("#movie_id").val());
                            }
                            addToFavList(content_id, content_type, false, 1);
                        } else {
                            $('#login_errors').html("");
                            if ($.trim($("#movie_id").val())) {
                                $('#loginModal').hide();
                                if ($("#chkPlay").val()) {
                                    chkPlayPerimission();
                                }
                                else if (parseInt($("#isppv").val()) || parseInt($("#is_ppv_bundle").val()) || parseInt($("#isadv").val())) {
                                    if ($.trim($("#stream_id").val()) === 0) {
                                        getPpvPlans();
                                    } else {
                                        episodegetPpvPlans();
                                    }
                                } else {
                                    playMovie();
                                }
                            } else {
                                if ($('#loginModal').length && $('#loginModal').css('display') == 'block') {
                                    location.reload();
                                } else {
                                    if ($.trim(data.action) != '') {
                                        window.location = data.action;
                                    } else {
                                        window.location = HTTP_ROOT + '/';
                                    }
                                }
                            }
                        }
                    } else if ($.trim(data.login) == 'error_limit_login') {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();
                        $("#logout_all").modal('show');
                    } else if($.trim(data.LoginForm_OTP) == 3) {
                        $('#login_success').hide();
                        $('#login_errors').html(JSLANGUAGE.otp_expired);
                        $('#login_errors').show();
                    } else if($.trim(data.LoginForm_OTP) == 4) {
                        $('#login_success').hide();
                        $('#login_errors').html(JSLANGUAGE.incorrect_login_credential);
                        $('#login_errors').show();
                    } else if($.trim(data.LoginForm_OTP) == 5) {
                        $('#login_success').hide();
                        $('#login_errors').html(JSLANGUAGE.generate_otp_msg);
                        $('#login_errors').show();
                    }else if($.trim(data.LoginForm_OTP) == 6){
                        $('.sub_title').html(JSLANGUAGE.subscribe_login_msg);
                        $('#playchkModal').modal('show');
                    }else if($.trim(data.LoginForm_OTP) == 7){
                        $('#login_success').hide();
                        $('#login_errors').html(JSLANGUAGE.subscribe_null);
                        $('#login_errors').show();
                    } else {
                        alert(data.LoginForm_OTP);
                        $('#login_errors').html(JSLANGUAGE.incorrect_email_or_password);
                        $('#login_errors').show();
                    }
                },
                complete: function () {
                    $('#loader_login').hide();
                    $('#login_loading').hide();
                }
            });
        }
    });

    $(".sub-popup-close").click(function () {
        if (parseInt(reload)) {
            location.reload();
        }
        reload = 0;
        $(".modal-backdrop").remove();
    });
});
function addToFavList(content_id, content_type, login_status, action) {
    var url = HTTP_ROOT + "/user/addtofavlist/";
    $.post(url, {'content_id': content_id, 'content_type': content_type, 'login_status': login_status, 'action': action}, function (res) {
        if ($.trim(res) == "success") {
            if (login_status) {
                location.reload();
                return res;
            } else {
                window.location.href = window.location.href;
            }
        }
    });
}
function showLogin() {
    $("#register_form_div").hide();
    $("#login_form_div").show();
}

function showRegister() {
    $("#login_form_div").hide();
    $("#register_form_div").show();
}

function chkPlayPerimission(obj) {
    $('#subscribe_success').hide();
    if (typeof (obj) == "undefined") {
        var movie_id = $('#movie_id').val();
        var stream_id = $('#stream_id').val();
        var permalink = $('#content-permalink').val();
        var content_title = $('#content_title').val();
    } else {
        var movie_id = $(obj).attr("data-movie_id");
        var stream_id = $.trim($(obj).attr("data-stream_id"));
        var permalink = $(obj).attr("data-content-permalink");
        var purchase_type = $(obj).attr("data-purchase_type");
        var content_title = $(obj).attr("data-content_title");
        var content_type = $(obj).attr("data-ctype");
    }
    $('#play_loading').show();
    $('#playchk_div').hide();
    var url = HTTP_ROOT + "/user/chkPemission/";
    $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type, 'stream_id': stream_id, 'content_title': content_title, 'content_type': content_type}, function (response) {
        $('#play_loading').hide();
        if ($.trim(response) === 'noaccess') {
            $('#playchkModal').modal('show');
            $('#playchk_div').show();
            return false;
        } else {
            $("#playchkModal").modal('hide');
            if (stream_id !== '') {
                permalink = permalink + '/stream/' + stream_id;
            }
            var url = HTTP_ROOT + "/" + Player_Page + "/" + permalink;
            window.location.href = url;
        }
    });

}

function subscribe() {
    var mob = $("#username").val();
    $('#subscribe_loading').show();
    $("#sub_btn").attr("disabled", "disabled");
    $("#cancel_btn").attr("disabled", "disabled"); 
    var url = HTTP_ROOT + "/user/subscribe/";
    $.post(url, {'mobile_number':mob}, function (response) {
        $('#subscribe_loading').hide();
        $("#sub_btn").removeAttr("disabled");
        $("#cancel_btn").removeAttr("disabled");
        reload = 1;
        if ($.trim(response.code) == '202') {
            $('#subscribe_errors').hide();
            $('#subscribe_success').show();
            $('#subscribe_success').html(JSLANGUAGE.api_subscribe_msg);
        } else {
            $('#subscribe_success').hide();
            $('#subscribe_errors').show();
            $('#subscribe_errors').html(JSLANGUAGE.api_subscribe_failed);
        }
        
        setTimeout(function () {
            $('#playchkModal').modal('hide');
            location.reload();
            $(".modal-backdrop").remove();
        }, 5000);
    }, 'json');
}
function generateOtp() {
    $('[name="LoginForm[otp]"]').rules('remove');
    $('#new_otp').val('');    
    var x = $("#otp_form").valid();
    if (x) {
        var otp = $('#otp').val();
        if (otp) {
            var url = '/user/generatOtp';
            $.ajax({
                url: url,
                data: $('#otp_form').serialize(),
                type: 'POST',
                dataType: "json",
                beforeSend: function () {
                    $('#login_loading').show();
                },
                success: function (data) {
                    if (data.otp == 'success') {
                        $('#login_errors').hide();
                        $('#login_success').html(data.msg);
                        $('#login_success').show();
                        $('#is_generateotp').val(1);
                        $('#otp_btn').html(JSLANGUAGE.resend_otp);
                    } else if (data.otp == 'failed') {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();
                        $('#login_success').hide();
                        $('#is_generateotp').val('');
                    } else {
                        $('#login_errors').html(data.msg);
                        $('#login_errors').show();
                        $('#login_success').hide();
                    }
                },
                complete: function () {
                    $('#login_loading').hide();
                }
            });
        }
    }

}

function validateOTPLogin() {
    $('[name="LoginForm[otp]"]').rules('add', {
        required: true,
        number: true,
        messages: {
            required: JSLANGUAGE.otp_required
        }
    });
    $("#otp_form").submit();
}