$(document).ready(function () {
    var requestType = 'mped_dash';
    var unique_id = 0;
    var bufferLog_id = 0;
    var bufferLog_idtemp = 0;
    var DRMbufferenStart = 0;
    var DRMbufferenEndd = 0;
    var DRMresolution = 0;
    var device_type = '';
    if(typeof deviceType != 'undefined'){
        device_type = deviceType;
    }
    var user_id_of_viewer_js = 0;
    if(typeof user_id_of_viewer != 'undefined'){
        user_id_of_viewer_js = user_id_of_viewer;
    }
    player.ready(function () {
       setInterval(function(){
            updateBufferLog(player,Math.round(player.currentTime()),bufferLog_id,unique_id);
        },60000);
        player.on("ended", function() {
            updateBufferLog(player,Math.round(player.currentTime()),bufferLog_id,unique_id);
        });
        if ($('#backButton').length) {
            $('#backButton').click(function(){
                updateBufferLog(player,Math.round(player.currentTime()),bufferLog_id,unique_id);
            });
	}
        if (window.history && window.history.pushState) {
            window.history.pushState('forward', null, '');
            $(window).on('popstate', function() {
               updateBufferLog(player,Math.round(player.currentTime()),bufferLog_id,unique_id);
            });
	}
        player.on('loadedmetadata', function(){
            $.ajax({
                    type: 'POST',
                    url: log_url + '/videoLogs/videoBandwidthLog',
                    async: false,
                    data :{
                            movie_id: movie_id ,
                            video_id: stream_id , 
							studio_id:studio_id,
                            start_time: DRMbufferenStart, 
                            end_time: DRMbufferenEndd, 
                            resolution: DRMresolution,
                            request_type : requestType,
                            bufferLog_id : bufferLog_id,
                            buff_log_idtemp : bufferLog_idtemp,
                            totDRMBandwidth: parseInt(totalDRMBandWidth/1024),
                            device_type:device_type,
                            user_id_of_viewer:user_id_of_viewer_js
                            },
                    success: function(res){
                        var obj = JSON.parse(res);
                        bufferLog_id = obj.buffer_log_id;
                        unique_id = obj.u_id;
                        bufferLog_idtemp = obj.buff_log_idtemp;
                    },
                    error: function(e)
                    {
                       alert("Exception ::: " + e.message);
                    }
                });
            });
        }); 
    function updateBufferLog(player , currentTime , bufferLog_id, unique_id){
        var browser_details = get_browser_version();
        browser_details = browser_details.name+"/version("+browser_details.version+")";
        if (parseFloat(bufferLog_id)) 
            {
                DRMbufferenEndd = currentTime;
                $.ajax({
                        url: log_url + '/videoLogs/videoBandwidthLog',
                        type: 'post',
                        data: {
                                movie_id: movie_id , 
                                video_id: stream_id,  
								studio_id:studio_id,
                                start_time: DRMbufferenStart, 
                                end_time: DRMbufferenEndd, 
                                resolution: DRMresolution, 
                                buff_log_id: bufferLog_id,
                                buff_log_idtemp:bufferLog_idtemp,
                                u_id: unique_id,
                                request_type : requestType,
                                browser_details : browser_details,
                                totDRMBandwidth: parseInt(totalDRMBandWidth/1024),
                            device_type:device_type,
                            user_id_of_viewer:user_id_of_viewer_js
                               },
                        success: function(res) {
                                var obj = JSON.parse(res);
                                bufferLog_id = obj.buffer_log_id;
                                unique_id = obj.u_id;
                                bufferLog_idtemp = obj.buff_log_idtemp;
                        },
                        error: function(jqXhr, textStatus, errorThrown){
                            console.log("Error :: " +  errorThrown );
                        }
                    });
            }
    }
    function get_browser_version() {
            var useraccount=navigator.userAgent,tem,res_obj=useraccount.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
            if(/trident/i.test(res_obj[1])){
                tem=/\brv[ :]+(\d+)/g.exec(useraccount) || []; 
                return {name:'IE',version:(tem[1]||'')};
            }   
            if(res_obj[1]==='Chrome'){
                tem=useraccount.match(/\bOPR|Edge\/(\d+)/)
                if(tem!=null){
                    temp = tem[0].split('/');
                    if(temp[0].toLowerCase().match('edge')){
                        return {name:'Edge', version:tem[1]};
                    }else{
                        return {name:'Opera', version:tem[1]};
                    }
                }
            }   
            res_obj=res_obj[2]? [res_obj[1], res_obj[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem=useraccount.match(/version\/(\d+)/i))!=null) {res_obj.splice(1,1,tem[1]);}
            return { name: res_obj[0], version: res_obj[1]};
    }
}); 


