$(document).ready(function () {
    var ended = 0;
   player.ready(function () {
       var started = 0;
       player.on("play", function() {
           started =1;
       });
       setInterval(function() {
            if(started == 1){
            var user_active = player.userActive();
            if(user_active === true || player.paused()){
                $('.common-css').show();
               // $('.vjs-control-bar').show();
            } else {
                $('.common-css').hide();
               // $('.vjs-control-bar').hide();
            }
        }
    },1000);
    if(current_contentType != "0"){
        if(stream_id_prev || permalinkPrev ){
            $('.prev-buttons').css( "opacity",1);
        }
    }
        if(stream_id_next || permalinkNext){
            $('.next-buttons').css( "opacity",1);
        }
        player.on("ended", function(){
            if(ended == 0){
                ended = 1;
				if(stream_id_next || permalinkNext){
					setTimeout(function () {
						$('.next-buttons').click();
					}, 3000);
				} 
            } 
        });
        if(stream_id_next || permalinkNext){
            $('.next-buttons').bind('click',function(e){
                if(contentType_next!="0"){
                    window.location.href = next_episode_url+'/player/'+permalinkNext_multi+'/stream/'+ embed_id_next;
                }else{
                    window.location.href = next_episode_url+'/player/'+permalinkNext;
                }
            });
        }
        if(current_contentType != "0"){
            if(stream_id_prev || permalinkPrev){
                $('.prev-buttons').bind('click',function(e){
                    if(contentType_prev!="0"){
                        window.location.href = next_episode_url+'/player/'+permalinkPrev_multi+'/stream/'+ embed_id_prev;
                    }else{
                        window.location.href = next_episode_url+'/player/'+permalinkPrev;
                    }
                });
            }
        }
        player.on('timeupdate',function(){
            var currentTime = player.currentTime().toFixed(0);
            var remainingTime = player.remainingTime();
            remainingTime = remainingTime.toFixed(0);
            if((remainingTime == 0) && currentTime >= 4 ){
                if(ended == 0){
                    ended =1;
                    if(stream_id_next || permalinkNext){
                        setTimeout(function () {
                            $('.next-buttons').click();
                        }, 3000);
                    }
                }
            }
		});
	});
});


