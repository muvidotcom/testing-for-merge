var s3upload = null;
var s3obj = new Array();
var size = '';
var sizeName = '';
var seconds;
/* It will contain all the Javascript related add Contents */
var isFormChanged = 0;
$('.checkInput').on('change', function () {
    if ($(this).val() && !isFormChanged) {
        isFormChanged = 1;
    }
});

(function () {
    $('#sub-btn').click(function () {
        var movie_id = $('#movie_id').val();
        removeTopBanner(movie_id);
    });
})();
var fireedPoster = false;
function removeposter(movie_id, obj_type, movie_stream_id) {
    $('#headerConfirmModal').html("Remove Poster?");
    $('#bodyConfirmModal').html("Are you sure to remove this poster?");
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    }).one('click', '#confirmUGCYes', function (e) {
        if (movie_id) {
            if (!fireedPoster) {
                fireedPoster = true;
                var url = HTTP_ROOT + "/admin/removeposter";
                $('#remove-poster-text').text('Removing...');
                $('#remove-poster-text').attr('disabled', 'disabled');
                $.post(url, {'movie_id': movie_id, 'obj_type': obj_type, 'movie_stream_id': movie_stream_id, 'is_ajax': 1}, function (res) {
                    if (res.err) {
                        $('#remove-poster-text').removeAttr('disabled');
                        var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>&nbsp;Error in deleting poster.</div>'
                        $('.pace').prepend(sucmsg);
                        $('#remove-poster-text').text('Remove');
                    } else {
                        var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>&nbsp;Poster removed successfully.</div>'
                        $('.pace').prepend(sucmsg);
                        $('#remove-poster-text').text('');
                        $('#avatar_preview_div').children('img').remove();
                        window.location.reload(1);
                    }
                }, 'json');
            }
        } else {
            return false;
        }
    });
}
/* Select the Form based on the content type */

function globalPopup(popupid) {
    $('#' + popupid).modal('show');
    if (popupid == 'topbanner') {
        $('#content_id').val($('#movie_id').val());
    }
}

function addCastCrewpopup() {
    $('#add_cast_crew').toggle('slow');
    settout = setTimeout('scrollwindow()', '1000');
}
function scrollwindow() {
    window.scrollTo(0, document.body.scrollHeight);
    clearTimeout(settout);
}
var firedDelVideo = false;
function deleteVideo(movie_id) {
    $('#headerConfirmModal').html("Remove video?");
    $('#bodyConfirmModal').html("Are you sure you want to remove this video from the movie? <br />This video will parmanetly deleted from your content.");
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    }).one('click', '#confirmUGCYes', function (e) {
        if (!firedDelVideo) {
            firedDelVideo = true;
            $.post(HTTP_ROOT + "/admin/removeVideo", {'is_ajax': 1, 'movie_id': movie_id}, function (res) {
                if (res.error == 1) {
                    showUGCAlert('Alert!', 'Error in removing movie');
                } else {
                    $('#fmovie_name').html("<small><em>Not Available</em></small>");
                    showUGCAlert('Alert!', 'Your movie video removed successfully');
                }
            }, 'json');
        }
    });

}

function click_browse(file_name) {
    $("#" + file_name).click();
}
var fired = false;
function checkfileSize(e) {
    var type = '';
    var movie_name = $('#movie_name').val();
    var stream_id = $('#movie_stream_id').val();
    var movie_st_id = $('#movie_id').val();
    var filename = $('#videofile').val().split('\\').pop();
    var content_types_id = $('#content_type').val();
    var extension = filename.replace(/^.*\./, '');
    if (extension == filename) {
        extension = '';
    } else {
        extension = extension.toLowerCase();
    }
    switch (extension) {
        case 'mp4':
            break;
        case 'mov':
            break;
        case 'mkv':
            break;
        case 'flv':
            break;
        case 'vob':
            break;
        case 'm4v':
            break;
        case 'avi':
            break;
        case '3gp':
            break;
        case 'mpg':
            break;
        case 'wmv':
            break;
        default:
            showUGCAlert('Alert!', "Sorry! This video format is not supported. <br>MP4/MOV/MKV/FLV/VOB/M4V/AVI/3GP/MPG/WMV format video are allowed to upload");
            return false;
            break;
    }
    $('#headerConfirmModal').html("Upload File?");
    $('#bodyConfirmModal').html("Are you sure to upload " + filename + " file for  " + movie_name + " ?");
    $('#confirmModal').modal({
        backdrop: 'static',
        keyboard: false
    }).one('click', '#confirmUGCYes', function (e) {
        if (!fired) {
            fired = true;
            $("#addvideo_popup").modal('hide');
            $("#addvideo_popup_physical").modal('hide');
            if (!$('#dprogress_bar').is(":visible")) {
                $('#dprogress_bar').show();
            }
            if (content_types_id == 5 || content_types_id == 6) {
                var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload audio</a></h5>";
            } else {
                var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a></h5>";
            }
            $('#' + stream_id).html(rtext);
            upload('user', 'pass', stream_id, movie_name, movie_st_id, content_types_id, filename, type);
            return true;
        }
    });
    return false;
}
function posterpreview(obj) {
    $("#previewcanvas").show();
    var canvaswidth = $("#reqwidth").val();
    var canvasheight = $("#reqheight").val();

    var x1 = $('#x1').val();
    var y1 = $('#y1').val();
    var width = $('#w').val();
    var height = $('#h').val();

    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
    };
    if ($("#avatar_preview_div").length) {
        $("#avatar_preview_div").hide();
    }
    img.src = $('#preview').attr("src");
    $('#ugc_poster').modal('hide');
    $(obj).html(JSLANGUAGE.btn_next);

}
function hide_file() {
    $('#preview').css("display", "none");
}
function hide_gallery() {
    $('#glry_preview').css("display", "none");
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
        $('#profile button').attr('disabled', 'disabled');

    } else {
        $('.loaderDiv').hide();
        $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
        $('#profile button').removeAttr('disabled');
    }
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function timeCal(seconds) {
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var sec = seconds - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (sec < 10) {
        sec = "0" + sec;
    }
    var timeLeft = hours + ':' + minutes + ':' + sec;
    return timeLeft;
}
function upload(user, pass, stream_id, movie_name, movie_st_id, content_types_id, filename, type) {
    var contentType = content_types_id;
    //var xhr = new XMLHttpRequest({mozSystem: true});
    var filename = $('input[type=file]').val().split('\\').pop();
    if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
        showUGCAlert('Alert!', "Sorry! You are using an older or unsupported browser. Please update your browser");
        return;
    }

    var sizeleft = 0;
    var timeLeft = '00:00:00';
    var sizeKb;
    var sizeLeftKb;
    var file = $('#videofile')[0].files[0];
    size = Math.round(file.size / 1000);
    sizeKb = size;
    timeLeft = ((sizeKb / 1000) / userinternetSpeed).toFixed(0);
    if (timeLeft < 4) {
        timeLeft = 4;
    }
    if ((size / 1000) < 6) {
        speed = userinternetSpeed + "mbps";
    }
    timeLeft = timeCal(timeLeft);
    sizeName = 'KB';
    if (size > 1000) {
        size = Math.round(size / 1000);
        var sizeName = 'MB';
    }
    if (size > 1000) {
        size = size / 1000;
        size = size.toFixed(2);
        var sizeName = 'GB';
    }
    //by me
    var speed = "0 kbps";
    var speedMbps;
    var startTime = (new Date()).getTime();
    var filename = file.name;
    filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g, "_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + filename.replace(/^.*\./, '');
    file.name = filename;
    var videoUploadTypeToS3 = "";
    if (typeof video_upload_type != 'undefined' && video_upload_type != '') {
        videoUploadTypeToS3 = video_upload_type;
    }
    s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_st_id, 'type': type, 'uploadType': videoUploadTypeToS3});
    s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 403) {
            showUGCAlert('Alert!', "Sorry you are not allowed to upload");
        } else {
            if (command == 'CompleteMultipartUpload') {
                window.setTimeout(function () {
                    s3upload.completeMultipartUpload();
                }, s3upload.RETRY_WAIT_SEC * 1000);
            } else if (command == 'CreateMultipartUpload') {
                window.setTimeout(function () {
                    s3upload.createMultipartUpload();
                }, s3upload.RETRY_WAIT_SEC * 1000);
            }
            console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
        }
    };

    s3upload.onS3UploadError = function (xhr) {
        s3upload.waitRetry();
        console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
    };

    //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
    s3upload.onProgressChanged = function (progPercent) {
        var endTime = (new Date()).getTime();
        var duration = (endTime - startTime) / 1000;
        if (progPercent == 100) {
            $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
            $('#upload_' + stream_id + ' .uploadFileSizeProgress').html(size);
            $('#upload_' + stream_id + ' .timeRemaining').html("00:00:00");
        } else {
            var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
            $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

            $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
            if (sizeName == 'GB') {
                sizeleft = (progper * size) / 100;
                sizeleft = sizeleft.toFixed(2);
            } else {
                sizeleft = Math.round((progper * size) / 100);
            }
            if (sizeName == 'GB') {
                speed = ((sizeleft * 1000 * 1000) / duration).toFixed(2);
                sizeLeftKb = sizeleft * 1000 * 1000;
            }
            if (sizeName == 'MB') {
                speed = ((sizeleft * 1000) / duration).toFixed(2);
                sizeLeftKb = sizeleft * 1000;
            }
            if (sizeName == 'KB') {
                speed = ((sizeleft) / duration).toFixed(2);
                sizeLeftKb = sizeleft;
            }
            speedMbps = (speed / 1000).toFixed(2);
            seconds = ((sizeKb - sizeLeftKb) / speed).toFixed(0);
            timeLeft = timeCal(seconds);
            if (speed > 999) {
                speed = (speed / 1000).toFixed(2) + "mbps";
            } else {
                speed = speed + "kbps";
            }

            $('#upload_' + stream_id + ' .uploadFileSizeProgress').html(sizeleft);
            $('#upload_' + stream_id + ' .uploadSpeed').html(speed);
            $.post(HTTP_ROOT + "/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
            $('#upload_' + stream_id + ' .timeRemaining').html(timeLeft);
        }
    };

    s3upload.onUploadCompleted = function () {
        var convText = '';
        $('#upload_' + stream_id).remove();
        var purl = '';
        var playtext = '';
        //purl = HTTP_ROOT + "/video/play_video?movie="+movie_st_id+"&movie_stream_id="+stream_id+"&preview=1";
        //playtext = '<a href="'+purl+'" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp;';
        if (videoUploadTypeToS3 == 'physical') {
            convText = '<h5><a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
            $('#trailerbtn' + movie_st_id).html(convText);
        }
        if (contentType != 5 && contentType != 6) {
            convText = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
            if ($("#encodingStreamId").length) {
                if ($('#encodingStreamId').is(":empty")) {
                    $('#encodingStreamId').html(stream_id);
                } else {
                    $('#encodingStreamId').html($('#encodingStreamId').html() + "," + stream_id);
                }
            }
        } else {
            convText = filename + '<br><h5><a href="javascript:void(0);" onclick="openUploadpopup(' + movie_st_id + ', ' + stream_id + ',' + contentType + ');" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Change Audio</a></h5>';
        }
        //location.reload();
        //var convText = '';
        //var convText = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><div class="col-lg-12 no-padding"><div class="col-lg-1 no-padding"><span class="glyphicon glyphicon-refresh glyphicon-spin glyphicon-2x"></span></div> <div class="col-lg-11 adminaction-icon">Encoding in progress</div></div><div class="cb" style="clear: both;"></div> </a>'; 
        //$('#'+stream_id).html(playtext);
        $('#' + stream_id).html(convText);
        $('#video_upload_' + stream_id).empty();
        $('#videoCorrupt' + stream_id).hide();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }
        if (contentType != 1) {
            $.post(HTTP_ROOT + "/cron/getThumbnail");
        }
        console.log("Congratz, upload is complete now");
    };
    s3upload.onUploadCancel = function () {
        $('#upload_' + stream_id).remove();
        if ($('#all_progress_bar').is(":empty")) {
            $('#dprogress_bar').hide();
        }
        if (videoUploadTypeToS3 == 'physical') {
            var ptext = '<h5><a href="javascript:void(0);" onclick="openUploadpopup(\'' + stream_id + '\',0);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></h5>';
            $('#trailerbtn' + movie_st_id).html(ptext);
        }
        if (content_types_id == 5 || content_types_id == 6) {
            var utext = '<a href="javascript:void(0);" onclick="openUploadpopup(' + movie_st_id + ',' + stream_id + ',' + content_types_id + ');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload audio</a>';
        } else {
            var utext = '<a href="javascript:void(0);" onclick="openUploadpopup(' + movie_st_id + ',\'' + movie_name + '\',' + stream_id + ',' + content_types_id + ');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload video</a>';
            //var utext = '<a href="javascript:void(0);" onclick="openUploadpopup('+movie_st_id+',\''+movie_name+'\','+stream_id+','+content_types_id+');"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a>&nbsp;';
        }
        $('#' + stream_id).html(utext);
        console.log("Upload Cancelled..");
    };

//Start s3 Multipart upload	
    s3upload.start();
    s3obj[stream_id] = s3upload;
    var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">' + sizeleft + '</span>/' + size + ' ' + sizeName + ') (<span class="uploadSpeed">' + speed + '</span>)<span class="timeRemaining" style="float:right">' + timeLeft + '</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
    $('#all_progress_bar').append(progressbar);
    $('#cancel_' + stream_id).on('click', function () {
        aid = this.id;
        sid = aid.split('_');
        /*swal({
         title: "Cancel Upload?",
         text: "Are you sure you want to cancel this upload?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
         confirmButtonText: "Yes",
         closeOnConfirm: true,
         html: true
         }, function () {
         s3obj[sid[1]].cancel();
         document.getElementById("videofile").value = "";
         }); */
        $('#headerConfirmModal').html('Cancel Upload?');
        $('#bodyConfirmModal').html("Are you sure you want to cancel this upload?");
        $('#confirmModal').modal({
            backdrop: 'static',
            keyboard: false
        }).one('click', '#confirmUGCYes', function (e) {
            s3obj[sid[1]].cancel();
            document.getElementById("videofile").value = "";
        });
        /*var cancel_confirm = confirm("Are you sure you want to cancel this upload?");
         if(cancel_confirm === true){
         s3obj[sid[1]].cancel();
         document.getElementById("videofile").value = "";
         } */

    });
}
/* Get Subcategory list*/
function getSubCategoryList() {
    $('.loading_div_subcategory').show();
    $('#content_subcategory_value').hide();
    var categoryValue = $('#content_category_value').val();
    var baseUrl = HTTP_ROOT;
    $.post(baseUrl + "/customForm/getSubCategory", {'category_value': categoryValue}, function (data) {
        $('#content_subcategory').html(data);
        $('.loading_div_subcategory').hide();
        $('#content_subcategory_value').show();
    });
}
function getCustomForm(obj) {
    var metadata_form_id = $(obj).find('option:selected').attr('data-value');
    var custom_form_id = $(obj).val();
    $.post(HTTP_ROOT + "/customForm/GetPosterDimention", {'custom_form_id': custom_form_id}, function (data) {
        VER_POSTER_WIDTH = data.vwidth;
        VER_POSTER_HEIGHT = data.vheight;
        if ((metadata_form_id == 6) && (data.pwidth && data.pheight)) {
            HOR_POSTER_WIDTH = data.pwidth;
            HOR_POSTER_HEIGHT = data.pheight;
        } else {
            HOR_POSTER_WIDTH = data.hwidth;
            HOR_POSTER_HEIGHT = data.hheight;
        }
        if ((metadata_form_id == 4) || (metadata_form_id == 9)) {
            $('#firetvimagediv').show();
            getFormContent('episode', metadata_form_id, custom_form_id);
        } else if ((metadata_form_id == 5) || (metadata_form_id == 10)) {
            $('#firetvimagediv').show();
            getFormContent('live', metadata_form_id, custom_form_id);
        } else if (metadata_form_id == 6) {
            $('#firetvimagediv').hide();
            getFormContent('physical', metadata_form_id, custom_form_id);
        } else {
            $('#firetvimagediv').show();
            getFormContent('basic', metadata_form_id, custom_form_id);
        }
    }, 'json');
}
/* Select the Form based on the content type */
function getFormContent(formType, ctypeid, custom_form_id) {
    isFormChanged = 0;
    $('#reqwidth').val(VER_POSTER_WIDTH);
    $('#reqheight').val(VER_POSTER_HEIGHT);
    $('.loading_div').show();
    $('#formContents').css({'opacity': '0.3'});
    var baseUrl = HTTP_ROOT;
    if (formType == 'episode') {
        $.post(baseUrl + "/customForm/getEpisodeForm", {'contentTypesId': ctypeid, 'custom_form_id': custom_form_id}, function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action', 'javascript:void(0);');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text=' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();
            $('#contentForm').removeAttr('onsubmit');
            $('#contentForm').attr('onsubmit', 'return submitEpisodeForm()');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#episode_date").datepicker();
        });
    } else if (formType == 'live') {
        $.post(baseUrl + "/customForm/getLiveStreamForm", {'contentTypesId': ctypeid, 'custom_form_id': custom_form_id}, function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action', HTTP_ROOT + '/admin/addChannel');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text=' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();
            $('#contentForm').removeAttr('onsubmit');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#release_date").datepicker();
            intializeTagsInput();
        });
    } else if (formType == 'physical') {
        $.post(baseUrl + "/customForm/getPhysicalForm", {'contentTypesId': ctypeid, 'custom_form_id': custom_form_id}, function (data) {
            $('#reqwidth').val(HOR_POSTER_WIDTH);
            $('#reqheight').val(HOR_POSTER_HEIGHT);
            $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
            $('#upload-sizes').html($('#reqimgsize').html());
            $('#contentForm').attr('action', 'javascript:void(0);');
            $("#avatar_preview_div").html('');
            Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text=' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
            Holder.run();
            $('#contentForm').removeAttr('onsubmit');
            $('#contentForm').attr('onsubmit', 'return submitPhysicalForm()');
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            $("[data-mask]").inputmask();
            $("#release_date").datepicker();
            intializeTagsInput();
        });
    } else {
        var content_types_id = 2;
        if (formType == 'basic') {
            content_types_id = 1;
        }
        $.post(baseUrl + "/customForm/getContentForm", {'content_types_id': content_types_id, 'contentTypesId': ctypeid, 'custom_form_id': custom_form_id}, function (data) {
            if (content_types_id == 2) {
                $('#contentForm').attr('action', HTTP_ROOT + '/admin/addContents');
                $('#reqwidth').val(HOR_POSTER_WIDTH);
                $('#reqheight').val(HOR_POSTER_HEIGHT);
                $('#reqimgsize').html(HOR_POSTER_WIDTH + 'X' + HOR_POSTER_HEIGHT);
                $('#upload-sizes').html($('#reqimgsize').html());
                $("#avatar_preview_div").html('');
                Holder.addImage('holder.js/' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT + '?text=' + HOR_POSTER_WIDTH + 'x' + HOR_POSTER_HEIGHT, "#avatar_preview_div");
                Holder.run();
                $('#contentForm').removeAttr('onsubmit');
                $('#contentForm').prop('onsubmit', false);
            } else {
                $('#contentForm').removeAttr('onsubmit');
                $('#contentForm').prop('onsubmit', false);
                $('#contentForm').removeAttr('action');
                $('#contentForm').attr('action', 'javascript:void(0);');
                $('#contentForm').prop('onsubmit', true);
                $('#contentForm').attr('onsubmit', 'return submitContentForm()');
                $("#avatar_preview_div").html('');
                $('#reqimgsize').html(VER_POSTER_WIDTH + 'X' + VER_POSTER_HEIGHT);
                $('#upload-sizes').html($('#reqimgsize').html());
                Holder.addImage('holder.js/' + VER_POSTER_WIDTH + 'x' + VER_POSTER_HEIGHT + '?text=' + VER_POSTER_WIDTH + 'x' + VER_POSTER_HEIGHT, "#avatar_preview_div");
                Holder.run();

            }
            $('.loading_div').hide();
            $('#formContents').html(data);
            $('#formContents').css({'opacity': '1'});
            intializeTagsInput();
            $("[data-mask]").inputmask();
            $("#release_date").datepicker({
                changeMonth: true,
                changeYear: true
            });
            $("#publish_date").datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)
            });
            $("#publish_time").timepicker({
                showInputs: false
            });
            $('#content_publish_date').click(function () {
                if (this.checked == false) {
                    $('#content_publish_date_div').hide();
                } else {
                    $('#content_publish_date_div').show();
                }
            });
        });
    }
    $("#avatar_preview_div").show();
    $("#previewcanvas").hide();
}

function intializeTagsInput() {
    if ($('#is_check_custom').val() == 1) {
        return false;
    }
    var genre = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=1',
            filter: function (list) {
                return $.map(list, function (genre) {
                    return {name: genre};
                });
            }
        }
    });
    genre.clearPrefetchCache();
    genre.initialize();

    $('#genre').tagsinput({
        typeaheadjs: {
            name: 'genre',
            displayKey: 'name',
            valueKey: 'name',
            source: genre.ttAdapter()
        }
    });
    var language = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=3',
            filter: function (list) {
                return $.map(list, function (language) {
                    return {name: language};
                });
            }
        }
    });
    language.clearPrefetchCache();
    language.initialize();

    $('#language').tagsinput({
        typeaheadjs: {
            name: 'language',
            displayKey: 'name',
            valueKey: 'name',
            source: language.ttAdapter()
        }
    });

    var censor_rating = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/getTags?taggable_type=2',
            filter: function (list) {
                return $.map(list, function (censor_rating) {
                    return {name: censor_rating};
                });
            }
        }
    });
    censor_rating.clearPrefetchCache();
    censor_rating.initialize();

    $('#censor_rating').tagsinput({
        typeaheadjs: {
            name: 'censor_rating',
            displayKey: 'name',
            valueKey: 'name',
            source: censor_rating.ttAdapter()
        }
    });
}

function seepreview(obj) {
    if ($("#x13").val() != "" && $("#x13").val() != undefined) {
        $(obj).html(JSLANGUAGE.pls_wait);
        //$('#ugc_poster').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj);
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#ugc_poster').modal('hide');
            $(obj).html(JSLANGUAGE.btn_next);
        } else {
            $(obj).html(JSLANGUAGE.pls_wait);
            //$('#ugc_poster').modal({backdrop: 'static', keyboard: false});
            if ($("#x13").val() != "" && $("#x13").val() != undefined) {
                posterpreview(obj);
            } else if ($('#x1').val() != "" && $("#x1").val() != undefined) {
                posterpreview(obj);
            } else {
                $('#ugc_poster').modal('hide');
                $(obj).html(JSLANGUAGE.btn_next);
            }
        }
    }
}

function fileSelectHandler() {
    $('#celeb_preview').css("display", "block");
    //$("#g_image_file_name").val('');
    //$("#g_original_image").val('');
    clearInfo();
    $(".jcrop-keymgr").css("display", "none");
    $("#editceleb_preview").hide();
    $("#celeb_preview").removeClass("hide");
    var reqwidth = parseInt($('#reqwidth').val());
    var reqheight = parseInt($('#reqheight').val());
    var aspectRatio = reqwidth / reqheight;
    var oFile = $('#celeb_pic')[0].files[0];
    var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        showUGCAlert('Alert!', 'Please select a valid image file (jpg and png are allowed)');
        //alert('Please select a valid image file (jpg and png are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#topbanner_submit_btn').attr('disabled', 'disabled');
        showUGCAlert('Alert!', 'File names with symbols such as \' , - are not supported');
        return;
    }
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (!rFilter.test(oFile.type)) {
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        showUGCAlert('Alert!', 'Please select a valid image file (jpg and png are allowed)');
        return;
    }
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);

        if (width < reqwidth || height < reqheight) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            showUGCAlert('Alert!', 'You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }
                $('#preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    }
}

function embedFromThirdPartyPlatform() {
    var str = $("#embed_url").val();
    var videoUrl1 = "youtube.com";
    var videoUrl2 = "vimeo.com";
    var videoUrl3 = 'm3u8';
    var videoUrl4 = 'iframe';
    var videoUrl5 = "dailymotion.com";
    if (str.length == 0) {
        $('#save-btn').attr('disabled', 'disabled');
        return false;
    } else if (str.length) {
        // console.log((str.indexOf(videoUrl4))+'.......'+(str.indexOf(videoUrl3))+'.....'+(str.indexOf(videoUrl2))+'.....'+(str.indexOf(videoUrl1)));
        if ((str.indexOf(videoUrl3) != -1) ||
                (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl1) != -1) ||
                (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl2) != -1) ||
                (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl5) != -1))
        {
            showLoader();
            var embed_text = $("#embed_url").val();
            var action_url = HTTP_ROOT + '/admin/embedFromThirdPartyPlatform';
            $('#embed_url_error').html('');
            var movie_id = $('#movie_id').val();
            var movie_stream_id = $('#movie_stream_id').val();
            $.post(action_url, {'thirdparty_url': embed_text, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
                showLoader(1);
                window.location.href = window.location.href;
            });
        } else {
            showUGCAlert('Alert!', "Oops! Provide a valid m3u8 or iframe embed url");
            //alert("Oops! Provide a valid m3u8 or iframe embed url");
            return false;
        }
    }
}
function embedThirdPartyPlatform() {
    var embedurl = $("#embed_url").val();
    if (embedurl.length == 0) {
        $('#save-btn').attr('disabled', 'disabled');
    } else {
        $('#save-btn').removeAttr('disabled');
        return true;
    }
}

function openUploadpopup(movie_id, movie_stream_id, content_types_id) {
    $("#addvideo_popup").modal('show');
    var name = $("#video_upload_" + movie_stream_id).attr('data-movie_name');

    $('input[type=file]').val('');
    $('#movie_name').val(name);
    $('#movie_stream_id').val(movie_stream_id);
    $('#movie_id').val(movie_id);
    $('#pop_movie_name').html(name);
    $('#content_types_id').html(content_types_id);
    $('#content_type').val(content_types_id);
    $('.error').html('');
}

function showUGCAlert(title, message) {
    $('#headerAlertModal').hide();
    if (title.trim()) {
        $('#headerAlertModal').html(title);
        $('#headerAlertModal').show();
    }
    $('#bodyAlertModal').html(message);
    $('#alertModal').modal();
}