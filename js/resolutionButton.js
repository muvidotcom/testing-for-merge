$(document).ready(function () {
    if (nAgt.match(/iPhone/i)) {
        $('.vjs-res-button').attr('style', 'width : 14em; position: relative');
        $(".vjs-res-button").bind('touchstart', function (event) {
            showSettingDiv();
            $("#video-res-sec").bind('touchstart', function (event) {
                showResDiv();
            });
        });
        $('#settingDiv').attr('style', 'right: 1% !important; bottom:40px');
        $('#video-res-sec').attr('style', 'right: 1% !important;  bottom:40px');
        $('#backButton').bind('touchstart', function () {
            if ($('#backbtnlink').val() != '')
                window.location.href = $('#backbtnlink').val();
            else
                parent.history.back();
            return false;
        });
        $(".vjs-control-content ").bind('touchstart', function (event) {
            if ($('.vjs-control-content:visible').length === 0) {
                $('.vjs-menu').show();
            } else {
                $('.vjs-menu').hide();
            }
        });
    }
});