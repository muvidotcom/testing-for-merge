// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(function() {
	var mov_name = "";
	//upload_file("","");
	$('.s3_uploader').S3Uploader();
//            remove_completed_progress_bar: false
            //progress_bar_target: $('.fileupload-progress')
//          });
//          $('.s3_uploader').bind('s3_upload_failed', function(e, content) {
//            return alert(content.filename + ' failed to upload');
//          });
});

function abort_request(){
  alert("jjjj");
  XHR.abort();
}

function upload_file(movie_name,movie_st_id){
	    mov_name = movie_name;
            var test_xhr = $('#s3_uploader_'+movie_st_id).S3Uploader({
            //remove_completed_progress_bar: false,
	    before_add: function(data){
              $("#file_name_"+movie_st_id).html(data.name);
              //$('#start_upload_'+movie_st_id).show();
              return confirm("Are you sure to upload "+data.name+" file for movie "+movie_name+ " ?" );
            },
            progress_bar_target: $('#all_progress_bar')
            //click_submit_target: $('#start_upload_'+movie_st_id)
          });
          $('#s3_uploader_'+movie_st_id).bind('s3_upload_failed', function(e, content) {
            return alert(content.filename + ' failed to upload');
          });
	  $('#s3_uploader_'+movie_st_id).bind('s3_uploads_start', function(e, content) {
		$("#prog_status_"+movie_st_id).show();
          });
	  $('#s3_uploader_'+movie_st_id).bind('s3_uploads_complete', function(e, content) {
                $("#prog_status_"+movie_st_id).hide();
          });
}
