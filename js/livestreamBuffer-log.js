$(document).ready(function () {
    var requestType = 'livestreaming';
    var unique_id = 0;
    var bufferLog_id = 0;
    var bufferLog_idtemp = 0;
    var bufferStart = 0;
    var bufferEnd = 0;
    var started = 1;
    player.ready(function () {
        if(started == 1 && feed_method == "push"){
            started = 0;
            $.ajax({
                url: HTTP_ROOT + '/videoLogs/videoBandwidthLog',
                type: 'post',
                data: {
                    movie_id: movie_id,
                    video_id: stream_id,
                    start_time:bufferStart,
                    end_time: bufferEnd,
                    resolution: "",
                    buff_log_id: bufferLog_id,
                    buff_log_idtemp: bufferLog_idtemp,
                    u_id: unique_id,
                    request_type: requestType,
                    browser_details: "",
                    totalBandwidth: parseInt(totalBandwidth / 1024)
                },
                success: function (res) {
                    var obj = JSON.parse(res);
                    bufferLog_id = obj.buffer_log_id;
                    unique_id = obj.u_id;
                    bufferLog_idtemp = obj.buff_log_idtemp;
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log("Error :: " + errorThrown);
                }
            });
            
        }
        setInterval(function(){
            //console.log("totalBandwidth" + totalBandwidth/(1024*1024));
            updateBufferLog();
        },60000);
    });
    function updateBufferLog() {
        if (parseFloat(bufferLog_id) && feed_method == 'push') {
            $.ajax({
                url: HTTP_ROOT + '/videoLogs/videoBandwidthLog',
                type: 'post',
                data: {
                    movie_id: movie_id,
                    video_id: stream_id,
					studio_id:studio_id,
                    start_time:bufferStart,
                    end_time: bufferEnd,
                    resolution: "",
                    buff_log_id: bufferLog_id,
                    buff_log_idtemp: bufferLog_idtemp,
                    u_id: unique_id,
                    request_type: requestType,
                    browser_details: "",
                    totalBandwidth: parseInt(totalBandwidth / 1024)
                },
                success: function (res) {
                    var obj = JSON.parse(res);
                    bufferLog_id = obj.buffer_log_id;
                    unique_id = obj.u_id;
                    bufferLog_idtemp = obj.buff_log_idtemp;
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log("Error :: " + errorThrown);
                }
            });
        }
    }
});


