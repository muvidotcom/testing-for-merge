$(document).ready(function () {
var buff_log_id = 0;
var buf_log_flag = 1;
var buff_log_idtemp = 0;
var unique_id = 0;
var total_duration;
var video_resolution = 0;
var bf_previousTime = 0;
var bf_currentTime = 0;
var bf_seekStart = null;
var bf_forChangeRes = 0;
var bf_previousBufferEnd = 0;
var bf_bufferDurationaas = 0;
var bf_bufferenEnddd = 0;
var bf_bufferenStart = 0;
var already_logged = 0;
player.ready(function () {		
        player.on('loadedmetadata', function () {			
            total_duration = player.duration();			
            if (typeof player.currentResolution === "function") {
				var resolution_obj = player.currentResolution();                
                 video_resolution = resolution_obj.sources[0].res;
            } else {
                 video_resolution = 144;
            }	
			console.log("Come Under Load Meta Data of buffer log"+video_resolution+"Duration="+total_duration);			
            buff_log_idtemp = 0;
            bf_bufferenStart = 0;
            bf_bufferenEnddd = 0;
            updateBuffered();
        });
        if ($('#backButton').length) {
            $('#backButton').click(function(){
                var player_buffered = player.buffered();
                var buffLen = player_buffered.length;
                bf_bufferenEnddd = player_buffered.end(buffLen-1);
                bf_bufferenStart = player_buffered.start(buffLen-1);
                updateBuffered();
            });
		}
        if (window.history && window.history.pushState) {
            window.history.pushState('forward', null, '');
            $(window).on('popstate', function() {
                var player_buffered = player.buffered();
                var buffLen = player_buffered.length;
                bf_bufferenEnddd = player_buffered.end(buffLen-1);
                bf_bufferenStart = player_buffered.start(buffLen-1);
                updateBuffered();
            });
		}
        player.on("ended", function() {
            var player_buffered = player.buffered();
            var buffLen = player_buffered.length;
            bf_bufferenEnddd = player_buffered.end(buffLen-1);
            bf_bufferenStart = player_buffered.start(buffLen-1);
            updateBuffered();
        });
        player.on('timeupdate', function () {
            bf_previousTime = bf_currentTime;
            bf_currentTime = player.currentTime();			
            bf_previousBufferEnd = bf_bufferDurationaas;
            var buffr = player.buffered(); 			
            var buffLen = buffr.length;
            buffLen = buffLen - 1;
            bf_bufferDurationaas = buffr.end(buffLen);
			//console.log("End time"+buffr.end(0)+"bf_bufferDurationaas"+bf_bufferDurationaas);  			
        });
        setInterval(function () {           
            var player_buffered = player.buffered();
            var buffLen = player_buffered.length;
            bf_bufferenEnddd = player_buffered.end(buffLen-1);
            bf_bufferenStart = player_buffered.start(buffLen-1);
			console.log("start time="+bf_bufferenStart+"End time"+bf_bufferenEnddd+"bf_bufferDurationaas"+bf_bufferDurationaas);
            if(buff_log_idtemp){
				if(bf_bufferenStart>=0 && bf_bufferenEnddd >0){
					updateBuffered();
				}
			}
        }, 60000);
        player.on("seeking", function () {			
            var currTim = player.currentTime();   
			//console.log("Bufferlog Seeking="+currTim);
            if (bf_forChangeRes === 0) {
				//console.log("Previous buffer end ======"+bf_previousBufferEnd+"Bufferlog Seeking="+currTim);
                if (bf_previousBufferEnd < currTim) {
                    if (bf_seekStart === null) {
                        bf_seekStart = bf_previousTime;
                        var player_buffered = player.buffered();
                        var buffLen = player_buffered.length;
                        bf_bufferenEnddd = player_buffered.end(buffLen-1);
                        bf_bufferenStart = player_buffered.start(buffLen-1);
						//console.log("Seeking start time="+bf_bufferenStart+"End time"+bf_bufferenEnddd+"bf_bufferDurationaas"+bf_bufferDurationaas);
						if(bf_bufferenStart>=0 && bf_bufferenEnddd >0){
							//console.log("Come Under Seeking condition="+bf_bufferenStart+"Bufferended"+bf_bufferenEnddd);							
							updateBuffered();							
						}
                    }
                }
            }
        });
        player.on('seeked', function () {
			//console.log("Come Under Bufferlog Seeking END="+player.currentTime());
            bf_seekStart = null;
        });
        player.on('resolutionchange', function () {			
            bf_forChangeRes = 123;
            bf_seekStart = bf_previousTime;           
            updateBuffered();
            bf_forChangeRes = 0;
        });
    });
    function updateBuffered() {		
        var browser_details = getBrowserDetailsForLog();
        browser_details = browser_details.name+"/version("+browser_details.version+")";
        var player_buffered = player.buffered();		
        var buffLen = player_buffered.length;       
        var buff_dur = 0;		
        if (player_buffered) {			
            if(bf_bufferenStart == 0 && bf_bufferenEnddd == 0){
                buff_log_idtemp = 0;
            }
            if(already_logged == 1 && buffLen == 0){
                return true;
            }
            if(already_logged == 0 && buffLen >=1){
                already_logged = 1;
            }			
            for( var i = 0; i < buffLen; i++){
                buff_dur = buff_dur + (player_buffered.end(i) - player_buffered.start(i));
            }
            if(buff_log_id > 0 && buff_log_idtemp == 0){
                buff_dur = buff_dur - (player_buffered.end(0) - player_buffered.start(0));
            }
            if (buf_log_flag || buff_log_id >0) {
		$.ajax({
                    type: 'POST',
                    url: URL + '/videoLogs/videoBandwidthLog',
                    dataType: "json",
                    data: {movie_id: movie_id, video_id: stream_id, start_time: bf_bufferenStart, end_time: bf_bufferenEnddd, buff_log_id: buff_log_id, resolution: video_resolution, u_id: unique_id, duration: total_duration, studio_id: studio_id, buff_log_idtemp: buff_log_idtemp, buff_dur:buff_dur,browser_details:browser_details},
                    success: function (res) {  						
			if(res){
                            buff_log_id = res.buffer_log_id;
                            unique_id = res.u_id;
                            buff_log_idtemp = res.buff_log_idtemp;
                        }
                    }
                });  
				buf_log_flag = 0;
            }
        }
    }
	
	function getBrowserDetailsForLog() {
		var useraccount=navigator.userAgent,tem,res_obj=useraccount.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
		if(/trident/i.test(res_obj[1])){
			tem=/\brv[ :]+(\d+)/g.exec(useraccount) || []; 
			return {name:'IE',version:(tem[1]||'')};
		}   
		if(res_obj[1]==='Chrome'){
			tem=useraccount.match(/\bOPR|Edge\/(\d+)/)
			if(tem!=null){
				temp = tem[0].split('/');
				if(temp[0].toLowerCase().match('edge')){
					return {name:'Edge', version:tem[1]};
				}else{
					return {name:'Opera', version:tem[1]};
				}
			}
		}   
		res_obj=res_obj[2]? [res_obj[1], res_obj[2]]: [navigator.appName, navigator.appVersion, '-?'];
		if((tem=useraccount.match(/version\/(\d+)/i))!=null) {res_obj.splice(1,1,tem[1]);}
		return { name: res_obj[0], version: res_obj[1]};
	}
});  