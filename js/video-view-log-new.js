$(document).ready(function () {
	var log_id_temp = 0;
	var length = 0;
	var resume_time = 0;
	//Previous seek end time
	var tTime = 0;
	var vf_previousTime = 0;
	var vf_currentTime = 0;
	var log_seekStart = null;
	var check_played_from = '';
	//Flag to Check log first time
	var log_check = 1;
	var check_enablewatchduration = 0;	
	if (typeof played_from != 'undefined') {
		check_played_from = played_from;
	}
	if (typeof enableWatchDuration != 'undefined') {
		check_enablewatchduration = enableWatchDuration;
	}
	var user_id_of_viewer_js = 0;
	if (typeof user_id_of_viewer != 'undefined') {
		user_id_of_viewer_js = user_id_of_viewer;
	}
	if (window.history && window.history.pushState) {
		if ((typeof browserName == 'undefined') || (typeof browserName != 'undefined' && browserName != 'Safari')) {
			window.history.pushState('forward', null, '');
			$(window).on('popstate', function () {
				updateViewLogIntervals();
				window.history.back();
			});
		}
	}	
	player.ready(function () {				
		if ($('#backButton').length) {
			$('#backButton').click(function () {				
				console.log("Call view log function");
				updateViewLogIntervals();
			});
		}
		player.on("play", function () {
			//When a user play again after ended a video
                    if(ended == 1){
                        log_check = 1;
                        started = 0;
                        log_id = 0;
                        ended = 0;
                    }
			if (log_check == 1) {
				resume_time = player.currentTime();				
				if (play_status == 0 && started === 0) {					
					log_check = 0;						
					tTime = player.currentTime();
					$.post(video_view_log_url, {log_id_temp: log_id_temp, movie_id: movie_id, episode_id: stream_id, status: "start", log_id: log_id, percent: percen, played_length: 0, video_length: player.duration(), 'studio_id': studio_id, 'resume_time': resume_time, 'played_from': check_played_from, 'user_id_of_viewer': user_id_of_viewer_js, 'enableWatchDuration': check_enablewatchduration}, function (res) {
						if (res['log_id'] > 0) {
							log_id_temp = 0;
							log_id = res['log_id'];
							if (res['avail_time'] == 0) {
								showMessage();
							}
						} else {
							history.go(-1);
						}
					}, 'json');
					$.post('/user/setWatchPeriod', {movie_id: movie_id, 'studio_id': studio_id}, function (res) {
					});
					started = 1;
					ended = 0;
					logged = 0					
				}
			}
		});
		if (is_live == 0) {
			player.on("ended", function () {
				console.log("Ended video view log section");
				if (ended === 0) {
					$.post(video_view_log_url, {log_id_temp: log_id_temp, movie_id: movie_id, episode_id: stream_id, status: "complete", log_id: log_id, played_length: (player.currentTime() - tTime), 'studio_id': studio_id, 'resume_time': tTime, 'played_from': check_played_from, 'enableWatchDuration': check_enablewatchduration}, function (res) {
						log_id_temp = 0;
						log_id = 0;
					}, 'json');
					ended = 1;
					started = 0;
				}
			});
		}
		player.on('timeupdate', function () {
			if ((vf_previousTime + 2 > vf_currentTime && vf_previousTime <= vf_currentTime) || (vf_previousTime - 2 < vf_currentTime && vf_previousTime >= vf_currentTime)) {
				vf_previousTime = vf_currentTime;
			}
			vf_currentTime = player.currentTime();
			if(ended == 1){
				log_check = 1;
				started = 0;
				log_id = 0;
				ended = 0;
            }
			if (log_check == 1) {
				resume_time = player.currentTime();				
				if (play_status == 0 && started === 0) {					
					log_check = 0;					
					tTime = player.currentTime();					
					$.post(video_view_log_url, {log_id_temp: log_id_temp, movie_id: movie_id, episode_id: stream_id, status: "start", log_id: log_id, percent: percen, played_length: 0, video_length: player.duration(), 'studio_id': studio_id, 'resume_time': resume_time, 'played_from': check_played_from, 'user_id_of_viewer': user_id_of_viewer_js, 'enableWatchDuration': check_enablewatchduration}, function (res) {
						if (res['log_id'] > 0) {
							log_id_temp = 0;
							log_id = res['log_id'];
							if (res['avail_time'] == 0) {
								showMessage();
							}
						} else {
							history.go(-1);
						}
					}, 'json');
					$.post('/user/setWatchPeriod', {movie_id: movie_id, 'studio_id': studio_id}, function (res) {
					});
					started = 1;
					ended = 0;
					logged = 0					
				}
			}
		});
		setInterval(function () {
			length = player.currentTime() - tTime;
			tTime = player.currentTime();
			if (length != 0) {
				updateViewLogIntervals();
			}
		}, 60000);
		player.on('seeking', function () {
			if (log_seekStart === null && log_id >= 1) {
				log_seekStart = vf_previousTime;
				length = log_seekStart - tTime;
				updateViewLogIntervals();
			}
		});

		player.on('seeked', function () {
			//tTime = player.vf_currentTime();
			vf_previousTime = vf_currentTime;
			if (started === 1 && log_id >= 1) {
				log_id_temp = 0;
				log_seekStart = null;
				length = 0;
			}
		});
	});
	function updateViewLogIntervals() {
		tTime = player.currentTime();
		if (length < 0) {
			length = 0;
		}
        if (started === 1 && ended === 0 && length != 0 && length < 61) {
            		$.post(video_view_log_url, {log_id_temp: log_id_temp, movie_id: movie_id, episode_id: stream_id, status: "halfplay", log_id: log_id, percent: percen, played_length: length, 'studio_id': studio_id, 'resume_time': player.currentTime(), 'played_from': check_played_from, 'enableWatchDuration': check_enablewatchduration}, function (res) {
				log_id_temp = 0;
				log_id = res['log_id'];
				if (res['avail_time'] == 0) {
					showMessage();
				}
			}, 'json');
		}
	}
	//if daily watch duration exhausted 
	function showMessage() {
		player.pause();
		$('#messge').modal('show');
	}
});