// Multipart Upload Code
var s3upload = null;
var s3obj=new Array();
var size = '';
var sizeleft = 0;
var sizeName = '';
var seconds;
function timeCal(seconds){
    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var sec = seconds - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (sec < 10) {sec = "0"+sec;}
    var  timeLeft = hours+':'+minutes+':'+sec;
    return timeLeft;
}
function upload(user, pass,stream_id,movie_name,movie_st_id,content_types_id,filename,type) {
	var contentType = content_types_id;
	//var xhr = new XMLHttpRequest({mozSystem: true});
	var filename = $('input[type=file]').val().split('\\').pop();
	if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
		swal("Sorry! You are using an older or unsupported browser. Please update your browser");
		return;
	}
	
	sizeleft = 0;
	var timeLeft = '00:00:00';
	var sizeKb = "";
	var sizeLeftKb;
	var file = $('#videofile')[0].files[0];
	size = Math.round(file.size/1000);
	sizeKb=size;
	timeLeft = ((sizeKb/1000)/userinternetSpeed).toFixed(0);
	if(timeLeft < 4){
		timeLeft = 4;
	}
	if((size/1000)<6){
		speed=userinternetSpeed+"mbps";
	}
	timeLeft = timeCal(timeLeft);
	sizeName = 'KB';
	if(size > 1000){
		size = Math.round(size/1000);
		var sizeName = 'MB';
	}
	if(size > 1000){
		size = size/1000;
		size = size.toFixed(2);
		var sizeName = 'GB';
	}
	var speed="0 kbps";
	var speedMbps;
	var startTime = (new Date()).getTime();
	var filename = file.name;
        filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g,"_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + filename.replace(/^.*\./, '');
	file.name = filename;
	var videoUploadTypeToS3 = "";
        if(typeof video_upload_type != 'undefined' && video_upload_type != ''){
            videoUploadTypeToS3 = video_upload_type;
        }
	s3upload = new S3MultiUpload(file, {user: user, pass: pass,'movie_stream_id':stream_id,'movie_id':movie_st_id,'type':type,'uploadType':videoUploadTypeToS3});
	s3upload.onServerError = function(command, jqXHR, textStatus, errorThrown) {
		if (jqXHR.status === 403) {
			swal("Sorry you are not allowed to upload");
		} else {
			if(command=='CompleteMultipartUpload'){
				window.setTimeout(function() {
					s3upload.completeMultipartUpload();
				}, s3upload.RETRY_WAIT_SEC * 1000);
			}else if(command=='CreateMultipartUpload'){
				window.setTimeout(function() {
					s3upload.createMultipartUpload();
				}, s3upload.RETRY_WAIT_SEC * 1000);
			}
			console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
		}
	};

	s3upload.onS3UploadError = function(xhr) {
	  s3upload.waitRetry();
	  console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
	};

	//s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
	s3upload.onProgressChanged = function(progPercent) {
		var endTime = (new Date()).getTime();
		var duration = (endTime-startTime)/1000;
		if(progPercent==100){
			$('#upload_'+stream_id+' .progress-bar-success').css('width','100%');
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(size);
			$('#upload_'+stream_id+' .timeRemaining').html("00:00:00");
		}else{
			var progper = parseFloat($('#upload_'+stream_id+' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
			$('#upload_'+stream_id+' .progress-bar-success').attr('percent',progper);
				
			$('#upload_'+stream_id+' .progress-bar-success').css('width',progper+'%');
			if(sizeName == 'GB'){
				sizeleft = (progper * size)/100;
				sizeleft = sizeleft.toFixed(2);
			}else{
				sizeleft = Math.round((progper * size)/100);
			}
			if(sizeName == 'GB'){
				 speed=((sizeleft*1000*1000)/duration).toFixed(2) ;
				 sizeLeftKb = sizeleft*1000*1000;
			}
			if(sizeName == 'MB'){
				speed=((sizeleft*1000)/duration).toFixed(2) ; 
				sizeLeftKb = sizeleft*1000;
			}
			if(sizeName == 'KB'){
				 speed=((sizeleft)/duration).toFixed(2);
				 sizeLeftKb=sizeleft;
			}
			speedMbps=(speed/1000).toFixed(2);
			seconds = ((sizeKb-sizeLeftKb)/speed).toFixed(0);
		   timeLeft = timeCal(seconds);
			if(speed > 999){
				speed=(speed/1000).toFixed(2)+"mbps";
			}
			else{
				speed=speed + "kbps";
			}
                       
			$('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
			$('#upload_'+stream_id+' .uploadSpeed').html(speed);
			$.post(HTTP_ROOT+"/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
			$('#upload_'+stream_id+' .timeRemaining').html(timeLeft);
		}
	};

	s3upload.onUploadCompleted = function() {
                var convText = '';
		$('#upload_'+stream_id).remove();
		var purl = '';var playtext='';
		//purl = HTTP_ROOT + "/video/play_video?movie="+movie_st_id+"&movie_stream_id="+stream_id+"&preview=1";
		//playtext = '<a href="'+purl+'" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp;';
		if(videoUploadTypeToS3=='physical'){
			convText = '<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>';
			$('#trailerbtn'+movie_st_id).html(convText);
		}
		if(contentType!='undefined'){
			convText = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
			if($("#encodingStreamId").length) {
				if($('#encodingStreamId').is(":empty")){
					$('#encodingStreamId').html(stream_id);
				} else{
					$('#encodingStreamId').html($('#encodingStreamId').html() + "," + stream_id);
				}
			}
		}
        //location.reload();
        //var convText = '';
		//var convText = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><div class="col-lg-12 no-padding"><div class="col-lg-1 no-padding"><span class="glyphicon glyphicon-refresh glyphicon-spin glyphicon-2x"></span></div> <div class="col-lg-11 adminaction-icon">Encoding in progress</div></div><div class="cb" style="clear: both;"></div> </a>'; 
		//$('#'+stream_id).html(playtext);
		$('#'+stream_id).html(convText);
                $('#video_upload_'+stream_id).empty();
		$('#videoCorrupt'+stream_id).hide();
		if($('#all_progress_bar').is(":empty")){
			$('#dprogress_bar').hide();
		}
		if(contentType != 1){
			$.post(HTTP_ROOT+"/cron/getThumbnail");
		}
		console.log("Congratz, upload is complete now");
	};
	s3upload.onUploadCancel = function() {
		$('#upload_'+stream_id).remove();
		if($('#all_progress_bar').is(":empty")){
			$('#dprogress_bar').hide();
		}
		if(videoUploadTypeToS3=='physical'){
			var ptext = '<h5><a href="javascript:void(0);" onclick="openUploadpopup(\''+stream_id+'\',0);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></h5>';
			$('#trailerbtn'+movie_st_id).html(ptext);
		}
		if(content_types_id == 5 || content_types_id == 6){
                    var utext = '<a href="javascript:void(0);" onclick="openUploadpopup('+movie_st_id+','+stream_id+','+content_types_id+');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload audio</a>'; 
		}else {
		var utext = '<a href="javascript:void(0);" onclick="openUploadpopup('+movie_st_id+',\''+movie_name+'\','+stream_id+','+content_types_id+');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload video</a>'; 
		//var utext = '<a href="javascript:void(0);" onclick="openUploadpopup('+movie_st_id+',\''+movie_name+'\','+stream_id+','+content_types_id+');"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a>&nbsp;';
		}
		$('#'+stream_id).html(utext);
		console.log("Upload Cancelled..");
	};
	
//Start s3 Multipart upload	
	s3upload.start();
	s3obj[stream_id] = s3upload;
	var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">'+sizeleft+'</span>/'+size+' '+sizeName+') (<span class="uploadSpeed">'+speed+'</span>)<span class="timeRemaining" style="float:right">'+timeLeft+'</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
        $('#all_progress_bar').append(progressbar);
        $('#cancel_' + stream_id).on('click', function () {
            aid = this.id;
            sid = aid.split('_');
            swal({
                title: "Cancel Upload?",
                text: "Are you sure you want to cancel this upload?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            }, function () {
                s3obj[sid[1]].cancel();
                document.getElementById("videofile").value = "";
                //$('#dprogress_bar').html('');
                //$('#dprogress_bar').hide();
            });
            
        });
}

// Checking File extenson and size

function checkfileSize(type){
	var movie_name = $('#movie_name').val();
	var stream_id = $('#movie_stream_id').val();
	var movie_st_id = $('#movie_id').val();
	var filename = $('#videofile').val().split('\\').pop();
	var content_types_id = $('#content_type').val();
	var extension = filename.replace(/^.*\./, '');
	if (extension == filename) {
		extension = '';
	} else {
		extension = extension.toLowerCase();
	}
        if(content_types_id == 5 || content_types_id == 6){
	switch (extension) {
		case 'mp3':
		break;
        case 'aac':
        break;
        case 'wav':
        break;
        case 'aiff':
        break;
        case 'm4a':
        break;
        case 'm4b':
        break;
        case 'm4p':
        break;
        case 'dvf':
        break;
        case 'raw':
        break;
        case 'wma':
        break;
        case 'webm':
        break;
		default:
                    // Cancel the form submission
                    swal('Sorry! This audio format is not supported. \n MP3 format audio are allowed to upload');
                    return false;
                    break; 
            }
        }else{
            switch (extension) {
		case 'mp4':
		break;
		case 'mov':
		break;
		case 'mkv':
		break;
		case 'flv':
		break;
		case 'vob':
		break;
		case 'm4v':
		break;
		case 'avi':
		break;
		case '3gp':
		break;
		case 'mpg':
		break;
                case 'wmv':
                break;
		default:
			// Cancel the form submission
			swal('Sorry! This video format is not supported. \n MP4/MOV/MKV/FLV/VOB/M4V/AVI/3GP/MPG/WMV format video are allowed to upload');
			return false;
			break; 
	}
        }
        swal({
            title: "Upload File?",
            text: "Are you sure to upload "+filename+" file for  "+movie_name+" ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#10CFBD",
            customClass: "cancelButtonColor",
            confirmButtonText: "Upload",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true,
            html:true
          },
          function(isConfirm){
            if (isConfirm) {
              $("#addvideo_popup").modal('hide');
              $("#addvideo_popup_physical").modal('hide');
		if(!$('#dprogress_bar').is(":visible")){
			$('#dprogress_bar').show();
		}
                if(content_types_id == 5 || content_types_id == 6){
                    var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload audio</a></h5>";
                }else{
                var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a></h5>";
                }
		$('#'+stream_id).html(rtext);
		/*if(type=='physical'){
			$('#trailerbtn'+movie_st_id).html('<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>');
		}*/
		upload('user','pass',stream_id,movie_name,movie_st_id,content_types_id,filename,type);
		return true;
            } else {
               return false;
            }
          });
	
}
$.fn.bindFirst = function(name, fn) {
    // bind as you normally would
    // don't want to miss out on any jQuery magic
    this.on(name, fn);

    // Thanks to a comment by @Martin, adding support for
    // namespaced events too.
    this.each(function() {
        var handlers = $._data(this, 'events')[name.split('.')[0]];
        // take out the handler we just inserted from the end
        var handler = handlers.pop();
        // move it at the beginning
        handlers.splice(0, 0, handler);
    });
};
// Restricting click on any link while upload is in progress
$(function(){
	$('html a:not(a.confirm,a.upload-video,a.embed-box)').bindFirst('click',function(e) {   
	
	if($('#dprogress_bar').is(":visible")){
		e.stopPropagation();
		e.preventDefault();
		var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi Studio on a new tab to continue with other actions.';
		var location = $(this).attr('href');
		bootbox.hideAll();
		bootbox.confirm({
			title: "Upload In progress",
			message: msg,
			buttons: {
				'confirm': {
					label: 'Stop Upload',
					className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
				},
				'cancel': {
					label: 'Ok',
					className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
				}
			},callback: function (confirmed) {
				if(confirmed) {
					bootboxConfirm =1;
					window.location.replace(location);
				}
			}
		}); 
	}
	});
});

function checkAllfileSize(type){
	var movie_name = $('#movie_name').val();
	var stream_id = $('#movie_stream_id').val();
	var movie_st_id = $('#movie_id').val();
	var filename = $('#allfile').val().split('\\').pop();
	var content_types_id = $('#content_type').val();
	var extension = filename.replace(/^.*\./, '');
	if (extension == filename) {
		extension = '';
	} else {
		extension = extension.toLowerCase();
	}
	var myArray = ['html','exe','php','net','java','script']
	var flag = $.inArray(extension, myArray);
	if(flag != -1){
		 swal('Sorry! This file format is not supported.');
		 return false;
	}
	swal({
		title: "Upload File?",
		text: "Are you sure to upload "+filename+" file for  "+movie_name+" ?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#10CFBD",
		customClass: "cancelButtonColor",
		confirmButtonText: "Upload",
		cancelButtonText: "Cancel",
		closeOnConfirm: true,
		closeOnCancel: true,
		html:true
	  },
	function(isConfirm){
		if (isConfirm) {
			$("#addvideo_popup").modal('hide');
			$("#addvideo_popup_physical").modal('hide');
			if(!$('#dprogress_bar').is(":visible")){
				$('#dprogress_bar').show();
			}
			if(content_types_id == 5 || content_types_id == 6){
				var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload audio</a></h5>";
			}else{
			var rtext = "<h5><a href='javascript:void(0);' class='disabled-upload'><i class='fa fa-upload'></i>&nbsp;&nbsp;&nbsp; Upload video</a></h5>";
			}
			$('#'+stream_id).html(rtext);                
			uploadAllFile('user','pass',stream_id,movie_name,movie_st_id,content_types_id,filename,type);
			return true;
		} else {
		   return false;
		}
	});	
}
function uploadAllFile(user, pass,stream_id,movie_name,movie_st_id,content_types_id,filename,type) {
	//var video_upload_type = 'filegallery';
	var contentType = content_types_id;
	//var xhr = new XMLHttpRequest({mozSystem: true});
	var filename = $('input[type=file]').val().split('\\').pop();
	if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
		swal("Sorry! You are using an older or unsupported browser. Please update your browser");
		return;
	}

	var sizeleft = 0;
	var timeLeft = '00:00:00';
	var sizeKb;
	var sizeLeftKb;
	var file = $('#allfile')[0].files[0];
	size = Math.round(file.size / 1000);
	sizeKb = size;
	timeLeft = ((sizeKb / 1000) / userinternetSpeed).toFixed(0);
	if (timeLeft < 4) {
		timeLeft = 4;
	}
	if ((size / 1000) < 6) {
		speed = userinternetSpeed + "mbps";
	}
	timeLeft = timeCal(timeLeft);
	sizeName = 'KB';
	if (size > 1000) {
		size = Math.round(size / 1000);
		var sizeName = 'MB';
	}
	if (size > 1000) {
		size = size / 1000;
		size = size.toFixed(2);
		var sizeName = 'GB';
	}
	//by me
	var speed = "0 kbps";
	var speedMbps;
	var startTime = (new Date()).getTime();
	var filename = file.name;
	filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g, "_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + filename.replace(/^.*\./, '');
	file.name = filename;
	var videoUploadTypeToS3 = "";
	if (typeof video_upload_type != 'undefined' && video_upload_type != '') {
		videoUploadTypeToS3 = video_upload_type;
	}
	s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'movie_id': movie_st_id, 'type': type, 'uploadType': videoUploadTypeToS3});
	s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
		if (jqXHR.status === 403) {
			swal("Sorry you are not allowed to upload");
		} else {
			if (command == 'CompleteMultipartUpload') {
				window.setTimeout(function () {
					s3upload.completeMultipartUpload();
				}, s3upload.RETRY_WAIT_SEC * 1000);
			} else if (command == 'CreateMultipartUpload') {
				window.setTimeout(function () {
					s3upload.createMultipartUpload();
				}, s3upload.RETRY_WAIT_SEC * 1000);
			}
			console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
		}
	};

	s3upload.onS3UploadError = function (xhr) {
		s3upload.waitRetry();
		console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
	};

	//s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
	s3upload.onProgressChanged = function (progPercent) {
		var endTime = (new Date()).getTime();
		var duration = (endTime - startTime) / 1000;
		if (progPercent == 100) {
			$('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
			$('#upload_' + stream_id + ' .uploadFileSizeProgress').html(size);
			$('#upload_' + stream_id + ' .timeRemaining').html("00:00:00");
		} else {
			var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
			$('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

			$('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
			if (sizeName == 'GB') {
				sizeleft = (progper * size) / 100;
				sizeleft = sizeleft.toFixed(2);
			} else {
				sizeleft = Math.round((progper * size) / 100);
			}
			if (sizeName == 'GB') {
				speed = ((sizeleft * 1000 * 1000) / duration).toFixed(2);
				sizeLeftKb = sizeleft * 1000 * 1000;
			}
			if (sizeName == 'MB') {
				speed = ((sizeleft * 1000) / duration).toFixed(2);
				sizeLeftKb = sizeleft * 1000;
			}
			if (sizeName == 'KB') {
				speed = ((sizeleft) / duration).toFixed(2);
				sizeLeftKb = sizeleft;
			}
			speedMbps = (speed / 1000).toFixed(2);
			seconds = ((sizeKb - sizeLeftKb) / speed).toFixed(0);
			timeLeft = timeCal(seconds);
			if (speed > 999) {
				speed = (speed / 1000).toFixed(2) + "mbps";
			}
			else {
				speed = speed + "kbps";
			}

			$('#upload_' + stream_id + ' .uploadFileSizeProgress').html(sizeleft);
			$('#upload_' + stream_id + ' .uploadSpeed').html(speed);
			$.post(HTTP_ROOT + "/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {
			});
			$('#upload_' + stream_id + ' .timeRemaining').html(timeLeft);
		}
	};

	s3upload.onUploadCompleted = function () {
		var convText = '';
		$('#upload_' + stream_id).remove();
		var purl = '';
		var playtext = '';
		//purl = HTTP_ROOT + "/video/play_video?movie="+movie_st_id+"&movie_stream_id="+stream_id+"&preview=1";
		//playtext = '<a href="'+purl+'" target="_blank"><span class="glyphicon glyphicon-play-circle"></span></a>&nbsp;';
		if (videoUploadTypeToS3 == 'physical') {
			convText = '<h5><a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
			$('#trailerbtn' + movie_st_id).html(convText);
		}
		if(contentType!='undefined'){
			convText = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
			if($("#encodingStreamId").length) {
				if($('#encodingStreamId').is(":empty")){
					$('#encodingStreamId').html(stream_id);
				} else{
					$('#encodingStreamId').html($('#encodingStreamId').html() + "," + stream_id);
				}
			}
		}
		//location.reload();
		//var convText = '';
		//var convText = '<a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><div class="col-lg-12 no-padding"><div class="col-lg-1 no-padding"><span class="glyphicon glyphicon-refresh glyphicon-spin glyphicon-2x"></span></div> <div class="col-lg-11 adminaction-icon">Encoding in progress</div></div><div class="cb" style="clear: both;"></div> </a>'; 
		//$('#'+stream_id).html(playtext);
		$('#' + stream_id).html(convText);
		$('#video_upload_' + stream_id).empty();
		$('#videoCorrupt' + stream_id).hide();
		if ($('#all_progress_bar').is(":empty")) {
			$('#dprogress_bar').hide();
		}
		if (contentType != 1) {
			$.post(HTTP_ROOT + "/cron/getThumbnail");
		}
		console.log("Congratz, upload is complete now");
	};
	s3upload.onUploadCancel = function () {
		$('#upload_' + stream_id).remove();
		if ($('#all_progress_bar').is(":empty")) {
			$('#dprogress_bar').hide();
		}
		if (videoUploadTypeToS3 == 'physical') {
			var ptext = '<h5><a href="javascript:void(0);" onclick="openUploadpopup(\'' + stream_id + '\',0);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></h5>';
			$('#trailerbtn' + movie_st_id).html(ptext);
		}
		if (content_types_id == 5 || content_types_id == 6) {
			var utext = '<a href="javascript:void(0);" onclick="openUploadpopup(' + movie_st_id + ',' + stream_id + ',' + content_types_id + ');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload audio</a>';
		} else {
			var utext = '<a href="javascript:void(0);" onclick="openUploadpopup(' + movie_st_id + ',' + stream_id + ',' + content_types_id + ');"><i class="fa fa-upload"></i>&nbsp;&nbsp; Upload video</a>';
			//var utext = '<a href="javascript:void(0);" onclick="openUploadpopup('+movie_st_id+',\''+movie_name+'\','+stream_id+','+content_types_id+');"><span class="glyphicon glyphicon-upload" title="Upload Video"></span></a>&nbsp;';
		}
		$('#' + stream_id).html(utext);
		console.log("Upload Cancelled..");
	};

	//Start s3 Multipart upload	
	s3upload.start();
	s3obj[stream_id] = s3upload;
	var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">' + sizeleft + '</span>/' + size + ' ' + sizeName + ') (<span class="uploadSpeed">' + speed + '</span>)<span class="timeRemaining" style="float:right">' + timeLeft + '</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
	$('#all_progress_bar').append(progressbar);
	$('#cancel_' + stream_id).on('click', function () {
		aid = this.id;
		sid = aid.split('_');
		swal({
			title: "Cancel Upload?",
			text: "Are you sure you want to cancel this upload?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
			confirmButtonText: "Yes",
			closeOnConfirm: true,
			html: true
		}, function () {
			s3obj[sid[1]].cancel();
			document.getElementById("allfile").value = "";
			//$('#dprogress_bar').html('');
			//$('#dprogress_bar').hide();
		});
	});
}
