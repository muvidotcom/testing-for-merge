$(document).ready(function () {
var buff_log_id = 0;
var log_id = -1;
var buff_log_idtemp = 0;
var unique_id = 0;
var total_duration;
var video_resolution;
var previousTime = 0;
var currentTime = 0;
var seekStart = null;
var forChangeRes = 0;
var previousBufferEnd = 0;
var bufferDurationaas = 0;
var bufferenEnddd = 0;
var previous_len = 0;
var bufferenStart = 0;
var already_logged = 0;
var device_type = '';
if(typeof deviceType != 'undefined'){
    device_type = deviceType;
}
var user_id_of_viewer_js = 0;
if(typeof user_id_of_viewer != 'undefined'){
    user_id_of_viewer_js = user_id_of_viewer;
}
player.ready(function () {
        player.on('loadedmetadata', function () {
            total_duration = player.duration();
            if (typeof player.getCurrentRes === "function") {
                 video_resolution = player.getCurrentRes();
            } else {
                 video_resolution = 144;
            }
            buff_log_idtemp = 0;
            bufferenStart = 0;
            bufferenEnddd = 0;
            updateBuffered();
        });
        if ($('#backButton').length) {
            $('#backButton').click(function(){
                var player_buffered = player.buffered();
                var buffLen = player_buffered.length;
                bufferenEnddd = player_buffered.end(buffLen-1);
                bufferenStart = player_buffered.start(buffLen-1);
                updateBuffered();
            });
		}
        if (window.history && window.history.pushState) {
            window.history.pushState('forward', null, '');
            $(window).on('popstate', function() {
                var player_buffered = player.buffered();
                var buffLen = player_buffered.length;
                bufferenEnddd = player_buffered.end(buffLen-1);
                bufferenStart = player_buffered.start(buffLen-1);
                updateBuffered();
            });
		}
        player.on("ended", function() {
            var player_buffered = player.buffered();
            var buffLen = player_buffered.length;
            bufferenEnddd = player_buffered.end(buffLen-1);
            bufferenStart = player_buffered.start(buffLen-1);
            updateBuffered();
        });
        player.on('timeupdate', function () {
            previousTime = currentTime;
            currentTime = player.currentTime();
            previousBufferEnd = bufferDurationaas;
            var buffr = player.buffered(); 
            var buffLen = buffr.length;
            buffLen = buffLen - 1;
            bufferDurationaas = buffr.end(buffLen);
            
        });
        setInterval(function () {
            var currTim = player.currentTime();
            var player_buffered = player.buffered();
            var buffLen = player_buffered.length;
            bufferenEnddd = player_buffered.end(buffLen-1);
            bufferenStart = player_buffered.start(buffLen-1);
            if(buff_log_idtemp)
            updateBuffered();
        }, 60000);
        $("video").on("seeking", function () {
            var currTim = player.currentTime();
            var buffr = player.buffered();
            var buffLen = buffr.length;
            buffLen = buffLen - 1;
            if (forChangeRes === 0) {
                if (previousBufferEnd < currTim) {
                    if (seekStart === null) {
                        seekStart = previousTime;
                        var player_buffered = player.buffered();
                        var buffLen = player_buffered.length;
                        bufferenEnddd = player_buffered.end(buffLen-1);
                        bufferenStart = player_buffered.start(buffLen-1);
                        updateBuffered();
                    }
                }
            }
        });
        $("video").on('seeked', function () {
            seekStart = null;
        });
        player.on('changeRes', function () {
            forChangeRes = 123;
            seekStart = previousTime;
            var currTim = previousTime;
            updateBuffered();
            forChangeRes = 0;
        });
    });

   
    function updateBuffered() {
		var browser_details = getBrowserDetailsForLog();
		browser_details = browser_details.name+"/version("+browser_details.version+")";
        var player_buffered = player.buffered();
        var buffLen = player_buffered.length;
        var i = 0;
        var buff_dur = 0;
        if (player_buffered) {
            if(bufferenStart == 0 && bufferenEnddd == 0){
                buff_log_idtemp = 0;
            }
            if(already_logged == 1 && buffLen == 0){
                return;
            }
            if(already_logged == 0 && buffLen > 1){
                already_logged = 1;
            }
            for( i = 0; i < buffLen; i++){
                buff_dur = buff_dur + (player_buffered.end(i) - player_buffered.start(i));
            }
            if(buff_log_id > 0 && buff_log_idtemp == 0){
                buff_dur = buff_dur - (player_buffered.end(0) - player_buffered.start(0));
            }
            if (parseFloat(log_id) || parseFloat(buff_log_id)) {
				console.log('Inside');
                $.ajax({
                    type: 'POST',
                    async: false,
                    url: URL + '/videoLogs/videoBandwidthLog',
                    dataType: "json",
                    data: {movie_id: movie_id, video_id: stream_id, start_time: bufferenStart, end_time: bufferenEnddd, buff_log_id: buff_log_id, resolution: video_resolution, u_id: unique_id, duration: total_duration, studio_id: studio_id, buff_log_idtemp: buff_log_idtemp, buff_dur:buff_dur,device_type:device_type,user_id_of_viewer:user_id_of_viewer_js,browser_details:browser_details},
                    success: function (res) {  						
                        if(res){
                            buff_log_id = res.buffer_log_id;
                            unique_id = res.u_id;
                            buff_log_idtemp = res.buff_log_idtemp;
                        }
                    }
                });
                log_id = 0;
            }
        }
    }
	
	function getBrowserDetailsForLog() {
		var useraccount=navigator.userAgent,tem,res_obj=useraccount.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
		if(/trident/i.test(res_obj[1])){
			tem=/\brv[ :]+(\d+)/g.exec(useraccount) || []; 
			return {name:'IE',version:(tem[1]||'')};
		}   
		if(res_obj[1]==='Chrome'){
			tem=useraccount.match(/\bOPR|Edge\/(\d+)/)
			if(tem!=null){
				temp = tem[0].split('/');
				if(temp[0].toLowerCase().match('edge')){
					return {name:'Edge', version:tem[1]};
				}else{
					return {name:'Opera', version:tem[1]};
				}
			}
		}   
		res_obj=res_obj[2]? [res_obj[1], res_obj[2]]: [navigator.appName, navigator.appVersion, '-?'];
		if((tem=useraccount.match(/version\/(\d+)/i))!=null) {res_obj.splice(1,1,tem[1]);}
		return { name: res_obj[0], version: res_obj[1]};
	}
});    