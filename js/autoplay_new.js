$(document).ready(function () {
    var ended = 0;
   player.ready(function () {
       var started = 0;
       player.on("play", function() {
           started =1;
       });
       setInterval(function() {
            if(started == 1){
				var user_active = player.userActive();
				if(user_active === true || player.paused()){
					$('.common-css').show();
				} else {
					$('.common-css').hide();
				}
			}
		},1000);
        player.on("ended", function(){
            if(ended == 0){
                ended = 1;
				if(nextButtonLink != ''){
					setTimeout(function () {
						$('.next-buttons').click();
					}, 3000);
				} 
            } 
        });
        if(nextButtonLink != ''){
            $('.next-buttons').bind('click',function(e){
				window.location.href = nextButtonLink;
            });
        }else{
            $('.next-buttons').css( "opacity",0);
        }
        if(prevButtonLink != ''){
			$('.prev-buttons').bind('click',function(e){
				window.location.href = prevButtonLink;
			});
        } else{
            $('.prev-buttons').css( "opacity",0);
        }
        var mouseTimer = null, cursorVisible = true;

        function disappearCursor() {
            mouseTimer = null;
            document.body.style.cursor = "none";
            cursorVisible = false;
        }

        document.onmousemove = function() {
            if (mouseTimer) {
                window.clearTimeout(mouseTimer);
            }
            if (!cursorVisible) {
                document.body.style.cursor = "default";
                cursorVisible = true;
            }
            mouseTimer = window.setTimeout(disappearCursor, 2300);
        };
        player.on('timeupdate',function(){
            var currentTime = player.currentTime().toFixed(0);
            var remainingTime = player.remainingTime();
            remainingTime = remainingTime.toFixed(0);
            if((remainingTime == 0) && currentTime >= 4 ){
                if(ended == 0){
                    ended =1;
                    if(nextButtonLink != ''){
                        setTimeout(function () {
                            $('.next-buttons').click();
                        }, 3000);
                    }
                }
            }
		});
	});
});


