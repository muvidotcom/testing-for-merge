var restrictDeviceId = 0;
$(document).ready(function () {
    player.ready(function () {
		addRestrictDevice();
		setInterval(function(){
			addRestrictDevice();
		},60000);
		if (window.history && window.history.pushState) {
			if ((typeof browserName == 'undefined') || (typeof browserName != 'undefined' && browserName != 'Safari') ){
				window.history.pushState('forward', null, '');
				$(window).on('popstate', function() {
					if(restrictDeviceId > 0){
						$.post(deleteRestrictDeviceUrl,{'id':restrictDeviceId},function(res){
						 window.history.back();
						});
					}
				});
			} 
		}
		if ($('#backButton').length) {
			$('#backButton').click(function(){
				if(restrictDeviceId > 0){
					$.post(deleteRestrictDeviceUrl,{'id':restrictDeviceId},function(res){
					});
				}
			});
		}
		if ($('.next-buttons').length) {
			$('.next-buttons').click(function(){
				if(restrictDeviceId > 0){
					$.post(deleteRestrictDeviceUrl,{'id':restrictDeviceId},function(res){
					});
				}
			});
		}
		if ($('.prev-buttons').length) {
			$('.prev-buttons').click(function(){
				if(restrictDeviceId > 0){
					$.post(deleteRestrictDeviceUrl,{'id':restrictDeviceId},function(res){
					});
				}
			});
		}
    });
});
function addRestrictDevice(){
	$.ajax({
			url: restrictDeviceUrl,
			type: 'post',
			data: {
				movie_stream_id: stream_id,
				id : restrictDeviceId,
			},
			success: function(res) {
				if(res != ''){
					restrictDeviceId = res;
				}
			}
	});
}