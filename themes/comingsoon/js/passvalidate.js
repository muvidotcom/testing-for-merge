window.setTimeout(function() {
    $('input[type="password"]').removeAttr('readonly');
}, 1000);
$("#passcodeFrm").validate({
    rules: {
        passcode: {
            required: true
        }
    },
    messages: {
        passcode: {
            required: "Please provide your passcode"
        },
    },
    errorPlacement: function ($error, $element) {
        $("#errorvalidation").html($error);
    },
    submitHandler: function() {
        $.ajax({
            type:'POST',
            url: HTTP_ROOT+"/site/comingpasscode",
            data:$("#passcodeFrm").serialize(),
            dataType: 'json',
            success:function(response)
            {
                if($.trim(response.status) == 'success'){
                    window.location.reload();
                }else{
                    $('#errorvalidation').html(response.message).show();
                }
            }
        });
    }
});