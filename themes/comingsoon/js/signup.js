$("#signupFrm").validate({
    rules: {
        email: {
            required: true,
            mail: true
        }
    },
    messages: {
        email: {
            required: "Please provide your email address",
            mail: "Please provide a correct email address"
        },
    },
    errorPlacement: function ($error, $element) {
        $("#errorvalidation").html($error);
    },
    submitHandler: function() {
        $.ajax({
            type:'POST',
            url: HTTP_ROOT+"/site/comingsignup",
            data:$("#signupFrm").serialize(),
            dataType: 'json',
            success:function(response)
            {
                if($.trim(response.status) == 'success'){
                    window.location.reload();
                }else{
                    $('#errorvalidation').html(response.message).show();
                }
            }
        });
    }
});