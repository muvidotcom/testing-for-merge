<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png" type="image/png" rel="icon">
        <?php
            $BusinessName = $this->studio->name;
            $meta = Yii::app()->common->pagemeta('home');
            if($meta['title'] != ''){
                $title = $meta['title'];
            }else{
                $title = 'Home | '.$BusinessName;
            }
            if($meta['description'] != ''){
                $description = $meta['description'];
            }else{
                $description = '';
            }
            if($meta['keywords'] != ''){
                $keywords = $meta['keywords'];
            }else{
                $keywords = '';
            }
            eval("\$title = \"$title\";");
            eval("\$description = \"$description\";");
            eval("\$keywords = \"$keywords\";");
            $this->pageTitle = $title;
            $this->pageDescription = $description;
            $this->pageKeywords = $keywords;
        
        ?>
        
        
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>" />
    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,400italic,500italic,500,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <?php Yii::app()->bootstrap->register(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />    
</head>
    <body class="<?php echo $class?>">
        <?php echo $content; ?>
    </body>
</html>
