<script>
    $('html,body').animate({
        scrollTop:  500
    });
    function calc_price(){
        var no_of_sub = $("#subscriber").val();
        var sub_fee = $("#subscribtion_fee").val();
        if(no_of_sub != "" && sub_fee != ""){
            $("#calculation_block").hide(500);
            var revenue_val = parseInt(no_of_sub)* parseInt(sub_fee);
            $("#revenue_value").html("$"+revenue_val);
            var bandwidth_per_month_TB = parseInt(no_of_sub)* 0.015;
            var bandwidth_per_month_GB = parseInt(no_of_sub)* 15;
            if(bandwidth_per_month_TB < 2){
                var cost = 500;
            }else if(bandwidth_per_month_TB > 2 && bandwidth_per_month_TB < 10){
                var cost = 0.16 * bandwidth_per_month_GB ;
            }else if(bandwidth_per_month_TB > 10 && bandwidth_per_month_TB < 100){
                var cost = 0.14 * bandwidth_per_month_GB ;
            }else if(bandwidth_per_month_TB > 100){
                var cost = 0.12 * bandwidth_per_month_GB ;
            }
            $("#cost_value").html("$"+cost);
            var cost_percentage = (cost/revenue_val)*100;
            $("#cost_bar").css("width",cost_percentage+"%");
            $("#calculation_block").show(500);
        }
    }
</script>    
<div class="container" id="price_section">
    <div class="span12">
        <div style="text-align: center;">
            <h2>Muvi SDK Pricing Calculator</h2>
        </div>
        <div style="clear:both;height:10px;"></div>
        <div style="font-weight: 400;">
            Enter the number of subscribers and subscription fee you expect. Pricing calculator shows the estimated revenue and cost of your streaming platform.
        </div>
        <div style="clear:both;height:20px;"></div>
        <div style="padding:20px 0px;">
            <div style="float: left;width:48%">
                <div style="float: left;">
                    <div style="height:40px;"> 
                        <div style="padding-right: 20px;display: inline;top:-5px;position: relative;">Number of subscribers</div>
                        <select name="subscriber" id="subscriber" onchange="calc_price();" style="border: 1px solid #ccc;padding:0px;">
                            <option value="">Select</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                            <option value="10000">10000</option>
                            <option value="100000">100000</option>
                        </select>
                    </div>
                    <div style="height:20px;clear:both;"></div>
                    <div style="height:40px;">
                        <div style="padding-right: 66px;display: inline;top:-5px;position: relative;">Subscription fee</div>
                        <select name="subscribtion_fee" id="subscribtion_fee" onchange="calc_price();" style="border: 1px solid #ccc;padding:0px;">
                            <option value="">Select</option>
                            <option value="6">$5.99</option>
                            <option value="8">$7.99</option>
                            <option value="10">$9.99</option>
                            <option value="15">$14.99</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="calculation_block" style="padding-left:2%;float:left;width:50%;display:none;">
                <div style="float: left;width:35%;">Estimated Revenue Per Month</div>
                <div id="est_revenue" style="float:left;width:50%">
                    <div id="revenue_bar"  style="float:left;width:100%;background-color:green;height:30px;"></div>
                    
                </div>
                <div id="revenue_value" style="float:left;padding-left:2%;width:10%;"></div>
                <div style="height:20px;clear:both;"></div>
                <div style="float: left;width:35%;">Estimated Cost Per Month</div>
                <div id="est_cost"  style="float:left;width:50%;">
                        <div id="cost_bar" style="float:left;background-color:red;height:30px;"></div>
                        
                </div>
                <div id="cost_value" style="float:left;padding-left:2%;width:10%;"></div>
            </div>
            
        </div>
        <div style="clear:both;height:20px;"></div>
        <div>
            Above calculation is made based on the following assumptions:
        </div>
        <div>
            <ul>
                <li>
                    Pay-per-view and advertising revenue is not included in the above calculation.
                </li>
                <li>
                    1 hour of content viewing consumes around 500 MB of bandwidth.
                </li>
                <li>
                    Average subscriber will watch around 30 hours of content per month.
                </li>
                <li>
                    Calculation assumes that content is consumed on computer. Content consumed on smaller screen devices such as mobile will consume less bandwidth, therefore cost will be lower.
                </li>
                <li>
                    Content are in standard  definition. Bandwidth consumed by HD (high definition) will be higher.
                 </li>
                 <li>
                     Premium add-ons such as mobile apps are not included.
                 </li>
            </ul>
        </div>
    </div>
</div>
