<div class="form-box" id="login-box">
    <div id="login_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'login_form',
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
                'method' => 'POST',
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                    'validateOnChange'=>true,
                    'validateOnType'=>false
                )
            )
        ); ?>

        <div id="login-error-div" class="error" style="display: none;"></div>
        <div class="row">
            <?php echo $form->textField($model, 'email', array("onfocus" => "$('#login-error-div').hide();", 'placeholder' => 'email')); ?>
         </div>
        <div class="row">
            <?php echo $form->passwordField($model, 'password' ,array("onfocus" => "$('#login-error-div').hide();", 'placeholder' => 'password')); ?>
        </div>
        <div class="row rememberMe">
            <?php echo $form->checkBox($model,'rememberMe'); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>

        <div class="row " style="margin-top:5px;">
            <a href="#" onclick="forgot_click();">Forgot Password</a>
        </div>    
        <div class="row submit" style="margin-top: 20px;">

            <?php echo CHtml::ajaxSubmitButton('Log In', array('/login/'),
                    array(  
                        'beforeSend' => 'function(){ 
                            $("#login").attr("disabled",true);
                        }',
                        'complete' => 'function(){
                            $("#login").attr("disabled", false);
                        }',
                        'success'=>'function(data){  
                            var obj = jQuery.parseJSON(data); 
                            if(obj.login == "success"){
                                parent.location.href = "/admin/dashboard/";
                            }
                            else if(obj.login == "multisuccess"){
                                parent.location.href = "/multistore/Dashboard/";
                            } 
                            else if(obj.login == "step_1"){
                                parent.location.href = "/signup/typeofContent/";
                            } 
                            else if(obj.login == "step_2"){
                                parent.location.href = "/signup/paymentGateway/";
                            }                                                        
                            else{
                                $("#login-error-div").show();
                                $("#login-error-div").html("Login failed! Try again.");$("#login-error-div").append("");
                            }
                        }' 
                    ),
                    array("id" => "login", "class" => "btn btn-blue")      
            ); ?>

        </div>           
        <?php $this->endWidget(); ?>
    </div>	
</div> 

<style type="text/css">
    input[type="text"], input[type="password"]{text-transform: inherit;}
</style>

<script type="text/javascript">

function forgot_click(){
    parent.$('#login-modal').modal('hide');
    parent.$('#LoginModal').modal('hide');
    parent.$('#ForgotModal').modal('show');
}
</script>
    