<?php if(WP_USE_THEMES==1){?>
<br><br>
<style>
.reset-pass-item::-webkit-input-placeholder{
    font-weight: 600;
    color:#000;
}
.reset-pass-item::-moz-placeholder{
    font-weight: 600;
    color:#000;
}
.reset-pass-item::-ms-input-placeholder{
    font-weight: 600;
    color:#000;
}
#new_password-error,#confirm_password-error{float:left;width:100%;margin-left: 27%;}
.fxdmenu{top: 0px !important;}
.free-trial-menu {
  padding: 6px 15px !important;
}
.buy-now-menu {
  padding: 6px 15px !important;
}
#login-modal, #ForgotModal{
  margin: 0 auto;
  z-index: 99999;
  box-shadow: none;
  border: none;background-color: transparent;
}
#login-modal .label{
    background-color: transparent;
}
#login-modal .checkbox{
    padding-left: 0px;
}
#LoginForm_email,#forgot_email,#LoginForm_password{
    height: auto;
    margin: 0;
    text-transform: none;
    border-radius: 4px;
}
#login-modal .modal-header, #ForgotModal .modal-header{
    color:#6b6b7d;
}
</style>
<br><br>
<div class="container center">
    <h2 style="font-weight:bold;">Reset Password</h2><br><br>
    <form method="post" id="reset_pass" novalidate="novalidate">
        <div style="" class="form-group">
            <input style="text-transform: none; width: 47%;height:50px; background-color: #D3D3D3; border-radius: 5px; border-style:none" type="password" class="form-control" placeholder="New password" name="new_password" id="new_password" class="reset-pass-items">
        </div>
        <div class="clear" style="height:10px;"></div>
        <div style="" class="form-group">
            <input style="text-transform: none; width: 47%;height:50px;background-color: #D3D3D3; border-radius: 5px; border-style:none" type="password" class="form-control" placeholder="Confirm password" name="confirm_password" id="confirm_password" class="reset-pass-items">
        </div>
        <div class="clear" style="height:10px;"></div>
        <input type="hidden" name="token" value="<?php echo $_REQUEST['user'] ?>" />
        <input id="submitt" style="width: 47%;  height:50px; color:white; border-radius: 5px; background-color:#00b8e6; font-weight: 800; border-style:none" name="submit" type="submit" value="Save Password"/>
    </form>
    <br><br><br>
</div>
<?php }else { ?>
<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">Reset Password</h2>
            <div class="row" style="padding-left:30%;">
                <form method="post" id="reset_pass" novalidate="novalidate">
                    <div>
                        <div class="pull-left" style="width:20%"> New password </div> 
                        <div class="pull-left"><input type="password" name="new_password" id="new_password"/></div>
                    </div>
                    <div class="clear" style="height:10px;"></div>
                    <div>
                        <div class="pull-left" style="width:20%"> Confirm password </div> 
                        <div class="pull-left"><input type="password" name="confirm_password"/></div>
                    </div>
                    <div class="clear" style="height:10px;"></div>
                    <div style="padding-left:20%">
                        <input type="hidden" name="token" value="<?php echo $_REQUEST['user'] ?>" />
                        <input id="submitt" name="submit" type="submit" value="Save Password" class="btn btn-primary" />
                    </div>

                </form>
                <div id="success" class="alert alert-success hide"></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        jQuery.validator.addMethod("alphaNumeric", function(value, element) {
            return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value);
        }, "password must contain atleast one number and one character");
        $("#reset_pass").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 8,
                    alphaNumeric: true
                },
                confirm_password: {
                    minlength: 8,
                    equalTo: "#new_password"
                }
            },
            messages: {
                new_password: {
                    required: "Please enter your password",
                    minlength: "Password must contain atleast 8 characters",
                    alphaNumeric: "password must contain atleast one number and one character"
                },
                confirm_password: {
                    minlength: "Password must contain atleast 8 characters",
                    equalTo: "Please enter the same value again"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/login/set_password/",
                    data: $('#reset_pass').serialize(),
                    type: 'POST',
                    dataType: "json",
                    beforeSend: function() {
                        $('#submitt').val('Loading...');
			$('#submitt').attr('disabled', 'disabled');
                    },
                    success: function(data) {
                        if (data.status == "success") {
                            $('#submitt').val('Save Password');
                            $('#submitt').removeAttr('disabled');
                            $("#success").html(data.message).fadeIn("slow");
                            setTimeout(function() {
                                location.href = "<?php echo Yii::app()->getBaseUrl(true) ?>";
                            }, 5000);

                        } else {
                            bootbox.alert(data.message, function() {
                            });
                        }
                    }
                });
            }
        });
    });
</script>