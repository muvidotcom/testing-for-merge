
<div class=""form-group row">
     <hr>
         <?php if ($movieStream->film->content_types_id != 4 || $movieStream->film->content_types_id != 5 && $movieStream->film->content_types_id != 6) { ?>
        <label for="" class="col-sm-4 control-label" id="trailerLable">
            <?php $trailerConversion = $movieStream->is_converted;
            if ($trailerConversion == 1) {
                echo $this->Language['change_video'];
            } else {
                echo $this->Language['upload_video'];
            }
            ?>:
            <?php if ($trailerConversion == 2) { ?>
                <span id="videoCorrupt"><img src="<?php echo Yii::app()->baseUrl; ?>/images/exclamation_point.png" alt="Video is could not encoded" class="vAlignTop"  data-toggle="tooltip" title="Encoding failed. Please check the video file and re-upload."/></span>
            <?php } ?>
        </label>
        <div class="col-sm-8" id="<?php echo $movieStream->id; ?>">
            <?php
            if ($video_data) {
                $trailerIsConverted = $movieStream->is_converted;
                if ($trailerIsConverted == 1) {
                    $info = pathinfo($video_data);
                    $pos = stripos($video_data, 'iframe');
                    $pos1 = stripos($video_data, 'youtube.com');
                    $pos2 = stripos($video_data, 'vimeo.com');
                    $pos3 = stripos($video_data, 'dailymotion.com');
                    if ($info["extension"] == 'm3u8') {
                        echo $thirtparty_data;
                    } elseif (($pos > 0) && ($pos1 > 0 || $pos2 > 0 || $pos3 > 0)) {
                        ?>
                        <div style="width: 250px; height: 120px;">
                <?php echo $video_data; ?>
                            <style type="text/css">
                                iframe{
                                    width: 250px !important;
                                    height: 120px !important;  
                                }
                            </style>
                        </div>
                    <?php } else { ?>
                        <video width="250" height="120" controls style="background: #333;" id="trailerplayer"> 
                            <source type="video/mp4"  src="<?php echo $video_data; ?>">
                            Your browser does not support the video tag.
                        </video>
            <?php }
            if ($disable == "") {
                ?>
                        <div class="fg-line m-b-10">
                            <a id="video_upload_<?php echo $movieStream->id; ?>" data-movie_name="<?php echo $movieStream->film->name; ?>" class="btn btn-danger btn-file btn-sm c-btn-uppercase" href="javascript:void(0);" onclick="openUploadpopup('<?php echo $movieStream->film->id; ?>', '<?php echo $movieStream->id; ?>', '<?php echo $movieStream->film->content_types_id; ?>');"><i class="remove"></i><?php echo $this->Language['browse']; ?></a>
                        </div> 
            <?php }
        } else { ?>
                    <div class="progress-encoding">
                        <a href="#" class="f-500">
                            <em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>  
                    </div>
            <?php }
        } else {
            if ($disable == "") {
                ?>
                    <div class="fg-line m-b-10">
                        <a id="video_upload_<?php echo $movieStream->id; ?>" data-movie_name="<?php echo $movieStream->film->name; ?>" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold" onclick="openUploadpopup('<?php echo $movieStream->film->id; ?>', '<?php echo $movieStream->id; ?>', '<?php echo $movieStream->film->content_types_id; ?>');" class="btn btn-primary" id="trailer_btn"><i class="icon-upload"></i> <?php echo $this->Language['browse']; ?> </a>
                    </div>
        <?php }
    } ?>
        </div>
        <hr class="hideforaudio">
<?php } ?>
</div>