<div class="wrapper">
    <div class="container home-page-studio">
        <h2 class="btm-bdr">Muvi DAM</h2>
        <h3 class="blue">Store all your digital assets in the cloud.</h3>
        <h3>Muvi DAM (Digital Asset Management) provides content creators and aggregators a secure, 
            <br />cost-effective cloud-based solution for storage, conversion and distribution of their media content.</h3>
    </div>
</div>

<div class="wrapper blubg" id="navblubar">
    <div class="container home-page-customers barlinks">
        <div class="span8">
            <a href="#overview">Overview</a>
            <a href="#pricing">Pricing</a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#contactModal" data-backdrop="static">Contact Us</a>
        </div>
        
        <div class="span3 pull-right top_social">
            <ul class="social">
                <li class="pull-right"><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" alt="linkedin" /></a></li>                
                <li class="pull-right"><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" alt="facebook" /></a></li>
                <li class="pull-right"><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" alt="twitter" /></a></li>
                
            </ul>
        </div>          
    </div>
</div> 

<div class="wrapper whtbg" id="overview">
    <div class="container pad50">
        <div class="row-fluid">
            <div class="span7">
                <div class="item zerocap">
                    <h2 class="blue">Zero CapEX</h2>
                    <p>No capital expenditure required to purchase expensive hardware for hosting or bandwidth to support video streaming. Muvi takes care of all this.
Get your website and mobile app up and running with investing $0, really!</p>  
                </div>
            </div>
            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/zero-capex.png" alt="zero-capex" />
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="second">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cms.png" alt="cms" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item cms">
                <h2 class="blue">Content Management System</h2>
                <p>Web-based Content management system (CMS) offers secure access<br />
                    to all your content. Intuitive UI makes it very easy to find and share<br />
                    content.</p>            
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="third">
    <div class="container pad50">

        <div class="span7">
            <div class="item wrldsec">
                <h2 class="blue">World-class Security</h2>
                <p>Muvi DAM is designed with multiple layers of protection, including secure data transfer, encryption, network configuration, and 
                    application- and user-level controls that are distributed across a scalable, secure infrastructure.</p>   
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/security.png" alt="security" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="fourth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/convert.png" alt="convert" />
            </div>  
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item convrt">
                <h2 class="blue">Convert to 10+ Formats</h2>
                <p>Muvi DAM supports conversion to 10+ different file formats as per your<br>
                    requirements. Supports the consistency of the content viewing<br>
                    experience across devices so that every media asset can be viewed in<br />
                    various screen resolutions. Muvi also helps in digitizing physical tapes.</p>     
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="fifth">
    <div class="container pad50">

        <div class="span7">
            <div class="item reltime">
                <h2 class="blue">Real-time Reporting</h2>
                <p>Access real-time analytics on all parameters including number of views, geography distribution, user statistics and browser/device 
                    statistics. Muvi offers a powerful reporting tool to tap into the huge amounts of data collected and leverage it to create compelling, 
                    personalized marketing campaigns and promotions.</p>     
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/reporting.png" alt="reporting" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="pricing"> 

    <div class="container pad50 center">
        <h2 class="blue">Pricing</h2>
        
        <div class="grid-view" id="yw32">
            <table class="items table table-striped table-condensed center-table">
                <thead>
                    <tr>
                        <th>Solution</th>
                        <th>Price</th>
                        <th></th>                     
                    </tr>                
                </thead>
                <tbody>
                    <tr class="odd">
                        <td>Storage — Offline (for source files)
                            <br />Files are available on request after 3 days</td>
                        <td><h3 class="blue">$0.015</h3></td>
                        <td>per GB per month</td>
                    </tr>
                    <tr class="even">
                        <td>Storage — Online<br />
                            Files are available instantly to download</td>
                        <td><h3 class="blue">$0.045</h3></td>
                        <td>per GB per month</td>
                    </tr>
                    <tr class="odd">
                        <td>Conversion</td>
                        <td><h3 class="blue">$0.02</h3></td>
                        <td>per GB per month</td>
                    </tr>
                    <tr class="even">
                        <td>Sharing</td>
                        <td><h3 class="blue">$0.18</h3></td>
                        <td>per GB per month</td>
                    </tr> 
                    <tr>
                        <td colspan="3" class="tbl_btm_bg">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>        
    </div>    
</div>

<script type="text/javascript">
$(document).ready(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() > 872){
            $('body').addClass('fixedtop');
        }
        else
        {
            $('body').removeClass('fixedtop');
        }
    });
});

$(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
        
        
        e.preventDefault();

        var target = this.hash;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });


    });
});
</script>

<?php require_once dirname(__FILE__).'/../layouts/contactform.php';?>