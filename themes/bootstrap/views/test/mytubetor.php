<script   src="https://code.jquery.com/jquery-3.1.0.min.js" crossorigin="anonymous"></script>
<script src="https://api.instafeez.com/public/erpgs.js"></script>
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/headers/header-default.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/animate.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/plugins/font-awesome/css/font-awesome.min.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/theme-colors/default.css" id="style_color">
<link rel="stylesheet" href="https://htmlstream.com/preview/unify-v1.9.6/assets/css/theme-skins/dark.css">

<style>
    .form-control {
        display: block;
        width: 100%;
        height: 34px !important;
        padding: 6px 12px !important;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        margin-bottom: 0 !important;
    }
</style>

<br />




<div class="panel panel-blue margin-bottom-40" style="width: 600px;margin: 10px auto;">
        <div class="panel-heading">
                <h3 class="panel-title">Mytubetor</h3>
        </div>
        <div class="panel-body">
                        <div class="form-group">
                                <span id="success"></span>
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1" style="color: red">All fields are compulsory</label>
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="email" class="form-control" id="name" value="Test User">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="email" value="test@test.com">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Mobile</label>
                                <input type="email" class="form-control" id="mob" value="1234567890">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputEmail1">Amount (&#8377;)</label>
                                <input type="email" class="form-control" id="price" value="10">
                        </div>
                        
                        <button type="button" id="pay" class="btn-u btn-u-blue">Submit</button>
                        (Transaction charges may apply)
        </div>
</div>




<script>
$("#pay").click(function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var mob = $('#mob').val();
    var price = $('#price').val();
    if(price){
        var options = { 
            "key":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc",
            "amount":price*100,
            "image":"https://api.instafeez.com/public/if-white-logo-icon.png",
            "name": "Mytubetor",
            "merchant_details": {
                    "access_token" : "ivp_qa_7klwDr4ew6Yd9g"
            },
            "callbackurl" : "YOUR CALLBACK URL ",
            "autofill": {
                    "name": name,
                    "email": email,
                    "contact": mob
            },
            "handler" : function (result) {
                if(result){
                    var _html = '';
                    _html += '<h4 style="color:#5cb85c">Successful</h4><br>';
                    _html += '<p>Transaction id: '+result.payment_id+'</p>';
                    _html += '<p>Payment Method: '+result.payment_method+'</p>';
                    $('#success').html(_html);
                    var key_id = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NDcwNzQ5ODR9.rl2OMs_SZLgfLDKpoMPjQ1Xkt1M25lHXIm5tfps-qRc";
                    var payment_id = result.payment_id;
                    var access_token = "ivp_qa_7klwDr4ew6Yd9g";
                    $.post("https://api.instafeez.com/erp/payments/id/receipt", {key_id: key_id,payment_id:payment_id,access_token:access_token}, function(res){
                        console.log(res);
                    });
                }else{
                    alert('Please try later');
                }
            },
            "notes" : {
                "student_id":"121",
                "student_name":"Aditya Kumar",
                "school_id":"1200"
            }
        };
            var dop = new DynamoPay(options);
            dop.open();
        }else{
            alert('Please enter a valid amount');
        }
    });
</script>