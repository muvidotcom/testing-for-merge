<div class="wrapper">
    <div class="container home-page-studio">
        <h2 class="btm-bdr">Muvi Affiliate</h2>
        <h3>Muvi.com is a growing marketplace for streaming companies. Muvi Affiliate helps you setup your<br />
            presence in this marketplace which in turns allows you acquire new subscribers. Imagine opening a<br />
            storefront in &quot;eBay for media content&quot;.</h3>
    </div>
</div>

<div class="wrapper blubg" id="navblubar">
    <div class="container home-page-customers">
        <div class="span8">
            <a href="#overview">Overview</a>
            <a href="#how">How does it work</a>
            <a href="#" data-toggle="modal" data-target="#contactModal" data-backdrop="static">Contact Us</a>
        </div>
        
        <div class="span3 pull-right top_social">
            <ul class="social">
                <li class="pull-right"><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" /></a></li>                
                <li class="pull-right"><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" /></a></li>
                <li class="pull-right"><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" /></a></li>
                
            </ul>
        </div>        
    </div>
</div> 

<div class="wrapper whtbg" id="overview">
    <div class="container pad50">
        <div class="row-fluid">
            <div class="span7">
                <div class="item acqsub">
                    <h2 class="blue">Acquire subscribers from new geographies</h2>
                    <p>Acquire customers from new geographies where you don&rsquo;t have a presence in. <br />
                        Muvi.com is the fastest growing media marketplace of such kind.</p>  
                </div>
            </div>
            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/subscribers.png" />
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="second">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/reporting.png" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item reltime">
            <h2 class="blue">Real-time reporting</h2>
            <p>Access real-time analytics reporting on user acquisition and other<br />
                parameters. Completely transparent and access to full information makes it easier to measure performance.</p>            
            </div>
        </div>

    </div>    
</div>

<div class="wrapper whtbg" id="third">
    <div class="container pad50">

        <div class="span7">
            <div class="item qkset">
            <h2 class="blue">Quick Setup</h2>
            <p>Get up and running quickly. Muvi API makes offers a quick and easy to integrate as an affiliate.</p>           
            </div>
        </div>
        <div class="span4 pull-right txt-right">
            <div class="item">
            <img class="move_top" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/quick-setup.png" />
            </div>
        </div>
    </div>    
</div>

<div class="wrapper graybg" id="fourth">
    <div class="container pad50">

        <div class="span4">
            <div class="item">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/brand-owners.png" />
            </div>
        </div>        
        <div class="span7 pull-right txt-right">
            <div class="item brndown">
            <h2 class="blue">Retain Brand Ownership</h2>
            <p>Your brand (logo and name) is displayed prominently.<br />
                You continue to own all rights to your content and brand identity.</p>          
            </div>
        </div>

    </div>    
</div>


<div class="wrapper whtbg" id="how">
    <div class="container pad50 center">

        <h2 class="blue">How does it work</h2>
        <table>
            <tr>
                <td rowspan="2" width="20%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/affiliate-search.png" />
                    <br /><br /><p>User finds your<br />content on <a href="http://www.muvi.com">Muvi.com</a></p>
                </td>
                <td rowspan="2" width="19%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/arrow-ltr.png" /></td>
                <td rowspan="2" width="19%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/how-play.png" /><br /><br />
                    <p>Clicking &lsquo;Play&rsquo; prompts<br />user to authenticate his<br />subscription to your<br />platform</p>
                </td>      
                <td width="22%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/arrow-dtu.png" /></td>
                <td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/how-logo.png" />
                    <br /><br /><p>If authenticated, user<br /> watches the content on<br /> your-branded player</p><br />
                </td>
                  
            </tr>
            <tr>
                <td style="vertical-align: top !important;"><img style="vertical-align: top !important;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/arrow-utd.png" /></td>
                <td><br /><br /><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/how-subscriber.png" />
                    <br /><br /><p>Otherwise, user is<br />prompted to subscribe<br />to your service</p>                    
                </td>                  
            </tr>
        </table>

    </div>    
</div>

<script type="text/javascript">
$(document).ready(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() > 872){
            $body').addClass('fixedtop');
        }
        else
        {
            $('body').removeClass('fixedtop');
        }
        //console.log($(this).scrollTop() + ' and ' +$('#navblubar').position().top);
    });
});

$(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
});
</script>
<?php require_once dirname(__FILE__).'/../layouts/contactform.php';?>