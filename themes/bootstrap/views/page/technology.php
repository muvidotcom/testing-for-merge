<div>    
    <div id="features" class="wrapper whtbg container">
	 <div class="container home-page-customers2" style="width: 100%;">
            <h2 class="btm-bdr">Built with the Latest and Greatest Technologies</h2>
            <ul style="margin-left: 10%;">
                <li style="width:12%;"><a href="#amazon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/amazon.png" alt="Maaflix" title="Maaflix"><br>Hosted in Cloud</a></li>
                <li style="width:12%;"><a href="#html5"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/html5-logo.png" title="Iskcon"><br>HTML5 Player</a></li>
                <li style="width:12%;"><a href="#drm"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/drm-logo.png" alt="Funkara" title="Funkara"><br>Security, DRM</a></li>
                <li style="width:12%;"><a href="#cdn"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cdn-logo.png" alt="Fountain" title="Fountain"><br>Integrated CDN</a></li>
                <li style="width:12%;"><a href="#soa"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/soa-logo.png" alt="SEPL" title="SEPL"><br>SOA</a></li>                       
            </ul>
        </div>
	<h3 class="clear2 btm-bdr"></h3>
        <a name="amazon"></a>
        <div class="container pad50">
            <div class="row-fluid">
                <div class="span5">
                    <div class="item">
                        <img title="Hosted in Amazon Cloud" alt="Hosted in Amazon Cloud" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/s3_tech.jpg">
                    </div>
                </div>        
                <div class="span7 pull-right txt-right">
                    <div class="item">
                        <h2 class="blue">Hosted in Amazon Cloud</h2>
                            <p>Muvi uses Amazon Web Services, world’s #1 cloud platform, the same hosting infrastructure used by Netflix. Cloud offers unlimited storage and scalability. You don’t need to worry about the number of subscribers and views on your VOD platform, may it be in hundreds or millions.</p>
                            <p>Cloud hosting allows us to offer you the platform at zero CapEx, just a pay a low monthly fee. Unlimited storage allows us to store all your videos in different formats and processing power to transcode your videos.</p>
                            <p>Using a multi-tenant architecture, backup and DR, Muvi offers 100% uptime, means your VOD platform is never down even for a single second.</p>

                    </div>
                </div>
            </div>
        </div>    
    </div>

    <a name="html5"></a>
    <div id="second" class="wrapper graybg">
        <div class="container pad50">
                    <div class="span7">
                <div class="item">
                    <h2 class="blue">Multi-platform HTML5 video player</h2>
                    <p>Traditional video players are built using Flash. One major issue with Flash is it’s not supported by mobile platforms and user has to download a plugin to play a video on his web browser.</p>
                    <p>Muvi video player is built using HTML5. HTML5 allows the video player to be truly platform independent. Viewers can play your video on any mobile device and web browser, without requiring any plugin. With HTML5 video, viewers can easily and successfully watch your videos from anywhere, at any time and hassle free.</p>
                    <p>HTML5 makes it easy to add links your videos. These are effective tools for improving user interaction, delivering additional information and promoting other content. HTML5 player is also easy to customize and style to fit your template.</p>

                </div>
            </div>
            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img title="Multi-platform HTML5 video player" alt="Multi-platform HTML5 video player" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/html5.png">
                </div>
            </div>        


        </div>    
    </div>
    <a name="drm"></a>
    <div id="third" class="wrapper whtbg">
        <div class="container pad50">

                    <div class="span5">
                <div class="item">
                    <img title="Unmatched security via DRM" alt="Unmatched security via DRM" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/drm_new.png">
                </div>
            </div>
            <div class="span7  pull-right txt-right">
                <div class="item">
                    <h2 class="blue">Unmatched security via DRM</h2>
                    <p>Muvi uses a built-in DRM to secure your content. Viewers can watch your content, but can’t download the file.</p>
                    <p>Videos are encrypted on the server. Only Muvi player has the decryption keys to playback these videos. DRM blocks any 3rd party sites, tools and software from accessing, downloading or playing the video. DRM takes care of user authentication so that only subscribers and valid users who have paid for the content are allowed access to the same.</p>
                    <p>Apart from DRM, several other world-class security measures are taken such as using a high security firewall on Amazon Cloud, SSL certificate on all your web pages and code to prevent against DoS and other hacking attacks.</p>

                </div>
            </div>

        </div>    
    </div>
    <a name="cdn"></a>
    <div id="fourth" class="wrapper graybg">
        <div class="container pad50">
                    <div class="span7">
                <div class="item">
                   <h2 class="blue">Integrated Content Delivery Network</h2>
                   <p>Muvi uses Amazon Cloudfront CDN (content delivery network). This allows your videos to be streamed fast to a global audience, irrespective of your viewer’s geographic location. </p>
                   <p>Without CDN, if your main hosting server is based in New York, users from Europe or Asia must make a number of trans-continental electronic hops when they access your video. CDN makes your videos available on localized data centers which are closer to the user, results in faster streaming and happier user &#9786;.</p>
                   <p>CDN also distributes load on your platform and saves bandwidth, therefore reducing your bandwidth cost.</p>
                   <p>Best part, this is at no extra cost to you! Price of CDN is included in the bandwidth cost</p>
 
                </div>
            </div>

            <div class="span4 pull-right txt-right">
                <div class="item">
                    <img title="CDN" alt="CDN" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cdn.png">
                </div>
            </div>        


        </div>    
    </div>
    <a name="soa"></a>
    <div id="fifth" class="wrapper whtbg">
        <div class="container pad50">
            <div class="span5">
                        <div class="item">
                            <img title="SOA" alt="SOA" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/soa.png">
                        </div>
             </div>

            <div class="span7  pull-right txt-right">
                <div class="item">
                    <h2 class="blue">Server Oriented Architecture (SOA)</h2>
                    <p>Muvi is built in a modular service oriented architecture (SOA). </p>
                    <p>Main philosophy of SOA is loose coupling i.e. avoiding temporal, technological and organizational constraints in the system design. Loosely coupled systems support dynamically binding to other components and can mediate the difference in the component's structure, protocols and semantics, thus abstracting volatility. Loose coupling in SOA is how the services are implemented without impacting other services or application. The only interaction between the application and services is through the publish interfaces.</p>
                    <p>All of it means that your VOD is future proof. It can be customized for specific needs and new features can be added easily and quickly.</p>

                </div>
            </div>

        </div>    
    </div>
</div>