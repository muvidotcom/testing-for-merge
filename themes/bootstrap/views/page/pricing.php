<script>
    $('html,body').animate({
        scrollTop: 500
    });
    function calc_price() {
        var no_of_sub = $("#subscriber").val();
        var sub_fee = $("#subscribtion_fee").val();
        if (no_of_sub != "" && sub_fee != "") {
            $("#calculation_block").hide(500);
            var revenue_val = parseInt(no_of_sub) * parseInt(sub_fee);
            $("#revenue_value").html("$" + revenue_val);
            

            var bandwidth_per_month_TB = parseInt(no_of_sub) * 0.015;
            var bandwidth_per_month_GB = parseInt(no_of_sub) * 15;
            $('#bandwidth_value').html(bandwidth_per_month_TB+'TB');
            if (bandwidth_per_month_TB < 1) {
                var cost = 399;
            } else if (bandwidth_per_month_TB > 1 && bandwidth_per_month_TB < 10) {
                var cost = 0.14 * bandwidth_per_month_GB;
            } else if (bandwidth_per_month_TB > 10 && bandwidth_per_month_TB < 100) {
                var cost = 0.12 * bandwidth_per_month_GB;
            } else if (bandwidth_per_month_TB > 100) {
                var cost = 0.10 * bandwidth_per_month_GB;
            }
            if (cost < 399) {
                cost = 399;
            }
            $("#cost_value").html("$" + cost);
            var cost_percentage = (cost / revenue_val) * 100;
            $("#cost_bar").css("width", cost_percentage + "%");
            $("#calculation_block").show(500);
        }
    }
</script>   
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pricing.css" type="text/css" rel="stylesheet" />
<div class="container" id="price_section">
    <div class="wrapper">
        <div class="container home-page-studio">
            <h1 class="btm-bdr">ZERO CapEX. ZERO Upfront Investment. Pay-as-you-grow.</h1>
            <p>No gold / silver / bronze package. One monthly fee includes everything you need<br> to launch your VOD platform. <strong>Simple pricing as it should be!</strong></p>
        </div>
        <div class="clear2"></div>
        <div class="clear2"></div>

        <div class="row">
            <div class="span2"></div>
            <div class="span8">
            <section id="pricePlans">
                <ul id="plans">

                    <li class="plan">
                        <ul class="planContainer">
                            <li class="title"><h2 class="bestPlanTitle">Muvi</h2></li>
                            <li>
                                <ul class="options">
                                    <li><span>All Muvi features</span></li>
                                    <li><span>White-labeled website or mobile app</span></li>
                                    <li><span>SVOD (subscription), AVOD (advertisement), TVOD (pay-per-view)</span></li>
                                    <li><span>VOD and Live streaming</span></li>
                                    <li><span>Video CMS & Analytics</span></li>
                                    <li><span>Cloud hosting</span></li>
                                    <li><span>Unlimited encoding</span></li>
                                    <li><span>Unlimited storage</span></li>
                                    <li><span>DRM</span></li>
                                    <li><span>CDN</span></li>
                                    <li><span>1TB bandwidth per mo</span></li>
                                    <li><span>HTML5 player</span></li>
                                    <li><span>Full API </span></li>
                                    <li><span>Web and email support</span></li>
                                </ul>
                            </li>
                            <li class="button">
                                <span class="bestPlanButton" id="muvi-studio">
                                    $399 per month<br> +<br> Bandwidth Overage
                                </span>
                            </li>
                        </ul>
                    </li>

                    <li class="plan">
                        <ul class="planContainer">
                            <li class="title li-no-color"><h2>Muvi On-premise</h2></li>
                            <li>
                                <ul class="options">
                                    <div class="li-blank">All the same features offered by standard Muvi, and videos are stored in your server and delivered using your CDN.</div>
                                </ul>
                            </li>
                            <li class="button">
                                <span id="muvi-studio-on-premise">
                                    $599 per month<br>
                                    + <br>
                                    $0.01 per hr of streaming
                                </span>
                            </li>
                        </ul>
                    </li>

                    <li class="plan">
                        <ul class="planContainer">
                            <li class="title li-no-color"><h2>Muvi Server</h2></li>
                            <li>
                                <ul class="options">
                                    <div class="li-blank">Video CMS and API.</div>
                                    <div class="li-blank">Use our API to build custom video applications. We take care of all video processing (storage, encoding, player and analytics).</div>
                                    <div class="li-blank">Includes 100GB per month bandwidth.</div>
                                </ul>
                            </li>
                            <li class="button">
                                <span id="muvi-studio-on-premise">
                                    $199 per month<br>
                                    + <br>
                                    Bandwidth Overage
                                </span>
                            </li>                               
                        </ul>
                    </li>
                </ul> <!-- End ul#plans -->
            </section>
            </div>
        </div>

    </div>

    <div class="wrapper martop20">
        <div class="container">
            <h2 class="btm-bdr">Bandwidth Cost</h2>
            <p class="center">Bandwidth consumed more than 1TB in a month is charged at the following rate:</p>
            <div class="clear2"></div>
            <div class="row-fluid">
                <table class="table table-bordered table-striped">

                    <tbody>
                        <tr>
                            <td>Bandwidth</td>
                            <td>upto 10TB per mo</td>
                            <td>11TB to 100TB per mo</td>
                            <td>101TB to 1000TB per mo</td>
                        </tr>
                        <tr>
                            <td>USA, Europe, Middle East, Africa</td>
                            <td><strong>$0.12 per GB</strong></td>
                            <td><strong>$0.10 per GB</strong></td>
                            <td><strong>$0.08 per GB</strong></td>
                        </tr>
                        <tr>
                            <td>Asia, Russia, Australia</td>
                            <td><strong>$0.20 per GB</strong></td>
                            <td><strong>$0.18 per GB</strong></td>
                            <td><strong>$0.14 per GB</strong></td>
                        </tr>
                        <tr>
                            <td>South America</td>
                            <td><strong>$0.28 per GB</strong></td>
                            <td><strong>$0.26 per GB</strong></td>
                            <td><strong>$0.22 per GB</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
    
    <div class="wrapper martop20">
        <div class="container">
            <h2 class="btm-bdr">Add-ons</h2>
            <div class="clear2"></div>
            <div class="row-fluid">
                <table class="table table-bordered table-striped">

                    <tbody>
                        <tr>
                            <td>Native iOS App</td>
                            <td>$299 per month</td>
                        </tr>
                        <tr>
                            <td>Native Android App</td>
                            <td>$299 per month</td>
                        </tr>
                        <tr>
                            <td>TV App (Roku, Apple TV, XBOX)</td>
                            <td>Ask sales</td>
                        </tr>
                        <tr>
                            <td>Custom template design</td>
                            <td>Ask sales</td>
                        </tr>
                         <tr>
                            <td>VIP support: 24x7x365 support via phone and email. Dedicated account manager</td>
                            <td>18% of monthly cost<br />$200 minimum</td>
                        </tr>
                        <tr>
                            <td>Custom design and development services</td>
                            <td>Ask sales</td>
                        </tr>
                        <tr>
                            <td>Marketing services</td>
                            <td>Ask sales</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>    

    <div class="wrapper martop20">
        <div class="container">
        <h2 class="btm-bdr">ROI Calculator</h2>
        <p class="center">Try the pricing calculator to find your estimated revenue and cost.<br />
            It accounts for the monthly platform fee and estimated bandwidth cost based on your number  of subscribers.</p>
        <div class="clear2"></div>
        <div class="row-fluid" style="padding:20px 0px;">
            <div class="span6">

                <div class="input-nm-style2">Number of subscribers</div>
                <select name="subscriber" id="subscriber" onchange="calc_price();" class="select input-xlarge">
                    <option value="">Select</option>
                    <option value="100000">100000</option>
                    <option value="10000">10000</option>
                    <option value="1000">1000</option>
                    <option value="500">500</option>
                    <option value="100">100</option>
                </select>
                <div class="clear2"></div>

                <div class="input-nm-style2a">Monthly Subscription fee</div>
                <select name="subscribtion_fee" id="subscribtion_fee" onchange="calc_price();" class="select input-xlarge">
                    <option value="">Select</option>
                    <option value="15">$14.99</option>
                    <option value="13">$12.99</option>
                    <option value="10">$9.99</option>
                    <option value="8">$7.99</option>
                    <option value="6">$5.99</option>

                </select>

                <p>Calculating is based on the <a href="#myModal" role="button" data-toggle="modal">assumptions</a>.</p>

                <!-- Modal Popup -->
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Assumptions of calculation</h3>
                    </div>
                    <div class="modal-body">
                        <p>Above calculation is made based on the following assumptions:</p>
                        <ul>
                            <li>Pay-per-view and advertising revenue is not included in the above calculation.</li>
                            <li>1 hour of content viewing consumes around 500 MB of bandwidth.</li>
                            <li>Average subscriber will watch around 30 hours of content per month.</li>
                            <li>Calculation assumes that content is consumed on computer. Content consumed on smaller screen devices such as mobile will consume less bandwidth, therefore cost will be lower.</li>
                            <li>Content are in standard  definition. Bandwidth consumed by HD (high definition) will be higher.</li>
                            <li>Premium add-ons such as mobile apps are not included.</li>
                        </ul>
                    </div>
                </div>                    

            </div>
            <div class="span6 bold-4" id="calculation_block" style="display: none;">
                <div class="row-fluid">
                    <div class="span4">Estimated Bandwidth</div>
                    <div class="span6">
                        <dl>
                            <dd><div id="bandwidth_bar" class="bar" style="width: 100%"></div></dd>
                        </dl>
                    </div>
                    <div class="span2 text-mid" id="bandwidth_value"></div>                    
                </div>
                <div class="row-fluid">
                    <div class="span4">Estimated Revenue Per Month</div>
                    <div class="span6">
                        <dl>
                            <dd><div id="revenue_bar" class="bar-green" style="width: 100%"></div></dd>
                        </dl>
                    </div>
                    <div class="span2 text-mid" id="revenue_value"></div>
                </div>                  
                <div class="row-fluid">
                    <div class="span4">Estimated Cost Per Month</div>
                    <div class="span6">
                        <dl>
                            <dd><div id="cost_bar" class="bar-blue" style="width: 60%"></div></dd>
                        </dl>
                    </div>
                    <div class="span2 text-mid" id="cost_value"></div>
                </div>                                                

            </div>

        </div>
        <div style="clear:both;height:20px;"></div>

        </div> 
    </div>

</div>

<div class="wrapper graybg martop20">
    <div class="container pad50">
        <h2 class="blue">Top Pricing Questions</h2>
        <div class="row-fluid">
            <div class="span6">
                <h4>How do I know what will be my bandwidth cost?</h4>
                <p>Try the pricing calculator to find your estimated revenue and cost. It accounts for the monthly platform fee and estimated bandwidth cost based on your number of subscribers.</p>   
            </div>

            <div class="span6">
                <h4>Do I have to pay extra for Tech Support and Maintenance?</h4>
                <p>No, your plan of $399 includes Tech Support, Product updates and regular Maintenance so that you don’t need to worry about anything once you launch!</p>   
            </div>            
        </div>

        <div class="row-fluid">

            <div class="span6">
                <h4>What all does $399 really include?</h4>
                <p>$399 includes the website, pre-set templates that you can customize with your color options, UNLIMITED STORAGE, hosting on the cloud, 1TB Bandwidth, DRM, Security &amp; CDN.</p>   
            </div>
            <div class="span6">
                <h4>Are there any hidden charges or taxes? Will I have to pay more at the end of the billing cycle?</h4>
                <p>$399 includes all the charges currently; you will be required to pay additional fees only if you exceed the 1TB bandwidth as per our pricing table. 
                    Local taxes if applicable in your country / regions are to be paid by you directly to your respective tax authorities. 
                    Any customizations requests are chargeable separately, for which you will receive a detailed quote before any work starts and is billed to you.</p>   
            </div>
        </div>

    </div>    
</div>   