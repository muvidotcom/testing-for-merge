<div class="container">
    <div class="container contact_us">
        <div class="span12">
            <h2 class="btm-bdr">About Muvi</h2>
            <p>Muvi enables video content owners to easily monetize your video content beyond the borders of traditional advertising. We do this by offering a 
                self-service white label platform to launch your own VoD platform,and a <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/muvi">marketplace</a> to sell your video content and start earning revenue.</p>            
            <h3>Our mission is simple: make video monetization easy.</h3>
            
            <p>Some of the products &amp; solutions we offer are –</p>
            
            <h4><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/">Muvi – Self-service VoD Platform</a></h4>
            <p>Launch your own branded White Label VoD Platform at Zero CapEx cost. Get up and running within minutes.</p>
            
            <h4><a href="<?php echo Yii::app()->getBaseUrl(true); ?>/muvi">Muvi Marketplace – Distribute your content on Muvi.com </a></h4>
            <p>Marketplace is a site that connects movie lovers worldwide! It is a social discovery platform for entertainment content (movies, TV and Internet video). 
                It collects data about user&rsquo;s taste and suggests them precisely the content that he/she will like.</p>                       
        </div>
    </div>
</div>