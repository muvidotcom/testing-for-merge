<div class="container">
    <div class="span8" style="min-height: 600px;width:70%;">
        <h2 class="btm-bdr">Team</h2>
        <div>
            <div class="pull-left" style="width:26%">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/team/anshuman.jpg" class="team_img" alt="Anshuman Das" title="Anshuman Das" />
            </div>
            <div class="pull-left" style="width: 70%;padding-left:3%;">
                <div class="teammem_name">Anshuman Das</div>
                <div class="clear" style="height: 2px;"></div>
                 <div class="team_designation">Founder & CEO</div>
                 <div class="clear" style="height: 10px;"></div>
                 <div class="team_desc">Anshuman is a serial entrepreneur, has built and sold multiple companies. Graduated with Master&rsquo;s in Computer Science and MBA 
                     in Marketing from State University of New York with 4.0 GPA. Has 15+ years of experience in engineering and product management. Holds multiple patents. 
                     Loves to watch movies and discuss new business ideas.
                 </div>
                 <div class="clear" style="height: 10px;"></div>
                 <div>
                     <div class="pull-left">
                         <a href="http://www.muvi.com/anshuman-das-5" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/m.png" alt="Muvi" title="Muvi"></a>
                     </div>
                     <div class="pull-left" style="padding-left:20px;">
                         <a href="http://www.linkedin.com/in/dasanshuman#sthash.Obbwqj9R.dpuf" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linked_in.png" alt="LinkedIn" title="LinkedIn" /></a>
                     </div>
                 </div>
            </div>
        </div>
        <div class="clear" style="height: 30px;"></div>
        <div>
            <div class="pull-left" style="width: 70%;">
                <div class="teammem_name">Mohan Kumar</div>
                <div class="clear" style="height: 2px;"></div>
                <div class="team_designation">Head of Development</div>
                <div class="clear" style="height: 10px;"></div>
                <div class="team_desc">Before joining Muvi, Mohan led the development team in Afixi Technologies Pvt Ltd. Mohan lives with his wife and 3-year-old daughter, 
                    is fond of new technologies, likes to play mobile games for fun.
                 </div>
                <div class="clear" style="height: 10px;"></div>
                 <div>
                     <div class="pull-left">
                         <a href="http://www.muvi.com/satyam-mohan-3"  target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/m.png" alt="Muvi" title="Muvi"></a>
                     </div>
                     <div class="pull-left" style="padding-left:20px;">
                         <a href="http://in.linkedin.com/pub/mohan-kumar/aa/458/757"  target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linked_in.png" alt="LinkedIn" title="LinkedIn"></a>
                     </div>
                 </div>
            </div>
            <div class="pull-left" style="width:26%">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/team/mohan.jpg" class="team_img" alt="Mohan Kumar" title="Mohan Kumar">
            </div>
        </div>
        <div class="clear" style="height: 30px;"></div>
        <div>
            <div class="pull-left" style="width:26%">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/team/Viraj.png" class="team_img" alt="Viraj Mehta" title="Viraj Mehta">
            </div>
            <div class="pull-left" style="width: 70%;padding-left:3%;">
                <div class="teammem_name">Viraj Mehta</div>
                <div class="clear" style="height: 2px;"></div>
                <div class="team_designation">Head of Marketing</div>
                <div class="clear" style="height: 10px;"></div>
                <div class="team_desc">Viraj has 10+ years of experience in the digital space spanning across Digital Marketing, Product Development, Alliances &amp; 
                    Partnerships and P&amp;L Management. Prior to joining Muvi, Viraj was heading digital at UBM where he led the overall transformation to digital for the 
                    company and its line of businesses and was instrumental in launch and growth of the digital business for UBM. Viraj is a gadget freak and can be 
                    found playing with the latest gadgets when he is not working.
                </div>
                <div class="clear" style="height: 10px;"></div>
                 <div>
                     <div class="pull-left">
                         <a href="http://www.muvi.com/viraj-mehta"  target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/m.png" alt="Muvi" title="Muvi"></a>
                     </div>
                     <div class="pull-left" style="padding-left:20px;">
                         <a href="http://in.linkedin.com/in/virajmehta"  target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linked_in.png" alt="LinkedIn" title="LinkedIn"></a>
                     </div>
                 </div>
            </div>
        </div>
        <div class="clear" style="height: 30px;"></div>
    </div>
    <div class="span3 pull-right">
        <div>
            <h3>Our Tweets</h3>
            <a class="twitter-timeline"  href="https://twitter.com/muvistudiob2b" data-widget-id="534576619390660608">Tweets by @muvistudiob2b</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

        </div>            
    </div>
</div>