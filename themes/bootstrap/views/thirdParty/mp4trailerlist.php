<script>
  var muviWaterMarkTrailer = "<?php echo $waterMarkOnPlayer; ?>";
</script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
<script type='text/javascript'>
        // video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var no_compatible_source ="<?php echo $this->Language["no_compatible_source"];?>";

 </script>          
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js?v=<?php echo $v?>"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js?v=<?php echo $v?>"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/video.js?v=<?php echo $v?>"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css?v=<?php echo $v?>" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo RELEASE;?>" rel="stylesheet" type="text/css" />
<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo RELEASE;?>"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?rand=<?php echo RELEASE;?>"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?rand=<?php echo RELEASE;?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/fancybox.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 
        
 <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo RELEASE?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/css/fancybox.css?v=<?php echo $v?>" type="text/css" media="screen" />
<script src="<?php echo Yii::app()->getbaseUrl(true);?>/js/video-trailerbuffer-log.js?v=<?php echo $v?>"></script>  
<!--        <script src="<?php echo Yii::app()->getbaseUrl(true);?>/js/video-buffer-log.js"></script>  -->

<!--<script data-cfasync="false" type="text/javascript">
    videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
</script>-->
<style type="text/css">
.video-js {width: <?php echo $halfwidth ?>px !important ; height: <?php echo $halfheight ?>px !important;}
 #pause_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none;
        }
        #play_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none; 
        }
        .vjs-tt-cue{font-size: 18px !important;}
        .vjs-live-controls {display:none !important;}
        @media screen and (max-width: 1100px){
            .vjs-res-button {left:-20px!important;} 
        }
        @media screen and (max-width: 400px){
            .vjs-res-button {left:-5px!important;} 
            .vjs-subtitles-button {left:15px !important;}
        }
        @media screen and (max-width: 330px){
            .vjs-res-button {left:-30px!important;} 
        }
</style>

<div id="myTrailer"> 
    <div class="wrapper">
        <div class="videocontent">
            <?php
                if ($video_remote_url != '') { 
                    if($trailerIsConverted == 1){
                     ?>
            <video id="video_block" class="video-js vjs-default-skin" autoplay="true" preload="auto" autobuffer controls  data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution;?>" } }}'  playsinline>
                 
                        <?php
                            foreach($multipleVideo as $key => $val){
                                echo '<source src="'.$trailer_path.'" data-res="'.$key.'" type="video/mp4" />';
                            }
                            foreach ($subtitleFiles as $subtitleFilesKey => $subtitleFilesval) {
                               if ($subtitleFilesKey == 1) {
                                   echo '<track kind="subtitles" src="' . $subtitleFilesval['url'] . '" srclang="' . $subtitleFilesval['code'] . '" label="' . $subtitleFilesval['language'] . '" default="true" ></track>';
                                } else {
                                   echo '<track kind="subtitles" src="' . $subtitleFilesval['url'] . '" srclang="' . $subtitleFilesval['code'] . '" label="' . $subtitleFilesval['language'] . '" ></track>';
                                }
                            }
                        ?>
                         </video>
            <input type="hidden" id="full_video_duration" value="" />
            <input type="hidden" id="full_video_resolution" value="" />
            <input type="hidden" id="u_id" value="0" />
            <input type="hidden" id="buff_log_id" value="0" />
            <input type="hidden" id="device_id" value="<?php echo @$_REQUEST['device_type'];?>" />
            
            <?php
                    }
                }
            ?>
        </div> 
    </div>
</div>


<script type="text/javascript">
    var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/user/getNewSignedUrlForTrailer";
    var multipleVideoResolution = new Array();
    multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
    var previousTime = 0;
    var currentTime = 0;
    var seekStart = null;
    var adavail = 0;
    var forSeek = 0;
    var previousBufferEnd =0;
    var bufferDurationaas = 0;
    var percen = 5;
    var log_id = 0;
    var url = "<?php echo Yii::app()->baseUrl; ?>/report/addTrailerLog";
    var movie_id = "<?php echo $movie_id ?>";
    var videoOnPause = 0;
    var off ="<?php echo $this->Language["off"];?>";
    var played_status = 0;
    $(document).ready(function () {
           var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            <?php 
                if ($video_remote_url != '') { 
                    if($trailerIsConverted  == 1){
            ?>
                       // videojs.options.autoplay = true;
                    //videourl = '<?php echo $video_remote_url ?>';
                    var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;
                    var playerSetup = videojs( 'video_block', { plugins : { resolutionSelector : {
                        force_types : [ 'video/mp4'],
                        default_res : '<?php echo $defaultResolution;?>'
                    } } } );
                    playerSetup.ready(function () {
                      main_video = this;
                      player =this;
                     
                    $(".vjs-control-bar").show();
                      main_video.progressTips(); // Displying duration in the player during playback.
                     main_video.play();
                    $(".vjs-error-display").attr("style","display:none"); 
                    $(".vjs-big-play-button").hide();
                    $('#video_block').append('<img src="/images/pause.png" id="pause"/>'); 
                     $('#video_block').append('<img src="/images/play-button.png" id="play"/>');
                     $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
                 $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
                if(is_mobile === 0){
                    $('#video_block_html5_api').on('click',function(e){ 
                          e.preventDefault();
                        if (player.paused()){
                            $('#pause').hide(); 
                            $('#play').show();
                        } else{
                            $('#play').hide(); 
                            $('#pause').show(); 
                        }
                    });
                    $('#video_block').trigger('click');
                }else{
                     //mobile operations start here...
                     player.on('loadedmetadata', function() {
                        player.play();
                        $('#video_block').trigger('click');
                     });
                     $('#pause_touch').bind('touchstart',function(e){
                            player.pause();
                            $('#play_touch').show();
                            $('#pause_touch').hide();
                     });
                     $('#play_touch').bind('touchstart',function(e){
                            player.play();
                            $('#video_block').trigger('click');
                            $('#play_touch').hide();
                            $('#pause_touch').show();
                            /*setTimeout(function(){
                                $('#pause_touch').hide();   
                                 },1000); */
                     });
                     setInterval(function(){
                        var user_active = player.userActive();
                        if(played_status && user_active ){  
                            if (player.paused()){
                                $('#pause_touch').hide(); 
                                $('#play_touch').show();
                            } else {  
                                $('#play_touch').hide(); 
                                $('#pause_touch').show(); 
                            }
                        }
                        if(!user_active){
                            if(!player.paused()){
                                $('#pause_touch').hide();
                            }
                        }
                    },100);
                    $('#video_block_html5_api').on('click',function(e){ 
                         e.preventDefault();
                        if(played_status){
                            if (player.paused()){
                                $('#pause_touch').hide(); 
                                    $('#play_touch').show(); 
                            } else{
                                $('#play_touch').hide();   
                                $('#pause_touch').show(); 
                            }
                        }
                     });
                    $(".vjs-control-bar").show();
                    $('#video_block').trigger('click');
                }
                    <?php if($v_logo != ''){ ?>
                        main_video.watermark({
                            file: "<?php echo $v_logo; ?>",
                            xrepeat: 0,
                            opacity: 0.75
                        });
                        
                    <?php } ?>  
                    $(".vjs-tech").mousemove(function () {
                        if (full_screen == true && show_control == false) {
                            $("#video_block .vjs-control-bar").show();
                            show_control = true;
                            var timeout = setTimeout(function () {
                                if (full_screen == true) {

                                }
                            }, 10000);
                            $(".vjs-control-bar").mousemove(function () {
                                event.stopPropagation();
                            }).mouseout(function () {
                                event.stopPropagation();
                            });
                        } else {
                            clearTimeout(timeout);
                        }
                    });

                    videojs("video_block").on("fullscreenchange", resize_player);
                    var player = videojs("video_block");
                    main_video.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                    });
                     player.on("play", function () {
                        $.post(url, {movie_id: movie_id,status: "start", log_id : log_id, video_length : player.duration()}, function (res) {
                            if(res > 0)
                            {
                                log_id = res;
                            }
                            else{
                                history.go(-1);
                            }                                        
                        });
                        started = 1;
                        played_status = 1;
                        ended = 0;
                        logged = 0
                    });
                    
                    player.on("ended", function () {
                        $.post(url, {movie_id: movie_id, status: "complete", log_id : log_id, played_length : player.currentTime()}, function (res) {
                            log_id = 0;
                        });
                        ended = 1;
                        started = 0;
                    });
                    
                    player.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                        previousBufferEnd = bufferDurationaas;
                        var r = player.buffered();
                        var buffLen = r.length;
                        buffLen = buffLen - 1;
                        bufferDurationaas = r.end(buffLen);
                        
                        
                        var curlength = Math.round(player.currentTime());
                        var fulllength = player.duration();
                        fulllength = Math.round((fulllength*percen)/100);
                        if(curlength === fulllength && fulllength > 1)
                        {
                            percen = parseInt(percen)+5;
                            //logged = 0;
                            var buff_log_id = document.getElementById('buff_log_id').value;
                            updateBuffered(player,curlength,buff_log_id);
                            $.post(url, {movie_id: movie_id, status: "halfplay", log_id : log_id, played_length : player.currentTime() }, function (res) {
                                log_id = res;
                            });
                        }
                        
                    });
                    player.on('changeRes', function () {
                    seekStart = 123;
                    forSeek = 0;
                    if(forSeek == 0){
                        $(".vjs-error-display").addClass("hide");
                        forSeek = 123;
                        player.pause();
                        var currTimeisisi = currentTime;
                        if (typeof player.getCurrentRes === "function") {
                            var currentVideoResolution = player.getCurrentRes();
                        } else {
                            var currentVideoResolution = 144;
                        }
                        var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                        var video = document.getElementsByTagName("video")[0];
                        $.post(createSignedUrl, {video_url: videoToBePlayed,is_ajax: 1}, function (res) {
                            player.src(res).play();
                            player.on("loadeddata", function () {
                                player.currentTime(currTimeisisi);
                                $(".vjs-control-bar").show();
								$('body').find('#vjs-tip').remove();
                                $(".vjs-error-display").removeClass("hide");
                            });
                        });
                    }
                });
                $("video").on("seeking", function () {
                    var bufferDuration = this.buffered.end(0);
                    var currTim = player.currentTime();
                    if(forSeek == 0){
                        if (bufferDuration < currTim) {
                            if (seekStart === null) {
                                $(".vjs-error-display").addClass("hide");
                                if (typeof player.getCurrentRes === "function") {
                                    var currentVideoResolution = player.getCurrentRes();
                                } else {
                                    var currentVideoResolution = 144;
                                }
                                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
								$('body').find('#vjs-tip').remove();
                                $.post(createSignedUrl, {video_url: videoToBePlayed,is_ajax: 1}, function (res) {
                                    seekStart = previousTime;
                                    player.src(res).play();
                                    player.on("loadeddata", function () {
                                        player.currentTime(currTim);
                                        $(".vjs-error-display").removeClass("hide");
                                    });
                                });
                            }
                        }
                        updateNewBuffered(player,previousBufferEnd,currentTime);
                    }
                });    
                        player.on('loadedmetadata', function() {
                            $(".vjs-control-bar").show();
                            var duration = player.duration();
                            $('#full_video_duration').val(duration);
                            if (typeof player.getCurrentRes === "function") {
                                var video_resolution = player.getCurrentRes();
                            } else {
                                var video_resolution = 144;
                            }
                            $('#full_video_resolution').val(video_resolution);
                            buffered_loaded();
                         });
                    $("video").on('seeked', function () {
                        if(adavail == 0)
                            seekStart = null;
                    });
                player.on('error', function () {
                    if($('.vjs-modal-dialog-content').html()!==''){
                            $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source+"</div>");
                        } else {
                    if(document.getElementsByTagName("video")[0].error != null){
                        var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                        if (videoErrorCode === 2) {
                            $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                        } else if (videoErrorCode === 3) {
                            $(".vjs-error-display").html("<div>"+the_video_playback_was_aborted+"</div>");
                        } else if (videoErrorCode === 1) {
                             $(".vjs-error-display").html("<div>"+you_aborted_the_video_playback+"</div>");
                        } else if (videoErrorCode === 4) {
                              $(".vjs-error-display").html("<div>"+the_video_could_not_be_loaded+"</div>");
                        } else if (videoErrorCode === 5) {
                            $(".vjs-error-display").html("<div>"+the_video_is_encrypted+"</div>");
                        }
                    }
                        }
            });
            if($("div.vjs-subtitles-button").length)
            {   
                var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
                var divtaggVal = divtagg.html();
                if(divtaggVal.trim() == 'subtitles off'){
                        divtagg.text("subtitles " +off);
                }
            }
            });
           <?php
                    }
                }
            ?>
     
      function resize_player() {
                if (full_screen == false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen == true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            
    });
</script>
