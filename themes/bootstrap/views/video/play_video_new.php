<!DOCTYPE html>
<html lang="en">
    <head>
        <!DOCTYPE html> 
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript">
		if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
        }
        //To know video is playing on which devices (1->mobile.0->web)
		var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
        </script>
		<?php 			
		//Notification Layout section		
		 $this->renderPartial('//layouts/notifications',array("authToken"=>$authToken));			
         $studio_ad = (isset($studio_ads) && count($studio_ads) > 0) ? $studio_ads[0] : array();
            $play_length = 0;
            $play_percent = 0;
            if(isset($durationPlayed['played_length']) && isset($durationPlayed['played_percent'])) {
                $play_length = $durationPlayed['played_length'];
                $play_percent = $durationPlayed['played_percent'];    
            } 
            $embed_idnext = $embed_id_next;
            $content_name = explode('.',$mvstream['full_movie']);
            ?>
        <?php if($play_percent != 0 && $play_length != 0) { ?>
        <div class="modal fade" id="play_confirm" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" style="display:none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"> <?php echo $this->Language['resume_watching']; ?></h4>
                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" onclick="FromDurationPlayed();"><?php echo $this->Language['yes']; ?></button>
                                <button data-dismiss="modal" class="btn btn-default-with-bg" type="button" onclick="FromBeginning();"><?php echo $this->Language['btn_cancel']; ?></button>
                            </div>  
                        </div>
                    </div>
                </div>
        <?php } ?>                
        <script type='text/javascript'>
            // video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var no_compatible_source ="<?php echo $this->Language["no_compatible_source"];?>";
            var restrict_platform_ios ="<?php echo $this->Language["restrict_platform_ios"];?>";
            var restrict_platform_Android ="<?php echo $this->Language["restrict_platform_Android"];?>";
            var muviWaterMark = "";
            <?php if($waterMarkOnPlayer != ''){ ?>
               muviWaterMark = "<?php echo $waterMarkOnPlayer;?>";
            <?php } ?>
        </script>        
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/css/bootstrap.min.css" />
		<!--Start videoJs-->
        <link rel="stylesheet" href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video-js.min.css?v=22223456">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video.min.js?v=<?php echo RELEASE ?>"></script>
		<!--Start Resolution Switcher-->			
		<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/videojs-resolution-switcher.css?rand=<?php echo RELEASE; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/videojs-resolution-switcher.js?rand=<?php echo RELEASE; ?>"></script>
		<!--Start Watermark Section--> 
		<link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/videojs.watermark.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />    
		<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/videojs.watermark.js?v=<?php echo RELEASE ?>"></script>  
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl() ?>/css/player.css?v=123" />
        <!-- for dfp -->
        <?php
        if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null)) {
        ?>   <link rel="stylesheet" href="//googleads.github.io/videojs-ima/src/videojs.ima.css" />
            <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
            <script src="//googleads.github.io/videojs-ima/src/videojs.ima.js"></script>
       <?php } ?>
		<!-- Play Pause link -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(); ?>/common/font-awesome/css/font-awesome.min.css">
		<script type="text/javascript">
			$(function(){				
				resizePlayer();				
				$(window).resize(function () {
					resizePlayer();
				});
			});
        </script>
        <style type="text/css">
            #ad-slot, video { width: 100%; height: 100%;}
            video {width: 100%;height: 100%;} 
            #ados-logger-events-list ul { list-style-type: none; }
            #ados-logger-events-list li.last { font-weight: bold; }
            #ad-slot{ position:absolute !important;top:0;left:0;right:0;left:-1%;width:100%;height:100%;}
           // .vjs-big-play-button{display: block !important;visibility: hidden;}
           <?php $check_mobile = Yii::app()->common->isMobile();
                 if(!$check_mobile){?>
                       .vjs-big-play-button{display: block !important;visibility: hidden;}
            <?php   }
                ?>
           .video-js .vjs-big-play-button{
                position: absolute;
                z-index: 1;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
           }
        </style>
<?php 
if(isset($block_ga)) {
	$block_ga = Yii::app()->session['block_ga'];
	if ($this->studio->google_analytics != '' && $block_ga != 0) {?>
		    <script>
					(function (i, s, o, g, r, a, m) {
						i['GoogleAnalyticsObject'] = r;
						i[r] = i[r] || function () {
							(i[r].q = i[r].q || []).push(arguments)
						}, i[r].l = 1 * new Date();
						a = s.createElement(o),
								m = s.getElementsByTagName(o)[0];
						a.async = 1;
						a.src = g;
						m.parentNode.insertBefore(a, m)
					})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
					ga('create', '<?php echo html_entity_decode($this->studio->google_analytics); ?>', 'auto');
					ga('send', 'pageview');
			</script>
		<?php
	}
}
?>
</head>
    <body>  
		<input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
        <div class="wrapper">
            <div class="videocontent" id="adContainer" style="overflow:hidden;">
                <div id="container">
                    <div id="video-player">
                <video id="video_block" class="video-js vjs-default-skin vjs-fluid vjs-16-9" webkit-playsinline>
                    <?php					
                    foreach ($multipleVideo as $key => $val) {	
						$label_key = ($key=='BEST')? $key: $key .'p';								
                        if(Yii::app()->common->isMobile() == 1){							
                            echo '<source src="' . $val . '" res="' . $key.'" label="' .$label_key.'" type="video/mp4" />';
                        } else{
                            echo '<source src="' . $fullmovie_path . '" res="' . $key . '" label="'.$label_key.'" type="video/mp4" />';
                        }
                    }					
                    foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                        if($subtitleFilesKey == 1){
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                        } else{
                            echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                        }
                    }
                    ?>					
                </video>				
            </div>
            </div>
        </div>
        </div>
        <div id="episode_block" class="episode_block">
            <a href="play_video_backup.php"></a>
        </div>                 
		<script>	
        //Poster Image
		var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";	
		//Video setup info
		var video_setup = { 
			'controls': true,
			'autoplay': true,					
			'poster': item_poster,						
			'nativeControlsForTouch': false,
            'preload':"none",
			//'textTrackSettings': false,
			'html5': {
				nativeTextTracks: false
			},
			'controlBar': {
					progressControl: {
					  keepTooltipsInside: true
					}
				},
			'plugins': {
				videoJsResolutionSwitcher: {
					default: '<?php echo $defaultResolution; ?>',					
					dynamicLabel: false
				}
			}			
		};
		//initialize video js
        var videoContent = document.getElementById('video_block');
		var player = videojs('video_block',video_setup,function () {});	
        //DFP - Ads Integration Code
        var dfdAdPlaying = 0;
        var adsManager;
        var adEvent;
        var adsLoader;
        var adDisplayContainer;
        var intervalTimer;
        
		//Flag variable to know play and pause (1 means pause 0 means play)
		var play_status = 0;
		//Flag Variable to check signed url
		var checkurlactive =0;
		//Only for resume watching
		var myplay_length = "<?php echo $play_length;?>";
		var multipleVideoResolution = new Array();
        multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
        var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
        var nAgt = navigator.userAgent;
        var browserName = getBrowserName(nAgt);
        $(document).ready(function () {	
            var seekStart = null;                
			var previousTime = 0;
			var currentTime = 0;		
			var previousBufferEnd =0;
			var bufferDurationaas = 0;																
            $('#video_block').bind('contextmenu', function () {
                return false;
            });
			player.ready(function() {
                <?php  if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4)) {
                        if(!($play_percent != 0 && $play_length != 0)) {
                ?>
                    if(!is_mobile){
                        init();
                    }                       
                <?php } 
                }?>
                //Remove title from subtitle label from edge and ie browser
				$('.vjs-menu-content li').removeAttr("title");
				$('#video_block').append('<img src="/images/left-arrow.png" width="35" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
				
				//Add button dynamically
                $('#video_block').append('<div class="common-css"><div class="prev-buttons"><i class="fa fa-step-backward" aria-hidden="true"></i></div><div class="player-buttons"><i class="fa fa-play" aria-hidden="true"></i></div><div class="next-buttons"><i class="fa fa-step-forward" aria-hidden="true"></i></div>');								
				
				player.on("play", function () {	
					player.posterImage.hide();
					play_status = 0;
				});
				player.on("pause",function(){
					play_status = 1;
				});
                //Disable Mouse Right Click on player
				$('#video_block').bind('contextmenu', function () {
					//return false;
				});
				//Play or Pause when press Space Key
				$(document).bind('keydown', function(e) {
					if (e.keyCode === 32) { 						
						playPauseToggle();				
					}
				});
				//For Mobile device TouchStart				
                $('.player-buttons').css( "opacity",1);
                $('.next-buttons').css( "opacity",0.5);
                $('.prev-buttons').css( "opacity",0.5);
				if(is_mobile === 1){
					$('.vjs-control-bar').bind('click',function(e){
						e.preventDefault();
					});
//					$('#video_block_html5_api').bind('touchstart',function(e){ 						
//						playPauseToggleMobile();
//					});	
                    $('.vjs-menu-content').bind('click',function(e){
                        $('.vjs-menu-content').hide();
                    });
                    $('.vjs-subtitles-button').bind('click',function(e){
                        $('.vjs-menu-content').show();
                    });
                    $('.vjs-menu-button').bind('click',function(e){
                        $('.vjs-menu-content').show();
                    });
                    $('#video_block').one('touchstart',function(e){
                       //put condition of dfp
                    <?php  if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4)) {
                        ?>
                        init();
                <?php 
                }?>
                    });
                   //play button touch
					$('.player-buttons').bind('touchstart',function(e){						
						e.preventDefault();
						if(player.paused()){
                            player.play();								
							$('.player-buttons').find("i").removeClass('fa-play').addClass('fa-pause');
							$('.common-css').show();
                        }else{						
							player.pause(); 							
							$('.player-buttons').find("i").removeClass('fa-pause').addClass('fa-play');
							$('.common-css').show();
                        }	
					});
				}else{					
					//Play and pause
                   $('.videocontent').mouseover(function() {
						if(!player.paused()){							
							$('.player-buttons').find("i").removeClass('fa-play').addClass('fa-pause');
							$('.common-css').show();
                        }else{
							$('.player-buttons').find("i").removeClass('fa-pause').addClass('fa-play');
							$('.common-css').show();
                        }
					});
				$('.videocontent').mouseout(function() {
						if(!player.paused()){
							$('.common-css').hide();							
						}
					});
					$('.videocontent').mousemove(function() {
						console.log('move-mouse');
						if(!player.paused()){
							$('.common-css').show();							
						}
					});
					//click on play/pause on middle
					$('.player-buttons').bind('click',function(e){
                        e.preventDefault();
						if(player.paused()){
							player.play();								
							$('.player-buttons').find("i").removeClass('fa-play').addClass('fa-pause');
							$('.common-css').show();
                        }else{						
							player.pause(); 							
							$('.player-buttons').find("i").removeClass('fa-pause').addClass('fa-play');
							$('.common-css').show();
                        }	
					});
                }
                setInterval(function() {
                    if(player.paused()){
                        $('.player-buttons').find("i").removeClass('fa-pause').addClass('fa-play');
                    } else {
                        $('.player-buttons').find("i").removeClass('fa-play').addClass('fa-pause');
                        $('.common-css').hide();
                    }
                }, 1000);
                player.on('resolutionchange', function(){								
                    checkurlactive = 0;
					seekStart = previousTime;                
					var currTim = player.currentTime();								
					createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, currTim,browserName);                
					console.info('Resolution Source changed to %s', player.src());
				});
				player.on("seeking", function () {	
					console.log("call seeking");
					//document.getElementById("video_block").style.cursor = 'none';																			
					var currTim = player.currentTime();	
					console.log("call seeking previous buffer"+previousBufferEnd+"Current Time"+currTim);
					if (previousBufferEnd < currTim) {						
						if (seekStart === null) {
							console.log("seek star");
							seekStart = previousTime;
							createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, player.currentTime(),browserName);
						}
					}		
				});

				player.on('seeked', function () {
					console.log("Seek End");
					seekStart = null;
				});
				player.on('timeupdate', function () {
					previousTime = currentTime;
					currentTime = player.currentTime();					
					previousBufferEnd = bufferDurationaas;
					var r = player.buffered();
					var buffLen = r.length;
					buffLen = buffLen - 1;
					bufferDurationaas = r.end(buffLen);				
					//console.log("Time update in video_new"+currentTime+"previous time"+previousTime+"BufferDurationaas"+bufferDurationaas+"checkurlactive"+checkurlactive);
					 t = currentTime + 1;
					if(currentTime  < t){
					   document.getElementById("video_block").style.cursor = 'default';
					}
					if(checkurlactive != 0 && currentTime>checkurlactive){
						checkurlactive = 0;
					}
				});
				player.on('ended', function() {	
					console.log("on Ended");
					 player.posterImage.show();
				});
				player.on('error', function () {
					console.log("Error section in video page");
					var errordetails = this.player().error();
					console.log("Error Code details"+JSON.stringify(errordetails));
					if (errordetails.code == 2 && seekStart === null){
						seekStart = 123;
						createSignUrlForpage(createSignedUrl, player, multipleVideoResolution,currentTime,browserName);
						$(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
					}
					if($('.vjs-modal-dialog-content').html()!==''){					
						$(".vjs-modal-dialog-content").html("<div>"+no_compatible_source+"</div>");
					} else {
						if(document.getElementsByTagName("video")[0].error != null){
							var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
							if (videoErrorCode == 2) {					
								if(seekStart === null){
									seekStart = 123;
									createSignUrlForpage(createSignedUrl, player, multipleVideoResolution,currentTime,browserName);
								}
								$(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
							} else if (videoErrorCode === 3) {
								$(".vjs-error-display").html("<div>"+the_video_playback_was_aborted+"</div>");
							} else if (videoErrorCode === 1) {
								 $(".vjs-error-display").html("<div>"+you_aborted_the_video_playback+"</div>");
							} else if (videoErrorCode === 4) {
								  $(".vjs-error-display").html("<div>"+the_video_could_not_be_loaded+"</div>");
							} else if (videoErrorCode === 5) {
								$(".vjs-error-display").html("<div>"+the_video_is_encrypted+"</div>");
							}
						}
				  }
			});
			//Adding Watermark Logo Section 
			<?php if($v_logo != ''){ ?>			
				player.watermark({
					file: "<?php echo $v_logo; ?>",
					xrepeat: 0,
					opacity: 0.75
				});
				if (is_mobile !=0) {				
					$(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;cursor:pointer;");
				}else{
					$(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
				}
				$(".vjs-watermark").click(function () {
					window.history.back();
				});
			<?php } ?>
			// Adding Resume Watching Section	
			<?php if($play_percent != 0 && $play_length != 0) { ?>				
					player.pause();
					play_status = 1;
					$('#play_confirm').modal('show');
					$(document).bind('keydown', function(e) {
					   if (e.keyCode === 13) {
						$('#confirm_yes').trigger('click');      
					   }
					});
			<?php }else{ ?>	
				player.play();
			<?php } ?>
            });
			setInterval(function() {
				if(!player.paused()){
						$('.common-css').hide();
				} 
			}, 5000);
		});
		// When we change Resolution and seek 
        function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, currentTimess, browserName) {				
           // document.getElementById("video_block").style.cursor = 'none';            			
            $(".vjs-error-display").addClass("hide"); 			
            if (typeof player.currentResolution === "function") {
				var resolution_obj = player.currentResolution();				
                var currentVideoResolution = resolution_obj.sources[0].res;//player.getCurrentRes();
            } else {				
                var currentVideoResolution = 144;
            }				
            if(checkurlactive!=0){				
                return false;
            }
            if (is_mobile === 0) {                                         
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];                                            
				//Ajax Call Start
                $.ajax({
					url: createSignedUrl,
					type: 'post',
					data: {
						video_url: videoToBePlayed+'?t='+currentTimess,                         
						browser_name: browserName
					},
					success: function(res) {
                        if(res){
                            if(dfdAdPlaying == 1){
                                return false;
                            }
                            if(!player.paused()){
								player.src(res).play().handleTechSeeked_();
							}else{
								player.src(res).pause().handleTechSeeked_();
							}	
                            checkurlactive = currentTimess+10;								
							$(".vjs-error-display").removeClass("hide");
                            player.on("loadedmetadata", function() {
							   player.currentTime(currentTimess);
                               if(player.pause() && play_status == 1){
                                    player.play();
                                    player.pause();
                                } else {
                                    player.play();
                                }
							});
                        }
					},
					error: function(jqXhr, textStatus, errorThrown){ 
						console.log("Error :: " +  errorThrown ); 
					}
				});        
			}
		}
		function resizePlayer(){
			var thisIframesHeight = $(window).height();
			$(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px;width:100%;');
		} 				
		// When Click On Yes Button on Resume Watching Modal Window
		function FromDurationPlayed() {    
            if(myplay_length!="")
                player.currentTime(myplay_length);		
            player.play();
            player.on('loadedmetadata',function(){
                player.currentTime(myplay_length);
            });
			play_status = 0;
            $('.modal').hide(); 
            //check DFP codes
            <?php  if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4)) {
                ?>
                        init();
                <?php 
                }?>
        }
		// When Click On Cancel Button on Resume Watching Modal Window
		function FromBeginning() {           
            player.play();
			play_status = 0;
            $('.modal').hide();
            <?php  if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4)) {
                ?>
                        init();
                <?php  
                }?>
        }
		// Return name of the browser
		function getBrowserName(nAgt){
			// In Opera 15+, the true version is after "OPR/" 
			var verOffset;
			if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
				browserName = "Opera";
			}
			// In older Opera, the true version is after "Opera" or after "Version"
			else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
				browserName = "Opera";
			}
			// In MSIE, the true version is after "MSIE" in userAgent
			else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
				browserName = "Microsoft Internet Explorer";
			}
			// In Chrome, the true version is after "Chrome" 
			else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
				browserName = "Chrome";
			}
			// In Safari, the true version is after "Safari" or after "Version" 
			else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
				browserName = "Safari";
			}
			// In Firefox, the true version is after "Firefox" 
			else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
				browserName = "Firefox";
			}else{
				browserName = navigator.appName;
			}
			return browserName;
		}
		function playPauseToggle(){
			if(player.paused()){
				player.play();	
				play_status = 0; 														
			}else{						
				player.pause(); 
				play_status = 1;							
			}		
		}
		function playPauseToggleMobile(){
//			if(!player.paused()){
//				$('.player-buttons').find("i").removeClass('fa-play').addClass('fa-pause');
//				$('.common-css').show();
//			}
			/*if(player.paused()){
				player.play();	
				play_status = 0; 														
			}else{						
				player.pause(); 
				play_status = 1;							
			}*/		
		}		
		function hidePlayPause(){
			if(!player.paused()){
				setTimeout(function(){
					$('.common-css').hide();   
				},5000); 
			}
		}
		</script>
		<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/screentime.js"></script>
		<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/new_backbutton.js"></script>
		<script>
		 <?php
			$action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
			$action = explode('/', $action);
			$page = $action[count($action)-1];
			if(strpos($action[count($action)-1],'?') !== FALSE){
				$page = $action[count($action)-2];
			}
			$full_domain = 'www.'.$studio->domain;
			 if($page == $studio->domain || $page == $full_domain){
				$page = 'home';
			}
        ?>
		 $.screentime({
              fields: [
                    { selector: 'html',
                      name: 'HTML'
                    }
              ],
              reportInterval: <?php echo REPORT_INTERVAL;?>,
              googleAnalytics: false,
              callback: function(data, log) {
                    <?php
                        $action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
                        $action = explode('/', $action);
                        $page = $action[count($action)-1];
                        if(strpos($action[count($action)-1],'?') !== FALSE){
                            $page = $action[count($action)-2];
                        }
                        $full_domain = 'www.'.$studio->domain;
                         if($page == $studio->domain || $page == $full_domain){
                            $page = 'home';
                        }
                    ?>
                    var timeInMs = Date.now();
                    var page = btoa(timeInMs+"<?php echo $page;?>"+timeInMs);
                    var timeDiff = <?php echo REPORT_INTERVAL;?>/60;
                    var urlForReport = "<?php echo Yii::app()->baseUrl; ?>/site/logVisitor?pid="+page;
                    $.post(urlForReport, {timeDiff:timeDiff}, function (res) {});
              }
			});
		</script>
		<?php 
        if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null)) {
            $this->renderPartial('//layouts/ads_spotX', array('MonetizationMenuSettings'=>$MonetizationMenuSettings,'response'=>$response,'movie_strm'=>$movie_strm,'bit_pre_roll'=>$bit_pre_roll,'bit_post_roll'=>$bit_post_roll,'mvstream'=>$movie_strm,'play_length'=>$play_length,'play_percent'=>$play_percent)); 
        }
        if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 3) && (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null)) {
            $this->renderPartial('//layouts/ads_dfp', array('MonetizationMenuSettings'=>$MonetizationMenuSettings,'response'=>$response,'movie_strm'=>$movie_strm,'mvstream'=>$movie_strm)); 
        }
        $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); 
        $this->renderPartial('//layouts/video_view_log_new', array('stream_id' => $stream_id, 'movie_id' => $movie_id, 'flag' => $flag, 'stream_id_next' =>$stream_id_next, 'permaLink' => $permaLink, 'embed_idnext' => $embed_idnext,"enableWatchDuration"=>$enableWatchDuration));		
        $this->renderPartial('//layouts/buffer_log_new', array('stream_id' => $stream_id, 'movie_id' => $movie_id));			 
        $this->renderPartial('//layouts/auto_play', array('stream_id_next' => $stream_id_next, 'embed_id_next' => $embed_id_next, 'permaLink' =>$permaLink,'embed_id_prev' =>$embed_id_prev, 'stream_id_prev' =>$stream_id_prev,'content' =>$content,'permalinkNext'=>$permalinkNext,'permalinkPrev'=>$permalinkPrev,'current_contentType'=>$current_contentType,'contentType_next'=>$contentType_next,'contentType_prev'=>$contentType_prev,'permalinkNext_multi'=>$permalinkNext_multi,'permalinkPrev_multi'=>$permalinkPrev_multi));
        ?>		
    </body>
</html>