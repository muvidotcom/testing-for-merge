<!DOCTYPE html>
<html lang="en">
    <head>
        <!DOCTYPE html> 
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=11" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            var user_id="<?php echo Yii::app()->user->id; ?>";
            var STORE_AUTH_TOKEN="<?php echo $authToken; ?>";  
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true); ?>";
            var notification = 0;
            <?php if ($this->notification){ ?>
                notification = "<?php echo $this->notification; ?>";
            <?php } ?>
            var favicon = "<?php echo $this->favIcon; ?>";
        </script>
        <?php
            $v = RELEASE;
            $randomVar = rand();
            $play_length = 0;
            $play_percent = 0;
            if(isset($durationPlayed['played_length']) && isset($durationPlayed['played_percent'])) {
                $play_length = $durationPlayed['played_length'];
                $play_percent = $durationPlayed['played_percent'];     
            }
            $adn = AdNetworks::model()->findByPk($MonetizationMenuSettings[1]['ad_network_id']);
            $arr = explode(",",@$mvstream->roll_after);
        ?>
         <?php if($play_percent != 0 && $play_length != 0) { ?>
                <div class="modal fade" id="play_confirm" tabindex="-1" role="dialog" style='z-index:12000'>
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"> <?php echo $this->Language['resume_watching']; ?></h4>
                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" onclick="FromDurationPlayed()"><?php echo $this->Language['yes']; ?></button>
                                <button data-dismiss="modal" class="btn btn-default-with-bg" type="button" onclick="FromBeginning()"><?php echo $this->Language['btn_cancel']; ?></button>
                            </div>  
                        </div>
                    </div>
                </div>
        <?php } ?>
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/js/themes/notification.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript">if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>
        <script type='text/javascript'>
            //video js error message
            var you_aborted_the_video_playback ="<?php echo $this->Language["you_aborted_the_video_playback"]; ?>";
            var a_network_error_caused ="<?php echo $this->Language["a_network_error_caused"]; ?>";
            var the_video_playback_was_aborted ="<?php echo $this->Language["the_video_playback_was_aborted"]; ?>";
            var the_video_could_not_be_loaded ="<?php echo $this->Language["the_video_could_not_be_loaded"]; ?>";
            var the_video_is_encrypted ="<?php echo $this->Language["the_video_is_encrypted"]; ?>";
            var muviWaterMark = "";
            <?php if(isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != ''){ ?>
                var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
            <?php } else if($waterMarkOnPlayer != ''){ ?>
                var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
            <?php } ?>
        </script>
        <style>
            body { margin: 0; padding: 0; margin: 0px; overflow: hidden; overflow-wrap: break-word; width: 103%; height: 100%;}
            #container { margin-left: 20px; }
            #video-player { position:relative;left:-1%;width:100%;height:100%;}
            #ad-slot, video { width: 100%; height: 100%;}
            video {width: 100%;height: 100%;} 
            #ados-logger-events-list ul { list-style-type: none; }
            #ados-logger-events-list li.last { font-weight: bold; }
            #ad-slot{ position:absolute !important;top:0;left:0;right:0;left:-1%;width:100%;height:100%;}
        </style>
        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl() ?>/common/bootstrap/css/bootstrap.min.css" />          
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videoseeking.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/urlparams.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs.progressTips.js"></script> 
        
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs.progressTips.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />    
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/video.js.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/mediascreenresolution.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>    
        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
            .video-js {height: 50%; padding-top: 48%;}
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls {
                display:none !important;
            }
            video::-webkit-media-controls-panel {
                display: none !important;
            }
            body {
                background: #000;
            }
        #backButton {
            position:absolute;
            left: 100px;
            top: 70px;
            display: none;
            z-index: 1; 
        }
        #pause_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none;
        }
        #play_touch{
            margin: auto;
            left:0;
            right: 0;
            bottom: 0;
            top: 0;
            position: absolute;
            height: 64px;
            width: 64px;
            display: none; 
        }
        </style>
        <?php 
        if(isset($block_ga)){ 
            $block_ga = Yii::app()->session['block_ga'];
        if ($this->studio->google_analytics != '' && $block_ga!=0 ) { ?>
            <script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', '<?php echo html_entity_decode($this->studio->google_analytics); ?>', 'auto');
    ga('send', 'pageview');
            </script>
            <?php
        }}
        ?>        
    </head>
    <body>
        <input type="hidden" id="full_video_duration" value="" />
        <input type="hidden" id="full_video_resolution" value="" />
        <input type="hidden" id="u_id" value="0" />
        <input type="hidden" id="buff_log_id" value="0" />
        <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
        <div class="wrapper">
            <div class="videocontent" style="overflow:hidden;">
                <div id="container">
                    <div id="video-player">
                        <video id="video_block" class="video-js moo-css vjs-default-skin" <?php if($play_percent != 0 && $play_length != 0){ ?> autoplay="false" preload="none" autobuffer="false" <?php } else{ ?> autoplay preload="auto" autobuffer <?php } ?> width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}' controls="true" webkit-playsinline>
                            <?php
                            foreach ($multipleVideo as $key => $val) {
                                if(Yii::app()->common->isMobile() == 1){
                                    echo '<source src="' . $val . '" data-res="' . $key . '" type="video/mp4" />';
                                } else{
                                    echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                                }
                            }
                            foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval){
                                if($subtitleFilesKey == 1){
                                    echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                                } else{
                                    echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                                }
                            }
                            ?>
                        </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        var play_status = 0;
        </script>
        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
      
        <script type="text/javascript" src="//js.spotx.tv/directsdk/v1/<?php echo $MonetizationMenuSettings[1]['channel_id'];?>.js?v=<?php echo $v ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/adstesting/sdk/custom_video_player.js"></script>
        <script type="text/javascript">
            var myplay_length = "<?php echo $play_length;?>";
            console.log("last_watched_value ::"+  myplay_length );
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;
            var setInterSpeed = '';
            var multipleVideoResolution = new Array();
            multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
            var ended = 0;
            var videoOnPause = 0;
            var adavail = 0;
            var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
            var nAgt = navigator.userAgent;
            var browserName = navigator.appName;
            var verOffset;
            var bitCLick = false;
            var bitFromBeginning = false;
            //var arrindexes = [50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000];
            var arrindexes = "<?php echo @$mvstream->roll_after;?>";
            arrindexes = arrindexes.split(',');
            var bit_pre_roll  ="<?php echo @$bit_pre_roll;?>";
            var bit_post_roll ="<?php echo @$bit_post_roll;?>";
            var cnti =0;
            var cntipre =0;
            var cntiend =0;
            var ad_bit_parameter = '';
            var rolltype ="<?php echo $mvstream->rolltype; ?>";
            /*
             * Spotx ads global variables
             */
            var adSlot = "";
            var videoPlayerspotx = document.getElementById('video-player');
            var videoelement = "";
			var directAdOS = "";
            var requestSpotxads = "";
            // In Opera 15+, the true version is after "OPR/" 
            if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
                browserName = "Opera";
            }
            // In older Opera, the true version is after "Opera" or after "Version"
            else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
                browserName = "Opera";
            }
            // In MSIE, the true version is after "MSIE" in userAgent
            else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
                browserName = "Microsoft Internet Explorer";
            }
            // In Chrome, the true version is after "Chrome" 
            else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                browserName = "Chrome";
            }
            // In Safari, the true version is after "Safari" or after "Version" 
            else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
                browserName = "Safari";
            }
            // In Firefox, the true version is after "Firefox" 
            else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
                browserName = "Firefox";
            }
            var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";
            var player = '';
            var percen = 5;
            var play_length = 0;
            var t = 0;
            var bitControlbarclick=false;
            var bitAdsFlag = true;
            var callfromduration = false;
            var successres = false;
            var checkurlactive =0;
            $(document).ready(function () {
                var movie_id = "<?php echo $movie_id ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var can_see = "<?php echo $can_see ?>";
                var wiki_data = "<?php echo isset($wiki_data) ? $wiki_data : '' ?>";
                var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
                var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
                var content_type = "<?php echo isset($content_type) ? $content_type : "" ?>";
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var previousBufferEnd =0;
                var bufferDurationaas = 0;
                var isiPad = null;
                var isAndroid = null;
                var is_restrict = "<?php echo isset($is_restrict) ? $is_restrict : 0 ?>";
                var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
                
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });

                $('.videocontent').mouseover(function() {
                    $('#backButton').show();
                });
                $('.videocontent').mouseout(function() {
                    $('#backButton').hide();
                });
                var playerSetup = videojs('video_block',{plugins: {resolutionSelector: {
                            force_types: ['video/mp4'],
                            default_res: "<?php echo $defaultResolution; ?>"
                        }}});
                playerSetup.ready(function () {
                $('.video_block_html5_api').attr('style','left:-40%;'); 
                player = this;
            
            //Added by prangya 
            this.progressTips(); // Displying duration in the player during playback.
                player.play();
            if(browserName.toLowerCase().match('firefox'))
                $(".vjs-control-bar").attr('style','left:0px;bottom:0px;right:0px');

        //SPOTX --------------------------------------------------------------------------------------------start@avi
        <?php if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) 
                && (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null)) { ?>                
    $('#player-controls').attr('style','display:none');
    //pre-roll
    player.on('loadedmetadata', function() {
        if(bit_pre_roll.match('start')){
                console.log("bit_pre_roll ::: " + bit_pre_roll + " bitAdsFlag ::: " + bitAdsFlag + " cntipre ::: " + cntipre);
        try {
                            if(cntipre>0 && bitAdsFlag==false){
                                return false;
                            }else{
			$('#player-controls').attr('style','display:none');
            <?php
            if ($play_percent != 0 && $play_length != 0) {
					if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) 
						&& (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null) 
						&& (@$mvstream->rolltype == 1) || ((@$mvstream->rolltype == 3) || (@$mvstream->rolltype == 7) 
						|| (@$mvstream->rolltype == 5))) {
                ?>
					player = this;
					if ($('.modal').is(':visible')) {
						player = this;
						player.pause();
						$('.modal').show();
					}
                <?php
				}
            } else {
                            if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0][menu] & 4) && (@$MonetizationMenuSettings[1]['channel_id'] != null) && ((@$mvstream->rolltype == 1) || (@$mvstream->rolltype == 3) || (@$mvstream->rolltype == 7) || (@$mvstream->rolltype == 5))) {
                ?>
                player = this;
                ad_bit_parameter = 'pre';
                requestSpotxads.ads_start(ad_bit_parameter,player);
				cntsulr=0;
				console.log("://pre-roll");
				cntipre++; 
				//createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, currentTime, browserName); 
        <?php
			}
        }
        ?>
        }
        }
		catch(err) {
			console.log(" :: SpotX AD_ERROR :: " + err);
		}
            }
    });
    //mid-roll
     player.on('timeupdate', function(){
       try {
                if(arrindexes.length>0){
                    var AddddTimmmmm = parseInt(Math.round(player.currentTime()));
                    AddddTimmmmm = AddddTimmmmm.toString();
                    //console.log("AddddTimmmmm :::::::::::: "+ AddddTimmmmm);
                        try {
            //only pre || post ads activated
            if(rolltype==1 || rolltype==5 || rolltype==4){
                                return false;
            }else{
                                    if($.inArray(AddddTimmmmm, arrindexes) !== -1){
                                    //if(arrindexes[0] == parseInt(Math.round(player.currentTime()))){
                                        var index_val = $.inArray(AddddTimmmmm, arrindexes);
                                        console.log("mid-roll ::: "+ arrindexes[index_val] + " other :: " + cntiend +"----------"+ cntipre +"----------"+ bitAdsFlag)
                                if($('#ad-slot').length > 0 && cntiend == 0 && cntipre >0){
                                    $("div[id^='ad-']").remove();
              } else {
                                                if(cnti===0){
                            ad_bit_parameter = 'mid';
                            requestSpotxads.ads_start(ad_bit_parameter,player);
                                    cntsulr =0;
                                    cnti++;
                                                var arrindexes_val = arrindexes.indexOf(AddddTimmmmm);
                                                delete arrindexes[arrindexes_val];
                                                console.log("arrindexes_FINAL ::: "+ arrindexes);
                }
                }
        }
                                }
                     }catch(err){
                            console.log(" :: SpotX AD_ERROR :: " + err);
                     }
            }
        }catch(err){
                console.log(" :: SpotX AD_ERROR :: " + err);         
        }});
    //post-roll 
    player.on('ended', function() {
            if(bit_post_roll.match('end')){
            console.log("bit_post_roll ::: " + bit_post_roll);
                    try {
                            if(cntiend >0 && bitAdsFlag==false){
                                player.pause();
                                return false;
                            }else{
                                $('#player-controls').attr('style','display:none');
										<?php
                                    if (@$mvstream->enable_ad == 1 && (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && (@$MonetizationMenuSettings[0][menu] & 4) 
                                    && (@$MonetizationMenuSettings[1]['channel_id'] != null) && ((@$mvstream->rolltype == 4) || (@$mvstream->rolltype == 7) 
                                    || (@$mvstream->rolltype == 6) || (@$mvstream->rolltype == 5))) {
								?>
                                        ad_bit_parameter = 'post';
                                        requestSpotxads.ads_start(ad_bit_parameter,player);
									cntsulr=0;
									console.log("://post-roll");
									cntiend++;  
                                        player.pause();
                                        //createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, currentTime, browserName); 
										<?php } ?>
								}
                        }
                        catch(err) {
                            console.log(" :: SpotX AD_ERROR :: " + err);
                }
                    }
    });
<?php }?>
        
        //SPOTX --------------------------------------------------------------------------------------------end@avi
                // Displying Play/pause button with animation during playback.
                <?php if(isset($_REQUEST['remove_back_button'])): ?>
                var iframeiheight = $(window).height();
    `           $( ".video-js" ).attr('style','padding-top:'+ iframeiheight +'px');
                <?php endif; ?>
                $('#video_block').append('<img src="/images/touchpause.png" id="pause_touch" />');
                $('#video_block').append('<img src="/images/touchplay.png" id="play_touch" />');
                $('#video_block').append('<img src="/images/pause.png" id="pause"/>'); 
                $('#video_block').append('<img src="/images/play-button.png" id="play" />');
                <?php if(!isset($_REQUEST['remove_back_button'])): ?>
                // Displying Back button with animation during playback.
                $('#video_block').append('<img src="/images/back-button.png" width="40" height="40" id="backButton" style="cursor:pointer" data-toggle="tooltip" title="Back to Browse" allowFullScreen = true/>'); 
                     $('#backButton').click(function(){
                         if($('#backbtnlink').val() !='')
                            window.location.href = $('#backbtnlink').val();
                        else
                           parent.history.back();
                        return false;
                      });
                      <?php endif; ?>
                    $(".vjs-fullscreen-control").click(function () {
                        $(this).blur();
                    });
                   
                    //Added by prangya
                    // use of enter key for pause the video during play.
                    $(document).bind('keydown', function(e) {
                        if (e.keyCode === 32) { 
                                //document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                                if(player.paused()){
                                    player.play();
                                } else{
                                    player.pause();
                                }
                        }
                    });
                   
                if(is_mobile === 0){ //if desktop browser
                    //res-buttion-click
                    $('.vjs-res-button').on('click',function(){
                        bitCLick = true;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                    });
                    $('.vjs-res-button').bind('touchstart',function(){
                        bitCLick = true;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                    });
                    $('#video_block_html5_api').on('click',function(e){ 
                            bitCLick = true;
                            $('#settingDiv').hide();
                            $('#video-res-sec').hide();
                            // document.getElementById('video_block_html5_api').onclick = function(e) {
                            e.preventDefault();                           
                            if (player.paused()){
                                $('#pause').hide(); 
                                $('#play').show();

                            } else{
                                $('#play').hide(); 
                                $('#pause').show(); 
                            }
                    });
                }else{ //For mobile browser
                     player.on("play", function () {
                         videoOnPause = 0;
                         $('#play_touch').hide();
                         $('#pause_touch').hide();
                     });
                     player.on("pause", function () {
                         videoOnPause = 1;
                         $('#play_touch').show();
                         $('#pause_touch').hide();
                     });
                     $('#pause_touch').bind('touchstart',function(e){
                            player.pause();
                     });
                     $('#play_touch').bind('touchstart',function(e){
                            player.play();
                     });
                     $('#video_block_html5_api').bind('touchstart',function(e){
                         //Hide Resolution - Cog.
                         bitCLick = true;
                         $('#settingDiv').hide();
                         $('#video-res-sec').hide();
                            if(player.play()){
                                $('#pause_touch').show(); 
                                $('#play_touch').hide(); 
                                setTimeout(function(){
                                $('#pause_touch').hide();   
                                 },3000); 
                             }
                    });
                }
                //Displaying videojs error message during play.
                    player.on('error', function () {
                        var errordetails = this.player().error();
                        if (errordetails.code == 2 && seekStart === null){
                            seekStart = 123;
                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime,browserName);
                            $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                        }
                       if($('.vjs-modal-dialog-content').html()!==''){
                            $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source+"</div>");
                        } else {
                        if(document.getElementsByTagName("video")[0].error != null){
                            var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                            if (videoErrorCode === 2) {
                                if(seekStart === null){
                                    seekStart = 123;
                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime,browserName);
                                }
                                $(".vjs-error-display").html("<div>"+a_network_error_caused+"</div>");
                            } else if (videoErrorCode === 3) {
                                $(".vjs-error-display").html("<div>"+the_video_playback_was_aborted+"</div>");
                            } else if (videoErrorCode === 1) {
                                 $(".vjs-error-display").html("<div>"+you_aborted_the_video_playback+"</div>");
                            } else if (videoErrorCode === 4) {
                                  $(".vjs-error-display").html("<div>"+the_video_could_not_be_loaded+"</div>");
                            } else if (videoErrorCode === 5) {
                                $(".vjs-error-display").html("<div>"+the_video_is_encrypted+"</div>");
                            }
                        }
                      }
                    });
                    
                    $(".vjs-tech").mousemove(function () {
                        if (full_screen === true && show_control === false) {
                            $("#video_block .vjs-control-bar").show();
                            show_control = true;
                            var timeout = setTimeout(function () {
                                if (full_screen === true) {

                                }
                            }, 10000);
                            $(".vjs-control-bar").mousemove(function () {
                                event.stopPropagation();
                            }).mouseout(function () {
                                event.stopPropagation();
                            });
                        } else {
                            clearTimeout(timeout);
                        }
                    });
                      //For Iphone;
                    player.on("fullscreenchange", resize_player);
                        isiPad = nAgt.match(/iPad/i);
                        if (isiPad) {
                            $('.vjs-fullscreen-control').hide();
                            var iframeiheight = $(window).height();
                            $( ".video-js" ).attr('style','padding-top:'+ iframeiheight +'px');
                            $( window ).on( "resize", function( event ){
                                var iframeiheight = $(window).height(); 
                            setTimeout(function () {
                                         $( ".video-js" ).attr('style','padding-top:'+ iframeiheight +'px');
                                     }, 0.001);
                            }); 
                            $('.vjs-res-button').attr('style','width : 14em; position: relative');
                            $('#backButton').bind('touchstart', function(){
                                if($('#backbtnlink').val() !='')
                                    window.location.href = $('#backbtnlink').val();
                                else
                                   parent.history.back();
                                return false;   
                            });
                            $(".vjs-control-content ").bind('touchstart', function(event) {
                                if ($('.vjs-control-content:visible').length === 0 ) {
                                    $('.vjs-menu').show();
                                }else{
                                    $('.vjs-menu').hide();
                                } 
                            });
                        }
                        
                        //For Android;
                        isAndroid = nAgt.match(/Android/i);
                        if(isAndroid) {
                           $('.vjs-fullscreen-control').hide();
                            var thisIframesHeight = $( window ).height();
                            $( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
                            $( window ).on( "orientationchange", function( event ) {
                                if(window.orientation === 0){
                                     thisIframesHeight = $( window ).height();
                                     $( ".video-js" ).attr('style','padding-top:155%; height:100%;');
                                }else{
                                    $( ".video-js" ).attr('style','padding-top:43%; height:50%;');
                                    }
                                });
                            
                            $(".vjs-res-button").bind('touchstart', function(event) {
                                     showSettingDiv();
                                });
                            $('#backButton').bind('touchstart', function(){
                                if($('#backbtnlink').val() !='')
                                    window.location.href = $('#backbtnlink').val();
                                else
                                   parent.history.back();
                                return false;   
                                });
                            $(".vjs-control-content ").bind('touchstart', function(event) {
                                if ($('.vjs-control-content:visible').length === 0 ) {
                                    $('.vjs-menu').show();
                                }else{
                                    $('.vjs-menu').hide();
                                    } 
                            });
                       }     
                    <?php if($v_logo != ''){ ?>
                        player.watermark({
                            file: "<?php echo $v_logo; ?>",
                            xrepeat: 0,
                            opacity: 0.75
                        });
                        if (is_mobile !== 0) {
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $("#video_block").attr('poster', item_poster)
                            $(".vjs-watermark").attr("style", "bottom:40px;right:1%;width:7%;cursor:pointer;");
                        }else{
                            $(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
                        }
                        $(".vjs-watermark").click(function () {
                            window.history.back();
                        });
                    <?php } ?>
                    if($("div.vjs-subtitles-button").length)
                    {
                        var divtagg = $(".vjs-subtitles-button .vjs-control-content .vjs-menu .vjs-menu-content li:first-child");
                        var divtaggVal = divtagg.html();
                        if(divtaggVal === 'subtitles off'){
                            divtagg.html("Subtitles Off");
                        }
                    }
                    $("#vid_more_info").hide();
                    $("#episode_block").html("");
                    $("#episode_block").hide();
                    if (is_mobile !== 0) {
                        $(".vjs-big-play-button").show();
                        player.pause();
                    } else {
                        if (full_movie.indexOf('http://youtu') > -1) {
                            player.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio"});
                            player.play();
                        }else{
                            if(browserName === "Safari"){
                                player.pause();
                                var videoToBePlayed = multipleVideoResolution["<?php echo $defaultResolution; ?>"];
                                $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data,browser_name:browserName}, function (res) {
                                    player.src(res).play();
                                });
                            }else{
                                player.src(full_movie).play();
                            }
                        } 
                    }
                
                <?php if($play_percent != 0 && $play_length != 0) { ?>
                 player.pause();
                 play_status = 1;
                 $('#play_confirm').modal('show');
                 $(document).bind('keydown', function(e) {
                if (e.keyCode === 13) {
                     $('#confirm_yes').trigger('click');      
                }
                });
                <?php } ?>
                    $(".vjs-control-bar").show();
                    // BACK BUTTON OF VIDEO PLAYER DOESN'T HIDE IN FULLSCREEN MODE.  
                    setInterval(function() {
                        var user_active = player.userActive();
                        if(user_active === true){
                            $('#backButton').show();
                        } else {
                            $('#backButton').hide();
                        }
                   }, 1000);
                    <?php 
                        if($showSetting == 1){ 
                            $showRes = 'AUTO ( '.$defaultResolution.'p )';
                            if($defaultResolution == 'BEST'){
                                $showRes = 'AUTO ( '.$defaultResolution.' )';
                            }
                    ?>
                            $('.vjs-res-button').html('<span onclick="showSettingDiv();" data-toggle="tooltip" title="Setting" onclick><svg xmlns:xlink="http://www.w3.org/1999/xlink" height="100%" version="1.1" viewBox="0 0 36 36" width="100%"><defs><path d="M27,19.35 L27,16.65 L24.61,16.65 C24.44,15.79 24.10,14.99 23.63,14.28 L25.31,12.60 L23.40,10.69 L21.72,12.37 C21.01,11.90 20.21,11.56 19.35,11.38 L19.35,9 L16.65,9 L16.65,11.38 C15.78,11.56 14.98,11.90 14.27,12.37 L12.59,10.69 L10.68,12.60 L12.36,14.28 C11.89,14.99 11.55,15.79 11.38,16.65 L9,16.65 L9,19.35 L11.38,19.35 C11.56,20.21 11.90,21.01 12.37,21.72 L10.68,23.41 L12.59,25.32 L14.28,23.63 C14.99,24.1 15.79,24.44 16.65,24.61 L16.65,27 L19.35,27 L19.35,24.61 C20.21,24.44 21.00,24.1 21.71,23.63 L23.40,25.32 L25.31,23.41 L23.62,21.72 C24.09,21.01 24.43,20.21 24.61,19.35 L27,19.35 Z M18,22.05 C15.76,22.05 13.95,20.23 13.95,18 C13.95,15.76 15.76,13.95 18,13.95 C20.23,13.95 22.05,15.76 22.05,18 C22.05,20.23 20.23,22.05 18,22.05 L18,22.05 Z" id="ytp-svg-39"></path></defs><use class="ytp-svg-shadow" xlink:href="#ytp-svg-39"></use><use class="ytp-svg-fill" xlink:href="#ytp-svg-39"></use></svg></span>');
                            $(".vjs-control-bar").append('<div onclick="showResDiv();" class="customized-res" id="settingDiv"><span style="text-align:left;">Quality<span> <span style="text-align:right; float:right;"><span id="currentPlayerRes"><?php echo $showRes; ?></span>&nbsp;&nbsp;<b>&gt;</b></span></div>');
                            $(".vjs-control-bar").append('<div id="video-res-sec" class="customized-res"></div>');
                            videoChangeRes('AUTO',player);
                            if (is_mobile !== 0) {    
                                $("#settingDiv").attr("style", "bottom:40px; width:140px;");
                                $("#video-res-sec").attr("style", "bottom:40px; width:60px;");
                            }else{
                                $("#settingDiv").attr("style", "bottom:46px; width:140px;");
                                $("#video-res-sec").attr("style", "bottom:46px; width:60px;");
                            }
                            if (isiPad) {
                                $("#settingDiv").bind('touchstart', function() {
                                    showResDiv();
                                });
                                $(".vjs-res-button").bind('touchstart', function(event) {
                                    showSettingDiv();
                                    $("#video-res-sec").bind('touchstart', function(event) {
                                        showResDiv(); 
                                    });
                                });
                            }
                    <?php } ?>
                    player.on("play", function () {
	                    document.getElementById("video_block").style.cursor = 'default';
                        //$('.vjs-loading-spinner').hide();
                        if($("div.vjs-poster").length)
                        {
                            $(".vjs-poster").attr('style','display: none;');
                        }
                        if (is_mobile !== 0) {
                            $(".vjs-big-play-button").hide();
                        }
                        videoOnPause = 0;
                        $("#episode_block").hide();
                    });

                    player.on("pause", function () {
                        videoOnPause = 1;
                        $("#episode_block").hide();
                    });

                    player.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                        previousBufferEnd = bufferDurationaas;
                        var r = player.buffered();
                        var buffLen = r.length;
                        buffLen = buffLen - 1;
                        bufferDurationaas = r.end(buffLen);
                        t = currentTime + 1;
                        if(currentTime  < t)    
                        {
                           document.getElementById("video_block").style.cursor = 'default';
                        }
                        if(checkurlactive != 0 && currentTime>checkurlactive){
                            checkurlactive =0;
                        }
                    });

                    player.on('changeRes', function () {
                        forChangeRes = 123;
                        seekStart = previousTime;
                        var currTim = previousTime;
                        
                       
                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim,browserName);
                        forChangeRes = 0;
                    });
                    player.on("progress",function(){
                        if (is_mobile === 0) {
                            if(videoOnPause === 0 && browserName=== "Safari" ){
                                //checkBuffer(player);
                            }
                        }
                    });

                   /*
                    * seeking protion
                    * @returns {undefined}
                    */

                    $("video").on('seeked', function () {
                            seekStart = null;

                    });
                    //Implementng Video Logs
                    var free_content_login = "<?php echo $free_content_login ?>";
                    if (is_studio_admin !== "true" || free_content_login == 0) {
                        <?php
                        if(Yii::app()->aws->isIpAllowed() && (isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log == 1)) || intval($free_content_login) == 0){?>
                            var started = 0;
                            var ended = 0;
                            var logged = 0;
                            var log_id = 0;


                           player.on('timeupdate', function() {
                            
                            if (adavail === 0 || adavail === 2) {
                                var curlength = Math.round(player.currentTime());
                                var fulllength = parseInt(document.getElementById('full_video_duration').value);
                                fulllength = Math.round((fulllength * percen) / 100);
                            }
                        });
                            
                            player.on('loadedmetadata', function() {
                                var duration = player.duration();
                                $('#full_video_duration').val(duration);
                                if (typeof player.getCurrentRes === "function") {
                                    var video_resolution = player.getCurrentRes();
                                } else {
                                    var video_resolution = 144;
                                }
                                $('#full_video_resolution').val(video_resolution);
                               
                                //focefully-show-subtitles
                                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                                {
                                    $('.vjs-subtitles').removeAttr("style");
                                    $('.vjs-text-track').removeAttr("style");
                                    $('.vjs-tt-cue').removeAttr("style");
                                    $('.vjs-text-track').attr("style","display:block");
                                    }
                             });
                        <?php } ?>
                    }
                });
                
                    /*
                    * modified :   Arvind 
                    * functionality : resolution cog click resolution options show/hide
                    * date     :   07-02-2017
                    */

                    $('.customized-res').on('click',function(e){
                        e.stopPropagation();
                    })
                     $('.vjs-res-button').on('click',function(e){
                        e.stopPropagation();
                        bitCLick = true;
                        getResolutionShowHide(bitCLick,player);
                     });
                     $(window).on('click',function(){
                        getResolutionShowHide(bitCLick,player)
                     });
                     $('.wrapper').on('click',function(){
                        getResolutionShowHide(bitCLick,player);
                     });
                     $('.vjs-control-bar').on('click',function(){
                        if($(this).find(".customized-res")){
                                $('#settingDiv').hide();
                                $('#video-res-sec').hide();
                        }
                     });
                     $('#video-res-sec').on('click',function(){
                        $('#video-res-sec').hide();
                     });
                  // functionality : resolution cog touch resolution options show/hide

                     $('.customized-res').bind('touchstart',function(e){
                        e.stopPropagation();
                     })
                     $('.vjs-res-button').bind('touchstart',function(e){
                        e.stopPropagation();
                        bitCLick = true;
                        getResolutionShowHide(bitCLick,player);
                     });
                     $(window).bind('touchstart',function(){
                        getResolutionShowHide(bitCLick,player)
                     });
                     $('.wrapper').bind('touchstart',function(){
                        getResolutionShowHide(bitCLick,player);
                     });
                     $('.vjs-control-bar').bind('touchstart',function(){ 
                        if($(this).find(".customized-res")){
                                $('#settingDiv').hide();
                                $('#video-res-sec').hide();
                        }
                     });
                     $('#video-res-sec').bind('touchstart',function(){
                        $('#video-res-sec').hide();
                    });
                    
                   
                    var x =1;
                    $("video").on("seeking", function () {
                    console.log("------------seeking 1st time "+ x)
                    x+=1;
                    bitAdsFlag =false;
                    cntseek = true;
                    document.getElementById("video_block").style.cursor = 'none';
                    var currTimmm = previousTime;
                    var currTim = player.currentTime();
                    console.log(previousBufferEnd + "-" + currTimmm);
                    console.log( bufferDurationaas + "-" + currTim);
                    if (forChangeRes === 0) {
                        if (previousBufferEnd < currTim) {
                            console.log(browserName);
                            if (seekStart === null) {
                                console.log("seek star");
                                seekStart = previousTime;
                                createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, player.currentTime(),browserName);
                                
                            }
                        }
                    }
                    //focefully-show-subtitles
                    if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                    {
                       $('.vjs-subtitles').removeAttr("style");
                       $('.vjs-text-track').removeAttr("style");
                       $('.vjs-tt-cue').removeAttr("style");
                       $('.vjs-text-track').attr("style","display:block");
                       }
            });
            });
        // Create Sign Url Forpage
        function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTimess, browserName) {
            document.getElementById("video_block").style.cursor = 'none';
            //$('.vjs-progress-control').attr("style", "pointer-events:none;");
            $(".vjs-subtitles ").hide();
            var currentTimesForVideo = currentTimess;
            var play_time = player.currentTime();
            $(".vjs-error-display").addClass("hide");
           
           // $(".vjs-control-bar").hide();
            if (typeof player.getCurrentRes === "function") {
                var currentVideoResolution = player.getCurrentRes();
            } else {
                var currentVideoResolution = 144;
            }
            if(checkurlactive!=0){
                return false;
            }
            if (is_mobile === 0) {
                var r = player.buffered();
                var buffLen = r.length;
                if (buffLen > 0) {
                    buffLen = buffLen - 1;
                } else {
                    buffLen = 0;
                }
                var bufferenEnddd = r.end(buffLen);
                var bufferStart = r.start(buffLen);
                if (typeof player.getCurrentRes === "function") {
                    var currentVideoResolution = player.getCurrentRes();
                } else {
                    var currentVideoResolution = 144;
                }
                lastBufferSize = 0;
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                $('#full_video_resolution').val(currentVideoResolution);
                
                var vid_duration = parseInt(Math.round(player.duration()%60));
                var curr_duration =  parseInt(Math.round(player.currentTime()%60));
                console.log("browserName ---- "+ browserName);
                bitAdsFlag = false;
            //Ajax Call Start
            $.ajax({
                url: createSignedUrl,
                type: 'post',
                data: {
                        video_url: videoToBePlayed+'?t='+currentTimess,
                        wiki_data: wiki_data, 
                        browser_name: browserName},
                success: function(res) {
                if(res)
                {
                    console.log("res1:::::"+ res);
                    if(ad_bit_parameter=='post'){
                        player.src(res).pause();
                    }
                    else{
                    player.src(res).play();
                    }
                    checkurlactive = currentTimess+10;
                    successres = true;
                    $('#custVidRes li').each(function() {
                        if ($(this).attr('selected') == 'selected') {
                            var newRes = currentVideoResolution + 'p';
                            if (currentVideoResolution == 'BEST') {
                                var newRes = currentVideoResolution;
                            }
                            if ($(this).attr('class') == 'AUTO') {
                                $("#currentPlayerRes").html('AUTO ( ' + newRes + ' )');
                            } else {
                                $("#currentPlayerRes").html(newRes);
                            }
                        }
                    });
                    
                    $(".vjs-error-display").removeClass("hide");
                    player.on("loadedmetadata", function() {
                       //console.log("after =========== "+ currentTimesForVideo +'====='+myplay_length);
                       player.currentTime(currentTimess); //seeking
                        //focefully-show-subtitles
                        if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                        {
                            $('.vjs-subtitles').removeAttr("style");
                            $('.vjs-text-track').removeAttr("style");
                            $('.vjs-tt-cue').removeAttr("style");
                            $('.vjs-text-track').attr("style","display:block");
                            }
                    });
                }
            },
            error: function(jqXhr, textStatus, errorThrown){ console.log("Error :: " +  errorThrown ); }
            });
        //Ajax Call End  
	} else {
                $('#custVidRes li').each(function() {
                    if ($(this).attr('selected') == 'selected') {
                        var newRes = currentVideoResolution + 'p';
                        if (currentVideoResolution == 'BEST') {
                            var newRes = currentVideoResolution;
                        }
                        if ($(this).attr('class') == 'AUTO') {
                            $("#currentPlayerRes").html('AUTO ( ' + newRes + ' )');
                        } else {
                            $("#currentPlayerRes").html(newRes);
                        }
                    }
                });
            }
        }
        function resize_player() {
            if (full_screen === false) {
                full_screen = true;
                var large_screen = setTimeout(function () {
                    if (full_screen === true) {
                    }
                }, 5000);
            } else {
                //clearTimeout(large_screen);
                full_screen = false;
            }
        }
            // If Safari browser
            function checkBuffer(player){
                    var currentTime = player.currentTime();
                    var playerDuration = player.duration();
                    var r = player.buffered();
                    var buffLen = r.length;
                    buffLen = buffLen - 1;
                    var bufferenEnddd = r.end(buffLen);
                    var bufferenEndd = bufferenEnddd - 1;
                    if(bufferenEndd < currentTime){
                        if(playerDuration !== bufferenEnddd){
                            player.pause();
                        }else{
                            player.play();
                         }
                    }else{
                        if(bufferenEnddd !== currentTime){
                            if(player.paused()){
                                player.play();
                            }
                        }
                    }
            }
        </script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/screentime.js"></script>
        <script type="text/javascript">
            <?php
                $action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
                $action = explode('/', $action);
                $page = $action[count($action)-1];
                if(strpos($action[count($action)-1],'?') !== FALSE){
                    $page = $action[count($action)-2];
                }
                $full_domain = 'www.'.$studio->domain;
                 if($page == $studio->domain || $page == $full_domain){
                    $page = 'home';
                }
            ?>

            $.screentime({
              fields: [
                    { selector: 'html',
                      name: 'HTML'
                    }
              ],
              reportInterval: <?php echo REPORT_INTERVAL;?>,
              googleAnalytics: false,
              callback: function(data, log) {
                    <?php
                        $action = Yii::app()->createAbsoluteUrl(Yii::app()->request->url);
                        $action = explode('/', $action);
                        $page = $action[count($action)-1];
                        if(strpos($action[count($action)-1],'?') !== FALSE){
                            $page = $action[count($action)-2];
                        }
                        $full_domain = 'www.'.$studio->domain;
                         if($page == $studio->domain || $page == $full_domain){
                            $page = 'home';
                        }
                    ?>
                    var timeInMs = Date.now();
                    var page = btoa(timeInMs+"<?php echo $page;?>"+timeInMs);
                    var timeDiff = <?php echo REPORT_INTERVAL;?>/60;
                    var urlForReport = "<?php echo Yii::app()->baseUrl; ?>/site/logVisitor?pid="+page;
                    $.post(urlForReport, {timeDiff:timeDiff}, function (res) {});
              }
            });
            
                        
            var interval;
              var buff_log_id = 0;
              var u_id = 0;
              var resolution = 144;
              
                
            function checkInterNetSpeed(multipleVideoResolution,player){
            return false;
                if(ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2) ){
                    var r = player.buffered();
                    var buffLen = r.length;
                    if(buffLen > 0){
                        buffLen = buffLen - 1;
                    }else{
                        buffLen = 0;
                    }
                    var bufferenEnddd = r.end(buffLen);
                    var bufferStart = r.start(buffLen); 
                    var changeUrlAccInternetSpeed = "<?php echo Yii::app()->baseUrl; ?>/user/getNewSignedUrlForInternetSpeedEmb";
                    console.log('start');
                    if (typeof player.getCurrentRes === "function") {
                        var currentVideoResolution = player.getCurrentRes();
                    } else {
                        var currentVideoResolution = 144;
                    }
                    var videoResolutionSizePerRes = videoResolutionSize[currentVideoResolution];
                    var playedTime = (bufferenEnddd - lastBufferSize);
                    if(lastBufferSize === 0){
                       var playedTime = (bufferenEnddd - bufferStart); 
                    }
                    if(playedTime !== 0){
                        var userIntSpeed = ((videoResolutionSizePerRes/videoDuration)*playedTime) + extraBufferSize;
                        var speedMbps = ((userIntSpeed/1024) / (30)).toFixed(2);
                        lastBufferSize = bufferenEnddd;
                        extraBufferSize = 0;
                        console.log(speedMbps + '---' + videoResolutionSizePerRes + '---' + playedTime + '---' + userIntSpeed + '---' + videoDuration);
                        $.post(changeUrlAccInternetSpeed, {multiple_video_url: multipleVideoResolution,user_internet_speed: speedMbps}, function (res) {
                            if(currentVideoResolution !== res){
                                if(ended === 0 && videoOnPause === 0 && (adavail === 0 || adavail === 2) ){
                                    console.log('new');
                                    player.changeRes(res);
                                }
                            }
                            console.log(res);
                        });
                    }
                }
            }
            function showSettingDiv(){
                if($('#settingDiv:visible').length == 0){
                    if($('#video-res-sec:visible').length == 0){
                        $("#settingDiv").show();
                    }else{
                        $("#video-res-sec").hide();
                    }
                } else{
                    $("#settingDiv").hide();
                }
            }
            function showResDiv(){
                //alert("init-1")
                $("#settingDiv").hide();
                if($('#video-res-sec:visible').length == 0){
                    $("#video-res-sec").show();
                } else{
                    $("#video-res-sec").hide();
                }
            }
            function videoChangeRes(videoRes){
                var showMultiReset = '';
                <?php
                    $setResMenu12  = '';
                    foreach ($multipleVideo as $key => $val) {
                        $addRes = '';
                        if($key != 'BEST'){
                            $addRes ='p';
                        }
                        ?>
                            showMultiReset += "<li class=\"<?php echo $key; ?>\" onclick=\"videoChangeRes('<?php echo $key; ?>');\" ontouchend=\"videoChangeRes('<?php echo $key; ?>');\"><span class=\"textlftAlg\"><?php echo $key.$addRes; ?></span></li>";
                        <?php
                        $setResMenu12 .= "<li class=\"".$key."\" onclick=\"videoChangeRes('".$key."');\" ontouchend=\"videoChangeRes('".$key."');\"><span class=\"textlftAlg\">".$key.$addRes."</span></li>";
                    }
                ?>
                showMultiReset += "<li class=\"AUTO\" onclick=\"videoChangeRes('AUTO');\" ontouchend=\"videoChangeRes('AUTO');\"><span class=\"textlftAlg\">AUTO</span></li>";
                $("#video-res-sec").html('<ul id="custVidRes">' + showMultiReset + '</ul>');
                $("." + videoRes).append('<span class="textRgtAlg"><b>&#10004;</b></span>');
                $("." + videoRes).attr('selected','selected');
                var videoPlayer = videojs('video_block');
                if(videoRes === "AUTO"){
                    setInterSpeed = setInterval(function(){
                     checkInterNetSpeed(multipleVideoResolution,videoPlayer)
                    },120000);
                    return false;
                } else{
                    bitAdsFlag=false;
                    clearInterval(setInterSpeed);
                    videoPlayer.changeRes(videoRes);
                }
                $("#settingDiv").hide();
                $("#video-res-sec").hide();
            }
        
        function FromDurationPlayed() {  
            percen = <?php echo $play_percent;?> ;
            play_length = <?php echo $play_length;?> ; 
            if(myplay_length!="")
                player.currentTime(myplay_length);
            player.play();
            play_status = 0;
            $('.modal').hide();
            player.on('loadedmetadata',function(){
                player.currentTime("<?php echo $play_length;?>");
                //focefully-show-subtitles
                if($('.vjs-text-track-display').find('.vjs-text-track') || $('.vjs-text-track-display').find('.vjs-text-track'))
                {
                    $('.vjs-subtitles').removeAttr("style");
                    $('.vjs-text-track').removeAttr("style");
                    $('.vjs-tt-cue').removeAttr("style");
                    $('.vjs-text-track').attr("style","display:block");
                    }
            })
        }
        
        function FromBeginning() {
            bitFromBeginning = true;
            bitAdsFlag = true; 
            percen = 5 ;
            play_length = 0 ;
            player.pause();
            player.play();
            play_status = 0;
            $('.modal').hide();     
            //check for spotx ads
            <?php if($play_percent != 0 && $play_length != 0){
                    if(@$mvstream->enable_ad == 1 && 
                    (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && 
                    (@$MonetizationMenuSettings[0][menu] & 4) && 
                    (@$MonetizationMenuSettings[1]['channel_id'] != null) && 
                    ((@$mvstream->rolltype == 1) || (@$mvstream->rolltype == 3) 
                    || (@$mvstream->rolltype == 7) ||(@$mvstream->rolltype == 6) ||(@$mvstream->rolltype == 5))){
                ?>
            if(rolltype==2 || rolltype==4 || rolltype==6){
                player.play();
            }else{
                ad_bit_parameter = 'pre';
                requestSpotxads.ads_start(ad_bit_parameter,player);
                    cntipre=1;
        }
            <?php }}?>
        }
        // functionality : resolution gear show/hide
        function getResolutionShowHide(bitCLick,player){
            if(bitCLick){
                if(!($('#settingDiv').is(':visible'))){
                    $('#settingDiv').hide();
                }else{
                    $('#settingDiv').show();
                    player.play();
                }
            }else{
                if(!($('#settingDiv').is(':hidden'))){
                    $('#settingDiv').show();
                    player.play();
                }else{
                    $('#settingDiv').hide();
                }
            }
        }
        
        function getCacheBusterRandom(length) { 
                return Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
        }
        </script>
        <style>
            .ytp-svg-shadow {
                stroke: #000;
                stroke-opacity: .15;
                stroke-width: 2px;
                fill: none;
            }
            .ytp-svg-fill {
                fill: #ccc;
            }
            .customized-res{
                display:none; 
                z-index: 2000;
                right:10%;
                padding:0px 5px 0px 5px;
                cursor:pointer; 
                position:absolute;
                visibility: visible;
                opacity: 0.1%;
                line-height: 23px;
                -webkit-transition: visibility .1s,opacity .1s;
                -moz-transition: visibility .1s,opacity .1s;
                -o-transition: visibility .1s,opacity .1s;
                transition: visibility .1s,opacity .1s;
                background-color: #07141e;
                background-color: rgba(7,20,30,.7);
                border-radius: 3px;
            }
            #video-res-sec ul{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            #video-res-sec ul li{
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            .textlftAlg{
                text-align:left;
            }
            .textRgtAlg{
                text-align:right; float:right;
            }
            </style>
            <script>
            requestSpotxads =  { 
					ads_start: function (ad_bit_parameter,player) {
											try{
													//master
													adSlot = document.createElement("div");
													adSlot.setAttribute("id", "ad-slot");
													
													//slave
													videoelement = document.createElement("video");
													videoelement.setAttribute("id", "spotxadplayingcontainerID");
													adSlot.appendChild(videoelement)		
													videoPlayerspotx.appendChild(adSlot);
													directAdOS = new SpotX.DirectAdOS({
																		channel_id: "<?php echo $MonetizationMenuSettings[1]['channel_id'];?>",
																		slot: adSlot,
																		video_slot: videoelement,
																		hide_skin: false,
																		autoplay: false
																		});
													customVideoPlayer = new CustomVideoPlayer(jQuery, videoPlayerspotx, directAdOS, false);
													customVideoPlayer.play();	
													$(adSlot).show();
													//play
													directAdOS.loadAd().then( function() {
                                                        $('.vjs-big-play-button').attr('style','z-index:-1 !important');
														console.log("Ad-Playing");
														return directAdOS.playAd();
													});
                                                    //stop
													directAdOS.subscribe(function() { 
                                                        cnti=0;
														console.log("Ad-AdStopped");
                                                        $(adSlot).remove();
                                                        $("#player-controls").remove();
                                                        $('.video_paused_overlay').remove();
                                                        $(adSlot).find('iframe').remove();
														$("#player-controls").remove();
                                                        if(ad_bit_parameter=='post'){
                                                            e.preventDefault();
                                                            player.pause();
                                                        }
                                                        else{
                                                        player.play();
                                                        }
                                                        player.volume = 0.5;
                                                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, player.currentTime(), browserName)
													}, "AdStopped");
                                                    /*
                                                    //ad-midpoint
													directAdOS.subscribe(function() { 
                                                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, player.currentTime(), browserName)
                                                    }, "AdVideoMidpoint");
                                                    //ad-completed
                                                    directAdOS.subscribe(function() { 
                                                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, player.currentTime(), browserName)
                                                    }, "AdVideoComplete");
                                                    */
													//error
													directAdOS.subscribe(function() { 
														console.log("Ad-AdError");
                                                        cnti=0;
													}, "AdError");
													console.log("s-start");
											}catch(error){
												console.log("Error:" + error)
											}
					}
			};
            </script>
            <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/resolutionButton.js"></script>
            <?php $this->renderPartial('//layouts/restrict_streaming_device', array('stream_id' => $stream_id, 'getDeviceRestriction' => $getDeviceRestriction)); ?>
            <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id,"enableWatchDuration"=>$enableWatchDuration)); ?>
            <?php $this->renderPartial('//layouts/buffer_log', array('stream_id' => $stream_id, 'movie_id' => $movie_id)); ?>
    </body>
</html>