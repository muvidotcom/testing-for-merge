<!doctype html>

<html>
    <base href="http://demos.flowplayer.org/basics">
    <head>
        <meta charset="utf-8">

        <title>Muvi:: Live stream</title>

        <!-- optimize mobile versions -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- The "minimalist" skin - choose from: "minimalist.css", "functional.css", "playful.css" -->
        <link rel="stylesheet" href="//releases.flowplayer.org/6.0.1/skin/minimalist.css">

        <!-- Minimal styling for this standalone page, can be removed -->
        <link rel="stylesheet" href="/media/css/demo.css">
        <!-- Syntax highlighting of source code, can be removed -->
        <link rel="stylesheet" href="/media/css/pygments.css">
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

			<!-- Optional theme -->
			<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

			<!-- Latest compiled and minified JavaScript -->
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="//releases.flowplayer.org/6.0.1/flowplayer.min.js"></script>
		
		<!-- Chang URLs to wherever Video.js files will be hosted -->
		<link href="http://www.muvi.com/vast/css/video.js.css" rel="stylesheet" type="text/css">
		<!-- video.js must be in the <head> for older IEs to work. -->
		<script src="http://www.muvi.com/js/video.js"></script>


        <script>
            flowplayer(function (api, root) {
                // announce missing stream
                api.on("error", function (e, api, err) {
                    if (err.code === 4 || err.code === 9) {
                        $(".fp-message p", root).text("We are sorry, currently no live stream available.");
                    }
                });
            });
			
			function liveStream(){
				if($('#hls_url').val()){
					$('#livestream_div').empty();
					$('#livestream_div').html('<div data-live="true"	data-ratio="0.5625" data-title="Live stream" class="flowplayer play-button fixed-controls"><video><source type="application/x-mpegurl"src="'+$.trim($('#hls_url').val())+'" data-title="Live stream"></video></div>');
					$(".flowplayer").flowplayer();
				}else{
					return false;
				}
			}

			videojs.options.flash.swf = "http://vjs.zencdn.net/4.2/video-js.swf";
			
        </script>
    </head>

    <body>
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-sm-12">
				  <div class="list-group">
					<div class="list-group-item active">
					  <h4 class="list-group-item-heading">Muvi Live Streaming</h4>
					</div>
					<div class="list-group-item" >
						<form class="form-inline" role="form">
							<div class="form-group col-sm-10" >
								<label for="hls_url">HLS URL:</label>
								<input type="text" class="form-control" style="width: 91%" id="hls_url" placeholder="http://wms.shared.streamshow.it/telesanremo/telesanremo/playlist.m3u8" required pattern="https?://.+">
							</div>
							<button type="button" class="btn btn-default btn-blue" onclick="liveStream();">Test It!</button>
						</form>
					</div>
					<div class="list-group-item" id="livestream_div">
						<div data-live="true"	data-ratio="0.5625" data-title="Live stream" class="flowplayer play-button fixed-controls" data-rtmp="rtmp://itv08.digizuite.dk/tv2b">
						   <video>
							   <source type="application/x-mpegurl" src="http://wms.shared.streamshow.it/telesanremo/telesanremo/playlist.m3u8" data-title="Live stream">
						   </video>
						</div>
<!--					  <div data-live="true"	data-ratio="0.5625" data-title="Live stream" class="flowplayer play-button fixed-controls" data-rtmp="rtmp://itv08.digizuite.dk/tv2b" app="live">
						   <video>
							   <source type="application/x-mpegurl" src="http://itv08.digizuite.dk:1935/tv2b/ngrp:ch1_all/playlist.m3u8" data-title="Live stream">
							   <source type="application/x-mpegurl" src="rtmp://itv08.digizuite.dk/tv2b" data-title="Live stream">
						   </video>
					   </div>-->
					</div>
					<div class="list-group-item" id="livestream_div">
						<video id="video_player" class="video-js vjs-default-skin" controls preload="auto" width="640" height="264" data-setup='{ "autoplay": true, "techOrder": ["flash"] }'>
							<source src="rtmp://wms.waystv.tv:1935/unrestricted_waystlive/68/VDgnQkq1Oxq0" type='rtmp/mp4'>
						</video>
					</div>
					<div class="list-group-item" id="livestream_div">
						<video class="video-js vjs-default-skin" 
							controls=""
							data-setup="{&quot;example_option&quot;:true}"
							height="264"
							id="example_video_1"
							poster="http://video-js.zencoder.com/oceans-clip.png"
							preload="auto"
							width="640">
					   <source src="rtmp://wms.waystv.tv:1935/unrestricted_waystlive/68/VDgnQkq1Oxq0" type="rtmp/flv"/>
					  
					 </video>
					</div>
				  </div>
				</div>
			</div>
		</div>
    </body>
</html>
  

<!--ffmpeg -loglevel quiet -i rtmp://flash.server.com/vod/_definst_/streamname -c:v libx264 -profile:v baseline -level 3.1 -c:a aac -strict experimental -f mpegts - | ffmpeg -i - -c copy -map 0 -f segment -segment_list ffmpeg.m3u8 -segment_format mpegts -segment_time 10 ffmpeg%03d.ts-->
