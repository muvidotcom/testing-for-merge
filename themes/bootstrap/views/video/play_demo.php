<?php $v = RELEASE;
$randomVar = rand();
$studio_ad = (isset($studio_ads) && count($studio_ads) > 0) ? $studio_ads[0] : array();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Playing Video</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }</script>

        <!--Start bootstrap-->
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />          

        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js"></script>
        <!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/vast-client.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.vast.js"></script>   


        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>        


        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
        <style type="text/css">
            .video-js {height: 50%; padding-top: 47%;}
            .vjs-fullscreen {padding-top: 0px}  
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls {
                display:none !important;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="videocontent" style="overflow:hidden;">
                <video id="video_block" class="video-js moo-css vjs-default-skin" controls preload="auto" poster="" width="auto" height="auto" data-setup='{"techOrder": ["youtube", "html5","flash"],"plugins" : { "resolutionSelector" : { "default_res" : "<?php echo $defaultResolution; ?>" } }}'>
                    <?php
                    foreach ($multipleVideo as $key => $val) {
                        echo '<source src="' . $fullmovie_path . '" data-res="' . $key . '" type="video/mp4" />';
                    }
                    ?>
                </video>
            </div> 
        </div>
        <div id="episode_block" class="episode_block">
            <a href="play_video_backup.php"></a>
        </div>

        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <?php
        if (strtolower(@$movieData['content_type']) == 'tv') {
            $ctype = 'tv-show';
        } else {
            $ctype = strtolower(@$movieData['content_type']);
        }
        ?>
        <script>
            var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = true;

            $(document).ready(function () {
                var movie_id = "<?php echo $_REQUEST['movie'] ?>";
                var full_movie = "<?php echo $fullmovie_path ?>";
                var play_type = "<?php echo $play_type; ?>";
                var can_see = "<?php echo $can_see ?>";
                var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                if (play_type === 'trailer') {
                    var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForTrailer";
                } else {
                    var createSignedUrl = "<?php echo Yii::app()->baseUrl; ?>/video/getNewSignedUrlForPlayer";
                }
                var wiki_data = "<?php echo isset($wiki_data) ? $wiki_data : '' ?>";
                var multipleVideoResolution = new Array();
                multipleVideoResolution = <?php echo json_encode($multipleVideo); ?>;
                var url = "<?php echo Yii::app()->baseUrl; ?>/video/add_log";
                var episode_id = "<?php echo isset($_REQUEST['episode_id']) ? $_REQUEST['episode_id'] : 0 ?>";
                var content_type = "<?php echo isset($content_type) ? $content_type : "" ?>";

                var previousTime = 0;
                var currentTime = 0;
                var seekStart = null;
                var adavail = 0;
                var forChangeRes = 0;
                var bufferenEnddd = 0;
                var bufferenEndd = 0;
                var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";

                var is_restrict = "<?php echo isset($is_restrict) ? $is_restrict : 0 ?>";
                var is_studio_admin = "<?php echo Yii::app()->session['is_studio_admin'] ?>";
                $('#video_block').bind('contextmenu', function () {
                    return false;
                });
                videojs('video_block', {plugins: {resolutionSelector: {
                            force_types: ['video/mp4'],
                            default_res: "<?php echo $defaultResolution; ?>"
                        }}}, function () {
                    var player = this;
                    player.on('error', function () {
                        var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                        if (videoErrorCode === 2) {
                            var currTim = player.currentTime();
                            seekStart = 123;
                            createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                        }
                    });

                    if (play_type !== 'trailer') {
                        if (is_mobile !== 0) {
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $("#video_block").attr('poster', item_poster);
                        }
                    }
                    
                    $(".vjs-tech").mousemove(function () {
                        if (full_screen === true && show_control === false) {
                            $("#video_block .vjs-control-bar").show();
                            show_control = true;
                            var timeout = setTimeout(function () {
                                if (full_screen === true) {

                                }
                            }, 10000);
                            $(".vjs-control-bar").mousemove(function () {
                                event.stopPropagation();
                            }).mouseout(function () {
                                event.stopPropagation();
                            });
                        } else {
                            clearTimeout(timeout);
                        }
                    });
                    
                    player.on("fullscreenchange", resize_player);
                    if (is_mobile === 0) {
                        player.watermark({
                            file: "<?php echo $v_logo; ?>",
                            xrepeat: 0,
                            opacity: 0.75
                        });
                        $(".vjs-watermark").attr("style", "top:75%;right:1%;width:7%;");
                    }
                    
                    $("#vid_more_info").hide();
                    $("#episode_block").html("");
                    $("#episode_block").hide();
                    if (is_mobile !== 0) {
                        player.pause();
                        var videoToBePlayed = multipleVideoResolution[<?php echo $defaultResolution; ?>];
                        $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                            console.log("Create Signed Url");
                            player.src(res).play();
                        });
                    } else {
                        if (full_movie.indexOf('http://youtu') > -1) {
                            player.src({type: "video/youtube", src: "http://youtu.be/32xWXN6Zuio"});
                        } 
                        player.play();
                    }
                    
                    player.on("play", function () {
                        $("#episode_block").hide();
                    });

                    player.on('timeupdate', function () {
                        previousTime = currentTime;
                        currentTime = player.currentTime();
                        if(is_mobile !==0){
                            checkBuffer(player);
                        }
                    });
                    
                    player.on('changeRes', function () {
                        forChangeRes = 123;
                        seekStart = previousTime;
                        var currTim = previousTime;
                        console.log(currTim + '-' + seekStart);
                        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                        forChangeRes = 0;
                    });
                    
                    player.on("progress",function(){
                        if(is_mobile !==0){
                            checkBuffer(player);
                        }
                    });
                    
                    //Implementing Ad Codes
                    <?php
                    if (@$mvstream->enable_ad == 1) {
                        $adn = AdNetworks::model()->findByPk($studio_ad->ad_network_id);
                        if ($studio_ad->ad_network_id == 1) {
                            ?>
                                                if (is_mobile === 1)
                                                    ad_url = "http://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id ?>?VPI=MP4&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + Math.random();
                                                else
                                                    ad_url = "http://search.spotxchange.com/vast/2.00/<?php echo $studio_ad->channel_id ?>?VPAID=0&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + Math.random() + "&player_width=" + $('#video_block').innerWidth() + "&player_height=" + $('#video_block').innerHeight();
                                                player.ads();
                                                adavail = 1;
                            <?php
                        }
                        if (@$studio_ad->ad_network_id == 2) {
                            ?>
                                                ad_url = "http://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=<?php echo $studio_ad->channel_id ?>";
                                                player.ads();
                                                adavail = 1;
                            <?php
                        }
                        if (@$mvstream->rolltype == 1) {
                            ?>
                                                var adCalled = 0;
                                                player.one("loadstart", function () {
                                                    console.log("Load started");

                                                    if (adCalled === 0)
                                                    {
                                                        console.log("Ad Called");
                                                        player.vast({
                                                            skip: 5,
                                                            url: ad_url
                                                        });
                                                        adCalled = 1;
                                                    }
                                                });

                                                player.one('adend', function (e) {
                                                    adavail = 2;
                                                    console.log('ad ended');
                                                    e.stopPropagation();
                                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data,0);
                                                });
                            <?php
                        }
                        if (@$mvstream->rolltype == 2) {
                            $roll_after = 0;
                            $roll_parts = explode(':', $mvstream->roll_after);
                            if ($roll_parts[0] > 0)
                                $roll_after += 3600 * $roll_parts[0];
                            if ($roll_parts[1] > 0)
                                $roll_after += 60 * $roll_parts[1];
                            if ($roll_parts[2] > 0)
                                $roll_after += $roll_parts[2];
                            ?>
                                                var state = {};
                                                var midrollPoint = <?php echo $roll_after ?>;
                                                var ad_played = 0;

                                                player.on('timeupdate', function (event) {
                                                    var currentTime = player.currentTime(), opportunity;
                                                    currentTime = currentTime.toFixed(0);

                                                    console.log(currentTime + ' - ' + midrollPoint);

                                                    if (ad_played === 0 && currentTime === midrollPoint)
                                                    {
                                                        ad_played = 1;
                                                        player.vast({
                                                            skip: 5,
                                                            url: ad_url
                                                        });
                                                        player.ads.endLinearAdMode();
                                                        previousTime = player.currentTime();
                                                    }

                                                });

                                                player.one('adend', function (e) {
                                                    adavail = 2;
                                                    console.log('ad ended');
                                                    e.stopPropagation();
                                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data,0);
                                                });
                            <?php
                        }
                    }
                    ?>                    
                    
                    $("video").on("seeking", function () {
                        console.log('seeking ' + seekStart);
                        var bufferDuration = this.buffered.end(0);
                        var currTim = player.currentTime();
                        console.log("forChangeRes" + forChangeRes);
                        if (forChangeRes === 0) {
                            if (bufferDuration < currTim) {
                                if (seekStart === null) {
                                    createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currTim);
                                    seekStart = previousTime;
                                }
                            }
                        }
                    });

                    $("video").on('seeked', function () {
                        console.log('seeked ' + seekStart);
                        if (adavail === 0)
                            seekStart = null;

                    });

                    //Implementng Video Logs
                    if (is_studio_admin !== "true") {
                        if (play_type !== 'trailer') {
                            var started = 0;
                            var ended = 0;
                            var logged = 0;
                            var log_id = 0;

                            player.on("loadstart", function () {
                                console.log("Video Start Log" + started + ' ' + adavail);
                                if(started === 0 && (adavail === 0 || adavail === 2) ){
                                    console.log("Video Start Log");
                                    $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "start", log_id : log_id}, function (res) {
                                        log_id = res;
                                    });
                                    started = 1;
                                }
                            });
                            player.on("ended", function () {
                                if(ended === 0 && (adavail === 0 || adavail === 2) ){
                                    console.log("Video End Log");
                                    $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "complete", log_id : log_id}, function (res) {
                                        log_id = res;
                                    });
                                    ended = 1;
                                }
                            });

                            player.on('timeupdate', function () {
                                if(logged === 0 && (adavail === 0 || adavail === 2) ){
                                    var curlength = Math.trunc(player.currentTime());
                                    var fulllength = player.duration();
                                    fulllength = Math.trunc(fulllength/2);
                                    console.log(curlength + ' and '+ fulllength);
                                    if(curlength === fulllength && fulllength > 1)
                                    {
                                        console.log("Video Middle Log");
                                        logged = 1;
                                        $.post(url, {movie_id: movie_id, episode_id: stream_id, status: "halfplay", log_id : log_id}, function (res) {
                                            log_id = res;
                                        });
                                    }                            
                                }
                            });
                        }
                    }
                });
            });
            function createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, wiki_data, currentTime){
                $(".vjs-error-display").addClass("hide");
                if (typeof player.getCurrentRes === "function") {
                    var currentVideoResolution = player.getCurrentRes();
                } else {
                    var currentVideoResolution = 144;
                }
                var videoToBePlayed = multipleVideoResolution[currentVideoResolution];
                $.post(createSignedUrl, {video_url: videoToBePlayed, wiki_data: wiki_data}, function (res) {
                    player.currentTime(currentTime);
                    player.src(res).play();
                    $(".vjs-error-display").removeClass("hide");
                    player.on("loadeddata", function () {
                        player.currentTime(currentTime);
                    });
                });
            }
            function resize_player() {
                if (full_screen === false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen === true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            function checkBuffer(player){
                var nAgt = navigator.userAgent;
                var browserName  = navigator.appName;
                var verOffset;
                // In Opera 15+, the true version is after "OPR/" 
                if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
                 browserName = "Opera";
                }
                // In older Opera, the true version is after "Opera" or after "Version"
                else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
                 browserName = "Opera";
                }
                // In MSIE, the true version is after "MSIE" in userAgent
                else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
                 browserName = "Microsoft Internet Explorer";
                }
                // In Chrome, the true version is after "Chrome" 
                else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
                 browserName = "Chrome";
                }
                // In Safari, the true version is after "Safari" or after "Version" 
                else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
                 browserName = "Safari";
                }
                // In Firefox, the true version is after "Firefox" 
                else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                 browserName = "Firefox";
                }
                // If Safari browser
                if (browserName === "Safari" ) {
                    console.log(browserName);
                    var currentTime = player.currentTime();
                    var playerDuration = player.duration();
                    var r = player.buffered();
                    var buffLen = r.length;
                    buffLen = buffLen - 1;
                    var bufferenEnddd = r.end(buffLen);
                    var bufferenEndd = bufferenEnddd - 1;
                    console.log(bufferenEndd + '-' + currentTime);
                    if(bufferenEndd < currentTime){
                        if(playerDuration !== bufferenEnddd){
                            player.pause();
                        }else{
                            player.play();
                        }
                    }else{
                        if(bufferenEnddd !== currentTime){
                            if(player.paused()){
                                player.play();
                            }
                        }
                    }
                }
            }
        </script>
    </body>
</html>
