<div class="container">
    <div class="container">
        <div class="span8" style="min-height: 600px;">
			<div style="float: left;margin-top: 0px;">
				<?php $this->widget('application.extensions.addThis', array(
					'id'=>'addThis',									
					'username'=>'username',
					'defaultButtonCaption'=>'Share',
					'showDefaultButton'=>true,
					'showDefaultButtonCaption'=>true,
					'separator'=>'|',
					'htmlOptions'=>array(),
					'linkOptions'=>array(),
					'showServices'=>array('facebook', 'twitter', 'myspace', 'email', 'print'),
					'showServicesTitle'=>false,
					'config'=>array('ui_language'=>'en'),
					'share'=>array(),
					)
				); ?>
			</div>
            <?php
            while ( have_posts() ) {
                the_post();
                ?>
                    <?php get_template_part( 'content', get_post_format() );?> 
                    <?php //echo do_shortcode("[huge_it_share-buttons id='']"); ?>
                    <?php 					
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                            comments_template();
                    }?>
            <?php
            }
            ?>
        </div>
        <div class="span3 pull-right">
            <!---->
            <div>
                <h3 class="">About Muvi</h3>
                <div class="sidebar-module-inset">
                    <p>Launch your Multi-screen OTT VoD Service using Muvi&rsquo;s VoD Platform which comes to you at Zero CapEx Cost and a go-live timeframe of 
                        few hours! Signup today for our 14-Days FREE TRIAL!</p>
                    <p class="center"><button class="btn btn-white btn-free-trial small" onclick="window.open('http://www.muvi.com/signup','_self');">Start Free Trial</button></p>
                </div>
            </div>            
            <!--Studio Blogs-->
            <div>
                <h3><a href="<?php echo home_url(); ?>/category/general/">Muvi Blogs</a></h3>
                <?php
                $args = array('cat' => 1, 'posts_per_page' => 5);
                query_posts($args);
                while (have_posts()) : the_post();
                ?>
                <p><a href="<?php the_permalink(); ?>"><?php the_title()?></a></p>
                <?php //the_excerpt();?>
                <?php
                endwhile;            
                ?>   
            </div>            
            
            <!--Industry News-->
            <div>
                <h3><a href="<?php echo home_url(); ?>/category/industry-news/">Industry News</a></h3>
                <?php
                $args = array('cat' => 2, 'posts_per_page' => 5);
                query_posts($args);
                while (have_posts()) : the_post();
                ?>
                <p><a href="<?php the_permalink(); ?>"><?php the_title()?></a></p>
                <?php //the_excerpt();?>
                <?php
                endwhile;            
                ?>            
            </div>                        
            
            <!--News-->
            <div>
                <h3><a href="<?php echo home_url(); ?>/category/news/">News</a></h3>
                <?php
                $args = array('cat' => 9, 'posts_per_page' => 5);
                query_posts($args);
                while (have_posts()) : the_post();
                ?>
                <p><a href="<?php the_permalink(); ?>"><?php the_title()?></a></p>
                <?php //the_excerpt();?>
                <?php
                endwhile;            
                ?>     
            </div>
            
            <div>
                <h3>Our Tweets</h3>
                <a class="twitter-timeline"  href="https://twitter.com/muvistudiob2b" data-widget-id="534576619390660608">Tweets by @muvistudiob2b</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                          
            </div>                                            
                       
        </div>
    </div>
</div>
