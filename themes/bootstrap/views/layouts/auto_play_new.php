<style>
    .common-css{
            position: absolute;
            top:50%;				
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            display: none;
        }
        .common-css>.player-buttons{
            color: #fff;
            font-size: 5.9em;
            line-height: 1.67;
            cursor: pointer;
            text-align: center;
            margin-top: -.75em;
            float:left;				
        }
        .common-css>.prev-buttons{
            color: #fff;
            font-size: 5.9em;
            line-height: 1.67;
            cursor: pointer;
            margin-top: -.75em;
            float:left;			
        }
        .common-css>.next-buttons{				
            color: #fff;
            font-size: 5.9em;
            line-height: 1.67;
            cursor: pointer;
            margin-top: -.75em;
            float:left;	
        }
        #playButton,#pauseButton{
            height:40px;
            width:40px;
        }
        #nextButton,#prevButton{
            height:35px;
            width:35px;
        }
        #nextButton,#prevButton,#playButton,#pauseButton{
            margin-left:60px;
            margin-right:60px;	
        }
        .prev-buttons img{		
            opacity: 0.4 !important;
        }
        .prev-buttons img:hover{	
            opacity: 1 !important;
            cursor: pointer !important;
            transition: opacity 1s !important;
        } 
        .next-buttons img {		
            opacity: 0.4 !important;
        }
        .next-buttons img:hover{	
            opacity: 1 !important;
            cursor: pointer !important;
            transition: opacity 1s !important;
        }
        .player-buttons img{		
            opacity: 0.4 !important;
        } 
        .player-buttons img:hover{	
            opacity: 1 !important;
            cursor: pointer !important;
            transition: opacity 1s !important;
        } 
        @media (min-width:980px) and (max-width:1366px) {
            #playButton,#pauseButton{
                height:40px;
                width:40px;
            }
            #nextButton,#prevButton{
                height:35px;
                width:35px;
            }
        }
        @media (min-width:900px) {
            #nextButton,#prevButton,#playButton,#pauseButton{
                margin-left:50px;
                margin-right:50px;	
            }
        }

        @media (min-width:700px) and (max-width:800px) {
            #playButton,#pauseButton{
                height:35px;
                width:35px;
            }
            #nextButton,#prevButton{
                height:33px;
                width:33px;
            }
               #nextButton,#prevButton,#playButton,#pauseButton{
                margin-left:40px;
                margin-right:40px;	
            }
        }
        @media (min-width:600px) and (max-width:667px) {
            #playButton,#pauseButton{
                height:32px;
                width:32px;
            }
            #nextButton,#prevButton{
                height:30px;
                width:30px;
            }
            #nextButton,#prevButton,#playButton,#pauseButton{
                margin-left:34px;
                margin-right:34px;	
            }
        }  
        @media (min-width:500px) and (max-width:568px) {
            #playButton,#pauseButton{
                height:30px;
                width:30px;
            }
            #nextButton,#prevButton{
                height:28px;
                width:28px;
            }
            #nextButton,#prevButton,#playButton,#pauseButton{
                margin-left:26px;
                margin-right:26px;	
            }
        } 
        @media (min-width:400px) and (max-width:499px) {
               #playButton,#pauseButton{
                height:25px;
                width:25px;
            }
            #nextButton,#prevButton{
                height:22px;
                width:22px;
            }
            #playButton,#pauseButton{
                margin-left:38px;
                margin-right:38px;	
            }
            #nextButton{
                margin-left:26px;
                margin-right:0px;	
            }
            #prevButton{
                margin-left:0px;
                margin-right:26px;	
            }
        } 
        @media (min-width:319px) and (max-width:399px) {
            #playButton,#pauseButton{
                height:22px;
                width:22px;
            }
            #nextButton,#prevButton{
                height:19px;
                width:19px;
            }
            #playButton,#pauseButton{
                margin-left:26px;
                margin-right:26px;	
            }
            #nextButton{
                margin-left:20px;
                margin-right:0px;	
            }
            #prevButton{
                margin-left:0px;
                margin-right:20px;	
            }
        } 
</style>

<script>
    var stream_id_next = '<?php echo $stream_id_next?>';
    var embed_id_next = '<?php echo $embed_id_next?>';
    var stream_id_prev = '<?php echo $stream_id_prev?>';
    var embed_id_prev = '<?php echo $embed_id_prev?>';
    var permaLink = '<?php echo $permaLink?>';
    var enable_autoplay = '<?php echo $enable_autoplay?>';
    var permalinkNext ='<?php echo $permalinkNext?>';
    var permalinkPrev ='<?php echo $permalinkPrev?>';
    var content = '<?php echo $content?>';
    var site_base_url = '<?php echo Yii::app()->getbaseUrl(true);?>';
    var current_contentType = '<?php echo $current_contentType?>';
    var contentType_next = '<?php echo $contentType_next?>';
    var contentType_prev =  '<?php echo $contentType_prev?>';
    var permalinkNext_multi= '<?php echo $permalinkNext_multi;?>';
    var permalinkPrev_multi='<?php echo $permalinkPrev_multi;?>';
    var nextButtonLink = '';
    var prevButtonLink = '';
    player.ready(function() {
        $('#video_block').append('<div class="common-css"><div class="prev-buttons"><img src="/images/prevButton.png" id="prevButton"/></div><div class="player-buttons"><img src="/images/pauseButton.png" id="pauseButton" /><img src="/images/playButton.png" id="playButton"/></div><div class="next-buttons"><img src="/images/nextButton.png" id="nextButton"/></div>');								
        $('.player-buttons').bind('click',function(e){
            e.preventDefault();
            if (player.paused()) {
                player.play(); 
                $('#playButton').hide();
                $('#pauseButton').show();
                $('.common-css').show();
            } else {
                player.pause();
                $('#playButton').show();
                $('#pauseButton').hide();
                $('.common-css').show();
            }
        });
        setInterval(function() {
            if(player.paused()){
                $('#playButton').show();
                $('#pauseButton').hide();
            }else{						
                $('#playButton').hide();
                $('#pauseButton').show();
            }
            var user_active =  player.userActive();
            if(user_active === true || player.paused()){
            $('.common-css').show();
            } else {
                $('.common-css').hide();
            }
        }, 100);
        if(stream_id_next || permalinkNext){
            if(contentType_next !="0"){
                nextButtonLink = site_base_url+'/player/'+permalinkNext_multi+'/stream/'+ embed_id_next;
            }else{
                nextButtonLink = site_base_url+'/player/'+permalinkNext;
            }
        }
        if(current_contentType != "0"){
            if(stream_id_prev || permalinkPrev){
                if(contentType_prev !="0"){
                    prevButtonLink = site_base_url+'/player/'+permalinkPrev_multi+'/stream/'+ embed_id_prev;
                }else{
                    prevButtonLink = site_base_url+'/player/'+permalinkPrev;
                }
            }
        }
    });
    
</script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/autoplay_new.js?v=<?php echo RELEASE ?>"></script>

