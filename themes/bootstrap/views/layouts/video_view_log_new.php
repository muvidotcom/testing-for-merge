<?php
    //Implementng Video Logs
    $is_video_log_enabled = 1;
    if(isset(Yii::app()->session['is_studio_admin']) && Yii::app()->session['is_studio_admin'] == 'true'){
        $is_video_log_enabled = 0;
    } else if(!Yii::app()->aws->isIpAllowed()){
        $is_video_log_enabled = 0;
    } else if(isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log != 1)){
        $is_video_log_enabled = 0;
    }
    if($is_video_log_enabled){
?>
        <script>
            var video_view_log_url = "<?php echo Yii::app()->baseUrl; ?>/videoLogs/videoViewlog";
            var started = 0;
            var ended = 0;
            var logged = 0;
            var log_id = 0;
            var videoFullLength = 0;
            var studio_id = "<?php echo Yii::app()->common->getStudioId();?>";
            if(typeof percen == 'undefined') {
                var percen = 0;
            }
            var user_id_of_viewer = "<?php echo isset(Yii::app()->user->id) ? Yii::app()->user->id : 0 ?>";
            var movie_id = "<?php echo $movie_id ?>";
            var stream_id = "<?php echo isset($stream_id) ? $stream_id : 0 ?>";
            var is_live = <?php echo isset($is_live) ? $is_live : 0 ?> ;
            var enableWatchDuration = <?php echo isset($enableWatchDuration) ? $enableWatchDuration : 0 ?>;
            function goBack(){
                window.history.back();
            }
        </script>
        <div class="modal fade" id="messge" tabindex="-1" role="dialog" data-backdrop="static" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel"> <strong style="color:black">Oops! You cannot access this content</strong></h4>
                    </div>
                    <div class="modal-body">
                    <br>
                    <center style="color:black">
                    <?php echo $this->Language['watch_duration_exceed_message']; ?></center>
                    <br>
                    </div>
                    <div class="modal-footer" style ="text-align:center;">
                        <button data-dismiss="modal" class="btn btn-danger" id="confirm_yes" type="button" style ="postion: realtive;" onclick="goBack()">OK</button>
                    </div>
                </div>
            </div>
        </div>
       <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/video-view-log-new.js?v=<?php echo "66666" ?>"></script>
<?php } ?>