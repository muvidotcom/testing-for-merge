<?php if(WP_USE_THEMES==1){?>
</div>
<div class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
<section id="footer-new-section" class="">
	<div class="container">
   <div class="row">
      <div class="col-md-8 footer-left col-sm-12 col-xs-12 foot-menu-left">
            <div class="col-md-3 col-sm-3 col-xs-6 dv-footer-men-480 dv-footer-men-360 dv-footer-men-640">
               <div class="footer-list">
                  <h3 class="">About Us</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footeraboutus',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <h3 class="">Partners</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footerpartner',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <div style="clear:both;"></div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 dv-footer-men-480 dv-footer-men-360 dv-footer-men-640">
               <div class="footer-list">
                  <h3 class="blue">Platform</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footerplatform',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <div style="clear:both;"></div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 dv-footer-men-480 dv-footer-men-360 dv-footer-men-640">
               <div class="footer-list">
                  <h3 class="blue">Usage</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footerusage',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <div style="clear:both;"></div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 dv-footer-men-480 dv-footer-men-360 dv-footer-men-640">
               <div class="footer-list">
                  <h3 class="blue">Knowledgebase</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footerknowledge',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <div style="clear:both;"></div>
                  <h3 class="blue">Support</h3>
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footersupport',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
                  <div style="clear:both;"></div>
               </div>
            </div>
      </div>
      <div class="col-md-4 footer-right foot-menu-right">
         <div class="dv-foot-soc-480">
            <?php
               if (is_active_sidebar('footer-social-link-widget-section')) {
               	dynamic_sidebar('footer-social-link-widget-section');
               }
               ?>
         </div>
      </div>
   </div>
   </div>
</section>
<footer id="footer" class="footer-4">
   <div class="container">
      <div class="row">
         <div class="footer-details">
            <div class="col-sm-6 last-footer-copyright">
               <?php
                  if (is_active_sidebar('footer-copyright-section-widget')) {
                  	dynamic_sidebar('footer-copyright-section-widget');
                  }
                             ?>
            </div>
            <div class="col-sm-6 last-footer-links">
               <ul class="list-inline">
                  <?php
                     $defaults = array(
                     	'theme_location'  => 'footerbuttom',
                     	'menu'            => '',
                     	'container'       => false,
                     	'container_class' => '',
                     	'container_id'    => '',
                     	'menu_class'      => 'menu',
                     	'menu_id'         => '',
                     	'echo'            => true,
                     	'fallback_cb'     => 'wp_page_menu',
                     	'before'          => '',
                     	'after'           => '',
                     	'link_before'     => '',
                     	'link_after'      => '',
                     	'items_wrap'      => '%3$s',
                     	'depth'           => 2,
                     	'walker'          => ''
                     	);
                     wp_nav_menu( $defaults );
                     ?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</footer>
</div>
<?php wp_footer(); ?>
<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" role="dialog"  aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close close2" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="gridModalLabel" class="modal-title">Login</h4>
         </div>
         <div class='modal-body'>
            <br/>
            <section id="header-login-popup">
               <div class="form-wrap">
                  <form role="form" method="post" id="login-form" autocomplete="off" action="/login/">
                     <div id="login-error-div" class="error" style="display: none;"></div>
                     <div class="form-group">
                        <label for="email" class="sr-only">Email</label>
                        <input onfocus="$('#header-login-popup-error-div').hide();" name="LoginForm[email]" id="LoginForm_email" type="email" class="form-control" placeholder="Email Address">
                     </div>
                     <div class="form-group">
                        <label for="key" class="sr-only">Password</label>
                        <input onfocus="$('#header-login-popup-error-div').hide();" name="LoginForm[password]" id="LoginForm_password" type="password" class="form-control" placeholder="Password">
                     </div>
                     <div class="checkbox">
                        <input name="LoginForm[rememberMe]" id="LoginForm_rememberMe" value="1" type="checkbox"> 
                        <span class="label" for="LoginForm_rememberMe">Remember me on this computer</span>
                        <div class="errorMessage" id="LoginForm_rememberMe_em_" style="display:none"></div>
                     </div>
                     <input value="2" name="LoginForm[chklogin]" id="LoginForm_chklogin" type="hidden">
                     <input id="ytLoginForm_rememberMe" value="0" name="LoginForm[rememberMe]" type="hidden">
                     <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block btn-blue" name="yt0" value="LOG IN">
                  </form>
                  <a onclick="forgot_click();" href="javascript:;" class="forget">Forgot your password?</a>
                  <br/>
               </div>
            </section>
            <script type="text/javascript">
               function forgot_click(){
               $("#login-error-div").hide();
               $("#success").hide();
               $(".error").hide();
               	parent.$('#login-modal').modal('hide');
               	parent.$('#LoginModal').modal('hide');
               	parent.$('#ForgotModal').modal('show');
               }
            </script>
            <script type="text/javascript">
               jQuery(function($) {
               jQuery('body').on('click','#btn-login',function(){
               $("#login-error-div").hide();
               jQuery.ajax({'beforeSend':function(){
               $("#btn-login").attr("disabled",true);},
               'complete':function(){$("#btn-login").attr("disabled", false);},
               'success':function(data){  
               var obj = jQuery.parseJSON(data); 
               if(obj.login == "success"){
               parent.location.href = "/admin/dashboard/";
               }
               else if(obj.login == "multisuccess"){
               parent.location.href = "/multistore/Dashboard/";
               } 
               else if(obj.login == "step_1"){
               parent.location.href = "/signup/typeofContent/";
               } 
               else if(obj.login == "step_2"){
               parent.location.href = "/signup/paymentGateway/";
               }                                                        
               else{
               $("#login-error-div").show();
               $("#login-error-div").html("Login failed! Try again.");
			   $("#login-error-div").append("");
               }
               		},
               'type':'POST',
               'url':'/login',
               'cache':false,
               'data':jQuery(this).parents("form").serialize()});return false;
               });
               });
            </script>
         </div>
      </div>
   </div>
</div>
<!-- END # MODAL LOGIN -->
<!-- BEGIN # MODAL FORGOT -->
<div class="modal fade" id="ForgotModal" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close close2" type="button"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Forgot Password</h4>
         </div>
         <div class='modal-body'>
            <br/>
            <section id="header-forgot-password-popup">
               <div class="form-wrap">
                  <div id="success" class="alert alert-success" style="display: none;">Forgot Password instructions are sent to your email.<!--<a href="javascript:void(0);" id="clk" onclick="login_click();">Click here</a> to go to login page.--></div>
                  <form id="my-popup-forgot" method="post" name="forgot" method="post" role="form" onsubmit="javascript: return send_instruction();">
                     <div id="login-error-div" class="error" style="display: none;">Email can not be blank!</div>
                     <div class="form-group">
                        <input class="form-control" name="email" id="forgot_email" type="email" placeholder="Email Address">
                     </div>
                     <input type="submit" id="btn-forgot-pass" class="btn btn-custom btn-lg btn-block btn-blue" name="yt0" value="SEND RESET PASSWORD INSTRUCTION">
                  </form>
                  <a onclick="login_click();"  href="javascript:;" class="forget">Back to login</a>
                  <br/>
               </div>
            </section>
            <script type="text/javascript">
               function login_click(){
               $("#login-error-div").hide();
               $("#success").hide();
               $(".error").hide();
                        	parent.$('#login-modal').modal('show');
                        	parent.$('#LoginModal').modal('hide');
                        	parent.$('#ForgotModal').modal('hide');
                        }
            </script>
            <script type="text/javascript">
               $(document).ready(function () {
               	$('#clk').click(function () {
					$("#success").hide();
					$(".error").hide();
					$('#login-form')[0].reset();
					parent.$('#LoginModal').modal('show');
					parent.$('#ForgotModal').modal('hide');
               	});
               });
               function send_instruction() {
               $('#header-forgot-password-popup #login-error-div').hide();
			   $("#success").hide();
               	var email = $("#forgot_email").val();
               	if (email != "") {
               		$.ajax({
               			url: "https://www.muvi.com/login/forgot",
               			data: {email: email},
               			type: 'POST',
               			dataType: "json",
               			success: function (data) {
               				if (data.status == 'success')
               				{
               					$('#header-forgot-password-popup #login-error-div').html('');
               					$('#header-forgot-password-popup #login-error-div').hide();
               					$('#login_form').hide();
               					$('#success').show();
								$('#my-popup-forgot')[0].reset();
               				}
               				else
               				{
               					$('#header-forgot-password-popup #login-error-div').html(data.message);
               					$('#header-forgot-password-popup #login-error-div').show();
               					return false;
               				}
               			}
               		});
               	} else {
               		$("#header-forgot-password-popup #login-error-div").show();
               	}
               	return false;
               }
            </script>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function(){        
        $('#menu-toggle-butn').click(function(){
                $('body').toggleClass('_nav-on');
        })
        $('#mobile_login').click(function(){
                $('body').removeClass('_nav-on');
        })
        $('ul.main-menu li._has-child a').click(function(){
                if($(window).width() <= 992){
                        $(this).parent('li').siblings('li._has-child').removeClass('_opend').find('ul').slideUp();
                        $(this).parent('li').siblings('li._has-child').find('li._has-child').removeClass('_opend');
                        $(this).parent('li').toggleClass('_opend');
                        $(this).siblings('ul').slideToggle();
                }
        })
});
</script>
<script type="text/javascript">
 $(document).ready(function () {
    $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                    $('.back-to-top').fadeIn();
            } else {
                    $('.back-to-top').fadeOut();
            }
    });
    $('.back-to-top').click(function() {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
});
</script>
<!-- END # MODAL FORGOT -->
<?php } else { ?>
   <div class="footer-above wrapper">
           <div class="container">
               <div class="span6">
                   <div class="pull-left">
                   <ul>
                       <div><li><a href="<?php echo Yii::app()->baseUrl; ?>/about">About Us</a></li></div>
<div>
   <li><a href="<?php echo Yii::app()->baseUrl; ?>/team">Team</a></li>
</div>
</ul>
</div>
<div class="pull-left" style="padding-left: 10%;">
   <ul>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/blogs">Blogs</a></li>
      </div>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/blogs/category/news">News</a></li>
      </div>
   </ul>
</div>
<div class="pull-left" style="padding-left: 10%;">
   <ul>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/faqs">FAQs</a></li>
      </div>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/howitworks">How it works</a></li>
      </div>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/howisitdiff">How is it different</a></li>
      </div>
   </ul>
</div>
<div class="pull-left" style="padding-left: 10%;">
   <ul>
      <div>
         <li><a href="<?php echo Yii::app()->baseUrl; ?>/contact">Contact Us</a></li>
      </div>
   </ul>
</div>
</div>
<div class="span6">
   <ul class="pull-right social">
      <li><a href="https://www.facebook.com/MuviStudioB2B" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/facebook.png" alt="facebook" title="Facebook" /></a></li>
      <li><a href="http://www.twitter.com/muvistudiob2b" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/twitter.png" alt="twitter" title="Twitter" /></a></li>
      <li><a href="https://www.linkedin.com/company/muvi-studio" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/linkedin.png" alt="linkedin" title="LinkedIn" /></a></li>
      <li><a href="http://plus.google.com/u/0/118356511554070797931?prsrc=3" rel="publisher" target="_top"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/google-plus.png" alt="google plus" title="Google Plus" /></a></li>
   </ul>
</div>
</div>
</div>
<footer>
   <div class="container">&copy; Muvi LLC</div>
</footer>
<div class="modal fade hide" id="LoginModal" style="width:330px; height: 360px;">
   <div class="modal-header">
      <div id="myModalLabel" style="position: relative;">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h3>LOG IN</h3>
      </div>
   </div>
   <div class="modal-body">
      <iframe style="border: none;" height="270" allowfullscreen="" src="<?php //echo Yii::app()->getBaseUrl(true);?>/login/"></iframe>
   </div>
</div>
<div class="modal fade hide" id="ForgotModal">
   <div class="modal-header">
      <div id="myModalLabel" style="position: relative;">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h3>FORGOT PASSWORD</h3>
      </div>
   </div>
   <div class="modal-body">
      <div class="row-fluid">
         <div class="col-md-12">
            <div class="form-group">
               <div id="forgot_loading1" class="hide"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif"></div>
            </div>
            <div class="form-group"><label>Email :</label>
               <input name="email" id="forgot_email" type="email" class="form-control" style="width: 95%;">
            </div>
            <div class="form-group">
               <div class="error hide">Email can not be blank!</div>
               <div class="clear" style="height:20px;"></div>
            </div>
            <div class="form-group">
               <button class="btn btn-blue" onclick="send_instruction();">Send reset password instruction</button>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   function send_instruction(){
       var email = $("#forgot_email").val();
       if(email != ""){
           $.ajax({
               url: "<?php echo Yii::app()->getbaseUrl(true); ?>/login/forgot",
               data: {email:email},
               type:'POST',
               dataType: "json",
               beforeSend:function(){
                   //$('#forgot_loading').show();
                   //$('#forgot_content').hide();
               },
               success: function (data) {  
                   if(data.status == 'success')
                   {
                       parent.$('#ForgotModal').modal('hide');
                       bootbox.alert("Forgot Password instructions are sent to your email.", function(){
                           //window.location = data.message;
                       });  
                   }
                   else
                   {
                       $('.error').html(data.message);
                       $('.error').show();
                       return false;
                   }
               }
           });
       }else{
           $(".error").show();
       }
   }
</script><?php } ?>