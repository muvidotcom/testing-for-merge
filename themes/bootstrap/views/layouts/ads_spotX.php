<script type="text/javascript" src="//js.spotx.tv/directsdk/v1/<?php echo $MonetizationMenuSettings[1]['channel_id']; ?>.js?v=<?php echo $v ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/adstesting/sdk/custom_video_player.js"></script>
<script>
    var arrindexes = "<?php echo @$mvstream->roll_after; ?>";
    arrindexes = arrindexes.split(',');
    //pre and post roll enable
    var bit_pre_roll = "<?php echo @$bit_pre_roll; ?>";
    var bit_post_roll = "<?php echo @$bit_post_roll; ?>";
    //count the ads played in pre post mid
    var cnti = 0;
    var cntipre = 0;
    var cntiend = 0;
    var ad_bit_parameter = '';
    var rolltype = "<?php echo $mvstream->rolltype; ?>";
    //Spotx ads global variables
    var bitAdsFlag = true;
    var adSlot = "";
    var videoPlayerspotx = document.getElementById('video-player');
    var videoelement = "";
    var directAdOS = "";
    var requestSpotxads = "";
    var check_ended = 0;
    var channel_id = "<?php echo $MonetizationMenuSettings[1]['channel_id'];?>";
    var ads_volume = 100;
    var nAgt = navigator.userAgent;
    var browserName = getBrowserName(nAgt);
    if(is_mobile || browserName == "Safari"){
        ads_volume = 0;
    }
    //for edge browser width need to be increase
    var edgeWidth = 10;
    if (/Edge\/\d./i.test(navigator.userAgent)){
        edgeWidth = 25;
    }
    //pre roll
    $(document).ready(function () {
        player.ready(function () {
            player.on('loadedmetadata', function () {
                if (bit_pre_roll.match('start')) {
                    console.log("bit_pre_roll ::: " + bit_pre_roll + " bitAdsFlag ::: " + bitAdsFlag + " cntipre ::: " + cntipre);
                    try {
                        if (cntipre > 0) {
                            return false;
                        } else {
<?php
if ($play_percent != 0 && $play_length != 0) {
    if ((@$mvstream->rolltype == 1) || ((@$mvstream->rolltype == 3) || (@$mvstream->rolltype == 7) || (@$mvstream->rolltype == 5))) {
        ?>
                player = this;
                if ($('#play_confirm').is(':visible')) {
                    player = this;
                    player.pause();
                   // $('.modal').show();
                }
        <?php
    }
} else {
    if (((@$mvstream->rolltype == 1) || (@$mvstream->rolltype == 3) || (@$mvstream->rolltype == 7) || (@$mvstream->rolltype == 5))) {
        ?>
                                    $('.vjs-control-bar').attr('style','display:none');
                                    player = this;
                                    ad_bit_parameter = 'pre';
                                   // requestSpotxads.ads_start(ad_bit_parameter, player);
                                    play_ads();
                                    cntsulr = 0;
                                    console.log("://pre-roll");
                                    cntipre++;
                                    //createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, currentTime, browserName); 
        <?php
    }
}
?>
                        }
                    }
                    catch (err) {
                        console.log(" :: SpotX AD_ERROR :: " + err);
                    }
                }
            });
            //post roll
            player.on('ended', function () {
                check_ended = 1;
                if (bit_post_roll.match('end')) {
                    console.log("bit_post_roll ::: " + bit_post_roll);
                    try {
                        if (cntiend > 0) {
                            player.pause();
                            return false;
                        } else {
<?php
if ((@$mvstream->rolltype == 4) || (@$mvstream->rolltype == 7) || (@$mvstream->rolltype == 6) || (@$mvstream->rolltype == 5)) {
    ?>
                                $('.vjs-control-bar').attr('style','display:none');
                                ad_bit_parameter = 'post';
                                //requestSpotxads.ads_start(ad_bit_parameter, player);
                                play_ads();
                                console.log("://post-roll");
                                cntiend++;
                                player.pause();
                                //createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, null, currentTime, browserName); 
<?php } ?>
                        }
                    }
                    catch (err) {
                        console.log(" :: SpotX AD_ERROR :: " + err);
                    }
                }
            });
            //mid roll

            player.on('timeupdate', function () {
                try {
                    if (arrindexes.length > 0) {
                        var check_ad_time = parseInt(Math.round(player.currentTime()));
                        check_ad_time = check_ad_time.toString();
                        try {
                            //only pre || post ads activated
                            if (rolltype == 1 || rolltype == 5 || rolltype == 4) {
                                return false;
                            } else {
                                if ($.inArray(check_ad_time, arrindexes) !== -1) {
                                    var index_val = $.inArray(check_ad_time, arrindexes);
                                    console.log("mid-roll ::: " + arrindexes[index_val] + " other :: " + cntiend + "----------" + cntipre + "----------" + bitAdsFlag)
                                    if ($('#ad-slot').length > 0 && cntiend == 0 && cntipre > 0) {
                                        $("div[id^='ad-']").remove();
                                    } else {
                                        if (cnti === 0) {
                                            ad_bit_parameter = 'mid';
                                            $('.vjs-control-bar').attr('style','display:none');
                                            play_ads();
                                            //requestSpotxads.ads_start(ad_bit_parameter, player);
                                            cntsulr = 0;
                                            cnti++;
                                            var arrindexes_val = arrindexes.indexOf(check_ad_time);
                                            delete arrindexes[arrindexes_val];
                                            console.log("arrindexes_FINAL ::: " + arrindexes);
                                        }
                                    }
                                }
                            }
                        } catch (err) {
                            console.log(" :: SpotX AD_ERROR :: " + err);
                        }
                    }
                } catch (err) {
                    console.log(" :: SpotX AD_ERROR :: " + err);
                }
            });
        });
    });
    function FromBeginning() {
        player.pause();
        $('.modal').hide();     
        $('.vjs-big-play-button').hide();
        //check for spotx ads
        //requestSpotxads.ads_start(ad_bit_parameter,player);
        <?php if($play_length != 0){
                if(@$mvstream->enable_ad == 1 && 
                (@$MonetizationMenuSettings[1]['ad_network_id'] == 1) && 
                (@$MonetizationMenuSettings[0][menu] & 4) && 
                (@$MonetizationMenuSettings[1]['channel_id'] != null) && 
                ((@$mvstream->rolltype == 1) || (@$mvstream->rolltype == 3) 
                || (@$mvstream->rolltype == 7) ||(@$mvstream->rolltype == 6) ||(@$mvstream->rolltype == 5))){
            ?>
        if(rolltype==2 || rolltype==4 || rolltype==6){
            player.play();
        }else{
            ad_bit_parameter = 'pre';
            $('.vjs-control-bar').attr('style','display:none');
            //requestSpotxads.ads_start(ad_bit_parameter,player);
             play_ads();
                cntipre=1;
        }
        <?php }}?>
    }
//setup for playing ads
    var userAgent = navigator.userAgent;
    function play_ads(){
        adSlot = document.createElement("div");
        adSlot.setAttribute("id", "ad-slot");
        videoelement = document.createElement("video");
        videoelement.setAttribute("id", "spotxadplayingcontainerID");
        adSlot.appendChild(videoelement)
        videoPlayerspotx.appendChild(adSlot);
        player.pause();
        var height = $("#video-player").height();
        var width = $("#video-player").width()+edgeWidth;
        $(".wrapper").append('<script type="text/javascript" src="//search.spotxchange.com/js/spotx.js"\n\
        data-spotx_channel_id="'+channel_id+'"\n\
        data-spotx_ad_unit="instream"\n\\n\
        data-spotx_ad_done_function="myAdDoneFunction" \n\\n\
        data-spotx_content_width="'+width+'" \n\
        data-spotx_content_height="'+height+'"\n\
        data-spotx_content_type="video" \n\
        data-spotx_vid_duration="6000"\n\
        data-spotx_ad_volume="'+ads_volume+'"\n\
        data-spotx_autoplay="1"\n\
        data-spotx_vid_id="test-video-id1"\n\
        data-spotx_vid_url="http://www.spotxchange.com"\n\
        data-spotx_vid_title="test" \n\
        data-spotx_content_container_id="ad-slot"\n\
        data-spotx_https="0"><\/script>');
    }
    function myAdDoneFunction(){
        cnti = 0;
        $(adSlot).remove();
        $(".vjs-control-bar").removeAttr("style");
        $('.video_paused_overlay').remove();
        $(adSlot).find('iframe').remove();
        $(".vjs-control-bar").removeAttr("style");
        if(!check_ended)
            player.play();
    }
</script>


