<div id="contactModal" class="modal fade login-popu" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="position: relative;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
		<div class="table-responsive row-fluid">                                   
                    <div class="contactform">
                            <h2 class="btm-bdr">Contact Us</h2>
                            <p class="error" id="errors"></p>
                            <form method="post" name="contact-form" id="contact-form" class="form-inline">   
                                <div id="loading" class="loading"></div>
                                <div class="row-fluid">
                                    <div class="input-prepend border martop10">
                                        <span class="add-on"><i class="icon-name"></i></span>
                                        <input type="text" id="fullname" name="fullname" placeholder="Name" />
                                    </div>                
                                    <div class="input-prepend border martop10">
                                        <span class="add-on"><i class="icon-email"></i></span>
                                        <input type="text" id="email" name="email" placeholder="Email" />
                                    </div>            
                                </div>
                                <div class="row-fluid">
                                    <div class="input-prepend border martop10">
                                        <span class="add-on"><i class="icon-company"></i></span>
                                        <input type="text" id="company" name="company" placeholder="Company" />
                                    </div>                
                                    <div class="input-prepend border martop10">
                                        <span class="add-on"><i class="icon-phone"></i></span>
                                        <input type="text" id="phone" name="phone" placeholder="Phone" />
                                    </div>            
                                </div>
                                <div class="row-fluid">
                                    <div class="input-prepend border tarea martop10">
                                        <span class="add-on"><i class="icon-message"></i></span>
                                        <textarea id="message" name="message" placeholder="Message" rows="12"></textarea>            
                                    </div>
                                </div>       
                                <div class="row-fluid center martop10">
									<input type="text" id="ccheck" name="ccheck" placeholder="" autocomplete="off">
									<input type="hidden" name="submit-btn" id="submit-btn"/>
                                    <input type="button" id="send" value="Send" class="btn btn-blue"  />
                                </div>
                            </form>                           

                    </div>

                </div>
            </div>		
        </div>
    </div>
</div>
<script type="text/javascript">
    
    function validEmail(email)
    {
        var res = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(email);
        return res;
    }
    
    function validPhone(phone)
    {
        var res = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(phone);
        return res;
    }
    jQuery(document).ready(function(){
        jQuery('#loading').hide();    
        jQuery('#send').click(function(){
            if(jQuery('#fullname').val() == '')
            {
                jQuery('#errors').html('Please enter Name');
                jQuery('#fullname').focus();
                return false;
            }
            else if(jQuery('#email').val() == '')
            {
                jQuery('#errors').html('Please enter Email');
                jQuery('#email').focus();
                return false;
            } 
            else if(!validEmail(jQuery('#email').val()))
            {
                jQuery('#errors').html('Please enter a valid Email');
                jQuery('#email').focus();
                return false;
            }   
            else if(jQuery('#phone').val() == '')
            {
                jQuery('#errors').html('Please enter Phone Number');
                jQuery('#phone').focus();
                return false;
            } 
            else if(!validPhone(jQuery('#phone').val()))
            {
                jQuery('#errors').html('Please enter a valid Phone Number');
                jQuery('#phone').focus();
                return false;
            }  
            else if(jQuery('#message').val() == '')
            {
                jQuery('#errors').html('Please enter Message');
                jQuery('#message').focus();
                return false;
            }             
            else
            {
                jQuery.ajax({
                    url: '<?php echo Yii::app()->baseUrl?>/contact/send',
                    data: jQuery('#contact-form').serialize(),
                    type:'POST',
                    dataType: "json",
                    beforeSend:function(){
                        jQuery('#loading').show();
						ga('send', 'event', { eventCategory: 'contact', eventAction: 'submit', eventLabel: 'contactform'});
                    },

                    success: function(data){
                        jQuery('#loading').hide();
                        if(data.status == 'success')
                        {
                            alert(data.message);
                            location.reload();
                        }
                        else
                        {
                            jQuery('#errors').html(data.message);
                            return false;
                        }
                    }                  
                });  
            }  
        });  
    });
</script>