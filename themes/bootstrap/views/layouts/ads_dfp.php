<script>
var check =1;
function init() {
        player.play();	
        $('.modal').hide();
        $(".vjs-big-play-button").hide();
        if(check ==1)
             requestAds();
    }
    function createAdDisplayContainer() {
//         We assume the adContainer is the DOM id of the element that will house the ads.
        adDisplayContainer = new google.ima.AdDisplayContainer(document.getElementById('adContainer'), videoContent);
//        console.log("adDisplayContainer ::: " + adDisplayContainer)
    }

    function requestAds() {
        check = 0;
        google.ima.settings.setPlayerType('google/codepen-demo-countdown-timer');
        google.ima.settings.setPlayerVersion('1.0.0');
        //Create the ad display container.
        createAdDisplayContainer();
        //Initialize the container. Must be done via a user action on mobile devices.
        adDisplayContainer.initialize();
//        videoContent.load();

        //         Create ads loader.
        adsLoader = new google.ima.AdsLoader(adDisplayContainer);
        //        DFP - I
        //        for (var prop in adsLoader){console.log(`adsLoader.${prop} = ${adsLoader[prop]}`);}

        // Listen and respond to ads loaded and error events.
        adsLoader.addEventListener(google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED, onAdsManagerLoaded, false);
        adsLoader.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, onAdError, false);

        //         Request video ads.
        var adsRequest = new google.ima.AdsRequest();
        adsRequest.adTagUrl = "<?php echo $MonetizationMenuSettings[1]['channel_id']; ?>";

        // Specify the linear and nonlinear slot sizes. This helps the SDK to
        adsLoader.requestAds(adsRequest);
    }

    function onAdsManagerLoaded(adsManagerLoadedEvent) {
//             Get the ads manager.
        var adsRenderingSettings = new google.ima.AdsRenderingSettings();
        adsRenderingSettings.restoreCustomPlaybackStateOnAdBreakComplete = true;
//             videoContent should be set to the content video element.
        adsManager = adsManagerLoadedEvent.getAdsManager(videoContent, adsRenderingSettings);

//             Add listeners to the required events.
        adsManager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, onAdError);
        adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, onContentPauseRequested);
        adsManager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, onContentResumeRequested);
        adsManager.addEventListener(google.ima.AdEvent.Type.ALL_ADS_COMPLETED, onAdEvent);

//             Listen to any additional events, if necessary.
        adsManager.addEventListener(google.ima.AdEvent.Type.LOADED, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.STARTED, onAdEvent);
        adsManager.addEventListener(google.ima.AdEvent.Type.COMPLETE, onAdEvent);

        try {
//                   Initialize the ads manager. Ad rules playlist will start at this time.
            adsManager.init(parseInt($(window).width() - 10), parseInt($(window).height() - 10), google.ima.ViewMode.NORMAL);
//                  adsManager.init($(window).width(),$(window).height(), google.ima.ViewMode.NORMAL);
//                   Call play to start showing the ad. Single video and overlay ads will
//                   start at this time; the call will be ignored for ad rules.
            adsManager.start();
        } catch (adError) {
//                   An error may be thrown if there was a problem with the VAST response.
            player.play();
            videoContent.play();
        }
    }
    function onAdError(adErrorEvent) {
//             Handle the error logging.
        console.log(adErrorEvent.getError());
        adsManager.destroy();
    }
    function onAdEvent(adEvent) {
//            for (var prop in adEvent) { console.log(`adEvent.${prop} = ${adEvent[prop]}`);}
//             Retrieve the ad from the event. Some events (e.g. ALL_ADS_COMPLETED)
//             don't have ad object associated.
        var ad = adEvent.getAd();
//            console.log("AD QUEUE :: "+ adsManager.getCuePoints());
//            console.log("adEvent.type" + adEvent.type);
        switch (adEvent.type) {
            case google.ima.AdEvent.Type.LOADED:
//                 This is the first event sent for an ad - it is possible to
//                 determine whether the ad is a video ad or an overlay.
                if (!ad.isLinear()) {
//                   Position AdDisplayContainer correctly for overlay.
//                   Use ad.width and ad.height.
                    videoContent.play();
                }
                break;
            case google.ima.AdEvent.Type.STARTED:
//                 This event indicates the ad has started - the video player
//                 can adjust the UI, for example display a pause button and
//                 remaining time.
                if (ad.isLinear()) {
//                   For a linear ad, a timer can be started to poll for
//                   the remaining time.
                    intervalTimer = setInterval(
                            function () {
                                var remainingTime = adsManager.getRemainingTime();
//                        countdownUi.innerHTML = 'Remaining Time: ' + parseInt(remainingTime);
                            },
                            300); // every 300ms

//                      DFP - AdsManager Properties :: functional information 
//                      console.log(adsManager);
//                      console.log(adsManager.g.A);
//                      for (var prop in adsManager) { console.log(`adsManager.${prop} = ${adsManager[prop]}`);}
//                      console.log(adsManager.K);
//                      console.log(adsManager.Oa);
                }
                break;
            case google.ima.AdEvent.Type.COMPLETE:
//                 This event indicates the ad has finished - the video player
//                 can perform appropriate UI actions, such as removing the timer for
//                 remaining time detection.
                if (ad.isLinear()) {
                    clearInterval(intervalTimer);
                }
                break;
        }
    }
    function onContentPauseRequested() {
        $('#container').attr('style', 'display:none');
        $('.modal').hide();
        player.pause();
        dfdAdPlaying = 1;
        videoContent.pause();
        if ($('.videocontent').find('iframe')) {
            $("iframe").parent().attr('style', 'display:block');
        }
    }
    function onContentResumeRequested() {
        $('#container').attr('style', 'display:block;cursor:default;');
        $(".vjs-control-bar").show();
        if ($('.videocontent').find('iframe')) {
            $("iframe").parent().attr('style', 'display:none');
        }
        dfdAdPlaying = 0;
        player.play();
        videoContent.play();
        currentTime = player.currentTime();
        createSignUrlForpage(createSignedUrl, player, multipleVideoResolution, currentTime,browserName);
    }
</script>
<link rel="stylesheet" href="//googleads.github.io/videojs-ima/src/videojs.ima.css" />
<script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
<script src="//googleads.github.io/videojs-ima/src/videojs.ima.js"></script>