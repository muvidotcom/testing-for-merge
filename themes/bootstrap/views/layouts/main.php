<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">         
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/img/favicon.png" type="image/png" rel="icon">
        <link rel="alternate" type="application/rss+xml" title="RSS feed" href="<?php echo Yii::app()->getBaseUrl(true);?>/rss.xml" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php if (Yii::app()->controller->id == 'blogs') {
          $keyword = '';  
        } else {
            $keyword = CHtml::encode($this->pageKeywords);  
        } ?>
        <meta name="keywords" content="<?php echo $keyword; ?>" />
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
	<meta name="p:domain_verify" content="7373eaab41578b6bc018b4f5f8f03fcd"/>
	<meta property="og:site_name" content="Muvi"/>
	<meta property="og:url" content="<?php echo Yii::app()->createAbsoluteUrl(Yii::app()->request->url);?>"/>
	<meta property="og:title" content="<?php echo CHtml::encode($this->pageTitle); ?>" />
        <meta property="og:description" content=" <?php echo CHtml::encode($this->pageDescription); ?>"/>
	<?php if(strtolower(Yii::app()->controller->id)!='blogs'){?>
	<meta property="og:image" content="<?php echo Yii::app()->getBaseUrl(true)."/themes/".Yii::app()->theme->name;?>/images/muvi_studio_social_logo.jpg" />
	<meta property="og:image:type" content="image/jpg" />
	<meta property="og:image:width" content="230" /> 
	<meta property="og:image:height" content=" 230" >
	<?php } ?>
	
	<meta name="msvalidate.01" content="12F48210ACFB9126A8D69AA7D8089200" />
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,400italic,500italic,500,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <?php 
        Yii::app()->bootstrap->register();
        $my_controller= Yii::app()->getController()->getId();
        $my_action= Yii::app()->getController()->getaction()->getId();
        if($my_controller=='login' && $my_action=='resetpassword' && isset($_REQUEST['user']) && $_REQUEST['user']!='' && WP_USE_THEMES==1){?>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/muvi_own_style.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/muvi_own_responsive.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css?v=<?php echo RELEASE?>" />
            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
            <script type="text/javascript">
            $(document).ready(function(){        
                    $('#menu-toggle-butn').click(function(){
                            $('body').toggleClass('_nav-on');
                    })
                    $('#mobile_login').click(function(){
                            $('body').removeClass('_nav-on');
                    })
                    $('ul.main-menu li._has-child a').click(function(){
                            if($(window).width() <= 992){
                                    $(this).parent('li').siblings('li._has-child').removeClass('_opend').find('ul').slideUp();
                                    $(this).parent('li').siblings('li._has-child').find('li._has-child').removeClass('_opend');
                                    $(this).parent('li').toggleClass('_opend');
                                    $(this).siblings('ul').slideToggle();
                            }
                    })
                    $(window).scroll(function(){
                       if($(window).scrollTop() > 100){
                               $('header').addClass('_scrolled');
                       }
                       else{
                               $('header').removeClass('_scrolled');
                       }
                    })
            });
        </script>
        <?php } else { ?>
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css?v=<?php echo RELEASE?>" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/fa/css/font-awesome.css?v=<?php echo RELEASE?>" />
        <?php } ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css?v=<?php echo RELEASE?>" />  
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/fa/css/font-awesome.css?v=<?php echo RELEASE?>" />
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/bootbox.min.js"></script>  
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.cycle.all.js"></script>  

        <script type="text/javascript">
        jQuery.validator.addMethod("mail", function(value, element) {
            return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
        }, "Please enter a correct email address");    
        </script>
    
    <?php if(strpos($_SERVER['HTTP_HOST'], 'muvi.in')){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script>
    <?php  } else if(strpos($_SERVER['HTTP_HOST'], 'muvi.com')){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script> 
    <?php  } else if($_SERVER['HTTP_HOST'] != 'localhost'){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script>
    <?php }  ?>	

<?php if(Yii::app()->controller->id=='site' && (Yii::app()->controller->action->id=='index')){?>
		<link rel="canonical" href="<?php echo $this->createAbsoluteurl('/'); ?>" />
<?php }elseif(isset($this->canonicalurl) && $this->canonicalurl ){?>
        <link rel="canonical" href="<?php echo Yii::app()->getbaseUrl(true).Yii::app()->request->url?>" />
<?php }else{?>
	  <link rel="canonical" href="<?php echo Yii::app()->getbaseUrl(true).Yii::app()->request->url?>" />
<?php } ?>

    
</head>

    <?php
    $is_home = 0;
    $class = '';
    if ((Yii::app()->controller->getId().'/'.Yii::app()->controller->getAction()->getId()) == 'site/index'  )
    {
        $is_home = 1;
        $class = Yii::app()->controller->getId();
    }
    else
    {
        $class = Yii::app()->controller->getId();
    }    
    ?>
    <body class="<?php echo $class?>">

    <?php require_once 'header.php';?>
    

    <?php echo $content; ?>

    
    <?php require_once 'footer.php'; ?>
    <?php if($is_home){?>
        <a href="https://plus.google.com/118356511554070797931" rel="publisher"></a>
    <?php }?>
        
        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute("charset", "utf-8");
                $.src = "//v2.zopim.com/?3Ck44EbYwaLXpRDAitMdvTyD4LH7YeoO";
                z.t = +new Date;
                $.
                        type = "text/javascript";
                e.parentNode.insertBefore($, e)
            })(document, "script");
        </script>
        <!--End of Zopim Live Chat Script-->         
        <script type="text/javascript">

    var intervalVariable;
intervalVariable = setInterval(zopim30sec, 10000);
$zopim(function () { 
    
    $zopim.livechat.setOnConnected(function () {
       
        $zopim.livechat.departments.clearVisitorDepartment();
        var zemail = $zopim.livechat.getEmail();
        var zname = $zopim.livechat.getName();
		//alert(CHECKSTUDIOLOGIN);
	<?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
		<?php } else { ?>
        
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
		<?php } ?>
        
        //zopim redirection to sales and support department as per the login details
        if (zemail != '') {
            if (getCookie("zopimemail") != zemail) {
                $.post("/contact/SendFromZopim", {"email": zemail, "name": zname}, function () {
                    document.cookie = "zopimemail=" + zemail;
                });
            }
        } else {
            //intervalVariable = setInterval(zopim30sec, 20000);
        }
    });
    
     
   });  
$zopim(function() {
$zopim.livechat.setOnChatEnd(end);
});
function end() {
$zopim.livechat.clearAll();
}
function zopim30sec() {
    
    $zopim.livechat.departments.clearVisitorDepartment();
    
    var zemail1 = $zopim.livechat.getEmail();
    var zname1 = $zopim.livechat.getName();
    //zopim redirection to sales and support department as per the login details
     //alert(CHECKSTUDIOLOGIN);
    <?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
        <?php } else { ?>
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
       <?php } ?>
	
        
    //zopim redirection to sales and support department as per the login details
    if (zemail1 != '') {
        if (getCookie("zopimemail") != zemail1) {
            $.post("/contact/SendFromZopim", {"email": zemail1, "name": zname1}, function () {
                document.cookie = "zopimemail=" + zemail1;
                //clearInterval(intervalVariable);
            });
        }
    }
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
</script>
<script type="text/javascript" id="pap_x2s6df8d" src="https://muviedocent.postaffiliatepro.com/scripts/gwzje3f"></script>
<script type="text/javascript">
PostAffTracker.setAccountId('default1');
try {
PostAffTracker.track();
} catch (err) { }
</script>

</body>
</html>
