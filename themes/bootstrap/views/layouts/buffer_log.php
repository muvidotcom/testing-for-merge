<?php
    //Implementng buffer Logs
    $is_buffer_log_enabled = 1;
    if(!Yii::app()->aws->isIpAllowed()){
        $is_buffer_log_enabled = 0;
    } else if(isset(Yii::app()->user->add_video_log) && (Yii::app()->user->add_video_log != 1)){
        $is_buffer_log_enabled = 0;
    }
    if($is_buffer_log_enabled){
?>
<script>
    var studio_id = "<?php echo Yii::app()->common->getStudioId();?>";
    var movie_id = "<?php echo $movie_id?>";
    var stream_id = "<?php echo $stream_id?>";
    var URL = "<?php echo Yii::app()->baseUrl?>";
</script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/buffer-log.js"></script>
<?php  }?>