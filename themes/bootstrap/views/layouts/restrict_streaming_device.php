
<?php if(isset(Yii::app()->user->id) && Yii::app()->user->id != 1038){ ?>
    <script>
        $(document).ready(function () {
            setInterval(function() {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->baseUrl; ?>/user/canlogin',
                    success: function(res) {
                        if(res != 1)
                            window.location.href ="<?php echo Yii::app()->getbaseUrl(); ?>/user/logout" ;
                    }
                });             
            }, 30000);
            setInterval(function(){
                $.ajax({
                    url: '<?php echo Yii::app()->getbaseUrl(); ?>/site/updateLoginHistory/',
                    type: "POST",
                    data: {login_count:'login_count'},          
                    success: function (data) {
                        console.log(data);
                    }
                }); 
            }, 300000);
        });
    </script>
<?php }  
    if($getDeviceRestriction != '' && $getDeviceRestriction > 0){ ?>
    <script>
        var stream_id = "<?php echo $stream_id; ?>";
        var restrictDeviceUrl = '<?php echo Yii::app()->baseUrl; ?>/videoLogs/addDataToRestrictDevice';
        var deleteRestrictDeviceUrl = '<?php echo Yii::app()->baseUrl; ?>/videoLogs/deleteDataFromRestrictDevice';
    </script>
    <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/js/video-streaming-restriction.js?v=<?php echo RELEASE ?>"></script>
<?php } ?>