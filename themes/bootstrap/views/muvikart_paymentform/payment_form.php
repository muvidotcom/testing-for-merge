<?php
$months =['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
$yearrange['start'] = date('Y');
$yearrange['end']   = date('Y')+20;
$free_offer = $settings['free_offer'];
if(!$settings['free_offer']){
   $free_offer = 0;
}
$is_physical=$this->IS_PCI_COMPLIANCE[$gateway_code];
?>
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <?php                        
                    if($is_physical == 1){
                ?>    
                    <div class="row top20" id="payformdiv">
                            <div>
                                <div class="col-md-8">
                                    <div class="text-center" id="card-error"></div>
                            </div>
                        </div>
                    </div>
                <form action="javascript:void(0);" id="payform" name="payform" method="POST">
                        <input type="hidden" id="pg_amount" name="pay[amount]" value="<?php echo $item_total;?>">
                        <input type="hidden" id="pg_currency_id" name="pay[currency_id]" value="<?php echo $currency_id;?>">
                        <input type="hidden" id="pg_shipping_cost" name="pay[shipping_cost]" value="<?php echo $shipping_cost;?>">
                        <input type="hidden" name="pay[amount]" value="<?php echo $item_total;?>" id="grandtotalamount">
                        <input type="hidden" id="amt" value="<?php echo $item_total;?>">
                        <input type="hidden" name="pay[discount]" id="discount" value="">
                        <input type="hidden" name="pay[currency_id]" value="<?php echo $currency_id;?>" id="currency_id">
                        <input type="hidden" name="pay[shipping_cost]" value="<?php echo $shipping_cost;?>" id="shipping_cost">
                        <input type="hidden" name="final_discount_amount" value="" id="final_discount_amount">
                        <input type="hidden" name="pay[discount_amount_specific]" value="" id="discount_amount_specific">
                        <input type="hidden" name="pay[physical_specific_ids]" value="" id="physical_specific_ids">
                    </form>

                    <div ><a href="javascript:void(0);" id="pci_pay" onclick="checkcard(<?php echo $free_offer;?>,1)" class="btn btn-primary c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r"><?php echo $this->Language['confirm_order'];?></a></div>
                    <!--form id ="paypal" action="javascript:void(0)" method="POST"></form-->
                    <?php
                    if($gateway_code == 'paypalpro'){
                    ?>                 
                        <form  style="display:none" target="hss_iframe" name="form_iframe" action="<?php echo $this->PAYPAL;?>" method="post">
                            <input type="hidden" name="cmd" value="_hosted-payment">
                            <input type="hidden" name="subtotal" id="pci_amount" value="0">
                            <input type="hidden" name="currency_code" id="pci_currency_code" value="">
                            <input type="hidden" name="business" value="<?php echo $this->PAYMENT_GATEWAY_NON_3D_SECURE['paypalpro'];?>">
                            <input type="hidden" name="paymentaction" value="sale">
                            <input type="hidden" name="invoice" id="invoice" value="">
                            <input type="hidden" name="billing_first_name" id="billing_first_name" value="">
                            <input type="hidden" name="billing_last_name" id="billing_last_name" value="">
                            <input type="hidden" name="buyer_email" id="buyer_email" value="">
                            <input type="hidden" name="first_name" id="first_name" value="">
                            <input type="hidden" name="last_name" id="last_name" value="">
                            <input type="hidden" name="return" id="return" value="">
                            <input type="hidden" name="cancel_return" id="cancel_return" value="">
                            <input type="hidden" name="template" value="templateD">
                        </form>
                        <iframe name="hss_iframe" style="display:none" width="100%" id="iframeContainer" height ="495px" frameborder="0"></iframe>
                    <?php } else { ?>
                        <button id="pci_click_event" style="display:none"></button>
                    <?php
                        } 
                    }else{
                    ?>
                <form action="javascript:void(0);" id="payform" name="payform"  method="POST">
                    <h3 class="c-font-bold c-font-uppercase c-font-24"><!--Choose method of Payment--><?php echo $this->Language['paymentdetails'];?></h3>
                    <hr>
                    <label class="radio radio-inline" style="display: none;">
                        <input type="radio" checked="checked" name="pay[payment_type]" value="0">
                        <i class="input-helper"></i><?php echo $this->Language['cash_on_delivery'];?>
                    </label>
                    <label class="radio radio-inline" style="display: none;">
                        <input type="radio" name="pay[payment_type]" value="1">
                        <i class="input-helper"></i><?php echo $this->Language['choose_payment'];?>
                    </label>
                    <div class="clearfix"></div>
                    <div class="row top20" id="payformdiv" style="display: none;">
                        <div>
                            <div class="col-md-8">
                                <div class="row onzirohide">
                                    <div class="col-md-6" style="margin-top: 20px;"><label style="font-size: 24px;"><?php echo $this->Language['credit_card_detail'];?></label></div>
                                    <div class="col-md-6 text-right" style="margin-top: 20px;">
                                        <img src="<?php Yii::app()->getBaseUrl(true)?>/images/visad.png" style="width: 50px;">
                                        <img src="<?php Yii::app()->getBaseUrl(true)?>/images/mastercard.png" style="width: 50px;">
                                        <img src="<?php Yii::app()->getBaseUrl(true)?>/images/maestro.png" style="width: 50px;">
                                    </div>
                                </div>
                                <hr class="onzirohide">
                                <div class="text-center" id="card-error"></div>
                                <div class="row onzirohide">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">
                                            <?php echo $this->Language['card_will_charge']?> <span style="color: #f55753;" class="grandtotalmsg"><?php echo Yii::app()->common->formatPrice($total, $item['currency_id']);?></span>
                                            <input type="hidden" name="pay[amount]" value="<?php echo $item_total;?>" id="grandtotalamount">
                                            <input type="hidden" id="amt" value="<?php echo $item_total;?>">
                                            <input type="hidden" name="pay[discount]" id="discount" value="">
                                            <input type="hidden" name="pay[currency_id]" value="<?php echo $currency_id;?>" id="currency_id">
                                            <input type="hidden" name="pay[shipping_cost]" value="" id="shipping_cost">
											<input type="hidden" name="final_discount_amount" value="" id="final_discount_amount">
                                        </label>
                                    </div>                                    
                                    <div class="form-group col-md-6">
                                        <?php 
                                        if(isset($cards) && !empty($cards) && intval($can_save_card)){

                                        ?>
                                            <select class="form-control c-square c-theme" name="card_options" id="card_options" onchange="showcarddiv('cardinfo', this);">
                                                <option value=""><?php echo $this->Language['use_new_card'];?></option>
                                                <?php
                                                foreach($cards as $value){
                                                ?>
                                                <option value="<?php echo $value['id'];?>" selected="selected"><?php echo $value['card_last_fourdigit'];?> <?php echo $value['card_type'];?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } else { ?>
                                            <input type="hidden" id="card_options" value="">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div id="cardinfo" style="display: none;">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label"><?php echo $this->Language['text_card_name'];?></label>
                                            <input type="text" id="card_name" name="pay[card_name]" class="form-control c-square c-theme" placeholder="<?php echo $this->Language['text_card_name']; ?>">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label"><?php echo $this->Language['text_card_number'];?></label>
                                            <input type="text" id="card_number" name="pay[card_number]" class="form-control c-square c-theme" placeholder="<?php echo $this->Language['text_card_number']; ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label class="control-label"><?php echo $this->Language['selct_exp_date'];?></label>
                                            <select id="exp_month" name="pay[exp_month]" id="exp_month" class="form-control c-square c-theme">
                                                <option value=""><?php echo $this->Language['select_month'];?></option>
                                                <?php
                                                for($i=1;$i <=12;$i++){
                                                ?>
                                                <option value="<?php echo $i;?>"><?php echo $months[$i - 1];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label">&nbsp;</label>
                                            <select id="exp_year" name="pay[exp_year]" id="exp_year" class="form-control c-square c-theme pull-right" onchange="getMonthList();">
                                                <option value=""><?php echo $this->Language['select_year'];?></option>
                                                <?php
                                                for($i=$yearrange['start'];$i <= $yearrange['end'];$i++){
                                                ?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label"><?php echo $this->Language['text_security_code'];?></label>
                                            <input id="security_code" type="password" name="pay[security]" class="form-control c-square c-theme" placeholder="<?php echo $this->Language['text_security_code']; ?>">
                                        </div>
                                        <?php if($can_save_card){ ?>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">&nbsp;</label>
                                            <input type="text" name="pay[nameoncard]" class="form-control c-square c-theme" placeholder="<?php echo $this->Language['card_name_optional']; ?>">
                                        </div>
                                        <?php }?>
                                    </div>
                                    <?php if($can_save_card){ ?>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <div class="c-checkbox">
                                                <input type="checkbox" value="1" class="c-check" name="savecard" id="checkbox2-22" checked="checked">
                                                <label for="checkbox2-22">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> <?php echo $this->Language['save_card_checkout'];?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>                                        
                            </div>
                        </div>
                    </div>
                    <div class="pull-right"><a href="javascript:void(0);" onclick="checkcard(<?php echo $free_offer;?>,0)" class="btn btn-primary c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r"><?php echo $this->Language['confirm_order'];?></a></div>
                </form>
                <?php } ?>        
                    <input type="hidden" name="grand_total_final_price" id="grand_total_final_price" value="0">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) . '/common/js/' . $this->PAYMENT_GATEWAY[$gateway_code];?>.js"></script>

<script type="text/javascript">
    var browserreloadflag = 0;
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    function removecart(id, cartid, qnty) {
        $('.loader_cart').show();
        var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/RemoveCart';
        $.post(url, {'id': id,'cartpopup':1}, function (res) {
            if(typeof qnty !== typeof undefined){
                $('.round-cart').html(eval($('.round-cart').html()) - eval(qnty));
                $('#cartpopup').html(res);
            }
            $('.loader_cart').hide();
            window.location.reload();
        })
    }
    function updatecart(id) {
        var qnt = $('#qnt_' + id).val();
        if(qnt>0){
            $('.loader_cart').show();
            var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/updateCart';
            $.post(url, {'id': id, 'update': 1, 'quantity': qnt,'cartpopup':1}, function (res) {
                if($('.cart-page-quant').length > 0){
                    var total_quan = 0;
                    $(".cart-page-quant").each(function(){
                        total_quan = parseInt(total_quan) + parseInt($(this).val());
                    });
                    $('.round-cart').html(eval(total_quan));
                    $('#cartpopup').html(res);
                }
                $('.loader_cart').hide();
                window.location.reload();
            })
        }else{
            alert(JSLANGUAGE.enter_quantity);
        }
    }
    function showpgdiv(divid) {
        $('#' + divid).show('slow');
        if (divid == 'payment_details') {
            if ($('#card_options').val() == '') {
                if(parseInt($('#grand_total_final_price').val()) > 0){
                    $('.onzirohide').show();
                $('#cardinfo').show('slow');
                }else{
                    $('.onzirohide').hide();
                    $('#cardinfo').hide('slow');
				}
			}
        }
        $('html, body').animate({'scrollTop': $('#' + divid).position().top});
    }
    function showcarddiv(carddiv, obj) {
        if ($(obj).val() == '') {
            $('#' + carddiv).show('slow');
        } else {
            $('#' + carddiv).hide('slow');
        }
    }
    function validateshiping(flag) {
        jQuery.validator.addMethod("phone_no", function (value, element) {
        return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/i.test(value);
	}, JSLANGUAGE.valid_phone_number);
        jQuery.validator.addMethod("zip", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 ]*$/i.test(value);
	}, JSLANGUAGE.valid_zip);
	
        jQuery.validator.addMethod("first_name", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9.-]*$/i.test(value);
	}, JSLANGUAGE.special_charecter_not_allowed);
        var validate = $("#shipingform").validate({
            rules: {
                'ship[first_name]': {
                    required: true
                },
                'ship[address]': {
                    required: true
                },
                'ship[zip]': {
                    required: true,
                    zip: true
                },
                'ship[phone_number]': {
                    required: true,                    
                    phone_no: true,
                    minlength: 10,
                    maxlength: 15
                }
            },
            messages: {
                'ship[first_name]': {
                    required: JSLANGUAGE.field_required,
                },
                'ship[address]': {
                    required: JSLANGUAGE.field_required,
                },
                'ship[city]': {
                    required: JSLANGUAGE.field_required,
                },
                'ship[state]': {
                    required: JSLANGUAGE.field_required,
                },
                'ship[zip]': {
                    required: JSLANGUAGE.field_required,
                },
                'ship[country]': {
                    required: JSLANGUAGE.field_required,
                },
                "ship[phone_number]": {
                    required: JSLANGUAGE.field_required,
                    minlength: JSLANGUAGE.min_length,
                    maxlength: JSLANGUAGE.max_length
                },
            },
        });
        var x = validate.form();
        if (x) {
            $('.loader_cart').show();
            $.ajax({
                url: "<?php echo Yii::app()->getbaseUrl(true);?>/shop/saveaddrsession/flag/" + flag,
                data: $('#shipingform').serialize(),
                type: 'POST',
                success: function (data) {
                    if (data != '1') {
                        $('#parentaddress').html(data);
                    }
                    addressclick();
                    var count_method ='<?php echo count($methods);?>';
                    var single_method ='<?php echo $single_method;?>';
                    if(count_method <= 1){
                        var divid = 'shipping_details';
                        showpgdiv(divid);
                        if(count_method==1){
                            $("#method").val(single_method);
                        }else{
                            $("#method").val('defaultprice');
                        }
                        applyshippingmethod();
                    }else{                        
                        var divid = 'shipping_details';
                        showpgdiv(divid);                        
                    }
                },
                complete: function () {
                    $('.loader_cart').hide();
                }
            });
        } else {
            $('.loader_cart').hide();
        }
    }
    function checkcard(fo,pp) {
        if(parseInt(pp)){
			/*
            if($('#checkbox2-33').is(":checked")){
	      $('#age_restriction-error').html('').hide();
            }else{
	      $('#age_restriction-error').html('Please accept the Terms & Conditions of Sale and Privacy Policy.').show();
	      return false;
            }
			
			if($('#checkbox2-34').is(":checked")){
	      $('#privacy_policy-error').html('').hide();
            }else{
	      $('#privacy_policy-error').html('Please accept the above statement.').show();
	      return false;
            }
			*/
            var x = 1;
        }else{
            var validate = $("#payform").validate({
                rules: {
                    "pay[card_name]": "required",
                    "pay[card_number]": {
                        required: true,
                        number: true
                    },
                    "pay[exp_month]": "required",
                    "pay[exp_year]": "required",
                    "pay[security]": {
                        required: true,
                        minlength:3                    
                    }
                },
                messages: {
                    "pay[card_name]": JSLANGUAGE.card_name_required,
                    "pay[card_number]": {
                        required: JSLANGUAGE.card_number_required,
                        number: JSLANGUAGE.card_number_required
                    },
                    "pay[exp_month]": JSLANGUAGE.expiry_month_required,
                    "pay[exp_year]": JSLANGUAGE.expiry_year_required,
                    "pay[security]": {
                        required: JSLANGUAGE.security_code_required,
                        minlength: JSLANGUAGE.min_3_char_req
                    }
                },
                errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element);
                }
            });
            var x = validate.form();
        }
        if(x){
            $('.loader_cart').show();
            if(fo==1){
                var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/avilFreeOffer';        
                $.post(url, {'checkoff': 1}, function (res) {
                    if(res.isSuccess){
                        openFreeOffer();
                    }else{
                        saveorder(pp);
                    }
                }, 'json');
            
            }else{
                saveorder(pp);
            }
            
        }else{
            return false;
        }        
    }    
    function saveorder(pp){
        var coupon = $.trim($("#coupon").val());
        var final_price = parseInt($('#grand_total_final_price').val());
        var method = $("#method").val();
        $.post('<?php echo Yii::app()->getbaseUrl(true);?>/shop/CheckProduct',{'coupon':coupon,'final_price':final_price,'method':method}, function (res1) {
            if(res1=='error'){
                $('.loader_cart').hide();
                $('#card-error').html(JSLANGUAGE.amount_mismatch);
            }else if(res1!=0 && res1!='error'){
                $('.loader_cart').hide();
                $('#card-error').html("The product(s) '"+res1+"' is removed by admin. Please remove from your cart.");
            }else{
                browserreloadflag = 1;
                $.cookie("showshipingaddr", 0);
                $('.loader_cart').hide();
                if(parseInt(pp)){
                    PaymentAction();
                }else{
                  /*  $("#loadingPopup").modal('show');
                    var url = '<?php echo Yii::app()->getbaseUrl(true);?>/userPayment/saveorder';
                    var data = $('#payform').serialize();
                    $.post(url, {'data': data,'coupon':$('#coupon').val()}, function (res) {
                        browserreloadflag = 0;
                        $("#loadingPopup").modal('hide');   
                        if(res.isSuccess){
                            $("#successPopup").modal('show');
                            setTimeout(function () {
                                window.location.href = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/success';
                                return false;
                            }, 5000);
                        }else{
                            if(typeof res.Message != 'undefined') {
                                $('#card-error').html(res.Message);
                            }else if(typeof res.response_text != 'undefined') {
                                var msg = JSLANGUAGE.transaction_not_processed+' '+JSLANGUAGE.valid_credit_card_number;
                                $('#card-error').html(msg);
                            }
                        }
                    }, 'json');  */
    
                    $('.loader_cart').show();
                    $('#pci_pay').text(JSLANGUAGE.wait);
                    <?php if(isset($gateway_code) && $gateway_code!=''){?>
                    var class_name = '<?php echo $gateway_code;?>' + '()';
                    eval ("var obj = new "+class_name);
                    obj.physicalPayment();
                    <?php }else{?>
                        $('.loader_cart').hide();
                        $('#card-error').html(JSLANGUAGE.checkout_not_allowed);
                    <?php }?>
                }
            }
        });
    }
    function openFreeOffer() {
        var movie_stream_id = 1;
        var url = '<?php echo Yii::app()->getbaseUrl(true);?>/Shop/freeOffer';
        $.post(url, {'movie_stream_id': movie_stream_id}, function (res) {
            $('#free_form_area').html(res);
        });
        $('.loader_cart').hide();
        $("#free_popup").modal('show');
    }
    function saveFreeOffer(pp) {
        if (!$("#freeoptions").val()) {
            $('#msg').show();
            return false;
        }else{
        $("#free_popup").modal('hide');
        $('.loader_cart').show();
            var id=$("#freeoptions").val();
            var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/addtocart';
            $.post(url,{'quantity':1,'id':id,'pricetype':'freeoffer'},function(res){
                $('.loader_cart').show();
                saveorder(pp);    
            })
        }
    }
    function showselectmsg(name,obj,id){
        $("#freeoptions").val(id);
        $("button").removeClass("active");
        var fname= JSLANGUAGE.you_selected + ' '+name;
        $('#fomsg').show();
        $('#fomsg').html(fname);
        $(obj).addClass('active');
        $(".tick-iconpop").hide();
        $("#fo"+id).show();         
        $("#continue").focus();
    }    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;
        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">' + JSLANGUAGE.expiry_month + '</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    $(document).ready(function () {
        /*$('#payform input:radio').click(function(){
         if($(this).val()==1){*/
        $('#payformdiv').show('slow');
        /*}else{
         $('#payformdiv').hide('slow');
         }
         });*/
        var user_id = "<?php echo Yii::app()->user->id;?>";
        if (user_id != '') {
            if ($.cookie('showshipingaddr') == 1) {
                $('#address_details').show('slow');
            }
            addressclick();
        }
        var coupon = $.trim($("#coupon").val());
        if(coupon !== ''){
            validateCoupon(0);
        }
    })
    function addressclick() {
        $('.plan-box').click(function () {
            $('.plan-box').removeClass('choosen');
            $(this).addClass('choosen');
            $('.tick-icon').hide();
            $(this).find('.tick-icon').show();
            $('#plan_id').val($(this).attr('data-id'));
        });
    }

    function post_comment() {
        var is_login = check_login();
        if (is_login) {
            $.cookie("showshipingaddr", 0);
            showpgdiv('address_details');
        } else {
            $.cookie("showshipingaddr", 1);
        }
    }
    function check_login() {
        var user_id = "<?php echo Yii::app()->user->id;?>";
        if (user_id != '') {
            return true;
        } else {
            $('#loginModal h4.modal-title').html(JSLANGUAGE.only_register_checkout);
            $("#login_link").click();
            return false;
        }
    }
    function deliveraddress(addressid,obj) {
        if($(obj).find(".tick-icon").css('display')=='none'){
            $('.loader_cart').show();
            var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/DeliverAddress';
            $.post(url, {'data': addressid}, function (res) {
                $('.loader_cart').hide();
                if(res==2){
                    $('.tick-icon').hide();
                    alert(JSLANGUAGE.shipping_not_allowed_to_this_country);
                }else{
                    var count_method ='<?php echo count($methods);?>';
                    var single_method ='<?php echo $single_method;?>';
                    if(count_method <= 1){
                        var divid = 'shipping_details';
                        showpgdiv(divid);
                        if(count_method==1){
                            $("#method").val(single_method);
                        }else{
                            $("#method").val('defaultprice');
                        }
                        applyshippingmethod();
                    }else{                        
                        var divid = 'shipping_details';
                        showpgdiv(divid);                        
                    }
                }                
            });
        }        
    }
    function editaddress(addressid) {
        $('.loader_cart').show();
        $('#payment_details').hide();
        var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/EditAddress';
        $.post(url, {'data': addressid}, function (res) {
            $('.loader_cart').hide();
            $('#parentaddress').html(res);
            addressclick();
        });
    }
    function deleteaddress(addressid) {
        var x = confirm(JSLANGUAGE.delete_address);
        if (x) {
            $('.loader_cart').show();
            $('#payment_details').hide();
            var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/DeleteAddress';
            $.post(url, {'data': addressid}, function (res) {
                $('.loader_cart').hide();
                $('#parentaddress').html(res);
                addressclick();
            });
        }
    }
    function shownew() {
        $('#savedaddress').hide('slow');
        $('#address_div').show('slow');
    }
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function validateCoupon(cf){
        var couponCode = $.trim($("#coupon").val());        
        var proId = JSON.stringify(productId);
        $('#physical_specific_ids').val(proId);
        if(couponCode !== ''){
            if(cf){$('.loader_cart').show();}
            $("#paynowbtn").prop("disabled", true);
            $("#coupon_btn").prop("disabled", true);
            
            var url = "<?php echo Yii::app()->getbaseUrl(true);?>/user/validateCoupon";
            $.post(url, {'couponCode': couponCode,'currency_id' : $("#currency_id").val(),'physical':1, proId:proId}, function (res) {
                if(cf){$('.loader_cart').hide();}
                $("#paynowbtn").removeAttr("disabled");
                $("#coupon_btn").removeAttr("disabled");
                
                if (parseInt(res.isError)) {
                    $("#valid_coupon_code").hide();
                    if (parseInt(res.isError) === 1) {
                        $("#coupon_use").val(0);
                        //$("#coupon_use_instafeez").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
                    } else if (parseInt(res.isError) === 2) {
                        $("#coupon_use").val(0);
                        //$("#coupon_use_instafeez").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.coupon_already_used);
                    }
                    var currency = $("#charged_amt").attr('data-currency');
                    var p = parseFloat($('#amt').val());
                    $(".grandtotalamount").text(currency+" "+(Math.round(p*100)/100));
                    $('#discount').val(0);
                    //$("#charged_amt").show();
                    //$("#discount_charged_amt").hide();
                    //$("#discount_charged_amt_span").text(0);
                    $('#final_discount_amount').val(0);
                } else {
                    $("#coupon_use").val(1);
                    $("#valid_coupon_code").show();
                    //$("#coupon_use_instafeez").val(1);
                    $("#invalid_coupon_error").html('').hide();
                    $("#valid_coupon_suc").show();
                    
                    var gross_amt = $("#charged_amt").attr('data-amount');
                    var currency = $("#charged_amt").attr('data-currency');
                    var discount_amount = res.discount_amount;
                    
                    if(parseInt(res.is_physicalSepecific)){
                        $("#coupon_code_appliedon").show();
                        $("#coupon_code_appliedon").html(JSLANGUAGE.applied_on+" "+res.productName);
                        if (parseInt(res.discount_type)) {
                            $("#coupon_in_amt").text(res.discount_amount+'%');
                            $('#final_discount_amount').val(res.finalDiscVal);
                            $(".grandtotalamount").text(currency+""+(Math.round(res.finalDiscountedAmount*100)/100));
                            $('#discount').val(res.discount_amount);
                            $('#discount_amount_specific').val(res.finalDiscountedAmount);
                        }else{
                            $("#coupon_in_amt").text(currency+" "+res.discount_amount);
                            $('#final_discount_amount').val(res.discount_amount);
                            $(".grandtotalamount").text(currency+""+(Math.round(res.finalDiscountedAmount*100)/100));
                            $('#discount').val(res.discount_amount);
                            $('#discount_amount_specific').val(res.finalDiscountedAmount);
                        }
                    }else{
                    var price = 0;
                    if (parseInt(res.is_cash)) {
                        $("#coupon_in_amt").text(currency+" "+discount_amount);
                        price = (parseFloat(gross_amt) - parseFloat(discount_amount));
                        $('#final_discount_amount').val(discount_amount);
                    } else {
                        $("#coupon_in_amt").text(discount_amount+'%');
                        price = (parseFloat(gross_amt) - (parseFloat(gross_amt) * parseFloat(discount_amount)/100));
                        var disc = (parseFloat(gross_amt) * parseFloat(discount_amount)/100);
                        $('#final_discount_amount').val(disc);
                    }
                    
                    var isprice = Math.round(price * 100) / 100;
                            
                    if (isprice < 0) {
                        price = 0;
                        discount_amount = gross_amt;
                        if (parseInt(res.is_cash)) {
                            $("#coupon_in_amt").text(currency+""+(parseFloat(discount_amount).toFixed(2)));
                        } else {
                            $("#coupon_in_amt").text(discount_amount+'%');
                        }
                    }
                    //var shipping = parseFloat($('#shipping_cost').val());
                    //price = shipping+price;
                    $(".grandtotalamount").text(currency+""+(Math.round(price*100)/100));
                    $('#discount').val(discount_amount);
                    //$("#charged_amt").hide();
                    //$("#discount_charged_amt").show();
                    //$("#discount_charged_amt_span").text(currency+""+price.toFixed(2));
                }
                }
            }, 'json');
        } else {
            var currency = $("#charged_amt").attr('data-currency');
            var p =parseFloat($('#amt').val());
            $(".grandtotalamount").text(currency+" "+(Math.round(p*100)/100));
            $('#discount').val(0);
            $("#coupon_use").val(0);
            //$("#coupon_use_instafeez").val(0);
            $("#valid_coupon_suc").hide();
            //$("#charged_amt").show();
            //$("#discount_charged_amt").hide();
            //$("#discount_charged_amt_span").text(0);
            $("#valid_coupon_code").hide();
            $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
            $('#final_discount_amount').val(0);
        }
        if($("#payment_details").is(':visible')){
                applyshippingmethod();
        }
    }
    var is_physical ='<?php echo $is_physical;?>';
    if (!is_physical) {
        window.addEventListener("beforeunload", function (e) {
            if (browserreloadflag) {

                var confirmationMessage = JSLANGUAGE.no_action_while_payment;
                //console.log(confirmationMessage);
                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                return confirmationMessage;                            //Webkit, Safari, Chrome
            }
        });
    }
    
    function PaymentAction() {
        $('.loader_cart').show();
        $('#pci_pay').text('Wait...');
        var class_name = '<?php echo $gateway_code;?>' + '()';
        eval ("var obj = new "+class_name);
        obj.pciPayment();
    }
    function applyshippingmethod(flag) {
        var validate = $("#shipingcost").validate({
            rules: {
                'method': {
                    required: true
                }
            },
            messages: {
                'method': {
                    required: JSLANGUAGE.field_required,
                }
            },
        });
        var x = validate.form();
        if (x) {
            $('.loader_cart').show();
            var method = $("#method").val();
            var currency_id = "<?php echo $currency_id;?>";
            var url = '<?php echo Yii::app()->getbaseUrl(true);?>/shop/calculateShippingCost';
            $.post(url, {'method': method,'currency_id': currency_id}, function (res) {
                if (res.error == 1) {
                    //swal('Error in removing movie');
                } else {
                    $('.loader_cart').hide();
                    $('#shipdiv').show();
                    var currency = res.ship_symbol; 
                    var shippingnew = res.shipcost;
                    var price = parseFloat($('#amt').val());
                    var discount_amt = parseFloat($('#final_discount_amount').val());
                    if(isNaN(discount_amt) ){
                        discount_amt=0;
                    }                       
                    var pricenew = price - discount_amt; 
                    if (pricenew < 0) {
                        pricenew = 0;
                    }
                    if(res.minimum_order_free_shipping) {
                        if(pricenew >= res.minimum_order_free_shipping){
                            shippingnew = '0.00';
                        }
                    }
                    pricenew = parseFloat(shippingnew) + parseFloat(pricenew);
                    $("#shipping_amt").text(currency+""+shippingnew);                    
                    $("#grand_total_final").text(currency+""+(pricenew.toFixed(2)));
                    $(".grandtotalmsg").text(currency+""+(pricenew.toFixed(2)));
                    $('#shipping_cost').val(shippingnew);
                    $("#pg_shipping_cost").val(shippingnew);
                    var divid = 'payment_details';
                    $('#grand_total_final_price').val(pricenew);
                    showpgdiv(divid);
                }
            }, 'json');
        } else {
            $('.loader_cart').hide();
        }
    }
</script>