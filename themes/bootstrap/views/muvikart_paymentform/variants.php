<?php 
foreach ($fieldval as $key12 => $value12) {
	foreach ($value12['variables'] as $key => $value) {
		$variable_attribute[$value['f_display_name']][$value['value']] = $value['value'];
	}
}
foreach ($fieldval as $key123 => $value123) {
	foreach ($value123['variables'] as $key1 => $value1) {
		$variable_attribute_new[$value1['f_display_name']] = $value1['value'];
	}
	$vn = implode(',',$variable_attribute_new);
	if(is_array($value123['price'])){
		$map_price[$vn]['price'] = $value123['price'][0]['symbol'].$value123['price'][0]['price'];
	}else{
		$map_price[$vn]['price'] = $value123['default_symbol'].$value123['price'];
	}
	$map_price[$vn]['sku'] = $value123['sku'];
        $map_price[$vn]['varient_id'] = $value123['id'];
}
if($selectedvariant){
	foreach ($variable_attribute as $k => $v) {
		foreach ($selectedvariant as $k1 => $v1) {
			if($k == $k1){
				if(!in_array($v1, array_keys($v))){
					array_push($variable_attribute[$k], $v1);
				}
			}
		}
	}
}
?>
<?php foreach ($map_price as $k => $v) {?>
	<input type="hidden" class="hiddentxt" data-key="<?php echo $k;?>" data-price="<?php echo $v['price'];?>" data-sku="<?php echo $v['sku'];?>" data-variantid="<?php echo $v['varient_id'];?>">
<?php }    
?>
        
<div class="c-product-variant c-margin-t-20 variants_div">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<?php foreach ($variable_attribute as $k => $v) {?>
				<div class="c-font-uppercase c-font-bold "><?php echo $k;?> : </div>
				<div class="form-group col-md-6 row">
					<select name="variants[<?php echo $k;?>]" class="form-control c-square c-theme variants" onchange="shownew_price_sku();">
					<?php
						foreach($v AS $opkey=>$opvalue){
							if($selectedvariant){
								if(in_array($opvalue, $selectedvariant)){
									$seleceted = 'selected = selected';
								}else{
									$seleceted = '';
								}
							}else{
								$seleceted = '';
							}
							echo "<option value='".$opvalue."' ".$seleceted.">" . $opvalue . "</option>";
						}
						?>
					</select>
				</div>
				<div class="clearfix"></div>
			<?php }?>
			<div id="variable_error" style="color:red;font-size:14px;display: none"></div>
		</div>
	</div>
</div>