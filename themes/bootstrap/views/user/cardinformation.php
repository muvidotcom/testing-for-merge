<style type="text/css">
    .error{font-weight: normal;}
</style>
<?php 
    if(isset($gateways) && !empty($gateways)){ ?>
                
<div class="container">
    <h2 class=""><?php echo $this->Language['card_info']; ?></h2>
    <div class="row-fluid">
        <div class="col-md-12 form-bg paddtopbtm20">
            <?php if (isset($usersub->status) && ($usersub->status == 1) && isset($usersub->payment_status) && ($usersub->payment_status != 0)) {//Subscriber 
                ?>
                <div style="color: #F00;">
                    <?php echo $last_charge_failed; ?> <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypal' && $this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypalpro') { ?><a href="javascript:void(0)" id="pamentpayhere" onclick="payFromPrimaryCard();" style="font-weight: bold"><?php echo $this->Language['btn_paynow']; ?></a><?php } ?>
                </div>
            <?php } ?>
            
            <?php
            if (isset($usersub) && !empty($usersub) && isset($cards) && !empty($cards)) {
                $total_cards = count($cards);
                ?>
                <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypalpro') { ?>
                <div style="padding-left: 5px;">
                    <h3><?php echo $this->Language['credit_cards']; ?></h3>
                </div>
                <?php }?>
                <div class="clearfix" style="height: 30px;"></div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->Language['text_card_number']; ?></th>
                            <th><?php echo $this->Language['type']; ?></th>
                            <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypal' && $this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypalpro') { ?>
                            <th><?php echo $this->Language['action']; ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                        <tbody>
                        <?php foreach ($cards as $key => $value) { ?>
                            <tr>
                                <td><?php echo $value->card_last_fourdigit; ?></td>
                                <td><?php echo $value->card_type; ?></td>
                                <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypal' && $this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypalpro') { ?>
                                <td>
                                    <?php
                                    if (isset($user->status) && ($user->status == 1) && isset($usersub->status) && ($usersub->status == 1)) {//Subscriber
                                        if ($total_cards == 1) {//Not needed delete and primary option
                                        } else {//More than one cards
                                            if ($value->is_cancelled == 0) {//If it is a primary, no need for delete and primary option
                                            } else {//Except primary cards 
                                                ?>
                                               <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['delete']; ?>" data-type="delete" onclick="showConfirmPopup(this);">
                                            <?php echo $this->Language['delete']; ?>
                                            </a>

                                                &nbsp;&nbsp;&nbsp;
                                                <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['make_primary']; ?>" data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                   <?php echo $this->Language['make_primary']; ?>
                                                </a>

                                                <?php
                                            }
                                        }
                                    } else if (isset($user->status) && ($user->status == 1)) {//Not Subscriber
                                        ?>
                                        <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['delete']; ?>" data-type="delete" onclick="showConfirmPopup(this);">
                                            <?php echo $this->Language['delete']; ?>
                                        </a>

                                        <?php if ($value->is_cancelled != 0) { ?>
                                            &nbsp;&nbsp;&nbsp;
                                             <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['make_primary']; ?>" data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                    <?php echo $this->Language['make_primary']; ?>
                                                </a>
                                           
                                        <?php } ?>
                                    <?php } else if (isset($user->is_deleted) && ($user->is_deleted == 1) && isset($usersub->status) && ($usersub->status == 0)) {//Cancelled  ?>
                                        <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['delete']; ?>" data-type="delete" onclick="showConfirmPopup(this);">
                                            <?php echo $this->Language['delete']; ?>
                                        </a>
                                        <?php if ($value->is_cancelled != 0) { ?>
                                            &nbsp;&nbsp;&nbsp;
                                              <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-header="<?php echo $this->Language['make_primary']; ?>" data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                    <?php echo $this->Language['make_primary']; ?>
                                                </a>
                                        <?php } ?>
                                    <?php } ?>
                                </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
                <div class="clearfix"></div>
            <?php } ?>
                
            <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] != 'paypal' && isset($cards) && !empty($cards)) { ?>
            <div style="padding-left: 5px;">
                <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] == 'paypalpro') { ?>
                <h3><?php echo $this->Language['update_credit_debit_card']; ?></h3>
                <?php }else{?>
                <h3><?php echo $this->Language['add_credit_card']; ?></h3>
                <?php }?>
            </div>
            <div class="clearfix" style="height: 30px;"></div>

            <?php
            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
            ?>
            <form id="membership_form" name="membership_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
                <div id="card-info-error" class="error" style="display: none;margin: 0 0 15px 150px;"></div>
                <div class="row form-group">
                    <div class="col-lg-9">
                        <label for="card_name" class="col-lg-3 control-label"><?php echo $this->Language['text_card_name']; ?></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_card_name']; ?>" id="card_name" name="data[card_name]" required />
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-9">
                        <label for="card_number" class="col-lg-3 control-label"><?php echo $this->Language['text_card_number']; ?></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_card_number']; ?>" id="card_number" name="data[card_number]" required />
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-9">
                        <label class="control-label col-lg-3"><?php echo $this->Language['selct_exp_date']; ?></label>                    
                        <div class="col-lg-9">
                            <div style="float: left;width: 49%;margin-right: 2%;">
                                <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
                                    <option value=""><?php echo $this->Language['select_month']; ?></option>	
                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="float: left;width: 49%;">
                                <select name="data[exp_year]" id="exp_year" class="form-control" required="true" onchange="getMonthList();">
                                    <option value=""><?php echo $this->Language['select_year']; ?></option>
                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-9">
                        <label for="security_code"  class="control-label col-lg-3"><?php echo $this->Language['text_security_code']; ?></label>                    
                        <div class="col-lg-9">
                            <input type="password" id="" name="" style="display: none;" />
                            <input type="password" class="form-control" autocomplete="false" placeholder="<?php echo $this->Language['text_security_code']; ?>" id="security_code" name="data[security_code]" required />
                        </div>
                    </div>                     
                </div>
                
                <div id="card_div"></div>
                <input type="hidden" name="data[payment_gateway]" id="payment_gateway" value="<?php echo $gateways->short_code; ?>" />
                <input type="hidden" id="email" name="data[email]" value="<?php if (isset(Yii::app()->user->email) && trim(Yii::app()->user->email)) { echo Yii::app()->user->email;} ?>" />
                
                <div class="form-group">
                    <div class="col-lg-9">
                        <label class="control-label col-lg-3">&nbsp;</label>
                        <div class="col-lg-5">
                            <button id="register_membership" name="register_membership" class="btn btn-primary" onclick="validateUserForm();">
                                <?php 
                                    if ($this->PAYMENT_GATEWAY[$gateways->short_code] == 'paypalpro') { 
                                        echo $this->Language['update_card']; 
                                    }else{
                                        echo $this->Language['save_card'];
                                    }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
            </form>
            
            <?php } ?>
        </div>
    </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg"><?php echo $this->Language['auth_your_card']; ?></div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?php if ($this->PAYMENT_GATEWAY[$gateways->short_code] == 'paypalpro') { ?>
                    <div class="modal-title auth-msg" style="color: #42B970"><?php echo $this->Language['card_updated_success']; ?></div>
                <?php }else{?>
                    <div class="modal-title auth-msg" style="color: #42B970"><?php echo $this->Language['card_saved_success']; ?></div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<div id="errorPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title auth-msg" style="color: #FE2E2E"><?php echo $this->Language['unable_to_process_try_another_card']; ?></div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paymentModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;text-transform: none;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="paymodal" id="paymodal" method="post" autocomplete="false">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_payment" name="id_payment" value="" />
                            <input type="hidden" id="gateway_code" name="gateway_code" value="<?php echo $this->PAYMENT_GATEWAY[$gateways->short_code]?>" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="paymentbtn" class="btn btn-default"><?php echo $this->Language['yes']; ?></a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button" style="text-transform: capitalize;"><?php echo $this->Language['btn_cancel']; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
    var action = 'saveCard';
    var btn = "<?php echo $this->Language['save_card']; ?>";
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">'+JSLANGUAGE.expiry_month+'</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }

    function validateUserForm() {
        $('#card-info-error').hide();
        var validate = $("#membership_form").validate({
            rules: {
                "data[card_name]": {
                    required: true
                },
                "data[card_number]": {
                    required: true,
                    number: true
                },
                "data[exp_month]": {
                    required: true
                },
                "data[exp_year]": {
                    required: true
                },
                "data[security_code]": {
                    required: true
                }
            },
            messages: {
                "data[card_name]": {
                    required: JSLANGUAGE.card_name_required
                },
                "data[card_number]": {
                    required: JSLANGUAGE.card_number_required,
                    number: JSLANGUAGE.card_number_required
                },
                "data[exp_month]": {
                    required: JSLANGUAGE.expiry_month_required
                },
                "data[exp_year]": {
                    required: JSLANGUAGE.expiry_year_required
                },
                "data[security_code]": {
                    required: JSLANGUAGE.security_code_required
                }
            }
        });
        var x = validate.form();
        if (x) {
            var payment_gateway = $("#payment_gateway").val();
            var class_name = payment_gateway+'()';
            eval ("var obj = new "+class_name);
            if (payment_gateway !== 'manual' && payment_gateway != 'paypalpro') {
                obj.processCard(1);
            }else if(payment_gateway == 'paypalpro'){
                obj.updateCard();
            }
        }
    }

    
    function showConfirmPopup(obj) {
        $("#paymentModal").modal('show');
        var header = $(obj).attr('data-header');
        var type = $(obj).attr('data-type');
        var header_lower = header.toLowerCase();
        $("#headermodal").text(header.charAt(0).toUpperCase() + header.slice(1)+" "+ JSLANGUAGE.card+"?");
        var myString = JSLANGUAGE.action_card;
        var myString = myString.replace(/\$/g, '');
        var oldword = "action";
        var reg = new RegExp(oldword, "g");
        var header1 = myString.replace(reg, header_lower);
        $("#bodymodal").text(header1);
        $("#paymentbtn").attr('data-managepayment_id', $(obj).attr('data-managepayment_id'));
        
        var onclick = type+'Card(this)';
        
        $("#paymentbtn").attr('data-type', type);
        $("#paymentbtn").attr('onclick',onclick).bind('click');
    }
    
    function makeprimaryCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        
        var action = HTTP_ROOT + "/user/" + type+"Card";
            
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }
    
    function deleteCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        
        var action = HTTP_ROOT + "/user/" + type+"Card";
            
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }
    
    function payFromPrimaryCard() {
        var action = HTTP_ROOT + "/user/payFromPrimaryCard?gateway_code=<?php echo $this->PAYMENT_GATEWAY[$gateways->short_code]?>";
        window.location.href = action;
    }
    
    $(document).ready(function() {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
        
        $("#security_code").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                 // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>

<?php if (isset($this->PAYMENT_GATEWAY[$gateways->short_code]) && $this->PAYMENT_GATEWAY[$gateways->short_code] != 'manual') {  ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateways->short_code].'.js';?>"></script>
<?php } ?>

<?php  } ?>
