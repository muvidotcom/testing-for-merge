<!-- PPV Plan Modal -->
<?php
if (isset($gateways[0]) && !empty($gateways[0])) {
    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
    if (isset($plan_details) && !empty($plan_details)) {
        $amount = '';
        $symbol = (isset($plan_details[0]['price']['currency_symbol']) && trim($plan_details[0]['price']['currency_symbol'])) ? $plan_details[0]['price']['currency_symbol'] : $plan_details[0]['price']['currency_code'] . ' ';
        if (isset($data['content_types_id']) && intval($data['content_types_id']) != 3) {
            if (isset($data['member_subscribed']) && intval($data['member_subscribed'])) {
                $amount = $plan_details[0]['price']['price_for_subscribed'];
                if ($plan_details[0]['description'] == '') {
                    $decription = "N/A";
                } else {
                    $decription = $plan_details[0]['description'];
                }
            } else {
                $amount = $plan_details[0]['price']['price_for_unsubscribed'];
                if ($plan_details[0]['description'] == '') {
                    $decription = "N/A";
                } else {
                    $decription = $plan_details[0]['description'];
                }
            }
        }
        ?>
<input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
<input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
<input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
<input type="hidden" value="<?php echo (isset($data['content_types_id']) && intval($data['content_types_id'])) ? $data['content_types_id'] : 0; ?>" id = "content_types_id" />
<input type="hidden" value="1" id = "is_ppv" />

<!-- For both single and multipart content when bundle is enabled-->
<div id="price_detail" style="position: relative;">
    <div class="col-md-12">
        <span class="error" id="plan_error"></span>
        <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
            <?php if ((isset($data['content_types_id']) && intval($data['content_types_id']) == 1) || (isset($data['content_types_id']) && intval($data['content_types_id']) == 2) ) { ?>
			<?php if(count($plan_details) > 1){?><div style="font-weight: bold"><?php echo $this->Language['choose_plan']; ?></div> <?php }?>
                <div class="form-group">
                    <?php
                    $ppv_price = 0;
                    for ($i = 0; $i < count($plan_details); $i++) {
                        if ($i == 0) {
                            $ppv_price = $amount;
                            $symbol = $plan_details[$i]['price']['currency_symbol'];
                        }
                        ?>
                        <div class="col-md-12">
                            <label style="font-weight: normal">
                                <input type="radio" name="data[plan]" id="ppvplan_id" class="ppv-bundled-cls" <?php if ($i == 0) { ?>checked="checked" <?php } ?> value="<?php echo $plan_details[$i]['id']; ?>" onclick="getppvplanPrice(this,<?php echo $plan_details[$i]['id']; ?>);" data-currency-id="<?php echo $plan_details[$i]['price']['currency_id']; ?>" />
                                <?php echo (trim($plan_details[$i]['title']) ? $plan_details[$i]['title'] : $this->Language['ppv_default']); ?>
                            </label>
                        </div>
                    <?php } ?>
                </div> 
                <div style="font-weight: bold">
                    <?php echo $this->Language['price'] . ":"; ?> 
                    <span id="charged_amt" data-amount="<?php echo $ppv_price; ?>" data-currency="<?php echo $symbol; ?>"><?php echo $symbol; ?><?php echo $ppv_price; ?></span>
                    
                    <span id="discount_charged_amt" style="display: none;">
                        <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol . $ppv_price; ?></span>
                        <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                    </span>
                 <span id="description_block" <?php if(trim($plan_details[0]['description'])){ ?> style="display:block" <?php } else{ ?> style="display:none" <?php } ?>> 
                    <br><?php echo $this->Language['description_ppv'] . ":"; ?> 
                    <span id="description" ><?php echo $decription; ?></span>
                    <span id="free_trail_txt" style="font-weight: bold;font-size: 13px; color: green"></span>
                 </span>
                </div>
                <?php } else if (isset($data['content_types_id']) && intval($data['content_types_id']) == 3) {
                ?>
                <div class="form-group">
                    <?php
                    for ($i = 0; $i < count($plan_details); $i++) {
                        if ($i == 0) {
                            $symbol = $plan_details[$i]['price']['currency_symbol'];
                            $currency_id = $plan_details[$i]['price']['currency_id'];
                            $code = $plan_details[$i]['price']['currency_code'];
                            $des_amt = 0;
                        }
                        ?>
                        <div class="col-md-12" id="<?php echo "plan" . $plan_details[$i]['id']; ?>">
                            <label style="font-weight: normal;width: 100%;">
                                <input type="radio" name="data[plan_category]" id="<?php echo "ppvplan_multi_id_" . $plan_details[$i]['id']; ?>" class="ppv-bundled-cls" <?php if ($i == 0) { ?>checked="checked" <?php } ?> value="<?php echo $plan_details[$i]['id']; ?>" onclick="getppvplanPrice(this,<?php echo $plan_details[$i]['id']; ?>,<?php echo $i; ?>);" data-currency-id="<?php echo $plan_details[$i]['price']['currency_id']; ?>" <?php  if(count($plan_details)==1){?> style="display:none"<?php } ?> />
                                <span <?php  if(count($plan_details)==1){?> style="font-size:16px;"<?php } ?> ><?php echo (trim($plan_details[$i]['title']) ? $plan_details[$i]['title'] : $this->Language['ppv_default']); ?></span> <br> <?php  if(count($plan_details)==1){?> <br><?php } ?>
                                <div class="col-md-12">
                                    <?php
                                    if (isset($data['min_season']) && intval($data['min_season'])) {
                                        $is_show_price_not_zero = $is_default_season_price_not_zero = $is_season_price_not_zero = $is_episode_price_not_zero = 1;
                                        $ppv_season_status = $season_price = $default_season_price = 0;
                                        if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                            if (abs($plan_details[$i]['price']['show_subscribed']) < 0.00001) {
                                                $is_show_price_not_zero = 0;
                                            }
                                        } else {
                                            if (abs($plan_details[$i]['price']['show_unsubscribed']) < 0.00001) {
                                                $is_show_price_not_zero = 0;
                                            }
                                        }

                                        if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                            $default_season_price = $season_price = $plan_details[$i]['price']['season_subscribed'];
                                            if (abs($plan_details[$i]['price']['season_subscribed']) < 0.00001) {
                                                $is_season_price_not_zero = 0;
                                            }
                                            if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($plan_details[$i]['price']['ppv_season_status']) && intval($plan_details[$i]['price']['ppv_season_status'])) {
                                                $ppv_season_status = 1;
                                                if (isset($plan_details[$i]['price']['default_season_price'])) {
                                                    $season_price = @$plan_details[$i]['price']['default_season_price'];
                                                    if (isset($plan_details[$i]['price']['default_season_price']) && abs($plan_details[$i]['price']['default_season_price']) < 0.00001) {
                                                        $is_default_season_price_not_zero = 0;
                                                    }
                                                }
                                            }
                                        } else {
                                            $default_season_price = $season_price = $plan_details[$i]['price']['season_unsubscribed'];
                                            if (abs($plan_details[$i]['price']['season_unsubscribed']) < 0.00001) {
                                                $is_season_price_not_zero = 0;
                                            }
                                            if (isset($ppv_buy->is_multi_season) && intval($ppv_buy->is_multi_season) && isset($plan_details[$i]['price']['ppv_season_status']) && intval($plan_details[$i]['price']['ppv_season_status'])) {
                                                $ppv_season_status = 1;
                                                if (isset($plan_details[$i]['price']['default_season_price'])) {
                                                    $season_price = @$plan_details[$i]['price']['default_season_price'];
                                                    if (isset($plan_details[$i]['price']['default_season_price']) && abs($plan_details[$i]['price']['default_season_price']) < 0.00001) {
                                                        $is_default_season_price_not_zero = 0;
                                                    }
                                                }
                                            }
                                        }
                                        if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                            if (abs($plan_details[$i]['price']['episode_subscribed']) < 0.00001) {
                                                $is_episode_price_not_zero = 0;
                                            }
                                        } else {
                                            if (abs($plan_details[$i]['price']['episode_unsubscribed']) < 0.00001) {
                                                $is_episode_price_not_zero = 0;
                                            }
                                        }
                                        $charged_amt = 0;
                                        $is_show_checked = $is_season_checked = $is_episode_checked = 0;

                                        if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) {
                                            if ($i == 0) {
                                                $temp_season_id = 0;
                                                $temp_season_title = '';
                                            }
                                            $is_show_checked = 1;
                                            $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $plan_details[$i]['price']['show_subscribed'] : $plan_details[$i]['price']['show_unsubscribed'];
                                        } else if (isset($data['is_season']) && intval($data['is_season']) && (intval($is_season_price_not_zero) || intval($is_default_season_price_not_zero))) {
                                            if ($i == 0) {
                                                $temp_season_id = $data['series_number'];
                                                $temp_season_title = $this->Language['season'] . ' ' . $data['series_number'];
                                            }
                                            $is_season_checked = 1;
                                            $charged_amt = $season_price;
                                        } else if (isset($data['is_episode']) && intval($data['is_episode']) && intval($is_episode_price_not_zero)) {
                                            if ($i == 0) {
                                                $temp_season_id = 0;
                                                $temp_season_title = '';
                                            }
                                            $is_episode_checked = 1;
                                            $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $plan_details[$i]['price']['episode_subscribed'] : $plan_details[$i]['price']['episode_unsubscribed'];
                                        }
                                        if ($i == 0) {
                                            $des_amt = $charged_amt;
                                        }
                                        ?>

            <?php if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) { ?>
                                            <div class="ppv_details" style="display:<?php if ($i == 0) {
                    echo 'block';
                } else {
                    echo 'none';
                } ?>">
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <label style="font-weight: normal">
                                                            <input type="radio" name="data[plan]" <?php if ($i == 0) { ?>checked="checked" <?php } ?> value="show" class="showtext" data-currency="<?php echo $plan_details[$i]['price']['currency_symbol']; ?>" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                    echo $plan_details[$i]['price']['show_subscribed'];
                                } else {
                                    echo $plan_details[$i]['price']['show_unsubscribed'];
                                } ?>" onclick="setPriceForMulitPart(this);" />
                                                <?php echo $this->Language['entire_show'] . ":"; ?>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-2">
                                                            <?php echo $plan_details[$i]['price']['currency_symbol']; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                                                echo $plan_details[$i]['price']['show_subscribed'];
                                                            } else {
                                                                echo $plan_details[$i]['price']['show_unsubscribed'];
                                                            } ?>
                                                    </div>
                                                </div>
                                                        <?php } ?>

                                                        <?php if (isset($data['is_season']) && intval($data['is_season']) && intval($data['min_season']) && (intval($is_season_price_not_zero) || intval($is_default_season_price_not_zero))) { ?>
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label style="font-weight: normal">
                                                            <input type="radio" name="data[plan]" <?php if (intval($is_season_checked)) { ?>checked="checked" <?php } ?> value="season" class="seasontext" data-price="<?php echo number_format((float) $season_price, 2, '.', ''); ?>" onclick="setPriceForMulitPart(this);"/>
                <?php echo $this->Language['season'] . ":"; ?>
                                                        </label>
                                                    </div>

                                                    <div class="col-md-7">
                                                        <select class="form-control seasonval" name="data[season]" id="seasonval_<?php echo $plan_details[$i]['id']; ?>" data-default_season_price="<?php echo $default_season_price; ?>" data-ppv_plan_id="<?php echo $plan_details[$i]['price']['ppv_plan_id']; ?>"  data-ppv_pricing_id="<?php echo $plan_details[$i]['price']['id']; ?>" data-currency_id="<?php echo $plan_details[$i]['price']['currency_id']; ?>" data-isSubscribed="<?php echo $data['member_subscribed']; ?>" data-is_season_multi_price="<?php echo $ppv_season_status; ?>" onchange="setSeason(this);">
                <?php for ($j = $data['min_season']; $j <= $data['max_season']; $j++) { ?>
                                                                <option value="<?php echo $j; ?>" <?php if ((int) @$season == $j) {
                        echo "selected";
                    } ?>><?php echo $this->Language['season']; ?> <?php echo $j; ?></option>
                                                <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="season-loader-ppv" style="text-allign:center;display: none;">
                                                            <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
                                                            <span class="sr-only"><?php echo $this->Language['loding']; ?></span>
                                                        </div>
                                                        <div class="season-price-dv">
                <?php echo $plan_details[$i]['price']['currency_symbol']; ?><span class="season-price-spn"><?php echo number_format((float) $season_price, 2, '.', ''); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                        <?php } ?>


            <?php if (isset($data['is_episode']) && intval($data['is_episode']) && intval($data['min_season']) && intval($is_episode_price_not_zero)) { ?>
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label style="font-weight: normal">
                                                            <input type="radio" name="data[plan]" <?php if (intval($is_episode_checked)) { ?>checked="checked" <?php } ?> value="episode" class="episodetext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                    echo $plan_details[$i]['price']['episode_subscribed'];
                } else {
                    echo $plan_details[$i]['price']['episode_unsubscribed'];
                } ?>" onclick="setPriceForMulitPart(this);"/>
                                                        <?php echo $this->Language['episode'] . ":"; ?>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control seasonvall" name="data[season]" id="seasonval1_<?php echo $plan_details[$i]['id']; ?>" data-id="<?php echo $plan_details[$i]['id']; ?>" onchange="showEpisode(this);">
                <?php for ($j = $data['min_season']; $j <= $data['max_season']; $j++) { ?>
                                                                <option value="<?php echo $j; ?>" <?php if ((int) @$data['series_number'] == $j) {
                        echo "selected";
                    } ?>><?php echo $this->Language['season']; ?> <?php echo $j; ?></option>
                <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        <select class="form-control episodeval" name="data[episode]" id="episodeval_<?php echo $plan_details[$i]['id']; ?>" onchange="setEpisode(this);">
                                            <?php foreach ($data['episodes'] as $key => $value) { ?>
                                                                <option data-episode_id="<?php echo $value['id']; ?>" value="<?php echo $value['embed_id']; ?>" <?php if (@$data['stream_id'] == $value['embed_id']) {
                        echo "selected";
                    } ?>><?php echo $value['episode_title']; ?></option>
                <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                <?php echo $plan_details[$i]['price']['currency_symbol']; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                    echo $plan_details[$i]['price']['episode_subscribed'];
                } else {
                    echo $plan_details[$i]['price']['episode_unsubscribed'];
                } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
            <?php } ?>

                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label id="data[plan]-error" class="error" for="data[plan]" style="display: none;"><?php echo $this->Language['choose_plan']; ?></label>
                                                </div>
                                            </div>


                                        </div>
                        <?php } else { ?>
                                        <span class="error"><?php echo $this->Language['no_video']; ?></span>
                        <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                            </label>
                        </div>
                    <?php } ?>
                </div> 


                <div class="form-group">
                    <div class="col-md-12">
                        <div style="font-weight: bold">
                            <?php echo $this->Language['price'] . ":"; ?> 
                            <span id="charged_amt" data-amount="<?php echo $des_amt; ?>" data-currency="<?php echo $symbol; ?>"><?php echo $symbol; ?><?php echo $des_amt; ?></span>
                            
                            <span id="discount_charged_amt" style="display: none;">
                                <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol . $des_amt; ?></span>
                                <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                            </span>
                            <span id="description_block" <?php if(trim($plan_details[0]['description'])){ ?> style="display:block" <?php } else{ ?> style="display:none" <?php } ?>> 
                            <br><?php echo $this->Language['description_ppv'] . ":"; ?> 
                            <span id="description" ><?php echo (isset($plan_details[0]['description']) && !empty($plan_details[0]['description'])) ? $plan_details[0]['description'] : 'N/A'; ?></span>  
                        </div>
                    </div>
                </div>
            <?php } else if(isset($data['content_types_id']) && intval($data['content_types_id']) == 4) {
            ?>
                <div style="font-weight: bold"><?php echo $this->Language['choose_plan']; ?></div>
                <div class="form-group">
                    <?php
                    $ppv_price = 0;
                    for ($i = 0; $i < count($plan_details); $i++) {  
                        if ($i == 0) {
                            $ppv_price = $amount;
                            $symbol = $plan_details[$i]['price']['currency_symbol'];   
                        }
                        ?>
                        <div class="col-md-12">
                            <label style="font-weight: normal">
                                <input type="radio" name="data[plan]" id="ppvplan_live_id" class="ppv-bundled-cls" <?php if ($i == 0) { ?>checked="checked" <?php } ?> value="<?php echo $plan_details[$i]['id']; ?>" onclick="getppvplanPrice(this,<?php echo $plan_details[$i]['id']; ?>);" data-currency-id="<?php echo $plan_details[$i]['price']['currency_id']; ?>" />
                                <?php echo (trim($plan_details[$i]['title']) ? $plan_details[$i]['title'] : $this->Language['ppv_default']); ?>
                            </label>
                        </div>
                    <?php } ?>
                </div> 
                <div style="font-weight: bold">
                    <?php echo $this->Language['price'] . ":"; ?> 
                    <span id="charged_amt" data-amount="<?php echo $ppv_price; ?>" data-currency="<?php echo $symbol; ?>"><?php echo $symbol; ?><?php echo $ppv_price; ?></span>
                            
                    <span id="discount_charged_amt" style="display: none;">
                        <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol . $ppv_price; ?></span>
                        <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                            </span>
                    <br><?php echo $this->Language['description_ppv'] . ":"; ?> 
                    <span id="description" ><?php echo $decription; ?></span>
                    <span id="free_trail_txt" style="font-weight: bold;font-size: 13px; color: green"></span>
                        </div>

<?php } else { ?>
                <div class="form-group">
                    <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) != 4) { ?>
                        <?php if (isset($validity->validity_period) && intval($validity->validity_period)) { 
				($validity->validity_period > 1)?$validity_recurrence = strtolower($validity->validity_recurrence).'s':$validity_recurrence = strtolower($validity->validity_recurrence);
			?>
                            <div class="col-md-12">
            <?php echo $this->Language['available_for']; ?> <?php echo $validity->validity_period . ' ' . $this->Language[$validity_recurrence] ?> <?php echo $this->Language['to_watch']; ?>
                            </div>
        <?php } ?>
    <?php } ?>
                </div>


                <div class="form-group">
                    <div class="col-md-12">
                        <div style="font-weight: bold">
    <?php echo $this->Language['price'] . ":"; ?> 
                            <span id="charged_amt" data-amount="<?php echo $amount; ?>" data-currency="<?php echo $symbol; ?>"><?php echo $symbol; ?><?php echo $amount; ?></span>
                            <span id="discount_charged_amt" style="display: none;">
                                <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol . $amount; ?></span>
                                <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                            </span>
                        </div>
                    </div>
                </div>
                <?php } ?>
            <div class="clearfix"></div>


            <div class="form-group ">
<?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                    <br/>
                    <div class="col-sm-6 pull-left">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                            <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                            </span>
                        </div>
                        <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                        <div id="valid_coupon_suc" class="has-success" style="display: none;">
                            <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                    </div>
<?php } else { ?>
                    <div class="col-md-12">
                        <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                    </div>
<?php } ?>

                <div class="clear"></div></div>
        </form>
    </div> 
</div>

<?php
}

include('ppv_card_detail.php');
}
?>

<input type="hidden" id="temp_season_id" value="<?php echo $temp_season_id?>" />
<input type="hidden" id="temp_episode_id" value="0" />
<input type="hidden" id="temp_season_title" value="<?php echo $temp_season_title?>" />
<input type="hidden" id="temp_episode_title" value="" />

<script type="text/javascript">
    var action = 'ppvpayment';
    var btn = '<?php echo $this->Language['btn_paynow']; ?>';
    var is_coupon_exists = 0;
<?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
        is_coupon_exists = 1;
<?php } ?>

    $(document).ready(function () {
        getCardDetail();
        isCouponExist();

        $('input').attr('autocomplete', 'off');
        $('form').attr('autocomplete', 'off');
    });

    $(document).click(function () {
        $(".popover").hide();
    });

    $(".popover").click(function (event) {
        event.stopPropagation();
    });

    function setEpisode(obj) {
        $('#temp_season_id').val(0);
        $('#temp_episode_id').val(0);
        $('#temp_season_title').val('');
        $('#temp_episode_title').val('');
        var episode = $(obj).val();
        var episode_title = $(obj).find("option:selected").text();
        $("#btn_proceed_payment").attr("onClick", "javascript: return validateCategoryForm();");
        if ($.trim(episode)) {

            var season_id = $(obj).parent().siblings().find('.seasonvall').val();
            $('#temp_season_id').val(season_id);
            $('#temp_episode_id').val(episode);

            var season_title = $(obj).parent().siblings().find('.seasonvall option:selected').text();
            $('#temp_season_title').val(season_title);
            $('#temp_episode_title').val(episode_title);

            //$(".episodetext").prop('checked', true);
        } else {
            //$(".episodetext").prop('checked', false);
        }
        resetCouponPrice();
        if ($('label.error').length) {
            $('label.error').hide();
        }
    }

    function setSeason(obj) {
        var season = $(obj).val();
        var season_title = $(obj).find("option:selected").text();
        $('#temp_season_id').val(season);
        $('#temp_season_title').val(season_title);
        var is_season_multi_price = $(obj).attr("data-is_season_multi_price");
        if (parseInt(season) && parseInt(is_season_multi_price)) {
            var ppv_plan_id = $(obj).attr("data-ppv_plan_id");
            var ppv_pricing_id = $(obj).attr("data-ppv_pricing_id");
            var isSubscribed = $(obj).attr("data-isSubscribed");
            var currency_id = $(obj).attr("data-currency_id");
            var default_season_price = $(obj).attr("data-default_season_price");

            var url = "<?php echo Yii::app()->baseUrl; ?>/user/getPPVSeasonPrice";

            //$("#season-price-dv").hide();
            $(obj).parent().siblings().find('.season-price-dv').hide();
            $(".season-loader-ppv").show();

            $("#btn_proceed_payment").prop("disabled", true);
            $("#coupon_btn").prop("disabled", true);

            $.post(url, {'ppv_plan_id': ppv_plan_id, 'ppv_pricing_id': ppv_pricing_id, 'isSubscribed': isSubscribed, 'currency_id': currency_id, 'season': season}, function (res) {
                $(".season-loader-ppv").hide();

                var price = res;
                if (parseInt(res) === -1) {
                    price = default_season_price;
                }
                $(obj).parent().siblings().find('.season-price-dv').children(".season-price-spn").text(price);
                //$("#season-price-spn").text(price);
                $(obj).parent().siblings().find(".seasontext").attr("data-price", price);
                $("#btn_proceed_payment").attr("onClick", "javascript: return validateCategoryForm();");
                //$("#season-price-dv").show();
                $(obj).parent().siblings().find('.season-price-dv').show();
                $("#btn_proceed_payment").text("<?php echo $this->Language['btn_proceed_payment']; ?>");
                $("#btn_proceed_payment").removeAttr("disabled");
                $("#coupon_btn").removeAttr("disabled");

                if ($(obj).parent().siblings().find(".seasontext").is(':checked')) {
                    var cur = $("#charged_amt").attr('data-currency');
                    $("#charged_amt").attr("data-amount", price);
                    $("#charged_amt").text(cur + price);
                    $("#dis_charged_amt").text(cur + price);
                    $("#coupon_use").val(0);
                    $("#coupon").val('');

                    if (parseFloat(price) === 0.00) {
                        $("#btn_proceed_payment").text("Play");
                        $("#coupon_btn").prop("disabled", true);
                    }
                }
            });
        } else {
            $("#btn_proceed_payment").attr("onClick", "javascript: return validateCategoryForm();");
        }
        resetCouponPrice();
        if (parseInt(season)) {
            //$(".seasontext").prop('checked', true);
        } else {
            //$(".seasontext").prop('checked', false);
        }
    }

    function setPriceForMulitPart(obj) {
        $('#temp_season_id').val(0);
        $('#temp_episode_id').val(0);
        $('#temp_season_title').val('');
        $('#temp_episode_title').val('');
        var price = $(obj).attr("data-price");
        var cur = $("#charged_amt").attr('data-currency');
        $("#charged_amt").text(cur + price);
        $("#charged_amt").attr('data-amount', price);
        $("#dis_charged_amt").text(cur + price);
        $("#btn_proceed_payment").text("<?php echo $this->Language['btn_proceed_payment']; ?>");
        $("#btn_proceed_payment").attr("onClick", "javascript: return validateCategoryForm();");
        if (parseFloat(price) === 0.00) {
            $("#btn_proceed_payment").text("Play");
            $("#coupon_btn").prop("disabled", true);
        }

        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }

        var season_id = $(obj).parent().parent().siblings().find('.seasonval').val();
        var season_title = $(obj).parent().parent().siblings().find('.seasonval option:selected').text();
        $('#temp_season_id').val(season_id);
        $('#temp_season_title').val(season_title);
        if ($.trim($(obj).val()) === 'episode') {
            var season_id = $(obj).parent().parent().siblings().find('.seasonvall').val();
            var season_title = $(obj).parent().parent().siblings().find('.seasonvall option:selected').text();
            $('#temp_season_id').val(season_id);
            $('#temp_season_title').val(season_title);
            var episode_id = $(obj).parent().parent().siblings().find('.episodeval').val();
            var episode_title = $(obj).parent().parent().siblings().find('.episodeval option:selected').text();
            $('#temp_episode_id').val(episode_id);
            $('#temp_episode_title').val(episode_title);
        }
    }

    function showEpisode(obj) {
        $('#temp_season_id').val(0);
        $('#temp_episode_id').val(0);
        $('#temp_season_title').val('');
        $('#temp_episode_title').val('');
        var movie_id = $("#ppvmovie_id").val();
        var season = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getEpisodes";
        resetCouponPrice();
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);

        if ($('label.error').length) {
            $('label.error').hide();
        }

        $.post(url, {'movie_id': movie_id, 'season': season}, function (res) {
            $("#btn_proceed_payment").removeAttr("disabled");
            var str = '';

            if (res.length) {
                for (var i in res) {
                    str = str + '<option data-episode_id="' + res[i].id + '" value="' + res[i].embed_id + '">' + res[i].episode_title + '</option>';
                }
            }
            var episode_element = $(obj).parent().siblings().find('.episodeval');
            episode_element.html(str);

            var season_id = $(obj).val();
            var season_title = $(obj).find("option:selected").text();
            var episode = episode_element.val();
            var episode_title = $(obj).parent().siblings().find('.episodeval option:selected').text();
            $('#temp_season_id').val(season_id);
            $('#temp_season_title').val(season_title);
            $('#temp_episode_id').val(episode);
            $('#temp_episode_title').val(episode_title);

            // $("#episodeval").html(str);
            $('#loader-ppv').show();
            $("#btn_proceed_payment").attr("onClick", "javascript: return validateCategoryForm();");
            if ($.trim(episode_element)) {
                $('#loader-ppv').hide();
                //$(".episodetext").prop('checked', true);
            } else {
                $('#loader-ppv').hide();
                //$(".episodetext").prop('checked', false);
            }
        }, 'json');
    }
/* Not re quired for now
 * 
    jQuery.validator.addMethod("isseason", function (value, element) {

        if ($(".seasontext").is(':checked')) {
            if ($.trim($("#seasonval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_season);

    jQuery.validator.addMethod("isepisode", function (value, element) {
        if ($(".episodetext").is(':checked')) {
            if ($.trim($("#episodeval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_episode);
    
    */
    function viewDetail(obj) {
        var plan = $(obj).attr("data-plan");
        if ($("#popover_" + plan).is(':hidden')) {
            $("#ldr_" + plan).show();
            $(".popover").hide();
            var url = "<?php echo Yii::app()->baseUrl; ?>/userPayment/GetsubscriptionBundledContents";
            $.post(url, {'plan': plan}, function (res) {
                $("#ldr_" + plan).hide();
                $("#popover_" + plan).show();
                $(".popover-content").html(res);
            });
        } else {
            $(".popover").hide();
        }
    }
    function getppvplanPrice(obj, ppvcategory, i) {
        var plan_id = $(obj).val();
        var currency_id = $(obj).attr('data-currency-id');
        $('#currency_id').val(currency_id);
        var url = "<?php echo Yii::app()->baseUrl; ?>/userPayment/getppvprice";
        $("#is_subscription_bundle_checked").val(ppvcategory);
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        var content_types_id =<?php echo $data['content_types_id']; ?>;
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
            resetCouponFreeTrial();
        }
        $("#plandetail_id").val(plan_id);
        var subscribed_or_not = <?php echo ($data['member_subscribed']); ?>;
        $.post(url, {'plan_id': plan_id}, function (res) {
            var data = JSON.parse(res);
            if (data.description === '') {
                $("#description_block").hide();
            } else {
                 $("#description_block").show();
                $("#description").html(data.description);
            }
            if (content_types_id === 1 || content_types_id === 4) {
                $('#loader-ppv').hide();
                $("#btn_proceed_payment").removeAttr("disabled");
                var str = '';

                //alert(subscribed_or_not);
                var cur = data.symbol;
                $("#timeframe_div").html(str);
                if (subscribed_or_not) {
                    $("#charged_amt").html(cur + data.price_for_subscribed);
                    $("#charged_amt").attr('data-amount', data.price_for_subscribed);
                    $("#dis_charged_amt").text(cur + data.price_for_subscribed);
                } else {
                    $("#charged_amt").html(cur + data.price_for_unsubscribed);
                    $("#charged_amt").attr('data-amount', data.price_for_unsubscribed);
                    $("#dis_charged_amt").text(cur + data.price_for_unsubscribed);
                }
                $("#charged_amt").attr('data-currency', cur);
                
            } else if (content_types_id === 3) {
                $('#loader-ppv').hide();
                $("#btn_proceed_payment").removeAttr("disabled");
                var str = '';
                var cur = data.symbol;
                var symbol = data.symbol;

                $("#timeframe_div").html(str);
              
                
                if (subscribed_or_not) {
                    if($.trim(data.show_subscribed)){
                        $("#charged_amt").html(cur + data.show_subscribed);
                        $("#charged_amt").attr('data-amount', data.show_subscribed);
                        $("#dis_charged_amt").text(cur + data.show_subscribed);
                    }else if($.trim(data.season_subscribed)){
                        $("#charged_amt").html(cur + data.season_subscribed);
                        $("#charged_amt").attr('data-amount', data.season_subscribed);
                        $("#dis_charged_amt").text(cur + data.season_subscribed);
                    }else{
                        $("#charged_amt").html(cur + data.episode_subscribed);
                        $("#charged_amt").attr('data-amount', data.episode_subscribed);
                        $("#dis_charged_amt").text(cur + data.episode_subscribed);
                    }
                    $("#charged_amt").attr('data-currency', cur);
                } else {
                    if($.trim(data.show_unsubscribed)){
                        $("#charged_amt").html(cur + data.show_unsubscribed);
                        $("#charged_amt").attr('data-amount', data.show_unsubscribed);
                        $("#dis_charged_amt").text(cur + data.show_unsubscribed);
                    }else if($.trim(data.season_unsubscribed)){
                        $("#charged_amt").html(cur + data.season_unsubscribed);
                        $("#charged_amt").attr('data-amount', data.season_unsubscribed);
                        $("#dis_charged_amt").text(cur + data.season_unsubscribed);
                    }else{
                        $("#charged_amt").html(cur + data.show_unsubscribed);
                        $("#charged_amt").attr('data-amount', data.episode_unsubscribed);
                        $("#dis_charged_amt").text(cur + data.episode_unsubscribed);
                    }
                    $("#charged_amt").attr('data-currency', cur);
                }
                $('.ppv_details').hide();
                $(obj).siblings('div').children('.ppv_details').show();
                $('#charged_amt').show();
                if ($(obj).children('input .showtext').length >= 0) {
                    $('input:radio[value=show]')[i].checked = true;
                    var y = $('input:radio').filter('[value=show]')[i];
                    var price = $(y).attr('data-price');
                    $("#charged_amt").html(symbol + price);

                } else if ($(obj).children('input .seasontext').length >= 0) {
                    $('input:radio[value=season]')[i].checked = true;
                    var y = $('input:radio').filter('[value=season]')[i];
                    var price = $(y).attr('data-price');

                } else if ($(obj).children('input .episodetext').length >= 0) {
                    $('input:radio[value=episode]')[i].checked = true;
                    var y = $('input:radio').filter('[value=episode]')[i];
                    var price = $(y).attr('data-price');
                }
            }
        }, 'text');
    }
    function resetCouponPrice() {
        $("#coupon").val('');
        $("#valid_coupon_suc").hide();
        $("#invalid_coupon_error").hide().html('');
        $("#charged_amt").show();
        $("#discount_charged_amt").hide();
    }
    function resetCouponFreeTrial() {
        $("#free_trail_txt").hide().html('');
        $("#subscriptionBundle_coupon").val('');
    }
    function getdetailsofmultipart(obj, i) {
        $('.ppv_details').hide();
        //////var description=[];
        $(obj).siblings('div').children('.ppv_details').show();
        $('#charged_amt').show();
        if ($(obj).children('input .showtext').length >= 0) {
            $('input:radio[value=show]')[i].checked = true;
            var y = $('input:radio').filter('[value=show]')[i];
            var price = $(y).attr('data-price');
            var symbol = $(y).attr('data-currency');
            $("#charged_amt").html(symbol + price);

        } else if ($(obj).children('input .seasontext').length >= 0) {
            $('input:radio[value=season]')[i].checked = true;
            var y = $('input:radio').filter('[value=season]')[i];
            var price = $(y).attr('data-price');
            var symbol = $(y).attr('data-currency');

        } else if ($(obj).children('input .episodetext').length >= 0) {
            $('input:radio[value=episode]')[i].checked = true;
            var y = $('input:radio').filter('[value=episode]')[i];
            var price = $(y).attr('data-price');
            var symbol = $(y).attr('data-currency');
        }
    }
</script>

<script type="text/javascript" src="<?php echo $this->siteurl; ?>/common/js/action.js"></script>
<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php
$payment_gateway_str = '';
if (isset($this->PAYMENT_GATEWAY) && count($this->PAYMENT_GATEWAY) > 1) {
    $payment_gateway_str = implode(',', $this->PAYMENT_GATEWAY);
    foreach ($this->PAYMENT_GATEWAY as $gateway_code) {
        ?>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code] . '.js'; ?>"></script>
    <?php
    }
} else {
    if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
        $payment_gateway_str = $this->PAYMENT_GATEWAY[$gateway_code];
        ?>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code] . '.js'; ?>"></script>
    <?php }
} ?>

<input type="hidden" id="payment_gateway_str" value="<?php echo $payment_gateway_str; ?>">
