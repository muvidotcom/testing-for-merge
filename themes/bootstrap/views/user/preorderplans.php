<!-- PPV Plan Modal -->
<?php
if(isset($gateways[0]) && !empty($gateways[0])){
    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
    
    if (isset($plan) && !empty($plan)) {
        $amount = '';
        $symbol = (isset($currency->symbol) && trim($currency->symbol)) ? $currency->symbol: $currency->code.' ';

        if (isset($data['content_types_id']) && intval($data['content_types_id']) != 3) {
            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                $amount =  $price['price_for_subscribed']; 
            } else {
                $amount =  $price['price_for_unsubscribed']; 
            }
        }
                
?>

    <input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
    <input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
    <input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
    <input type="hidden" value="<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>" id = "content_types_id" />
    <input type="hidden" value="1" id = "isadv" />
    <!-- For both single and multipart content when bundle is enabled-->
   
    <div id="price_detail" style="position: relative;">
      <div class="col-md-12">
            <span class="error" id="plan_error"></span>
            <?php if (isset($data['msg']) && trim($data['msg'])) { ?>
            <span class="error"><?php echo $data['msg'];?></span>
            <?php } else { ?>

             <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
                 <div class="form-group">
                     <div class="col-md-12">
                         <?php echo $plan->description;?>
                     </div>

                     <div class="col-md-12">
                         <div style="font-weight: bold">
                             <?php echo $this->Language['price'].":"; ?> 
                                 <span id="charged_amt" data-amount="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?></span>
                                 <span id="discount_charged_amt" style="display: none;">
                                 <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$amount;?></span>
                                 <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                                 </span>
                         </div>
                     </div>
                 </div>
                 <div class="form-group ">
                   <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                     <div class="col-sm-6 pull-left">
                       <div class="input-group input-group-sm">
                          <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                          <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                          <span class="input-group-btn">
                          <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                          </span>
                       </div>
                       <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                       <div id="valid_coupon_suc" class="has-success" style="display: none;">
                          <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                       </div>
                     </div>
                     <div class="col-md-6">
                         <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                      </div>
                 <?php } else { ?>
                     <div class="col-md-12">
                         <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                      </div>
                 <?php } ?>
                  <div class="clear"></div>
               </div>
            </form>
            <?php } ?>
         </div>
   </div>
<?php }
    include('ppv_card_detail.php');
} ?>
    

<script type="text/javascript">
    var action = 'ppvpayment';
    var btn = '<?php echo $this->Language['btn_paynow']; ?>';
    var is_coupon_exists = 0;
    <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
            is_coupon_exists = 1;
    <?php }?>
    
    $(document).ready(function () {
        getCardDetail();
        isCouponExist();
        
        $('input').attr('autocomplete', 'off');
        $('form').attr('autocomplete', 'off');
    });
    
    $(document).click(function() {
        $(".popover").hide();
    });

    $(".popover").click(function(event) {
        event.stopPropagation();
    });
    
</script>

<script type="text/javascript" src="<?php echo $this->siteurl;?>/common/js/action.js"></script>
<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php 
$payment_gateway_str = '';
if(isset($this->PAYMENT_GATEWAY) && count($this->PAYMENT_GATEWAY) > 1){
    $payment_gateway_str = implode(',', $this->PAYMENT_GATEWAY);
    foreach ($this->PAYMENT_GATEWAY as $gateway_code){
        
    ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
    <?php }}else{
if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
    $payment_gateway_str = $this->PAYMENT_GATEWAY[$gateway_code];?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
<?php } }?>

<input type="hidden" id="payment_gateway_str" value="<?php echo $payment_gateway_str;?>">