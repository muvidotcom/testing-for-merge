<!-- PPV Plan Modal -->
<div class="modal-dialog" id="ppvModalMain">
   
    <!-- For both single and multipart content when bundle is enabled-->
   
   <div class="modal-content" id="price_detail" style="position: relative;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title"> <?php echo $this->Language['voucher_will_be_applied_on'].' '. ucfirst($films['name']);?></h4>
      </div>
      <div  id="loader-ppv" style="margin-left: 250px;text-allign:center;display: none">
         <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
         <span class="sr-only"><?php echo $this->Language['loding']; ?></span>
      </div>
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
               <span class="error" id="plan_error"></span>
               <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
                   
                   <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) == 3  && $is_ppv_bundle == 0) { ?>
                        <?php if (isset($data['min_season']) && intval($data['min_season'])) { ?>
                        <?php
                            $is_show_checked = $is_season_checked = $is_episode_checked = 0;
                            if (isset($data['is_show']) && intval($data['is_show'])) {
                                $is_show_checked =1;
                            } else if (isset($data['is_season']) && intval($data['is_season'])) {
                                $is_season_checked = 1;
                            } else if (isset($data['is_episode']) && intval($data['is_episode'])) {
                                $is_episode_checked = 1;
                            }
                        ?>
                   
                   <?php if (isset($data['is_show']) && intval($data['is_show'])) { ?>
                        <div class="form-group">
                           <div class="col-md-10">
                              <label style="font-weight: normal">
                                  <input type="radio" data-movie_id="<?php echo $films['id'];?>" name="data[plan]" <?php if (intval($is_show_checked)) { ?>checked="checked" <?php } ?> value="show" id="showtext"  />
                              <?php echo $this->Language['entire_show'].":"; ?>
                              </label>
                           </div>
                           
                        </div>
                        <?php } ?>

                        <?php if (isset($data['is_season']) && intval($data['is_season']) && intval($data['min_season'])) { ?>
                        <div class="form-group">
                           <div class="col-md-3">
                              <label style="font-weight: normal">
                              <input type="radio" name="data[plan]" <?php if (intval($is_season_checked)) { ?>checked="checked" <?php } ?> value="season" id="seasontext"/>
                              <?php echo $this->Language['season'].":"; ?>
                              </label>
                           </div>
                           <div class="col-md-7">
                              <select class="form-control" name="data[season]" id="seasonval" onchange="setSeason(this);">
                                 <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                                 <option value="<?php echo $i;?>"><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           
                           <div class="clearfix"></div>
                        </div>
                        <?php } ?>

                        <?php if (isset($data['is_episode']) && intval($data['is_episode']) && intval($data['min_season'])) { ?>
                        <div class="form-group">
                           <div class="col-md-3">
                              <label style="font-weight: normal">
                              <input type="radio" name="data[plan]" <?php if (intval($is_episode_checked)) { ?>checked="checked" <?php } ?> value="episode" id="episodetext"/>
                              <?php echo $this->Language['episode'].":"; ?>
                              </label>
                           </div>
                           <div class="col-md-3">
                              <select class="form-control" name="data[season]" id="seasonval1" onchange="showEpisode(this);">
                                 <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                                 <option value="<?php echo $i;?>"><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="col-md-4 ">
                              <select class="form-control" name="data[episode]" id="episodeval" onchange="setEpisode(this);">
                                 <?php foreach ($data['episodes'] as $key => $value) { ?>
                                 <option data-episode_id="<?php echo $value['id'];?>" data-episode="<?php echo $value['episode_title'];?>" value="<?php echo $value['embed_id'];?>"><?php echo $value['episode_title'];?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           
                           <div class="clearfix"></div>
                        </div>
                   
                        <div class="form-group">
                            <div class="col-md-12">
                                <div style="font-weight: bold">
                                    <?php echo $this->Language['voucher_applied_on_for_download'].":"; ?> 
                                        <span id="content_show" ><?php echo ucfirst($films['name']);?></span>
                                        
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <?php } else { ?>
                            <span class="error"><?php echo $this->Language['no_video']; ?></span>
                        <?php 
                        
                        }
                   }
                        
                        ?>
                       <div class="form-group ">
                           <br>
                        <div class="col-sm-6 pull-left">
                            <input type="text" class="form-control" name="data[guest_name]" id="guest_name" placeholder="Name">
                            <div id="name_error" style="color:red;font-size:11px;display: none"></div>
                        </div>
                           <div class="col-sm-6 pull-left">
                            <input type="text" class="form-control" name="data[guest_email]" id="guest_email" placeholder="Email">
                            <div id="email_error" style="color:red;font-size:11px;display:none"></div>
                        </div>
                       </div>
                            
                  <div class="form-group ">
                        <br/>
                        <div class="col-sm-6 pull-left">
                          <div class="input-group input-group-sm">
                             <input type="text" class="form-control" name="data[voucher_code]" id="voucher" placeholder="<?php echo $this->Language['voucher_code']; ?>">
                             <input type="hidden" name="data[voucher_use]" value="0" id="voucher_use" />
                             <input type="hidden" name="data[content]" value="<?php echo $content;?>" id="voucher_content" />
                             <input type="hidden" name="data[user_id]" value="<?php echo $user_id;?>" id="user_id" />
                             <input type="hidden" name="data[movie_id]" id="ppvmovie_id" value="<?php if (isset($data['movie_id']) && trim($data['movie_id'])) { echo $data['movie_id'];} ?>" />
                             <input type="hidden" name="data[type]" value="<?php echo $type;?>" id="c_type" />
                             <span class="input-group-btn">
                             <button type="button" class="btn btn-info btn-flat" id="voucher_btn" onclick="validateVoucher();"><?php echo $this->Language['btn_apply']; ?></button>
                             </span>
                          </div>
                          <div id="invalid_coupon_error" style="color:red;font-size:11px !important;display: none;padding-top: 7px !important;"></div>
                          <div id="valid_coupon_suc" class="has-success" style="display: none;">
                             <label for="inputSuccess" class="control-label" style="color: #4da30c !important;font-weight: 500 !important;font-size:11px !important;text-align: left !important;"><?php echo $this->Language['success_voucher']; ?> </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <button id="btn_proceed_payment" disabled="disabled" class="btn btn-primary  pull-right" onclick="return submitVoucherForm();"><?php echo $this->Language['watch_now']; ?></button>
                        </div>
                    
                     <div class="clear"></div>
                  </div>
               </form>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
    
    <!-- Payment form-->
 
</div>
<script>
 $('#guest_email').blur(function(){
    var email_error = $('#email_error');
    var email = $('#guest_email');
    if($.trim(email.val()) != ""){
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if( !emailReg.test(email.val()) ) {
            email_error.show().html('Please enter valid email!');
            $("#btn_proceed_payment").prop("disabled", true);
        }else{
            email_error.hide().html('');
            $("#btn_proceed_payment").removeAttr("disabled");
        }
    }else{
        email_error.hide().html('');
        $("#btn_proceed_payment").removeAttr("disabled");
    }
});
 function validateVoucher(){
    var voucherCode = $.trim($("#voucher").val());
    var season_id = 0;
     var episode_id = 0;
     var movie_id = 0;
     var content_id = "";
     
    if ($("#showtext").is(':checked')) {
         purchase_type = "show";
         movie_id = $("#showtext").data('movie_id');
         content_id = movie_id;
    } else if ($("#seasontext").is(':checked')) {
        season_id = $.trim($("#seasonval").val());
        content_id = $("#showtext").data('movie_id')+":"+season_id;
        purchase_type = "season";
        season_title = $("#seasonval option:selected").text();;
    } else if ($("#episodetext").is(':checked')) {
        season_id = $.trim($("#seasonval1").val());
        episode_id = $("#episodeval option:selected").data('episode_id');
        content_id = $("#showtext").data('movie_id')+":"+season_id+":"+episode_id;
        purchase_type = "episode";
        season_title = $("#seasonval1 option:selected").text();
        episode_title = $("#episodeval option:selected").text();
    }else{
        content_id = $("#voucher_content").val();
    }
    
    var c_type = $.trim($("#c_type").val());
        if(voucherCode !== '' && content_id !== ''){
            $('#loader-ppv').show();
            $("#btn_proceed_payment").prop("disabled", true);
            $("#voucher_btn").prop("disabled", true);
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/validateVoucher";
            $.post(url, {'voucherCode': voucherCode, 'contentId':content_id, 'ctype':c_type}, function (res) {
                $('#loader-ppv').hide();
                
                $("#voucher_btn").removeAttr("disabled");
                
                if (parseInt(res.isError)) {
                    $("#voucher_use").val(0);
                    $("#valid_coupon_suc").hide();
                    $("#btn_proceed_payment").prop("disabled", true);

                    if (parseInt(res.isError) === 2) {
                        $("#free_content_success").html('').hide();
                        $("#invalid_coupon_error").show().html("<?php echo $this->Language['invalid_voucher']; ?>");
                    } else if (parseInt(res.isError) === 3) {
                        $("#free_content_success").html('').hide();
                        $("#invalid_coupon_error").show().html("<?php echo $this->Language['voucher_already_used']; ?>");
                    } else if (parseInt(res.isError) === 4) {
                        $("#invalid_coupon_error").hide();
                        $("#valid_coupon_suc").show();
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#btn_proceed_payment").attr( "onClick", "javascript: watchContent();" );
                    } else if (parseInt(res.isError) === 5) {
                        $("#invalid_coupon_error").hide();
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#valid_coupon_suc").show();
                    } else if (parseInt(res.isError) === 6) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['access_period_expired']; ?>");
                        $("#btn_proceed_payment").removeAttr("disabled");
                    } else if (parseInt(res.isError) === 7) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['watch_period_expired']; ?>");
                        $("#btn_proceed_payment").removeAttr("disabled");
                    } else if (parseInt(res.isError) === 8) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['crossed_max_limit_of_watching']; ?>");
                        $("#btn_proceed_payment").removeAttr("disabled");
                    }
                    return false;
                } else {
                
                    var email_error = $('#email_error');
                    var email = $('#guest_email');
                    if($.trim(email.val()) != ""){
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if( !emailReg.test(email.val()) ) {
                            email_error.show().html('Please enter valid email!');
                            $("#btn_proceed_payment").prop("disabled", true);
                    $("#voucher_use").val(1);
                    $("#invalid_coupon_error").html('').hide();
                            $("#valid_coupon_suc").show();
                        }else{
                            email_error.hide().html('');
                            $("#btn_proceed_payment").removeAttr("disabled");
                            $("#voucher_use").val(1);
                            $("#invalid_coupon_error").html('').hide();
                    $("#free_content_success").html('').hide();
                    $("#valid_coupon_suc").show();
                    $("#btn_proceed_payment").removeAttr("disabled"); 
                        }
                    }else{
                        email_error.hide().html('');
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#voucher_use").val(1);
                        $("#invalid_coupon_error").html('').hide();
                        $("#free_content_success").html('').hide();
                        $("#valid_coupon_suc").show();
                        $("#btn_proceed_payment").removeAttr("disabled"); 
                    }
                    
                }
            }, 'json');
        } else {
            $("#voucher_use").val(0);
            $("#valid_coupon_suc").hide();
            $("#btn_proceed_payment").prop("disabled", true);
            $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_voucher);
        }
    }
    
    
     function submitVoucherForm() {
        var voucher = $.trim($("#voucher").val());
        var guest_name = $.trim($("#guest_name").val());
        var guest_email = $.trim($("#guest_email").val());
        var user_id = $("#user_id").val();
        var season_id = 0;
        var episode_id = 0;
        var episode_cnt = 0;
        var movie_id = 0;
        var content_id = "";
        
        var purchase_type = "";
                
        var season_title = '';
        var episode_title = '';
        var guestId = '<?php echo $data['guest_id'];?>';
       if ($("#showtext").is(':checked')) {
            purchase_type = "show";
            movie_id = $("#showtext").data('movie_id');
            content_id = movie_id;
       } else if ($("#seasontext").is(':checked')) {
           season_id = $.trim($("#seasonval").val());
           content_id = $("#showtext").data('movie_id')+":"+season_id;
           purchase_type = "season";
           season_title = $("#seasonval option:selected").text();;
       } else if ($("#episodetext").is(':checked')) {
           season_id = $.trim($("#seasonval1").val());
           episode_id = $("#episodeval option:selected").data('episode_id');
           episode_cnt = $("#episodeval option:selected").val();
           content_id = $("#showtext").data('movie_id')+":"+season_id+":"+episode_id;
           purchase_type = "episode";
           season_title = $("#seasonval1 option:selected").text();
           episode_title = $("#episodeval option:selected").text();
       }else{
           content_id = $("#voucher_content").val();
       }
       
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/voucherSubscription";
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        $.post(url, {'guest_name':guest_name,'guest_email':guest_email,'voucher': voucher,'content_id': content_id, 'voucher_download':1, 'guestId':guestId}, function (res) {
            $('#loader-ppv').hide();
            
            if (parseInt(res.isError)) {
                if (parseInt(res.isError) === 2) {
                        $("#voucher_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#btn_proceed_payment").prop("disabled", true);
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_voucher);
                    } else if (parseInt(res.isError) === 3) {
                        $("#voucher_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#btn_proceed_payment").prop("disabled", true);
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.voucher_already_used);
                    }else{
                        $("#ppvModal").modal('hide');
                        window.location.href = "<?php echo Yii::app()->baseUrl.'/user/DownloadContent/vlink/'.$films['permalink']; ?>";
                    }
            }else{
                var uri = '';
                if (parseInt(season_id)) {
                    uri = uri + '/season/'+season_id;
                }
                if (episode_cnt !== 0) {
                    uri = uri + '/stream/'+episode_cnt;
                }
                if(uri != ""){
                    $("#ppvModal").modal('hide');
                    window.location.href = "<?php echo Yii::app()->baseUrl.'/user/DownloadContent/vlink/'.$films['permalink']; ?>"+uri;
                }else{
                    $("#ppvModal").modal('hide');
                    window.location.href = "<?php echo Yii::app()->baseUrl.'/user/DownloadContent/vlink/'.$films['permalink']; ?>";
                }
            }
            
        }, 'json');
    }
    
     
    function showEpisode(obj) {
        var movie_id = $("#ppvmovie_id").val();
        var season = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getEpisodes";
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        $("#voucher_btn").prop("disabled", true);
        if($('label.error').length) {
            $('label.error').hide();
        }
        
        $.post(url, {'movie_id': movie_id, 'season': season}, function (res) {
            var str = '';
            if (res.length) {
                for (var i in res) {
                    str = str + '<option data-episode_id="'+res[i].id+'" data-episode="'+res[i].episode_title+'" value="'+res[i].embed_id+'">'+res[i].episode_title+'</option>';
                }
            }
            $("#episodeval").html(str);
            $('#loader-ppv').hide();
            $("#voucher_btn").removeAttr("disabled");

            var episode = $('#episodeval option:selected').data('episode');
            $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval1").val()+"-"+episode);
            var content = <?php echo $films['id']?>+":"+$("#seasonval").val()+":"+$('#episodeval option:selected').data('episode_id');
            $("#content_id_multi").val(content);
        }, 'json');
    }
    
    $(document).ready(function(){
        $("#showtext").click(function(){
            $("#content_show").html("<?php echo $films['name'];?>");
            var content = $('#showtext').data('movie_id');
            $("#content_id_multi").val(content);
        });
        $("#seasontext").click(function(){
            $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval").val());
            var content = <?php echo $films['id']?>+":"+$("#seasonval").val();
            $("#content_id_multi").val(content);
        });
        $("#seasonval").change(function(){
            if($('#seasontext').is(':checked')){
                $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval").val());
                var content = <?php echo $films['id']?>+":"+$("#seasonval").val();
                $("#content_id_multi").val(content);
            }
        });
        
        $("#episodetext").click(function(){
            var episode = $('#episodeval option:selected').data('episode');
            $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval1").val()+"-"+episode);
            var content = <?php echo $films['id']?>+":"+$("#seasonval").val()+":"+$('#episodeval option:selected').data('episode_id');
            $("#content_id_multi").val(content);
        });
        $("#seasonval1").change(function(){
            if($('#episodetext').is(':checked')){ 
                var episode = $('#episodeval option:selected').data('episode');
                $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval1").val()+"-"+episode);
                var content = <?php echo $films['id']?>+":"+$("#seasonval").val()+":"+$('#episodeval option:selected').data('episode_id');
                $("#content_id_multi").val(content);
            }
        });
        $("#episodeval").change(function(){
            if($('#episodetext').is(':checked')){ 
                var episode = $('#episodeval option:selected').data('episode');
                $("#content_show").html("<?php echo $films['name']."-Session-";?>"+$("#seasonval1").val()+"-"+episode);
                var content = <?php echo $films['id']?>+":"+$("#seasonval").val()+":"+$('#episodeval option:selected').data('episode_id');
                $("#content_id_multi").val(content);
            }
        });
    });
    
    </script>