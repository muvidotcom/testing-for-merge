<style>
    .voucher_success{
        font-weight: normal;
        font-size: 14px;
    }
    
</style>
<!-- PPV Plan Modal -->
<input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
<input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
<input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
<input type="hidden" value="<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>" id = "content_types_id" />

<div id="price_detail" style="position: relative;">
    <div class="col-md-12">
        <span class="error" id="plan_error"></span>
        <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
            <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) == 3  && $is_ppv_bundle == 0) { ?>
                 <?php if (isset($data['min_season']) && intval($data['min_season'])) { ?>
                 <?php
                     $is_show_checked = $is_season_checked = $is_episode_checked = 0;
                     if (isset($data['is_show']) && intval($data['is_show'])) {
                         $is_show_checked =1;
                     } else if (isset($data['is_season']) && intval($data['is_season'])) {
                         $is_season_checked = 1;
                     } else if (isset($data['is_episode']) && intval($data['is_episode'])) {
                         $is_episode_checked = 1;
                     }
                 ?>
            <?php if (isset($data['is_show']) && intval($data['is_show'])) { ?>
                 <div class="form-group">
                    <div class="col-md-10">
                       <label style="font-weight: normal">
                           <input <?php if(in_array($films['id'],$free_data_show)){?>data-freeshow='1'<?php }else{?> data-freeshow='0'<?php }?> type="radio" data-movie_id="<?php echo $films['id'];?>" name="data[plan]" <?php if (intval($is_show_checked)) { ?>checked="checked" <?php } ?> value="show" id="showtext"  />
                       <?php echo $this->Language['entire_show']; ?>
                       </label>
                    </div>

                 </div>
                 <?php } ?>

                 <?php if (isset($data['is_season']) && intval($data['is_season']) && intval($data['min_season'])) { ?>
                    <input type="hidden" id="mov_id" value="<?php echo $films['id'];?>">
                 <div class="form-group">
                    <div class="col-md-3">
                       <label style="font-weight: normal">
                       <input type="radio" name="data[plan]" <?php if (intval($is_season_checked)) { ?>checked="checked" <?php } ?> value="season" id="seasontext"/>
                       <?php echo $this->Language['season'].":"; ?>
                       </label>
                    </div>
                    <div class="col-md-7">
                       <select class="form-control" name="data[season]" id="seasonval">
                          <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                          <option <?php if(in_array($i,$free_data_season)){?>data-freeseason1='1'<?php }else{?> data-freeseason1='0'<?php }?> value="<?php echo $i;?>" <?php if((int)@$season == $i){ echo "selected";} ?>><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                          <?php } ?>
                       </select>
                    </div>

                    <div class="clearfix"></div>
                 </div>
                 <?php } ?>

                 <?php if (isset($data['is_episode']) && intval($data['is_episode']) && intval($data['min_season'])) { ?>
                    <input type="hidden" id="mov_id" value="<?php echo $films['id'];?>">
                 <div class="form-group">
                    <div class="col-md-3">
                       <label style="font-weight: normal">
                       <input type="radio" name="data[plan]" <?php if (intval($is_episode_checked)) { ?>checked="checked" <?php } ?> value="episode" id="episodetext"/>
                       <?php echo $this->Language['episode'].":"; ?>
                       </label>
                    </div>
                    <div class="col-md-3">
                       <select class="form-control" name="data[season]" id="seasonval1" onchange="showEpisode(this);">
                          <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                          <option <?php if(in_array($i,$free_data_season)){?>data-freeseason2='1'<?php }else{?> data-freeseason2='0'<?php }?> value="<?php echo $i;?>" <?php if((int)@$data['series_number'] == $i){ echo "selected";} ?>><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                          <?php } ?>
                       </select>
                    </div>
                    <div class="col-md-4 ">
                       <select class="form-control" name="data[episode]" id="episodeval">
                          <?php foreach ($data['episodes'] as $key => $value) { ?>
                          <option <?php if(in_array($value['id'],$free_data_episode)){?>data-freeepisode='1'<?php }else{?> data-freeepisode='0'<?php }?> data-episode_id="<?php echo $value['id'];?>" data-episode="<?php echo $value['episode_title'];?>" value="<?php echo $value['embed_id'];?>"><?php echo $value['episode_title'];?></option>
                          <?php } ?>
                       </select>
                    </div>

                    <div class="clearfix"></div>
                 </div>
                <?php } ?>
                 <div class="form-group">
                     <div class="col-md-12">
                         <div style="font-weight: bold">
                             <?php echo $this->Language['voucher_applied_on'].":"; ?> 
                                 <span id="content_show" ><?php echo ucfirst($films['name']);?></span>

                         </div>
                     </div>
                 </div>
                 
                 <div class="clearfix"></div>
                 <?php } else { ?>
                     <span class="error"><?php echo $this->Language['no_video']; ?></span>
                 <?php 

                 }
            }

                 ?>


           <div class="form-group ">
                 <br/>
                 <div class="col-sm-6 pull-left">
                   <div class="input-group input-group-sm">
                      <input type="text" class="form-control" name="data[voucher_code]" id="voucher" placeholder="<?php echo $this->Language['voucher_code']; ?>">
                      <input type="hidden" name="data[voucher_use]" value="0" id="voucher_use" />
                      <input type="hidden" name="data[content]" value="<?php echo $content;?>" id="voucher_content" />
                      <input type="hidden" name="data[user_id]" value="<?php echo $user_id;?>" id="user_id" />
                      <input type="hidden" name="data[movie_id]" id="ppvmovie_id" value="<?php if (isset($data['movie_id']) && trim($data['movie_id'])) { echo $data['movie_id'];} ?>" />
                      <input type="hidden" name="data[type]" value="<?php echo $type;?>" id="c_type" />
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" id="voucher_btn" onclick="validateVoucher();"><?php echo $this->Language['btn_apply']; ?></button>
                      </span>
                   </div>
                   <div id="invalid_coupon_error" style="color:red;font-size:11px !important;display: none;padding-top: 7px !important;"></div>
                   <div id="free_content_success" style="color:green;font-size:13px;display: none"></div>
                   <div id="valid_coupon_suc" class="has-success" style="display: none;">
                       <label for="inputSuccess" class="control-label voucher_success" style="font-size:11px !important;font-weight: 500 !important;color: #4da30c !important; text-align: left !important;" ><?php echo $this->Language['success_voucher']; ?> </label>
                   </div>
                 </div>
                 <div class="col-md-6">
                     <button id="btn_proceed_payment2" disabled="disabled" class="btn btn-primary  pull-right" onclick="return submitVoucherForm();"><?php echo($data['is_adv'] == 1)? $this->Language['reedem_watch']:$this->Language['reedem_and_watch']; ?></button>
                 </div>

              <div class="clear"></div>
           </div>
        </form>
     </div>
    <?php 
    $timeuniqueid=time().uniqid();
    $_SESSION['showconfirmuniqueid']=$timeuniqueid ?>
    <input type="hidden" value="<?php echo $timeuniqueid;?>" id="uniquesessionid" >
</div>
<script type="text/javascript">
 var trans_season = "<?php echo $this->Language['season']; ?>";
 function validateVoucher(){
    var voucherCode = $.trim($("#voucher").val());
    var season_id = 0;
     var episode_id = 0;
     var movie_id = 0;
     var content_id = "";
     
    if ($("#showtext").is(':checked')) {
         purchase_type = "show";
         movie_id = $("#showtext").data('movie_id');
         content_id = movie_id;
    } else if ($("#seasontext").is(':checked')) {
        season_id = $.trim($("#seasonval").val());
        content_id = $("#mov_id").val()+":"+season_id;
        purchase_type = "season";
        season_title = $("#seasonval option:selected").text();
    } else if ($("#episodetext").is(':checked')) {
        season_id = $.trim($("#seasonval1").val());
        episode_id = $("#episodeval option:selected").data('episode_id');
        content_id = $("#mov_id").val()+":"+season_id+":"+episode_id;
        purchase_type = "episode";
        season_title = $("#seasonval1 option:selected").text();
        episode_title = $("#episodeval option:selected").text();
    }else{
        content_id = $("#voucher_content").val();
    }
    
    var c_type = $.trim($("#c_type").val());
        if(voucherCode !== '' && content_id !== ''){
            $('#loader-ppv').show();
            $("#btn_proceed_payment2").prop("disabled", true);
            $("#voucher_btn").prop("disabled", true);
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/validateVoucher";
            $.post(url, {'voucherCode': voucherCode, 'contentId':content_id, 'ctype':c_type}, function (res) {
                $('#loader-ppv').hide();
                $("#voucher_btn").removeAttr("disabled");
                $("#btn_proceed_payment2").show();
                
                if (parseInt(res.isError)) {
                    $("#voucher_use").val(0);
                    $("#valid_coupon_suc").hide();
                    $("#btn_proceed_payment2").prop("disabled", true);

                    if (parseInt(res.isError) === 2) {
                        $("#free_content_success").html('').hide();
                        $("#invalid_coupon_error").show().html("<?php echo $this->Language['invalid_voucher']; ?>");
                    } else if (parseInt(res.isError) === 3) {
                        $("#free_content_success").html('').hide();
                        $("#invalid_coupon_error").show().html("<?php echo $this->Language['voucher_already_used']; ?>");
                    } else if (parseInt(res.isError) === 4) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['free_content']; ?>");
                        $("#btn_proceed_payment2").removeAttr("disabled");
                        $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                    } else if (parseInt(res.isError) === 5) {
                        $("#invalid_coupon_error").hide();
                        $("#btn_proceed_payment2").removeAttr("disabled");
                        $("#free_content_success").show().html("<?php echo $this->Language['already_purchased_content']; ?>");
                    } else if (parseInt(res.isError) === 6) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['access_period_expired']; ?>");
                        $("#btn_proceed_payment2").removeAttr("disabled");
                    } else if (parseInt(res.isError) === 7) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['watch_period_expired']; ?>");
                        $("#btn_proceed_payment2").removeAttr("disabled");
                    } else if (parseInt(res.isError) === 8) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html("<?php echo $this->Language['crossed_max_limit_of_watching']; ?>");
                        $("#btn_proceed_payment2").removeAttr("disabled");
                    } else if (parseInt(res.isError) === 9) {
                        $("#invalid_coupon_error").hide();
                        $("#free_content_success").show().html(res.message);
                        $("#btn_proceed_payment2").removeAttr("disabled");
                    }
                    return false;
                } else {
                    $("#voucher_use").val(1);
                    $("#invalid_coupon_error").html('').hide();
                    $("#free_content_success").html('').hide();
                    $("#valid_coupon_suc").show();
                    $("#btn_proceed_payment2").attr( "onClick", "return submitVoucherForm();" );
                    $("#btn_proceed_payment2").removeAttr("disabled"); 
                    
                }
            }, 'json');
        } else {
            $("#voucher_use").val(0);
            $("#valid_coupon_suc").hide();
            $("#free_content_success").html('').hide();
            $("#btn_proceed_payment2").prop("disabled", true);
            $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_voucher);
        }
    }
    
    function watchContent(){
        var season_id=0;
        var episode_cnt=0;
        if ($("#seasontext").is(':checked')) {
            season_id = $.trim($("#seasonval").val());
        } else if ($("#episodetext").is(':checked')) {
            season_id = $.trim($("#seasonval1").val());
            episode_cnt = $("#episodeval option:selected").val();
        }
        var uri = '';
        if (parseInt(season_id)) {
            uri = uri + '/season/'+season_id;
        }
        if (episode_cnt !== 0) {
            uri = uri + '/stream/'+episode_cnt;
        }
		
		var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
		var permalink = '<?php echo $films['permalink']; ?>';
        if(uri !== ""){
			window.location.href = baseUrl+"/"+ Player_Page +"/"+permalink+uri;
        }else{
			window.location.href = baseUrl+"/"+ Player_Page +"/"+permalink;
        }
    }
    
     function submitVoucherForm() {
        var voucher = $.trim($("#voucher").val());
        var user_id = $("#user_id").val();
        var season_id = 0;
        var episode_id = 0;
        var episode_cnt = 0;
        var movie_id = 0;
        var content_id = "";
        
        var purchase_type = "";
                
        var season_title = '';
        var episode_title = '';

       if ($("#showtext").is(':checked')) {
            purchase_type = "show";
            movie_id = $("#showtext").data('movie_id');
            content_id = movie_id;
       } else if ($("#seasontext").is(':checked')) {
           season_id = $.trim($("#seasonval").val());
           content_id = $("#mov_id").val()+":"+season_id;
           purchase_type = "season";
           season_title = $("#seasonval option:selected").text();;
       } else if ($("#episodetext").is(':checked')) {
           season_id = $.trim($("#seasonval1").val());
           episode_id = $("#episodeval option:selected").data('episode_id');
           episode_cnt = $("#episodeval option:selected").val();
           content_id = $("#mov_id").val()+":"+season_id+":"+episode_id;
           purchase_type = "episode";
           season_title = $("#seasonval1 option:selected").text();
           episode_title = $("#episodeval option:selected").text();
       }else{
           content_id = $("#voucher_content").val();
       }
       var download_type = '<?php echo (isset($data['download_type'])) ? $data['download_type'] : ''; ?>';
       var is_preorder_content = $("#is_preorder_content").val();
       
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/voucherSubscription";
        $('#loader-ppv').show();
        $("#btn_proceed_payment2").prop("disabled", true);
        $.post(url, {'voucher': voucher,'content_id': content_id,'user_id': user_id, 'is_preorder_content': is_preorder_content}, function (res) {
            $('#loader-ppv').hide();
            
            if (parseInt(res.isError)) {
                $("#voucher_use").val(0);
                $("#valid_coupon_suc").hide();
                $("#btn_proceed_payment2").prop("disabled", true);
                        
                if (parseInt(res.isError) === 2) {
                    $("#free_content_success").hide();
                    $("#invalid_coupon_error").show().html("<?php echo $this->Language['invalid_voucher']; ?>");
                } else if (parseInt(res.isError) === 3) {
                    $("#free_content_success").hide();
                    $("#invalid_coupon_error").show().html("<?php echo $this->Language['voucher_already_used']; ?>");
                } else if (parseInt(res.isError) === 4) {
                    $("#invalid_coupon_error").hide();
                    $("#free_content_success").show().html("<?php echo $this->Language['free_content']; ?>");
                } else if (parseInt(res.isError) === 5) {
                    $("#invalid_coupon_error").hide();
                    $("#free_content_success").show().html("<?php echo $this->Language['already_purchased_content']; ?>");
                } else if (parseInt(res.isError) === 6) {
                    $("#invalid_coupon_error").hide();
                    $("#free_content_success").show().html("<?php echo $this->Language['access_period_expired']; ?>");
                } else if (parseInt(res.isError) === 7) {
                    $("#invalid_coupon_error").hide();
                    $("#free_content_success").show().html("<?php echo $this->Language['watch_period_expired']; ?>");
                } else if (parseInt(res.isError) === 8) {
                    $("#invalid_coupon_error").hide();
                    $("#free_content_success").show().html("<?php echo $this->Language['crossed_max_limit_of_watching']; ?>");
                }
                
            }else{
			
                if (parseInt(is_preorder_content)) {
                    window.location.href = "<?php echo Yii::app()->baseUrl; ?>";
                } else {
					var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
					var permalink = '<?php echo $films['permalink']; ?>';
        if ($("#showtext").is(':checked')) {
            var uniquesessionid=$.trim($("#uniquesessionid").val());
						window.location.href = baseUrl+"/"+permalink+"?unique_id="+uniquesessionid;
           return false;
       } else if ($("#seasontext").is(':checked')) {
           season_id = $.trim($("#seasonval").val());
           var uniquesessionid=$.trim($("#uniquesessionid").val());
					   window.location.href = baseUrl+"/"+permalink+"?unique_id="+uniquesessionid+"&season_id="+season_id;
           return false;   
                   } else if ($("#episodetext").is(':checked')) {
                       season_id = $.trim($("#seasonval").val());
                       episode_id = $("#episodeval option:selected").data('episode_id');
                       episode_cnt = $("#episodeval option:selected").val();
                       episode_title = $("#episodeval option:selected").text();
                       var uniquesessionid=$.trim($("#uniquesessionid").val());
                       window.location.href = baseUrl+"/"+permalink+"?unique_id="+uniquesessionid+"&season_id="+season_id+"&episode_id="+episode_id+"&embed_id="+episode_cnt+"&episode="+episode_title;
                       return false;
                    }else{
                       var uniquesessionid=$.trim($("#uniquesessionid").val());
                       window.location.href = baseUrl+"/"+permalink+"?unique_id="+uniquesessionid;
                       return false;
       }
                    var uri = '';
                    if (parseInt(season_id)) {
                        uri = uri + '/season/'+season_id;
                    }
                    if (episode_cnt !== 0) {
                        uri = uri + '/stream/'+episode_cnt;
                    }
                    if(uri !== ""){
                        if ((download_type == 1) || (download_type == 2)) {
                                $("#ppvModal").modal('hide');
							window.location.href = baseUrl+"/user/DownloadContent/vlink/"+permalink+uri;
                    }else{
							window.location.href = baseUrl+"/"+ Player_Page +"/"+permalink+uri;
                    }
                    }else{
                        if ((download_type == 1) || (download_type == 2)) {
                                $("#ppvModal").modal('hide');
							window.location.href = baseUrl+"/user/DownloadContent/vlink/"+permalink;
                        }else{
							window.location.href = baseUrl+"/"+ Player_Page +"/"+permalink;
                }
            }
                }
            }
            
        }, 'json');
    }
    
     
    function showEpisode(obj) {
        var movie_id = $("#ppvmovie_id").val();
        var season = $(obj).val();
        var free_episode_array = JSON.parse('<?php echo JSON_encode($free_data_episode);?>');
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getEpisodes";
        $('#loader-ppv').show();
        $("#voucher").attr("disabled", true);
        $("#voucher_btn").attr("disabled", true);
        $("#btn_proceed_payment2").attr("disabled", true);
        $("#invalid_coupon_error").html("");
        $("#valid_coupon_suc").hide();
        $("#voucher").val("");
        if($('label.error').length) {
            $('label.error').hide();
        }
        
        $.post(url, {'movie_id': movie_id, 'season': season, 'free_episode_array':free_episode_array}, function (res) {
            var str = '';
            if (res.length) {
                for (var i in res) {
                    str = str + '<option data-freeepisode="'+res[i].free_episode+'" data-episode_id="'+res[i].id+'" data-episode="'+res[i].episode_title+'" value="'+res[i].embed_id+'">'+res[i].episode_title+'</option>';
                }
            }
            $("#episodeval").html(str);
            $('#loader-ppv').hide();

            var episode = $('#episodeval option:selected').data('episode');
            if($("#episodetext").is(':checked')) {
                var free_season = $('#seasonval1 option:selected').data('freeseason2');
                var freeepisode = $('#episodeval option:selected').data('freeepisode');
                $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval1").val()+"-"+episode);
                var content = <?php echo $films['id']?>+":"+$("#seasonval1").val()+":"+$('#episodeval option:selected').data('episode_id');
                $("#content_id_multi").val(content);
                $("#voucher").removeAttr("disabled");
                $("#voucher_btn").removeAttr("disabled");
                if(freeepisode == 1 || free_season == 1){
                    $("#btn_proceed_payment2").removeAttr("disabled");
                    $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                    $("#voucher").attr("disabled", true);
                    $("#voucher_btn").attr("disabled", true);
            }
            }else if($("#seasontext").is(':checked')){
                var free_season = $('#seasonval option:selected').data('freeseason1');
                $("#voucher").removeAttr("disabled");
                $("#voucher_btn").removeAttr("disabled");
                $("#btn_proceed_payment2").attr("disabled", true);
                $("#invalid_coupon_error").html("");
                $("#valid_coupon_suc").hide();
                $("#voucher").val("");
                if(free_season == 1){
                    $("#btn_proceed_payment2").removeAttr("disabled");
                    $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                    $("#voucher").attr("disabled", true);
                    $("#voucher_btn").attr("disabled", true);
                }
            }else{
                $("#voucher").removeAttr("disabled");
                $("#voucher_btn").removeAttr("disabled");
            }
            
        }, 'json');
    }
    
    $(document).ready(function(){
    
        if($("#showtext").is(':checked')) {
            $("#content_show").html("<?php echo $films['name'];?>");
            var content = $('#showtext').data('movie_id');
            $("#content_id_multi").val(content);
            
        }
        
        $("#showtext").click(function(){
            var freeshow = $('#showtext').data('freeshow');
            $("#content_show").html("<?php echo $films['name'];?>");
            var content = $('#showtext').data('movie_id');
            $("#content_id_multi").val(content);
            $("#voucher").removeAttr("disabled");
            $("#voucher_btn").removeAttr("disabled");
            $("#btn_proceed_payment2").attr("disabled", true);
            $("#invalid_coupon_error").html("");
            $("#valid_coupon_suc").hide();
            $("#voucher").val("");
            if(freeshow == 1){
                $("#btn_proceed_payment2").removeAttr("disabled");
                $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                $("#voucher").attr("disabled", true);
                $("#voucher_btn").attr("disabled", true);
            }
        });
    
        if($("#seasontext").is(':checked')) {
            $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval").val());
            var content = <?php echo $films['id']?>+":"+$("#seasonval").val();
            $("#content_id_multi").val(content);
            
        }
        
        $("#seasontext").click(function(){
            var free_season = $('#seasonval option:selected').data('freeseason1');
            
            $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval").val());
            var content = <?php echo $films['id']?>+":"+$("#seasonval").val();
            $("#content_id_multi").val(content);
            $("#voucher").removeAttr("disabled");
            $("#voucher_btn").removeAttr("disabled");
            $("#btn_proceed_payment2").attr("disabled", true);
            $("#invalid_coupon_error").html("");
            $("#valid_coupon_suc").hide();
            $("#voucher").val("");
            if(free_season == 1){
                $("#btn_proceed_payment2").removeAttr("disabled");
                $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                $("#voucher").attr("disabled", true);
                $("#voucher_btn").attr("disabled", true);
            }
        });
        $("#seasonval").change(function(){
            if($('#seasontext').is(':checked')){
                var free_season = $('#seasonval option:selected').data('freeseason1');
                $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval").val());
                var content = <?php echo $films['id']?>+":"+$("#seasonval").val();
                $("#content_id_multi").val(content);
                $("#voucher").removeAttr("disabled");
                $("#voucher_btn").removeAttr("disabled");
                $("#btn_proceed_payment2").attr("disabled", true);
                $("#invalid_coupon_error").html("");
                $("#valid_coupon_suc").hide();
                $("#voucher").val("");
                if(free_season == 1){
                    $("#btn_proceed_payment2").removeAttr("disabled");
                    $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                    $("#voucher").attr("disabled", true);
                    $("#voucher_btn").attr("disabled", true);
            }
            }
        });
        
        if($("#episodetext").is(':checked')) {
            var episode = $('#episodeval option:selected').data('episode');
            $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval1").val()+"-"+episode);
            var content = <?php echo $films['id']?>+":"+$("#seasonval1").val()+":"+$('#episodeval option:selected').data('episode_id');
            $("#content_id_multi").val(content);
            
        }
        $("#episodetext").click(function(){
            var free_season = $('#seasonval1 option:selected').data('freeseason2');
            var freeepisode = $('#episodeval option:selected').data('freeepisode');
            var episode = $('#episodeval option:selected').data('episode');
            $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval1").val()+"-"+episode);
            var content = <?php echo $films['id']?>+":"+$("#seasonval1").val()+":"+$('#episodeval option:selected').data('episode_id');
            $("#content_id_multi").val(content);
            $("#voucher").removeAttr("disabled");
            $("#voucher_btn").removeAttr("disabled");
            $("#btn_proceed_payment2").attr("disabled", true);
            $("#invalid_coupon_error").html("");
            $("#valid_coupon_suc").hide();
            $("#voucher").val("");
            if(freeepisode == 1 || free_season == 1){
                $("#btn_proceed_payment2").removeAttr("disabled");
                $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                $("#voucher").attr("disabled", true);
                $("#voucher_btn").attr("disabled", true);
            }
        });
        $("#seasonval1").change(function(){
            if($('#episodetext').is(':checked')){ 
                var episode = $('#episodeval option:selected').data('episode');
                $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval1").val()+"-"+episode);
                var content = <?php echo $films['id']?>+":"+$("#seasonval1").val()+":"+$('#episodeval option:selected').data('episode_id');
                $("#content_id_multi").val(content);
                
            }
        });
        $("#episodeval").change(function(){
            if($('#episodetext').is(':checked')){ 
                var free_season = $('#seasonval1 option:selected').data('freeseason2');
                var freeepisode = $('#episodeval option:selected').data('freeepisode');
                var episode = $('#episodeval option:selected').data('episode');
                $("#content_show").html("<?php echo $films['name'];?>"+"-"+trans_season+"-"+$("#seasonval1").val()+"-"+episode);
                var content = <?php echo $films['id']?>+":"+$("#seasonval1").val()+":"+$('#episodeval option:selected').data('episode_id');
                $("#content_id_multi").val(content);
                $("#voucher").removeAttr("disabled");
                $("#voucher_btn").removeAttr("disabled");
                $("#btn_proceed_payment2").attr("disabled", true);
                $("#invalid_coupon_error").html("");
                $("#valid_coupon_suc").hide();
                $("#voucher").val("");
                if(freeepisode == 1 || free_season == 1){
                    $("#btn_proceed_payment2").removeAttr("disabled");
                    $("#btn_proceed_payment2").attr( "onClick", "javascript: watchContent();" );
                    $("#voucher").attr("disabled", true);
                    $("#voucher_btn").attr("disabled", true);
            }
            }
        });
    });
    
    </script>
    