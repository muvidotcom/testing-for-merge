<?php 
    $is_error = 0;
    if (!empty($error)) {
        $is_error = 1;
    }

    $playble_error_msg = '';
    if (isset($monetization) && !empty($monetization) && array_key_exists ( 'playble_error_msg' , $monetization )) {
        $playble_error_msg = $monetization['playble_error_msg'];
        unset($monetization['playble_error_msg']);
    }
    
    $is_on_mulitple_plan = 0;
    
    if (!$is_error && isset($monetization) && !empty($monetization) && count($monetization) > 1) {
        $is_on_mulitple_plan = 2;
    } else if (isset($monetization) && !empty($monetization) && count($monetization) == 1) {
        $is_on_mulitple_plan = 1;
    }
    $default_monetization = '';
    if ($is_on_mulitple_plan >= 1) {
        reset($monetization);
        $default_monetization = key($monetization);
    }
    ?>
<style>
    #ppvModalMain {
        width: 700px;
    }
	@media(max-width:600px){
		#ppvModalMain {width :100%;}
		#btn_proceed_payment,#btn_proceed_payment2{
			margin-top  :10px; 
		}
		.ppvModalTab {
			width: 100%;
			overflow: auto;
			height: 50px;
	}
		.ppvModalTab #tabContent {
			width: 480px;
		}
	}
</style>
<div class="modal-dialog" id="ppvModalMain">
    <input type="hidden" value="<?php echo $default_monetization; ?>" id="default_monetization">
    
   <div class="modal-content" id="monetization_detail" style="position: relative;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <?php if ($is_error) { ?>
            <h4 class="modal-title"><?php echo ucfirst($films['name']);?></h4>
         <?php } else { ?>
            <h4 class="modal-title"><?php echo ($is_on_mulitple_plan > 1) ? $this->Language['purchase_options_for'] : $this->Language['purchase']; ?> <?php echo ucfirst($films['name']);?></h4>
         <?php } ?>
            <?php if (isset($gateway_code) && trim($gateway_code == 'testgateway')) { ?>
            <h5 style=" color: red;display:none;" id="test_transaction"><?php echo $this->Language['test_transaction'];?></h5>
         <?php }  ?>
           
          <div><span class="error" id="playble_error_msg"><?php if (trim($playble_error_msg)) {echo $playble_error_msg;} ?></span></div>
      </div>
       
       <input type="hidden" id="content_ids" value="<?php if (isset($films['uniq_id']) && trim($films['uniq_id'])) { echo $films['uniq_id'];} ?>" />
       <input type="hidden" id="is_preorder_content" value="<?php echo @$is_preorder_content; ?>" />
       
       <?php 
       if (!$is_error && $is_on_mulitple_plan == 2) { ?>
	   <div class="ppvModalTab">
       <ul class="nav nav-tabs"id="tabContent">
            <?php 
            $count = 1;
            foreach ($monetization as $key => $value) {
                ?>
                <li <?php if ($count == 1){ ?>class="active"<?php } ?>><a href="#<?php echo $key;?>-tab" data-toggle="tab" onclick="getMonetizationPlans('<?php echo $key;?>')"><?php echo $value;?></a></li>
            <?php 
                $count++;
            } ?>
       </ul>
	   </div>
       <?php } ?>
       
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
                <div  id="loader-ppv" style="margin-left: 250px;text-allign:center;display: none">
                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                    <span class="sr-only"><?php echo $this->Language['loding']; ?></span>
                </div>
                
                <span class="error" id="plan_error"></span>
                <?php if ($is_error) { ?>
                     <span class="error"><?php echo $error['msg'];?></span>
                <?php } else { ?>
                <div class="tab-content">
                   <div role="tabpanel" class="tab-pane active" id="all-monetization-plan-tab"></div>
                </div>
               <?php } ?>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>

<!-- Card form which will put on payment form-->
<div id="new_card_div" style="display: none;">
   <div class="form-group">
      <div class="col-sm-6 pull-left">
         <label for="card_name"><?php echo $this->Language['text_card_name']; ?></label>
      </div>
      <div class="col-sm-6 pull-left">
         <label for="card_number"><?php echo $this->Language['text_card_number']; ?></label>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-6 pull-left">
         <input type="text" class="form-control" autocomplete="false" id="card_name" name="data[card_name]" required />
      </div>
      <div class="col-sm-6 pull-left">
         <input type="text" class="form-control" autocomplete="false" id="card_number" name="data[card_number]" required />
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 pull-left">
         <label for="exp_month"><?php echo $this->Language['selct_exp_date']; ?></label>
      </div>
      <div class="col-sm-6 pull-left">
         <label for="security_code"><?php echo $this->Language['text_security_code']; ?></label>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-6 pull-left">
         <?php
            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
            ?>
         <div class="col-sm-6" style="padding: 0">
            <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
               <option value=""><?php echo $this->Language['select_month']; ?></option>
               <?php for ($i = 1; $i <= 12; $i++) { ?>
               <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
               <?php } ?>
            </select>
            <div class="sbox-err-lbl">
               <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
            </div>
         </div>
         <div class="col-sm-6" style="padding-left: 5px;padding-right: 0;">
            <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
               <option value=""><?php echo $this->Language['select_year']; ?></option>
               <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
               <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
               <?php } ?>
            </select>
            <div class="sbox-err-lbl">
               <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>
            </div>
         </div>
      </div>
      <div class="col-sm-2 pull-left">
         <input type="password" id="" name="" style="display: none;" />
         <input type="password" class="form-control" autocomplete="false" id="security_code" name="data[security_code]" required />
      </div>
      <div class="col-sm-4 pull-left">
         <input style="padding: 6px 8px;" type="text" name="data[card_name_optional]" class="form-control" placeholder="<?php echo $this->Language['card_name_optional']; ?>" />
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="clear"></div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" style="border: none;">
            <div class="modal-title success-popup-payment"><?php echo $this->Language['thanks_card_auth_sucess']; ?></div>
         </div>
      </div>
   </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" style="border: none;">
            <div class="modal-title auth-msg"><?php echo $this->Language['auth_your_card']; ?></div>
            <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" alt="" title="" style="padding:5px;"/></div>
         </div>
      </div>
   </div>
</div>

<div id="paypalproPopup" class="modal fade" style="">
   <div class="modal-dialog" style="width: 660px;">
      <div class="modal-content">
         <div class="modal-header">
            <div class="modal-title success-popup-payment"><?php echo $this->Language['pls_wait']; ?></div>
            <div id="test_iframe" style="display:none">
               <iframe>
                  <p><?php echo $this->Language['browser_iframe_not_support']; ?></p>
               </iframe>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="ppvPricingModal" class="modal fade login-popu" data-backdrop="static" data-keyboard="false"></div>
<?php if(isset($this->IS_PCI_COMPLIANCE[$gateway_code]) && intval($this->IS_PCI_COMPLIANCE[$gateway_code]) == 1) { ?>
        <form name="pci_form" id="pci_form"></form>
<?php } ?>
<div style="display: none;" class="loader" id="payment-loading"></div>

<script type="text/javascript">
    var is_plan_error = '<?php echo $is_error;?>';
    var method = 'get'+'<?php echo $default_monetization;?>'+'Plan';
	var proceed_to_payment = '<?php echo $this->Language['btn_proceed_payment'] ?>';
    var reedem_and_watch_now = '<?php echo $this->Language['reedem_and_watch'] ?>';
    var reedem_and_pay = '<?php echo $this->Language['reedem_and_payment'] ?>';
    var reedem_watch = '<?php echo $this->Language['reedem_watch'] ?>';
    $(document).ready(function(){
        <?php if(!$not_refresh_on_close){ ?>
        $(".close").click(function(){
            if (parseInt(reload)) {
                location.reload();
            }
            reload = 0;
            $(".modal-backdrop").remove();
        });
        <?php } ?>
        if (!parseInt(is_plan_error)) {
            eval(method)();
        }
    });
    
    function getMonetizationPlans(arg) {
        if ($.trim(arg)) {
            var acton = 'get'+arg+'Plan';
            eval(acton)();
        } else {
            return false;
        } 
    }
    
    function getppvPlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").show();
        }
        $('#loader-ppv').show();
        var purchase_type = '<?php echo (isset($data['purchase_type'])) ? $data['purchase_type'] : 'show'; ?>';
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var season = 0;
        var permalink = '<?php echo $films['permalink'];?>';
        if (purchase_type === "season") {
            season = '<?php echo (isset($data['multipart_season'])) ? $data['multipart_season'] : 0; ?>';
            if(parseInt(season)) {
                permalink+='/season/'+season;
            }
        }
        
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        
        var url = HTTP_ROOT + "/user/getPpvPlan";
        
        $("#all-monetization-plan-tab").empty();
        
        $.post(url, {'movie_id': movie_id, 'season': season, 'stream_id': stream_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function getpre_orderPlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").show();
        }
        $('#loader-ppv').show();
        var purchase_type = "show";
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var permalink = '<?php echo $films['permalink'];?>';
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        var url = HTTP_ROOT + "/user/getPreOrderPlan";
        
        $("#all-monetization-plan-tab").empty();
        
        $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function getppv_bundlePlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").show();
        }
        $('#loader-ppv').show();
        var purchase_type = "show";
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var permalink = '<?php echo $films['permalink'];?>';
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        var url = HTTP_ROOT + "/user/getPpvBundlePlan";
        
        $("#all-monetization-plan-tab").empty();
        $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function getsubscription_bundlesPlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").show();
        }
        $('#loader-ppv').show();
        var purchase_type = "show";
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var permalink = '<?php echo $films['permalink'];?>';
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        var url = HTTP_ROOT + "/user/getSubscriptionBundlesPlan";
        
        $("#all-monetization-plan-tab").empty();
        $.post(url, {'movie_id': movie_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function getvoucherPlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").hide();
        }
        $('#loader-ppv').show();
        var purchase_type = '<?php echo (isset($data['purchase_type'])) ? $data['purchase_type'] : 'show'; ?>';
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var season = 0;
        var permalink = '<?php echo $films['permalink'];?>';
        var download_type = '<?php echo (isset($data['download_type'])) ? $data['download_type'] : ''; ?>';
        if (purchase_type === "season") {
            season = '<?php echo (isset($data['multipart_season'])) ? $data['multipart_season'] : 0; ?>';
            if(parseInt(season)) {
                permalink+='/season/'+season;
            }
        }
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        var url = HTTP_ROOT + "/user/getVoucherPlan";
        
        $("#all-monetization-plan-tab").empty();
        $.post(url, {'download_type':download_type, 'movie_id': movie_id, 'season': season, 'stream_id': stream_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function getcreditPlan() {
        var gt_code = '<?php echo $gateway_code; ?>';
        if( gt_code === 'testgateway'){
            $("#test_transaction").hide();
        }
        $('#loader-ppv').show();
        var purchase_type = '<?php echo (isset($data['purchase_type'])) ? $data['purchase_type'] : 'show'; ?>';
        var movie_id = $('#content_ids').val();
        var stream_id = '<?php echo (isset($data['stream_id'])) ? $data['stream_id'] : ''; ?>';
        var season = 0;
        var permalink = '<?php echo $films['permalink'];?>';
        <?php if(!empty($data['download_type'])){?>
        var download_type = <?php echo $data['download_type'];?>;
        <?php } ?>
        if (purchase_type === "season") {
            season = '<?php echo (isset($data['multipart_season'])) ? $data['multipart_season'] : 0; ?>';
            if(parseInt(season)) {
                permalink+='/season/'+season;
            }
        }
        if($.trim(stream_id)) {
            permalink+='/stream/'+stream_id;
        }
        var url = HTTP_ROOT + "/userPayment/getCreditPlan";
        
        $("#all-monetization-plan-tab").empty();
        $.post(url, {'movie_id': movie_id, 'season': season, 'stream_id': stream_id, 'purchase_type': purchase_type}, function (res) {
            if (parseInt(res) === 1) {
                var purl = HTTP_ROOT + "/"+Player_Page +"/" + permalink;
                window.location.href = purl;
            } else {
                $("#all-monetization-plan-tab").html(res);
                $("#ppvModal").modal('show');
                $('#loader-ppv').hide();
            }
        });
    }
    
    function validateCategoryForm() {
                var use_coupon = $.trim($("#coupon_use").val());
                var coupon_code = '';
                var coupon_currency_id = 0;
                if(use_coupon === "1"){
                    coupon_code =  $.trim($("#coupon").val());
                    coupon_currency_id =  $("#currency_id").val();
                }
                var payment_method =  $("#payment_method").val();
                
                var movie_id = $("#ppvmovie_id").val();
                var season_id = 0;
                var episode_id = 0;
                var isadv = 0;
                
                var is_show = '<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>';
                var is_season = '<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>';
                var is_episode = '<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>';
                var content_types_id = '<?php echo (isset($films['content_types_id']) && intval($films['content_types_id'])) ? $films['content_types_id'] : 0; ?>';
                isadv = '<?php echo (isset($data['isadv']) && intval($data['isadv'])) ? 1 : 0; ?>';
                
                var purchase_type = "";
				var is_bundle = '<?php echo (isset($data['is_ppv_bundle'])) ? $data['is_ppv_bundle'] : 0; ?>'; 

                var season_title = '';
                var episode_title = '';
                var plan = $("input[type='radio'].ppv-bundled-cls:checked").val();
                
                if ($(".showtext").is(':checked')) {
                     purchase_type = "show";
                } else if (parseInt(is_season) && $(".seasontext").is(':checked')) {
                    season_id = $.trim( $('#temp_season_id').val());
                    episode_id = 0;
                    purchase_type = "season";
                    season_title = $("#temp_season_title").val();
                    //season_title = $(".seasonval option:selected").text();
                } 
                else if (parseInt(is_episode) && $(".episodetext").is(':checked')) {
                    season_id = $.trim( $('#temp_season_id').val());
                    episode_id = $.trim( $('#temp_episode_id').val());
                    purchase_type = "episode";
                    season_title = $("#temp_season_title").val();
                    episode_title = $("#temp_episode_title").val();
                    //season_title = $(".seasonval1 option:selected").text();
                    //episode_title = $(".episodeval option:selected").text();
                }
                
                $("#ppvseason_id").val(season_id);
                $("#ppvepisode_id").val(episode_id);
				$('#is_bundle').val(is_bundle);
                var url = "<?php echo Yii::app()->baseUrl; ?>/user/isPPVSubscribed";
                $('#loader-ppv').show();
                $("#btn_proceed_payment").prop("disabled", true);
                var good_type = 'digital_payment';
                var permalink = '<?php echo $films['permalink']; ?>';
                $.post(url, {'plan': plan,'movie_id': movie_id, 'season_id': season_id, 'episode_id': episode_id, 'purchase_type': purchase_type, 'content_types_id': content_types_id, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'isadv': isadv,'good_type':good_type}, function (res) {
                    $('#loader-ppv').hide();
                    if (parseInt(res) === 1) {
                        if(parseInt(content_types_id) === 3) {
                            var uri = '';
                            if (parseInt(season_id)) {
                                uri = uri + '/season/'+season_id;
                            }
                            if (episode_id !== 0) {
                                uri = uri + '/stream/'+episode_id;
                            }
                            window.location.href = HTTP_ROOT + "/"+ Player_Page +"/" + permalink+uri;
                        } else {
                            window.location.href = HTTP_ROOT + "/"+ Player_Page +"/" + permalink;
                        }
                    } else if (parseInt(res.isAdvanceAmtZero) === 1) {
                        $('body').prepend('<div class="top_msg_bar"><div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'+res.msg+'</div></div>');
                        setTimeout(function () {
                            location.reload();
                            return false;
                        }, 2000);
                    } else {
                            if ($.trim(res.msg)) {
                                $("#btn_proceed_payment").removeAttr("disabled");
                                $("#plan_error").html(res.msg).show();
                            } else {
                                if ($("#payment_gateway").length && $("#payment_gateway").val() === 'instafeez') {
                                    var obj = new instafeez();
                                    obj.doPayment();
                                    return false;
                                }else if($("#payment_gateway").length && $("#payment_gateway").val() === 'ippayment'){
                                    $('#loader-ppv').show();
                                    $("#paynowbtn").hide();
                                    
                                    var obj = new ippayment();
                                    obj.generateSST();
                                    $("#price_detail").hide();
                                    $("#card_detail").show();
                                    var currency = $("#charged_amt").attr('data-currency');
                                    $("#price_amount").attr('data-amount', res.amount);
                                    $("#price_amount").text(currency+res.amount);
                                    if ($.trim(season_title) || $.trim(episode_title)) {
                                        var str = '<div style="color: #ccc;margin-top: 10px;">'+season_title+' '+ episode_title+'</div>';
                                        $('#title-hdrdiv').html(str);
                                    }
                                }else if(parseInt(res.is_pci_compliance) === 1 && !parseInt(res.is_hosted_ppv)){
                                    $('#loader-ppv').show();
                          
            var class_name = <?php echo $this->PAYMENT_GATEWAY[$gateway_code];?>+'()';
                                    eval ("var obj = new "+class_name);
                                    obj.processCard();
                                }else{
                                    //$("#ppvModalMain").css('width', '60%');
                                    $("#price_detail").hide();
                                    $("#card_detail").show();
                                    $("#season_id").val(res.season_id);
                                    $("#episode_id").val(res.episode_id);
                                     $("#plandetail_id").val(res.plan_id);
                                    var currency = $("#charged_amt").attr('data-currency');
                                    //$("#hdr-amnt").text(currency+res.amount);
                                    //$("#charged_amt").attr('data-amount', res.amount);
                                    //$("#charged_amt").text(currency+res.amount);
                                    //$("#dis_charged_amt").text(currency+res.amount);

                                    $("#coupon_code").val(res.coupon_code);
                                    $("#price_amount").attr('data-amount', res.amount);
                                    $("#price_amount").text(currency+res.amount);

                                    if (res.playble_error_msg.length && $.trim(res.playble_error_msg)) {
                                        $("#playble_error_msg").text(res.playble_error_msg);
                                    } else {
                                        $("#playble_error_msg").text('');
                                    }
                                    
                                    if ($.trim(season_title) || $.trim(episode_title)) {
                                        var str = '<div style="color: #ccc;margin-top: 10px;">'+season_title+' '+ episode_title+'</div>';
                                        $('#title-hdrdiv').html(str);
                                    }
                                }
                            }
                    }
                }, 'json');
    }
    function watchContent(){
        var season_id=0;
        var episode_cnt=0;
        var permalink = '<?php echo $films['permalink']; ?>';
        if ($("#seasontext").is(':checked')) {
            season_id = $.trim($("#seasonval").val());
        } else if ($("#episodetext").is(':checked')) {
            season_id = $.trim($("#seasonval1").val());
            episode_cnt = $("#episodeval option:selected").val();
        }
        var uri = '';
        if (parseInt(season_id)) {
            uri = uri + '/season/'+season_id;
        }
        if (episode_cnt !== 0) {
            uri = uri + '/stream/'+episode_cnt;
        }
        if(uri !== ""){
            window.location.href = HTTP_ROOT + "/"+ Player_Page +"/" + permalink+uri;
        }else{
            window.location.href = HTTP_ROOT + "/"+ Player_Page +"/" + permalink;
        }
    } 
    function submitForm() {
      if($('#plandetailbundles_id').val()!=''){
            subscriptionbundles_plan_id=$('#plandetailbundles_id').val();
            controllerRoot="userPayment";
        }
        else{
         controllerRoot="user";   
        }
        document.membership_form.action = HTTP_ROOT+"/"+controllerRoot+"/"+action;
        document.membership_form.submit();
        return false;
    }
    
    function validateCoupon(){
        var season_id=0;
        var episode_id=0;
        var movie_id=0;
        
        var is_season = '<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>';
        var is_episode = '<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>';
        movie_id=$.trim($("#content_ids").val());
        if (parseInt(is_season) && $(".seasontext").is(':checked')) {
            season_id = $.trim( $('#temp_season_id').val());
            episode_id = 0;
        } else if (parseInt(is_episode) && $(".episodetext").is(':checked')) {
            season_id = $.trim( $('#temp_season_id').val());
            episode_id = $.trim( $('#temp_episode_id').val());
        }
        
        var couponCode = $.trim($("#coupon").val());
        if(couponCode !== ''){
            $('#loader-ppv').show();
            $("#btn_proceed_payment").prop("disabled", true);
            $("#coupon_btn").prop("disabled", true);
            $("#paypal").prop("disabled", true);
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/validateCoupon";
            $.post(url, {'couponCode': couponCode,'season_id':season_id,'episode_id':episode_id,'movie_unq_id':movie_id,'currency_id' : $("#currency_id").val()}, function (res) {
                $('#loader-ppv').hide();
                $("#paypal").removeAttr("disabled");
                $("#btn_proceed_payment").removeAttr("disabled");
                $("#coupon_btn").removeAttr("disabled");
                if (parseInt(res.isError)) {
                    if (parseInt(res.isError) === 1) {
                        $("#coupon_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
                    } else if (parseInt(res.isError) === 2) {
                        $("#coupon_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.coupon_already_used);
                    }else if (parseInt(res.isError) === 3) {
                        $("#invalid_coupon_error").show().html('<span style="color:green">'+JSLANGUAGE.free_content+'</span>');
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#valid_coupon_suc").hide();
                        $("#btn_proceed_payment").attr( "onClick", "javascript: watchContent();" );
                    }

                    $("#charged_amt").show();
                    $("#discount_charged_amt").hide();
                    $("#discount_charged_amt_span").text(0);
                    $("#btn_proceed_payment").html(proceed_to_payment);
                } else {
                    var isppv="";
                    var isppvbundles="";
                    var isadv="";
                    $("#coupon_use").val(1);
                    $("#invalid_coupon_error").html('').hide();
                    $("#valid_coupon_suc").show();
                    isppv=$("#is_ppv").val();
                    isppvbundles=$("#is_ppvbundles").val();
                    isadv=$("#isadv").val();
                    if(isppv==1){
                    $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
                    }else if(isppvbundles==1){
                        $("#btn_proceed_payment").attr( "onClick", "javascript: return showPaymentForm();" );
                    }else if(isadv==1){
                        $("#btn_proceed_payment").attr( "onClick", "javascript: return validateCategoryForm();" );
                    }
                    var gross_amt = $("#charged_amt").attr('data-amount');
                    var currency = $("#charged_amt").attr('data-currency');
                    var discount_amount = res.discount_amount;
                    
                    var price = 0;
                    if (parseInt(res.is_cash)) {
                        $("#coupon_in_amt").text(currency+""+discount_amount);
                        price = (parseFloat(gross_amt) - parseFloat(discount_amount));
                    } else {
                        $("#coupon_in_amt").text(discount_amount+'%');
                        price = (parseFloat(gross_amt) - (parseFloat(gross_amt) * parseFloat(discount_amount)/100));
                    }
                    
                    var isprice = Math.floor(price * 100) / 100;
                    isprice = Math.round(isprice);
                            
                    if (isprice < 0) {
                        price = 0;
                        discount_amount = gross_amt;
                        if (parseInt(res.is_cash)) {
                            $("#coupon_in_amt").text(currency+""+discount_amount);
                        } else {
                            $("#coupon_in_amt").text(discount_amount+'%');
                        }
                    }
					if(isprice <= 0){
                        if(isadv == 1){
                            $("#btn_proceed_payment").html(reedem_watch);
					}else{
                            $("#btn_proceed_payment").html(reedem_and_watch_now);
					}
					}else{
						$("#btn_proceed_payment").html(reedem_and_pay);
					}
                    $("#charged_amt").hide();
                    $("#discount_charged_amt").show();
                    $("#discount_charged_amt_span").text(currency+""+price.toFixed(2));
                }
            }, 'json');
        } else {
            $("#coupon_use").val(0);
            $("#valid_coupon_suc").hide();
            $("#charged_amt").show();
            $("#discount_charged_amt").hide();
            $("#discount_charged_amt_span").text(0);
            $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
            $("#btn_proceed_payment").html(proceed_to_payment);
        }
    }
    
    function isCouponExist() {
        var id = '#coupon';
        $(id).keyup(function() {
            if($.trim($(id).val()) === ''){
                $("#charged_amt").show();
                $("#discount_charged_amt").hide();
                
                $("#coupon_use").val(0);
                $("#valid_coupon_suc").hide();
            }
        });
    }
    
    function resetCouponPrice() {
        $("#coupon").val('');
        $("#valid_coupon_suc").hide();
        $("#invalid_coupon_error").hide().html('');
        $("#charged_amt").show();
        $("#discount_charged_amt").hide();
    }
   function resetCouponFreeTrial(){
       $("#free_trail_txt").hide().html('');
        $("#subscriptionBundle_coupon").val('');
    
   }
   function backPpvForm(){
		 var default_mon = $('#default_monetization').val();
		 var callFunction = 'get'+default_mon+'Plan';
		 $('ul#tabContent li').removeClass('active');
		 $('ul#tabContent li').first().addClass('active');
		 eval(callFunction)();
	}
</script>