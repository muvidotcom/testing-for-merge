<!-- PPV Plan Modal -->
<?php
if(isset($gateways[0]) && !empty($gateways[0])){
    $can_save_card = (isset($gateways[0]['paymentgt']['can_save_card']) && intval($gateways[0]['paymentgt']['can_save_card'])) ? $gateways[0]['paymentgt']['can_save_card'] : 0;
    
    if (isset($plan) && !empty($plan)) {
        $amount = '';
        $symbol = (isset($currency->symbol) && trim($currency->symbol)) ? $currency->symbol: $currency->code.' ';

        if (isset($is_ppv_bundle) && intval($is_ppv_bundle)) {
            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                $amount =  $timeframe_prices[0]['price_for_subscribed']; 
            } else {
                $amount =  $timeframe_prices[0]['price_for_unsubscribed']; 
            }
        } else {
            if (isset($data['content_types_id']) && intval($data['content_types_id']) != 3) {
                if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                    $amount =  $price['price_for_subscribed']; 
                } else {
                    $amount =  $price['price_for_unsubscribed']; 
                }
            }
        }
                
?>
<div class="modal-dialog" id="ppvModalMain">
    <input type="hidden" value="<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>" id = "is_show" />
    <input type="hidden" value="<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>" id = "is_season" />
    <input type="hidden" value="<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>" id = "is_episode" />
    <input type="hidden" value="<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>" id = "content_types_id" />
    <input type="hidden" value="<?php echo (isset($data['isadv']) && intval($data['isadv'])) ? 1 : 0; ?>" id = "isadv" />
    <!-- For both single and multipart content when bundle is enabled-->
   <?php if (isset($is_ppv_bundle) &&  intval($is_ppv_bundle)== 1 && !empty($plan)) { ?>
   <div class="modal-content" id="price_detail" style="position: relative;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title"><?php echo $this->Language['purchase_options_for']; ?> <?php echo ucfirst($films['name']);?></h4>
      </div>
      <div  id="loader-ppv" style="margin-left: 250px;text-allign:center;display: none">
         <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
         <span class="sr-only"><?php echo $this->Language['loding']; ?></span>
      </div>
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
               <span class="error" id="plan_error"></span>
               <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
                  <div style="font-weight: bold"><?php echo $this->Language['select_bundle']; ?></div>
                  <div class="form-group">
                     <?php 
                        $dflt = 1;
                        $is_timeframe = 0;
                        $bundle_price = 0;
                        foreach ($plan as $key => $value) {
                            if ($dflt == 1) {
                                if (intval($value->is_timeframe)) {
                                    $is_timeframe = 1;
                                }
                                $bundle_price = $amount;
                            } ?>
                     <div class="col-md-10">
                        <label style="font-weight: normal">
                        <input type="radio" name="data[bundleplan]" class="ppv-bundled-cls" <?php if($dflt ==1){ ?>checked="checked" <?php } ?> value="<?php echo $value->id; ?>" onclick="getBundlePrice(this);"/>
                        <?php echo (trim($value->title)) ? $value->title : 'Default'; ?>
                        
                        <?php if (intval($value->is_advance_purchase) == 2) { ?>
                        <a href="javascript:void(0);" data-plan="<?php echo $value->id;?>" onclick="viewDetail(this);">View Detail</a>
                        <span id="ldr_<?php echo $value->id;?>" style="display:none;">
                            <img src="<?php echo Yii::app()->baseUrl; ?>/img/loading.gif" />
                        </span>
                        <div class="popover fade right in" id="popover_<?php echo $value->id;?>" style="top: -21.5px; left: 285px; width: 60%;display: none;"><!-- overflow-y: auto;-->
                            <div class="arrow" style="top: 28.4615%;"></div>
                            <h3 class="popover-title"><?php echo (trim($value->title)) ? $value->title : 'Default'; ?></h3>
                            <div class="popover-content" style="max-height:150px;"></div>
                        </div>
                        <?php } ?>
                        </label>
                     </div>
                     <div class="clearfix"></div>
                     <?php 
                        $dflt++;
                        }?>
                  </div>
                  <div class="clearfix"></div>
                  <div id="timeframe_div">
                     <?php if (intval($is_timeframe) && isset($timeframe_prices) && !empty($timeframe_prices)) { ?>
                     <div style="font-weight: bold"><?php echo $this->Language['select_timeFrame']; ?></div>
                     <div class="form-group">
                        <?php 
                        $dflt_tmfrm = 1;
                        foreach ($timeframe_prices as $key => $value) {
                           if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                               $timeframeprice =  $value['price_for_subscribed']; 
                           } else {
                               $timeframeprice =  $value['price_for_unsubscribed']; 
                           }
                           ?>
                        <div class="col-md-3">
                           <label style="font-weight: normal">
                           <input type="radio" name="data[timeframeprice]" class="ppv-bundled-tmframe-cls" <?php if($dflt_tmfrm ==1){ ?>checked="checked" <?php } ?> value="<?php echo $value['id']; ?>" data-price="<?php echo $timeframeprice;?>" onclick="setTimeFramePrice(this);"/>
                           &nbsp;<?php echo $value['title']; ?>
                           </label>
                        </div>
                        <?php 
                        $dflt_tmfrm++;
                        } ?>
                     </div>
                     <div class="clearfix"></div>
                     <?php } ?>
                  </div>
                  <div style="font-weight: bold">
                      <?php echo $this->Language['price'].":"; ?> 
                        <span id="charged_amt" data-amount="<?php echo $bundle_price;?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php echo $bundle_price;?></span>
                        <span id="discount_charged_amt" style="display: none;">
                            <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$bundle_price;?></span>
                            <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                        </span>
                  </div>
                  <div class="form-group ">
                      <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                        <br/>
                        <div class="col-sm-6 pull-left">
                          <div class="input-group input-group-sm">
                             <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                             <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                             <span class="input-group-btn">
                             <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                             </span>
                          </div>
                          <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                          <div id="valid_coupon_suc" class="has-success" style="display: none;">
                             <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return showPaymentForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-12">
                            <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return showPaymentForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                        </div>
                    <?php } ?>
                     <div class="clear"></div>
                  </div>
               </form>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
<?php } else { ?>
    <div class="modal-content" id="price_detail" style="position: relative;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title"><?php echo $this->Language['purchase']; ?> <?php echo ucfirst($films['name']);?></h4>
      </div>
      <div  id="loader-ppv" style="margin-left: 250px;text-allign:center;display: none">
         <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
         <span class="sr-only">Loading...</span>
      </div>
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
               <span class="error" id="plan_error"></span>
               <?php if (isset($data['msg']) && trim($data['msg'])) { ?>
               <span class="error"><?php echo $data['msg'];?></span>
               <?php } else { ?>
                  
                <form id="ppv_plans_form" name="ppv_plans_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false">
                    
                    <!-- For multipart content when bundle is not enabled-->
                    <?php if (isset($data['content_types_id']) && intval($data['content_types_id']) == 3  && $is_ppv_bundle == 0) { ?>
                        <?php if (isset($data['min_season']) && intval($data['min_season'])) { ?>
                    
                        <?php 
                            $is_show_price_not_zero = $is_season_price_not_zero = $is_episode_price_not_zero = 1;
                            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                if (abs($price['show_subscribed']) < 0.00001) {
                                    $is_show_price_not_zero = 0;
                                }
                            } else {
                                if (abs($price['show_unsubscribed']) < 0.00001) {
                                    $is_show_price_not_zero = 0;
                                }
                            }

                            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                if (abs($price['season_subscribed']) < 0.00001) {
                                    $is_season_price_not_zero = 0;
                                }
                            } else {
                                if (abs($price['season_unsubscribed']) < 0.00001) {
                                    $is_season_price_not_zero = 0;
                                }
                            }

                            if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) {
                                if (abs($price['episode_subscribed']) < 0.00001) {
                                    $is_episode_price_not_zero = 0;
                                }
                            } else {
                                if (abs($price['episode_unsubscribed']) < 0.00001) {
                                    $is_episode_price_not_zero = 0;
                                }
                            }
                        
                            $charged_amt = 0;
                            $is_show_checked = $is_season_checked = $is_episode_checked = 0;
                            
                            if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) {
                                $is_show_checked =1;
                                $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $price['show_subscribed']: $price['show_unsubscribed']; 
                            } else if (isset($data['is_season']) && intval($data['is_season']) && intval($is_season_price_not_zero)) {
                                $is_season_checked = 1;
                                $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $price['season_subscribed']: $price['season_unsubscribed']; 
                            } else if (isset($data['is_episode']) && intval($data['is_episode']) && intval($is_episode_price_not_zero)) {
                                $is_episode_checked = 1;
                                $charged_amt = (isset($data['member_subscribed']) && trim($data['member_subscribed'])) ? $price['episode_subscribed']: $price['episode_unsubscribed']; 
                            }
                        ?>
                        
                        <?php if (isset($data['is_show']) && intval($data['is_show']) && intval($is_show_price_not_zero)) { ?>
                        <div class="form-group">
                           <div class="col-md-10">
                              <label style="font-weight: normal">
                                  <input type="radio" name="data[plan]" <?php if (intval($is_show_checked)) { ?>checked="checked" <?php } ?> value="show" id="showtext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['show_subscribed']; } else { echo $price['show_unsubscribed']; }?>" onclick="setPriceForMulitPart(this);" />
                              <?php echo $this->Language['entire_show'].":"; ?>
                              </label>
                           </div>
                           <div class="col-md-2">
                              <?php echo $symbol; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['show_subscribed']; } else { echo $price['show_unsubscribed']; }?>
                           </div>
                        </div>
                        <?php } ?>
                        
                        <?php if (isset($data['is_season']) && intval($data['is_season']) && intval($data['min_season']) && intval($is_season_price_not_zero)) { ?>
                        <div class="form-group">
                           <div class="col-md-3">
                              <label style="font-weight: normal">
                              <input type="radio" name="data[plan]" <?php if (intval($is_season_checked)) { ?>checked="checked" <?php } ?> value="season" id="seasontext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['season_subscribed']; } else { echo $price['season_unsubscribed']; }?>" onclick="setPriceForMulitPart(this);"/>
                              <?php echo $this->Language['season'].":"; ?>
                              </label>
                           </div>
                           <div class="col-md-7">
                              <select class="form-control" name="data[season]" id="seasonval" onchange="setSeason(this);">
                                 <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                                 <option value="<?php echo $i;?>" <?php if((int)@$season == $i){ echo "selected";} ?>><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="col-md-2">
                              <?php echo $symbol; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['season_subscribed']; } else { echo $price['season_unsubscribed']; }?>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        
                        <?php if (isset($data['is_episode']) && intval($data['is_episode']) && intval($data['min_season']) && intval($is_episode_price_not_zero)) { ?>
                        <div class="form-group">
                           <div class="col-md-3">
                              <label style="font-weight: normal">
                              <input type="radio" name="data[plan]" <?php if (intval($is_episode_checked)) { ?>checked="checked" <?php } ?> value="episode" id="episodetext" data-price="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['episode_subscribed']; } else { echo $price['episode_unsubscribed']; }?>" onclick="setPriceForMulitPart(this);"/>
                              <?php echo $this->Language['episode'].":"; ?>
                              </label>
                           </div>
                           <div class="col-md-3">
                              <select class="form-control" name="data[season]" id="seasonval1" onchange="showEpisode(this);">
                                 <?php for ($i = $data['min_season']; $i <= $data['max_season']; $i++) { ?>
                                 <option value="<?php echo $i;?>"><?php echo $this->Language['season']; ?> <?php echo $i;?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="col-md-4 ">
                              <select class="form-control" name="data[episode]" id="episodeval" onchange="setEpisode(this);">
                                 <?php foreach ($data['episodes'] as $key => $value) { ?>
                                 <option value="<?php echo $value['embed_id'];?>"><?php echo $value['episode_title'];?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="col-md-2">
                              <?php echo $symbol; ?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['episode_subscribed']; } else { echo $price['episode_unsubscribed']; }?>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <div class="form-group">
                           <div class="col-md-12">
                              <label id="data[plan]-error" class="error" for="data[plan]" style="display: none;"><?php echo $this->Language['choose_plan']; ?></label>
                           </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-12">
                                <div style="font-weight: bold">
                                    <?php echo $this->Language['price'].":"; ?> 
                                        <span id="charged_amt" data-amount="<?php echo $charged_amt;?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php echo $charged_amt;?></span>
                                        <span id="discount_charged_amt" style="display: none;">
                                        <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$charged_amt;?></span>
                                        <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                                        </span>
                                </div>
                            </div>
                        </div>
                        
                        <?php } else { ?>
                            <span class="error"><?php echo $this->Language['no_video']; ?></span>
                        <?php } ?>
                        <?php } else  { ?>
                        <div class="form-group">
                            <?php if (intval(@$data['isadv'])) { ?>
                                <div class="col-md-12">
                                    <?php echo $plan->description;?>
                                </div>
                            <?php } else { ?>
                                <?php if (isset($validity->validity_period) && intval($validity->validity_period)) { ?>
                                <div class="col-md-12">
                                    <?php echo $this->Language['available_for']; ?> <?php echo $validity->validity_period.' '.strtolower($validity->validity_recurrence)."s"?> <?php echo $this->Language['to_watch']; ?>
                                </div>
                                <?php } ?>
                            <?php } ?>
                            
                            <div class="col-md-12">
                                <div style="font-weight: bold">
                                    <?php echo $this->Language['price'].":"; ?> 
                                        <span id="charged_amt" data-amount="<?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?>" data-currency="<?php echo $symbol;?>"><?php echo$symbol;?><?php if (isset($data['member_subscribed']) && trim($data['member_subscribed'])) { echo $price['price_for_subscribed']; } else { echo $price['price_for_unsubscribed']; }?></span>
                                        <span id="discount_charged_amt" style="display: none;">
                                        <span id="dis_charged_amt" style="text-decoration: line-through;"><?php echo $symbol.$amount;?></span>
                                        <sup><span style="font-weight: bold;font-size: 15px;" id="discount_charged_amt_span"></span></sup>
                                        </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group ">
                      <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
                        <div class="col-sm-6 pull-left">
                          <div class="input-group input-group-sm">
                             <input type="text" class="form-control" name="data[coupon_code]" id="coupon" placeholder="<?php echo $this->Language['coupon_code_optional']; ?>">
                             <input type="hidden" name="data[coupon_use]" value="0" id="coupon_use" />
                             <span class="input-group-btn">
                             <button type="button" class="btn btn-info btn-flat" id="coupon_btn" onclick="validateCoupon();"><?php echo $this->Language['btn_apply']; ?></button>
                             </span>
                          </div>
                          <div id="invalid_coupon_error" style="color:red;font-size:11px;display: none"></div>
                          <div id="valid_coupon_suc" class="has-success" style="display: none;">
                             <label for="inputSuccess" class="control-label" style="font-weight: normal;color: #4da30c;"><?php echo $this->Language['discount_on_coupon']; ?> <span id="coupon_in_amt"></span></label>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                         </div>
                    <?php } else { ?>
                        <div class="col-md-12">
                            <button id="btn_proceed_payment" class="btn btn-primary  pull-right" onclick="return validateCategoryForm();"><?php echo $this->Language['btn_proceed_payment']; ?></button>
                         </div>
                    <?php } ?>
                     <div class="clear"></div>
                  </div>
               </form>
               <?php } ?>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
<?php } ?>
    
    
    
    <!-- Payment form-->
   <div class="modal-content" id="card_detail" style="position: relative;display: none;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" style="text-transform: none;">
            <?php echo $this->Language['you_are_purchasing']; ?> <span id="bundle-hdr"><?php echo ucfirst($films['name']);?> <?php //echo $this->Language['for']; ?> <!--<span id="hdr-amnt"><?php //echo $symbol.$amount;?></span>--></span>
         </h4>
         <?php if(isset($data['isadv']) && intval($data['isadv'])) {?>
         <div style="color: #5b5654;margin-top: 10px;">
             <?php echo $this->Language['content_available_from']; ?> <?php echo date('M d, Y',strtotime($plan->expiry_date . "+1 Days"));?>
         </div>
         <?php if (isset($plan->description) && trim($plan->description)) { ?><div style="color:  #5b5654;margin-top: 10px;"><?php echo $plan->description;?></div><?php } ?>
         <?php } ?>
         <div id="title-hdrdiv"></div>
      </div>
      <div  id="loader-ppv" style="margin-left: 250px;text-allign:center;display: none">
         <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
         <span class="sr-only">Loading...</span>
      </div>
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
               <form id="membership_form" name="membership_form" method="POST" class="form-horizontal" action="javascript:void(0);" autocomplete="false" >
                  <div id="other-payment-gateway">
                     <div id="card-info-error" class="error" style="display: block;margin: 0 0 15px;"></div>
                     <input type="hidden" id="email" name="data[email]" value="<?php if (isset(Yii::app()->user->email) && trim(Yii::app()->user->email)) { echo Yii::app()->user->email;} ?>" />
                     <input type="hidden" id="payment_gateway" value="<?php echo $gateways[0]->short_code; ?>" />
                     <input type="hidden" name="data[coupon]" id="coupon_code" value="<?php if (isset($data['coupon_code'])) { echo $data['coupon_code'];} ?>" />
                     <input type="hidden" name="data[plan_id]" id="plandetail_id" value="<?php echo $plan->id; ?>" />
                     <input type="hidden" name="data[timeframe_id]" value="" id="timeframe_id" />
                     <input type="hidden" name="data[is_bundle]" value="" id="is_bundle" />
                     <input type="hidden" name="data[movie_id]" id="ppvmovie_id" value="<?php if (isset($data['movie_id']) && trim($data['movie_id'])) { echo $data['movie_id'];} ?>" />
                     <input type="hidden" name="data[season_id]" id="season_id" value="<?php if (isset($data['season_id']) && trim($data['season_id'])) { echo $data['season_id'];} else { echo 0;} ?>" />
                     <input type="hidden" name="data[episode_id]" id="episode_id" value="<?php if (isset($data['episode_id']) && trim($data['episode_id'])) { echo $data['episode_id'];} else { echo 0;} ?>" />
                     <input type="hidden" name="data[contentTypePermalink]" value="<?php echo $films['content_permalink'];?>" />
                     <input type="hidden" id="permalink" name="data[permalink]" value="<?php echo $films['permalink'];?>" />
                     <input type="hidden" name="data[ppv_plan]" id="ppv_plan" value="<?php echo (isset($this->PAYMENT_GATEWAY[$gateway_code]) && ($this->PAYMENT_GATEWAY[$gateway_code] == 'paypalpro' || $this->PAYMENT_GATEWAY[$gateway_code] == 'paygate')) ? 1 : 0;?>" />
                     <input type="hidden" name="data[payment_method]" id="payment_method" value="<?php echo $gateway_code;?>" />
                     <input type="hidden" name="data[is_advance_purchase]" id="is_adv_plan" value="<?php echo intval(@$data['isadv']);?>" />
                     <div class="form-group">
                         <div class="col-md-12" id="credit_card_detail_info">
                           <h4>
                              <?php echo $this->Language['credit_card_detail']; ?>
                           </h4>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-6 pull-left">
                           <label style="font-weight: normal">
                           <?php echo $this->Language['card_will_charge']; ?>: 
                           <span id="price_amount" data-amount="" data-currency="<?php echo $symbol;?>" style="font-weight: bold;font-size: 18px;"></span>
                           </label>
                        </div>
                        <?php if (isset($cards) && !empty($cards) && intval($can_save_card)) { ?>
                        <div class="col-sm-6 pull-left">
                           <select class="form-control" name="data[creditcard]" id="creditcard" onchange="getCardDetail();">
                              <option value=""><?php echo $this->Language['use_new_card']; ?></option>
                              <?php foreach ($cards as $key => $value) { ?>
                              <option value="<?php echo $value['id']; ?>" ><?php echo trim($value['card_name']) ? $value['card_name']."-".$value['card_last_fourdigit'] . " " . $value['card_type'] : $value['card_last_fourdigit'] . " " . $value['card_type']; ?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                     </div>
                     <div id="card_detail_div"></div>
                     <input type="hidden" name="data[currency_id]" value="<?php if (isset($currency->id)) { echo $currency->id; } else { echo 0; }?>" id="currency_id" />
                     <?php if (intval($can_save_card)) { ?>
                     <div class="form-group" id="save_card_option">
                        <?php if (intval($can_save_card)) { ?>
                        <div class="col-sm-6 pull-left" id="for_last_checkout">
                           <div class="checkbox">
                              <label for="save_card">
                              <input type="checkbox" checked="checked" id="for_last_checkout_chk" name="data[save_this_card]" value="1" onclick="saveThisCard(this);"/> <?php echo $this->Language['save_this_card']; ?>
                              </label>
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                     <?php }?>
                     <div class="clear"></div>
                     <div id="card_div"></div>
                     <?php if(isset($this->API_ADDONS_OTHERGATEWAY[$gateway_code]) && !empty($this->API_ADDONS_OTHERGATEWAY[$gateway_code])){ ?>
                     <div class="form-group" id="other-gateway">
                        <div class="loader" id="othergateway_loading" style="z-index:10;"></div>
                        <div class="col-md-12">
                            <div id="paypal-container"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php }?>
                    <div class="form-group">
                            <div class="row" style="padding-left: 16px;">
                                <?php if (isset($this->PAYMENT_GATEWAY) && array_key_exists('paypalpro', $this->PAYMENT_GATEWAY) && $this->PAYMENT_GATEWAY['paypalpro'] == 'paypalpro') {?>
                                <div class="col-md-6">
                                    <input type="checkbox" style="display:none" id="have_paypal_pro" name="havePaypal" value="1" class="process-check" />
<!--                                    <button id="paypal" class="btn btn-primary pull-right" onclick="return savePayPalCardForm();"><img src="<?php //echo Yii::app()->baseUrl; ?>/images/paypal.png" alt="" title="" /></button>-->
                                    <input type="image" src="<?php echo Yii::app()->baseUrl; ?>/images/paypal.png" id="paypal" onclick="return savePayPalCardForm();" style="margin-top: -15px;">
                                </div>
                                <?php }?>
                                <div class="col-md-6"><button id="paynowbtn" class="btn btn-primary" onclick="return validateCardForm();"><?php echo $this->Language['btn_paynow']; ?></button></div>
                                
                            <div class="clear"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    
                  </div>
               </form>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>

<!-- Card form which will put on payment form-->
<div id="new_card_div" style="display: none;">
   <div class="form-group">
      <div class="col-sm-6 pull-left">
         <label for="card_name"><?php echo $this->Language['text_card_name']; ?></label>
      </div>
      <div class="col-sm-6 pull-left">
         <label for="card_number"><?php echo $this->Language['text_card_number']; ?></label>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-6 pull-left">
         <input type="text" class="form-control" autocomplete="false" id="card_name" name="data[card_name]" required />
      </div>
      <div class="col-sm-6 pull-left">
         <input type="text" class="form-control" autocomplete="false" id="card_number" name="data[card_number]" required />
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 pull-left">
         <label for="exp_month"><?php echo $this->Language['selct_exp_date']; ?></label>
      </div>
      <div class="col-sm-6 pull-left">
         <label for="security_code"><?php echo $this->Language['text_security_code']; ?></label>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-6 pull-left">
         <?php
            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
            ?>
         <div class="col-sm-6" style="padding: 0">
            <select name="data[exp_month]" id="exp_month" class="form-control" required="true">
               <option value=""><?php echo $this->Language['select_month']; ?></option>
               <?php for ($i = 1; $i <= 12; $i++) { ?>
               <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
               <?php } ?>
            </select>
            <div class="sbox-err-lbl">
               <label id="exp_month-error" class="error" for="exp_month" style="display: none;"></label>
            </div>
         </div>
         <div class="col-sm-6" style="padding-left: 5px;padding-right: 0;">
            <select name="data[exp_year]" id="exp_year" class="form-control sbox" required="true" onchange="getMonthList();">
               <option value=""><?php echo $this->Language['select_year']; ?></option>
               <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
               <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
               <?php } ?>
            </select>
            <div class="sbox-err-lbl">
               <label id="exp_year-error" class="error" for="exp_year" style="display: none;"></label>
            </div>
         </div>
      </div>
      <div class="col-sm-2 pull-left">
         <input type="password" id="" name="" style="display: none;" />
         <input type="password" class="form-control" autocomplete="false" id="security_code" name="data[security_code]" required />
      </div>
      <div class="col-sm-4 pull-left">
         <input style="padding: 6px 8px;" type="text" name="data[card_name_optional]" class="form-control" placeholder="<?php echo $this->Language['card_name_optional']; ?>" />
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="clear"></div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" style="border: none;">
            <div class="modal-title success-popup-payment"><?php echo $this->Language['thanks_card_auth_sucess']; ?></div>
         </div>
      </div>
   </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" style="border: none;">
            <div class="modal-title auth-msg"><?php echo $this->Language['auth_your_card']; ?></div>
            <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" alt="" title="" style="padding:5px;"/></div>
         </div>
      </div>
   </div>
</div>

<div id="paypalproPopup" class="modal fade" style="width: 660px;">
   <div class="modal-dialog" style="width: 660px;">
      <div class="modal-content">
         <div class="modal-header">
            <div class="modal-title success-popup-payment"><?php echo $this->Language['pls_wait']; ?></div>
            <div id="test_iframe" style="display:none">
               <iframe>
                  <p><?php echo $this->Language['browser_iframe_not_support']; ?></p>
               </iframe>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="ppvPricingModal" class="modal fade login-popu" data-backdrop="static" data-keyboard="false"></div>
<?php }} else { ?>
<?php if (isset($data['msg']) && trim($data['msg'])) { ?>
<div class="modal-dialog">
   <div class="modal-content" style="position: relative;">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title"><?php echo ucfirst($films['name']);?></h4>
      </div>
      <div class="modal-body">
         <div class="row-fluid">
            <div class="col-md-12">
               <span class="error"><?php echo $data['msg'];?></span>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<?php } ?>

<?php
    if(isset($this->IS_PCI_COMPLIANCE[$gateway_code]) && intval($this->IS_PCI_COMPLIANCE[$gateway_code]) == 1) { ?>
        <form name="pci_form" id="pci_form"></form>
<?php }
?>
<div style="display: none;" class="loader" id="payment-loading"></div>
<script type="text/javascript">
    var action = 'ppvpayment';
    var btn = '<?php echo $this->Language['btn_paynow']; ?>';
    var is_coupon_exists = 0;
    <?php if (isset($data['is_coupon_exists']) && intval($data['is_coupon_exists'])) { ?>
            is_coupon_exists = 1;
    <?php }?>
    
    $(document).ready(function () {
        $(".close").click(function(){
            if (parseInt(reload)) {
                location.reload();
            }
            reload = 0;
            $(".modal-backdrop").remove();
        });
        getCardDetail();
        isCouponExist();
        
        $('input').attr('autocomplete', 'off');
        $('form').attr('autocomplete', 'off');
    });
    
    $(document).click(function() {
        $(".popover").hide();
    });

    $(".popover").click(function(event) {
        event.stopPropagation();
    });
    
    function viewDetail(obj) {
        var plan = $(obj).attr("data-plan");
        
        if($("#popover_"+plan).is(':hidden')) {
            $("#ldr_"+plan).show();
            $(".popover").hide();
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/getBundledContents";
            $.post(url, {'plan': plan}, function (res) {
                $("#ldr_"+plan).hide();
                $("#popover_"+plan).show();
                $(".popover-content").html(res);
            });
        } else {
            $(".popover").hide();
        }
    }
    
    function getCardDetail() {
        if (parseInt($("#creditcard option:selected").val())) {
            $("#card_detail_div").html('');
            $("#for_last_checkout_chk").val(0);
            $("#for_last_checkout_chk").prop("checked", false);
            $("#for_last_checkout").hide();
            $("#other-gateway").hide();
            if($("#paypal").length){
                $("#paypal").attr("disabled",true);
            }
        } else {
            $("#card_detail_div").html($("#new_card_div").html());
            $("#for_last_checkout").show();
            $("#other-gateway").show();
            $("#for_last_checkout_chk").prop("checked", true);
            $("#for_last_checkout_chk").val(1);
            if($("#paypal").length){
                $("#paypal").removeAttr("disabled");
            }
        }
    }
    
    function setEpisode(obj) {
        var episode = $(obj).val();
        
        if ($.trim(episode)) {
            $("#episodetext").prop('checked', true);
        } else {
            $("#episodetext").prop('checked', false);
        }
        
        if($('label.error').length) {
            $('label.error').hide();
        }
    }
    
    function setSeason(obj) {
        var season = $(obj).val();
        
        if (parseInt(season)) {
            $("#seasontext").prop('checked', true);
        } else {
            $("#seasontext").prop('checked', false);
        }
    }
    
    function savePayPalCardForm(){
        if (parseInt($("#creditcard option:selected").val())) {
            if (confirm(JSLANGUAGE.confirm_existing_card)) {
                submitForm();
            } else {
                return false;
            }
        } else {
            $("#have_paypal_pro").prop('checked', true);
                if ($("#payment_gateway").length) {
                if ($("#payment_gateway").val() !== 'manual') {
                    var payment_gateway = $("#payment_gateway").val();
                    var have_paypal_pro = 1;
                    var payment_gateway_str = $('#payment_gateway_str').val();
                    if(!have_paypal_pro && payment_gateway != 'paypalpro'){
                         payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                    }else if(have_paypal_pro){
                        if(payment_gateway = 'paypalpro'){
                            payment_gateway_str = payment_gateway;
                        }else{
                             payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                         }
                     }else{
                         payment_gateway_str = payment_gateway;
                     }
                     payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                    var class_name = payment_gateway_str+'()';
                    eval ("var obj = new "+class_name);
                    obj.processCard();
                    
                    $("#card_detail").hide();
                    $("#card_name").removeAttr('required');
                    $("#card_number").removeAttr('required');
                    $("#exp_month").removeAttr('required');
                    $("#exp_year").removeAttr('required');
                    $("#security_code").removeAttr('required');
                }
            } else {
                submitForm();
            }
        }
        
    }
    
    function validateCardForm() {
        if($.trim($("#payment_gateway").val()) === 'paypalpro'){
            document.getElementById('paypal').setAttribute('disabled', 'disabled');
        }
        if (parseInt($("#creditcard option:selected").val())) {
            if (confirm(JSLANGUAGE.confirm_existing_card)) {
                submitForm();
            } else {
                $("#paypal").removeAttr("disabled");
                $("#paynowbtn").removeAttr("disabled");
                return false;
            }
        } else {
            $("#membership_form").validate({
                rules: {
                    "data[card_name]": {
                        required: true
                    },
                    "data[card_number]": {
                        required: true,
                        number: true
                    },
                    "data[exp_month]": {
                        required: true
                    },
                    "data[exp_year]": {
                        required: true
                    },
                    "data[security_code]": {
                        required: true
                    }
                },
                messages: {
                    "data[card_name]": {
                        required: JSLANGUAGE.card_name_required,
                    },
                    "data[card_number]": {
                        required: JSLANGUAGE.card_number_required,
                        number: JSLANGUAGE.card_number_required
                    },
                    "data[exp_month]": {
                        required: JSLANGUAGE.expiry_month_required
                    },
                    "data[exp_year]": {
                        required: JSLANGUAGE.expiry_year_required
                    },
                    "data[security_code]": {
                        required: JSLANGUAGE.security_code_required
                    }
                },
                submitHandler: function (form) {
                    if ($("#payment_gateway").length) {
                        if ($("#payment_gateway").val() !== 'manual') {
                            var payment_gateway = $("#payment_gateway").val();
                            var have_paypal_pro = 0;
                            
                            if($('#have_paypal_pro').is(":checked")){
                                have_paypal_pro = 1;
                            }
                            
                            var payment_gateway_str = $('#payment_gateway_str').val();
                            if(!have_paypal_pro && payment_gateway !== 'paypalpro'){
                                 payment_gateway_str = payment_gateway_str.replace('paypalpro','');
                            }else if(have_paypal_pro){
                                if(payment_gateway === 'paypalpro'){
                                    payment_gateway_str = payment_gateway;
                                }else{
                                     payment_gateway_str = payment_gateway_str.replace(payment_gateway,'');
                                 }
                             }else{
                                 payment_gateway_str = payment_gateway;
                             }
                             payment_gateway_str = payment_gateway_str.replace(/(^,)|(,$)/g, "");
                            var class_name = payment_gateway_str+'()';
                            eval ("var obj = new "+class_name);
                            obj.processCard();
                        }
                    } else {
                        document.getElementById('paypal').setAttribute('disabled', 'disabled');
                        submitForm();
                    }             
                }
            });
        }
    }
    
    function submitForm() {
        document.membership_form.action = HTTP_ROOT+"/user/"+action;
        document.membership_form.submit();
        return false;
    }
    
    function saveThisCard(obj) {
        if ($(obj).is(':checked')) {
            $(obj).val(1);
        } else {
            $(obj).val(0);
        }
    }
    
    function showEpisode(obj) {
        var movie_id = $("#ppvmovie_id").val();
        var season = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getEpisodes";
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        
        if($('label.error').length) {
            $('label.error').hide();
        }
        
        $.post(url, {'movie_id': movie_id, 'season': season}, function (res) {
            $("#btn_proceed_payment").removeAttr("disabled");
            var str = '';

            if (res.length) {
                for (var i in res) {
                    str = str + '<option value="'+res[i].embed_id+'">'+res[i].episode_title+'</option>';
                }
            }
            $("#episodeval").html(str);
              $('#loader-ppv').show();
            
            if ($.trim($("#episodeval"))) {
                 $('#loader-ppv').hide();
                $("#episodetext").prop('checked', true);
            } else {
                  $('#loader-ppv').hide();
                $("#episodetext").prop('checked', false);
            }
        }, 'json');
    }
    
    function validateCategoryForm() {
        $("#ppv_plans_form").validate({
            rules: {
                "data[plan]": {
                    required: true
                },
                "data[season]": {
                    isseason: true
                },
                "data[episode]": {
                    isepisode: true
                }
            },
            messages: {
                "data[plan]": {
                    required: JSLANGUAGE.choose_option
                },
                "data[season]": {
                    isseason: JSLANGUAGE.choose_season
                },
                "data[episode]": {
                    isepisode: JSLANGUAGE.select_episode
                }
            },
            submitHandler: function (form) {
                var use_coupon = $.trim($("#coupon_use").val());
                var coupon_code = '';
                var coupon_currency_id = 0;
                if(use_coupon === "1"){
                    coupon_code =  $.trim($("#coupon").val());
                    coupon_currency_id =  $("#currency_id").val();
                }
                var payment_method =  $("#payment_method").val();
                
                var movie_id = $("#ppvmovie_id").val();
                var season_id = 0;
                var episode_id = 0;
                var isadv = 0;
                
                var is_show = '<?php echo (isset($data['is_show']) && intval($data['is_show'])) ? 1 : 0; ?>';
                var is_season = '<?php echo (isset($data['is_season']) && intval($data['is_season'])) ? 1 : 0; ?>';
                var is_episode = '<?php echo (isset($data['is_episode']) && intval($data['is_episode'])) ? 1 : 0; ?>';
                var content_types_id = '<?php echo (isset($plan->content_types_id) && intval($plan->content_types_id)) ? $plan->content_types_id : 0; ?>';
                isadv = '<?php echo (isset($data['isadv']) && intval($data['isadv'])) ? 1 : 0; ?>';
                
                var purchase_type = "";
                
                var season_title = '';
                var episode_title = '';
                
                if ($("#showtext").is(':checked')) {
                     purchase_type = "show";
                } else if (parseInt(is_season) && $("#seasontext").is(':checked')) {
                    season_id = $.trim($("#seasonval").val());
                    episode_id = 0;
                    purchase_type = "season";
                    
                    season_title = $("#seasonval option:selected").text();;
                } else if (parseInt(is_episode) && $("#episodetext").is(':checked')) {
                    season_id = $.trim($("#seasonval1").val());
                    episode_id = $.trim($("#episodeval").val());
                    purchase_type = "episode";
                    
                    season_title = $("#seasonval1 option:selected").text();
                    episode_title = $("#episodeval option:selected").text();
                }
                
                var url = "<?php echo Yii::app()->baseUrl; ?>/user/isPPVSubscribed";
                $('#loader-ppv').show();
                $("#btn_proceed_payment").prop("disabled", true);
                $.post(url, {'movie_id': movie_id, 'season_id': season_id, 'episode_id': episode_id, 'purchase_type': purchase_type, 'content_types_id': content_types_id, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method, 'isadv': isadv}, function (res) {
                    $('#loader-ppv').hide();
                    if (parseInt(res) === 1) {
                        if(parseInt(content_types_id) === 3) {
                            var uri = '';
                            if (parseInt(season_id)) {
                                uri = uri + '/season/'+season_id;
                            }
                            if (episode_id !== 0) {
                                uri = uri + '/stream/'+episode_id;
                            }
                            window.location.href = "<?php echo Yii::app()->baseUrl; ?>/player/<?php echo $films['permalink'];?>"+uri;
                        } else {
                            window.location.href = "<?php echo Yii::app()->baseUrl; ?>/player/<?php echo $films['permalink'];?>";
                        }
                    } else if (parseInt(res.isAdvanceAmtZero) === 1) {
                        $('body').prepend('<div class="top_msg_bar"><div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'+res.msg+'</div></div>');
                        setTimeout(function () {
                            location.reload();
                            return false;
                        }, 2000);
                    } else {
                            if ($.trim(res.msg)) {
                                $("#btn_proceed_payment").removeAttr("disabled");
                                $("#plan_error").html(res.msg).show();
                            } else {
                                if ($("#payment_gateway").length && $("#payment_gateway").val() === 'instafeez') {
                                    var obj = new instafeez();
                                    obj.doPayment();
                                    return false;
                                }else if(parseInt(res.is_pci_compliance) === 1){
                                    $('#loader-ppv').show();
                                    var class_name = <?php echo $this->PAYMENT_GATEWAY[$gateway_code];?>+'()';
                                    eval ("var obj = new "+class_name);
                                    obj.processCard();
                                }else{
                                    $("#ppvModalMain").css('width', '60%');
                                    $("#price_detail").hide();
                                    $("#card_detail").show();
                                    $("#season_id").val(res.season_id);
                                    $("#episode_id").val(res.episode_id);

                                    var currency = $("#charged_amt").attr('data-currency');
                                    //$("#hdr-amnt").text(currency+res.amount);
                                    //$("#charged_amt").attr('data-amount', res.amount);
                                    //$("#charged_amt").text(currency+res.amount);
                                    //$("#dis_charged_amt").text(currency+res.amount);

                                    $("#coupon_code").val(res.coupon_code);
                                    $("#price_amount").attr('data-amount', res.amount);
                                    $("#price_amount").text(currency+res.amount);

                                    if ($.trim(season_title) || $.trim(episode_title)) {
                                        var str = '<div style="color: #ccc;margin-top: 10px;">'+season_title+' '+ episode_title+'</div>';
                                        $('#title-hdrdiv').html(str);
                                    }
                                }
                            }
                    }
                }, 'json');
            }
        });
    }
    
    function showPaymentForm() {
        var movie_id = $("#ppvmovie_id").val();
        var episode_id = $("#episode_id").val();
        var plan = $("input[type='radio'].ppv-bundled-cls:checked").val();
        var timeframe = 0;
        var use_coupon = $.trim($("#coupon_use").val());
        var coupon_code = '';
        var coupon_currency_id = 0;
        if(use_coupon === "1"){
            coupon_code =  $.trim($("#coupon").val());
            coupon_currency_id =  $("#currency_id").val();
        }
        var payment_method =  $("#payment_method").val();
        
        if ($(".ppv-bundled-tmframe-cls").length && $("input[type='radio'].ppv-bundled-tmframe-cls").is(":checked")) {
            timeframe = $("input[type='radio'].ppv-bundled-tmframe-cls:checked").val();
        }
        
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/isPPVSubscribed";
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        $.post(url, {'movie_id': movie_id, 'episode_id': episode_id, 'plan': plan, 'timeframe': timeframe, 'is_ppv_bundle': 1, 'coupon_code' : coupon_code,'coupon_currency_id' : coupon_currency_id, 'payment_method': payment_method}, function (res) {
            $('#loader-ppv').hide();
            if (parseInt(res) === 1) {
                window.location.href = "<?php echo Yii::app()->baseUrl; ?>/player/<?php echo $films['permalink'];?>";
            } else {
                
                    if ($.trim(res.msg)) {
                        $("#btn_proceed_payment").removeAttr("disabled");
                        $("#plan_error").html(res.msg).show();
                    }else if(parseInt(res.is_pci_compliance) === 1){
                        $('#loader-ppv').show();
                        $("#plandetail_id").val(plan);
                        $("#timeframe_id").val(timeframe);
                        $("#bundle-hdr").text(res.bundle_title);
                        $("#is_bundle").val(1);
                        var currency = $("#charged_amt").attr('data-currency');
                        $("#coupon_code").val(res.coupon_code);
                        $("#price_amount").attr('data-amount', res.amount);
                        $("#price_amount").text(currency+res.amount);
                        var class_name = <?php echo $this->PAYMENT_GATEWAY[$gateway_code];?>+'()';
                        eval ("var obj = new "+class_name);
                        obj.processCard();
                    } else {
                        $("#ppvModalMain").css('width', '60%');
                        $("#price_detail").hide();
                        $("#card_detail").show();
                        
                        $("#plandetail_id").val(plan);
                        $("#timeframe_id").val(timeframe);
                        $("#bundle-hdr").text(res.bundle_title);
                        $("#is_bundle").val(1);
                        var currency = $("#charged_amt").attr('data-currency');
                        //$("#charged_amt").attr('data-amount', res.amount);
                        //$("#charged_amt").text(currency+res.amount);
                        //$("#dis_charged_amt").text(currency+res.amount);
                        
                        $("#coupon_code").val(res.coupon_code);
                        $("#price_amount").attr('data-amount', res.amount);
                        $("#price_amount").text(currency+res.amount);
                    }
            }
        }, 'json');
    }
    
    function validateCoupon(){
    var couponCode = $.trim($("#coupon").val());
        if(couponCode !== ''){
            $('#loader-ppv').show();
            $("#btn_proceed_payment").prop("disabled", true);
            $("#coupon_btn").prop("disabled", true);
            $("#paypal").prop("disabled", true);
            var url = "<?php echo Yii::app()->baseUrl; ?>/user/validateCoupon";
            $.post(url, {'couponCode': couponCode,'currency_id' : $("#currency_id").val()}, function (res) {
                $('#loader-ppv').hide();
                $("#paypal").removeAttr("disabled");
                $("#btn_proceed_payment").removeAttr("disabled");
                $("#coupon_btn").removeAttr("disabled");
                
                if (parseInt(res.isError)) {
                    if (parseInt(res.isError) === 1) {
                        $("#coupon_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
                    } else if (parseInt(res.isError) === 2) {
                        $("#coupon_use").val(0);
                        $("#valid_coupon_suc").hide();
                        $("#invalid_coupon_error").show().html(JSLANGUAGE.coupon_already_used);
                    }
                    
                    $("#charged_amt").show();
                    $("#discount_charged_amt").hide();
                    $("#discount_charged_amt_span").text(0);
                    
                } else {
                    $("#coupon_use").val(1);
                    $("#invalid_coupon_error").html('').hide();
                    $("#valid_coupon_suc").show();
                    
                    var gross_amt = $("#charged_amt").attr('data-amount');
                    var currency = $("#charged_amt").attr('data-currency');
                    var discount_amount = res.discount_amount;
                    
                    var price = 0;
                    if (parseInt(res.is_cash)) {
                        $("#coupon_in_amt").text(currency+""+discount_amount);
                        price = (parseFloat(gross_amt) - parseFloat(discount_amount));
                    } else {
                        $("#coupon_in_amt").text(discount_amount+'%');
                        price = (parseFloat(gross_amt) - (parseFloat(gross_amt) * parseFloat(discount_amount)/100));
                    }
                    
                    var isprice = Math.floor(price * 100) / 100;
                    isprice = Math.round(isprice);
                            
                    if (isprice < 0) {
                        price = 0;
                        discount_amount = gross_amt;
                        if (parseInt(res.is_cash)) {
                            $("#coupon_in_amt").text(currency+""+discount_amount);
                        } else {
                            $("#coupon_in_amt").text(discount_amount+'%');
                        }
                    }
                    
                    $("#charged_amt").hide();
                    $("#discount_charged_amt").show();
                    $("#discount_charged_amt_span").text(currency+""+price.toFixed(2));
                }
            }, 'json');
        } else {
            $("#coupon_use").val(0);
            $("#valid_coupon_suc").hide();
            $("#charged_amt").show();
            $("#discount_charged_amt").hide();
            $("#discount_charged_amt_span").text(0);
            $("#invalid_coupon_error").show().html(JSLANGUAGE.invalid_coupon);
        }
    }
    
    function isCouponExist() {
    var id = '#coupon';
        $(id).keyup(function() {
            if($.trim($(id).val()) === ''){
                $("#charged_amt").show();
                $("#discount_charged_amt").hide();
                
                $("#coupon_use").val(0);
                $("#valid_coupon_suc").hide();
            }
        });
    }
    
    function setPriceForMulitPart(obj) {
        var price = $(obj).attr("data-price");
        var cur = $("#charged_amt").attr('data-currency');
        $("#charged_amt").text(cur+price);
        $("#charged_amt").attr('data-amount', price);
        $("#dis_charged_amt").text(cur+price);
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
    }
    
    function setTimeFramePrice(obj) {
        var price = $(obj).attr("data-price");
        var cur = $("#charged_amt").attr('data-currency');
        $("#charged_amt").text(cur+price);
        $("#charged_amt").attr('data-amount', price);
        $("#dis_charged_amt").text(cur+price);
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
    }
    
    function resetCouponPrice() {
        $("#coupon").val('');
        $("#valid_coupon_suc").hide();
        $("#invalid_coupon_error").hide().html('');
        $("#charged_amt").show();
        $("#discount_charged_amt").hide();
    }
    
    function getBundlePrice(obj) {
        var plan_id = $(obj).val();
        var url = "<?php echo Yii::app()->baseUrl; ?>/user/getBundlePrice";
        
        $('#loader-ppv').show();
        $("#btn_proceed_payment").prop("disabled", true);
        
        if (parseInt(is_coupon_exists)) {
            resetCouponPrice();
        }
        
        $.post(url, {'plan_id': plan_id}, function (res) {
            $('#loader-ppv').hide();
            $("#btn_proceed_payment").removeAttr("disabled");
            
            var str = '';
            
            if (parseInt(res.is_timeframe)) {
                str = '<div style="font-weight: bold">Select TimeFrame</div>';
                str+='<div class="form-group">';
                
                var tm_cnt = 1;
                var is_checked = '';
                
                for(var i in res.timeframeprice) {
                    is_checked = '';
                    if(tm_cnt === 1) {
                        is_checked = 'checked="checked"';
                    }
                    str+='<div class="col-md-3">';
                    str+='<label style="font-weight: normal">';
                    str+='<input type="radio" name="data[timeframeprice]" class="ppv-bundled-tmframe-cls" '+is_checked+' value="'+res.timeframeprice[i].id+'" data-price="'+res.timeframeprice[i].price+'" onclick="setTimeFramePrice(this);"/>&nbsp;'+res.timeframeprice[i].title;
                    str+='</label>';
                    str+='</div>';
                    
                    tm_cnt++;
                }
                
                str+='</div>';
                str+='<div class="clearfix"></div>';
            }
            var cur = $("#charged_amt").attr('data-currency');
            $("#timeframe_div").html(str);
            $("#charged_amt").html(cur+res.price);
            $("#charged_amt").attr('data-amount', res.price);
            $("#dis_charged_amt").text(cur+res.price);
        }, 'json');
    }
    
    jQuery.validator.addMethod("isseason", function (value, element) {
        if ($("#seasontext").is(':checked')) {
            if ($.trim($("#seasonval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_season);
    
    jQuery.validator.addMethod("isepisode", function (value, element) {
        if ($("#episodetext").is(':checked')) {
            if ($.trim($("#episodeval").val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }, JSLANGUAGE.select_episode);
    
    $('#have_paypal_pro').click(function () {
        if ($(this).is(":checked")) {
            $('#card_detail_div').hide(1500);
            $('#creditcard').hide(1500);
            <?php if (intval($can_save_card)) { ?>
            $('#for_last_checkout').hide(1500);
            <?php }?>
        } else {
            $('#card_detail_div').show(1500);
            $('#creditcard').show(1500);
            <?php if (intval($can_save_card)) { ?>
            $('#for_last_checkout').show(1500);
            <?php }?>
        }
    });
    
</script>

<script type="text/javascript" src="<?php echo $this->siteurl;?>/common/js/action.js"></script>
<!-- As the processing the card in different payment gateway is different. So processCard javascript method is in following included file.-->

<?php 
$payment_gateway_str = '';
if(isset($this->PAYMENT_GATEWAY) && count($this->PAYMENT_GATEWAY) > 1){
    $payment_gateway_str = implode(',', $this->PAYMENT_GATEWAY);
    foreach ($this->PAYMENT_GATEWAY as $gateway_code){
        
    ?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
    <?php }}else{
if (isset($this->PAYMENT_GATEWAY[$gateway_code]) && $this->PAYMENT_GATEWAY[$gateway_code] != 'manual') {
    $payment_gateway_str = $this->PAYMENT_GATEWAY[$gateway_code];?>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/common/js/<?php echo $this->PAYMENT_GATEWAY[$gateway_code].'.js';?>"></script>
<?php } }?>

<input type="hidden" id="payment_gateway_str" value="<?php echo $payment_gateway_str;?>">
