<div class="container" id="price_section">
    <div class="wrapper">
        <h1 class="btm-bdr">World&rsquo;s Most Comprehensive Ad Delivery Engine</h1>
        <p class="center">Complete advertising delivery from integrating with ad networks to delivering ads on your VOD website and mobile app. 
            Supports VAST and VPAID. Integrates with leading AD Servers such as SpotXchange and YuMe. See a live demo below.</p>
    </div>
</div>

<div class="container">
    <div class="span12 center">
        <form name="video_frm" action="javascript:void(0);" id="video_frm" class="form-inline" method="post" onsubmit="return submitAdsForm();">
            <div class="form-group">
                <div>
                    <?php
                    $mid_time = '';
                    if ($ad_type == 2) {
                        $mid_time = $midroll_time;
                        $roll_after = 0;
                        $roll_parts = explode(':', $midroll_time);
                        if ($roll_parts[0] > 0)
                            $roll_after += 3600 * $roll_parts[0];
                        if ($roll_parts[1] > 0)
                            $roll_after += 60 * $roll_parts[1];
                        if ($roll_parts[2] > 0)
                            $roll_after += $roll_parts[2];
                        if ($roll_after > 282) {
                            echo '<span class="error">Please enter a value less than video duration</span>';
                        }
                    }
                    ?>
                </div>
                <div>
                    <div class="checkbox">
                        <input type="radio" name="video_type" <?php echo ($ad_type == 0) ? 'checked="checked"' : ''; ?> value="0" id="ad_0" />
                        <label class="inline" for="ad_0">No Ads</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div>
                    <div class="checkbox">
                        <input type="radio" name="video_type" <?php echo ($ad_type == 1) ? 'checked="checked"' : ''; ?> value="1" id="ad_1" />
                        <label class="inline" for="ad_1">Pre-roll</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <input type="radio" name="video_type" <?php echo ($ad_type == 2) ? 'checked="checked"' : ''; ?> value="2" id="ad_2" />
                    <label class="inline" for="ad_2">Mid-roll</label>
                    <input type="text" name="midroll_time" id="midroll_time" style="width:80px;" placeholder="HH:MM:SS" value="<?php echo $mid_time; ?>" />
                </div>
            </div>  
			<input type="hidden" value="" id="hid_val" name="hid_val">
            <div class="form-group">
                <input type="submit" name="ad_submit" class="btn btn-default btn-blue" id="ad_submit" value="Save" />
            </div>

        </form>

    </div>
    <div class="span3">&nbsp;</div>
</div>

<div class="container">
    <div class="videocontent">
        <video id="muvivideo" class="video-js vjs-default-skin" poster="" width="auto" height="auto" controls preload="auto" autoplay data-setup='{"techOrder": ["youtube", "html5","flash"]}'>
            <source src="https://d1yjifjuhwl7lc.cloudfront.net/public/muviStudioVideo.mp4" type="video/mp4" />
        </video>
    </div> 
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>if (!window.jQuery) {
        document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
    }</script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js"></script>
<!--<script src="<?php echo Yii::app()->baseUrl; ?>/js/videojs.hls.min.js"></script>-->
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.ads.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/videojs.vast.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />

<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.ads.js"></script>
<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/vast-client.js"></script>
<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/js/videojs.vast.js"></script>   


<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.css?rand=<?php echo $randomVar; ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/video/quality/video-quality-selector.js?rand=<?php echo $randomVar; ?>"></script>    

<script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>


<script type="text/javascript">
    videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
</script>
<style type="text/css">
    .video-js {height: 40%; padding-top: 56%; background: #FFF;}
    .vjs-fullscreen {padding-top: 0px}  
    .RDVideoHelper{display: none;}
    .form-inline .form-group{display: inline-block; padding-right: 10px;}
</style>
<script type="text/javascript">
function submitAdsForm(){
	$('#hid_val').val('active');
	$('#video_frm').attr('action','<?= Yii::app()->getBaseUrl(true)."/ads";?>');
	$('#video_frm').submit();
}
<?php
$ad = 'spot';
if($ad_type == 1 || $ad_type == 2)
{
    if($ad == 'yume')
    {
    ?>
        var ad_url = "https://shadow01.yumenetworks.com/dynamic_preroll_playlist.vast2xml?domain=1552hCkaKYjg";    
    <?php
    }
    else
    {
    ?>
        //
        var is_mobile = "<?php echo Yii::app()->common->isMobile(); ?>";
        if(is_mobile == 1)
            var ad_url = "https://search.spotxchange.com/vast/2.00/79391?VPI=MP4";
        else
            var ad_url = "https://search.spotxchange.com/vast/2.00/79391?VPAID=0&content_page_url=" + encodeURIComponent(window.location.href) + "&cb=" + Math.random() + "&player_width=" + $('#muvivideo').innerWidth() + "&player_height=" + $('#muvivideo').innerHeight();
    <?php     
    }
}
if ($ad_type == 1) {
    ?>
        var player = videojs('muvivideo');
        player.ads();
        player.vast({
            skip: 5,
            url: ad_url
        });
    <?php
} 
else if ($ad_type == 2) {
    ?>
        var player = videojs('muvivideo');
        var state = {};
    <?php
    if ($roll_after < 282) {
        ?>
            var midrollPoint = <?php echo $roll_after; ?>;


            player.on('timeupdate', function (event) {

                if (state.midrollPlayed) {
                    return;
                }

                var currentTime = player.currentTime(), opportunity;

                if ('lastTime' in state) {
                    opportunity = currentTime > midrollPoint && state.lastTime < midrollPoint;
                }

                state.lastTime = currentTime;
                if (opportunity) {
                    state.midrollPlayed = true;

                    player.ads();
                    player.vast({
                        skip: 5,
                        url: ad_url
                    });
                }

            });   
            
            player.one('adended', function () {
                // play your linear ad content, then when it's finished ...
                player.ads.endLinearAdMode();
                state.adPlaying = false;

                player.play();
                player.on("loadeddata", function () {
                    player.currentTime(midrollPoint);
                });
                                        
            });            


            var pad = function (n, x, c) {
                return (new Array(n).join(c || '0') + x).slice(-n);
            };
            var padRight = function (n, x, c) {
                return (x + (new Array(n).join(c || '0'))).slice(0, n);
            };
            videojs.Html5.Events.concat(videojs.Html5.Events.map(function (evt) {
                return 'ad' + evt;
            })).concat(videojs.Html5.Events.map(function (evt) {
                return 'content' + evt;
            })).concat([
                // events emitted by ad plugin
                'adtimeout',
                'contentupdate',
                'contentplayback',
                // events emitted by third party ad implementors
                'adsready',
                'adscanceled',
                'adstart', // startLinearAdMode()
                'adend'     // endLinearAdMode()

            ]).filter(function (evt) {
                var events = {
                    progress: 1,
                    timeupdate: 1,
                    suspend: 1,
                    emptied: 1,
                    contentprogress: 1,
                    contenttimeupdate: 1,
                    contentsuspend: 1,
                    contentemptied: 1,
                    adprogress: 1,
                    adtimeupdate: 1,
                    adsuspend: 1,
                    ademptied: 1
                }
                return !(evt in events);

            }).map(function (evt) {
                player.on(evt, function (event) {
                    var d, str, li;

                    li = document.createElement('li');

                    d = new Date();
                    d = '' +
                            pad(2, d.getHours()) + ':' +
                            pad(2, d.getMinutes()) + ':' +
                            pad(2, d.getSeconds()) + '.' +
                            pad(3, d.getMilliseconds());

                    if (event.type.indexOf('ad') === 0) {
                        li.className = 'ad-event';
                    } else if (event.type.indexOf('content') === 0) {
                        li.className = 'content-event';
                    }

                    str = '[' + (d) + '] ' + padRight(19, '[' + (event.state ? event.state : player.ads.state + '*') + ']', ' ') + ' ' + evt;

                    if (evt === 'contentupdate') {
                        str += "\toldValue: " + event.oldValue + "\n" +
                                "\tnewValue: " + event.newValue + "\n";
                        li.className = 'content-adplugin-event';
                    }
                    if (evt === 'contentplayback') {
                        li.className = 'content-adplugin-event';
                    }

                    console.log(str);
                });
            });

        <?php
    }
}
?>

    jQuery.validator.addMethod("vaidHHMMSS", function (value, element) {
        // allow any non-whitespace characters as the host part
        return this.optional(element) || /^\d{2}:\d{2}:\d{2}$/.test(value);
    }, 'Please enter time in correct format.');

    jQuery.noConflict();
    jQuery(document).ready(function () {

        $('input:radio[name="video_type"]').change(
        function(){
            if ($(this).is(':checked') && $(this).val() == 2) {
                $('#midroll_time').val('00:01:00');
            }
            else
            {
                $('#midroll_time').val('');
            }
        });        
        
        jQuery('#video_frm').validate({
            rules: {
                video_type: "required",
                midroll_time: {
                    required: {
                        depends: function (element) {
                            return (jQuery('input:radio[name=video_type]:checked').val() == 2);
                        }
                    },
                    vaidHHMMSS: true
                },
            },
            messages: {
                video_type: "Please choose one of the options!",
            }
        });
    });

</script>