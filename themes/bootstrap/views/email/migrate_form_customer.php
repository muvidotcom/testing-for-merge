<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
   <tbody>
      <tr>
         <td>
            <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                <tbody>
                  <tr style="background-color:#f3f3f3">
                     <td style="text-align:left;padding-top:10px"><span mc:edit="logo"><?php echo $params['logo']; ?></span></td>
                  </tr>
                </tbody>
            </table>
            <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
            <tbody>
                  <tr>
                     <td>
                        <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                           <p style="display:block;margin:0 0 17px">Hi <?php echo $params['title']; ?>,<br><br/> 
                              Thank you for your interest in migrating to Muvi. 
                           </p>
                           <p style="display:block;margin:0 0 17px">  
                              Someone from our sales team will reach out to you within the next 48 hours to discuss further steps and answer any questions or queries you may have on the migration process. 
                           </p>
                           <p style="display:block;margin:0 0 17px">
                               Meanwhile please feel free to reply to this email or write to <a alt="email" href="mailto:support@muvi.com">support@muvi.com</a> in case you need any further assistance or information.
                           </p>
                           <p style="display:block;margin:0 0 17px">
                           You can sign up for a free trial with Muvi <a alt="sign up" href="https://www.muvi.com/signup">here</a>. 
                           </p>
                           <p style="display:block;margin:0">
                              Regards,<br>
                              Muvi Sales
                           </p>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>