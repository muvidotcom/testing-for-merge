<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $params['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Hi,
                            </p> 
			   <p style="display:block;margin:0 0 17px">
				Congratulations!
                            </p>              
				<p style="display:block;margin:0 0 17px">
                                 <strong><span mc:edit="name"><?php echo $params['name']; ?></span></strong> has purchased a subscription to <span mc:edit="package_name"><?php echo $params['package_name']; ?></span>. Below are the details of the user
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                
                                Name: <strong><span mc:edit="name"><?php echo $params['name']; ?></span></strong><br/>
				Email: <strong><span mc:edit="email"><?php echo $params['email']; ?></span></strong><br/>
				Phone: <strong><span mc:edit="phone"><?php echo $params['phone']; ?></span></strong><br/>
                		Company Name: <strong><span mc:edit="company"><?php echo $params['company']; ?></span></strong><br/>
                                Location: <strong><span mc:edit="location"><?php echo $params['location']; ?></span></strong><br/>
                                Lable Plan: <strong><span mc:edit="package_name"><?php echo $params['package_name']; ?></span></strong><br/>
                                Plan: <strong><span mc:edit="plan"><?php echo $params['plan']; ?></span></strong><br/>
                                Amount charged: <strong><span mc:edit="amount_charged"><?php echo $params['amount_charged']; ?></span></strong><br/>
                                Payment Method: <strong><span mc:edit="payment_method"><?php echo $params['payment_method']; ?></span></strong><br/>
                                <span mc:edit="cloud_hosting"><?php echo $params['cloud_hosting']; ?></span>
                                <!--Trial Period Expires: <strong><span mc:edit="expiry_date"></span></strong><br/> -->
                            </p>
 
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;width:70%;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $params['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $params['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $params['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>