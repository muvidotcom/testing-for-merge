<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
    <p style="display:block;margin:0 0 17px">
        A new ticket is added.
    </p> 
    <p style="display:block;margin:0 0 17px">
        <strong>Ticket : </strong>  <span mc:edit="ticket_id"><?php echo $params['admin_url']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Customer : </strong>  <span mc:edit="name"><?php echo $params['name']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Customer Type : </strong>  <span mc:edit="type"><?php echo $params['customer_type']; ?> <?php echo $params['highvalue']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Priority : </strong>  <span mc:edit="priority"><?php echo $params['priority']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>App : </strong>  <span mc:edit="app"><?php echo $params['app']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Title : </strong>  <span mc:edit="title"><?php echo $params['title']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Description : </strong>  <span mc:edit="description"><?php echo $params['description']; ?></span>
    </p>
</div>