<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Dear <span mc:edit="fname"></span>,
                            </p>              
							<p style="display:block;margin:0 0 17px">
                                Congratulations on your first step towards launching your own VoD platform!
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                We noticed that you have signed up for our FREE TRIAL, but have not completed the account setup. Let us know if you require any clarification and need help in setting up your account, please note that your credit card is NOT charged till the end of your FREE TRIAL and you can cancel anytime from the Admin panel itself. Alternatively, you can try the following demo account.
				<br/>				
URL: <a href="http://www.muvi.com/" target="_blank" style="text-decoration:none;display:inline;">www.muvi.com</a>
				<br/>
                            </p>
		           <p style="display:block;margin:0 0 17px">
                                Please note that the demo account is shared with others, therefore, any data you upload may be seen and deleted after a while. We reset password and clean up all data in the demo account once a week.
                            </p>
                            <p style="display:block;margin:0 0 17px">
                            	Muvi team is always here to help if you have any questions or concerns. Just reply to this email and a Muvi representative will reply you right away.
                            </p>
                        	<p style="display:block;margin:0 0 17px">
				Login to Muvi anytime to start where you left off. Your login is your email address, <span mc:edit="email"></span>
                        	</p>
                        	<p style="display:block;margin:0 0 17px;background-color:#42B7DA;width:70%;text-align:center;font-weight:bold;">
                        		<a href="http://www.muvi.com/" style="color:#fff;text-decoration:none;display:block">Log in to Admin Panel</a>
                        	</p>
                        	</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:70%;">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"></span>&nbsp;<span mc:edit="gplus_link"></span>&nbsp;<span mc:edit="twitter_link"></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>