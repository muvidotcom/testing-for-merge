<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	 Hi <span mc:edit="name"><?php echo $params['name']; ?></span>
                            </p>              
							
                            <p style="display:block;margin:0 0 17px">
                             A note Has been added.
                            </p>
                              <p style="display:block;margin:0 0 17px">
                             	Ticket details:-
                            </p>
                              <p style="display:block;margin:0 0 17px">
                                  <b>Priority</b>:Medium
                            </p>
                            <p style="display:block;margin:0 0 17px">
                            	 <b>Description</b>:<span mc:edit="description"><?php echo $params['description']; ?></span>
                            </p>
     <p style="display:block;margin:0 0 17px">
                            	 
         You will hear from Muvi Support team soon. Please find updates to your 			ticket <span mc:edit="url1"><?php echo $params['url1']; ?></span>. Add an update to the ticket by logging into <span mc:edit="url2"><?php echo $params['url2']; ?></span>, or simply replying to this email.
                            </p>

                            <p style="display:block;margin:0 0 17px">
                                Regards,<br/>
                                Muvi Team
                            </p>
 
						</div>