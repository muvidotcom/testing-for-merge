<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:none;background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
							<p style="display:block;margin:0 0 17px">
                                <strong>Dear <span mc:edit="name"></span></strong>,<br><br/>                               
                                    Thank you for registering at <span mc:edit="site_link"></span>.</p>
                            <p style="display:block;margin:0 0 17px">If you have any questions, please reply to this email and one of our team members will reply to you ASAP.</p>
							<p style="display:block;margin:0">
                                <strong>Thanks,</strong><br>
								<span mc:edit="site_link"></span>
							</p>
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"></span>&nbsp;<span mc:edit="gplus_link"></span>&nbsp;<span mc:edit="twitter_link"></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>