<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
    <p style="display:block;margin:0 0 17px">
        Your support ticket <span mc:edit="ticket_id"><?php echo $params['store_url']; ?></span> is created.
    </p>              
    <p style="display:block;margin:0 0 17px">
        <strong>Title : </strong>  <span mc:edit="title"><?php echo $params['title']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        <strong>Description : </strong>  <span mc:edit="description"><?php echo $params['description']; ?></span>
    </p>
    <p style="display:block;margin:0 0 17px">
        You will hear from Muvi Support team soon. You can add an update to the ticket by logging into <a href="https://www.muvi.com/">Muvi</a> or simply replying to this email.
    </p>
    <p>Regards, <br/> Muvi Support</p>
</div>