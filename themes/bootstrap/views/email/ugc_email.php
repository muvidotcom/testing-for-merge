<?php
if (!empty($params)) {
    if ($params['type'] == 'approve_content') {
        ?>
        <table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
            <tbody>
                <tr>
                    <td align="left" style="padding-left:20px;"><?php echo $params['logo']; ?></td>
                </tr>
                <tr>
                    <td>
                        <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                            <p style="display:block;margin:0 0 17px">
                                                Hi <?php echo $params['name']; ?>, 
                                            </p>
                                            <p style="display:block;margin:0 0 17px">
                                                Your content <b>"<?= $params['content_name'] ?>"</b> has been approved successfully.
                                            </p>              
                                            <p style="display:block;margin:0 0 17px">
                                                Please <a href="<?php echo $params['movie_url']; ?>">click here</a> to view your content
                                            </p>

                                            <p style="display:block;margin:0 0 17px">
                                                Regards,<br/><?php echo $params['studio_name']; ?>
                                            </p>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    } else if ($params['type'] == 'delete_content') {
        ?>
        <table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
            <tbody>
                <tr>
                    <td align="left" style="padding-left:20px;"><?php echo $params['logo']; ?></td>
                </tr>
                <tr>
                    <td>
                        <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                            <p style="display:block;margin:0 0 17px">
                                                Hi <?php echo $params['name']; ?>, 
                                            </p>
                                            <p style="display:block;margin:0 0 17px">
                                                Your content <b>"<?= $params['content_name'] ?>"</b> has been deleted by admin.
                                            </p>              
                                            <p style="display:block;margin:0 0 17px">
                                                Please <a href="<?php echo $params['ugc_listing_url']; ?>">click here</a> to view your existing content
                                            </p>

                                            <p style="display:block;margin:0 0 17px">
                                                Regards,<br/><?php echo $params['studio_name']; ?>
                                            </p>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        <?php
    }
}
?>