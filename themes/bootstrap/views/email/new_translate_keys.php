<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
                        <td style="text-align:left;padding-top:10px"><span mc:edit="logo"><a href="<?php echo $params['site_link']; ?>"><img src="<?php echo EMAIL_LOGO; ?>"></a></span></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
							<p style="display:block;margin:0 0 17px">
								Hi <?php echo $params['fname']; ?>,<br/><br/> 
                                The following new strings have been added for your store, please add the translated text for the same from the CMS.</p>
                            <span mc:edit="msg"><?php echo $params['msg']; ?></span>
                            <p style="display:block;margin:0">Browse to 'Settings -> Language -> Translations' to view and add translated text for these strings.</p>
							<p style="display:block;margin:0">
								Thanks,<br>
								Muvi
							</p>
						</div>
					</td>
				</tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>