<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
    <tr>
        
        <td align="left" style="padding-left:20px;"><img src="<?php echo EMAIL_LOGO; ?>"/></td>
            </tr>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	A new website has gone live. Below are the details of the website:
                            </p>              
                            <p style="display:block;margin:0 0 17px">
                                <b><span mc:edit="domain_name">Website:</span></b> <?php echo $params['domain_name']; ?>
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                <b><span mc:edit="studio_name">Company Name:</span></b> <?php echo $params['studio_name']; ?>
                            </p>
                            <p style="display:block;margin:0 0 17px">
                                Regards,<br/>
                                Muvi Team
                            </p>
 
						</div>
					</td>
				</tr>
                 
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>