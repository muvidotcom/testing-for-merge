<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:none;background-repeat:repeat-x" width="100%">
    <tbody>
        <tr>
            <td>
                <table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
                    <tbody>
                        <tr style="background-color:#f3f3f3">
                            <td style="text-align:left;padding-top:10px">
                                <div mc:edit="logo"><?php echo $params['logo']; ?></div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
                    <tbody>
                        <tr>
                            <td>
                                <div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                                    <p style="display:block;margin:0 0 17px">
                                    Hi <?php echo @$params['studio_name'];?>,
                                    </p>
                                    <div mc:edit="mailcontent">A user has requested to delete a device. Please find below the details: <br>
										User Email: <strong><span mc:edit="user_email"><?php echo $params['email']; ?></span></strong><br/>                                                                  
                                        Device Id: <strong><span mc:edit="device_id"><?php echo $params['device']; ?></span></strong><br/>
										Device Info: <strong><span mc:edit="device_info"><?php echo $params['device_info']; ?></span></strong><br/>
                                    </div>
                                    <p style="display:block;margin:20px 0 17px">Regards,<br/>Team Muvi</p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>