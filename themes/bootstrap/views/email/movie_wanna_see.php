<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Muvi</title>
</head>
<body>
 <table height="400px" width="700px" bgcolor="#f0f0f0" align="center" border="0">
         <tr height="50px">
             <td width="50"></td>
             <td width="650" align="left"><a href="http://www.muvi.com"><img src="http://www.muvi.com/images/new_logo.png"/></a></td>
            </tr>
         <tr height="300px">
             <td colspan="2">
                 <table border="0" align="center" width="600px" height="300px" bgcolor="#FFFFFF">
                    <tr>
                        <td style="height:50px;font-size:18px;font-family:Arial, Helvetica, sans-serif">
                            Hi <span mc:edit="user_name"></span>,
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;font-family:Arial, Helvetica, sans-serif">
                            <span mc:edit="subject_line"></span>
                        </td>
                    </tr>
                     <tr>
                        <td style="font-size:14px;font-family:Arial, Helvetica, sans-serif">
                     		<span mc:edit="story_block"></span>
                         </td>
                     </tr>
                     <tr>
                        <td style="padding-bottom:25px;font-size:14px;font-family:Arial, Helvetica, sans-serif">
                            <span mc:edit="admin_content"></span>
                        </td>
                    </tr>
                     <tr>
                         <td>
                             <table border="0" style="padding:0; margin:0; vertical-align:text-top;">
                                 <tr>
                                     <td>
                                      <table width="600px" border="0" align="left" style="padding:0; margin:0;">
                                             <tr>
                                                 <td width="12%" rowspan="3"><span mc:edit="poster_block"></span></td>
                                                 <td height="29" colspan="2"><font size="+2" face="Arial, Helvetica, sans-serif" color="#494949"><span mc:edit="name"></span></font></td>
                                                </tr>
                                                <tr>
                                                    <td height="23" colspan="2"><font color="#5F5F5F" face="Arial, Helvetica, sans-serif" size="-1"><span mc:edit="cast1"></span>,&nbsp;<span mc:edit="cast2"></span>   </font></td>
                                            </tr>
                                                <tr>
                                                    <td width="12%" height="30">&nbsp;</td>
                                                    <td><table width="20%">	<tr>
                                                        <td width="132px" bgcolor="#0099FF" align="center"><span style="padding:5px 10px; width:75px; font:normal 14px Arial, Helvetica, sans-serif; display:block; color:#FFF; background-image: linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -o-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -moz-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -webkit-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -ms-linear-gradient(bottom, rgb(0,134,212) 10%, rgb(1,151,238) 100%); background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.1, rgb(0,134,212)), color-stop(1, rgb(1,151,238))); border-radius:4px; border:solid 1px #0086d4; text-decoration:none;"><span mc:edit="wanna_see_btn"></span></span></td></tr></table></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                         <td></td>
                      </tr>
                     	<tr>
                         <td height="20">
                             <span style="font:normal 10px Arial, Helvetica, sans-serif; display:block; color:#494949;">If you don't want to recieve email updates from Muvi, please login in to <a href="http://www.muvi.com">Muvi</a> and disable email notifications in your user settings at the top right corner of the screen.</span>
                         </td>
                      </tr>
                  </table>
                </td>
            </tr>
            <tr height="50px">
             <td colspan="2"></td>
            </tr>
        </table>
</body>
</html>