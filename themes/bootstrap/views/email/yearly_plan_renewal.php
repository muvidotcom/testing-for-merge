<table style="border:1px solid #ddd;padding:13px 0 20px;font-family:helvetica,Arial;font-size:13px;text-align:center;background-color:#f3f3f3;background-image:url('');background-repeat:repeat-x" width="100%">
<tbody>
	<tr>
		<td>
			<table style="font-family:helvetica,Arial;font-size:13px;margin:0 auto 13px;background-color:#f3f3f3;border-bottom-left-radius:8px;border-bottom-right-radius:8px;width:96%">
				<tbody>
					<tr style="background-color:#f3f3f3">
						<td style="text-align:left;padding-top:10px">
                            <div mc:edit="logo"><?php echo $param['logo']; ?></div>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table style="font-family:helvetica,Arial;font-size:13px;padding:20px;margin:0 auto;text-align:left;border-radius:8px;background-color:#ffffff;border:1px solid #ddd;width:96%">
			<tbody>
				<tr>
					<td>
						<div style="color:#555;font-size:14px;line-height:1.8em;text-align:left">
                            <p style="display:block;margin:0 0 17px">
                            	Dear <span mc:edit="fname"><?php echo $param['fname']; ?></span>,
                            </p>              
                            <p style="display:block;margin:0 0 17px">
                                This is a <span mc:edit="cntnt_msg"><?php echo $param['cntnt_msg']; ?></span> notification that your yearly payment for <span mc:edit="package_name"><?php echo $param['package_name']; ?></span> will be auto-renewed on <span mc:edit="next_billing_date"><?php echo $param['next_billing_date']; ?></span>. Your credit card on account will be charged an amount of $<span mc:edit="total_amount_to_be_charged"><?php echo $param['total_amount_to_be_charged']; ?></span> on this date. Please contact your Muvi representative if you have any questions regarding this.
                            </p>
						</div>
					</td>
				</tr>
                 <tr>
                    <td>
                        <table style="width:100%;font-family:helvetica,Arial;">
                            <tbody>
                                <tr>
                                    <td style="color:#555;font-size:14px;line-height:1.8em;text-align:left;width:70%;">
                                        Regards,
                                        <p style="font-size:14px;margin:2px 0px">
                                            Team Muvi
                                        </p>
                                    </td>
                             		<td style="width:25%">
                                        <p style="font-size:13px;margin:10px 0px">
                                        <span mc:edit="fb_link"><?php echo $param['fb_link']; ?></span>&nbsp;<span mc:edit="gplus_link"><?php echo $param['gplus_link']; ?></span>&nbsp;<span mc:edit="twitter_link"><?php echo $param['twitter_link']; ?></span>
                                        </p>    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
      </td>
    </tr>
  </tbody>
</table>