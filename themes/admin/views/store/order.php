<style type="text/css">
    .dropbox{
        display:none;
    }
</style>
<?php
    $sort = "fa fa-sort";
    if (isset($sortBy)) {
        if ($sortBy == 'grand_total_desc') {
            $sort = "fa fa-sort-desc";
        } elseif ($sortBy == 'grand_total_asc') {
            $sort = "fa fa-sort-asc";
        }
        $sortby = $sortBy;
    }else{
        $sortby = 'grand_total_desc';
    }
?>
<?php $url = $this->createUrl('store/order'); ?>
<div class="row m-b-40">
    <div class="col-xs-4">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Export to CSV</button> 
        <button class="btn btn-primary primary m-t-10" id="open-download-modal">Download Personalized Orders</button>
    </div>
</div>
<div class="filter-div">
    <div class="row">
        <div class="col-md-9 m-b-10">
            <form id="searchform" action="<?php echo $url; ?>" name="searchForm" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group  input-group">
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" id="search" placeholder="Enter order number,item name or customer name" value="<?php echo @$searchText; ?>" name="search">
                                <input type="hidden" name="sortBy" id="sortBy" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table" id="example">
            <thead>
                <?php if ($items_count >= 3) { ?>
                    <tr>
                        <td colspan="8" class="text-right">
                            <?php
                            $opts = array('class' => 'pagination m-t-0 m-b-40');
                            $this->widget('CLinkPager', array(
                                'currentPage' => $pages->getCurrentPage(),
                                'itemCount' => $items_count,
                                'pageSize' => $page_size,
                                "htmlOptions" => $opts,
                                'maxButtonCount' => 6,
                                'nextPageLabel' => '&raquo;',
                                'prevPageLabel' => '&laquo;',
                                'selectedPageCssClass' => 'active',
                                'lastPageLabel' => '',
                                'firstPageLabel' => '',
                                'header' => '',
                            ));
                            ?>

                        </td>
                    </tr>
                <?php }
                ?>
                <tr>
                    <th>Order Number</th>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Item Name</th>
                    <th data-hide="phone" id='grand_total' onclick="sortby('<?php echo $sortby;?>');" class="sortcolumn <?php if ($sortBy == 'grand_total_desc' || $sortBy == 'grand_total_asc') { ?> sortactive<?php } ?>">
                        <i class="<?php echo $sort; ?>"></i> 
                        Total Amount
                    </th>
                </tr>	
            </thead>
            <tbody>
                <?php
                $first = $cnt = 1;
                if ($data) {
                    $first = $cnt = (($pages->getCurrentPage()) * 3) + 1;
                    foreach ($data AS $key => $details) {
                        $cnt++;
                        ?>
                        <tr>
                            <td>
                                <a href=<?php echo Yii::app()->baseUrl; ?>"/store/orderdetails/id/<?php echo $details['id']; ?>">#<?php echo $details['order_number']; ?></a>
                                <?php
                                if($_SERVER['REMOTE_ADDR']=='111.93.166.194'){
                                    echo '<br />CDS-' . $details['cds_order_status'];
                                }
                                ?>
                            </td>
                            <td><?php echo $details['date'] ? date('jS M ,Y', strtotime($details['date'])) : 'NA'; ?></td>   
                            <td><?php echo $details['customer_name']; ?></td>
                            <td> 
                                <?php
                                foreach ($details['details'] as $key => $value) {
                                    if($details['cds_order_status']=='-1'){
                                        $revstatus = "Payment confirmation not received from Gateway";
                                    }else if($details['payment_status']=='Pending'){
                                        $revstatus = "Pending Status not updated ";
                                    }else{
                                        $revstatus = $status[$value['item_status']];
                                    }
                                    //$revstatus = ($details['cds_order_status']!='-1')?$status[$value['item_status']]:'Payment confirmation not received from Gateway';
                                    echo 'NAME-' . $value['name'] . '<br/>';
                                    echo 'PRICE- ' . Yii::app()->common->formatPrice($value['price'], $details['currency_id']) . '<br/>';
                                    echo 'QUANTITY-' . $value['quantity'] . '<br/>';
                                    echo 'STATUS- <b>' .$revstatus. '</b><br/>';
                                }
                                ?>  
                            </td>
                            <td> <?php echo Yii::app()->common->formatPrice($details['total_amount'], $details['currency_id']); ?>    </td>     
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="5">No Record found!!!</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<style>
    .comiseo-daterangepicker-triggerbutton {
        background: #fff none repeat scroll 0 0;
        border-bottom: 1px solid #d3d3d3;
        border-top:0px !important;
        border-left:0px !important; 
        border-right:0px !important;
        border-radius: 0px !important;
        color: #333;
        font-weight: normal;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #d3d3d3;
        color: #333;
        font-weight: normal;
    }    
</style>
<div class="modal fade bs-example-modal-lg" id="download-modal-popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Select Date Range<button type="button" class="close pull-right" onclick="closeDownloadModal()"><span aria-hidden="true">&times;</span></button></h4>


            </div>
            <div class="modal-body">  
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Date: </label>
                                <div class="col-sm-10">
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                                        <div class="fg-line">
                                            <div class="select">
                                                <input class="form-control" type="text" id="revenue_date" name="searchForm[revenue_date]" value='<?php echo @$dateRange; ?>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="download-btn" onclick="downloadPersonalizeData();">Submit</button>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    function sortby(sortby) {
        $('#sortBy').val(sortby);
        $('#searchform').submit();
    }
    $('.report-dd').click(function(){
        var searchText = $('#search').val();
        var type = $(this).val();
        if(type == 'csv'){
            /*window.location = '<?php echo Yii::app()->baseUrl."/store/exportOrder?type=";?>'+type+'&search_text='+searchText; */
            window.location = '<?php echo Yii::app()->baseUrl."/store/exportOrders";?>';
        }
    });
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script src="../../common/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"--->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    function closeDownloadModal(){
        $('#download-modal-popup').modal('toggle');
        $("#revenue_date").daterangepicker("close");
    }
    
    $(function() {
        $('#open-download-modal').on('click', function(){
            $('#download-modal-popup').modal({
                backdrop: 'static',
                keyboard: false
            });
        }) 
        
        loadDateRangePicker('revenue_date','2015-10-05');
    });
    
    function downloadPersonalizeData(){
        var dateRange = $('#revenue_date').val();
        if(dateRange === ''){
            swal('Select date range');
        } else {
            getRangeData(dateRange);
        }
    }
    
    function getRangeData(date_range){
        $('#download-btn').html('Wait...');
        $('#download-btn').attr('disabled', true);
        $.ajax({
            method: 'post',
            dataType: 'json',
            contentType: 'json',
            data: date_range,
            url: '<?php echo Yii::app()->baseUrl."/store/downloadPersonalizeOrders"?>',
            success: function(data){
                $('#download-btn').html('Submit');
                $('#download-btn').removeAttr('disabled');
                closeDownloadModal();
                if(data.action === 'success'){
                    window.location = data.message;
                } else {
                    swal(data.message);
                }
            }
        });
    }
    
</script>