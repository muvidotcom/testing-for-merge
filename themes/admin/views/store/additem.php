<style>
    .upload-Image {
        height: 60px;
        width: 60px;
        background-color: #fff;
        border-color: #fff;
        display: inline-flex;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
    }      
    .upload-Image em{
        font-size: 1.5em;
        top: 62%;
        position: absolute;
        margin-top: -17px;
        left: 60%;
        margin-left: -17px;
        color:#2cb7f6;
    }
    .upload-Image:hover{ 
        background-color: #edf1f2;
    }
    .upload-Image:hover em{
        color: #0aa1e5;
    }
    .btn.upload-Image.relative {
        margin-top: 2px;
        float: left;
    }

    ul.sortable {width: 100%; float: left; margin: 20px 0; list-style: none; position: relative !important;}
    ul.sortable li {height: 107px; float: left;  border: 2px solid #fff; cursor: move;}
    ul.sortable li.ui-sortable-helper {border-color: #3498db;}
    ul.sortable li.placeholder {width: 107px; height: 107px; float: left; background: #eee; border: 2px dashed #bbb; display: block; opacity: 0.6; border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px; }    

    .loading_div{display: none;}      
    .jcrop-keymgr{display:none !important;}    
    .addmore-content{display:none;}
    .fixedWidth--Preview img {max-width: none !important;}
    .pg-thumb-img a em{top: 2px;right: 0;z-index: 9;}
    .pg-thumb-img img.active{border:2px solid #3498db; opacity: 0.8; box-shadow: 3px 3px 3px #d2cfcf;}
    #avatar_preview_div {
        position: relative;
    }
    #avatar_preview_div .faded-area {
        background-color: rgba(255, 255, 255, 0.6);
        position: absolute;
        height: 100%;
        z-index:999;
        width: 100%;
        display: none;
    }
    #avatar_preview_div .loading-img {
        background: transparent url('/img/loading.gif') center center no-repeat;
        position: absolute;
        top: 45%;
        width: 100%;
    }
</style>
<script>
    var userinternetSpeed = "<?php echo  @$_SESSION['internetSpeed']?>"; 
    var video_upload_type = 'physical';
</script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
<div id="success_msg"></div>
<input type="hidden" name="is_multi_content" id="is_multi_content" value="<?= @$is_multi_content; ?>" />
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
$vers = RELEASE;
$enable_lang = 'en';
if (isset($fieldval[0]['id'])) {
    $enable_lang = $this->language_code;
	if ($_COOKIE['Language']) {
		$enable_lang = $_COOKIE['Language'];
	}
    $disable = 'disabled="disabled"';
    if (@$fieldval[0]['language_id'] == $this->language_id && @$fieldval[0]['parent_id'] == 0) {
        $disable = "";
    }
}
?>

<form role="form" method="post" enctype="multipart/form-data" id="itemform" name="itemform" class="form-horizontal">
<input type="hidden" value="<?php echo @$fieldval[0]['id']; ?>" name="pg[id]"  />
    <div class="row m-t-40">
        <div class="col-md-4 col-sm-12 pull-right--desktopOnly">
            <div class="Block">
                <div class="Block-Header">
                    <?php                    
                    $dmn = $customData['formData']['poster_size'];
                    if($dmn){
                     $expl = explode('x', strtolower($dmn));
                        $cropDimesion = array('width' => @$expl[0], 'height' => @$expl[1]);
                    }else{
						$cropDimesion = Yii::app()->common->getPgDimension();
                    }
                    ?>
                    <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth2" id="reqwidth2" />
                    <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight2" id="reqheight2" />   
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Upload Poster</h4>
                </div>
                <hr>

                <div class="border-dotted m-b-40">
                    <div class="text-center">
                        <input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target="#myPGLargeModalLabel" onclick="openPGImageModal(this)" value="Browse">

                        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'X' . $cropDimesion['height']; ?></span></h5>
                    </div>
                    <div class="text-center">
                        <div  style="width:<?php echo $cropDimesion['width']; ?>px; margin:0 auto;">
                        <div class="m-b-10 displayInline fixedWidth--Preview">
                            <?php
                            $no_image_array = '/img/No-Image-Vertical.png';
                            if (!in_array($posterImg, $no_image_array)) {
                                ?>
                                <div class="poster-cls  avatar-view jcrop-thumb">
                                    <div id="avatar_preview_div">    
                                        <?php if(@$fieldval[0]['id']==''){
                                            $posterImg = POSTER_URL . '/no-image-a.png';
                                        }else{
                                            $posterImg = $pgposter[0]['poster'];
                                        }?>
                                        <?php if (strpos($posterImg, 'no-image') > -1) { ?>
                                            <img id="preview_content_img" data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" />
                                        <?php } else { ?>
                                            <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>" style="width: <?php echo $cropDimesion['width']; ?>px;">
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php //echo $this->renderPartial('//layouts/image_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url)); ?>
                                <?= $this->renderPartial('//layouts/image_pg_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url)); ?>                            

                            <?php } else { ?>
                                <div class="poster-cls  avatar-view jcrop-thumb">
                                    <div id="avatar_preview_div">
                                        <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $posterImg; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
                        <?php /*if(@$fieldval[0]['id'] && !(strpos($posterImg, 'no-image-a') > -1) ){
                        if($disable == ""){
                        ?>
                            <div class="caption">
                            <a id="remove-poster-text" data-poster-id="<?php echo $pgposter[0]['id'];?>" class="btn remove_btn" href="javascript:void(0);" onclick="return removePGposter('<?php echo @$fieldval[0]['id']; ?>', this);" ><em class="icon-close small-icon"></em>&nbsp;&nbsp;Remove Poster</a>
                        </div>
                    <?php } } */?>
                        <div class="clear-fix"></div>
                        <div class="text-left">
                        <?php
                        foreach ($pgposter as $pgThumb) {
                            if (!(strpos($pgThumb['poster'], 'no-image-a') > -1)) {
                                $thumbPoster = str_replace('standard/', 'thumb/', $pgThumb['poster']);
                                ?>
                                <div class="displayInline pg-thumb-img relative" id="thumb-<?php echo $pgThumb['id']; ?>">
                                    <a href="javascript:;" data-poster-id="<?php echo $pgThumb['id']; ?>" onclick="return removePGposter('<?php echo @$fieldval[0]['id']; ?>', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>
                                    <a href="javascript:void(0);" data-poster-id="<?php echo $pgThumb['id']; ?>" data-product-id="<?php echo $fieldval[0]['id']; ?>" onclick="changePgPoster(this)" id="" class="btn upload-Image relative" style="padding: 0;">
                                        <img src="<?php echo $thumbPoster; ?>" width="100%" />
                                    </a>
                                </div>
                            <?php }
                        } ?>
                        <div class="displayInline">
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#myPGLargeModalLabel" data-name="Poster" onclick="openPGImageModal(this)" id="add-more-pg-image" class="btn upload-Image relative">
                                <em class="icon-plus absolute" title="Add Poster"></em>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-info icon left-icon "></em>
                    </div>
                    <h4>Basic Information</h4>
                </div>
                <hr>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="loading_div">
                            <div class="preloader pls-blue preloader-sm">
                                <svg viewBox="25 25 50 50" class="pl-circular">
                                <circle r="20" cy="50" cx="50" class="plc-path"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="formContents">
                    <?php 
                        if(@$customData){
                            $defaultFields = array('name','description','sale_price','sku','product_type');
                            $relationalFields = Yii::app()->Helper->getRelationalPGField(Yii::app()->user->studio_id); 
                            $formData = $customData['formData'];
                            unset($customData['formData']);
                            foreach ($customData AS $ckey=>$cval){
                                if(!in_array($cval['f_name'],$defaultFields)){
                                    if($relationalFields && array_key_exists($cval['f_name'],$relationalFields)){
                                        $cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
                                    }
                                }
                                $active_fields[] = $cval['f_name'];
                                ?>
                            <div class="form-group">
                                <label for="<?= $cval['f_display_name'];?>" class="col-md-4 control-label"><?= $cval['f_display_name'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
                                <?php if(!$cval['f_type']){?>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <?php if($cval['f_name']=='sale_price'){?>
                                            <div class="moreCurrencyDiv">
                                                <?php
                                                if(count($currency_code) > 1){
                                                    $pgcurrenyname = "pg[multi_currency_id][]";
                                                    $pgsalepricename = "pg[multi_sale_price][]";
                                                }else{
                                                    $pgcurrenyname = "pg[currency_id]";
                                                    $pgsalepricename = "pg[sale_price]";                                                    
                                                }
                                                if((count($currency_code) > 1) && !empty($pgmulti)){
                                                    $counter = 1;
                                                    foreach ($pgmulti as $mkey => $multivalue) {?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                                                <?php 
                                                                    foreach ($currency_code as $key => $value) {
                                                                            if(@$mkey==$value["id"]){
                                                                                echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                            }else{
                                                                                echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                            }                                                                                    
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="fg-line">
                                                        <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01">
                                                        <span class="countdown1"></span>
                                                    </div>
                                                </div>                                                            
                                                <div class="col-md-4">                                                    
                                                    <div class="fg-line">
                                                        <h5>
                                                            <?php if($counter==1){?>
                                                            <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">
                                                                &nbsp; Add more currency
                                                            </a>
                                                            <?php }else{?>
                                                            <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                                                                &nbsp; Remove currency
                                                            </a>
                                                            <?php }?>
                                                        </h5>
                                                    </div>                                                                                                       
                                                </div>                                                            
                                            </div>
                                            <?php   $counter++;}
                                                }else{
                                            ?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                                                <?php 
                                                                    foreach ($currency_code as $key => $value) {
                                                                        if(@$fieldval[0]['currency_id']==$key){
                                                                            echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                        }else{
                                                                            echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="fg-line">
                                                        <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
                                                        <span class="countdown1"></span>
                                                    </div>
                                                </div>
                                                <?php if(count($currency_code) > 1){?>
                                                <div class="col-md-4">                                                    
                                                    <div class="fg-line">
                                                        <h5>
                                                            <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink">
                                                                &nbsp; Add more currency
                                                            </a>
                                                        </h5>
                                            </div>
                                                </div>
                                                <?php }?>
                                            </div>
                                            <?php }?>
                                        </div>
                                        <?php }else{?>
                                                <input type='text' placeholder="<?= $cval['f_value'];?>" id="<?= $cval['f_id'];?>" name="pg[<?= $cval['f_name'];?>]" value="<?php echo @$fieldval[0][$cval['f_name']]; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> >
                                        <?php }?>
                                        <?php if($cval['f_id']=='skuno'){?>
                                                <span class="red" id="email-error" style="display: none;"></span>
                                        <?php }?>        
                                        </div>
                                    </div>	
                                <?php }elseif($cval['f_type']==1){?>
                                        <div class="col-md-8">
                                            <div class="fg-line">
                                                <textarea class="form-control input-sm checkInput" rows="5" placeholder="<?= $cval['f_value'];?>" name="pg[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$fieldval[0][$cval['f_name']]; ?></textarea>
                                            </div>
                                        </div>
                                <?php }elseif($cval['f_type']==2){?>
                                        <div class="col-md-8">
                                            <div class="fg-line">
                                                <div class="select">
                                                <?php if($cval['f_name']=='size'){?>
                                                <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                    <?php
                                                        echo "<option value=''>-Select-</option>";
                                                        $opData = $shipping_size;
                                                        foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]==$opkey) {
                                                                $selectedDd = 'selected ="selected"';
                                                            }
                                                            echo "<option value='".$opkey."' ".$selectedDd." >" . $opvalue . "</option>";
                                                        }
                                                    ?>
                                                </select>
                                                <?php }else{?>
                                                <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                    <?php
                                                        echo "<option value=''>-Select-</option>";
                                                        $opData = json_decode($cval['f_value'],true);
														$opData_new = $opData;
														$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
														$opData = empty($opData)?$opData_new:$opData;
                                                        foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]==$opvalue) {
                                                                $selectedDd = 'selected ="selected"';
                                                            }
                                                            echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                                        }
                                                    ?>
                                                </select>
                                                <?php }?>
                                            </div>
                                        </div>
                                        </div>
                                <?php }elseif($cval['f_type']==3){
                                        if($cval['f_id']=='genre' || $cval['f_id']=='language' || $cval['f_id']=='censor_rating'){?>
                                        <select  data-role="tagsinput"  name="pg[<?= $cval['f_name'];?>][]" placeholder="Use Enter or Comma to add new" id="<?= $cval['f_id'];?>" multiple >
                                            <?php
                                            if (@$fieldval[0]['id'] && @$fieldval[0][$cval['f_name']]) {
                                                $pregenre = json_decode($fieldval[0][$cval['f_name']]);
                                                foreach ($pregenre AS $k => $v) {
                                                    echo "<option value='" . $v . "' selected='selected'>" . $v . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php }else{ ?>
                                            <div class="col-md-8">
                                                <select name="pg[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                                <?php
                                                    $opData = json_decode($cval['f_value'],true);
													$opData_new = $opData;
													$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
													$opData = empty($opData)?$opData_new:$opData;
                                                    foreach($opData AS $opkey=>$opvalue){
                                                        $selectedDd = '';
                                                        if (@$fieldval[0]['id'] && in_array($opvalue, json_decode($fieldval[0][$cval['f_name']],true))) {
                                                            $selectedDd = 'selected ="selected"';
                                                    }
                                                        echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                <?php	}?>
                                <?php }elseif($cval['f_type']==4){
                                if($cval['f_name']=='product_type'){?>
                                    <div class="col-md-8">
                                        <div class="control-label row">
                                            <div class="col-sm-12">
                                                <label class="radio radio-inline m-r-20">
                                                    <input type="radio" value="0" name="pg[<?= $cval['f_name'];?>]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['product_type'] == 0)?'checked=checked':'';?>>
                                                    <i class="input-helper"></i>Standalone
                                                </label>
                                                <label class="radio radio-inline m-r-20">
                                                    <input type="radio" value="1" name="pg[<?= $cval['f_name'];?>]"<?php echo (@$fieldval[0]['product_type'] == 1)?'checked=checked':'';?>>
                                                    <i class="input-helper"></i>Link to content
                                                </label>
                                            </div>
                                            <div id="showcntent" <?php echo (@$fieldval[0]['id'] != '' && @$fieldval[0]['product_type'] != 0)?'':'style="display: none;"';?> class="fg-line col-sm-12">
                                                <select id="movie_idd" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple"  >
                                                    <?php
                                                                    $movie_ids = array();
                                                                    $seleteced = '';
                                                                    if (isset($movies) && !empty($movies)) {
                                                                        foreach ($movies as $key => $value) {
                                                                            ?>
                                                                            <?php
                                                                            if (in_array($value['movie_id'], $pgcontent)) {
                                                                                $movie_ids[] = ltrim($value['movie_id'], ',');
                                                                                $seleteced = 'selected=selected';
                                                                            }
                                                                            ?>
                                                                            <option value="<?php echo $value['movie_id']; ?>" <?php echo $seleteced; ?> ><?php echo $value['movie_name']; ?></option>

                                                                            <?php
                                                                            $seleteced = '';
                                                                        }
                                                                        ?>
                                                                     <?php
                                                                    }
                                                                    ?>
                                                </select>                                    
                                            </div>
                                            <?php $all_id=implode(',',$movie_ids);  ?>
                                            <span id="error_movie_id" class="red col-sm-12"></span>
                                            <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $all_id;?>">
                                            <span class="countdown1"></span>
                                        </div>
                                    </div>
								<?php } else{?>
									<div class="col-md-8">
									<textarea class="form-control input-sm checkInput RichText" rows="5" placeholder="<?= $cval['f_value'];?>" name="pg[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$fieldval[0][$cval['f_name']]; ?></textarea>
									</div>
                                <?php } }?>
                        </div>
                        <?php }
                            //show default size field if not present
                            if(!in_array('size',$active_fields)){
                        ?>
                            <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Shipping Size:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="size" name="pg[size]" class="form-control input-sm">
                                            <?php
                                            foreach($shipping_size AS $key=>$val){
                                            ?>
                                                <option value="<?php echo $key;?>" <?php echo (@$fieldval[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php    
                            }
                        }else{?>
                    <div class="form-group" id="<?php echo @$fieldval[0]['id']; ?>_title">
                        <label for="movieName" class="col-md-4 control-label">Item Name<span class="red"><b>*</b></span>:</label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' placeholder="Enter item name here." id="mname" name="pg[name]" value="<?php echo htmlspecialchars(@$fieldval[0]['name']); ?>" class="form-control input-sm checkInput" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Description: </label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <textarea class="form-control input-sm checkInput" rows="5" placeholder="Enter item description here" name="pg[description]" id="mdescription"><?php echo @$fieldval[0]['description']; ?></textarea>
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="itemtype" class="col-md-4 control-label">Item Type: </label>
                        <div class="col-md-8">
                            <div class="control-label row">
                                <div class="col-sm-12">
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="0" name="pg[product_type]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['product_type'] == 0)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Standalone
                                    </label>
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="1" name="pg[product_type]"<?php echo (@$fieldval[0]['product_type'] == 1)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Link to content
                                    </label>
                                </div>
                                <div id="showcntent" <?php echo (@$fieldval[0]['id'] != '' && @$fieldval[0]['product_type'] != 0)?'':'style="display: none;"';?> class="fg-line col-sm-12">
                                    <select id="movie_idd" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple"  >
                                        <?php
                                                                    $movie_ids = array();
                                                                    $seleteced = '';
                                                                    if (isset($movies) && !empty($movies)) {
                                                                        foreach ($movies as $key => $value) {
                                                                            ?>
                                                                            <?php
                                                                            if (in_array($value['movie_id'], $pgcontent)) {
                                                                                $movie_ids[] = ltrim($value['movie_id'], ',');
                                                                                $seleteced = 'selected=selected';
                                                                            }
                                                                            ?>
                                                                            <option value="<?php echo $value['movie_id']; ?>" <?php echo $seleteced; ?> ><?php echo $value['movie_name']; ?></option>

                                                                            <?php
                                                                            $seleteced = '';
                                                                        }
                                                                        ?>
                                                                     <?php
                                                                    }
                                                                    ?>
                                    </select> 
                                    <?php $all_id=implode(',',$movie_ids);  ?>
                                </div>
                                <span id="error_movie_id" class="red col-sm-12"></span>
                                <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $all_id;?>">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-4 control-label">Price<span class="red"><b>*</b></span>: </label>
                        <div class="col-md-8">
                            <div class="moreCurrencyDiv">
                            <?php
                            if(count($currency_code) > 1){
                                $pgcurrenyname = "pg[multi_currency_id][]";
                                $pgsalepricename = "pg[multi_sale_price][]";
                            }else{
                                $pgcurrenyname = "pg[currency_id]";
                                $pgsalepricename = "pg[sale_price]";                                                    
                            }
                            if((count($currency_code) > 1) && !empty($pgmulti)){
                                $counter = 1;
                                foreach ($pgmulti as $mkey => $multivalue) {?>
                                    <div class="row">
                                        <div class="col-md-3">
                            <div class="fg-line">
                                <div class="select">
                                                    <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                        <?php 
                                            foreach ($currency_code as $key => $value) {
                                                                if(@$mkey==$value["id"]){
                                                                    echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                }else{
                                                                    echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="fg-line">
                                <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="fg-line">
                                <h5>
                                    <?php if($counter==1){?>
                                    <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">
                                        &nbsp; Add more currency
                                    </a>
                                    <?php }else{?>
                                    <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                                        &nbsp; Remove currency
                                    </a>
                                    <?php }?>
                                </h5>
                            </div>                                                                                                       
                        </div>                                                            
                        </div>
                        <?php   $counter++;}
                            }else{
                        ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                            <?php 
                                                foreach ($currency_code as $key => $value) {
                                                    if(@$fieldval[0]['currency_id']==$key){
                                                        echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }else{
                                                        echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="fg-line">
                                    <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
                                <span class="countdown1"></span>
                            </div>
                        </div>
                            <?php if(count($currency_code) > 1){?>
                            <div class="col-md-4">                                                    
                                <div class="fg-line">
                                    <h5>
                                        <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink">
                                            &nbsp; Add more currency
                                        </a>
                                    </h5>
                    </div>
                            </div>
                            <?php }?>
                        </div>
                        <?php }?>
                        </div>
                        </div>
                    </div>
                    <div id="sku">
                        <div class="form-group">
                            <label for="sku" class="col-md-4 control-label">SKU Number<span class="red"><b>*</b></span>:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type='text' placeholder="Enter SKU number." id="skuno" name="pg[sku]" value="<?php echo @$fieldval[0]['sku']; ?>" class="form-control input-sm checkInput" >
                                    <span class="red" id="email-error" style="display: none;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="size">
                        <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Size:</label>
                            <div class="col-md-8">
                                <div class="select">
                                    <select id="size" name="pg[size]" class="form-control input-sm">
                                    <?php foreach($shipping_size AS $key=>$val){?>
                                        <option value="<?php echo $key;?>" <?php echo (@$fieldval[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                                    <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
<!--                    <div class="form-group">
                        <label for="releaseDate" class="col-md-4 control-label">Release/Recorded Date:</label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="pg[release_date]" value="<?php
                                if (isset($fieldval[0]['release_date']) && $fieldval[0]['release_date']!='0000-00-00') {
                                        echo date('m/d/Y', strtotime(@$fieldval[0]['release_date']));
                                }
                                ?>" class="form-control input-sm checkInput" >
                            </div>
                        </div>
                    </div>-->
                    <div id="product_status">
                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status<span class="red"><b>*</b></span>:</label>
                            <div class="col-md-8">
                                <div class="select">
                                    <select id="product_status" name="pg[status]" class="form-control input-sm">
                                       <option value="1" <?php echo (@$fieldval[0]['status'] == 1)?'selected=selected':''; ?> >Active</option>
                                       <option value="2" <?php echo (@$fieldval[0]['status'] == 2)?'selected=selected':''; ?>  >Inactive</option>
                                       <option value="3" <?php echo (@$fieldval[0]['status'] == 3)?'selected=selected':''; ?>  >Out of Stock</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label for="Discount" class="col-md-4 control-label">Publish on<span><b></b></span>: </label>

                        <div class="col-md-4">
                            <div class="fg-line">
                                <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="pg[publish_date]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['publish_date'])):''; ?>">
                               
                                <span class="countdown1"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="fg-line">
                                <!--<input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="publish_time" value="">
                            </div>                            
                        </div>
                    </div>-->
                    <?php if(@$enable_category == 1){?>
                    <div class="form-group">
                        <label for="category" class="col-md-4 control-label">Content Category<span class="red"><b>*</b></span>: </label>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <select  name="pg[content_category_value][]" id="content_category_value" required="true" class="form-control input-sm checkInput">
                                    <option value="">-Select-</option>
                                    <?php
                                    foreach ($contentCategories AS $k => $v) {
										if(@$fieldval && in_array($k, explode(',', $fieldval[0]['content_category_value']))){//($fieldval[0]['content_category_value'] & $k)
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = '';
                                        }
                                        echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <?php }?>
<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="pg[content_publish_date]" id="content_publish_date" <?php if (@$fieldval[0]['publish_date']) { ?>checked="checked"<?php } ?>/>
                <i class="input-helper"></i> Make it Live at Specific time
            </label>
        </div>
        <div class="row m-t-10 <?php if (@$fieldval[0]['content_publish_date']) {} else { ?>content_publish_date_div<?php } ?>" id="content_publish_date_div" <?php if (@$fieldval[0]['publish_date']) { ?>style=""<?php }else{ ?> style="display:none;"<?php } ?>>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="pg[publish_date]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['publish_date'])):''; ?>">
                    </div>
                    <span class="input-group-addon"><i class="icon-calendar"></i></span>

                </div>



            </div>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="pg[publish_time]" value="<?php echo (@$fieldval[0]['publish_date'] != '0000-00-00' && @$fieldval[0]['publish_date'] != '')?date('H:i',strtotime($fieldval[0]['publish_date'])):''; ?>" type="text">
                    </div>
                    <span class="input-group-addon"><em class="icon-clock"></em></span>

                </div>



            </div>
            <div class="col-sm-2">
                <label for="content_publish_date">UTC</label>
            </div>
        </div>
    </div>
</div>
<!--<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="pg[feature]" id="feature" <?php if (@$fieldval[0]['feature']) { ?>checked="checked"<?php } ?>/>
                <i class="input-helper"></i> Make it featured product
            </label>
        </div>
    </div>
</div>
                     <div class="form-group">
                        <label for="Discount" class="col-md-4 control-label">Discount<span><b></b></span>: </label>
                        <div class="col-md-2">
                            <div class="fg-line">
                                <input type="number" class="checkInput form-control input-sm" name="pg[discount]" value="<?php echo @$fieldval[0]['discount']; ?>" placeholder="Enter Discount...">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="fg-line">
                                <input id="datepicker_for_no" class="form-control input-sm" type="text"  name="pg[discount_till_date]" value="<?php echo (@$fieldval[0]['discount_till_date'] != '0000-00-00' && @$fieldval[0]['discount_till_date'] != '')?date('m/d/Y',strtotime($fieldval[0]['discount_till_date'])):''; ?>" placeholder="validate until"  onkeypress="return onlyAlphabets(event, this);" autocomplete="off" readonly>
                               
                                <span class="countdown1"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="discountype" class="col-md-4 control-label">Discount type: </label>
                        <div class="col-md-8">
                            <div class="fg-line control-label row">
                                <div class="col-sm-12">
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="0" name="pg[discount_type]" <?php echo (@$fieldval[0]['discount_type'] == 0)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Flat
                                    </label>
                                    <label class="radio radio-inline m-r-20">
                                        <input type="radio" value="1" name="pg[discount_type]" <?php echo (@$fieldval[0]['discount_type'] == 1)?'checked=checked':'';?>>
                                        <i class="input-helper"></i>Percent
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>-->

<?php if($productize){?>
                <div class="form-group">
                    <label for="itemtype" class="col-md-4 control-label">Personalization: </label>
                    <div class="col-md-8">
                        <div class="control-label row">
                            <div class="col-sm-12">
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" value="0" name="pg[productize_flag]" <?php echo (@$fieldval[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['productize_flag'] == 0)?'checked=checked':'';?>>
                                    <i class="input-helper"></i>Disabled
                                </label>
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" value="1" name="pg[productize_flag]"<?php echo (@$fieldval[0]['productize_flag'] == 1)?'checked=checked':'';?>>
                                    <i class="input-helper"></i>Enabled
                                </label>
                                <?php if(@$fieldval[0]['id'] != ''){$cid = $fieldval[0]['id']; ?>
                                <a href="javascript:void(0)" onclick="addCmsCustomizationRecord(<?php echo $cid;?>)">Edit Customizer</a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
               <div class="form-group m-t-30">
                    <div class="col-md-offset-4 col-md-8">

                        <button type="submit" class="btn btn-primary btn-sm" id="save-btn">
                            <?php echo (@$fieldval[0]['id'] != '')?'Update':'Save';?>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
          <?php $authToken   = OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select'=>'oauth_token'))->oauth_token; ?>
		<div class="form-horizontal">
				<div class="hideforaudio">
					<div class="col-md-8">
						<div class="form-group">
							<label for="" class="col-sm-4 control-label" id="trailerLable">
								Preview :
								<?php
								$trailerConversion = $this->getTrailerIsConverted(@$fieldval[0]['id']);
								if ($trailerConversion == 2) {
									?>
									<span id="videoCorrupt"><img src="<?php echo Yii::app()->baseUrl; ?>/images/exclamation_point.png" alt="Video is could not encoded" class="vAlignTop"  data-toggle="tooltip" title="Encoding failed. Please check the video file and re-upload."/></span>
								<?php } ?>
							</label>
							<div class="col-sm-8" id="trailerbtn<?php echo @$fieldval[0]['id']; ?>">

								<?php
								$is_physical = 1;
								$trailer = $this->getTrailer(@$fieldval[0]['id'],$is_physical);
								$disable = 'disabled';
								if ($trailer) {
									$trailerIsConverted = $this->getTrailerIsConverted(@$fieldval[0]['id'],$is_physical);
									if ($trailerIsConverted == 1) {
										$disable = '';
										$info = pathinfo($trailer);
										$pos = stripos($trailer, 'iframe');
										$pos1 = stripos($trailer, 'youtube.com');
										$pos2 = stripos($trailer, 'vimeo.com');
										$pos3 = stripos($trailer, 'dailymotion.com');
										if ($info["extension"] == 'm3u8') {
											$this->renderPartial('player_third_party', array('third_party_url' => $trailer));
										} else if (($pos > 0) && ($pos1 > 0 || $pos2 > 0 || $pos3 > 0)) {
											?>
											<div style="width: 250px; height: 120px;margin-bottom: 17px;" class="embeded_trailer">
												<?php echo $trailer; ?>
												<style type="text/css">
													iframe{
													  width: 250px !important;
													  height: 120px !important;  
													}
												</style>
    </div>
										<?php } else { ?>
											<video width="250" height="120" controls style="background: #333;" id="trailerplayer"> 
												<source type="video/mp4"  src="<?php echo $trailer; ?>">
												Your browser does not support the video tag.
											</video>
										<?php } ?>
								<div class="trailer_upload_succ">
										<?php if ($disable == "") { ?>
											<div class="fg-line m-b-10">
												<a id="remove-trailer-text" class="btn btn-danger btn-file btn-sm" href="javascript:void(0);" onclick="return removePhysicalTrailer(<?php echo @$fieldval[0]['id']; ?>);"><i class="remove"></i> Remove </a>
											</div>  
										<?php
										 }
											?>
										</div> <?php 
								} else {
										?>
										<div class="progress-encoding">
											<a href="#" class="f-500">
												<em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress
											</a>  
										</div>    
										<?php
									} ?>
									<?php    
									} else {?>
										<div class="trailer_upload_start <?php if($trailer) {echo 'show';} ?>">
										<a href="javascript:void(0);" onclick="openUploadpopup('<?php echo @$fieldval[0]['id'];?>',0);" class="upload-video btn btn-default btn-file btn-sm"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a>
										</div>
										<?php
									}
								?>
							</div>
						</div>
					</div>
				</div>
				<hr class="hideforaudio">
		</div>
	</div>
	
    
    <input type="hidden" id="authToken" value="<?php echo $authToken; ?>">
    <input type="hidden" id="x1" name="jcrop[x1]" />
    <input type="hidden" id="y1" name="jcrop[y1]" />
    <input type="hidden" id="x2" name="jcrop[x2]" />
    <input type="hidden" id="y2" name="jcrop[y2]" />
    <input type="hidden" id="w" name="jcrop[w]">
    <input type="hidden" id="h" name="jcrop[h]">    
</form>	
<div style="display: none;" id="defaultcurrencydiv">
    <?php echo $this->renderPartial('addmorecurrency', array('currency_code'=>$currency_code,'pgcurrenyname'=>$pgcurrenyname,'pgsalepricename'=>$pgsalepricename), true);?>
</div> 
<!--Physical Properties-->
<?php
	if(@$product_variants_flag){
		$this->renderPartial('variants_list', array('fieldval'=>$fieldval,'variantlist'=>$variantlist));
	}
?>
<!---->
<div class="modal fade is-Large-Modal bs-example-modal-lg1" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 
</div>
<div class="modal fade is-Large-Modal bs-example-modal-lg1" id="addvideo_popup_physical"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addvideo_popupLabel">Upload Video</h4>
                </div>
                <div class="modal-body">
                        <div role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                                <a href="#Upload_preview" aria-controls="Upload_preview" role="tab" data-toggle="tab">Upload Video</a>
                                        </li>
                                        <li role="presentation">
                                                <a href="#Choose_from_library" aria-controls="Choose_from_library" role="tab" data-toggle="tab">Choose from Gallery</a>
                                        </li>
                                        <li role="presentation">
                                                <a href="#Embed_from_3rd_party" aria-controls="Embed_from_3rd_party" role="tab" data-toggle="tab">Embed from 3rd party</a>
                                        </li> 
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Upload_preview">
                                                <div class="row is-Scrollable">
                                                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                                                <input type="hidden" value="?" name="utf8">
                                                                <input type="hidden" name="movie_id" id="movie_id" value="<?php echo @$fieldval[0]['id']; ?>"/>
                                                                <input type="button" value="Upload File" class="btn btn-default-with-bg btn-sm"  onclick="click_browse('videofile');">
                                                                <input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize('physical');" >
                                                        </div>
                                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Choose_from_library">
                                                <div class="row m-b-20 m-t-20">
                                                        <div class="col-xs-6">
                                                                <div class="form-group input-group">
                                                                        <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                                                        <div class="fg-line">
                                                                                <input type="text" id="search_video1" placeholder="What are you searching for?" class="form-control fg-input input-sm" onkeyup="searchvideo();">

                                                                        </div>

                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="text-center row m-b-20 loaderDiv" style="display: none;">
                                                        <div class="preloader pls-blue text-center " >
                                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                                </svg>
                                                        </div>
                                                </div>

                                                <div class="row  Gallery-Row">
                                                        <div class="col-md-12 is-Scrollable-has-search p-t-40 p-b-40" id="video_content_div">

                                                        </div>

                                                </div>
                                        </div>
                                     <!------Embed from 3rd Party------->
                                     <div role="tabpanel" class="tab-pane" id="Embed_from_3rd_party">
                                         <div role="tabpanel" class="tab-pane" id="home">
                                             <div class="row is-Scrollable m-b-20 m-t-20">
                                                 <div class="col-sm-12 m-t-40 m-b-20">
                                                    <div class="form-group">
                                                        <label for="uploadVideo" class="control-label col-md-3">Enter Youtube url &nbsp;</label>
                                                        <div class="col-md-4">
                                                            <input type="hidden" name="product_id" id="product_id" value=""/>                                                
                                                            <input type="hidden" name="movie_name" id="movie_name" value=""/>
                                                            <div class="fg-line">
                                                                <!--
                                                                <input type="text" id="embed_url" class="form-control  input-sm" placeholder="Link from YouTube, Vimeo or another OVP" onkeyup="embedThirdPartyPlatform();">
                                                                -->
                                                                <input type="text" id="embed_url" class="form-control  input-sm" name="embedurl" required="" placeholder="Link from YouTube">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-10">
                                                            <!--
                                                            <input type="button" value="Save" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm">
                                                            -->
                                                            <br />
                                                            <input type="button" value="Save" id="save-btn" onclick="physicalembedThirdPartyPlatform();" class="btn btn-primary btn-sm" >
                                                        </div>
                                                    </div>
                                                    <span class="error red help-block" id="embed_url_error"></span>
                                                    <div id="embedcode"></div>
                                                 </div>  
                                             </div>
                                         </div>
                                     </div>
                                     <!-------End of Embed from 3rd party------>
                                </div>
                        </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>    
</div>
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
  <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
  </div>
  <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" />
<input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" />   
<div style="clear: both;"></div>
<!-- Change Poster Popup-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.tokenize.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/physical.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript">
     var authToken = $('#authToken').val();
     sessionStorage.setItem('HTTP_ROOT',HTTP_ROOT);
     sessionStorage.setItem('STORE_AUTH_TOKEN',authToken);
     var movie_id = "<?php echo $fieldval[0]['id']; ?>";

    Array.prototype.getUnique = function () {
        var u = {}, a = [];
        for (var i = 0, l = this.length; i < l; ++i) {
            if (u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }
    $(function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/movie_autocomplete";
        $('#movie_id').tokenize({
            datas: url,
        });
    });
    
    $("#movie_idd").on("click", function(){
          var str = "";
            $("#movie_idd option:selected").each(function(){
            str += $( this ).val() + ",";
        });
       $("#movie_ids").val(str);
        });
    function click_browse(file_name) {
        $("#" + file_name).click();
    }

    $(document).ready(function(){
        $('.cost').on("keypress",function(event) {
            return decimalsonly(event);
        });
        $('.cost').keyup(function() {
            if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
        });
	$("[data-mask]").inputmask(), $("#release_date").datepicker({changeMonth: !0, changeYear: !0})
        $('input[name="pg[product_type]"]').click(function(){
            if ( $(this).is(':checked') ) {
                if ($(this).val() == '1' ) {
                    $('#showcntent').show();
                } else {
                     $('#showcntent').hide();
                     $('#error_movie_id').html('');
                }
            }
        });
   //---------------------------------
   $("#itemform").validate({
    rules: {
        "pg[name]": "required",        
        "pg[sale_price]": "required",
        "pg[sku]": "required",

    },
    messages: {
        "pg[name]": "Please enter item name",
        "pg[sale_price]": "Please enter amount",
        "pg[sku]": "Please enter SKU number",

    },
    errorPlacement: function(error, element) {
        error.addClass('red');
        error.insertAfter(element.parent());
    },
    submitHandler: function(form)
    {
        if($("input[name='pg[product_type]']:checked"). val()==1){
            if($("#movie_idd option:selected").length==0){
                $('#error_movie_id').html('Enter content name');
                return false;
            }else{
                $('#error_movie_id').html('');
            }                
        }else{
            $('#error_movie_id').html('');
        }        
        var isprice = 1;
        $(".moreCurrencyDiv").find(".cost").each(function () {
            $(this).parent().find('label').remove();
            if($(this).val()==''){
                $(this).parent().after("<label class='error red multierrorprice'>This field is required.</label>");
                isprice = 0;
            }
        });
        if(isprice==0){
            return false;
        }
        var currency = new Array();
        $(".moreCurrencyDiv").find(".select").each(function () {
            currency.push($(this).find('select').val());
        });
        var x = currency.getUnique();
        if (x.length === currency.length) {
            //do nothing
        } else {
            swal("Currency should be unique");
            return false;
        }
        var url = "<?php echo Yii::app()->getBaseUrl(true) ?>/store/checkSkuNumber";         
        $('#save-btn').attr('disabled','disabled');
        $.post(url, {'skuno': $('#skuno').val(),'check':'<?php echo (@$fieldval[0]['id'] != '')?'FALSE':'TRUE';?>','id':'<?php echo @$fieldval[0]['id']?>'}, function (res)
        {
            if (res.succ)
            {
                $('#is_submit').val(1);
                document.itemform.action = '<?php echo Yii::app()->baseUrl; ?>/store/<?php echo (@$fieldval[0]['id'] != '')?'EditSaveItem':'SaveItems';?>';
                document.itemform.submit();
            } else {
                $('#save-btn').removeAttr('disabled');
                $('#email-error').show();
                $('#email-error').html('This sku number has been used.');
            }
        }, 'json');
    } 
    }); 
 });
    function posterpreview(obj) {
        $("#previewcanvas").show();
        var canvaswidth = $("#reqwidth").val();
        var canvasheight = $("#reqheight").val();
        if ($('#g_image_file_name').val() == '') {
            var x1 = $('#x1').val();
            var y1 = $('#y1').val();
            var width = $('#w').val();
            var height = $('#h').val();
        } else {
            var x1 = $('#x13').val();
            var y1 = $('#y13').val();
            var width = $('#w3').val();
            var height = $('#h3').val();
        }
        var canvas = $("#previewcanvas")[0];
        var context = canvas.getContext('2d');
        var img = new Image();
        img.onload = function () {
            canvas.height = canvasheight;
            canvas.width = canvaswidth;
            context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
            //$('#imgCropped').val(canvas.toDataURL());
        };
        $("#avatar_preview_div").hide();
        if ($('#g_image_file_name').val() == '') {
            img.src = $('#preview').attr("src");
        } else {
            img.src = $('#glry_preview').attr("src");
        }
        $('#myLargeModalLabel').modal('hide');
        $(obj).html("Next");
    }
    function hide_file() {
        $('#preview').css("display", "none");
    }
    function hide_gallery() {
        $('#glry_preview').css("display", "none");
    }
    function seepreview(obj) {

        if ($("#x13").val() != "") {
            $(obj).html("Please Wait");
            $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
            posterpreview(obj);
        } else {
            if ($("#celeb_preview").hasClass("hide")) {
                $('#myLargeModalLabel').modal('hide');
                $(obj).html("Next");
            } else {
                $(obj).html("Please Wait");
                $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
                if ($("#x13").val() != "") {
                    posterpreview(obj);
                } else if ($('#x1').val() != "") {
                    posterpreview(obj);
                } else {
                    $('#myLargeModalLabel').modal('hide');
                    $(obj).html("Next");
                }
            }
        }
    }
    
    function fileSelectHandler() {
        $('#celeb_preview').css("display", "block");
        $("#g_image_file_name").val('');
        $("#g_original_image").val('');
        $('#glry_preview').css("display", "none");
        clearInfo();
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview").hide();
        $("#celeb_preview").removeClass("hide");
        var reqwidth = parseInt($('#reqwidth').val());
        var reqheight = parseInt($('#reqheight').val());
        var aspectRatio = reqwidth / reqheight;
        var oFile = $('#celeb_pic')[0].files[0];
        var ext = oFile.name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        if (oFile.name.match(/['|"|-|,]/)) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('File names with symbols such as \' , - are not supported');
            return;
        }
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";

            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(oFile);
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);

            if (width < reqwidth || height < reqheight) {
                $("#celeb_preview").addClass("hide");
                document.getElementById("celeb_pic").value = "";
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);

                return;
            }
            var oImage = document.getElementById('preview');
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                oImage.src = e.target.result;
                oImage.onload = function () {
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                        $('#glry_preview').width("450");
                        $('#glry_preview').height("250");
                    }
                    $('#preview').Jcrop({
                        minSize: [reqwidth, reqheight],
                        aspectRatio: aspectRatio,
                        boxWidth: 400,
                        boxHeight: 150,
                        bgFade: true,
                        bgOpacity: .3,
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function () {
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        jcrop_api = this;
                        jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                    });
                };
            };
            oReader.readAsDataURL(oFile);
        }
    }
    function toggle_preview(id, img_src, name_of_image) {

        $('#gallery_preview').css("display", "block");
        clearInfo();
        $('#gallery_preview').removeClass("hide");
        document.getElementById("celeb_pic").value = "";
        var reqwidth = parseInt($('#reqwidth').val());
        var reqheight = parseInt($('#reqheight').val());
        var aspectRatio = reqwidth / reqheight;
        showLoader();
        var image_file_name = name_of_image;
        var image_src = img_src;
        $("#g_image_file_name").val(image_file_name);
        $("#g_original_image").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        var img = new Image();
        img.src = img_src;
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < reqwidth || height < reqheight) {
                showLoader(1);
                $("#gallery_preview").addClass("hide");
                $("#g_image_file_name").val("");
                $("#g_original_image").val("");
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
                return;
            }
            var oImage = document.getElementById('glry_preview');
            showLoader(1)
            oImage.src = img_src;
            oImage.onload = function () {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview').width(oImage.naturalWidth);
                    $('#glry_preview').height(oImage.naturalHeight);
                    $('#preview').width("800");
                    $('#preview').height("300");
                }
                $('#glry_preview').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateInfoallImage,
                    onSelect: updateInfoallImage,
                    onRelease: clearInfoallImage
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
    }
    var gmtDate = '<?= gmdate('d'); ?>';
    var gmtMonth = '<?= gmdate('m'); ?>';
    var gmtYear = '<?php echo gmdate('Y'); ?>';
    $("#publish_date").datepicker({changeMonth: !0, changeYear: !0, minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
   
    
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#myLargeModalLabel button,input[type="text"]').attr('disabled', 'disabled');
            $('#profile button').attr('disabled', 'disabled');

        } else {
            $('.loaderDiv').hide();
            $('#myLargeModalLabel button,input[type="text"]').removeAttr('disabled');
            $('#profile button').removeAttr('disabled');
        }
    }
    function addmorecurrency(cnt){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv < cnt){
            var morecurreny = $('#defaultcurrencydiv').html();
            $('.moreCurrencyDiv').append(morecurreny);
            var childdiv1 = childdiv+1;
            if(childdiv1==cnt){$('#addmorelink').hide();}
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').keyup(function() {
                if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
            });
        }else{
            $('#addmorelink').hide();
        }
    }
    function removecurrency(cnt,obj){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv <= cnt){
            $(obj).parent().parent().parent().parent().remove();
            $('#addmorelink').show();
        }else{
            $('#addmorelink').show();
        }
    }
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57)){                
                return true;
            }else
                return false;
        }
    }
	function removePhysicalTrailer(movie_id) {
    swal({
            title: "Remove Trailer?",
            text: "Are you sure you want to remove this trailer?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
        if (movie_id) {
            var url = HTTP_ROOT + "/admin/removeContentTrailer";
            $('#remove-trailer-text').attr('disabled', 'disabled');
            $.post(url, {'movie_id': movie_id, 'is_ajax': 1,'is_physical' : 1}, function (res) {
                if (res.err) {
                    $('#remove-trailer-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting preview.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-trailer-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Preview removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#trailerplayer').remove();
                    $('#remove-trailer-text').remove();
					if(('.trailer_upload_succ').length > 0){
					$('.trailer_upload_succ').html('');
					}
					if($('.embeded_trailer').length >0){
						$('.embeded_trailer').remove();
					}
					$('.trailer_upload_succ').html('<a href="javascript:void(0);" onclick="openUploadpopup(' + movie_id + ',0)" class="upload-video btn btn-default btn-file btn-sm"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></p>');
					$('.trailer_upload_start').toggleClass('hide');
                }
            }, 'json');
        } else {
            return false;
        }
    }); 
}
function view_filtered_list(){ alert('pop');
       showLoader();
       var video_duration =$("#video_duration").val();
       var file_size =$("#file_size").val();
       var is_encoded =$("#is_encoded").val();
       var uploaded_in =$("#uploaded_in").val();
      var search_text=$("#search_video1").val();
    var url = "<?php echo Yii::app()->baseUrl; ?>/admin/ajaxFiltervideo/search_text/"+search_text+"/video_duration/" + video_duration + "/file_size/" + file_size +"/is_encoded/" + is_encoded+"/uploaded_in/" + uploaded_in;   
     //window.location.href = url;
       $.post(url, {'search_text': search_text,'video_duration': video_duration,'file_size':file_size, 'is_encoded': is_encoded,'uploaded_in': uploaded_in}, function (res) {
            if (res) {
                $('#video_content_div').html();
                 showLoader(1);
                 
          $('#video_content_div').html(res);      
           }
       });
       }

    function physicalembedThirdPartyPlatform(){
           var str =$("#embed_url").val();
            var videoUrl1 = "youtube.com";
            var videoUrl2 = "vimeo.com"; 
            var videoUrl3 = 'm3u8';
            var videoUrl4 = 'iframe';
            var videoUrl5 = "dailymotion.com";
          if(str.length ==0){
            $('#save-btn').attr('disabled', 'disabled');
			var movie_id = $('#product_id').val();
			var remove_url = HTTP_ROOT + "/admin/removeContentTrailer";
			$.post(remove_url, {'movie_id': movie_id, 'is_ajax': 1,'is_physical' : 1}, function (res) {
					window.location.href = window.location.href;
			});
           }else if(str.length){
            // console.log((str.indexOf(videoUrl4))+'.......'+(str.indexOf(videoUrl3))+'.....'+(str.indexOf(videoUrl2))+'.....'+(str.indexOf(videoUrl1)));
                 if ((str.indexOf(videoUrl3) != -1) || 
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl1) != -1)  || 
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl2) != -1) ||
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl5) != -1))
                 {                         
                     showLoader();
                     var embed_text =$("#embed_url").val();
                     var action_url = '<?php echo $this->createUrl('admin/embedThirdPartyPlatformForTrailer'); ?>'
                     $('#embed_url_error').html('');
                     var movie_id = $('#product_id').val();
                     $.post(action_url, {'third_party_url': embed_text,'movie_id': movie_id,'type': 'physical'}, function (res) {
                     showLoader(1);
                     window.location.href = window.location.href;
                     });
                 }else{
                     swal("Oops! Provide a valid m3u8 or iframe embed url.");
                     return false;
             }
         }        
    }
    function searchvideo()
    { 
        var key_word = $("#search_video1").val();
        var key_word = key_word.trim();
        if(key_word.length>=3 || key_word.length==0){
            $('.loaderDiv').show();                                            
            $('#profile button').attr('disabled', 'disabled');
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/ajaxSearchVideo",
                data: {'search_word': key_word},
                contentType: false,
                cache: false,
                success: function (result)
                {
                    $('.loaderDiv').hide(); 
                    $('#profile button').removeAttr('disabled');
                    $("#video_content_div").html(result);
                }
            });
        }
        else
        {
        return;

        }
    }

    function addvideoFromVideoGallery(videoUrl, galleryId) {
        showLoader();
        var action_url = HTTP_ROOT + '/admin/addVideoFromVideoGalleryToTrailer';
        $('#server_url_error').html('');
        var movie_id = $('#product_id').val();
        $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id,'type': 'physical'}, function (res) {
            showLoader(1);
            if (res.error) {
                $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
            } else {
                if (res.is_video === 'uploaded') {
                    window.location.href = window.location.href;
                } else if (res.msg === 'Trailer updated successfully') {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Wow! Preview for the content uploaded successfully. It will be available after completion of encoding.</div>'
                    $("#addvideo_popup_physical").modal('hide');
                    $('.pace').prepend(sucmsg);
                    $('#trailer_btn').remove();
                    $('#videoCorrupt').hide();
                    $('#trailerbtn'+movie_id).html('<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>');
                } else {
                    $('#trailerLable').html('There seems to be something wrong with the video');
                }
            }
        }, 'json');
    }
      function openUploadpopup(product_id,flag) {
			var name = $("#" +product_id + "_title").html();
			$('input[type=file]').val('');
			$('#movie_name').val(name);
			$('#product_id').val(product_id);
			$('#movie_id').val(product_id);
			$('#pop_movie_name').html(name);        
			if(flag){
				$('#embed_url').val($("#" +product_id + "_link").html());
			}else{
				$('#embed_url').val('');
			}
			$("#addvideo_popup_physical").modal('show');
			$('#save-btn').removeAttr('disabled');
			$('.error').html('');
			searchvideo(1);
	}  
 </script>
 
<script type="text/javascript">
    // Minified Javascript 	
     $("#content_publish_date").click(function () {
        0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show()
    }), function () {
        $("[data-mask]").inputmask(),$("#publish_date").datepicker({changeMonth: !0, changeYear: !0, minDate: $("#publish_date").val() ? new Date($("#publish_date").val()) : new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)}), $("#publish_time").timepicker({showInputs: !1}), $("#castname").autocomplete({source: function (e, t) {

            }, select: function (e, t) {
                e.preventDefault(), $("#castname").val(t.item.label), $("#cast_id").val(t.item.value)
            }, focus: function (e, t) {
                $("#cast_name").val(t.item.label), $("#cast_id").val(t.item.value), e.preventDefault()
            }}), $("#dprogress_bar").draggable({appendTo: "body", start: function (e, t) {
                isDraggingMedia = !0
            }, stop: function (e, t) {
                isDraggingMedia = !1
            }})
    }();    
     function addCmsCustomizationRecord(prodId){        
       sessionStorage.removeItem('CUSTO_PROD_ID');
       sessionStorage.removeItem('LOG_ID');
       sessionStorage.removeItem('STUDIO_ID');
       sessionStorage.setItem('STORE_AUTH_TOKEN',authToken);
       var url = HTTP_ROOT+'/shop/UpdateCmsCustomizationRecord';
       $.post(url, {'product_id':prodId}, function(res){            
          window.location.href = HTTP_ROOT+'/cmscustomize';
       })  
   console.log(prodId);
    sessionStorage.setItem('CUSTO_PROD_ID',prodId);
    } 
    
    function click_browse(file_name) {
        $("#" + file_name).click();
    }

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://superal.github.io/canvas2image/canvas2image.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
        selector: ".RichText",
        formats: {
            bold: {inline: 'b'},  
        },
         valid_elements : '*[*]',
         force_br_newlines : true,
         force_p_newlines : false,
         forced_root_block : false,
         menubar: false,
         element_format : 'html',
         extended_valid_elements : 'div[*], style[*]',
         valid_children : "+body[style]", 
         height: 200,
         plugins: [
         'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
         'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
         'save table contextmenu directionality emoticons template paste textcolor'
         ],
        setup: function (editor) {
        editor.addButton('newmedia', {
         text: 'Add Image',
         title: 'Add image',
         icon: 'image',
         onclick: function() {
            $("#MediaModal").modal("show");
            $('#glry_preview').removeAttr('src width height');
            $('#imgtolib').removeAttr('src width height');
            $('#choose_img_name').val("");
            $("#InsertPhoto").text("Insert Image");
            $("#InsertPhoto").removeAttr("disabled");
            $("#cancelPhoto").removeAttr("disabled");
            $('.overlay').removeAttr('style');
            $("#browsefiledetails").text("No file selected");
            $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true)?>/template/tinyGallery");
        } });
         },
        content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | code",
        });
    });
</script>
