<div class="row">
    <div class="col-md-3">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                    <?php
                    foreach ($currency_code as $key => $value) {
                        echo '<option value=' . $value["id"] . '>' . $value["code"] . '(' . $value["symbol"] . ')' . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="fg-line">
            <input type='number' min="1" placeholder="Enter Amount" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
            <span class="countdown1"></span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="fg-line">
            <h5>
                <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                    &nbsp; Remove currency
                </a>
            </h5>
        </div>
    </div>
</div>