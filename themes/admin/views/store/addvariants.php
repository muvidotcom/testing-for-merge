<style>
    .upload-Image {
        height: 60px;
        width: 60px;
        background-color: #fff;
        border-color: #fff;
        display: inline-flex;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
    }      
    .upload-Image em{
        font-size: 1.5em;
        top: 62%;
        position: absolute;
        margin-top: -17px;
        left: 60%;
        margin-left: -17px;
        color:#2cb7f6;
    }
    .upload-Image:hover{ 
        background-color: #edf1f2;
    }
    .upload-Image:hover em{
        color: #0aa1e5;
    }
    .btn.upload-Image.relative {
        margin-top: 2px;
        float: left;
    }

    ul.sortable {width: 100%; float: left; margin: 20px 0; list-style: none; position: relative !important;}
    ul.sortable li {height: 107px; float: left;  border: 2px solid #fff; cursor: move;}
    ul.sortable li.ui-sortable-helper {border-color: #3498db;}
    ul.sortable li.placeholder {width: 107px; height: 107px; float: left; background: #eee; border: 2px dashed #bbb; display: block; opacity: 0.6; border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px; }    

    .loading_div{display: none;}      
    .jcrop-keymgr{display:none !important;}    
    .addmore-content{display:none;}
    .fixedWidth--Preview img {max-width: none !important;}
    .pg-thumb-img a em{top: 2px;right: 0;z-index: 9;}
    .pg-thumb-img img.active{border:2px solid #3498db; opacity: 0.8; box-shadow: 3px 3px 3px #d2cfcf;}
    #avatar_var_preview_div {
        position: relative;
    }    
    #avatar_var_preview_div {
        position: relative;
    }
    #avatar_var_preview_div .faded-area {
        background-color: rgba(255, 255, 255, 0.6);
        position: absolute;
        height: 100%;
        z-index:999;
        width: 100%;
        display: none;
    }
    #avatar_var_preview_div .loading-img {
        background: transparent url('/img/loading.gif') center center no-repeat;
        position: absolute;
        top: 45%;
        width: 100%;
    }
</style>
<?php 
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
$vers = RELEASE;
$enable_lang = 'en';
if(isset($data[0]['id'])){
    $enable_lang = $this->language_code;
	if ($_COOKIE['Language']) {
		$enable_lang = $_COOKIE['Language'];
	}
}
                  
    $dmn = $customData['formData']['poster_size'];
    if($dmn){
     $expl = explode('x', strtolower($dmn));
        $cropDimesion = array('width' => @$expl[0], 'height' => @$expl[1]);
    }else{
        $cropDimesion = Yii::app()->common->getPgDimension();
    }

?>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title"><?php
		if (@$cf) {
			echo "Edit";
		} else {
			echo "Add";
		}
		?> Variants </h3>
	Add a new variant for your product
</div>
<div class="modal-body" style="max-height: 450px; overflow-y: auto; overflow-x: hidden;">
	<?php if(empty($pgfind)){?>
	<h3> No variables found for this product</h3>
	<?php }else{?>	
    <form action="javascript:void(0);" method="post" name="addnewfieldform" id="addnewfieldform" enctype="multipart/form-data">
		<div class="form-horizontal">
                            <div class="col-md-6" id="modalFields">
                                <br>
				<div class="form-group" >
					<label class="col-sm-2 toper  control-label" for="Field Name" > SKU:</label>
					<input type="hidden" value="<?php if(@$cf){echo $cf['id'];}else{echo 0;}?>" name="var[edit_id]">
					<input type="hidden" value="<?php if(@$productid){echo $productid;}else{echo 0;}?>" name="var[productid]">
					<div class="col-sm-10">
						<div class="fg-line">
							<div>
								<input type='text' class="form-control input-sm checkInput" name="var[sku]">
							</div>
						</div>
						<label id="var[sku]-error" class="error red has-error" style="display: none;" for="var[sku]">Please enter SKU</label>
					</div>
				</div>
				<?php 
				foreach ($data as $key => $value) {?>
				<div class="form-group" >
					<label class="col-sm-2 toper  control-label" for="Field Name" > <?php echo $value['f_display_name'];?>:</label>
					<div class="col-sm-10">
						<div class="fg-line">
						<?php if($value['f_type']==2){?>
							<div class="select">
								<select name="var[variable][<?= $value['f_name'];?>]" placeholder="" id="<?= $value['f_id'];?>" class="form-control input-sm checkInput" required>
									<?php
										echo "<option value=''>-Select-</option>";
										$opData = json_decode($value['f_value'],true);
										$opData_new = $opData;
										$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
										$opData = empty($opData)?$opData_new:$opData;
										foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
											if (@$data[0]['id'] && @$data[0][$cvalue]==$opvalue) {
												$selectedDd = 'selected ="selected"';
											}
											echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
										}
									?>
								</select>
							</div>
						<?php }?>
						</div>
					</div>
				</div>
				<?php }?>				
				<div class="form-group" >
					<label class="col-sm-2 toper  control-label" for="Field Name" > Price:</label>
					<div class="col-sm-10">
						<div class="moreCurrencyDiv_new">
							<?php
								if(count($currency_code) > 1){
									$pgcurrenyname = "var[multi_currency_id][]";
									$pgsalepricename = "var[multi_sale_price][]";
								}else{
									$pgcurrenyname = "var[currency_id]";
									$pgsalepricename = "var[sale_price]";                                                    
								}
								if((count($currency_code) > 1) && !empty($pgmulti)){
									$counter = 1;
									foreach ($pgmulti as $mkey => $multivalue) {?>
							<div class="row">
								<div class="col-md-4">
									<div class="fg-line">
										<div class="select">
											<select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
												<?php 
													foreach ($currency_code as $key => $value) {
															if(@$mkey==$value["id"]){
																echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
															}else{
																echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
															}                                                                                    
													}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="fg-line">
										<input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01">
										<span class="countdown1"></span>
									</div>
								</div>                                                            
								<div class="col-md-3">                                                    
									<div class="fg-line">
										<h5>
											<?php if($counter==1){?>
											<a onclick="addmorecurrencynew('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelinknew" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">Add more currency</a>
											<?php }else{?>
											<a onclick="removecurrencynew('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">Remove currency</a>
											<?php }?>
										</h5>
									</div>                                                                                                       
								</div>                                                            
							</div>
							<?php   $counter++;}
								}else{
							?>
							<div class="row">
								<div class="col-md-4">
									<div class="fg-line">
										<div class="select">
											<select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
												<?php 
													foreach ($currency_code as $key => $value) {
														if(@$fieldval[0]['currency_id']==$key){
															echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
														}else{
															echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
														}
													}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="fg-line">
										<input type="number" min="1" placeholder="Enter Amount" name="<?= $pgsalepricename;?>" value="<?php echo @$fieldval[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01">
										<span class="countdown1"></span>
									</div>
								</div>
								<?php if(count($currency_code) > 1){?>
								<div class="col-md-3">
									<div class="fg-line">
										<h5>
											<a onclick="addmorecurrencynew('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelinknew">Add more currency</a>
										</h5>
									</div>
								</div>
								<?php }?>
							</div>
							<?php }?>
						</div>
					</div>
				</div>
                            </div>
                            <div class="col-md-6">
                                <div class="border-dotted m-b-40">   
                                <div class="text-center">
                                        <!--<input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target=".bs-example-modal-lg" value="Browse">-->
                                        <input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target="#myPGLargeVariantModal" onclick="openPGVariantModal(this)" value="Browse">

                                        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'X' . $cropDimesion['height']; ?></span></h5>
                                </div>
                                    <div class="text-center">
                                            <div  style="width:<?php echo $cropDimesion['width']; ?>px; margin:0 auto;">
                                            <div class="m-b-10 displayInline fixedWidth--Preview">
                                                    <?php
                                                    $no_image_array = '/img/No-Image-Vertical.png';
                                                    if (!in_array($posterImg, $no_image_array)) {   
                                                            ?>
                                                            <div class="poster-cls  avatar-view jcrop-thumb">
                                                                    <div id="avatar_var_preview_div">    
                                                                            <?php if(@$fieldval[0]['id']==''){
                                                                                    $posterImg = POSTER_URL . '/no-image-a.png';
                                                                            }else{
                                                                                    $posterImg = $pgposter[0]['poster'];
                                                                            }?>
                                                                            <?php if (strpos($posterImg, 'no-image') > -1) { ?>
                                                                                    <img id="preview_var_content_img" data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" />
                                                                            <?php } else { ?>
                                                                                    <img title="poster-img" rel="tooltip" id="preview_var_content_img" src="<?php echo $posterImg; ?>" style="width: <?php echo $cropDimesion['width']; ?>px;">
                                                                            <?php } ?>
                                                                    </div>
                                                            </div>
                                                            <?= $this->renderPartial('//layouts/image_pg_variant_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url)); ?>                            

                                                    <?php } else { ?>
                                                            <div class="poster-cls  avatar-view jcrop-thumb">
                                                                    <div id="avatar_var_preview_div">
                                                                            <img title="poster-img" rel="tooltip" id="preview_var_content_img" src="<?php echo $posterImg; ?>">
                                                                    </div>
                                                            </div>
                                                    <?php } ?>
                                            </div>
                                            <canvas id="previewvariantcanvas" style="overflow:hidden;display: none;"></canvas>
                                            <div class="clear-fix"></div>
                                            <div class="text-left">

                                                    <div class="displayInline">
                                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#myPGLargeVariantModal" data-name="Poster" onclick="openPGVariantModal(this)" id="add-more-pg-var-image" class="btn upload-Image relative">
                                                                    <em class="icon-plus absolute" title="Add Poster"></em>
                                                            </a>
                                                    </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>                                                    
                    
                           </div>
                    
		</div>
    <input type="hidden" id="authToken" value="<?php echo $authToken; ?>">
    <input type="hidden" id="x18" name="jcrop[x18]" />
    <input type="hidden" id="y18" name="jcrop[y18]" />
    <input type="hidden" id="x28" name="jcrop[x28]" />
    <input type="hidden" id="y28" name="jcrop[y28]" />
    <input type="hidden" id="w8" name="jcrop[w8]">
    <input type="hidden" id="h8" name="jcrop[h8]">         
    </form>
	<?php }?>
</div>
<div style="display: none;" id="defaultcurrencydiv_new">
    <?php echo $this->renderPartial('addmorecurrency_variant', array('currency_code'=>$currency_code,'pgcurrenyname'=>$pgcurrenyname,'pgsalepricename'=>$pgsalepricename), true);?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	<?php if(empty($pgfind)){}else{?>
	<button type="button" class="btn btn-primary inputDisabled" aria-hidden="true" onclick="savenewvariants(this);">Save</button>
	<?php }?>
</div>
<input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" />
<input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" />  
<!-- Change Poster Popup-->

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/physical.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>

<script>
	function addmorecurrencynew(cnt){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv_new').children('div').length;
        if(childdiv < cnt){
            var morecurreny = $('#defaultcurrencydiv_new').html();
            $('.moreCurrencyDiv_new').append(morecurreny);
            var childdiv1 = childdiv+1;
            if(childdiv1==cnt){$('#addmorelinknew').hide();}
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').keyup(function() {
                if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
            });
        }else{
            $('#addmorelinknew').hide();
        }
    }
	function removecurrencynew(cnt,obj){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv_new').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv <= cnt){
            $(obj).parent().parent().parent().parent().remove();
            $('#addmorelinknew').show();
        }else{
            $('#addmorelinknew').show();
        }
    }

</script>
