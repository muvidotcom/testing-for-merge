<?php
$pgsettings =Yii::app()->general->getPgSettings();
$pgcontries = json_decode($pgsettings['restrict_country']);
?>
<form class="c-shop-form-1" action="javascript:void(0);" id="shipingform" name="shipingform">
<input type="hidden" name="ship[id]" value="<?php echo $req->id;?>">    
<div class="row">
    <div class="col-xs-12">
        <div class="block">
            <div class="block-body">
                <div class="form-group">
                    <label class="control-label">First Name</label>
                    <div class="fg-line">
                        <input type='text' name="ship[first_name]" value="<?php echo $req->first_name;?>" class="form-control input-sm">            
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <div class="fg-line">
                        <input type='text' name="ship[last_name]" value="<?php echo $req->last_name;?>" class="form-control input-sm">            
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Address line 1</label>
                    <div class="fg-line">
                        <input type='text' name="ship[address]" value="<?php echo $req->address;?>" class="form-control input-sm">            
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Address line 2</label>
                    <div class="fg-line">
                        <input type='text' name="ship[address2]" value="<?php echo $req->address2;?>" class="form-control input-sm">            
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">                
                        <div class="form-group">
                            <label class="control-label">City</label>
                            <div class="fg-line">
                                <input type='text' name="ship[city]" value="<?php echo $req->city;?>" class="form-control input-sm cost">            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">State</label>
                            <div class="fg-line">
                                <input type='text' name="ship[state]" value="<?php echo $req->state;?>" class="form-control input-sm cost">            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Postcode</label>
                            <div class="fg-line">
                                <input type='text' name="ship[zip]" value="<?php echo $req->zip;?>" class="form-control input-sm cost">            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Country</label>
                            <div class="fg-line">
                                <select name="ship[country]" class="form-control c-square c-theme" required >
                                    <option value="">--Select--</option>
                                    <?php 
                                    if(empty($pgcontries)){
                                        foreach($countries as $key=>$coun){
                                    ?>
                                        <option value="<?php echo $coun['code'];?>" <?php echo ($req['country']==$coun['code'])?'selected="selected':'';?> ><?php echo $coun['country'];?></option>
                                    <?php
                                        }
                                    }else{
                                        foreach($countries as $key=>$coun){
                                            if(in_array($coun['code'],$pgcontries)){
                                    ?>                  
                                        <option value="<?php echo $coun['code'];?>" <?php echo ($req['country']==$coun['code'])?'selected="selected"':'';?> ><?php echo $coun['country'];?></option>
                                    <?php    
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Phone Number</label>
                    <div class="fg-line">
                        <input type='text' name="ship[phone_number]" value="<?php echo $req->phone_number;?>" class="form-control input-sm cost">            
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm" id="save-btn" onclick="validateshiping();">Update</button>&nbsp;&nbsp;
                    <button type="button" class="btn btn-default btn-sm" onclick="showShippingAddress('<?php echo $req->id;?>');">Cancel</button>
                </div>                 
                
            </div>
        </div>
    </div>
</div>
</form>
