<div class="modal-dialog">
    <form action="javascript:void(0);" method="post" name="category_form" id="category_form" data-toggle="validator" novalidate="novalidate" class="form-horizontal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    Choose Geo-block Category
                </h4>
            </div>
            <div class="modal-body">                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="Category Name">Geo-block Category:</label>
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <div class="select">
                                <select name="category_name" id="category_name" class="filter_dropdown form-control input-sm">
                                    <option value="">Select Geo-block Category</option>
                                    <?php foreach ($cat_array as $key => $value) { ?>
                                        <option value="<?php echo $key; ?>" <?php if ($categoryid == $key) { ?>selected="selected"<?php } ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>     
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($categoryid) { ?>
                    <div class="form-group">
                        <div class="pull-right">
                            <div class="col-sm-12">
                                <a href="javascript:void(0)" onclick="removegeoblock();">
                                    <em class="icon-trash"></em>
                                    Remove Geo-Block
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="pg_product_id" value="<?php echo $pg_product_id; ?>">
                <button type="submit" class="btn btn-primary" id="bill-btn">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#category_form").validate({
            rules: {
                category_name: {
                    required: true
                }
            },
            messages: {
                category_name: {
                    required: "Please choose Geo-block Category"
                }
            },errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent().parent());
            },
            submitHandler: function (form) {
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/store/AddGeoBlockToPGContent",
                    data: $('#category_form').serialize(),
                    type: 'POST',
                    beforeSend: function () {
                        //$('#reset-loading').show();
                    },
                    success: function (data) {
                        if (data) {
                            $('html').animate({scrollTop: 0}, 2000);
                            $("#ppvPopup").modal('hide');
                            $('.alert').remove();
                            var meg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i> &nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Updated successfully.</div>';
                            $('.pace').before(meg).show("slow");
                        }
                    }
                });
            }
        });
    });
    function removegeoblock() {
        swal({
            title: "Delete Geo block?",
            text: "Do you want to remove geo block from this product?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $('#category_name').attr('disabled', 'disabled')
            $('#category_name').val('');
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/store/AddGeoBlockToPGContent",
                data: $('#category_form').serialize(),
                type: 'POST',
                beforeSend: function () {
                    //$('#reset-loading').show();
                },
                success: function (data) {
                    if (data) {
                        $('html').animate({scrollTop: 0}, 2000);
                        $("#ppvPopup").modal('hide');
                        $('.alert').remove();
                        var meg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i> &nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Updated successfully.</div>';
                        $('.pace').before(meg).show("slow");
                    }
                }
            });
        });
    }
</script>
