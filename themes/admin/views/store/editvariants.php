<style>   
    .upload-Image {
        height: 60px;
        width: 60px;
        background-color: #fff;
        border-color: #fff;
        display: inline-flex;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
    }      
    .upload-Image em{
        font-size: 1.5em;
        top: 62%;
        position: absolute;
        margin-top: -17px;
        left: 60%;
        margin-left: -17px;
        color:#2cb7f6;
    }
    .upload-Image:hover{ 
        background-color: #edf1f2;
    }
    .upload-Image:hover em{
        color: #0aa1e5;
    }
    .btn.upload-Image.relative {
        margin-top: 2px;
        float: left;
    }
    ul.sortable {width: 100%; float: left; margin: 20px 0; list-style: none; position: relative !important;}
    ul.sortable li {height: 107px; float: left;  border: 2px solid #fff; cursor: move;}
    ul.sortable li.ui-sortable-helper {border-color: #3498db;}
    ul.sortable li.placeholder {width: 107px; height: 107px; float: left; background: #eee; border: 2px dashed #bbb; display: block; opacity: 0.6; border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px; }    

    .loading_div{display: none;}      
    .jcrop-keymgr{display:none !important;}    
    .addmore-content{display:none;}
    .fixedWidth--Preview img {max-width: none !important;}
    .pg-thumb-img a em{top: 2px;right: 0;z-index: 9;}
    .pg-thumb-img img.active{border:2px solid #3498db; opacity: 0.8; box-shadow: 3px 3px 3px #d2cfcf;}
    
    #avatar_var_preview_div {
        position: relative;
    }    
    #avatar_var_preview_div {
        position: relative;
    }
    #avatar_var_preview_div .faded-area {
        background-color: rgba(255, 255, 255, 0.6);
        position: absolute;
        height: 100%;
        z-index:999;
        width: 100%;
        display: none;
    }
    #avatar_var_preview_div .loading-img {
        background: transparent url('/img/loading.gif') center center no-repeat;
        position: absolute;
        top: 45%;
        width: 100%;
    }
</style>
<?php
//print'<pre>'; print_r($pgvarposter); exit;
$studio_id = Yii::app()->user->studio_id;
$vers = RELEASE;
$enable_lang = 'en';
if(isset($data[0]['id'])){
    $enable_lang = $this->language_code;
	if ($_COOKIE['Language']) {
		$enable_lang = $_COOKIE['Language'];
	}
}
                  
    $dmn = $customData['formData']['poster_size'];
    if($dmn){
     $expl = explode('x', strtolower($dmn));
        $cropDimesion = array('width' => @$expl[0], 'height' => @$expl[1]);
    }else{
        $cropDimesion = Yii::app()->common->getPgDimension();
    }

?>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title">Edit Variants</h3><br>
	Edit variant for your product
</div>
<div class="modal-body">

    <form action="javascript:void(0);" method="post" name="editfieldform" id="editfieldform" enctype="multipart/form-data">
    <input type="hidden" name="productid" id="productid" value="<?=$productid?>">
    <input type="hidden" name="variant_sku" id="variant_sku" value="<?=$variant_sku?>">
		<div class="form-horizontal">                            
                            <div class="text-center">
                                <div class="border-dotted m-b-40 center-block">   
                                    <div class="text-center">
                                            <input type="button" class="btn btn-default-with-bg btn-file" data-toggle="modal" data-target="#myPGLargeVariantModal" onclick="openPGVariantModal(this)" value="Browse">

                                            <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'X' . $cropDimesion['height']; ?></span></h5>
                                    </div>
                                    <div class="text-center">
                                            <div  style="width:<?php echo $cropDimesion['width']; ?>px; margin:0 auto;">
                                            <div class="m-b-10 displayInline fixedWidth--Preview">
                                                    <?php    
                                                    $no_image_array = '/img/No-Image-Vertical.png';
                                                    if (!in_array($posterImg, $no_image_array)) {
                                                            ?>
                                                            <div class="poster-cls  avatar-view jcrop-thumb">
                                                                    <div id="avatar_var_preview_div">    
                                                                            <?php if(@$productid==''){
                                                                                    $posterImg = POSTER_URL . '/no-image-a.png';
                                                                            }else{
                                                                                    $posterImg = $pgvarposter[0][0]['poster'];
                                                                            }?>
                                                                            <?php if (strpos($posterImg, 'no-image') > -1) { ?>
                                                                                    <img id="preview_var_content_img" data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" />
                                                                            <?php } else { ?>
                                                                                    <img title="poster-img" rel="tooltip" id="preview_var_content_img" src="<?php echo $posterImg; ?>" style="width: <?php echo $cropDimesion['width']; ?>px;">
                                                                            <?php } ?>
                                                                    </div>
                                                            </div>
                                                            <?= $this->renderPartial('//layouts/image_pg_variant_gallery', array('cropDimesion' => $cropDimesion, 'all_images' => $all_images, 'celeb' => $celeb, 'base_cloud_url' => $base_cloud_url)); ?>                            

                                                    <?php } else { ?>
                                                            <div class="poster-cls  avatar-view jcrop-thumb">
                                                                    <div id="avatar_var_preview_div">
                                                                            <img title="poster-img" rel="tooltip" id="preview_var_content_img" src="<?php echo $posterImg; ?>">
                                                                    </div>
                                                            </div>
                                                    <?php } ?>
                                            </div>
                                            <canvas id="previewvariantcanvas" style="overflow:hidden;display: none;"></canvas>
                                            <div class="clear-fix"></div>
                                            <div class="text-left">
                                                    <?php 
                                                    $pgvarposter = $pgvarposter[0];
                                                    foreach ($pgvarposter as $key => $pgThumb) {   
                                                            if (!(strpos($pgvarposter[$key]['poster'], 'no-image-a') > -1)) {
                                                                    $thumbPoster = str_replace('standard/', 'thumb/', $pgvarposter[$key]['poster']); 
                                                                    ?>
                                                                    <div class="displayInline pg-thumb-img relative" id="thumb-<?php echo $pgvarposter[$key]['id']; ?>">
                                                                            <a href="javascript:;" data-poster-id="<?php echo $pgvarposter[$key]['id']; ?>" onclick="return removePGVariantposter('<?php echo @$productid; ?>', this);"><em class="icon-close small-icon absolute" title="Remove Poster"></em></a>
                                                                            <a href="javascript:void(0);" data-poster-id="<?php echo $pgvarposter[$key]['id']; ?>" data-product-id="<?php echo @$productid; ?>" onclick="changePGVariantPoster(this)" id="" class="btn upload-Image relative" style="padding: 0;">
                                                                                    <img src="<?php echo $thumbPoster; ?>" width="100%" />
                                                                            </a>
                                                                    </div>
                                                            <?php }
                                                    }  ?>
                                                    <div class="displayInline">
                                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#myPGLargeVariantModal" data-name="Poster" onclick="openPGVariantModal(this)" id="add-more-pg-var-image" class="btn upload-Image relative">
                                                                    <em class="icon-plus absolute" title="Add Poster"></em>
                                                            </a>
                                                    </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>                                                                                   
                    
                           </div>
                    
		</div>
    <input type="hidden" id="authToken" value="<?php echo $authToken; ?>">
    <input type="hidden" id="x18" name="jcrop[x18]" />
    <input type="hidden" id="y18" name="jcrop[y18]" />
    <input type="hidden" id="x28" name="jcrop[x28]" />
    <input type="hidden" id="y28" name="jcrop[y28]" />
    <input type="hidden" id="w8" name="jcrop[w8]">
    <input type="hidden" id="h8" name="jcrop[h8]">         
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" aria-hidden="true" onclick="this.disabled=true; saveEditVariants(this); return false;">Save</button>
</div>
<input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" />
<input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" />  
<!-- Change Poster Popup-->

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/physical.js?v=<?php echo $vers; ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>


<script>
    function saveEditVariants(obj) {
                var formObj = document.getElementById('editfieldform');
                var formData = new FormData(formObj);        
                var url = HTTP_ROOT+"/store/SaveEditVariants";
                $.ajax({
                    method: 'post',
                    url: url,
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                success: function(data){
                        if(data.success == 1){
                                //$('#product_variants_div').html(data.variant);  
                                $('#myvariantmodaldiv').html('');
                                $("#myvariantmodal").modal('hide');
                                $("#flsmsg").html('<div class="alert alert-success"> Product variant updated successfully </div>');
                                setTimeout(location.reload(true),3000);                               
                        }else if(data.success == 0){
                                alert(data.msg);
                        }                                
                }
                });     
                              
    }
</script>