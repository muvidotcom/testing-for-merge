<div class="row">
	<div class="col-sm-12">
		<div class="form-horizontal">
			<table class="table" id="list_tbl">
				<thead>
					<tr>
						<th>SKU</th>
						<th>Image</th>
						<th>Variable Metadata</th>
						<th>Price</th>
						<th class="width">Actions</th>
					</tr>
				</thead>
				<?php
				if (!empty($variantlist)) { 
					foreach ($variantlist as $key => $val) {     
                                            //print'<pre>'; print_r($val); exit;
				        ?>
						<tbody>
							<tr>
								<td>
									<?php echo $val['sku'];?>
								</td>
                                                                <td>                                                                    
                                                                    <div class="Box-Container-width-Modified">
                                                                        <div class="Box-Container">
                                                                             <div class="thumbnail">
                                                                                 <?php
                                                                                 if(@$val['poster'][$val['id']][0]['poster'] == '') {
                                                                                  $thumbImg = POSTER_URL . '/no-image-a.png';   
                                                                                  ?> 
                                                                                 <img src="<?php echo $thumbImg; ?>" style="max-width:100px;">
                                          
                                                                                 <?php } else {?>
                                                                                <img src="<?php echo $val['poster'][$val['id']][0]['poster'];?>" alt="No Image" style="max-width:100px;">
                                                                                 <?php } ?>
                                                                             </div>
                                                                         </div>   
                                                                    </div>   
                                                                    <br>
                                                                    <div class="m-b-10"> 
                                                                            <a href="javascript:void(0);" onclick="editVariants('<?php echo $fieldval[0]['custom_metadata_form_id'];?>','<?php echo $fieldval[0]['id'];?>','<?php echo $val['sku'];?>')">
                                                                                <button type="submit" class="btn btn-primary btn-default editbtn" style="width:100px;"> Update </button>
                                                                            </a>
                                                                    </div>
                                                                </td>
								<td>
									<?php 
										foreach ($val['variables'] as $key1 => $value) {
											echo $value['f_display_name'].":".$value['value'].'<br>';
										}
									?>
								</td>
								<td>
									<?php 
									if(!empty($val['price'])){
										if(is_array($val['price'])){
											foreach ($val['price'] as $key2 => $value2) {
												echo $value2['price']." (".$value2['symbol'].')<br>';
											}
										}else{
											echo $val['price'];
										}
									}else{
										echo 'N/A';
									}										
									?>
								</td>
								<td>									
									<h5>
										<a href="javascript:void(0)" class="confirm" onclick="confirmDelete('<?php echo $val['id']; ?>');"><em class="icon-trash"></em>&nbsp;&nbsp; Remove variants</a>
									</h5>
								</td>
							</tr>	
                                                        <input type="hidden" name="varientSku" id="varientSku" value="<?=$val['sku']?>">
						</tbody>
						<?php
					}
				} else {
					?>					
					<tbody>
						<tr>
							<td colspan="4">No Variants Found</td>
						</tr>
					</tbody>
				<?php } ?> 
                                
   
			</table>
		</div>
	</div>
</div> 