<div class="row m-t-40 m-b-40">
    <div class="col-xs-12">
        <div class="Block">
            <div class="row">
                <div class="col-md-8">
                    <form class="form-horizontal" method="post" role="form" id="account" name="account" action="<?php echo Yii::app()->baseUrl; ?>/store/importExcel" enctype="multipart/form-data">
                        <input type="hidden" name="setupload" value="Y">
                        <div class="loading" id="subsc-loading"></div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Upload excel file:</label>  
                            <div class="col-md-8">
                                <button class="btn btn-default-with-bg" id="upload_file_button1" type="button" onclick="click_browse('upload_file1')">Browse</button>
                                <input type="file" class="upload" name="upload_file1" id="upload_file1" onchange="preview1(this, '1');" style="display:none;"/>
                                <div id="preview1" class="m-b-10 fixedWidth--Preview relative Preview-Block"></div>                                
                            </div>              
                        </div>              
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" name="save" value="save" class="btn btn-primary btn-sm m-t-30">Upload</button>
                            </div>
                        </div>                

                    </form>        
                </div>
                <div class="col-md-4">
                    <a href="#">Download Sample Excel File</a>
                    <p>Add your data as per the given sample excel file. Do not add / edit any column in excel.</p>
                </div>
            </div>
        </div> 
    </div>
<?php
if(isset($data) && !empty($data))
{
?>
    <div class="col-xs-12">
        <div class="Block">

            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table" >
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>SKU</th>
                                <th>Error</th>
                            </tr>
                        </thead>
                        <?php
                        foreach($data as $val)
                        {
                            $count++;
                        ?>
                            <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo $val['name']; ?></td>
                                <td><?php echo $val['price']; ?></td>
                                <td><?php echo $val['sku']; ?></td>
                                <td class="red"><?php echo $val['error']; ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                </div> 
            </div>
        </div> 
    </div>
<?php
}
?>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js"></script>
<script type="text/javascript">
   function click_browse(upload){
        $('#'+upload).click();
    }
</script>