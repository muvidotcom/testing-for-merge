<div class="modal-dialog modal-lg">
    <form action="<?php echo Yii::app()->baseUrl; ?>/store/ChangeStatus" method="post" name="status_form1" id="status_form1" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Order Status</h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="currentstatus">Current Status<span class="red"><b>*</b></span>: </label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <?php echo $muvi_status[$order_status_id];?>
                                        <input type="hidden" name="pg[from_status]" value="<?php echo $order_status_id;?>">                                        
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="currentstatus">Change To<span class="red"><b>*</b></span>: </label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select id="from_status" name="pg[to_status]" class="form-control input-sm valid" aria-invalid="false" onchange="checkFields(this.value)">
                                                <?php foreach ($to_status as $key => $value) {?>
                                                <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Comments: </label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <textarea class="form-control input-sm auto-size" placeholder="Changing the status because..." name="pg[comments]" autocomplete="off"><?php echo $current_status->comments; ?></textarea>
                                        <input type="hidden" name="pg[orderdetailsid]" value="<?php echo $orderdetailsid;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Tracking Code: </label>
                                <div class="col-sm-7">
                                    <div class="fg-line">
                                        <input type='text' placeholder="Enter tracking code" id="cds_tracking" name="pg[cds_tracking]" value="<?php echo $tracking_status->cds_tracking;?>" class="form-control input-sm checkInput">   
                        </div>
                    </div>
                                <div class="col-sm-3">
                                    <div class="checkbox" style="margin-top: 12px">
                                        <label>
                                            <input type="checkbox" value="1" name="pg[copytoall]" id="copytoall" /><i class="input-helper"></i>Copy to All Items
                                        </label>
                </div>                
            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description">Tracking URL: </label>
                                <div class="col-sm-10">
                                    <div class="fg-line">
                                        <input type='url' placeholder="Enter tracking URL" id="tracking_url" name="pg[tracking_url]" value="<?php echo $tracking_status->tracking_url;?>" class="form-control input-sm checkInput">
                                        <span id="error_tracking_url" class="red col-sm-12"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>
<script type="text/javascript">
    function showConfirmPopup(obj) {        
        var text = "Are you sure you want to change status ?";
        swal({
            title: 'Change Status',
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true,
        },
        function () {
            validateSingleAdvForm(obj);
        });
    }
    //function validateSingleAdvForm() { alert('pop');
    jQuery.validator.addMethod("track", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9 ]*$/i.test(value);
    }, "Please enter a valid tracking code, enter only alfanumeric value.");
      
    
    $("#status_form1").validate({
        rules: {
            'pg[cds_tracking]': {
                track: true
            },
            
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },    
        submitHandler: function(form)
        {
        if($("#cds_tracking"). val()!=''){
            if($('#tracking_url').val()==''){
                $('#error_tracking_url').html('Please enter tracking url');
                return false;
            }else{
                $('#error_tracking_url').html('');
            }                
        }else{
            $('#error_tracking_url').html('');
        } 
        $(obj).attr('disabled',true);
            $(obj).html('Saving');
            var action ="<?php echo Yii::app()->baseUrl; ?>/store/ChangeStatus";

            $('#status_form').attr("action", action);
            document.status_form.submit();
        } 
    });
   
    
    function checkFields(obj) {
        if(obj !=12){
            $('#cds_tracking').attr('disabled',true);
            $('#tracking_url').attr('disabled',true);
            $('#copytoall').attr('disabled',true);
            $('#cds_tracking').val('');
            $('#tracking_url').val('');
        }else{
            $('#cds_tracking').attr('disabled',false);
            $('#tracking_url').attr('disabled',false);
            $('#copytoall').attr('disabled',false);
        }
        
    }
</script>