<style>
    .infobox{
        border: 1px solid #E9E9E9;
        border-radius: 10px;
        margin-bottom: 20px;
        padding: 5px;
    }
</style>
<div class="row">
    <div class="col-sm-12 text-right">
            <a href="javascript:void(printDiv('print'));" class="btn btn-primary btn-sm">Print</a>
    </div>
</div>
<div id="print" >
<div class="row">
    <div class="col-sm-12" style="padding: 20px 0 40px;">
        <div class="col-sm-12">
            <h2 style="display: inline-block;padding-right: 10px;">Order #<?php echo $order['order_number']; ?></h2>placed on <b><?php echo date('F d, Y h:i a', strtotime($order['created_date']." UTC")); ?></b><br /><?php if($order['manually_modified']==1) { ?><span class="red">Updated Manually</span><?php } ?>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="col-sm-8 infobox">
            <table class="table">
                <thead>
                    <th>&nbsp;</th>
                    <th width="35%">Item</th>
                    <th width="35%">Status</th>
                    <th width="15%">Price</th>
                </thead>
                <tbody>
                    <?php 
                    foreach ($order_details as $k=>$value){
                        if($value['personalization_id']!=''){
                            $userLog = $value['personalization_id'];
                            $userProdId = $value['product_id'];
                            $studio_id = CustomizerLog::model()->findByAttributes(array('id' =>$userLog), array('select'=>'studio_id'))->studio_id;
                            $authToken   = OuathRegistration::model()->findByAttributes(array('studio_id' => $studio_id), array('select'=>'oauth_token'))->oauth_token;
                        }
                    }
                    ?>
                <input id="omgID" value="<?php echo $userLog; ?>" style="display:none">
                <input id="omgProdID" value="<?php echo $userProdId; ?>" style="display:none">
                <input id="omgAuth" value="<?php echo  $authToken; ?>" style="display:none">
                    <?php 
                    foreach ($order_details as $k=>$value){
                        if($value['personalization_id']!=''){
                            $img_path = PGProduct::getPersionalizeImage($value['personalization_id']);
                        }else{
							if($value['pg_varient_id']){
								$img_path = PGProduct::getpgImage($value['product_id'], 'thumb', $value['pg_varient_id']);
							}else{
								$img_path = PGProduct::getpgImage($value['product_id'], 'thumb');
							}
                        }
						if($value['varient_sku']){
							$productsku = $value['varient_sku'];
						}else{
							$productsku = PGProduct::model()->find(array('select'=>'sku','condition' => 'id=:id','params' => array(':id' =>$value['product_id'])))->sku;
						}
                        ?>
                    <tr>
                        <td><img src="<?php echo $img_path; ?>" alt="<?php echo $value['name']; ?>" style="max-width:100px;"></td>
                        <td>
                            <?php echo $value['name']; ?><br>
                            <?php echo 'Quantity:'.$value['quantity']; ?><br>
                            <?php echo 'SKU:'.$productsku . ($value['personalization_id'] != 0?'_P':''); ?><br>
                            <!--<?php if($value['personalized_sku']) { ?><?php echo 'New SKU:'.$value['personalized_sku']; ?><br> <?php } ?>-->
                            <?php if(count($order_details)> 1) { ?><br><a href="javascript:void(0);" onclick="removeItem('<?php echo $order['id'];?>','<?php echo $value['id'];?>');">Remove Item</a><?php } ?>
                            <?php if($value['personalization_id']!=''){?><br><a href="javascript:void(0);" onclick="downloadData('<?php echo $value['id'];?>');">Download files</a><?php } ?>
                            <?php if($value['personalization_id']!=''){?><br><a href="javascript:void(0);" onclick="gotoprintPersonalization();">Print</a><?php } ?>
                        </td>
                        <td>
                            <?php
                            if(isset($_COOKIE['timezone'])){
                                $crdate = date('F d, Y h:i a',strtotime("+{$_COOKIE['timezone']} minute",  strtotime($order['created_date'])));
                            }else{
                                $crdate = date('F d, Y h:i a', strtotime($order['created_date']." UTC")); 
                            }
                            $statuslog = PGOrderStatusLog::getOrderStatusLog($value['id']);
                            if(!empty($statuslog)){
                                echo $status[1].'-'.$crdate."<br>"; 
                                foreach ($statuslog as $key1 => $value1) {
                                    if(isset($_COOKIE['timezone'])){
                                        echo $status[$value1['to_status']].'-'.date('F d, Y h:i a', strtotime("+{$_COOKIE['timezone']} minute",  strtotime($value1['created_date'])))."<br>";
                                    }else{
                                        echo $status[$value1['to_status']].'-'.date('F d, Y h:i a', strtotime($value1['created_date']." UTC"))."<br>";
                                    }
                                }
                            }else{
                                if($value['item_status']==9){
                                    $stmsg = $status[1];                                        
                                }else{
                                    $stmsg = $status[$value['item_status']];                                        
                                }
                                echo $stmsg.'-'.$crdate;
                            }
                            ?>
                            <br /><?php if($value['cds_tracking']!='' && $value['tracking_url']!='') { ?> Tracking Code <a href="<?php echo $value['tracking_url'];?>" target="_blank"><?php echo $value['cds_tracking'];?></a><?php } ?>
                            <?php 
                            if($order['cds_order_status']!='-1' && $order['transaction_status']!='Pending')
                            {
                            ?>
                            <br><a href="javascript:void(0);" onclick="changestatus('<?php echo $value['item_status'];?>','<?php echo $value['id'];?>');">Change Status</a>
                            <?php } ?>
                        </td>
                        <td align='right'>
                            <?php 
                            if($value['sub_total'] != '0.00'){
                                echo Yii::app()->common->formatPrice($value['sub_total'], $value['currency_id']);
                            }else{
                                echo 'Free';
                            }
                            ?>
                        </td>
                    </tr>    
                    <?php }?>
                    <tr>
                        <td rowspan="4" colspan="2"><button type="submit" class="btn btn-primary btn-sm" id="save-btn-add" onclick="addItem(<?php echo $order['id'];?>);">Add Item</button></td>
                        <td align='right'>Subtotal : </td>
                        <td align='right'>
                           <?php echo Yii::app()->common->formatPrice($order['total'],$order_details[0]['currency_id']);?>
                        </td>
                    </tr>
                    <?php if($order['discount']!='0.00'){?>
                    <tr>
                        <td align='right'>
                            Coupon Discount: <?php echo $order['coupon_code'];?>&nbsp;(
                            <?php if($order['discount_type']){
                                echo $order['discount_amount'].'% OFF';
                            }else{
                                echo Yii::app()->common->formatPrice($order['discount_amount'],$order['coupon_currency']).'&nbsp;OFF';
                            }?>
                            )
                        </td>
                        <td align='right'>
                            <?php  echo Yii::app()->common->formatPrice($order['discount'],$order_details[0]['currency_id']); ?>
                        </td>
                    </tr>
                    <?php }?>
                    <tr>
                        <td align='right'>Shipping Cost : </td>
                        <td align='right'>
                           <?php echo Yii::app()->common->formatPrice($order['shipping_cost'],$order_details[0]['currency_id']);?>
                        </td>
                    </tr>
                    <tr>
                        <td align='right'>Total : </td>
                        <td align='right'>
                           <?php echo Yii::app()->common->formatPrice(($order['grand_total']),$order_details[0]['currency_id']);?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4">
            <div class="infobox">
                <h3>Customer Info</h3>
                <p>Name : <?php echo $userinfo['display_name'];?></p>
                <p>Email : <?php echo $userinfo['email'];?></p>
            </div>
            <div class="infobox">
                <h3>Shipping Address</h3>                
                <div id="shipadd">
                <p>
                    <?php if($addr['manually_modified']==1){?><span class="red">Updated Manually</span><br /><?php } ?>
                    <?php echo 'Name: '.$addr['first_name']." ".$addr['last_name']; ?><br>
                    <?php echo $addr['address']?>,<?php echo $addr['address2']?><br>
                    <?php echo $addr['city']?><br>
                    <?php echo $addr['state']?>
                    <?php 
                        $country = Countries::model()->findByAttributes(array('code' => $addr['country']));
                        echo isset($country->country)?$country->country:$addr['country'];
                    ?>
                </p>
                <p>
                    <?php echo 'Zip : '.$addr['zip']?><br>
                    <?php echo 'Phone : '.$addr['phone_number']?>
                </p>
                <p><a href="javascript:void(0);" onclick="updateAddress('<?php echo $addr['id'];?>');">Update Address</a></p>
            </div>
            </div>
            <div class="infobox">
                <h3>Payment Status</h3>
                <p>
                    <?php 
                        if($order['cds_order_status']=='-1'){
                            echo "<em class='fa fa-exclamation-circle warning'></em>&nbsp;Payment confirmation not received from Gateway";
                        ?>
                        <br/><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">Update Status Manually</a>
                        <?php
                        }elseif($order['transaction_status']=='Pending'){
                            echo "<em class='fa fa-exclamation-circle warning'></em>&nbsp;Pending";
                        ?>
                        <br/><a href="javascript:void(0)" class="confirm" data-msg ="Are you sure to update the payment status?" onclick="confirmPayment('<?php echo $this->createUrl('shop/acceptPayment/', array('id' => $order['transactions_id'])); ?>', '<?php echo ""; ?>');">Accept Payment</a>
                        <?php
                        }else{
                            echo "<em class='icon-check green'></em>&nbsp;Successful";
                        }
                    ?>
                        <br />Payment Reference - <b><?php echo ($order['order_reference_number']!='')?$order['order_reference_number']:'Not Available'; ?></b>
                </p>
        </div>
    </div>
</div>
</div>

</div>
<div class="modal fade" id="statuspopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
<div class="modal fade" id="additempopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
<div class="modal fade is-small-Modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <form method="post" class="form-horizontal" action="<?php echo Yii::app()->baseUrl; ?>/shop/UpdateTransactionDetails">
        <input type="hidden" name="transaction_id" value="<?php echo $order['transactions_id']; ?>">
        <input type="hidden" name="orderid" value="<?php echo $order['id']; ?>">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Update payment Status</h4>
          </div>
          <div class="modal-body">
            <!-- Use this is-Scrollable block considering the Situation. Else ask the available Designer -->
            <div class="row is-Scrollable">
                <p>Search <b><?php echo $order['order_reference_number']; ?></b> in PayPal for invoice ID and put the Unique Transaction ID (from PayPal) in below text box.</p>
                <p><input type='text' placeholder="Transaction ID from PayPal" id="paypal_transaction_id" name="paypal_transaction_id"  class="form-control input-sm checkInput" required ></p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update Payment</button>
          </div>
        </div>
    </form>
    </div>
</div>
<script language="javascript">
    var user_log_id_from_product = document.getElementById("omgID").value;
    var user_log_id_product_id = document.getElementById("omgProdID").value;
    var STORE_AUTH_TOKEN =  document.getElementById("omgAuth").value;
    sessionStorage.setItem('USER_PROD_ID', user_log_id_product_id );
    sessionStorage.setItem('USER_LOG_ID', user_log_id_from_product);
    sessionStorage.setItem('HTTP_ROOT', HTTP_ROOT);
    sessionStorage.setItem('AUTH_TOKEN',STORE_AUTH_TOKEN);
    
    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
    }
    function changestatus(order_status_id,orderdetailsid){
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/StatusPopup",{'order_status_id':order_status_id,'orderdetailsid':orderdetailsid},function(res){
            $("#statuspopup").html(res).modal('show');
        });
    }

    function confirmPayment(location, msg) {
        swal({
            title: "Are you sure to update the payment status?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            window.location.replace(location);
        });

    }    
    //6063: Muvi Kart: Edit option for Oder Changes
    function updateAddress(shipping_id){
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/updateAddress",{'shipping_id':shipping_id},function(res){
            $('#shipadd').html(res);
        });
    }    
    function showShippingAddress(shipping_id){
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/showShippingAddress",{'shipping_id':shipping_id},function(res){
            $('#shipadd').html(res);
        });
    } 
        
    function validateshiping() {
        jQuery.validator.addMethod("phoneno", function (value, element) {
        return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/i.test(value);
	}, "Please enter a valid phone number");
        jQuery.validator.addMethod("zip", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 ]*$/i.test(value);
	}, "Please enter a valid Postcode /Zip");
	
        jQuery.validator.addMethod("first_name", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9.-]*$/i.test(value);
	}, "Special charecters not allowed except . -");
        var validate = $("#shipingform").validate({
            rules: {
                'ship[first_name]': {
                    required: true
                },
                'ship[address]': {
                    required: true
                },
                'ship[zip]': {
                    required: true,
                    zip: true
                },
                'ship[phone_number]': {
                    required: true,                    
                    phoneno: true,
                    minlength: 10,
                    maxlength: 15
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element);
            }
        });
        var x = validate.form();
        if (x) {
            $('#save-btn').html('Please wait...');
            $("#save-btn").prop("disabled", true);
            var data = $('#shipingform').serialize();
            $.post("<?php echo Yii::app()->baseUrl; ?>/store/saveShippingAddress", {'data': data}, function (res) {                
                if(res.isSuccess){
                    showShippingAddress(res.id);
                }else{
                    alert('Error in saving. Please try again.');
                }
            }, 'json');
        }
    }    
    function addItem(order_id){
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/addItemPopup",{'order_id':order_id},function(res){
            $("#additempopup").html(res).modal('show');
        });
    }        
    function showProductDetails(){ 
        var skuno = $("#skuno").val();
        if(skuno){
            $('#btn-additem').html('Please wait...');
            $("#btn-additem").prop("disabled", true);
            $.post("<?php echo Yii::app()->baseUrl; ?>/store/showProductDetails", {'skuno': skuno}, function (res) {                
                if(res.isSuccess){
                    $("#name").val(res.name);
                    $("#quantity").val('1');
                    $("#error").html('');
                    $('#btn-additem').html('Add Item');
                    $("#btn-additem").prop("disabled", false);
                }else{
                    $("#error").html('<span class="red">SKU number not found</span>');
                    $('#btn-additem').html('Add Item');
                    $("#btn-additem").prop("disabled", true);
                }
            }, 'json');
        }
    }
    function saveItem() {
        var validate = $("#status_form").validate({
            rules: {
                'quantity': {
                    required: true,
                    min: 1
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element);
            }
        });
        var x = validate.form();
        if (x) {
            $('#btn-additem').html('Please wait...');
            $("#btn-additem").prop("disabled", true);
            var data = $('#status_form').serialize();
            $.post("<?php echo Yii::app()->baseUrl; ?>/store/saveItem", {'data': data}, function (res) {                
                if(res.isSuccess){
                    $("#error").html('<span class="green">Item added successfully</span>');
                setTimeout(function () {
                        window.location.href = '<?php echo Yii::app()->baseUrl; ?>/store/orderdetails/id/'+res.order_id;
                    return false;
                    }, 1000);
                }else{
                    alert('Error in saving. Please try again.');
            }
        }, 'json');
    }
}   
    function removeItem(order_id,detail_id) {        
    var text = "Are you sure you want to remove this item?";
    swal({
        title: 'Remove Item',
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true,
    },
    function () {
        $.post("<?php echo Yii::app()->baseUrl; ?>/store/removeItem", {'order_id': order_id,'detail_id': detail_id}, function (res) {                
            if(res.isSuccess){
                window.location.href = '<?php echo Yii::app()->baseUrl; ?>/store/orderdetails/id/'+res.order_id;
            }else{
                alert('Error in saving. Please try again.');
            }
        }, 'json');
    });
    }
    var download_xhr;
    function downloadData(detail_id) {
        if(download_xhr && download_xhr.readyState != 4){
            download_xhr.abort();
        }
        download_xhr = $.ajax({
            async: false,
            type: "POST",
            url: '<?php echo $this->createUrl('store/downloadpersonalizationdata/'); ?>',
            data: {'detail_id': detail_id},
            dataType: 'json'
        }).done(function(data) {
            if(data.action=='success'){
                window.open(data.message, '_blank');
            } else {
                swal(data.message);
            }
        });
    }
    
    function gotoprintPersonalization(){
     window.location.href = HTTP_ROOT+'/cmscustomizeprint';
     
    }
</script> 