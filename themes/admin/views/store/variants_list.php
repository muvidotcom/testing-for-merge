<hr>
<div class="col-xs-12">
	<div class="row">
		<div class="Block">
			<div class="Block-Header">
				<div class="icon-OuterArea--rectangular">
					<i class="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>
				</div>
				<h3 class="text-capitalize f-300">Variants</h3>            
			</div>
			Add other variants for this product
			<hr>
			<div class="m-b-10">
				<a href="javascript:void(0);" onclick="addVariants('<?php echo $fieldval[0]['custom_metadata_form_id'];?>','<?php echo $fieldval[0]['id'];?>')">
					<button type="submit" class="btn btn-primary btn-default"> Add Variant </button>
				</a>
			</div>
			<div id='product_variants_div'>
				<?php $this->renderPartial('//store/variantlisting', array('fieldval'=>$fieldval, 'variantlist'=>$variantlist));?>
			</div>
		</div>
	</div>
</div>

<div id="myvariantAddmodal" class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myLargeModalLabelAdd" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodalAdddiv">

			</div>
		</div>
	</div>
</div>
<script>
	function editVariants(customformid,productid,variantSku){
        $('.editbtn').prop('disabled', true);
        var url ="<?php echo Yii::app()->getBaseUrl(); ?>/store/EditVariants";		  
        $.post(url,{'customformid':customformid,'productid':productid,'variantSku':variantSku}, function (data) {
			$('#mymodalAdddiv').html(data);
            $("#myvariantAddmodal").modal('show');
            $('.editbtn').prop('disabled',false);
        });
	}
	function addVariants(customformid,productid){
        var url ="<?php echo Yii::app()->getBaseUrl(); ?>/store/AddVariants";		  
        $.post(url,{'customformid':customformid,'productid':productid}, function (data) {
            $('#mymodalAdddiv').html(data);
            $("#myvariantAddmodal").modal('show');
        });
	}
	Array.prototype.getUnique = function () {
        var u = {}, a = [];
        for (var i = 0, l = this.length; i < l; ++i) {
            if (u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }
	function savenewvariants(obj) {
		var validate = $("#addnewfieldform").validate({
			rules: {
				"var[sku]": {required: true}
			},
			messages: {
				"var[sku]": {required: "Please enter SKU"}
			},
			errorPlacement: function (error, element) {
				error.addClass('red');
				error.insertAfter(element.parent().parent());
			},
		});
		var x = validate.form();
                var formObj = document.getElementById('addnewfieldform');
                var formData = new FormData(formObj);

		if (x) {                            
			var currency = new Array(); 
			$(".moreCurrencyDiv_new").find(".select").each(function () {
				currency.push($(this).find('select').val());
			});
			var x = currency.getUnique();
			if (x.length === currency.length) {
				//do nothing
			} else {
				swal("Currency should be unique");
				return false;
			}
			$(obj).attr('disabled','disabled');
			var url = HTTP_ROOT+"/store/SaveVariants";
                            $.ajax({
                                method: 'post',
                                url: url,
                                data: formData,
                                contentType: false,
                                cache: false,
                                processData: false,
                                dataType: 'json',
                            success: function(data){
									$(obj).removeAttr('disabled');
                                    if(data.success == 1){
                                            $('#product_variants_div').html(data.variant);  
                                            $('#mymodalAdddiv').html('');
                                            $("#myvariantAddmodal").modal('hide');                                            
                                            $("#flsmsg").html('<div class="alert alert-success"> Product variant added successfully </div>');
                                           
                                            var action =HTTP_ROOT+"/store/EditItem?uid="+data.uid;
                                            window.location.href = action;  
                                    }else if(data.success == 0){
                                            //alert(data.msg);
                                            $(".has-error").html(data.msg).show();
                                    }                           
                            }
                        });

                                }
		}
	function confirmDelete(id) {
		swal({
			  title: "Delete Variant?",
			  text: "Do you want to delete this variant?",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
			  confirmButtonText: "Yes",
			  closeOnConfirm: true,
			  html:true
		  }, function() {
				var url = HTTP_ROOT+"/store/DeleteVariant";
				$.post(url,{'id':id},function(data){
					if(data.success == 1){
						$('#product_variants_div').html(data.variant);
                                                $("#flsmsg").html('<div class="alert alert-success">Variant was deleted successfully </div>');
                                                setTimeout(location.reload(true),3000);
					}else if(data.success == 0){
						alert(data.msg);
					}
				},'json');
		  });
	}
</script>