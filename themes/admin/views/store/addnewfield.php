<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title"><?php
		if (@$cf) {
			echo "Edit";
		} else {
			echo "Add";
		}
		?> Variable Format</h3>
</div>
<div class="modal-body">
    <form action="javascript:void(0);" method="post" name="addnewfieldform" id="addnewfieldform" enctype="multipart/form-data">
		<div class="form-horizontal">
            <div class="form-group" >
                <label class="col-sm-4 toper  control-label" for="Field Name" > Physical Content Format:</label>
				<input type="hidden" value="<?php if(@$cf){echo $cf['id'];}else{echo 0;}?>" name="data[edit_id]">
				<input type="hidden" value='<?= json_encode($custom_data); ?>' id="jsoncustomdata">
				<input type="hidden" value='<?= json_encode(explode(',', @$cf['custom_field_id'])); ?>' id="jsoncustomfielddata">
                <div class="col-sm-7">
                    <div class="fg-line">
						<div class="select">
							<select class="form-control input-sm showcatform" name="data[custom_form_id]" id="form_dropdown" onchange="choosevariablefields(this);">
								<?php
								foreach ($form as $value) {
									$selected = ($value['id'] == $cf['custom_form_id']) ? 'selected' : '';
									echo '<option value=' . $value['id'] . ' ' . $selected . '>' . $value['name'] . '</option>';
								}
								reset($form);
								$first = current($form);
								$firstelement = (isset($cf['custom_form_id'])) ? $cf['custom_form_id'] : $first['id'];
								?>
							</select>
						</div>
                    </div>					
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type">Metadata Fields:</label>
                <div class="col-sm-7">
                    <div class="fg-line">							
						<select class="form-control input-sm" id='metadata_dropdown' name="data[custom_field_id][]" multiple>
							<?php 
								$cid = explode(',', $cf['custom_field_id']);
							foreach ($custom_data[$firstelement] as $key => $value) {
									if(@$cf && in_array($key, $cid)){
										$selected = "selected='selected'";
									}else{
										$selected ='';
									}
							?>
								<option value="<?= $key; ?>" <?php echo $selected;?>><?= $value; ?></option>
							<?php } ?>
						</select>                                       
                    </div>
                </div>
            </div>
		</div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" aria-hidden="true"  onclick="savenewfield(this);">Save</button>
</div>
<script>
	function savenewfield(obj) {
		var validate = $("#addnewfieldform").validate({
			rules: {				
				"data[custom_field_id][]": {required: true}
			},
			messages: {
				"data[custom_field_id][]": {required: "Please select metadata field"}
			},
			errorPlacement: function (error, element) {
				error.addClass('red');
				error.insertAfter(element.parent());
			},
		});
		var x = validate.form();		
		if (x) {
			var url = "<?php echo Yii::app()->baseUrl; ?>/store/SaveVarible";
			document.addnewfieldform.action = url;
			document.addnewfieldform.submit();
		}
	}
	function choosevariablefields(obj) {
		var jsondata = $('#jsoncustomdata').val();
		var JSONObject = JSON.parse(jsondata);
		var formid = $(obj).val();
		var JSONObjectNew = JSONObject[formid];
		var peopleHTML = "";
		var selected = "";
		var jsoncustomdata = JSON.parse($('#jsoncustomfielddata').val());
		for (var key in JSONObjectNew) {
			if (JSONObjectNew.hasOwnProperty(key)) {
				if ($.inArray(key, jsoncustomdata) > -1){
					selected = 'selected="selected"';
				} else {
					selected = '';
				}
				peopleHTML += "<option value="+key+" "+selected+">"+JSONObjectNew[key]+"</option>";
			}
		}		
		$("#metadata_dropdown").html(peopleHTML);
	}
</script>
