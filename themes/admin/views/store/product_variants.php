<hr>
<div class="row">
    <div class="col-sm-12">
        <div class="form-horizontal">	
            <table class="table" id="list_tbl">
                <thead>
                    <tr>
						<th>CONTENT FORMAT</th>
						<th>VARIABLE FIELDS</th>
						<th class="width">ACTIONS</th>
					</tr>
                </thead>
                <?php
                if (!empty($variant['PGVariable'])) {					
                    foreach ($variant['PGVariable'] as $key => $val) {
                        ?>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo $val['name'];?>
                                </td>
                                <td>
                                    <?php echo $val['variable'];?>
                                </td>  
                                <td> 
                                    <h5><a href="javascript:void(0);" data-id="<?php echo $key;?>" onclick="editfield(this)"><em class="icon-pencil"></em>&nbsp;&nbsp; Edit</a></h5>
                                    <?php 
                                        if(in_array($val['id'], $content_ids)){$disable = 1;}else{$disable = 0;}
                                        ?>
                                    <h5>
                                        <?php if($disable){?>
                                        <a href="javascript:void(0)" class="confirm" style="cursor:not-allowed" title="Form can't be deleted as you currently have content in this form. Please delete those content and try again."><em class="icon-trash"></em>&nbsp;&nbsp; Delete</a>
                                        <?php }else{?>
                                        <a href="javascript:void(0)" class="confirm" data-msg ="Are you sure?" onclick="confirmDelete('<?php echo $key;?>');"><em class="icon-trash"></em>&nbsp;&nbsp; Delete</a>
                                        <?php }?>
                                    </h5>
                                </td>
                            </tr>	
                        </tbody>
                        <?php
                    }
                } else {
                    ?>					
                    <tbody>
                        <tr>
                            <td colspan="4">No Variable Found</td>
                        </tr>
                    </tbody>
            <?php } ?> 
            </table>
        </div>
    </div>
</div>