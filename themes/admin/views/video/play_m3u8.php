<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Muvi.com</title>
       
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"   integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>

    <link href="//vjs.zencdn.net/5.10/video-js.css" rel="stylesheet">
    <script src="//vjs.zencdn.net/5.10/video.js"></script>
    <script src="https://npmcdn.com/videojs-contrib-hls@^3.0.0/dist/videojs-contrib-hls.js"></script>
<style type="text/css">
    .video-js{height:50%;padding-top:48%}.vjs-fullscreen{padding-top:0}.RDVideoHelper{display:none}video::-webkit-media-controls{display:none!important}video::-webkit-media-controls-panel{display:none!important}#backButton,.customized-res{display:none;position:absolute}body{background:#000}#backButton{left:100px;top:70px;z-index:1}.ytp-svg-shadow{stroke:#000;stroke-opacity:.15;stroke-width:2px;fill:none}.ytp-svg-fill{fill:#ccc}.customized-res{z-index:2000;right:10%;padding:0 5px;cursor:pointer;visibility:visible;opacity:.1%;line-height:23px;-webkit-transition:visibility .1s,opacity .1s;-moz-transition:visibility .1s,opacity .1s;-o-transition:visibility .1s,opacity .1s;transition:visibility .1s,opacity .1s;background-color:#07141e;background-color:rgba(7,20,30,.7);border-radius:3px}#video-res-sec ul,#video-res-sec ul li{list-style:none;margin:0;padding:0}.textlftAlg{text-align:left}.textRgtAlg{text-align:right;float:right}
            #play{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }


            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }


            #pause{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }
            @-webkit-keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform: 5%; }
                50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
                0%,100% { opacity: 0; }
                50% { opacity: 1; }
                from { transform:  5%; }
                50% { transform: scale(1.4); }
            }
            #pause_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none;
            }
            #play_touch{
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                display: none; 
            }
</style>
 </head>
    <body>
   
   <div class="videocontent" style="overflow:hidden;">
    <video id="video_player" class="video-js vjs-default-skin" controls autoplay preload="auto" autobuffer style="width: 100%;"  webkit-playsinline>
      <source src="<?php echo $thirdparty_url; ?>" type="application/x-mpegURL">
    </video>  
    </div>
    
        <script type="text/javascript">
            var videoOnPause = 0;
            var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
            var player = videojs('video_player');
            player.play();
            $(document).ready(function () {
                $('#video_player').append('<img src="/images/touchpause.png" id="pause_touch" />');
                $('#video_player').append('<img src="/images/touchplay.png" id="play_touch" />');
                $('#video_player').append('<img src="/images/pause.png" id="pause"/>');
                $('#video_player').append('<img src="/images/play-button.png" id="play"/>');
                player.ready(function () {
                    if (is_mobile === 0) {
                        $('#video_player_html5_api').on('click', function (e) {
                            e.preventDefault();
                            if (player.paused()) {
                                $('#pause').hide();
                                $('#play').show();
                            } else {
                                $('#play').hide();
                                $('#pause').show();
                            }
                        });
                    } else {
                        $('#pause_touch').bind('touchstart', function (e) {
                            player.pause();
                            $('#play_touch').show();
                            $('#pause_touch').hide();
                        });
                        $('#play_touch').bind('touchstart', function (e) {
                            player.play();
                            setTimeout(function () {
                                $('#play_touch').hide();
                            }, 500);
                        });
                        $('#video_player_html5_api').bind('touchstart', function (e) {
                            if (player.play()) {
                                $('#pause_touch').show();
                                $('#play_touch').hide();
                                setTimeout(function () {
                                    $('#pause_touch').hide();
                                }, 3000);
                            }
                        });
                    }
                });
            });
</script>
</body>
</html>





<!--//https://s3.amazonaws.com/_bc_dml/example-content/bipbop-advanced/bipbop_16x9_variant.m3u8-->
<!--    <video id="video_player" class="video-js vjs-default-skin" controls preload="auto" width=960 height=400 data-setup='{ "autoplay": true, "techOrder": ["flash"] }'>-->
















