<div class="graph-indicator text-center m-t-20 m-b-40">
    <div id="container"></div>
</div>
<div class="indicator-Desc">
    <div class="info-block">

        <p>
            <span class="grey  p-l-20">
                <em class="fa fa-square icon left-icon blue"></em>

            </span>
            <span class="h5 f-500">
                Traffic from Geography
            </span>
        </p>
    </div>
</div>
<script>
    $(function () {
        // Initiate the chart
        $('#container').highcharts('Map', {
            chart: {
                // Edit chart spacing
                spacingBottom: 15,
                spacingTop: 0,
                height:300
            },
            title: {
                text: ''
            },
            colorAxis: {
                minColor: '#B1DFF6',
                maxColor: '#338BCB',
                dataClasses: [{
                        to: 3
                    }, {
                        from: 3,
                        to: 30
                    }, {
                        from: 30,
                        to: 100
                    }, {
                        from: 100,
                        to: 300
                    }, {
                        from: 300,
                        to: 1000
                    }, {
                        from: 1000
                    }]
            },
            mapNavigation: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            exporting: { enabled: false },
            series: [{
                    data: <?php echo $data; ?>,
                    mapData: Highcharts.maps['custom/world'],
                    joinBy: ['iso-a2', 'code'],
                    name: 'Traffic from',
                    borderColor: 'black',
                    borderWidth: 0.2,
                    states: {
                        hover: {
                            borderWidth: 1
                        }
                    }
                }]
        });
    });
</script>