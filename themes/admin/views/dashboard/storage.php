<div class="indicator m-t-40 m-b-20" style="font-size: 50px;"><?php echo $storage; ?></div>
<div class="indicator-Desc">
    <h5 class="text-capitalise f-500">Storage</h5>
    <div class="info-block">
        <p>
            <span class="grey p-l-20">
                <em class="fa fa-square icon left-icon blue"></em>
                Used Storage:
            </span>
            <span class="h5">
                <?php echo $storage; ?>
            </span>
        </p>
    </div>
    <div class="info-block">
        <p>
            <span class="grey p-l-20">
                <em class="fa fa-square icon left-icon blue"></em>
                Storage Location:
            </span>
            <span class="h5">
                Amazon S3 <br><span id="storageloc" style="padding-left: 120px;"></span>
            </span>
        </p>
    </div>
</div>
