
<div class="graph-indicator text-center m-t-20 m-b-40">
    <div id="videowatchhour"></div>
</div>
<div class="indicator-Desc">
    <div class="info-block">

        <p>
            <span class="grey  p-l-20">
                <em class="fa fa-square icon left-icon blue"></em>

            </span>
            <span class="h5 f-500">
                Average Watching Duration: <?php echo $tot;?>
            </span>
        </p>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#videowatchhour').highcharts({
            chart: {
                zoomType: 'x',
                height:300
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?php echo $xdata ?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -70,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function () {
                    
                    var hours = parseInt(this.y / 3600);
                    var minutes = parseInt(parseInt(this.y / 60) % 60);
                    var seconds = parseInt(this.y % 60);
                    
                    return '<b>' + this.x + '</b><br/>' +
                            this.series.name + '-' + hours+":"+minutes+":"+seconds;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: { enabled: false },
            series: <?php echo $graphData; ?>,
        });
    });
</script>