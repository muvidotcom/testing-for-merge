
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="modalclose();">&times;</button>
    <h3 class="modal-title">Reordering Bellow Section</h3>
</div>
<div class="modal-body">
    <div class="row is-Scrollable">
         <div class="col-sm-12 m-t-20 m-b-20">
            <div class="form-horizontal">	
        <div id="sortable" >
            <?php
            
            foreach ($orderarray as $key => $value) {
                ?>
            <div class="drag-cursor" style="background: #fff;" id="<?php echo $key; ?>">
                <div  class="border-dotted m-b-10 f-300"><?php echo ucfirst($value); ?></div>
            </div>
                    <?php
                }
                ?>
        </ul>
    </div>
    </div>
</div>
        </div>
    </div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true"  onclick="modalclose();">Close</button>
</div>
<script>
    var closemodal = 0;
    $("#sortable").sortable({
        update: function () {
            var order = $('#sortable').sortable("toArray");
            $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/DivReorder", {"order": order}, function () {
                closemodal = 1;
            });
        }
    });
    $(document).ready(function () {
        $("#mymodal").modal({backdrop: 'static', keyboard: false});
    });
    function modalclose() {
        if (closemodal) {
            window.location.reload();
        }
    }
</script>