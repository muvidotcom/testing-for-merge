<input type="hidden" name="appmenu" id ="appmenu" value="1" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/application.css?v=45">
<div class="row m-t-40 m-b-40">   
    <div class="col-md-12">
        <div class="Block">      
            <div class="row">
                <div class="col-md-4">
                    <h3 class="f-300 m-t-0 m-b-20">Set the Main Menu</h3>
                    <div class="panel-group" id="res-left" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_category">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_category" aria-expanded="false" aria-controls="collapse_category">Content Categories</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_category" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_category">
                                <div class="panel-body">
                                    <form name="FRM_MN" method="post" id="FRM_MN">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="0" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <?php
                                        if(count($contentCategories) > 0) { ?>
                                            <ul>
                                                <?php
                                                foreach($contentCategories as $category) { ?>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="category-<?php echo $category['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $category['id']; ?>" name="category-values[]" id="category-<?php echo $category['id']; ?>" />
                                                                <i class="input-helper"></i><?php echo $category['category_name']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                                <li class="button-controls">
                                                    <span class="add-to-menu">
                                                        <input type="button" id="add-category" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                        <span class="spinner"></span>
                                                    </span>
                                                </li>       
                                            </ul>
                                        <?php } ?>                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--Start CMS pages for menu-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_page">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_page" aria-expanded="false" aria-controls="collapse_page">Static Pages</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_page" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_page">
                                <div class="panel-body">
                                    <form id="frmpg" name="frmpg" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="1" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />    
                                        <?php
                                        if (count($pages) > 0) {
                                            ?>
                                            <ul>
                                                <?php foreach ($pages as $page) { ?>   
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="pages-<?php echo $page['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $page['id']; ?>" name="pages[]" id="pages-<?php echo $page['id']; ?>" /> 
                                                                <i class="input-helper"></i><?php echo $page['title']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                <?php } ?>     
                                                <li class="button-controls">
                                                    <span class="add-to-menu">
                                                        <input type="button" id="add-page" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                        <span class="spinner"></span>
                                                    </span>
                                                </li>
                                            </ul>
                                        <?php } ?>                                          
                                    </form>
                                </div>
                            </div>
                        </div> 
                        <!--Start Extensions/Apps-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_apps">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_apps" aria-expanded="false" aria-controls="collapse_apps">Apps</a>
                                </h4>
                            </div>                                
                            <div id="collapse_apps" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_apps">
                                <div class="panel-body">
                                    <form id="frm_APPS" name="frm_APPS" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="3" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <?php if (count($extensions) > 0) { ?>
                                            <ul>
                                                <?php foreach ($extensions as $extension) { ?>    
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="extensions-<?php echo $extension['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $extension['id']; ?>" name="extensions[]" id="extensions-<?php echo $extension['id']; ?>" /> 
                                                                <i class="input-helper"></i><?php echo $extension['name']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                                <li class="button-controls">
                                                    <span class="add-to-menu">
                                                        <input type="button" id="add-app" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                        <span class="spinner"></span>
                                                    </span>
                                                </li> 
                                            </ul>
                                        <?php } ?>                                         
                                    </form>
                                </div>
                            </div>
                        </div>  
                        <!--Start External Links-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_external">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_external" aria-expanded="false" aria-controls="collapse_external">External Links</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_external" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_external">
                                <div class="panel-body">
                                    <form id="frm_EXT" name="frm_EXT" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="2" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <div class="form-group">
                                            <label class="control-label">Link Text</label>
                                            <div class="fg-line"><input type="text" name="menu_title" class="form-control input-sm" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">URL</label>
                                            <div class="fg-line"><input type="text" name="menu_permalink" class="form-control input-sm" /></div>
                                        </div>     
                                        <input type="submit" class="btn btn-primary" name="sbt" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>  
                    </div>                        
                </div>
                <div class="col-md-offset-4 col-md-4" id="serialization">
                    <h3 class="f-300 m-t-0 m-b-20">Menu Structure</h3>
                    <ol class="serialization vertical p-l-0" role="tablist" id="res-accordion" aria-multiselectable="true">
                        <?php
                        if (!empty($topmenuitems)) {
                            foreach ($topmenuitems as $menuitem) {
                                $parent_menu_item_id  = $menuitem['id'];
                                $item_type            = $menuitem['link_type'];
                                $value                = $menuitem['value'];
                                $menu_title           = $menuitem['title'];
                                $menu_permalink       = $menuitem['permalink'];
                                $menu_language        = $menuitem['language_id'];
                                $menu_parent_language = $menuitem['language_parent_id'];
                                $childmenus           = $menuitem['child'];
                                if($menu_parent_language > 0){
                                    $parent_menu_item_id = $menu_parent_language;
                                }
                                $this->renderPartial('menuitems', array('studio_id' => $this->studio->id, 'item_type' => $item_type, 'menu_id' => $menu_id, 'menu_item_id' => $parent_menu_item_id, 'value' => $value, 'menu_title' => $menu_title, 'menu_permalink' => $menu_permalink, 'menu_parent_language' => $menu_parent_language, 'menu_language' => $menu_language, 'language_id' => $this->language_id));
                                echo '<ol>';
                                if(!empty($childmenus)){
                                    foreach($childmenus as $childmenu){
                                        $parent_menu_item_id  = $childmenu['id'];
                                        $item_type            = $childmenu['link_type'];
                                        $value                = $childmenu['value'];
                                        $menu_title           = $childmenu['title'];
                                        $menu_permalink       = $childmenu['permalink'];
                                        $menu_language        = $childmenu['language_id'];
                                        $menu_parent_language = $childmenu['language_parent_id'];
                                        if($menu_parent_language > 0){
                                            $parent_menu_item_id = $menu_parent_language;
                                        }
                                        $this->renderPartial('menuitems', array('studio_id' => $this->studio->id, 'item_type' => $item_type, 'menu_id' => $menu_id, 'menu_item_id' => $parent_menu_item_id, 'value' => $value, 'menu_title' => $menu_title, 'menu_permalink' => $menu_permalink, 'menu_parent_language' => $menu_parent_language, 'menu_language' => $menu_language, 'language_id' => $this->language_id));
                                        echo "</li>";
                                    }
                                }
                                echo "</ol></li>";
                            }    
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    ol{list-style: none;}
    ol#res-accordion{padding-left: 0px;}
    .panel-body ul{list-style: none;}
    .panel{padding:0px;}
    #res-accordion li{margin-bottom: 5px;}
</style>    
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-sortable.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/menu.js"></script>
