<div class="row m-t-40">
    <div class="col-md-12">
        <div class="Block">
			<div class="Block-Header">
				<div class="icon-OuterArea--rectangular">
					<em class="icon-cloud-upload icon left-icon "></em>
				</div>
				<h4>Manage Template</h4>
			</div>
		</div>
		<hr>
		<?php
		if (!empty($templates)) {
			foreach ($templates as $template) {
				$template_name = $template->name;
				$template_code = $template->code;
				$screenshot = $this->siteurl . "/images/Home.jpg";
				$screenshotAudio = $this->siteurl . "/images/aeonaudio.PNG";
				$screenshotPhysical = $this->siteurl . "/images/physical.jpg";

				if ($template_code == 'onyx-android' || $template_code == 'onyx-ios') {
					?>
					<div class="row m-b-20">            
						<div class="m-b-20 col-sm-6 templatediv">
							<div class="padding-40 border-dotted">
								<div class="row">
									<div class="col-sm-6 m-b-10">
										<div class="template-Sec my-tooltip">
											<img src="<?php echo $screenshot ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
										</div>  
										<p class="text-center"> 
											<?php if ($template->preview_option == '1') { ?>
												<a class="btn m-t-10 btn-primary" target="_blank" href="<?php echo $template->app_link != "" ? $template->app_link : 'javascript:void(0);' ?>">Preview</a>
											<?php } ?>
										</p>
									</div>
									<div class="col-sm-6">
										<div class="m-b-40">
											<h4><a><?php echo $template_name; ?></a></h4>
											<?php echo $template->short_desc; ?>
										</div>
									</div>
								</div>
							</div> 
						</div>
					<?php } else if ($template_code == 'onyx-android-audio' || $template_code == 'onyx-ios-audio') { ?>
						<!-------------------  Audio  ------------------------->
						<div class="m-b-20 col-sm-6 templatediv">
							<div class="padding-40 border-dotted">
								<div class="row">
									<div class="col-sm-6 m-b-10">
										<div class="template-Sec my-tooltip">
											<img src="<?php echo $screenshotAudio ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
										</div>  
										<p class="text-center"> 
											<?php if ($template->preview_option == '1') { ?>
												<a class="btn m-t-10 btn-primary" target="_blank" href="<?php echo $template->app_link != "" ? $template->app_link : 'javascript:void(0);' ?>">Preview</a>
												<?php if ($template->store_link != "") { ?>
													<a class="btn m-t-10 btn-primary" target="_blank" href="<?php echo $template->store_link != "" ? $template->store_link : 'javascript:void(0);' ?>">Get Demo</a>                               
												<?php }
											} ?>
										</p>
									</div>
									<div class="col-sm-6">
										<div class="m-b-40">
											<h4><a><?php echo $template_name; ?></a></h4>
			<?php echo $template->short_desc; ?>
										</div>
									</div>
								</div>
							</div> 
						</div>                
					</div>
		<?php } else if ($template_code == 'onyx-android-physical' || $template_code == 'onyx-ios-physical') { ?>
					<!------------------  Physical  ------------------------->
					<div class="row m-b-20">
						<div class="m-b-20 col-sm-6 templatediv">
							<div class="padding-40 border-dotted">
								<div class="row">
									<div class="col-sm-6 m-b-10">
										<div class="template-Sec my-tooltip">
											<img src="<?php echo $screenshotPhysical ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
										</div>  
										<p class="text-center"> 
											<?php if ($template->store_link != "") { ?>
												<a class="btn m-t-10 btn-primary" target="_blank" href="<?php echo $template->store_link != "" ? $template->store_link : 'javascript:void(0);' ?>">Get Demo</a>                               
			<?php } ?>
										</p>
									</div>
									<div class="col-sm-6">
										<div class="m-b-40">
											<h4><a><?php echo $template_name; ?></a></h4>
			<?php echo $template->short_desc; ?>
										</div>
									</div>
								</div>
							</div> 
						</div>
					</div>
				<?php
				}
			}
		}
		?>

	</div>
</div>
