<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  height: 60%;
}
</style>

<div class="row m-t-40">
    <div class="col-md-12">
            <hr>
            <div class="row m-b-20">
                <?php
                    if(!empty($templates)){
                        foreach ($templates as $template) {
                        $template_name = $template->name;
                        $template_code = $template->code;
                        $template_type = $template->app_type;
                        $screenshot = $this->siteurl."/images/Home.jpg";
                        if($template_code == 'onyx-ios') {
                        ?>
                        <div class="m-b-20 col-sm-6 templatediv">
                            <div class="padding-40 border-dotted">
                                <div class="row">
                                    <div class="col-sm-6 m-b-10">
                                        <div class="template-Sec my-tooltip">
                                            <img src="<?php echo $screenshot ?>" alt="<?php echo $template_name ?>" id="screen_<?php echo $template_code ?>" class="thumbnail img-responsive">
                                        </div>  
                                        <p class="text-center"> 
                                            <?php if($template->store_link !=""){ ?>
                                            <a class="btn m-t-10 btn-primary" target="_blank" href="<?php echo $template->store_link !="" ? $template->store_link : 'javascript:void(0);' ?>">Get Demo App</a>        
                                            <?php } ?>                                          
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="m-b-40">
                                            <h4><a>Default App Template</a></h4>
                                            Default App template For <strong class="text-capitalize"><?php echo $template_type; ?> </strong>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <?php   }
                        }
                    }
                ?>
            </div>    
    </div>
</div>

            
      
