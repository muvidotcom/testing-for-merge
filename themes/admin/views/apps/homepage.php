<?php
    $hpage_layout = 2;
    $fstyle       = '';
    $allcat       = "";
    $featsection  = "checked";  
    if(!empty($home_layout)){
        $hpage_layout = $home_layout->config_value;
    }
    if($hpage_layout == 1){
        $fstyle      = 'display:none';
        $allcat      = "checked";
        $featsection = "";
    }
?>
<input type="hidden" id="img_width" name="img_width" />
<input type="hidden" id="img_height" name="img_height" />
<input type="hidden" id="apphome" name="apphome" value="1" />
<div class="row m-t-40 m-b-40">
    <div class="col-sm-12">
        <?php
        if(!empty($banner_details)){ ?>
            <div class="row m-t-20" id="banner_section">
                <div class="col-sm-12">
                    <div class="Block">
                        <div class="Block-Header">
                            <div class="icon-OuterArea--rectangular">
                                <em class="icon-docs icon left-icon "></em>
                            </div>
                            <h4>Banner</h4>                      
                        </div>
                        <hr>
                        <div class="Block-Body">
                            <?php echo $this->renderPartial('banner',array('banner_details' => $banner_details, 'banners' =>$banners, 'banner_text' => $banner_text, 'banner_url'=> $banner_url, 'base_cloud_url' => $base_cloud_url, 'all_images' => $all_images, 'studio' => $studio)) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } 
        ?>
        <div class="row m-t-20" id="featured_sections">
            <div class="col-sm-12">
                <?php echo $this->renderPartial('featuredsections', array('sections' => $featured, 'has_physical'=>$has_physical, 'home_layout' => $hpage_layout,'fstyle' => $fstyle, 'allcat' => $allcat, 'featsection' => $featsection)); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal is-Large-Modal fade" id="homePageModal" tabindex="-1" role="dialog" aria-labelledby="homePageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="upload_image_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="app_image" value="1" />
                <div class="modal-header text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="section_id1" id="section_id1" value="" />
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li id="show1" role="presentation" class="active" onclick="hide_file()">
                                <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                            </li>
                            <li id="show2" role="presentation" onclick="hide_gallery()"> 
                                <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                        <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('celeb_pic')">
                                        <input id="celeb_pic" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                        <p class="help-block"></p>
                                    </div>
                                    <input type="hidden" id="x1" name="fileimage[x1]" />
                                    <input type="hidden" id="y1" name="fileimage[y1]" />
                                    <input type="hidden" id="x2" name="fileimage[x2]" />
                                    <input type="hidden" id="y2" name="fileimage[y2]" />
                                    <input type="hidden" id="w" name="fileimage[w]"/>
                                    <input type="hidden" id="h" name="fileimage[h]"/>
                                    <div class="col-xs-12">
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="celeb_preview">
                                                <img id="preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                                <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                <input type="hidden" name="g_original_image" id="g_original_image" />
                                <input type="hidden" id="x13" name="jcrop_allimage[x13]" />
                                <input type="hidden" id="y13" name="jcrop_allimage[y13]" />
                                <input type="hidden" id="x23" name="jcrop_allimage[x23]" />
                                <input type="hidden" id="y23" name="jcrop_allimage[y23]" />
                                <input type="hidden" id="w3" name="jcrop_allimage[w3]" />
                                <input type="hidden" id="h3" name="jcrop_allimage[h3]" />
                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

                                    </div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                        <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                            <div class="preloader pls-blue  ">
                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="gallery_preview">
                                                <img id="glry_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="uplad_buton" disabled="disabled">Upload</button>
                    <button type="button" class="btn btn-default" id="cancl_upload" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php if($hpage_layout == 2){ ?>
<div id="addPopularcontnet" class="modal fade">
    <div class="modal-dialog">
        <form  method="post" id="add_content" class="form-horizontal">
            <input type="hidden" id="featured_section" name="featured_section" />
            <input type="hidden" id="fs_content_type" name="fs_content_type" />            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Popular Content</h4>
                </div>
                <div class="modal-body">                        
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name:</label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input type="text" id="title" class="form-control input-sm" name="title" placeholder="Start Typing movie name..." required>
                            </div>
                            <div id="pcontent_poster relative">
                                <div id="pcontent_poster_loader" class="preloader pls-blue text-center">
                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                    </svg>                                   
                                </div>
                                <span class="help-block" id="preview_poster"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="fcontent_save" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <input type="hidden" value="" id="movie_id" name="movie_id" />
            <input type="hidden" value="" id="is_episode" name="is_episode" />
        </form>	
    </div>
</div>
<div id="addHomepageSection" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo $this->createUrl('apps/addHomepageSection'); ?>" class="form-horizontal" method="post" id="add_homepage_section">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Home Page Section</h4>
                </div>
                <div class="modal-body">
                    <?php if ($has_physical||(((isset($content_enable) && ($content_enable['content_count'] & 4))))) { ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Content Type</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <div class="select">
                                        <select name="content_type" id="content_type" class="form-control">
                                            <option value="0">Video</option>
                                            <?php if (isset($content_enable) && ($content_enable['content_count'] & 4)) {?><option value="2">Audio</option><?php } ?>
											<?php if ($has_physical) {?><option value="1">Physical</option><?php } ?>
                                        </select>                        
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="content_type" id="content_type" value="0" />
                    <?php } ?>    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Section Name:</label> 
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input required type="text" id="section_name" class="form-control input-sm" name="section_name" placeholder="Enter Section Name" value="" />
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>	
    </div>
</div>
<?php } ?>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/homepage.css" type="text/css" >
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/homepage.js?v=2"></script>

