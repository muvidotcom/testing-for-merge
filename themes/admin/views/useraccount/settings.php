<div class="row m-t-20">
   <div class="col-sm-12">
        <form class="form-horizontal" class="update_favourite" name="update_usergencontent"  action="<?php echo Yii::app()->getBaseUrl(true) ?>/useraccount/settings" method='post'>
           <div class="form-group">   
                <div class="col-md-12"> 
                    <label class="checkbox checkbox-inline m-r-20">
                     <input type="checkbox" name="autoUserGen" value="1" <?php echo ($is_generate)?'checked="checked"':"";?>/><i class="input-helper"></i>  Enable UGC(user-generated-content).Will allow users to add content.
                    </label>
                </div>
            </div>
            <?php if($is_generate==1){?>
            <div class="form-group">   
                <div class="col-md-12"> 
                    <label class="checkbox checkbox-inline m-r-20">
                    <input type="checkbox" name="user_review" value="1" <?php echo ($review_gen)?'checked="checked"':"";?>/><i class="input-helper"></i>  Review UGC(user-generated-content).
                    </label>
                </div>
            </div>
            <?php }?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="submit" class="btn btn-primary" name="usergenbtn" id="usergenbtn" value="save" />
                </div>
            </div>
        </form>
    </div>
</div>

