<div class="container">
    <table class="table table-hover mob-compress" id="movie_list_tbl">
        <thead>
            <tr>
                <th>Content Name</th>
                <th>Uploaded By</th>
                <th>Action</th>
            </tr>  
        </thead>
        <tbody id="body_alert" class="has__message--block">
            <?php
            $studio_id = Yii::app()->user->studio_id;
            $user_name = Yii::app()->user->first_name;

            $template_name = '';
            if ($data) {
                if ($template_name == 'modern-byod') {
                    $default_img = '/img/No-Image-Default-Traditional.jpg';
                    $default_episode_img = '/img/No-Image-Default-Traditional.jpg';
                } else {
                    $default_img = '/img/No-Image-Vertical-Thumb.png';
                    $default_episode_img = '/img/No-Image-Horizontal.png';
                }
                $postUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                foreach ($data AS $key => $details) {
                    $movie_id = $details['id'];
                    $stream_id = $details['stream_id'];
                    $content_types_id = $details['content_types_id'];
                    $is_episode = $details['is_episode'];
                    $cont_id = $movie_id;
                    if ($is_episode == 1) {
                        $cont_id = $stream_id;
                    }
                    $language_id = $this->language_id;
                    $langcontent = Yii::app()->custom->getTranslatedContent($cont_id, $is_episode, $language_id);
                    if (array_key_exists($stream_id, $langcontent['episode'])) {
                        $details['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                    }
                    if (array_key_exists($movie_id, $langcontent['film'])) {
                        $details['name'] = $langcontent['film'][$movie_id]->name;
                    }
                    $views = 0;
                    if ($details['is_episode']) {
                        $views = isset($episodeViewcount[$stream_id]) ? $episodeViewcount[$stream_id] : 0;
                        if (isset($episodePosters[$stream_id]) && $episodePosters[$stream_id]['poster_file_name'] != '') {
                            $img_path = $postUrl . '/system/posters/' . $episodePosters[$stream_id]['id'] . '/episode/' . $episodePosters[$stream_id]['poster_file_name'];
                        } else {
                            $img_path = $default_episode_img;
                        }
                        $contentName = $details['name'] . ' - ' . $details['episode_title'];
                    } else {
                        $views = isset($viewcount[$movie_id]) ? $viewcount[$movie_id] : 0;

                        if ($details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                            $size = 'episode';
                        } else {
                            $size = 'thumb';
                        }
                        if (isset($posters[$movie_id]) && $posters[$movie_id]['poster_file_name'] != '') {
                            $img_path = $postUrl . '/system/posters/' . $posters[$movie_id]['id'] . '/' . $size . '/' . $posters[$movie_id]['poster_file_name'];
                        } else {
                            $img_path = $default_img;
                        }
                        $contentName = $details['name'];
                    }
                    $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));

                    if ((false === file_get_contents($img_path))) {

                        if (($details['is_episode'] || $details['content_types_id'] == 2 || $details['content_types_id'] == 4)) {
                            $img_path = $default_episode_img;
                        } else {
                            $img_path = $default_img;
                        }
                    }
                    ?>
                    <tr id="ugc_listing_<?php echo $details['stream_id']; ?>">
                        <td>
                            <a href="<?php echo 'http://' . Yii::app()->user->siteUrl . '/' . @$plink; ?>" target='_blank' >
                                <div class="Box-Container-width-Modified">
                                    <div class="Box-Container">
                                        <div class="thumbnail">
                                            <img src="<?php echo $img_path; ?>" alt="<?php echo $details['name']; ?>" >
                                        </div>
                                    </div>   
                                </div>   
                                <div class="caption" style="max-width: 255px;">
                                    <?php echo $contentName; ?>
                                </div>
                            </a>
                        </td>
                        <td><?php echo $user_name; ?></td>
                        <td style="width:470px;">
                            <?php $review_flag = $details['review_flag']; ?>

                            <?php if ($details['full_movie']) { ?>
                                <h5><a href="<?php echo $this->createUrl('video/play_video', array('movie' => $movie_id, 'movie_stream_id' => $details['stream_id'], 'preview' => true)); ?>" target="_blank"><em class="icon-control-play"></em>&nbsp;&nbsp; Preview Content</a></h5>
                            <?php } ?>

                            <h5 id="approve-<?php echo $details['stream_id']; ?>">
                                <?php if($details['review_flag']==0){?>
                                    <em class="icon-check"></em>&nbsp;&nbsp; Approved
                                <?php }else{?>
                                    <a href="javascript:;" onclick="acceptreviewflag('<?php echo $details['stream_id']; ?>')"><em class="icon-check"></em>&nbsp;&nbsp; Approve</a>
                                <?php }?>
                            </h5>

                            <h5>
                                <a href="<?php echo $this->createUrl('useraccount/editMovie/', array('movie_id' => $movie_id, 'uniq_id' => $details['uniq_id'], 'movie_stream_id' => $details['stream_id'], 'type' => 'ugc')); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; View/Edit</a>
                            </h5>

                            <h5 id="remove-<?php echo $details['stream_id']; ?>">
                                <a href="javascript:;" onclick="removeContent('<?php echo $details['id']; ?>', '<?php echo $details['stream_id'];?>')"><em class="icon-trash"></em>&nbsp;&nbsp; Remove</a>
                            </h5>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="4">
                        <?php if ($contentType) { ?>
                            <p class="text-red">Oops! You don't have any content under this category.</p>
                        <?php } else { ?>
                            <p class="text-red">Oops! You don't have any content in your UGC.</p>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table> 
</div>
<div class="row" style="margin-right:0px !important;">
    <div class="col-md-12">
        <div class="product-pagination text-right">
            <?php
            if ($data) {
                $opts = array('class' => 'pagination m-t-0');
                $this->widget('CLinkPager', array(
                    'currentPage' => $pages->getCurrentPage(),
                    'itemCount' => $item_count,
                    'pageSize' => $page_size,
                    'maxButtonCount' => 6,
                    "htmlOptions" => $opts,
                    'selectedPageCssClass' => 'active',
                    'nextPageLabel' => '&raquo;',
                    'prevPageLabel' => '&laquo;',
                    'lastPageLabel' => '',
                    'firstPageLabel' => '',
                    'header' => '',
                ));
            }
            ?>                        
        </div>
    </div>
</div>

<!--<div class="modal fade" id="reject_modal" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Top Banner?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span>Are you sure you want to <b>remove the top banner</b> from Studio?</span> 
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" data-dismiss="modal" id="sub-btn" class="btn btn-default content_id">Yes</a>

                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>-->
<script type="text/javascript">
                                    $(document).ready(function () {
                                        var flag = false;
                                        $(".reject").click(function () {
                                            //$('#reject_modal').modal('show');
                                            var id = $(this).attr('data-rejectid');
                                            if (flag == false)
                                            {
                                                $(this).prop("checked", true);
                                                flag = true;
                                                $("#tb" + id).show();
                                                // $("label[for="#tb" + id]").text("Rejected");
                                            } else
                                            {
                                                $(this).prop("checked", false);
                                                flag = false;
                                                $("#tb" + id).hide();
                                            }
                                        });
                                    });
</script>
<script type="text/javascript">
    function acceptreviewflag(id) {
        swal({
            title: "Approve UGC?",
            text: "Do you want to approve the content?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $('#approve-'+id).html('<em class="icon-check"></em>&nbsp;&nbsp; Wait...');
            $.ajax({
                type: "POST",
                url: HTTP_ROOT + "/useraccount/updatereview",
                dataType: 'json',
                data: ({movie_stream_id: id}),
                success: function (res) {
                    if(res.error==0){
                        $('#approve-'+id).html('<em class="icon-check"></em>&nbsp;&nbsp; Approved');
                        /*$('#ugc_listing_'+id).slideUp('slow');*/
                    } else {
                        
                    }
                    swal(res.message);
                }
            });
        });
    }
</script>
<script type="text/javascript">
    function removeContent(movie_id, stream_id) {
        swal({
            title: "Remove Content?",
            text: "Are you sure you want to remove this content from the UGC listing? <br> This content will parmanetly deleted from the UGC listing.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $('#remove-'+stream_id).html('<em class="icon-trash"></em>&nbsp;&nbsp; Wait...');
            $.ajax({
                type: "POST",
                url: HTTP_ROOT + "/useraccount/removeUGC",
                dataType: 'json',
                data: ({movie_id: movie_id}),
                success: function (res) {
                    if(res.error==0){
                        $('#remove-'+stream_id).html('<em class="icon-trash"></em>&nbsp;&nbsp; removed');
                        $('#ugc_listing_'+stream_id).remove();
                    } else {
                        
                    }
                    swal(res.message);
                }
            });
        });
    }
</script>
<script>
    var bootboxConfirm = 0;
    $(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            if ($('#dprogress_bar').is(":visible")) {
                //  var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.';
                var location = $(this).attr('href');
                bootbox.hideAll();
                bootbox.confirm({
                    title: "Upload In progress",
                    message: msg,
                    html: true,
                    buttons: {
                        'confirm': {
                            label: 'Stop Upload',
                            className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
                        },
                        'cancel': {
                            label: 'Ok',
                            className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
                        }
                    }, callback: function (confirmed) {
                        if (confirmed) {
                            bootboxConfirm = 1;
                            confirm(location, msg);
                        }
                    }
                });
            } else {
                confirm(location, msg);
            }
        });
    });
    function confirm(location, msg) {
        swal({
            title: "Approve Content?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            window.location.replace(location);
        });
    }
</script>
