<style type="text/css">
    .table > tbody > tr > th{border-top: 0px solid;}
    .cbd{clear:both;margin: 10px 0;height: 20px;border-top: 1px solid #F4F4F4;}
    .input-group.srch{padding: 5px;margin-top: -50px;position: initial;float: right}
    .input-group.dlt{padding: 5px;margin-top: -50px;position: relative;float: right;margin-right: 25%;}
    .cb10{clear:both;height: 10px;}
    .small-padding-left{padding-left:4px;}
    .search_cover{width:80%;}
    .no-padding-right{padding-right:0px;}
    .no-padding{padding:0px;}
</style>
<script type="text/javascript">
    function studioLogin(obj) {
        var studio_id = $(obj).attr('data-studio_id');
        var studio_name = $(obj).attr('data-studio_name');
        var studio_email = $(obj).attr('data-studio_email');
        var chkmaster = "<?php if ($relation_type == 'master') {echo 1;}else{0;}?>";
        //if(chkmaster==1){
            var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?";
        /*}else{
            var confmsg = "Are you sure you want to login to studio '" + studio_name + "'?\nYou will be logout from partner portal.";
        }*/
        if (confirm(confmsg)) {            
            $("#studio_login").show();
            $("#studio_id").val(studio_id);
            $("#studio_name").val(studio_name);
            $("#studio_email").val(studio_email);
            window.open('', 'StudioWindow');
            document.getElementById('studioLoginForm').submit();
            $("#studio_login").hide();
        }
    }
</script>
<div class="box-body table-responsive">
    <div class="col-lg-12 box-body no-padding">
        <div class="box box-primary report" style="min-height: 200px;">
            <div>
                <?php
                if ($relation_type == 'reseller') {
                    $pageurls = 'partner/reseller';
                    $newcustomer = 'ResellerCustomer';
                } elseif ($relation_type == 'master') {
                    $pageurls = 'partner/master';
                    $newcustomer = 'MasterCustomer';
                } elseif ($relation_type == 'referrer') {
                    $pageurls = 'partner/referrer';
                    $newcustomer = 'ReferrerCustomer';
                }
                ?>
                <div class="box-body table-responsive no-padding">
                    <?php    
                    //hide create button for enterprise customer
                    $logged_id=Yii::app()->user->id;
                    $all_enterprise_id=array();
                    $sql="select id from portal_user where id=7 or parent_id=7";
                    $result_id=Yii::app()->db->createCommand($sql)->queryALL();                 
                    foreach($result_id as $key=>$ids){
                    $all_enterprise_id[]=$ids['id'];
                    }
                    if(!in_array($logged_id,$all_enterprise_id)){                    
                    ?>
                    <div class="clearfix">
                        <div class="col-sm-5 small-padding-left">
                    <div class="pull-left">
                        <a href="<?php echo $newcustomer;?>"><button class="freeuser btn btn-primary btn-default m-t-10" value="csv">New Customer</button></a>
                    </div>
                        </div>
                        <div class="col-sm-7 no-padding-right">
                            <form name="customer_search" action="reseller" method="get">
                                <div class="search_cover clearfix pull-right">
                                    <div class="form-group  input-group">
                                        <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                        <div class="fg-line" style="width:100%;">
                                            <input class="filter_input form-control fg-input" name="search" id="search_content" placeholder="Search Store name, Name or Email" type="text">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <?php } ?>
                    <div class="clear"></div>
                    <div class="cb10"></div>

                    <div class="input-group col-md-6 srch">
                        <?php
                        $url = $this->createUrl($pageurls);
                        if (isset($_GET['type']) && trim($_GET['type'])) {
                            $url = $this->createUrl($pageurls . '/type/' . $_GET['type']);
                        }
                        ?>
                    </div>
                    <div class="cb10"></div>
                    <form method="post" action="javascript:void(0);" id="mngUserForm" name="mngUserForm">
                        <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <tr>                               
                                    <th class="width">Customer</th>
                                    <th>Login ID</th>
                                    <th width="250">Package</th>   
                                    <th>Customer Since</th>
                                    <th>Action</th>
                                </tr>	
                                <?php
                                if ($data) {
                                    $first = $cnt = (($pages->getCurrentPage()) * $page_size) + 1;
                                    foreach ($data as $key => $value) {
                                        $cnt++;
                                        $style1 = "";
                                        $name = (trim($value['last_name'])) ? $value['first_name'] . " " . $value['last_name'] : $value['first_name'];
                                        $status = $value['status'];
                                        $is_subscribed = $value['is_subscribed'];
                                        $is_default = $value['is_default'];
                                        $is_deleted = $value['is_deleted'];
                                        $logins = isset($value['logins']) ? $value['logins'] : 0;

                                        $type = "";
                                        if ($is_subscribed == 1 && $status == 1 && $is_deleted == 0  && $is_default == 0) {
                                            $type = "Paying";
                                            $style1 = "style='color:#00A65A;'";
                                        } else if ($is_subscribed == 0 && $status == 1 && $is_deleted == 0  && $is_default == 0) {
                                            $type = "Lead";
                                        } else if ($is_subscribed == 0 && $status == 4 && $is_deleted == 0 ) {// && $is_default == 0
                                            $type = "Cancelled";
                                        } else if ($is_subscribed == 0 && $status == 0 && $is_deleted == 1  && $is_default == 0) {
                                            $type = "Deleted";
                                        } else if ($status == 1 && $is_default == 1) {
                                            $type = "Demo";
                                        }
                                        ?>
                                        <?php if ($type == "Paying" || $type == "Lead" || $type == "Demo") {?>
                                        <tr>
                                            <td>
                                                <?php echo $value['fullname']; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['email']; ?>
                                            </td>
                                            <td>
                                                <div><?php echo $value['packages']; ?><br>
                                                    Plan: <?php echo $value['plan'].ly; ?><br>
                                                    <?php if($value['app']){ echo 'Apps: '.$value['app'];} ?><br>
                                                </div>
                                            </td>
                                            <td>
                                                <?php
                                                if (isset($_GET['type']) && trim($_GET['type']) == 1) {

                                                    if (trim($value['subscription_start']) && $value['subscription_start'] != '' && $value['subscription_start'] != '00-00-00 00:00:00') {
                                                        echo Date('M d, Y', strtotime($value['subscription_start']));
                                                    } else {
                                                        print 'N/A';
                                                    }
                                                } else if (isset($_GET['type']) && trim($_GET['type']) == 3) {
                                                    if (trim($value['start_date']) && $value['start_date'] != '' && $value['start_date'] != '00-00-00 00:00:00') {
                                                        echo Date('M d, Y', strtotime($value['start_date']));
                                                    } else {
                                                        print 'N/A';
                                                    }
                                                } else {
                                                    if (trim($value['created_at']) && $value['created_at'] != '' && $value['created_at'] != '00-00-00 00:00:00') {
                                                        echo date('M d, Y', strtotime($value['created_at']));
                                                    } else {
                                                        print 'N/A';
                                                    }
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <div>
                                                    <?php $value['studio_id'] = ($value['studio_id'] == 3113)?3058:$value['studio_id'];?>
                                                    <a href="javascript:void(0);" data-studio_id="<?php echo $value['studio_id']; ?>" data-studio_name="<?php echo $value['fullname']; ?>" data-studio_email="<?php echo $value['email']; ?>" onclick="studioLogin(this);" class="">Log in to customer account</a><br>
                                                 <?php  if(!in_array($logged_id,$all_enterprise_id)){  ?>
                                                    <a href="javascript:void(0);" data-studio_id="<?php echo $value['studio_id']; ?>" data-studio_name="<?php echo $value['fullname']; ?>" data-studio_email="<?php echo $value['email']; ?>" data-resellerid="<?php echo $value['reseller_id']; ?>" data-userid="<?php echo $value['user_id'];?>" onclick="cancel_reseller_account(this)">Delink Account</a>
                                                 <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }?>
                                        <?php
                                    }
                                    if ($items_count > $page_size) {
                                        ?>
                                        <tr>
                                            <td colspan="7" style="text-align: right;">
                                                <span style="font-size: 10;">Showing <?php echo $first . " to " . --$cnt; ?> of <?php echo $items_count; ?></span>
                                                <?php
                                                $this->widget('CLinkPager', array(
                                                    'currentPage' => $pages->getCurrentPage(),
                                                    'itemCount' => $items_count,
                                                    'pageSize' => $page_size,
                                                    'maxButtonCount' => 6,
                                                    'nextPageLabel' => 'Next &gt;',
                                                    'header' => '',
                                                ));
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="7">No Record found!!!</td>
                                    </tr>	
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Extend trial period Popup start -->
<div id="extendTrialPopup" class="modal fade" data-backdrop="static" data-keyboard="false"></div>
<!-- Extend trial period Popup end -->

<?php
if ($relation_type == 'master') {
    $login_url = "http://" . $domain . "/login/studioLogin";
    $method="get";
} else{
    $method="post";
	if (stristr($_SERVER['HTTP_HOST'], "partners.muvi.com") && ($referer_id == 7)) {
		$login_url = "https://sonydadc.muvi.com/login/studioLogin";
	}else if (stristr($_SERVER['HTTP_HOST'], "partners.muvi.com") ) {
        $login_url = "https://www.muvi.com/login/studioLogin";
    } else if (stristr($_SERVER['HTTP_HOST'], "partners.muvi.in")) {
        $login_url = "http://www.studio.muvi.in/login/studioLogin";
    } else if (stristr($_SERVER['HTTP_HOST'], "partners.idogic.com")) {
        $login_url = "http://www.idogic.com/login/studioLogin";
    } else if(stristr($_SERVER['HTTP_HOST'], "partners.edocent.com")) {
        $login_url = "http://www.edocent.com/login/studioLogin";
    }else{
        $login_url = "http://devstudio.satyajit.com/login/studioLogin";
    }
}
?>
<div id="studio_login" style="display: none;">
    <form id="studioLoginForm" name="studioLoginForm" method="<?php echo $method;?>" action="<?php echo $login_url; ?>" target="StudioWindow">
        <input type="hidden" id="studio_id" name="studio_id" value="" />
        <input type="hidden" id="studio_name" name="studio_name" value="" />
        <input type="hidden" id="studio_email" name="studio_email" value="" />
    </form>
</div>

<!-- Change password Popup start -->
<div id="changePassword" class="modal fade"></div>
<!-- Change password Popup end -->
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs col-sm-12" id="mymodaldiv" >

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<!-- delink modal start-->
<div id="delinkpopup" class="modal fade" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow: hidden;">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're de-linking your customer<br>please don't refresh or close this page.</div>
                <div><img src="/images/payment_loading.gif" style="padding:5px;"></div>
            </div>
        </div>
    </div>
</div>
<!-- delink modal end-->
<script type="text/javascript">
    $(function () {
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('#from_date').datepicker({
            startDate: today,
            format: 'mm-dd-yyyy'
        });

        $('#to_date').datepicker({
            startDate: today,
            format: 'mm-dd-yyyy'
        });
    });
</script>
<script type="text/javascript">
    function cancel_reseller_account(data){ 
     var studio_name = $(data).attr('data-studio_name');
    swal({
       title: "Are you sure?",
       text: "Are you sure you want to Delink "+studio_name+"? Click Yes to permanently delink "+studio_name+" from your account",
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Yes, delink it!",
       closeOnConfirm: true
     },
     function(){
        $("#delinkpopup").modal('show');
        var studio_id = $(data).attr('data-studio_id');
        var reseller_id = $(data).attr('data-resellerid');
        var user_id = $(data).attr('data-userid');
         $.post('/partnerPayment/cancelcustomer',{'studio_id':studio_id,'reseller_id':reseller_id,'user_id':user_id},function(res){
               location.reload();
         });        
       
     });     

    }
 </script>
 <?php 
 if($display_subscription_popup == 1){ 
 ?>
    <script type="text/javascript">
    $(document).ready(function(){
        $.post('/partnerPayment/subscription',function(res){
        $('#mymodaldiv').html(res);
       $("#mymodal").modal('show');
    });
    });
    </script>
 <?php 
    } 
    ?>