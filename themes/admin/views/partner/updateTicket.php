<style>
    
    .success_flash_msg {display:none;margin-top:-4% !important;}
    .success_flash_msg {display:none;margin-top:-4% !important;}
</style>
<div class="alert alert-success alert-dismissable flash-msg success_flash_msg">
<i class="fa fa-check"></i>
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
<span class="success_msg"></span>
</div>

   
    <div class="row m-t-40">
        <div class="col-md-8">
            <div class="form Block">
                 <?php
 
//       $form = $this->beginWidget('CActiveForm', array(
//                    'id' => 'ticket-form',
//                    
//                    'htmlOptions' => array('onsubmit'=>"return checkDescription(event);", 'class'=>'form-horizontal','enctype' => 'multipart/form-data'),
//                    'action' => Yii::app()->createUrl('ticket/updateTicket'),
//                ));
//               
//    $this->breadcrumbs = array(
//        'Update Ticket',
//    );
    ?>
<form id="ticket-form" name="ticket-form" action="<?php echo Yii::app()->request->baseUrl; ?>/partner/updateTicket" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return checkDescription(event);">  
               
                <div class="form-group">
		 <label class="col-md-4 control-label" >Store</label>
                  <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                            <select class="form-control" id="master_id" name="TicketForm[ticket_master_id]" >
                              <option value="0" <?php if($ticket['portal_user_id']){ ?>selected <?php } ?> >All Stores</option>  
                             <?php foreach($master_id as $key=>$val) { ?>
                             <option value="<?php echo $key; ?>" <?php if($ticket['ticket_master_id']==$key) { ?>selected <?php } ?>><?php echo $val ;?></option>
                             <?php } ?>                     
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                 
    
                <div class="form-group add-ticket demo">
                    <input type="hidden" name="TicketForm[id_ticket]" id="id_ticket" value="<?php echo $ticket['id']; ?>" />
                      
                         <label class="col-md-4 control-label" id="title">Title</label>
                     
                    <div class="col-md-8">
                        <span class="hint"></span>
                        <div class="fg-line">
                            <input type="text"  class="checkSpace form-control input-sm" name="TicketForm[title]"  id="txt1" required value="<?php if(isset($_GET['error'])) echo stripslashes($_SESSION['title']); else echo stripslashes($ticket['title']);?>">
                        </div>
                     </div>
                </div>
		 <div class="form-group add-ticket demo">
                      
                  
                         <label class="col-md-4 control-label" id="Description">Description</label>
                    
                      
                      <div class="col-md-8">
                        <div class="fg-line">
                            <textarea class="checkSpace form-control input-sm" name="TicketForm[description]" placeholder="Describe your support request" rows='8' cols='130' id="txt1" required><?php if ($_GET['error']) echo stripslashes($_SESSION['description']);
                    else echo stripslashes($ticket['description']); ?></textarea>
                        </div>
                    <span class="error"><?php if ($_GET['error']) echo "Special characters not allowed"; ?></span>
                    </div>
                </div>
		
               
                <?php if($ticket['status']=='Closed'){ ?>
                 <div class="form-group">
		
                         <label class="col-md-4 control-label" id="Status">Status</label>
                   
                     
                     <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select class="form-control"  name="TicketForm[status]">
                              <option value="New" <?php if($ticket['status']=='New'){ ?>selected <?php } ?>>New</option>
                              <option value="Working" <?php if($ticket['status']=='Working'){ ?>selected <?php } ?> >Working</option>
                              <option value="Closed" <?php if($ticket['status']=='Closed'){ ?>selected <?php } ?>>Closed</option>
                              <option value="Re-opened" <?php if($ticket['status']=='Re-opened'){ ?>selected <?php } ?>>Re-opened</option>
                                </select>
                            </div>
                        </div>
                     
		
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    
                         <label class="col-md-4 control-label" id="priority">Priority</label>
                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                             
                             <select class="form-control"  name="TicketForm[priority]">
                              <option value="Critical" <?php if($ticket['priority']=='critical'){ ?>selected <?php } ?>>Critical</option>
                              <option value="High" <?php if($ticket['priority']=='high'){ ?>selected <?php } ?> >High</option>
                              <option value="Medium" <?php if($ticket['priority']=='medium'){ ?>selected <?php } ?>>Medium</option>
                              <option value="Low" <?php if($ticket['priority']=='low'){ ?>selected <?php } ?>>Low</option>
                             </select>
                            </div>
                        </div>
                   
                    </div>
                </div>
                 <div class="form-group">
		
                 <label class="col-md-4 control-label" id="type">Type</label>
                
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                             
                            <select class="form-control"  name="TicketForm[type]">
                              <option value="Issue" <?php if($ticket['type']=='Issue'){ ?>selected <?php } ?>>Issue</option>
                              <option value="New Feature" <?php if($ticket['type']=='New Feature'){ ?>selected <?php } ?> >New Feature</option>
                              <option value="Support" <?php if($ticket['type']=='Support'){ ?>selected <?php } ?>>How To</option>
                              <option value="SetupMigration" <?php if($ticket['type']=='SetupMigration'){ ?>selected <?php } ?>>Setup/Migration</option>

                            </select>
                            </div>
                        </div>
		
		
                    </div>
                </div>
    
    
                <div class="form-group">
		
                 <label class="col-md-4 control-label" id="app">App</label>
                
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                              
                            <select class="form-control"  name="TicketForm[app]">
                                <option value="0" <?php if($ticket['app']==0){ ?>selected <?php } ?>>All</option>
                                <option value="1" <?php if($ticket['app']==1){ ?>selected <?php } ?>>Website</option>
                                <option value="2" <?php if($ticket['app']==2){ ?>selected <?php } ?>>iOS App</option>
                                <option value="3" <?php if($ticket['app']==3){ ?>selected <?php } ?>>Android App</option>
                                <option value="4" <?php if($ticket['app']==4){ ?>selected <?php } ?>>Roku App</option>
                                <option value="5" <?php if($ticket['app']==5){ ?>selected <?php } ?>>Android TV App</option>
                                <option value="6" <?php if($ticket['app']==6){ ?>selected <?php } ?>>Fire TV App</option>
                                <option value="7" <?php if($ticket['app']==7){ ?>selected <?php } ?>>Apple TV App</option>
                                <option value="8" <?php if($ticket['app']==8){ ?>selected <?php } ?>>MUVI Server</option>  
                              
                            </select>
                            </div>
                        </div>
		
		
                    </div>
                </div>
                <div class="form-group add-ticket demo">                    
                         <label class="col-md-4 control-label">CC</label>                     
                    <div class="col-md-8">
                        <span class="hint"></span>
                        <div class="fg-line">
                            <textarea class="form-control" name="TicketForm[ticket_email_cc]"><?php echo stripslashes($ticket['ticket_email_cc']);?></textarea>
                            </div>
                        <small>Add emails with separated by comma ( , ) or semicolon ( ; )</small>
                     </div>
                </div>
                <div class="form-group">
		
                    <label class="col-md-4 control-label ">Add Attachment</label>
		   
                   <div class="col-md-8">
                       <button class="btn btn-default-with-bg" id="upload_file_button1" type="button" onclick="click_browse('upload_file1')">Browse</button>
                       <input type="file" class="upload" name="upload_file1" id="upload_file1" onchange="preview1(this, '1');" accept="image/*"  style="display:none;"/>
                  
                        
                        <div id="preview1" class="m-b-10 fixedWidth--Preview relative Preview-Block"></div>
                        <div id="moreImageUpload" class=""></div>
                        
                        <div id="moreImageUploadLink" style="display:none;margin-left: 10px;">
                            <a href="javascript:void(0);" id="attachMore">Attach another file</a>
                        </div>
                     </div> 
                </div>
                
               
               
                <input type="hidden" value="<?php echo Yii::app()->getBaseUrl(true); ?>" id='BASEURL'/>
                <input type="hidden" name="prevfiles" id='prevfiles' value="<?php echo $ticket['attachment']; ?>" />
                <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" />
                <input type="hidden" name="sortBy" value="<?php echo $_GET['sortBy']; ?>" />
                <input type="hidden" name="search" value="<?php echo $_GET['search']; ?>" />
               <?php $attachment_ticket=$ticket['attachment'];
                            $image_url=$ticket['attachment_url'].$ticket['attachment'];
                            print !empty($ticket['attachment']) ? '<label class="control-label "> Attachments:</label>':''; ?>
                    
                           
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <?php
                        $studio = Yii::app()->common->getStudioId();
                        $attachment = explode(',', ltrim($ticket['attachment'], ", "));
                        if (!empty($ticket['attachment'])) {
                            foreach ($attachment as $key => $value) {
                                 $image_url=$ticket['attachment_url'].$value;
                                $value = ltrim($value, ' ');
                                $file = substr($value,strpos($value,'_')+1);
                                $ext = pathinfo($value, PATHINFO_EXTENSION);
                                $removefile="<a href='javascript:void(0);' class='remove{$key}' onclick='deleteFile({$ticket['studio_id']},{$ticket['id']},{$key});'><i class='fa fa-times'></i></a>";
                               // if ($ext == "jpg" || $ext == "JPG" || $ext == "JPEG" || $ext == "GIF" || $ext == "PNG" || $ext == "gif" || $ext == "png" || $ext == 'jpeg')
                                 //   $attachments.="<a target='_blank' data-value='{$value}' id='atag{$key}'  href='{$ticket['attachment_url']}{$ticket['studio_id']}/{$ticket['id']}/{$value}'><img src='{$ticket['attachment_url']}{$value}' height='150' width='150'></a>{$removefile}" . ', ';
                              //  else if($ext!='')
                                    $attachments.="<a target='_blank' data-value='{$value}' id='atag{$key}' href='{$ticket['attachment_url']}{$value}'> {$file} </a>{$removefile}" . '<br /> ';
                            }
                            print rtrim($attachments, ", ");
                        }
                        ?>
                    </div>
                </div>
            <div class="form-group">
                <div class=" col-md-offset-4 col-md-8  m-t-30">
               
                <button id="submit"  class="action-btn btn btn-primary m-t-30">Update Ticket</button>
                
                    <button class="action-btn btn btn-default  m-t-30" onclick="window.history.go(-1);return false;" >Back</button>
                </div>
                </div>
           
</form>
        </div><!-- form -->
    </div></div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootbox.js" type="text/javascript"></script>
<script type="text/javascript">
   function click_browse(upload){
        $('#'+upload).click();
    }
    
</script>