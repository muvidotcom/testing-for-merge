<style type="text/css">
    .toper{padding-bottom: 20px;}
</style>
<script type="text/javascript">    
    function validateNewUser() {
        $("#newmembership").html('Saving...');
        $("#newmembership").attr("disabled", true);
        var validate = $("#new_user_form").validate();
        var x = validate.form();

        if (x) {
            $.post("InsertRefercustomer", {'email': $('#email').val(),'chkemail':1}, function (res) {
                if(res!=0){
                    $('#email-error').html(res).show();
                    $("#newmembership").html('Add Referral');
                    $("#newmembership").removeAttr("disabled");
                    return false;
                }else{
                    document.new_user_form.action = 'InsertRefercustomer';
                    document.new_user_form.submit();
                }
            });
        } else {
            $("#newmembership").html('Add Referral');
            $("#newmembership").removeAttr("disabled");
        }
    }    
</script>
<form action="javascript:void(0);" method="post" name="new_user_form" id="new_user_form" enctype="multipart/form-data" class="form-horizontal">
    <div class="row form-group" id="packagediv">
        <div class="col-lg-8">
            <h3 class="text-capitalize f-300">New Referral</h3><br />
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="Company Name">Company Name</label>                
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="company_name" autocomplete="off" required="true" placeholder="Company Name"/>
                    </div>
                    <label id="company_name-error" class="error red" for="company_name"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="Primary Contact">Primary Contact</label>           
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control" name="primary_contact" autocomplete="off" required="true" placeholder="FirstName LastName"/>
                    </div>
                    <label id="primary_contact-error" class="error red" for="primary_contact"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="Email">Email Address</label>   
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="email" class="form-control  col-lg-5" name="email" id="email" autocomplete="off" required="true" placeholder="Email Address"/>
                    </div>
                    <label id="email-error" class="error red" for="email"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="Phone">Phone Number</label>
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control  col-lg-5" name="phone" id="phone" autocomplete="off" required="true" placeholder="Phone Number"/>
                    </div>
                    <label id="phone-error" class="error red" for="phone"></label>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="Proof">Relationship Proof</label>
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="file" style='width: 100%;' name="proof" id="proof" autocomplete="off" required="true"/>
                    </div>
                    <label id="proof-error" class="error red" for="proof"></label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 m-t-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="terms" id="terms" required="true"><i class="input-helper"></i>                                  
                            I agree with the <a href="<?php echo 'https://www.'.DOMAIN.'.com/partneragreement/referral';?>" target="_blank">Muvi Referral Terms</a>
                        </label>
                        <label id="terms-error" class="error red" for="terms" style="display: none;"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" onclick="return validateNewUser();" id="newmembership">Add Referral</button>
                </div>
            </div>
        </div>
    </div>
</form>