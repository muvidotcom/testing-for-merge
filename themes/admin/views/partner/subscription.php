
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">
        <?php if($reactivate_flag==1){
            echo 'Thank you for coming back!<br>Please purchase subscription to continue';
        }else{
            echo 'Purchase Subscription';
        }?>
    </h4>
</div>
<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
?>

<form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
    <div class="modal-body">
        <div class="row ">
            <div class="col-sm-12">            
                <div class="row" id="leveldiv">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Level </label>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select id="levels" class="form-control input-sm"  onchange="getPrice(this);">
                                                    <?php
                                                    $charging_now = 0;
                                                    $yearly_discount = 0;
                                                    if (isset($levels['level']) && !empty($levels['level'])) {
                                                        $cnt = 0;
                                                        foreach ($levels['level'] as $key => $value) {
                                                            if ($cnt == 0) {
                                                                $charging_now = $value['base_price'];
                                                                $yearly_discount = $value['yearly_discount'];
                                                            }
                                                            $cnt++;
                                                            ?>
                                                            <option value="<?php echo $value['id']; ?>" data-code="<?php echo $value['code']; ?>"  data-price="<?php echo $value['base_price']; ?>" data-yearly_discount="<?php echo $value['yearly_discount']; ?>"><?php echo $value['name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>					

                        <div class="form-group">
                            &nbsp;
                            <div class="col-sm-12">
                                <div class="row">
                                    <label class="col-sm-1 m-b-10 f-400 m-b-20" style="width: 3.33333%;"> Plan</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="all_plans" value="Month" name="plans" checked="checked" /><i class="input-helper"></i>Monthly
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-12">

                                <div class="row">
                                    <div class="col-sm-12">
                                        Price: <label class="control-label">
                                            $<span class="charge_now"><?php echo $charging_now; ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <input type="hidden" name="levels" id="leveltext" value="" />
                <input type="hidden" name="plan" id="plantext" value="" />
                
                
                <div id="carddetaildiv" class="row" style="display: none;">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                Charging to your card now: <label class="control-label">
                                    $<span class="charge_now"><?php echo $charging_now; ?></span>
                                </label>
                            </div>
                        </div>

                        <div id="new_card_info" class="row m-t-30" style="display: none;">
                            <div id="card-info-error" class="error red" style="display: none;"></div>
                            <div class="col-sm-6">

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Name on Card:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Card Number:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Expiry Date:</label>                    
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select name="exp_month" id="exp_month" class="form-control input-sm">
                                                    <option value="">Expiry Month</option>	
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                                    <option value="">Expiry Year</option>
                                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-sm-4">Security Code:</label>                    
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="password" id="" name="" style="display: none;" />
                                            <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Billing Address:</label>                    
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="nextbutn" onclick="showCards();">Continue to Authorize Payment</button>
        <button type="submit" class="btn btn-primary" id="nextbtn" style="display:none;">Purchase Subscription</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Thank you, Your subscription has been activated successfully.<br/>Please wait we are redirecting you...</h4>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
var levels = '';
var plans = '';

$(document).ready(function () {
    $('input').attr('autocomplete', 'off');
    $('form').attr('autocomplete', 'off');
    showPrice();
    $("#mymodal").modal({backdrop: 'static', keyboard: false});
});

function getPrice(obj) {
    showPrice();
}

function showPrice() {
    var base_price = parseFloat($('option:selected', '#levels').attr('data-price'));
    var plan = $('.all_plans:checked').val();
    var price_total = base_price;
    
    if (plan === 'Year') {
        var yearly_discount = parseInt($('option:selected', '#levels').attr('data-yearly_discount'));
        price_total = ((price_total * 12) - (((price_total * 12) * yearly_discount) / 100));
    }
    
    price_total = price_total.toFixed(2);
    $(".charge_now").html(price_total);
    
    $('#leveltext').val($('option:selected', '#levels').val());
    levels = $('#leveltext').val();

    $("#plantext").val(plan);
    plans = plan;
}

function showCards() {
    $('#leveldiv').hide();
    showPrice();
    $("#new_card_info").show();
    $('input').attr('autocomplete', 'off');
    $('form').attr('autocomplete', 'off');
    $('#carddetaildiv').show();
    
    $('#nextbutn').hide();
    $('#nextbtn').show();
}

function validateForm() {
    $('#card-info-error').hide();
    
    //form validation rules
    var validate = $("#paymentMethod").validate({
        rules: {
            payment_info: "required",
            card_name: "required",
            exp_month: "required",
            exp_year: "required",
            security_code: "required",
            address1: "required",
            city: "required",
            state: "required",
            zipcode: "required",
            card_number: {
                required: true,
                number: true
            }
        },
        messages: {
            payment_info: "Please select payment information",
            card_name: "Please enter a valid name",
            exp_month: "Please select the expiry month",
            exp_year: "Please select the expiry year",
            security_code: "Please enter your security code",
            address1: "Please enter your address",
            city: "Please enter your city",
            state: "Please enter your State",
            zipcode: "Please enter your Zipcode",
            card_number: {
                required: "Please enter a valid card number",
                number: "Please enter a valid card number"
            }
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            switch (element.attr("name")) {
                case 'exp_month':
                    error.insertAfter(element.parent().parent());
                    break;
                case 'exp_year':
                    error.insertAfter(element.parent().parent());
                    break;
                default:
                    error.insertAfter(element.parent());
            }
        }
    });

    var x = validate.form();

    if (x) {
        $("#leveltext").val(levels);
        $("#plantext").val(plans);

        $('.close').hide();
        $('#nextbtn').html('wait!...');
        $('#nextbtn').attr('disabled', 'disabled');                 
        $("#loadingPopup").modal('show');
        
        var url = "<?php echo Yii::app()->baseUrl; ?>/payment/subscribeToReseller";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        var address1 = $('#address1').val();
        var address2 = $('#address2').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var zip = $('#zipcode').val();
        var level = levels;                 
        var plan = plans;
        
        $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'levels': level, 'plan': plan}, function (data) {
            $("#loadingPopup").modal('hide');
            if (parseInt(data.isSuccess) === 1) {
                $("#successPopup").modal('show');
                setTimeout(function () {
                    document.paymentMethod.action = '<?php echo Yii::app()->baseUrl; ?>/admin/managecontent';
                    document.paymentMethod.submit();
                    return false;
                }, 5000);
            } else {
                $('.close').show();
                $('#nextbtn').html('Purchase Subscription');
                $('#nextbtn').removeAttr('disabled');
                if ($.trim(data.Message)) {
                    $('#card-info-error').show().html(data.Message);
                } else {
                    $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                }
            }
        }, 'json');
    }
}

function getMonthList() {
    var d = new Date();
    var curyr = d.getFullYear();
    var selyr = parseInt($('#exp_year').val());
    var curmonth = d.getMonth() + 1;
    var sel_month = $.trim($("#exp_month").val());
    var startindex = 1;

    if (curyr === selyr) { startindex = curmonth; }

    var month_opt = '<option value="">Expiry Month</option>';
    for (var i = startindex; i <= 12; i++) {
        var selected = '';
        if (i === parseInt(sel_month)) {
            selected = 'selected="selected"';
        }
        month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';         }
    $('#exp_month').html(month_opt);
}
</script>