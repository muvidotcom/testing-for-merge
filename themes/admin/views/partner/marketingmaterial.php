<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<div class="row">
    <div class="col-lg-12"><h2 class="m-b-40">Marketing Material</h2></div>
    <div class="col-lg-12">
        <h4><a href="/docs/Muvi Digital Brochure.pdf" target="_blank" style="font-weight: normal;">Muvi brochure (PDF)</a></h4>
    </div>
    <div class="col-lg-12">
        <h4><a href="/docs/Muvi Presentation.pdf" target="_blank" style="font-weight: normal;">Muvi Slide Deck (Powerpoint)</a></h4>
    </div>
    <div class="col-lg-3">
        <h4><a href="/img/muvi-resleller.png" target="_blank" style="font-weight: normal;">Muvi Reseller Logo</a></h4>
    </div>
    <div class="col-lg-6">
        <h4><a style="font-weight: normal;" onclick="openEmbedBox('embedbox');" href="javascript:void(0);"><em class="fa fa-chain"></em>&nbsp;&nbsp; Embed</a></h4>
        <div style="display:none;" class="animate-div" id="embed">Copied!</div>
        <h5 class="search-new" id='embedbox' style="display:none;">
            <div class="form-group input-group">
                <input type="text"  class="form-control iframeEmbed" placeholder="Embed Code..." value='<a href="https://www.muvi.com/?src=<?php echo Yii::app()->user->id; ?>" target="_blank"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/img/muvi-resleller.png" style="width: 300px;"></a>'>
                <span class="input-group-btn">
                    <button class="btn btn-default btn-blue copyToClipboard iframeEmbedButton"  data-clipboard-text='<a href="https://www.muvi.com/?src=<?php echo Yii::app()->user->id; ?>" target="_blank"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/img/muvi-resleller.png" style="width: 300px;"></a>' type="button" onclick="CopytoClipeboard('embed');">Copy!</button>
                </span>
            </div><!-- /input-group -->
        </h5>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-12">
        <h4 class="m-t-40 m-b-20">
            <i>Right click on the links and click on "Save Link As" to download these files</i>
        </h4>
    </div>
</div>
<script>
    // Copy to Clip Borad code
    clientTarget = new ZeroClipboard($('.copyToClipboard'), {
        moviePath: "<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.swf",
        debug: false
    });
    function CopytoClipeboard(id) {
        $('#' + id).css('display', 'block');
        $('#' + id).animate({
            opacity: 0,
            top: '-=75',
        }, {
            easing: 'swing',
            duration: 500,
            complete: function () {
                $('#' + id).css({'display': 'none', 'opacity': 1, top: '+=75'});
            }
        });
    }
    function openEmbedBox(embedid) {
        if ($('#' + embedid).is(':visible')) {
            $('#' + embedid).fadeOut(1000)
        } else {
            $('#' + embedid).fadeIn(1000);
        }
    }
</script>