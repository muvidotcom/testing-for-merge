<style>
    .modal-footer button{width: 66px;}
    .success_flash_msg {display:none;}
    table td, table td * { vertical-align: top;}
    .modal-open .modal {z-index: 9999;}
    .ticket_label{margin-top: 8px;font-weight: 600;}
    .ticket_content{margin-top: 8px; word-wrap: break-word;}
</style>
<div class="alert alert-success alert-dismissable flash-msg success_flash_msg">
    <i class="fa fa-check"></i>
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span class="success_msg"></span>
</div>

<?php
if (strlen($ticket['title']) > 35) {
    $page_title = wordwrap($ticket['title'], 35);
    $page_title = substr($page_title, 0, strpos($page_title, "\n"));
} else {
    $page_title = $ticket['title'];
}
$this->pageTitle = ' "Ticket  ' . $ticket['id'] . ' : ' . $page_title . '"';

$this->breadcrumbs = array(
    'Ticket Detail',
);
$std = new Studio();
$studio = $std->findByPk($ticket['studio_id']);
$ticket_master_id=$ticket['ticket_master_id'];
if($ticket_master_id>0){
$tkt_master=new TicketMaster();
$studioname=$tkt_master->findByPk($ticket_master_id)->studio_name;
}else {
$studioname="All Stores";
}  
?>

<?php if (Yii::app()->user->hasFlash('ticketList')): ?>
    <div class="flash-success">
<?php echo Yii::app()->user->getFlash('ticketList'); ?>
    </div>
<?php elseif($valid_portal_user == 0):
    echo "<span style='color:red'>You are not authorized to access this URL</span>";
 ?>
<?php else: ?>
    <div class="row m-t-40 m-b-40">
        <div class="">
            <div class="col-md-12">
                <form  method="post" action="<?php echo Yii::app()->getBaseUrl(true); ?>/partner/jumptoTicketnumber">                   
                    <div class="fg-line">
                         <input type="text" class="form-control  input-sm" name="ticket_no" autocomplete="false"  placeholder="Jump to Ticket Number" />                            
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <br />
            </div>
            <div class="col-md-12 row">

            <div class="col-md-4">
                <div class="row">
                <div class="col-md-5 ticket_label">Ticket Number :</div>
                <div class="col-md-7 ticket_content"><?php echo $ticket['id'];?> </div>
                </div>               
                <div class="row">
                <div class="col-md-5 ticket_label">Store :</div>
                <div class="col-md-7 ticket_content"><?php echo $studioname;?></div>
                </div>   
                <div class="row">
                <div class="col-md-5 ticket_label">Priority :</div>
                <div class="col-md-7 ticket_content"><?php echo ucfirst($ticket['priority']);?></div>
                </div>
                <div class="row">
                <div class="col-md-5 ticket_label">Type :</div>
                <div class="col-md-7 ticket_content"><?php print $ticket['type']==''?'--': $ticket['type'];?></div>
                </div>                
               <div class="row">
                <div class="col-md-5 ticket_label">App :</div>
                <div class="col-md-7 ticket_content"><?php echo Yii::app()->common->getMuviApps($ticket['app']); ?></div>
               </div>
               
            </div>
            
             <div class="col-md-4">    
                <div class="row">
                <div class="col-md-6 ticket_label">Last Updated :</div>
                <div class="col-md-6 ticket_content"><?php print $ticket['last_updated_date']=='0000-00-00 00:00:00'?'--': date('m-d-Y',strtotime($ticket['last_updated_date'])) ;?></div>
                </div>
                
               <div class="row">                
                <div class="col-md-6 ticket_label">Submitted :</div>
                <div class="col-md-6 ticket_content"><?php print $ticket['creation_date']=='0000-00-00 00:00:00'?'--': date('m-d-Y',strtotime($ticket['creation_date'])) ;?></div>
                </div>
                
               <div class="row">                
                <div class="col-md-6 ticket_label">ETA (Estimated Time) :</div>
                <div class="col-md-6 ticket_content"><?php print $ticket['eta']=='0000-00-00'?'--': date('m-d-Y',strtotime($ticket['eta'])) ;?></div>
                </div>
                
               <div class="row">                
                <div class="col-md-6 ticket_label">Reported by :</div>
                <div class="col-md-6 ticket_content"><?php echo Yii::app()->common->getTicketReporter($ticket['id'])?></div>
                </div>
            </div>
            
             <div class="col-md-4">
                <div class="row">                
                <div class="col-md-5 ticket_label">Status :</div>
                <div class="col-md-7 ticket_content"><?php echo $ticket['status'];?></div>
                </div>
               <div class="row">                
                <div class="col-md-5 ticket_label">Dev Hours :</div>
                <div class="col-md-7 ticket_content"><?php echo $ticket['dev_hours'];?></div>
                </div>              
               <div class="row">                
                <div class="col-md-5 ticket_label">CC :</div>
                <div class="col-md-7 ticket_content cc">
                    <?php echo $ticket['ticket_email_cc']!=''?$ticket['ticket_email_cc']:'N/A'; ?>
               </div>
               </div>                 
            </div>
            </div>
        <div class="col-md-12">
            <div class="Block form ticketlist">
                <div class="row m-t-20">
                    <div class="col-sm-8">
                        <p class="m-b-10"><?php echo '<b>Title : </b>' . $ticket['title']; ?></p>
                        <p><?php echo '<b>Description : </b>' . stripslashes(htmlspecialchars_decode(stripslashes(nl2br(trim($ticket['description']))))); ?></p>
                    </div>
                </div>
                <div class="row m-b-10">
                    <div class="col-md-12">
                       <span class="grey">
                    <?php print !empty($ticket['attachment']) ? ' Attachments:' : ''; ?><br />
                    <?php
                    $studio = $ticket['studio_id'];
                    $attachment = explode(',', ltrim($ticket['attachment'], ", "));
                    if (!empty($ticket['attachment'])) {
                    foreach ($attachment as $key => $value) {
                    $image_url = $ticket['attachment_url'] . $value;
                    $file = substr($value, strpos($value, '_') + 1);
                    $ext = pathinfo($value, PATHINFO_EXTENSION);
                    ?>
                    <a target='_blank'  href="<?php echo htmlspecialchars($image_url); ?>"><?php echo $file; ?> </a></br>
                    <?php }} ?>
 
                     </span>
                    </div>
                </div>
<div class="row m-b-10">
    <div class="col-md-12">
        <button class="action-btn btn btn-primary" onclick="editpartnerTicket('<?php echo Yii::app()->getBaseUrl(true); ?>', '<?php echo $_GET['page']; ?>', '<?php echo $_GET['sortBy']; ?>',<?php echo $ticket['id']; ?>, '<?php echo $_GET['search']; ?>');">Edit Ticket</button>
<?php if ($ticket['status'] != 'Closed') { ?>
            <button class="action-btn btn btn-danger delete-ticket" onclick="deletepartnerTicket('<?php echo Yii::app()->getBaseUrl(true); ?>',<?php echo $ticket['id']; ?>, '<?php echo $page; ?>', '<?php echo $sortby; ?>', '<?php echo $search; ?>');">Close Ticket</button>
<?php } ?>
<?php if ($ticket['status'] == 'Closed') { ?>
            <button class="action-btn btn btn-danger" onclick="editpartnerTicket('<?php echo Yii::app()->getBaseUrl(true); ?>', '<?php echo $_GET['page']; ?>', '<?php echo $_GET['sortBy']; ?>',<?php echo $ticket['id']; ?>, '<?php echo $_GET['search']; ?>');">Re-Open Ticket</button>
<?php } ?>
        <a class="action-btn btn btn-default" href="<?php echo Yii::app()->getBaseUrl(true); ?>/partner/ticketList" >Back</a>
        <hr />
    </div>
</div>
<div class="row">
                    <div class="col-md-8">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'note',
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('onsubmit' => "return checkDescription(event);", 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal'),
                            'action' => Yii::app()->createUrl('partner/addNote'),
                        ));
                        ?>

                        <input type="hidden" value="<?php echo Yii::app()->getBaseUrl(true); ?>" id='BASEURL'/>
                        <input type="hidden" value="<?php echo $ticket['id']; ?>" name="id_ticket" id="id_ticket"/>
                        <input type="hidden" name="page" value="<?php print isset($_GET['page']) ? $_GET['page'] : ''; ?>" />
                        <input type="hidden" name="sortBy" value="<?php print isset($_GET['sortBy']) ? $_GET['sortBy'] : ''; ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3">Add Update:</label>
                            <div class="col-md-9">
                                <div class="fg-line">
                                    <textarea id="cookieMsg" class="checkSpace form-control input-sm" placeholder="Add your note" name='note' rows="6" cols="80" ></textarea>
                                </div>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Add Attachment:</label>
                            <div class="col-md-9">
                                <button class="btn btn-default-with-bg" id="upload_file_button1" type="button" onclick="click_browse('upload_file1')">Browse</button>
                                <input type="file" class="upload" name="upload_file1" id="upload_file1" onchange="preview1(this, '1');" accept="image/*"  style="display:none;"/>


                                <div id="preview1" class="m-b-10 fixedWidth--Preview relative Preview-Block"></div>
                                <div id="moreImageUpload" class=""></div>

                                <div id="moreImageUploadLink" style="display:none;margin-left: 10px;">
                                    <a href="javascript:void(0);" id="attachMore">Attach another file</a>
                                </div>
                            </div> 
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
<?php echo CHtml::submitButton('Add Update', array('class' => 'action-btn btn btn-primary')); ?>
                            </div>
                        </div>

<?php $this->endWidget(); ?>   


                    </div>
                </div>
<div class="row"> 
<div class="col-md-12">
    <?php
    if (!empty($notes))
        echo '<h3 class="m-t-0 f-300  m-b-20">Notes</h3>';
    foreach ($notes as $knote => $notes) {
        ?>
    <div class="paragraph--block ">
    <div class="col-12 view_note<?php echo $notes['id']; ?>">
        <p>
            <?php
            $pos = strpos($notes['note'], 'quoted-printable');
            if ($pos) {
                $notes_explode = explode('quoted-printable', $notes['note']);


                $note_description = str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($notes_explode[1])))), 130, '<br />', true));
            } else {

                $pos1 = strpos($notes['note'], 'UTF-8');
                if ($pos1) {
                    $notes_explode1 = explode('UTF-8', $notes['note']);
                    $note_description = str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($notes_explode1[1])))), 130, '<br />', true));
                } else {
                    $note_description = str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($notes['note'])))), 130, '<br />', true));
                }
            }
            echo preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1">$1</a>', $note_description);
            ?>
        </p>

        <p><span class="grey">By : <?php echo Yii::app()->common->getTicketNoteUser($notes['id']); ?>

                At : <?php echo date('M d, Y, h:ia T', strtotime($notes['updated_date'])); ?></span></p>
        <?php
        if (!empty($notes['attachment_url'])) {
            $url = $notes['attachment_url'] . $notes['attachment'];
            ?>
            <span class="grey">
                <?php
                $studio_id = $ticket['studio_id'];
                $note_id = $notes['id'];
                $attachment_ticket = $notes['attachment'];

                print !empty($notes['attachment']) ? ' Attachments:' : '';
                ?>                                       
                <br />
                <?php
                $studio = $ticket['studio_id'];
                $attachment_note = explode(',', ltrim($notes['attachment'], ", "));

                if (!empty($notes['attachment'])) {
                    foreach ($attachment_note as $key => $value) {

                        $image_url = $notes['attachment_url'] . $value;
                        $file = substr($value, strpos($value, '_') + 1);
                        $ext = pathinfo($value, PATHINFO_EXTENSION);
                        $removefile = "<a href='javascript:void(0);' class='remove{$key}' onclick='deleteNoteFile({$note_id},{$studio_id},{$notes['id_ticket']},{$key});'><i class='fa fa-times'></i></a>";
                        ?>
                        <a target='_blank' id="atag<?php echo $key; ?>"  href="<?php echo htmlspecialchars($image_url); ?>"><?php echo $value; ?> </a><br />
                    <?php }
                } ?>
                </span>
                <?php } ?>
                </div>

     <div class="">

    <!-- Trigger the modal with a button -->
    <div class="tkt_note<?php echo $notes['id']; ?>">
<?php if ($notes['id_user'] == Yii::app()->common->getStudiosId() && ($knote == 0)) { ?>
    <button class="editnote action-btn btn btn-primary" data-toggle="modal" data-target="#myModal<?php echo $notes['id']; ?>" id="note<?php echo $notes['id']; ?>"><i class="fa fa-edit"></i></button>
<?php } ?>                         
    </div>
                                    <hr />
                                    <!-- Modal -->
<form name="edit_note" enctype = "multipart/form-data" id="edit_note">
            <div class="modal fade" id="myModal<?php echo $notes['id']; ?>" data-id="<?php echo $notes['id']; ?>" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Update Note</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <input type="hidden" value="<?php echo $notes['id']; ?>" name="id_note" id="id_note"/>
                                <input type="hidden" value="<?php echo $notes['id_ticket']; ?>" name="id_ticket" id="id_ticket"/>
                                <input type="hidden" value="<?php echo $notes['attachment']; ?>" name="attachment" id="attachment"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-4">Note</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <textarea name='note' placeholder="Your Note" class="form-control input-sm"   id="txt_note_edit"> 
                                                <?php
                                                $pos = strpos($notes['note'], 'quoted-printable');

                                                if ($pos) {
                                                    $notes_explode = explode('quoted-printable', $notes['note']);


                                                    $note_description = wordwrap(stripslashes(nl2br((trim($notes_explode[1])))), 130, '<br />', true);
                                                } else {

                                                    $pos1 = strpos($notes['note'], 'UTF-8');
                                                    if ($pos1) {
                                                        $notes_explode1 = explode('UTF-8', $notes['note']);
                                                        $note_descriptio = wordwrap(stripslashes(nl2br((trim($notes_explode1[1])))), 130, '<br />', true);
                                                    } else {
                                                        $note_description = wordwrap(stripslashes(nl2br(($notes['note']))), 130, '<br />', true);
                                                    }
                                                }
                                                echo $note_description;
                                                ?>                                                                     </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4"> 
                                        <?php
                                        $studio_id = $ticket['studio_id'];
                                        $note_id = $notes['id'];
                                        $attachment_ticket = $notes['attachment'];

                                        print !empty($notes['attachment']) ? ' Attachments:' : '';
                                        ?>
                                    </label>
                                    <div class="col-sm-8">
                                        <?php
                                        $studio = $ticket['studio_id'];
                                        $attachment_note = explode(',', ltrim($notes['attachment'], ", "));
                                        //print_r($attachment_note);
                                        if (!empty($notes['attachment'])) {
                                            foreach ($attachment_note as $key => $value) {

                                                $image_url = $notes['attachment_url'] . $value;

                                                $file = substr($value, strpos($value, '_') + 1);
                                                $ext = pathinfo($value, PATHINFO_EXTENSION);
                                                $removefile = "<a href='javascript:void(0);' class='remove{$key}' onclick='deleteNoteFile({$note_id},{$studio_id},{$notes['id_ticket']},{$key});'><i class='fa fa-times'></i></a>";
                                                ?>
                                                <a target='_blank' class="atag<?php echo $key; ?>" id="atag<?php echo $key; ?>"  href="<?php echo htmlspecialchars($image_url); ?>"><?php echo $value; ?> </a><?php echo $removefile; ?><br />
                                            <?php }
                                        } ?>

                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-md-4">Add Attachment:</label>
                                    <div class="col-md-8">
                                        <button type="button" class="btn btn-default-with-bg btn-sm" id="edit_upload_file_button1" onclick="click_browse_edit('upload_file_edit1');">Browse</button>
                                        <input type="file" class="upload" name="upload_file_edit1" id="upload_file_edit1"  accept="image/*" style="display:none;" onchange="editpreview1(this, '1');" />
                                        <div id="editpreview1" class="m-b-10 fixedWidth--Preview relative Preview-Block"></div>
                                        <div id="editmoreImageUpload" class=""></div>
                                        <div id="editmoreImageUploadLink" style="display:none;margin-left: 10px;">
                                            <a href="javascript:void(0);" id="editattachMore">Attach another file</a>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary updatenote" onclick="return updatepartnernote('<?php echo Yii::app()->getBaseUrl(true); ?>',<?php echo $notes['id']; ?>);">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                        </div>
                    </div>
                </div>
            </div>
        </form>

                                </div>
                            </div>
    <?php } ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

<?php endif; ?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="/themes/admin/js/bootstrap-typeahead.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootbox.js" type="text/javascript"></script>

<script type="text/javascript">
    function click_browse(upload) {
        $('#' + upload).click();
    }
    function click_browse_edit(upload)
    {

        $("#" + upload).click();
    }

    function deleteNoteFile(note_id, studio, id, key) {

        var url = $('#BASEURL').val() + '/partner/deletenoteImage';
        var imgName = $('#atag' + key).html();
        bootbox.dialog({
            message: "Are you sure to <b>delete this file</b>?",
            title: "Delete Attachment",
            buttons: {
                main: {
                    label: "Yes",
                    className: "",
                    callback: function () {

                        $('.remove' + key).after("<img src='" + $('#BASEURL').val() + "/images/loader.gif' height='20px' class='ajax-loader'/>");
                        $('.ajax-loader').show();
                        show_alert(note_id, url, id, studio, imgName, key);
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-primary",
                    callback: function () {
                        //do something
                    }
                }

            }
        });
    }

    function show_alert(note_id, url, id, studio, imgName, key)
    {
        jQuery.post(url, {'note_id': note_id, 'id_ticket': id, 'studio_id': studio, 'image': imgName, 'is_ajax': 1, 'key': key}, function (res) {
            //alert(res);
            var result = jQuery.parseJSON(res);
            if (result.deleted == 1) {
                $(".remove" + key).hide();
                $('.ajax-loader').hide();
                $(".atag" + key).hide();
            }
        });

    }


    $(document).on('focusin', function (e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }

    });
    function toggle_name(id)
    {
        if ($('#' + id).html() == 'View Details')
        {
            $('#' + id).html("Hide Details");
        }
        else if ($('#' + id).html() == 'Hide Details')
        {
            $('#' + id).html("View Details");
        }
    }


    function editpartnerTicket(url, page, sortby, id_ticket, search) {
        var url = url + "/partner/UpdateTicket";
        if (search != '')
            url += '/search/' + encodeURI(search);
        if (sortby != '')
            url += '/sortBy/' + sortby;
        if (page != '')
            url += '/page/' + page;
        $.post(url, {'id': id_ticket}, function (res) {
            window.location.href = url + '/id/' + id_ticket;
        })
    }

    function updatepartnernote(url, id_note) {
        var note = $("#txt" + id_note).val();
        var text_note = $("#txt_note_edit").val();
        if (text_note == '') {
            $("#ajax_load_" + id_note).hide();
            $('#myModal' + id_note).modal('hide');
            $('.success_flash_msg').addClass("alert-danger");
            $('.success_flash_msg').css('display', 'block');
            $('.success_msg').html("Please Add Content");
            window.location.reload();
        } else {
            var inValid = /^\s+$/;
            if (!inValid.test(note)) {
                $('.updatenote').after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
                var id_ticket = $("#id_ticket").val();
                var formData = new FormData($("#edit_note")[0]);
                formData.append('editor_val', $("#txt_note_edit").val());
                $.ajax({
                    url: url + "/partner/EditNote",
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (res) {
                        $("#ajax_load_" + id_note).hide();
                        $('#myModal' + id_note).modal('hide');
                        $(".view_note" + id_note).html("<div class='col-12'>" + res + "</div>");

                        $('.success_flash_msg').css('display', 'block');
                        $('.success_msg').html("Updated successfully");
                        window.location.reload();
                    }
                })
            } else {
                alert('No updates Added');
            }
        }
    }
    function deletepartnerTicket(url, id_ticket, page, sortby, search) {
        var url = url;
         swal({
        title: "Close Ticket?",
        text: "Are you sure to <b>close this ticket</b>?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        html:true,
      },
      function(){
        $('.delete-ticket').after('<button class="preloader preloader-small pls-blue btn btn-default"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"></circle></svg></button>');
        window.location.href = $('#BASEURL').val() + "/partner/DeleteTicket/" + id_ticket;
      });

    }
</script>


