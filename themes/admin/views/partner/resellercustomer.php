<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
$packagemsg = json_encode(array('muvi_studio' => 'Includes all features and white-labeled website',
    'muvi_studio_server' => 'Only CMS and API. Build your own apps',
    'muvi_studio_on_premise' => 'Same as Muvi, but videos are hosted on your server',
    'muvi_studio_enterprise' => 'Includes ALL features with additional support, training and all apps'
        ));
$applications = json_encode($packages['appilication']);

$app_code = '';
$index = $packages['package'][0]['id'];
$pricing = $packages['appilication'][$index];
foreach ($pricing as $key => $value) {
    if ($value['code'] == 'ios_app') {
        $ios_price = '$' . $value['price'];
    } elseif ($value['code'] == 'android_app') {
        $android_price = '$' . $value['price'];
    } elseif ($value['code'] == 'roku_app') {
        $roku_price = '$' . $value['price'];
    }
}
$app_price = Yii::app()->general->getAppPrice($packages['package'][0]['code']);
?>
<style type="text/css">
    label.error{color:#f55753 !important;}
    #login_errors{color:#f55753 !important;}
    .small-margin-left{margin-left:7px;}
    .no-padding{padding:0px;}
</style>
<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
    var pricingall = '';
    var packagesall = '';
    var planall = '';
    var custom_codes = '';

    $(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    
        if ($('#custom_code-div').length) {
            $('#custom_code').on('change', function () {
                if ($.trim($("#custom_code").val())) {
                    getPackageCustomeCodePrice(0);
                } else {
                    custom_codes = '';
                    $('option:selected', '#packages').attr('data-price', 0);
                    showPrice();
                }
            });
        }
    });
    function hideCards() {
        $('#packagediv').show();
        //showPrice();
        $("#new_card_info").hide();
        $("#terms-div").hide();
        $('#carddetaildiv').hide();
    }
    function showCards() {
        $('#packagediv').hide();
        //showPrice();
        $("#new_card_info").show();
        $("#terms-div").show();
        $('#carddetaildiv').show();
    }
    function validateCustomCode() {
        if ($.trim($("#custom_code").val())) {
            getPackageCustomeCodePrice(1);
        } else {
            $("#custom_code").focus();
            $('#custom_code-error').show().html('Enter custom code');
            return false;
        }
    }
    function getPackageCustomeCodePrice(arg) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/payment/validatePackageCustomCode";
        var package_code = $('option:selected', '#packages').attr('data-code')
        var custom_code = $.trim($('#custom_code').val());

        $.post(url, {'package_code': package_code, 'custom_code': custom_code}, function (res) {
            if (parseInt(res.isExists)) {
                $('#custom_code-error').html('').hide();

                custom_codes = custom_code;
                $('option:selected', '#packages').attr('data-price', res.price);
                showPrice();

                if (parseInt(arg)) {
                    $("#custom_codes").val(custom_codes);
                    validateNewUser();
                    //showCards();
                }
            } else {
                custom_codes = '';
                $('option:selected', '#packages').attr('data-price', 0);
                showPrice();
                $('#custom_code-error').show().html('Incorrect custom code. Please ask your Muvi representative');
                return false;
            }
        }, 'json');
    }

    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    function validateNewUser() {
        var validate = $("#new_user_form").validate({
            rules: {
                'customer_name': {
                    required: true
                },
                'email': {
                    required: true,
                    mail: true
                },
                'subdomain': {
                    required: true
                },
            },
            messages: {
                'customer_name': {
                    required: "Please enter Name"
                },
                'email': {
                    required: "Please enter email address",
                    //$('#errors').html(msg).show();
                    mail: "Please enter a valid email address"
                },
                'subdomain': {
                    required: "Please enter subdomin"
                },
            }
        });
        var x = validate.form();

        if (x) {
            checkEmailExists(1);
        } else {
            return false;
        }
    }
    function checkEmailExists(arg) {
        $("#nextbutn").html('Wait...');
        $("#nextbutn").attr('disabled', 'disabled');
        var url = "<?php echo Yii::app()->baseUrl; ?>/signup/checkDomainEmail";
        var email = $.trim($('#email').val());
        var wb = $('#subdomain').val();
        var newsubdomain;
        wb = wb.replace('http://', '');
        var wb_parts = wb.split(/[\s.]+/);
        if (wb_parts.length == 1) {
            //$('#subdomain').val(wb_parts + '.com');
            newsubdomain = wb_parts + '.com';
        } else {
            newsubdomain = wb;
        }
        $.post(url, {'email': email, 'subdomain': $('#subdomain').val()}, function (res) {
            $("#nextbutn").html('Add Customer');
            $("#nextbutn").removeAttr("disabled");
            if (res.succ) {
                var payform = $('input[name="payform"]:checked').val();
                if(payform == 'execting_card'){
                    $("#loadingPopup").modal('show');
                      $.post('<?php echo Yii::app()->baseUrl; ?>/partnerPayment/insertNewCustomer', {'data': $('#new_user_form').serialize()}, function (dt) {
                         var obj = JSON.parse(dt);
                         if(obj.isSuccess == 0){
                             $("#loadingPopup").modal('hide');
                             $('#card-info-error').show().html(obj.Message);
                         }else{
                             window.location="<?php echo Yii::app()->baseUrl; ?>/partnerPayment/paymentHistory";
                             return false;
                         }
                      });
                      return false;
                }
                showCards();
            } else {
                if (res.email_error || res.purchase_error) {
                    $('#email-error').show();
                    $('#email-error').html('This email address has already been used.');
                }
                if (res.domain_error) {
                    $('#subdomain-error').show();
                    $('#subdomain-error').html('Oops! Sorry this domain name has already taken by someone else.');
                }
                if (res.invalid_url) {
                    $('#subdomain-error').show();
                    $('#subdomain-error').html('Please enter a valid domain name.');
                }
            }
        }, 'json');
    }
    function getAppilication(obj) {
        var parent = $(obj).val();
        var code = $.trim($('option:selected', obj).attr('data-code'));
        $("#packages_code").val(code);
        //if (code === 'muvi_studio_enterprise') {
        //    $('option:selected', obj).attr('data-price', 0);
        //}

        var base_price = parseFloat($('option:selected', obj).attr('data-price'));
        var is_bandwidth = $('option:selected', obj).attr('data-is_bandwidth');

        if (parseInt(is_bandwidth)) {
            $(".bandwidth").show();
        } else {
            $(".bandwidth").hide();
        }
        var pkgmag = jQuery.parseJSON('<?php echo $packagemsg; ?>');
        $('#packagemessage').html(pkgmag[code]);
        var applications = jQuery.parseJSON('<?php echo $applications; ?>');
        var str = '';

        if (applications[parent]) {
            str = str + '<label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Applications</label>';
            str = str + '<div class="col-sm-12">';

            for (var i in applications[parent]) {
                str = str + '<label class="checkbox checkbox-inline m-r-20">';
                var checked = '';
                if (applications[parent][i].code === "monthly_charge") {
                    checked = 'checked="checked"';
                }
                str = str + '<input type="checkbox" class="price_chkbox" data-id="' + applications[parent][i].id + '" data-code="' + applications[parent][i].code + '" data-price="' + applications[parent][i].price + '" ' + checked + ' onclick="selectApplication();" />';
                str = str + '<i class="input-helper"></i>' + applications[parent][i].name;
                str = str + '</label>';
            }
            str = str + '</div>';

            var APP_PRICE = 0;
            $.ajax({
                url: "<?php echo Yii::app()->baseUrl; ?>/site/packageappprice",
                method: "POST",
                data: {'code': code},
                dataType: "json",
                async: false
            }).done(function (res) {
                APP_PRICE = res.app_price;
                $('#price_per_app').val(APP_PRICE);
            });

            str = str + '<div class="col-sm-12 grey">';
            str = str + 'First application is charged at $' + base_price + ' per month, others are $' + APP_PRICE + ' per month per app';
            str = str + '</div>';
        }

        $("#cloud_hosting").val(0);
        if (code === 'muvi_studio_server') {
            $("#cloud_hosting-div").show();
        } else {
            $("#cloud_hosting-div").hide();
        }

        $("#custom_code").val('');
        //if (code === 'muvi_studio_enterprise') {
        //    $("#custom_code-div").show();
        //   $("#nextbutn").attr("onclick", "validateCustomCode()").bind('click');
        //} else {
        $("#custom_code-div").hide();
        $("#nextbutn").attr("onclick", "validateNewUser()");
        //}

        $("#appilication_div").html(str);
        showPrice();
    }
    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                payment_info: "required",
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                terms_of_use: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                payment_info: "Please select payment information",
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                terms_of_use: "Please select terms of use",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function (error, element) {
                error.addClass('red');
                switch (element.attr("name")) {
                    case 'exp_month':
                        error.insertAfter(element.parent().parent());
                        break;
                    case 'exp_year':
                        error.insertAfter(element.parent().parent());
                        break;
                    default:
                        error.insertAfter(element.parent());
                }
            }
        });

        var x = validate.form();

        if (x) {
           showPrice();
            $("#packagetext").val(packagesall);
            $("#pricingtext").val(pricingall);
            $("#plantext").val(planall);
            $("#custom_codes").val(custom_codes);
            $('.close').hide();
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled', 'disabled');
           // $("#loadingPopup").modal('show');
            /*Implement payment*/
            var url = "<?php echo Yii::app()->baseUrl; ?>/partnerPayment/insertNewCustomer";
                    var card_name = $('#card_name').val();
                    var card_number = $('#card_number').val();
                    var exp_month = $('#exp_month').val();
                    var exp_year = $('#exp_year').val();
                    var cvv = $('#security_code').val();
                    var address1 = $('#address1').val();
                    var address2 = $('#address2').val();
                    var city = $('#city').val();
                    var state = $('#state').val();
                    var zip = $('#zipcode').val();
                    var pricing = pricingall;
                    var packages = packagesall;
                    var plan = planall;
                    var cloud_hosting = $.trim($("#cloud_hosting").val());
                    var custom_code = $.trim($("#custom_codes").val());
            var csrfToken = $('#csrfToken').val();
            
            $("#loadingPopup").modal('show');
            $.post(url, {'data': $('#new_user_form').serialize(),'card_name':card_name,'card_number':card_number,'exp_month':exp_month,'exp_year':exp_year,'cvv':cvv,'address1':address1,'address2':address2,'city':city,'state':state,'zip':zip,'packages':packages,'pricing':pricing,'plan':plan,'cloud_hosting': cloud_hosting, 'custom_code': custom_code, 'csrfToken': csrfToken}, function (dt) {
                var obj = JSON.parse(dt);
                if(obj.isSuccess == 0){
                        $("#loadingPopup").modal('hide');
                     $('#card-info-error').show().html(obj.Message);
                     $('#nextbtn').html('Authorize Payment Add Customer');
                     $('#nextbtn').removeAttr('disabled');
                }else{
                    window.location="<?php echo Yii::app()->baseUrl; ?>/partnerPayment/paymentHistory";
                                return false;
                            }
            }); 
                            }
                        }
    function selectApplication() {
        var check = $("input:checkbox.price_chkbox:checked").length;
            var setup_app_arr = [];
            $("input:checkbox.price_chkbox:checked").each(function(){
                setup_app_arr.push($(this).attr('data-code'));
            });
            setup_app_arr.toString;
            $("#selected_app_code").val(setup_app_arr);
            
        if (parseInt(check) === 0) {
            $("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('checked', true);
        }

        showPrice();
    }
    function cloudHosting(obj) {
        if ($(obj).is(":checked")) {
            $("#cloud_hosting").val(1);
        } else {
            $("#cloud_hosting").val(0);
        }
    }
    function showPlan(obj) {
        var plan = $(obj).val();

        if (plan === 'Year') {
            $(".monthly_payment").hide();
            $(".yearly_payment").show();
        } else {
            $(".yearly_payment").hide();
            $(".monthly_payment").show();
        }

        showPrice();
    }

    function showPrice() {
        var base_price = parseFloat($('option:selected', '#packages').attr('data-price'));
        var code = parseFloat($('option:selected', '#packages').attr('data-code'));
        var plan = $('.all_plans:checked').val();
        var price_total = base_price;
        var priceids = new Array();

        if ($('.price_chkbox').length) {
            var cnt = 0;

            $('.price_chkbox').each(function () {
                if ($(this).is(":checked")) {
                    var priceid = $(this).attr('data-id');
                    priceids.push(priceid);
                    cnt++;
                }
            });

            var APP_PRICE = $('#price_per_app').val();
            if (cnt > 1) {
                price_total = price_total + ((cnt - 1) * APP_PRICE);
            }
        }

        if (plan === 'Year') {
            var yearly_discount = parseInt($('option:selected', '#packages').attr('data-yearly_discount'));
            price_total = ((price_total * 12) - (((price_total * 12) * yearly_discount) / 100));
        }

        $("#pricingtext").val(priceids.toString());
        pricingall = $("#pricingtext").val();

        $('#packagetext').val($('option:selected', '#packages').val());
        packagesall = $('#packagetext').val();

        $("#plantext").val(plan);
        planall = plan;

        price_total = price_total.toFixed(2);
        var day_in_month= '<?php echo $number_of_day_in_month ;?>';
        var date_diff = '<?php echo $data_diffrence ;?>';
        var price_per_day = price_total/day_in_month;
        var prorated_amount = price_per_day * date_diff; 
        var round_prorated_amount = prorated_amount.toFixed(2);
        $(".charge_now").html(round_prorated_amount+'(Prorated Amount)');
        $(".new_charge").html(price_total);
    }
    $(document).ready(function () {
        showPrice();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var existing_card_default = $("#existing_card_select").val();
        $("#existing_card_id").val(existing_card_default);
    });
 function change_existing_card(card_id){
     $("#existing_card_id").val(card_id);
 }
 function hide_existing_card(data){
     if($(data).is(':checked')){
         $("#existing_card_select").hide();
     }
 }
 function show_existing_card(data){
     if($(data).is(':checked')){
         $("#existing_card_select").show();
     }
 }
 function check_both(data){
     if($(data).prop('checked')== true){
         $("#video_checkbox_two").prop('checked',true);
     }else{
         $("#video_checkbox_two").prop('checked',false);
     }
 }
</script>
<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Thank you, Your subscription has been activated successfully.<br/>Please wait we are redirecting you...</h4>
            </div>
        </div>
    </div>
</div>
<div id="card-info-error" class="error red" style="display: none;"></div>
<form action="javascript:void(0);" method="post" name="new_user_form" id="new_user_form" class="form-horizontal">
    <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />
    <input type="hidden" name="is_customer" id="is_customer" value="0">
    <input type="hidden" name="newuserid" id="newuserid" value="">
    <input type="hidden" name="newstudioid" id="newstudioid" value="">
    <input type="hidden" name="existing_card_id" id="existing_card_id" value=""/>
    <input type="hidden" name="selected_app_code" id="selected_app_code" value="monthly_charge"/>
   <!-- <h3 class="text-capitalize f-300">New Customer</h3>--><br />
    <div id="packagediv">
        <div class="col-lg-12">            
            <div class="form-group" >
                <label class="col-sm-2 control-label f-400" for="Customer Name" > Customer Name :</label>                
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="customer_name" autocomplete="false" required="true"/>
                    </div>
                    <label id="customer_name-error" class="error" for="customer_name"></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label f-400" for="Email">Email Address :</label>                
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="email" id="email" autocomplete="false" required="true"/>
                    </div>
                    <label id="email-error" class="error" for="email"></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label f-400" for="subdomain">Domain name :</label>                
                <div class="col-sm-5">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="subdomain" id="subdomain" autocomplete="false" required="true"/>
                    </div>
                    <label id="subdomain-error" class="error" for="subdomain"></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Package </label>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <div class="select">
                                    <select id="packages" class="form-control input-sm" onchange="getAppilication(this);">
                                        <?php
                                        $charging_now = 0;
                                        $yearly_discount = 0;
                                        if (isset($packages['package']) && !empty($packages['package'])) {
                                            $cnt = 0;
                                            foreach ($packages['package'] as $key => $value) {
                                                if ($cnt == 0) {
                                                    $charging_now = $value['base_price'];
                                                    $yearly_discount = $value['yearly_discount'];
                                                }
                                                $cnt++;
                                                ?>
                                                <option value="<?php echo $value['id']; ?>" data-code="<?php echo $value['code']; ?>"  data-price="<?php echo $value['base_price']; ?>" data-yearly_discount="<?php echo $value['yearly_discount']; ?>" data-is_bandwidth="<?php echo $value['is_bandwidth']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="packagemessage" class="col-sm-8 grey">Includes all features and white-labeled website</div>
                    </div>
                </div>
            </div>
            <div class="form-group" id="appilication_div">

<?php if (isset($packages['appilication'][$packages['package'][0]['id']]) && !empty($packages['appilication'][$packages['package'][0]['id']])) { ?>
                    <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Applications</label>
                    <div class="col-sm-12">

    <?php foreach ($packages['appilication'][$packages['package'][0]['id']] as $key => $value) { ?>
                            <label class="checkbox checkbox-inline m-r-20">
                                <input type="checkbox" class="price_chkbox" data-id="<?php echo $value['id']; ?>" data-code="<?php echo $value['code']; ?>" data-price="<?php echo $value['price']; ?>" <?php if (($value['code'] == $app_code) || ($value['code'] == 'monthly_charge')) { ?>checked="checked"<?php } ?> onclick="selectApplication();" />
                                <i class="input-helper"></i> <?php echo $value['name']; ?>
                            </label>

    <?php } ?>
                    </div>
                    <div class="col-sm-12 grey">
                        First application is charged at $<?php echo $charging_now; ?> per month, others are $<?php echo $app_price; ?> per month per app
                    </div>

<?php } ?>
            </div>
            <div id="cloud_hosting-div" class="form-group" style="display: none;">
                <div class="col-sm-12">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" onclick="cloudHosting(this);"/><i class="input-helper"></i> Add Cloud hosting for your application (powered by Amazon Web Services)
                        </label>
                    </div>
                    <div class="lead">
                        <label id="cloud_hosting-error" class="error red" for="cloud_hosting" style="display: none;"></label>
                    </div>
                </div>
            </div>
            <div id="custom_code-div" class="form-group" style="display: none;">
                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Custom Code</label>
                <div class="col-sm-12">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" id="custom_code" placeholder="Code given to you by Muvi representive" autocomplete="false" />
                    </div>
                    <div class="lead">
                        <label id="custom_code-error" class="error" for="custom_code" style="display: none;"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4"> Select Plan</label>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="radio">
                                <label>
                                    <input type="radio" class="all_plans" value="Month" name="plans" checked="checked" onclick="showPlan(this);" /><i class="input-helper"></i>Month
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-9 grey">
                            Pay month by month, cancel anytime
                        </div>
                    </div>
                   <!-- <div class="row">
                        <div class="col-sm-3">
                            <div class="radio">
                                <label>
                                    <input type="radio" class="all_plans" value="Year" name="plans" onclick="showPlan(this);" /><i class="input-helper"></i>Yearly <?php if (intval($yearly_discount)) { ?>(save <?php echo $yearly_discount; ?>%)<?php } ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-9 grey">
<?php if (intval($yearly_discount)) { ?>Save <?php echo $yearly_discount; ?>% by paying for a year in advance.<?php } ?> No refund if canceled during the year
                        </div>
                    </div>-->
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Projected Pricing</label>
                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                            Charging to your card now: <label class="control-label">
                                $<span class="charge_now"><?php echo $charging_now; ?></span>
                            </label>
                        </div>
                        <div class="col-sm-12 monthly_payment">
                            <div>
                                Monthly payment: 
                                <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                            </div>
                            <div>
                                Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-12 yearly_payment" style="display: none;">
                            <div>
                                Yearly payment: 
                                <label class="control-label">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="javascript:void(0);">bandwidth</a></span>
                            </div>
                            <div>
                                Next Billing date: <label class="control-label"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                            </div>
                        </div>
                    </div>              
                </div>
            </div>
        </div>
        <input type="hidden" name="relation_type" value="reseller" />
        <input type="hidden" name="plan" id="plantext" value="" />
        <input type="hidden" name="packages" id="packagetext" value="" />
        <input type="hidden" name="packages_code" id="packages_code" value="<?php echo $packages['package'][0]['code']; ?>" />
        <input type="hidden" name="pricing" id="pricingtext" value="" />
        <input type="hidden" name="cloud_hosting" id="cloud_hosting" value="" />
        <input type="hidden" name="custom_code" id="custom_codes" value="" />
        <div class="col-lg-12 m-b-40">            
            <div>
                See <a href="https://muvi.com/pricing" target="_blank">pricing</a> for more detail. Contact your Muvi representative if you have any questions.
            </div>
            <br />
      <!-- content type ------------>      
      <div class="form-group clearfix">
            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select the content type used on the platform </label>       
           <div class="col-sm-12">
                <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="content[]" value="1" type="checkbox" id="video_check" onclick="check_both(this)" checked>
                        <i class="input-helper"></i> Video 
                    </label>
                </div>
               <div class="video_checkbox hide">
                     <input class="platform_chkbox" name="content[]" checked="" value="2" type="checkbox" id="video_checkbox_two">
               </div>
                 <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="content[]" value="4" type="checkbox">
                        <i class="input-helper"></i> Audio  
                    </label>
                </div>
                 <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="content_physical[]" value="1" type="checkbox">
                        <i class="input-helper"></i> Physical  
                    </label>
                </div>     
           </div>
      </div>
      <!-- content type end------------>
            <br/>
       <!-- Monetization options ------------>      
      <div class="form-group clearfix">
            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select monetization options you wish to enable </label>       
           <div class="col-sm-12">
                <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="monetization[]" value="1" type="checkbox" checked>
                        <i class="input-helper"></i> Subscription 
                    </label>
                </div>
                 <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="monetization[]" value="2" type="checkbox">
                        <i class="input-helper"></i> Pay Per View  
                    </label>
                </div>
                 <div class="checkbox col-sm-2 no-padding">
                    <label>
                        <input class="platform_chkbox" name="monetization[]" value="4" type="checkbox">
                        <i class="input-helper"></i> Advertising   
                    </label>
                </div>     
           </div>
      </div>
      <!-- Monetization options end ------------>           
      <div class="form-group clearfix">
		<label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Pay Using</label>       
	   <div class="col-sm-6">
           <div class="col-sm-12 no-padding"> 
                   <div class="radio">
                       <label>
                           <input type="radio" name="payform" id="payform" value="new_card" onchange="javascript:hide_existing_card(this)" checked><i class="input-helper"></i>New Card
                       </label>
                   </div>
                   <?php 
                   if($number_of_card > 0){ ?>
                    <div class="radio">
                       <label>
                           <input type="radio" name="payform" id="pay_form_exe_card" value="execting_card" onchange="javascript:show_existing_card(this)" checked><i class="input-helper"></i>Existing Card
                       </label>
                        <select class="small-margin-left" onchange="javascript:change_existing_card(this.value)" id="existing_card_select">
                            <?php foreach($existing_card as $key => $val){ ?>
                            <option value="<?php echo $val['id']; ?>" <?php echo ($val['is_cancelled']==0)?'selected':'';?>><?php echo $val['card_last_fourdigit'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                   <?php } ?>
           </div>
       </div> 
</div>       
            <button type="button" class="btn btn-primary" onclick="return validateNewUser();" id="nextbutn">Add Customer</button>                

        </div>
    </div>    
</form>
<form method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);" class="form-horizontal">
   <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />
    <div id="carddetaildiv" style="display: none;">
        <div class="col-sm-12 m-b-20">
            Charging to your card now: <label class="control-label">
                $<span class="charge_now"><?php echo $charging_now; ?></span>
            </label>
        </div>
        <div id="new_card_info" style="display: none;">
            <div id="card-info-error" class="col-sm-12 error red" style="display: none;"></div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label col-sm-4">Name on Card:</label>                    
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                        </div>
                    </div>                     

                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Card Number:</label>                    
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" />
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Expiry Date:</label>                    
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <div class="select">
                                <select name="exp_month" id="exp_month" class="form-control input-sm">
                                    <option value="">Expiry Month</option>	
                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="fg-line">
                            <div class="select">
                                <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                    <option value="">Expiry Year</option>
                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Security Code:</label>                    
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="password" id="" name="" style="display: none;" />
                            <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
                        </div>
                    </div>                     
                </div>
            </div>  
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label col-sm-4">Billing Address:</label>                    
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                </div>                     
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div id="terms-div" style="display: none;">
            <div class="col-sm-12 m-t-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="terms_of_use" id="terms_of_use"><i class="input-helper"></i>                                  
                        I agree with the <a href="https://www.muvi.com/partneragreement/reseller" target="_blank">Muvi Reseller Terms</a>
                    </label>
                    <div><label id="terms_of_use-error" class="error red" for="terms_of_use" style="display: none;"></label></div>
                </div>
            </div>                     
        </div>
             
        <div class="form-group">
            <div class="col-lg-9 m-t-20">
                <label class="col-lg-4">
                    <button type="submit" class="btn btn-primary" id="nextbtn">Authorize Payment & Add Customer</button>
                </label>
                <div class="col-lg-1">
                    <button type="button" class="btn btn-default" id="backbtn" onclick="hideCards();">Back</button>
                </div>
            </div>
        </div>
    </div>
</form>
<input type="hidden" name="price_per_app" id="price_per_app" value="<?php echo $app_price; ?>" />