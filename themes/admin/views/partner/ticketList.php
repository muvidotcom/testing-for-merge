<?php 
    $url = Yii::app()->request->url;
    $url_arr = explode('?', $url);
    if(count($url_arr) > 1){
        $url_export = '?'.$url_arr[1];
    }
?>
<div class="row m-b-40">
    <div class="col-sm-12">
        <ul class="list-inline m-l-0">
            <li>
                <a href="<?php //echo Yii::app()->getBaseUrl(true);     ?>/partner/addTicket" class="btn btn-primary m-t-10">Add Ticket</a>
                <button class="btn btn-primary primary report-dd m-t-10" style="width:100px" value="export">Export</button> 
            </li>

        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="notification"></div>
 <form name="search" id="searchform">
       <div class="row m-b-20">
            <div class="col-sm-6">
                <div class="form-group input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                    <div class="fg-line">
                        <input class="search form-control input-sm" name="searchtext" id="searchtext" value="<?php echo $_REQUEST['searchtext']; ?>" placeholder="Search for Keyword"/></div>
                </div>
            </div>
           
            <div class="col-sm-3">
                <div class="fg-line">
                    <input  type="text" class="form-control input-sm" id="ticketnumber" name="ticket" value="<?php echo $_REQUEST['ticket']; ?>" placeholder="Jump to Ticket Number" autocomplete="false" />
                </div>
            </div>
            <div class="col-sm-2 pull-left">  
                <div class="select">
                    <select class="form-control input-sm" name="allstore" id="allstores" onchange="this.form.submit()">
                        <option value="0">All Store</option>
                        <?php foreach ($master_id as $key => $val) { ?>
                            <option value="<?php echo $key; ?>" <?php if($_REQUEST['allstore'] == $key) {?> selected <?php } ?>><?php echo $val; ?></option>
                        <?php } ?>  
                    </select>
                </div>
            </div>
           
       </div>
        <div class="row m-b-20">
            
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="reporter" id="reporter" onchange="this.form.submit()">
                        <option value="All" <?php if($_REQUEST['reporter'] == 'All') {?> selected <?php } ?>>All Reporters</option>
                        <?php foreach($reporters as $reporter) { ?>
                            <option value="<?php echo $reporter->id; ?>" <?php if($_REQUEST['reporter'] === $reporter->id) {?> selected <?php } ?>><?php echo $reporter->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                 
            </div>
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="status" id="status" onchange="this.form.submit()">
                        <option value="Open" <?php if($_REQUEST['status'] == 'Open') {?> selected <?php } ?>>All Open</option>
                        <option value="Re-opened" <?php if($_REQUEST['status'] == 'Re-opened') {?> selected <?php } ?>>Re-opened</option>
                        <option value="Closed" <?php if($_REQUEST['status'] == 'Closed') {?> selected <?php } ?>>Closed</option>
                        <option value="All" <?php if($_REQUEST['status'] == 'All') {?> selected <?php } ?>>All</option>
                    </select>
                </div>                
            </div>             
            <div class="col-sm-2"> 
                <div class="select">
                    <select class="form-control input-sm" name="priority" id="priority" onchange="this.form.submit()">
                        <option value="All" <?php if($_REQUEST['priority'] == 'All') {?> selected <?php } ?>>All Priorities</option>
                        <option value="critical" <?php if($_REQUEST['priority'] == 'critical') {?> selected <?php } ?>>Critical</option>
                        <option value="high" <?php if($_REQUEST['priority'] == 'high') {?> selected <?php } ?>>High</option>
                        <option value="medium" <?php if($_REQUEST['priority'] == 'medium') {?> selected <?php } ?>>Medium</option>
                        <option value="low" <?php if($_REQUEST['priority'] == 'low') {?> selected <?php } ?>>Low</option>
                    </select>
                </div>
            </div> 
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="app" id="app" onchange="this.form.submit()" >
                    <?php foreach($muvi_apps as $muvi_app) { ?>
                        <option value="<?php echo $muvi_app->app_id; ?>" <?php if($_REQUEST['app'] === $muvi_app->app_id) {?> selected <?php } ?>><?php echo $muvi_app->app_name; ?></option>
                    <?php } ?>
                    </select>
                </div>                
            </div>  
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="type" id="type" onchange="this.form.submit()" >
                        <option value="All" <?php if($_REQUEST['type'] == 'All') {?> selected <?php } ?>>All Types</option>
                        <option value="Issue" <?php if($_REQUEST['type'] == 'Issue') {?> selected <?php } ?>>Bug</option>
                        <option value="NewFeature" <?php if($_REQUEST['type'] == 'NewFeature') {?> selected <?php } ?>>New Feature</option>
                        <option value="Support" <?php if($_REQUEST['type'] == 'Support') {?> selected <?php } ?>>How To</option>
                        <option value="SetupMigration" <?php if($_REQUEST['type'] == 'SetupMigration') {?> selected <?php } ?>>Setup/Migration</option>
                    </select>
                </div>                
            </div> 
              <div class="col-sm-2">          
                  <a href="<?php echo Yii::app()->baseUrl; ?>/partner/ticketList" class="btn btn-primary">Reset Filter</a>
            </div> 
                
           </div>
        </form>

        <div id="loaderDiv" class="text-center m-b-20 loaderDiv"  style="display:none;">
            <div class="preloader pls-blue text-center " >
                <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                </svg>
            </div> 
        </div>
        <div class="row m-b-40">


            <input type="hidden" id="page_size" value="<?php echo $page_size ?>" />

            <table id="ticketlist" class="table" >
                <thead>
                    <tr>
                        <th>Ticket#</th>                        
                        <th style="width: 250px;">Title</th>
                        <th>Store Name</th>
                        <th id='priority'  class="sortcolumn">
                            Priority
                        </th>
                         <th >Status</th>
                        <th  id='eta'  class="sortcolumn center <?php if ($_GET['sortBy'] == 'eta_desc' || $_GET['sortBy'] == 'eta_asc') { ?> sortactive<?php } ?>">
                            App
                        </th>
                        <th  id='last_updated_date' onclick="sortby('last_updated_date', 'desc');" class="sortcolumn <?php if ($_GET['sortBy'] == 'last_updated_date_desc' || $_GET['sortBy'] == 'last_updated_date_asc') { ?> sortactive<?php } ?>">
                            <?php
                            if ($sort_by == 'last_updated_date_desc') {
                                $sort = "fa fa-sort-desc";
                            } elseif ($sort_by == 'last_updated_date_asc') {
                                $sort = "fa fa-sort-asc";
                            } else {
                                $sort = "fa fa-sort-desc";
                            }
                            ?>
                            <i id="last_updated_date_i" class="<?php echo $sort ?>"></i> 
                            Last Update
                        </th> 

                    </tr>	
                </thead>
                <tbody id="autosearchdata"></tbody>
                <tbody id="pageloadmore">
<?php
foreach ($tickets as $key => $item) {
    ?>
    <tr> 
        <td class="center"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/partner/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php echo $item['id']; ?></a></td>
 
        <td style="word-break: inherit;"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/partner/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php
                if ($item['title'] != '') {
                    print strlen($item['title']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['title']))) ;
                } else {
                    print strlen($item['description']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['description']))) ;
                }
                ?>
                <p id="sort_grey"> <?php
                    $note = $this->ticketNotes($item['id'], 'LIMIT 1');

                    $pos = strpos($note[0]['note'], 'quoted-printable');
                    if ($pos) {
                        $notes_explode = explode('quoted-printable', $note[0]['note']);

                        $final_note = trim($notes_explode[1]);
                    } else {

                        $pos1 = strpos($note[0]['note'], 'UTF-8');
                        if ($pos1) {
                            $notes_explode1 = explode('UTF-8', $note[0]['note']);
                            $final_note = trim($notes_explode1[1]);
                        } else {
                            $final_note = trim($note[0]['note']);
                        }
                    }

                    print !empty($note) ? strlen($final_note) > 60 ? substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) . "..." : substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) : '';
                    ?></p>
            </a></td>
            <td>
            <?php
            if ($item['ticket_master_id'] != 0) {
                $master = new TicketMaster();
                $master = $master->findByPk($item['ticket_master_id']);
                echo $master->studio_name;
            } else {
                echo "All Stores";
            }
            ?> 
            </td>
        <td class='td_priority'><?php echo $item['priority']; ?></td>
        <td class="center"><?php echo $item['status']; ?></td>        
        <td><?php echo Yii::app()->common->getMuviApps($item['app']); ?></td>
        <td class = 'td_last_updated_date '><?php echo date('M d, h:ia', strtotime($item['last_updated_date'])); ?></td>
    </tr>  
    <?php
}
?>
                </tbody>
            </table>

        </div>
         <?php if(count($tickets) >= 20 ) { ?>
        <input type="button" class="pull-right" id="loadmore"  value="Load More" />
         <?php } ?>
    </div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js" type="text/javascript"></script>
<script type="text/javascript" src="/themes/admin/js/bootstrap-typeahead.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootbox.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery.bootpag.min.js"></script>

<script type="text/javascript">
    $('.report-dd').click(function(){
            window.location = '<?php echo Yii::app()->baseUrl."/partner/ExportTickets".$url_export?>';       
    });

    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var pricingall = '';
    var packagesall = '';
    var planall = '';
    var custom_codes = '';
    var num = 2;

    function getAutoSearchTicket()
    {
        var allstore = $("#allstores :selected").val();
        var reporter = $("#reporter :selected").val();
        var status = $("#status :selected").val();
        var priority = $("#priority :selected").val(); 
        var app = $("#app :selected").val();
        var type = $("#type :selected").val();
         $('#loaderDiv').show();
         $.post('/partner/AutoSearchticket', {'allstore':allstore,'reporter':reporter, 'status': status, 'priority':priority, 'app':app, 'type':type }, function (res) {
            $('#autosearchdata').html(res);
            $('#loaderDiv').hide();
         });
    }
    $(document).ready(function () {
        setInterval(function(){ 
           getAutoSearchTicket();
        }, 60000);
        
     $("#ticketnumber").keyup(function(event){
        if(event.keyCode == 13){
            $("#searchform").submit();
        }
     });
     $("#searchtext").keyup(function(event){
        if(event.keyCode == 13){
            $("#searchform").submit();
        }
     });
     
    $('#loadmore').click(function () {
        $('#loaderDiv').hide();
        $('#loadmore').val("Loading...");
        var searchtext = $.trim($("#searchtext").val());
        var allstore = $("#allstores :selected").val();
        var reporter = $("#reporter :selected").val();
        var status = $("#status :selected").val();
        var priority = $("#priority :selected").val(); 
        var app = $("#app :selected").val();
        var type = $("#type :selected").val();
        var sortby = '<?php echo $_REQUEST['sortby'] == 'asc'?'asc':'desc' ?>';
        $.post('/partner/searchTicket', {'searchtext': searchtext, 'allstore':allstore, 'reporter':reporter, 'status': status, 'priority':priority, 'app':app, 'type':type, 'sortby': sortby, 'page': num,}, function (res) {           
             $('#pageloadmore').append(res);
            $('#loadmore').val("Load More");
            num++;
        });

        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
    });

</script>
