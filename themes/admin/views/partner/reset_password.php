<style>
    .form-group {
        margin: 10px;
    }
</style>
<div class="container">
    <div class="container contact_us">
        <div class="span12" style="margin:5% auto auto; <?php if ($user == 1) { ?>width: 500px;<?php }?>">
            <?php 
            if ($user == 1) { ?>
                <h2 class="btm-bdr">Activate Your account</h2>
                <div class="row">
                    <form method="post" id="reset_pass" novalidate="novalidate">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="Enter password">Enter password</label>
                            <div class="col-sm-9">
                                <input type="password" value="" placeholder="" name="new_password" id="new_password" class="form-control">
                            </div>
                        </div>
                        <div style="clear: both; height:20px;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="Enter password">Confirm password</label>
                            <div class="col-sm-9">
                                <input type="password" value="" placeholder="" name="confirm_password" class="form-control">
                            </div>
                        </div>                    
                        <div style="clear: both;height:20px;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                                <input type="checkbox" value="" name="chkreq">
                                I agree to Muvi’s <a target="_blank" href="https://muvi.com/partneragreement/reseller">Reseller Agreement</a>
                                <label id="chkreq-error" class="error" for="chkreq"></label>
                            </div>                            
                        </div>                    
                        <div class="clear" style="height:20px;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                                <input name="submit" type="submit" value="Save Password" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>
                </div>
            <?php } else { ?>
                <h3 style="line-height: 30px;text-align: center;">Invalid Security Token . please try again.<br><br> If you are facing any problem to activate your account . please contact us at <a href="javascript:void(0);">studio@muvi.com</a></h3>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        jQuery.validator.addMethod("alphaNumeric", function (value, element) {
            return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value);
        }, "password must contain atleast one number and one character");
        $("#reset_pass").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 8,
                    alphaNumeric: true
                },
                confirm_password: {
                    minlength: 8,
                    equalTo: "#new_password"
                },
                chkreq:{
                    required: true
                }
            },
            messages: {
                new_password: {
                    required: "Please enter your password",
                    minlength: "Password must contain atleast 8 characters",
                    alphaNumeric: "password must contain atleast one number and one character"
                },
                confirm_password: {
                    minlength: "Password must contain atleast 8 characters",
                    equalTo: "Please enter the same value again"
                }
            },
            submitHandler: function (form) {

                //console.log($('#reset_pass').serialize());
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/partner/set_password",
                    data: $('#reset_pass').serialize(),
                    type: 'POST',
                    beforeSend: function () {
                        //$('#reset-loading').show();
                    },
                    success: function (data) {
                        if (data == "success") {                            
                            var meg = '<div class="alert alert-success alert-dismissable flash-msg" style="margin-top:-2% !important;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your account has been activated. Please login using your credentials.</div>';
                            $('.contact_us').before(meg);
                            setTimeout(function () {
                                window.location = "<?php echo Yii::app()->getBaseUrl(true) ?>";
                                return false;
                            }, 2000);
                        } else {
                            var meg = '<div class="alert alert-error alert-dismissable flash-msg" style="margin-top:-2% !important;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>An error occured . please try again.</div>';
                            $('.contact_us').before(meg);return false;
                            setTimeout(function () {
                                location.reload();
                                return false;
                            }, 2000);                            
                        }
                    }
                });
            }
        });
    });

</script>