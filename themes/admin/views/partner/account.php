<style>
    #cancel_subscription label.control-label{font-weight: normal;width: auto}
    .error{font-weight: normal;}
</style>
<div class="">
    <?php if ($reseller_type == 1) { ?>
        <div class="col-lg-8 m-b-20">
            <h2>Billing</h2>
            <h3 class="text-capitalize f-300 m-b-20">Muvi Premier Reseller : $3500 per month</h3>
            <div class="box box-primary" style="min-height: 150px;">

                <?php
                if (isset($cards) && !empty($cards)) {
                    $total_cards = count($cards);
                    ?>
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-info icon left-icon "></em>
                        </div>
                        <h4>Credit Cards</h4>
                    </div>
                    <div class="clearfix" style="height: 30px;"></div>
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Card Number</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            <?php foreach ($cards as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $value->card_last_fourdigit; ?></td>
                                    <td><?php echo $value->card_type; ?></td>
                                    <td>                                        
                                        <?php if ($value->is_cancelled != 0) { ?>
                                        <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="delete" onclick="showConfirmPopup(this);">
                                            Delete
                                        </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="makeprimary" onclick="showConfirmPopup(this);">
                                            Make Primary
                                        </a>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                <?php } ?>
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-info icon left-icon "></em>
                    </div>
                    <h4>Add Credit Card</h4>
                </div>
                <div class="clearfix" style="height: 30px;"></div>

                <?php
                $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
                ?>
                <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
                    <div id="card-info-error" class="error red" style="display: none;margin: 0 0 15px 150px;"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name on Card</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                            </div>
                        </div>                     
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Card Number</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" />
                            </div>
                        </div>                     
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Expiry Date:</label>                    
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="exp_month" id="exp_month" class="form-control input-sm">
                                        <option value="">Expiry Month</option>	
            <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                        <option value="">Expiry Year</option>
            <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Security Code:</label>                    
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="password" id="" name="" style="display: none;" />
                                <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
                            </div>
                        </div>
                    </div>        
                    <div class="form-group">
                        <label class="control-label col-sm-3">Billing Address:</label>                    
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">&nbsp;</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <button type="submit" class="btn btn-primary" id="nextbtn">Save Card</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="border: none;">
                            <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                            <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" style="color: #42B970">Your card has been saved successfully.</h4>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="paymentModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" name="paymodal" id="paymodal" method="post">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span id="bodymodal"></span>
                                        <input type="hidden" id="id_payment" name="id_payment" value="" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="javascript:void(0);" id="paymentbtn" class="btn btn-default">Yes</a>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="col-sm-6">
        <div class="Block-Header">
            <div class="icon-OuterArea--rectangular">
                <em class="icon-info icon left-icon "></em>
            </div>
            <h4>Update Password</h4>
        </div>
        <div style="height: 30px;" class="clearfix"></div>
        <form class="form-horizontal" method="post" role="form" id="security" name="security" action="<?php echo Yii::app()->baseUrl; ?>/partner/savepassword">
            <div class="loading" id="security-loading"></div>  
            <div class="form-group">
                <label class="control-label col-sm-4">Old Password:</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="password" id="password" name="password" class="form-control input-sm" autocomplete="false" placeholder="Your current password" />
                    </div>
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-sm-4">New Password</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="password" id="new_password" name="new_password" class="form-control input-sm" autocomplete="false" placeholder="New password of your choice" />
                    </div>
                </div>                     
            </div>               
            <div class="form-group">
                <label class="control-label col-sm-4">Confirm Password</label>                    
                <div class="col-sm-8">
                    <div class="fg-line">
                        <input type="password" id="conf_password" name="conf_password" class="form-control" autocomplete="false" placeholder="Re-enter new password" />
                    </div>
                </div>                     
            </div>            
            <div class="form-group">
                <label class="control-label col-sm-4">&nbsp;</label>
                <div class="col-sm-8">
                    <div class="fg-line">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    
        $("#security").validate({
            rules: {
                password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    minlength: 6,
                },
                conf_password: {
                    equalTo: '#new_password',
                },
            },
            messages: {
                password: {
                    required: 'Please enter your current password',
                },
                new_password: {
                    required: 'Please enter new password',
                },
                conf_password: {
                    equalTo: 'Please enter the same password again',
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
    });
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }

    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                switch (element.attr("name")) {
                    case 'exp_month':
                        error.insertAfter(element.parent().parent());
                        break;
                    case 'exp_year':
                        error.insertAfter(element.parent().parent());
                        break;
                    default:
                        error.insertAfter(element.parent());
                }
            }
        });
        var x = validate.form();
        if (x) {
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled', 'disabled');
            $("#loadingPopup").modal('show');
            var url = "<?php echo Yii::app()->baseUrl; ?>/payment/SaveResellerCard";
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();

            $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip}, function (data) {
                $("#loadingPopup").modal('hide');
                if (parseInt(data.isSuccess) === 1) {
                    $("#successPopup").modal('show');
                    setTimeout(function () {
                        location.reload();
                        return false;
                    }, 5000);
                } else {
                    $('#nextbtn').html('Save Card');
                    $('#nextbtn').removeAttr('disabled');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
                }
            }, 'json');
        }
    }


    function showConfirmPopup(obj) {
        $("#paymentModal").modal('show');
        var type = $(obj).attr('data-type');

        $("#headermodal").text(type.charAt(0).toUpperCase() + type.slice(1) + " Card?");
        $("#bodymodal").text("Are you sure you want to " + type + "  Card?");

        $("#paymentbtn").attr('data-managepayment_id', $(obj).attr('data-managepayment_id'));

        var onclick = type + 'Card(this)';

        $("#paymentbtn").attr('data-type', type);
        $("#paymentbtn").attr('onclick', onclick).bind('click');
    }
    function makeprimaryCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        var action = "<?php echo Yii::app()->baseUrl; ?>/partner/" + type + "Card";
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }

    function deleteCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        var action = "<?php echo Yii::app()->baseUrl; ?>/partner/" + type + "ResellerCard";
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }
</script>