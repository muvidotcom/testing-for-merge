<div class="row m-t-20 m-b-40">
    <div class="col-xs-12">
        <div class="Block-Header">
            <div class="icon-OuterArea--rectangular">
                <em class="icon-settings left-icon "></em>
            </div>
            <h3 class="text-capitalize f-300">FILE SIZE </h3>
        </div>
        <hr>
        &nbsp;
        Total Size: <strong><?= @$totalSize;?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bucket Name: <strong><?= @$bucketName;?></strong>  
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-horizontal">	
					<table class="table table-bordered table-responsive" >
						<thead>
							<tr>
								<th class="col-sm-1">S/L#</th>
								<th>Content Name</th>
								<th>Types</th>
								<th>Episode Name</th>
								<th>Video Path</th>
								<th>Size(Bytes)</th>
								<th>Size(MB)</th>
							</tr>
						</thead>
						<?php if ($data) {$bsize =0 ;
							?>
							<tbody>
									<?php foreach ($data AS $key => $details) { $bsize +=$details ['bytesize'];  ?>		
									<tr class="" >
										<td><?= ($key + 1); ?> </td>
										<td><?= $details ['name']; ?> </td>
										<td><?php if($details ['content_types_id']==1){echo "SPLF";}elseif ($details ['content_types_id']==2) {echo "SPSF";}else{echo "Episodes";}; ?> </td>
										<td><?= @$details ['episode_title']; ?> </td>
										<td><?= @$details ['video_path']; ?> </td>
										<td><?= @$details ['bytesize']; ?> </td>
										<td><?= @$details ['size']; ?> </td>
									</tr>
							<?php } ?>
									<tr>
										<td colspan=7 class="pull-right"> <strong>Total Size: <?= @$totalSize;?></strong>  
									</tr>
							</tbody>
						<?php } else { ?>
							<tr><td colspan="3">No content type found!</td></tr>
					<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
