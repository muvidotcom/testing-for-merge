<div class="modal is-Large-Modal fade" id="myPGLargeModalLabel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload</h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" onclick="hide_file()">
                            <a href="#pg_Upload-Video" aria-controls="pg_Upload-Video" role="tab" data-toggle="tab">Upload Poster</a>
                        </li>
                        <li role="presentation" onclick="hide_gallery()"> 
                            <a href="#pg_Choose-From-Library" aria-controls="pg_Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="pg_Upload-Video">
                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                    <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="pg_click_browse('pg_celeb_pic')">
                                    <input id="pg_celeb_pic" name="pg_Filedata[]" type="file" onchange="pg_fileSelectHandler()" style="display:none;" />
                                    <p class="help-block"><?php echo "Upload a transparent image of size <span id='upload-sizes'>" . $cropDimesion['width'] . "x" . $cropDimesion['height'] . '</span>'; ?></p>
                                </div>
                                <input type="hidden" id="pg_x1" name="pg_fileimage[x1]" />
                                <input type="hidden" id="pg_y1" name="pg_fileimage[y1]" />
                                <input type="hidden" id="pg_x2" name="pg_fileimage[x2]" />
                                <input type="hidden" id="pg_y2" name="pg_fileimage[y2]" />
                                <input type="hidden" id="pg_w" name="pg_fileimage[w]"/>
                                <input type="hidden" id="pg_h" name="pg_fileimage[h]"/>
                                <div class="col-xs-12">
                                    <div class="Preview-Block">
                                        <div class="thumbnail m-b-0">
                                            <div id="pg_celeb_preview" class=" col-md-12 margin-topdiv">
                                                <img id="pg_preview" />
                                                <?php if(!empty($checkImageKey) && $checkImageKey->config_value==1){?>
                                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                                    <div for="upload" class="col-sm-12">
                                                        Unique Key:
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="fg-line" style="width:30%;">
                                                            <input placeholder="A unique key for this image ex. tomcurise_london" id="unique_key" name="image_key[]" class="form-control checkInput" onkeyup="checkimagekey(this)" type="text" autocomplete="off" pattern="^[a-zA-Z0-9_]*$" />
                                                        </div>
                                                            <span id="image_key_error" class="error red" style="display: block;"></span>
                                                    </div>
                                                </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <?php if ($celeb['poster']['poster_file_name']) { ?>
                                            <div class="thumbnail m-b-0" id="pg_editceleb_preview">
                                                <div class="relative m-b-10">
                                                    <?php
                                                    $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                                    ?>
                                                    <img src="<?php echo $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'] ?>" />
                                                </div>
                                            </div>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pg_Choose-From-Library">
                            <input type="hidden" name="pg_image_file_name" id="pg_image_file_name" />
                            <input type="hidden" name="pg_original_image" id="pg_original_image" />
                            <input type="hidden" id="pg_x13" name="pg_jcrop_allimage[x13]" />
                            <input type="hidden" id="pg_y13" name="pg_jcrop_allimage[y13]" />
                            <input type="hidden" id="pg_x23" name="pg_jcrop_allimage[x23]" />
                            <input type="hidden" id="pg_y23" name="pg_jcrop_allimage[y23]" />
                            <input type="hidden" id="pg_w3" name="pg_jcrop_allimage[w3]" />
                            <input type="hidden" id="pg_h3" name="pg_jcrop_allimage[h3]" />
                            <div class="row  Gallery-Row">
                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40">
                                    <ul class="list-inline text-left">
                                        <?php
                                        foreach ($all_images as $key => $val) {
                                            if ($val['image_name'] == '') {
                                                $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                                            } else {
                                                $img_path = $base_cloud_url . $val['s3_thumb_name'];
                                                $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                                            }
                                            ?> 
                                            <li>
                                                <div class="Preview-Block">
                                                    <div class="thumbnail m-b-0">
                                                        <div class="relative m-b-10">
                                                            <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                                                            <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                                                            <img class="img" src="<?php echo $img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >

                                                            <div class="caption overlay overlay-white">
                                                                <div class="overlay-Text">
                                                                    <div>
                                                                        <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="pg_toggle_preview(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                                                            <span class="btn btn-primary icon-with-fixed-width">
                                                                                <em class="icon-check"></em>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
<?php } ?>    
                                    </ul>
                                </div>
                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue  ">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="pg_gallery_preview">
                                            <img id="pg_glry_preview" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="pg_seepreview(this);">Next</button>
            </div>
        </div>
    </div>
</div>
<script>
var key_xhr;
function checkimagekey(obj, str){
        if(!str){
            var str='';
        }
        var unique_key = $(obj).val();
        $("#image_key_error"+str).css("display","none");
        if(unique_key.trim()==''){
            $("#image_key_error"+str).css("display","block");
            $("#image_key_error"+str).html("<span>*</span> Please enter image key");
        } else if(!unique_key.match(/^[a-zA-Z0-9\_]+$/g)){
            $("#image_key_error"+str).css("display","block");
            $("#image_key_error"+str).html("<span>*</span> Image key only allowed letter and underscore(_)"); 
        } else if(unique_key.length > 150){
            $("#image_key_error"+str).css("display","block");
            $("#image_key_error"+str).html("<span>*</span> Image key length should be 150 or less");
        } else {
            if (key_xhr) {
                key_xhr.abort();
            }
            key_xhr = $.ajax({
                method: 'post',
                url: HTTP_ROOT + '/management/checkimagekey',
                data: {id:null, image_key:unique_key},
                dataType : 'json',
                success: function(response){
                    if(response.success==0){
                        $("#image_key_error"+str).css("display","block");
                        $("#image_key_error"+str).html("<span>*</span> Image key already exist");
                    }
                }
            });
        }
    }
</script>