<header class="header bg-black" style="min-height: 70px;">
	<a class="logo" href="<?php echo $this->createUrl('admin/home');?>">
		<img src="<?php echo Yii::app()->baseUrl.'/images/muvi_logo.png';?>" />
	</a>
	<nav role="navigation" class="navbar navbar-static-top">
		<?php if(Yii::app()->controller->action->id!='login'){?>
		<div class="navbar-right" style="margin-right: 3%;float: right;">
			<ul class="nav navbar-nav">
				<li class="login-link">
					<a  href="<?php echo $this->createUrl('admin/login');?>" class="">
						<i class="glyphicon glyphicon-log-in"></i>
						<span>&nbsp;Login</span>
					</a>
				</li>
			</ul>
		</div>
		<?php }?>
	</nav>
</header>

