<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>    
        <link href="<?php echo Yii::app()->baseUrl; ?>/css/admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/adminlte/css/AdminLTE.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/adminlte/css/skins/_all-skins.css?v=<?php echo RELEASE ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/autocomplete.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css?v=<?php echo RELEASE ?>" />
        <style type="text/css">
            .flash-msg{
                position: absolute !important;
                margin-left: 25% !important;
                margin-top: 2% !important;
                z-index: 999999;
            }
            .content { overflow:auto; padding-bottom:100px; /* this needs to be bigger than footer height*/}
            .colheight{min-height: 295px;}
            .sidebar-menu .lasttop{top:-53px;}
        </style>
        <script type="text/javascript">
            var HTTP_ROOT = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
      
        

        <!-- Bootstrap -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-typeahead.js"></script>        
        
        <script type="text/javascript">
            $(document).ready(function () {
                $.validator.addMethod("mail", function (value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function (value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");
            });
        </script>      
          
    </head>
    <?php
    $studio = Studios::model()->findByPk(Yii::app()->user->studio_id);
    $config = new StudioConfig();           
    ?>
    <body>
    <section class="content">
<?php echo $content; ?>
   </section>
    </body>
</html>
