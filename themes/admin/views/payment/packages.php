<?php
    $studio = $this->studio;
    if (intval($studio->reseller_id)) {//Reseller ?>
        <div class="row m-t-40 m-b-40 Purchase-Subscription">
            <div class="col-sm-12">
                <div class="Block-Body">
                    <p>
                        <span class="icon-check green"></span>&nbsp;&nbsp;Link to reseller <b><?php echo ucwords($reseller_account_name);?></b>
                    </p>
                </div>
            </div>
        </div>
    <?php } else { ?>
<?php
$applications = json_encode($packages['appilication']);
if($package_code != '')
    $sel_id = $package_code;
else
    $packages['package'][0]['code'];
$app_price = Yii::app()->general->getAppPrice($sel_id);
?>

<div class="row m-b-40 Purchase-Subscription">
    <div class="col-sm-12">
        <form class="form-horizontal" method="post" autocomplete="off" role="form" id="plan_subscription" name="plan_subscription" action="javascript:void(0);" onsubmit="return validatePlanForm();">
            <h4>Make Changes to your Muvi Subscription</h4>
            <div class="row">
                <div class="col-sm-12">
                     <div class="form-group">
                         <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Select Package </label>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                         <input type="hidden" value="0" id="is_upgrade">

                                            <div class="select">
                                                <select id="packages" name="packages" class="form-control input-sm" onchange="getAppilication(this);"> 
                                                    <?php 
                                                    $charging_now = 0;
                                                    $yearly_discount = 0;
                                                    $is_bandwidth = 0;
                                                    if (isset($packages['package']) && !empty($packages['package'])) {
                                                        $i = 0;
                                                        foreach ($packages['package'] as $key => $value) {
                                                            $i++;
                                                            $base_price = $charging_now = $value['base_price'];
                                                            $yearly_discount = $value['yearly_discount'];
                                                            $is_bandwidth = $value['is_bandwidth'];
                                                            $package_code = $value['code'];
                                                            if($i == 1)
                                                                $current_package_price = $base_price;
                                                            
                                                            if($selpackage == $value['id']) {                                                                
                                                                $current_package_price = $charging_now;   
                                                                if (isset($plan) && !empty($plan)) {
                                                                    if($plan->is_old && $value['is_old_base_price']){
                                                                        $current_package_price = $plan->base_price;
                                                                        $base_price = $plan->base_price;
                                                            }
                                                                }
                                                            }
                                                            ?>
                                                    <option value="<?php echo $value['id'];?>" data-code="<?php echo $value['code'];?>"  data-price="<?php echo $base_price;?>" data-yearly_discount="<?php echo $value['yearly_discount'];?>" data-is_bandwidth="<?php echo $is_bandwidth;?>" <?php if($selpackage == $value['id']) { ?>selected="selected"<?php } ?>><?php echo $value['name'];?></option> 
                                                   <?php }
                                                    } ?>
                                                </select>
                                            <input type="hidden" value="<?php echo $current_package_price; ?>" id="current_base_price" />
                                            <input type="hidden" value="<?php echo $selpackage;?>" id="current_package_id" />
   
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    <div class="form-groupapplications[]-error" id="appilication_div">
                            <?php if (isset($packages['appilication'][$selpackage]) && !empty($packages['appilication'][$selpackage])) { ?>
                                <label class=" m-b-10 f-400 m-b-20 h4">Select Applications</label>
                                <div class="">
                                    
                                <?php $count_app = 0; foreach ($packages['appilication'][$selpackage] as $key => $value) {?>       
                                        <label class="checkbox checkbox-inline m-r-20">
                                           <input type="checkbox" value="<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>" name="applications[]" data-code="<?php echo $value['code'];?>" onclick="selectApplication();" <?php if (in_array($value['id'], $selapplications)) { $count_app++;?>checked="checked" class="price_chkbox"<?php } else { ?>data-price="<?php echo $value['price'];?>" class="not_sel price_chkbox"<?php } ?> />
                                            <i class="input-helper"></i> <?php echo $value['name']; ?>
                                        </label>

                                    <?php } ?>
                                </div>
                      
                                <div class="grey">
                                    First application is charged at $<?php echo $current_package_price;?> per month, others are $<?php echo $app_price;?> per month per app
                                </div>

                            <?php }else { ?>
                    <input type="hidden" value="0" data-code="" data-price="0" class="not_sel price_chkbox" />
                <?php } ?>
                        </div>
                    <div class="">
                        <div class="col-sm-12">
                            <label id="applications[]-error" class="error red" for="applications[]" style="display: none;"></label>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4"> Select Plan</label>
                            <div class="col-sm-12">
                                
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input data-id="monthly-plan" type="radio" class="all_plans" value="Month" id="monthly" name="plans" selected-plan="<?php echo $selplan;?>" <?php if ($selplan == 'Month') { ?>checked="checked" <?php } ?> onclick="showPlan(this);" /><i class="input-helper"></i> Monthly
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 grey">
                                        Pay month by month, cancel anytime
                                    </div>
                                </div>
                            
                               
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                               <input data-id="yearly-plan" type="radio" class="all_plans" value="Year" name="plans" id="yearly"  selected-plan="<?php echo $selplan;?>" <?php if ($selplan == 'Year') { ?>checked="checked" <?php } ?> onclick="showPlan(this);" /> <i class="input-helper"></i> Yearly
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 grey">
                                        <?php if (intval($yearly_discount)) { ?>Save <?php echo $yearly_discount; ?>% by paying for a year in advance.<?php } ?> No refund if canceled during the year
                                    </div>
                                </div>
                            </div>
                            </div>
                       <div class="form-group">
                            <label class="col-sm-12 m-b-10 f-400 m-b-20 h4">Projected Pricing</label>
                            <div class="col-sm-12" >

                                <div class="row">
                                    
                                    <div class="col-sm-12 monthly_payment" <?php if ($selplan == 'Month') { ?>style="display: block"<?php } else {?>style="display: none;"<?php } ?>>
                                        <div>
                                            New monthly payment:
                                            <label class="control-label">$<span class="new_charge"><?php echo $data['billing_amount'];?></span></label>
                                            per month <span class="bandwidth" <?php if(!intval($is_bandwidth)) { ?>style="display: none" <?php } ?>>+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                                        </div>
                                        <div>
                                            Charge to your card now: <label class="control-label">
                                                $<span class="charge_now">0</span> (Prorated for the month)
                                            </label>
                                        </div>
                                        <div>
                                            Next billing: <label class="control-label"><?php echo date('F d, Y', strtotime($data['billing_date'])); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 yearly_payment" <?php if ($selplan == 'Year') { ?>style="display: block"<?php } else {?>style="display: none;"<?php } ?>>
                                        <div>
                                            New yearly payment: 
                                            <label class="control-label">$<span class="new_charge"><?php echo $data['billing_amount'];?></span></label>
                                            per year <span class="bandwidth" <?php if(!intval($is_bandwidth)) { ?>style="display: none" <?php } ?>>+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                                        </div>
                                        <div>
                                            Charging to your card now: <label class="control-label">
                                                $<span class="charge_now">0</span> (Prorated for the year)
                                            </label>
                                        </div>
                                        <?php if ($selplan == 'Year') { ?>
                                        <div>Next billing: <label class="control-label"><?php echo date('F d, Y', strtotime($data['billing_date'])); ?></label></div>
                                        <?php } else { ?>
                                        <div>Next billing: <label class="control-label"><?php echo date('F d, Y', strtotime($data['billing_date_after_year'])); ?></label></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 m-t-10">
                                See <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">pricing</a> for more detail. Contact your Muvi representative if you are not sure about the package and add-ons you need.
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 m-t-30">
                                <button type="submit" class="btn btn-primary btn-sm contbtn">Continue to Authorize Payment</button>
                            </div>
                        </div>
                </div>
            </div>
     </form>
    </div>
</div>
<input type="hidden" name="price_per_app" id="price_per_app" value="<?php echo $app_price; ?>" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
var days = '<?php echo $data['days'];?>';
var total_days = '<?php echo $data['total_days'];?>';

function validatePlanForm() {
    var validate = $("#plan_subscription").validate({ 
        rules: {    
            "applications[]": {
                required: true
            }         
        },
        messages: {
            "applications[]": {
                required: 'Please choose the option.'
            }
        }
    });
    
    var isTrue = validate.form();
    
    if (isTrue) {
        //$("#packages").removeAttr("disabled");
        document.plan_subscription.action = '<?php echo Yii::app()->baseUrl; ?>/payment/updatePackages';
        document.plan_subscription.submit();return false;
    }
        $('form').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
        });

}

function getAppilication(obj) {
    var parent = $(obj).val();
    var current_bp = parseFloat($('#current_base_price').val());
    var base_price = parseFloat($('option:selected', obj).attr('data-price'));
    var current_package = '<?php echo $selpackage; ?>';
    //var selected_package = $('option:selected', '#packages').val();

    $("#monthly").prop('disabled',false);
    var check_selected =  $('#current_package_id').val();
    if(current_package === check_selected){
        $("#packages option[value='<?php echo $selpackage;?>']").prop('disabled',true);
    }
//    if(current_package != selected_package){
//        $('#yearly').removeAttr('checked');
//        $("#monthly").prop('disabled',false);
//        $("#monthly").prop('checked','checked');
//    }
    if(base_price <= current_bp){       // downgrading..
        $("#is_upgrade").val(0);
     swal({
            title: "Oops!",
            text: "Sorry! Please add a support ticket if you wish to downgrade your package or remove specific apps from your subscription.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Add Ticket",
            closeOnConfirm: true,
            html:true
        },function(isConfirm) {
            if (isConfirm) {
                var action ="<?php echo Yii::app()->baseUrl; ?>/ticket/addTicket";
                window.location.href = action;
            }else{
                location.reload(true);
                $(".contbtn").prop('disabled',true);
            }
        });           
    }else {     
            $("#is_upgrade").val(1);     //upgrading..
    }
    
    var code = $('option:selected', obj).attr('data-code');
    var is_bandwidth = $('option:selected', obj).attr('data-is_bandwidth');

    if (parseInt(is_bandwidth)) {
        $(".bandwidth").show();
    } else {
        $(".bandwidth").hide();
    }

    var applications = jQuery.parseJSON('<?php echo $applications;?>');
    var str = '';
    var check = '<?php echo $count_app;?>';

    if (applications[parent]) {
        str = str +'<div class="pull-left">';
        str = str +'<h3>Select Applications</h3>';
        str = str +'</div>';
        str = str +'<div class="clearfix"></div>';

        for (var i in applications[parent]) {
            str = str +'<div class="pull-left" style="width: 20%;">';
            str = str +'<label>';
            str = str +'<div class="pull-left" style="margin-right: 5px;margin-top: -2px;">';
            var checked = '';
            if(applications[parent][i].code === "monthly_charge") {
                checked = 'checked="checked"';
            }
            
            str = str +'<input type="checkbox" name="applications[]" value="'+applications[parent][i].id+'" class="price_chkbox" data-id="'+applications[parent][i].id+'" data-code="'+applications[parent][i].code+'" data-price="'+applications[parent][i].price+'" '+checked+' onclick="selectApplication();" />';
            str = str +'</div>';
            str = str +applications[parent][i].name;
            str = str +'</label>';
            str = str +'</div>';
        }

        var APP_PRICE = 0;
        $.ajax({
          url: "<?php echo Yii::app()->baseUrl; ?>/site/packageappprice",
          method: "POST",
          data: { 'code': code },
          dataType: "json",
          async: false
        }).done(function( res ) {    
            APP_PRICE = res.app_price;
            $('#price_per_app').val(APP_PRICE);
        });
            
        str = str +'<div class="clearfix"></div><div class="pull-left" style="color: #929292;">';
        str = str +'First application is charged at $'+base_price+' per month, others are $'+APP_PRICE+' per month per app';
        str = str +'</div>';
    }

    $("#appilication_div").html(str);
    showPrice();
}

function selectApplication() {
    var check_current = $("input:checkbox:checked").length;     // check no. of apps selected in all
    var check = '<?php echo $count_app;?>';     // check no. of previously existing apps

    var is_upgrade= $('#is_upgrade').val();
    //$("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('checked', true);  
    //$("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('readonly', 'readonly');  
    if(is_upgrade == 0){
    if(parseInt(check) > parseInt(check_current)){
     swal({
            title: "Oops!",
            text: "Sorry! Please add a support ticket if you wish to downgrade your package or remove specific apps from your subscription.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Add Ticket",
            closeOnConfirm: true,
            html:true
          },function(isConfirm) {
            if (isConfirm) {
                var action ="<?php echo Yii::app()->baseUrl; ?>/ticket/addTicket";
                window.location.href = action;
            }else{
                location.reload(true);
                $(".contbtn").prop('disabled',true);
            }          
      });       
    }
    }
//    if (parseInt(check) === 0) {
//        $("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('checked', true);  
//    } 
    showPrice();
}

function showPlan(obj) {
    var plan = $(obj).val();
    var existing_plan = '<?php echo $selplan;?>'
    var selected_package = $('option:selected', '#packages').val();
    var current_package = '<?php echo $selpackage; ?>';

    var selected_plan = $('.all_plans:checked').attr('data-id');
    if(existing_plan == 'Year' && selected_plan == 'monthly-plan'){
        swal({
              title: "Oops!",
              text: "Sorry! Please add a support ticket if you wish to downgrade your package or remove specific apps from your subscription.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
              confirmButtonText: "Add Ticket",
              closeOnConfirm: true,
              html:true
          },function(isConfirm) {
                if (isConfirm) {
                    var action ="<?php echo Yii::app()->baseUrl; ?>/ticket/addTicket";
                    window.location.href = action;
                }else{
                    location.reload(true);
                    $(".contbtn").prop('disabled',true);
                }          
          });         
    } else if(current_package == selected_package && existing_plan == 'Month' && selected_plan == 'yearly-plan'){
        $("#monthly").prop('disabled',true);
    } else if(current_package != selected_package && selected_plan == 'monthly-plan'){
        $("#monthly").prop('disabled',false);

    }
    
    
    if (plan === 'Year') {
        $(".monthly_payment").hide();
        $(".yearly_payment").show();
    } else {
        $(".yearly_payment").hide();
        $(".monthly_payment").show();
    }

    showPrice();
}

function showPrice() {
 /*   var base_price = parseFloat($('option:selected', '#packages').attr('data-price'));
    var code = parseFloat($('option:selected', '#packages').attr('data-code'));
    var plan = $('.all_plans:checked').val();
    var price_total = base_price;
    var sel_chk_cnt = 0;
    
    if ($('.price_chkbox').length) {
        var cnt = 0;

        $('.price_chkbox').each(function() {
            if($(this).is(":checked")){
                cnt++;
            }
        });
        var APP_PRICE = $('#price_per_app').val();
        sel_chk_cnt = cnt;
        if (cnt > 1) {
            price_total = price_total + ((cnt -1) * APP_PRICE);
        }
    }

   if (plan === 'Year') {
        var yearly_discount = parseInt($('option:selected', '#packages').attr('data-yearly_discount'));
        price_total = ((price_total * 12) - (((price_total * 12) * yearly_discount)/100));
    }
    
    var billing_amount = 0;
    var sel_plan = $('.all_plans:checked').attr('selected-plan');
    
    if ($('.not_sel').length) {
        var pro_price_total = 0;
        $('.not_sel').each(function() {
            if($(this).is(":checked")){
                var price = $(this).attr('data-price');
                pro_price_total = parseFloat(pro_price_total) + parseFloat(price);
            }
        });
        
        if (plan === 'Year') {
            if (sel_plan === 'Month') {
                billing_amount = price_total + ((parseInt(days) * pro_price_total) / parseInt(total_days));
            } else {
                var yearly_pro = (pro_price_total * 12);
                billing_amount = ((parseInt(days) * yearly_pro) / parseInt(total_days));
                billing_amount = (billing_amount - ((billing_amount * yearly_discount)/100))
            }
        } else {
            billing_amount = ((parseInt(days) * pro_price_total) / parseInt(total_days));
        }
    }
    
    billing_amount = billing_amount.toFixed(2);
    price_total = price_total.toFixed(2);
    
    if (sel_chk_cnt === 4 && plan === 'Year' && sel_plan=='Month') {
        billing_amount = price_total;
    }
    $(".charge_now").html(billing_amount);
    $(".new_charge").html(price_total);  
 */   
   
    //$('.contbtn').attr('disabled', 'disabled');
    var package = $('option:selected', '#packages').val();
    var plan = $('.all_plans:checked').val();
    var csrfToken = $('#csrfToken').val();
    var applications = '';
    if ($('.price_chkbox').length) {
        $('.price_chkbox').each(function() {
            if($(this).is(":checked")){
                applications = applications+','+$(this).attr('data-id');
            }
        });
        applications = applications.replace(',','');
    }
    
    var url = "<?php echo Yii::app()->baseUrl; ?>/payment/getPrice";
    $.post(url, {'package': package, 'plan': plan, 'applications': applications, 'csrfToken': csrfToken}, function (res) {
        $(".charge_now").html(res.billing_amount);
        $(".new_charge").html(res.price_total);
        //$('.contbtn').removeAttr("disabled");
    }, 'json');
}
</script>
<?php } ?>