<div class="row m-t-40">
    <div class="col-sm-12">
        <div class="Block">
            <?php 
            $studio = $this->studio;
            if (intval($studio->reseller_id)) {//Reseller ?>
            <div class="Block-Body">
                    <p>
                        <span class="icon-check green"></span>&nbsp;&nbsp;Link to reseller <b><?php echo ucwords($reseller_account_name);?></b>
                    </p>
                </div>
            <?php } else { ?>
                <div class="Block-Body">
                <?php
                if ($studio->is_subscribed == 1 && $studio->status == 1 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Customer ?>
                    <div>
                        Subscription expiry date: <b><?php echo gmdate("F d, Y", strtotime($studio->start_date . '-1 Days'));?></b>
                    </div>
                    <div>
                        Next Billing date: <b><?php echo gmdate("F d, Y", strtotime($studio->start_date));?></b>
                    </div>
                    <div style="clear: both;height: 10px;"></div>
                <?php } ?>
                <div id="Block-Body-id">
                
                </div>
            </div>
            <div class="paggi"></div>
            <input type="hidden" id="page_size" value="<?php echo $page_size?>" />
            <?php } ?>
        </div>
    </div>
</div>
<?php if (intval($studio->reseller_id) == 0) { ?>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<script type="text/javascript">
$(function() {
    paymentHistory();
});    
    
function payBill(obj) {
    var uniqid = $(obj).attr('data-uniqid');
    var url = '<?php echo Yii::app()->baseUrl; ?>/payment/payBill/bill/'+uniqid;
    window.location.href = url;
}

function printReceipt(obj) {
    var uniqid = $(obj).attr('data-uniqid');
    var url = '<?php echo Yii::app()->baseUrl; ?>/payment/printReceipt/bill/'+uniqid;
    window.open(url,'_blank');
}

function paymentHistory() {
    $.post('/payment/paymentHistoryData',function(res){
        $('#Block-Body-id').html(res);
        var regex = /\d+/g;
        var mydata = $(res).find('data-count');
        var count = mydata.prevObject[0].outerHTML.match(regex);
        
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
        if(maxVisible <= 5){
            maxVisible = total;
        }
        if(parseInt(count) < parseInt(page_size)){
            $('#page-selection').parent().hide();
        }else{
            if($('#page-selection').length){
                $('#page-selection').parent().show();
            }else{
                $('.paggi').html('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>');
                $('#page-selection').parent().show();
            }
        }
        $('#page-selection').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
            $.post('/payment/paymentHistoryData',{'page':num},function(res){
                $('#Block-Body-id').html(res);
            });
        });
    });
}
</script>
<?php } ?>