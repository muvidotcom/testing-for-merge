<input type="hidden" class="data-count" value="<?php echo $data['count'];?>" />
<table class="table table-hover billing-table">
    <thead>
            <tr>
                <th>Bill</th>
                <th data-hide="phone">Description</th>
                <th data-hide="phone">Date</th>
                <th data-hide="phone">Amount</th>
                <th data-hide="phone"></th>
            </tr>
    </thead>
    <tbody>
        <?php 
        if(isset($data) && !empty($data['data'])){
                foreach ($data['data'] as $row){
                    $total_amount = 0;
                    $details = '';
                    if(isset($row['billing_details']) && !empty($row['billing_details'])){
                        foreach($row['billing_details'] as $value){
                            $details .= $value['title'].': $'.$value['amount'].'<br/>';
                            $total_amount += $value['amount'];
                        }
                    }
            ?>
        <tr>
            <td>Receipt #<?php echo $row['id']?></td>
            <td>
                <?php
                    if(isset($row['billing_period_start']) && trim($row['billing_period_start']) && isset($row['billing_period_end']) && trim($row['billing_period_end'])){
                        
                ?>
                Payment for billing period <?php echo Date('M d, Y',strtotime($row['billing_period_start']));?> - <?php echo Date('M d, Y',strtotime($row['billing_period_end']));?><br/>
                <?php
                    }
                    echo $details;
                ?>

            </td>
            <td><?php echo Date('M d, Y',strtotime($row['billing_date']));?></td>
            <td>$<?php echo $row['billing_amount'];?></td>
            <td>
                <?php if (isset($row['is_paid']) && $row['is_paid'] == 0 && isset($row['transaction_type']) && intval($row['transaction_type']) ==5) { ?>
                <div>
                    <a href="javascript:void(0);" data-uniqid="<?php echo $row['uniqid'];?>" onclick="payBill(this);"><i class="fa fa-bitcoin left-icon"></i> Pay Bill</a>
                </div>
                <?php } else if (isset($row['is_paid']) && $row['is_paid'] == 2) { ?>
                <div>
                    <a href="javascript:void(0);" data-uniqid="<?php echo $row['uniqid'];?>" onclick="printReceipt(this);"><i class="fa fa-print"></i> Print Receipt</a>
                </div>
                <?php } ?>
            </td>
        </tr>
        <?php }}else{?>
        <tr>
            <td colspan="4">No Record found!!!</td>
        </tr>
        <?php }?>
    </tbody>
</table>