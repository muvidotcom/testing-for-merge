<style type="text/css">
    .error{font-weight: normal;}
</style>
<div class="row m-t-40 m-b-40">
     <?php 
        $studio = $this->studio;
        if (intval($studio->reseller_id)) {//Reseller ?>
    <div class="col-xs-12">
                <div class="Block">
                    <div class="Block-Body">
                        <p>
                            <span class="icon-check green"></span>&nbsp;&nbsp;Link to reseller <b><?php echo ucwords($reseller_account_name);?></b>
                        </p>
                    </div>
                </div>
            </div>
        <?php } else { ?>
        <div class="col-xs-12">
        <?php
            if (isset($cards) && !empty($cards)) {
                $total_cards = count($cards);
                ?>
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-info icon left-icon "></em>
                </div>
                <h4>Basic Information</h4>
            </div>
            <hr>
            <?php if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 1)) {//Customer 
                    if (isset($studio->payment_status) && ($studio->payment_status != 0)) {
                    ?>
                    <div class="red">
                        <?php $cancel_date = date('F d, Y', strtotime('+10 days', strtotime($studio->partial_failed_date)));?>
                        Your account is past due! Not paying the balance before <?php echo $cancel_date;?> will cancel your account. <a href="javascript:void(0)" id="pamentpayhere" onclick="payFromPrimaryCard();" style="font-weight: bold">Pay Now</a>.
                    </div>
                <?php }
                } ?>
            <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Card Number / Customer Id</th>
                            <th>Type</th>
                            <th class="width">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cards as $key => $value) { ?>
                            <tr>
                                <td><?php echo trim($value->card_last_fourdigit) ? $value->card_last_fourdigit : $value->customer_id; ?></td>
                                <td><?php echo trim($value->card_last_fourdigit) ? $value->card_type : 'PayPal'; ?></td>
                                <td>
                                    <?php
                                    if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 1)) {//Customer
                                        if ($total_cards == 1) {//Not needed delete and primary option
                                        } else {//More than one cards
                                            if ($value->is_cancelled == 0) {//If it is a primary, no need for delete and primary option
                                            } else {//Except primary cards 
                                                ?>
                                    <h5><a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="delete" onclick="showConfirmPopup(this);">
                                            <em class="icon-trash"></em>&nbsp;Delete
                                        </a></h5>

                                               
                                    <h5><a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                   <em class="icon-key"></em>&nbsp;Make Primary
                                        </a></h5>

                                                <?php
                                            }
                                        }
                                    } else if (isset($studio->status) && ($studio->status == 1) && ($studio->is_subscribed == 0)) {//Lead 
                                        ?>
                                    <h5> <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="delete" onclick="showConfirmPopup(this);">
                                            <em class="icon-trash"></em>&nbsp;Delete
                                        </a></h5>

                                        <?php if ($value->is_cancelled != 0) { ?>
                                    <h5>
                                             <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                     <em class="icon-key"></em>&nbsp;Make Primary
                                                </a>
                                    </h5>
                                        <?php } ?>
                                    <?php } else if (isset($studio->status) && ($studio->status == 4) && ($studio->is_subscribed == 0)) {//Cancelled  ?>
                                    <h5><a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="delete" onclick="showConfirmPopup(this);">
                                            <em class="icon-trash"></em>&nbsp;Delete
                                        </a></h5>
                                        <?php if ($value->is_cancelled != 0) { ?>
                                    <h5>
                                              <a href="javascript:void(0);" data-managepayment_id="<?php echo $value['id']; ?>"   data-type="makeprimary" onclick="showConfirmPopup(this);">
                                                     <em class="icon-key"></em>&nbsp;Make Primary
                                              </a></h5>
                                        <?php } ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
            <?php } ?>
        
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-credit-card icon left-icon "></em>
                </div>
                <h4>Add Credit Card</h4>
            </div>
            <hr>
             <?php
            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
            ?>
            <div class="col-md-8">
            <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
                <div id="card-info-error" class="error red"></div>
				<input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />
                <div class="form-group">
                    <label class="col-md-4 control-label">Name on Card:</label>                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Card Number:</label>                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number"  />
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Expiry Date:</label>                    
                    <div class="col-md-4">
                        <div class="fg-line">
                            <div class="select">
                            <select name="exp_month" id="exp_month" class="form-control input-sm" >
                                    <option value="">Expiry Month</option>	
                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="fg-line">
                            <div class="select">
                                <select name="exp_year" id="exp_year" class="form-control input-sm"  onchange="getMonthList();">
                                    <option value="">Expiry Year</option>
                                    <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>      
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Security Code:</label>                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="password" id="" name="" style="display: none;" />
                            <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code"  />
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Billing Address:</label>                    
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="city" name="city" placeholder="City" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                </div>
                            </div>
                        
                        
                            <div class="col-md-6">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode"  />
                                </div>
                            </div>
                        </div>
                    </div>                     
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary m-t-30" id="nextbtn">Save Card</button>
                    </div>
                </div>
                       
              
            </form>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?php if ($studio->id == 54 || $studio->id == 4247) { ?>
<div class="Block">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
        <em class="icon-credit-card icon left-icon "></em>
        </div>
        <h4>Pay using Paypal</h4>
    </div>   
    <div class="form-group">
        <div class="col-md-offset-0 col-md-2">
            <button type="submit" class="btn btn-primary m-t-30" id="btnPayPal" onclick="paypalPay()">Click here to Authorize</button>
        </div>
    </div>
</div>
<?php } ?>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Your card has been saved successfully.</h4>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paymentModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="paymodal" id="paymodal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_payment" name="id_payment" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="paymentbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if (intval($studio->reseller_id) == 0) { ?>
<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    });
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }

    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
            switch (element.attr("name")) {
                case 'exp_month':
                    error.insertAfter(element.parent().parent());
                    break;
                case 'exp_year':
                    error.insertAfter(element.parent().parent());
                    break;
                default:
                    error.insertAfter(element.parent());
            }
        }

        });
        var x = validate.form();
        if (x) {
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled', 'disabled');
            $("#loadingPopup").modal('show');
            var url = "<?php echo Yii::app()->baseUrl; ?>/payment/saveCard";
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();
            var csrfToken = $('#csrfToken').val();

            $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'csrfToken': csrfToken}, function (data) {
                $("#loadingPopup").modal('hide');
                if (parseInt(data.isSuccess) === 1) {
                    $("#successPopup").modal('show');
                    setTimeout(function () {
                        location.reload();
                        return false;
                    }, 5000);
                } else {
                    $('#nextbtn').html('Save Card');
                    $('#nextbtn').removeAttr('disabled');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
                }
            }, 'json');
        }
    }

    
    function showConfirmPopup(obj) {
    var type = $(obj).attr('data-type');
    var msg = type;
    if(type=="makeprimary"){
        msg = "make primary";
    }
    swal({
            title: msg+" card?",
            text: "Are you sure you want to "+msg+" this card?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            if(type=="delete"){
                deleteCard(obj);
            }else{
                makeprimaryCard(obj);
            }
        });
        //$("#paymentModal").modal('show');
        //$("#headermodal").text(type.charAt(0).toUpperCase() + type.slice(1)+" Card?");
       //$("#bodymodal").text("Are you sure you want to "+type+"  Card?");
        
       $("#paymentbtn").attr('data-managepayment_id', $(obj).attr('data-managepayment_id'));
       
       var onclick = type+'Card(this)';
       
        $("#paymentbtn").attr('data-type', type);
        $("#paymentbtn").attr('onclick',onclick).bind('click');
      


  /*  $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/payment/"+type+"Card";
            
        $('#paymodal').attr("action", action);
        document.paymodal.submit();*/
        
    }
     function makeprimaryCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/payment/"+type+"Card";
            
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }
    
    function deleteCard(obj) {
        $("#id_payment").val($(obj).attr('data-managepayment_id'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/payment/"+type+"Card";
            
        $('#paymodal').attr("action", action);
        document.paymodal.submit();
    }
    
    function payFromPrimaryCard() {
        var action ="<?php echo Yii::app()->baseUrl; ?>/payment/payFromPrimaryCard";
        window.location.href = action;
    }
    
    function billDetail() {
        var action ="<?php echo Yii::app()->baseUrl; ?>/payment/billDetail";
        window.open(action,'_blank');
    }
    
    function paypalPay() {
		var action ="<?php echo Yii::app()->baseUrl; ?>/payment/authenticatePaypal";
        window.location.href = action;
    }
    
</script>
<?php } ?>
