<?php
$name = $rule->name;
$short_desc = str_replace('<br />', '', $rule->short_description);
$mapArr = $durationType = $mapIds = array();
if (!empty($rule)) {
    foreach ($rule['policy_rules_mapping'] as $map) {
        $mapIds[] = $map['policy_rules_master_id'];
        $mapArr[$map['policy_rules_master_id']] = $map['policy_value'];
        $durationType[$map['policy_rules_master_id']] = $map['duration_type'];
    }
}
?>
<?php echo CHtml::beginForm('', 'post', array('class' => 'form-horizontal', 'data-toggle' => "validator", 'role' => "form", "id" => "ruleFrm")); ?>    
<div class="row m-t-40">
    <div class="col-md-8 col-sm-12">
        <div class="Block">
            <!--<div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-info icon left-icon "></em>
                </div>
                <h4>Basic Information</h4>
            </div><hr>-->


            <div id="formContents" style='border:0px solid red;'>
                <div class="form-group">
                    <label for="movieName" class="col-md-4 control-label">Rule Name<span class="red">*</span>: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Enter rule name.." id="mname" name="rule[name]" value="<?php echo $name; ?>" class="form-control input-sm checkInput" required="">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="genre" class="col-md-4 control-label">Short Description<span class="red">*</span>: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea placeholder="Enter short description..." rows="5" class="form-control textarea checkInput" name="rule[short_description]" id="story1" required="" maxlength="200"><?php echo $short_desc; ?></textarea>
                            <span class="countdown1"></span>
                        </div>
                    </div>
                </div>
                <span id="policy-rules-area">
                    <?php foreach ($policies as $key => $masterPolicy) { ?>
                        <div class="form-group">
                            <label for="policy<?php echo $masterPolicy['id']; ?>" class="col-md-4 control-label"><?php echo ucwords($masterPolicy['policy_name']); ?>: </label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <!--<textarea placeholder="Enter short description..." rows="5" class="form-control textarea checkInput" name="rule[short_description]" id="story1"><?php echo $short_desc; ?></textarea>-->
                                    <span class="countdown1"></span>
                                    <?php
                                    $myPolicyArr = in_array($masterPolicy['id'], $mapIds) ? array('policy_value' => $mapArr[$masterPolicy['id']], 'duration_type' => $durationType[$masterPolicy['id']]) : array();
                                    echo Yii::app()->Helper->rulesInputType($masterPolicy, $myPolicyArr, $rule);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </span>



                <div class="form-group m-t-30">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" name="rule_btn" class="btn btn-primary btn-sm" id="save-rule-btn" value="<?php echo ($rule->id) ? "Update Rule" : 'Save'; ?>" />

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php echo CHtml::endForm(); ?>

<script>
    function checkRuleValue() {
        var getAllFields = document.querySelectorAll('[name="policy[value][]"]'), is_values = 0, values = [];
        Array.prototype.forEach.call(getAllFields, function (el) {
            if (el.value.trim() && el.value != 0) {
                is_values = 1;
            }
            /*values.push(el.value);*/
        });
        return is_values;
    }
    $(function () {
        $('[name="policy[value][]').bind('change keyup', function () {
            var getValue = checkRuleValue();
            if (getValue == 1) {
                $('#policy-rules-area').removeClass('has-error');
            } else {
                $('#policy-rules-area').addClass('has-error');
            }
            return false;
        });
        $('#ruleFrm').on('submit', function () {
            var getValue = checkRuleValue();
            if (getValue == 0) {
                $('#policy-rules-area').addClass('has-error');
                return false;
            }
            $('#policy-rules-area').removeClass('has-error');
            $('#save-rule-btn').attr('disabled', 'disabled');
            document.getElementById("ruleFrm").submit();
        });
    });

</script>