<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/modernizr-2.6.2.min.js"></script>

<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo $this->createAbsoluteUrl('policy/rulesbuilder'); ?>" data-toggle="tooltip" title="Add Rule">
            <button type="button" class="btn btn-primary btn-default m-t-10">Add Rule</button>
        </a>
    </div>
</div>
<div class="">
    <input type="hidden" class="data-count" value="<?php echo $total_video ?>" /> 
    <input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 
    <?php
    $studio_id = Yii::app()->user->studio_id;
    $base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
    ?>
    <table class="table" id="list_tbl">
        <thead>
            <tr>
                <!--<th style="width:100px;">
                    <div class="checkbox m-b-15">
                        <label>
                            <input type="checkbox" class="sub_chkall" id="check_all">
                            <i class="input-helper"></i>
                        </label>
                    </div>
                </th>-->
                <th>Rule Name</th>
                <th>Short Description</th>
                <th data-hide="status" class="width">Status</th>
                <th data-hide="phone" class="width">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // echo "<pre>";
            // print_r($image_details);
            if (!empty($data_arr)) {
                foreach ($data_arr as $key => $val) {
                    $appliedRule = PolicyRules::model()->countAppliedRules($val['id']);
                    ?>
                    <tr>
                        <!--<td>
                            <div class="checkbox m-b-15">
                                <label>
                                    <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                                    <i class="input-helper"></i>

                                </label>
                            </div> 
                        </td>-->
                        <td>
                            <?php echo $val['name']; ?>
                        </td>

                        <td>
                            <?php
                            echo substr($val['short_description'], 0, 75);
                            if (strlen($val['short_description']) > 75) {
                                $description = base64_encode($val['short_description']);
                                $name = base64_encode($val['name']);
                                echo '..<a href="javascript:;" onclick="openDescriptionPopup(\'' . $name . '\', \'' . $description . '\');">more</a>';
                            }
                            ?>
                        </td>  
                        <td> 
                            <?php if ($val['status'] == 1) { ?>
                                <p><a title="Click here to inactive" data-toggle="tooltip" href="javascript:void(0);" <?php if(!$appliedRule){?>onclick="changeStatus('<?php echo $val['id']; ?>', this)"<?php } else {?>style="cursor: not-allowed;"<?php }?>>Active</a></p>
                            <?php } else {
                                ?>
                                <p><a title="Click here to active" data-toggle="tooltip" href="javascript:void(0);" <?php if(!$appliedRule){?>onclick="changeStatus('<?php echo $val['id']; ?>', this)"<?php } else {?>style="cursor: not-allowed;"<?php }?>>Inactive</a></p>
                            <?php }
                            ?>
                        </td>
                        <td>
                            <?php if (count($val['policy_rules_mapping'])) { ?>
                                <p><a href="javascript:void(0);" data-id="<?php echo $val['id']; ?>" onclick="openRulePopup(this);" data-toggle="tooltip" title="View Rules" class="upload-video"><em class="fa fa-eye"></em>&nbsp;&nbsp; View Rule</a></p>
                            <?php } ?>
                            <p><a href="<?php echo $this->createAbsoluteUrl('policy/rulesbuilder', array('uniqid' => $val['uniqid'])); ?>" data-toggle="tooltip" title="Edit Rule"><em class="fa fa-pencil"></em>&nbsp;&nbsp; Edit Rule</a></p>
                            <p>
                                <?php if($appliedRule){?>
                                <a href="javascript:;" data-toggle="tooltip" title="Delete Rule" style="cursor: not-allowed;"><em class="icon-trash"></em>&nbsp; Delete Rule </a>
                                <?php } else {?>
                                    <a href="<?php echo $this->createAbsoluteUrl('policy/deleterule', array('rule_id' => $val['id'])); ?>" data-toggle="tooltip" title="Delete Rule" data-msg="Are you sure to delete the rule?" class="confirm" id="confirm"><em class="icon-trash"></em>&nbsp; Delete Rule </a>
                                <?php }?>
                            </p>  
                        </td>
                    </tr>	


                    <?php
                }
            } else {
                ?>
            </tbody>				
            <tbody>
                <tr>
                    <td colspan="4">No Rules Found</td>
                </tr>
            </tbody>
        <?php } ?> 
    </table>


    <div class="pull-right">
        <?php
        $opts = array('class' => 'pagination m-t-0');
        $this->widget('CLinkPager', array(
            'currentPage' => $pages->getCurrentPage(),
            'itemCount' => $item_count,
            'pageSize' => $page_size,
            'maxButtonCount' => 6,
            "htmlOptions" => $opts,
            'selectedPageCssClass' => 'active',
            'nextPageLabel' => '&raquo;',
            'prevPageLabel' => '&laquo;',
            'lastPageLabel' => '',
            'firstPageLabel' => '',
            'header' => '',
        ));
        ?>
    </div>
</div>
<!-- view popup start -->
<div id="viewrule_popup" class="modal fade in" role="dialog" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalRuleLabel">Rules</h4>
            </div> 
            <div class="modal-body">
                <div id="viewrule_body">Wait...</div>
            </div>
        </div>
    </div>
</div>

<div id="viewdescripion_popup" class="modal fade in" role="dialog" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-overflow popup_bottom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalRuleName">Rules</h4>
            </div> 
            <div class="modal-body">
                <div id="viewdescription_body">Wait...</div>
            </div>
        </div>
    </div>
</div>
<!-- view popup end -->

<style type="text/css">
    .loaderDiv{position: absolute;left: 30%;top:10%;display: none;}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<!-- Script for Upload Trailer -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>

<script type="text/javascript">

                        function confirmDelete(location, msg) {
                            swal({
                                title: "Delete Rule?",
                                text: msg,
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html: true
                            }, function () {
                                window.location.replace(location);
                            });

                        }
                        function showLoader(isShow) {
                            if (typeof isShow == 'undefined') {
                                $('.loaderDiv').show();
                                $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
                            } else {
                                $('.loaderDiv').hide();
                                $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
                            }
                        }

                        function delete_selected() {

                            var allVals = [];
                            $(".sub_chk:checked").each(function () {
                                allVals.push($(this).attr('data-id'));
                            });
                            //alert(allVals.length); return false;  
                            if (allVals.length <= 0)
                            {
                                swal("Please select rule to delete");
                            } else {
                                //show alert as per the videos mapped status
                                check_videomapped(allVals);
                            }
                        }
//delete viudeo
                        function delete_multiple_rules(allVals) {
                            $.ajax({
                                url: '<?php echo Yii::app()->getBaseUrl(true); ?>/policy/DeleteSelectedPolicy',
                                method: 'POST',
                                data: {id: allVals},
                                success: function ()
                                {
                                    window.location.reload();
                                }
                            });
                        }

                        function openRulePopup(obj) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/policy/ajaxgetrulemapping",
                                data: {rule_id: $(obj).attr('data-id')},
                                dataType: 'json',
                                success: function (response) {
                                    $('#myModalRuleLabel').html(response.name);
                                    if (response.mapping_rules.length) {
                                        var contentHtml = '<table class="table"><thead><tr><th>Type</th><th>Value</th></tr></thead><tbody>';
                                        for (var i = 0; i < response.mapping_rules.length; i++) {
                                            var duration_type = '';
                                            if (response.mapping_rules[i].policy_type.toLowerCase() == 'watch duration' || response.mapping_rules[i].policy_type.toLowerCase() == 'access duration') {
                                                if(response.mapping_rules[i].duration_type == 1){
                                                    duration_type = 'Hour(s)'
                                                } else if(response.mapping_rules[i].duration_type == 2){
                                                    duration_type = 'Day(s)'
                                                } else if(response.mapping_rules[i].duration_type == 3){
                                                    duration_type = 'Month(s)'
                                                } else{
                                                    duration_type = 'Minute(s)'
                                                }
                                            }
                                            contentHtml += '<tr>';
                                            contentHtml += '<td>' + response.mapping_rules[i].policy_type + '</td>';
                                            contentHtml += '<td>' + response.mapping_rules[i].policy_value + ' ' + duration_type + '</td>';
                                            contentHtml += '</tr>';
                                        }
                                        contentHtml += '</tbody></table>';
                                    } else {
                                        var contentHtml = '<p>No rules exist</p>';
                                    }
                                    $("#viewrule_body").html(contentHtml);
                                    $("#viewrule_popup").modal('show');
                                }
                            });
                        }

                        function changeStatus(id, obj) {
                            if (!id) {
                                return;
                            }
                            var htmlText = $(obj).html();
                            $(obj).html('Loading...');
                            $.ajax({
                                method: 'post',
                                url: '/policy/changestatus',
                                data: {id: id},
                                dataType: 'json',
                                success: function (response) {
                                    var htm = htmlText;
                                    if (response.success == true) {
                                        if (response.status == 1) {
                                            htm = 'Active';
                                            $(obj).attr('title', 'Click here to inactive');
                                        } else {
                                            htm = 'Inactive';
                                            $(obj).attr('title', 'Click here to active');
                                        }
                                    } else {
                                        swal(response.message);
                                    }
                                    $(obj).html(htm);
                                }

                            });

                        }

                        function openDescriptionPopup(name, desc) {
                            //var decodedString = Base64.decode(desc);
                            var decodedNameString = atob(name);
                            var decodedDescrString = atob(desc);
                            $("#myModalRuleName").html(decodedNameString);
                            $("#viewdescription_body").html(decodedDescrString);
                            $("#viewdescripion_popup").modal('show');
                        }


                        $(document).ready(function () {
                            $(document).on("click", "#check_all", function () {
                                $(".sub_chk").prop('checked', $(this).prop('checked'));
                            });
                            $(document).on('click', "a.confirm", function (e) {
                                e.preventDefault();
                                var location = $(this).attr('href');
                                var msg = $(this).attr('data-msg');
                                confirmDelete(location, msg);
                            });
                        });

</script>
