 <?php if(count($data)<=10) { ?>
<style>
    .paging_simple_numbers{display:none;}
    #example_filter{display:none;}
</style>
 <?php } ?>  

      
   <div class="row m-b-40">
        <div class="col-xs-12">
            <a href="<?php echo $this->createUrl('adminceleb/add'); ?>">
                <button type="submit" class="btn btn-primary btn-default m-t-10">
                        Add New Cast/Crew
                </button>
            </a>
            <a href="<?php echo $this->createUrl('adminceleb/managedatafields'); ?>">
                <button type="submit" class="btn btn-primary btn-default m-t-10">
                    Manage Data Fields   
                </button>
            </a>
        </div>
    </div>
  
    <div class="filter-div m-b-10">
        <form action="<?php echo $this->createUrl('adminceleb/celeblist'); ?>" name="searchForm" id="search_content" method="post">
        <div class="row">
            <div class="col-sm-4 m-b-10">
                <div class="form-group input-group">
                        <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                        <div class="fg-line">
                            <input type="text" name="searchForm[search_text]" value="<?php echo @$searchText; ?>"  
                            id="search_content" onkeypress="handle(event);" class="form-control fg-input" placeholder="What are you searching for?" >
                        </div>

                </div>
            </div>
        </div>
        </form>
    </div>

	  <table id="example" class="table mob-compress display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="min-width">Name</th>
				<th>Image</th>
				<th data-hide="phone">Bio </th>
				<th data-hide="phone" class="width">Action</th>
			</tr>
		</thead>
		<tbody>
		
		<?php
            if ($data) {
                $default_img = POSTER_URL . '/no-image.png';
                $subdomain = isset(Yii::app()->user->studio_subdomain) ? Yii::app()->user->studio_subdomain : SUB_DOMAIN;
                foreach ($data AS $key => $details) {
                    $poster_id = $details['poster']['id'];
                    $celeb_id  = $img_id = $details['id'];
                    $permalink = $details['permalink'];
                        if ($details['parent_id'] > 0) {
                        $celeb_id  = $img_id = $details['parent_id'];
                        }
                    $img_path = $this->getPoster($img_id, 'celebrity', 'medium');
                     if (false === file_get_contents($img_path)) {
                         $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                        $img_path = $postUrl . '/system/posters/' . $details['poster']['id'] . "/medium/" . $details['poster']['poster_file_name'];
                        if(false === file_get_contents($img_path)){
                        $img_path = "/img/No-Image-Vertical.png";
                        }
                      }
                    //Poster 
                    //$img_path = isset($poster_id)?POSTER_URL.'/system/posters/'.$poster_id."/thumb/".$details['poster']['poster_file_name']:$default_img;
                    ?>
                    <tr>
                        <td class="text-capitalize">
                            <strong><a href="<?php echo Yii::app()->common->studiourl($this->studio->id) . '/star/' . $permalink; ?>"><?php echo $details['name']; ?></a></strong><br/>
                        </td>
                        
                        <td>
                            <div class="Box-Container thumbnail-small m-b-10">
                                <div class="thumbnail">
                                        <img src="<?php echo $img_path; ?>" alt="" >
                                </div>
                            </div>
                        </td>
			<td>
                            <?php echo $details['summary']; ?>
                        </td>
                        <td>
                            <p><a href="<?php echo $this->createUrl('adminceleb/edit/', array('celeb_id' => $celeb_id)); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit</a></p>
                            
                                <?php if (($language_id == $details['language_id']) && ($details['parent_id'] == 0)) { ?>
                                    <p><a href="javascript:void(0);"  celeb_id="<?php echo $celeb_id; ?>"  onclick="showConfirmPopup(this);" > <em class="icon-trash"></em>&nbsp;&nbsp; Remove</a></p>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php
                    $cnt++;
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">
                        <p class="text-red">Oops! You don't have any cast/crew in your Studio.</p>
                    </td>
                </tr>
            <?php } ?>
            
			
		</tbody>
	</table>	

    <div class="pull-right">
        <?php
            if ($data) {
                $opts = array('class' => 'pagination m-t-0');
                $this->widget('CLinkPager', array(
                    'currentPage' => $pages->getCurrentPage(),
                    'itemCount' => $item_count,
                    'pageSize' => $page_size,
                    'maxButtonCount' => 6,
                    "htmlOptions" => $opts,
                    'selectedPageCssClass' => 'active',
                    'nextPageLabel' => '&raquo;',
                    'prevPageLabel'=>'&laquo;',
                    'lastPageLabel'=>'',
                    'firstPageLabel'=>'',
                    'header' => '',
                ));
            }
        ?>
    </div>
    <!-- <div class="pull-right">
        <ul class="pagination m-t-0" id="yw0"><li class="first"><a href="/adminceleb/celeblist"></a></li>
            <li class="previous"><a href="/adminceleb/celeblist">«</a></li>
            <li class="page"><a href="/adminceleb/celeblist">1</a></li>
            <li class="page active"><a href="/adminceleb/celeblist?page=2">2</a></li>
            <li class="next hidden"><a href="/adminceleb/celeblist?page=3">»</a></li>
            <li class="last hidden"><a href="/adminceleb/celeblist?page=4">4</a></li>
            <li class="last hidden"><a href="/adminceleb/celeblist?page=5">5</a></li>
            <li class="last hidden"><a href="/adminceleb/celeblist?page=6">6</a></li>
        </ul>    
    </div> -->

    <div class="h-40"></div>



<div class="modal fade" id="celebmodal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Cast /Crew?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span>Are you sure you want to <b>Delete Cast/Crew?</b> </span> 
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" id="celeb_list" class="btn btn-default " celeb_id=""  onclick="removeBox(this);">Yes</a>
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($data) {?>
<!-- <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable_custom_paging.js"></script> -->
<script>
// $(document).ready(function() {
//     oTable =  $('#example').DataTable( {
//          "paging":false,
//          "ordering": false,
//         "info":     false
//     } );
    
//     $('#myInputTextField').keyup(function(){
//       oTable.search($(this).val()).draw() ;
//     });


// });

    </script>
<?php }?>
<script>
    function showConfirmPopup(obj) {
        swal({
                title: "Delete Cast /Crew?",
                text: "Are you sure you want to Delete Cast/Crew?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html:true
            }, function() {
                var celeb_id = $(obj).attr('celeb_id');
                if (parseInt(celeb_id)) {
                    var url = HTTP_ROOT + "/adminceleb/remove/celeb_id/" + celeb_id;
                    window.location.href = url;
                } else {
                    return false;
                }
            });
        //$("#celebmodal").modal('show');
       // $("#celeb_list").attr('celeb_id', $(obj).attr('celeb_id'));
    }

    function removeBox(obj) {
        var celeb_id = $(obj).attr('celeb_id');
        if (parseInt(celeb_id)) {
            var url = HTTP_ROOT + "/adminceleb/remove/celeb_id/" + celeb_id;
            window.location.href = url;
        } else {
            return false;
        }
    }
    function handle(e) {
        if (e.keyCode === 13) {
            $('#search_content').submit();
        }
        return false;
    }
   
   
</script>