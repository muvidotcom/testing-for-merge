<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<style type="text/css">
    .jcrop-keymgr
    {
        display:none !important;
    }
</style>
<?php
$enable_lang = $this->language_code;
if ($_COOKIE['Language']) {
	$enable_lang = $_COOKIE['Language'];
}
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
$celeb_poster = Yii::app()->general->getCelebPosterSize($studio_id);
$poster_width = $celeb_poster['poster_width'];
$poster_height = $celeb_poster['poster_height'];
if($celeb->id){
    $img_path = $this->getPoster($celeb->id, 'celebrity', 'medium');
}else{
    $img_path = ""; 
}
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';

$celeb_name =  (isset($celeb->name) && !empty($celeb->name)) ? $celeb->name : ""; 
$summary =  (isset($celeb->summary) && !empty($celeb->summary)) ? $celeb->summary : ""; 
if(!empty(@$celeblang)){
    if (array_key_exists($celeb->id, @$celeblang)) {
        $celeb_name = @$celeblang[$celeb->id]->name;
        $summary    = @$celeblang[$celeb->id]->summary;
    }
}
?>
<div class="row m-t-40">
    <div class="col-md-8">
        <div class="block">
            <form role="form" class="form-horizontal" method="post" id="celeb_form" name="celeb_form" onsubmit="return validate_form();"  enctype="multipart/form-data"  action="<?php echo $this->createUrl('adminceleb/save'); ?>" >
                


                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Enter name..." id="celeb_name" name='celeb[name]' class="form-control input-sm" value="<?php echo $celeb_name; ?>" >
                        </div>
                        <label class="celeb_name_error red"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="summary" class="col-md-4 control-label">Summary:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="celeb[summary]" id="summary" rows="5" placeholder="Bio of Celebrity" class="form-control input-sm textarea auto-size"><?php echo $summary; ?></textarea>
                        </div>
                        <label class="summary_error red"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="profile" class="col-md-4 control-label">Profile Picture:</label>
                    <div class="col-md-8">
                        <?php if(!($celeb->id) || ($celeb->language_id == $this->language_id  && $celeb->parent_id ==0 )){ ?>
                        <div class="fg-line">
                            <div class="btn btn-default-with-bg btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg" >Upload Image</div>
                        </div>
                        <?php } ?>
                        <label class="image_error red"></label>
                        <div class="Preview-Block m-t-20">
                            <div class="" id="castcrew_preview" <?php if(!$celeb->id){echo "style='display:none;'";} ?>>
                                <?php
                                $img_path = $this->getPoster($celeb->id, 'celebrity', 'medium');
                                if (false === file_get_contents($img_path)) {
                         $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                        $img_path = $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'];
                        if(false === file_get_contents($img_path)){
                        $img_path = "";
                        }
                      }
                       if($img_path !=""){
                       ?>       
                                
                                <img id="cast_preview" src="<?php echo $img_path; ?>" />
                       <?php } ?>
                            </div>
                            <canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
                        </div>
                    </div>
                </div>
                

                <!--tabbed pop up --->   
                <div class="modal fade is-Large-Modal bs-example-modal-lg" id="Preview-Video"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Upload Preview</h4></div>
                            <div class="modal-body">

                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active" onclick="hide_file()">
                                            <a id="home-tab1" href="#Upload-image"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Upload Image</a>
                                        </li>
                                        <li role="presentation" onclick="hide_gallery()">
                                            <a id="profile-tab1"  href="#Choose-From-Library" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Library</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Upload-image">
                                            <div class="row is-Scrollable">
                                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                                    <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm"  onclick="click_browse();" data-width="<?php echo @$poster_size['width']; ?>" data-height="<?php echo @$poster_size['height']; ?>">
                                                    <input id="celeb_pic" name="Filedata" type="file" style="display:none;" onchange="fileSelectHandler()" />
                                                    <p class="help-block">Upload image size of <?php echo $poster_width;?>px (w) X <?php echo $poster_height;?>px (h).</p>
                                                    <input type="hidden" id="x1" name="x1" />
                                                    <input type="hidden" id="y1" name="y1" />
                                                    <input type="hidden" id="x2" name="x2" />
                                                    <input type="hidden" id="y2" name="y2" />
                                                    <input type="hidden" id="w" name="w"/>
                                                    <input type="hidden" id="h" name="h"/>
                                                    <div class="col-xs-12">
                                                        <div class="Preview-Block">
                                                            <div class="thumbnail m-b-0">
                                                          <div class="relative m-b-10" id="celeb_preview">
                                                                <img id="preview"  class="jcrop-preview" style="display:none;">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="Preview-Block">
                                                            <?php if ($celeb['poster']['poster_file_name']) { ?>
                                                            <div class="thumbnail m-b-0">
                                                                <div class="relative m-b-10" id="editceleb_preview">

                                                                     <?php
                                                                        $img_path = $this->getPoster($celeb->id, 'celebrity', 'medium');
                                                                        if (false === file_get_contents($img_path)) {
                                                                         $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                                                            $img_path = $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'];
                                                                            if(false === file_get_contents($img_path)){
                                                                            $img_path = "";
                                                                            }
                                                                          }
                                                                          if($img_path !=""){
                                                                       ?>       
                                                                    <img src="<?php echo $img_path; ?>" />
                                                                          <?php } ?>

                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Choose-From-Library">
                                         <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                                    <input type="hidden" name="g_original_image" id="g_original_image" />
                                                    <input type="hidden" id="x13" name="jcrop_allimage[x13]" />
                                                    <input type="hidden" id="y13" name="jcrop_allimage[y13]" />
                                                    <input type="hidden" id="x23" name="jcrop_allimage[x23]" />
                                                    <input type="hidden" id="y23" name="jcrop_allimage[y23]" />
                                                    <input type="hidden" id="w3" name="jcrop_allimage[w3]" />
                                                    <input type="hidden" id="h3" name="jcrop_allimage[h3]" />

                                            <div class="row  Gallery-Row">
                                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="video_content_div">
                                                    <ul class="list-inline">
                                                     <?php
                                                            foreach ($all_images as $key => $val) {
                                                                if ($val['image_name'] == '') {
                                                                    $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                                                                } else {
                                                                    //$img_path = $val['list_image_name'];
                                                                    $img_path = $base_cloud_url . $val['s3_thumb_name'];
                                                                    $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                                                                }
                                                                ?> 
                                                        <li>
                                                            <div class="Preview-Block">
                                                                <div class="thumbnail m-b-0">
                                                                    <div class="relative m-b-10">
                                                                     <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                                                                        <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                                                                        <img class="img" src="<?php echo $img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >
                                                                        <div class="caption overlay overlay-white">
                                                                            <div class="overlay-Text">
                                                                                <div>
                                                                                    <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="toggle_preview(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                                                                        <span class="btn btn-primary icon-with-fixed-width">
                                                                                          <em class="icon-check"></em>
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                                        <div class="preloader pls-blue text-center " >
                                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="Preview-Block row">
                                                        <div class="col-md-12 text-center" id="gallery_preview">
                                                            <img id="glry_preview" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" id="next" class="btn btn-primary" data-dismiss="modal" onclick="seepreview(this);">Next</button>
                            </div>
                        </div>
                    </div>
                </div>

            <?php   
            foreach (@$customData AS $ckey=>$cval){
                $svalue = array_key_exists($cval['id'], $custom_field_value) ? $custom_field_value[$cval['id']] : "";
                $svalue = is_string($svalue) && is_array(json_decode($svalue, true)) ? json_decode($svalue, true) : $svalue;
            ?>
                <div class="form-group">
                    <label for="<?= $cval['label'];?>" class="col-md-4 control-label"><?= $cval['label'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
                    <?php if(!$cval['field_type']){?>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <textarea class="form-control input-sm checkInput" rows="5" placeholder="" name="custom[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?>><?php echo @$svalue; ?></textarea>
                            </div>
                        </div>
                    <?php }elseif($cval['field_type']==1){?>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' placeholder="" id="<?= $cval['f_id'];?>" name="custom[<?= $cval['f_name'];?>]" value="<?php echo @$svalue; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> >
                            </div>
                        </div>
                    <?php }elseif($cval['field_type']==2){?>
                        <div class="col-md-8">
                            <div class="fg-line">
                                <div class="select">    
                                    <select name="custom[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                        <?php
                                        echo "<option value=''>-Select-</option>";
                                        $opData = json_decode($cval['f_value'],true);
                                        $opData_new = $opData;
                                        $opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
                                        $opData = empty($opData)?$opData_new:$opData;
                                        foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                            if ($svalue == $opvalue) {
                                                $selectedDd = 'selected ="selected"';
                                            }
                                            echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php }elseif($cval['field_type']==3){?>				
                        <div class="col-md-8">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="custom[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput">
                                        <?php
                                        $opData = json_decode($cval['f_value'],true);
                                        $opData_new = $opData;
                                        $opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
                                        $opData = empty($opData)?$opData_new:$opData;
                                        foreach($opData AS $opkey=>$opvalue){
                                            $selectedDd = '';
                                            if (in_array($opvalue, $svalue)) {
                                                $selectedDd = 'selected ="selected"';
                                            }
                                            echo "<option value='".$opvalue."'".$selectedDd.">" . $opvalue . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
		<?php } ?>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 m-t-30">
                        <input type="hidden" name='celeb[id]'  value="<?php echo $celeb->id ?>" >
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
                <input type="hidden" id="img_width" value="<?php echo $poster_width; ?>" />
                <input type="hidden" id="img_height" value="<?php echo $poster_height; ?>" />
            </form>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
    $('input.ind_image').on('change', function () {
        $('input.ind_image').not(this).prop('checked', false);
    });
    function validate_form() {
        var celeb_name = $("#celeb_name").val();
        var celeb_summ = document.getElementById("summary").value;
        var celeb_file_img = $("#celeb_pic").val();
        var celeb_gallery_img = $("#g_image_file_name").val();
        var file_crop = $("#w").val();
        var img_crop = $("#w3").val();
        var flag_gallery = $("#g_original_image").val();
        var regex = /^[a-zA-Z ]*$/;
        if (celeb_name == "") {
            $(".celeb_name_error").html("Name Can not be blank!");
            return false;
        }
        if(celeb_name.trim() ==="")
        {
            $(".celeb_name_error").html("Name Can not be Blank Space!");
            return false;
        }
        /* 
         * special charecter removed issue id - 5155 24/8/16 
        else if (!regex.test(celeb_name)) {
            $(".celeb_name_error").html("It Can Not Contain Special Character And Number");
            return false;
         }
        */
        else if (celeb_summ == "") {
            $(".summary_error").html("Summary Can not be blank!");
            return false;
        }
        else if (celeb_summ == "" && celeb_name == '') {
            $(".celeb_name_error").html("Name Can not be blank!");
            $(".summary_error").html("Please fill the details");
            return false;
        }
        /*
        else if (celeb_file_img == "" && celeb_gallery_img == "") {
            $(".image_error").html("Please select image");
            return false;
        }
        */
        else if (flag_gallery != '' && img_crop == '') {

            $(".image_error").html("Please crop the image");
            return false;
        }
        else if (celeb_file_img != "" && celeb_gallery_img != "") {
            $(".image_error").html("You can select any one resource for images");
            return false;
        }
	 else if (celeb_file_img != "" && (celeb_file_img.match(/['|"|-|,]/) == true) ) {
           $(".image_error").html("File names with symbols such as ' , - are not supported");
            return false;
        }
        else
        {
            return true;
        }

    }
    function toggle_preview(id, img_src, name_of_image)
    {
        var poster_width = <?php echo $poster_width;?>;
        var poster_height = <?php echo $poster_height;?>;
        $('#glry_preview').css("display","block");
    	$('#celeb_pic').val('');
        
        showLoader();
       // $("#glry_preview").css("display", "none");
        var image_file_name = name_of_image;
        //$("#thumb_"+id).css("border","3px solid #efefef !important");
        var image_src = img_src;
        clearInfo();

        $("#g_image_file_name").val(image_file_name);
        $("#g_original_image").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        //alert(image_type);
           var aspectratio = poster_width / poster_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(img_src);
    var width = 0;
    var height = 0;
     
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            if (width < poster_width || height < poster_height) {
                showLoader(1);                
                swal('You have selected small file, please select one bigger image file more than '+poster_width+' X '+poster_height);
                $("glry_preview").addClass("hide");
                $("#g_image_file_name").val("");
                $("#g_original_image").val("");
                $("#celeb_preview").addClass("hide");
                //$('button[type="submit"]').attr('disabled', 'disabled');
                return;
            }

            // preview element
            var oImage = document.getElementById('glry_preview');
            showLoader(1)
            oImage.src = img_src;
            $("#glry_preview").css("display", "block");
            oImage.onload = function () {
                //alert(oImage.naturalWidth);
                // alert(oImage.naturalHeight);
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview').width(oImage.naturalWidth);
                    $('#glry_preview').height(oImage.naturalHeight);
                    $('#preview').width("450");
                    $('#preview').height("250");
                }

                //setTimeout(function(){
                // initialize Jcrop
                $('#glry_preview').Jcrop({
                    minSize: [poster_width, poster_height], // min crop size
                    aspectRatio: 1, // keep aspect ratio 1:1
                    boxWidth: 500,
                    boxHeight: 500,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoallImage,
                    onSelect: updateInfoallImage,
                    onRelease: clearInfoallImage
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, poster_width, poster_height]);
                });
                //},100);

            };
            //};

            // read selected file as DataURL
            //oReader.readAsDataURL(oFile);                
        };

    }
    function fileSelectHandler() {
        var poster_width = <?php echo $poster_width;?>;
        var poster_height = <?php echo $poster_height;?>;        
        $('#celeb_preview').css("display","block");
        $("#g_image_file_name").val('');
        $("#g_original_image").val('');
        $('#glry_preview').css("display","none");
        var aspectratio = poster_width / poster_height;
        clearInfo();
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview").hide();
        $("#celeb_preview").removeClass("hide");
        // get selected file
        var oFile = $('#celeb_pic')[0].files[0];
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#celeb_pic").val("");
            $("#celeb_preview").addClass("hide");
            //$('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }
            
        var img = new Image();

        img.src = window.URL.createObjectURL(oFile);

        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < poster_width || height < poster_height) {
                swal('You have selected small file, please select one bigger image file more than '+poster_width+' X '+poster_height);
                $("#celeb_pic").val("");
                $("#celeb_preview").addClass("hide");
                //$('button[type="submit"]').attr('disabled', 'disabled');
                return;
            }

            // preview element
            var oImage = document.getElementById('preview');

            // prepare HTML5 FileReader
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                // e.target.result contains the DataURL which we can use as a source of the image
                //alert(e.target.result);
                oImage.src = e.target.result;
                oImage.onload = function () { // onload event handler
                    // destroy Jcrop if it is existed
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                        $('#glry_preview').width("450");
                        $('#glry_preview').height("250");

                        //$('#preview').css("height","100%");
                        //$('#preview').css("width","");
                    }

                    //setTimeout(function(){
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        minSize: [poster_width, poster_height], // min crop size
                        aspectRatio: aspectratio, // keep aspect ratio 1:1
                        boxWidth: 500,
                        boxHeight: 500,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function () {

                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];

                        // Store the Jcrop API in the jcrop_api variable
                        // Store the Jcrop API in the jcrop_api variable
                        jcrop_api = this;
                        //jcrop_api.animateTo([10, 10, 290, 410]);
                        jcrop_api.setSelect([10, 10, poster_width, poster_height]);
                    });
                    //},100);

                };
            };

            // read selected file as DataURL
            oReader.readAsDataURL(oFile);
        };
    }

    // Converts image to canvas; returns new canvas element
    function convertImageToCanvas(image) {
        var canvas = document.createElement("canvas");
        canvas.width = image.width;
        canvas.height = image.height;
        canvas.getContext("2d").drawImage(image, 0, 0);

        return canvas;
    }

// Converts canvas to an image
    function convertCanvasToImage(canvas, image_type)
    {
        var image = new Image();
        image.src = canvas.toDataURL("image/jpg");
        return image;
    }
 function hide_file()
        {
          $('#preview').css("display","none");   
        }
function hide_gallery()
{
 $('#glry_preview').css("display","none");     
}
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
        } else {
            $('.loaderDiv').hide();
            $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
        }
    }
    function click_browse() {
        $("#celeb_pic").click();
    }
function seepreview(obj){    
    var canvaswidth = $("#img_width").val();
    var canvasheight = $("#img_height").val();
    if($("#x1").val() != ""){
        var x1 = $('#x1').val();
        var y1 = $('#y1').val();
        var width = $('#w').val();
        var height = $('#h').val();
    }else if($("#x13").val() != ""){
        var x1 = $('#x13').val();
        var y1 = $('#y13').val();
        var width = $('#w3').val();
        var height = $('#h3').val();
    }else{
        return false;
    }
    $("#previewcanvas").show();
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        //$('#imgCropped').val(canvas.toDataURL());
    };
    $("#castcrew_preview").hide();
    if($('#g_image_file_name').val()==''){
        img.src = $('#preview').attr("src");
    }else{
        img.src = $('#glry_preview').attr("src");
    }    
    $('#myLargeModalLabel').modal('hide');
    $(obj).html("Next");
}
</script>