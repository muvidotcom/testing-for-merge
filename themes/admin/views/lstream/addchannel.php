<style type="text/css">
    .loading_div{
        display: none;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
<div id="success_msg"></div>
<?php
if (@$data) {
    $formAction = $this->createAbsoluteUrl('lstream/updateChannel');
} else {
    $formAction = $this->createAbsoluteUrl('lstream/addChannel');
}
?>
<form role="form" class="form-horizontal" action="<?php echo $formAction; ?>" method="post" enctype="multipart/form-data" name="movie" id='movie' data-toggle="validator">
    <div class="row m-t-40 m-b-40">
        <div class="col-md-4 col-sm-12 pull-right--desktopOnly">
            <div class="Block">
                <div class="Block-Header">
                    <?php
                    if (isset($data) && $data['id']) {
                        $movie_stream_id = $_REQUEST['movie_stream_id'];
                        $posterImg = $this->getPoster($data['id'], 'films', 'episode');
                        $obj_type = 0;
                    } else {
                        $posterImg = POSTER_URL . '/no-image-h.png';
                    }
                    $cropDimesion = Yii::app()->common->getCropDimension('episode');
                    ?>
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Upload Poster</h4>
                </div>
                <hr>
                <div class="border-dotted m-b-40">
                    <div class="text-center">
                        <input type="button" class="btn btn-default-with-bg" value="Browse" data-toggle="modal" data-target=".bs-example-modal-lg">
                        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'px (w) X' . $cropDimesion['height'] . 'px (h)'; ?></span></h5>   
                    </div>
                    <div class="m-b-10 displayInline fixedWidth--Preview">
                        <div class="poster-cls  avatar-view jcrop-thumb">
                            <div id="avatar_preview_div">
                                <img src="<?php echo $posterImg; ?>" id="preview_content_img" rel="tooltip" title="<?php echo @$data['name'] ? @$data['name'] : 'Upload your poster'; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="modal is-Large-Modal fade bs-example-modal-lg" id="myLargeModalLabel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header text-left">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Upload</h4></div>
				<div class="modal-body">
					<div class="row is-Scrollable">
						<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<input type="button" class="btn btn-default-with-bg" value="Browse" onclick="click_browse();">
							<input class="avatar-input" id="avatarInput" name="Filedata" type="file" onchange="fileSelectHandler();" style="display:none;">
							<p class="help-block">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $cropDimesion['width'] . 'px (w) X' . $cropDimesion['height'] . 'px (h)'; ?></span></p>
						</div>
                                                <input type="hidden" name="add_chnel" value="add">
						<input type="hidden" id="x1" name="jcrop[x1]" />
						<input type="hidden" id="y1" name="jcrop[y1]" />
						<input type="hidden" id="x2" name="jcrop[x2]" />
						<input type="hidden" id="y2" name="jcrop[y2]" />
						<input type="hidden" id="w" name="jcrop[w]">
						<input type="hidden" id="h" name="jcrop[h]">
                                                <input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidth" id="reqwidth" readonly/>
                                                <input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheight" id="reqheight" readonly/>
						<div class="col-xs-12">
							<div class="Preview-Block">
								<div class="thumbnail m-b-0">
									<div id="celeb_preview" class="col-md-12 margin-topdiv">
										<img id="preview"/>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>		
				
	
		<div class="modal-footer">
			<button type="button" class="btn btn-default" onclick="seepreview(this);">Next</button>
		</div>
	</div>
</div>
</div>

                    <input type="hidden" value="<?php echo @$data['id']; ?>" name="movie_id" id="movie_id"  />
                    <input type="hidden" value="<?php echo @$data['livestream_id']; ?>" name="livestream_id" id="livestream_id"  />
                    <input type="hidden" value="<?php echo @$data['uniq_id']; ?>" name="uniq_id" id="uniq_id"  />
                    <input type="hidden" value="<?php echo @$selected_content_types_id; ?>" name="content_types_id" id="content_types_id"  />
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-info icon left-icon "></em>
                    </div>
                    <h4>Basic Information</h4>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-sm-4">Channel Category:</label>
                    <div class="col-sm-8">
                        <div class="fg-line">
                            <div class="select">
                                <select class="form-control input-sm" id="contentTypes" name="movie[content_type]" onchange="select_type(this.value)">
                                    <?php if (!@$data) { ?>
                                        <option value="">Select</option>
                                        <?php
                                    }
                                    if (@$data['content_type_id']) {
                                        $selected_index = $data['content_type_id'] . '-' . $data['content_types_id'];
                                    }
                                    $selected_content_types_id = '';
                                    foreach ($contentList AS $key => $cont) {
                                        $selected = '';
                                        if (($key == @$data['content_type_id']) && $cont['content_types_id'] == @$data['content_types_id']) {
                                            $selected = 'selected=\'selected\'';
                                        }
                                        echo '<option value="' . $key . '-' . $cont['content_types_id'] . '"' . $selected . ' >&nbsp;' . $cont['display_name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="loading_div">
                            <div class="preloader pls-blue preloader-sm">
                                <svg viewBox="25 25 50 50" class="pl-circular">
                                <circle r="20" cy="50" cx="50" class="plc-path"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="formContents">
                    <?php
                    $this->renderPartial('channel_form', array("data" => @$data, 'content_types_id' => $selected_content_types_id));
                    ?>
                </div>
            </div>
        </div>
    </div>
</form>

<input type="hidden" value="" name="last_selected_content_type" id="last_selected_content_type"  readonly />
<!-- Top Banner Popup -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>


<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>
<!-- Script for Upload Trailer -->

<script type="text/javascript">
    var gmtDate = '<?php echo gmdate('d'); ?>';
    var gmtMonth = '<?php echo gmdate('m'); ?>';
    var gmtYear = '<?php echo gmdate('Y'); ?>';
    var jcrop_api;
    var bounds, boundx, boundy;
    function fileSelectHandler() {
        var reqwidth = parseInt($('#reqwidth').val());
        var reqheight = parseInt($('#reqheight').val());
        var aspectRatio = reqwidth / reqheight;
        clearInfo();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            $('#file_error').html('Please select a valid image file (jpg and png are allowed)').show();
            $("#celeb_preview").html('');
            $("#celeb_preview").html('<img id="preview"/>');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    $('#file_error').html('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight).show();
                    $("#celeb_preview").html('');
                    $("#celeb_preview").html('<img id="preview"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview').width(oImage.naturalWidth);
                $('#preview').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }

    /* TAGS INPUT PRESELECT */
    var genre = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '<?php echo $this->createUrl('admin/getTags', array('taggable_type' => 1)); ?>',
            filter: function (list) {
                return $.map(list, function (genre) {
                    return {name: genre};
                });
            }
        }
    });
    genre.clearPrefetchCache();
    genre.initialize();

    $('#genre').tagsinput({
        typeaheadjs: {
            name: 'genre',
            displayKey: 'name',
            valueKey: 'name',
            source: genre.ttAdapter()
        }
    });

    $('.edit-poster').hover(function (res) {
        $('.edit-poster').show();
    }, function () {
        $('.edit-poster').hide();
    });

    function globalPopup(popupid) {
        $('#' + popupid).modal('show');
        if (popupid == 'topbanner') {
            $('#content_id').val($('#movie_id').val());
        }

    }

    //Input Masking and Auto Complete 
    (function () {
        $("[data-mask]").inputmask();
        $("#release_date").datepicker({
            changeMonth: true,
            changeYear: true
        });

        /* Counter for Story */
        $('#story').change(updateCountdown);
        $('#story').keyup(updateCountdown);

        // Drag 
        $('#dprogress_bar').draggable({
            appendTo: 'body',
            start: function (event, ui) {
                isDraggingMedia = true;
            },
            stop: function (event, ui) {
                isDraggingMedia = false;
            }
        });
    })();

    var settout = '';

    function scrollwindow() {
        window.scrollTo(0, document.body.scrollHeight);
        clearTimeout(settout);
    }

    function updateCountdown() {
        // 140 is the max message length
        if ($.trim($('#story').val()).length > 200) {
            var story = $.trim($('.textarea').val());
            $('#story').val(story.substring(0, 200));
        }
        var remaining = 200 - $.trim($('#story').val()).length;
        $('.countdown').text(remaining + ' characters remaining.');
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function removeposter(movie_id, obj_type, movie_stream_id) {
        if (movie_id) {
            var url = "<?php echo Yii::app()->baseUrl; ?>/admin/removeposter";
            $('#remove-poster-text').text('Removing...');
            $('#remove-poster-text').attr('disabled', 'disabled');
            $.post(url, {'movie_id': movie_id, 'obj_type': obj_type, 'movie_stream_id': movie_stream_id, 'is_ajax': 1}, function (res) {

                if (res.err) {
                    $('#remove-poster-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg" style="display: block;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error in deleting poster.</div>'
                    $('body').prepend(sucmsg);
                    $('#remove-poster-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Poster removed successfully.</div>'
                    $('body').prepend(sucmsg);
                    $('#remove-poster-text').text('');
                    $('#avatar_preview_div').children('img').remove();
                    $('#avatar_preview_div').html('<img src="' + defaultposter + '" rel="tooltip" title="Upload your poster" />');
                    //$('#add_change-top-banner-text').html(" Add New Banner ");
                }
            }, 'json');
        } else {
            return false;
        }

        setTimeout(function () {
            window.location.reload(1);
        }, 1000);

    }
    /* Hide so live streaming infos based on the radio values*/
    function checkFeedType() {
        var val = $('input[name=method_type]:checked').val();
        $('.info_text').hide();
        $('.info_text').removeClass('hidden');
        $('#method_' + val).show();
        if (val == 'push') {
            $('#pull_feeds').hide();
            $('#pull_feedss').hide();
            $('#push_feeds').show();
            $('#rtmp_feed_type').prop('checked', 'checked');
            $('#feed_url').attr('pattern', 'rtmp?://.+');
            $('#feed_url').prop('placeholder', 'Enter a valid RTMP URL');
        } else {
            $('#feed_url').prop('placeholder', 'Enter a valid URL');
            $('#pull_feeds').show();
            $('#pull_feedss').show();
            $('#push_feeds').hide();
        }
    }
    function checkUrlpartern(type) {
        if (type == 2) {
            $('#feed_url').attr('pattern', 'rtmp?://.+');
        } else {
            $('#feed_url').attr('pattern', 'https?://.+');
        }
    }
    function click_browse() {
        $("#avatarInput").click();
    }
    function seepreview(obj) {
        if ($("#g_image_file_name").val() != "") {
            $(obj).html("Please Wait");
            $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
            posterpreview(obj);
        } else {
            if ($("#celeb_preview").hasClass("hide")) {
                $('#myLargeModalLabel').modal('hide');
                $(obj).html("Next");
            } else {
                $(obj).html("Please Wait");
                $('#myLargeModalLabel').modal({backdrop: 'static', keyboard: false});
                if ($("#g_image_file_name").val() != "") {
                    posterpreview(obj);
                } else if ($('#celeb_preview').is(":visible")) {
                    posterpreview(obj);
                } else {
                    $('#myLargeModalLabel').modal('hide');
                    $(obj).html("Next");
                }
            }
        }
    }
    function posterpreview(obj) {
        var formobj = document.getElementById($('form').attr('id'));
        var action_url = '<?php echo $this->createUrl('admin/posterpreview'); ?>';
        $.ajax({
            url: action_url, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(formobj), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data)  // A function to be called if request succeeds
            {
                $('#preview_content_img').attr('src', data);
                $('#myLargeModalLabel').modal('hide');
                $(obj).html("Next");
            }
        });
    }
   
</script>