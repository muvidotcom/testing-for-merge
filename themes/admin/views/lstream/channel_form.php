<div class="form-group">
    <label for="movieName" class="col-sm-4 control-label">Channel Name:</label>
    <div class="col-sm-8">
        <div class="fg-line">
            <input type='text' placeholder="Enter content name.." id="mname" name="movie[name]" value="<?php echo @$data['name'];?>" class="form-control input-sm" required >
        </div>
    </div>
</div>
<div class="form-group">
    <label for="releaseDate" class="col-sm-4 control-label">Release/Recorded Date:</label>
    <div class="col-sm-8">
        <div class="fg-line">
            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php if(@$data['release_date']){echo date('m/d/Y',strtotime($data['release_date']));}?>" class="form-control input-sm">
        </div>
    </div>
</div>
<div class="form-group">
    <label for="genre" class="col-sm-4 control-label">Genre:</label>
    <select  data-role="tagsinput" class="form-control input-sm" name="movie[genre][]" placeholder="Use Enter or Comma to add new" id="genre" multiple >
        <?php if(@$data['id'] && @$data['genre']){
                $pregenre = json_decode($data['genre']);
                foreach($pregenre AS $k =>$v){
                        echo "<option value='".$v."' selected='selected'>".$v."</option>";
                }
        }?>
    </select>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="genre" name="content_filter_type"/><i class="input-helper"></i> Show as a Filter
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="story" class="col-sm-4 control-label">Story/Description:</label>
    <div class="col-sm-8">
        <div class="fg-line">
            <textarea placeholder="Enter story..." rows="5" class="form-control input-sm textarea" name="movie[story]" id="story1"   ><?php echo @$data['story'];?></textarea>
        </div>
        <span class="countdown1"></span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-4">Method:</label>
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-6">
                <label class="radio radio-inline m-r-20">
                    <input type="radio" id="method_type_pull"  value="pull" name="method_type" checked="checked"  onchange="checkFeedType();">
                    <i class="input-helper"></i>  Pull(<small style="font-weight: normal;">You already have an existing feed</small>)
                </label>
            </div>
            <div class="col-sm-6">
                <label class="radio radio-inline m-r-20">
                    <input type="radio" id="method_type_push"  value="push" name="method_type" <?php if(@$data['feed_method']=='push'){?>checked="checked"<?php }?> onchange="checkFeedType();">
                    <i class="input-helper"></i> Push(<small style="font-weight: normal;">Point your camera to our encoder. Find your instructions <a href="https://www.muvi.com/article/live-streaming" target="_blank" >here</a></small>)
                </label>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="push" class="control-label col-sm-4" id="push_feeds" <?php if(@$data['feed_method']!='push'){?>style="display: none;"<?php }?> >Feed URL(RTMP):</label>
    <label for="pull" class="control-label col-sm-4" id="pull_feeds" <?php if(@$data['feed_method']=='push'){?>style="display: none;"<?php }?>>Feed:</label>
    <div class="col-sm-8">
        <div id="pull_feedss" <?php if(@$data['feed_method']=='push'){?>style="display: none;"<?php }?>>
        <label class="radio radio-inline m-r-20">
            <input type="radio" id="hls_feed_type"  value="1" name="feed_type" class="hls-feed-type"  checked="checked" onchange="checkUrlpartern(1);">
            <i class="input-helper"></i>HLS
        </label>
        <label class="radio radio-inline m-r-20">
            <input type="radio" id="rtmp_feed_type"  value="2" name="feed_type" <?php if(@$data['feed_type']==2){?>checked="checked"<?php }?> onchange="checkUrlpartern(2);">
            <i class="input-helper"></i> RTMP
        </label>
        </div>
         <div class="fg-line">
            <input type="text" placeholder="Feed URL" id="feed_url" value="<?php echo @$data['feed_url'];?>" class="form-control input-sm" name="feed_url" <?php if(@$data['feed_type']==2){?>pattern="rtmp?://.+"<?php }else{?>pattern="https?://.+"<?php }?>required>
        </div>
    </div>
    
   
</div>
<div class="form-group m-t-30">
    <div class="col-md-offset-4 col-md-8">
        <button type="submit" class="btn btn-primary btn-sm" disabled="disabled" id="save-btn"><?php if(@$data['id']){?>Update Content<?php }elseif($content_types_id==2){echo 'Save';}else{?>Save & Continue<?php }?></button>
    </div>
</div>    

