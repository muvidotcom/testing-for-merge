<div class="fg-line">
    <div class="select">
        <select class="filter_dropdown form-control input-sm" name="content_name" required="" id="content_name">
            <option value="">Content Title</option>
            <?php
                foreach($data as $dataKey => $dataVal){
                    $seasonId = 0;
                    $seasonText = '';
                    if(($dataVal['season'] != "") || ($dataVal['season'] != NULL)){
                        $seasonId = $dataVal['season'];
                        $seasonText = ' - Season - '.$dataVal['season'];
                    }
                    echo '<option value="'.$dataVal['id'].','.$seasonId.'">'.$dataVal['name'].$seasonText.'</option>';
                }
            ?>
        </select>	
    </div>
</div>