<div class="row m-t-40">
    <div class="col-xs-12">
        <div class="block">
            <div class="loading_div" style="display:none">
                <div class="preloader pls-blue">
                    <svg viewBox="25 25 50 50" class="pl-circular">
                    <circle r="20" cy="50" cx="50" class="plc-path"/>
                    </svg>
                </div>
            </div>  
            <form class="form-horizontal" action="javascript: void(0)" name="save_mrss_feed" id="save_mrss_feed" method="post">
            <input type="hidden" name="feed_id" id="feed_id" value="<?php if(isset($exportData['id'])){ echo $exportData['id'];} ?>">
           <div class="col-md-12">
            <div class="form-group">
                <label for="genre">MRSS Feed Title:</label>
                <div class="fg-line">
                    <input class="form-control input-sm" type="text" name="title" autocomplete="off" required="required" placeholder="MRSS Feed Title" id="title" value="<?php if(isset($exportData['title'])){ echo $exportData['title'];} ?>">
                </div>
                <small class="help-block error" id="title-error"></small>
            </div>
           </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="genre">Content list:</label>
                            <div class="fg-line">
                            <input type="text" name="filter_ajax" class="form-control input-sm" placeholder="Search Content" id="filter_ajax">
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group fg-line">
                            <div class="select">
                            <select name="available_stream" id="available_stream" class="left_stream form-control" style="width: 100%; min-height: 200px; overflow-x: scroll;" multiple="multiple">
                                <?php
                                if(count($data) > 0)
                                {
                                    foreach($data as $dataKey => $dataVal)
                                    {
                                        if($dataVal['is_episode'] == 0 && $dataVal['content_types_id'] == 3){
                                        } else{
                                            if(!in_array($dataVal['streamId'], $selectedStreamId)){
                                                echo '<option value="'.$dataVal['streamId'].'">';
                                                    if($dataVal['is_episode'] == 1){
                                                         echo $dataVal['fileName'].' Season - '.$dataVal['series_number'].' Episode - '.$dataVal['episode_number'].' '.$dataVal['episode_title'];
                                                    } else {
                                                        echo $dataVal['fileName'];
                                                    }
                                                echo '</option>';
                                            }
                                        }
                                    }
                                }
                                ?>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <div class="col-md-2 text-center">
                    <h4>&nbsp;</h4>
                    <h4>&nbsp;</h4>
                    <input type="button" class="btn btn-primary add" value=" &gt;&gt; " style="margin-bottom:5px;" /><br />
                <input type="button" class="btn btn-default remove" value=" &lt;&lt; " /> 
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                           <label for="genre">MRSS Content List:</label> 
                           <div class="fg-line">
                                <input type="text" name="filter_ajax_for_mrss" class="form-control input-sm" placeholder="Search MRSS Content" id="filter_ajax_for_mrss">
                           </div>
                       </div>
                   </div>
                    <div class="col-md-12">
                        <div class="form-group fg-line">
                            <div class="select">
                            <select id="selected_stream" name="selected_stream[]" class="right_stream form-control" required="required" multiple style="width: 100%; min-height: 200px; overflow-x: scroll;">
                                <?php
                                 if(count($data) > 0)
                                {
                                    foreach($data as $dataKey => $dataVal)
                                    {
                                        if(in_array($dataVal['streamId'], $selectedStreamId))
                                        {
                                            echo '<option value="'.$dataVal['streamId'].'">';
                                                if($dataVal['is_episode'] == 1){
                                                     echo $dataVal['fileName'].' Season - '.$dataVal['series_number'].' Episode - '.$dataVal['episode_number'].' '.$dataVal['episode_title'];
                                                } else {
                                                    echo $dataVal['fileName'];
                                                }
                                            echo '</option>';
                                        }
                                    }
                                }
                                ?>                            
                            </select> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
                
                <input type="hidden" id="selected_stream_back" name="selected_stream_back"/>
                <div class="form-group">
                    <div class="col-md-12 m-t-30">
                        <button id="sub-btn" class="btn btn-primary btn-sm" name="sub-btn"  onclick="return validateMrssFeed();">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.selso.js"></script>
<script type="text/javascript">
    $(function(){
        var selectedStreamValues = new Array();
        <?php
            foreach ($selectedStreamId as $selectedStreamIdKey => $selectedStreamIdVal){
        ?>
                selectedStreamValues.push("<?php echo $selectedStreamIdVal?>");
        <?php   
            }
        ?>
        $("#selected_stream").val(selectedStreamValues);
        $('#selected_stream_back').val($("#selected_stream").val());
        function sortBoxes(){	
            $('#selected_stream_back').val(selectedStreamValues);
                $('select.left_stream, select.right_stream').find('option').selso({
                        type: 'alpha', 
                        extract: function(o){ return $(o).text(); } 
                });

                // clear all highlighted items
                $('select.left_stream').find('option:selected').removeAttr('selected');
        }

        // add/remove buttons for combo select boxes
        $('input.add').click(function(){
                var left = $(this).parent().parent().find('select.left_stream option:selected');
                left.each(function(i){
                    selectedStreamValues.push($(this).val());
                });
                var right = $(this).parent().parent().find('select.right_stream');
                right.append(left);
                sortBoxes();	
        });

        $('input.remove').click(function(){
                var left = $(this).parent().parent().find('select.left_stream');
                var right = $(this).parent().parent().find('select.right_stream option:selected');
                right.each(function(i){
                    for (var i = selectedStreamValues.length; i--;) {
                        if (selectedStreamValues[i] === $(this).val()) {
                           selectedStreamValues.splice(i, 1);
                           break; //remove this line to remove all occurrences
                        }
                    }
                });
                left.append(right);
                var right = $(this).parent().parent().find('select.right_stream option');
                right.prop('selected', true);
                sortBoxes();
        });
        $('#filter_ajax').on('input', function(){
            var filterData = $.trim($("#filter_ajax").val());
            $('.loading_div').show();
            $('.btn').attr('disabled','disabled');
            $('.add').attr('disabled','disabled');
            $('.remove').attr('disabled','disabled');
            var streamId = $("#selected_stream_back").val();
            var url = "<?php echo Yii::app()->baseUrl; ?>/mrss/showFilterVideo";
            $.post(url,{'streamId':streamId,'filterData':filterData,'showSelected':0},function(res){
                $('#available_stream').html(res);
                $('.btn').removeAttr('disabled');
                $('.add').removeAttr('disabled');
                $('.remove').removeAttr('disabled');
                $('.loading_div').hide();
            });
        });
        $('#filter_ajax_for_mrss').on('input', function(){
            var filterData = $.trim($("#filter_ajax_for_mrss").val());
            var streamId = $("#selected_stream_back").val();
            if(streamId !== ''){
                $('.loading_div').show();
                $('.btn').attr('disabled','disabled');
                $('.add').attr('disabled','disabled');
                $('.remove').attr('disabled','disabled');
                var url = "<?php echo Yii::app()->baseUrl; ?>/mrss/showFilterVideo";
                $.post(url,{'streamId':streamId,'filterData':filterData,'showSelected':1},function(res){
                    $('#selected_stream').html(res);
                    var selectedStreamSearch = new Array();
                    $('#selected_stream option').each(function(i){
                        selectedStreamSearch.push($(this).val());
                    });
                    $('#selected_stream').val(selectedStreamSearch);
                    $('.btn').removeAttr('disabled');
                    $('.add').removeAttr('disabled');
                    $('.remove').removeAttr('disabled');
                    $('.loading_div').hide();
                });
            }
        });
    });
    function validateMrssFeed() {
    $.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9A_Z ]+$/i.test(value);
    }, "Username must contain only letters, numbers");
        $("#save_mrss_feed").validate({
            rules: {
                "title": {
                    required: true,
                    loginRegex: true
                },
                "selected_stream": {
                    required: true
                }
            },
            messages: {
                "title": {
                    required: "enter a valid title name"
                },
                "selected_stream": {
                    required: "Please select MRSS Content List"
                }
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            },    
            submitHandler: function (form) {
                var url = "<?php echo Yii::app()->getbaseUrl(true); ?>/mrss/checkTitleExist";
                var title = $('#title').val();
                var feed_id = $('#feed_id').val();
                $('.btn').attr('disabled','disabled');
                $('.form-control').attr('disabled','disabled');
                 $.post(url,{'title':title,'feed_id':feed_id},function(res){
                    if (res.succ) {
                        $('.btn').removeAttr('disabled');
                        $('.form-control').removeAttr('disabled');
                        document.save_mrss_feed.action = '<?php echo $this->createUrl('mrss/saveMrssFeed');?>';
                        document.save_mrss_feed.submit();
                    } else{
                        $('#title-error').show();
                        $('#title-error').html('MRSS Feed Title already exist.');
                        $('.btn').removeAttr('disabled');
                        $('.form-control').removeAttr('disabled');
                    }
                },'json');
            }
        });
    }
</script>