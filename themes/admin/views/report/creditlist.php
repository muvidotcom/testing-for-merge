<input type="hidden" class="data-count" value="<?php echo $details['count']?>" />
<?php
    if(isset($details['data']) && !empty($details['data'])){
        foreach($details['data'] AS $data){
?>
<tr>
    <td><?php echo Date('Y-m-d', strtotime($data['created_date']));?></td>
    <td><?php echo $data['display_name'];?></td>
    <td><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $data['user_id'];?>');"><?php echo $data['email'];?></a></td>
    <td><?php 
        if(isset($data['content_title']) && trim($data['content_title'])){
            echo $data['content_title'];
        }else if(isset($data['rule_action']) && trim($data['rule_action'])){
            echo $data['rule_action'];
        }
    ?>
    </td>
    <?php 
        if(isset($data['plan_name']) && trim($data['plan_name'])){
            echo "<td>".$data['plan_name']."</td>";
        }
    ?>
    <td><?php 
        if(isset($data['debit_value']) && trim($data['debit_value'])){
            echo $data['debit_value'];
        }else if(isset($data['credit_value']) && trim($data['credit_value'])){
            echo $data['credit_value'];
        }
    ?></td>
    
</tr>

<?php   } 
    }else{
?>
<tr>
    <td colspan="3">No records found!</td>
</tr>
<?php } ?>

