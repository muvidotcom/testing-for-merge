<input type="hidden" class="data-count" value="<?php echo $ppvbundles_subscription_count;?>" />
<thead>
    <th>Voucher</th>
    <th>User</th>
    <th data-hide="phone">Date & Time</th>
    <th data-hide="phone">Content User On</th>
</thead>
<tbody class="list">
    <?php
    
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppvbundles_subscription) && count($ppvbundles_subscription)){
            foreach ($ppvbundles_subscription as $key=>$value){
                $date = new DateTime($value['created_date']);
                $date->setTimezone(new DateTimeZone('GMT'));
    ?>
    <tr>
        <td class="amount"><?php echo $value['voucher_code'];?></td>
        <td class="email_ppv">
            <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){ ?>
                <?php echo ucfirst($value['dname']);?>
            <?php } else { 
                if($value['is_register_user'] == 1){
                ?>
            <a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email']?$value['email']:$value['dname'];?></a>
                <?php }else{
                    echo (trim($value['email']))?$value['email']:'Anonymous';
                }
            
                } ?>
            
        </td>
        <td class="date_time"><?php echo $date->format('F j, Y, g:ia'). ' GMT';?></td>
        <td class="content"><?php echo $value['content_name']?$value['content_name']:'';?></td>
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>