<div class="row m-b-40">
    <form class="form-horizontal" role="form" name="frmcustom_report" method="post" id="frmcustom_report">
    <input type="hidden" name="custom_report_id" value="<?php echo $_REQUEST['Report']?>">       
      <div class="col-xs-12 m-b-20">
         <div class="Block m-t-40 ">
            <div class="Block-Header">
               <div class="icon-OuterArea--rectangular">
                  <em class="icon-info icon left-icon "></em>
               </div>
               <h4>Create Custom Report</h4>
            </div>
            <hr>
            <div class="row">
               <div class="col-sm-8">
                  <div class="form-group">
                     <div class="col-md-4">
                        <label for="report_title">Report Title</label> 
                     </div>
                     <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="report[report_title]"  id="report_title_text" class="form-control input-sm" value="<?php echo $column_data[0]['report_title'] ?>" placeholder="Give a name to your custom report" />
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-4">
                        <label for="report_title">Report Template</label> 
                     </div>
                     <div class="col-md-8">
                        <div class="fg-line">
                           <div class="select">
                              <select class="form-control input-sm" name="report[report_template]">
                                  <option value="1" <?php if($column_data[0]['report_type']==1){ echo 'selected="selected"'; } ?> >By User</option>
                                
                              </select>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="Block m-t-40">
            <div class="Block-Header p-l-0 m-t-20 m-b-20">
               <h4>Report Columns</h4>
            </div>
            <div class="col-sm-6 p-l-0">
                <div class="border-solid padding-20">
                    <ul id="sortable" class="list-unstyled adddiv">
                        <?php 
                        if(!empty($column_data[0]['label_code'])){
                        foreach($column_data as $datas){
                            //print_r($datas);
                           ?>
                   <li class="btn-default-with-bg padding m-b-10 drag-cursor"><div class="row"><div class="col-sm-8" id="customreportreplace<?php echo $datas['id']; ?>"><?php echo $datas['label_name']; ?></div><div class="col-sm-4 text-right"><div class="row"><div  class="col-sm-4 p-r-0"><a><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a></div><div class="col-sm-4 p-r-0" id="editedcustomreport<?php echo $datas['reporttemplate_id']; ?>" data-type="<?php echo $datas['reporttemplate_id']; ?>" onclick="editcustomreportcolumn(this)"><a href="javascript:void(0)"><i class="icon-pencil h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a></div><div id="movecustomreport<?php echo $datas['id']; ?>" class="col-sm-4 text-right" data-type="<?php echo $datas['label_name']; ?>" data-idd="<?php echo $datas['id']; ?>" data-id="<?php echo $datas['id']; ?>" data-temp="<?php if($datas['studiotype']==0){ echo 'System';}else{ echo 'Custom';} ?>"  onclick="movetoravailablecolumn(this)"><a href="javascript:void(0)"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a></div></div></div></div><input type="hidden" name="colvalue[]" id="colvalue<?php echo $datas['reporttemplate_id']; ?>" value="<?php echo $datas['report_template_id']; ?>"></li>
                        <?php } }?>
                    </ul>
            </div>
            </div>
            <div class="col-sm-6 p-r-0">
               <div class="border-solid padding-20">
                  <div class="row">
                     <div class="col-sm-12"><label>Available Columns</label></div>
                     <div class="col-sm-12 m-b-20">
                         <label><button type="button" class="btn btn-primary" onclick="addnewcolumn()">New Column</button></label>
                     </div>
                     <span id="adddivtoavailable"></span>
                     <?php if(isset($template_data) && !empty($template_data)){
                         //echo count($column_data);
                            foreach($template_data as $template) {
                               $label_code=$template['label_code'];
                               $column_datacount= ReportTemplate::model()->getLabelCodeCount($label_code);
                                $column_datacount[0]['label_code'];
                               //$label_codecount= ReportTemplate::model()->getLabelCodeCount($label_code);
                               //$labelcount=$label_codecount[0]['label_code'];
                               $disable=0;
                             for($i=0;$i<count($column_data);$i++){
                                 $var=$column_data[$i]['label_code']; 
                                 if($var==$column_datacount[0]['label_code']){
                                    $disable=1; 
                                 }
                             }
                                ?>
                     <div class="col-sm-12" <?php if($disable==1){?> style="display:none;"<?php } ?> >
                           <div class="row">
                              <div class="col-sm-5">
                                 <p><?php echo $template['label_name']?></p>
                              </div>
                              <div class="col-sm-5"><?php if($template['type'] == 1){ $type='System'; }else{ $type='Custom';} echo $type;?></div>
                              <div class="col-sm-2 text-right" data-id="<?php echo $template['id'] ?>" data-type="<?php echo $template['label_name'] ?>" data-temp="<?php echo $type; ?>" onclick="movetoreportcolumn(this)"><a href="javascript:void(0)"><i class="icon-plus h4" aria-hidden="true"></i></a></div>
                           </div>
                        </div>
                     <?php }}?>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <div class="col-sm-12" align="center"><button type="button" class="btn btn-primary" onclick="validateCustomReportForm()">Save</button></div>

   </form>
</div>
<div class="modal fade" id="ppvModale" role="dialog" data-backdrop="static" data-keyboard="false" >
</div>
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal">Add New Column</span></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12" style="padding-left: 0px;">
                        <div class="col-sm-4" style="padding-left: 0px;"><input type="radio" name="available_col" value="1" onclick="static_col(this.value)" checked=""> Available Column</div>  <div class="col-sm-4"><input type="radio" name="available_col" onclick="static_col(this.value)" value="2"> Static Column</div>
                    </div>
                </div>    
                <br>
                <span id="col_available">
                <form class="form-horizontal  ppv_modal" id="custom_report_addcolumn" name="custom_report_addcolumn" method="post">
                    <input type="hidden" name="report[custom_report_id]" value="<?php echo $_REQUEST['Report']?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="content">Column Heading: </label>
                        <div class="col-sm-8"> <?php
                        
                        if(!empty($available_column)){ ?>
                         <div class="fg-line">
<div class="select">                                        
                                                                    <select class="form-control input-sm currency" name="data[col_heading]">
                                                                        <?php foreach ($available_column as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'] ?>" ><?php echo $value['label_name'];?></option>
                                                                        <?php }?>
                                                                    </select>

    
                                                                </div> 
                           
    
                        
                                    </div>
                              <?php }
                              else{?> <label style="padding-top: 7px;" id="col_heading_replace-error" class="error red" for="col_heading_replace">Columns Not available to add </label> <?php  }?>
                    </div>
                        <br><br><br>
                    <div class="modal-footer">
                        <?php if(!empty($available_column)){ ?>
                        <a href="javascript:void(0);" id="ppvbtn" onclick="validateSingleAdvForm()" class="btn btn-primary">Save</a><?php }else { ?>
                        
                            <?php } ?>
                        <button class="btn btn-default-with-bg" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
                </span>        
         <!-- Static Value-->     
         <span id="static" style="display: none;"> 
           <form class="form-horizontal  ppv_modall" id="static_custom_report_addcolumn" name="static_custom_report_addcolumn" method="post">
               <input type="hidden" name="report[custom_report_id]" value="<?php echo $_REQUEST['Report']?>">       
               <div class="form-group">
                        <div class="col-sm-12 row">    
                        <label class="col-sm-4 control-label" for="content">Column Heading: </label>
                        <div class="col-sm-8"> 
                         <div class="fg-line">
                           <input type="hidden" value="3" name="report[static]">
                           <input type="text" name="report[static_report_title]"  id="report_title_text" class="form-control input-sm" placeholder="Give a Column Heading Name" />
                          </div>
                    </div>
                         </div>
                            <br><br><br>
                             <div class="col-sm-12 row">    
                        <label class="col-sm-4 control-label" for="content">Column Value: </label>
                        <div class="col-sm-8"> 
                         <div class="fg-line">
                           <input type="text" name="report[static_report_value]"  id="report_value_text" class="form-control input-sm" placeholder="Give a Column Value" />
                                    </div>
                            
                    </div>
                         </div>
                        <br><br><br>
                    <div class="modal-footer">
                        <?php if(!empty($column_data)){ ?>
                        <a href="javascript:void(0);" id="ppvbtn" onclick="validateStaticValue()" class="btn btn-primary">Save</a><?php }else { ?>
                            <?php } ?>
                        <button class="btn btn-default-with-bg" data-dismiss="modal" type="button">Cancel</button>
                    </div>
            </div>
                  </form>
              </span>   
        </div>
    </div>
</div>

</div>

<!--scripts-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-sortable.js"></script>
<script>
      function static_col(val){
      if(val==2){
       $("#col_available").hide();  
        $("#static").show();
      }
      if(val==1){
        $("#col_available").show();
         $("#static").hide();
      }
    }
    $("#sortable").sortable({
        cancel: ".fixed"
    });
    $("#sortable").disableSelection();
    function movetoreportcolumn(obj)
    {
        var  valuee=$(obj).attr('data-type');
        var  idd=$(obj).attr('data-id');
        var  temp=$(obj).attr('data-temp');
         
       $(".adddiv").prepend('<li class="btn-default-with-bg padding m-b-10 drag-cursor"><div class="row"><div class="col-sm-8" id="customreportreplace'+idd+'">'+valuee+'</div><div class="col-sm-4 text-right"><div class="row"><div  class="col-sm-4 p-r-0"><a><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a></div><div class="col-sm-4 p-r-0" id="editedcustomreport'+idd+'" data-type="'+idd+'" onclick="editcustomreportcolumn(this)"><a href="javascript:void(0)"><i class="icon-pencil h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a></div><div id="movecustomreport'+idd+'" class="col-sm-4 text-right" data-type="'+valuee+'" data-idd="'+idd+'" data-id="'+idd+'" data-temp="'+temp+'"  onclick="movetoravailablecolumn(this)"><a href="javascript:void(0)"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a></div></div></div></div><input type="hidden" name="colvalue[]" id="colvalue'+idd+'" value="'+idd+'"></li>'); 
       $(obj).parent().parent().remove();
    }
     function movetoravailablecolumn(obj)
    {
     
       var  idd=$(obj).attr('data-idd');
       var  idmodify=$(obj).attr('data-id');
        var  temp=$(obj).attr('data-temp');
         var  moveto= $("#customreportreplace"+idd).html();
     $("#adddivtoavailable").prepend('<div class="col-sm-12"><div class="row"><div class="col-sm-5"><p>'+moveto+'</p></div><div class="col-sm-5">'+temp+'</div><div class="col-sm-2 text-right" data-type="'+moveto+'" data-id="'+idmodify+'" data-temp="'+temp+'"  onclick="movetoreportcolumn(this)" ><a href="javascript:void(0)"><i class="icon-plus h4" aria-hidden="true"></i></a></div></div></div>'); 
       $(obj).parent().parent().parent().parent().remove();
    }
    function deletetemplatecolumn(obj)
    {
        $(obj).parent().parent().parent().parent().remove();
    }
   function addnewcolumn()
    {  
    $("#ppvModal").modal('show');
    }
    
    function editcustomreportcolumn(obj)
    {  
    var  editid=$(obj).attr('data-type');
    $(obj).parent().parent().parent().children(":first").prop( "id",'customreportreplace'+editid);
     $.post("<?php echo Yii::app()->baseUrl; ?>/report/EditCustomReport",{'editid': editid},function(res){
               if(res){
                    //$("#customreporteditmodal").html(res);
                            $("#ppvModale").html(res).modal('show');
               }
                  
              })
    
   
    } 
    
      function validateSingleAdvForm() {  
        var validate = $("#custom_report_addcolumn").validate({
            rules: {
                'data[col_heading]': {
                    required: true
                },
                'data[col_value]': {
                    required: true
                }
            },
            messages: {
                'data[col_heading]': {
                    required: 'Please enter column heading',
                },
                'data[col_value]': {
                    required: 'Please enter column value',
                } 
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
      var x = validate.form();
        if (x) {
            
                var url = "<?php echo Yii::app()->baseUrl; ?>/report/AddNewColumnEdit";
                document.custom_report_addcolumn.action = url;
                document.custom_report_addcolumn.submit();
        }
    }
       function validateStaticValue() {  
        var validate = $("#static_custom_report_addcolumn").validate({
            rules: {
                'data[static_report_title]': {
                    required: true
                },
                'data[static_report_value]': {
                    required: true
                }
            },
            messages: {
                'data[static_report_title]': {
                    required: 'Please enter column heading',
                },
                'data[static_report_value]': {
                    required: 'Please enter column value',
                } 
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
      var x = validate.form();
        if (x){
                var url = "<?php echo Yii::app()->baseUrl; ?>/report/AddNewColumnEdit";
                document.static_custom_report_addcolumn.action = url;
                document.static_custom_report_addcolumn.submit();
        }
    }
      function validateCustomEditForm() {  
        var validate = $("#editcolumn").validate({
            rules: {
                'data[col_heading_replace]': {
                    required: true
                }
            },
            messages: {
                'data[col_heading_replace]': {
                    required: 'Please enter Replace Text',
                }
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
      var x = validate.form();
        if (x) {
            
            var replace_heading=$("#col_heading_replace").val(); 
            var label_code=$("#col_heading_code").val();
            var customlabel_id=$("#customlabel_id").val();
            var customlabel_name=$("#customlabel_name").val(); 
            var col_id=$("#col_id").val();
               var type=$("#type").val();
              var static_val=$("#static_val").val();
           $.post("<?php echo Yii::app()->baseUrl; ?>/report/UpdateCustomReport",{'col_id': col_id,'col_heading_replace':replace_heading,'customlabel_id':customlabel_id,'label_code':label_code,'customlabel_name':customlabel_name,'type':type,'static_val':static_val},function(res){
       
        var jsondecode=JSON.parse(res);
        var colname=jsondecode.col_name;
        var resid=jsondecode.id;
        var resname=jsondecode.lable_name;
        if(colname==1){
                      var resid=jsondecode.id;
                      var resname=jsondecode.lable_name;
                     $("#ppvModale").modal('hide');
                     $("#customreportreplace"+col_id).html(resname);
                     $( "#editedcustomreport"+col_id ).attr( "data-type",resid);
                     $( "#movecustomreport"+col_id ).attr( "data-id",resid);
                     $("#colvalue"+col_id).val(resid);
               }
               else if(colname==2)
               {
                   $("#error_heading_id").html('<div class="col-md-12"><label id="col_heading_replace-error" class="error red" for="col_heading_replace">Column Name Already Exist..</label></div>');
               }
               else
               {
                   $("#error_heading_id").html('<div class="col-md-12"><label id="col_heading_replace-error" class="error red" for="col_heading_replace">You Have already Customise  ...</label></div>');

               }
              })
            
        }
    }
    
      function validateCustomReportForm() {  
        var validate = $("#frmcustom_report").validate({
            rules: {
                'report[report_title]': {
                    required: true
                },
                'report[report_template]': {
                    required: true
                }
            },
            messages: {
                'report[report_title]': {
                    required: 'Please enter Report Title',
                },
                'report[report_template]': {
                    required: 'Please enter column value',
                } 
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
      var x = validate.form();
        if (x) {
            
                var url = "<?php echo Yii::app()->baseUrl; ?>/report/EditCustomReportUpdate";
                document.frmcustom_report.action = url;
                document.frmcustom_report.submit();
        }
    }
    
</script>