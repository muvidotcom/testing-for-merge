<style>
    .comiseo-daterangepicker-triggerbutton {
        background: #fff none repeat scroll 0 0;
        border-bottom: 1px solid #d3d3d3;
        border-top:0px !important;
        border-left:0px !important; 
        border-right:0px !important;
        border-radius: 0px !important;
        color: #333;
        font-weight: normal;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #d3d3d3;
        color: #333;
        font-weight: normal;
    }    
</style>
<?php
    $total = $data['registration']['count']/$page_size;
    $maxVisible = $total/2;
    if($maxVisible <= 5){
        $maxVisible = $total;
    }
?>
<div class="alert alert-success alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-check"></i> &nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="success-msg"></span>
</div>
<div class="alert alert-danger alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-ban"></i>&nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="error-msg"></span>
</div>
<div class="row m-b-40">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                    <div class="fg-line">
                        <div class="select">
                            <input class="form-control" type="text" id="user_date" name="searchForm[revenue_date]" value='<?php echo @$dateRange;?>' />
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-12">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <td>Registrations:</td>
                            <td id="reg"><?php echo $data['registration']['count']?$data['registration']['count']:0;?></td>
                        </tr>
                    <?php if(isset($studioPlans) && !empty($studioPlans)){?>
                        <tr>
                            <td>Subscriptions:</td>
                            <td id="sub"><?php echo $data['subscription']['count']?$data['subscription']['count']:0;?></td>
                        </tr>
                    <?php }?>
                    <?php if(!empty(array_filter($ppv_users))){?>
                        <tr>
                            <td>PPV Users:</td>
                            <td id="ppv"><?php echo $data['ppv_user']['count']?$data['ppv_user']['count']:0;?></td>
                        </tr>
                    <?php }?>
                        <tr>
                            <td>Pre-Order:</td>
                            <td id="advppv"><?php echo $data['adv_ppv']['count']?$data['adv_ppv']['count']:0;?></td>
                        </tr>
                        <tr>
                            <td colspan="2" id="loader" style="display: none;"><img src="<?php echo Yii::app()->baseUrl;?>/images/loading.gif" style="left: 30%;position: absolute;top: 20%;"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div id="userschart"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 m-t-40">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group input-group">
                    <span class="input-group-addon"><em class="icon-user"></em></span>
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm" id="users-box">
                                <option value="registrations">Registrations</option>
                            <?php if(isset($studioPlans) && !empty($studioPlans)){?>
                                <option value="subscriptions">Subscriptions</option>
                            <?php }?>
                            <?php if(!empty(array_filter($ppv_users))){?>
                                <option value="ppvusers">Ppv Users</option>
                            <?php }?>
                                <option value="advppvusers">Pre-Order Users</option>
                                <option value="cancelled">Cancellations</option>
                                <option value="deleted">Deletions</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group input-group">
                    <span class="input-group-addon"><em class="icon-magnifier"></em></span>
                    <div class="fg-line">
                        <input class="search form-control input-sm" placeholder="Search" />
                    </div>
                </div>
            </div>
        </div>
    </div>        
        <div class="col-md-12">
        <table class="table tablesorter" id="users-table-body"></table>
    </div>
</div>
<div class="h-40"></div>
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="user-profile">
        </div>
    </div>
</div>
<input type="hidden" value="1" id="loadCounter" />    
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    $(function() {
        loadDateRangePicker('user_date','<?php echo $lunchDate;?>');
    });
    
    $('#user_date').change(function(){
        var date_range = $('#user_date').val();
        var type = $('#users-box').val();
        var searchText = $.trim($(".search").val());
        getUserDetails(date_range,type,searchText);
    });
    
    $('#users-box').change(function(){
        var date_range = $('#user_date').val();
        var type = $('#users-box').val();
        var searchText = $.trim($(".search").val());
        getUserDetails(date_range,type,searchText);
    });
    
    
    $(document.body).on('keydown', '.search', function (event) {
        var searchText = $.trim($(".search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            var date_range = $('#user_date').val();
            var type = $('#users-box').val();
            getUserDetails(date_range,type,searchText);
        }
    });
    
    $('.report-dd').click(function(){
        var date_range = $('#user_date').val();
        var searchText = $.trim($(".search").val());
        var typeofsubscription=$('#users-box').val();
        var type = $(this).val();
        if(type == 'csv'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getUserReport?dt=";?>'+date_range+'&type='+type+'&search_value='+searchText+'&typeofsubscription='+typeofsubscription;
        }
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#userschart').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: <?php echo $xdata?>
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Users'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                enabled:false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: <?php echo $graphData;?>,
            credits: {
                enabled: false
            },
            exporting: { 
                enabled: false
            }
        });
    });
</script>