<style>
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}   
.table > thead > tr > th{
    
    text-transform:none;
}
</style>
<?php
    $videoTotal = $viewsDetails['count']/$page_size;
    $videoMaxVisible = $videoTotal/2;
    if($videoMaxVisible < 5){
        $videoMaxVisible = $videoTotal;
    }
?>
<div class="row m-b-10">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />

  


<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 ">
                <div class="input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                    <div class="fg-line">
                        <div class="select">
                            <input class="form-control" type="text" id="video_date" name="searchForm[video_date]" value='<?php echo @$dateRange;?>' />
                        </div>
                    </div>
                </div>
            </div>
             <?php if(isset($currency) && !empty($currency) && count($currency) > 1){?>
        <div class="col-xs-4 pull-right">
            <div class="form-group input-group">
                <span class="input-group-addon">Currency</span>
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" id="currency-box" onchange="ContentPartnerMultiCurrency(this.value)">
                            <?php foreach ($currency as $curr){?>
                                <option value="<?php echo $curr['id']?>" <?php if($curr['id'] == $default_currency){ echo 'selected=selected';}?> data-symbol = "<?php echo $curr['symbol']?>"><?php echo $curr['code']?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    <?php } ?>
         
        </div>
    </div>
   
 <!-- <div class="col-sm-4">
        <div class="form-group input-group">
            <span class="input-group-addon">Device</span>
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm" id="device-box">
                         <option value="0">All</option>
                        <option value="1">Subscriptions</option>
                        <option value="2">Pay-Per-View</option>
                    </select>
                </div>
            </div>
        </div>
    </div> 
    -->
  
</div>
 <div class="m-b-40"></div>
<div class="row">
   <div class="col-md-12">
       <table class="table table-hover" style="margin-bottom:0px;">
                    <tbody>
                        <tr style="background-color: #e9eef0;">
                            <td >
                                 
                                Total Subscriptions Revenue  : <span id="multiCurrency"> <?php $totalsubscription= round($data,2)? round($data,2) : 0; 
                            echo  Yii::app()->common->formatPrice($tot_subscriptions,$default_currency);
                            ?> </span>, Total Views:  <?php if(!empty($datatotalviewciunt)){ echo $datatotalviewciunt;}else { echo 0;} ?></td>
                            <td ></td>
                        </tr>
                    </tbody>
                </table>
            </div></div>
<div class="row"> 
    <div class="col-sm-12">
    <div class="table-responsive table-responsive-tabletAbove">
        <table class="table tablesorter" id="video-table-body"  >
            <thead>
            <th style="width: 18%;">Content Partner Report</th>
                <th style="width: 18%;">Total Views</th>
                <th>% Shared Views</th>
               
            </thead>
            <tbody class="list">
            </tbody>
        </table>
    </div>
    <div class="loader text-center" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
</div>
    </div>
<div class="h-40"></div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script src="../../common/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"--->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    $(function() {
        loadDateRangePicker('video_date','<?php echo $lunchDate;?>');
    });
    $('#video_date').change(function(){
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
        getContentPartnersReport(date_range,device_type,1);
    });
    $('#device-box').change(function(){
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
        getContentPartnersReport(date_range,device_type,0);
    });
  
    $('.report-dd').click(function(){
        var date_range = $('#video_date').val();
        var device_type = $('#device-box').val();
      var type=$(this).val();
        if(type == 'csv' || type == 'pdf'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getContentPartnerReport?dt=";?>'+date_range+'&type='+type+'&device_type='+device_type;
        }
    });

  function ContentPartnerMultiCurrency(val){
  var currency_id=val;
  $.post('/report/ContentPartnerMultiCurrency',{'currency_id':currency_id},function(res){
        $('#multiCurrency').html(res);
         //$('.loader').hide();
    });
  }
  
</script>
