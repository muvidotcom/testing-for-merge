<input type="hidden" class="data-count" value="<?php echo $userdata['count']?>" />
<thead>
    <th>Subscriber</th>
    <th data-hide="phone">TRANSACTION REF</th>
    <th>Plan</th>
    <th data-hide="phone">Last Billed Amount</th>
    <th data-hide="phone">Next Billing Date</th>
    <th data-hide="phone">Active?</th>
    <th data-hide="phone">Canceled Date</th>
</thead>
<tbody class="list">
    <?php
        $cnt = ($page- 1) * $page_size + 1;
        if(isset($subscription) && count($subscription)){
            foreach ($subscription as $key=>$value){
    ?>
    <tr>
        <td class="email">
            <?php if (isset(Yii::app()->user->id) && isset(Yii::app()->user->is_partner)){ ?>
                <?php echo ucfirst($value['dname']);?>
            <?php } else { ?>
            <a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email']?$value['email']:$value['dname'];?></a>
            <?php } ?>
        </td>
         <td class="gatewayref"><?php if($value['payment_method'] != ''){ echo (($value['payment_method'])?$value['payment_method']:'').' : '.(($value['order_number'])?$value['order_number']:'');}else { echo 'N/A';} ?></td>
        <td class="plan"><?php echo $value['name']?$value['name']:'';?></td>
        
        <td class="amount"><?php echo ($value['amount'] && $value['transaction_user_id'] != '')?Yii::app()->common->formatPrice($value['amount'],$value['currency_id']):'N/A';?></td>
        <td class="nbd"><?php echo $value['start_date']?$value['start_date']:'';?></td>
        <td class="status"><?php echo $value['status']?'Yes':'No';?></td>
        <td class="cancel_date"><?php echo $value['status']?'':$value['cancel_date'];?></td>
    </tr>
    <?php } }else{?>
    <tr>
        <td colspan="5">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>