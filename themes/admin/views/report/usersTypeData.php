<input type="hidden" class="data-count" value="<?php echo $data['count']?>" />
<?php if(isset($type) && $type == 'cancelled'){ ?>
    <thead>
        <th>User</th>
        <th>Name</th>
        <th data-hide="phone">Country</th>
        <th data-hide="phone">Address</th>
        <th data-hide="phone">Registered On</th>
        <th data-hide="phone">Canceled On</th>
        <th data-hide="phone" class="width">Cancel Reason</th>
    </thead>
    
    <tbody class="list">
        <?php 
            if(isset($data) && count($data['data'])){
                foreach($data['data'] as $value){
                    $signup_location = Yii::app()->common->getLocationFromData(trim($value['signup_location']));                    
                    $local_address = @$signup_location['city']; 
                    $local_address .= @$signup_location['region']; 
                    $local_address = trim($local_address) ? $local_address:'';
                    $country = @$signup_location['country_name'];                      
        ?>
            <tr >
                <td class="email"><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email'];?></a></td>
                <td class="name"><?php echo $value['display_name'];?></td>
                <td class="country"><?php echo $country;?></td>
                <td class="city"><?php echo $local_address;?></td>
                <td><?php echo date('F d, Y',strtotime($value['created_date']));?></td>
                <td><?php echo date('F d, Y',strtotime($value['cancel_date']));?></td>
                <td>
                    <?php if(isset($value['reason']) && trim($value['reason'])){?>
                    <span class="f-700"><?php echo $value['reason']?></span><br/>
                    <?php }
                    ?>
                    <?php echo $value['cancel_note']?>
                </td>
                <td></td>
            </tr>
        <?php }
            }else{
        ?>
            <tr>
                <td colspan="8">No Record found!!!</td>
            </tr>
        <?php }?>
    </tbody>
        
<?php }else{?>

    <thead>
        <th>User</th>
        <th data-hide="phone">Type</th>
        <th data-hide="phone">Country</th>
        <th data-hide="phone">Address</th>
        <th data-hide="phone">Registered On</th>
        <th data-hide="phone" class="width">Source</th>
        <?php if($type == 'subscriptions'){ ?>
            <th data-hide="phone" class="width">Device</th>
        <?php } ?>
    </thead>
    <tbody class="list">
        <?php 
            if(isset($data) && count($data['data'])){
                foreach($data['data'] as $value){
             if(intval($value['device_type'])){
                    switch($value['device_type']){
                     case 0:
                        $dvc_type = 'All';
                     break;
                      case 1:
                        $dvc_type = 'Web';
                     break;
                     case 2:
                        $dvc_type = 'Android';
                     break;
                     case 3:
                        $dvc_type = 'iOS';
                     break; 
                      case 4:
                        $dvc_type = 'Roku';
                     break;            
                    }
             }
                    $signup_location = Yii::app()->common->getLocationFromData(trim($value['signup_location']));                    
                    $local_address = $signup_location['city']; 
                    $local_address .= $signup_location['region']; 
                    $local_address = trim($local_address) ? $local_address:'';
                    $country = $signup_location['country_name']; 
        ?>
            <tr >
                <td class="email"><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email'];?></a></td>
                <td><?php echo (isset($value['status']) && $value['status'] == 1) ? 'Subscriber': ((isset($type) && trim($type) == 'cancelled')?'Cancelled':(isset($value['is_free']) && $value['is_free'])?'Free User':'Free registration');?></td>
                <td class="country"><?php echo $country;?></td>
                <td class="city"><?php echo $local_address;?></td>
                <td><?php echo date('F d, Y',strtotime($value['created_date']));?></td>
                <td><?php echo $value['source'];?></td>
                <?php if($type == 'subscriptions'){ ?>
                     <td><?php echo isset($value['device_type'])?$dvc_type:'N/A';?></td>
                <?php } ?>
                <td></td>
            </tr>
        <?php }
            }else{
        ?>
            <tr>
                <td colspan="8">No Record found!!!</td>
            </tr>
        <?php }?>
    </tbody>

<?php }?>
