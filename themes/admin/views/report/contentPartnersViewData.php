
<thead>
    <th style="width: 18%;">Content Partner</th>
    <th style="width: 18%;">Total Views</th> 
    <th>% Shared Views</th>
   
</thead>
<tbody class="list">
    <?php
   
        if(isset($content_data) && !empty($content_data)){
            foreach ($content_data as $contentviews){
                $viewcountt=$contentviews['viewcount']? $contentviews['viewcount'] : 0;
               ?>
                <tr>
                    <td class="content"><?php echo $contentviews['first_name']; ?>
                    </td>
                    <td ><?php echo $viewcountt; ?></td>
                      <td ><?php echo floor($viewcountt * ($contentviews['percentage_revenue_share']/100)); ?></td>
                     
                </tr>
    <?php   }
        }else{
            echo '<tr><td colspan="4">No Record found!!!</td></tr>';
        }
    ?>
</tbody>