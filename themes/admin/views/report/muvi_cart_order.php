<input type="hidden" class="data-count" value="<?php echo $ppv_subscription_count;?>" />
<thead>
    <th>ORDER #</th>
    <th data-hide="phone">TRANSACTION REF</th>
    <th data-hide="phone">EMAIL ID</th>
    <th data-hide="phone">NO OF ITEMS</th>
    <th>SUB TOTAL</th>
     <th>COUPON</th>
     <th>DISCOUNT</th>
     <th>Shipping Cost</th>
     <th>TOTAL</th>
    
</thead>
<tbody class="list">
    <?php
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppv_subscription) && count($ppv_subscription)){
            foreach ($ppv_subscription as $key=>$value){                
    ?>
    <tr>
        <td class="email_ppv"><?php echo $value['onumber']; ?></td>
        <td class="gatewayref"><?php if($value['payment_method'] != ''){ echo (($value['payment_method'])?$value['payment_method']:'').' : '.(($value['order_number'])?$value['order_number']:''); }else{ echo 'N/A'; } ?></td>
        <td class="amount"><?php echo $value['email'];?></td> 
        <td class="date_time"><?php echo $value['no_of_items'];?></td>
        <td class="revenue"><?php echo Yii::app()->common->formatPrice($value['sub_total'],$value['currency_id']);?></td>
       <td class="date_time"><?php echo ($value['coupon_code'] != '')?$value['coupon_code']:'N/A';?></td>
       <td class="date_time"><?php echo Yii::app()->common->formatPrice($value['coupon_discount'],$value['currency_id']);?></td>
       <td class="date_time"><?php echo Yii::app()->common->formatPrice($value['shipping_charge'],$value['currency_id']);?></td>
       <td class="date_time"><?php echo Yii::app()->common->formatPrice($value['grand_total'],$value['currency_id']);?></td>
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>