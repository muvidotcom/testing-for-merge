<style>
    .comiseo-daterangepicker-triggerbutton {
        background: #fff none repeat scroll 0 0;
        border-bottom: 1px solid #d3d3d3;
        border-top:0px !important;
        border-left:0px !important; 
        border-right:0px !important;
        border-radius: 0px !important;
        color: #333;
        font-weight: normal;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #d3d3d3;
        color: #333;
        font-weight: normal;
    }    
</style>

<div class="alert alert-success alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-check"></i> &nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="success-msg"></span>
</div>
<div class="alert alert-danger alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-ban"></i>&nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="error-msg"></span>
</div>
<!--<div class="row m-b-40">
    <div class="col-xs-12">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button>
    </div>
</div>-->
<div class="row m-b-40">
    <div class="col-xs-12">
        <div class="m-t-10"></div>
    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
<div class="row">
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                    <div class="fg-line">
                        <div class="select">
                            <input class="form-control" type="text" id="user_date" name="searchForm[revenue_date]" value='<?php echo @$dateRange;?>' />
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-12">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <td>Total Credits Generated:</td>
                            <td id="tot_credit"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div id="userschart"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 m-t-40">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group input-group">
                    <span class="input-group-addon"><em class="icon-magnifier"></em></span>
                    <div class="fg-line">
                        <input class="search form-control input-sm" id="search" placeholder="Search" />
                    </div>
                </div>
            </div>
        </div>
    </div>        
    <div class="col-md-12">
        <table class="table tablesorter">
            <thead>
                <th>Date</th>
                <th>User Name</th>
                <th data-hide="phone">User Email</th>
                <th data-hide="phone">Action</th>
                <th data-hide="phone">Details</th>
                <th data-hide="phone">Credits Generated</th>
            </thead>
            <tbody id="credit-table-body"></tbody>
        </table>
        <div class="page-selection-div" style="display:none">
            <div id="page-selection" class="pull-right"></div>
        </div>
        <div class="loader text-center" id="loader" style="display:none">
            <div class="preloader pls-blue">
                <svg viewBox="25 25 50 50" class="pl-circular">
                    <circle r="20" cy="50" cx="50" class="plc-path"/>
                </svg>
            </div>
        </div>
    </div>
</div>
<div class="h-40"></div>
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="user-profile">
        </div>
    </div>
</div>
    
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>

<script>
    
    function getCreditDetails (date_range, searchText) {
        $('.loader').show();
        $.post('/report/creditGenerated', {'dt': date_range, 'searchText': searchText, 'is_ajax': 1}, function (total_credit) {                             
            if(parseInt(total_credit)){
                var total_credit = total_credit;
            }else{
                var total_credit = 0;
            }
            $('#tot_credit').text(total_credit);
        });
        var url = '/report/creditgeneratedlist';
        $.post(url, {'dt': date_range, 'searchText': searchText}, function (res) {
            var regex = /\d+/g;
            var mydata = $(res).find('data-count');
            if (!isEmpty(mydata.prevObject)) {
                var count = mydata.prevObject[0].outerHTML.match(regex);
            }else{
                var count = 0;
            }
            var page_size = '<?php echo $page_size;?>';
            var total = parseInt(count) / parseInt(page_size);
            var maxVisible = total / 2;
            if (maxVisible >= 5) {
                maxVisible = 5;
            } else {
                maxVisible = total;
            }
            
            $('#credit-table-body').html(res);
            $('#loader').hide();
             if (parseInt(count) <= parseInt(page_size)) {
                $('#page-selection').parent().hide();
            } else {
                $('#page-selection').parent().show();
            }
            
            $('#page-selection').bootpag({
                total: Math.ceil(total),
                page: 1,
                maxVisible: Math.round(maxVisible)
            }).on('page', function (event, num) {
                $('#loader').show();
                $.post(url, {'page': num, 'dt': date_range, 'searchText': searchText}, function (res) {
                    $('#credit-table-body').html(res);
                    $('#loader').hide();
                });
            });
        });

        
        
    }
    
    $(function() {
        loadDateRangePicker('user_date','<?php echo $lunchDate;?>');
    });
    
    $('#user_date').change(function() {
        var date_range = $('#user_date').val();
        var searchText = $.trim($("#search").val());
        getCreditDetails(date_range, searchText);
    });
    
    $(document.body).on('keydown','#search',function (event){
        var searchText = $.trim($("#search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            var date_range = $('#user_date').val();
            getCreditDetails(date_range,searchText);
        }
    });
    
</script>


