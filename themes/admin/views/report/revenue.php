<style>
    .comiseo-daterangepicker-triggerbutton {
        background: #fff none repeat scroll 0 0;
        border-bottom: 1px solid #d3d3d3;
        border-top:0px !important;
        border-left:0px !important; 
        border-right:0px !important;
        border-radius: 0px !important;
        color: #333;
        font-weight: normal;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #d3d3d3;
        color: #333;
        font-weight: normal;
    }    
</style>
<?php
if(Yii::app()->general->getStoreLink()){
    $storeflag = 1;
}else{
    $storeflag = 0;
}
$subscriptionTotal = $subscription_count / $page_size;
$subscriptionMaxVisible = $subscriptionTotal / 2;
if ($subscriptionMaxVisible < 5) {
    $subscriptionMaxVisible = $subscriptionTotal;
}

$ppvSubscriptionTotal = $ppv_subscription_count / $page_size;
$ppvSubscriptionMaxVisible = $ppvSubscriptionTotal / 2;
if ($ppvSubscriptionMaxVisible < 5) {
    $ppvSubscriptionMaxVisible = $ppvSubscriptionTotal;
}
 $studio_id = $this->studio->id;
        $data = Yii::app()->general->monetizationMenuSetting($studio_id);
        
 ?>
  <?php if(isset(Yii::app()->user->is_partner)){ ?>
<input type="hidden" id="is_partner" value="1" />

  <?php } else{ ?>
<input type="hidden" id="is_partner" value="0" />

  <?php } ?>
<input type="hidden" id="page_size" value="<?php echo $page_size ?>" />
 <?php if ($studioPlans || intval($isPpv) || (in_array($this->studio->id,array(2663,3057))) || ($data['menu'] & 128) || ($data['menu'] & 1) || ($data['menu'] & 2) || ($data['menu'] & 16) || ($data['menu'] & 64) || ($data['menu'] & 256) || ($data['menu'] & 4)) { ?>
<div class="row m-b-40">
    <div class="col-xs-4">
        <button class="btn btn-primary primary report-dd m-t-10" value="csv">Download CSV</button> 
    </div>
    <?php if(isset($currency) && !empty($currency) && count($currency) > 1){?>
        <div class="col-xs-4 pull-right">
            <div class="form-group input-group">
                <span class="input-group-addon">Currency</span>
                <div class="fg-line">
                    <div class="select">
                        <select class="form-control input-sm" id="currency-box">
                            <?php foreach ($currency as $curr){?>
                                <option value="<?php echo $curr['id']?>" <?php if($curr['id'] == $default_currency){ echo 'selected=selected';}?> data-symbol = "<?php echo $curr['symbol']?>"><?php echo $curr['code']?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    <?php }else{?>
        <input type="hidden" id="currency-box" data-symbol = "<?php echo $currency_details->symbol?>" value="<?php echo $currency_details->id;?>"/>
    <?php }?>
</div>
 <?php }
  
 ?>
<div class="row">
    <?php if (($data['menu'] & 1) || ($data['menu'] & 2) || ($data['menu'] & 16) || ($data['menu'] & 64)|| (in_array($this->studio->id,array(2663,3057))) || ($data['menu'] & 128) || intval($storeflag) ) { ?>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon p-l-0"><i class="icon-calendar"></i></span>
                        <div class="fg-line">
                            <div class="select">
                                <input class="form-control" type="text" id="revenue_date" name="searchForm[revenue_date]" value='<?php echo @$dateRange; ?>' />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover" id="revenue">
                        <tbody>
                            <?php
                            $total_sub = 0;
                            if (isset($studioPlans) && !empty($studioPlans) && !isset(Yii::app()->user->is_partner)) {
                                foreach ($studioPlans AS $key => $val) {
                                    $total_sub += @$revenueData[preg_replace('/[^a-zA-Z]/', str_replace(' ', '', $val)) . '_' . $key];
                                    ?>
                                    <tr>
                                        <td><?php echo ucfirst($val); ?>:</td>
                                        <td id="<?php echo preg_replace('/[^a-zA-Z]/', str_replace(' ', '', $val)) . '_' . $key; ?>"><?php echo Yii::app()->common->formatPrice(@$revenueData[preg_replace('/[^a-zA-Z]/', str_replace(' ', '', $val)) . '_' . $key],$default_currency); ?></td>
                                    </tr>
                                <?php }
                            }
                            ?>
                            <?php
                            
                            if (isset($isPpv) && intval($isPpv)) {
                                $total_sub += @$revenueData['ppv'];
                               //print_r($revenueData);
                                
                               
                                ?>
                                    <tr>
                                        <td>Pay-per-view:</td>
                                        <td id="ppv"><?php echo Yii::app()->common->formatPrice(@$revenueData['ppv'],$default_currency); ?></td>
                                    </tr>
                          <?php if(!isset(Yii::app()->user->is_partner)){ ?>
                                   <tr>
                                        <td>Pay-per-view Bundles:</td>
                                        <td id="ppv_bundles"><?php echo Yii::app()->common->formatPrice(@$revenueData['ppv_bundles'],$default_currency); ?></td>
                                    </tr>
                                       
                                   
                                    
                          <?php } } ?>
                             <?php if(!in_array($this->studio->id,array(2663,3057))){?>
                                 <tr>
                                        <td>Pre-Order:</td>
                                        <td id="advppv"><?php echo Yii::app()->common->formatPrice(@$revenueData['adv_ppv'],$default_currency); ?></td>
                                    </tr>
                             <?php }?>        
                                    
                                   
                                    
                                <?php if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){?>
                                <tr>
                                    <td>Physical Goods</td>
                                    <td id="pg">
                                        <?php echo Yii::app()->common->formatPrice(@$revenueData['pg'],$default_currency); ?>
                                    </td>
                                </tr>
                                <?php }?>
                                <?php if(Yii::app()->general->getStoreLink() && !isset(Yii::app()->user->is_partner)){?>
                                <tr>
                                    <td>MuviKart Order</td>
                                    <td id="oc">
                                        <?php echo Yii::app()->common->formatPrice(@$revenueData['oc'],$default_currency); ?>
                                    </td>
                                </tr>
                                <?php }?>                                
                                <?php if (isset($studioPlans) && !empty($studioPlans) && !isset(Yii::app()->user->is_partner)) { ?>
                                <tr>
                                    <th>Recurring Payments</th>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php
                                foreach ($studioPlans AS $key => $val) {
                                    $total_sub += @$rrevenueData[str_replace(' ', '_', $val) . '_' . $key];
                                    ?>
                                    <tr>
                                        <td><?php echo ucfirst($val); ?>:</td>
                                        <td id="r_<?php echo preg_replace('/[^a-zA-Z]/', str_replace(' ', '', $val)) . '_' . $key; ?>"><?php echo Yii::app()->common->formatPrice(@$rrevenueData[preg_replace('/[^a-zA-Z]/', str_replace(' ', '', $val)) . '_' . $key]); ?></td>
                                    </tr>
                                <?php }
                            }
                            ?>
                            <tr>
                                <td colspan="2" id="loader" style="display: none;"><img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" style="left: 30%;position: absolute;top: 20%;"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8 movie-details">
            <div id="revenuechart"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tabpanel">
                <?php if(in_array($this->studio->id,array(2663,3057))){?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab4default" data-toggle="tab">Physical Items</a></li>
                </ul>
                <?php }else{?>
                <ul class="nav nav-tabs">
                    <?php if (($data['menu'] & 1) && !isset(Yii::app()->user->is_partner)) { ?>
                        <li class="active"><a href="#tab1default" data-toggle="tab">Subscription</a></li>
                       <!-- <li><a href="#tab2default" data-toggle="tab">Pay-per-view</a></li>-->
                    <?php } if (($data['menu'] & 2) && !isset(Yii::app()->user->is_partner)) { ?>
                    <li <?php if (!($data['menu'] & 1)){ ?>class="active" <?php } echo (isset($data) && ($data['menu'] & 2)) ? 'style="display:block;"' : 'style="display:none;"' ?>><a href="#tab2default" data-toggle="tab">Pay-per-view</a></li>
                    <?php } ?>
                    
                    <?php  if (($data['menu'] & 2) && isset(Yii::app()->user->is_partner)) { ?>
                    <li <?php if (($data['menu'] & 2)){ ?>class="active" <?php } echo (isset($data) && ($data['menu'] & 2)) ? 'style="display:block;"' : 'style="display:none;"' ?>><a href="#tab2default" data-toggle="tab">Pay-per-view</a></li>
                    <?php } ?>
                        <li <?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && ($data['menu'] & 16)){ echo 'class="active"'; }?> <?php echo (isset($data) && ($data['menu'] & 16)) ? 'style="display:block;"' : 'style="display:none;"' ?>><a href="#tab3default" data-toggle="tab">Pre-Order</a></li>
                     <?php if(!isset(Yii::app()->user->is_partner)){ ?>   
                        <li <?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && ($data['menu'] & 64)){ echo 'class="active"'; } ?> <?php echo (isset($data) && ($data['menu'] & 64)) ? 'style="display:block;"' : 'style="display:none;"' ?>><a href="#tab5default" data-toggle="tab">Pay-per-view Bundles</a></li>
                        <li <?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16)&& !($data['menu'] & 64) && ($data['menu'] & 128)){ echo 'class="active"'; } ?>  <?php  echo (isset($data) && ($data['menu'] & 128)) ? 'style="display:block;"' : 'style="display:none;"' ?>><a  href="#tab6default" data-toggle="tab">Voucher</a></li>
                     <?php } ?>
                    <?php if($storeflag && !isset(Yii::app()->user->is_partner)){?>
                        <li <?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && !($data['menu'] & 64) && !($data['menu'] & 128)){ echo 'class="active"'; } ?> ><a href="#tab4default"  data-toggle="tab">Physical Goods</a></li>
                    <?php }?>
                   <?php if($storeflag && !isset(Yii::app()->user->is_partner)){?>
                        <li <?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && !($data['menu'] & 64) && !($data['menu'] & 128)){ echo 'class="active"'; } ?> ><a href="#tab9default"  data-toggle="tab">MuviKart - Orders</a></li>
                    <?php }?>
                </ul>
                <?php }?>
                <div class="panel-body ">
                    <div class="tab-content">
                        <?php if(in_array($this->studio->id,array(2663,3057))){?>
                                <div class="tab-pane fade in active" id="tab4default">
                                    <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-pg" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <table class="table tablesorter" id="pg-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-pg-subscription" class="pull-right"></div>
                                        </div>
                                </div>                        
                        <?php }?>
                            <?php if (($data['menu'] & 1) && !isset(Yii::app()->user->is_partner)) { ?>
                            <div class="tab-pane fade <?php if(($data['menu'] & 1)){ echo 'in active'; } ?>" id="tab1default">
                                <?php } else { ?>
                                <div class="tab-pane fade in active" id="tab1default" style="display:none">
                                    <?php } ?>
                                    <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-sub" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div>
                                         </div>
                                    <table class="table tablesorter" id="subscription-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-subscription" class="pull-right"></div>
                                        </div>
                                </div>
                                <?php if (($data['menu'] & 2) && !isset(Yii::app()->user->is_partner)) { ?>
                                    <div class="tab-pane fade <?php if(!($data['menu'] & 1) && ($data['menu'] & 2)){ echo 'in active'; } ?>" id="tab2default">
                              <?php   }
                               else if(($data['menu'] & 2) && isset(Yii::app()->user->is_partner)){ ?>
                                <div class="tab-pane fade in active" id="tab2default">  
                            <?php   }
                                else { ?>
                                    <div class="tab-pane fade in active" id="tab2default" style="display:none">
                                <?php } ?>
                                        <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-ppv" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div></div>
                                    <table class="table tablesorter" id="ppv-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-ppv-subscription" class="pull-right"></div>
                                        </div>
                                </div>
                                
                            <!-- advance purchase tab starts-->
                            <?php if (($data['menu'] & 16) && !isset(Yii::app()->user->is_partner)) { ?>
                                <div class="tab-pane fade<?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && ($data['menu'] & 16)){ echo 'in active'; } ?>" id="tab3default">
                                     <?php   }
                                else { ?>
                                    <div class="tab-pane fade in" id="tab3default" >
                                <?php } ?>
                                    <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-adv-ppv" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <table class="table tablesorter" id="adv-ppv-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-adv-ppv-subscription" class="pull-right"></div>
                                        </div>
                                </div>
                            <!-- advance purchase tab ends-->
                            
                             <?php if (($data['menu'] & 64) && !isset(Yii::app()->user->is_partner)) { ?>
                                    <div class="tab-pane fade<?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && ($data['menu'] & 64)){ echo 'in active'; } ?>" id="tab5default">
                                <?php } else { ?>
                                    <div class="tab-pane fade in active" id="tab5default" style="display:none">
                                <?php } ?>
                                        <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-ppvbundles" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div></div>
                                    <table class="table tablesorter" id="ppvbundles-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-ppvbundles-subscription" class="pull-right"></div>
                                        </div>
                                </div>
                                        
                                        
                            <?php if (($data['menu'] & 128) && !isset(Yii::app()->user->is_partner)) { ?>
                                    <div class="tab-pane fade<?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16)&& !($data['menu'] & 64) && ($data['menu'] & 128)){ echo 'in active'; } ?>" id="tab6default">
                                <?php } else { ?>
                                    <div class="tab-pane fade in active" id="tab6default" style="display:none">
                                <?php } ?>
                                        <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-coupononly" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div></div>
                                    <table class="table tablesorter" id="coupononly-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-coupononly-subscription" class="pull-right"></div>
                                        </div>
                                </div>             
                                        
                            
                            
                            
                            
                            <?php if($storeflag && !isset(Yii::app()->user->is_partner)){?>
                            <!-- physical goods tab starts-->
                                <div class="tab-pane fade<?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && !($data['menu'] & 64) && !($data['menu'] & 128)){ echo 'in active'; } ?> " id="tab4default">
                                    <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-pg" placeholder="Search" />
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <table class="table tablesorter" id="pg-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-pg-subscription" class="pull-right"></div>
                                        </div>
                                </div>
                            <!-- physical goods tab ends-->
                            <?php }
                            if($storeflag && !isset(Yii::app()->user->is_partner)){?>
                            <!-- muvi cart order tab start -->
                                 <div class="tab-pane fade<?php if(!($data['menu'] & 1) && !($data['menu'] & 2) && !($data['menu'] & 16) && !($data['menu'] & 64) && !($data['menu'] & 128)){ echo 'in active'; } ?> " id="tab9default">
                                    <div class="row">
                                    <div class="col-md-4 p-l-0">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                                            <div class="fg-line">
                                                <input class="search form-control input-sm" id="search-cart-order" placeholder="Search" />
                            </div>
                        </div>
                    </div>
                </div>
                                    <table class="table tablesorter" id="cart-order-table">
                                    </table>
                                        <div class="page-selection-div">
                                            <div id="page-selection-pg-muvicart-order" class="pull-right"></div>
                                        </div>
                                </div>                  
                            <!-- muvi cart order tab end -->
                            <?php } ?>
                            
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-12 m-t-40">
                    <div class='' style="padding: 10px;">Oops! Sorry your studio don't have any active plan yet!</div>
                </div>	
            <?php } ?>
        </div>
    </div>
</div>
<div class="h-40"></div>
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="user-profile">
        </div>
    </div>
</div>
<input type="hidden" value="1" id="loadCounter" />    
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/list.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl;?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/daterangepicker/moment.min.js"></script>
<script src="../../common/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<!--link rel="stylesheet" href="<?php echo ASSETS_URL;?>css/jquery.bootpag.min.css"--->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/reports.js"></script>
<script>
    $(function() { 
        loadDateRangePicker('revenue_date','<?php echo $lunchDate;?>');
    });
    
    function loadDefault() {
        var date_range = $('#revenue_date').val();
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var search_order_cart = $.trim($("#search-cart-order").val());
        var ispartners = $.trim($("#is_partner").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        var searchVoucher=$.trim($("#search-coupononly").val());
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        //alert(currency_symb);
        getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners,search_order_cart);
    }
    
    $('#revenue_date').change(function(){
        loadDefault();
    });
    
    $('#currency-box').change(function(){
        loadDefault();
    });
    
    $('.report-dd').click(function(){
        var date_range = $('#revenue_date').val();
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
         var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var search_order_cart = $.trim($("#search-cart-order").val());
        var currency_id = $('#currency-box').val();
         var searchVoucher=$.trim($("#search-coupononly").val());
        var type = $(this).val();
        if(type == 'csv'){
            window.location = '<?php echo Yii::app()->baseUrl."/report/getRevenueReport?dt=";?>'+date_range+'&type='+type+'&search_sub='+searchTextSub+'&search_ppv='+searchTextPpv+'&adv_search='+searchTextAdvPpv+'&currency_id='+currency_id+'&search_pg='+searchPg+'&search_ppvbundles='+searchTextPpvbundles+'&searchVoucher='+searchVoucher+'&SearchOrderCart='+search_order_cart;
        }
    });
    
    $(document.body).on('keydown','#search-sub',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var ispartners = $.trim($("#is_partner").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextSub.length > 2 || searchTextSub.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
    $(document.body).on('keydown','#search-ppv',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var ispartners = $.trim($("#is_partner").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextPpv.length > 2 || searchTextPpv.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
    $(document.body).on('keydown','#search-adv-ppv',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var ispartners = $.trim($("#is_partner").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextAdvPpv.length > 2 || searchTextAdvPpv.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
    $(document.body).on('keydown','#search-pg',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var ispartners = $.trim($("#is_partner").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextPpvbundles.length > 2 || searchTextPpvbundles.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
    $(document.body).on('keydown','#search-ppvbundles',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var currency_id = $('#currency-box').val();
        var ispartners = $.trim($("#is_partner").val());
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextAdvPpv.length > 2 || searchTextAdvPpv.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
     $(document.body).on('keydown','#search-coupononly',function (event){
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var currency_id = $('#currency-box').val();
        var ispartners = $.trim($("#is_partner").val());
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
         var searchVoucher=$.trim($("#search-coupononly").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextAdvPpv.length > 2 || searchTextAdvPpv.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners);
        }
    });
    ////////////////search for cart order 
    $(document.body).on('keyup','#search-cart-order',function (event){
        var search_order_cart = $("#search-cart-order").val();
        var searchTextSub = $.trim($("#search-sub").val());
        var searchTextPpv = $.trim($("#search-ppv").val());
        var searchTextAdvPpv = $.trim($("#search-adv-ppv").val());
        var searchPg = $.trim($("#search-pg").val());
        var searchTextPpvbundles = $.trim($("#search-ppvbundles").val());
        var ispartners = $.trim($("#is_partner").val());
        var currency_id = $('#currency-box').val();
        var currency_symb = $('#currency-box').find(':selected').attr('data-symbol');
        if(!$.trim(currency_symb)){
             var currency_symb = $('#currency-box').attr('data-symbol');
        }
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEnter = (event.keyCode == 13);
         var searchVoucher=$.trim($("#search-coupononly").val());
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchTextPpvbundles.length > 2 || searchTextPpvbundles.length <= 0)){
            var date_range = $('#revenue_date').val();
            getRevenueDetails(date_range,searchTextSub,searchTextPpv,searchTextAdvPpv,currency_id,currency_symb,searchPg,searchTextPpvbundles,searchVoucher,ispartners,search_order_cart);
        }
    });
    
    
</script>
