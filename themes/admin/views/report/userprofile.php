<?php
$display_name = "";
$email = "";

$user_address = $userdata['signup_location'];
$display_name = $user->display_name;
$email = $user->email;
$created_date = $user->created_date;
$paid_amount = '';
if(isset($currency_info) && !empty($currency_info)){
    foreach($currency_info as $curr){
        $paid_amount .= "<span><b>";
        $paid_amount .= $curr['total_amount'] ? Yii::app()->common->formatPrice($curr['total_amount'],$curr['currency_id']):Yii::app()->common->formatPrice(0,$curr['currency_id']);
        $paid_amount .= "</b></span>, ";
    }
}
$paid_amount = rtrim($paid_amount);
$paid_amount = rtrim($paid_amount,',');
$thumb_image = $this->getProfilePicture($user->id, 'profilepicture', 'thumb');
$user_status = $userdata['user_status'];
$is_deleted = $userdata['is_deleted'];
$last_login = Date('F d, Y',strtotime($user_login_info['login_at']));
$login_count = $user_login_info['total_login'];
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">User Details</h4>
</div>
<div class="modal-body">
    <!-- Use this is-Scrollable block considering the Situation. Else ask the available Designer -->
    <div class="row is-Scrollable">
        <div class="col-sm-12">
            <!--  Row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="displayInline">
                        <div class="circle displayInline" style="vertical-align:top;padding:0;border:0;">
                            <img src="<?php echo $thumb_image?>" alt="" width="30" />
                        </div>&nbsp;&nbsp;
                        <div class="displayInline">
                            <p class="m-b-0"><b><?php echo ucfirst ($display_name);?></b></p>
                            <p class="m-b-0 light-grey font-12">
                                <?php echo $created_date ? 'User Since '.date('F d, Y',strtotime($created_date)): '';?>
                            </p>
                                
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 m-t-20">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="grey m-b-0">Email:</h5>
                            <p>
                                <?php echo $email;?>
                            </p>
                            <?php if (trim($user_address)) { ?>
                                <h5 class="grey m-b-0">Address:</h5>
                                <p>
                                    <?php echo rtrim(trim(trim($user_address),','),',');?>
                                </p>
                            <?php } ?>
                                <h5 class="grey m-b-0">Subscribed to Announcements:</h5>
                                <p>
                                <?php if(count($chkAnnoucement)>0){ ?>
                                <span class="green">Yes</span>
                                <?php }else{ ?>
                                <span class="red">No</span>
                                <?php } ?>
                                </p>
                                 <?php if ($user_status == 0 && $is_deleted == 1) { ?>
                                <h5 class="grey m-b-0">Status:</h5>
                                <p>
                                <span class="red">Deleted</span>
                                        <span>&nbsp;&nbsp;Deleted at: <?php echo date('F d, Y', strtotime($userdata['deleted_at']));?></span>
                                </p>
                                <?php }else{
                                if ($user_status == 1 && $is_deleted == 1) { ?>
                                <h5 class="grey m-b-0">Status:</h5>
                                <p>
                                        Canceled
                                        </p>
                                <?php }
                                if( $user_status == 1 && $is_deleted == 0 && $subscriptions[0]['payment_status']==0){ ?>
                                        <h5 class="grey m-b-0">Status:</h5>
                                        <p>
                                        <span class="green">Active</span>
                                        </p>
                                <?php }
                                else if($subscriptions[0]['payment_status']==2){ ?>
                                        <h5 class="grey m-b-0">Status:</h5>
                                        <p>
                                        <span class="red">Payment Pending</span>
                                        </p>
                                <?php
                                    
                                }
                                
                                }?>
                        </div>
                        <div class="col-sm-6">
                            <h5 class="grey m-b-0">Watched:</h5>
                            <h4 class="m-t-0">
                                <?php echo gmdate("H:i:s", $uservideodata['watched_time']);?> <span class="font-12">Hours</span>
                            </h4>
                            <?php if(trim($paid_amount)){?>
                                <h5 class="grey m-t-0 m-b-0">Paid:</h5>
                                <p class="m-t-0">
                                    <?php echo $paid_amount;?>
                                </p>
                            <?php }?>
                                <?php if(isset($login_count) && $login_count){?>
                                <h5 class="grey m-b-0">Login:</h5>  
                                <p class="m-t-0">
                                    <span>Total Login(s):&nbsp;</span><?php echo $login_count;?>
                                </p>
                                <p class="m-t-0">
                                    <span>Last Login:&nbsp;</span><?php echo $last_login;?>
                                </p>
                                <?php }?> 
                        </div>
                    </div>
                </div>
            </div>
            
            <?php if ($user_status == 0 && $is_deleted == 1) { }else{?>
                <div class="row">
                    <div class="col-sm-12">
                        <?php if (!empty($subscriptions)) { ?>
                        <h5 class="grey m-b-10">Subscriptions:</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Plan Name</th>
                                    <th>Started</th>
                                    <th>
                                    <?php if ($user_status == 1 && $is_deleted == 1) { ?>
                                        Cancel Date
                                        <?php } else if(!empty($subscriptions) && count($subscriptions) == 1 && $subscriptions[0]['payment_status']==0 ) { ?>
                                        Next Subscription Date
                                        <?php } else { ?>
                                        Next Subscription Date / Cancel Date
                                    <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($subscriptions as $key => $value) { ?>
                                <tr style="<?php if (intval($value['status'])) {?>color:#2CB7F6 <?php } else { ?>color:#7D7D7D<?php } ?>">
                                    <td><?php echo $value['plan_name'];?></td>
                                    <td><?php echo date('F d, Y', strtotime($value['created_date']));?></td>
                                    <?php if (intval($value['status'])) { ?>
                                    <td><?php echo date('F d, Y', strtotime($value['start_date']));?></td>
                                    <?php } else { ?>
                                    <td><?php echo date('F d, Y', strtotime($value['cancel_date']));?></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
            <?php }}?>
                        <?php
                        if(isset($custom_data) && count($custom_data) > 0){
                            foreach($custom_data as $data){
                                echo '<p><strong>'.$data['field_label'].':</strong> ';
                                $c = 0;
                                foreach($data['field_value'] as $val){
                                    if($c > 0)
                                        echo ', ';
                                    echo '<span>'.$val.'</span>';
                                    $c++;
                                }
                                echo '</p>';
                            }
                        }
                        ?>
                    </div>
                </div>
            
        </div>
    </div>
</div>
