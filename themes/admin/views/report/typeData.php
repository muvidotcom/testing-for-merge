<input type="hidden" class="data-count" value="<?php echo $data['count']?>" />
   
     <div class="col-md-4 pull-right">
            <div class="form-group">
                <div class="fg-line">
                    <input class="search pull-right form-control input-sm"  placeholder="Search" />
                </div>
             </div>
        </div>
    <table class="table" id="users-table-body tablesorter">
        <thead>
            <th>User</th>
            <th>Name</th>
            <th>Type</th>
            <th data-hide="phone">Country</th>
            <th data-hide="phone">Address</th>
            <th data-hide="phone">Registered On</th>
            <th data-hide="phone">Source</th>
        </thead>
        <tbody class="list">
            <?php 
        if(isset($data) && count($data['data'])){
            foreach($data['data'] as $value){
                $signup_location = Yii::app()->common->getLocationFromData(trim($value['signup_location']));
                $address = @$signup_location['city']; 
                $address .= @$signup_location['region']; 
                $address = trim($address) ? $address:'';
                $country = $signup_location['country_name'];
            ?>
                <tr>
                    <td class="email"><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email'];?></a></td>
                    <td class="name"><?php echo $value['display_name'];?></td>
                    <td><?php echo $value['status'] ? 'Subscriber':'Free registration';?></td>
                    <td class="country"><?php echo $country;?></td>
                    <td class="city"><?php echo $address;?></td>
                    <td><?php echo date('F d, Y',strtotime($value['created_date']));?></td>
                    <td><?php echo $value['source'];?></td> 
                </tr>
            <?php }
                }else{
            ?>
                <tr>
                    <td colspan="7">No Record found!!!</td>
                </tr>
            <?php }?>
        </tbody>
    </table>
    <?php
if($data['count'] > $page_size){
    ?>
        <div class="page-selection-div">
    <div id="page-selection-<?php echo $data['type'];?>" class="pull-right"></div>
        </div>
    <?php }?>