<style type="text/css">
    .myDragClass{
        background: #dfdfdf;
        border: 1px solid #fff;
        z-index: 9999;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jqueryui-editable.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jqueryui-editable.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="javascript:void(0);" onclick="addsubContent()">
            <button type="submit" class="btn btn-primary btn-default m-t-10"> Add Content Sub Category </button>
        </a>
        <a href="<?= Yii::app()->getBaseUrl(TRUE);?>/category/manageCategory">
            <button type="submit" class="btn btn-default-with-bg m-t-10"> Back </button>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-8">
                <div class="form-horizontal">	
                    <?php
                    if ($data) {
                        $totalContent = count($data);
                        ?>
                            <?php foreach ($data AS $key => $details) { 
                                $id = $details['id'];                                
                                $poster = $this->getPoster($id, 'content_subcategory','cmsthumb');
                                ?>
                            <div class="" id="<?php echo $id; ?>">
                                <div class="<?php if(@$cat_img_option && ($poster !="")){}else{echo "m-b-10";}?>">
                                <div class="row">
                                    <div class="col-md-5">
                                        <span class="edit" data-id="<?php echo $id; ?>"><?php echo $details ['subcat_name']; ?></span>
                                        <form class="editform" style="display: none;" id="frm<?php echo $id; ?>" action="javascript:void(0);" method='post'>
                                            <input type="text" name="d<?php echo $id; ?>" id="d<?php echo $id; ?>" value="<?php echo $details['subcat_name']; ?>">
                                            <input type="hidden" id="cid<?php echo $id; ?>" value="<?php echo $details['id']; ?>">
                                            <input type="hidden" id="sid<?php echo $id; ?>" value="<?php echo $details['category_value']; ?>">
                                        </form>                                        
                                    </div>
                                    <div class="col-md-3">
                                        Category : <?= Yii::app()->Helper->categoryNameFromCommavalues($details['category_value'],$contentList);?>
                                    </div>
                                    <?php if(@$cat_img_option){
                                        if($poster !=""){ ?>
                                    <div class="Box-Container-width-Modified col-md-3">
                                        <div class="Preview-Block">
                                            <div class="thumbnail">
                                                <div class="relative m-b-10"> 
                                                    <img src="<?php echo $poster; ?>" alt="<?= $details ['subcat_name']; ?>" title="<?= $details ['subcat_name']; ?>" style="max-width:100px;max-height:100px;"> <!--This is your Image overlay if exists --> 
                                                    <div class="caption overlay overlay-white"> 
                                                        <div class="overlay-Text"> 
                                                            <div> 
                                                                <a href="javascript:void(0);" onclick="editsubContentType(this);" data-permalink="<?= $details ['permalink']; ?>" data-id="<?= $id;?>"><span class="btn btn-primary icon-with-fixed-width"> <em class="fa fa-upload"></em> </span> </a> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                        <?php } 
                                    } ?>
                                    <div class="col-md-1">
                                        <?php if ($totalContent > 1) { ?>
                                            <a href="javascript:void(0);"  subcategory_id="<?php echo $details['id']; ?>"  onclick="showConfirmPopup(this);" title="Remove Category"> <em class="icon-trash"></em></a>
                                            <?php 
                                        }?>
                                    </div>
                                </div>
                                    </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        No content type found!
                    <?php } ?>
                </div>
            </div>
        </div>
								</div>
							</div> 

<form action="javascript:void(0);"  method="post" class="form-horizontal" data-toggle="validator" id="addContentForm" enctype="multipart/form-data">
<div id="addContentType" class="modal fade login-popu" data-backdrop="static" data-keyboard="false"></div>
<?php if(@$cat_img_option){ ?>
<div class="modal is-Large-Modal fade" id="category_img" tabindex="-1" role="dialog" aria-labelledby="category_img">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="section_id1" id="section_id1" value="" />
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" onclick="hide_file()">
                                <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                            </li>
                            <li role="presentation" onclick="hide_gallery()"> 
                                <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                        <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('browse_cat_img')">
                                        <input id="browse_cat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                        <p class="help-block"></p>
                                    </div>
                                    <input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
                                    <input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
                                    <input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
                                    <input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
                                    <input type="hidden" class="w" id="w" name="fileimage[w]"/>
                                    <input type="hidden" class="h" id="h" name="fileimage[h]"/>
                                    <div class="col-xs-12">
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="upload_preview">
                                                <img id="preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                                <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                <input type="hidden" name="g_original_image" id="g_original_image" />
                                <input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
                                <input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
                                <input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
                                <input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
                                <input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
                                <input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

                                    </div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                        <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                            <div class="preloader pls-blue  ">
                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="gallery_preview">
                                                <img id="glry_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
                </div>
            </div>
        </div>
    </div>    
    <input type="hidden" id="img_width" name="img_width" value="">
    <input type="hidden" id="img_height" name="img_height" value="">
<?php } ?>  
</form>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">
	function showConfirmPopup(obj) {
            swal({
                title: "Remove Sub Category?",
                text: "Are you sure you want to remove the sub category from Studio?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html:true
            },
            function(){
                removeContent(obj);
            });
           
	}

	function removeContent(obj) {
		var subcategory_id = $(obj).attr('subcategory_id');
		if (parseInt(subcategory_id)) {
			var url = HTTP_ROOT + "/category/removeContentsubCategory/subcategory_id/" + subcategory_id;
			window.location.href = url;
		} else {
			return false;
		}
	}
	function addsubContent(){
            var url ="<?= Yii::app()->getBaseUrl(TRUE);?>/category/ajaxsubContent";
            $.post(url,{},function(res){
				$("#addContentType").html(res).modal('show');
            });
	}
	
	function editsubContentType(obj){
		var content_id = $(obj).attr('data-id');
		var url ="<?= Yii::app()->getBaseUrl(TRUE);?>/category/ajaxSubContent";
		$.post(url,{'content_id': content_id},function(res){
			//$("#addContentType").html(res).modal('show');		
                        $("#addContentType").html(res);
                        var obj = $('#cat_img');
                        openImageModal(obj);
		});
	}
	
	function addSubContentType(){
		$('#cerror').hide();
		$.validator.addMethod("loginRegex", function(value, element) {
			return this.optional(element) || /[a-zA-Z]{1}/i.test(value);
		}, "Sub Catagory name must contain an alphabate");
      
		//form validation rules
		var validate =  $("#addContentForm").validate({
			rules: {
				displayname: {
					required:true,
					loginRegex:true,
					maxlength:50
				}
			   }, 

			messages: {
				displayname: {
					required:"Please enter a valid category name"

				}
			},
			errorPlacement: function(error, element) {
				error.addClass('red');
				error.insertAfter(element.parent());
			},

		});
		var x = validate.form();
		if(x){
			$('#add_btn').html('loading...');
			$('#add_btn').attr('disabled','disabled');
			var category_id = $('#sub_category_id').val();
			var url ='';
			if (category_id) {
				url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/updateSubContentCategory";
			}else{ 
				url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/addContentSubCategory";
			}
			var formData = new FormData($('#addContentForm')[0]);
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				dataType: 'json',
				success: function (res) {
					if(res.succ){	
						$('#addContentType').modal('hide');
						window.location.reload();
					}else{
						$('#add_btn').removeAttr('disabled');
						$('#add_btn').html('submit');
						$('#cerror').show();
						if(res.msg){
							$('#cerror').html(res.msg);
						}else{
							$('#cerror').html('Oops! Sorry you have already added this Subcategory!');
						}
					}
				}
			});
		}
	}
    function openImageModal(obj) {
        var width = $(obj).attr('data-width');
        var height = $(obj).attr('data-height');
        $(".help-block").html("Upload a transparent image of size " + width + " x " + height+'px');
        $("#img_width").val(width);
        $("#img_height").val(height);
        $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
        $("#category_img").modal('show');
    }
    function click_browse(modal_file) {
        $("#" + modal_file).click();
    }
    function fileSelectHandler() {
        document.getElementById("g_original_image").value = "";
        document.getElementById("g_image_file_name").value = "";
        var img_width = $("#img_width").val();
        var img_height = $("#img_height").val();
        $(".jcrop-keymgr").css("display", "none");
        $("#celeb_preview").removeClass("hide");
        $('#uplad_buton').removeAttr('disabled');
        var oFile = $('#browse_cat_img')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#celeb_preview").addClass("hide");
            document.getElementById("browse_cat_img").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var aspectratio = img_width / img_height;
        var img = new Image();
        img.src = window.URL.createObjectURL(oFile);
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < img_width || height < img_height) {
                swal('You have selected small file, please select one bigger image file');
                $("#celeb_preview").addClass("hide");
                document.getElementById("browse_cat_img").value = "";
                $('#uplad_buton').attr('disabled', 'disabled');
                return;
            }
            var oImage = document.getElementById('preview');
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                oImage.src = e.target.result;
                oImage.onload = function () { // onload event handler
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                        $('#glry_preview').width("450");
                        $('#glry_preview').height("250");
                    }
                    $('#preview').css("display", "block");
                    $('#celeb_preview').css("display", "block");
                    $('#preview').Jcrop({
                        minSize: [img_width, img_height], // min crop size
                        aspectRatio: aspectratio, // keep aspect ratio 1:1
                        boxWidth: 450,
                        boxHeight: 250,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function () {
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        jcrop_api = this;
                        jcrop_api.setSelect([10, 10, img_width, img_height]);
                    });
                };
            };
            oReader.readAsDataURL(oFile);
        };
    }
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image)
{
    $('#glry_preview').css("display", "block");
    document.getElementById("browse_cat_img").value = "";
    showLoader();
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    var aspectratio = img_width / img_height;
    var image_file_name = name_of_image;
    var image_src = img_src;
    clearInfo();
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            showLoader(1);
            swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
            $("#celeb_preview").addClass("hide");
            $("glry_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("450");
                $('#preview').height("250");
            }
            $("#glry_preview").css("display", "block");
            $('#gallery_preview').css("display", "block");
            $('#glry_preview').Jcrop({
                minSize: [img_width, img_height], // min crop size
                aspectRatio: aspectratio, // keep aspect ratio 1:1
                boxWidth: 450,
                boxHeight: 250,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, img_width, img_height]);
            });
        };
    };

}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#celeb_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('browse_cat_img').value = null;
}
function hide_gallery()
{
    $('#preview').css("display", "none");
    $("#glry_preview").css("display", "block");
    $('#gallery_preview').css("display", "none");
    $("#g_image_file_name").val("");
    $("#g_original_image").val("");
}
function seepreview(obj) {
    if ($('#g_image_file_name').val() == '')
        curSel = 'upload_by_browse';
    else
        curSel = 'upload_from_gallery';
    if ($('#'+curSel).find(".x13").val() != "") {
        $(obj).html("Please Wait");
        $('#category_img').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj, curSel);
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#category_img').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#category_img').modal({backdrop: 'static', keyboard: false});
            if ($('#'+curSel).find(".x13").val() != "") {
                posterpreview(obj, curSel);
            } else if ($('#'+curSel).find('.x1').val() != "") {
                posterpreview(obj, curSel);
            } else {
                $('#category_img').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function posterpreview(obj, curSel) {
    $("#previewcanvas").show();
    var canvaswidth = $("#img_width").val();
    var canvasheight = $("#img_height").val();
    var x1 = $('#'+curSel).find('.x1').eq(0).val();
    var y1 = $('#'+curSel).find('.y1').eq(0).val();
    var width  = $('#'+curSel).find('.w').val();
    var height = $('#'+curSel).find('.h').val();
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function() {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        //$('#imgCropped').val(canvas.toDataURL());
    };
    $("#avatar_preview_div").hide();
    if ($('#g_image_file_name').val() == '') {
        img.src = $('#preview').attr("src");
    } else {
        img.src = $('#glry_preview').attr("src");
    }
    //$('#category_img').modal('hide');
    //$(obj).html("Next");
    $('button').attr('disabled', 'disabled');
    addSubContentType();//submit the form
}
$.fn.editable.defaults.mode = 'inline';
    $(function(){
        $('.edit').editable({
            type: 'text',
            validate:function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }
                if($.trim(value).length > 15){
                    return 'Please enter no more than 15 characters.';
                }
                if(!validateCatname($.trim(value))){
                    return 'Subcatagory name must be contain alphabate';
                }
            },
            url: function(params) {
                var displayname    = params.value;
                var category_id    = $('#cid'+$(this).data('id')).val();
                var category_value = $('#sid'+$(this).data('id')).val();
                var url ='';
                if (category_id) {
                    url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/updateSubContentCategory";
                } else {
                    url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/addContentSubCategory";
                }
                return $.post(url,{'displayname':displayname, 'sub_category_id':category_id,'category_value':category_value},function(res){
                },'json');
            },
            success: function (res) {
                if(res.succ){
                    //window.location.reload();
                }else{
                    if(res.msg){
                        return res.msg;
                    }else{
                        return 'Oops! Sorry you have already added this subcategory!';
                    }
                }
            }
        });
    }); 
    function validateCatname(nm) {
        var re = /[a-zA-Z]{1}/i;
        return re.test(nm);
    }
</script>