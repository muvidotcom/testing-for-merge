<style type="text/css">
    body {overflow-x: visible;}//add due to chrome and safari sortable jump issue
    .myDragClass{
        background: #dfdfdf;
        border: 1px solid #fff;
        z-index: 9999;
    }
    .overlay-white{
        background: rgba(255, 255, 255, 0.7);
    }
    #sortableform .editable-container.editable-inline{float: left;width: 33.3% !important;padding: 0 15px;}
    #sortableform .editable-input :nth-child(1){width: 160px;}
    #sortableform .editable-buttons{display: block;margin-top: 7px;margin-left:0px !important;}
    .loading_div{display: none;}    
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jqueryui-editable.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jqueryui-editable.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <?php if($cmfdata){?>
        Edit Content Format
        <?php }else{?>
        Add a new Content Format & Define its Related  Properties.
        <?php }?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-12">
        <form action="javascript:void(0);" method="post" id="customparentform">
            <div class="col-sm-12 m-b-20">
                <div class="col-sm-8 p-l-0">
                <div class="row m-t-20">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Content Format Name</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type='text' name="formfield[name]" class="form-control input-sm checkInput" placeholder="Enter Content Format Name" value="<?= @$cmfdata['name'];?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $content = Yii::app()->general->content_count($studio_id);
                    $content_child = Yii::app()->general->content_count_child($studio_id);
                    if(empty($content)){$content['content_count']=1;}
                    if(empty($content_child)){$content_child['content_count']=3;}
                    if(Yii::app()->general->getStoreLink()){
                        $content['content_count'] = $content['content_count']+16;//16 is binary value of muvikart see "parent_content_type" table
                    }
                    ?>
                <div class="row m-t-20">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Content Type</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <?php if($cmfdata){?>
                                    <?php $c_type = CHtml::listData($parent_content_type_data, 'id', 'name');
                                    echo $c_type[$cmfdata['parent_content_type_id']];?>
                                <input type="hidden" value="<?=$cmfdata['parent_content_type_id'];?>" id="parent_content_type_id" name="formfield[parent_content_type_id]">
                                <?php }else{?>
                                    <div class="select">
                                        <select class="form-control input-sm" onchange="getCustomForm()" name="formfield[parent_content_type_id]" id="parent_content_type_id">
                                    <?php foreach ($parent_content_type_data as $key => $value) {
                                            if(@$cmfdata['parent_content_type_id'] == $value['id']){$selected="selected";}else{$selected="";}
                                            if((int)$value['binary_value'] & (int)$content['content_count']){
                                                ?><option value="<?= $value['id'];?>" <?=$selected;?>><?= $value['name'];?></option><?php                                       
                                            }
                                        }
                                    ?>
                                        </select>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20" id="content_structure_div">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Content Structure</label>
                        <div class="col-sm-8">
                            <?php if($cmfdata){
                                if($cmfdata['content_type']==0){echo "Single Part";}
                                elseif($cmfdata['content_type']==1){echo "Multi Part";}
                            ?>
                            <input name="formfield[content_type]" value="<?=$cmfdata['content_type'];?>" type="radio" style="display: none;" checked="checked">
                            <?php }else{
                                $vodsingle = $aodsingle = $vodmulti = $aodmulti = 0;
                                if(((int)$content['content_count'] & 1) && ((int)$content_child['content_count'] & 1)){
                                    $vodsingle = 1;
                                }
                                if(((int)$content['content_count'] & 4) && ((int)$content_child['content_count'] & 8)){
                                    $aodsingle = 1;
                                }
                                if(((int)$content['content_count'] & 1) && ((int)$content_child['content_count'] & 4)){
                                    $vodmulti = 1;
                                }
                                if(((int)$content['content_count'] & 4) && ((int)$content_child['content_count'] & 16)){
                                    $aodmulti = 1;
                                }
                            ?>
                            <input type="hidden" id="vodsingle" value="<?=$vodsingle?>">
                            <input type="hidden" id="aodsingle" value="<?=$aodsingle?>">
                            <input type="hidden" id="vodmulti" value="<?=$vodmulti?>">
                            <input type="hidden" id="aodmulti" value="<?=$aodmulti?>">
                            
                            <div class="radio">
                                <?php if($vodsingle || $aodsingle){?>
                                <label id="sp">
                                    <input name="formfield[content_type]" value="0" type="radio" onclick="showlevel(0);" <?php if(empty($cmfdata) || ($cmfdata['content_type']==0)){echo 'checked="checked"';}?>>
                                    <i class="input-helper"></i>
                                    Single Part
                                </label>&nbsp;&nbsp;
                                <?php }if($vodmulti || $aodmulti){?>
                                <label id="mp">
                                    <input name="formfield[content_type]" value="1" type="radio" onclick="showlevel(1);">
                                    <i class="input-helper"></i>
                                    Multi Part
                                </label>
                                <?php }?>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20" <?php if($cmfdata['is_child']){}else{?>style="display: none;"<?php }?> id="st_level">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Structure Level</label>
                        <div class="col-sm-8">
                            <?php if($cmfdata){
                                if($cmfdata['is_child']==0){echo "Parent";}
                                elseif($cmfdata['is_child']==1){echo "Child";}
                            ?>
                            <input name="formfield[is_child]" value="<?=$cmfdata['is_child'];?>" type="radio" style="display: none;" checked="checked">
                            <?php }else{?>
                            <div class="radio">
                                <label>
                                    <input checked="checked" name="formfield[is_child]" value="0" type="radio" onclick="getCustomForm();" <?php if(empty($cmfdata) || ($cmfdata['is_child']==0)){echo 'checked="checked"';}?>>
                                    <i class="input-helper"></i>
                                    Parent
                                </label>&nbsp;&nbsp;
                                <label>
                                    <input name="formfield[is_child]" value="1" type="radio" onclick="getCustomForm();" <?php if($cmfdata['is_child']==1){echo 'checked="checked"';}?>>
                                    <i class="input-helper"></i>
                                    Child
                                </label>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Poster Size</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input name="formfield[poster_size]" placeholder="Enter poster width and height eg . 280x400" class="form-control input-sm" type="text" value="<?= @$cmfdata['poster_size'];?>">
                                <input type="hidden" id="editid" name="editid" value="<?=$cmfdata['id'];?>">
                            </div>
                        </div>
                    </div>
                </div>                
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="content_metadata_div">
                    <?php 
                    //$this->renderPartial('content_metadata', array('column_data'=>$column_data,'template_data'=>$template_data,'defaultFields'=>$defaultFields,'formid'=>$formid,'parent_content_type_data' => $parent_content_type_data));
                    ?>
                </div>
            </div>
        <!--removed form end tag-->
    </div>
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<script type="text/javascript">
	var uniquexhr;
    $.fn.editable.defaults.mode = 'inline';
    $(document).ready(function(){
        getCustomForm();
		jQuery.validator.addMethod("dimension", function (value, element) {
            return this.optional(element) || /^[0-9]{1,4}x[0-9]{1,4}$/i.test(value);
        }, "Dimension must be in 280x400 format in px.");
    });
    $(function(){
        InlineEditName();
    });
    function validateCatname(nm) {
        var re = /[a-zA-Z]{1}/i;
        return re.test(nm);
    }
    $("#sortableform").sortable({
        //cancel: ".fixed"
    });
    function InlineEditName(){
        $('.editdisplayname').editable({
            type: 'text',
            validate:function(value) {
                if($.trim(value) == '') {
                    return 'This field is required';
                }                
                if(!validateCatname($.trim(value))){
                    return 'Name must be contain alphabate';
                }
            },
            url: function(params) {
                var d = $(this).attr('data-displayname');
                var displayname = params.value;
                $('input[name="'+d+'"]').val(displayname);
                //$(this).parent().children().find("div[id^='movecustomreport']").attr('data-name',displayname);
                return displayname;
            }
        });
    }
    function showlevel(flag){
        if(flag){$('#st_level').show('slow');}else{$('#st_level').hide('slow');}
        getCustomForm();
    }   
    function getCustomForm(){
        var parent_content_type_id = $('#parent_content_type_id').val();
        if((parent_content_type_id == 2) || (parent_content_type_id == 4) || (parent_content_type_id == 5)){//live stream and muvikart
            $('#content_structure_div').hide('slow');
            $('#st_level').hide('slow');
            if($('input[name="formfield[content_type]"]').length > 0){
                document.querySelectorAll('[name="formfield[content_type]"]')[0].checked = true;
                document.querySelectorAll('[name="formfield[is_child]"]')[0].checked = true;
            }
        }else{
            if(((parent_content_type_id == 1) && $('#vodmulti').val() == 1) || ((parent_content_type_id == 3) && $('#aodmulti').val() == 1)){
                $('#mp').show();
            }else{
                $('#mp').hide();
            }
            if(((parent_content_type_id == 1) && $('#vodsingle').val() == 1) || ((parent_content_type_id == 3) && $('#aodsingle').val() == 1)){
                $('#sp').show();
            }else{
                $('#sp').hide();
            }
            if((parent_content_type_id == 1) && $('#vodsingle').val() == 0){
                document.querySelectorAll('[name="formfield[content_type]"]')[1].checked = true;
            }else if((parent_content_type_id == 3) && $('#aodsingle').val() == 0){
                document.querySelectorAll('[name="formfield[content_type]"]')[1].checked = true;
            }
            
            $('#content_structure_div').show('slow');
        }
        var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
        if(is_multipart==1){
            $('#st_level').show();
        }else{
            is_multipart = 0;
            $('#st_level').hide();
        }
        var is_child = $('input[name="formfield[is_child]"]:checked').val();
        var editid = $('#editid').val();
        $('.loading_div').show();
        $('#sortableform').css({'opacity': '0.3'});
        $("#customparentform :button").attr("disabled", "disabled");
        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/category/GetCustomForm";
        $.post(url, {'formid': parent_content_type_id,'is_multipart':is_multipart,'is_child':is_child,'editid':editid}, function (data) {
            $('#content_metadata_div').html(data);
            $( "#sortableform" ).sortable();
            InlineEditName();
            $('.loading_div').hide();
            $('#sortableform').css({'opacity': '1'});
            $("#customparentform :button").removeAttr("disabled");
        });
    }    
    function SaveContentType(obj){
        var validate =  $("#customparentform").validate({
            rules: {
                "formfield[name]": {required:true},
				"formfield[poster_size]": {dimension: true}
               },                   
            messages: {
                "formfield[name]": {required:"Please enter a name"}
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            },                
        });
        var x = validate.form();
        if(x){
            $(obj).attr('disabled','disabled');
            var url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/SaveCustomeForm";
            $('#customparentform').attr('action',url);
            //var customfields_serialize = $("#metadatafieldform").serialize();
            //$('#customparentform').append("<input type='hidden' name='customfields_serialize' value='"+customfields_serialize+"' />");
            $('#customparentform').submit();         
        }        
    }
    function openinmodal(url) {
        var parent_content_type_id = $('#parent_content_type_id').val();
        var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
        var is_child = $('input[name="formfield[is_child]"]:checked').val();   
        $.post(url, {"parent_content_type_id": parent_content_type_id,'is_multipart':is_multipart,'is_child':is_child}, function (data) {
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function movetoformcolumn(obj){
        var id=$(obj).attr('data-id');
        var type=$(obj).attr('data-type');
        var name= $(obj).attr('data-name');
        var moveto = $("#customfieldreplace"+id).html();
        //alert(moveto);
        //alert(id+"=="+type+"=="+name+"=="+moveto);
        //return false;
        $(".adddiv").append(moveto);
        $(obj).parent().parent().remove();
        InlineEditName();
    }
    function movetofieldcolumn(obj,flag){
        var id=$(obj).attr('data-id');
        var type=$(obj).attr('data-type');
        var name= $(obj).attr('data-name');
        var tempid=$(obj).attr('data-tempid');
        var moveto = $("#customreportreplace"+id).html();
        var customflag = $("#customreportreplace"+id+" > .cfield").val();
        if (customflag == "custom") {
            var editdelete = '<div class="col-sm-1 text-right" data-id="'+tempid+'" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4" aria-hidden="true"></i></a></div><div class="col-sm-1 text-right" data-id="'+tempid+'" onclick="deletefield(this)"><a href="javascript:void(0)" title="Delete field"><i class="icon-close h4" aria-hidden="true"></i></a></div>';
        }else if((type=='List' || type=='Dropdown') && (flag!=1)){
            var editdelete = '<div class="col-sm-1 text-right" data-id="'+tempid+'" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4" aria-hidden="true"></i></a></div>';
        }else{
            var editdelete = '';
        }
        $("#adddivtoavailable").append('<div class="col-sm-12"><div class="row"><div class="col-sm-5"><p>'+name+'</p></div><div class="col-sm-4">'+type+'<div style="display: none;" id="customfieldreplace'+id+'"><li class="m-t-20" id="customreportreplace'+id+'">'+moveto+'</li></div></div><div class="col-sm-1 text-right" data-type="'+type+'" data-id="'+id+'" data-name="'+name+'"  onclick="movetoformcolumn(this)" ><a href="javascript:void(0)" title="Add Field"><i class="icon-plus h4" aria-hidden="true"></i></a></div>'+editdelete+'</div></div>'); 
        $(obj).parent().parent().parent().parent().remove();
    }    
    function savenewfield(obj){
        var validate =  $("#addnewfieldform").validate({
            rules: {
                "data[field_name]": {required:true},
                "data[field_type]": {required:true}
               },                   
            messages: {
                "data[field_name]": {required:"Please enter a name"},
                "data[field_type]": {required:"Please select field type"}
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            },                
        });
        var x = validate.form();
        var formData = $('#addnewfieldform').serialize();
        if(x){
            $(obj).attr('disabled','disabled');
            var url = "<?php echo Yii::app()->getBaseUrl(true) ?>/category/SaveNewField";
            $.post(url, {"formData":formData}, function (res) {
                if(res){
                    $('#success_msg').html('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;'+res+'</div>');
                    getCustomForm();
                }else{
                    $('#success_msg').html('<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error occured please try again.</div>');
                }
                $("#mymodal").modal('hide');
            });
            //document.addnewfieldform.action = url;
            //document.addnewfieldform.submit();            
        }        
    }
    function updateUniqueId(val1){
        var url = HTTP_ROOT + "/category/CheckUniqueFieldName";
		if(uniquexhr && uniquexhr.readyState != 4){
			uniquexhr.abort();
		}
        uniquexhr = $.ajax({
            url: url,
            type: "POST",
            data: {'fieldname' : val1.value},
            success: function (res) {
                $('#f_id').val(res);
                $('#uniq_value').html(res);
            }
        });            
    }
    function choosevalue(obj){
		var fvalue = $(obj).val();
        if(fvalue > 1 && (fvalue!=4)){
            $('#fieldtypevalue').html($(obj).find(':selected').data('value'));
            $('.fieldvalue').show();
        }else{
            $('.fieldvalue').hide();
        }
    }
    function addmorefield(){
        var length = $('.addmoretext').length;
        var div = '<div class="form-group fieldvalue"><label class="col-sm-4 toper control-label" for="Role">&nbsp;</label><div class="col-sm-7"><div class="fg-line"><input type="text" class="form-control input-sm addmoretext" name="data[f_value]['+length+']" autocomplete="off" required="true"/></div></div><div class="col-sm-1 m-t-10"><a href="javascript:void(0);" onclick="removefield(this);"><em class="fa fa-minus"></em></a></div><div>';
        $('.fieldvalue').last().after(div);
    }
    function editfield(obj){
        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/category/AddNewField";
        var parent_content_type_id = $('#parent_content_type_id').val();
        var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
        var is_child = $('input[name="formfield[is_child]"]:checked').val(); 
        $.post(url, {'id': $(obj).attr('data-id'),"parent_content_type_id": parent_content_type_id,'is_multipart':is_multipart,'is_child':is_child}, function (data) {
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function deletefield(obj){
        swal({
            title: "Remove metadata field?",
            text: "It will be removed from all associative content forms",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        },
        function(){
            var url ="<?= Yii::app()->getBaseUrl(TRUE);?>/category/RemoveCustomField";
            $.post(url,{'id': $(obj).attr('data-id')},function(res){
                if(res){$(obj).parent().parent().remove();}
            });
        });
    }
    function removefield(obj){
        $(obj).parent().parent().remove();
    }
	function changeLangCustom(lang_code) {
		var url = "<?php echo Yii::app()->getBaseUrl(); ?>/category/AddNewField";
		var parent_content_type_id = $('#parent_content_type_id').val();
		var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
		var is_child = $('input[name="formfield[is_child]"]:checked').val();
		var id = $('input[name="data[edit_id]"]').val(); 
		$.post(url, {'id': id,'lang_code':lang_code,"parent_content_type_id": parent_content_type_id,'is_multipart':is_multipart,'is_child':is_child}, function (data) {
			$('#mymodaldiv').html(data);
			$("#mymodal").modal('show');
		});
	}
</script>