<div class="modal-dialog">
    <form action="javascript:void(0);" method="post" class="form-horizontal" data-toggle="validator" id="addContentForm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span id='popup-header-txt'><?php if (isset($type) && trim($type)) { echo $type; } else { ?>Add<?php } ?></span> Content Sub Category</h4>
            </div>
            <div class="modal-body">
                <div class="error red text-red" id="cerror" style="display:none"></div>
                
                <div class="form-group">
                   <label class="control-label col-sm-3">Category:</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <div class="select"> 
				<select class="form-control" id="category_id" name="category_parent[]" multiple required <?php if(@$data->id){?>disabled="true" <?php }?>>
                                    <?php
                                    if(!empty($category)){
                                        foreach($category as $cat){ ?>
									<option value="<?php echo $cat['id']; ?>" <?php if(in_array($cat['id'],  explode(',',$data->category_value))){?>selected="selected"<?php }?>><?php echo $cat['category_name']; ?></option>
                                       <?php }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                   <label class="control-label col-sm-3">Sub Category Name:</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" id="displayname" name="displayname" required class="form-control input-sm" value="<?php echo @$data->subcat_name;?>" >
                        </div>
                    </div>
                </div>
                <?php if(@$cat_img_option){ ?>
                <div class="form-group">
                    <label class="control-label col-sm-3">Sub Category Image:</label>
                    <div class="col-sm-9">
                        <input class="btn btn-default-with-bg btn-sm" type="button" name="cat_img" value="Upload Image" data-width="<?php echo @$cat_img_size[0]; ?>" data-height="<?php echo @$cat_img_size[1]; ?>" id="cat_img" onclick="openImageModal(this);"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="avatar_preview_div">
                                <?php if(@$poster !=""){ ?>
                                    <img src="<?php echo @$poster; ?>" alt="<?php echo @$data->category_name;?>" title="<?php echo @$data->category_name;?>">
                                <?php } ?>                                   
                        </div>
                        <canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
                    </div>
                </div>
                <?php } ?>
               	<input type="hidden" name="sub_category_id" value="<?php echo @$data->id;?>" id="sub_category_id"/>
                <input type="hidden" name="category_value" value="<?php echo @$data->category_value; ?>" id="category_value" />
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return addSubContentType()" id="add_btn" >Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>