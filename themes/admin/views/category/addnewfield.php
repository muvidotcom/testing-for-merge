<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title"><?php if(@$cf){echo "Edit";}else{echo "Add";}?> Metadata Field</h3>
</div>
<div class="modal-body">
    <form action="javascript:void(0);" method="post" name="addnewfieldform" id="addnewfieldform" enctype="multipart/form-data">
        <?php 
		if(@$cf && in_array($cf['f_type'],array('3','2'))){
		$languages = $this->enable_laguages;
		if($lang_code){
			$enable_lang = $lang_code;
		}else{
			$enable_lang = $this->language_code;
			if ($_COOKIE['Language']) {
				$enable_lang = $_COOKIE['Language'];
			}
		}		
		if (count($languages) > 1) {?>
			<div class="col-sm-3 p-r-0 select pull-right">
				<select class="form-control input-sm" name="CustomFieldLang" onchange="changeLangCustom(this.value)">
					<?php
					foreach ($languages as $key => $value) {
						if ($value['status'] != 0) {?>
							<option value="<?php echo $value['code']; ?>" <?php
								if ($enable_lang == $value['code']) {
									echo "SELECTED";
								}
								?>><?php echo $value['name']; ?>
							</option>
								<?php } elseif ($value['code'] == "en") { ?>
							<option value="<?php echo $value['code']; ?>" <?php
								if ($enable_lang == $value['code']) {
									echo "SELECTED";
								}
								?>><?php echo $value['name']; ?>
							</option>
							<?php }
						} ?>
				</select>
			</div>
			<div class="clearfix"></div>
	<?php }}?>
        <div class="form-horizontal">
            <div class="form-group" >
                <label class="col-sm-4 toper  control-label" for="Field Name" > Field Name:</label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" id="unique_key" class="form-control input-sm" name="data[f_display_name]" autocomplete="off" required="true" value="<?=$cf['f_display_name'];?>" <?php if(@$cf['id'] > 0){}else{?> onkeyup="javascript: updateUniqueId(this)"<?php }?> />
                        <input type="hidden" value="<?php if(@$cf){echo $cf['id'];}else{echo 0;}?>" name="data[edit_id]">
                    </div>
                    <p>Unique Key : <strong><span id="uniq_value"><?php if(@$cf){echo $cf['f_id'];}else{echo '';}?></span></strong></p>
                    <input type="hidden" class="form-control input-sm" id="f_id" name="data[f_id]" readonly value="<?php if(@$cf){echo $cf['f_id'];}else{echo '';}?>" />
					<div id="plink"></div>
					
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type">Field Type:</label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm" name="data[f_type]" onchange="choosevalue(this);">
                                <?php foreach ($field_type as $key => $value) {
                                    if($cf['f_type']==$key){$selected="selected";}else{$selected="";}
                                ?>
                                <option data-value="<?=$value;?>" value="<?=$key;?>" <?=$selected;?>><?=$value;?></option>
                                <?php }?>
                            </select>
                        </div>                                                
                    </div>
                </div>
            </div>
            <?php if(in_array($cf['f_type'],array('3','2'))){$showvaluefield = 1;}else{$showvaluefield=0;}?>
            <div class="form-group fieldvalue" <?php if(@$cf['f_value'] || $showvaluefield){}else{?>style="display: none;"<?php }?>>
                <?php if(@$cf){?>
                    <label class="col-sm-4 toper  control-label" for="Role">Values in <span id="fieldtypevalue"><?=$field_type[$cf['f_type']];?></span>:</label>
                    <?php 
                    $f_value = json_decode($cf['f_value'],true);
                    if(is_array($f_value)){
						$f_value_new = $f_value;
						$f_value = (array_key_exists($enable_lang, $f_value))?$f_value[$enable_lang]:$f_value['en'];
						$f_value = empty($f_value)?$f_value_new:$f_value;
                        $index = 0;
                        foreach ($f_value as $key1 => $value1) {?>
                            <?php if(!$index){?>
                            <div class="col-sm-7">
                                <div class="fg-line">
                                    <input type="text" value="<?=$value1;?>" class="form-control input-sm addmoretext" name="data[f_value][<?=$key1;?>]" autocomplete="off" required="true"/>
                                </div>
                            </div>
                            <div class="col-sm-1 m-t-10">
                                <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                            </div>
                            <?php }else{?>
                                </div><div class="form-group fieldvalue">
                                <label class="col-sm-4 toper  control-label" for="Role"></label>
                                <div class="col-sm-7">
                                    <div class="fg-line">
                                        <input type="text" value="<?=$value1;?>" class="form-control input-sm addmoretext" name="data[f_value][<?=$key1;?>]" autocomplete="off" required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-1 m-t-10">
                                    <a href="javascript:void(0);" onclick="removefield(this);"><em class="fa fa-minus"></em></a>
                                </div>
                            <?php }?>                            
                    <?php $index++;}
                    }else{?>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm addmoretext" name="data[f_value][0]" autocomplete="off" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-1 m-t-10">
                            <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                        </div>
                    <?php }?>                    
                <?php }else{?>
                    <label class="col-sm-4 toper  control-label" for="Role">Values in <span id="fieldtypevalue"></span>:</label>
                    <div class="col-sm-7">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm addmoretext" name="data[f_value][0]" autocomplete="off" required="true"/>
                        </div>
                    </div>
                    <div class="col-sm-1 m-t-10">
                        <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                    </div>
                <?php }?>                
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" aria-hidden="true"  onclick="savenewfield(this);">Save</button>
</div>
<script>
    $(document).ready(function(){
        $('#unique_key').keyup(function(){
			checkperma($(this).val());
        });
    });
	function checkperma(val){
		if(val != ''){
		   var newperm = name_to_url(val);
				if (newperm==''){
			   $('#plink').html("<input type='text' name='data[f_id]' id='unique_key' class='form-control input-sm checkInput' required placeholder='Please enter english equivalent name' onblur='checkperma(this.value)'>&nbsp;<br />Please provide the english equivalent name which will be used for permalink purpose only.&nbsp;<em class='icon-info'></em>");               
		   }else{
			   $('#plink').html("");
				}
			}			
	}
	function name_to_url(name) {
		name = name.toLowerCase(); // lowercase
		name = name.replace(/^\s+|\s+$/g, ''); // remove leading and trailing whitespaces
		name = name.replace(/\s+/g, ''); // convert (continuous) whitespaces to one -
		name = name.replace(/[^a-z-]/g, ''); // remove everything that is not [a-z] or -
		return name;
	}
</script>