<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal"  id="update_apps" action="<?php echo $this->createUrl('/userfeature/saveapps')?>" method='post' onsubmit="return validate_mobile_app();">
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="chromecast" value="1" <?php echo ( $chromecast== 1)?'checked="checked"':'';?> />
                        <i class="input-helper"></i>  Chromecast
                    </label>
                </div> 
            </div>
             <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="offline_view" id="offline_view" value="1" onclick="uncheck_subitems(this)" <?php echo ($offline == 1)?'checked="checked"':''; ?> />
                        <i class="input-helper"></i>  Offline View
                    </label>
                </div>
            </div>
			<div id="access_watch_section" <?php if(!$offline){?> style="display:none;" <?php } ?>>
            <div class="form-group">                
					<div class="col-md-3">
						<div class="checkbox checkbox-inline">
							<label>
								<input type="checkbox" id="offline_view_access_period" name="offline_view_access_period" value="1" <?php echo ($offline_view_access_period == 1)?'checked="checked"':''; ?>/> <i class="input-helper"></i>Access Period (in days):</label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="fg-line">
							<input type="text" name="offline_view_access_period_limit" id="offline_view_access_period_limit" class="form-control input-sm" onkeypress="return isNumberKey(event)" value="<?php echo $offline_view_access_period_limit;?>"  maxlength="4" onkeyup="remove_error_msg('error_access_period');"/>                                    
						</div>
						<span class="error red error_notify" id="error_access_period" style="display:none;"></span>
					</div>
				</div>	
			</div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="submit" class="btn btn-primary" name="queuebtn" id="queuebtn" value="Save" />
                </div>
            </div>
        </form>
    </div>        
</div>
<script type="text/javascript">
	function isNumberKey(e){
		var unicode = e.charCode ? e.charCode : e.keyCode;
		if ((unicode !== 8) && (unicode !== 9)) {
			if (unicode >= 48 && unicode <= 57)
				return true;
			else
				return false;
		}
	}
	function uncheck_subitems(obj){
		$(".error_notify").hide();
		if($(obj).prop('checked')){
			$('#access_watch_section').show();			
		}else{
			$('#access_watch_section').hide();
			$('#offline_view_access_period').prop('checked', false);
		}	
	}
	function validate_mobile_app(){
		if($('#offline_view').prop('checked')){
			if($('#offline_view_access_period').prop('checked')){	
				var acces_period_val = parseInt($('#offline_view_access_period_limit').val());					
				if(isNaN(acces_period_val) || acces_period_val < 1){
					$('#error_access_period').html('Value should not be less than 1');
					$('#error_access_period').show();
					return false;
				}
			}
				}
			}
	function remove_error_msg(element_id){	
		$('#'+element_id).hide().html('');
	}
</script>	