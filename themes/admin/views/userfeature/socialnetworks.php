<?php 
$is_subscribed = $status['is_subscribed'];
$is_default = $status['is_default'];
//$is_subscribed = 0;
?>
<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal validateForm" name="facebook_login" id="facebook_login" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/facebooklogin" method='post'>
            <input type="hidden" value="<?php echo $sociallogininfos['id']; ?>" name="id">
            <?php if(($sociallogininfos['whose_fb']!='') || ($sociallogininfos['whose_gplus']!='')){
				if(($sociallogininfos['whose_fb']!='')) $v[] = 1;
				if(($sociallogininfos['whose_gplus']!='')) $v[] = 2;
            ?>
            <input type="hidden" value="<?php echo array_sum($v);?>" name="enb_fblogin[]">
            <?php
            }?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($sociallogininfos['whose_fb'] !='')?'checked':''; ?> name="enb_fblogin[]" id="enb_fblogin" class="content"  <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>
                        <i class="input-helper"></i>  Enable Facebook Login for users&nbsp;&nbsp;
                    </label>
                </div>
            </div>
            <div class="form-group" id='existing_facebook' style="<?php echo ($sociallogininfos['whose_fb']=='')?'display:none;':''; ?>">
                <div class="col-md-12">                  
                    <?php if($is_subscribed !=0 || $is_default != 0){ ?>
                    <label class="radio radio-inline">                            
                    <input required type="radio" name="whose_fb" id="whose_fb2" class="radiobtn" value="muvi_fb" <?php echo ($sociallogininfos['whose_fb']=='muvi_fb')?'checked':''; ?> <?php echo ($sociallogininfos['whose_fb']=='')?'checked':''; ?> <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>                                
                    <i class="input-helper"></i> Muvi to create and manage the Facebook App (recommended)
                    </label>
                    <?php } ?>
                    <label class="radio radio-inline">                                       
                    <input required type="radio" name="whose_fb" id="whose_fb1" class="radiobtn" value="user_fb" <?php echo ($sociallogininfos['whose_fb']=='user_fb')?'checked':''; ?>  <?php echo ($sociallogininfos['whose_fb']!='')?'disabled':''?>>                                
                    <i class="input-helper"></i> Use my Facebook App
                    </label>                      
                </div>
            
            <div id='user_app' style="<?php echo ($sociallogininfos['whose_fb']=='user_fb')?'':'display:none;'; ?>">
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-md-4 control-label">App ID<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_id" value="<?php echo $sociallogininfos['fb_app_id'];?>" class="form-control input-sm" placeholder="Please enter facebook App Id">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">App Secret<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_secret" value="<?php echo $sociallogininfos['fb_secret'];?>" class="form-control input-sm" placeholder="Please enter App secret key">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">App Version<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_version" value="<?php echo $sociallogininfos['fb_app_verson'];?>" class="form-control input-sm" placeholder="Please enter facebook App version">
                            </div>
                        </div>              
                    </div> 
                    <div class="form-group">
                        <label class="col-md-4 control-label">Redirect URL<span class="red"><b>*</b></span>:</label>  
                        <div class="col-md-8">
                            <div class="fg-line">
                                <input type='text' name="app_redirect_url" value="<?php echo Yii::app()->getBaseUrl(true);?>/Login/FacebookAuth/" class="form-control input-sm" readonly>
                            </div>
                        </div>              
                    </div> 
                </div>
            </div>
			</div>
            <div class="row">
                <div class="col-md-8">
                    <?php
                    if($sociallogininfos['status']==2)
                    {
                    ?>
                        <h4>Your Facebook Login is <font color="#FFDE7D">PROCESSING</font></h4>
                        <p>Your request is received, Muvi is working to create an App. You can expect a swanky new login in 2 days.</p>
                    <?php
                    }
                    else if($sociallogininfos['status']==3)
                    {
                    ?>
                        <button type="submit" id="fblogin" class="btn btn-primary">Update Facebook info</button>
                        <button type="submit" id="fblogin" class="btn btn-primary" value="redirect_fb" name="redirect_fb">Redirect URL entered to App</button>
                        <h4>Your Facebook Login is <font color="#27A226">Pending AuthURL</font></h4>
                        <p>Your login will be enabled as soon as the Redirect URL is entered into the App.</p>         
                    <?php            
                    }
                    else if($sociallogininfos['status']==1)
                    {
                        if($sociallogininfos['whose_fb']=='user_fb')
                        {
                        ?>
                        <button type="submit" id="fblogin" class="btn btn-primary">Update Facebook info</button>
                        <?php
                        }
                    ?>
                        <h4>Your Facebook Login is <font color="#27A226">Active</font></h4>     
                    <?php               
                    }
                    else
                    {
                    ?>
                    <?php
                    }
                    ?>                    
                </div>                
            </div>
    
<?php
if($sociallogininfos['status']==1)
{
?>
<?php /*
<form class="form-horizontal" class="update_content_type" name="facebook_loginactive" id="facebook_loginactive" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/facebookloginactive" method='post'>
*/?>	
<div class="row m-t-20 m-b-20">
    <div class="col-sm-12">
        <?php
        if($status['social_logins'] & 1)
        {
            $confirm_title="Deactive Facebook Login";
            $confirm_message="Are you sure to Deactive Facebook Login";
        ?>
        <input type="hidden" name="social_logins" value="0">
        <button type="button" id="fbact" class="btn btn-primary" data-msg="0"  >Deactive Facebook Login</button>
        <br />Facebook login button will not shown in login page.
        <?php
        }
        else
        {
            $confirm_title="Active Facebook Login";
            $confirm_message="Are you sure to Active Facebook Login";
        ?> 
        <input type="hidden" name="social_logins" value="1">
        <button type="button" id="fbact" class="btn btn-primary" data-msg="1"  >Active Facebook Login</button>
        <br />Facebook login button will shown in login page.
        <?php    
        }
        ?>
    </div>
</div> 
<?php /*</form>*/?>
<?php
}
?>
<div class="col-sm-12">
	<div class="form-group">                  
		<label class="checkbox checkbox-inline m-r-20">
			<input type="checkbox" value="2" <?php echo ($sociallogininfos['whose_gplus'] != '') ? 'checked' : ''; ?> name="enb_fblogin[]" id="enb_gplogin" class="content"  <?php echo ($sociallogininfos['whose_gplus'] != '') ? 'disabled' : '' ?>>
			<i class="input-helper"></i>  Enable google Login for users&nbsp;&nbsp;
		</label>
	</div>
</div>
<div class="form-group" id='existing_google' style="<?php echo ($sociallogininfos['whose_gplus']=='')?'display:none;':''; ?>">
	<div class="col-md-12 m-b-10">                  
		<?php if ($is_subscribed != 0 || $is_default != 0) { ?>
			<label class="radio radio-inline">                            
				<input required type="radio" name="whose_gplus" id="whose_gp2" class="radiobtn" value="muvi_gplus" <?php echo ($sociallogininfos['whose_gplus'] == 'muvi_gplus') ? 'checked' : ''; ?> <?php echo ($sociallogininfos['whose_gplus'] == '') ? 'checked' : ''; ?> <?php echo ($sociallogininfos['whose_gplus'] != '') ? 'disabled' : '' ?>>                                
				<i class="input-helper"></i> Muvi to create and manage the Google App (recommended)
			</label>
		<?php } ?>
		<label class="radio radio-inline">                                       
			<input required type="radio" name="whose_gplus" id="whose_gp1" class="radiobtn" value="user_gplus" <?php echo ($sociallogininfos['whose_gplus'] == 'user_gplus') ? 'checked' : ''; ?>  <?php echo ($sociallogininfos['whose_gplus'] != '') ? 'disabled' : '' ?>>                                
			<i class="input-helper"></i> Use my Google App
		</label>                      
	</div>

	<div id='user_app_gplus' style="<?php echo ($sociallogininfos['whose_gplus'] == 'user_gplus') ? '' : 'display:none;'; ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="col-md-4 control-label">Client ID<span class="red"><b>*</b></span>:</label>  
				<div class="col-md-8">
					<div class="fg-line">
						<input type='text' name="gplus_client_id" value="<?php echo $sociallogininfos['gplus_client_id']; ?>" class="form-control input-sm" placeholder="Please enter Google App Id">
					</div>
				</div>              
			</div> 
			<div class="form-group">
				<label class="col-md-4 control-label">Client secret<span class="red"><b>*</b></span>:</label>  
				<div class="col-md-8">
					<div class="fg-line">
						<input type='text' name="gplus_client_secret" value="<?php echo $sociallogininfos['gplus_client_secret']; ?>" class="form-control input-sm" placeholder="Please enter App secret key">
					</div>
				</div>              
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">Redirect URL<span class="red"><b>*</b></span>:</label>  
				<div class="col-md-8">
					<div class="fg-line">
						<input type='text' name="app_redirect_gplus_url" value="<?php echo Yii::app()->getBaseUrl(true);?>/Login/GoogleplusAuth/" class="form-control input-sm" readonly>
					</div>
				</div>              
			</div> 
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<?php
		if($sociallogininfos['gplus_status']==2)
		{
		?>
			<h4>Your Google Login is <font color="#FFDE7D">PROCESSING</font></h4>
			<p>Your request is received, Muvi is working to create an App. You can expect a swanky new login in 2 days.</p>
		<?php
		}
		else if($sociallogininfos['gplus_status']==3)
		{
		?>
			<button type="submit" id="fblogin" class="btn btn-primary">Update google info</button>
			<button type="submit" id="fblogin" class="btn btn-primary" value="redirect_gp" name="redirect_gp">Redirect URL entered to App</button>
			<h4>Your Google Login is <font color="#27A226">Pending AuthURL</font></h4>
			<p>Your login will be enabled as soon as the Redirect URL is entered into the App.</p>         
		<?php            
		}
		else if($sociallogininfos['gplus_status']==1)
		{
			if($sociallogininfos['whose_gplus']=='user_gplus')
			{
			?>
			<button type="submit" id="fblogin" class="btn btn-primary">Update google info</button>
			<?php
			}
		?>
			<h4>Your Google Login is <font color="#27A226">Active</font></h4>     
		<?php               
		}
		else
		{
		?>
		<?php
		}
		?>                    
	</div>                
</div>			
<?php
if($sociallogininfos['gplus_status']==1)
{
?>
<div class="row m-t-20 m-b-20">
    <div class="col-sm-12">
        <?php
        if($status['social_logins'] & 2)
        {            
        ?>
        <button type="button" id="fbact_gp" class="btn btn-primary" data-msg="0" data-confirm-title="Deactive Google Login" data-confirm-msg="Are you sure to Deactive Google Login" >Deactive Google Login</button>
        <br />Google login button will not shown in login page.
        <?php
        }
        else
        {
            $confirm_title="Active Google Login";
            $confirm_message="Are you sure to Active Google Login";
        ?> 
        <button type="button" id="fbact_gp" class="btn btn-primary" data-msg="1" data-confirm-title="Active Google Login" data-confirm-msg="Are you sure to Active Google Login" >Active Google Login</button>
        <br />Google login button will shown in login page.
        <?php    
        }
        ?>
    </div>
</div> 
<?php }?>
<?php if($sociallogininfos['status']==0 && $sociallogininfos['gplus_status']==0){ $cnf = 'Confirm'; }else{ $cnf = 'Update'; }?>
	<?php if(($sociallogininfos['whose_gplus'] == '') || ($sociallogininfos['whose_fb'] == '')){?>
	<div class="row">
		<div class="col-md-8">
			<button id="submitbutton" class="btn btn-primary" <?php echo ($status['status'] != 1)?'disabled':''?>><?php echo $cnf;?></button>
			<p></p>
			<p id="msgdis">A Muvi developer will create your App, and link it to the website. takes 2 days.</p>          
		</div>
	</div>
	<?php }?>		
        </form>
	</div>        
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#fbact').click(function(){
			var fb = $(this).attr('data-msg');
            swal({
                title: "<?php echo $confirm_title;?>",
                text: "<?php echo $confirm_message;?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function() {
                $.ajax({
					url: "<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/facebookloginactive",
					data: {'social_logins':fb},
					method: 'post',
					success: function (result) {
						window.location.reload();
					}
				});
            });

        });
		
		$('#fbact_gp').click(function(){
			var fb = $(this).attr('data-msg');
            swal({
                title: $(this).attr('data-confirm-title'),
                text: $(this).attr('data-confirm-msg'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function() {
                $.ajax({
					url: "<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/googleloginactive",
					data: {'social_logins':fb},
					method: 'post',
					success: function (result) {
						window.location.reload();
					}
				});
            });

        });
    });   
</script>    
<script type="text/javascript">
     $("#enb_fblogin").click(function () {
        0 == this.checked ? $("#existing_facebook").hide() : $("#existing_facebook").show()
    });
     $("#whose_fb1").click(function () {
        0 == this.checked ? $("#user_app").hide() : $("#user_app").show();
        $('#msgdis').html("The redirect URL needs to be linked to your account.");
    }); 
     $("#whose_fb2").click(function () {
        0 == this.checked ? $("#user_app").show() : $("#user_app").hide();
        $('#msgdis').html("A Muvi developer will create your App, and link it to the website. takes 2 days.");
    });    
    
	$("#enb_gplogin").click(function () {
        0 == this.checked ? $("#existing_google").hide() : $("#existing_google").show()
    });
	$("#whose_gp1").click(function () {
        0 == this.checked ? $("#user_app_gplus").hide() : $("#user_app_gplus").show();
        $('#msgdis').html("The redirect URL needs to be linked to your account.");
    }); 
     $("#whose_gp2").click(function () {
        0 == this.checked ? $("#user_app_gplus").show() : $("#user_app_gplus").hide();
        $('#msgdis').html("A Muvi developer will create your App, and link it to the website. takes 2 days.");
    });
	
	$('#facebook_login').validate({ 
        rules: {
            "enb_fblogin[]": {required: true},
            app_id: {required: true},
            app_secret: {required: true},
            app_version: {required: true},
			gplus_client_id: {required: true},
            gplus_client_secret: {required: true},
        }, 
        messages: { 
            "enb_fblogin[]": 'Click on the checkbox to enable facebook/google login.',
            app_id: 'Please enter app id.',
            app_secret: 'Please enter app secrete key.',
            app_version: 'Please enter app version.',
			gplus_client_id: 'Please enter google app id.',
            gplus_client_secret: 'Please enter google app secrete key.',
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }
    });
    var x = validate.form();
    if (x) {
        $('#facebook_login').submit();
    }
</script>