<?php
$content = Yii::app()->general->content_count($studio_id);
$signup_steps = Yii::app()->general->signupSteps();
?>
<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal" class="update_favourite" name="update_favourite" id="update_favourite" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/Settings" method='post'>
            <input type="hidden" name="favliststatus" id="favliststatus" value="<?php echo ($status == 1) ? $status : 0; ?>" />
            <?php if ($signup_steps != 1) { ?>
                <div class="form-group">                
                    <div class="col-md-12">                  
                        <label class="checkbox checkbox-inline m-r-20">
                            <input type="checkbox" value="1" <?php echo ($verify_val != 0) ? 'checked' : ''; ?> name="verify" id="verify" class="content">
                            <i class="input-helper"></i>  Verify Email for Registration
                        </label>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($status == 1)?'checked':''; ?> name="addfav" id="addfav" class="content">
                        <i class="input-helper"></i>  Add to Favorites
                    </label>
                </div>
            </div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="mylibrary_activated" value="1" <?php echo ($studio->mylibrary_activated == 1)?'checked="checked"':''?> />
                        <i class="input-helper"></i>  My Library
                    </label>
                </div>
            </div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($watch_val != 0)?'checked':''; ?> name="watch" id="watch" class="content">
                        <i class="input-helper"></i>  Watch History                        
                    </label>
                </div>
            </div>
            <div class="form-group" id="duration" style="display: <?php echo ($watch_val != 0)?'all':'none'; ?>">
                <div class="col-md-1" style="margin-left: 30px;margin-top: 10px;"><strong>Duration : &nbsp;</strong></div>
                <div class="col-md-2">                    
                    
                    
                    <div class="select">
                        <select class="form-control input-sm" name="duration">
                            <option value="7" <?php echo ($watch_val==7)?'selected':'';?> >1 Week</option>
                            <option value="30" <?php echo ($watch_val==30)?'selected':'';?> >1 Month</option>
                            <option value="180" <?php echo ($watch_val==180)?'selected':'';?> >6 Months</option>
                        </select>
                    </div> 
                </div>
            </div>     
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo (@$reminder != 0)?'checked':''; ?> name="reminder" id="reminder" class="reminder">
                        <i class="input-helper"></i>  Reminder                      
                    </label>
                </div>
            </div>  
            <div class="form-group" id="notify" style="display: <?php echo (@$reminder != 0)?'all':'none'; ?>">                
                <div class="col-md-12" style="margin-left: 30px;">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo (@$reminder != 0 && @$notification != 0)?'checked':''; ?> name="notification" id="notification" class="notification">
                        <i class="input-helper"></i>  Notifications                      
                    </label>
                </div>
            </div>
            <div class="form-group" id="remind_before" style="display: <?php echo (@$reminder != 0 && @$notification != 0)?'all':'none'; ?>"> 
                <label class="col-md-2 control-label" style="margin-left: 60px; width:130px;">Remind Before:</label>
                <div class="col-md-1 p-r-0">    
                    <div class="fg-line">
                        <input type="number" name="notify_before" id="notify_before" min="1" value="<?php echo $reminder_before; ?>" class="form-control" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" />   
                    </div>
                    <span class="error red error_notify" id="error" style="display:none;">Please enter only numbers</span>
                </div>
                <div class="col-md-2 m-t-10">
                    (In Minutes) 
                </div>
            </div>
            <?php  if ((isset($content) && ($content['content_count'] & 4))) { ?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($queue_enabled == 1)?'checked':''; ?> name="adque" id="adque" class="content">
                        <i class="input-helper"></i>  Add to Queue
                    </label>
                </div>
            </div>
			<div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($playlist_enable == 1)?'checked':''; ?> name="enplaylist" id="enplaylist" class="content">
                        <i class="input-helper"></i>  Add to Playlist
                    </label>
                </div>
            </div>
            <?php } ?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                     <input type="checkbox" name="user_generated_content" value="1" <?php echo ($is_generate_ugc)?'checked="checked"':"";?>/><i class="input-helper"></i>  Enable UGC(user-generated content). Will allow users to add content.
                    </label>
                </div>
            </div>
            <?php if($is_generate_ugc==1){?>
            <div class="form-group">   
                <div class="col-md-12"> 
                    <label class="checkbox checkbox-inline m-r-20">
                    <input type="checkbox" name="user_review_content" value="1" <?php echo ($is_review_ugc)?'checked="checked"':"";?>/><i class="input-helper"></i>  Review UGC(user-generated content).
                    </label>
                </div>
            </div>
            <?php }?>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="button" class="btn btn-primary" name="favbtn" id="favbtn" value="Save" />
                </div>
            </div>
        </form>
    </div>        
</div>
<script type="text/javascript">
    var favliststatus;
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        $(".error_notify").hide();
        $(".error_notify").html("Please enter only numbers");
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("error").style.display = ret ? "none" : "inline";
        return ret;
        }
    $(document).ready(function(){
        $("#watch").click(function(){
            if($("#watch").prop('checked')){
                $('#duration').show();
            }else{
                $('#duration').hide();
            }
        });
        $("#reminder").click(function(){
            if($("#reminder").prop('checked')){
                $('#notify').show();
                notificationCheck();
            }else{
                $('#notify').hide();
                $('#remind_before').hide();
            }
        });
        $("#notification").click(function(){
            notificationCheck();
        });
        $("#favbtn").click(function(){
            if($("#notification").prop('checked')){
                var notify_before = $.trim($("#notify_before").val());
                if(notify_before > 0){
                    $("#update_favourite").submit();
                }else{
                    $(".error_notify").show();
                    $(".error_notify").html("Please add a notification time");
                }
            }else{
                $("#update_favourite").submit();
            }
        });
    });
    function notificationCheck(){
        if($("#notification").prop('checked')){
            $('#remind_before').show();
        }else{
            $('#remind_before').hide();
        }
    }
</script>

