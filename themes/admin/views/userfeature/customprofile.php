<style type="text/css">
    body {overflow-x: visible;}/*add due to chrome and safari sortable jump issue*/
    .myDragClass{
        background: #dfdfdf;
        border: 1px solid #fff;
        z-index: 9999;
    }
    .overlay-white{
        background: rgba(255, 255, 255, 0.7);
    }
    #sortableform .editable-container.editable-inline{float: left;width: 33.3% !important;padding: 0 15px;}
    #sortableform .editable-input :nth-child(1){width: 160px;}
    #sortableform .editable-buttons{display: block;margin-top: 7px;margin-left:0px !important;}
    .loading_div{display: none;}    
    .required{content:"*"; color:red;}
    .cuspolsory_msg{color:red; font-size: 12px;}
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/cssnew/jqueryui-editable.css">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/jqueryui-editable.min.js"></script>

<hr>
<div class="row">
    <div class="col-sm-12">

        <div class="row">
            <div class="col-sm-12" id="custom_field">
            </div>
        </div>
        <!--</form>-->
        <!--removed form end tag-->
    </div>
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript">
    $.fn.editable.defaults.mode = 'inline';
    $(document).ready(function () {
        //getCustomForm();
        getCustomField();
        jQuery.validator.addMethod("dimension", function (value, element) {
            return this.optional(element) || /^[0-9]{1,4}x[0-9]{1,4}$/i.test(value);
        }, "Dimension must be in 280x400 format in px.");
    });
    $(function () {
        InlineEditName();
    });
    function validateCatname(nm) {
        var re = /[a-zA-Z]{1}/i;
        return re.test(nm);
    }
    $("#sortableform").sortable({
        //cancel: ".fixed"
    });
    function InlineEditName() {
        $('.editdisplayname').editable({
            type: 'text',
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
                if (!validateCatname($.trim(value))) {
                    return 'Name must be contain alphabate';
                }
            },
            url: function (params) {
                var d = $(this).attr('data-displayname');
                var displayname = params.value;
                $('input[name="' + d + '"]').val(displayname);
                //$(this).parent().children().find("div[id^='movecustomreport']").attr('data-name',displayname);
                return displayname;
            }
        });
    }
    function getCustomField() {
        $('.loading_div').show();

        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/userfeature/getCustomField";
        $.post(url, function (data) { //alert(toString(data)); return false;
            $('#custom_field').html(data);
            $("#sortableform").sortable();
            InlineEditName();
            $('.loading_div').hide();
            $('#sortableform').css({'opacity': '1'});
            $("#customparentform :button").removeAttr("disabled");
        });
    }
    function showlevel(flag) {
        if (flag) {
            $('#st_level').show('slow');
        } else {
            $('#st_level').hide('slow');
        }
        getCustomForm();
    }


    function SaveCustomField(obj) {
        $(obj).attr('disabled', 'disabled');
        var url = "<?= Yii::app()->getBaseUrl(TRUE); ?>/userfeature/SaveCustomeForm";
        $('#customparentform').attr('action', url);
        $('#customparentform').submit();
    }

    function openinmodal(url) {
        var parent_content_type_id = $('#parent_content_type_id').val();
        var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
        var is_child = $('input[name="formfield[is_child]"]:checked').val();
        $.post(url, {"parent_content_type_id": parent_content_type_id, 'is_multipart': is_multipart, 'is_child': is_child}, function (data) {
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function movetoformcolumn(obj) {
        var id = $(obj).attr('data-id');
        var type = $(obj).attr('data-type');
        var name = $(obj).attr('data-name');
        var moveto = $("#customfieldreplace" + id).html();
        //alert(moveto);
        //alert(id+"=="+type+"=="+name+"=="+moveto);
        //return false;
        $(".adddiv").append(moveto);
        $(obj).parent().remove();
        InlineEditName();
        $('#no_record').hide();
    }
    function movetofieldcolumn(obj) {
        var id = $(obj).attr('data-id');
        var type = $(obj).attr('data-type');
        var name = $(obj).attr('data-name');
        var tempid = $(obj).attr('data-tempid');
        var moveto = $("#customreportreplace" + id).html();
        var field_name = $(obj).attr('data-field_name');

        var editdelete = '<div class="col-sm-1 text-right" data-id="' + tempid + '" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4" aria-hidden="true"></i></a></div><div class="col-sm-1 text-right" data-id="' + tempid + '" onclick="deletefield(this)"><a href="javascript:void(0)" title="Delete field"><i class="icon-close h4" aria-hidden="true"></i></a></div>';
        if (field_name == 'confirm_password') {
            var editdelete = '<div class="col-sm-1 text-right" data-id="' + tempid + '" onclick="deletefield(this)"><a href="javascript:void(0)" title="Delete field"><i class="icon-close h4" aria-hidden="true"></i></a></div>';
        }

        $("#adddivtoavailable").append('<div class="col-sm-12"><div class="row"><div class="col-sm-5"><p>' + name + '</p></div><div class="col-sm-4">' + type + '<div style="display: none;" id="customfieldreplace' + id + '"><li class="m-t-20" id="customreportreplace' + id + '">' + moveto + '</li></div></div><div class="col-sm-1 text-right" data-type="' + type + '" data-id="' + id + '" data-name="' + name + '"  onclick="movetoformcolumn(this)" ><a href="javascript:void(0)" title="Add Field"><i class="icon-plus h4" aria-hidden="true"></i></a></div>' + editdelete + '</div></div>');
        $(obj).parent().parent().parent().parent().remove();
    }
    function savenewfield(obj) {
        var validate = $("#addnewfieldform").validate({
            rules: {
                "data[f_label]": {required: true},
                "data[field_id]": {required: true},
                "data[field_type]": {required: true}
            },
            messages: {
                "data[f_label]": {required: "Please enter a field label"},
                "data[field_type]": {required: "Please select field type"}
            },
            errorPlacement: function (error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            },
        });
        var x = validate.form();
        var formData = $('#addnewfieldform').serialize();
        if (x) {
            $(obj).attr('disabled', 'disabled');
            var flag = $(obj).attr('data-flag');
            var url = "<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/SaveNewField";
            $.post(url, {"formData": formData}, function (res) {
                if (res) {
                    $('#success_msg').html('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;' + res + '</div>');
                    //if(flag == '1'){ getCustomField(); }
                    getCustomField();
                    $(obj).removeAttr("disabled");
                } else {
                    $('#success_msg').html('<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error occured please try again.</div>');
                }
                setTimeout(function () {
                    $("#mymodal").modal('hide');
                }, 5000);
                
                
            });
            //document.addnewfieldform.action = url;
            //document.addnewfieldform.submit();            
        }
    }
    function updateUniqueId(val1) {
        var url = HTTP_ROOT + "/userfeature/CheckUniqueFieldName";
        $.ajax({
            url: url,
            type: "POST",
            data: {'fieldname': val1.value},
            success: function (res) {
                $('#field_id').val(res);
                $('#uniq_value').html(res);
            }
        });
    }
    function choosevalue(obj) {
        if ($(obj).val() > 1 && ($(obj).val() != 6)) {
            $('#fieldtypevalue').html($(obj).find(':selected').data('value'));
            $('.fieldvalue').show();
        } else {
            $('.fieldvalue').hide();
        }
        $('.addmoretext').val(function () {
            return this.defaultValue;
        });
    }
    function addmorefield() {
        var length = $('.addmoretext').length;
        var div = '<div class="form-group fieldvalue"><label class="col-sm-4 toper control-label" for="Role">&nbsp;</label><div class="col-sm-7"><div class="fg-line"><input type="text" class="form-control input-sm addmoretext" name="data[field_values][' + length + ']" autocomplete="off" required="true"/></div></div><div class="col-sm-1 m-t-10"><a href="javascript:void(0);" onclick="removefield(this);"><em class="fa fa-minus"></em></a></div><div>';
        $('.fieldvalue').last().after(div);
    }
    function editfield(obj,flag) {
        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/userfeature/AddNewField";
        $.post(url, {'id': $(obj).attr('data-id'),'flag':flag}, function (data) {
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function deletefield(obj) {
        swal({
            title: "Remove metadata field?",
            text: "It will be removed from all associative content forms",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        },
                function () {
                    var url = "<?= Yii::app()->getBaseUrl(TRUE); ?>/userfeature/RemoveCustomField";
                    $.post(url, {'id': $(obj).attr('data-id')}, function (res) {
                        if (res) {
                            getCustomField();
                            $(obj).parent().remove();
                        }
                    });
                });
    }
    function removefield(obj) {
        $(obj).parent().parent().remove();
    }
    function changeLangCustom(lang_code) {
        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/category/AddNewField";
        var parent_content_type_id = $('#parent_content_type_id').val();
        var is_multipart = $('input[name="formfield[content_type]"]:checked').val();
        var is_child = $('input[name="formfield[is_child]"]:checked').val();
        var id = $('input[name="data[edit_id]"]').val();
        $.post(url, {'id': id, 'lang_code': lang_code, "parent_content_type_id": parent_content_type_id, 'is_multipart': is_multipart, 'is_child': is_child}, function (data) {
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }

    function showdate(obj) {
        var id = $(obj).attr('data-id');
        $('#' + id).datepicker();
    }
	function changeLangCustomProfile(lang_code){
		var url = "<?php echo Yii::app()->getBaseUrl(); ?>/userfeature/AddNewField";
		var id = $('input[name="data[edit_id]"]').val();
		$.post(url, {'id': id, 'lang_code': lang_code,'flag':0}, function (data) {
			$('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
		});
	}
</script>