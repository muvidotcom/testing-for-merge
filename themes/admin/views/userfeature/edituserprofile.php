<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit user profile</h4>
    <div id="success_msg"></div>
</div>
<div class="modal-body">
    <form action="javascript:void(0);" method="post" name="update_profile" id="update_profile" data-toggle="validator">
        <div class="form-horizontal">
            <?php if ($this->chkStudioForCustomProfile) { ?>
            <?php if (count($custom_fields) > 0) { ?>
                <?php foreach ($custom_fields as $fields) { ?>
                    <div class="form-group">
                        <label  class="control-label col-sm-3 toper"><?= $fields['label'] ?></label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <?= $fields['field'] ?>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                }
            } else {
                ?>
                <div class="form-group">
                    <label  class="control-label col-sm-3 toper">Name</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" class="form-control" required="required" name="name" value="<?= $user->display_name ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="control-label col-sm-3 toper">Email</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" class="form-control" name="email" value="<?= $user->email ?>"/>
                        </div>
                    </div>
                </div>
                <?php if (count($custom_fields) > 0) { ?>
                    <?php foreach ($custom_fields as $fields) { ?>
                        <div class="form-group">
                            <label  class="control-label col-sm-3 toper"><?= $fields['label'] ?></label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <?= $fields['field'] ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
            <div class="form-group" >
                <input type="hidden" value="<?= $user->announcement_subscribe ?>" name="subscribe_newsletter">
                <input type="hidden" value="<?= $user->id ?>" name="id">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary"  id="profile_btn" onclick="return updateProfile()">Submit</button>
</div>
<script>
    $(document).ready(function () {
        $(".date_picker").datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>