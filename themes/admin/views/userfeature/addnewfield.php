<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title"><?php
        if (@$cf) {
            echo "Edit";
        } else {
            echo "Add";
        }
        ?> Custom Field</h3>
    <div id="success_msg"></div>
</div>
<div class="modal-body">
    <form action="javascript:void(0);" method="post" name="addnewfieldform" id="addnewfieldform" enctype="multipart/form-data">
         <?php 
		if(@$cf && in_array($cf['input_type'],array('3','2','4','5'))){
		$languages = $this->enable_laguages;
		if($lang_code){
			$enable_lang = $lang_code;
		}else{
			$enable_lang = $this->language_code;
			if ($_COOKIE['Language']) {
				$enable_lang = $_COOKIE['Language'];
			}
		}		
		if (count($languages) > 1) {?>
			<div class="col-sm-3 p-r-0 select pull-right">
				<select class="form-control input-sm" name="data[CustomFieldLang]" onchange="changeLangCustomProfile(this.value)">
					<?php
					foreach ($languages as $key => $value) {
						if ($value['status'] != 0) {?>
							<option value="<?php echo $value['code']; ?>" <?php
								if ($enable_lang == $value['code']) {
									echo "SELECTED";
								}
								?>><?php echo $value['name']; ?>
							</option>
								<?php } elseif ($value['code'] == "en") { ?>
							<option value="<?php echo $value['code']; ?>" <?php
								if ($enable_lang == $value['code']) {
									echo "SELECTED";
								}
								?>><?php echo $value['name']; ?>
							</option>
							<?php }
						} ?>
				</select>
			</div>
			<div class="clearfix"></div>
	<?php }}?>
        <div class="form-horizontal">
            <div class="cuspolsory_msg">'*' marked fields are mandatory to fill up</div>
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type">Field Label:<span class="required">*</span></label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="data[f_label]" autocomplete="off" required="true" value="<?= $cf['field_label']; ?>" <?php
                        if (@$cf['id'] > 0) {
                            
                        } else {
                            ?> onkeyup="javascript: updateUniqueId(this)"<?php } ?> />
                    <p>Unique Key : <strong><span id="uniq_value"><?php
                                if (@$cf) {
                                    echo $cf['field_id'];
                                } else {
                                    echo '';
                                }
                    ?></span></strong></p> 
                        <input type="hidden" value="<?php if (@$cf) { echo $cf['id']; } else { echo 0; }?>" name="data[edit_id]">
                        <?php if (@$cf) { ?>
                        <input type="hidden" class="form-control input-sm" id="field_id" name="data[field_id]" readonly value="<?php if (@$cf) { echo $cf['field_id']; } else { echo ''; } ?>" />
                        <input type="hidden" class="form-control input-sm" id="field_id" name="data[field_name]" readonly value="<?php if (@$cf) { echo $cf['field_name']; } else { echo ''; } ?>" />
                        <?php } ?>
                </div>
            </div>
            </div>
            
            
            <?php if($cf['field_name'] != 'name'){ ?>
            <div class="form-group" style="display:none;">
                <label class="col-sm-4 toper  control-label" for="Field Type">Field ID:<span class="required">*</span></label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="data[field_id]" autocomplete="off" required="true" value="<?= $cf['field_id']; ?>" id="field_id" readonly="true">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type">Field Type:<span class="required">*</span></label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm" name="data[input_type]" onchange="choosevalue(this);">
                                <?php
                                foreach ($field_type as $key => $value) {
                                    if ($cf['input_type'] == $key) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option data-value="<?= $value; ?>" value="<?= $key; ?>" <?= $selected; ?>><?= $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>                                                
                    </div>
                </div>
            </div>
            <?php
            if (in_array($cf['input_type'], array('3', '2','4','5'))) {
                $showvaluefield = 1;
            } else {
                $showvaluefield = 0;
            }
            ?>
            <div class="form-group fieldvalue" <?php
            if (@$cf['field_values'] || $showvaluefield) {
                
            } else {
                ?>style="display: none;"<?php } ?>>
                 <?php if (@$cf) { ?>
                    <label class="col-sm-4 toper  control-label" for="Role">Values in <span id="fieldtypevalue"><?= $field_type[$cf['input_type']]; ?></span>:</label>
                    <?php
                    $field_values = json_decode($cf['field_values'], true);
                    if (is_array($field_values)) {
                        $field_values_new = $field_values;
                        $field_values = (array_key_exists($enable_lang, $field_values)) ? $field_values[$enable_lang] : $field_values['en'];
                        $field_values = empty($field_values) ? $field_values_new : $field_values;
                        $index = 0;
                        foreach ($field_values as $key1 => $value1) {
                            ?>
                            <?php if (!$index) { ?>
                                <div class="col-sm-7">
                                    <div class="fg-line">
                                        <input type="text" value="<?= $value1; ?>" class="form-control input-sm addmoretext" name="data[field_values][<?= $key1; ?>]" autocomplete="off" required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-1 m-t-10">
                                    <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                                </div>
                            <?php } else { ?>
                            </div><div class="form-group fieldvalue">
                                <label class="col-sm-4 toper  control-label" for="Role"></label>
                                <div class="col-sm-7">
                                    <div class="fg-line">
                                        <input type="text" value="<?= $value1; ?>" class="form-control input-sm addmoretext" name="data[field_values][<?= $key1; ?>]" autocomplete="off" required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-1 m-t-10">
                                    <a href="javascript:void(0);" onclick="removefield(this);"><em class="fa fa-minus"></em></a>
                                </div>
                            <?php } ?>                            
                            <?php
                            $index++;
                        }
                    } else {
                        ?>
                        <div class="col-sm-7">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm addmoretext" name="data[field_values][0]" autocomplete="off" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-1 m-t-10">
                            <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                        </div>
                    <?php } ?>                    
                <?php } else { ?>
                    <label class="col-sm-4 toper  control-label" for="Role">Values in <span id="fieldtypevalue"></span>:</label>
                    <div class="col-sm-7">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm addmoretext" name="data[field_values][0]" autocomplete="off" required="true"/>
                        </div>
                    </div>
                    <div class="col-sm-1 m-t-10">
                        <a href="javascript:void(0);" onclick="addmorefield();"><em class="icon-plus"></em></a>
                    </div>
                <?php } ?>                
            </div>
            <?php } ?>
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type">Hint Text:</label>
                <div class="col-sm-7">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="data[f_placeholder]" autocomplete="off"  value="<?= $cf['placeholder']; ?>">
                    </div>
                </div>
            </div>
              <?php if($cf['field_name'] != 'name'){ ?>
            <div class="form-group">
                <label class="col-sm-4 toper  control-label" for="Field Type"></label>
                <div class="col-sm-7">
                    <div class="checkbox">
                        <label>
                        <input type="checkbox" name="data[is_require]" id="is_require" value="1" <?php if($cf['is_required']){ echo "checked"; } ?>><i class='input-helper'></i>Is Required
                        </label>
                    </div>
                </div>
            </div>
              <?php } ?>
            
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" aria-hidden="true" data-flag="<?= $flag ?>"  onclick="savenewfield(this);">Save</button>
</div>
