<?php
$enable_lang = $this->language_code;
if ($_COOKIE['Language']) {
	$enable_lang = $_COOKIE['Language'];
}
?>
<div class="col-sm-7 p-l-0">
    <form action="javascript:void(0);" method="post" id="customparentform">
        <div class="border-dotted">
            <div class="row">
                <!--                                <h3 class="col-sm-12 text-capitalize f-300"></h3>-->
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <div class="loading_div" style="display: none;">
                        <div class="preloader pls-blue preloader-sm">
                            <svg viewBox="25 25 50 50" class="pl-circular">
                            <circle r="20" cy="50" cx="50" class="plc-path"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <ul id="sortableform" class="list-unstyled adddiv">
                <?php
                if (!empty($defaultField)) {
                    foreach ($defaultField as $defaultdata) {
                        if ($defaultdata['input_type'] == 0) {
                            $types = 'Textarea';
                        } elseif ($defaultdata['input_type'] == 1) {
                            $types = 'Textfield';
                        } elseif ($defaultdata['input_type'] == 2) {
                            $types = 'Dropdown';
                        } elseif ($defaultdata['input_type'] == 3) {
                            $types = 'List';
                        } elseif ($defaultdata['input_type'] == 4) {
                            $types = 'Radio';
                        } elseif ($defaultdata['input_type'] == 5) {
                            $types = 'Checkbox';
                        } elseif ($defaultdata['input_type'] == 6) {
                            $types = 'Date Picker';
                        }
                        ?>
                        <li class="m-t-20" id="customreportreplace<?= $defaultdata['id'] ?>">
                            <div class="row">
                                <label class="col-sm-4 control-label editdisplayname" data-displayname="custom[<?= $defaultdata['id'] ?>][field_label]"><?= $defaultdata['field_label'] ?></label>
                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <?php if (!$defaultdata['input_type']) { ?>

                                            <textarea class="form-control input-sm checkInput" rows="5" readonly="readonly"></textarea>

                                        <?php } elseif ($defaultdata['input_type'] == 1 || $defaultdata['input_type'] == 7 || $defaultdata['input_type'] == 8) { ?>
                                            <input type='text' class="form-control input-sm checkInput" readonly="readonly">


                                        <?php } elseif ($defaultdata['input_type'] == 2) { ?>

                                            <select class="form-control input-sm checkInput" >
                                                <?php
                                                echo "<option value=''>-Select-</option>";
                                                $opData = json_decode($defaultdata['field_values'], true);
                                                $opData_new = $opData;
                                                $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                $opData = empty($opData) ? $opData_new : $opData;
                                                foreach ($opData AS $opkey => $opvalue) {
                                                    echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                }
                                                ?>
                                            </select>

                                        <?php } elseif ($defaultdata['input_type'] == 3) { ?>

                                            <select class="form-control input-sm checkInput" multiple>
                                                <?php
                                                $opData = json_decode($defaultdata['field_values'], true);
                                                $opData_new = $opData;
                                                $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                $opData = empty($opData) ? $opData_new : $opData;
                                                foreach ($opData AS $opkey => $opvalue) {
                                                    echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                }
                                                ?>
                                            </select>               

                                        <?php } elseif ($defaultdata['input_type'] == 4) { ?>
                                            <?php
                                            $opData = json_decode($defaultdata['field_values'], true);
                                            $opData_new = $opData;
                                            $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                            $opData = empty($opData) ? $opData_new : $opData;
                                            foreach ($opData AS $opkey => $opvalue) {
                                                //echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                echo "<div class='radio'><label><input value='" . $opvalue . "'  type='radio' name='" . $defaultdata['field_name'] . "'><i class='input-helper'></i>" . $opvalue . "</label></div>";
                                            }
                                            ?>
                                        <?php } elseif ($defaultdata['input_type'] == 5) { ?>
                                            <?php
                                            $opData = json_decode($defaultdata['field_values'], true);
                                            $opData_new = $opData;
                                            $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                            $opData = empty($opData) ? $opData_new : $opData;
                                            foreach ($opData AS $opkey => $opvalue) {
                                                //echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                echo "<div class='checkbox'><label><input value='" . $opvalue . "'  type='checkbox'><i class='input-helper'></i>" . $opvalue . "</label></div>";
                                            }
                                            ?>
                                        <?php } else if ($defaultdata['input_type'] == 6) { ?>
                                            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="<?= $defaultdata['field_id'] ?>" data-id="<?php echo $defaultdata['field_id']; ?>" onclick="showdate(this)" name="movie[release_date]" value="" class="form-control input-sm checkInput" readonly="true">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-2 text-right">
                                    <div class="row">
                                        <div  class="col-sm-4 p-r-0">
                                            <a style="cursor: move;"><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                        </div>
                                        <?php if ($defaultdata['field_name'] == 'confirm_password') { ?>
                                            <div id="movecustomreport<?= $defaultdata['id'] ?>" class="col-sm-4 text-right" data-name="<?= $defaultdata['field_label'] ?>" data-field_name='<?= $defaultdata['field_name'] ?>' data-id="<?= $defaultdata['id'] ?>" data-tempid="<?= $defaultdata['id'] ?>" data-type="<?= $types ?>" onclick="movetofieldcolumn(this)">
                                                <a href="javascript:void(0)" title="Remove from content type"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                            </div>
                                        <?php } ?>
                                        <?php if ($defaultdata['field_name'] == 'name') { ?>
                                            <div id="movecustomreport<?= $defaultdata['id'] ?>" class="col-sm-4 text-right" data-name="<?= $defaultdata['field_name'] ?>" data-id="<?= $defaultdata['id'] ?>" data-tempid="<?= $defaultdata['id'] ?>" data-type="<?= $types ?>" onclick="editfield(this, 1)">
                                                <a href="javascript:void(0)" title="Edit"><i class="icon-pencil h4" aria-hidden="true"></i></i></a>
                                            </div>
                                        <?php } ?>
                                        <?php if (!$defaultdata['is_default']) { ?>
                                            <div id="movecustomreport<?= $defaultdata['id'] ?>" class="col-sm-4 text-right" data-name="<?= $defaultdata['field_label'] ?>" data-id="<?= $defaultdata['id'] ?>" data-tempid="<?= $defaultdata['id'] ?>" data-type="<?= $types ?>" onclick="movetofieldcolumn(this)">
                                                <a href="javascript:void(0)" title="Remove from content type"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="custom[<?= $defaultdata['id'] ?>][id]" value="<?= $defaultdata['id'] ?>">
                            <input type="hidden" name="custom[<?= $defaultdata['id'] ?>][field_name]" value="<?= $defaultdata['field_name'] ?>">
                        </li>
                        <?php
                    }
                } else {
                    ?> 
                    <li class="m-t-20" id="no-record">
                        <div class="row">
                            <div class="col-sm-6">
                                No Record found!!!
                            </div>
                        </div>
                    </li>
                <?php }
                ?> 
            </ul>
            <div class="m-t-40"></div>        
        </div>

        <?php if (!empty($template_data) || !empty($defaultField)) { ?>
            <div class="row" id="save_btn">
                <div class="col-sm-12 text-center m-t-40 m-b-20">
                    <?php if (!empty($defaultField)) { ?>
                        <input type="hidden" name="data_available" id="data_available" value="1">
                    <?php } ?>
                    <button type="button" class="btn btn-primary" onclick="SaveCustomField(this)">Save</button>
                </div>
            </div>
        <?php } ?>
    </form>
</div>
<div class="col-sm-5 p-r-0">
    <div class="border-dotted">
        <div class="row">
            <form action="javascript:void(0);" method="post" name="metadatafieldform" id="metadatafieldform">
                <h3 class="col-sm-12 text-capitalize f-300">Available Fields</h3>
                <div class="col-sm-12 m-b-20">
                    <label><button type="button" class="btn btn-default-with-bg" onclick="openinmodal('/userfeature/AddNewField');">Add Custom Field</button></label>
                </div>
                <?php
                if (isset($template_data) && !empty($template_data)) {
                    $count = 1;
                    foreach ($template_data as $template) {
                        ?>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5"><p><?php echo $template['field_label']; ?></p></div>
                                <div class="col-sm-4">
                                    <?php
                                    if ($template['input_type'] == 0) {
                                        $type = 'Textarea';
                                    } elseif ($template['input_type'] == 1) {
                                        $type = 'Textfield';
                                    } elseif ($template['input_type'] == 2) {
                                        $type = 'Dropdown';
                                    } elseif ($template['input_type'] == 3) {
                                        $type = 'List';
                                    } elseif ($template['input_type'] == 4) {
                                        $type = 'Radio';
                                    } elseif ($template['input_type'] == 5) {
                                        $type = 'Checkbox';
                                    } elseif ($template['input_type'] == 6) {
                                        $type = 'Date Picker';
                                    }
                                    echo $type;
                                    ?>
                                    <div style="display: none;" id="customfieldreplace<?php echo $template['id']; ?>">
                                        <li class="m-t-20" id="customreportreplace<?php echo $template['id']; ?>">
                                            <div class="row">
                                                <label class="col-sm-4 control-label editdisplayname" data-displayname="custom[<?= $template['id'] ?>][field_label]"><?php echo $template['field_label']; ?></label>
                                                <div class="col-sm-6">
                                                    <div class="fg-line">
                                                        <?php if (!$template['input_type']) { ?>

                                                            <textarea class="form-control input-sm checkInput" rows="5" readonly="readonly"></textarea>

                                                        <?php } elseif ($template['input_type'] == 1 || $template['input_type'] == 7 || $template['input_type'] == 8) { ?>
                                                            <input type='text' class="form-control input-sm checkInput" readonly="readonly">

                                                        <?php } elseif ($template['input_type'] == 2) { ?>

                                                            <select class="form-control input-sm checkInput" >
                                                                <?php
                                                                echo "<option value=''>-Select-</option>";
                                                                $opData = json_decode($template['field_values'], true);
                                                                $opData_new = $opData;
                                                                $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                                $opData = empty($opData) ? $opData_new : $opData;
                                                                foreach ($opData AS $opkey => $opvalue) {
                                                                    echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                                }
                                                                ?>
                                                            </select>

                                                        <?php } elseif ($template['input_type'] == 3) { ?>

                                                            <select class="form-control input-sm checkInput" multiple>
                                                                <?php
                                                                $opData = json_decode($template['field_values'], true);
                                                                $opData_new = $opData;
                                                                $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                                $opData = empty($opData) ? $opData_new : $opData;
                                                                foreach ($opData AS $opkey => $opvalue) {
                                                                    echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                                }
                                                                ?>
                                                            </select>               

                                                        <?php } elseif ($template['input_type'] == 4) { ?>
                                                            <?php
                                                            $opData = json_decode($template['field_values'], true);
                                                            $opData_new = $opData;
                                                            $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                            $opData = empty($opData) ? $opData_new : $opData;
                                                            foreach ($opData AS $opkey => $opvalue) {
                                                                //echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                                echo "<div class='radio'><label><input value='" . $opvalue . "'  type='radio' name='" . $template['field_name'] . "'><i class='input-helper'></i>" . $opvalue . "</label></div>";
                                                            }
                                                            ?>
                                                        <?php } elseif ($template['input_type'] == 5) { ?>
                                                            <?php
                                                            $opData = json_decode($template['field_values'], true);
                                                            $opData_new = $opData;
                                                            $opData = (array_key_exists($enable_lang, $opData)) ? $opData[$enable_lang] : $opData['en'];
                                                            $opData = empty($opData) ? $opData_new : $opData;
                                                            foreach ($opData AS $opkey => $opvalue) {
                                                                //echo "<option value='" . $opvalue . "'>" . $opvalue . "</option>";
                                                                echo "<div class='checkbox'><label><input value='" . $opvalue . "'  type='checkbox'><i class='input-helper'></i>" . $opvalue . "</label></div>";
                                                            }
                                                            ?>
                                                        <?php } else if ($template['input_type'] == 6) { ?>
                                                            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="<?php echo $template['field_id']; ?>" data-id="<?php echo $template['field_id']; ?>" onclick="showdate1(this)" name="movie[release_date]" value="" class="form-control input-sm checkInput"  readonly="true">
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 text-right">
                                                    <div class="row">
                                                        <div  class="col-sm-4 p-r-0">
                                                            <a style="cursor: move;"><i class="icon-cursor-move h4 p-r-10 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                                        </div>

                                                        <div id="movecustomreport<?php echo $template['id']; ?>" class="col-sm-4 text-right" data-name="<?php echo $template['field_label']; ?>" data-field_name="<?php echo $template['field_name']; ?>" data-id="<?php echo $template['id']; ?>" data-tempid="<?= $template['id'] ?>" data-type="<?php echo $type; ?>" onclick="movetofieldcolumn(this)">
                                                            <a href="javascript:void(0)" title="Remove from content type"><i class="icon-close h4 m-t-0 m-b-0" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="custom[<?= $template['id'] ?>][id]" value="<?= $template['id'] ?>">
                                            <input type="hidden" name="custom[<?= $template['id'] ?>][field_name]" value="<?= $template['field_name'] ?>">
                                        </li>
                                    </div>
                                </div>
                                <div  class="col-sm-1 text-right" data-id="<?php echo $template['id']; ?>" data-name="<?= $template['field_label'] ?>" data-type="<?php echo $type; ?>" data-ftype='custom' onclick="movetoformcolumn(this)"><a href="javascript:void(0)" title="Add Field"><i class="icon-plus h4"  aria-hidden="true"></i></a></div>
                                <?php if ($template['field_name'] != 'confirm_password') { ?>
                                    <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="editfield(this)"><a href="javascript:void(0)" title="Edit field"><i class="icon-pencil h4"  aria-hidden="true"></i></a></div>
                                <?php } ?>
                                <div class="col-sm-1 text-right" data-id="<?= $template['id'] ?>" onclick="deletefield(this)"><a href="javascript:void(0)" title="Delete field"><i class="icon-close h4"  aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                        <?php
                        $count++;
                    }
                }
                ?>
                <span id="adddivtoavailable"></span>
            </form>
        </div>
    </div>
</div>
<!--<div class="row">
    <span id="adddivtoavailable"></span>
</div>-->

