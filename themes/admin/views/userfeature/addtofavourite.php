<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal" class="update_favourite" name="update_favourite" id="update_favourite" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/updatefavourite" method='post'>
            <input type="hidden" name="favliststatus" id="favliststatus" value="<?php echo ($status == 1)?$status:0; ?>" />
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($status == 1)?'checked':''; ?> name="addfav" id="addfav" class="content">
                        <i class="input-helper"></i>  Add to Favorites
                    </label>
                </div>
            </div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="button" class="btn btn-primary" data-confirm="<?php echo ($status == 1)?'Deactive':'Activate'; ?>" name="favbtn" id="favbtn" value="Confirm" />
                </div>
            </div>
        </form>
    </div>        
</div>
<script type="text/javascript">
    var favliststatus;
    $(document).ready(function(){
       $("#favbtn").click(function(){
            if($("#addfav").prop('checked')){
                favliststatus = 1;
            }else{
                favliststatus = 0;
            }
            $("#favliststatus").val(favliststatus);
            var confirm = $(this).attr('data-confirm');
            var title = confirm+" Add to Favorites?";
            var message = "Are You Sure to "+confirm+" Add to Favorites?";
            swal({
                title: title,
                text: message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function() {
                $('#update_favourite').submit();
            });
       });
    });
</script>

