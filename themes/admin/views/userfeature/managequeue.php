
<div class="row m-t-20">
    <div class="col-sm-12">
        <form class="form-horizontal" class="update_favourite" name="update_queue" id="update_queue" action="<?php echo Yii::app()->getBaseUrl(true) ?>/userfeature/manageQueue" method='post'>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" <?php echo ($autoplay_enable == 1)?'checked':''; ?> name="autoPlay" id="autoPlay" class="content">
                        <i class="input-helper"></i>  Autoplay next episode
                    </label>
                </div>
            </div>
            <div class="form-group hide">                
                <div class="col-md-12">                  
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" name="related_content" value="1" <?php echo ($related_content_enable == 1)?'checked':''; ?> />
                        <i class="input-helper"></i>  Related content
                    </label>
                </div>
            </div>
            <div class="form-group">                
                <div class="col-md-12">                  
                    <input type="submit" class="btn btn-primary" name="queuebtn" id="queuebtn" value="save" />
                </div>
            </div>
        </form>
    </div>        
</div>


