<style>
.cat_cov{display:none;} 
.cat_cov .btn-group{width:100%;}
.cat_cov .btn{width:100% !important;text-align:left;}
</style>
<?php 
if($type == 'edit'){
    if($credit_rule->is_allcontent == 1){ ?>
    <style>
    .cat_cov{display:none;}  
    .content_cov{display:none;} 
    </style>
    <?php }
    if($credit_rule->content_category_id != ''){
    ?>
    <style>
    .cat_cov{display:block;}  
    .content_cov{display:none;} 
    </style>
<?php } } ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Credit Category</label>
    <div class="col-sm-9">
        <div class="fg-line">
             <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name" value="<?php echo ($type == 'edit' && $credit_rule->name != '')?$credit_rule->name:''; ?>">
        </div>
    </div>
  </div>  
  <div class="form-group">
    <label class="control-label col-sm-3">Credit</label>
    <div class="col-sm-9">
        <div class="fg-line">
             <input type="text" class="form-control" id="credit_value" name="credit_value" placeholder="Credit Value" value="<?php echo ($type == 'edit' && $credit_rule->credit_value != '')?$credit_rule->credit_value:''; ?>">
        </div>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-sm-3">Content</label>
    <div class="col-sm-9">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" name="credit_action" id="credit_action" onchange="check_content_type(this);">
                    <option value="1" <?php echo ($type == 'edit' && $credit_rule->content != 1 && $credit_rule->content_category_id == '')?'selected':''; ?>>Individual Content</option>
                     <option value="2" <?php echo ($type == 'edit' && $credit_rule->content_category_id != '')?'selected':''; ?>>Content Category</option>
                     <option value="3" <?php echo ($type == 'edit' && $credit_rule->is_allcontent == 1)?'selected':''; ?>>All Contents</option>
                </select>
            </div>
        </div>
    </div>
  </div>
<?php if($type == 'edit'){?>
<input type="hidden" name="edit" value="yes">
<input type="hidden" name="cid" value="<?php echo $credit_rule->id; ?>" >
<?php } ?>
<div class="content_cov clearfix">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="content"> Select Content: </label>
        <div class="col-sm-9">
             <div class="fg-line">
                <select class="input-sm" data-role="tagsinput" name="content[]" placeholder="Type to add new content" id="content" multiple=""> </select>
             </div>
        </div>
    </div>
</div>
<div class="cat_cov clearfix">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="content_category"> Select Category: </label>
        <div class="col-sm-9">
             <div class="fg-line">
                    <?php foreach($content_cat as $key => $val){   
                        if(in_array($val['content_id'],$sel_cat)){
                            $checked = 'checked';
                        }else{
                            $checked = '';
                        }
                        ?>
                    <div class="col-sm-6">
                           <label class="col-md-9 control-label"><?php echo ucfirst($val['name']);?></label>   
                           <div class="col-sm-3">
                               <div class="checkbox">
                                  <label>
                                       <input class="singlechk" name="content_category[]" value="<?php echo $val['content_id']; ?>" type="checkbox" <?php echo $checked; ?>>
                                       <i class="input-helper"></i>
                                  </label>
                               </div>
                           </div>
                    </div>
                    <?php } ?>
             </div>
        </div>
    </div>
</div>
   <div class="form-group">
    <label class="control-label col-sm-3"></label>
    <div class="col-sm-9">
        <div class="fg-line">
            <button type="button" id="add_rule" class="btn btn-primary" onclick="javascript:add_credit_category()"><?php echo ($type == 'edit')?'Update Category':'Add Category'; ?></button>
        </div>
    </div>
  </div>
<script type="text/javascript">
  var sel_contents_categories  = '<?php echo json_encode($sel_cat); ?>';
  var sel_contents = '<?php echo json_encode($sel_content); ?>';
</script>
    
<script typr="text/javascript">
    var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/creditContents", filter: function (e) {
         return e;
     }}});
     content.clearPrefetchCache(),
     content.initialize(),
     $("#content").tagsinput({
         itemValue: function(item) {
             return item.content_id;
         },
         itemText: function(item) {
             return item.name;
         },
         typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
    // var sel_contents = '';
     var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
     if (contnts.length) {
         for (var i in contnts) {
             if (parseInt(contnts[i].content_id)) {
                 $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": contnts[i].name});
             }
         }
     }
       $(".bootstrap-tagsinput ").removeClass('col-md-8');

    if ($('.auto-size')[0]) {
         autosize($('.auto-size'));
     }  
     $(".bootstrap-tagsinput").css({"width": "100%"});    
 //for content_categories  
  
     function check_content_type(data){
        var value = data.value;
        if(value == 1){
             $(".content_cov").show();
             $(".cat_cov").hide();           
        }else if(value == 2){
                 $(".content_cov").hide();
                 $(".cat_cov").show();               
        }else if(value == 3){
                  $(".content_cov").hide();
                 $(".cat_cov").hide();            
        }
     }
    $(document).ready(function() {
        $('#content_cat').multiselect({
            //enableFiltering: true,
            includeSelectAllOption: false,
           // maxHeight: 300,
           // dropUp: true,
             nonSelectedText:'Select Content Category',
        });
    });    
</script>

