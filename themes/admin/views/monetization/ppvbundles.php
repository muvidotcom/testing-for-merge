<?php 

    $selcontents = '';
    if (@$content) {
        $selcontents = json_encode($content);
    }
?>
<div class="row m-t-10 m-b-40">
    <div class="col-md-12">
        <div class="row">
             
    <div class="col-xs-12">
        <button type="button" class="btn btn-primary m-t-10" onclick="addEditCategory(this);" data-id="">New PPV Bundle</button>
    </div>
 
            <?php if (isset($data) && intval($data)) { ?>
            <div id="ppv_content" class="col-md-12">
               
               
                
                <div class="Block m-t-40">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                                <em class="icon-settings icon left-icon "></em>
                        </div>
                        <h4>Manage PPV Bundles</h4>
                    </div>
                    <hr/>
                  
                        <div>
                            <?php 
                            if (isset($data) && !empty($data)) {
                                  
                                foreach ($data as $key => $value) {
                                  
                                    ?>
                             <div class="row m-b-10" id="main_single_<?php echo $value['id'];?>">
                                
                                    <div class="col-md-12">
                                        <div class="border-solid padding  m-b-10" style="border: 2px solid #e5e5e5 !important" >
                                            <!--style="border: 1px solid #a19797 !important;";-->
                                            <div class="row">
                                            
                                <div class="col-sm-6">
                                    <label><?php echo $value['title'];?></label>
                                </div>
                                <div class="col-sm-3 text-left"> 
                                    
                                </div>
                                 <div class="col-sm-3 text-right"> 
                                    <a href="javascript:void(0);" data-type="1" data-id_ppv="<?php echo $value['id'];?>" onclick="addEditCategory(this);" class="text-black">
                                        <em class="icon-pencil"></em>&nbsp;Edit
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="javascript:void(0);" data-manage="<?php echo $value['id'];?>" data-type="<?php echo $value['id'];?>" data-name="<?php echo $value['title'];?> category" class="text-black" onclick="showConfirmPopupdeleteBundle(this);">
                                        <em class="icon-trash"></em>&nbsp;Delete
                                    </a>
                                </div>
                             </div>
                                       
                                      <div class="divider"></div>
                                         <div class="row">
                                      
                                        <div class="col-sm-6" style="border-right: 1px solid #e5e5e5;">
                                         <?php /* ?>    <div class="row m-b-10">
                                                <div class="col-sm-3">
                                                    Price 
                                                </div>
                                                <div class="col-sm-3">
                                                    <?php
                                                    echo  $symbolofcurrency . $value['subscribe_price']?>
                                                </div>
                                            </div><?php */ ?>
                                            <?php 
                                          
                                            if(isset($value['timeframename']) && !empty($value['timeframename'])){ ?>
                                           <!-- <hr style="border-top: dotted 1px rgb(171, 162, 162) !important; margin-top: 5px !important;
margin-bottom: 5px !important;" />-->
                                            <div class="col-sm-4">Time Frame Price </div>
                                            <div class="col-sm-4">Non-Subscribers </div>
                                            <div class="col-sm-4">Subscribers </div>
                                            <div class="clearfix"></div>
                                           <hr style="border-top: dotted 1px rgb(171, 162, 162) !important; margin-top: 5px !important; margin-bottom: 5px !important;" />
                                            
                                            <?php foreach($value['timeframename'] as $key=>$tname){ ?>
                                            <div class="">
                                                <div class="col-sm-4"> 
                                                     <?php
                                                   echo $tname['validity_days'].'  days'.':';?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?php
                                                    $currency_idppvbundles=$tname['currency_id'];
                                                    $symbolofcurrency = Currency::model()->findByPk($currency_idppvbundles)->symbol;
                                                    echo $symbolofcurrency . $tname['price_for_unsubscribed'];?>
                                                </div>
                                                <div class="col-sm-4">
                                                <?php echo $symbolofcurrency . $tname['price_for_subscribed'];?>
                                            </div>
                                            </div> &nbsp;
                                            <?php } } ?>
                                            
                                        </div>
                                             <div class="col-sm-6">
                                                  <?php foreach($value['contentname'] as $key=>$tcontent){ ?>
                                                 
                                                 <span class="tag label label-info"> <?php   echo $tcontent['name']; ?> </span>&nbsp;&nbsp;&nbsp;
                                                 
                                                  <?php } ?>
                                                 
                                                 
                                             </div>    
                                             
                                             
                                       </div>
                                        <?php 
                                        $cnt++;
                                        if ($cnt != count($value['pricing'])) { ?>
                                      
                                        <?php } ?>
                                       
                                        
                                      
                                        </div>  
                                    </div>
                                  </div>
                                <?php  } ?>
                            <?php } ?>
                        </div>
                
                   
        </div>
    </div>
            
            <?php } else { ?>
            <div class="col-md-12 m-t-30 red">
               No plan added yet!
            </div>
            <?php } ?>
            
</div>
</div>
    </div>
<div class="loader text-center" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
<!-- modal start here -->
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  ppv_modal" name="ppv_modal" id="ppv_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_ppv" name="id_ppv" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="ppvbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<!--<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/autosize.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/new_relese/typeahead.bundle.js"></script>
<script type="text/javascript">
    $(function () {        
        $(".content-status").change(function(){
            if (parseInt($(this).val())) {
                $(this).prop( "checked", true);
            } else {
                $(this).prop( "checked", false);
            }
        });
    });
    
     function showConfirmPopupdeleteBundle(obj) {
       // $("#ppvModal").modal('show');
        var deleteid = $(obj).attr('data-type');
        var name = " "+$(obj).attr('data-name');
        var type='Delete';
        var title = type.charAt(0).toUpperCase() + type.slice(1)+ name+"?";
        var text = "Are you sure you want to Delete"+name+"?";
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },
          function(){
              $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/DeletePpvBundle",{'deleteid': deleteid},function(res){
                  if(res){
                    window.location.href ="<?php echo Yii::app()->baseUrl; ?>/monetization/ppvbundles";
                   
               }
                  
              })
          });
        
    }
    
    
    
   
    
    function addEditCategory(obj) {
        var id_ppv = $(obj).attr('data-id_ppv');
       $('.loader').show();
        var type = 1;
       
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/addEditCategoryPPVbundles",{'id_ppv' : id_ppv, 'type' : type},function(res){
            $("#ppvModal").modal('hide');
            $('.loader').hide();
            $("#ppvpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
            
             var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/BundleContents", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                   var item_name = $.trim(item.name);
                    if (item_name) {
                        item_name = item_name.replace("u0027", "'");
                    }
                    return item_name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    var content_name = $.trim(contnts[i].name);
                    if (content_name) {
                        content_name = content_name.replace("u0027", "'");
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": content_name});
                    }
                    
                }
            }
            
            $(".bootstrap-tagsinput ").removeClass('col-md-8');
            
            if ($('.auto-size')[0]) {
                autosize($('.auto-size'));
            }
            
            
            
        });
    }
    
    function deletePPV(obj) {
        $("#id_ppv").val($(obj).attr('data-manage'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/"+type+"PPV";
            
        $('#ppv_modal').attr("action", action);
        document.ppv_modal.submit();
    }
    
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
</script>