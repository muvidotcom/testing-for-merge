<table class="table tablesorter">
    <thead>
        <th>Currency</th>
        <th>Currency Code</th>
        <th>Action</th>
    </thead>
    <tbody>
        <?php
            if(isset($data) && !empty($data)){
                foreach ($data as $value) { ?>
                    <tr>
                        <td><?php echo $value['title'];?></td>
                        <td><?php echo $value['code'].' ('.$value['symbol'].')';?></td>
                        <td>
                            <?php if(!$value['is_default']){?>
                                    <h5><a onclick="makedefault(<?php echo $value['scs_id'];?>);" href="javascript:void(0);">
                                        <i class="icon-pencil"></i>&nbsp; Make Default
                                    </a></h5>
                            <?php }else{?>
                                    <h5><a></a></h5>
                            <?php }?>
                        </td>
                    </tr>

        <?php   }
            }else{
                echo '<tr><td colspan="3">No Record found!!!</td></tr>';
            }
        ?>
    </tbody>
</table>