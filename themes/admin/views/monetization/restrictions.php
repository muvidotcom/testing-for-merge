<?php //echo $free_month;?>
<?php
$studio = $this->studio;
?>
<div id="success_msg"></div>
<div class="row m-t-40">
    <div class="col-md-10">
        <form class="form-horizontal" role="form" id="frmMS">
            <div class="loading" id="subsc-loading"></div>
            
            <div class="form-group">     
                <div class="col-md-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="need_login" name="need_login" value="<?php echo ($studio->need_login == 0) ? 0 : 1?>" <?php echo ($studio->need_login == 0)?' checked="checked"':''?> /> <i class="input-helper"></i>Don&rsquo;t require Login or Registration</label>
                </div>
                </div>
            </div>
            
            <div class="form-group">   
                <div class="col-md-4">
                <div class="checkbox">
                    <label for="view_restriction"> <input type="checkbox" id="view_restriction" name="view_restriction" value="1" <?php if($studio->limit_video)echo 'checked';?> /> 
                    <i class="input-helper"></i> Restrict to number of views</label> 
                </div>
                </div>
                <div class="col-md-8">
                    <div class="fg-line">
                        <input type="text" name="limit_video" id="view_restriction_text" class="form-control input-sm" value="<?php if($studio->limit_video)echo $studio->limit_video;?>" <?php if(!$studio->limit_video){ echo 'disabled';}?> />
                    </div>
                </div>
            </div>
            <div class="form-group">   
                <div class="col-md-4">
                <div class="checkbox">
                    <label for="ppv_view_restriction"> <input type="checkbox" id="ppv_view_restriction" name="ppv_view_restriction" value="1" <?php if($ppvValidity->validity_period)echo 'checked';?> />
                    <i class="input-helper"></i> Restrict PPV content to duration (hours)</label> 
                </div>
                </div>
                <div class="col-md-8">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="ppv_limit_video" id="ppv_view_restriction_text" value="<?php if (isset($ppvValidity) && !empty($ppvValidity) && $ppvValidity->validity_period) { echo $ppvValidity->validity_period; } ?>" onkeypress="return isNumberKey(event)" <?php if(!$ppvValidity->validity_period){ echo 'disabled';}?> /> 
                    </div>
                    <p style="color: #aaa;">(applicable to a single content, NOT entire season or show of multi-part content)</p>
                     <div id="error"></div>
                </div>
            </div>
            
            
<!--            <div class="form-group">    
                <div class="col-md-4">
                    <div class="checkbox">
                       <label for="default_currency"> <input type="checkbox" id="default_currency" name="default_currency" value="1" <?php if($studio->default_currency_id)echo 'checked';?>  disabled="true"/> 
                        <i class="input-helper"></i>Default Currency</label> 
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="fg-line">
                        <div class="select">
                            <select name="default_currency_id" class="form-control input-sm">
                                <?php foreach ($currency as $key => $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if ($value['id'] == $studio->default_currency_id) { ?> selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                                <?php } ?>
                            </select>
                         </div>
                    </div>
                </div>
            </div>-->
            
            <div class="form-group">   
                <div class="col-md-12">
                <div class="checkbox">
                   <label for="geoblock"> <input type="checkbox" id="geoblock" name="geoblock" value="1" <?php echo ($geoblock['active'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block</label>
                </div>
                </div>
            </div>
            <div id="div_geoblock" <?php echo ($geoblock['active'] != 1)?'style="display: none;"':''?>>
                <div class="form-group">
                    <div class="col-md-12">
                        <p class="red">Your content is available worldwide by default. You can limit to specific countries using the following options</p>
                    </div>
                </div>
                <div class="form-group">   
                    <div class="col-md-12">
                    <div class="checkbox">
                       <label for="geoblock_active"> <input type="checkbox" id="geoblock_active" name="geoblock_active" value="1" <?php echo ($geoblock['website'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block entire website</label>
                    </div>
                    </div>
                </div> 
                <div id="subsc_frm" <?php echo ($geoblock['website'] != 1)?'style="display: none;"':''?>>
                    <!--<div class="form-group">
                        <div class="col-md-12">
                            <p class="red">Your website is available worldwide by default. You can limit it to specific countries by selecting the above checkbox and selecting countries below</p>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <div class="col-md-5">
                            <h4>Available in Countries</h4>
                            <div class="fg-line">
                                <div class="">
                                    <select name="available_country" id="available_country" class="left_country form-control" multiple="multiple" size="8">
                                        <?php
                                        if(count($countries) > 0)
                                        {
                                            foreach($countries as $coun)
                                            {
                                                if(!in_array($coun['code'], $restricted_countries))
                                                {
                                                ?>
                                        <option value="<?php echo $coun['code']?>"><?php echo $coun['country']?></option>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div> 
                            </div>

                        </div>
                        <div class="col-md-2 text-center">
                            <h4>&nbsp;</h4>
                            <input type="button" class="add btn btn-primary m-t-30 m-b-10" value=" >> " /><br />
                            <input type="button" class="remove btn btn-default" value=" << " /> 
                             <h4>&nbsp;</h4>
                        </div>
                        <div class="col-md-5">
                            <h4>Not Availabe in Countries</h4>
                            <div class="fg-line">
                                <div class="">
                                    <select id="selected_countries" name="selected_countries[]" class="right_country form-control" multiple size="8">
                                        <?php
                                        if(count($countries) > 0)
                                        {
                                            foreach($countries as $coun)
                                            {
                                                if(in_array($coun['code'], $restricted_countries))
                                                {
                                                ?>
                                        <option value="<?php echo $coun['code']?>" selected="selected"><?php echo $coun['country']?></option>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>                            
                                    </select>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">   
                    <div class="col-md-12">
                    <div class="checkbox">
                       <label for="geoblock_content"> <input type="checkbox" id="geoblock_content" name="geoblock_content" value="1" <?php echo ($geoblock['content'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block Specific content</label>
                    </div>
                    </div>
                </div>
                <div id="subcontent_frm" <?php echo ($geoblock['content'] != 1)?'style="display: none;"':''?>>
                    <div class="form-group">
                        <div class="col-md-12">
                            <p class="red">Select this if you want to limit some, NOT all, content to a few specific countries. Define categories such as OnlyUSA, OnlyAsia etc...You can then set these geo-block categories from "Manage Content" screen.</p>
                        </div>
                    </div>
                    <div id="contentwise_country">
                        <?php
                        $countindex = 0;
                        foreach($grestcat as $k=>$v){
                            $this->renderPartial('geoblock_country', array('studio' => $this->studio->id, "countries" => $countries, "restricted_countries" => $restricted_content_countries[$v['id']], "category_name" => $v['category_name'], "cnt" => $countindex,'categoryid'=>$v['id']));
                            $countindex++;
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <a onclick="addEditCategory(this);" class="grey" href="javascript:void(0);">
                                <i class="icon-plus"></i>&nbsp; Add new geo-block category
                            </a>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="form-group">
                <div class=" col-md-8 m-t-30">
                     <button type="submit" id="updatebtn" class="btn btn-primary btn-sm">Update</button>
                </div>
            </div> 
            
        </form>        
        
    </div>
</div>
<div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>


<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.selso.js"></script>
<script type="text/javascript">
$(function(){
        // sorts all the combo select boxes
        function sortBoxes(){			
                $('select.left_country, select.right_country').find('option').selso({
                        type: 'alpha', 
                        extract: function(o){ return $(o).text(); } 
                });

                // clear all highlighted items
                $('select.left_country').find('option:selected').removeAttr('selected');
        }

        // add/remove buttons for combo select boxes
        $('input.add').click(function(){
                var left = $(this).parent().parent().find('select.left_country option:selected');
                var right = $(this).parent().parent().find('select.right_country');
                right.append(left);
                sortBoxes();	
        });

        $('input.remove').click(function(){
                var left = $(this).parent().parent().find('select.left_country');
                var right = $(this).parent().parent().find('select.right_country option:selected');
                left.append(right);
                var right = $(this).parent().parent().find('select.right_country option');
                right.prop('selected', true);
                sortBoxes();
        });
        if($('#view_restriction').is(':checked')){
            $('#ppv_view_restriction').attr('disabled','disabled');
        }else if($('#ppv_view_restriction').is(':checked')){
            $('#view_restriction').attr('disabled','disabled');
        }
});
</script>  
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
    $(document).ready(function(){
        $( "#geoblock_active" ).change(function() {
            $( "#subsc_frm" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
        $( "#geoblock" ).change(function() {
            $( "#div_geoblock" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
        $( "#geoblock_content" ).change(function() {
            $( "#subcontent_frm" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
    });
    
$(document).ready(function(){
    $('#subsc-loading').hide();
    $("#frmMS").validate({         
        submitHandler: function(form) {
            //console.log($('#frmMS').serialize());
            var ppv_view_restriction_text = $.trim($('#ppv_view_restriction_text').val());
            if($('#ppv_view_restriction').is(':checked')){
                if(ppv_view_restriction_text !='' && parseInt(ppv_view_restriction_text) >= 12){
                    $('#error').html('');
                }else{
                    $('#error').css('color','red');
                    $('#error').html('PPV duration cannot be less than 12 hours.');
                    return false;
                }
            }
                $('#error').html('');
                $.ajax({
                    url: "<?php echo Yii::app()->baseUrl; ?>/monetization/saverestrictions",
                    data: $('#frmMS').serialize(),
                    type:'POST',
                    dataType: "json",
                    beforeSend:function(){
                        $('#updatebtn').attr('disabled','disabled');
                        $('#updatebtn').html("Saving");
                        $('#subsc-loading').show();
                    },
                    complete:function(){
                        $('#subsc-loading').hide();
                    },
                    success:function(){
                        $('html').animate({scrollTop: 0}, 800);
                        $('.pace').prepend('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>&nbsp;Restirctions updated successfully.</div>');
                        setTimeout(function () {
                            window.location = "<?php echo Yii::app()->baseUrl; ?>/monetization/restrictions";
                            return false;
                        }, 2000);
                    }
                });
        }
    });
});
function isNumberKey(e){
    var unicode = e.charCode ? e.charCode : e.keyCode;
    if ((unicode !== 8) && (unicode !== 9)) {
        if (unicode >= 48 && unicode <= 57)
            return true;
        else
            return false;
    }
}
$('#ppv_view_restriction_text').on("contextmenu",function(event) {
    return false;
});

</script>
<script>
    $('#view_restriction').click(function(){
        if($(this).is(':checked')){
            $('#view_restriction_text').removeAttr('disabled','disabled');
            $('#ppv_view_restriction').removeAttr('checked');
            $('#ppv_view_restriction').attr('disabled','disabled');
            $('#ppv_view_restriction_text').attr('disabled','disabled');
        }else{
            $('#view_restriction_text').val('');
            $('#view_restriction_text').attr('disabled','disabled');
            $('#ppv_view_restriction').removeAttr('disabled','disabled');
            var status = $('#ppv_view_restriction_text').attr('disabled');
            if(status != 'disabled'){
                $('#ppv_view_restriction_text').removeAttr('disabled','disabled');
            }
        }
    });
    $('#ppv_view_restriction').click(function(){
        if($(this).is(':checked')){
            $('#ppv_view_restriction_text').removeAttr('disabled','disabled');
            $('#view_restriction').attr('disabled','disabled');
        }else{
            $('#ppv_view_restriction_text').val('');
            $('#ppv_view_restriction_text').attr('disabled','disabled');
            $('#view_restriction').removeAttr('disabled');
        }
    });
    function addEditCategory(obj) {
        var cnt = 0;
        if ($(".geoblock_country").length > 0) {
            cnt = $(".geoblock_country").length;
        }
        $.post("/monetization/AddEditRestictions",{'cnt':cnt},function(res){            
            $("#ppvpopup").html(res).modal('show');
        });
    }
    function delcontentdiv(divid,delid){
        swal({
            title: "Delete Category ?",
            text: "Do you want to remove this category ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            if(delid){
                $.post("/monetization/DeleteRestictions",{'delid':delid},function(res){                
                });
            }
            $('#contentdiv'+divid).slideUp("slow", function() { $(this).remove();});
        });        
    }
    function editcontentdiv(divid,category_name,category_id){
        var selectvalue = $('#contentdiv'+divid).find("select").serialize();
        $.post("/monetization/AddEditRestictions",{'cnt':divid,'category_name':category_name,'category_id':category_id,'selectvalue':selectvalue},function(res){            
            $("#ppvpopup").html(res).modal('show');
        });
    }
</script>
<style>
    .geoblock_country{
        //border: 1px solid #aaa;
        margin: 0 0 20px 20px;
        padding: 10px;
    }
</style>