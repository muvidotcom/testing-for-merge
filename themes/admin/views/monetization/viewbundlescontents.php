<style type="text/css">
    .m-t-15{margin-top: 15px;margin-left: 5px;}
</style>
<?php


    $selcontents = '';
    if (@$content) {
        $selcontents = json_encode($content);
    }
?>
<script>
    sel_contents = '<?php echo $selcontents;?>';
</script>
<?php 
$disable = "";

if (!empty($res)) {
        if ($language_id == $res[0]['language_id']) {
            if ($res[0]['parent_id'] > 0) {
                $disable = "disabled";
            }
        } else {
            $disable = "disabled";
        }
    }

?>
<div class="modal-dialog">
    <form action="javascript:void(0);" class="form-horizontal" method="post" name="subscription_form" id="subscription_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php if (isset($plan_id) && !empty($plan_id)){echo 'Edit';} else {echo 'Add';} ?> Subscription Bundles Content Of Plan-<?php echo $plan_name; ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="errorTxt"></div>
                    </div>
                </div>
                <input type="hidden" name="plan_id" value="<?php echo $plan_id;  ?>">
                <div class="form-group">
                                    <label class="col-sm-4" for="content">Content: </label>
                                    <div class="col-sm-8">

                                        <div class="fg-line">
                                            <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" multiple>
                                            </select>
                                        </div>
                                        <small class="help-block">
                                            <label id="data[content]-error" class="error red" for="data[content][]" style="display: none"></label>
                                        </small>
                                    </div>
                                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" onclick="return validateSubscriptionForm();" id="sub-btn"><?php if (isset($plan_id) && !empty($plan_id)){echo 'Update';} else {echo 'Submit';} ?></button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>


<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    
    
    function validateSubscriptionForm() {
        var validate = $("#subscription_form").validate({
            rules: {
                'data[name]': {
                    required: true
                },
                'data[recurrence]': {
                    required: true
                },
                'data[price][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                }
            },
            messages: {
                'data[name]': {
                    required: 'Please enter plan name'
                },
                'data[recurrence]': {
                    required: 'Please enter subscription period'
                },
                'data[price][]': {
                    required: 'Please enter subscription fee',
                    allrequired: 'Please enter subscription fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                }
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        if (x) {
             if (!$.trim($("#content").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                return false;
            }
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/ViewsubscriptionsContents";
            $('.rmvdisable').removeAttr('disabled');
            document.subscription_form.action = url;
            document.subscription_form.submit();
        }
    }
        
jQuery.validator.addMethod("allrequired", function (value, element) {
    var isTrue = 1;
    $("#sub_price_box").find(".statuschk").each(function(){
        if ($(this).is(':checked')) {
            var price = $(this).parent().parent().parent().parent().next('div').find('.subscription_fee').val();
            if (price === '') {
                isTrue = 0;
                return false;
            }
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Please enter subscription fee");

jQuery.validator.addMethod("price", function (value, element) {
    var isTrue = 1;
    $("#sub_price_box").find(".statuschk").each(function(){
        if ($(this).is(':checked')) {
            var price = $(this).parent().parent().parent().parent().next('div').find('.subscription_fee').val();
            
            price = $.trim(price);
            price = Math.floor(price * 100) / 100;
            price = Math.round(price);
            if (price <= 0) {
                isTrue = 0;
                return false;
            }
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Minimum price should be 50 cent");


  
</script>


 