<div class="geoblock_country" id="contentdiv<?php echo $cnt;?>">
    <div class="form-group">
        <div class="pull-left"><?php echo $category_name;?> &nbsp; <a href="javascript:void(0);" onclick="editcontentdiv(<?php echo $cnt;?>,'<?php echo $category_name;?>','<?php echo $categoryid;?>')"><i class="icon-pencil" aria-hidden="true"></i></a></div>
        <div class="pull-right"><a class="grey" href="javascript:void(0);" onclick="delcontentdiv(<?php echo $cnt;?>,'<?php echo $categoryid;?>')"><i class="icon-trash" aria-hidden="true"></i> Delete Geo-block Category</a></div>
        <input type="hidden" name="catname[<?php echo $cnt;?>]" value="<?php echo $category_name;?>">
        <input type="hidden" name="categoryid[<?php echo $cnt;?>]" value="<?php echo $categoryid;?>">
    </div>
    <div class="clearfix"></div>
    <div class="form-group" style="border: 1px solid #aaa;padding-bottom: 20px;">
        <div class="col-md-5">
            <h4>Available in Countries</h4>
            <div class="fg-line">
                <div class="">
                    <select name="selected_countries_content[<?php echo $cnt;?>][]" class="left_country<?php echo $cnt;?> form-control" multiple="multiple" size="8">
                        <?php
                        if (count($countries) > 0) {
                            foreach ($countries as $coun) {
                                if (in_array($coun['code'], $restricted_countries)) {
                                    ?>
                                    <option value="<?php echo $coun['code'] ?>" selected="selected"><?php echo $coun['country'] ?></option>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </select>
                </div> 
            </div>
        </div>
        <div class="col-md-2 text-center">
            <h4>&nbsp;</h4>
            <input type="button" class="add<?php echo $cnt;?> btn btn-default m-t-30 m-b-10" value=" >> " /><br />
            <input type="button" class="remove<?php echo $cnt;?> btn btn-primary" value=" << " /> 
            <h4>&nbsp;</h4>
        </div>
        <div class="col-md-5">
            <h4>Not Availabe in Countries</h4>
            <div class="fg-line">
                <div class="">
                    <select name="not_available_country<?php echo $cnt;?>"  class="right_country<?php echo $cnt;?> form-control" multiple size="8">
                        <?php
                        if (count($countries) > 0) {
                            foreach ($countries as $coun) {
                                if (!in_array($coun['code'], $restricted_countries)) {
                                    ?>
                                    <option value="<?php echo $coun['code'] ?>"><?php echo $coun['country'] ?></option>
                                    <?php
                                }
                            }
                        }
                        ?>                            
                    </select>   
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        // sorts all the combo select boxes
        function sortBoxes() {
            $('select.left_country<?php echo $cnt;?>, select.right_country<?php echo $cnt;?>').find('option').selso({
                type: 'alpha',
                extract: function (o) {
                    return $(o).text();
                }
            });

            // clear all highlighted items
            $('select.right_country<?php echo $cnt;?>').find('option:selected').removeAttr('selected');
        }

        // add/remove buttons for combo select boxes
        $('input.add<?php echo $cnt;?>').click(function () {
            var left = $(this).parent().parent().find('select.left_country<?php echo $cnt;?> option:selected');
            var right = $(this).parent().parent().find('select.right_country<?php echo $cnt;?>');
            right.append(left);
            var left = $(this).parent().parent().find('select.left_country<?php echo $cnt;?> option');
            left.prop('selected', true);
            sortBoxes();
        });

        $('input.remove<?php echo $cnt;?>').click(function () {
            var left = $(this).parent().parent().find('select.left_country<?php echo $cnt;?>');
            var right = $(this).parent().parent().find('select.right_country<?php echo $cnt;?> option:selected');
            left.append(right);
            var right = $(this).parent().parent().find('select.right_country<?php echo $cnt;?> option');
            //right.prop('selected', true);
            sortBoxes();
        });

    });
</script>