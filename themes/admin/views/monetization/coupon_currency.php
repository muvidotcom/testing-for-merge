<div id="coupon_currency_amt_<?php echo $couponType?>_<?php echo $discountType;?>" >
    <div class="form-group">
        <div class="col-md-offset-3 col-md-3">
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm currency" name="data[currency_id][]" disabled="disabled">
                        <?php foreach ($currency as $key => $value) { ?>
                            <option value="<?php echo $value['id']; ?>" <?php if ($studio->default_currency_id == $value['id']) {?>selected="selected"<?php } ?>><?php echo $value['code']; ?>(<?php echo $value['symbol']; ?>)</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fg-line">
                 <input id="amount_value" autocomplete="off" class="form-control input-sm cost multi-cost" <?php if($discountType == 1){?> onKeyPress="handleChange(this)" onKeyUp="handleChange(this)" <?php } ?> type="text" required="required" name="data[amount_value][]" placeholder="Value" >
            </div>
        </div>
        
    </div>
   
</div>

<p class="col-md-offset-3 col-md-9">
    <a href="javascript:void(0);" onclick="addMore('<?php echo $couponType;?>_<?php echo $discountType;?>');" class="text-gray">
        <em class="icon-plus"></em>&nbsp; Add more price for specific country
    </a>
</p>
   

<script>
    $('.cost').on("keypress",function(event) {
        return decimalsonly(event);
    });

    $('.cost').on("contextmenu",function(event) {
        return false;
    });
    function handleChange(input) {
        if (input.value < 0) input.value = 0;
        if (input.value > 100) input.value = 100;
    }
    function addMore(divId) {
        var isTrue = 0;
        $("#coupon_currency_amt_" + divId).find(".multi-cost").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        
        if (parseInt(isTrue)) {
            var str = $("#coupon_currency_dup_field").html();
            $("#coupon_currency_amt_" + divId).append(str);
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        }
    }
       function removeBox(obj) {
        $(obj).parent().parent().remove();
    }
</script>