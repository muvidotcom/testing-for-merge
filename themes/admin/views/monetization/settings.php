<?php
$paypalexpress =0;
if(isset($subscriptiondata) && count($subscriptiondata) == 1 && $subscriptiondata[0]['is_primary'] == 0 && $subscriptiondata[0]['short_code'] == 'paypalpro'){
	$paypalexpress=1;
}

$isMonetizationEnabled = 0;

$payment_gateway=Yii::app()->common->canTakeSubscription();
$is_subscription=$payment_gateway['is_subscription'];
$is_ppv=$payment_gateway['is_ppv'];

if(Yii::app()->common->canTakeSubscription() ||  $paypalexpress == 1) {
	$isMonetizationEnabled = 1;
       
}
if(isset($config) && !empty($config))
{   
    $is_checked = $config['config_value']; 
} else {
    $is_checked = 1; 
}
?><div class="loader text-center centerLoader" style="display:none">
    <div class="preloader pls-blue">
        <svg viewBox="25 25 50 50" class="pl-circular">
        <circle r="20" cy="50" cx="50" class="plc-path"/>
        </svg>
    </div>
</div>

<?php  if ($isMonetizationEnabled) {?>
	<div class="row">
        <div class="col-sm-12">
			<div class="alert alert-success alert-dismissable flash-msg m-t-20" style="display:none">
                <i class="icon-check"></i> &nbsp;
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                <span id="success-msg"></span>
            </div>
            <div class="row m-t-40">
                <div class="col-md-12">
                    <h3>Multiple Currency Setup</h3>
                </div>
                <div class="col-md-4">
                
                    <div class="input-group">
                        <span class="input-group-addon p-l-0"><em class="fa fa-money"></em></span>
                        <div id="input-error">
                            <div class="fg-line">
                                <input class="search form-control input-sm auto" placeholder="Search by currency name or code" />
                                <input type="hidden" id="curr_id" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="input-group">
                        <div class="fg-line">
                            <a href="javascript:void(0)" class="btn btn-default" onclick="addcurrency()">Add</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="error" style="display: none">
                <div class="form-group input-group">
                    <div class="col-md-12">
                        <div class="p-l-40" id="error-txt">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row m-t-20">
                <div class="col-md-12">
                    <div id="currency_list"></div>
                </div>
                <div class="loader text-center centerLoader" style="display:none">
                    <div class="preloader pls-blue">
                        <svg viewBox="25 25 50 50" class="pl-circular">
                        <circle r="20" cy="50" cx="50" class="plc-path"/>
                        </svg>
                    </div>
                </div>
            </div>
		</div>
    </div>
<?php } ?> 
        
        <div class="col-sm-12">
            <h3>Monetization Model</h3>
        <?php if(isset($popup) && $popup) {?>
            <p class="m-b-20">Select the monetization model for your platform. You can change this later from Monetization > Settings.</p>
        <?php }else{?>
            <p class="m-b-20 red">If you are changing these settings after your platform goes live, please submit a <a>support ticket</a> first.</p>
        <?php }?>
            <form class="form-horizontal" method="POST" id="monetization_form">
                <input type="hidden" name="hidden_settings" value="0" />
				<?php if ($is_subscription){?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="subscription" value="1" <?php echo (isset($data) && ($data['menu'] & 1)) ? 'checked' : ''?> /><i class="input-helper"></i>Subscription
                            </label>
                        </div>
                    </div>
                </div>
                <?php }?>
                <?php  if ($is_ppv == 1){?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="ppv" name="ppv" value="2" <?php echo (isset($data) && ($data['menu'] & 2)) ? 'checked' : ''?> /><i class="input-helper"></i>Pay-per-view
                            </label>
                        </div>
                    </div>
                </div>
                <?php }?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="advertising" value="4" <?php echo (isset($data) && ($data['menu'] & 4)) ? 'checked' : ''?> /><i class="input-helper"></i>Advertising
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="freecontent" value="8" <?php echo (isset($data) && ($data['menu'] & 8)) ? 'checked' : ''?> /><i class="input-helper"></i>Free Content (Previews/Clips are free by default. Select this if you will have some main content for free)
                            </label>
                        </div>
                    </div>
                </div>
				 <?php if ($is_ppv == 1){?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="pre-order" name="advancepurchase" value="16" <?php echo (isset($data) && ($data['menu'] & 16)) ? 'checked' : ''?> /><i class="input-helper"></i>Pre-Order
                            </label>
                        </div>
                    </div>
                </div>
                  <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="payperviewbundles" name="payperviewbundles" value="64" <?php echo (isset($data) && ($data['menu'] & 64)) ? 'checked' : ''?> /><i class="input-helper"></i>Pay-Per-View Bundles
                            </label>
                        </div>
                    </div>
                 </div><?php }?>
                <?php  if ($isMonetizationEnabled) {?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="coupon" name="coupons" value="32" <?php echo (isset($data) && ($data['menu'] & 32)) ? 'checked' : ''?> /><i class="input-helper"></i>Coupons
                            </label>
                        </div>
                    </div>
                </div>
				<?php } ?>
				
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="voucher" name="voucher" value="128" <?php echo (isset($data) && ($data['menu'] & 128)) ? 'checked' : ''?> /><i class="input-helper"></i>Voucher
                            </label>
                        </div>
                    </div>
                </div>
                <?php if($this->PAYMENT_GATEWAY['stripe']=='stripe') {?>
                <?php  if($data['menu'] & 1){?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="credits" name="credits" value="256" <?php echo (isset($data) && ($data['menu'] & 256)) ? 'checked' : ''?> /><i class="input-helper"></i>Credits
                            </label>
                        </div>
                    </div>
                </div> 
                <?php } ?>
                <?php } ?>
				<?php if (in_array($studio->id,array(2626,4248))) { ?>
<!--                <div class="form-group">
                    <div class="col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="subscriptionbundles" name="subscriptionbundles" value="256" <?php echo (isset($data) && ($data['menu'] & 256)) ? 'checked' : ''?> /><i class="input-helper"></i>Subscription Bundles
                            </label>
                        </div>
                    </div>
                </div>-->
				<?php } ?>
                <!--
                            <div class="form-group">
                <?php if(isset($popup) && $popup) {?>
                                    <div class="col-sm-12 m-t-30">
                    <?php }else{?>
                                    <div class="col-sm-8 m-t-30">
                    <?php }?>
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                
                                </div>
                            </div>-->
            </form>
            
            
            <!--Restriction Section starts--> 
            
            <div id="success_msg"></div>
            <div class="row m-t-40">
                
                <div class="col-md-10">
                    <h3>Restriction</h3>
					<input type="hidden" id="restrict_no_devices" value="<?php echo $restrict_no_devices;?>">
					<input type="hidden" id="limit_devices" value="<?php echo $limit_devices;?>">
                    <form class="form-horizontal" role="form" id="frmMS">
                        <div class="loading" id="subsc-loading"></div>
                        
                        <div class="form-group">     
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="need_login" name="need_login" value="<?php echo ($studio->need_login == 0) ? 0 : 1?>" <?php echo ($studio->need_login == 0)?' checked="checked"':''?> /> <i class="input-helper"></i>Don&rsquo;t require Login or Registration</label>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">     
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="hide_simultaneous_login" name="hide_simultaneous_login" value="<?php echo ($hide_simultaneous_login == 0) ? 0 : 1 ?>" <?php echo ($hide_simultaneous_login == true) ? ' checked="checked"' : '' ?> /> <i class="input-helper"></i>Don&rsquo;t allow simultaneous login on number of devices</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="fg-line">
                                    <input type="text" name="limit_login" id="limit_login" class="form-control input-sm" onkeypress="return isNumberKey(event)" value="<?php echo (@$limit_login > 1) ? @$limit_login : 1 ?>" maxlength="3" onkeyup="remove_error_msg();"/>
                                </div>
								<span class="error red error_notify" id="error_limit_login" style="display:none;"></span>
                            </div>
                        </div>-->
                        <div class="form-group">     
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="login_free_content" name="login_free_content" value="<?php echo ($is_checked == 0) ? 0 : 1 ?>" <?php echo ($is_checked == 1) ?'checked="checked"':'' ?> /> <i class="input-helper"></i>Login required For Free Content</label>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">   
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label for="geoblock"> <input type="checkbox" id="geoblock" name="geoblock" value="1" <?php echo ($geoblock['active'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block</label>
                                </div>
                            </div>
                        </div>
                        <div id="div_geoblock" <?php echo ($geoblock['active'] != 1)?'style="display: none;"':''?>>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <p class="red">Your content is available worldwide by default. You can limit to specific countries using the following options</p>
                                </div>
                            </div>
                            <div class="form-group">   
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label for="geoblock_active"> <input type="checkbox" id="geoblock_active" name="geoblock_active" value="1" <?php echo ($geoblock['website'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block entire website</label>
                                    </div>
                                </div>
                            </div> 
                            <div id="subsc_frm" <?php echo ($geoblock['website'] != 1)?'style="display: none;"':''?>>
                                <!--<div class="form-group">
                                    <div class="col-md-12">
                                        <p class="red">Your website is available worldwide by default. You can limit it to specific countries by selecting the above checkbox and selecting countries below</p>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <h4>Available in Countries</h4>
                                        <div class="fg-line">
                                            <div class="">
                                                <select name="available_country" id="available_country" class="left_country form-control" multiple="multiple" size="8">
                                        <?php
                                        if(count($countries) > 0)
                                        {
                                            foreach($countries as $coun)
                                            {
                                                if(!in_array($coun['code'], $restricted_countries))
                                                {
                                                ?>
                                                    <option value="<?php echo $coun['code']?>"><?php echo $coun['country']?></option>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                                </select>
                                            </div> 
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h4>&nbsp;</h4>
                                        <input type="button" class="add btn btn-primary m-t-30 m-b-10" value=" >> " /><br />
                                        <input type="button" class="remove btn btn-default" value=" << " /> 
                                        <h4>&nbsp;</h4>
                                    </div>
                                    <div class="col-md-5">
                                        <h4>Not Availabe in Countries</h4>
                                        <div class="fg-line">
                                            <div class="">
                                                <select id="selected_countries" name="selected_countries[]" class="right_country form-control" multiple size="8">
                                        <?php
                                        if(count($countries) > 0)
                                        {
                                            foreach($countries as $coun)
                                            {
                                                if(in_array($coun['code'], $restricted_countries))
                                                {
                                                ?>
                                                    <option value="<?php echo $coun['code']?>" selected="selected"><?php echo $coun['country']?></option>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>                            
                                                </select>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">   
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label for="geoblock_content"> <input type="checkbox" id="geoblock_content" name="geoblock_content" value="1" <?php echo ($geoblock['content'] == 1)?' checked="checked"':''?> /><i class="input-helper"></i> Geo-block Specific content</label>
                                    </div>
                                </div>
                            </div>
                            <div id="subcontent_frm" <?php echo ($geoblock['content'] != 1)?'style="display: none;"':''?>>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <p class="red">Select this if you want to limit some, NOT all, content to a few specific countries. Define categories such as OnlyUSA, OnlyAsia etc...You can then set these geo-block categories from "Manage Content" screen.</p>
                                    </div>
                                </div>
                                <div id="contentwise_country">
                        <?php
                        $countindex = 0;
                        foreach($grestcat as $k=>$v){
                            $this->renderPartial('geoblock_country', array('studio' => $this->studio->id, "countries" => $countries, "restricted_countries" => $restricted_content_countries[$v['id']], "category_name" => $v['category_name'], "cnt" => $countindex,'categoryid'=>$v['id']));
                            $countindex++;
                        }
                        ?>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a onclick="addEditCategory(this);" class="grey" href="javascript:void(0);">
                                            <i class="icon-plus" ></i>&nbsp; Add new geo-block category
                                        </a>
                                    </div>
                                </div>                    
                            </div>
                        </div>
                        
                    </form>        
                    
                </div>
            </div>
            <div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
            <div class="form-group">
                <div class=" col-md-8 m-t-30">
                    <button type="button" id="updatebtn" class="btn btn-primary btn-sm">Update</button>
                </div>
            </div> 
        </div>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.selso.js"></script>
<script type="text/javascript">
$(function(){
        // sorts all the combo select boxes
        function sortBoxes(){			
                $('select.left_country, select.right_country').find('option').selso({
                        type: 'alpha', 
                        extract: function(o){ return $(o).text(); } 
                });

                // clear all highlighted items
                $('select.left_country').find('option:selected').removeAttr('selected');
        }

        // add/remove buttons for combo select boxes
        $('input.add').click(function(){
                var left = $(this).parent().parent().find('select.left_country option:selected');
                var right = $(this).parent().parent().find('select.right_country');
                right.append(left);
                sortBoxes();	
        });

        $('input.remove').click(function(){
                var left = $(this).parent().parent().find('select.left_country');
                var right = $(this).parent().parent().find('select.right_country option:selected');
                left.append(right);
                var right = $(this).parent().parent().find('select.right_country option');
                right.prop('selected', true);
                sortBoxes();
        });
});
</script>  
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
    $(document).ready(function(){
        $( "#geoblock_active" ).change(function() {
            $( "#subsc_frm" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
        $( "#geoblock" ).change(function() {
            $( "#div_geoblock" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
        $( "#geoblock_content" ).change(function() {
            $( "#subcontent_frm" ).toggle( "slow", function() {
            // Animation complete.
            });
        });
    });
    
function isNumberKey(e){
    var unicode = e.charCode ? e.charCode : e.keyCode;
    if ((unicode !== 8) && (unicode !== 9)) {
        if (unicode >= 48 && unicode <= 57)
            return true;
        else
            return false;
    }
}
$('#limit_login').on("contextmenu",function(event) {
    return false;
});

    function addEditCategory(obj) {
        var cnt = 0;
        if ($(".geoblock_country").length > 0) {
            cnt = $(".geoblock_country").length;
        }
        $.post("/monetization/AddEditRestictions",{'cnt':cnt},function(res){            
            $("#ppvpopup").html(res).modal('show');
        });
    }
    function delcontentdiv(divid,delid){
        swal({
            title: "Delete Category ?",
            text: "Do you want to remove this category ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            if(delid){
                $.post("/monetization/DeleteRestictions",{'delid':delid},function(res){                
                });
            }
            $('#contentdiv'+divid).slideUp("slow", function() { $(this).remove();});
        });        
    }
    function editcontentdiv(divid,category_name,category_id){
        var selectvalue = $('#contentdiv'+divid).find("select").serialize();
        $.post("/monetization/AddEditRestictions",{'cnt':divid,'category_name':category_name,'category_id':category_id,'selectvalue':selectvalue},function(res){            
            $("#ppvpopup").html(res).modal('show');
        });
    }
</script>
<style>
    .geoblock_country{
        /*border: 1px solid #aaa;*/
        margin: 0 0 20px 20px;
        padding: 10px;
    }
</style>


<script>
    $(function() {
        $('.loader').show();
        $.post('/monetization/getCurrencyList',{},function(res){
            $('#currency_list').html(res);
            $('.loader').hide();
        });
    });
    function addcurrency() {
        $('.loader').show();
        var currency = $('#curr_id').val();
        if(currency.trim()){
            var currency = $('#curr_id').val();
        }else{
            var currency = $('.auto').val();
        }
        if(currency.trim()){
            $.post('/monetization/addCurrency',{'currency':currency},function(res){
                var data = $.parseJSON(res);
                if(data.isCurr){
                    $('#input-error').addClass('has-error');
                    $('#error-txt').addClass('red');
                    $('#error-txt').html(data.error);
                    $('#error').show();
                    $('.loader').hide();
                    setTimeout(function(){
                        $('#input-error').removeClass('has-error');
                        $('#error-txt').html('');
                        $('#error').hide();
                    }, 3000);
                }else{
                    $('.pace').prepend('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>&nbsp;Currency added successfully.</div>');
                    $.post('/monetization/getCurrencyList',{},function(curr){
                        $('#currency_list').html(curr);
                        $('.auto').val('');
                        $('.loader').hide();
                    });
                }
            });
        }else{
            $('#input-error').addClass('has-error');
            $('#error-txt').addClass('red');
            $('#error-txt').html("Currency field can't be blank");
            $('#error').show();
            $('.loader').hide();
            setTimeout(function(){
                $('#input-error').removeClass('has-error');
                $('#error-txt').html('');
                $('#error').hide();
            }, 3000);
        }
    }
    $(function() {
	$(".auto").autocomplete({
		source: "filterCurrency",
		minLength: 3,
                change: function( event, ui ) {
                    if(ui.item){
                        $( '#curr_id' ).val(ui.item.id);
                    }else{
                        $( '#curr_id' ).val('');
                    }
                       
                }
	});				
    });
    
    function makedefault(id) {
        swal({
            title: "Change Default Currency",
            text: 'Are you sure? You want to change your default currency which will affect in all payment sections for your users.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('.loader').show();
                $.post('/monetization/makeDefault',{'id':id},function(res){
                if(res){
                    $.post('/monetization/getCurrencyList',{},function(curr){
                        $('#currency_list').html(curr);
                        $('.loader').hide();
                    });
                }
            });
        });
    }
    
    $('#updatebtn').click(function(){
		//Checking simultaneous login with restrict no of devices is commented for mantis id 7689
		/*if($('#hide_simultaneous_login').prop('checked')){	
			if(parseInt($('#restrict_no_devices').val())>0){
				var limit_login = parseInt($('#limit_login').val());
				if(limit_login > 0){				
					if(limit_login > parseInt($('#limit_devices').val())){							
						$('#error_limit_login').show();
						$('#error_limit_login').html('Number of simultaneous login cannot be greater than number of restricted devices');
						return false;
					}
			   }else{
				   $('#error_limit_login').show();
				   $('#error_limit_login').html('Value should not be less than 1');
				   return false;
			   }				   
			}
		}*/		
        $('.loader').show();
        var action= "<?php echo Yii::app()->getBaseUrl(true); ?>/monetization/settings";
        var monetization = $('#monetization_form').serialize();  
        var res = 0;
        $.post(action, {monetization: monetization}, function(result){
            res = 1;
        });
        var data= $('#frmMS').serialize();
        var url=  "<?php echo Yii::app()->baseUrl; ?>/monetization/saverestrictions";
        $.post(url, {data: data}, function(result){
            res = 2
        });
        $('.loader').hide();
        $('html').animate({scrollTop: 0}, 800);
        $('.pace').prepend('<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>&nbsp;Settings updated successfully.</div>');
        setTimeout(function () {
            location.reload();
            return false;
        }, 2000);
    });
	
	function remove_error_msg(){
		$('#error_limit_login').hide().html('');
	}
</script>    
