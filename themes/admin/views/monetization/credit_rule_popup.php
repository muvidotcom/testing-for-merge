<?php
if($type == 'edit' && $credit_rule->is_automated == 0){ ?>
<style>
.custom_validity{display:block;}
</style>
<?php }else{ ?>
<style>
.custom_validity{display:none;}
</style>
<?php } ?>
<?php if($type == 'edit'){?>
    <input type="hidden" name="edit" id="edit_hidden" value="yes"> 
    <input type="hidden" name="credit_id" value="<?php echo $credit_rule->id; ?>" id="credit_id"> 
<?php } ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Credit Rule</label>
    <div class="col-sm-9">
        <div class="fg-line">
             <input type="text" class="form-control" id="rule_name" name="rule_name" placeholder="Rule Name" value="<?php echo ($type == 'edit' && $credit_rule->rule_name != '')?$credit_rule->rule_name:''; ?>">
        </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3">Credit Action</label>
    <div class="col-sm-9">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" name="credit_action" id="credit_action">
                    <option value="1" <?php echo ($type == 'edit' && $credit_rule->rule_action == 1)?'selected':''; ?>>Subscription Activation</option>
                     <option value="2" <?php echo ($type == 'edit' && $credit_rule->rule_action == 2)?'selected':''; ?>>Subscription renewal</option>
                </select>
            </div>
        </div>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-sm-3">Credit</label>
    <div class="col-sm-9">
        <div class="fg-line">
             <input type="text" class="form-control" id="credit_value" name="credit_value" peholder="Credit Value" value="<?php echo ($type == 'edit' && $credit_rule->credit_value != '')?$credit_rule->credit_value:''; ?>">
        </div>
    </div>
  </div> 
   <div class="form-group">
    <label class="control-label col-sm-3">Plans</label>
    <div class="col-sm-9">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" name="plans" id="plans">
                    <?php foreach($subscription_plan as $key => $val){ ?>
                    <option value="<?php echo $val['id']; ?>" <?php echo ($type == 'edit' && $credit_rule->plan_id == $val['id'])?'selected':''; ?>><?php echo $val['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
  </div>  
   <div class="form-group">
    <label class="control-label col-sm-3">Validity</label>
    <div class="col-sm-9">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" name="validity" id="validity" onchange="setvalidity(this.value)">
                    <option value="1" <?php echo ($type == 'edit' && $credit_rule->is_automated == 1)?'selected':''; ?>>Same as subscription period</option>
                     <option value="0" <?php echo ($type == 'edit' && $credit_rule->is_automated == 0)?'selected':''; ?>>Custom</option>
                </select>
            </div>
        </div>
    </div>
  </div> 
  <div class="custom_validity clearfix">
    <div class="form-group">
      <label class="control-label col-sm-3">Validity in Days</label>
      <div class="col-sm-9">
          <div class="fg-line">
               <input type="text" class="form-control" id="validity_period" name="validity_period" peholder="Validity" value="<?php echo ($type == 'edit' && $credit_rule->is_automated == 0)?$credit_rule->credit_validity:''; ?>" required>
          </div>
      </div>
    </div> 
  </div> 
   <div class="form-group">
    <label class="control-label col-sm-3"></label>
    <div class="col-sm-9">
        <div class="fg-line">
            <button type="button" id="add_rule" class="btn btn-primary" onclick="javascript:add_credit_rule()"><?php echo ($type == 'edit')?'Update Rule':'Add Rule'; ?></button>
        </div>
    </div>
  </div>    
