<?php 
$api_username = $api_password = $api_mode = '';
if (isset($payment_gateway_data) && !empty($payment_gateway_data) && $payment_gateway_data->short_code == 'firstdata') {
    $api_username = $payment_gateway_data->api_username;
    $api_password = $payment_gateway_data->api_password;
    $api_signature = $payment_gateway_data->api_signature;
    $api_mode = $payment_gateway_data->api_mode;
} ?>

<div class="form-group">
    <label class="control-label col-md-4">Merchant ID:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_username]" value="<?php echo $api_username; ?>" required>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">Public Key:</label>                    
        <div class="col-md-8">
            <div class="fg-line">
                <input type="text" class="form-control input-sm" name="data[api_password]" value="<?php echo $api_password; ?>" required>
            </div>
        </div>            
</div>
<div class="form-group">
    <label class="control-label col-md-4">Private Key:</label>                    
        <div class="col-md-8">
            <div class="fg-line">
                <input type="text" class="form-control input-sm" name="data[api_signature]" value="<?php echo $api_signature; ?>" required>
            </div>
        </div>            
</div>
<?php if ((HOST_IP == '52.0.64.95') || (HOST_IP == '127.0.0.1')) { ?>
<div class="form-group">
    <label class="control-label col-md-4">Mode:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" require name="data[api_mode]" id="mode">
                    <option value="sandbox" <?php if ($api_mode == 'sandbox'){ ?>selected="selected"<?php } ?>>Sandbox</option>
                    <option value="live" <?php if ($api_mode == 'live'){ ?>selected="selected"<?php } ?>>Live</option>
                </select>
            </div>
        </div>
    </div>
</div>
<?php } ?>