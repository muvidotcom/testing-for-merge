<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<input type="hidden" id="img_width" name="img_width">
<input type="hidden" id="img_height" name="img_height">
<div id="success"></div>
<?php 
    
    $width = $height = '';
    if($get_invoice_logo){
        $get_invoice_logos = explode('x',$get_invoice_logo->config_value);
        $width = $get_invoice_logos[0];
        $height = $get_invoice_logos[1];
    }
?>
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row" id="logo_section">
            <div class="col-sm-12">
                <div class="Block">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-docs icon left-icon "></em>
                        </div>
                        <h4>Invoice Logo Dimension</h4>                      
                    </div>
                    <hr>
                    <!--<div class="form-group">
                        <div class="col-sm-12">
                            <div id="err_message" class="error red"></div>
                        </div>
                    </div>-->
                    <div class="form-inline">                    
                        <label class="col-sm-2 control-label">Logo Dimension (in px)</label>
                        <div class="col-sm-2">
                            <div class="fg-line">
                                <input name="invoicelogo_width_dimension" id="invoicelogo_width_dimension" value="<?php echo $width; ?>" class="form-control input-sm" type="text" placeholder="Logo Width" />
                            </div>
                            <div id="err_message" class="error red"></div>
                        </div>
                    </div>    
                    <div class="form-inline">
                        <div class="col-sm-2">
                            <div class="fg-line">
                                <input name="invoicelogo_height_dimension" id="invoicelogo_height_dimension" value="<?php echo $height; ?>" class="form-control input-sm" type="text" placeholder="Logo Height" />
                            </div>
                            <div id="err_message1" class="error red"></div>
                        </div>
                    </div>
                    <div class="form-inline">
                        <div class=" col-md-4">
                            <button type="submit" name="invoicelogosettings_btn" id="invoicelogosettings_btn" class="btn btn-primary btn-sm" >Save</button>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row" id="logo_section">
            <div class="col-sm-12">
                <div class="Block">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-docs icon left-icon "></em>
                        </div>
                        <h4>Invoice Logo</h4>                      
                    </div>
                    <hr>
                    <div class="Block-Body">
                        <?php echo $this->renderPartial('logo',array('studio' => $studio,'studio_id' => $studio_id,'dimension' => $dimension, 'all_images' => $all_images,'language_id' => $language_id)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal is-Large-Modal fade" id="homePageModal" tabindex="-1" role="dialog" aria-labelledby="homePageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/monetization/Saveinvoicelogo" id="upload_image_form" method="post" enctype="multipart/form-data">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="section_id1" id="section_id1" value="" />
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li id="show1" role="presentation" class="active" onclick="hide_file()">
                            <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                        </li>
                        <li id="show2" role="presentation" onclick="hide_gallery()"> 
                            <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                    <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('celeb_pic')">
                                    <input id="celeb_pic" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                    <p class="help-block"></p>
                                </div>
                                <input type="hidden" id="x1" name="fileimage[x1]" />
                                <input type="hidden" id="y1" name="fileimage[y1]" />
                                <input type="hidden" id="x2" name="fileimage[x2]" />
                                <input type="hidden" id="y2" name="fileimage[y2]" />
                                <input type="hidden" id="w" name="fileimage[w]"/>
                                <input type="hidden" id="h" name="fileimage[h]"/>
                                <div class="col-xs-12">
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="celeb_preview">
                                            <img id="preview" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                            <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                            <input type="hidden" name="g_original_image" id="g_original_image" />
                            <input type="hidden" id="x13" name="jcrop_allimage[x13]" />
                            <input type="hidden" id="y13" name="jcrop_allimage[y13]" />
                            <input type="hidden" id="x23" name="jcrop_allimage[x23]" />
                            <input type="hidden" id="y23" name="jcrop_allimage[y23]" />
                            <input type="hidden" id="w3" name="jcrop_allimage[w3]" />
                            <input type="hidden" id="h3" name="jcrop_allimage[h3]" />
                            <div class="row  Gallery-Row">
                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">
                                  
                                </div>
                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue  ">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="gallery_preview">
                                            <img id="glry_preview" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="uplad_buton" disabled="disabled">Upload</button>
                <button type="button" class="btn btn-default" id="cancl_upload" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- Progress Bar popup -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
    <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
        <div style="float:left;font-weight:bold;">File Upload Status</div>
        <div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
    </div>
    <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Progress Bar popup end --> 
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/homepage.js?v=<?= RELEASE;?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>
<script>
function manage_progressbar() {
    $("#all_progress_bar").toggle('slow');
}
    $("#hide").click(function(){
    $("#uplad_buton").hide();
    $("#cancl_upload").hide();
});
    
    $("#show1").click(function(){
    $("#uplad_buton").show();
    $("#cancl_upload").show();
});
    $("#show2").click(function(){
    $("#uplad_buton").show();
    $("#cancl_upload").show();
});
    $(document).ready(function(){
        $("#invoicelogo_width_dimension").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#err_message").html("Please Enter Numeric Value").show().fadeOut(5000);
                return false;
            }
        });
        $("#invoicelogo_height_dimension").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#err_message1").html("Please Enter Numeric Value").show().fadeOut(5000);
                return false;
            }
        });
        $('#invoicelogosettings_btn').click(function(){
            var width = $('#invoicelogo_width_dimension').val();
            var height = $('#invoicelogo_height_dimension').val();
            var dataString = 'width='+width+'&height='+height;
            if(width == '' || height == ''){
                swal("Please Enter Logo Dimension");
            }else if(width === '0' || height === '0'){
                swal("Please Enter Exact Logo Dmension");
            }else{
                $.ajax({
                    url: '<?php echo Yii::app()->baseUrl; ?>/monetization/Invoice',
                    type: 'POST',
                    data: dataString,
                    success : function(html){
                        swal("Data has been save sucessfully");
                        location.reload();
                    }
                });
            }
            return false;            
        });
    });
</script>



