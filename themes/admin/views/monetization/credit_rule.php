<p>  Add & edit credit rules for generation of credit  </p>
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-primary small-margin-left" id="show_cred"  onclick="show_creaditpopup()">Add Credit Rule</button>
                <?php if(isset($data) && !empty($data)){  ?>
                    <button class="btn btn-danger small-margin-right pull-right"  onclick="delete_credit()" style="margin-right:15px;">Delete Rules</button>
                <?php } ?>
                <div class="table-responsive" style="margin-top:15px;">
                    <form method="post" name="credit_rule_action" id="credit_rule_action">
                     <input type="hidden" name="studio_id" value="<?php echo $studio_id; ?>" />

                        <?php if(isset($data) && !empty($data)){  ?>
                    <table class="table">
                        <tr>
                            <th>
                            <div class="checkbox m-b-0">
                                <label>
                                    <input class="chkall" id="selectall" name="checkall" onclick="checkUncheckAll();" type="checkbox"><i class="input-helper"></i> 
                                </label>
                            </div>                                
                            </th>
                            <th>Rule</th>
                            <th>Action</th>
                            <th>Credits</th>
                            <th>Type</th>
                            <th>Validity</th>
                            <th></th>
                        </tr>
                         <?php
                          foreach($data as $key => $val){  
                         ?>
                                <tr style="<?php echo ($val['status'] == 0)?'color:rgb(175, 175, 175);':'';?>">
                                    <td>
                                        <div class="checkbox m-b-0">
                                           <label>
                                               <input class="chkind" id="data" name="data[]" value="<?php echo $val['id'];?>"  type="checkbox" onclick="checkUncheckAll(1);"><i class="input-helper"></i> 
                                           </label>
                                       </div>                                
                                    </td>
                                    <td><?php echo $val['rule_name']; ?></td>
                                    <td><?php echo ($val['rule_action'] == 1)?'Subscription Activation':'Subscription Renewal';?></td>
                                    <td><?php echo $val['credit_value']; ?> Credits</td>
                                    <td><?php echo ($val['is_automated'] == 1)?'Automated':'Custom'; ?></td>
                                    <td><?php echo ($val['is_automated'] == 1)?$subscription_details[$val['id']]->frequency.' '.$subscription_details[$val['id']]->recurrence:$val['credit_validity'].' Day(s)'; ?></td>
                                    <td>
                                        <h5><a href="javascript:void(0);"  onclick="edit_creditrule('<?php echo $val['id'];?>');"><i class="fa fa-cog" aria-hidden="true" style="font-size:19px"></i> Edit</a></h5>
                                       <?php if($val['status'] == 1){ ?>
                                        <h5><a href="javascript:void(0);" onclick="disable_credit('<?php echo $val['id'];?>','<?php echo $val['rule_name'];?>')"><i class="icon-ban red"></i> Disable</a></h5>
                                       <?php }else{ ?>
                                         <h5><a href="javascript:void(0);" onclick="enable_credit('<?php echo $val['id'];?>','<?php echo $val['rule_name'];?>')"><i class="icon-check"></i> Enable</a></h5>
                                       <?php } ?>
                                    </td>

                                </tr>
                          <?php } ?> 
                           </table> 
                        <?php  } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="credit_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="padding:5px 10px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h3><span class="content_typ">Add</span> Credit Rule</h3>Define a credit rule to generate credits on an action
      </div>
      <div class="modal-body">
          <form class="form-horizontal credit" name="credit_modal_form" id="credit_modal_form" method="post">
              
          </form>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">  
    function show_creaditpopup(){
        $(".content_typ").html('');
        $(".content_typ").html('Add');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/getcreditPopup";
        $.post(url,function(res){
          $('#credit_modal').modal('toggle'); 
          $("#credit_modal").modal('show');
          $("#credit_modal_form").html(res);
        });
        
    }
</script>
<script type="text/javascript">
     function add_credit_rule(){
        var validity =  $("#validity").val();
    var validate = $("#credit_modal_form").validate({
        rules: {
            rule_name: "required",
            credit_action: "required",
            credit_value: {
                required: true,
                number: true
            },
            validity_period:{
                required: function (element) {
                    if(validity == 0){
                       return true; 
                    }
                },
                number: true,
                max: 999,
                min: 1
            },
            plans: "required",

        },
        messages: {
            rule_name: "Please enter rule name",
            credit_action: "Please select credit action",
            credit_value: "Please enter credit value",
            credit_value: {
                required: "Please enter credit value",
                number: "Please enter valid credit value"
            },
            validity_period:{
                required : "Please enter validity in days",
                 number: "Please enter valid number"
            },
            plans: "Please select plan",
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
            
        }
    });  
    var is_valid = validate.form();  
        if(is_valid){
            var type_val = $("#edit_hidden").val();
            if(type_val == 'yes'){
                var type = 'edit';
            }else{
                var type = 'add';
            }
            var check_plan = checkplan(type);
            $("#add_rule").attr("disabled", true);
            $("#add_rule").html("Please wait...");
            if(check_plan == 1){
                var url_save = "<?php echo Yii::app()->baseUrl; ?>/monetization/savecreditRule";
                var data = $("#credit_modal_form").serialize();
                $.post(url_save,{'data':data},function(res){
                    if(res){
                         location.reload();
                     }
                });
            }else{
                swal({
                  title: 'Validate',
                  text: 'Plan already exists',
                  type: "warning",
                  showCancelButton: false,
                  confirmButtonColor: "#ff0000",customClass: "confirmButtonTextColor primary",
                  confirmButtonText: "Ok",
                  closeOnConfirm: true,
                  html:true,
                });  
             $("#add_rule").attr("disabled", false);
            
               if(type_val == 'yes'){
                   $("#add_rule").html("Update Rule");
               }else{
                    $("#add_rule").html("Add Rule");
               }           
            }
                
            
        }
    } 

   function checkUncheckAll(arg) {
    if (parseInt(arg)) {
        $(".chkind").prop('click', function () {
            if ($(this).is(':checked')) {
                var len = $(".chkind").length;
                var ind_len = 0;
                $(".chkind").each(function () {
                    if ($(this).is(':checked')) {
                        ind_len++;
                    }
                });

                if (parseInt(len) === parseInt(ind_len)) {
                    $(".chkall").prop("checked", true);
                }
            } else {
                $(".chkall").prop("checked", false);
            }
        });
    } else {
        $(".chkall").prop('click', function () {
            if ($(this).is(':checked')) {
                $(".chkind").prop("checked", true);
            } else {
                $(".chkind").prop("checked", false);
            }
        });
    }
}
 function delete_credit(){
    var check = CheckForm();
    if(check){
        swal({
            title: 'Warning',
            text: 'Are you sure you want to delete this credit rule',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var data = $("#credit_rule_action").serialize();
              var del_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/deletecreditRule"
              $.post(del_url,{'data':data},function(res){
                  location.reload();
              });
          })
    }else{
        swal({
            title: 'Validate',
            text: 'Please select credit rule for delete',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor primary",
            confirmButtonText: "Ok",
            closeOnConfirm: true,
            html:true,
          });       
    }
 }
 function CheckForm(){
	var checked=false;
	var elements = document.getElementsByName("data[]");
	for(var i=0; i < elements.length; i++){
	if(elements[i].checked)
		checked = true;
	}
	return checked ;
}
function edit_creditrule(id){
        $(".content_typ").html('');
        $(".content_typ").html('Edit');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/getcreditPopup";
        $.post(url,{'id':id,'edit':'yes'},function(res){
          $('#credit_modal').modal('toggle'); 
          $("#credit_modal").modal('show');
          $("#credit_modal_form").html(res);
        });
}
function disable_credit(id,name){
         swal({
            title: 'Disable Credit Rule',
            text: 'Are you sure you want to disable <b>'+name+'</b> ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var disable_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/disableEnableCreditRule";
              $.post(disable_url,{'id':id,'name':name,'type':'disable'},function(res){
                  location.reload();
              });
          });  
}
function enable_credit(id,name){
         swal({
            title: 'Enable Credit Rule',
            text: 'Are you sure you want to enable <b>'+name+'</b> ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var disable_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/disableEnableCreditRule";
              $.post(disable_url,{'id':id,'name':name,'type':'enable'},function(res){
                  location.reload();
              });
          });  
}
function setvalidity(data){
   if(data == 0){
       $(".custom_validity").show();
   }else{
       $(".custom_validity").hide();
   }
}
function checkplan(type){
    var plan_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/checkPlanExist";
    var plan_id = $("#plans").val();
    var action = $("#credit_action").val();
    rule_id = '';
    if(type == 'edit'){
        var rule_id = $("#credit_id").val();
    }
    var return_val = 1;
     $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'url': plan_url,
        'data': {'type':type,'plan_id':plan_id,'action':action,'rule_id':rule_id},
        'success': function (res) {
            if(res == 0){
                return_val  = 0;
           }else{
               return_val  = 1;
           }
        }
    });
    return return_val;  
}
</script>