<?php 
$api_short_code = $api_username = $api_password = $api_signature = $api_mode = '';
if (isset($payment_gateway_data) && !empty($payment_gateway_data) && $payment_gateway_data->short_code == 'paypal' || $payment_gateway_data->short_code == 'paypalpro') {
    $api_username = $payment_gateway_data->api_username;
    $api_password = $payment_gateway_data->api_password;
    $api_signature = $payment_gateway_data->api_signature;
    $api_mode = $payment_gateway_data->api_mode;
    $api_short_code = $payment_gateway_data->short_code;
} ?>

<!--<div class="row form-group">
    <div class="col-lg-10">
        <div class="col-lg-5">
            <input type="radio" name="data[short_code_paypal]" class="short_code_paypal" value="paypalpro" required <?php if(isset($api_short_code) && $api_short_code == 'paypalpro'){ echo 'checked disabled';}elseif(isset($api_short_code) && $api_short_code == 'paypal'){echo 'disabled';}?> />
            <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->baseUrl.'/images/paypalpro.jpg'; ?>">
            (Recommended)
        </div>
        <div class="col-lg-5">
            <input type="radio" name="data[short_code_paypal]" class="short_code_paypal" value="paypal" required <?php if(isset($api_short_code) && $api_short_code == 'paypal'){ echo 'checked disabled';}elseif(isset($api_short_code) && $api_short_code == 'paypalpro'){echo 'disabled';}?> />
           <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->baseUrl.'/images/paypalexpress.jpg'; ?>">
        </div> 
    </div>              
</div>-->

<div class="form-group">
    
    <label class="control-label col-sm-4">API Username:</label>                    
    <div class="col-sm-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_username]" value="<?php echo $api_username; ?>">
        </div>
    </div>              
</div>
<div class="form-group">
    
    <label class="control-label col-sm-4">API Password:</label>                    
    <div class="col-sm-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_password]" value="<?php echo $api_password; ?>">
        </div>
    </div>              
</div>
<div class="form-group">
    
    <label class="control-label col-sm-4">API Signature:</label>                    
    <div class="col-sm-8">
        <div class="fg-line">
            <input type="text" class="form-control input-sm" name="data[api_signature]" value="<?php echo $api_signature; ?>">
        </div>
    </div>              
</div>


<?php if ((isset($is_default) && intval($is_default)) || (HOST_IP == '52.0.64.95') || (HOST_IP == '127.0.0.1')) { ?>
    <div class="form-group">
    <label class="control-label col-sm-4">Mode:</label>                    
        <div class="col-sm-8">
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm" require name="data[api_mode]" id="mode">
                        <option value="sandbox" <?php if ($api_mode == 'sandbox'){ ?>selected="selected"<?php } ?>>Sandbox</option>
                        <option value="live" <?php if ($api_mode == 'live'){ ?>selected="selected"<?php } ?>>Live</option>
                    </select>
                </div>
            </div>
        </div>              
    </div>
<?php } ?>