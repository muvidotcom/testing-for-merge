
<div class="modal-dialog">
    <form action="javascript:void(0);" method="post" name="category_form" id="category_form" data-toggle="validator" novalidate="novalidate" class="form-horizontal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <?php if ($category_name) { ?>
                        Edit Category
                    <?php } else { ?>
                        Add Category
                    <?php } ?>
                </h4>
            </div>
            <div class="modal-body" style="max-height: 161px; overflow-y: auto; overflow-x: hidden;">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="Category Name">Category Name:</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" name="category_name" id="category_name" value="<?php echo $category_name; ?>" class="form-control input-sm">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="studio_id" value="<?php echo $studio; ?>">
                <input type="hidden" name="cnt" id="cntpopup" value="<?php echo $cnt; ?>">
                <input type="hidden" name="selectvalue" value="<?php echo $selectvalue; ?>">
                <input type="hidden" name="category_id" value="<?php echo $category_id; ?>">
                <button type="submit" class="btn btn-primary" id="bill-btn">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#category_form").validate({
            rules: {
                category_name: {
                    required: true
                }
            },
            messages: {
                category_name: {
                    required: "Please enter category name"
                }
            },
            submitHandler: function (form) {

                //console.log($('#reset_pass').serialize());
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/monetization/AddGeoCategory",
                    data: $('#category_form').serialize(),
                    type: 'POST',
                    beforeSend: function () {
                        //$('#reset-loading').show();
                    },
                    success: function (data) {
                        $("#ppvpopup").modal('hide');
                        var ct = $('#cntpopup').val();
                        if ($("#contentdiv"+ct).length > 0) {
                            $("#contentdiv"+ct).replaceWith(data);
                        } else {
                            $("#contentwise_country").before(data);
                        }
                    }
                });
            }
        });
    });
</script>