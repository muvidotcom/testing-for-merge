<?php 
$api_username = $api_password = $api_mode = '';
if (isset($payment_gateway_data) && !empty($payment_gateway_data) && $payment_gateway_data->short_code == 'stripe') {
    $api_username = $payment_gateway_data->api_username;
    $api_password = $payment_gateway_data->api_password;
    $api_mode = $payment_gateway_data->api_mode;
} ?>

<div class="form-group">
    <label class="control-label col-md-4">Secret Key:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" id="secret_key" class="form-control input-sm" name="data[api_username]" value="<?php echo $api_username; ?>" required onkeyup="changeMode();" />
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">Public Key:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" id="public_key" class="form-control input-sm" name="data[api_password]" value="<?php echo $api_password; ?>" required onkeyup="changeMode();" />
        </div>
    </div>
</div>
<?php if ((HOST_IP == '52.0.64.95') || (HOST_IP == '127.0.0.1')) { ?>
<div class="form-group">
    <label class="control-label col-md-4">Mode:</label>                    
    <div class="col-md-8">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm" require name="data[api_mode]" id="mode">
                    <option value="sandbox" <?php if ($api_mode == 'sandbox'){ ?>selected="selected"<?php } ?>>Sandbox</option>
                    <option value="live" <?php if ($api_mode == 'live'){ ?>selected="selected"<?php } ?>>Live</option>
                </select>
            </div>
        </div>
    </div>
</div>
<?php } else if (stristr($_SERVER['HTTP_HOST'], "muvi.com")) { ?>
<input type="hidden" name="data[api_mode]" id="stripapimode" value="<?php echo $api_mode;?>" />
<?php } ?>

<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script>-->
<script type="text/javascript">
    function changeMode() {
        if ($("#stripapimode").length) {
            $("#stripapimode").val('live');

            var secret_key = $.trim($("#secret_key").val());
            var public_key = $.trim($("#public_key").val());

            if (secret_key !== '' && public_key !== '') {
                if (secret_key.indexOf("sk_test_") !== -1) {
                    if (public_key.indexOf("pk_test_") !== -1) {
                        $("#stripapimode").val('sandbox');
                    }
                }
            }
        }
    }
</script>