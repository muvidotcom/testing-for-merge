<style type="text/css">
    .small-box{
        width: 100%;
        display: -webkit-flex; /* Safari */
        display: flex;
    }
    .small-box div {
        -webkit-flex: 1;  /* Safari 6.1+ */
        -ms-flex: 1;  /* IE 10 */    
        flex: 1;
    }
    .payment_logos{cursor: pointer;}
    .cb25{clear: both;height: 25px;}
    .bold{
        font-weight: 600;
    }
    .error-box{
        display:none;
    }
    
col-xs-5ths,.col-sm-5ths,.col-md-5ths,.col-lg-5ths {
    position: relative;
    min-height: 1px;
    padding-right: 10px;
    padding-left: 10px;
}

.col-xs-5ths {
    width: 20%;
    float: left;
 margin-top:20px;
}

@media (min-width: 768px) {
    .col-sm-5ths {
        width: 20%;
        float: left;
    }
}

@media (min-width: 992px) {
    .col-md-5ths {
        width: 20%;
        float: left;
  margin-top:30px;
    }
}

@media (min-width: 1200px) {
    .col-lg-5ths {
        width: 20%;
        float: left;
    }
</style>
<?php

if(isset(Yii::app()->user->lastname) && @Yii::app()->user->lastname != ''){
    $fullname = Yii::app()->user->first_name.' '.Yii::app()->user->lastname;
}else{
    $fullname = Yii::app()->user->first_name;
}
if (isset($data['0']['short_code']) && !empty($data['0']['short_code'])) {
    $short_code = $data['0']['short_code'];
} else {
    $short_code = '';
}

$is_payment_gateway = 1;
if ($short_code == 'manual') {
    $is_payment_gateway = 0;
}

$isNotPurchased = 0;
if (isset($studio) && !empty($studio) && $studio->status == 4 && $studio->is_subscribed == 0 && $studio->is_deleted == 0 && $studio->is_default == 0) {//Canceled
    $isNotPurchased = 1;
}  else if (isset($studio) && !empty($studio) && $studio->status == 0 && $studio->is_subscribed == 0 && $studio->is_deleted == 1 && $studio->is_default == 0) {//Deleted
    $isNotPurchased = 1;
}

if(isset($data['0']) && count($data['0'])>1){
?>
<input type="hidden" value="<?php echo count($data['0']);?>" id="is_exist" />
<input type="hidden" value="<?php echo $data['0']['api_mode'];?>" id="api_mode" />
<?php }?>

 <?php if (intval($isNotPurchased)) { ?>
    <div class="row m-t-40">
        <div class="col-md-12">
            <h4><span class="bold red">A paid subscription is required to set a Payment Gateway.</span><a href="<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/payment-gateway/1"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
        </div>
    </div>
<?php } ?>
<div class="row m-t-40">
    <div class="col-sm-12">
        <h3>Select Your Payment Gateway</h3>
        <p>Add a support <a href="../ticket/addTicket">ticket</a> if your gateway not listed here.</p>
        <div class="error-box">
            <div class="error-inner-box red">Incorrect payment gateway information</div>
        </div>
    </div> 
    <div class="col-md-12" id="gateway-body">
       
        <form action="javascript:void(0);" class="form-horizontal" name="payment_gateway_form" id="payment_gateway_form" onsubmit="return validatePaymentGatewayForm();" autocomplete="false" method="post" >
                
                    <div class="row">
                        <?php 
                        $default_gateway_id = '';
                        foreach ($payment_gateways as $key => $value) {
                            if ($value->short_code == 'authorizenet') {
                                $default_gateway_id = $value->id;
                            }
                            if($short_code == 'paypalpro'){
                                $temp_short_code = 'paypal';
                            }else{
                                $temp_short_code = $short_code;
                            }
                            ?>
                            <div class="payment_logos col-xs-5ths col-xs-6 text-center <?php echo 'cls_'.$value->short_code;?>" data-id="<?php echo $value->id;?>" data-short_code="<?php echo $value->short_code;?>" data-is_test_transactions="<?php  if(isset($is_test_transactions) && !empty($is_test_transactions)){echo 1;}?>" data-is_payment_gateway="<?php echo $value->is_payment_gateway;?>" <?php if (!isset($data['0']['gateway_id'])) { ?>  onclick="showGateways(this);" <?php } ?>>
                                <div class="default_logo" <?php if ($temp_short_code == $value->short_code){ ?> style="display: none;"<?php } ?>>
                                    <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->baseUrl.'/images/'.$value->short_code.'_black.jpg'; ?>" class="img-responsive">
                                </div>
                                <div class="original_logo" <?php if ($temp_short_code == $value->short_code){ ?> style="display: block;"<?php } else { ?>style="display: none;"<?php } ?>>
                                    <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->baseUrl.'/images/'.$value->short_code.'.jpg'; ?>" class="img-responsive">
                                </div>
                            </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="errorTxt red"></div>
                        </div>
                    </div>
                    
                    <input type="hidden" value="<?php if (isset($data['0']['gateway_id']) && !empty($data['0']['gateway_id'])) {echo $data['0']['gateway_id'];} else { echo $default_gateway_id; } ?>" name="data[gateway_id]" id="gateway_id"/>
                    <input type="hidden" value="<?php echo $short_code; ?>" name="data[short_code]" id="short_code"/>
                    <input type="hidden" value="1" name="data[dprp_status]" id="paypal_dprp_status"/>
                    
                    <div id="loader" style="display: none;text-align: center">
                        <div class="preloader pls-blue">
                            <svg viewBox="25 25 50 50" class="pl-circular">
                                <circle r="20" cy="50" cx="50" class="plc-path"/>
                            </svg>
                        </div>
                    </div>
                    
                    <div class="row m-t-20" id="instafeez-payment-gateway" style="display:none">
                        <div class="col-sm-12">
                            <p><b>Test a Transaction</b></p>
                            <p>Clicking on "Integrate Payment Gateway" will charge a small amount &#8377; 1.01 to your credit card to verify that the
                            gateway is correctly integrated.</p>
                        </div>
                    </div>
                    
                    <div id="payment_desc" class="col-md-10" style="display: none;"></div>
                    
                    <div class="form-group" id="manual_desc" style="display: none;">
                        <div class="col-md-12">
                            You have selected manual process. Please submit the form.
                            
                        </div>
                    </div>
                    
                    <div class="form-group" id="sbmt-div" style="display: none;">
                        <div class="col-md-10">
                            <div class="col-md-offset-4 col-md-8 m-t-30">
                                <button type="submit" id="integrate-payment-gateway" class="btn btn-primary btn-sm" <?php if (intval($isNotPurchased)) { ?>disabled<?php }?>>Integrate Payment Gateway</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 red" id="api-error">
                            
                        </div>
                    </div>
            </form>
    </div>
</div>
<div class="row m-t-20" id="transaction-body" style="display:none">
    <div class="col-sm-12">
        <p><b>Test a Transaction</b></p>
        <p>Your gateway information is now added. Conduct a sample transaction on your credit card to complete the integration.
        Clicking on "Complete Integration" will charge a small amount of one dollar ($1.00) to your credit card to verify that the
        gateway is correctly integrated.</p>
    </div>
     <?php
    $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
    ?>
    <div class="col-sm-8">
        <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);" autocomplete="false">
    <div class="form-group">
        <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />        
        <label class="control-label col-sm-4">Name on Card:</label>    
        <div class="col-sm-8">
            <div class="fg-line">
                <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
            </div>
        </div>                     
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Card Number:</label>    
        <div class="col-sm-8">
            <div class="fg-line">
                <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" />
            </div>
        </div>                     
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Expiry Date:</label>    
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-6">
                    <div class="fg-line">
                        <div class="select">
                            <select name="exp_month" id="exp_month" class="form-control input-sm">
                                <option value="">Expiry Month</option>	
                                <?php for ($i = 1; $i <= 12; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="fg-line">
                        <div class="select">
                            <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                <option value="">Expiry Year</option>
                                <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>                     
    </div>
   
    <div class="form-group">
        <label class="control-label col-sm-4">Security Code</label>  
        <div class="col-sm-8">
            <div class="fg-line">
                <input type="password" id="" name="" style="display: none;" />
                <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
            </div>
        </div>                     
    </div> 
    
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" class="btn btn-primary btn-sm m-t-20" id="nextbtn">Complete Integration</button>
            <button type="button" class="btn btn-default btn-sm m-t-20" id="back-to-gateway-body">Back</button>
        </div>
    </div>
</form>
</div>
</div>
<div class="modal fade" id="paypalConfirmation" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;">DPRP not enabled!</h4>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span>
                                <p>DPRP (Direct Payment Recurring Payment) needs to be enabled in your Paypal
                                    account for subscription billing.</p>
                                <p>
                                    If you will have only pay-per-view and not Subscription, please click
                                    "Continue". Otherwise, click "Cancel", enable DPRP in your Paypal account
                                    and try again.
                                </p>
                            </span> 
                        </div>
                    </div>
                 </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-block" id="continue-integration" onclick="paypalDPRPCheck()">Continue</button>
                   <button type="button" class="btn btn-primary btn-block" id="cancel-integration" onclick="cancel()">Cancel</button>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="updateModal" role="dialog" style="overflow-y:hidden !important;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" id="payment-head">Change Payment gateway mode to Live ?</h4>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="sandbox-live" style="display:none;">Changing payment gateway from <b>Sandbox to Live</b>, will reset all user data and revenue reports. Do you still want to proceed?</span>
                            <span id="live-sandbox" style="display:none;color:red;">Changing payment gateway from <b>Live to Sandbox</b>, will reset all user data and revenue reports. Do you still want to proceed?</span> 
                        </div>
                    </div>
                    <div class="modal-footer">

                        <a href="javascript:void(0);" onclick="updatePaymentGateway(1);" id="sub-btn"  class="btn btn-default delete_coupon coupon_yes">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
            </div>

        </div>
    </div>
</div>
<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card & gateway information.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

        <form name="pci_form" id="pci_form"></form>
<script type="text/javascript">
    var default_short_code = '<?php echo $temp_short_code;?>';
    var default_is_payment_gateway = '<?php echo $is_payment_gateway;?>';
    
    $(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    
        $('.payment_logos').hover(function () {
            $(this).find('.default_logo').hide();
            $(this).find('.original_logo').show();
        }, function () {
            $(this).find('.original_logo').hide();
            $(this).find('.default_logo').show();
            
            $('.cls_'+default_short_code).find('.default_logo').hide();
            $('.cls_'+default_short_code).find('.original_logo').show();
        });

        jQuery(function () {
            if (parseInt(default_is_payment_gateway)) {
                //getPaymentGateway(default_short_code);
            }
        });
    });
    
    function showGateways(obj) {
       var is_test_transactions = $(obj).attr('data-is_test_transactions');
       
       if (parseInt(is_test_transactions)) {
            swal({
            title: "Delete Test Transaction?",
            text: "This Store has some test transactions.<br>Are you sure you want to delete those test transactions?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        },function (isConfirm) {
            if(isConfirm && parseInt(is_test_transactions)){
               checktestdataAvailableOrNot(obj);
              $(".payment_logos").attr('data-is_test_transactions', '');
          }
         if(!isConfirm){
        var id = $(obj).attr('data-id');
        var short_code = $(obj).attr('data-short_code');
        var is_payment_gateway = $(obj).attr('data-is_payment_gateway');
       
        $("#gateway_id").val(id);
        $("#short_code").val(short_code);
        
        $('.payment_logos').find('.original_logo').hide();
        $('.payment_logos').find('.default_logo').show();
        $('.cls_'+short_code).find('.default_logo').hide();
        $('.cls_'+short_code).find('.original_logo').show();
        default_short_code = short_code;
        default_is_payment_gateway = is_payment_gateway;
       
        $('#payment_desc').html('').hide();
        $("#sbmt-div").hide();
        if (parseInt(is_payment_gateway)) {
            $("#manual_desc").hide();
            getPaymentGateway(short_code);
        } else {
            $("#manual_desc").show();
            $("#sbmt-div").show();
        }}
        });
           
           
       }else{
            var id = $(obj).attr('data-id');
        var short_code = $(obj).attr('data-short_code');
        var is_payment_gateway = $(obj).attr('data-is_payment_gateway');
       
        $("#gateway_id").val(id);
        $("#short_code").val(short_code);
        
        $('.payment_logos').find('.original_logo').hide();
        $('.payment_logos').find('.default_logo').show();
        $('.cls_'+short_code).find('.default_logo').hide();
        $('.cls_'+short_code).find('.original_logo').show();
        default_short_code = short_code;
        default_is_payment_gateway = is_payment_gateway;
       
        $('#payment_desc').html('').hide();
        $("#sbmt-div").hide();
        if (parseInt(is_payment_gateway)) {
            $("#manual_desc").hide();
            getPaymentGateway(short_code);
        } else {
            $("#manual_desc").show();
            $("#sbmt-div").show();
        }
    }
    
       
    }
    
    function getPaymentGateway(short_code) {
        $("#loader").show();
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/paymentGateways";
        $.post(url, {'short_code' : short_code}, function (res) {
            $("#loader").hide();
            if (res !==0) {
                if(short_code === 'instafeez'){
                    $("#instafeez-payment-gateway").show();
                }else{
                    $("#instafeez-payment-gateway").hide();
                }
                
                $("#payment_desc").html(res).show();
                scroll_up();
                $("#sbmt-div").show();
            }
        });
    }
    
    function validatePaymentGatewayForm() {
        var validate = $("#payment_gateway_form").validate({
            rules: {
                'data[short_code_paypal]': "required",
                'data[api_username]': "required",
                'data[api_password]': "required",
                'data[api_signature]': "required"
            },
            messages: {
                'data[api_username]': "Please enter API username",
                'data[api_password]': "Please enter API password",
                'data[api_signature]': "Please enter API signature"
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();
        
        if (x) {
            var api_mode = $('#api_mode').val();
            var is_exist = $('#is_exist').val();
            var is_ccavenue = $("#is_ccaenuev").val();
            
            if(parseInt(is_exist) > 1){
                   if(is_ccavenue == 1){
                       updateCCAvenueCredential();
                       // updatePaymentGateway(0);
                   }else{
                   updatePaymentGateway(0);
                } 
            }
        }
        }
    
    function updatePaymentGateway(){
        <?php if (intval($isNotPurchased)) { ?>
            return false;
        <?php }else{?>
            $("#loader").show();
            var form_data = $('#payment_gateway_form').serializeArray();
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/setGatewayCredentials";
            $.ajax({
                type: "POST",
                url: url,
                data: form_data,
                dataType: "json",
                success: function(data) {
                    var short_code = $('#short_code').val();
                    var access_token = $('#secret_key').val();
                    if(short_code === 'instafeez'){
                        $("#loader").hide();                        
                        instafeezIntegration ("<?php echo $fullname;?>","<?php echo Yii::app()->user->email?>","<?php echo isset(Yii::app()->user->phone_no)&& trim(Yii::app()->user->phone_no)?Yii::app()->user->phone_no:''?>",access_token);
                    }else{
                        var is_pci_compliance = $('#is_pci_compliance').val();
                        var is_sisp = $('#is_sisp').val();
                        if(parseInt(is_pci_compliance) === 1){
                            processSampleTransaction();
                        }else{
                            $('#gateway-body').delay(4000).hide();
                            $('#transaction-body').show();
                            $("#loader").delay(10000).hide();
                        }
                    }
                }
            });
            return false;
        <?php }?>
    }
    
    function validatePaymentGatewayCredentials(i){
        var form_data = $('#payment_gateway_form').serializeArray();
        
        $("#loader").show();
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/validatePaymentGatewayCredentials";
        if($('#short_code').val() === 'paypal' || $('#short_code').val() === 'paypalpro') {
            $("#api-error").html('');
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
            if(i){
                var _html = "<input type='hidden' value='1' name=data[update_data] />";
                $('#payment_gateway_form').append(_html);
                document.payment_gateway_form.action = url;
                document.payment_gateway_form.submit();
            }else{
                document.payment_gateway_form.action = url;
                document.payment_gateway_form.submit();
            }
        }else{
        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            dataType: "json",
            success: function(data) {
                $('#updateModal').modal('hide');
                if(data.isSuccess === 0){
                    $("#loader").hide();
                    $("#api-error").html('Invalid Payment Gateway credentials.');
                    return false;
                }else{
                    $("#api-error").html('');
                    var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
                    if(i){
                        var _html = "<input type='hidden' value='1' name=data[update_data] />";
                        $('#payment_gateway_form').append(_html);
                        document.payment_gateway_form.action = url;
                        document.payment_gateway_form.submit();
                    }else{
                        document.payment_gateway_form.action = url;
                        document.payment_gateway_form.submit();
                    }
                }
            }
        });
        }
    }
    
    $('#back-to-gateway-body').click(function(){
        $('#gateway-body').show();
        $('#transaction-body').hide();
    });
    
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }
        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: {
                    required: true,
                    number: true
                },
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function (error, element) {
                //$(".error-box").show();
               // $(".error-inner-box").html('credit card information is incorrect');
               // $(".error-box").delay(4000).fadeOut(1500);
               error.addClass('red');
            switch (element.attr("name")) {
                case 'exp_month':
                    error.insertAfter(element.parent().parent());
                    break;
                case 'exp_year':
                    error.insertAfter(element.parent().parent());
                    break;
                default:
                    error.insertAfter(element.parent());
            }
            }
        });
        var x = validate.form();
        if (x) {
            /*var short_code = $.trim($("#short_code").val());
            if (short_code === "stripe") {
                getStripeToken();
            } else {
                processSampleTransaction();
            }*/
            
            processSampleTransaction();
        }
    }
    
    /*function getStripeToken() {
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        
        Stripe.createToken({
            name: card_name,
            number: card_number,
            cvc: cvv,
            exp_month: exp_month,
            exp_year: exp_year
        }, stripeAuthResponse);
    }
    
    function stripeAuthResponse(status, response) {
        if (response.error) {
            $(".error-box").show();
            $(".error-inner-box").html(response.error.message);
            $(".error-box").delay(4000).fadeOut(1500);
            return false;
        } else {
            var token = response.id;
            processSampleTransaction(token);
        }
    }
    */
    
    function processSampleTransaction(token) {
        $('#nextbtn').html('wait!...');
        $('#nextbtn').attr('disabled', 'disabled');
        $("#loadingPopup").modal('show');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/transactionForGatewayIntegration";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        var csrfToken = $('#csrfToken').val();
        var is_pci_compliance = $('#is_pci_compliance').val();
        var is_sisp = $('#is_sisp').val();
        //var stripetoken = $.trim(token) ? token : '';
        var form_data = $('#payment_gateway_form').serialize();
        if(parseInt(is_sisp) === 1){
           integratePaymentGateway();
       } else if(parseInt(is_pci_compliance) === 1){
            integratePaymentGateway();
        }
        else{
        $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'csrfToken': csrfToken, data: form_data}, function (data) {
            $("#loadingPopup").modal('hide');
            if (parseInt(data.isSuccess) === 1) {
                var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
                document.payment_gateway_form.action = url;
                document.payment_gateway_form.submit();
            } else {
                var short_code = $('#short_code').val();
                if(short_code === 'paypalpro'){
                    if(data.error_message === 'DPRP is disabled for this merchant.' || data.error_message === 'DPRP is disabled for this merchant' || data.error_message === 'DPRP is disabled.' || data.error_message === 'DPRP is disabled'){
                        $('#paypal_dprp_status').val(0);
                        $('#paypalConfirmation').modal('show');
                    }else{
                        $('#nextbtn').html('Complete Integration');
                        $('#nextbtn').removeAttr('disabled');
                        $(".error-box").show();
                        $(".error-inner-box").html(data.error_message);
                        $(".error-box").delay(4000).fadeOut(1500);
                    }
                }else{
                    $('#nextbtn').html('Complete Integration');
                    $('#nextbtn').removeAttr('disabled');
                    $(".error-box").show();
                    $(".error-inner-box").html(data.error_message);
                    $(".error-box").delay(4000).fadeOut(1500);
                }
            }
        }, 'json');}
		}
    
    function paypalDPRPCheck()
    {
        $('#paypalConfirmation').modal('hide');
        $('#continue-integration').html('wait!...');
        $('#continue-integration').attr('disabled', 'disabled');
        $("#loadingPopup").modal('show');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/transactionForGatewayIntegration";
        var card_name = $('#card_name').val();
        var card_number = $('#card_number').val();
        var exp_month = $('#exp_month').val();
        var exp_year = $('#exp_year').val();
        var cvv = $('#security_code').val();
        var form_data = $('#payment_gateway_form').serialize();
        $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv,data: form_data}, function (data) {
            $("#loadingPopup").modal('hide');
            if (parseInt(data.isSuccess) === 1) {
                var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
                document.payment_gateway_form.action = url;
                document.payment_gateway_form.submit();
            } else {
                $('#continue-integration').html('Continue');
                $('#continue-integration').removeAttr('disabled');
                $('#nextbtn').html('Complete Integration');
                $('#nextbtn').removeAttr('disabled');
                $(".error-box").show();
                $(".error-inner-box").html('Sorry, payment failure! Your gateway or credit card information is incorrect');
                $(".error-box").delay(4000).fadeOut(1500);
            }
        }, 'json');
    }
    
    function cancel()
    {
        $('#paypalConfirmation').modal('hide');
        $('#nextbtn').html('Complete Integration');
        $('#nextbtn').removeAttr('disabled');
    }
function scroll_up() {
    $('html, body').animate({
        scrollTop: $("#payment_desc").offset().top
    }, 500);
}
function integratePaymentGateway(){
    var form_data = $('#payment_gateway_form').serialize();
    var is_sisp = $('#is_sisp').val();
    var short_code=$('#short_code').val();
    var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/isIntegratePaymentgateway";
    if(short_code === 'sbw'){
        $("#loadingPopup").modal('show');
        $('#integrate-payment-gateway').attr('disabled', 'disabled'); 
        $.post(url, {data: form_data}, function (data) {   
        var data=JSON.parse(data);
            if(data.short_code === 'sbw'){
                if (parseInt(data.isSuccess) === 1) {
                    for (var i in data.response) {
                        $("#pci_form").append("<input type='hidden' name='" + i + "' value='" + data.response[i] + "' />");
                    }
                    setTimeout(function () {
                        window.location = 'https://secure.sbw.com/v2/join?pay_type=cc&prod_id=' + data.prod_id + '&mid=' + data.mid + '&success_url=' + data.success_url;
                    }, 3000);
                }else{
                     window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>'/monetization/payment";
                }
            }
        });
    }
    else if(short_code === 'sisp'){
    var card_name = $('#card_name').val();
    var card_number = $('#card_number').val();
    var exp_month = $('#exp_month').val();
    var exp_year = $('#exp_year').val();
    var cvv = $('#security_code').val();
     
        $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv,data: form_data}, function (data) {
            var data=JSON.parse(data);
            if (parseInt(data.isSuccess) === 1) {
                $.each(data, function(key, value){
                     $("#pci_form").append("<input type='hidden' name='"+ key +"' value='" + value + "' />");
                });
                
                setTimeout(function () {
                    document.pci_form.action = data.endpoint;
                    document.pci_form.submit();
                    return false;
                }, 3000);
            }else{
             window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>'/monetization/payment";
            }
        });
    }else{
        $("#loadingPopup").modal('show');
        $('#integrate-payment-gateway').attr('disabled', 'disabled'); 
        $.post(url, {data: form_data}, function (data) {   
        var data=JSON.parse(data);
            if (parseInt(data.isSuccess) === 1) {
                for (var i in data.response) {
                    $("#pci_form").append("<input type='hidden' name='" + i + "' value='" + data.response[i] + "' />");
                }
                setTimeout(function () { 
                    document.pci_form.action = data.action;
                    document.pci_form.submit();
                    $("#loadingPopup").modal('hide');
                    var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
                    return false;
                }, 3000);
            }else{
            window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>'/monetization/payment";
            }
           });
    }
}
function checktestdataAvailableOrNot(obj)
{
    var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/RemoveTestUsersBeforeIntegration";
    $.post(url, function (data) {
    var data=JSON.parse(data);
    if(data=='True')  {
       swal({ 
  title: "",
   text: "Deleted Successfully",
    type: "success" 
  },
  function(){
       var id = $(obj).attr('data-id');
        var short_code = $(obj).attr('data-short_code');
        var is_payment_gateway = $(obj).attr('data-is_payment_gateway');
       
        $("#gateway_id").val(id);
        $("#short_code").val(short_code);
        
        $('.payment_logos').find('.original_logo').hide();
        $('.payment_logos').find('.default_logo').show();
        $('.cls_'+short_code).find('.default_logo').hide();
        $('.cls_'+short_code).find('.original_logo').show();
        default_short_code = short_code;
        default_is_payment_gateway = is_payment_gateway;
       
        $('#payment_desc').html('').hide();
        $("#sbmt-div").hide();
        if (parseInt(is_payment_gateway)) {
            $("#manual_desc").hide();
            getPaymentGateway(short_code);
        } else {
            $("#manual_desc").show();
            $("#sbmt-div").show();
        }
});
      
    } 
    else{
        return false;
    }
    });   
}
    function updateCCAvenueCredential(){
            $("#loader").show();
            $('#integrate-payment-gateway').html('wait!...');
            $('#integrate-payment-gateway').attr('disabled', 'disabled');           
            var form_data = $('#payment_gateway_form').serializeArray();
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/SetCCAvenueCredential";
            $.ajax({
                type: "POST",
                url: url,
                data: form_data,
                dataType: "json",
                success: function(data) {
                       if(data == 1){
                           window.location.href = "<?php echo Yii::app()->baseUrl; ?>/monetization/payment";
                       }  
                }
            });
    }
</script>