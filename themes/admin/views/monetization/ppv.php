
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row">
            
            <?php if (isset($data['isPPVEnable']) && intval($data['isPPVEnable'])) { ?>
            <div id="ppv_content" class="col-md-12">
                <p class="f-300">You can set PPV value for ALL content of one type OR define categories such as 'Latest', 'Premium' and tag content to these categories from 'Manage Content' screen.</p>
                <div class="Block m-t-40">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                                <em class="icon-check icon left-icon "></em>
                        </div>
                        <h4>Applies to ALL</h4>
                    </div>
                    <hr/>
                    <p class="f-300 m-b-0">Following PPV settings will apply to all content of the corresponding type.</p>
                    <?php 
                   //$single_part = $multi_part = '';
                    //if (isset($data['studioContent']) && !empty($data['studioContent'])) { ?>
                        <div class="row m-b-10">
                            <div class="col-sm-3 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="content-status" data-content_types_id="1" data-manage="<?php echo $data['all_single']['plans']->id;?>" data-ppv_type="1" data-name="All Single-part Content PPV" <?php if (isset($data['all_single']) && !empty($data['all_single'])) { ?> checked="checked" value="1" data-type="disable" <?php } else { ?> data-type="enable" <?php } ?> onclick="showConfirmPopup(this);" /><i class="input-helper"></i> All Single-part Content
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="content-status" data-content_types_id="3" data-manage="<?php echo $data['all_multi']['plans']->id;?>" data-ppv_type="1" data-name="All Multi-part Content PPV" <?php if (isset($data['all_multi']) && !empty($data['all_multi'])) { ?> checked="checked" value="1" data-type="disable" <?php } else { ?> data-type="enable" <?php } ?> onclick="showConfirmPopup(this);" /><i class="input-helper"></i> All Multi-part Content
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($data['all_single']) && !empty($data['all_single'])) {
                            $value['plans'] = $data['all_single']['plans'];
                            $value['pricing'] = $data['all_single']['pricing'];
                            
                            //foreach ($data['all_single'] as $key1 => $value) { ?>
                            <div class="row m-b-20">
                                <div class="col-md-12 m-b-10">
                                    <label>All Single-part Content</label>
                                </div>
                                    
                                <div class="col-md-12">
                                    <div class="row">
                                          
                                    <div class="col-md-12">
                                        <div class="border-solid padding m-b-10">
                                            <div class="row">
                                            <div class="col-sm-6">
                                        
                                    </div>
                                    
                                    <div class="col-md-6 text-right">
                                        
                                            <a href="javascript:void(0);" class="text-black" data-id="<?php echo $value['plans']->id;?>" data-type="<?php echo $value['plans']->content_types_id;?>" onclick="addEditAll(this);">
                                            <em class="icon-pencil"></em>&nbsp;Edit
                                            </a>
                                        
                                    </div>
                                            </div>    
                                                <div class="divider"></div>
                                <?php 
                                $cnt = 0;
                                foreach ($value['pricing'] as $key => $value1) {
                                    $price_for_unsubscribed = $value1['price_for_unsubscribed'];
                                    $price_for_subscribed = $value1['price_for_subscribed'];
                                    $symbol = $value1['symbol'];
                                    $pricing_id = $value1['ppv_pricing_id'];
                                    $currency_id = $value1['currency_id'];
                                    $code= $value1['code'];
                                    ?>
                                   
                                    <div class="row">
                                        <div class="col-md-2">
                                            <?php echo $code;?>(<?php echo $symbol;?>)
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                     Non-subscribers
                                                </div>
                                                <div class="col-sm-3">
                                                    <?php echo $price_for_unsubscribed;?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    Subscribers
                                                </div>
                                                <div class="col-sm-3">
                                                    <?php echo $price_for_subscribed;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    
                                     
                                    <?php  $cnt++;
                                    if ($cnt != count($value['pricing'])) {
                                    ?>
                                    <div class="divider"></div>
                                    <?php } ?>
                                <?php } ?>
                                  
                                    </div>
                                    </div> 
                                   
                                </div>
                                </div>
                            </div>
                        <?php //} ?>
                        <?php } ?>
                    
                   
                    <?php if (isset($data['all_multi']) && !empty($data['all_multi'])) {
                            $value['plans'] = $data['all_multi']['plans'];
                            $value['pricing'] = $data['all_multi']['pricing'];
                            //foreach ($data['all_multi'] as $key1 => $value) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>All Multi-part Content</label>
                                </div>
                                  <div class="col-sm-12">  
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="border-solid padding m-b-10">
                                            <div class="row">
                                            <div class="col-sm-6">
                                        
                                    </div>
                                   
                                    <div class="col-sm-6 text-right">
                                        <a href="javascript:void(0);" class="text-black" data-id="<?php echo $value['plans']->id;?>" data-type="<?php echo $value['plans']->content_types_id;?>" onclick="addEditAll(this);">
                                            <em class="icon-pencil" aria-hidden="true"></em>&nbsp;Edit
                                        </a>
                                    </div>
                                  
                                            </div>
                                     <div class="divider"></div>
                                                <?php
                                                $cnt = 0;
                                foreach ($value['pricing'] as $key => $value1) {
                                    $show_unsubscribed = $value1['show_unsubscribed'];
                                    $season_unsubscribed = $value1['season_unsubscribed'];
                                    $episode_unsubscribed = $value1['episode_unsubscribed'];

                                    $show_subscribed = $value1['show_subscribed'];
                                    $season_subscribed = $value1['season_subscribed'];
                                    $episode_subscribed = $value1['episode_subscribed'];
                                    
                                    $symbol = $value1['symbol'];
                                    $pricing_id = $value1['ppv_pricing_id'];
                                    $currency_id = $value1['currency_id'];
                                    $code= $value1['code'];
                                    ?>
                                 
                                     
                                           
                                   <div class="row">
                                    <div class="col-sm-2">
                                        <?php echo $code;?>(<?php echo $symbol;?>)
                                    </div>

                                    <div class="col-sm-10">
                                        
                                        <div class="row m-b-10">
                                            <div class="col-sm-3">
                                                Non-subscribers
                                            </div>
                                            <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                            <div class="col-sm-3">
                                                Show      &nbsp;&nbsp;
                                                <?php echo $show_unsubscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                            <div class="col-sm-3">
                                                Per Season &nbsp;&nbsp;
                                            
                                                <?php echo $season_unsubscribed;?>
                                            </div>
                                            <?php } ?>

                                            <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                            <div class="col-sm-3">
                                                Per Episode
                                                &nbsp;&nbsp;
                                                <?php echo $episode_unsubscribed;?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                        <div class="col-sm-3">
                                            Subscribers
                                        </div>
                                        <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                        <div class="col-sm-3">
                                            Show
                                            &nbsp;&nbsp;
                                            <?php echo $show_subscribed;?>
                                        </div>
                                        <?php } ?>

                                        <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                        <div class="col-sm-3">
                                            Per Season
                                             &nbsp;&nbsp;
                                            <?php echo $season_subscribed;?>
                                        </div>
                                        <?php } ?>

                                        <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                        <div class="col-sm-3">
                                            Per Episode
                                         &nbsp;&nbsp;
                                            <?php echo $episode_subscribed;?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    </div>
                                    </div>
                                   
                                    <?php  $cnt++;
                                    if ($cnt != count($value['pricing'])) {
                                    ?>
                                    <div class="divider"></div>
                                    <?php } ?>
                                <?php } ?>
                                   </div>
                                    </div>
                                      </div>
                                </div> 
                            </div>
                        <?php //} ?>
                        <?php } ?>
                    <?php //} ?>
                </div>
                
                
                <div class="Block m-t-40">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                                <em class="icon-settings icon left-icon "></em>
                        </div>
                        <h4>Manage PPV Categories</h4>
                    </div>
                    <hr/>
                    <p class="f-300 m-b-0">Define the PPV categories below and set category PER content on 'Manage Content' screen</p>
                    
                    
                    <?php //$single_part = ltrim($single_part, ', ');
                        //if (trim($single_part) != '') { ?>
                        
                        <div class="row m-t-20">
                            <div class="col-sm-8">
                                <label>
                                    PPV categories for Single-part Content
                                </label>
                            </div>
                            <div class="col-sm-4 text-right">
                                
                                    <a href="javascript:void(0);"  class="grey" data-type="1" data-id_ppv="" onclick="addEditCategory(this);" class="text-gray">
                                    <em class="icon-plus"></em>
                                    Add category
                                    </a>
                                
                            </div>
                            
                        </div>
                        
                        <div>
                            <?php 
                            if (isset($data['single_cat']) && !empty($data['single_cat'])) {
                                foreach ($data['single_cat'] as $key => $value) { ?>
                             <div class="row m-b-10" id="main_single_<?php echo $value['plans']->id;?>">
                                
                                    <div class="col-md-12">
                                        <div class="border-solid padding  m-b-10">
                                            <div class="row">
                                            
                                <div class="col-sm-6">
                                    <label><?php echo $value['plans']->title;?></label>
                                </div>
                                <div class="col-sm-6 text-right">  
                                    <a href="javascript:void(0);" data-type="1" data-id_ppv="<?php echo $value['plans']->id;?>" onclick="addEditCategory(this);" class="text-black">
                                        <em class="icon-pencil"></em>&nbsp;Edit
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="javascript:void(0);" data-manage="<?php echo $value['plans']->id;?>" data-type="delete" data-name="<?php echo $value['plans']->title;?> category" class="text-black" onclick="showConfirmPopup(this);">
                                        <em class="icon-trash"></em>&nbsp;Delete
                                    </a>
                                </div>
                             </div>
                                       
                                      <div class="divider"></div>
                                  
                                        <?php 
                                        $cnt = 0;
                                        foreach ($value['pricing'] as $key => $value1) {
                                            $price_for_unsubscribed = $value1['price_for_unsubscribed'];
                                            $price_for_subscribed = $value1['price_for_subscribed'];
                                            
                                            $symbol = $value1['symbol'];
                                            $pricing_id = $value1['ppv_pricing_id'];
                                            $currency_id = $value1['currency_id'];
                                            $code= $value1['code'];
                                            ?>
                                         <div class="row">
                                        <div class="col-sm-2">
                                            <?php echo $code;?>(<?php echo $symbol;?>)
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="row m-b-10">
                                                <div class="col-sm-3">
                                                    Non-subscribers
                                                </div>
                                                <div class="col-sm-3">
                                                    <?php echo $price_for_unsubscribed;?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    Subscribers
                                                </div>
                                                <div class="col-sm-3">
                                                    <?php echo $price_for_subscribed;?>
                                                </div>
                                            </div>
                                        </div>
                                       </div>
                                        <?php 
                                        $cnt++;
                                        if ($cnt != count($value['pricing'])) { ?>
                                       <div class="divider"></div>
                                        <?php } ?>
                                        <?php } ?>
                                        
                                      
                                        </div>  
                                    </div>
                                  </div>
                                <?php  } ?>
                            <?php } ?>
                        </div>
                    <?php //} ?>
                        
                    <?php //$multi_part = ltrim($multi_part, ', ');
                        //if (trim($multi_part) != '') { ?>
                        
                        <div class="row m-t-20 m-b-10">
                            <div class="col-sm-8">
                                <label>
                                    PPV categories for Multi-part Content
                                </label>
                            </div>
                            <div class="col-sm-4 text-right">
                                <a href="javascript:void(0);" class="grey" data-type="3" data-id_ppv="" onclick="addEditCategory(this);" class="text-gray">
                                    <em class="icon-plus"></em>
                                    Add category
                                </a>
                            </div>
                            
                        </div>
                        
                        <div>
                            <?php 
                            if (isset($data['multi_cat']) && !empty($data['multi_cat'])) {
                                foreach ($data['multi_cat'] as $key => $value) { ?>
                            
                                <div class="row m-b-10" id="main_multi_<?php echo $value['plans']->id;?>">
                                      <div class="col-md-12">
                                        <div class="border-solid padding  m-b-10">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label><?php echo $value['plans']->title;?></label>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <a href="javascript:void(0);" data-type="3" data-id_ppv="<?php echo $value['plans']->id;?>" onclick="addEditCategory(this);" class="text-black">
                                                        <em class="icon-pencil"></em>&nbsp;Edit
                                                    </a>
                                                    &nbsp;&nbsp;
                                                    <a href="javascript:void(0);" data-manage="<?php echo $value['plans']->id;?>" data-type="delete" data-name="<?php echo $value['plans']->title;?> category" class="text-black" onclick="showConfirmPopup(this);">
                                                        <em class="icon-trash"></em>&nbsp;Delete
                                                    </a>
                                                </div>
                                            </div>
                                    <div class="divider"></div>
                                    <?php 
                                        $cnt = 0;
                                        foreach ($value['pricing'] as $key => $value1) {
                                            $show_unsubscribed = $value1['show_unsubscribed'];
                                            $season_unsubscribed = $value1['season_unsubscribed'];
                                            $episode_unsubscribed = $value1['episode_unsubscribed'];

                                            $show_subscribed = $value1['show_subscribed'];
                                            $season_subscribed = $value1['season_subscribed'];
                                            $episode_subscribed = $value1['episode_subscribed'];
                                            
                                            $symbol = $value1['symbol'];
                                            $pricing_id = $value1['ppv_pricing_id'];
                                            $currency_id = $value1['currency_id'];
                                            $code= $value1['code'];
                                            ?>
                                    <div class="row">
                                        <div class="col-sm-2">
                                             <?php echo $code;?>(<?php echo $symbol;?>)
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="row m-b-10">
                                                <div class="col-sm-3">
                                                    Non-subscribers
                                                </div>
                                                <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                                <div class="col-sm-3">
                                                    Show &nbsp;&nbsp;
                                                     <?php echo $show_unsubscribed;?>
                                                </div>
                                                 <?php } ?>
                                                 <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                                <div class="col-sm-3">
                                                    Per Season&nbsp;&nbsp;
                                                
                                                    <?php echo $season_unsubscribed;?>
                                                </div>
                                                <?php } ?>

                                                <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                                <div class="col-sm-3">
                                                    Per Episode&nbsp;&nbsp;
                                                
                                                    <?php echo $episode_unsubscribed;?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    Subscribers
                                                </div>
                                                <?php if (isset($ppv_buy->is_show) && intval($ppv_buy->is_show)) { ?>
                                                <div class="col-sm-3">
                                                    Show&nbsp;&nbsp;
                                                
                                                    <?php echo $show_subscribed;?>
                                                </div>
                                                <?php } ?>

                                                <?php if (isset($ppv_buy->is_season) && intval($ppv_buy->is_season)) { ?>
                                                <div class="col-sm-3">
                                                    Per Season&nbsp;&nbsp;
                                               
                                                    <?php echo $season_subscribed;?>
                                                </div>
                                                <?php } ?>

                                                <?php if (isset($ppv_buy->is_episode) && intval($ppv_buy->is_episode)) { ?>
                                                <div class="col-sm-3">
                                                    Per Episode&nbsp;&nbsp;
                                               
                                                    <?php echo $episode_subscribed;?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                     <?php 
                                        $cnt++;
                                        if ($cnt != count($value['pricing'])) { ?>
                                        <div class="divider"></div>
                                        <?php } ?>
                                    <?php } ?>
                                    
                                    
                                </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php //}?>
                    </div>
                  </div>
        </div>
    </div>
            
            <?php } else { ?>
            <div class="col-md-12 red">
                PPV has not enabled yet!
            </div>
            <?php } ?>
            
</div>
</div>
    </div>

<!-- modal start here -->
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  ppv_modal" name="ppv_modal" id="ppv_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_ppv" name="id_ppv" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="ppvbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>

<script type="text/javascript">
    $(function () {        
        $(".content-status").change(function(){
            if (parseInt($(this).val())) {
                $(this).prop( "checked", true);
            } else {
                $(this).prop( "checked", false);
            }
        });
    });
    
    function showConfirmPopup(obj) {
       // $("#ppvModal").modal('show');
        var type = $(obj).attr('data-type');
        var name = " "+$(obj).attr('data-name');
        var ppv_type = " "+$(obj).attr('data-ppv_type');
        var title = type.charAt(0).toUpperCase() + type.slice(1)+ name+"?";
        var text = "Are you sure you want to "+type+name+"?";
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },
          function(){
             if (parseInt(ppv_type) === 1) {
                //$(obj).attr('data-type', type+"All");
                //$("#ppvbtn").attr('data-content_types_id', $(obj).attr('data-content_types_id')); 
                if(type==="enable"){
                    enableAllPPV(obj);
                }else if(type==="disable"){
                    disableAllPPV(obj);
                }else{
                     type+'AllPPV(obj)';
                }
        } else {
            $(obj).attr('data-type', type);
                if(type==="enable"){
                    enablePPV(obj);
                    
                }else if(type==="disable"){
                    disablePPV(obj);
                }else if(type==="delete"){
                     deletePPV(obj);
                }
            
        }
          });
        
    }
    
    function disableAllPPV(obj) {
        $("#id_ppv").val($(obj).attr('data-manage'));
        var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/disableAllPPV";
        $('#ppv_modal').attr("action", action);
        document.ppv_modal.submit();
    }
    
    function enableAllPPV(obj) {
        $("#id_ppv").val($(obj).attr('data-manage'));
        var id_ppv = $("#id_ppv").val();
        
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/isAllPPV",{'id_ppv': id_ppv},function(res){
            if (parseInt(res)) {
                var type = $(obj).attr('data-type');
               
                var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/enableAllPPV";

                $('#ppv_modal').attr("action", action);
                document.ppv_modal.submit();
            } else {
                var type = $(obj).attr('data-content_types_id');
                
                $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/addEditAllPPV",{'type' : type},function(res){
                    $("#ppvModal").modal('hide');
                    $("#ppvpopup").html(res).modal('show');

                    $('.cost').on("keypress",function(event) {
                        return decimalsonly(event);
                    });

                    $('.cost').on("contextmenu",function(event) {
                        return false;
                    });
                });
            }
        });
    }
    
    function addEditAll(obj) {
        var id_ppv = $(obj).attr('data-id');
        var type = $(obj).attr('data-type');
        
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/addEditAllPPV",{'id_ppv': id_ppv, 'type' : type},function(res){
            $("#ppvModal").modal('hide');
            $("#ppvpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        });
    }
    
    function addEditCategory(obj) {
        var id_ppv = $(obj).attr('data-id_ppv');
        var type = $(obj).attr('data-type');
        
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/addEditCategoryPPV",{'id_ppv' : id_ppv, 'type' : type},function(res){
            $("#ppvModal").modal('hide');
            $("#ppvpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        });
    }
    
    function deletePPV(obj) {
        $("#id_ppv").val($(obj).attr('data-manage'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/"+type+"PPV";
            
        $('#ppv_modal').attr("action", action);
        document.ppv_modal.submit();
    }
    
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57))
                return true;
            else
                return false;
        }
    }
    
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if (unicode >= 48 && unicode <= 57)
                return true;
            else
                return false;
        }
    }
    
</script>