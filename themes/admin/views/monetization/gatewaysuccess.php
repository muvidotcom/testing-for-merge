<?php
    $gateway_name = $payment_gateways->name;
    if(isset($data) && count($data) == 1 && $data[0]['is_primary'] == 0 && $payment_gateways->name == 'Paypalpro'){
        $gateway_name = 'Paypal Express';
    }
?>
<div class="row m-t-40 m-b-40">
    <div class="col-sm-12">
        <div class="Block">
            <h4><em class="icon-check green"></em> &nbsp;Payment Gateway Activated!</h4>
            
            <p>
                Your <span class="f-700"><?php echo $gateway_name;?></span> payment gateway is now successfully integrated. You can add pay-per-view and subscription plans
                and start making money!
            </p>
            <?php if(isset($gateway_name) && (trim($gateway_name) == 'Paypalpro' || trim($gateway_name) == 'Paypal Express')) {?>
            <p>
                If you have chosen <span class="f-700"><?php echo $gateway_name;?></span> as your preferred payment gateway,
                    <span class="red">we recommend you to enable "Instant Payment Notification (IPN)" in your PayPal account. </span>
                    
            </p>
            <p>
                Please <span class="f-700"><a href="https://www.muvi.com/help/activating-paypal-ipn" title="click here" target="_blank">click here</a></span> to know how to enable IPN in your PayPal account.
            </p>
            <?php }?>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="Block">
        <h4>
            Change Payment Gateway ?
        </h4>
            <p>
                It's recommended NOT to change the payment gateway once you go live as it may disrupt your service.
                If you need to change the gateway for some reason, please add a <a href="../ticket/addTicket">support ticket</a>.
            </p>
        </div>
    </div>
</div>