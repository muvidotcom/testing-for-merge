
<?php if ($has_pay_gateway != 1) { ?>
<div class="row m-t-40">
    <div class="col-xs-12">
    <p class="red">A payment gateway must be available before you can enabled subscription on your platform. Please email us at 
        <a href="mailto:studio@muvi.com">studio@muvi.com</a> your payment gateway details or ask us to help you setup one.
    </p>
    </div>
</div>
<?php } else { ?>
<div class="row m-b-40">
    <div class="col-xs-12">
        <button type="button" class="btn btn-primary m-t-10" onclick="addEditCategory(this);" data-id="">Add</button>
    </div>
</div>
<table class="table table-hover" id="example">
  
        <?php if (!empty($data)) { ?>
        <thead>       
            <tr>
                <th>Plan</th>
                <th>Duration</th>
                <th data-hide="phone">Subscription Fee</th>
                <th data-hide="phone">Free Trial Period</th>
                <th data-hide="phone">Contents</th>
                <th data-hide="phone">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $value) { ?>
                <tr <?php if (intval($value['plan']['plan_status']) == 0) { ?>style="color: #AFAFAF;"<?php } ?>>
                    <td title="<?php echo $value['plan']['short_desc']; ?>"><a href="javascript:void(0)" data-html="true"  data-toggle="popover" title="Contents" data-placement="bottom" data-content="<?php  foreach($value['plan']['content_name'] as $tcontent){
                           echo (strlen($tcontent['name']) > 20) ? substr($tcontent['name'], 0, 20) . '...' : $tcontent['name'].'<br />'; }?>"><?php echo (strlen($value['plan']['name']) > 20) ? substr($value['plan']['name'], 0, 20) . '...' : $value['plan']['name']; ?></a></td>
                    <td><?php echo $value['plan']['frequency'] . " " . $value['plan']['recurrence']; ?></td>
                    <td>
                        <?php foreach ($value['price'] as $key1 => $value1) { 
                            
                            $price = $value1['price'];
                            $symbol = trim($value1['symbol']) ? $value1['symbol'] : $value1['code'].' ';
                            echo $symbol.$price.'<br/>';
                        } ?>
                    </td>
                    <td><?php
                        if (intval($value['plan']['trial_period'])) {
                            echo $value['plan']['trial_period'] . " " . $value['plan']['trial_recurrence'];
                        } else {
                            echo 'N/A';
                        }
                        ?></td>
                    <td >
                        
                        <?php 
                        $i=1;
                     
                        foreach($value['plan']['content_name'] as $tcontent){
                             if($i<=2){
                            ?>
                          <span class="tag label label-info"> <?php   echo (strlen($tcontent['name']) > 20) ? substr($tcontent['name'], 0, 20) . '...' : $tcontent['name']; ?> </span>&nbsp;&nbsp;&nbsp;
                                                 
                             <?php }
                             
                             $i++; }
                             if($i>2){
                      ?>
                          <a href="javascript:void(0)" <?php if (intval($value['plan']['plan_status']) == 0) { ?>  style="color: grey"  <?php } else { ?> onclick="ViewCategory(this);" <?php } ?> data-id="<?php echo $value['plan']['plan_id']; ?>" data-planname="<?php echo $value['plan']['name']; ?>">View</a>  
                             <?php } ?>
                    </td>  
                    
                    <td>
                        <?php if (intval($value['plan']['plan_status'])) { ?>
                        <h5><a href="javascript:void(0);" onclick="addEditCategory(this);" data-id="<?php echo $value['plan']['plan_id']; ?>" >
                                <i class="icon-pencil"></i>&nbsp; Edit Plan
                            </a></h5> 
                        <?php if (($language_id == $value['plan']['language_id']) && ($value['plan']['parent_id'] == 0)) { ?>
                            <h5>
                                <a href="javascript:void(0);" data-subscription_id="<?php echo $value['plan']['plan_id'];?>" sub_name ="<?php echo $value['plan']['name']; ?>" data-type="disable" onclick="showConfirmPopup(this);">
                                    <i class="icon-ban red"></i>&nbsp; Disable
                                </a>
                            </h5>
                        <?php } ?>                                        
                        <?php } else { ?>    
                        <?php if (($language_id == $value['plan']['language_id']) && ($value['plan']['parent_id'] == 0)) { ?>
                            <h5> <a href="javascript:void(0);" data-subscription_id="<?php echo $value['plan']['plan_id'];?>" sub_name ="<?php echo $value['plan']['name']; ?>"  data-type="enable" onclick="showConfirmPopup(this);">
                               <i class="icon-check"></i>&nbsp; Enable
                                </a>
                            </h5>
                        <?php } ?>   
                        <?php } ?>
                    </td>
                </tr>
                 <?php } ?>
        </tbody>
        <?php } else { ?>
          <tbody>
            <tr>
                <td colspan="5">No plan added yet.</td>
            </tr>
         </tbody>
         <?php } ?>
</table>
<?php } ?>

<div class="loader text-center" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
<!-- modal start here -->
<div class="modal fade" id="ppvModal" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  ppv_modal" name="ppv_modal" id="ppv_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_ppv" name="id_ppv" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="ppvbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Add subscription Popup start -->
<div id="subscriptionPopup" class="modal fade"></div>
<!-- Add subscription Popup end -->


<div class="modal fade" id="subscriptionmodal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="submodal" id="submodal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_subscription" name="id_subscription" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="subscriptionbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="ppvpopup" role="dialog" data-backdrop="static" data-keyboard="false" ></div>
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/autosize.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
<script type="text/javascript">
    $(function () {        
        $(".content-status").change(function(){
            if (parseInt($(this).val())) {
                $(this).prop( "checked", true);
            } else {
                $(this).prop( "checked", false);
            }
        });
    });
    
  
    
     function showConfirmPopup(obj) {
        //$("#subscriptionmodal").modal('show');
        var name = $(obj).attr('sub_name');
        var type = $(obj).attr('data-type');
        //$("#headermodal").text(type.charAt(0).toUpperCase() + type.slice(1)+" Subscription?");
        //$("#bodymodal").text("Are you sure you want to "+type+" "+name +" Subscription?");
        //$("#subscriptionbtn").attr('data-subscription_id', $(obj).attr('data-subscription_id'));
       // var onclick = type+'Subscription(this)';
       // $("#subscriptionbtn").attr('data-type', type);
      //  $("#subscriptionbtn").attr('onclick',onclick).bind('click');
        swal({
        title: type.charAt(0).toUpperCase() + type.slice(1)+" Subscription Bundles?",
        text: "Are you sure you want to "+type+" "+name +" Subscription?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html:true
      },
      function(){
        if(type=="disable"){
            disableSubscription(obj);
        }else{
            enableSubscription(obj);
        }
      });
    }
     function enableSubscription(obj) {
        $("#id_subscription").val($(obj).attr('data-subscription_id'));
        var type = $(obj).attr('data-type');
        
        var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/"+type+"SubscriptionBundles";
            
        $('#submodal').attr("action", action);
        document.submodal.submit();
    }
    
    function disableSubscription(obj) {
        $("#id_subscription").val($(obj).attr('data-subscription_id'));
        var type = $(obj).attr('data-type');
        var action ="<?php echo Yii::app()->baseUrl; ?>/monetization/"+type+"SubscriptionBundles";
            
        $('#submodal').attr("action", action);
        document.submodal.submit();
    }
    
   
    
    function addEditCategory(obj) {
          var subscription_plan_id = $(obj).attr('data-id');
          
       $('.loader').show();
        var type = 1;
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/AddEditSubscriptionbundles",{'subscription_plan_id' : subscription_plan_id,'type' : type},function(res){
            $("#ppvModal").modal('hide');
            $('.loader').hide();
            $("#ppvpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
            
             var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/BundleContents", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    return item.name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    if (parseInt(contnts[i].content_id)) {
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": contnts[i].name});
                    }
                }
            }
            
            $(".bootstrap-tagsinput ").removeClass('col-md-8');
            
            if ($('.auto-size')[0]) {
                autosize($('.auto-size'));
            }
            
            
            
        });
    }
    
   function ViewCategory(obj) {
          var subscription_plan_id = $(obj).attr('data-id');
          var plan_name = $(obj).attr('data-planname');
       $('.loader').show();
        var type = 1;
        $.post("<?php echo Yii::app()->baseUrl; ?>/monetization/ViewsubscriptionsContents",{'subscription_plan_id' : subscription_plan_id,'plan_name':plan_name, 'type' : type},function(res){
            $("#ppvModal").modal('hide');
            $('.loader').hide();
            $("#ppvpopup").html(res).modal('show');
            
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });

            $('.cost').on("contextmenu",function(event) {
                return false;
            });
            
             var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/BundleContents", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    return item.name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    if (parseInt(contnts[i].content_id)) {
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": contnts[i].name});
                    }
                }
            }
            
            $(".bootstrap-tagsinput ").removeClass('col-md-8');
            
            if ($('.auto-size')[0]) {
                autosize($('.auto-size'));
            }
            
            
            
        });
    }
    
</script>