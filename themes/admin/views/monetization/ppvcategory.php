<style>
.modal-dialog{
width:55%;
  margin: auto;
}
#deletbtn {
  color: #38ACEC;
  font-weight: bold;
    top: 50%;
 
}
#deletbtn:hover{
  color: black;
  font-weight: bold;
}
</style>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Set PPV - <span class="" id="ppv_movie_name"></span> </h4>
        </div>
         <div class="modal-body">
            <div class="form-group">
                <?php if (isset($ppv_categories) && !empty($ppv_categories)) { ?>
                    <form id="ppv_category_form" class="form-horizontal" method="post" name="ppv_category_form"  onsubmit="PPVCategoryForm();" action="javascript:void(0);" >
                    <input type="hidden" name="data[movie_id]" value="<?php echo $res['movie_id']; ?>" />
                    <div class="form-group">
                        <label class="control-label col-sm-3">Category:</label>
                            <div class="col-sm-6">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm" name="data[ppv_plan_id]" id="selection_categories" >
                                            <option>Select Category</option>
                                            <?php foreach ($ppv_categories as $key => $value) { ?>
                                                <option value ="<?php echo $value->id; ?>" <?php if (isset($film->ppv_plan_id) && $film->ppv_plan_id == $value->id) { ?> selected="selected"<?php } ?>><?php echo $value->title; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary" id="addMoreContent" onclick="showallContentDetails()"data-content_types_id="<?php echo $res['content_types_id']; ?>">Add</button>
                            <div class="row"style="position: relative;" id="showdetails">
                                <div class="col-sm-6" style="margin-left:12px;">
                                    <h4>Details</h4>
                                </div>
                                <div class="">
                                    <div class="">
                                            <div class="col-sm-12" id="subscriber" style="float:left;position: relative;">
                                                <?php if (isset($res['content_types_id']) && $res['content_types_id'] == 3) {?>
                                                  <div class="col-sm-2" style="font-weight: bold;"> Plan Name</div>
                                                    <div class="col-sm-2" style="font-weight: bold;"> Currency</div>
                                                    <div class="col-sm-2" style="font-weight: bold;"> User Type</div>
                                                    <div class="col-sm-2" style="font-weight: bold;"> Show</div>
                                                    <div class="col-sm-2" style="font-weight: bold;"> Season</div>
                                                    <div class="col-sm-1" style="font-weight: bold;"> Episode</div>
                                                    <?php $ppv_plan_multipart = Yii::app()->common->getcontentdetailsofplan(3, $film->ppv_plan_id);
                                                    $dup = array();
                                                    ?>
                                                        <?php for ($i = 0; $i < count($ppv_plan_multipart); $i++) { ?>
                                                        <div class="row allprice catgryprice_<?php echo $ppv_plan_multipart[$i]->id; ?>" <?php if (isset($film->ppv_plan_id) && !empty($film->ppv_plan_id)) { ?> style="display: block;"<?php } else { ?> style="display: none;"<?php } ?>>
                                                            <?php
                                                            if (isset($pricing[$ppv_plan_multipart[$i]->id]) && !empty($pricing[$ppv_plan_multipart[$i]->id])) {

                                                                foreach ($pricing[$ppv_plan_multipart[$i]->id] as $key => $value1) {
                                                                    $show_unsubscribed = $value1['show_unsubscribed'];
                                                                    $season_unsubscribed = $value1['season_unsubscribed'];
                                                                    $episode_unsubscribed = $value1['episode_unsubscribed'];

                                                                    $show_subscribed = $value1['show_subscribed'];
                                                                    $season_subscribed = $value1['season_subscribed'];
                                                                    $episode_subscribed = $value1['episode_subscribed'];

                                                                    $symbol = $value1['symbol'];
                                                                    $code = $value1['code'];
                                                                    if (in_array($ppv_plan_multipart[$i]["title"], $dup)) {
                                                                        $display = 0;
                                                                    } else {
                                                                        array_push($dup, $ppv_plan_multipart[$i]["title"]);
                                                                        $display = 1;
                                                                    }
                                                                    ?>
                                                             <div class="col-sm-12">
                                                                <div class="control-label col-sm-2"style="word-wrap: break-word"> <?php if ($display) echo $ppv_plan_multipart[$i]["title"];?></div>
                                                                <div class="col-sm-2"> <?php echo $code.'('.$symbol.')';?></div>
                                                                <div class="col-sm-2">Subscriber <div class="row"> <div class="col-sm-1">Non-Subscriber </div></div></div>
                                                                <div class="col-sm-2"> <?php echo $show_subscribed;?> <div class="row"> <div class="col-sm-2">  <?php echo $show_unsubscribed;?>  </div></div></div>
                                                                <div class="col-sm-2"> <?php echo $season_unsubscribed;?> <div class="row"> <div class="col-sm-1">  <?php echo $season_subscribed;?>  </div></div></div>
                                                                <div class="col-sm-1"> <?php echo $episode_unsubscribed;?> <div class="row"> <div class="col-sm-1">  <?php echo $episode_subscribed;?>  </div></div></div>
                                                                <div class="col-sm-1"> <?php if ($display) { ?> 
                                                                    <a href="javascript:void(0);" name="all_selected_plan" data-type="single" data-title='<?php echo $ppv_plan_multipart[$i]->title; ?>' value='<?php echo $ppv_plan_multipart[$i]->id; ?>' onclick="$('.catgryprice_<?php echo $ppv_plan_multipart[$i]->id; ?>').remove();removebox(this);">
                                                                        <em class="icon icon-trash icon-4x" id="deletbtn" ></em>
                                                                    </a>
                                                                <?php } ?> 
                                                                </div>

                                                             </div>
                                                           
                                                        <?php } 
                                                        
                                                        }?> 
                                                      </div>   <br>              
                                             <?php } ?>      
                                                <?php } else{ ?>
                                                    <div class="col-sm-3" style="font-weight: bold;"> Plan Name</div>
                                                    <div class="col-sm-2" style="font-weight: bold;"> Currency</div>
                                                    <div class="col-sm-3" style="font-weight: bold;"> User Type</div>
                                                    <div class="col-sm-3" style="font-weight: bold;"> Price</div><br><br>
                                                    <?php
                                                    $ppv_plan_multipart = Yii::app()->common->getcontentdetailsofplan(1, $film->ppv_plan_id);
                                                    //print'<pre>';print_r($ppv_plan_multipart);exit;
                                                    $dup = array();
                                                    ?>
                                                    <?php for ($i = 0; $i < count($ppv_plan_multipart); $i++) {
                                                        ?>
                                                        <div class="row allprice catgryprice_<?php echo $ppv_plan_multipart[$i]->id; ?>" <?php if (isset($film->ppv_plan_id) && !empty($film->ppv_plan_id)) { ?> style="display: block;"<?php } else { ?> style="display: none;"<?php } ?>>
                                                            <?php
                                                            if (isset($pricing[$ppv_plan_multipart[$i]->id]) && !empty($pricing[$ppv_plan_multipart[$i]->id])) {
                                                                foreach ($pricing[$ppv_plan_multipart[$i]->id] as $key => $value1) {
                                                                    $price_for_unsubscribed = $value1['price_for_unsubscribed'];
                                                                    $price_for_subscribed = $value1['price_for_subscribed'];

                                                                    $symbol = $value1['symbol'];
                                                                    $code = $value1['code'];
                                                                    if (in_array($ppv_plan_multipart[$i]["title"], $dup)) {
                                                                        $display = 0;
                                                                    } else {
                                                                        array_push($dup, $ppv_plan_multipart[$i]["title"]);
                                                                        $display = 1;
                                                                    }
                                                                    ?>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3" style="word-wrap: break-word">  <?php  if ($display) echo $ppv_plan_multipart[$i]["title"];?></div>
                                                                <div class="col-sm-2"><?php echo $code.'('.$symbol.')';?> </div>
                                                                <div class="col-sm-3"> Non-Subscriber <div class="row"> <div class="col-sm-1"> Subscriber </div></div></div>
                                                                <div class="col-sm-2"> <?php echo $price_for_unsubscribed;?> <div class="row"> <div class="col-sm-1">  <?php echo $price_for_subscribed;?>  </div></div></div>
                                                                <div class="col-sm-1"> <?php if ($display) { ?> 
                                                                    <a href="javascript:void(0);" name="all_selected_plan" data-type="single" data-title='<?php echo $ppv_plan_multipart[$i]->title; ?>' value='<?php echo $ppv_plan_multipart[$i]->id; ?>' onclick="$('.catgryprice_<?php echo $ppv_plan_multipart[$i]->id; ?>').remove();removebox(this);">
                                                                        <em class="icon icon-trash icon-4x" id="deletbtn" ></em>
                                                                    </a>
                                                                <?php } ?> </div>
                                                               
                                                            </div>
                                                                <?php } ?>
                                                            <?php } ?>

                                                       </div> 
                                                   <br>
        <?php } ?>


                                                <?php } ?>
                                    </div>  
                                   
                                </div>
                                </div>
                                <div id="plan_id_post"></div>
                    </div>
                    
                        <div class="row form-group" id="sbmt-div">
                            <div class="col-sm-12 text-right  m-t-30">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                </div>
                    </form>
                
                <?php } else { ?>
                    <div class="row form-group">
                        <div class="col-sm-12 red">
                            No PPV Category found for this content type.
                        </div>              
                    </div>
                
                <?php } ?>
            
        </div>   
    </div>
</div>
</div>
<script type="text/javascript">
    var plan_id_globally = "";
<?php if (isset($film->ppv_plan_id) && !empty($film->ppv_plan_id)) { ?>

        var plan_id_globally = "<?php echo $film->ppv_plan_id; ?>" + ",";
        var hidden = "<input type='hidden' name='data[plan_id][]' value=" + plan_id_globally + ">";
        $("#plan_id_post").html(hidden);
<?php } ?>

    var array = [];
    function showPricing(obj) {
        var id = $('option:selected', obj).val();

        if (parseInt(id)) {
            $(".pricedetail").show();
            $(".allprice").hide();
            $(".catgryprice_" + id).show();
        } else {
            $(".pricedetail").hide();
            $(".allprice").hide();
        }
    }

    function PPVCategoryForm() {
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/setPPVCategory";
        document.ppv_category_form.action = url;
        document.ppv_category_form.submit();
    }
    function showallContentDetails(){
        var i, plan_id;
        parsedTest = (plan_id_globally).split(',');
        //alert(parsedTest);  
        var plan_id = $("#selection_categories option:selected").val();
        if ($.inArray(plan_id, parsedTest) == -1) {
            array.push(plan_id);
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/getppvcategorypricedetails";
            $.ajax({
                type: "POST",
                url: url,
                data: {plan_id: plan_id},
                success: function (res) {
                    var resdata = JSON.parse(res);
                    var content_types_id =<?php echo $res['content_types_id']; ?>;
                    plan_id_globally += plan_id + ",";
                    var hidden = "<input type='hidden' name='data[plan_id][]' value=" + plan_id_globally + ">";
                    $("#plan_id_post").html(hidden);
                    // alert(plan_id_globally);
                    var _html = '';
                    var dup=[];
                   //  alert(dup);  
                 
                   if (content_types_id === 3) {
                     var _html_multipart = '';
        _html_multipart +='<div class="row allprice catgryprice_' + resdata[0].ppv_plan_id + '" style="display: block;">';              
		 for (var i = 0; i < resdata.length; i++) {
             if ($.inArray(resdata[i].title ,dup) == -1) {
                                dup.push(resdata[i].title);
                                  var  display = 0;
                                } else {
                                        var display = 1;
                                }
     
      _html_multipart +='<div class="col-sm-12"><div class="control-label col-sm-2" style="word-wrap: break-word">';
                if(display == 0){ _html_multipart += '' + resdata[i].title + '';}
       _html_multipart+='</div><div class="col-sm-2">'+ resdata[i].symbol["code"] + '(' + resdata[i].symbol["symbol"] + ')</div>\n\
		<div class="col-sm-2">Subscriber<div class="row"><div class="col-sm-1">Non-Subscriber</div></div></div>\n\
		<div class="col-sm-2">'+ resdata[i].show_subscribed +'<div class="row"><div class="col-sm-2">'+ resdata[i].show_unsubscribed +'</div></div></div>\n\
		<div class="col-sm-2">'+  resdata[i].season_subscribed +'<div class="row"><div class="col-sm-1">'+ resdata[i].season_unsubscribed +'</div></div></div>\n\
		<div class="col-sm-1">'+  resdata[i].episode_subscribed +'<div class="row"><div class="col-sm-1">'+  resdata[i].episode_unsubscribed +'</div></div></div>';
        _html_multipart += '<div class="col-sm-1">';
        if(display == 0){_html_multipart += '<a href="javascript:void(0);" name="all_selected_plan" data-type="single" data-title="'+ resdata[i].title + '" value="'+ resdata[i].ppv_plan_id + '" onclick="parent_remove(' + resdata[0].ppv_plan_id + ');removebox(this);"> <em class="icon icon-trash icon-4x" id="deletbtn"></em></a>';}
        _html_multipart += '</div></div>';}
        _html_multipart +='</div> <br>';
                        $("#subscriber").append(_html_multipart);
                        $("#showdetails").show();
                    }else {     
                         _html += '<div class="row allprice catgryprice_' + resdata[0].ppv_plan_id + '" style="display: block;">';
                            for (var i = 0; i < resdata.length; i++) {
                                if ($.inArray(resdata[i].title ,dup) == -1) {
                                dup.push(resdata[i].title);
                                  var  display = 0;
                                } else {
                                        var display = 1;
                                }
                                
                                 _html +='<div class="col-sm-12"><div class="col-sm-3"style="word-wrap: break-word">';
                                 if(display == 0){  _html += '' + resdata[i].title + '';}
                                _html += '</div><div class="col-sm-2"> '+ resdata[i].symbol["code"] + '(' + resdata[i].symbol["symbol"] + ')</div>\n\
                                            <div class="col-sm-3">Non-Subscriber<div class="row"><div class="col-sm-1">Subscriber</div></div></div>\n\
                                            <div class="col-sm-2">'+ resdata[i].price_for_unsubscribed +'<div class="row"><div class="col-sm-1">'+ resdata[i].price_for_subscribed +'</div></div></div>';
                                _html += '<div class="col-sm-1">';
                               if(display == 0){ _html += ' <a href="javascript:void(0);" name="all_selected_plan" data-type="single" data-title="' + resdata[0].title + '" value="' + resdata[0].ppv_plan_id + '" onclick="parent_remove(' + resdata[0].ppv_plan_id + ');removebox(this);"> <em class="icon icon-trash icon-4x" id="deletbtn"></em></a>';}
                                 _html += '</div>\n\
                                    </div>';
                               
                                
                    
        
                        }
                     _html +='</div> <br>';
                        
                        $("#subscriber").append(_html);
                        $("#showdetails").show();
                    } 

                }

            });
        } else {
            swal("Please Select another category.", '', 'error');
        }
    }

    function removebox(obj) {
        var id = $(obj).attr('value');
        // alert(id);
        plan_id_globally = plan_id_globally.replace(id + ",", '');
       // alert(plan_id_globally);
        var hidden = "<input type='hidden' name='data[plan_id][]' value=" + plan_id_globally + ">";
        $("#plan_id_post").html(hidden);

    }
    function parent_remove(id) {
        var cls = 'catgryprice_' + id;
        $('.' + cls).remove();
    }
</script>
