<link href="<?php echo ASSETS_URL; ?>css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .bootstrap-tagsinput{width: 100%}
</style>

<?php
    $selcontents = '';
    if (@$content) {
        $selcontents = json_encode($content);
    }
?>
<script>
    sel_contents = '<?php echo $selcontents; ?>';
</script>
 

<div class="modal-dialog modal-lg">
    <form action="javascript:void(0);" method="post" name="adv_form" id="adv_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                     <?php if (isset($pricing) && !empty($pricing)) { ?>
                   Edit Pay-per-view Bundle
                     <?php } else { ?>
                   New Pay-per-view Bundle
                     <?php } ?>
                </h4>
            </div>
            
            <input type="hidden" name="data[id]" value="<?php echo $advPlans->id;?>" />
            <input type="hidden" name="data[is_all]" value="0" />
            <input type="hidden" name="data[content_types_id]" value="<?php echo $type;?>" />
            <input type="hidden" name="data[studio_content_types_id]" value="0" />
            <input type="hidden" name="data[is_advance_purchase]" value="2" />
            <input type="hidden" name="data[allstatus]" value=""  id="status_id" />
            <input type="hidden"  name="data[settimeframe]" value="1" autocomplete="off" />
            <div class="modal-body" >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row m-b-20">
                            <?php
                            if (isset($pricing) && !empty($pricing)) {
                                            
                                            foreach ($pricing as $key => $value) {
                                                $price_for_unsubscribed = $value['price_for_unsubscribed'];
                                                $price_for_subscribed = $value['price_for_subscribed'];
                                                $symbol = $value['symbol'];
                                                $pricing_id = $value['ppv_pricing_id'];
                                                $currency_id = $value['currency_id'];
                                    $code = $value['code'];
                                }
                            }
                            ?>
                            
                             <div class="col-sm-3">
                             <!--<div class="col-sm-8 p-l-0 p-r-0">
                                                            <input type="hidden" name="data[pricing_id][]" value="<?php echo $pricing_id;?>" />
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>-->
                             </div>
                            
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div id="single_part_box">
                            <div class="form-horizontal">
                              
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="content">Title: </label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                         <input type="text" class="form-control input-sm single-title" placeholder="Title" name="data[title]" value="<?php echo $advPlans->title; ?>" autocomplete="off" />
                                        </div>
                                        <small class="help-block">
                                            <label id="data[title][]-error" class="error red" for="data[content]" style="display: none"></label>
                                        </small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="content">Content: </label>
                                    <div class="col-sm-10">

                                        <div class="fg-line">
                                            <select class="input-sm" data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" multiple>
                                            </select>
                                        </div>
                                        <small class="help-block">
                                            <label id="data[content]-error" class="error red" for="data[content][]" style="display: none"></label>
                                        </small>
                                    </div>
                                </div>
                                <?php if($policyStatus == true){?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="policies">Rule: </label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                         <div class="select">
                                            <?php echo Yii::app()->Helper->rulesSelectBox($advPlans->rules_id, $policyStatus, $isPurchased);?>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
          
                           <?php /* ?>   <div class="form-group">
                                    <label class="col-md-2 control-label" for="description">Price (<?php echo $symbolofcurrency; ?>):</label>
                                    
                                    <?php /*if(count($currency)>1){ ?><?php if(count($currency)==1){ ?><?php } ?>
                                       <div class="col-sm-3">
                             <div class="col-sm-7 p-l-0 p-r-0">
                                                            <input type="hidden" name="data[pricing_id]" value="<?php echo $pricing_id;?>" />
                                                            <div class="fg-line">
                                                                <div class="select">
                                                                    <select class="form-control input-sm currency" name="data[currency_id]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                             </div>
                                    
                                    
                                    <div class="col-sm-5">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm cost single-title" placeholder="Price " id="prices" name="data[price_nonsubscribe]" value="<?php echo $price_for_subscribed; ?>" autocomplete="off" />
                                        </div>
                                    </div>
                                     <!--<div class="col-sm-3">
                                        <div class="fg-line">
                                         <input type="text" class="form-control input-sm cost single-title" placeholder="Subscribers " name="data[price_subscribe]" value="<?php //echo $advPlans->title; ?>" autocomplete="off" />
                                        </div>
                                    </div>-->
                                </div><?php */ ?>
                              

                               <!-- <div class="form-group">
                                    <label class="col-md-2 control-label" for="description">Description: </label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                            <textarea class="form-control input-sm auto-size" placeholder="Pre-purchase this content at a discounted rate. We will notify when it becomes available." name="data[description]" value="" autocomplete="off"><?php //echo $advPlans->description;?></textarea>
                                        </div>
                                    </div>
                                </div>-->
                                
                              <?php /* ?>  <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-8 m-t-10" style="padding-left: 0px !important;">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="singlechk" name="data[settimeframe]" id="chk" onclick="settimeframes(this.value)" value="1" <?php if ($advPlans->is_timeframe==1) { ?>checked="checked"<?php }?> /><i class="input-helper"></i>
                                                                 Set price for multiple timeframes</label>
                                                               
                                                            </div>
                                                             
                                                        </div>
                                                        
                                                    </div>
                                                </div> <?php */ ?>
                               <div class="col-sm-12" id="timeframeid1" <?php if(isset($advPlans->is_timeframe) && ($advPlans->is_timeframe==1)){ ?> style="display:block" <?php }else {?>style="display:block"  <?php } ?> > 
                          
                                
                                   <div class="form-group" style="margin-bottom: 0px !important;">
                                   
                                    
                                       <div class="col-sm-12" style="padding-left: 0px !important;">
                                        <div id="single_parent_box">
                                                <?php
                                                if (isset($pricingtimeframe) && !empty($pricingtimeframe)) {
                                           
                                            $cnt_single = 0;
                                            $i=0;
                                           
                                            foreach ($pricingtimeframe as $key => $value) {
                                                $price_for_unsubscribed = $value['price_for_unsubscribed'];
                                                $price_for_subscribed = $value['price_for_subscribed'];
                                                $validitydays = $value['validity_days'];
                                                $ppv_timeframelable_id = $value['ppv_timeframelable_id'];
                                                $ppv_timeframe_id = $value['id'];
                                                
                                                //$symbol = $value['symbol'];
                                                //$pricing_id = $value['ppv_pricing_id'];
                                                //$currency_id = $value['currency_id'];
                                                //$code= $value['code'];
                                            ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                                <input type="hidden" value="<?php echo $ppv_timeframelable_id; ?>" name="data[ppv_timeframelable_id][]" />
                                                <input type="hidden" value="<?php echo $ppv_timeframe_id; ?>" name="data[ppv_timeframe_id][]" />
                                                <!-- Subscriber and Non Subscriber-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            
                                                           <label  class="control-label" for="description"> <?php if($i==0){ ?>Price (<?php echo $symbolofcurrency; ?>):<?php } ?></label>
                                                        </div>
                                                         <div class="col-sm-3">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost tdays" placeholder="Validity in number Of Days"  name="data[days][]"  value="<?php echo $validitydays; ?>" autocomplete="off" />
                                                                            <input type="hidden" class="form-control input-sm cost single-cost" placeholder="Number Of Days" name="data[timeframeid][]" value="<?php echo $value['id']; ?>" autocomplete="off" />
 <input type="hidden" class="form-control input-sm cost single-cost" placeholder="" name="data[timeframedays][]" value="<?php echo $validitydays; ?>" autocomplete="off" />

                                                            </div>
                                                           <!-- <small class="help-block"><label id="data[days][]-error" class="error red" for="data[days][]" style="display: none;"></label></small>-->
                                                        </div>
                                                        <?php /*if(count($currency)>1){ ?>
                                                        <div class="col-sm-3">
                                                            <div class="fg-line"pricingtimeframe>
<div class="select">
                                                                    <select class="form-control input-sm currency" name="data[timeframecurrency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>                                                            </div>
                                                            <small class="help-block"><label id="data[currency][]-error" class="error red" for="data[currency][]" style="display: none;"></label></small>
                                                        </div>
                                                        <?php } */?>
                                                        <div class="col-sm-1 text-right" style="padding-right: 0px !important;margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                                                        
                                                                    <div class="col-sm-2">
                                                            <div class="fg-line">
                                                                            <input type="text" class="form-control input-sm cost single-cost timeframeprice" placeholder="Non-Subscriber" name="data[price_for_unsubscribed][]" value="<?php echo $price_for_unsubscribed; ?>" autocomplete="off"  />
                                                            </div>
                                                           <!-- <small class="help-block"><label id="data[price_for_unsubscribed][]-error" class="error red" for="data[price_for_unsubscribed][]" style="display: none;"></label></small>-->
                                                        </div>
                                                                    <div class="col-sm-1 text-right" style="padding-right: 0px !important;margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                                                                    <div class="col-sm-2">
                                                                        <div class="fg-line">
                                                                            <input type="text" class="form-control input-sm cost single-cost timeframepriceSub" placeholder="Price" name="data[price_for_subscribed][]" value="<?php echo $price_for_subscribed; ?>" autocomplete="off"  />
                                                                        </div>
                                                                       <!-- <small class="help-block"><label id="data[price_for_unsubscribed][]-error" class="error red" for="data[price_for_unsubscribed][]" style="display: none;"></label></small>-->
                                                                    </div>                                                           
                                                                        <?php if ($i != 0) { ?>
                                                                        <div class="col-md-1  text-right">
            <a class="text-black" onclick="removeBox(this);" data-type="single" href="javascript:void(0);">
                <em class="icon-trash blue"></em>&nbsp; Remove
            </a>
                                                        </div><?php }?>
                                                        <!--<div class="col-sm-3">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="<?php //echo $price_for_subscribed; ?>" autocomplete="off" />
                                                            </div>
                                                            <small class="help-block"><label id="data[price_for_subscribed][]-error" class="error red" for="data[price_for_subscribed][]" style="display: none;"></label></small>
                                                        </div>-->
                                                          
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <?php 
                                            $i++;
                                            }
                                                } else {
                                                    ?>
                                            <div class="row m-b-20">
                                                <!-- Checkbox-->
                                              <?php if (isset($pricing) && !empty($pricing) && empty($pricingtimeframe)){ ?>  
                                             <input type="hidden" name="data[update][]" value="1">
                                              <?php } ?>
                                                <!-- Subscriber and Non Subscriber-->
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-2"> <label  class="control-label"  for="description">Price (<?php echo $symbolofcurrency; ?>):</label>
</div>
                                                         <div class="col-sm-3">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost tdays"  <?php if (empty($pricing)){ ?> name="data[days][]" <?php } else {?>name="data[daysu][]" <?php } ?>  value="" placeholder="Validity in number of days" required="" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                        <?php /*if(count($currency)>1){ ?>
                                                         <div class="col-sm-3">
                                                            <div class="fg-line">
<div class="select">
                                                                    <select class="form-control input-sm currency" name="data[timeframecurrency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>                                                            </div>
                                                        </div>
                                                        <?php }*/ ?>
                                                        <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                                                                <div class="col-sm-2">
                                                            <div class="fg-line">
                                                                        <input type="text" class="form-control input-sm cost single-cost timeframeprice" placeholder="Non-Subscribers" <?php if (empty($pricing)) { ?> name="data[price_for_unsubscribed][]" <?php } else { ?>name="data[price_for_unsubscribedu][]" <?php } ?>  value="" required="" autocomplete="off" />
                                                            </div>
                                                        </div>
                                                                <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                                                                <div class="col-sm-2">
                                                                    <div class="fg-line">
                                                                        <input type="text" class="form-control input-sm cost single-cost timeframepriceSub" placeholder="Subscribers" <?php if (empty($pricing)) { ?> name="data[price_for_subscribed][]" <?php } else { ?>name="data[price_for_subscribedu][]" <?php } ?>  value="" required="" autocomplete="off" />
                                                                    </div>
                                                                </div>                                                                
                                                        <!--<div class="col-sm-3">
                                                            <div class="fg-line">
                                                                <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                                                            </div>
                                                            
                                                        </div>-->
                                                        
                                                    </div>
                                                </div>
                                                        <!-- Subscriber and Non Subscriber ends-->


                                            </div>
                                        <?php } ?>
                                        </div>
                                        
                                        <!-- Add Specific price category-->
                                       <!-- <div class="row">
                                            <div class="col-md-12 text-left">
                                                <a href="javascript:void(0);" onclick="addMore('single', 0);" class="text-gray">
                                                    <em class="icon-plus blue"></em> Add more price for specific country
                                                </a>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                                   <div class="text-right">
           <a href="javascript:void(0);" onclick="addMore('single', 0);" class="text-gray">
                                                    <em class="icon-plus blue"></em> Add more
                                                </a>
        </div>
                                   
                               </span>
                               
                             
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" <?php if (isset($pricing) && !empty($pricing)) { ?> onclick="return validateSingleAdvFormbundles();" <?php } else { ?> onclick="return validateSingleAdvForm();" <?php } ?> id="bill-btn"><?php if (isset($pricing) && !empty($pricing)) { ?> Update<?php } else {?>Add<?php } ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>	
</div>
  <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-3"></div>
                                                
        <div class="col-sm-2">
                                                    <small class="help-block"><label id="data[days][]-error" class="error red" for="data[days][]" style="display: none;"></label></small>
                                                </div>
                                                <?php /*if(count($currency)>1){ ?>
                                                 <div class="col-sm-3">
                                                    <small class="help-block"><label id="data[days][]-error" class="error red" for="data[days][]" style="display: none;"></label></small>
                                                </div>
                                                <?php }*/ ?>
                                                <div class="col-sm-1" ></div>
        <div class="col-sm-2">
                                                    <small class="help-block"><label id="data[price_for_unsubscribed][]-error" class="error red" for="data[price_for_unsubscribed][]" style="display: none;"></label></small>
                                                </div>
        <div class="col-sm-2">
            <small class="help-block"><label id="data[price_for_subscribed][]-error" class="error red" for="data[price_for_subscribed][]" style="display: none;"></label></small>
        </div>        
                                                 <!--<div class="col-sm-3">
                                                    <small class="help-block"><label id="data[price_for_subscribed][]-error" class="error red" for="data[price_for_subscribed][]" style="display: none;"></label></small>
                                                </div>-->
                                               
                                            </div>
                                        </div>

         <?php if (isset($pricing) && !empty($pricing)) { ?>
        <div id="single-data" style="display: none;">
    <div class="row m-b-20">
        <!-- Subscriber and Non Subscriber-->
        <div class="col-sm-12">
            <div class="row">
                <input type="hidden" name="data[update][]" value="1">
                 <div class="col-sm-2"> <label  control-label for="description"></label>
</div>
                
                  <div class="col-sm-3" >
                       <div class="fg-line">
                           <input type="text" class="form-control input-sm cost single-cost tdays" id="tdays"  name="data[daysu][]" value="" placeholder="Validity in number of days" required="" autocomplete="off" />
                       </div>
             </div>
                 <?php /* if(count($currency)>1){ ?>
                 <div class="col-sm-3">
                       <div class="fg-line">
<div class="select">
                                                                    <select class="form-control input-sm currency" name="data[timeframecurrency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>                       </div>
             </div>
                      <?php } */ ?>
                    <!---------EDIT ADD MORE---------------->
                 <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                    <div class="col-sm-2">
                    <div class="fg-line">
                            <input type="text" class="form-control input-sm cost single-cost timeframeprice" id="timeframeprice" name="data[price_for_unsubscribedu][]" placeholder="Non-Subscribers" value="" required="" autocomplete="off" />
                    </div>
                </div>
                    <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                    <div class="col-sm-2">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm cost single-cost timeframepriceSub" id="timeframeprice" name="data[price_for_subscribedu][]" placeholder="Subscribers" value="" required="" autocomplete="off" />
                        </div>
                    </div>                    
                
                    <div class="col-md-1  text-right">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                <em class="icon-trash blue" ></em>&nbsp; Remove
            </a>
        </div>
                <!--<div class="col-sm-3">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                    </div>
                </div>-->
               
            </div>
            
        </div>
      
    </div>
</div>
         <?php } else{ ?>
        <div id="single-data" style="display: none;">
    <div class="row m-b-20">
        <!-- Subscriber and Non Subscriber-->
        <div class="col-sm-12">
            <div class="row">
                <input type="hidden" name="data[update][]" value="1">
                 <div class="col-sm-2"> <label  class="control-label"  for="description"> </label>
</div>
                
                  <div class="col-sm-3" >
                       <div class="fg-line">
                           <input type="text" class="form-control input-sm cost single-cost tdays" id="tdays"  name="data[days][]" value="" placeholder="Validity in number of days" required="" autocomplete="off" />
                       </div>
             </div>
                 <?php /* if(count($currency)>1){ ?>
                 <div class="col-sm-3">
                       <div class="fg-line">
<div class="select">
                                                                    <select class="form-control input-sm currency" name="data[timeframecurrency_id][]" <?php if ($studio->default_currency_id == $currency_id) {?>disabled="true"<?php }?>>
                                                                        <?php foreach ($currency as $key => $value) { ?>
                                                                        <option value="<?php echo $value['id'];?>" <?php if ($currency_id == $value['id']) {?>selected="selected"<?php }?>><?php echo $value['code'];?>(<?php echo $value['symbol'];?>)</option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>                       </div>
             </div>
                      <?php } */ ?>
                    <!--------NEW PPV - ADD MORE----------->
                 <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                    <div class="col-sm-2">
                    <div class="fg-line">
                            <input type="text" class="form-control input-sm cost single-cost timeframeprice" id="timeframeprice" name="data[price_for_unsubscribed][]" placeholder="Non-Subscribers" value="" required="" autocomplete="off" />
                    </div>
                </div>
                    <div class="col-sm-1 text-right" style="padding-right: 0px !important; margin-top: 7px;"> <?php echo $symbolofcurrency; ?></div>
                    <div class="col-sm-2">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm cost single-cost timeframepriceSub" id="timeframeprice" name="data[price_for_subscribed][]" placeholder="Subscribers" value="" required="" autocomplete="off" />
                        </div>
                    </div>
                    <div class="col-md-1  text-right">
            <a href="javascript:void(0);" data-type="single" onclick="removeBox(this);" class="text-black">
                <em class="icon-trash blue" ></em>&nbsp; Remove
            </a>
        </div>
                <!--<div class="col-sm-3">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm cost single-cost" name="data[price_for_subscribed][]" value="" autocomplete="off" />
                    </div>
                </div>-->
               
            </div>
            
        </div>
      
    </div>
</div>
         <?php } ?>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript">
    var gmtDate = '<?= gmdate('d'); ?>';
    var gmtMonth = '<?= gmdate('m'); ?>';
    var gmtYear = '<?php echo gmdate('Y'); ?>';
    
    
    
    
   /* function settimeframes(val)
    {
    var chkval=document.getElementById("chk").checked
    if(chkval==true)
    {
    $('#timeframeid').show(500);
    }
    else
    {
    $('#timeframeid').hide(500);  
    }
   
    }
    */
    
    //$("#expiry_date").datepicker({changeMonth: !0, changeYear: !0, minDate: $("#expiry_date").val() ? new Date($("#expiry_date").val()) : new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
    $(".expire_datee").datepicker({changeMonth: !0, changeYear: !0, minDate: new Date(gmtYear, parseInt(gmtMonth) - 1, gmtDate)})
                    
    Array.prototype.getUnique = function(){
       var u = {}, a = [];
       for(var i = 0, l = this.length; i < l; ++i){
          if(u.hasOwnProperty(this[i])) {
             continue;
          }
          a.push(this[i]);
          u[this[i]] = 1;
       }
       return a;
    }
    
    function addMore(type, flag) {
     
        var isTrue = 0;
        $("#"+type+"_part_box").find("."+type+"-cost").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        
        if (parseInt(flag)) {
            isTrue = 1;
        }
        
        if (parseInt(isTrue)) {
            var str ='';
            if (type === 'single') {
                str = $("#single-data").html();
            } else if (type === 'multi') {
                str = $("#multi-data").html();
            }
            
            $("#"+type+"_parent_box").append(str);
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').on("contextmenu",function(event) {
                return false;
            });
        }
    }
    
    function removeBox(obj) {
        $(obj).parent().parent().parent().parent().remove();
    }
       
    function validateSingleAdvForm() {  
        var validate = $("#adv_form").validate({
            rules: {
                'data[title]': {
                    required: true
                },
                'data[days][]': {
                    required: true
                }
                ,
                'data[price_nonsubscribe]': {
                    required: true
                }
                ,
                'data[price_subscribe]': {
                    required: true
                }
                ,
                'data[content]': {
                    required: true
                }
                ,
                'data[timeframe][]': {
                    required: true
                },
                'data[price_for_unsubscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                },
                'data[price_for_subscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                }
            },
            messages: {
                'data[title]': {
                    required: 'Please enter the title'
                },
                'data[days][]': {
                    required: 'Please enter Validity Of Days',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                } ,
                'data[price_nonsubscribe]': {
                    required: 'Please enter the Price'
                } ,
                'data[price_subscribe]': {
                    required: 'Please enter the Price'
                },'data[content]': {
                    required: 'Please enter the content'
                },
                'data[timeframe][]': {
                    required: 'Please enter the time frame'
                },
                'data[price_for_unsubscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                },
                'data[price_for_subscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                }
                
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
      var x = validate.form();
     // var chkval=document.getElementById("chk").checked;
                            var len = $(".tdays").length;
                            var lent = $(".timeframeprice").length;
                            var lentSub = $(".timeframepriceSub").length;
                            var prices = parseFloat($("#prices").val());
                            if ((prices == 0)) {
       swal("Oops! Price should be greater than zero");
       preventDefault();
       return false;
    }
      
        
   
        $i=1;
         $(".tdays").each(function() {
    if (($(this).val() == "") && ($i<len)) {
       swal("Oops! Please Enter Validity in number of days");
       preventDefault();
       return false;
    }
    $i++; });
 $i=1;
         $(".tdays").each(function() {
    if ((parseFloat($(this).val()) == 0) && ($i<len)) {
       swal("Oops! Number of days Should greated than zero");
       preventDefault();
       return false;
    }
    $i++; });

    $j=1;
         $(".timeframeprice").each(function() {
    if ($(this).val() == "" && $j<lent) {
       swal("Oops! Please Enter price");
       preventDefault();
       return false;
    }
    $j++; });

                             $j = 1;
                            $(".timeframepriceSub").each(function () {
                                if ($(this).val() == "" && $j < lentSub) {
                                    swal("Oops! Please Enter price");
                                    preventDefault();
                                    return false;
                                }
                                $j++;
                            });                           

                            $j = 1;
                            $(".timeframeprice").each(function () {
                                if (parseFloat($(this).val()) == 0 && $j < lent) {
       swal("Oops! price should greater than zero");
       preventDefault();
       return false;
    }
    $j++; });

   
        if (x) {
            if (!$.trim($("#content").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                return false;
            }
            else {
                var currency = new Array();

                $("#single_part_box").find(".singlechk").each(function(){
                    if ($(this).is(':checked')) {
                        currency.push($(this).parent().parent().parent().next('div').find('select').val());
                    }
                });

                $("#status_id").val(currency.getUnique().toString());

                $(".singlechk").each(function(){
                    $(this).prop("disabled", false);
                });

                $(".currency").each(function(){
                    $(this).prop("disabled", false);
                });

                var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/addppvbundles";
                document.adv_form.action = url;
                document.adv_form.submit();
            }
        }
    }
    
    
    function validateSingleAdvFormbundles() {    
        var validate = $("#adv_form").validate({
           rules: {
                'data[title]': {
                    required: true
                },
                'data[days][]': {
                    required: true
                }
                ,
                'data[price_nonsubscribe]': {
                    required: true
                }
                ,
                'data[price_subscribe]': {
                    required: true
                }
                ,
                'data[content]': {
                    required: true
                }
                ,
                'data[timeframe][]': {
                    required: true
                },
                'data[price_for_unsubscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                },
                'data[price_for_subscribed][]': {
                    required: true,
                    allrequired: true,
                    price: true,
                    uniquecurrency: true
                }
            },
            messages: {
                'data[title]': {
                    required: 'Please enter the title'
                },
                'data[days][]': {
                    required: 'Please enter Validity Of Days',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                } ,
                'data[price_nonsubscribe]': {
                    required: 'Please enter the Price'
                } ,
                'data[price_subscribe]': {
                    required: 'Please enter the Price'
                },'data[content]': {
                    required: 'Please enter the content'
                },
                'data[timeframe][]': {
                    required: 'Please enter the time frame'
                },
                'data[price_for_unsubscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                },
                'data[price_for_subscribed][]': {
                    required: 'Please enter fee',
                    allrequired: 'Please enter fee',
                    price: 'Minimum price should be 50 cent',
                    uniquecurrency: 'Currency should be unique'
                }
                
            },
           errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
       
       //var chkval=document.getElementById("chk").checked;
                            var len = $(".tdays").length;
                            var lent = $(".timeframeprice").length;
                            var lentSub = $(".timeframepriceSub").length;
      
       var prices=parseFloat($("#prices").val());
     if ((prices== 0)) {
       swal("Oops! Price should be greater than zero");
       preventDefault();
       return false;
    }
      
   
        $i=1;
         $(".tdays").each(function() {
    if ($(this).val() == "" && $i<len) {
       swal("Oops! Please Enter Validity in number of days");
       preventDefault();
       return false;
    }
    $i++; });

$i=1;
         $(".tdays").each(function() {
    if ((parseFloat($(this).val()) == 0) && ($i<len)) {
       swal("Oops! Number of days Should greated than zero");
       preventDefault();
       return false;
    }
    $i++; });


    $j=1;
         $(".timeframeprice").each(function() {
    if ($(this).val() == "" && $j<lent) {
       swal("Oops! Please Enter price");
       preventDefault();
       return false;
    }
    $j++; });

                            $j = 1;
                            $(".timeframepriceSub").each(function () {
                                if ($(this).val() == "" && $j < lentSub) {
                                    swal("Oops! Please Enter price");
                                    preventDefault();
                                    return false;
                                }
                                $j++;
                            });                            

                            $j = 1;
                            $(".timeframeprice").each(function () {
                                if (parseFloat($(this).val()) == 0 && $j < lent) {
       swal("Oops! price should greater than zero");
       preventDefault();
       return false;
    }
                                $j++;
                            });
   

   
     var x = validate.form();
        if (x) {
            if (!$.trim($("#content").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                return false;
            } else {
                var currency = new Array();

                $("#single_part_box").find(".singlechk").each(function(){
                    if ($(this).is(':checked')) {
                        currency.push($(this).parent().parent().parent().next('div').find('select').val());
                    }
                });

                $("#status_id").val(currency.getUnique().toString());

                $(".singlechk").each(function(){
                    $(this).prop("disabled", false);
                });

                $(".currency").each(function(){
                    $(this).prop("disabled", false);
                });

                var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/editppvbundles";
                document.adv_form.action = url;
                document.adv_form.submit();
            }
        }
    }
    

jQuery.validator.addMethod("allrequired", function (value, element) {
    var isTrue = 1;
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            var cost = $(this).parent().parent().parent().parent().parent().next('div').find('.single-cost');
            cost.each(function(){
                var price = $(this).val();
                
                if (price === '') {
                    isTrue = 0;
                    return false;
                }
            });
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Please enter fee");

jQuery.validator.addMethod("price", function (value, element) {
    var isTrue = 1;
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            var cost = $(this).parent().parent().parent().parent().parent().next('div').find('.single-cost');
            cost.each(function(){
                var price = $(this).val();
                
                price = $.trim(price);
                price = Math.floor(price * 100) / 100;
                price = Math.round(price);
                if (price <= 0) {
                    isTrue = 0;
                    return false;
                }
            });
        }
    });
    
    if (isTrue) {
        return true;
    }
}, "Minimum price should be 50 cent");

jQuery.validator.addMethod("uniquecurrency", function (value, element) {
    var currency = new Array();
    
    $("#single_parent_box").find(".singlechk").each(function(){
        if ($(this).is(':checked')) {
            currency.push($(this).parent().parent().parent().next('div').find('select').val());
        }
    });
    
    var cur = currency.getUnique();
    if (cur.length === currency.length) {
        return true;
    } else {
        return false;
    }
}, "Currency should be unique");
</script>