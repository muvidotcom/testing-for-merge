<p>  Define price of content or categories in credit</p>
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-primary small-margin-left"  onclick="show_creaditpopup()">Add Credit Category</button>
                <?php if(isset($data) && !empty($data)){  ?>
                    <button class="btn btn-danger small-margin-right pull-right"  onclick="delete_credit()" style="margin-right:15px;">Delete Categories</button>
                <?php } ?>
                <div class="table-responsive" style="margin-top:15px;">
                    <form method="post" name="credit_categories_action" id="credit_categories_action">
                     <input type="hidden" name="studio_id" value="<?php echo $studio_id; ?>" />

                        <?php if(isset($data) && !empty($data)){  ?>
                    <table class="table">
                        <tr>
                            <th>
                            <div class="checkbox m-b-0">
                                <label>
                                    <input class="chkall" id="selectall" name="checkall" onclick="checkUncheckAll();" type="checkbox"><i class="input-helper"></i> 
                                </label>
                            </div>                                
                            </th>
                            <th>Credit Category</th>
                            <th>Content/Content Category</th>
                            <th>Credit Price</th>
                            <th></th>
                        </tr>
                         <?php
                          foreach($data as $key => $val){  
                         ?>
                                <tr style="<?php echo ($val['status'] == 0)?'color:rgb(175, 175, 175);':'';?>">
                                    <td>
                                        <div class="checkbox m-b-0">
                                           <label>
                                               <input class="chkind" id="data" name="data[]" value="<?php echo $val['id'];?>"  type="checkbox" onclick="checkUncheckAll(1);"><i class="input-helper"></i> 
                                           </label>
                                       </div>                                
                                    </td>
                                    <td><?php echo $val['name']; ?></td>
                                    <td><?php echo $val['content_category']; ?></td>
                                    <td><?php echo $val['credit_value']; ?> Credits</td>
                                    <td>
                                        <h5><a href="javascript:void(0);" onclick="edit_creditcontent('<?php echo $val['id'];?>');"><i class="fa fa-cog" aria-hidden="true" style="font-size:19px;"></i> Edit</a></h5>
                                        <?php if($val['status'] == 1){ ?>
                                        <h5><a href="javascript:void(0);" onclick="disable_credit('<?php echo $val['id'];?>','<?php echo $val['name'];?>')"><i class="icon-ban red"></i> Disable</a></h5>
                                       <?php }else{ ?>
                                         <h5><a href="javascript:void(0);" onclick="enable_credit('<?php echo $val['id'];?>','<?php echo $val['name'];?>')"><i class="icon-check"></i> Enable</a></h5>
                                       <?php } ?>                                   
                                    </td>

                                </tr>
                          <?php } ?> 
                           </table> 
                        <?php  } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="creditcontent_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3><span class="content_typ" id="content_typ">Add</span> Credit Category</h3>Define pricing of a content or content category

      </div>
      <div class="modal-body">
          <form class="form-horizontal credit" name="credit_content_modal_form" id="credit_content_modal_form" method="post">
              
          </form>
      </div>
    </div>

  </div>
</div>
<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<!--<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/autosize.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/new_relese/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-multiselect.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function show_creaditpopup(){
        $("#content_typ").html('');
        $("#content_typ").html('Add');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/getcreditcontentPopup";
        $.post(url,function(res){
          $('#creditcontent_modal').modal('toggle'); 
          $("#creditcontent_modal").modal('show');
          $("#credit_content_modal_form").html(res);
        });
    }
    function add_credit_category(){
        var validate = $("#credit_content_modal_form").validate({
            rules: {
                category_name: "required",
                credit_action: "required",
                credit_value: {
                    required: true,
                    number: true
                }
            },
            messages: {
                category_name: "Please enter category name",
                credit_action: "Please select credit action",
                credit_value: "Please enter credit value",
                credit_value: {
                    required: "Please enter credit value",
                    number: "Please enter valid credit value"
                }           
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());

            }
        });  
         var is_valid = validate.form(); 
         if(is_valid){
             var content_type = $("#credit_action").val();
             var error_val = 1;
             var error_msg = '';
             if(content_type == 1){
                 var content_val = $("#content").val();
                if(content_val == null){
                    error_val = 0;
                    error_msg = 'Please select Content';
                }
             } else if(content_type == 2){
                 check_arr = [];
                $('.singlechk').each(function () {
                 if($(this).is(":checked")){
                    check_arr.push(1); 
                 }
              });
                var count = check_arr.length;
                if(count < 1){
                   error_val = 0;
                   error_msg = 'Please select content category';                       
                }
             }
             if(error_val == 1){
                $("#add_rule").attr("disabled", true);
                $("#add_rule").html("Please wait...");               
                var url_cat = "<?php echo Yii::app()->baseUrl; ?>/monetization/saveCreditCategory";
                var data = $("#credit_content_modal_form").serialize();
                //alert(data);
                $.post(url_cat,{'data':data},function(res){
                    location.reload();
                });
             }else{
                swal({
                  title: 'Validate',
                  text: error_msg,
                  type: "warning",
                  showCancelButton: false,
                  confirmButtonColor: "#f55753",customClass: "confirmButtonTextColor primary",
                  confirmButtonText: "Ok",
                  closeOnConfirm: true,
                  html:true,
                });                
             }
         }
     }
    function checkUncheckAll(arg) {
    if (parseInt(arg)) {
        $(".chkind").prop('click', function () {
            if ($(this).is(':checked')) {
                var len = $(".chkind").length;
                var ind_len = 0;
                $(".chkind").each(function () {
                    if ($(this).is(':checked')) {
                        ind_len++;
                    }
                });

                if (parseInt(len) === parseInt(ind_len)) {
                    $(".chkall").prop("checked", true);
                }
            } else {
                $(".chkall").prop("checked", false);
            }
        });
    } else {
        $(".chkall").prop('click', function () {
            if ($(this).is(':checked')) {
                $(".chkind").prop("checked", true);
            } else {
                $(".chkind").prop("checked", false);
            }
        });
    }
}
 function delete_credit(){
    var check = CheckForm();
    if(check){
        swal({
            title: 'Warning',
            text: 'Are you sure you want to delete this Category',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var data = $("#credit_categories_action").serialize();
              var del_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/deleteCreditCategory"
              $.post(del_url,{'data':data},function(res){
                  location.reload();
              });
          })
    }else{
        swal({
            title: 'Validate',
            text: 'Please select content category for delete',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor primary",
            confirmButtonText: "Ok",
            closeOnConfirm: true,
            html:true,
          });       
    }
 }
  function CheckForm(){
	var checked=false;
	var elements = document.getElementsByName("data[]");
	for(var i=0; i < elements.length; i++){
	if(elements[i].checked)
		checked = true;
	}
	return checked ;
}
function edit_creditcontent(id){
        $(".content_typ").html('Edit');
        var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/getcreditcontentPopup";
        $.post(url,{'id':id,'edit':'yes'},function(res){
          $('#creditcontent_modal').modal('toggle'); 
          $("#creditcontent_modal").modal('show');
          $("#credit_content_modal_form").html(res);
        });    
}
function disable_credit(id,name){
         swal({
            title: 'Disable Credit Category',
            text: 'Are you sure you want to disable <b>'+name+'</b> ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var disable_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/disableEnableCreditCategory";
              $.post(disable_url,{'id':id,'name':name,'type':'disable'},function(res){
                  location.reload();
              });
          });  
}
function enable_credit(id,name){
         swal({
            title: 'Enable Credit Category',
            text: 'Are you sure you want to enable <b>'+name+'</b> ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true,
          },function(){
              var disable_url = "<?php echo Yii::app()->baseUrl; ?>/monetization/disableEnableCreditCategory";
              $.post(disable_url,{'id':id,'name':name,'type':'enable'},function(res){
                  location.reload();
              });
          });  
}
</script>
