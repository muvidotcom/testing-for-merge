<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<!--<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>-->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/new_relese/typeahead.bundle.js"></script>

<?php 
$selcontents = '';
if (isset($data) && !empty($data)) {
    $selcontents = json_encode($data);
}
?>

<div class="row m-t-40">
    <form action="javascript:void(0);" method="post" name="free_content_form" id="free_content_form" data-toggle="validator">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="free_content">Free Content</label>

                    <div class="col-sm-10" style="margin-left: -15px;">
                        <select  data-role="tagsinput" name="data[content][]" placeholder="Type to add new content" id="content" multiple>
                        </select>
                        <label id="data[content][]-error" class="error red" for="data[content][]" style="display: none"></label>
                    </div>
                </div>
            </div>
        </div>
        
        <div style="clear: both; height: 10px;"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-2 control-label">&nbsp;</label>

                    <div class="col-sm-10">
                        <input type="hidden" name="data[is_update]" value="<?php if (trim($selcontents)) { ?>1<?php } else { ?>0<?php } ?>" />
                        <a href="javascript:void(0)" class="btn btn-primary" data-is_update="<?php if (trim($selcontents)) { ?>1 <?php } else { ?>0<?php } ?>" onclick="return validateFreeContentForm(this)"><?php if (trim($selcontents)) { ?>Update<?php } else { ?>Add<?php } ?></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    var sel_contents = '<?php echo $selcontents;?>';
    var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/monetization/getContents", filter: function (e) {
                return e;
            }}});
            content.clearPrefetchCache(),
            content.initialize(),
            $("#content").tagsinput({
                itemValue: function(item) {
                    return item.content_id;
                },
                itemText: function(item) {
                    var item_name = $.trim(item.name);
                    if (item_name) {
                        item_name = item_name.replace("u0027", "'");
                    }
                    return item_name;
                },
                typeaheadjs: {name: "content", displayKey: "name", source: content.ttAdapter()}});
            
            var contnts = $.trim(sel_contents) ? jQuery.parseJSON(sel_contents) : '';
            if (contnts.length) {
                for (var i in contnts) {
                    var content_name = $.trim(contnts[i].name);
                    if (content_name) {
                        content_name = content_name.replace("u0027", "'");
                        $("#content").tagsinput('add', { "content_id": contnts[i].content_id , "name": content_name});
                    }
                }
            }
            
    function validateFreeContentForm(obj) {
        var is_update = $(obj).attr('data-is_update');
        
        if (!$.trim($("#content").val()) && parseInt(is_update) === 0) {
            swal("Please add content");
            return false;
        } else {
            var url = "<?php echo Yii::app()->baseUrl; ?>/monetization/addEditFreeContent";
            document.free_content_form.action = url;
            document.free_content_form.submit();
        }
    }
</script>