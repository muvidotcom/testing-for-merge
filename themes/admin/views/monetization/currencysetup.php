<div class="alert alert-success alert-dismissable flash-msg m-t-20" style="display:none">
    <i class="icon-check"></i> &nbsp;
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <span id="success-msg"></span>
</div>
<div class="row m-t-40">
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon p-l-0"><em class="fa fa-money"></em></span>
            <div id="input-error">
                <div class="fg-line">
                    <input class="search form-control input-sm auto" placeholder="Search by currency name or code" />
                    <input type="hidden" id="curr_id" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="input-group">
            <div class="fg-line">
                <a href="javascript:void(0)" class="btn btn-primary" onclick="addcurrency()">Add</a>
            </div>
        </div>
    </div>
</div>
<div class="row" id="error" style="display: none">
    <div class="form-group input-group">
        <div class="col-md-12">
            <div class="p-l-40" id="error-txt">
            </div>
        </div>
    </div>
</div>

<div class="row m-t-20">
    <div class="col-md-12">
        <div id="currency_list"></div>
    </div>
    <div class="loader text-center centerLoader" style="display:none">
        <div class="preloader pls-blue">
            <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
            </svg>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('.loader').show();
        $.post('/monetization/getCurrencyList',{},function(res){
            $('#currency_list').html(res);
            $('.loader').hide();
        });
    });
    function addcurrency() {
        $('.loader').show();
        var currency = $('#curr_id').val();
        if(currency.trim()){
            var currency = $('#curr_id').val();
        }else{
            var currency = $('.auto').val();
        }
        if(currency.trim()){
            $.post('/monetization/addCurrency',{'currency':currency},function(res){
                var data = $.parseJSON(res);
                if(data.isCurr){
                    $('#input-error').addClass('has-error');
                    $('#error-txt').addClass('red');
                    $('#error-txt').html(data.error);
                    $('#error').show();
                    $('.loader').hide();
                    setTimeout(function(){
                        $('#input-error').removeClass('has-error');
                        $('#error-txt').html('');
                        $('#error').hide();
                    }, 3000);
                }else{
                    $.post('/monetization/getCurrencyList',{},function(curr){
                        $('#currency_list').html(curr);
                        $('.auto').val('');
                        $('.loader').hide();
                    });
                }
            });
        }else{
            $('#input-error').addClass('has-error');
            $('#error-txt').addClass('red');
            $('#error-txt').html("Currency field can't be blank");
            $('#error').show();
            $('.loader').hide();
            setTimeout(function(){
                $('#input-error').removeClass('has-error');
                $('#error-txt').html('');
                $('#error').hide();
            }, 3000);
        }
    }
    $(function() {
	$(".auto").autocomplete({
		source: "filterCurrency",
		minLength: 3,
                select: function( event, ui ) {
                    $( '#curr_id' ).val( ui.item.id );                  
                }
	});				
    });
    
    function makedefault(id) {
        swal({
            title: "Change Default Currency",
            text: 'Are you sure? You want to change your default currency which will affect in all payment sections for your users.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            $('.loader').show();
                $.post('/monetization/makeDefault',{'id':id},function(res){
                if(res){
                    $.post('/monetization/getCurrencyList',{},function(curr){
                        $('#currency_list').html(curr);
                        $('.loader').hide();
                    });
                }
            });
        });
    }
</script>