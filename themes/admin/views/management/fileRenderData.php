<input type="hidden" id="data-count" value="<?php echo $total_image ?>" /> 
<input type="hidden" id="data-count1" value="<?php echo $count_searched ?>" /> 
<input type="hidden" id="page_size" value="<?php echo $page_size?>" /> 
<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
?>
<table class="table" id="list_tbl">
<thead>
            <tr>
                <th style="width:100px;">
                <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" class="sub_chkall" id="check_all" onchange="check_allbox()" >
                        <i class="input-helper"></i>

                    </label>
                </div>
                </th>
                <th>File Name</th>
                <th data-hide="phone" class="width">Properties</th>
                <th data-hide="phone" class="width">Action</th>
        </tr>
        </thead>
        
        <?php
        // echo "<pre>";
        // print_r($image_details);
        if (!empty($allimage)) {
            foreach ($allimage as $key => $val) {
                ?>
        
                <tbody>
                    <tr>
                        <td>
                           <div class="checkbox m-b-15">
                            <label>
                            <input type="checkbox" class="sub_chk" data-id="<?php echo $val['id']; ?>" name="delete_all[]" >
                            <i class="input-helper"></i>
                            
                            </label>
                            </div> 
                        </td>
                        <td>
                            <?php 
                                echo $val['file_name'];                                 
                            ?>
                        </td>

                        <td>
                          <?php echo "Filesize : ".$val['file_size']." MB";?>
                        </td>   



                        <td> 

                            <?php if ($val['flag_uploaded'] == 1) { ?>

                        <p><a title="Video download in progress. It will be available within an hour" data-toggle="tooltip" href="javascript:void(0);"><em class="icon-cloud-download"></em>&nbsp; Download in progress</a></p>

       <?php } else if ($val['flag_uploaded'] == 2) {
            ?>

                       <p data-toggle="tooltip" title="Downloading failed"><em class="fa fa-info-circle" style="color:red;"></em></p>

                   <?php
                    } else {
                        ?>
                        <p>
                            <a href="javascript:delete_file(<?php echo $val['id'];?>)" data-toggle="tooltip" title="Delete Video" data-msg ="Are you sure to delete the File ?" class="confirm" id="confirm"><em class="icon-trash" ></em>&nbsp; Delete File </a>
                        </p>
       <?php } ?>
                </td>
                </tr>	
                </tbody>
       
                <?php }
            } else { ?>					
            <tbody>
                <tr>
                    <td colspan="4">No Files Found</td>
                </tr>
            </tbody>
<?php } ?> 
 </table>

<script>
    $(document).ready(function () {
    $("#check_all").click(function () {
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
});
$(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            confirmDelete(location, msg);
        });
    });
    var change="";
$(function() {
        $(".imageSlide").mouseenter(function() {
            console.log('enter');
            var obj= this;
            var data_slide= $(obj).attr('data-slide');
            var id = $(obj).attr('data-id');
            if(data_slide){
            $.post('/management/ImageThumbnail',{id: id},function(res){
            if(res){
                $.each(res, function(key, value){
                    $(obj).append("<img src='"+value+"' class='hide' />");
                });
                clearInterval(change);
                change = setInterval(function () {
                    var img = $(obj).find("img:not(.hide)");
                    img.addClass("hide");
                    var nextImg = "";
                    if( img[0] === $(obj).find("img").last()[0] ){
                        nextImg = $(obj).find("img").first();
                    } else{
                        nextImg = img.next("img");
                    }
                    nextImg.removeClass("hide").addClass("show");
                }, 2000);
                    };
                   $(obj).attr('data-slide',false); 
            },'json');
            }
        });
        $('.imageSlide').mouseleave(function() {
            console.log("out");
            clearInterval(change);
            $(this).find("img:not(:first)").remove();
            $(this).find('img').first().removeClass('hide');
        });
    });
	function delete_file(id){
		$.post('/management/DeleteFile',{rec_id: id},function(res){
			location.reload();
		});
	}
</script>
    <style>
        .thumbnail {
            width : 280px;
            height: 160px;
        }
        .thumbnail  > img {
            height: 100%;
        }
        .hide {
            display: none !important;
            }
        
    </style>
