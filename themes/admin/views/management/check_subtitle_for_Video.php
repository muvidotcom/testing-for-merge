<style type="text/css">
	.preloader{
		display : none;
		position : absolute;
		left: 44%;
		top: 34%;
	}
	.c-fa{
		color : #2cb7f6 !important;
		font-size : 15px;
	}
	h5{
		margin-top: 0;
	}
</style>
<div>
    <div class="form-group col-sm-12">
        <b>List of Subtitles already added</b>
    </div>
    <div class="form-group col-sm-12">
        <table class="table" id="video_list_tbl">
            <thead>
                <tr>
                    <th>Language</th>
					<th>Display name</th>
                    <th>File Name</th>
                    <th class="width">Action</th>
            </tr>
            </thead>
            <tbody>
			<div id="subtitle_poster_loader" class="preloader pls-blue text-center">
				<svg class="pl-circular" viewBox="25 25 50 50">
				<circle class="plc-path" cx="50" cy="50" r="20"></circle>
				</svg>                                   
			</div>
                <?php
                    if (!empty($getVideoSubtitle)) {
                        foreach ($getVideoSubtitle as $key => $val) {
							$display_name = (isset($val['display_name']) && ($val['display_name'] !='')) ? $val['display_name'] : $val['name'];
							?>
                          <tr>
                          <td><?php echo $val['name']; ?></td>
						 <td><div class="section_settings form-horizontal">
                                <div class="editsubtitlesection hide form-group">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <input type="text" class="subtitle_name form-control" value="<?php echo $display_name; ?>" id="subtitle_name_<?php echo $val['subId']; ?>" name="subtitle_name_<?php echo $val['subId']; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0);"  onclick="updateSubtitleName(<?php echo $val['subId']; ?>);return false;" title="Update"><i class="h3 green icon-check"></i></a>
                                    </div>
                                </div> 
                            </div>
							 <span class="edit_subtitle"><?php echo $display_name; ?> <em class="fa fa-pencil  p-l-20 c-fa"></em></span></td>
                          <td><?php echo $val['filename'] ?></td>
                            <td><h5><a href='<?php  echo $this->createAbsoluteUrl('management/deleteSubtitle',array('subtitleId' => $val['subId'])) ?>' data-msg='Are you sure want to delete the subtitle?' class='confirm'><em class='icon-trash'></em>&nbsp;&nbsp; Delete</a></h5></td>
						</tr>
                      <?php   }
                    } else { ?>	
                        <tr>
                            <td colspan="3">No Data Found</td>
                        </tr>
                <?php } ?> 
            </tbody>
        </table>
    </div>
</div>
<input type="hidden" name="subVideoId" value="<?php echo $video_id; ?>" />
<div id='subLangUpload'>
    <div class="form-group col-sm-12">
        <b>Add Subtitle</b>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            <div class="fg-line">
                <div class="select">
                    <select class="form-control input-sm subtiLang" name="data[subtitle_language][]">
                        <?php foreach ($getSubLang as $getSubLangkey => $getSubLangvalue) { ?>
                            <option value="<?php echo $getSubLangvalue['id']; ?>"><?php echo $getSubLangvalue['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <button type="button" class="btn btn-default-with-bg btn-file btn-sm" onclick="browsesubtitle(this)" >Browse</button>
            <input class="form-control input-sm cost multi-file subtitle_file" type="file" onchange="showfilename(this)" required="required"  name="data[subtitle_file][]" style="display: none;">
            <span>&nbsp;&nbsp;No file chosen</span>
        </div>
    </div>
</div>

<p class="col-sm-12">
    <a href="javascript:void(0);" onclick="addMore('<?php echo $couponType;?>_<?php echo $discountType;?>');" class="text-gray">
        <em class="icon-plus"></em>&nbsp; Add more subtitle
    </a>
</p>
<script>
	
	$('.edit_subtitle').click(function () {
		   $(this).parent().find('.editsubtitlesection').removeClass('hide');
		   $(this).parent().find('.editsubtitlesection').addClass('show');
		   $(this).addClass('hide');
	   });
    function addMore() {
        var isTrue = 0;
        $("#subLangUpload").find(".multi-file").each(function() {
            if ($.trim($(this).val()) !== '') {
                isTrue = 1;
            } else {
                isTrue = 0;
                $(this).focus();return false;
            }
        });
        if (parseInt(isTrue)) {
            $("#subLangUpload").append('<div class="form-group"><div class="col-sm-6"><div class="fg-line"><div class="select"><select class="form-control input-sm subtiLang" name="data[subtitle_language][]"><?php foreach ($getSubLang as $getSubLangkey => $getSubLangvalue) { ?><option value="<?php echo $getSubLangvalue['id']; ?>"><?php echo $getSubLangvalue['name']; ?></option><?php } ?></select></div></div></div><div class="col-sm-6"><button type="button" class="btn btn-default-with-bg btn-file btn-sm" onclick="browsesubtitle(this)">Browse</button><input id="subtitle_file" class="form-control input-sm cost multi-file" onchange="showfilename(this)" type="file" required="required"  name="data[subtitle_file][]" style="display: none;"><span>&nbsp;&nbsp;No file chosen</span></div></div>');
        } 
    }
    function browsesubtitle(browse){
        $(browse).next('input[type=file]').trigger('click');
    }
    function showfilename(browse){
        var filename = $(browse).val();
        filename = "&nbsp;&nbsp;"+filename;
        $(browse).next().html(filename);
    }
	function updateSubtitleName(subId) {
		var title = $.trim($('#subtitle_name_' + subId).val());
		$('#subtitle_poster_loader').show();
		if (title) {
			$.post(HTTP_ROOT + "/management/editSubtitle", {'id': subId, 'name': title}, function (res) {
				$('#subtitle_poster_loader').hide();
				if(res.msg = "Success"){
					swal("Display name is updated successfully.", '','success');
					$('#addsubtitle_popup').modal('hide');
				}else{
					swal("Display name is not updated.",'', 'error');
					$('#addsubtitle_popup').modal('hide');
				}
			});
		} else {
			alert("Section Name can't be blank.");
			return false;
		}
  }
    $(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            confirmDelete(location, msg);
        });
    });
</script>