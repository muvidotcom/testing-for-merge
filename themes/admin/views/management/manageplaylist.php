<style>
	.current_collapse{
		display: none;
	}
	.block-header{
		padding : 0;
	}
	.has-right-icon{
		padding-left : 50px;
	}
	.preloader{
		display: none;
	}
	.icon-OuterArea--rectangular.right{
		width: 80px;
	}
	.btn-cancel{
		margin-top : 10px;
	}
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<div class="row m-b-40">
    <div class="col-xs-12">
		<a href="javascript:void(0);" id="addnew_playlist" onclick="AddNewPlaylist();">
			<button type="submit"  class="btn btn-primary btn-default m-t-10">Create a playlist</button>
        </a>
        <hr>
	</div></div>

<?php
$k = 1;
foreach ($playlist_data as $data) {
	$listData = $data;
	foreach ($listData as $list) {
		$resultData = $list['lists'];
		
		?>
		<script type="text/javascript">
			$(document).ready(function () {
				$('#collap-<?php echo $k; ?>').click(function () {
					$('#playlist_data_<?php echo $k; ?>').toggleClass('current_collapse')
					$('#playlist_data_<?php echo $k; ?>').toggle();
					$('.editPlaylist[data-id=' + <?php echo $k; ?> + ']').toggleClass('hide');
					$('.delete_playlist[data-id=' + <?php echo $k; ?>+ ']').toggleClass('hide');
				
				});
			});
		</script>  
		<div class="Collapse-Block"> 
			<div class="Block-Header has-right-icon"> <!-- Icon exist to left :--> 
				<div class="icon-OuterArea--rectangular"> 
					<?php echo $k; ?>
				</div> <!-- Icon exist to Right : if you need it.-->
				<div class="icon-OuterArea--rectangular right icons">
					<a href="<?php echo Yii::app()->getBaseUrl(true); ?>/management/editPlaylist/playlist/<?php echo $list['playlist_permalink'] ?>"  class="editPlaylist" data-id="<?php echo $k; ?>" data-name="<?php echo addslashes($list['list_name']); ?>" data-id="<?php echo $list['list_id'] ?>">
						<em class="fa fa-pencil-square-o m-r-20" title="Edit" aria-hidden="true"></em>
					</a>
					<a href="javascript:void(0);" id="delete_playlist_btn"  data-id="<?php echo $k; ?>" class='delete_icon delete_playlist' data-name="<?php echo $list['list_name']; ?>" data-id="<?php echo $list['list_id']; ?>">
						<em class="fa fa-trash-o" title="Delete" aria-hidden="true"></em>
					</a>
					<a href="javascript:void(0);" id="collap-<?php echo $k; ?>" data-id="<?php echo $k; ?>"> 
						<em class="fa fa-plus plus_icon" title="Open" data-id="<?php echo $list['list_id']; ?>"></em> 
					</a> 



				</div> <h4><?php echo $list['list_name'] ?> <small> <?php echo $list['counts']; ?> <?php if ($list['counts'] > 1){ echo 'Tracks';}else{echo 'Track';}?></small> </h4> 
			</div> 
			<div class="Collapse-Content current_collapse" id="playlist_data_-<?php echo $k; ?>">
				<div class="m-t-20"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="block-body table-responsive">
							<table class="table table-condensed">
								<tbody>
									<?php
									$i = 1;
									foreach ($resultData as $result) {
										$casts = $result['casts'];
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $result['content_title']; ?></td>
											<td><?php echo implode(',',$result['casting']['castlist']); ?></td>
											<td><?php echo $result['video_duration']; ?></td>
											<td>
												<a href="javascript:void(0);" id="delete_content_btn" data-stream_id="<?php echo $result['movie_stream_id']; ?>" data-list_id="<?php echo $list['list_id']; ?>">
													<em class="fa fa-trash-o" title='Delete' aria-hidden="true"></em>
												</a>
											</td>
										</tr>
										<?php
										$i++;
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php
		$k++;
	}
}
?>
<form name="playlist_form" method="post" id="playlist_form" onsubmit="return validate_form();"  enctype="multipart/form-data"  action="<?php echo $this->createUrl('management/addToPlaylist'); ?>">
<div class="modal fade" id="addPlaylist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog">
		
			<div id="playlist">
				<input type="hidden" id="movie_id" name="movie_id" value="" />
				<input type="hidden" id="user_id" name="user_id" value="0" />
				<input type="hidden" id="is_episode" name="is_episode" value="" />
				<input type="hidden" id="add_content" name="add-content" value="0">
				<input type="hidden" id="list_id" name="list_id" value="">
				<input type="hidden" id="img_width" name="img_width" value="">
				<input type="hidden" id="img_height" name="img_height" value="">
			</div>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><span id='popup-header-txt text_playlist'>Add new playlist</span> </h4>
				</div>
				<div class="modal-body">
					<div class="error red text-red" id="cerror" style="display:none"></div>
					<div class="form-group">
						<label class="control-label col-sm-3">Title<span class="red"><b>*</b></span>:</label>
						<div class="col-sm-9">
							<div class="fg-line">
								<input type="text"  id="new_playlist" name="newplaylist" class="form-control input-sm" value="<?php echo @$data->category_name; ?>" >	                        </div>
							<label class="new_playlist_error red"></label>
						</div>
					</div>
					<div class="form-group">
						<label for="category" class="col-md-3 control-label">Category<span class="red"><b>*</b></span>: </label>
						<div class="col-md-9">
							<div class="fg-line">
								<select  name="content_category_value[]" id="content_category_value" multiple  class="form-control input-sm checkInput" <?php echo $disable; ?>>
									<?php
									foreach ($contentList AS $k => $v) {
										if (@$data && ($data[0]['id'] & $k)) {
											$selected = "selected='selected'";
										} else {
											$selected = '';
										}
										echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
									}
									?>
								</select>
								<label class="playlist_category_error red"></label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Poster:</label>
						<div class="col-sm-9">
							<input class="btn btn-default-with-bg btn-sm" type="button" name="playlist_img" value="Upload Image" data-width="<?php echo @$poster_size['width']; ?>" data-height="<?php echo @$poster_size['height']; ?>" id="playlist_img" onclick="openImageModal(this);" <?php echo $disable; ?>/>
							<span>(Upload a image of <?php echo $poster_size['width'].' x '.$poster_size['height'].'px'; ?> )</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<div id="avatar_preview_div">
									<img class="img-responsive img-playlist-edit">
							</div>
							<div class="canvas-image" style="width:200px;height:200px;display:none">
							<canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
							</div>
						</div>
					</div>
					<div class="preloader pls-blue" id="playlist_loading">
						<svg class="pl-circular" viewBox="25 25 50 50">
						<circle class="plc-path" cx="50" cy="50" r="20"/>
						</svg>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-default m-t-10 save_playlist" name="save_playlist" id="save_playlist" value="save">Save playlist</button>
					<button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		
	</div>
</div>

<div class="modal is-Large-Modal" id="category_img" tabindex="-1" role="dialog" aria-labelledby="category_img">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header text-left">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="section_id1" id="section_id1" value="" />
				<div role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active" onclick="hide_file()">
							<a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
						</li>
						<li role="presentation" onclick="hide_gallery()"> 
							<a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="upload_by_browse">
							<div class="row is-Scrollable">
								<div class="col-xs-12 m-t-40 m-b-20 text-center">
									<input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('browse_cat_img')">
									<input id="browse_cat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
									<p class="help-block"></p>
								</div>
								<input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
								<input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
								<input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
								<input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
								<input type="hidden" class="w" id="w" name="fileimage[w]"/>
								<input type="hidden" class="h" id="h" name="fileimage[h]"/>
								<div class="col-xs-12">
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="upload_preview">
											<img id="preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="upload_from_gallery">
							<input type="hidden" name="g_image_file_name" id="g_image_file_name" />
							<input type="hidden" name="g_original_image" id="g_original_image" />
							<input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
							<input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
							<input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
							<input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
							<input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
							<input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
							<div class="row  Gallery-Row">
								<div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

								</div>
								<div class="col-md-6  is-Scrollable p-t-40 p-b-40">
									<div class="text-center m-b-20 loaderDiv"  style="display: none;">
										<div class="preloader pls-blue  ">
											<svg class="pl-circular" viewBox="25 25 50 50">
											<circle class="plc-path" cx="50" cy="50" r="20"></circle>
											</svg>
										</div>
									</div>
									<div class="Preview-Block row">
										<div class="col-md-12 text-center" id="gallery_preview">
											<img id="glry_preview" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/playlist.js?v=<?= RELEASE;?>"></script>
