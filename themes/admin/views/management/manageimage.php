<?php
/* $image_path= "studio_id/public/image_gallery/$studio_id" */
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);

$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;
?>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a onclick="show_upload()" id="add_btn" data-toggle="modal" data-target="#image_upload">
            <button type="submit" class="btn btn-primary m-t-10">
                Add Image
            </button>
        </a>

    </div>
</div>


<div class="">   
<div class="row">
        <div class="col-md-8">
            <div class="block">
                <div id="show_upload_div" style="display:none;" class="m-b-20">
                    <form class="form-horizontal" action="<?php echo Yii::app()->getbaseUrl(true) ?>/management/uploadAllImage" method="POST" enctype="multipart/form-data" id="upload-image-from" onsubmit="return check_upload();" >  
                       
                    <div class="form-group">
                        <div class="fg-line">
                            <label for="upload" class="col-sm-4 control-label">Browse:</label>
                            <div class="col-sm-8">
                                <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm"  onclick="ShowClickbrowse();">
                              <input type="file" class="" id="Filedata" name="Filedata"  style="display:none;">
                              <span>&nbsp;&nbsp;No file chosen</span>
                            <span id="file_error" class="error red" for="Filedata" style="display: block;"></span>
                            
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8" id="upl_img">
                                
                            </div>
                        </div>
                        <?php if(!empty($checkImageKey) && $checkImageKey->config_value==1){?>
                       <div class="form-group">
                            <label for="upload" class="col-sm-4 control-label">Image Key:</label>
                            <div class="col-sm-8">
                                <div class="fg-line">
                                    <input placeholder="A unique key for this image ex. tomcurise_london" id="unique_key" name="image_key" class="form-control input-sm checkInput" onkeyup="checkimagekey(this)" type="text" autocomplete="off">
                                </div>
                                <span id="image_key_error" class="error red" style="display: block;"></span>
                            </div>
                        </div>
                        <?php }?>
                       <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                   <button id="save-btn" class="btn btn-primary btn-default btn-sm" type="submit">Upload</button>
                        </div>
                        </div>
                        
                        <div class=" has-error">
                            <span class="help-block" id="error"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-b-40">
    <div class="col-xs-3 text-left">
            <div class="form-group input-group">
                <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                <div class="fg-line">
                    <input class="search form-control input-sm" placeholder="Search"/>
                </div>
            </div>
    </div>
    <div class="col-xs-9 text-left">
        
        <a onclick="delete_selected_image()" id="delete_btn">
            <button id="delete_image" type="button" class="btn btn-danger m-t-10">
                Delete Selected
            </button>
        </a>

    </div>
</div>
<input type="hidden" id="page_size" value="<?php echo $page_size?>" />
    <div  id="image_list_tbl">
       
    </div>




<style type="text/css">
   
    .loaderDiv{position: absolute;left: 30%;top:10%;display: none;}
   

</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $v ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<script>
                                    $(document).ready(function () {
                                        $(".preview").click(function () { // Click to only happen on announce links
                                            $("#image_block").attr(
                                                    'src', $(this).data('id'));
                                            $('#preview_image').modal('show');
                                            //alert();

                                        });
                                    });

     
</script>
<!--cropping image functioanlity ---->

<script type="text/javascript">
    
    $(function() {
        
        var searchText = $.trim($(".search").val());
        getimageDetails(searchText);
    });
    
     $(document.body).on('keypress','.search',function (event){
        var searchText = $.trim($(".search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEsearchVideoPagenter = (event.keyCode == 13);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            getimageDetails(searchText,1);
        }
        
        
    });
    
    function getimageDetails(searchText,page)
    {
   
    $.post('/management/searchImagePage',{'search_value':searchText,'page':page},function(res){
        
        $('#image_list_tbl').html(res);
        var mydata = $(res).find('data-count');
        var mydata1=$("#data-count1").val();
        var regex = /\d+/g;
        var count = mydata.prevObject[0].outerHTML.match(regex);       
        var page_size = $('#page_size').val();       
        var total = parseInt(count)/parseInt(page_size);       
        var maxVisible = total/2;
        if(maxVisible <= 5){
            maxVisible = total;
        }
        //alert(mydata1);
        
        if(parseInt(count) < parseInt(page_size) || parseInt(mydata1)<20){
          $('#page-selection').parent().hide();
        }else{
            if($('.page-selection-div').length <= 0){
                $('#image_list_tbl').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
            }
            $('#page-selection').parent().show();
        }
        $('#page-selection').bootpag({
            
            total: Math.ceil(total),
            page: page,
            maxVisible: 5
        }).on('page', function(event, num){
            $.post('/management/searchImagePage',{'page':num},function(res){
                $('#image_list_tbl').html(res);
                $('.loader').hide();
            });
        });
    });
}
    var bootboxConfirm = 0;
    $(function () {
        $("a.confirm").bind('click', function (e) {
            e.preventDefault();
            var location = $(this).attr('href');
            var msg = $(this).attr('data-msg');
            if ($('#dprogress_bar').is(":visible")) {
                var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.';
                var location = $(this).attr('href');
                bootbox.hideAll();
                bootbox.confirm({
                    title: "Upload In progress",
                    message: msg,
                    buttons: {
                        'confirm': {
                            label: 'Stop Upload',
                            className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
                        },
                        'cancel': {
                            label: 'Ok',
                            className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
                        }
                    }, callback: function (confirmed) {
                        if (confirmed) {
                            bootboxConfirm = 1;
                            confirmDelete(location, msg);
                        }
                    }
                });
            } else {
                confirmDelete(location, msg);
            }
        });
    });


    function confirmDelete(location, msg) {
        swal({
                    title: "Delete Content?",
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true,
                    html:true
                }, function() {
                  window.location.replace(location);
                });
       
    }


    function openEmbedBox(embedid) {
        if ($('#' + embedid).is(':visible')) {
            $('#' + embedid).fadeOut(1000)
        } else {
            $('#' + embedid).fadeIn(1000);
        }
    }

    window.addEventListener("beforeunload", function (e) {
        //').is(":visible") && !bootboxConfirm){
        //var confirmationMessage
        if ($('#dprogress_bar').is(":visible") && !bootboxConfirm) {
            var confirmationMessage = "Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.";
            //console.log(confirmationMessage);
            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Webkit, Safari, Chrome
        }
    });</script>

<script>

    clientTarget = new ZeroClipboard($('.copyToClipboard'), {
        moviePath: "<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.swf",
        debug: false
    });

    function openImageEmbedBox(embedid)
    {
        if ($('#' + embedid).is(':visible')) {
            $('#' + embedid).fadeOut(1000)
        } else {
            $('#' + embedid).fadeIn(1000);
        }
    }

    function CopytoImageClipeboard(id)
    {
        $('#' + id).css('display', 'block');
        $('#' + id).animate({
            opacity: 0,
            top: '-=75',
        }, {
            easing: 'swing',
            duration: 500,
            complete: function () {
                $('#' + id).css({'display': 'none', 'opacity': 1, top: '+=75'});
            }
        });
    }


    /**new codes for image management**/
    function check_upload() {
        /* check whether the image is uploaded or not ? */
        var done = 1;
        var file_name = $("#Filedata").val();
        var unique_key_length = $("#unique_key").length;
        $("#file_error, #image_key_error").html("");
        $("#file_error, #image_key_error").css("display","none");
        if (file_name == '') {
             $("#file_error").css("display","block");
            $("#file_error").html("<span>*</span> Please upload the image");
            done = 0;
        } else if(file_name.match(/['|"|-|,]/)){
              $("#file_error").css("display","block");
            $("#file_error").html("<span>*</span> File names with symbols such as ' , - are not supported");  
            done = 0;
        }
        if(unique_key_length==1){
            var unique_key = $("#unique_key").val();
            if(unique_key.trim() == ''){
                $("#image_key_error").css("display","block");
                $("#image_key_error").html("<span>*</span> Please enter image key");
                done = 0;
            } else if(!unique_key.match(/^[a-zA-Z0-9\_]+$/g)){
                $("#image_key_error").css("display","block");
                $("#image_key_error").html("<span>*</span> Image key only allowed letter and underscore(_)"); 
                done = 0;
            } else if(unique_key.length > 150){
                $("#image_key_error").css("display","block");
                $("#image_key_error").html("<span>*</span> Image key length should be 150 or less");
                done = 0;
            }
        }
        if(done == 0){
            return false;
        }else{
            return true;
        }
    }
    var key_xhr;
    function checkimagekey(obj){
        var unique_key = $(obj).val();
        $("#image_key_error").css("display","none");
        if(unique_key.trim()==''){
            $("#image_key_error").css("display","block");
            $("#image_key_error").html("<span>*</span> Please enter image key");
        } else if(!unique_key.match(/^[a-zA-Z0-9\_]+$/g)){
            $("#image_key_error").css("display","block");
            $("#image_key_error").html("<span>*</span> Image key only allowed letter and underscore(_)"); 
        } else if(unique_key.length > 150){
            $("#image_key_error").css("display","block");
            $("#image_key_error").html("<span>*</span> Image key length should be 150 or less");
        } else {
            if (key_xhr) {
                key_xhr.abort();
    }
            key_xhr = $.ajax({
                method: 'post',
                url: HTTP_ROOT + '/management/checkimagekey',
                data: {id:null, image_key:unique_key},
                dataType : 'json',
                success: function(response){
                    if(response.success==0){
                        $("#image_key_error").css("display","block");
                        $("#image_key_error").html("<span>*</span> Image key already exist");
                    }
                }
            });
        }
    }

    function edit_key(id){
        $('#imageKeyText'+id).toggle();
        $('#imageKeyInput'+id).toggle();
        $('#image_key'+id).val($('#imageKeyText'+id).text());
    }

    function updateImageKey(id){
        var unique_key = $('#image_key'+id).val();
        if(unique_key.trim()==''){
            swal("Please enter unique key");
        } else if(!unique_key.match(/^[a-zA-Z0-9\_]+$/g)){
            swal("Image key only allowed letter and underscore(_)"); 
        } else if(unique_key.length > 150){
            swal("Image key length should be 150 or less");
        } else {
            if (key_xhr) {
                key_xhr.abort();
            }
            key_xhr = $.ajax({
                method: 'post',
                url: HTTP_ROOT + '/management/updateimagekey',
                data: {id:id, image_key:unique_key},
                dataType : 'json',
                success: function(response){
                    if(response.success==0){
                        swal(response.message);
                    } else {
                        $('#imageKeyText'+id).toggle();
                        $('#imageKeyInput'+id).toggle();
                        $('#imageKeyText'+id).text(unique_key);
                    }
                }
            });
        }
    }

    function show_upload() {
        $("#show_upload_div").toggle();
        $("#file_error").html("");
    }

function ShowClickbrowse(){
$('#Filedata').click();
}
$(document).ready(function(){
            $('input[type="file"]').change(function(e){

            var fileName = e.target.files[0].name;

            //$(this).after('<span>'+fileName+'</span>');
            $(this).next('span').html(fileName);

        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#Filedata").change(function(){
    $("#file_error").css("display","none");
    readURL(this);
});
$("#check_all").click(function () {
   
        $(".sub_chk").prop('checked', $(this).prop('checked'));
    });
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
             var file = e.target.result;
               
                if (file.substr(0, 10) == 'data:image')
                {
                   
                $('#upl_img').html('<img id="upload_preview" src="'+e.target.result+'" data-original="' + e.target.result + '">');
                $("#upload_preview").load(function(){
                        aspectratio();
                    });
                
                }
            
        }

        reader.readAsDataURL(input.files[0]);
    }
}


function aspectratio() {
   
    $("#upload_preview").each(function () {
        var maxWidth = 200;
        var maxHeight = 200;

        var width = $("#upload_preview").width();
        var height = $("#upload_preview").height();
        var ratioW = maxWidth / width;  // Width ratio
        var ratioH = maxHeight / height;  // Height ratio

        // If height ratio is bigger then we need to scale height
        if (ratioH > ratioW) {

            //  $("#preview" + id).css("width", maxWidth);
            //  $("#preview" + id).css("height", height * ratioW);
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", height * ratioW);// Scale height according to width ratio
        }
        else if (ratioH == ratioW)
        {
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", maxHeight);// Scale height according to width ratio

        }
        else { // otherwise we scale width

            // $("#preview" + id).css("height", maxHeight);
            // $("#preview" + id).css("width", height * ratioH); 
            $("#upload_preview").css("width", maxWidth);
            $("#upload_preview").css("height", height * ratioW);// according to height ratio
        }
    });
}

function delete_selected_image()
{
    
    var allVals = [];  
        $(".sub_chk:checked").each(function() {  
            allVals.push($(this).attr('data-id'));
        });  
        //alert(allVals.length); return false;  
        if(allVals.length <=0)  
        {  
            swal("Please select Image to delete");  
            return false;  
        }  
        else {            
            //show alert as per the videos mapped status
          //check_videomapped(allVals); 
         delete_selected_images(allVals);
       }
 }
 
 function delete_selected_images(allVals)
 {
                            swal({
                                title: "Delete Image?",
                                text:"Are you sure to delete "+allVals.length+" images?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function() { 
                                
                               // console.log("inside");
                               delete_images(allVals);
                                });
                                
 }
 
 function delete_images(allVals)
 {
     $('#delete_btn').after("<img src='"+HTTP_ROOT+"/images/loader.gif' height='20px' class='ajax-loader'/>");
     $("#delete_image").attr("disabled","disabled");
     $('.ajax-loader').show();
    //     var a = $('#page-selection ul.bootpag li.prev').data('lp');
    //     alert(a);
    //     return;
     var curent_active_page =  $('#page-selection ul.bootpag li.active').data('lp');
     var prev_page =  $('#page-selection ul.bootpag li.prev').data('lp');
     var searchText = $.trim($(".search").val());    
     var imagecount_activepage=$("#image_list_tbl > tbody > tr").length;
//     alert(imagecount_activepage);
//     return;
    console.log("Total_count"+imagecount_activepage);
    console.log("active_page"+curent_active_page);
    console.log("prev_page"+prev_page);
    
        if(allVals.length<imagecount_activepage)
       {
        var page=curent_active_page;          
       }
       else
       {
        var page=prev_page;           
       }
       console.log("page"+page);    
          $.ajax({  
                                url:'<?php echo Yii::app()->getBaseUrl(true); ?>/management/DeleteSelectedImages',  
                                method:'POST',  
                                data:{id:allVals},  
                               success:function()  
                                 {  
                                    $('.ajax-loader').remove();
                                     $("#delete_image").attr("disabled",false);
//                                  window.location.reload();
                                    getimageDetails(searchText,page);
                                    swal("Image deleted successfully");
                                }
                            });    
 }

    </script>