<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<style type="text/css" >
.jcrop-keymgr
    {
        display:none !important;
    }
</style>
<div class="row m-t-40">
    <div class="col-sm-12">
        <form class="form-horizontal" action="<?php echo $this->createUrl('mrss/enablePlayerLogo');?>" name="mng_mrss_feed" id="mng_mrss_feed" method="post">
            <div class="checkbox">     
                <label class="col-md-12"><input type="checkbox" id="restrict_player_logo" name="restrict_player_logo" value="0" <?php echo (@$playerLogoRestrictionConfig['config_value'] == 1)?' checked="checked"':''?> /> <i class="input-helper"></i> Show logo on the player (show on bottom right corner of the player)</label>
            </div>
        </form>
    </div> 
</div>
<div id="showPlayerLogoResDiv" <?php echo (@$playerLogoRestrictionConfig['config_value'] == 1)?' style="display:block"':'  style="display:none"'?>>
    
    <form enctype="multipart/form-data" method="post" role="form" id="logoFRM" name="logoFRM" action="<?php echo Yii::app()->baseUrl; ?>/management/savePlayerlogo">
                        <div class="loading" id="subsc-loading"></div>
                        <input type="hidden" id="logo_width" name="logo_width" value="140" />
                        <input type="hidden" id="logo_height" name="logo_height" value="140"/>
                        <div class="col-md-12">
                            <div class="form-group m-b-0">
                                <div class="col-xs-8">
                                    <div class="fg-line">
                                        <input type="button" value="Upload Player Logo" class="btn btn-default-with-bg btn-file btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg">
                                    </div>
                                    <?php 
                                        $studio_id = Yii::app()->common->getStudiosId();
                                        $playerLogoUlr = Yii::app()->general->getPlayerLogo($studio_id);
                                        if($playerLogoUlr != ''){
                                    ?>
                                            <div class="fg-line m-b-20">
                                                <img src="<?php echo $playerLogoUlr;?>" alt="player Logo" />
                                            </div>
                                    <?php
                                        }
                                    ?>
  <div class="modal fade is-Large-Modal bs-example-modal-lg" id="Preview-Video"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Logo</h4></div>
            <div class="modal-body">

                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" onclick="hide_file()">
                            <a id="home-tab" href="#home"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Upload Image</a>
                        </li>
                        <li role="presentation" onclick="hide_gallery()">
                            <a id="profile-tab"  href="#profile" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Library</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">


                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                   
                                    <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm" onclick="click_browse('celeb_pic')">
                                    <input id="celeb_pic" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                    <p class="help-block">Upload a transparent image of size 140x140.</p>
                                    <p id="error_msg" style="color:red" class="help-block"></p>
                                    <input type="hidden" id="x1" name="x1" />
                                    <input type="hidden" id="y1" name="y1" />
                                    <input type="hidden" id="x2" name="x2" />
                                    <input type="hidden" id="y2" name="y2" />
                                    <input type="hidden" id="w" name="w"/>
                                    <input type="hidden" id="h" name="h"/>
                                    <div class="col-xs-12">

                                        <div class="Preview-Block">
                                            <div class="thumbnail m-b-0">
                                                <div class=" m-b-10" id="celeb_preview">

                                                    <!--Place your Image Preview Here-->
                                                    <img id="preview">

                                                </div>
                                            </div>



                                        </div>
                                        <div class="Preview-Block">
                                            <?php if ($celeb['poster']['poster_file_name']) { ?>
                                                <div class="thumbnail m-b-0">
                                                    <div class="relative m-b-10" id="editceleb_preview">
                                                        <?php
                                                        $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                                        ?>
                                                        <img src="<?php echo $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'] ?>" />
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                            <input type="hidden" name="g_original_image" id="g_original_image" />

                            <input type="hidden" id="x13" name="jcrop_allimage[x13]" />
                            <input type="hidden" id="y13" name="jcrop_allimage[y13]" />
                            <input type="hidden" id="x23" name="jcrop_allimage[x23]" />
                            <input type="hidden" id="y23" name="jcrop_allimage[y23]" />
                            <input type="hidden" id="w3" name="jcrop_allimage[w3]" />
                            <input type="hidden" id="h3" name="jcrop_allimage[h3]" />
                            <div class="row  Gallery-Row">
                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="video_content_div">
                                    <ul class="list-inline text-left">
                                        <?php
                                        $studio_id = Yii::app()->user->studio_id;
                                        $base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
                                        foreach ($all_images as $key => $val) {
                                            if ($val['image_name'] != '') {
                                                $img_path = $base_cloud_url . $val['s3_thumb_name'];
                                                $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                                            }
                                            ?> 
                                            <li>
                                                <div class="Preview-Block">
                                                    <div class="thumbnail m-b-0">
                                                        <div class="relative m-b-10">
                                                            <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                                                            <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                                                            <img class="img" src="<?php echo $img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >

                                                            <div class="caption overlay overlay-white">
                                                                <div class="overlay-Text">
                                                                    <div>
                                                                        <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="toggle_preview(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                                                            <span class="btn btn-primary icon-with-fixed-width">
                                                                                <em class="icon-check"></em>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </li>
<?php } ?>

                                    </ul>
                                </div>
                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue text-center " >
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="gallery_preview">
                                           
                                                    <img id="glry_preview" />

                                        </div>
                                    </div>	


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="logoSubmit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>

                                    <!--Preview Window-->
                                    <div class="fixedWidth--Preview m-b-20">

                                        <img src="<?php echo $logo_path ?>" alt="<?php echo $studio->name ?>" />

                                    </div>

                                   

                                </div>
                            </div>
                        </div>
                    </form>
    
</div>
<div class="row m-t-10">
    <div class="col-sm-12">
        <form class="form-horizontal" action="<?php echo $this->createUrl('mrss/enableEmbedUrlRestiction');?>" name="mng_mrss_feed" id="mng_mrss_feed" method="post">
            <div class="checkbox">     
                <label><input type="checkbox" id="restrict_embed_player" name="restrict_embed_player" value="0" <?php echo (@$embedUrlRestrictionConfig['config_value'] == 1)?' checked="checked"':''?> /> <i class="input-helper"></i> Restrict Embed Player only to the website</label>
            </div>
        </form>
    </div> 
</div>
<div  id="showEmbedUrlResDiv"  <?php echo (@$embedUrlRestrictionConfig['config_value'] == 1)?' style="display:block"':'  style="display:none"'?>>
    <div class="row m-t-40">
        <form action="<?php echo $this->createUrl('management/embedUrlRestiction');?>" name="embed_url_res" id="embed_url_res" method="post">
            <div class="col-sm-5">
                <div class="input-group form-group">
                    <span class="input-group-addon p-l-0"><i class="icon-link"></i></span>
                    <div class="fg-line">
                        <input name="embed_url_restriction" autocomplete="off" id="embed_url_restriction" class="form-control input-sm" type="text" required="required" placeholder="muvi.com">
                    </div>
                </div>    
            </div>
            <div class="col-md-3">
                <button id="sub-btn" class="btn btn-primary btn-sm" name="submit"  onclick="return validate_embed_url_restriction();">Save</button>
            </div>
        </form> 
    </div>
    <div class="row m-t-20">
        <div class="col-sm-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sl.No</th>
                        <th>Website Url</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($embedUrlRestriction){
                            $sr =1;
                            foreach($embedUrlRestriction as $embedUrlRestrictionkey => $embedUrlRestrictionVal){
                                echo '<tr>';
                                    echo '<td>'.$sr.'</td>';
                                    echo '<td>'.$embedUrlRestrictionVal['website_url'].'</td>';
                                    echo '<td>
                                            <h5><a href="javascript:void(0);"  url_restriction_id='.$embedUrlRestrictionVal['id'].'  onclick="showConfirmPopup(this);" > <em class="icon-trash"></em>&nbsp; Remove</a></h5>
                                          </td>';
                                echo '</tr>';
                                $sr++;
                            }
                        }else{
                            echo "<tr><td colspan='3'>No data found.</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row m-t-10">
    <div class="col-sm-12">
        <form class="form-horizontal" method="post">
            <div class="checkbox ">     
                <label><input type="checkbox" id="add_watermark" name="add_watermark" <?php echo (@$addWatermarkConfig['config_value'] == 1)?' checked="checked"':''?>/> <i class="input-helper"></i> Add watermark on the player</label>
            </div>
            <div class="m-t-10 m-l-30"id="watermarkoption" <?php echo (@$addWatermarkConfig['config_value'] == 1)?' style="display:block"':'  style="display:none"'?>>
                    <label class="checkbox checkbox-inline p-r-40">
                     <input type="checkbox" id="option_watermark1" name="option_watermark1" onclick="showDetails()" <?php echo (@$selectWatermarkOption['email'] == 1)?' checked="checked"':'' ?>/>
                     <i class="input-helper"></i>
                      User Email Address
                    </label>
                                   <label class="checkbox checkbox-inline p-r-40">
                                       <input type="checkbox" id="option_watermark2" name="option_watermark2" onclick="showDetails()" <?php echo (@$selectWatermarkOption['ip'] == 1)?' checked="checked"':'' ?>/>
                     <i class="input-helper"></i>
                     IP Address
                    </label>
                                   <label class="checkbox checkbox-inline p-r-40">
                     <input type="checkbox" id="option_watermark3" name="option_watermark3" onclick="showDetails()" <?php echo (@$selectWatermarkOption['date'] == 1)?' checked="checked"':'' ?>/>
                     <i class="input-helper"></i>
                     Date
                    </label>
                </div>
        </form>
    </div> 
</div>

<div class="modal fade" id="websiteUrl" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Website Url?</h4>
            </div>
            
            <div class="modal-body">
                <form class="form-horizontal" action="<?php echo $this->createUrl('management/removeWebsiteUrlForEmbed');?>"  name="submodal" id="submodal" method="post">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span>Are you sure you want to <b>Delete Website Url?</b> </span> 
                        <input type="hidden" id="embUrl_id" name="embUrl_id" value="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <!--a href="javascript:void(0);" id="mrss_list" class="btn btn-default" onclick="removeBox();">Yes</a-->
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#restrict_embed_player').click(function() {
        var enableRestriction = 0;
        if(this.checked){
            enableRestriction = 1;
        }
        var url = "<?php echo Yii::app()->baseUrl; ?>/management/enableEmbedUrlRestiction";
        $.post(url,{enableRestriction:enableRestriction},function(res){
            if(res == 1){
                $("#showEmbedUrlResDiv").show();
            } else{
                $("#showEmbedUrlResDiv").hide();
            }
        });
    });
    $('#restrict_player_logo').click(function() {
        var enableRestriction = 0;
        if(this.checked){
            enableRestriction = 1;
        }
        var url = "<?php echo Yii::app()->baseUrl; ?>/management/enablePlayerLogo";
        $.post(url,{enableRestriction:enableRestriction},function(res){
            if(res == 1){
                $("#showPlayerLogoResDiv").show();
            } else{
                $("#showPlayerLogoResDiv").hide();
            }
        });
    });
    function validate_embed_url_restriction(){
        $("#embed_url_res").validate({
            rules: {
                "embed_url_restriction": {
                    required: true
                }
            },
            messages: {
                "embed_url_restriction": {
                    required: "Please provide Website Url!"
                }
            },
            submitHandler: function (form) {
                document.embed_url_res.submit();
                    return false;     
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
    }
    function showConfirmPopup(obj) {
        $("#embUrl_id").attr('value', $(obj).attr('url_restriction_id'));
         swal({
            title: "Delete Website Url?",
            text: "Are you sure you want to <b>Delete Website Url?</b>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            removeBox();
        });
        
        
    }
    function removeBox() {
        document.submodal.submit();
    }
    function fileSelectHandler() {
        var logo_width=$("#logo_width").val();
        var logo_height=$("#logo_height").val();
        clearInfo();
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview").hide();
        $("#celeb_preview").removeClass("hide");
        $('#logoSubmit').removeAttr('disabled');
        // get selected file
        var oFile = $('#celeb_pic')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
                //alert( rFilter.test(oFile.type));
                if (!rFilter.test(oFile.type)) {
                    swal('Please select a valid image file (jpg and png are allowed)');
                    $("#celeb_preview").addClass("hide");
                    document.getElementById("celeb_pic").value = "";
                    $('#logoSubmit').attr('disabled', 'disabled');
                    return;
                }
                
        
        var aspectratio=logo_width/logo_height;
        var img = new Image();

            img.src = window.URL.createObjectURL( oFile );
            
            var width = 0;
            var height = 0;
            img.onload = function() {
                var width = img.naturalWidth;
                var height = img.naturalHeight;
                window.URL.revokeObjectURL( img.src );
               
            if (width < logo_width || height < logo_height) {
                swal('You have selected small file, please select one bigger image file');
                $("#celeb_preview").addClass("hide");
                document.getElementById("celeb_pic").value = "";
                $('#logoSubmit').attr('disabled', 'disabled');
                return;
                }
               

                // preview element
                var oImage = document.getElementById('preview'); 
                // prepare HTML5 FileReader
                var oReader = new FileReader();
                oReader.onload = function(e) {
                $('.error').hide();    
                // e.target.result contains the DataURL which we can use as a source of the image
                oImage.src = e.target.result;
                oImage.onload = function () { // onload event handler
                    // destroy Jcrop if it is existed
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                        $('#glry_preview').width("450");
                        $('#glry_preview').height("250");
                    }
                     $('#preview').css("display","block");
                     $('#celeb_preview').css("display","block"); 
                    //setTimeout(function(){
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        minSize: [logo_width,logo_height], // min crop size
                        maxSize: [logo_width, logo_height], // max crop size
                            aspectRatio :aspectratio, // keep aspect ratio 1:1
                            boxWidth:450,
                            boxHeight: 300,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function () {

                            // use the Jcrop API to get the real image size
                            var bounds = this.getBounds();
                            boundx = bounds[0];
                            boundy = bounds[1];

                        // Store the Jcrop API in the jcrop_api variable
                        // Store the Jcrop API in the jcrop_api variable
                        jcrop_api = this;
                        //jcrop_api.animateTo([10, 10, 290, 410]);
                        jcrop_api.setSelect([10, 10, logo_width, logo_height]);
                    });
                    //},100);

                    };
                };

            // read selected file as DataURL
            oReader.readAsDataURL(oFile);
        };
    }
    function toggle_preview(id, img_src, name_of_image)
    {
        $('#glry_preview').css("display","block");
        document.getElementById("celeb_pic").value = "";
        showLoader(); 
        var logo_width=$("#logo_width").val();
        var logo_height=$("#logo_height").val();
        var aspectratio=logo_width/logo_height;
       // $("#glry_preview").css("display","none");
        var image_file_name = name_of_image;
        //$("#thumb_"+id).css("border","3px solid #efefef !important");
        var image_src = img_src;
        clearInfo();
        $("#g_image_file_name").val(image_file_name);
        $("#g_original_image").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        //alert(image_type);
        var img = new Image();
        img.src = img_src;

        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            if (width < logo_width || height < logo_height) {
                showLoader(1); 
                swal('You have selected small file, please select one bigger image file more than '+logo_width+' X '+logo_height);
                $("#celeb_preview").addClass("hide");
                $("glry_preview").addClass("hide");  
                $("#g_image_file_name").val("");
                $("#g_original_image").val("");
                $('#logoSubmit').attr('disabled', 'disabled');
                return;
            }

            // preview element
            var oImage = document.getElementById('glry_preview');
            showLoader(1)
            oImage.src = img_src;
           // $("#glry_preview").css("display","block");
            oImage.onload = function () {
                //alert(oImage.naturalWidth);
                // alert(oImage.naturalHeight);
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview').width(oImage.naturalWidth);
                    $('#glry_preview').height(oImage.naturalHeight);
                     $('#preview').width("450");
                     $('#preview').height("250");
                }
                $("#glry_preview").css("display","block");
                 $('#gallery_preview').css("display","block"); 
                //setTimeout(function(){
                // initialize Jcrop
                $('#glry_preview').Jcrop({
                    minSize: [logo_width, logo_height], // min crop size
                    maxSize: [logo_width, logo_height], // max crop size
                     
                            aspectRatio :aspectratio, // keep aspect ratio 1:1
                            boxWidth: 450,
                            boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoallImage,
                    onSelect: updateInfoallImage,
                    onRelease: clearInfoallImage
                }, function () {

                    // use the Jcrop API to get the real image size
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, logo_width, logo_height]);
                });
                //},100);

            };
            //};

            // read selected file as DataURL
            //oReader.readAsDataURL(oFile);                
        };

    }
    function hide_file()
    {
        $('#glry_preview').css("display","none"); 
        $('#celeb_preview').css("display","none"); 
        $('#preview').css("display","none");
        document.getElementById('celeb_pic').value= null;
    }
    
    function hide_gallery()
    {
        $('#preview').css("display","none");
        $("#glry_preview").css("display","block");
        $('#gallery_preview').css("display","none"); 
        $("#g_image_file_name").val("");
        $("#g_original_image").val("");
    }
    
    function click_browse(modal_file) {
        $("#" + modal_file).click();
    }
    
    function showLoader(isShow){
        if(typeof isShow=='undefined'){
                $('.loaderDiv').show();
                $('#showPlayerLogoResDiv button,input[type="text"]').attr('disabled','disabled');
        }else{
                $('.loaderDiv').hide();
                $('#showPlayerLogoResDiv button,input[type="text"]').removeAttr('disabled');
        }
    }
    
    function showDetails()
    {		
            var wm_emailAddress = $("#option_watermark1").is(":checked") ? 1 : 0;
            var wm_ipAddress = $("#option_watermark2").is(":checked") ? 1 : 0;
            var wm_date = $("#option_watermark3").is(":checked") ? 1 : 0;
            var url = "<?php echo Yii::app()->baseUrl; ?>/management/watermarkOption";
            $.post(url,{wm_emailAddress:wm_emailAddress,wm_ipAddress:wm_ipAddress,wm_date:wm_date},function(res){

            });
            
    }
    $('#add_watermark').click(function() {
        var addWatermark = 0;
        if(this.checked){
            addWatermark = 1; 
            $('#option_watermark1').attr('checked', true);
            $('#option_watermark2').attr('checked', true);
            $('#option_watermark3').attr('checked', true);
        }
        
        var url = "<?php echo Yii::app()->baseUrl; ?>/management/addWatermarkOnPlayer";
        $.post(url,{addWatermark:addWatermark},function(res){
            showDetails();
            if(res == 1){
                $("#watermarkoption").show();
                
            } else{
                $("#watermarkoption").hide();
            }
        });
    });
</script>