<?php

$studio = $this->studio;
$posterImg = "https://d1yjifjuhwl7lc.cloudfront.net/public/no-image-h-thumb.png";
if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;



if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;
?>

<!--link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/normalize.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css"-->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/modernizr-2.6.2.min.js"></script>


<div id="myvisyncLoader"  style="display: none; position: absolute; z-index: 99999999999999999999999999999;">
    <header   class="entry-header">
        <h1 class="entry-title">Files are now syncing to your file gallery. Do not close this window</h1>
    </header>

    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
</div>
<div class="modal fade" id="videoSync" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure to sync the files with your server?</h4>
            </div>
            <div class="modal-body">
                <p>The files which are in <b>MuviSync</b> folder will be added to file gallery and deleted from the folder.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" onclick="videoSync()">Yes</button>
                <button data-dismiss="modal" class="btn btn-primary" type="button">Cancel</button>
            </div>  
        </div>
    </div>
</div>

<div class="modal fade" id="videogallery" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Sync File from your ftp server?</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-default-with-bg" type="button" onclick="UploadVideoS3()">Yes</button>
                <button data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
            </div>  
        </div>
    </div>
</div>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a onclick="show_upload()" data-toggle="modal" data-target="#image_upload">
            <button type="submit" class="btn btn-primary btn-default m-t-10">
                Upload File
            </button>
        </a>
       <?php 
       
       
       if (isset($getStudioConfig['config_value']) && $getStudioConfig['config_value']==1) {
           
         ?>
      <a onclick="UploadVideoS3" data-toggle="modal" data-target="#videogallery" data-toggle="tooltip">
               <button type="submit" class="btn btn-primary btn-default m-t-10" >
                Sync to Video Library
                </button></a>
       <?php } ?>
        <?php if (Yii::app()->user->studio_s3bucket_id != 0) { ?>
<!--            <a class="btn bg-olive btn-flat margin btn_add_movie" data-toggle="modal" data-target="#videoSync" data-toggle="tooltip" title="Maximux video file size should be less than 5GB."><em class="fa fa-refresh fa-spin"></em> <button type="submit" class="btn btn-primary btn-default m-t-10">Sync with On-premise Server </button></a>-->
        <?php } ?>
    </div>
</div>
<div class="">



    <div>
        <div class="col-md-12"> 
            <div class="block">
                <div id="show_upload_div" style="display:none;" class="m-b-20">
                    <form class="form-horizontal">
                    <div class="form-group">
                        <div class="form-horizontal">	
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label for="uploadVideo" class="control-label">Browse &nbsp;</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="hidden" value="?" name="utf8">
									<div class="savefile" id="browes_div">
										<div class="col-md-4">
											<input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm"  onclick="ShowClickbrowse();">
											<input type="file" class="" id="Filedata" name="Filedata" onchange="checkfileSize()" style="display:none;">
											<span>&nbsp;&nbsp;No file chosen</span>
											<span id="file_error" class="error" for="Filedata" style="display: block;"></span>
											<input type="hidden" id="video_exist_flag" name="video_exist_flag" >
										</div>
									</div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="loaderDiv" style="display: none;">
                        <div class="preloader pls-blue">
                            <svg viewBox="25 25 50 50" class="pl-circular">
                            <circle r="20" cy="50" cx="50" class="plc-path"/>
                            </svg>
                        </div>
                       
                    </div>
                    
                    </form>
                </div>
            </div>
        </div>  
    </div> 
    <div class="row">
        <div class="col-sm-3 p-r-0">
            <div class="form-group input-group">
                <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                <div class="fg-line">
                    <input class="search form-control input-sm" placeholder="Search"/>
                </div>
            </div>
        </div>
        <div class="col-sm-1 p-r-0">
            <div class="form-group"> 
                <div class="fg-line">
                <button class="btn btn-danger" onclick="delete_selected()">Delete Selected</button> 
                </div>
            </div>
        </div>
    </div>
    <div  id="video_list_tbl">
       
    </div>
    <!--<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div>-->
</div>
<!-- Progress Bar popu -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
  <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
  </div>
  <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Progress Bar popu end -->  


<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/jquery.bootpag.min.js"></script>
<style type="text/css">
   
    .loaderDiv{position: absolute;left: 30%;top:10%;display: none;}
</style>
<script>
    $(function() {
        
        var searchText = $.trim($(".search").val());
        getvideoDetails(searchText);
    });
    
     $(document.body).on('keypress','.search',function (event){
        var searchText = $.trim($(".search").val());
        var c= String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode == 8 || event.keyCode == 46);
        var isEsearchVideoPagenter = (event.keyCode == 13);
        var isEnter = (event.keyCode == 13);
        if((isWordCharacter || isBackspaceOrDelete || isEnter) && (searchText.length > 2 || searchText.length <= 0)){
            getvideoDetails(searchText);
        }
        
        
    });
    
    function getvideoDetails(searchText){
		var url = "<?php echo Yii::app()->baseUrl; ?>/Management/searchFilePage";		
		$.post(url,{'search_value':searchText},function(res){
        $('#video_list_tbl').html(res);
        var mydata1=$("#data-count1").val();
		var count = $("#data-count").val();
        var page_size = $('#page_size').val();
        var total = parseInt(count)/parseInt(page_size);
        var maxVisible = total/2;
         if(maxVisible >= 10){
             maxVisible = 10;
         }else{
            maxVisible = Math.ceil(total);
         }
         if($('.page-selection-div').length){
            $('.page-selection-div').remove(); 
        }
        if(parseInt(count) < parseInt(page_size)  || parseInt(mydata1)<20){
            $('#page-selection').parent().hide();
        }else{
        if($('.page-selection-div').length){
            $('#page-selection').parent().show();
        }else{
            $('#video_list_tbl').after('<div class="page-selection-div"><div id="page-selection" class="pull-right"></div></div><div class="h-40"></div>');
            $('#page-selection').parent().show();
        }
    }
        $('#page-selection').bootpag({
            total: Math.ceil(total),
            page: 1,
            maxVisible: Math.round(maxVisible)
        }).on('page', function(event, num){
				$.post('/management/searchFilePage',{'search_value':searchText,'page':num},function(res){
                $('#video_list_tbl').html(res);
                $('.loader').hide();
            });
        });
    });
}    
    $(function () {
        $('#dprogress_bar').draggable({
            containment: 'window',
            scroll: false
        });
    });
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/video.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/vast/css/video.js.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/video_style.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

<!-- Script for Upload Trailer -->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery-ui-timepicker-addon.js"></script>


<script type="text/javascript">

            function confirmDelete(location, msg) {
                swal({
                    title: "Delete Content?",
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true,
                    html:true
                }, function() {
                  window.location.replace(location);
                });
               
            }
    function click_browse() {
        $("#all_videofile").click();
    }
    function checkfileSize() {
        //swal();
        var movie_name = 'All video';
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "" + currentdate.getMonth() + "" + currentdate.getFullYear() + "" + currentdate.getHours() + "" + currentdate.getMinutes() + "" + currentdate.getSeconds();
        var stream_id = '<?php echo $studio_id; ?>' + datetime;

        var filename = $('#Filedata').val().split('\\').pop();
        //swal(filename);
        var extension = filename.replace(/^.*\./, '');


        if (extension == filename) {
            extension = '';
        } else {
            extension = extension.toLowerCase();
        }
		var myArray = ['html','exe','php','net','java','script']
		var flag = $.inArray(extension, myArray);
		if(flag != -1){
			 swal('Sorry! This file format is not supported.');
			 return false;
		}
        

swal({
  title: "Confirm Upload",
  text: "Are you sure to upload " + filename + " ?",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#10CFBD",customClass: "cancelButtonColor",
  confirmButtonText: "Upload",
  cancelButtonText: "Cancel",
  closeOnConfirm: true,
  closeOnCancel: true
},
function(isConfirm){
  if (isConfirm) {
   $("#show_upload_div").hide();

            $("#addvideo_popup").modal('hide');

            if (!$('#dprogress_bar').is(":visible"))
            {
                $('#dprogress_bar').show();
            }

            upload('user', 'pass', stream_id, movie_name);
            return true;

  } else {
	    document.getElementById("Filedata").value = "";
            return false;
  }
});
        
       

    }
// Multipart Upload Code
   var s3upload = null;
    var s3obj = new Array();
    var size = '';
    var sizeName = '';
    var seconds;
    function timeCal(seconds){
         var hours   = Math.floor(seconds / 3600);
         var minutes = Math.floor((seconds - (hours * 3600)) / 60);
         var sec = seconds - (hours * 3600) - (minutes * 60);

          if (hours   < 10) {hours   = "0"+hours;}
          if (minutes < 10) {minutes = "0"+minutes;}
          if (sec < 10) {sec = "0"+sec;}
          var  timeLeft = hours+':'+minutes+':'+sec;
         return timeLeft;
}
    function upload(user, pass, stream_id, filename) {

        //var xhr = new XMLHttpRequest({mozSystem: true});
        var filename = $('#Filedata').val().split('\\').pop();
        //swal(filename);
        if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
            swal("Sorry! You are using an older or unsupported browser. Please update your browser");
            return;
        }

        /*if (s3upload != null) {
         swal("Though it is possible to upload multiple files at once, this demonstration does not allowed to do so to demonstrate pause and resume in a simple manner. Sorry :-(");
         return;
         }*/
        var sizeleft = 0;
        var timeLeft = '00:00:00';
		var sizeKb;
        var sizeLeftKb;
        var speed="0 kbps";
        var speedMbps;
        var startTime = (new Date()).getTime();
        var file = $('#Filedata')[0].files[0];
        // swal(file.name);
        //swal(file.type);
        // swal(file.size);
        
        size = Math.round(file.size/1000);
        sizeKb=size;
        timeLeft = ((sizeKb/1000)/userinternetSpeed).toFixed(0);
        if(timeLeft < 4){
            timeLeft = 4;
        }
        if((size/1000)<6){
            speed=userinternetSpeed+"mbps";
        }
        timeLeft = timeCal(timeLeft);
        sizeName = 'Kb';
        if(size > 1000){
            size = Math.round(size/1000);
            sizeKb = size*1000;
            var sizeName = 'MB';
        }
        if(size > 1000){
            size = size/1000;
            size = size.toFixed(2);
            sizeKb = size*1000;
            var sizeName = 'GB';
        }
        filename = filename.replace(/(\.[^/.]+)+$/, "").replace(/[^a-z0-9.\s]/gi, '_').replace(/ /g, "_").replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '') + "." + (filename).replace(/^.*\./, '');
        //swal(filename);

        s3upload = new S3MultiUpload(file, {user: user, pass: pass, 'movie_stream_id': stream_id, 'uploadType': 'filegallery', 'file_name': filename}, 'management');
        s3upload.onServerError = function (command, jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 403) {
                swal("Sorry you are not allowed to upload");
            } else {
                console.log("Our server is not responding correctly");
            }
        };

        s3upload.onS3UploadError = function (xhr) { 
            s3upload.waitRetry();
            console.log("Upload is failing, we will retry in " + s3upload.RETRY_WAIT_SEC + " seconds");
        };

        //s3upload.onProgressChanged = function(uploadingSize, uploadedSize, totalSize) {
         s3upload.onProgressChanged = function (progPercent) {
            var endTime = (new Date()).getTime();
            var duration = (endTime-startTime)/1000;
            if (progPercent == 100) {
                $('#upload_' + stream_id + ' .progress-bar-success').css('width', '100%');
                $('#upload_'+stream_id+' .uploadFileSizeProgress').html(size);
                $('#upload_'+stream_id+' .timeRemaining').html("00:00:00");
            } else {
                var progper = parseFloat($('#upload_' + stream_id + ' .progress-bar-success').attr('percent')) + parseFloat(progPercent);
                $('#upload_' + stream_id + ' .progress-bar-success').attr('percent', progper);

                $('#upload_' + stream_id + ' .progress-bar-success').css('width', progper + '%');
                
                if(sizeName == 'GB'){
                    sizeleft = (progper * size)/100;
                    sizeleft = sizeleft.toFixed(2);
                }else{
                    sizeleft = Math.round((progper * size)/100);
                }
                if(sizeName == 'GB'){
                    speed=((sizeleft*1000*1000)/duration).toFixed(2) ;
                    sizeLeftKb = sizeleft*1000*1000;
                }
                if(sizeName == 'MB'){
                    speed=((sizeleft*1000)/duration).toFixed(2) ; 
                    sizeLeftKb = sizeleft*1000;
                }
                if(sizeName == 'KB'){
                    speed=((sizeleft)/duration).toFixed(2);
                    sizeLeftKb=sizeleft;
                }
                speedMbps=(speed/1000).toFixed(2);
                seconds = ((sizeKb-sizeLeftKb)/speed).toFixed(0);
                timeLeft = timeCal(seconds);         
                if(speed > 999){
                    speed=(speed/1000).toFixed(2)+"mbps";
                }
                else{
                    speed=speed + "kbps";
                }
                $('#upload_'+stream_id+' .uploadFileSizeProgress').html(sizeleft);
                $('#upload_'+stream_id+' .uploadSpeed').html(speed);
                $.post(HTTP_ROOT+"/user/setUserInterNetSpeed", {speedMbps: speedMbps}, function (res) {});
                $('#upload_'+stream_id+' .timeRemaining').html(timeLeft);
            }
        };


        s3upload.onUploadCompleted = function (data) {
            console.log(data);
            var obj = jQuery.parseJSON(data);
            $('#upload_' + stream_id).remove();
            if ($('#all_progress_bar').is(":empty")) {
                $('#dprogress_bar').hide();

            }
            else
            {
                $('#dprogress_bar').show();
            }
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg" style="display: block;"><i class="icon-check"></i>&nbsp;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wow! File upload was successful.</div>'
            $('.pace').prepend(sucmsg);
            console.log("Congratz, upload is complete now");
            if ($('#all_progress_bar').is(":empty")) {
                location.reload();
            }

        };
        s3upload.onUploadCancel = function () {
            $('#upload_' + stream_id).remove();
            if ($('#all_progress_bar').is(":empty")) {
                $('#dprogress_bar').hide();
            }
            console.log("Upload Cancelled..");
        };

//Start s3 Multipart upload	
        s3upload.start();
        s3obj[stream_id] = s3upload;
       var progressbar = '<div id="upload_' + stream_id + '" class="upload"><h5 style="word-wrap: break-word; margin-top: 10px; line-height: 1.5; margin-bottom: 10px;">' + filename + ' &nbsp;&nbsp;<a href="javascript:void(0);" id="cancel_' + stream_id + '" class="pull-right cancel" style="cursor:pointer;"><i class="fa fa-remove"></i></a>&nbsp;<br/> (<span class="uploadFileSizeProgress">'+sizeleft+'</span>/'+size+' '+sizeName+') (<span class="uploadSpeed">'+speed+'</span>)<span class="timeRemaining" style="float:right">'+timeLeft+'</span></h5><div class="progress xs progress-striped active" ><div class="bar progress-bar progress-bar-success bgm-green" percent="0" style="width: 0%"></div></div></div>';
        $('#all_progress_bar').append(progressbar);
        $('#cancel_' + stream_id).on('click', function () {
            aid = this.id;
            sid = aid.split('_');
            swal({
                title: "Cancel Upload?",
                text: "Are you sure you want to cancel this upload?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            }, function () {
                s3obj[sid[1]].cancel();
                document.getElementById("Filedata").value = "";
                //$('#dprogress_bar').html('');
                //$('#dprogress_bar').hide();
            });

        });
    }


    function manage_progressbar() {
        $("#all_progress_bar").toggle('slow');
    }
    function show_upload()
    {

        $("#show_upload_div").toggle();
        $("#file_error").html("");
    }   

    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
        } else {
            $('.loaderDiv').hide();
            $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
        }
    }
    function videoSync() {
        $('#myvisyncLoader').show();
        $("#videoSync").modal('hide');
        $('#videoSync button').attr('disabled', 'disabled');
        $.post('<?php echo Yii::app()->getBaseUrl(true); ?>/management/videoSync', function (res) {
            window.location = "<?php echo Yii::app()->getBaseUrl(true); ?>/management/managevideo";
        });
    }
    function UploadVideoS3()
   {
    // $('#myvisyncLoader').show();
       // $("#videogallery").modal('hide');
       
          $('#videogallery button').attr('disabled', 'disabled');
        $.post('<?php echo Yii::app()->getBaseUrl(true); ?>/management/UploadVideoS3', function (res) {
          
            window.location = "<?php echo Yii::app()->getBaseUrl(true); ?>/management/managevideo";
        });  
   }
   
function ShowClickbrowse(){
$('#Filedata').click();
}

function delete_selected()
{
    
    var allVals = [];  
        $(".sub_chk:checked").each(function() {  
            allVals.push($(this).attr('data-id'));
        });  
        //alert(allVals.length); return false;  
        if(allVals.length <=0)  
        {  
            swal("Please select File to delete");  
        }  
        else {            
            //show alert as per the videos mapped status
           check_videomapped(allVals);  
            }
 }
function check_videomapped(allVals)
{
   //show alert as per the videos mapped
  
            $.ajax({  
                          url:'<?php echo Yii::app()->getBaseUrl(true); ?>/management/CheckMappedFile',  
                          method:'POST',  
                          data:{id:allVals},  
                          success:function(res)  
                          { 
                            
                            if(res==0)
                            {
                                
                            //swal("Are you sure to delete "+allVals.length+" videos?"); 
                                swal({
                                title: "Delete Content?",
                                text:"Are you sure to delete "+allVals.length+" files?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function() {
                              delete_multiple_video(allVals);
                            });
                            
                            }
                            else
                            {
                            //swal("Following videos are mapped to a content. Are you sure to delete them?");      
                            swal({
                                title: "Delete Content?",
                                text:"Following videos are mapped to a content. Are you sure to delete them?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true,
                                html:true
                            }, function() {
                              delete_multiple_video(allVals);
                            });
                            }
                        //delete video
                           
                           }
                  });               
}


//delete viudeo
function delete_multiple_video(allVals){
 $.ajax({  
                                url:'<?php echo Yii::app()->getBaseUrl(true); ?>/management/DeleteSelectedFile',  
                                method:'POST',  
                                data:{id:allVals},  
								success:function(){
                                  window.location.reload();
                                 }  
                            });  
                            }
	function reset_filter(){
		$("#video_duration").val(0); 
		$("#file_size").val(0);
		$("#is_encoded").val(0);
		$("#uploaded_in").val(0); 
		view_filtered_list();
	}
function view_filtered_list(){
	var searchText = $.trim($(".search").val());
	getvideoDetails(searchText);
}
$(document).ready(function () {    
    $( "#filetype" ).change(function() {
        $('.savefile').hide();
        $('#'+$(this).val()+'_div').show();
    });
});
</script>
<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>
