<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<form name="playlist_form" method="post" class="form-horizontal" id="playlist_form" onsubmit="return validate_form();"  enctype="multipart/form-data"  action="<?php echo $this->createUrl('management/editPlaylist'); ?>">
    <div class="row m-t-40">
        <div class="col-md-8">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-info icon left-icon "></em>
                    </div>
                    <h4>Basic Information</h4>
                </div>
                <hr/>
                <div class="form-group">
                    <label class="control-label col-sm-3">Title<span class="red"><b>*</b></span>:</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" id="new_playlist" name="newplaylist" class="form-control input-sm" value="<?php echo @$playlist->playlist_name; ?>" >	
                        </div>
                        <label class="new_playlist_error red"></label>
                    </div>
                </div>
                <?php
                $content_categories = explode(',', $playlist->content_category_value);
                ?>
                <input type="hidden" id="movie_id" name="movie_id" value="" />
				<input type="hidden" id="user_id" name="user_id" value="0" />
				<input type="hidden" id="is_episode" name="is_episode" value="" />
				<input type="hidden" id="add_content" name="add-content" value="0">
				<input type="hidden" id="img_width" name="img_width" value="">
				<input type="hidden" id="img_height" name="img_height" value="">
                <input type="hidden" id="list_id" name="list_id" value="<?php echo @$playlist->id; ?>">
                <div class="form-group">
                    <label for="category" class="col-md-3 control-label">Category<span class="red"><b>*</b></span>: </label>
                    <div class="col-md-9">
                        <div class="fg-line">
                            <select  name="content_category_value[]" id="content_category_value" multiple  class="form-control input-sm checkInput" <?php echo $disable; ?>>
                                <?php
                                foreach ($contentList AS $k => $v) {
                                    if(in_array($k, $content_categories)) {
                                        $selected = "selected='selected'";
                                    } else {
                                        $selected = '';
                                    }
                                    echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
                                }
                                ?>
                            </select>
                            <label class="playlist_category_error red"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary btn-default m-t-10 save_playlist" name="save_playlist" id="save_playlist" value="save">Save playlist</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-cloud-upload icon left-icon "></em>
                    </div>
                    <h4>Upload Poster</h4>
                </div>
                <hr/>
                <div class="border-dotted m-b-40">
                    <div class="text-center">
                        <input class="btn btn-default-with-bg btn-sm" type="button" name="playlist_img" value="Browse" data-width="<?php echo @$poster_size['width']; ?>" data-height="<?php echo @$poster_size['height']; ?>" id="playlist_img" onclick="openImageModal(this);" <?php echo $disable; ?>/>
                        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize" id="reqimgsize"><?php echo $poster_size['width'].' x '.$poster_size['height'].'px'; ?></span></h5>
                    </div>
                    <div class="text-center">
                        <div class="m-b-10 displayInline fixedWidth--Preview">
                            <div class="poster-cls  avatar-view jcrop-thumb">
                                <div id="avatar_preview_div">
                                    <?php if ((strpos($poster, 'No-Image') > -1) || (strpos($poster, 'no-image') > -1)) { ?>
                                        <img data-src="holder.js/<?php echo $poster_size['width']; ?>x<?php echo $poster_size['height']; ?>" alt="<?php echo $poster_size['width']; ?>x<?php echo $poster_size['height']; ?>" style="max-width: 100% !important;"/>
                                    <?php } else { ?>
                                        <img class="img-responsive img-playlist-edit" src="<?php echo @$poster; ?>" style="max-width: 100% !important;">
                                    <?php } ?>
                                </div>
                                <div class="canvas-image" style="max-width: 100%;display:none;">
                                    <canvas id="previewcanvas" style="overflow:hidden;display: none;"></canvas>
                                </div>
                            </div>
                        </div>
                        <?php if(!(strpos($poster, 'No-Image') > -1)): ?>
                        <div class="caption">
                            <a id="remove-poster-text" class="btn remove_btn" href="javascript:void(0);" onclick="return removePlaylistPoster(<?php echo @$playlist->id; ?>);"><em class="icon-close small-icon"></em>&nbsp;&nbsp;Remove Poster</a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="preloader pls-blue" id="playlist_loading" style="display:none;">
                        <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal is-Large-Modal" id="category_img" tabindex="-1" role="dialog" aria-labelledby="category_img">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="section_id1" id="section_id1" value="" />
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" onclick="hide_file()">
                                <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                            </li>
                            <li role="presentation" onclick="hide_gallery()"> 
                                <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                        <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('browse_cat_img')">
                                        <input id="browse_cat_img" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                        <p class="help-block"></p>
                                    </div>
                                    <input type="hidden" class="x1" id="x1" name="fileimage[x1]" />
                                    <input type="hidden" class="y1" id="y1" name="fileimage[y1]" />
                                    <input type="hidden" class="x2" id="x2" name="fileimage[x2]" />
                                    <input type="hidden" class="y2" id="y2" name="fileimage[y2]" />
                                    <input type="hidden" class="w" id="w" name="fileimage[w]"/>
                                    <input type="hidden" class="h" id="h" name="fileimage[h]"/>
                                    <div class="col-xs-12">
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="upload_preview">
                                                <img id="preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                                <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                <input type="hidden" name="g_original_image" id="g_original_image" />
                                <input type="hidden" class="x1" id="x13" name="jcrop_allimage[x13]" />
                                <input type="hidden" class="y1" id="y13" name="jcrop_allimage[y13]" />
                                <input type="hidden" class="x2" id="x23" name="jcrop_allimage[x23]" />
                                <input type="hidden" class="y2" id="y23" name="jcrop_allimage[y23]" />
                                <input type="hidden" class="w" id="w3" name="jcrop_allimage[w3]" />
                                <input type="hidden" class="h" id="h3" name="jcrop_allimage[h3]" />
                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

                                    </div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                        <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                            <div class="preloader pls-blue  ">
                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="gallery_preview">
                                                <img id="glry_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!--div class="row m-b-40">
    <div class="col-xs-12">
        <div class="Block addmore-content">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-people icon left-icon "></em>
                </div>
                <h4>Cast and Crew</h4>
            </div>
            <hr>
            <?php if($this->language_id == 20): ?>
            <div class="row m-b-10">
                <div class="col-xs-12">
                    <a  class="btn btn-primary btn-file btn-sm" role="button" data-toggle="collapse" href="#Cast-Crew-Collapse-Block" aria-expanded="false" aria-controls="Cast-Crew-Collapse-Block">
                        Add New Cast
                    </a>
                </div>
            </div>
            <?php endif; ?>
            <div class="row p-t-20 collapse" id="Cast-Crew-Collapse-Block">
                <div class="col-xs-12">
                    <form  class="row" role="form" name="castncrewform" id="castncrewform" >
                        <div class="col-sm-4">
                            <label >Cast/Crew</label>
                            <div class="input-group form-group">
                                <span class="input-group-addon p-l-0"><i class="icon-user"></i></span>
                                <div id="showcastcrew">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" id="castname" name="cast_name" placeholder="Enter Cast/Crew Name">
                                        <input type="hidden" name="cast_id" value="" id="cast_id"/>
                                    </div>
                                    <div id="searchres" class="searchres"></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label >Type</label>
                            <div class="input-group form-group">
                                <span class="input-group-addon p-l-0"><i class="icon-user"></i></span>
                                <div class="fg-line" id="showtype">
                                    <div class="btn-group btn-block" id="ajaxshowtype">
                                    </div>
                                </div>
                                <div id="showtypeadd" style="display:none">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" class="form-control" id="typenew" name="typenew" placeholder="Enter Type" value="" width="10px">
                                                <input type="hidden" id="typenew_id" name="typenew_id" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-3" style="margin-top:16px">
                                            <em class="icon-check" style="cursor:pointer" title="Save" onclick="saveType()"></em>&nbsp;&nbsp;&nbsp;<em class="icon-close" style="cursor:pointer" title="Cancel" onclick="removeshowType();"></em>
                                        </div>
                                    </div>

                                </div>                                            
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>&nbsp;</label>
                            <div class="fg-line">
                                <button type="button" id="add_btn" class="btn btn-primary btn-sm m-t-5 " onclick="addCast();">Add</button>
                                <button type="button" class="btn btn-default btn-sm m-t-5 " data-toggle="collapse" onclick="addCastCrewpopup();"  href="#Cast-Crew-Collapse-Block" >Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>        
            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="width">Name</th>
                                <th >Image</th>
                                <th data-hide="phone">Cast Type</th>
                                <th data-hide="phone" class="width">Action</th>
                            </tr>
                        </thead>
                        <tbody id="castcrew_body">
                            <?php
                            if (isset($movie_casts) && !empty($movie_casts)) {
                                foreach ($movie_casts AS $key => $val) {
                                    $img = $this->getPoster($val['celebrity_id'], 'celebrity', 'thumb');
                                    if (getimagesize($img) == false) {
                                        $img = str_replace('/thumb/', '/medium/', $img);
                                    }
                                    ?>
                                    <tr>
                                        <td width="40%"><?php echo $val['name']; ?></td>
                                        <td>
                                            <div class="Box-Container m-b-10"> 
                                                <div class="thumbnail thumbnail-small">
                                                    <img src="<?php echo $img; ?>" alt="<?php echo $val['name']; ?>">
                                                </div>
                                            </div>
                                        </td>
                                        <td> <?php echo ucwords(implode(',', json_decode($val['cast_type']))); ?></td>
                                        <td>  
                                            <?php if ($val['parent_id'] == 0 && $val['language_id'] == $this->language_id) { ?>

                                                <a href="javascript:void(0)" onclick="removeCast(<?php echo $val['celebrity_id']; ?>, this);">
                                                    <em class="icon-trash"></em>&nbsp;&nbsp;
                                                    Remove
                                                </a>

                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="4" id="nocast_crew"> No Cast/Crew Found</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="h-50"></div>
                </div>
            </div>
        </div>
    </div>
</div-->
<style type="text/css">
    .castcrew {
        margin-top: -20px;
    }
</style>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/playlist.js?v=<?= RELEASE;?>"></script>
<script type="text/javascript">
 var celebxhr;
 function checkperma(val)
 {
     if(val != '')
     {
        var newperm = name_to_url(val);
        if (newperm=='')
        {
            $('#plink').html("<input type='text' name='movie[permalink]' id='permalink' class='form-control input-sm checkInput' required placeholder='Please enter english equivalent name' onblur='checkperma(this.value)'>&nbsp;<br />Please provide the english equivalent name which will be used for permalink purpose only.&nbsp;<em class='icon-info'></em>");               
        }         
    }
 }
 function name_to_url(name) {
    name = name.toLowerCase(); // lowercase
    name = name.replace(/^\s+|\s+$/g, ''); // remove leading and trailing whitespaces
    name = name.replace(/\s+/g, ''); // convert (continuous) whitespaces to one -
    name = name.replace(/[^a-z-]/g, ''); // remove everything that is not [a-z] or -
    return name;
}

function saveCastCrew(){ 
    var castname = $('#castnamenew').val();
    var studio_id = <?php echo $studio_id;?>;
    var url = HTTP_ROOT + "/Adminceleb/ajaxcastCrewSave";    
    $.post(url, {'castname': castname, 'studio_id': studio_id}, function (data) {
        if (data.isSuccess) {
            $('#showcastcrew').show();
            $('#showcastcrewnew').hide();
            $('#castname').val('');
            $('#castnamenew').val('');
        }
    }, 'json');    
}
function confirmDelete(id,msg) {
    swal({
        title: "Delete Type?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        
        var studio_id = <?php echo $studio_id;?>;
        var url = HTTP_ROOT + "/Adminceleb/ajaxcastCrewDelete";        
        $.post(url, {'id': id, 'studio_id': studio_id}, function (data) {
            if (data.isSuccess) {
               ajaxshowType();
            }
        }, 'json'); 
    });
}
function showType(id,name){
    $('#showtype').hide();
    $('#showtypeadd').show();
    $('#typenew').val(name);
    $('#typenew_id').val(id);
}
function removeshowType(){
    $('#showtype').show();
    $('#showtypeadd').hide();
}
function saveType(){ 
    var typename = $('#typenew').val();
    var typename_id = $('#typenew_id').val();
    var studio_id = <?php echo $studio_id;?>;
    var url = HTTP_ROOT + "/Adminceleb/ajaxtypeSave";    
    $.post(url, {'typename': typename,'typename_id':typename_id, 'studio_id': studio_id}, function (data) {
        if (data.isSuccess) {
            $('#showtype').show();
            $('#showtypeadd').hide();
            $('#typenew').val('');
            ajaxshowType();
        }else{
            ajaxshowType();
        }
    }, 'json');    
}
$(document).ready(function(){
    ajaxshowType();
    $("#castname").autocomplete({source: function (e, t) {
        var castname = $("#castname").val().replace(/^'/g, '');
        castname = castname.replace(/^"/g, '');
        if(celebxhr && celebxhr.readyState != 4){
            celebxhr.abort();
        }
         celebxhr = $.ajax({url: HTTP_ROOT + "/admin/celebAutocomp?",data:{"term":castname}, dataType: "json", success: function (e) {
            t($.map(e.celeb, function (e) {
                return{label: e.celeb_name, value: e.celeb_id}
            }))
        }});
    }, select: function (e, t) {
        e.preventDefault(), $("#castname").val(t.item.label), $("#cast_id").val(t.item.value)
    }, focus: function (e, t) {
        $("#cast_name").val(t.item.label), $("#cast_id").val(t.item.value), e.preventDefault()
    }}), $("#dprogress_bar").draggable({appendTo: "body", start: function (e, t) {
        isDraggingMedia = !0
    }, stop: function (e, t) {
        isDraggingMedia = !1
    }});
});
function ajaxshowType(){
    var url = HTTP_ROOT + "/Adminceleb/ajaxshowtype";
    $.post(url,function(postresult){    
    $("#ajaxshowtype").html(postresult);    
    });
}
function addCast(){
    if(celebxhr && celebxhr.readyState != 4){
       celebxhr.abort();
    }
    if($("#castname").val()!='' && $("input:radio[name='cast_type']").is(":checked")){
        $('#add_btn').html('Please wait');
        $('#add_btn').attr('disabled','disabled');
    }
    var e=$("#cast_id").val(),t=$("#castname").val(),a=$("#castchar").val(),o=$("input[name='cast_type']:checked").val(),i=$("#list_id").val(),r=HTTP_ROOT+"/admin/addCastCrew",n="";
    $.post(r,{castid:e,castname:t,castchar:a,casttype:o,movie_id:i,content_type:1},function(a){
        if(a.success){
            $('#add_btn').html('Add');
            $('#add_btn').removeAttr('disabled');
            n=a.img;
            m=a.castid;
            var i="<tr><td>"+t+'</td><td><div class="Box-Container m-b-10"><div class="thumbnail thumbnail-small"><img src="'+n+'" alt="'+t+'"></div></div></td><td>'+o+'</td><td><h5><a href="javascript:void(0)" onclick="removeCast('+m+', this);"><em class="icon-trash"></em>&nbsp;&nbsp;Remove</a></h5></td></tr>';
            $("#castcrew_body").append(i),$("#cast_id").val(""),$("#castname").val(""),$("#castchar").val(""),$("#add_cast_crew").toggle("slow"),$("#nocast_crew").hide()
        }else
            a.error&&swal(a.msg)},"json")
}
function removeCast(cast_id, obj) {
    swal({
        title: "Remove cast?",
        text: "Are you sure you want to remove this cast from the movie?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        var url = HTTP_ROOT + "/admin/removeCastCrew";
        var list_id = $('#list_id').val();
        $.post(url, {'castid': cast_id, 'movie_id': list_id, 'content_type': 1}, function (res) {
            $(obj).parents('tr').remove();
            console.log(obj);
        });
    });

}
</script>
<script type="text/javascript" src="/themes/admin/js/dropdown-enhancement.js"></script>
<link href="/themes/admin/css/dropdown-enhancement.css" rel="stylesheet" type="text/css" />
