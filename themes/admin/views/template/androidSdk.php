<?php
    $existing_pkg_id = $packageDetails[package][0]['id'];
    $packages_db_id =  $packagedb->id;  
    //Check isCustomer
    $subscribed = $studio->is_subscribed;
    $status = $studio->status;   
    $is_deleted = $studio->is_deleted;
    $is_default = $studio->is_default;
    
    if(isset($existing_pkg_id) && isset($packages_db_id) && ($existing_pkg_id != $packages_db_id) && ($subscribed ==1) && ($status == 1) && ($is_deleted ==0) && (is_default ==0)) {
?>
<div class="form-group">
    <div class="sdk-text" style="margin-top:20px;">
        <h4>Build immersive native apps for android that take full advantage of our powerful, flexible platform. </h4>
    </div>
    
    <form id="add_package" method="POST" id="data-from"  class="form-horizontal">
        <div class="bg-primary text-white col-sm-8" style="padding:10px;margin-top:35px;">
              <div class="col-sm-8">     
                <div class="">
                  <label class="control-label col-sm-4" for="">Package Name</label>    
                  <div class="col-sm-8">  
                     <input type="text" class="form-control" id="packagename" name="packagename" value="<?php echo $package; ?>" style="width:100%;" required>
                  </div>
                </div>
              </div>
            <div class="col-sm-4">
                <button type="submit" id="btnSubmit" class="btn btn-default btn-block" onclick="setPackages()"> GENERATE </button>
            </div>
            <div class="clearfix"></div>
            <div class="sdk-text" style="margin-top:15px;margin-left:31px;"> <label> Note: <i class="text-white"> The package name should be same as the app package name </i></label></div>
            <div class="btn_sdk col-sm-12" style="text-align:center;margin-top:35px;margin-bottom:55px;">
                <button type="button" id="btnDownload" class="btn btn-default"> DOWNLOAD SETUP </button>
            </div>
        </div>
    </form>
</div>
    <?php } else {   ?>     
        <div class="alert alert-danger" style="margin-top: 30px;">
          <strong> Please upgrade your subscription to Muvi Standard or higher to access Android SDK. </strong>
        </div>
        
    <?php } ?>
<script type="text/javascript">
    $(document).ready(function (){
        $('#btnDownload').prop('disabled', true);
        
        var pack = $('#packagename').val();
        if(pack != ''){
            $('#packagename').prop('readonly', true);
            $('#btnSubmit').prop('disabled', true);
            $('#btnDownload').removeAttr("disabled");
        }
        
        setTimeout(function() {
        $(".alert").alert('close');
        }, 20000);   
        
        $("#btnDownload").click(function(e) {
           e.preventDefault();

           var a = document.createElement('a');
            a.href = "<?php echo Yii::app()->baseUrl; ?>/docs/AndroidSDKInstalationProcess.pdf";
            a.download = "AndroidSDKInstalationProcess.pdf";
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
       });         

    });

    function setPackages(){
        var package = $('#packagename').val();
        //alert(package);
        //var data_from = $('#data-from').serialize();
        
        var url = "<?php echo Yii::app()->baseUrl; ?>/template/androidSdk";
        $.post(url,{'package': package}, function (res) {
            
        }, 'json');
    }


</script>


