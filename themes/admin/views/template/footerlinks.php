<?php
$studio = $this->studio;

$app_link='';
$play_link='';
foreach($app_config as $key=>$val)
{
    if($val['config_key']=='appstore_link' && $val['config_value']!='')
    {
      $app_link=$val['config_value']; 
    }
     if($val['config_key']=='playstore_link' && $val['config_value']!='')
    {
      $play_link=$val['config_value']; 
    }
}
?>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="javascript:void(0)">
            <button class="btn btn-primary m-t-10" onclick="openPagepopup();" type="button">
                Add New Page
            </button>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <div class="Block">

            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-docs icon left-icon "></em>
                </div>
                <h4>Static Pages</h4>                      
            </div>
            <hr>
            <div class="Block-Body">
                <table class="table tbl_repeat">
                    <thead>
                        <tr>
                            <th>S/L#</th>
                            <th>Page Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($pages as $page) {
                            ?> 
                            <tr class="" id="<?php echo $page['id']; ?>">
                                <td><?php echo $i++ ?></td>
                                <td><?php echo Yii::app()->common->htmlchars_encode_to_html($page['title']); ?></td>

                                <td>
                                    <h5><a href="<?php echo Yii::app()->getBaseUrl(true) ?>/template/editpage/page/<?php echo $page['permalink'] ?>"><em class="icon-pencil"></em> &nbsp;&nbsp;Edit</a></h5>
                                    <?php
                                    if ($language_id == $page['language_id']) {
                                            ?>
                                            <h5><a href="<?php echo 'http://' . $studio->domain ?>/page/<?php echo $page['permalink'] ?>" target="_blank"><em class="icon-eye"></em>&nbsp;&nbsp; View</a></h5>
                                            <?php
                                            if ($page['permalink'] != 'terms-privacy-policy') {
                                                if (($language_id == $page['language_id']) && ($page['parent_id'] == 0)) {
                                                    ?>
                                                    <h5><a href="#" onclick="openDeletePagepopup('<?php echo $page['permalink'] ?>');return false;"><em class="icon-trash"></em>&nbsp;&nbsp; Remove</a></h5>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                </td>
                            </tr>        
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="Block">
            <form method="post" role="form" name="frmSOCL" class="form-horizontal" id="frmSOCL" action="<?php echo Yii::app()->baseUrl; ?>/template/savesocial">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-link icon left-icon "></em>
                    </div>
                    <h4>Social Media Links</h4>
                </div>
                <hr>
                <!--Social Links-->
                <div class="row m-b-10">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-facebook"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $studio->fb_link; ?>" aria-describedby="inputGroupSuccess1Status" id="fb_link" name="fb_link" class="form-control" placeholder="Enter the link to your Facebook page if you have one" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-twitter"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $studio->tw_link; ?>" aria-describedby="inputGroupSuccess1Status" id="tw_link" name="tw_link" class="form-control" placeholder="Enter the link to your Twitter page if you have one" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-google-plus"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $studio->gp_link; ?>" aria-describedby="inputGroupSuccess1Status" id="gp_link" name="gp_link" class="form-control" placeholder="Enter the link to your Google+ page if you have one" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-instagram"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $studio->ig_link; ?>" aria-describedby="inputGroupSuccess1Status" id="gp_link" name="ig_link" class="form-control" placeholder="Enter the link to your Instagram page if you have one" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-youtube"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $studio->yb_link; ?>" aria-describedby="inputGroupSuccess1Status" id="yb_link" name="yb_link" class="form-control" placeholder="Enter the link to your Youtube page if you have one" />
                                </div>
                            </div>
                        </div>
                        
                          <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-apple"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $app_link; ?>" aria-describedby="inputGroupSuccess1Status" id="apstr_link" name="apstr_link" class="form-control" placeholder="Enter the link to App store" />
                                </div>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-android"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input type="text" value="<?php echo $play_link; ?>" aria-describedby="inputGroupSuccess1Status" id="gglpl_link" name="gglpl_link" class="form-control" placeholder="Enter the link to Google Play" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-b-10">
                            <div class="col-sm-12">
                                <h5>Copyrights Statement:</h5>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label"><i class="fa fa-copyright"></i></label>
                            <div class="col-sm-11">
                                <div class="fg-line">
                                    <input value="<?php echo html_entity_decode($studio->copyright); ?>" type="text" id="copyright" name="copyright" class="form-control" placeholder="Enter your copyright text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11">
                                <button type="submit" class="btn btn-primary btn-sm">Save</button>
                            </div>
                        </div>  
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- CMS page Form start -->
<div id="PagePopup" class="modal fade modal_new" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-x-lg" style="position: relative;">
        <div class="loader" id="frm_loader"></div>
        <form class="form-horizontal" action="<?php echo $this->createUrl('template/addpage'); ?>" method="post" enctype="multipart/form-data" id="add_page">
            <input type="hidden" name="page_id" id="page_id" value="" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Enter Page Content</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="page_name" class="control-label col-sm-2">Page Name:</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input required type="text" id="page_name" class="form-control input-sm" name="page_name" placeholder="Enter a Page Name" />
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="minimal" value="external" name="ltype" id="ltype" /><i class="input-helper"></i> External Link
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="internal">
                        <label for="page_name" class="control-label col-sm-2">Content:</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <textarea id="page_content" class="form-control input-sm" name="page_content" aria-hidden="true"></textarea>
                            </div>
                        </div>
                    </div>  
                    <div class="form-group" id="external">
                        <label for="page_name" class="control-label col-sm-2">Page Link:</label>
                        <div class="col-sm-10">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" name="external_url" id="external_url" placeholder="Enter external page link" />
                            </div>
                        </div>
                    </div>                       

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save Page</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>	
    </div>
</div>


<!--Delete Page-->
<div id="PageDeletePopup" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog" style="">
        <form action="<?php echo $this->createUrl('template/deletepage'); ?>" method="post" enctype="multipart/form-data" id="delete_footerlink">
            <input type="hidden" id="permalink" name="permalink" value="" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="page_title">Delete Footerlinks?</h4>
                </div>
                <div class="modal-body" id="popup_page_content">              
                    Do you really want to delete this page?
                </div>              

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>	
    </div>
</div>



<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>

<!-- Add New CMS Page -->
<script type="text/javascript">
$(document).ready(function(){
    tinymce.init({
    selector: "#page_content",
    formats: {
        bold: {inline: 'b'},  
    },
    valid_elements : '*[*]',
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : false,
     menubar: false,
     height: 200,
     plugins: [
     'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
     'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
     'save table contextmenu directionality emoticons template paste textcolor'
     ],
    setup: function (editor) {
    editor.addButton('newmedia', {
     text: 'Add Image',
     title: 'Add image',
     icon: 'image',
     onclick: function() {
        $("#MediaModal").modal("show");
        $('#glry_preview').removeAttr('src width height');
        $('#imgtolib').removeAttr('src width height');
        $('#choose_img_name').val("");
        $("#InsertPhoto").text("Insert Image");
        $("#InsertPhoto").removeAttr("disabled");
        $("#cancelPhoto").removeAttr("disabled");
        $('.overlay').removeAttr('style');
        $("#browsefiledetails").text("No file selected");
        $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true)?>/template/tinyGallery");
    } });
     },
    content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | media code",
    });
});
function openDeletePagepopup(page) {
    swal({
        title: "Delete Footerlinks ?",
        text: "Do you really want to delete this page ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function() {
        $('#permalink').val(page);
        $('#delete_footerlink').submit();
    });

    //$("#PageDeletePopup").modal('show');
}

function openPagepopup() {
    $("#PagePopup").modal('show');
    return false;
}

function openEditPagepopup(page) {
    $('#frm_loader').show();
    $.ajax({
        type: "POST",
        url: '<?php echo $this->createUrl('template/getpagedetails'); ?>',
        data: {'page': page},
        dataType: 'json'
    }).done(function(data) {
        $('#page_id').val(page);
        $('#page_name').val(data.title);
        $('#page_name').attr('readonly');
        $('#page_content').val(data.content);
        $('#frm_loader').hide();
    });

    $("#PagePopup").modal('show');
    return false;
}

function openViewPagepopup(page) {
    $('#view_loader').show();
    $.ajax({
        type: "POST",
        url: '<?php echo $this->createUrl('template/getpagedetails'); ?>',
        data: {'page': page},
        dataType: 'json'
    }).done(function(data) {
        $('#page_title').html(data.title);
        $('#popup_page_content').html(data.content);
        $('#view_loader').hide();

    });

    return false;
}
</script>   
<script type="text/javascript">
    $('#frm_loader').hide();
    $('#view_loader').hide();

    $(function() {
        var validator = $("#add_page").submit(function() {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            ignore: "",
            rules: {
                page_content: {
                    required: function(element) {
                        if ($('#ltype').is(":checked"))
                            return false;
                        else
                            return true;
                    }
                },
                external_url: {
                    required: function(element) {
                        if ($('#ltype').is(":checked"))
                            return true;
                        else
                            return false;
                    },
                    url: function(element) {
                        if ($('#ltype').is(":checked"))
                            return true;
                        else
                            return false;
                    }
                },
            },
            messages: {
                page_content: {
                    required: 'Please enter page content'
                },
                external_url: {
                    required: 'Please enter external page Link'
                },
            },
            errorPlacement: function(label, element) {
                // position error label after generated textarea 
                label.addClass('red');
                label.insertAfter(element.parent());

            },
        });
        validator.focusInvalid = function() {

            // put focus on tinymce on submit validation
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    console.log("Called" + toFocus.is("textarea"));
                    if (toFocus.is("textarea")) {
                        console.log("This is field");
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
    });


    $(document).ready(function() {
        $('#external').hide();
        $('#ltype').change(function() {
            if ($(this).is(":checked"))
            {
                $('#internal').hide();
                $('#external').show();
            } else {
                $('#external').hide();
                $('#internal').show();
            }
        });
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
    });
</script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.tablednd_0_5.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#sortable").sortable();
        $("#sortable").disableSelection();

        $(".tbl_repeat tbody").tableDnD({
            onDrop: function(table, row) {
                var key = 1;
                $('table tbody tr').each(function() {
                    $(this).children('td:first-child').html(key++)
                });
                var orders = $.tableDnD.serialize();
                $.post('<?php echo $this->createUrl('template/reorderfooterlinks'); ?>', {'orders': orders}, function(res) {
                    console.log(res);
                });
            }
        });
    });

</script>        
<style>
    #bannercontent_switch {
        display: none;
    }    
</style>

