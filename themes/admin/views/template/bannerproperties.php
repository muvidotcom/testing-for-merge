<?php if($language_id == 20 || $banner == ''){?>
<div class="row">
    <div class="col-sm-12">
    <a href="javascript:void(0);" class="advance_properties pull-right">Advanced Configuration</a>
    </div>
</div>
<?php } ?>
<div class="banner_details <?php if($banner['advance_content'] != '') {echo 'hide';}else{echo 'show';} ?>">
    <form id="bannerSetting" method="post" name="bannerSetting" onsubmit="return ValidURL()" action="<?php echo Yii::app()->getBaseUrl(true) ?>/template/bannerproperties">

    <input type="hidden" value="<?php echo $banner_id; ?>" name="banner_id">
         <input type="hidden" value="0" name="is_advance">

    <div class="col-sm-12" id='text-properties'>
        <h4 class="m-t-10"> Text On banner</h4>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Heading</label>
                <div class="col-sm-9">
                    <div class="fg-line">
                        <input type="text" class="form-control" id="text-heading" placeholder="Heading" name="text_heading" value="<?php
                        if ($banner['banner_text_heading'] != '') {
                            echo $banner['banner_text_heading'];
                        }
                        ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <div class="fg-line">
                        <textarea type="text" class="form-control" id="text-description" name="text_description" rows="4" cols="80" placeholder="Description"><?php
                            if ($banner['banner_text_description'] != '') {
                                echo $banner['banner_text_description'];
                            }
                            ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 m-t-10">
        <h4 class="m-t-10"> Button On banner</h4>
        <div class="form-horizontal">

            <div class="form-group">
                <label class="col-sm-3 control-label">Button Text</label>
                <div class="col-sm-9">
                    <div class="fg-line">
                        <input type="text" class="form-control" id="button-text" name="button_text" value="<?php
                        if ($banner['banner_button_text'] != '') {
                            echo $banner['banner_button_text'];
                        } else {
                            echo 'Join Now';
                        }
                        ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Button link</label>
                <div class="col-sm-9">
                    <label class="radio radio-inline m-r-20">
                        <input type="radio" class="button_link" value="0" name="button_link" id="button_register" <?php echo ($banner['banner_button_type'] == 0) ? ' checked="checked"' : ''; ?>>
                        <i class="input-helper"></i>
                        Registration
                    </label>
                    <label class="radio radio-inline m-r-20">
                        <input type="radio" class="button_link check" value="1" name="button_link" id="button_custom"  <?php echo ($banner['banner_button_type'] == 1) ? ' checked="checked"' : ''; ?> >
                        <i class="input-helper"></i>
                        Custom
                    </label>
                    <div class="fg-line">
                        <input type="url" id="button_link_url" class="form-control <?php echo ($banner['banner_button_type'] == 1) ? '' : 'hide'; ?>" name="button_link_url"  value="<?php echo $banner['banner_button_link']; ?>" placeholder="Enter the URL like:https://www.google.co.in">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-12 m-t-10">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-3">
                    <div class="urltext">
                        <label>
                            <i class="input-helper"></i> Banner Redirect Url:
                        </label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" id="url_text" name="url_text" placeholder="Enter the URL like:https://www.google.co.in" value="<?php echo $banner['banner_url']; ?>">
                    </div>
                   <div class="url_validation error red" style="display:none"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-sm-9 ">
            <div id="bnr_txt_error" class="error m-b-20"></div>
            <button class="btn btn-primary" id="save-properties" name="save" value="save" type="submit">Save</button>
        </div>
    </div>
         </form>
</div>
   <div id="advanceContent" class="advance_setting  m-t-20 <?php if($banner['advance_content'] == '') {echo 'hide';}else{echo 'show';} ?>">
    <form id="bannerSettingAdvance" method="post" name="bannerSettingAdvance" action="<?php echo Yii::app()->getBaseUrl(true) ?>/template/bannerproperties">
     <input type="hidden" value="<?php echo $banner_id; ?>" name="banner_id">
     <input type="hidden" value="1" name="is_advance">
        <textarea name="text_advance" id="text_advance" rows="10" placeholder="Content of Email" class="form-control input-sm textarea"><?php echo $banner['advance_content']; ?></textarea>
         <div class="form-group">
        <div class="col-sm-9 p-l-0">
            <button class="btn btn-primary save-advance m-t-20" name="save" value="save" type="submit">Save</button>        </div>
    </div>                 
       
    </form>
</div>
   
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.button_link').click(function () {
            if ($('#button_custom').is(":checked")) {
                $('#button_link_url').removeClass('hide');
            } else {
                $('#button_link_url').addClass('hide');
            }
        });
        $("#url_text").keyup(function(){
            $(".url_validation").hide();
            $(".url_validation").html("");
        });
    });
      
tinymce.init({
     selector: "#text_advance",
     formats: {
            bold: {inline: 'b'},  
        },
     valid_elements : '*[*]',
     force_br_newlines : true,
     force_p_newlines : false,
     forced_root_block : false,
     menubar: false,
     height: 200,
     plugins: [
     'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
     'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
     'save table contextmenu directionality emoticons template paste textcolor'
     ],
    content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  code",
    });

$( ".advance_properties" ).on( "click", function() {
  $('.banner_details').toggleClass('hide');
  $('.advance_setting').toggleClass('show');
  $('.advance_setting').toggleClass('hide');
});
 $(function() {
        var validator = $("#bannerSettingAdvance").submit(function() {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
           
        }).validate({
            ignore: "",
            rules: {
                text_advance: {
                    required: function(element) {
                        if( $('#advanceContent').hasClass('show') ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            },
            messages: {
                text_advance: {
                    required: 'Please enter advance properties'
                },
            },
            errorPlacement: function(label, element) {
                // position error label after generated textarea 
                label.addClass('red');
                label.insertAfter(element);

            },
        });
    });
    function ValidURL() {
        var url_text = $("#url_text").val();  
        if($.trim(url_text) !=""){
            var regex = /[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;
            if(!regex .test(url_text)) {
                $(".url_validation").show();
                $(".url_validation").html("Please enter a valid url");
                return false;
            }else{
                return true;
            }
        } 
    }
</script>