<style type="text/css">
    .btn_add_movie{
        border-radius: 4px !important;
        box-shadow: 2px 2px 2px #999 !important;
    }
    #addmovie_popup .modal-footer{
        margin-top: 0px !important;
    }
    #addmovie_popup .modal-body{
        padding-bottom: 0px !important;
    }
    .ui-autocomplete{
        z-index: 999999;
    }
    .disabled-upload{
        cursor:no-drop;
        color: #ccc;
    }

    .upload {
        border-top: 1px solid #CCCCCC;
        margin-top: 10px;
        padding-top: 10px;
        width: 400px;
    }
    .upload .progress {
        border: 1px solid #555555;
        border-radius: 3px;
        margin-top: 8px;
    }
    .upload .progress .bar {
        background: none repeat scroll 0 0 #3EC144;
        height: 10px;
    }
    ul.yiiPager{
        font-size: 14px !important;
    }
    .filter_dropdown{
        height: 30px;
        width: auto;
        border-radius: box;
        font-size: 16px;
        cursor: pointer;
    }
</style>

<div class="box-body table-responsive no-padding">
        <!--<button class="btn bg-olive btn-flat margin btn_add_movie" onclick="addMovie(<?php echo Yii::app()->user->id; ?>);"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Movie</button>-->
    <a href="javascript:void(0);" onclick="addPopularMovie();"><button class="btn bg-olive btn-flat margin btn_add_movie" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Featured Content</button></a>
    <div class="pull-right">
        <?php if (count($contentList) > 1) {
            ?>
            <select class="filter_dropdown" onChange="window.location.href = this.value">                
                <?php
                foreach ($contentList AS $cont) {
                    $selected = '';
                    if ($contentType == $cont->id) {
                        $selected = 'selected="selected"';
                    }
                    echo '<option value="' . $this->createUrl('template/popularContent', array('contentType' => $cont->id)) . '"' . $selected . ' >&nbsp;' . $cont->display_name . '</option>';
                }
                ?>
            </select>		
<?php } ?>
    </div>	
    <table class="table table-hover tbl_repeat">
        <thead>
            <tr>
                <th>S/L#</th>
                <th>Poster</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($data) {
                //echo "<pre>";print_r($data);exit;
                foreach ($data AS $key => $details) {
                    $movie_id = $details['id'];
                    $img_path =  $this->getPoster($movie_id);
                    $plink = $details['permalink'];
                    $ctype = $details['studio_content_type']->muvi_alias;
                    $strems = $details['movie_streams'];
                    $movie_stream_id = $strems[0]->id;
                    ?>
                    <tr id="<?php echo $movie_stream_id; ?>">
                        <td><?php echo $key + 1; ?> </td>
                        <td>
                            <?php if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) {
                                ?>
                                <a href="javascript:void(0);" ><img src="<?php echo $img_path; ?>" alt=""></a></td>
                <?php } else { ?>
                    <a href="http://muvi.com/movies/<?php echo @$plink; ?>" class="" target="_blank" rel="tooltip" data-title="Tooltip"><img src="<?php echo $img_path; ?>" alt=""></a></td>
        <?php } ?>

                <td>
                    <?php
                    if ($ctype == 'tv') {
                        $dtlsurl = $this->createUrl('admin/tvserise', array('movie_id' => $movie_id));
                    } elseif ($ctype == 'event') {
                        $dtlsurl = $this->createUrl('admin/eventDetails/', array('movie_id' => $movie_id));
                    } else {
                        if (isset(yii::app()->user->is_sdk) && yii::app()->user->is_sdk) {
                            $dtlsurl = $this->createUrl('admin/editMovie/', array('movie_id' => $movie_id));
                        } else {
                            $dtlsurl = $this->createUrl('admin/movieDetails/', array('movie_id' => $movie_id));
                        }
                    }
                    ?>
                    <strong><a href="<?php echo $dtlsurl; ?>"><?php echo $details['name']; ?></a></strong><br/>
                </td>
                <td>

                    <a href="<?php echo $this->createUrl('template/removePopularContent', array('movie_id' => $movie_id, 'contentType' => $details['content_type_id'])); ?>" onclick="return confirm('Are you sure you want to remove from Popular Content?')"><span class="glyphicon glyphicon-remove" title="Remove"></span></a>

                </td>
                </tr>
            <?php }
        } else {
            ?>
            <tr>
                <td colspan="5">
                        <p class="text-red">Oops! You don't have any <?php echo $title?> yet!.</p>
                </td>
            </tr>
<?php } ?>
        </tbody>
    </table>
</div>


<!-- Maaflix login form start-->
<form action=" <?php echo MAAFLIX_URL; ?>site/muvilogin" method="post" name="loginForm" id="maaflixloginForm" target='_blank'>
    <input type="hidden" name="email" value="" id="email"/>
    <input type="hidden" name="user_id" value="" id="user_id"/>
    <input type="hidden" name="remember" value="" id="remember"/>
    <input type="hidden" name="token" value="" id="token"/>
    <input type="hidden" name="fb_id" value="" id="fb_id"/>
    <input type="hidden" name="display_name" value="" id="display_name"/>
    <input type="hidden" name="agent" value="" id="agent"/>
    <input type="hidden" name="address1" value="" id="address1"/>
    <input type="hidden" name="city" value="" id="city"/>
    <input type="hidden" name="state" value="" id="state"/>
    <input type="hidden" name="country" value="" id="country"/>
    <input type="hidden" name="zip" value="" id="zip"/>
    <input type="hidden" name="phone" value="" id="phone"/>
    <input type="hidden" name="user_image" value="" id="user_image"/>
    <input type="hidden" name="is_studio_admin" value="" id="is_studio_admin"/>
    <input type="hidden" name="studio_user_id" value="" id="studio_user_id"/>
    <input type="hidden" name="permalink" value="" id="permalink"/>
</form>

<!-- Add Popular Movie Popup Start -->
<div id="addPopularcontnet" class="modal fade">
    <div class="modal-dialog">
<?php if (count($data) < 10) { ?>
            <form action="<?php echo $this->createUrl('template/addPopularMovie'); ?>" method="post" enctype="multipart/form-data" id="addbanner_form" data-toggle="validator">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Popular Content</h4>
                    </div>
                    <div class="modal-body">
                        
                        <div class="form-group">
                            <label for="content_type_id">Content Type</label>
                            <select class="filter_dropdown" name="content_type_id" id="content_type_id">                                
                                <?php
                                foreach ($contentList AS $cont) {
                                    $selected = '';
                                    if ($contentType == $cont->id) {
                                        $selected = 'selected="selected"';
                                    }
                                    echo '<option value="' . $cont->id . '"' . $selected . ' >&nbsp;' . $cont->display_name . '</option>';
                                }
                                ?>
                            </select>                                                        
                        </div>
                        
                        <div class="form-group">
                            <label for="uploadposter">Name</label>
                            <input type="text" id="title" class="form-control" name="title" placeholder="Start Typing movie name..." required>
                            <br/><span id="preview_poster"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <input type="hidden" value="" id="movie_id" name="movie_id"/>
                <input type="hidden" value="<?php echo $contentType; ?>" id="content_type" name="content_type"/>
            </form>	
<?php } else { ?>
            <div class="modal-content">
                <div class="modal-body"><span class="text-red"> Oops! You reached the limit.<br/> So please remove one featured content to add another!</span></div>
            </div>	
<?php } ?>
    </div>
</div>
<!-- Add Popular Movie Popup End -->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.tablednd_0_5.js" type="text/javascript"></script>
<script type="text/javascript">
                $(function () {
                    $(".tbl_repeat tbody").tableDnD({
                        onDrop: function (table, row) {
                            var key = 1;
                            var orders = $.tableDnD.serialize();
                            $('table tbody tr').each(function () {
                                $(this).children('td:first-child').html(key++)
                            });
                            $.post('<?php echo $this->createUrl('template/reorderPopularshow'); ?>', {'orders': orders}, function (res) {
                                //console.log(res);
                            });
                        }
                    });
                });
                var url = '<?php echo Yii::app()->baseUrl; ?>';

                function addPopularMovie() {
                    $('#addPopularcontnet').modal('show');
                    $('#title').autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: url + '/template/autocomplete/movie_name?term=' + $('#title').val(),
                                dataType: "json",
                                data: {"contentType": $('#content_type_id').val()},
                                init: function (data) {
                                },
                                success: function (data) {
                                    if (data.length == 0) {
                                        $('#movie_id').val('');
                                        $('#preview_poster').html('');
                                    } else {
                                        response($.map(data.movies, function (item) {
                                            return {
                                                label: item.movie_name,
                                                value: item.movie_id
                                            }
                                        }));
                                    }
                                }
                            });
                        },
                        select: function (event, ui) {
                            event.preventDefault();
                            $("#title").val(ui.item.label);
                            $.post(url + "/admin/getPoster", {'movie_id': ui.item.value}, function (res) {
                                if (res.toLowerCase().indexOf("no-image.png") >= 0) {
                                    $('#preview_poster').html("<img src='" + res + "' />")
                                } else {
                                    $('#preview_poster').html("<img src='" + res + "' /><br/><h6>If the above poster is correct, please click 'Save' to add this content to your popular Movie</h6>")
                                }

                            });
                        },
                        focus: function (event, ui) {
                            $("#title").val(ui.item.label);
                            $("#movie_id").val(ui.item.value);
                            event.preventDefault(); // Prevent the default focus behavior.
                        }
                    });
                }
// Login to the maaFlix admin when we click on the link
                function logintoMovie(permalink) {
                    document.getElementById("maaflixloginForm").reset();

                    var hid_pw = '<?php echo $_SERVER['HTTP_HOST'] == 'localhost' ? base64_encode('maatv') : base64_encode('maamuvi2014'); ?>';
                    $.ajax({
                        url: '<?php echo LOGINURL; ?>' + "/authenticate_user",
                        data: 'username=<?php echo Yii::app()->user->email; ?>&password=' + hid_pw + "&oauthApp=maatv",
                        type: 'POST',
                        dataType: "jsonp",
                        jsonpCallback: 'jsonpcallback',
                        beforeSend: function () {
                            $('#permalink').val(permalink);
                        },
                        complete: function () {

                        }
                    });
                }

                function jsonpcallback(data) {
                    console.log(data);
                    var objs = jQuery.parseJSON(data);
                    var items = objs.items;
                    for (i = 0; i < items.length; i++)
                    {
                        var item = items[i];
                        var result = item.result;
                        if (result === 'incorrect login')
                        {
                            return true;
                        }
                        else
                        {
                            var logged_user_id = '', logged_email = '', logged_fb_id = '', logged_display_name = '';
                            var address1 = '', city = '', state = '', country = '', zip = '', zip = '', phone = '';
                            var user_image = '';
                            var is_studio_admin = '';
                            var studio_user_id = '';
                            var token = '';
                            var remember = $('#remember_me').prop('checked');
                            var agent = $('#oauthApp').val();
                            for (j = 0; j < result.length; j++)
                            {
                                if (j === 0)
                                {
                                    var user_data = result[j].user_info;
                                    logged_user_id = user_data.id;
                                    logged_email = user_data.email;
                                    logged_fb_id = user_data.facebook_id;
                                    token = user_data.token;
                                    is_studio_admin = user_data.is_studio_admin;
                                    studio_user_id = user_data.studio_user_id;
                                    var user_profile = result[j].user_profile;
                                    logged_display_name = user_profile.display_name;

                                    var user_address = result[j].user_address;
                                    if (user_address != null)
                                    {
                                        address1 = user_address.address1;
                                        city = user_address.city;
                                        state = user_address.state;
                                        country = user_address.country;
                                        zip = user_address.zip;
                                        phone = user_address.phone;
                                    }
                                    user_image = result[j].user_image;
                                }
                            }
                            if (logged_user_id > 0)
                            {
                                $('#user_id').val(logged_user_id);
                                $('#email').val(logged_email);
                                $('#remember').val(remember);
                                $('#token').val(token);
                                $('#fb_id').val(logged_fb_id);
                                $('#display_name').val(logged_display_name);
                                $('#agent').val(agent);
                                $('#address1').val(address1);
                                $('#city').val(city);
                                $('#state').val(state);
                                $('#country').val(country);
                                $('#zip').val(zip);
                                $('#phone').val(phone);
                                $('#user_image').val(user_image);
                                $('#studio_user_id').val(studio_user_id);
                                $('#is_studio_admin').val(is_studio_admin);
                                $('#maaflixloginForm').submit();
                            }
                        }
                    }
                }

</script>