<style>
    #banner_sortable li{
        display: inline;
    }
    .img-thumbnail {
        height: 107px !important;
        width: 107px !important;
        margin-bottom: 3px;
    }
    .upload-Image {
        height: 107px;
        width: 107px;
        background-color: #fff;
        border-color: #fff;
        display: inline-flex;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
    }      
    .upload-Image em{
        font-size: 2.5em;
        top: 50%;
        position: absolute;
        margin-top: -17px;
        left: 47%;
        margin-left: -17px;
        color:#2cb7f6;
    }
    .upload-Image:hover{ 
        background-color: #edf1f2;
    }
    .upload-Image:hover em{
        color: #0aa1e5;
    }
    .btn.upload-Image.relative {
        margin-top: -3px;
         float: left;
    }
    .play-btn-big {
    background: rgba(0, 0, 0, 0) url("../images/play-button.png") repeat scroll 0 0;
    bottom: 0;
    height: 64px;
    left: 0;
    margin: auto;
    position: absolute;
    right: 0;
    top: 0;
    width: 64px;
}

ul.sortable {width: 100%; float: left; margin: 20px 0; list-style: none; position: relative !important;}
ul.sortable li {height: 107px; float: left;  border: 2px solid #fff; cursor: move;}
ul.sortable li.ui-sortable-helper {border-color: #3498db;}


ul.sortable li.placeholder {width: 107px; height: 107px; float: left; background: #eee; border: 2px dashed #bbb; display: block; opacity: 0.6;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
}    
</style>

<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);

$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$theme = $studio->parent_theme;
?>
 <div id="crop-avatar">
<form id="bannerText" method="post" name="bannerText" action="<?php echo Yii::app()->getBaseUrl(true) ?>/template/saveBannerText">
    <div class="row m-t-40">
        <div class="col-xs-12 BannerCarouselSec">
           
            <?php
            $studio_id = Yii::app()->common->getStudioId();
            $total_sections = count($sections);
            if ($total_sections > 0) {
                ?>    

                <input type="hidden" name="banner_id" id="banner_id" />  
                <input type="hidden" name="section_id"  id="section_id" />
                <input type="hidden" name="update_text" value="0" id="update_text" />                
                <?php
                $p = 0;
                foreach ($sections as $section) {
                    $p++;
                    $section_id = $section->id;

                    $resp = Yii::app()->common->getSectionBannerDimension($section_id);
                    $max_allowed = $section->max_allowed;
                    $max_width = $resp['width'];
                    $max_height = $resp['height'];
                    $placeholder = 'Add text that goes on top of banner here';
                    $btn_txt = 'JOIN NOW';
                    $url_text='';

                    $ban = new StudioBanner;
                    $data = $ban->findAllByAttributes(array('banner_section' => $section_id, 'studio_id' => $studio_id), array('order' => 'id_seq ASC'));
                    $total_bnrs = count($data);
                    if ($total_bnrs > 0) {
                        ?>             

                        <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide relative carousel-fade" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner">
                                <?php
                                $seq = array();
                                if ($total_bnrs > 0) {
                                    $j = 0;
                                    $bannUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                    foreach ($data as $banner) {
                                        $banner_src = $banner->image_name;
                                        $banner_title = $banner->title;
                                        $banner_id = $banner->id;
                                        $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                        $banner_video_image=$banner->video_placeholder_img;
                                        $banner_video=$banner->video_remote_url;
                                        $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                        $join_btn_txt = 'JOIN NOW';
                                        $url_text=$banner->url_text;
                                        $txt_bnr=$banner->banner_text;
                                        $banner_type= $banner->banner_type;
                                      
                                        $seq[] = $banner_id;
                                        ?>
                                        <div class="item <?php echo ($j == 0) ? 'active' : '' ?> <?php  if($theme == "traditional" || $theme == "traditional-byod" ) {echo 'traditional';} ?>">
                                       <img class="<?php echo $j ?>-slide img-responsive" src="<?php if ($banner_type == '0') {echo $banner_video_image; } else {echo $banner_full_url;}?>" alt="<?php echo $banner_title; ?>"  />
                                       <?php if ($banner_type == '0') {?><a href="javascript:void(0);" class="play_video"  data-name="banner_video" id="<?php echo $banner_id; ?>" data-height ="<?php echo  $max_height; ?>" data-width="<?php echo $max_width; ?>"><div class='play-btn-big'></div></a>
                                      <?php } ?>
                                            
                                                <?php if (@IS_LANGUAGE == 1) {
                                                        if ($this->language_id == $banner_text['language_id'] && $banner_text['parent_id'] == 0) { ?>
                                                        <div class="overlay-bottom overlay-white">
                                                            <div class="overlay-Text">
                                                                <div>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>">
                                                                            <em class="icon-plus" title="Add Banner"></em>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if ($total_bnrs > 1) {
                                                                        if ($j > 0) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                            </a>   
                                                                   <?php }
                                                                        if($j == 0 || $j < ($total_bnrs - 1) ) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                            </a>                                                                
                                                                    <?php } 
                                                                    }?>                                                                      
                                                                    <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id;?> )" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash" title="Remove Banner"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php }
                                                    } 
                                                    else { ?>
                                                    <div class="overlay-bottom overlay-white">
                                                        <div class="overlay-Text">
                                                            <div>
                                                               <?php  if($theme == "traditional" || $theme == "traditional-byod") {?>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                     ?>
                                                                    <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view">
                                                                       <em class="icon-plus" title="Add Banner"></em>
                                                                    </a>
                                                                    <?php } ?> 
                                                               <?php }
	                                                        else { ?>
                                                                   <?php } ?> 
                                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id;?> )" class="btn btn-danger icon-with-fixed-width">
                                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } 
                                                if($theme == "traditional" || $theme == "traditional-byod"){
                                                ?>
                                                </div>
                                                <?php } ?>
                                          <div class="<?php if($theme != "traditional-byod"){ echo "absolute";} else {echo "m-t-20 m-b-20";} ?> full-width url-position">                                               
                                                  <div class="row">
                                                  <div class="col-md-8">
                                                    <?php if($theme != "traditional-byod"){?><div class="form-horizontal"><?php }?>
                                                            <div class="form-group">
                                                                <input type="hidden" value="<?php echo $banner_id; ?>" name="banner_id[]">
                                                                <div class="col-md-4">
                                                                    <div class="urltext">
                                                                        <label>
                                                                            <i class="input-helper"></i> Enter the URL for Banner:
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="fg-line">
                                                                        <input type="url" class="form-control input-sm" id="url_text" name="url_text[]" placeholder="Enter the URL like:https://www.google.co.in" value="<?php echo $url_text; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php if($theme == "traditional-byod"){ ?>    
                                                            <div class="form-group m-t-10">
                                                               
                                                                    <div class="col-md-4">
                                                                        <label class="control-label">Text on Banner:</label>
                                                                    </div>
                                                            <div class="col-md-8">
                                                           <div class="fg-line"> 
                                                               <input type="text" class="form-control input-sm" id="bnr_txt" name="bnr_txt[]" placeholder="<?php echo $placeholder ?>" value="<?php echo $txt_bnr; ?>">
                                                           </div>    
                                                                 </div>
                                                                 </div> 
                                                           <?php } ?>
                                                          <?php if($theme != "traditional-byod"){?></div><?php }?>
                                                    </div>
                                                      </div>
                                            </div> 
                                        <?php if($theme != "traditional" || $theme != "traditional-byod"){ ?>    
                                        </div>

                                        <?php
                                        }
                                        $j++;
                                    }
                                } else {
                                      
                                    ?>
                                    <div class="item active">                        
                                        <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                        <div class="overlay-bottom overlay-white">
                                            <div class="overlay-Text">
                                                <div>
                                                    <a href="#" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>"  data-toggle="modal" data-target="#avatar-modal">
                                                        <em class="icon-plus" title="Add Banner"></em>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

            <?php } ?>
                            </div>
                                <?php if ($max_allowed != 1) { ?>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>
                            <?php } ?>
                       
                        <?php } else {  
                            $theme = $this->studio->parent_theme; 
                            ?>
                        <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide m-t-20" data-ride="carousel" data-interval="false">
                            <!-- Indicators -->
                            <div class="carousel-inner" role="listbox" >

                                <div class="item active <?php  if($theme == "traditional" || $theme == "traditional-byod") {echo 'traditional';} ?>"> 
                                    <?php
                                     if($total_sections > 1){
                                         $style="width:". $max_width."px; height:".$max_height."px;";
                                     }else{
                                         $style="width:100%";
                                     }
                                    ?>
                      <?php  if($theme == "traditional-byod") {?>
                                         <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner_traditional.jpg" alt=""  />              
                                        <?php }
                                        else { ?>
                                      <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                     <?php } ?> 
                                      <div class="overlay-bottom <?php  if($theme == "traditional" || $theme == "traditional-byod") { echo "overlay-white";}?>">
                                        <div class="overlay-Text">
                                            <div>
                                                <?php  if($theme == "traditional" || $theme == "traditional-byod") {?>
                                                <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view">
                                                    <em class="icon-plus" title="Add Banner"></em>
                                                </a>
                                                <?php }
	                                    else { ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div> 
                                </div>  
                            </div>
            <?php if ($max_allowed != 1) { ?>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>
                            <?php } ?>
                        </div> 
                        <?php
                        }
                    }

                    $bnr_txt = '';
                    $show_join_btn = 0;
                    $join_btn_txt = 'JOIN NOW';
                    if (!empty($banner_text)) {
                        $bnr_txt = $banner_text['bannercontent'];
                        $show_join_btn = $banner_text['show_join_btn'];
                        $join_btn_txt = $banner_text['join_btn_txt'];
                        
                        if (@IS_LANGUAGE == 1) {
                            if ($this->language_id != $banner_text['language_id']) {
                                $bnr_txt = '';
                                $join_btn_txt = '';
                            }
                        }
                    }
                }
                ?>        
 </div>
        </div>
       
    <?php 
    if(!empty($seq)){
        $seq = implode(',', $seq);
    }else{
        $seq ="";
    }
    ?>
    
    <div class="col-sm-12 m-t-10">
        <div class="displayInline">
        <ul id="banner_sortable" class="p-l-0 sortable">
            <?php  if($theme == "traditional" || $theme == "traditional-byod" ) {?>
            <?php }
	else { ?>
            <?php
            $ban = new StudioBanner;
            $data = $ban->findAllByAttributes(array('banner_section' => $section_id, 'studio_id' => $studio_id), array('order' => 'id_seq ASC'));
            $total_bnrs = count($data);
        
            if ($total_bnrs > 0 ) {
            $j = 0;
            $bannUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
            foreach ($data as $banner) {
                $banner_src = $banner->image_name;
                $banner_title = $banner->title;
                $banner_type= $banner->banner_type;
                 $banner_video_image=$banner->video_placeholder_img;
                 $banner_video=$banner->video_remote_url;
                $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src; 
             ?>
                
            <li id="sortable_<?php echo $banner->id; ?>"> <img class="img-thumbnail" src="<?php if ($banner_type == '0' ) {echo $banner_video_image; } else {echo $banner_full_url;}?>" />  </li>     
                
            <?php }
          }?> 
            <?php } ?> 
        
        </ul> 
     </div>
        <?php  if($theme == "traditional" || $theme == "traditional-byod" ) {?>
            <?php }
            else { ?>
        <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
            ?><div class="displayInline">
            <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn upload-Image relative">
                <em class="icon-plus absolute" title="Add Banner"></em>
            </a></div>
        <?php } ?>
            <?php } ?> 
    </div>

<div class="col-sm-12 m-t-20">
    <input type="hidden" name="seq_ary" id="seq_ary" value="<?php echo $seq; ?>">
    <div class="row m-b-40 ">
        <div class="col-md-8">
            <div class="form-horizontal">
             <?php if( $theme != "traditional-byod"){ ?>  
                <div class="form-group">
                    <label class="col-md-4 control-label">Text on Banner:</label>
                    <div class="col-md-8">
                        <div class="fg-line"> 
                            <input type="text" class="form-control input-sm" id="bnr_txt" name="bnr_txt" placeholder="<?php echo $placeholder ?>" value="<?php echo html_entity_decode($bnr_txt); ?>">
                        </div>

                    </div>
                </div>
                 <?php } ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="rmvdisable" value="1" name="show_join_btn" <?php echo ($show_join_btn > 0) ? ' checked="checked"' : ''; ?> id="show_join_btn" <?php if (@IS_LANGUAGE == 1) { if ($banner_text['parent_id'] > 0) {?>disabled="disabled" <?php }} ?>>
                                <i class="input-helper"></i> Show button to registration page
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" name="join_btn_txt" id="join_btn_txt" value="<?php echo $join_btn_txt; ?>">
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div id="bnr_txt_error" class="error m-b-20"></div>
                        <input type="button" class="btn btn-primary btn-sm" id="savetxt" value="Update" onclick="return removeDisable();" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
   <div id="play_video" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
 </div>
                <div class="modal-body">                   
                    <div class="embed-responsive embed-responsive-16by9 video_banner">
 
                    </div>
                </div>
            </div>
        </div>
    </div>
  
</div>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12/themes/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){
$('.play_video').click(function(){
    var id=$(this).attr('id');
    var height=$(this).attr('data-height');
    var width=$(this).attr('data-width');
    $.ajax({
        type:"POST",
        url :"<?php  echo Yii::app()->getBaseUrl(true) ?>/template/PlayVideobanner",
        data :{'banner_id': id},
        success :function(data){
            $('#play_video').modal('show');
            $('.video_banner').html(data);
           
       }
    });
    
});
$("#play_video").on('hidden.bs.modal', function () {
$('.video_banner').html('');
        });
});
</script>