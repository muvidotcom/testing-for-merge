<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/application.css?v=45">
<div class="row m-t-40 m-b-40">   
    <div class="col-md-12">
        <div class="Block">      
            <div class="holderjs" id="holder1"></div>  
            <div class="row">
                <div class="col-md-4">
                    <h3 class="f-300 m-t-0 m-b-20">Item</h3>
                    <div class="panel-group" id="res-left" role="tablist" aria-multiselectable="true">
                        
                        <!--Start CMS pages for footer-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_page">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_page" aria-expanded="false" aria-controls="collapse_page">Static Pages</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_page" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_page">
                                <div class="panel-body">
                                    <form id="frmpg" name="frmpg" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="1" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />    
                                        <?php
                                        if (count($pages) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($pages as $page) {
                                                    if(@IS_LANGUAGE == 1){
                                                    ?>    
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="pages-<?php echo $page['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $page['id']; ?>" name="pages[]" id="pages-<?php echo $page['id']; ?>" <?php echo $page['language_id']==$this->language_id && $page['parent_id'] == 0?"":"disabled"; ?> />
                                                                <i class="input-helper"></i><?php echo $page['title']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <input type="hidden" name="language_id" value="<?php echo $page['language_id'] ?>" />
                                                    <?php
                                                    }else{ ?>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="pages-<?php echo $page['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $page['id']; ?>" name="pages[]" id="pages-<?php echo $page['id']; ?>" /> 
                                                                <i class="input-helper"></i><?php echo $page['title']; ?>
                                                            </label>
                                                        </div>
                                                    </li>

                                                 <?php  }
                                                }
                                                ?>
                                                    <li class="button-controls">
                                                        <span class="add-to-menu">
                                                            <input type="button" id="add-page" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                            <span class="spinner"></span>
                                                        </span>
                                                    </li>
                                            </ul>
                                        <?php } ?>                                          
                                    </form>
                                </div>
                            </div>
                        </div>                      

                        <!--Start External Links for footer-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_external">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_external" aria-expanded="false" aria-controls="collapse_external">External Links</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_external" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_external">
                                <div class="panel-body">
                                    <form id="frm_EXT" name="frm_EXT" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="2" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <div class="form-group">
                                            <label class="control-label">Link Text</label>
                                            <div class="fg-line"><input type="text" name="menu_title" class="form-control input-sm" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">URL</label>
                                            <div class="fg-line"><input type="text" name="menu_permalink" class="form-control input-sm" /></div>
                                        </div>     
                                        <input type="submit" class="btn btn-primary" name="sbt" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>                    

                    </div>                        

                </div>
                <div class="col-md-offset-4 col-md-4" id="serialization">
                    <h3 class="f-300 m-t-0 m-b-20">Show on Footer</h3>
                    <ol class="serialization vertical p-l-0" role="tablist" id="res-accordion" aria-multiselectable="true">
                        <?php
                        if (count($footermenuitems) > 0) {

                            foreach ($footermenuitems as $menuitem) {
                                $parent_menu_item_id = $menuitem['id'];
                                $item_type = $menuitem['link_type'];
                                $value = $menuitem['value'];
                                $menu_title = $menuitem['title'];
                                $menu_permalink = $menuitem['permalink'];
                                $menu_language = $menuitem['language_id'];
                                $menu_parent_language = $menuitem['language_parent_id'];
                                if($menu_parent_language > 0){
                                    $parent_menu_item_id = $menu_parent_language;
                                }
                                echo $menuform = Yii::app()->general->menuItemForm($this->studio->id, $item_type, $menu_id, $parent_menu_item_id, $value, $menu_title, $menu_permalink, $menu_parent_language,$menu_language,$this->language_id);
                                        $footermenus = MenuItem::model()->findAll(array(
                                    'condition' => 'studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND (language_id=:language_id OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND language_id=:language_id))',
                                    'params' => array(':studio_id' => $this->studio->id, ':menu_id' => $menu_id, ':parent_id' => $parent_menu_item_id,':language_id' =>$this->language_id),
                                            'order' => 'id_seq ASC'
                                        ));
                                 echo '<ol>';
                                if (count($footermenus) > 0)
                                {
                                    foreach ($footermenus as $footermenuitem) {
                                        $menu_item_id = $footermenuitem['id'];
                                        $item_type = $footermenuitem['link_type'];
                                        $value = $footermenuitem['value'];
                                        $menu_title = $footermenuitem['title'];
                                        $menu_permalink = $footermenuitem['permalink'];
                                            $menu_language = $footermenuitem['language_id'];
                                            $menu_parent_language = $footermenuitem['language_parent_id'];
                                        if($menu_parent_language > 0){
                                            $menu_item_id = $menu_parent_language;
                                        }
                                            echo $menuform = Yii::app()->general->menuItemForm($this->studio->id, $item_type, $menu_id, $menu_item_id, $value, $menu_title, $menu_permalink, $menu_parent_language,$menu_language,$this->language_id);
                                         echo '</li>';
                                    }
                                }
                               echo '</ol></li>';
                            }
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery.validator.addMethod("external_url", function(value, element) {
        var reg = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
        return value == '#' || reg.test( value );
    }, "Please enter correct url");  
    $(document).ready(function () {        
        $("#frm_EXT").validate({
            rules: {
                menu_title: {
                    required: true,
                    minlength: 4
                },
                menu_permalink: {
                    required: true,
                    external_url: true
                }
            },
            messages: {
                menu_title: {
                    required: "Please enter menu text",
                    minlength: "Menu text should be more than 3"
                },
                menu_permalink: {
                    required: "Please enter menu link",
                    external_url: "Please enter a valid url"
                }
            },errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            },submitHandler: function (form) {
                var url = "<?php echo Yii::app()->getBaseUrl(); ?>/template/footeritem";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#frm_EXT").serialize(),
                    dataType: "json"
                }).done(function (result) {
                    if (result.action == "success") {
                        window.location.reload();
                    } else {
                        $("#err_message").html(result.message)
                    }
                });
            }
        });
        $(".menu-item-save").click(function () {  
            var frm = $(this).closest("form");
            $(frm).validate({
                rules: {
                    menu_title: {
                        required: true,
                        minlength: 4
                    },
                    menu_permalink: {
                        required: true,
                        external_url: true
                    }
                },
                messages: {
                    menu_title: {
                        required: "Please enter menu text",
                        minlength: "Menu text should be more than 3"
                    },
                    menu_permalink: {
                        required: "Please enter a valid url",
                        external_url: "Please enter a valid url"
                    }
                },errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
                },submitHandler: function (form) {
                    var url = "<?php echo Yii::app()->getBaseUrl(); ?>/template/footeritem";
                    $.ajax({
                        url: url,
                        data: frm.serialize(),
                        dataType: "json",
                        method: "post",
                        success: function (result) {
                            if (result.action == "success") {
                                window.location.reload();
                            } else {
                                $("#err_message").html(result.message)
                            }
                        }
                    });
                }
            });            
                                  
        });
        $(".menu-item-remove").click(function () {
            var item_id = $(this).attr("data-id");
            swal({
                title: "Delete Menu Item",
                text: "Do you really want to delete?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html:true
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/footeritem",
                        data: {"item_id": item_id, "option": "delete_item"},
                        dataType: "json",
                        method: "post",
                        success: function (result) {
                            if (result.action == "success") {
                                window.location.reload();
                            } else {
                                $("#err_message").html(result.message)
                            }
                        }
                    });                 
                }
            });
        });
        $('#add-category').click(function () {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/footeritem",
                data: $('#FRM_MN').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });
        $('#add-page').click(function () {
            console.log($('#frmpg').serialize());
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/footeritem",
                data: $('#frmpg').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });
        $('#add-app').click(function () {
            console.log($('#frm_APPS').serialize());
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/footeritem",
                data: $('#frm_APPS').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });

        $('#ext_submit').click(function () {
            $("#frm_EXT").validate({
                rules: {
                    ex_menu_title: {
                        required: true,
                        minlength: 4
                    },
                    ex_menu_permalink: {
                        required: true,
                        url: true
                    }
                },
                messages: {
                    ex_menu_title: {
                        required: "Please enter menu text",
                        minlength: "Menu text should be more than 3"
                    },
                    ex_menu_permalink: {
                        required: "Please enter menu link",
                        url: "Please enter a valid url"
                    }
                },
            });
        });
    });
</script> 
<style>
    ol{list-style: none;}
    ol#res-accordion{padding-left: 0px;}
    .panel-body ul{list-style: none;}
    .panel{padding:0px;}
    #res-accordion li{margin-bottom: 5px;}
</style>    

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-sortable.js"></script>
<script type="text/javascript">
    $(function() {
    var group = $("ol.serialization").sortable({
        group: 'serialization',
        delay: 500,
        isValidTarget: function ($item, container) {
            var depth = 1, // Start with a depth of one (the element itself)
            maxDepth = 1,
            children = $item.find('ol').first().find('li');

            // Add the amount of parents to the depth
            depth += container.el.parents('ol').length;

            // Increment the depth for each time a child
            while (children.length) {
                depth++;
                children = children.find('ol').first().find('li');
            }

            return depth <= maxDepth;
        },
        onDrop: function($item, container, _super) {
            var data = group.sortable("serialize").get();

            var jsonString = JSON.stringify(data, null, ' ');
            //console.log(jsonString);
           // $('#serialize_output2').text(jsonString);
            _super($item, container);
            var data = {jsonString:jsonString};
             $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/sortmenu/",
                data: data,
                dataType: 'json',
                async: false,
                method: 'post',
                success: function (result) {
                    console.log(result);
                }
            });            
            
        }
    });
});
</script>
