<?php
$block_id = isset($block->id) ? @$block->id : 0;
$block_title = isset($block->title) ? @$block->title : '';
$block_code = isset($block->code) ? @$block->code : '';
$block_content = isset($block->content) ? @$block->content : '';
$language_id = $this->language_id;
$readonly = "";
if(($language_id != 20) && ($block_id)){
    $readonly = "readonly";
}
?>
<div class="row m-t-40">
    <div class="col-md-12">
        <div class="Block">
            <form action="<?php echo $this->createUrl('template/cmsblock'); ?>" method="post" id="add_block" class="form-horizontal">
                <input type="hidden" name="id" id="id" value="<?php echo $block_id ?>" />
                <input type="hidden" name="option" value="add" />
                <div id="block_error"></div>
                <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Widget Name:</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" id="block_name" class="form-control input-sm" name="block_name" placeholder="Enter Widget Name" value="<?php echo $block_title;?>" />
                        </div>
                    </div>
                </div>      
                <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Widget Code(Unique):</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <input type="text" id="block_code" class="form-control input-sm" name="block_code" placeholder="Enter Unique Widget Code" value="<?php echo $block_code; ?>" <?php echo $readonly; ?> />
                        </div>
                    </div>
                </div>                 
                <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Content:</label>
                    <div class="col-sm-10">
                        <div class="fg-line">
                            <textarea id="block_content" class="form-control input-sm" cols="40" rows="10" name="block_content"><?php echo Yii::app()->common->htmlchars_encode_to_html($block_content);?></textarea> 
                        </div>
                    </div>
                </div>

                <div class="form-group m-t-30">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary btn-sm">Save and Continue</button>
                    </div>
                </div>
            </form>	    
        </div>
    </div>

</div>


<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
        selector: "#block_content",
        formats: {
            bold: {inline: 'b'},  
        },
         valid_elements : '*[*]',
         force_br_newlines : true,
         force_p_newlines : false,
         forced_root_block : false,
         menubar: false,
         element_format : 'html',
         extended_valid_elements : 'div[*], style[*]',
         valid_children : "+body[style]", 
         height: 300,
         plugins: [
         'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
         'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
         'save table contextmenu directionality emoticons template paste textcolor'
         ],
        setup: function (editor) {
        editor.addButton('newmedia', {
         text: 'Add Image',
         title: 'Add image',
         icon: 'image',
         onclick: function() {
            $("#MediaModal").modal("show");
            $('#glry_preview').removeAttr('src width height');
            $('#imgtolib').removeAttr('src width height');
            $('#choose_img_name').val("");
            $("#InsertPhoto").text("Insert Image");
            $("#InsertPhoto").removeAttr("disabled");
            $("#cancelPhoto").removeAttr("disabled");
            $('.overlay').removeAttr('style');
            $("#browsefiledetails").text("No file selected");
            $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true)?>/template/tinyGallery");
        } });
         },
        content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | code",
        });
    });
    $('#frm_loader').hide();
    $('#view_loader').hide();

    $(function () {
        jQuery.validator.addMethod("alnum", function (value, element) {
            return this.optional(element) || /^[a-zA-Z0-9_]+$/i.test(value);
        }, "Only alphanumeric, underscore are allowed");         
        var validator = $("#add_block").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            ignore: "",
            rules: {
                block_name:{
                    required: true
                },
                block_code:{
                    required: true,
                    rangelength: [3, 50],
                    alnum:true
                },                
                block_content: {
                    required: true
                },
            },
            messages: {
                block_name: {
                    required: 'Please enter Widget name'
                },
                block_code: {
                    required: 'Please enter Widget code'
                },
                block_content: {
                    required: 'Please enter Widget content'
                },                
            },
            errorPlacement: function (label, element) {
                // position error label after generated textarea 
                label.addClass('red');
                label.insertAfter(element.parent());

            },
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/actcmsblock",
                    data: $('#add_block').serialize(),
                    dataType: "json",
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.error == 1){
                            $('#block_error').addClass('alert alert-danger');
                            $('#block_error').html(data.message);
                            return false;
                        }else{
                            $('#block_error').removeClass('alert alert-danger');
                            window.location = '<?php echo Yii::app()->getBaseUrl(true) ?>/template/cmsblocks';
                        }
                    }
                });                
             }            
        });
        validator.focusInvalid = function () {

            // put focus on tinymce on submit validation
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    console.log("Called" + toFocus.is("textarea"));
                    if (toFocus.is("textarea")) {
                        console.log("This is field");
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
    });
</script>