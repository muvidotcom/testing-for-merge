<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);
?>
<style>
    .fc-axis {
        background-color: #FAFAFA;
    }
    .custom_width .ui-datepicker{
        width:100%;
    }
    #external-events{
        height: 50vh;
        overflow-y: scroll;
        border-right: 1px solid #d7d7d7;
    }
    #external-events .list-inline > li{
        width:33%;
        margin-bottom: 5px;
        overflow: scorll;
    }
    .highlight-date{
        background-color:#c6ffff;
    }
    .list-unstyled > li:hover {
        cursor: pointer !important;
    }
    .fc-time-grid-event{
        font-size: 15px !important;
        text-align:center;
        padding-top: 5px;
    }
    .brwose-width{ width: 100%; overflow: hidden;}
    
    .fc-highlight{
    width:0px !important;
    height:0px !important
    }
    .tooltipevent{
        top:5px !important;
        left:0 !important;
        display:none;
        transition:.5s;
        color:#333;
    }
    .tooltipevent .caret{
        position: absolute;
        top: -4px;
        color: #d7d7d7;
        left: 40px;
        -ms-transform: rotate(180deg);
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }
    .fc-time-grid-event {
        overflow: initial !important;
    }
    .fc-time-grid-event .fc-content{
        display: none !important;
    }
    .fc-time-grid-event:hover .tooltipevent{
        display:block;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/custom.css" type="text/css" >
<!-- fullCalendar 2.2.5 -->
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/fullcalendar.min.css" type="text/css" >
<link rel="stylesheet" media="print" href="<?php echo Yii::app()->baseUrl; ?>/css/fullcalendar.print.css" type="text/css" >




<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.timepicker.css" type="text/css" >


<!-- fullCalendar js  -->
<script src="<?php echo Yii::app()->baseUrl; ?>/js/moment.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/fullcalendar.min.js"></script>

<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.timepicker.min.js"></script>



<div class="row m-t-40">
    <div class="col-sm-3" >
        <div class="row">
            <div class="col-sm-10 p-r-0">
                <input type="text" id="chnl_name" class="form-control input-sm" placeholder="Add Channel..."  onkeypress="return alpha(event)"  />
            </div>
            <div class="col-sm-2" id="add_channel" onclick="add_channel()" style="cursor:pointer">
                <em class="fa fa-plus m-t-10 p-t-10"></em> 
            </div>


        </div>
        <div class="row m-t-10 pull-10">

            <div class="col-sm-12">
                <div class="dropdown">

                    <button type="button" class="btn btn-primary btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="manage_channel" <?php if (count($all_channel) == 0) { ?> disabled <?php } ?>>
                        Manage Channels
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu tv_channel_edit full-width">
                        <ul class="list-unstyled p-l-10 p-r-10">
                            <?php foreach ($all_channel as $key => $val) { ?>
                            <li id="channel_<?php echo $val['id']; ?>" class="row"><a class="col-xs-8" href="#"><?php echo $val['channel_name']; ?></a> <div class="col-xs-4 text-right"><span   onclick="edit_channel(<?php echo $val['id']; ?>, '<?php echo $val['channel_name']; ?>', '<?php echo end(explode('/', $val['channel_logo'])); ?>')"><em class="icon-pencil"></em></span>&nbsp;&nbsp;<span onclick="delete_channel(<?php echo $val['id']; ?>)"><em class="fa fa-trash"></em></span></div></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!--  <button class="btn btn-primary dropdown-toggle" id="manage_channel" type="button" data-toggle="dropdown" aria-haspopup="true"  >Manage Channels
                <span class="caret"></span></button>
                <ul class="list-inline">
                <?php foreach ($all_channel as $key => $val) { ?>
                            <li><a href="#"><?php echo $val['channel_name']; ?></a> &nbsp;&nbsp;<span><em class="fa fa-pencil"></em></span> &nbsp;&nbsp;<span><em class="fa fa-trash"></em></span></li>
                <?php } ?>
                </ul>-->
            </div>


        </div>
        <div class="row m-t-10">
            <div class="col-sm-12">
                <div class="form-group ">
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control" id="channel_list" name="channel_list" onchange="show_eventcalender(this.value)" <?php if (count($all_channel) == 0) { ?> disabled="disabled" <?php } ?> >

                                <?php foreach ($all_channel as $key => $val) { ?>
                                    <option value="<?php echo $val['id']; ?>"><?php echo $val['channel_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-t-10">
            
            <div class="col-sm-12">
                <div class="form-group " >
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control" id="timezone_list" name="timezone_list" onchange="show_calender_withzone(this.value)" <?php if (count($all_channel) == 0) { ?> disabled="disabled" <?php } ?> >
                           <?php 
                            foreach($timezone_list as $key=>$timz)
                            {
                             ?>
                            <option value="<?php echo $key; ?>" <?php if($time_zone==$key) { ?> selected <?php } ?> )><?php echo $timz; ?> </option>;

                            <?php } ?>
        
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-t-10">
            <div class="col-sm-12 custom_width"> 
                <div id="datepicker"></div>
            </div>
        </div> 

        <?php
        $base_url=Yii::app()->getBaseUrl();
        if ($preview_image == '') {

            $src = $base_url . '/images/preview_img_with_content.jpg';
        } else {

            $src = $base_cloud_url . "tv_guide/" . $preview_image;
        }
        ?>
        <div class="row m-t-20">
            <div class=" form-group col-sm-12 Preview-Block"> 
                <div class="thumbnail border1"> 
                    <div class="relative"> 
                        <img src="<?php echo $src; ?>" height="60" width="100" alt="">
                    </div> 
                </div> 
            </div>  
        </div>
        <div class="col-md-12">
            <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                <div class="preloader pls-blue text-center " >
                    <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                    </svg>
                </div>
            </div>
        </div>
        <div class="row m-t-20">
            <div class="col-sm-10">
                <button type="button" class="btn btn-default-with-bg" onclick="browse('upload_preview')" >Browse</button>
                <input id="upload_preview" type="file" name="upload_preview" value="Load Image" onchange="showplaceholder(this.value)"  style="display:none"/> 
                <span id="preview_file">  No file chosen</span>
            </div>      
        </div>

       <!-- <div class="row m-t-20">
            <div class="col-sm-12"> 
                <h5 class="f-700">Content List</h5>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group input-group">
                            <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                            <div class="fg-line">
                                <input id="search_content" class="search form-control" placeholder="Search" onkeyup="search_content()" >
                            </div>
                        </div>
                    </div>
                </div>
               <!--<div id="external-events" class="overflow-auto">
                    <ul class="list-inline text-center">

                        <?php
                      /*  if ($all_list) {
                            $default_img = '/img/No-Image-Vertical-Thumb.png';
                            $default_episode_img = '/img/No-Image-Horizontal.png';
                            $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);

                            foreach ($all_list AS $key => $details) {
                                $movie_id = $details['id'];
                                $stream_id = $details['stream_id'];
                                //View Count 
                                $views = 0;
                                if ($details['is_episode']) {
                                    $views = isset($episodeViewcount[$stream_id]) ? $episodeViewcount[$stream_id] : 0;
                                    if (isset($episodePosters[$stream_id]) && $episodePosters[$stream_id]['poster_file_name'] != '') {
                                        $img_path = $postUrl . '/system/posters/' . $episodePosters[$stream_id]['id'] . '/episode/' . $episodePosters[$stream_id]['poster_file_name'];
                                    } else {
                                        $img_path = $default_episode_img;
                                    }
                                    $contentName = $details['name'] . ' - ' . $details['episode_title'];
                                } else {

                                    $views = isset($viewcount[$movie_id]) ? $viewcount[$movie_id] : 0;
                                    if ($details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                                        $size = 'episode';
                                    } else {
                                        $size = 'thumb';
                                    }

                                    if (isset($posters[$movie_id]) && $posters[$movie_id]['poster_file_name'] != '') {
                                        $img_path = $postUrl . '/system/posters/' . $posters[$movie_id]['id'] . '/' . $size . '/' . $posters[$movie_id]['poster_file_name'];
                                    } else {
                                        $img_path = $default_img;
                                    }
                                    $contentName = $details['name'];
                                }
                                $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
                                if (false === file_get_contents($img_path)) {
                                    if ($details['is_episode'] || $details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                                        $img_path = $default_episode_img;
                                    } else {
                                        $img_path = $default_img;
                                    }
                                }
                                ?>
                                <li> 
                                    <div class="Preview-Block fc-event"> 
                                        <div class="thumbnail m-b-0 thumbnail-list"> 
                                            <div class="relative m-b-5" style="cursor:pointer;">
                                                <!--Place your Image Preview Here--> 

                                            <!--    <img  class="img-responsive" src="<?php echo $img_path; ?>" alt="<?php echo $details['name']; ?>" title="<?php echo $details['name']; ?>" <?php if($details['is_episode']==1) {  ?>style="margin-top:32px;" <?php }  ?>> 
                                                <input type="hidden" class="event_title" id="image_name_<?php echo $details['stream_id']; ?>" name="event_title" value="<?php echo $contentName; ?>" />
                                                <input type="hidden" class="event_id" id="event_id_<?php echo $details['stream_id']; ?>" name="event_id" value="<?php echo $details['stream_id']; ?>" />
                                                <input type="hidden" class="content_types_id" id="content_types<?php echo $details['stream_id']; ?>" name="content_types_id" value="<?php echo $details['content_types_id']; ?>" />
                                               

                                            </div> 
                                        </div>
                                    </div> 
                                </li> 
        <?php
    }
}
 */ ?>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="modal" id="chanelContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
            <div class="modal-dialog" role="document"> 
                <div class="modal-content"> 
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="disable_channel_form()"> 
                            <span aria-hidden="true">&times;</span> 
                        </button> 
                        <h4 class="modal-title"><span id="mycontentLabel">Add Channel</span></h4> 
                    </div> 
                    <div class="modal-body">    
                        <div class="row">
                            <div class="col-md-12">            

                                <form class="form-horizontal" name='add_chnl' id='add_chnl' method="post" action="/template/saveChannel" enctype="multipart/form-data" onsubmit="disable_savebtn()" >

                                    <div class="form-group">
                                        <label for="channel_name" class="col-sm-4 control-label">Channel Name<span class="red">
                                                <b>*</b>
                                            </span></label>

                                        <div class="col-sm-8">
                                            <div class="fg-line">
                                                <input  type="hidden" id="chnel_id" name="chnel_id" value="" />
                                                <input class="form-control input-sm" placeholder="Type here..." type="text" id="channel_name" name="channel_name" autocomplete="off" onkeypress="return alpha(event)" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-offset-4 col-xs-8 m-b-20">
                                        <button type="button" class="btn btn-default-with-bg" onclick="browse('channel_image')" >Select Channel Logo</button>
                                        <input type="file" id="channel_image"  name="channel_image" style="display:none;" onchange="show_channelimage(this.value)"  /> 
                                        <span id="ch_image">  No file chosen</span>
                                    </div>



                                    <div class="form-group m-t-30">
                                        <div class="col-sm-6">
                                            <button type="submit" id="add_channel_btn" class="btn btn-primary btn-sm waves-effect pull-right" >Add</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="disable_channel_form()">Cancel</button>
                                        </div>
                                    </div>

                                </form>
                            </div> </div>
                    </div> 
                </div>
            </div> 
        </div>
    </div>
    <div class="col-sm-9" style="position:relative">
        <div class="text-center m-b-20  loader" style="display: none;"></div>
        <div class="row">
            <div class="col-sm-12" id="parent_container">
               
                <div class="box box-primary padding-20 border-solid" <?php  if (count($all_channel) == 0) { ?> style="min-height:700px;"<?php } ?>>
                    
                     <?php  if (count($all_channel) != 0) { ?>
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->

                        <div class="row" id="calender_container">
                            <div class="col-sm-12">
                                <input type="hidden" id="date_picker_date" value="" />
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="eventContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
                        <div class="modal-dialog" role="document"> 
                            <div class="modal-content"> 
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset_form();" > 
                                        <span aria-hidden="true">&times;</span> 
                                    </button> 
                                    <h4 class="modal-title"><span id="myModalLabel">Add Content</span></h4> 
                                </div> 
                                <div class="modal-body">    


                                    <div class="row">
                                        <div class="col-md-12">            

                                            <form class="form-horizontal" name='add_event' id='add_event'>
                                                <input type="hidden" id="channel_id" name="channel_id" value="<?php echo $first_channel; ?>" />
                                                <input type="hidden" id="movie_id" name="movie_id" />
                                                <input type="hidden" id="content_types_id" name="content_types_id" />
                                                <input type="hidden" id="ls_schedule_id" name="ls_schedule_id" value="" />

                                                <div class="form-group">
                                                    <label for="search_text" class="col-sm-4 control-label">Search Content<span class="red">
                                                            <b>*</b>
                                                        </span></label>

                                                    <div class="col-sm-8">
                                                        <div class="fg-line">
                                                            <input class="form-control input-sm" placeholder="Search Here..." type="text" id="search_text" name="search_text" autocomplete="off" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="startDate" class="col-sm-4 control-label">Start Date<span class="red">
                                                            <b>*</b>
                                                        </span></label>

                                                    <div class="col-sm-8">

                                                        <div class="fg-line">
                                                            <input class="form-control input-sm" type="text" id="startDate" name="start_date">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="startTime" class="col-sm-4 control-label">Start Time<span class="red">
                                                            <b>*</b>
                                                        </span></label>

                                                    <div class="col-sm-8">

                                                        <div class="fg-line">
                                                            <input class="form-control input-sm" type="text" id="startTime" name="start_time" autocomplete="off" onchange="fill_endtime()">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="endTime" class="col-sm-4 control-label">End Time</label>
                                                    <div class="col-sm-8">

                                                        <div class="fg-line">

                                                            <input class="form-control input-sm" disabled type="text" id="endTime" name="end_time" autocomplete="off">

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" id="rweekday">
                                                    <label for="endTime" class="col-sm-4 control-label"></label>
                                                    <div class="col-sm-8">
                                                        <div class="checkbox">
                                                            <label >
                                                                <input type="hidden" name="repeat_weekday" id="repeat_weekday" />
                                                                <input id="weekday" type="checkbox"  name="weekday" onclick="weekday_disable_time()" />
                                                                <i class="input-helper"></i>
                                                                Repeat on weekdays
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="rweekend">
                                                    <label for="endTime" class="col-sm-4 control-label"></label>
                                                    <div class="col-sm-8">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="hidden" name="repeat_weekend" id="repeat_weekend"class="control-label" />
                                                                <input id="weekend" type="checkbox" name="weekend" onclick="weekend_disable_time()" />
                                                                <i class="input-helper"></i>
                                                                Repeat on weekends
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="rhours">
                                                    <label for="endTime" class="col-sm-4 control-label"></label>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <label class="col-sm-4 control-label light-lavel" for="startTime">
                                                                Repeat For
                                                            </label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control input-sm" type="text" id="repeat_hour" name="repeat_hour" value="" onkeypress="return disable_week(event)" />

                                                            </div>
                                                            <label class="col-sm-2 control-label light-lavel" for="startTime">Hrs</label>
                                                        </div>
                                                    </div>
                                                </div>                                                   



                                                <div class="form-group m-t-30">
                                                   
                                                    <div class="col-sm-8 col-sm-offset-4">
                                                          <button id="submit_content" type="button" class="btn btn-primary btn-sm " onclick="return saveevent()">Add</button>
                  
                                                        <button type="button" id="delete_content" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="delete_event()" style="display:none;">Delete</button>
                                                        <button type="button" id="cancel_content" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div> 
                    </div> 
                <?php } else { ?>
                    <div class="row" style="margin-top:250px;"><center>Add channels to get started </center> </div>  
                <?php } ?>   
                </div>
            </div> 
        </div>

        <div class="row m-t-20">
            <div class="col-sm-6">
                <button class="btn btn-primary" data-toggle="modal" data-target="#eventContent" onclick="reset_form();" <?php if (count($all_channel) == 0) { ?> disabled <?php } ?> >Add Content</button>

            </div>
            <div class="col-sm-6 text-right">
                <button class="btn btn-primary" <?php if (count($all_channel) == 0) { ?> disabled <?php } ?>  onclick="window.open('http://<?php echo $this->studio->domain; ?>/tvguide', '_blank');" >Preview Guide</button> 

            </div>
        </div>
        <div id="powerTip" style="display:none;color:red;height:100px;width:100px;">
    
    </div>
        <!-- /.box-body -->
    </div>
    <!-- /. box -->
</div>



<script>
    /*** code for copy paste of event ***/
    $(document).ready(function (e) {
        fill_endtime();
        
       
        $('#chanelContent').on('hidden.bs.modal', function () {
        disable_channel_form();
    });
        
        
    });

    $(document).keydown(function (e) {
        $('input.myTextInput').on('input', function (e) {
            ("#endTime").val(result);
        });
        $("input.myTextInput").on('input', function (e) {
            ("#endTime").val(result);
        });
    });


    /*** code for copy paste of event ***/
    $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        $('#external-events .fc-event').each(function () {
            //var test = $(this).children().find(".event_title").val();
            //alert(test);

            var externalEvents = {
                title: $.trim($(this).children().find(".event_title").val()),
                id: $.trim($(this).children().find(".event_id").val()),
                content_types_id: $.trim($(this).children().find(".content_types_id").val())
            };
            // creating event object and makes event text as its title
            $(this).data('externalEvents', externalEvents); //saving events into DOM
            // make the event draggable using jQuery UI
//            $(this).draggable({
//                zIndex: 999,
//                revert: true, // will cause the event to go back to its
//                revertDuration: 0 //  original position after the drag
//            });
        });

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)

        $('#calendar').fullCalendar({
            header: {
                left: 'prev',
                center: '',
                right: 'agendaDay,agendaWeek,next'
            },
            defaultView: 'agendaWeek',
            allDaySlot: false,
            slotDuration: "00:30:01",
            axisFormat: 'HH:mm', //,'h(:mm)tt',


            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day'
            },
            eventLimit: 3,
            //Random default events


            events: [
<?php foreach ($all_event as $key => $val) { ?>
                    {
                        id: "<?php echo $val['id']; ?>",
                        title: "<?php echo addslashes($val['event_name']); ?>"  ,
                        start: "<?php echo $val['start_time']; ?>",
                        end: "<?php echo $val['end_time']; ?>",
                        backgroundColor: "#2cb7f6", //Primary (light-blue)
                        borderColor: "#17a2ac", //Primary (light-blue)

                        textColor: 'white',
                        color: 'black'

                    },
<?php } ?>

            ],
            eventOverlap: function (stillEvent, movingEvent) {
                return stillEvent.allDay && movingEvent.allDay;
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventDataTransform: function (event) {  //stop dragging of events inside the full calendar

                event.editable = false;
                return event;
            },
            eventRender: function (event, element) {
                
                element.append('<div id="tool'+event.id+'" class="tooltipevent" style="padding:5px;min-height:35px;background:#fff;border:1px solid #d7d7d7;position:relative;z-index:10001;">' +'<div id="name_'+event.id+'">'+event.title+'</div><span class="caret"></span></div>');
                //element.tooltip("test");
                element.attr('href', 'javascript:void(0);');
                element.click(function () {
                     $("#rweekday").hide();
                     $("#rweekend").hide();
                     $("#rhours").hide();
                    // console.log(event);
                    //console.log(event.title);
                    //console.log(moment(event.start).format('MM-DD-YYYY HH:mm:ss'));
                    if (event.title != '' && moment(event.start).format('MM-DD-YYYY HH:mm:ss') != '')
                    {

                        $("#myModalLabel").html("Edit Content");
                        $("#submit_content").html("Save");
                        //$("#delete_content").css("display","inline-block");
                         $("#delete_content").show();
                    }

                    $.ajax({
                        method: "POST",
                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/editEvent',
                        data: {'event_title': event.title, 'event_start': moment(event.start).format('MM-DD-YYYY HH:mm:ss'), 'event_end': moment(event.end).format('MM-DD-YYYY HH:mm:ss')},
                        dataType: "json",
                        success: function (res) {

                            document.getElementById("ls_schedule_id").value = res.event_id;
                            document.getElementById("content_types_id").value = res.stream_type;
                            document.getElementById("movie_id").value = res.movie_id;
                        }
                    });
                    $("#startDate").val(moment(event.start).format('MM-DD-YYYY'));
                    $("#startTime").val(moment(event.start).format('HH:mm:ss'));

                    $("#endTime").val(moment(event.end).format('MM-DD-YYYY HH:mm:ss'));

                    $("#search_text").val(event.title);

                    //$("#eventContent").dialog({ modal: true, title: event.title, width:500});
                    $('#eventContent').modal('show');
                    $("#repeat_weekday").val("");
                    $("#repeat_weekend").val("");
                    //$('#myModalLabel').html(event.title);
                    $('#search_text').autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                                dataType: "json",
                                init: function () {
                                },
                                success: function (data) {
                                    if (data.length == 0) {
                                        // $('#movie_id').val('');
                                        //  $('#preview_poster').html('');
                                    } else {
                                        response($.map(data.movies, function (item) {
                                            return {
                                                label: item.movie_name,
                                                value: item.movie_id,
                                                ctype: item.stream_type,
                                                duration: item.duration
                                            }
                                        }));
                                    }
                                }
                            });
                        },
                        select: function (event, ui) {
                            event.preventDefault();
                            $("#search_text").val(ui.item.label);

                        },
                        focus: function (event, ui) {
                            var lb = ui.item.label;
                            $("#search_text").val(ui.item.label);
                            $("#movie_id").val(ui.item.value);
                            $("#content_types_id").val(ui.item.ctype);
                            if (lb.indexOf("Episode") > -1) {
                                $("#is_episode").val(1);
                            } else {
                                $("#is_episode").val(0);
                            }
                            event.preventDefault(); // Prevent the default focus behavior.
                        }
                    });
                });
            },
            dayClick: function (date, allDay, jsEvent, view) {
                $("#delete_content").hide();
                var date_time = (moment(date).format('MM-DD-YYYY HH:mm:ss'));
                //alert(date_time);
                var date_split = date_time.split(" ")
                var start_day = date_split[0];
                var start_time1 = date_split[1].split(":");
                var start_time = start_time1[0] + ":" + start_time1[1] + ":00";
                //alert('Clicked on: ' + date.format("hh:mm:ss"));
                document.getElementById("add_event").reset();
                $("#startDate").val(start_day);
                $("#startTime").val(start_time);
                //$("#eventContent").dialog({ modal: true, title: , width:500});
                document.getElementById("ls_schedule_id").value = "";
                document.getElementById("content_types_id").value = "";
                document.getElementById("movie_id").value = "";
                $("#rweekday").show();
                $("#rweekend").show();
                $("#rhours").show();
                $('#eventContent').modal('show');
                $('#myModalLabel').html("Add Content");
                $("#repeat_weekday").val("");
                $("#repeat_weekend").val("");
                $("#submit_content").html("Add");
                $('#search_text').autocomplete({
                    source: function (request, response) {

                        $.ajax({
                            url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                            dataType: "json",
                            init: function () {
                            },
                            success: function (data) {
                                if (data.length == 0) {
                                    // $('#movie_id').val('');
                                    //  $('#preview_poster').html('');
                                } else {
                                    response($.map(data.movies, function (item) {

                                        return {
                                            label: item.movie_name,
                                            value: item.movie_id,
                                            ctype: item.stream_type,
                                            duration: item.duration
                                        }


                                    }));
                                }
                            }
                        });
                    },
                    select: function (event, ui) {
                        event.preventDefault();
                        $("#search_text").val(ui.item.label);
                    },
                    focus: function (event, ui) {
                        var lb = ui.item.label;
                        //$("#search_text").val(ui.item.label);
                        $("#movie_id").val(ui.item.value);
                        $("#content_types_id").val(ui.item.ctype);
                        if (lb.indexOf("Episode") > -1) {
                            $("#is_episode").val(1);
                        } else {
                            $("#is_episode").val(0);
                        }
                        event.preventDefault(); // Prevent the default focus behavior.
                    }
                });

            },
//            drop: function (date, allDay, externalEvents) {
//
//                showLoader1();
//                var id = $(this).data('externalEvents').id;
//                var title = $(this).data('externalEvents').title;
//                var start_date = moment(date).format('MM-DD-YYYY HH:mm:ss');
//                var content_type_id = $(this).data('externalEvents').content_types_id;
//                var selected_channel = $("#channel_list").val();
//
//                if (selected_channel != 0) {
//                    $.ajax({
//                        url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/updateDroppedImage',
//                        cache: false,
//                        data: {'id': id, 'title': title, 'start_date': start_date, 'content_type': content_type_id, 'channel_id': selected_channel},
//                        success: function (res) {
//                            showLoader1(1);
//                            if (res == "error") {
//                                swal('Error in adding Event');
//                            } else {
//
//                                $('#calender_container').html();
//                                swal('Event added successfully');
//                                $('#calender_container').html(res);
//                            }
//                        }
//                    });
//
//
//                } else
//                {
//                    swal("Please select a channel");
//                }
//
//                $('#calendar').fullCalendar();
//
//
//            },
//            eventDragStart: function (event, jsEvent, ui, view) {
//
//                if (!copyKey)
//                    return;
//                var eClone = {
//                    title: event.title,
//                    start: event.start,
//                    end: event.end
//                };
//                $('#calendar').fullCalendar('renderEvent', eClone);
//
//            }

        });






        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });

    $(document).ready(function () {
        $('#search_text').autocomplete({
            source: function (request, response) {

                $.ajax({
                    url: '<?php echo Yii::app()->getBaseUrl(true) ?>/template/eventAutocomplete/movie_name?term=' + $('#search_text').val(),
                    dataType: "json",
                    init: function () {
                    },
                    success: function (data) {
                        if (data.length == 0) {
                            // $('#movie_id').val('');
                            //  $('#preview_poster').html('');
                        } else {
                            response($.map(data.movies, function (item) {
                                return {
                                    label: item.movie_name,
                                    value: item.movie_id,
                                    ctype: item.stream_type,
                                    duration: item.duration
                                }
                            }));
                        }

                    }
                });
            },
            select: function (event, ui) {
                event.preventDefault();
                $("#search_text").val(ui.item.label);

            },
            focus: function (event, ui) {
                var lb = ui.item.label;
                $("#search_text").val(ui.item.label);
                $("#movie_id").val(ui.item.value);
                $("#content_types_id").val(ui.item.ctype);
                $("#myModalLabel").trigger("click");
                if (lb.indexOf("Episode") > -1) {
                    $("#is_episode").val(1);
                } else {
                    $("#is_episode").val(0);
                }

                event.preventDefault(); // Prevent the default focus behavior.
            }
        });


        $('#search_text').on('autocompleteselect', function (e, ui) {
            var start_datevalue = $("#startDate").val();
            var start_timevalue = $("#startTime").val();
            var start_value = start_datevalue + ' ' + start_timevalue;

            var movie_id = ui.item.value;

            if (start_value != '' && movie_id != '')
            {
                $("#endTime").val();
                $.ajax({
                    url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/getEndTime",
                    data: {'start_value': start_value, 'movie_id': movie_id},
                    contentType: false,
                    cache: false,
                    success: function (result)
                    {
                        if(result){
                        $("#endTime").val(result);
                          }
                       
                    }
                });
            }
        });


        $("#chnl_name").on("keypress", function (e) {
            if (e.which === 32 && !this.value.length)
                e.preventDefault();
        });
        $("#channel_name").on("keypress", function (e) {
            if (e.which === 32 && !this.value.length)
                e.preventDefault();
        });
        
        $("#calender_container").on("mouseenter",".fc-time-grid-event",function(){
            $(this).css('z-index','10000000');
           }).on("mouseleave",".fc-time-grid-event",function(){
               $( this ).css('z-index','1');
    });
    });
    $("#startDate").datepicker({
        dateFormat: 'mm-dd-yy',
        timeFormat: 'HH:mm:ss'


    });
    $("#startTime").timepicker({
        timeFormat: 'H:i:s',
        dropdown: 'true',
        scrollbar: 'true'

    });


    $("#endTime").datepicker({
        dateFormat: 'mm-dd-yy',
        timeFormat: 'HH:mm:ss'

    });
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {

                var date = $(this).val();

                $('#calendar').fullCalendar('gotoDate', date);
            }
        });
    });

    function add_channel()
    {
        var channel_name = $("#chnl_name").val();
        $("#mycontentLabel").html("Add Channel");
        $("#add_channel_btn").html("Add");
        $("#channel_name").val(channel_name);
        $('#chanelContent').modal('show');
    }


    $('#add_chnl').submit(function () {
        var channel_name = $("#channel_name").val();
        if (channel_name === '') {
            swal("Please enter the channel name");
            return false;
        }
    });


    $('#eventContent').submit(function () {
        var channel_id = $("#channel_id").val();
        var event_name = $("#search_text").val();
        var startDate = $("#startDate").val();
        var startTime = $("#startTime").val();
        if (event_name === '') {
            swal("Please enter event name");
            return false;
        }
        else if (startDate === '')
        {
            swal("Please enter start date");
            return false;
        }
        else if (startTime === '')
        {
            swal("Please enter start time");
            return false;
        }
        else if (channel_id === '')
        {
            swal("PleasesSelect channel");
            return false;
        }
        else
        {
            return true;
        }
    });


    function search_content()
    {
        var key_word = $("#search_content").val();
        var key_word = key_word.trim();
        if (key_word.length >= 3 || key_word.length == 0) {

            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/ajaxSearchContent",
                data: {'search_word': key_word},
                contentType: false,
                cache: false,
                success: function (result)
                {
                    $("#external-events").html();
                    $("#external-events").html(result);
                }
            });
        }
        else
        {
            return;

        }
    }


    function weekend_disable_time()
    {
        if (document.getElementById('weekend').checked)
        {
            $("#repeat_weekend").val(1);
            $("#repeat_hour").val("");
        }
        else
        {
            $("#repeat_weekend").val(""); 
        }
    }

    function weekday_disable_time()
    {
        if (document.getElementById('weekday').checked)
        {
            $("#repeat_weekday").val(1);
            $("#repeat_hour").val("");
        }
        else
        {
             $("#repeat_weekday").val("");
        }
    }

    function disable_week(evt)
    {
        document.getElementById('weekday').checked = false;
        document.getElementById('weekend').checked = false;
        $("#repeat_weekend").val("");
        $("#repeat_weekday").val("");

        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;

    }

    function fill_endtime()
    {
        var start_datevalue = $("#startDate").val();
        var start_timevalue = $("#startTime").val();
        var start_value = start_datevalue + ' ' + start_timevalue;

        var movie_id = $("#movie_id").val();

        if (start_value != '' && movie_id != '')
        {
            $("#endTime").val();
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/getEndTime",
                data: {'start_value': start_value, 'movie_id': movie_id},
                contentType: false,
                cache: false,
                success: function (result)
                {
                  
                    if(result){
                        $("#endTime").val(result);
                          }else
                          {
                              $("#endTime").val("");
                          }
                       
                }
            });
        } else {
            //swal("Please check the start time and the content name");
        }
    }

    function showplaceholder(ele)
    {

        var file_name = $("#upload_preview").val();
        $("#preview_file").html(file_name);
        var sFileExtension = file_name.split('.')[file_name.split('.').length - 1].toLowerCase();

        if (!(sFileExtension === "jpg" || sFileExtension === "jpeg" || sFileExtension === "JPG" || sFileExtension === "JPEG" || sFileExtension === "PNG" || sFileExtension === "png" || sFileExtension === "gif")) {
            swal("Please upload Image files");
            $("#upload_preview").val("");
            $("#preview_file").html("");
        }
        else
        {
            var file_data = $("#upload_preview").prop("files")[0];
            var form_data = new FormData();
            form_data.append("file", file_data);
            //alert(form_data);
            showLoader();
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/uploadPreview",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'POST',
                success: function (res) {
                    if (res)
                    {
                        showLoader(1);
                        window.location.reload();
                    }

                }
            });
        }
    }
    function delete_channel(id)
    {
        swal({
            title: "Delete Channel?",
            text: "Are you sure to delete Channel",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/deleteChannel",
                cache: false,
                type: 'POST',
                data: {'channel_id': id},
                success: function (res) {
                   console.log(res);
                    if ($.trim(res)==='deleted') {
                        $('#channel_'+id).remove();
                        $("#channel_list option[value='"+id+"'").remove();
                        swal({
                            title: "Channel deleted successfully",
                            confirmButtonColor:"#2cb7f6"
                        });                     
                       }
                    else
                    {
                        swal("Channel is already in use");
                    }
                    
                }
            });
        });

    }

    function edit_channel(id, name, image)
    {

        $("#chnel_id").val(id);
        $("#chanelContent").modal('show');
        $("#mycontentLabel").html("Edit Channel");
        $("#add_channel_btn").html("Save");
        $("#channel_name").val(name);
        $("#channel_image").val(image);


    }

    function show_eventcalender(a)
    {

        var selected_text = channel_list.options[channel_list.selectedIndex].innerHTML;

        $("#channel_id").val(a);
        showLoader1();
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/showCalender",
            cache: false,
            type: 'POST',
            data: {'channel_id': a},
            success: function (res) {
                if (res)
                {
                    $(".fc-center").text(selected_text);
                    $('#calender_container').html();
                    showLoader1(1);

                    $('#calender_container').html(res);
                    return false;

                }
                else
                {

                }
            }
        });
    }
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();

        } else {
            $('.loaderDiv').hide();

        }
    }

    function showLoader1(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loader').show();

        } else {
            $('.loader').hide();

        }
    }


    function delete_event(id, event)
    {
     var ls_schedule=document.getElementById("ls_schedule_id").value;
     console.log(ls_schedule);
        swal({
            title: "Delete Event?",
            text: "Are you sure to delete event",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        }, function () {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/deleteEvent",
                cache: false,
                type: 'POST',
                data: {'event_id':ls_schedule},
                success: function (res) {
                    if (res) {

                        $('#calendar').fullCalendar('removeEvents', ls_schedule);
                        swal({
                            title: "Event deleted successfully",
                            confirmButtonColor:"#2cb7f6"
                        });
                    }
                    else
                    {
                        swal("Event already started");
                    }

                }
            });
        });
    }

    function alpha(e) {

        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }


    function reset_form()
    {
        document.getElementById("add_event").reset();
        $("#myModalLabel").text("Add Content");
        $("#submit_content").text("Add");
        $("#rweekday").show();
        $("#rweekend").show();
        $("#rhours").show();
        $("#repeat_weekday").val("");
        $("#repeat_weekend").val("");
    }
    function browse(fileinput) {
        $("#" + fileinput).click();
    }

    function show_channelimage(ele)
    {
        var file_name = $("#channel_image").val();
        $("#ch_image").html(file_name);
    }

    function disable_channel_form()
    {
        $("#ch_image").html("");
        document.getElementById("add_chnl").reset();

    }
    
    function disable_savebtn()
    {
       //document.getElementById('add_channel_btn').disabled = 1;   
       $("#add_channel_btn").attr("disabled","disabled");
    }

    function saveevent()
    {
        $("#submit_content").attr("disabled","disabled");
        var end_time=$("#endTime").val();
        if(end_time!=''){
        var ls_schedule=$("#ls_schedule_id").val();
     
        var formData = new FormData($("#add_event")[0]);

        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/saveEvent",
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                document.getElementById('submit_content').disabled = 0;
                if ($.trim(result) === "Timingerror") {
                swal('Selected date should be greater than current date');
                }
                else if($.trim(result) === "Nochannel")
                {
                  swal('Please select a channel');  
                }
                else if($.trim(result) === "Eventexists")
                {
                 swal('Event already exists');      
                }
                else if($.trim(result) === "NoDetail")
                {
                  swal('Please enter the event details');     
                }
                else if($.trim(result) === "Eventstarted")
                {
                     swal('Event started already');    
                }
                else if($.trim(result) === "DurationError")
                 {
                   swal("Please upload the video again");    
                 }
                else {
                   
                    $("#calender_container").html();
                    $("#cancel_content").trigger("click");
                     if(ls_schedule=='')
                    {
                        swal({
                            title: "Event added successfully",
                            confirmButtonColor:"#2cb7f6"
                        });
                       $("#calender_container").html(result);
                    }
                    else
                    {
                      swal({
                            title: "Event updated successfully",
                            confirmButtonColor:"#2cb7f6"
                        });
                      $("#calender_container").html(result);
                    }
                    
                    
                }
            }
        });
        
        }else
        {
        swal("Please upload the video again");
        }
    }
    
    function show_calender_withzone(a)
    {
        //alert(a);
       var channel_id= $("#channel_id").val();
       showLoader1();
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true); ?>/template/showZone",
            cache: false,
            type: 'POST',
            data: {'zone_name': a,'channel_id':channel_id},
            success: function (res) {
                if (res)
                {
                    
                    $('#calender_container').html();
                    showLoader1(1);

                    $('#calender_container').html(res);
                    return false;

                }
                else
                {

                }
            }
        }); 
        
    }
</script>