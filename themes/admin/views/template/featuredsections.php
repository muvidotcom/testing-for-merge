<style>
    /* css :It's Re defined' */
.ui-widget-content{
    z-index: 1050 !important;
    height: 200px !important;
    overflow-y: scroll !important;
}
.ui-widget-content li:hover{
    background-color: #0081c2;
    background-image: linear-gradient(to bottom, #0088cc, #0077b3);
    background-repeat: repeat-x;
    color:white;
    text-decoration: none;
}
.featured_cont .ui-sortable-handle{
	height: 100%;
}
.ui-menu .ui-menu-item{
    padding: 5px;
}
.drag-cursor {
    list-style: outside none none;
}
.Collapse-Content ul.featured_contents{
    padding-left: 0;
}
ul.featured_contents h5{
    min-height: 35px;
}
ul.featured_contents li{
    float: left;
}
.err_msg{
    color:red;
}
.ui-state-default { background: none; border: 1px dotted #fff;  cursor: move;}
@media (min-width: 1560px) {
   ul.featured_contents li{
        width: 18%;
    } 
}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.showsectionname').click(function () {
            $(this).parent().find('.editsection').removeClass('hide');
            $(this).parent().find('.editsection').addClass('show');
            $(this).addClass('hide');
        });
    });
</script>
<?php 
$totalContent = count($data); 
$studio = $this->studio;
$totalContent = 0;
$home_layout  = Yii::app()->custom->HomePageLayout();
$home_design  = Yii::app()->custom->HomePageLayoutType();
$has_physical = Yii::app()->general->getStoreLink();
if ($home_design == 1) {
?>
<div class="Block">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-docs icon left-icon "></em>
        </div>
        <h4>Home Page Layout</h4>                      
    </div>
    <hr>
        <div class="Block-Body">
            <div class="row m-b-40">
                <form action="<?php echo $this->siteurl; ?>/template/updatehomepagelayout" method="post">
                    <label class="col-sm-2 c-black f-500">Layout Type</label>
                    <div class="form-group col-sm-2">
                        <div class="fg-line">
                            <div class="select">
                                <select name="layout_type" class="form-control">
                                    <option value="0"<?php echo ($home_layout == 0) ? ' selected' : ''; ?>>Featured Sections</option>
                                    <option value="1"<?php echo ($home_layout == 1) ? ' selected' : ''; ?>>All Product Listing</option>                        
                                </select>                        
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary btn-sm m-t-5 waves-effect">Save</button>
                    </div> 
                </form>
            </div>
        </div>
</div>
<?php
}
if ($home_design == 1 && $home_layout == 1) {    
}
else{
?>
<div class="Block">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-docs icon left-icon "></em>
        </div>
        <h4>Featured Sections</h4>                      
    </div>
    <hr>
    <div class="Block-Body">
        <div class="row m-b-40">       
        <div class="col-xs-12">
		 <?php
        if ($theme->max_featured_sections == 0 || $theme->max_featured_sections > $totalContent) {
            ?>
            <button class="btn btn-primary m-t-10 p-l-40" id="nextbtn" onclick="addHomepageSection(); return false;"> Add Featured Section</button>
		<?php } ?>
         </div> 
    </div>
<?php
        if ($data) {
            $totalContent = count($data);
            $x = 1;
            ?>
        <script type="text/javascript">
                $(function () {
                    $('#sort_section').sortable({
                        update: function () {
                            var order = $('#sort_section').sortable('serialize');
                            $.ajax({
                                type: "POST",
                                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/sortfeaturedsection",
                                data: "order=" + $('#sort_section').sortable('serialize'),
                                dataType: "json",
                                cache: false,
                                success: function (data)
                                {
                                    console.log(data);
                                }
                            });
                        }
                    });
                });
            </script>  
<form class="form-horizontal" method="post">
                <input type="hidden" name="sec_id" id="sec_id" />
				<div class="row m-b-40 featured_cont" id="sort_section">
                    <?php
                    $k=0;
                    $content_type = 0;
                    foreach ($data AS $key => $details) {
                        $content_type = $details['content_type'];
                        $section_id = $details['id'];
                        $featureds = Yii::app()->common->getFeaturedAllContents($section_id);
                        ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#collap-<?php echo $section_id; ?>').click(function () {
                                    $('#section_data_<?php echo $section_id; ?>').toggle();
                                });
                            });
                        </script>                    
                            <div class="col-xs-12 ui-state-default" id="sort_<?php echo $section_id; ?>" >
				
                    <div class="Collapse-Block" >
					
                        <div class="Block-Header has-right-icon">
                            <div class="icon-OuterArea--rectangular">
                                <a href="javascript:void(0);" id="collap-<?php echo $section_id; ?>"><em class="icon-arrow-up icon left-icon"></em></a>
                            </div>
                            
                            <?php if (($language_id == $details['language_id']) && ($details['parent_id'] == 0)) { ?>
                                <div class="icon-OuterArea--rectangular right">
                                    <a href="javascript:void(0);" onclick="deleteSection(<?php echo $section_id; ?>)"><em class="icon-trash icon" title="Remove the Section"></em></a>
                                </div>
                            <?php } ?>
                            <?php if ($details['section_type'] == 2) { ?>
                                <div class="icon-OuterArea--rectangular right" style="margin-right:20px;">
                                    <a href="javascript:void(0);" onclick="editSection(<?php echo $section_id; ?>)"><em class="icon-pencil icon" title="Edit the Section"></em></a>
                                </div>
                            <?php } ?>
                            <div class="section_settings form-horizontal">
                                <div class="editsection hide form-group">
                                    <div class="col-sm-4">
                                        <div class="fg-line">
                                            <input type="text" class="sectionname form-control" value="<?php echo $details['title']; ?>" id="sectionname_<?php echo $section_id; ?>" name="sectionname_<?php echo $section_id; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0);"  onclick="updateSection(<?php echo $section_id; ?>);return false;" title="Update"><i class="h3 green icon-check"></i></a>
                                    </div>
                                </div> 
                            </div>
                                        <h4 <?php if ($details['section_type'] != 2) { ?>class="showsectionname" <?php } ?>><?php echo $details['title']; ?> </h4>
                            
                        </div>

                        <div class="Collapse-Content row" id="section_data_<?php echo $section_id; ?>">
                                        <?php if ($details['section_type'] == 2) { ?>
                                            <div class="col-sm-12 m-t-20 m-b-5">Auto Generated Featured Section&nbsp;&nbsp;&nbsp; <span><b>Criteria</b>: <?= $details['section_criteria'] ?></span>&nbsp;&nbsp;&nbsp; <?php if ($details['section_category']) { ?><span><b>Category</b>: <?= $details['section_category'] ?></span>&nbsp;&nbsp;&nbsp; <?php } ?><?php if($details['content_types']){ ?><span><b>Content Types</b>: <?= $details['content_types'] ?></span>&nbsp;&nbsp;&nbsp;<?php } ?><span><b>Content Limit</b>: <?= $details['content_limit'] ?></span></div>
                                        <?php } else { ?>
                                   <!--Add New Content-->
                                    <div class="col-sm-12 m-t-20 m-b-20">
                                            <a href="javascript:void(0);" class="btn btn-primary m-t-10 p-l-40" onclick="addPopularMovie(<?php echo $section_id ?>, <?php echo $content_type; ?>); return false;">
                                            Add Content
                                        </a>
                                    </div>
                                   
                            <ul class="row featured_contents" id="sortables_<?php echo $section_id; ?>" >
							
                                
									 <?php
                                                        foreach ($featureds as $featured) {
                                                            $is_episode = $featured->is_episode;
                                                            if ($is_episode == 1) {
                                                                $stream_id = $featured->movie_id;
                                                                $langcontent = Yii::app()->custom->getTranslatedContent($stream_id,$is_episode,$language_id);
                                                                $movie_stream = new movieStreams;
                                                                $movie_stream = $movie_stream->findByPk($stream_id);
                                                                if(array_key_exists($stream_id, $langcontent['episode'])){
                                                                    $movie_stream->episode_title = $langcontent['episode'][$stream_id]->episode_title;
                                                                }
                                                                $movie_id = $movie_stream->movie_id;

                                                                $film = new Film();
                                                                $film = $film->findByPk($movie_id);
                                                                if(array_key_exists($movie_id, $langcontent['film'])){
                                                                    $film->name = $langcontent['film'][$movie_id]->name;
                                                                }
                                                                $cont_name = $film->name . ' ';

                                                                $cont_name .= ($movie_stream->episode_title != '') ? $movie_stream->episode_title : "SEASON " . $movie_stream->series_number . ", EPISODE " . $movie_stream->episode_number;
                                                                $poster = $this->getPoster($stream_id, 'moviestream', 'episode');
                                                            } else if($is_episode == 2){
                                                                $movie_id = $content_id = $featured->movie_id;
                                                                $poster = PGProduct::getpgImage($content_id, 'standard');
                                                                $content = Yii::app()->custom->getProductDetails($content_id, array('name'));                                                                
                                                                $cont_name = @$content[0]['name'];
															}elseif ($is_episode == 4){
																$movie_id = $content_id = $featured->movie_id;
																$poster = $this->getPoster($movie_id, 'playlist_poster');
																$playlist = new UserPlaylistName();
																$playlistdata = $playlist->findByPk($movie_id);
																$cont_name = $playlistdata->playlist_name . ' ';
															}else {
                                                                $movie_id = $featured->movie_id;
                                                                $langcontent = Yii::app()->custom->getTranslatedContent($movie_id,$is_episode,$language_id);
                                                                $film = new Film();
                                                                $film = $film->findByPk($movie_id);
                                                                if(array_key_exists($movie_id, $langcontent['film'])){
                                                                    $film->name = $langcontent['film'][$movie_id]->name;
                                                                }
                                                                $cont_name = $film->name;
																$ctype = $film->content_types_id;
                                                                if ($ctype == 2)
                                                                    $poster = $this->getPoster($film->id, 'films', 'episode');
                                                                else if($ctype == 4)
                                                                    $poster = $this->getPoster($film->id, 'films', 'original');
                                                                else
                                                                    $poster = $this->getPoster($film->id, 'films', 'standard');
                                                            }
                                                            if (false === file_get_contents($poster)) {
                                                                $poster = "/img/No-Image-Horizontal.png";
                                                            }
                                                            ?>
                                <li class="col-sm-3 drag-cursor" id="sort_<?php echo $section_id; ?>_<?php echo $featured->id ?>" >
                                    <div class="Preview-Block"  >		
                                        <div class="thumbnail m-b-0"  >
                                            <div class="relative m-b-10 drag-cursor">
                                                <img src="<?php echo $poster ?>" alt="">
                                                <div class="overlay-bottom overlay-white text-right">
                                                    <div class="overlay-Text">
                                                        <div>
                                                            <a href="javascript:void(0);"  onclick="deleteFeatured(<?php echo $section_id; ?>,<?php echo $featured->id ?>);" class="btn btn-danger icon-with-fixed-width">
                                                                <em class="icon-trash"></em>
                                                            </a>
                                                        </div>
                                                     </div>
                                                </div>
                                            </div>
                                                        <h5><?php echo (strlen($cont_name) > 50) ? strip_tags(substr($cont_name, 0, 50)). '...' : $cont_name;?></h5>
                                        </div>
                                    </div>
                                </li>
                                       <?php } ?>
                                    </ul>
                                        <?php } ?>
                        </div>
                     </div>
                     </div>
                    <?php 
                    $is_sort = 1;
                    if (intval($is_sort)) { ?>
                        <!--script type="text/javascript">
                                    $('#sortable_<?php echo $section_id; ?>').sortable({
                                        update: function () {
                                            var order = $('#sortable_<?php echo $section_id; ?>').sortable('serialize');
                                            console.log(order);
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/sortfeatured",
                                                data: "order=" + $('#sortable_<?php echo $section_id; ?>').sortable('serialize'),
                                                dataType: "json",
                                                cache: false,
                                                success: function (data)
                                                {
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                            $("#sortable_<?php echo $section_id; ?>").disableSelection();
                        </script--> 
                        <?php }
                        $k++;
                    } ?>                                                
               </div>
            </form>
        <?php 
        } 
        ?>            
    </div>
</div>
       
<div id="addHomepageSection" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo $this->createUrl('template/addHomepageSection'); ?>" class="form-horizontal" method="post" id="add_homepage_section">
                <input type="hidden" name="section_id" id="sectionId" value="0" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Featured Section</h4>
                </div>
                <div class="modal-body">
                        <div class="err_msg"></div>
                    <div class="form-group">
                            <label class="control-label col-sm-3">Section Type:</label> 
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <div class="select">
                                        <select name="section_type" id="section_type" class="form-control" onchange="formchange(this)">
                                            <option value="1">Manually Generated</option>
                                            <option value="2">Auto Generated</option>
                                        </select>                        
                                    </div>
                                </div>
                            </div>
                        </div>
    <?php if ($has_physical || (((isset($content_enable) && ($content_enable['content_count'] & 4))))) { ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Content Type</label>
                                <div class="col-sm-4">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select name="content_type" id="content_type" class="form-control" onchange="selectCriteria(this)">
                                                <option value="0">Video</option>
        <?php if (isset($content_enable) && ($content_enable['content_count'] & 4)) { ?><option value="2">Audio</option><?php } ?>
        <?php if ($has_physical) { ?><option value="1">Physical</option><?php } ?>
                                            </select>                        
                                        </div>
                                    </div>
                                </div>                                    
                            </div>
    <?php } else { ?>
                            <input type="hidden" name="content_type" id="content_type" value="<?php echo $theme->content_type; ?>" />
    <?php } ?>    
                        <div class="form-group">
                            <label class="control-label col-sm-3">Section Name:</label> 
                            <div class="col-sm-9">
                                <div class="fg-line">
                                     <input required type="text" id="section_name" class="form-control input-sm" name="section_name" placeholder="Enter Section Name" value="<?php echo $page->title ?>" />
                                </div>
                            </div>
                    </div>                        
                        <div id="auto_generated">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Section Criteria:</label> 
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select name="section_criteria" id="section_criteria" class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">Most Viewed</option>
                                                <option value="2">Top Rated</option>
                                                <option value="3">Latest Uploads</option>
                                                <option value="4">Content Category</option>
                                                <!--<option value="5">Latest Movies, TV Series</option>-->
                                                <option value="6">My Library</option>
                                                <option value="7">My Favorites</option>
                                                <option value="8">Recently Seen</option>
                                            </select>                        
                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="scategory">
                                <label class="control-label col-sm-3">Section Category:</label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select name="section_category[]" id="section_category" class="form-control" multiple onchange="viewMore(this)">
                                                <option value="0">All</option>
    <?php foreach ($category as $key => $value) { ?>
                                                    <option value="<?= $key ?>"><?= $value ?></option>
    <?php } ?>
                                            </select>                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3"></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <div class="checkbox">
                                            <label>
                                                <input name="is_viewmore" id="is_viewmore" value="1" type="checkbox"><i class="input-helper"></i>Show a view more button
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="scategory">
                                <label class="control-label col-sm-3">Content Types:</label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select name="content_types[]" id="content_types" class="form-control" multiple>
                                                <option value="0">All</option>
                                                <option value="1">Single Part</option>
                                                <option value="3">Multi Part Parent</option>
                                                <option value="10">Multi Part Child</option>
                                            </select>                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Content Limit:</label> 
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <input type="number" min="0" max="99" id="content_limit" class="form-control input-sm" name="content_limit" placeholder="0-99" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>	
        <script type="text/javascript">
            function addSection()
            {
                var data = $("#add_homepage_section").serialize();
                var url = "<?php echo $this->createUrl('template/addHomepageSection'); ?>";
                $.post(url + "?" + data, function (res) {
                    if (res.succ == 0) {
                        $(".err_msg").html(res.msg);
                        $(".err_msg").show();
                    }
                });
            }
        </script>
    </div>
</div>
<!-- Add Popular Section Popup End -->

<!-- Add Popular Movie Popup Start -->
<div id="addPopularcontnet" class="modal fade">
    <div class="modal-dialog">
        <form  method="post" id="add_content" class="form-horizontal">
            <input type="hidden" id="featured_section" name="featured_section" />
            <input type="hidden" id="fs_content_type" name="fs_content_type" />            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Popular Content</h4>
                </div>
                <div class="modal-body">                        
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name:</label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input type="text" id="title" class="form-control input-sm" name="title" placeholder="Start Typing content name..." required>
                            </div>
                            <div id="pcontent_poster relative">
                                <div id="pcontent_poster_loader" class="preloader pls-blue text-center">
                                    <svg class="pl-circular" viewBox="25 25 50 50">
                                    <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                    </svg>                                   
                                </div>
                                <span class="help-block" id="preview_poster"></span>
                            </div>
                        </div>
                            
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="fcontent_save" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <input type="hidden" value="" id="movie_id" name="movie_id" />
            <input type="hidden" value="" id="is_episode" name="is_episode" />
        </form>	
    </div>
</div>
<?php } ?>
<!-- Add Popular Movie Popup End -->
<script>
    function myFunction() {
        document.getElementById("add_content").reset();
    }
    function formchange(obj) {
        var setCriteria;
        var select;
        if (obj.value === '2') {
            var contentType = $('#content_type').val();
            getSectionCriteria(contentType);
            $('#auto_generated').slideDown(400);
            //$("#section_criteria").prop("required", "true");
            $("#content_limit").prop("required", "true");
            $("#section_category").prop("required", "true");
            $("#content_types").prop("required", "true");
        } else {
            $('#auto_generated').slideUp(400);
            $("#section_criteria").removeAttr("required");
            $("#content_limit").removeAttr("required");
            $("#section_category").removeAttr("required");
            $("#content_types").removeAttr("required");
        }
    }
    function selectCriteria(obj) {
        var sectionType = $('#section_type').val();
        var contnetTypes;
        var options;
        if (sectionType === '2') {
            getSectionCriteria(obj.value);
        }
        if(obj.value === '2'){
            contnetTypes = [[0, 'All'], [5, 'Single Part'], [6, 'Multi Part Parent'], [10, 'Multi Part Child']];
    }
        else if (obj.value === '0') {
            contnetTypes = [[0, 'All'], [1, 'Single Part'], [3, 'Multi Part Parent'], [10, 'Multi Part Child']];
        }  
        if ($('#content_type').val() === '1') {
            $('select#content_types option').removeAttr("selected");
            $("#content_types").attr('disabled', true);
            $("#content_types").removeAttr("required");
            if ($('#is_viewmore').is(':checked')) {
                $("#is_viewmore").removeAttr("checked");
            }
            $("#is_viewmore").attr("disabled", true);
        }else{
            $("#content_types").removeAttr('disabled');
            if (sectionType === '2')
            $("#content_types").prop("required", "true");
            $("#is_viewmore").removeAttr("disabled");
        }
        if (sectionType === '2'){
            if($('#content_type').val() === '1'){
                $("#content_types").prop("required", false);
            }else{
                $.each(contnetTypes, function (index, value) {
                    options += '<option value="' + value[0] + '">' + value[1] + '</option>';
                });
                $('#content_types').html(options);
            }
        }
    }

    function getSectionCriteria(str) {
        var setCriteria;
        var select;
        setCriteria = criteria(str);
            $.each(setCriteria, function (index, value) {
            select += '<option value="' + value[0] + '">' + value[1] + '</option>';
            });
            if(select !== 'undefined')
            $('#section_criteria').html(select);

        if (str === '1') {
            $('#scategory').hide();
            $("#section_category").removeAttr("required");
        } else {
            $('#scategory').show();
            $("#section_category").prop("required", "true");
        }
    }

    function viewMore(obj,flag=0){
        var len;
        var selectedValue;
        if(flag){ 
            len = obj.length;
            selectedValue = obj;
        }else{
            selectedValue = $("#section_category").val();
            len = selectedValue.length;
        }
        if(len > 1 || ($.inArray('0', selectedValue) != -1)){
            if ($('#is_viewmore').is(':checked')) {
                $("#is_viewmore").removeAttr("checked");
            }
            $("#is_viewmore").attr("disabled", true);
        }else{
            $("#is_viewmore").removeAttr("disabled");
        }
    }
    $(document).ready(function () {
        $('#auto_generated').hide();
    });
</script>
