<div class="row m-t-40">   
    <div class="col-md-9">
        <div class="Block">            
            <div class="holderjs" id="holder1"></div>
            
            <form id="FRM_ST" name="FRM_ST" action="<?php echo $this->createUrl('template/settings'); ?>" method="post" class="form-horizontal">
                <p class="m-t-20 c-black f-500">Poster Dimension (in px)</p>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="err_message" class="error red"></div>
                    </div>
                </div>
                <div class="form-group">                    
                    <label class="col-sm-4 control-label">Horizontal:</label>
                    <div class="col-sm-8">
                        <div class="fg-line"><input name="h_poster_dimension" value="<?php echo $data['h_poster_dimension']?>" class="form-control input-sm" type="text" /></div>
                    </div>
                </div>    
                <div class="form-group"> 
                    <label class="col-sm-4 control-label">Vertical:</label>
                    <div class="col-sm-8">
                        <div class="fg-line"><input name="v_poster_dimension" value="<?php echo $data['v_poster_dimension']?>" class="form-control input-sm" type="text" /></div>
                    </div>
                </div> 
                <div class="form-group m-t-30">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    jQuery.validator.addMethod("dimension", function (value, element) {
            return this.optional(element) || /^[0-9]{1,4}x[0-9]{1,4}$/i.test(value);
    }, "Dimension must be in 123x456 format in px.");    
    $("#FRM_ST").validate({ 
        rules: {    
            h_poster_dimension: {
                required: true,
                dimension: true
            },   
            v_poster_dimension: {
                required: true,
                dimension: true
            }           
        },errorPlacement: function (label, element) {
            label.addClass('red');
            label.insertAfter(element.parent());
        },         
        submitHandler: function(form) {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/savesettings",
                data: $('#FRM_ST').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });  
        }                
    }); 
});
</script> 

