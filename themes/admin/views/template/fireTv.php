<?php
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$posterImg2 = POSTER_URL . '/no-image-a.png';
$posterImg3 = POSTER_URL . '/no-image-a.png';
?>
<style>
    #developer_id{
        display: none;
    }
    
</style>


<div class="row m-t-40 m-b-40">
    <div class="col-md-8 col-sm-12">
        <div class="Block">
            <form class="form-horizontal" method="POST" id="data-from" autocomplete="off">
                <input type="hidden" name="app_type" value="firetv" />
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4><span class=" bold red">Fire TV App is a paid add-on.</span><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/android/1');"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
                        </div>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="appname" class="col-md-4 control-label">App Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="app_name" placeholder="App Name" class="form-control input-sm" value="<?php if (isset($appdata)) {
                    echo $appdata['app_name'];
                } ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Short Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="short_description" placeholder="Short Description" class="form-control input-sm" value="<?php if (isset($appdata)) {
                    echo $appdata['short_description'];
                } ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="description" placeholder="Description.." class="form-control input-sm" rows="5" ><?php if (isset($appdata)) {
                    echo $appdata['description'];
                } ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">App Icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="app-icon-modal-btn">Upload App Icon</button>
                        <?php
                        $app_icon_url = $appdata['app_icon'];
                        $splash_screen_url = $appdata['splash_screen'];
                        
                        ?>
<?php if (isset($app_icon_url) && $app_icon_url != '') { ?>
                        <div class="fixedWidth--Preview m-t-20">
                            <img src="<?php echo $app_icon_url; ?>"   rel="tooltip" />
                        </div>
<?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="screen" class="col-md-4 control-label">Splash Screen:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-screen-modal-btn">Upload Splash Screen</button>

<?php if (isset($splash_screen_url) && $splash_screen_url != '') { ?>
                        <div class="fixedWidth--Preview m-t-20">
                            <img src="<?php echo $splash_screen_url; ?>"  rel="tooltip" />
                        </div>    
<?php } ?>
                    </div>
                </div>



                <div class="form-group">
                    <label for="distribution" class="col-md-4 control-label">Distribution Geography:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['distribution_geography'];
} ?>" id="geography" />
                                <select class="form-control input-sm" name="distribution_geography[]" style="height: 200px;" id="distribution_geography" multiple>
                                    <option value="All" selected>All Countries</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina">Burkina</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Chile">Chile</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote d' Ivore">Cote d' Ivore</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Laos">Laos</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Moldova">Moldova</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russia</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia">Serbia</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Korea">South Korea</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Taiwan">Taiwan</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States (including Puerto Rico, American Samoa, Guam, Marshall Islands, Northern Mariana Islands, Palau and US Virgin Islands)">United States (including Puerto Rico, American Samoa, Guam, Marshall Islands, Northern Mariana Islands, Palau and US Virgin Islands)</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Vietnam</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Language:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) {
    echo $appdata['language'];
} ?>" id="language" />
                                <select class="form-control input-sm" name="language" id="language-dd">
                                    <option value="Australian English">Australian English</option>
                                    <option value="Afrikaans – af">Afrikaans – af</option>
                                    <option value="Amharic – am">Amharic – am</option>
                                    <option value="Arabic – ar">Arabic – ar</option>
                                    <option value="Armenian – hy-AM">Armenian – hy-AM</option>
                                    <option value="Azerbaijani – az-AZ">Azerbaijani – az-AZ</option>
                                    <option value="Basque – eu-ES">Basque – eu-ES</option>
                                    <option value="Belarusian – be">Belarusian – be</option>
                                    <option value="Bengali – bn-BD">Bengali – bn-BD</option>
                                    <option value="Bulgarian – bg">Bulgarian – bg</option>
                                    <option value="Burmese – my-MM">Burmese – my-MM</option>
                                    <option value="Catalan – ca">Catalan – ca</option>
                                    <option value="Chinese (Simplified) – zh-CN">Chinese (Simplified) – zh-CN</option>
                                    <option value="Chinese (Traditional) – zh-TW">Chinese (Traditional) – zh-TW</option>
                                    <option value="Croatian – hr">Croatian – hr</option>
                                    <option value="Czech – cs-CZ">Czech – cs-CZ</option>
                                    <option value="Danish – da-DK">Danish – da-DK</option>
                                    <option value="Dutch – nl-NL">Dutch – nl-NL</option>
                                    <option value="English – en-AU">English – en-AU</option>
                                    <option value="English (United Kingdom) – en-GB">English (United Kingdom) – en-GB</option>
                                    <option value="English (United States) – en-US" selected="">English (United States) – en-US</option>
                                    <option value="Estonian – et">Estonian – et</option>
                                    <option value="Filipino – fil">Filipino – fil</option>
                                    <option value="Finnish – fi-FI">Finnish – fi-FI</option>
                                    <option value="French – fr-FR">French – fr-FR</option>
                                    <option value="French (Canada) – fr-CA">French (Canada) – fr-CA</option>
                                    <option value="Galician – gl-ES">Galician – gl-ES</option>
                                    <option value="Georgian – ka-GE">Georgian – ka-GE</option>
                                    <option value="German – de-DE">German – de-DE</option>
                                    <option value="Greek – el-GR">Greek – el-GR</option>
                                    <option value="Hebrew – iw-IL">Hebrew – iw-IL</option>
                                    <option value="Hindi – hi-IN">Hindi – hi-IN</option>
                                    <option value="Hungarian – hu-HU">Hungarian – hu-HU</option>
                                    <option value="Icelandic – is-IS">Icelandic – is-IS</option>
                                    <option value="Indonesian – id">Indonesian – id</option>
                                    <option value="Italian – it-IT">Italian – it-IT</option>
                                    <option value="Japanese – ja-JP">Japanese – ja-JP</option>
                                    <option value="Kannada – kn-IN">Kannada – kn-IN</option>
                                    <option value="Khmer – km-KH">Khmer – km-KH</option>
                                    <option value="Korean (South Korea) – ko-KR">Korean (South Korea) – ko-KR</option>
                                    <option value="Kyrgyz – ky-KG">Kyrgyz – ky-KG</option>
                                    <option value="Lao – lo-LA">Lao – lo-LA</option>
                                    <option value="Latvian – lv">Latvian – lv</option>
                                    <option value="Lithuanian – lt">Lithuanian – lt</option>
                                    <option value="Macedonian – mk-MK">Macedonian – mk-MK</option>
                                    <option value="Malay – ms">Malay – ms</option>
                                    <option value="Malayalam – ml-IN">Malayalam – ml-IN</option>
                                    <option value="Marathi – mr-IN">Marathi – mr-IN</option>
                                    <option value="Mongolian – mn-MN">Mongolian – mn-MN</option>
                                    <option value="Nepali – ne-NP">Nepali – ne-NP</option>
                                    <option value="Norwegian – no-NO">Norwegian – no-NO</option>
                                    <option value="Persian – fa">Persian – fa</option>
                                    <option value="Polish – pl-PL">Polish – pl-PL</option>
                                    <option value="Portuguese (Brazil) – pt-BR">Portuguese (Brazil) – pt-BR</option>
                                    <option value="Portuguese (Portugal) – pt-PT">Portuguese (Portugal) – pt-PT</option>
                                    <option value="Romanian – ro">Romanian – ro</option>
                                    <option value="Romansh – rm">Romansh – rm</option>
                                    <option value="Russian – ru-RU">Russian – ru-RU</option>
                                    <option value="Serbian – sr">Serbian – sr</option>
                                    <option value="Sinhala – si-LK">Sinhala – si-LK</option>
                                    <option value="Slovak – sk">Slovak – sk</option>
                                    <option value="Slovenian – sl">Slovenian – sl</option>
                                    <option value="Spanish (Latin America) – es-419">Spanish (Latin America) – es-419</option>
                                    <option value="Spanish (Spain) – es-ES">Spanish (Spain) – es-ES</option>
                                    <option value="Spanish (United States) – es-US">Spanish (United States) – es-US</option>
                                    <option value="Swahili – sw">Swahili – sw</option>
                                    <option value="Swedish – sv-SE">Swedish – sv-SE</option>
                                    <option value="Tamil – ta-IN">Tamil – ta-IN</option>
                                    <option value="Telugu – te-IN">Telugu – te-IN</option>
                                    <option value="Thai – th">Thai – th</option>
                                    <option value="Turkish – tr-TR">Turkish – tr-TR</option>
                                    <option value="Ukrainian – uk">Ukrainian – uk</option>
                                    <option value="Vietnamese – vi">Vietnamese – vi</option>
                                    <option value="Zulu – zu">Zulu – zu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                <label for="keyword" class="col-md-4 control-label">Are you charging for this app?: </label>
                <div class="col-md-8">
                    <div class="radio m-b-20">
                        <label>
                    <input type="radio" name="app_charge" value="0" <?php if($appdata['app_charge']=="" || $appdata['app_charge']==NULL){ echo "checked";}?> >
                    <i class="input-helper"></i>No, This is a free app
                        </label>
                    </div>
                    
                    <div class="radio m-b-20">
                        <label>
                    <input type="radio" name="app_charge" value="1" <?php if($appdata['app_charge']!="" || $appdata['app_charge']==1){ echo "checked";}?>>  
                    <i class="input-helper"></i>Yes, My base list price is
                        </label>
                    </div>
                    
                    
                    <input type="number" class="form-control" name="price" <?php if($appdata['app_charge']!="" || $appdata['app_charge']!=NULL){?> value="<?php echo $appdata['app_charge_amount'];?>"<?php }?>> 
                    <select name="currency" class="form-control">
                            <option value="US Dollar" <?php if($appdata['app_charge_currency']=="US Dollar"){?> selected <?php }?>>US Dollar</option>
                            <option value="AUS Dollar" <?php if($appdata['app_charge_currency']=="AUS Dollar"){?> selected <?php }?>>AUS Dollar</option>
                            <option value="IND Rupees" <?php if($appdata['app_charge_currency']=="IND Rupees"){?> selected <?php }?>>IND Rupees</option>
                        </select>
                </div>
                </div>
                
                <div class="form-group">
                <label for="keyword" class="col-md-4 control-label">Product Feature Bullets: </label>
                <div class="col-md-8">
                    <div class="fg-line">
                    <textarea name="pf_bullet" class="form-control input-sm" rows="5"><?php echo $appdata['product_feature_bullets'];?></textarea>
                    </div>
                </div>
                </div>
                
                <div class="form-group">
                <label for="keyword" class="col-md-4 control-label">Keywords: </label>
                <div class="col-md-8">
                    <div class="fg-line">
                    <textarea name="keywords" class="form-control input-sm" rows="5"><?php echo $appdata['keywords'];?></textarea>
                    </div>
                </div>
                </div>
                
                
                <div class="form-group">
                <?php
                    $support_data = json_decode($appdata['support_contact']);
                    //var_dump($support_data);
                ?>
                <label for="contact" class="col-md-4 control-label">Support Contact: </label>
                <div class="col-md-8">
                    <div class="row form-group-cancel-magin">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="admin_contact[name]" value="<?php echo @$support_data->name;?>" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telephone" class="col-md-4 control-label">Telephone: </label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="admin_contact[telephone]" value="<?php echo @$support_data->telephone;?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email: </label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="admin_contact[email]" value="<?php echo @$support_data->email;?>">
                                </div>
                            </div>
                        </div>
                     </div>
                    </div>
                </div>
             </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Privacy Policy URL:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Privacy Policy Url" name="privacy_policy_url" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['privacy_policy_url'];
} ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="checkbox">
                            <label>
                        <?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                                    <input type="checkbox" id="developer_id_checkbox" name="developer_check" value="1" checked />
<?php } else { ?>
                                    <input type="checkbox" id="developer_id_checkbox"  name="developer_check" value="1" />
<?php } ?>
                                <i class="input-helper"></i> Use my Fire Tv Developers ID
                            </label>
                        </div>
                    </div>
                </div>   
               
<?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                    <div id="developer_id" style="display:block">
<?php } else { ?>
                        <div id="developer_id">
<?php } ?>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">User Name:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" placeholder="User Name" name="developer_username" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_username'];
} ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password:</label>
                            <div class="col-md-8">
                                <input type="password" placeholder="Password" name="developer_password" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_password'];
} ?>" />
                            </div>
                        </div>
                    </div>

            </form>
            <form <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?> action="javascript:void(0)"<?php } else { ?>action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/fireTvApp" method="POST" id="data-from-save" <?php } ?> class="form-horizontal">
                <input type="hidden" name="app_type" value="firetv" />
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" class="btn btn-primary waves-effect btn-sm m-t-30" id="data-from-btn" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled<?php } ?>> &nbsp;&nbsp;Save&nbsp;&nbsp;  </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-lg-2"></label>
                    <div class="col-md-10">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
 <div class="modal fade is-Large-Modal" id="app-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appIconFireTv" method="POST" enctype="multipart/form-data" id="app-icon-from">
			 <input type="hidden" name="app_type" value="firetv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput')">Browse </button>
							
							<p class="help-block">Upload image size of 512x512</p>
							 <input class="avatar-input" id="avatarInput" name="lunchicon" type="file" style="display:none;" onchange="fileSelectHandler();">
							<span id="file_error" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($app_icon_url) && $app_icon_url != '') {
									$posterImg = $app_icon_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
											<div class=" m-b-10" id="avatar_preview_div" >
												<img class="jcrop-preview"  src="<?php echo $posterImg; ?>" id="preview_content_img" rel="tooltip" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x1" name="jcrop_lunchicon[x1]" />
									<input type="hidden" id="y1" name="jcrop_lunchicon[y1]" />
									<input type="hidden" id="x2" name="jcrop_lunchicon[x2]" />
									<input type="hidden" id="y2" name="jcrop_lunchicon[y2]" />
									<input type="hidden" id="w" name="jcrop_lunchicon[w]">
									<input type="hidden" id="h" name="jcrop_lunchicon[h]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="app-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>

 <div class="modal fade is-Large-Modal" id="splash-screen-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/splashScreenFireTv" method="POST" enctype="multipart/form-data" id="splash-screen-from">
            <input type="hidden" name="app_type" value="firetv" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
                                                        <!--PORTRAIT-->
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarSplashInput')">Browse </button>
         
							<p class="help-block">Upload image size of 1920x1080</p>
							 <input class="avatar-input" id="avatarSplashInput" name="splashicon" type="file" style="display:none;" onchange="splashFileSelectHandler();">
							<span id="file_error_2" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($splash_screen_url) && $splash_screen_url != '') {
									$posterImg2 = $splash_screen_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="splashicon_div">
											<div class="m-b-10" id="avatar_preview_div_splash">
                                                                                            <img class="jcrop-preview"  src="<?php echo $posterImg2; ?>" id="preview_content_img_splash" rel="tooltip" style="height:400px; width: 600px;" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x11" name="jcrop_splashicon[x11]" />
									<input type="hidden" id="y11" name="jcrop_splashicon[y11]" />
									<input type="hidden" id="x21" name="jcrop_splashicon[x21]" />
									<input type="hidden" id="y21" name="jcrop_splashicon[y21]" />
									<input type="hidden" id="w1" name="jcrop_splashicon[w1]">
									<input type="hidden" id="h1" name="jcrop_splashicon[h1]">
									
								</div>
                                                                
                                                                <!--PORTRAIT END-->
                                                                
                                                                
							</div>
                                        
                                        
					</div>
					
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="splash-screen-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>

 <div class="modal fade is-Large-Modal" id="feature-graphic-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/featureGraphic" method="POST" enctype="multipart/form-data" id="feature-graphic-from">
            <input type="hidden" name="app_type" value="android" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarFeatureGraphicInput')">Browse </button>
         
							<p class="help-block">Upload image size of 1024x500</p>
							 <input class="avatar-input" id="avatarFeatureGraphicInput" type="file" style="display:none;" name="feature_graphic" onchange="featureGraphicFileSelectHandler();">
							<span id="file_error_3" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($feature_graphic_url) && $feature_graphic_url != '') {
									$posterImg3 = $feature_graphic_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="feature_graphic_div">
											<div class="m-b-10" id="avatar_preview_div_feature_graphic" >
												<img src="<?php echo $posterImg3; ?>" id="preview_content_img_feature_graphic" rel="tooltip" />
											</div>
										</div>
									</div>
									 <input type="hidden" id="x12" name="jcrop_feature_graphic[x12]" />
									<input type="hidden" id="y12" name="jcrop_feature_graphic[y12]" />
									<input type="hidden" id="x22" name="jcrop_feature_graphic[x22]" />
									<input type="hidden" id="y22" name="jcrop_feature_graphic[y22]" />
									<input type="hidden" id="w2" name="jcrop_feature_graphic[w2]">
									<input type="hidden" id="h2" name="jcrop_feature_graphic[h2]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="feature-graphic-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>


<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script>
    function click_browse(modal_file) {
        $("#"+modal_file).click();
    }
                    var jcrop_api;
                    function fileSelectHandler() {
                        var reqwidth = 512;
                        var reqheight = 512;
                        var aspectRatio = reqwidth / reqheight;
                        clearInfo();
                        //$("#avataredit_preview").hide();
                        //$("#avatar_preview_div").removeClass("hide");
                        // get selected file
                        var oFile = $('#avatarInput')[0].files[0];
                        // hide all errors
                        // check for image type (jpg and png are allowed)
                        var rFilter = /^(image\/jpeg|image\/png)$/i;
                        if (!rFilter.test(oFile.type)) {
                            swal('Please select a valid image file (jpg and png are allowed)');
                            $("#avatar_preview_div").html('');
                            $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                            $('button[type="submit"]').attr('disabled', 'disabled');
                            return;
                        }

                        // preview element
                        var oImage = document.getElementById('preview_content_img');

                        // prepare HTML5 FileReader
                        var oReader = new FileReader();
                        oReader.onload = function (e) {
                            $('#file_error').hide();
                            // e.target.result contains the DataURL which we can use as a source of the image
                            oImage.src = e.target.result;
                            //$('#contentImg').attr('src',e.target.result);
                            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

                            oImage.onload = function () { // onload event handler
                                var w = oImage.naturalWidth;
                                var h = oImage.naturalHeight;
                                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                                console.log("Width=" + w + " Height=" + h);
                                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                                    $("#avatar_preview_div").html('');
                                    $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                                    $('button[type="submit"]').attr('disabled', 'disabled');
                                    return;
                                }

                                // destroy Jcrop if it is existed
                                if (typeof jcrop_api != 'undefined') {
                                    jcrop_api.destroy();
                                    jcrop_api = null;
                                }
                                $('#preview_content_img').width(oImage.naturalWidth);
                                $('#preview_content_img').height(oImage.naturalHeight);
                                //setTimeout(function(){
                                // initialize Jcrop
                                $('#preview_content_img').Jcrop({
                                    minSize: [reqwidth, reqheight], // min crop size,
                                    //maxSize: [reqwidth, reqheight],
                                    boxWidth: 300,
                                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                                    bgFade: true, // use fade effect
                                    bgOpacity: .3, // fade opacity
                                    onChange: updateInfo,
                                    onSelect: updateInfo,
                                    onRelease: clearInfo
                                }, function () {

                                    // use the Jcrop API to get the real image size
                                    bounds = this.getBounds();
                                    boundx = bounds[0];
                                    boundy = bounds[1];

                                    // Store the Jcrop API in the jcrop_api variable
                                    jcrop_api = this;
                                    //jcrop_api.animateTo([10, 10, 290, 410]);
                                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                                });
                                //},100);

                            };
                        };

                        // read selected file as DataURL
                        oReader.readAsDataURL(oFile);
                    }
</script>

<script>
    var jcrop_api;
    function splashFileSelectHandler() {
        var reqwidth = 1920; //before 1242
        var reqheight = 1080; //before 2208
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash").html('');
            $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash").html('');
                    $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash').width(oImage.naturalWidth);
                $('#preview_content_img_splash').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                   
                    boxWidth: 300,
                    
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplash,
                    onSelect: updateInfoSplash,
                    onRelease: clearInfoSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<!--SPLASH HANDLER LANDSCAPE-->
<script>
    var jcrop_api;
    function splashFileSelectHandlerLand() {
        var reqwidth = 1136; 
        var reqheight = 639;
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplashLscape();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput2')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash2").html('');
            $("#avatar_preview_div_splash2").html('<img id="preview_content_img_splash2"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash2');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash2").html('');
                    $("#avatar_preview_div_splash2").html('<img id="preview_content_img_splash2"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash2').width(oImage.naturalWidth);
                $('#preview_content_img_splash2').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash2').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplashLscape,
                    onSelect: updateInfoSplashLscape,
                    onRelease: clearInfoSplashLscape
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<!--SPLASH HANDLER LANDSCAPE END-->
<script>
    var jcrop_api;
    function featureGraphicFileSelectHandler() {
        var reqwidth = 1024;
        var reqheight = 500;
        var aspectRatio = reqwidth / reqheight;
        clearInfoFeatureGraphic();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarFeatureGraphicInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_feature_graphic").html('');
            $("#avatar_preview_div_feature_graphic").html('<img id="preview_content_img_feature_graphic"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_feature_graphic');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_feature_graphic").html('');
                    $("#avatar_preview_div_feature_graphic").html('<img id="preview_content_img_feature_graphic"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_feature_graphic').width(oImage.naturalWidth);
                $('#preview_content_img_feature_graphic').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_feature_graphic').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 500,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoFeatureGraphic,
                    onSelect: updateInfoFeatureGraphic,
                    onRelease: clearInfoFeatureGraphic
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<script>
    $('#app-icon-modal-btn').click(function () {
        $("#app-icon-modal-box").modal('show');
    });
    $('#splash-screen-modal-btn').click(function () {
        $("#splash-screen-modal-box").modal('show');
    });
    $('#feature-graphic-modal-btn').click(function () {
        $("#feature-graphic-modal-box").modal('show');
    });
    $('#developer_id_checkbox').click(function () {
        if ($(this).is(":checked")) {
            $('#developer_id').show();
        } else {
            $('#developer_id').hide();
        }
    });

    $('#app-icon-btn').click(function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateFireTvApp", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#app-icon-from').submit();
            }
        });
    });

    $('#splash-screen-btn').click(function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateFireTvApp", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#splash-screen-from').submit();
            }
        });
    });

    $('#feature-graphic-btn').click(function () {
        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateAppInfo", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#feature-graphic-from').submit();
            }
        });
    });

    $('#data-from-btn').click(function () {

        var data_from = $('#data-from').serialize();
        $.post(HTTP_ROOT + "/template/updateFireTvApp", {'is_ajax': 1, 'data_from': data_from}, function (res) {
            if (res) {
                $('#data-from-btn').html('Wait...');
                $('#data-from-btn').attr('disabled', 'disabled');
                $('#data-from-save').submit();
            }
        });
    });

    $(document).ready(function () {
        var category = $('#category').val();
        $('#category-dd option[value="' + category + '"]').attr('selected', true);
        var rating = $('#rating').val();
        $('#rating-dd option[value="' + rating + '"]').attr('selected', true);
        var language = $('#language').val();
        $('#language-dd option[value="' + language + '"]').attr('selected', true);

        var geography = $('#geography').val();
        if (geography != '' || geography != null) {
            $("#distribution_geography").children('option').each(function () {
                $(this).removeAttr('selected');
            });
        }

        $.each(geography.split(","), function (i, e) {
            $("#distribution_geography option[value='" + e + "']").prop("selected", true);
        });

    });
    function openinmodal(url) {
        //$('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            //$('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
</script>
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:20%;display: none;}
    .box.box-primary{border:none !important;}
    /*button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }*/
</style>