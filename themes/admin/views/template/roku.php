<?php
$studio = $this->studio;
$sscreen = POSTER_URL . '/no-image-a.png';
$fhd = POSTER_URL . '/no-image-a.png';
$hd = POSTER_URL . '/no-image-a.png';
$sd = POSTER_URL . '/no-image-a.png';
?>

<form method="POST" class="form-horizontal" id="data-from" autocomplete="off" action="rokuApp">
    <div class="row m-t-40 m-b-40">
        <div class="col-md-8 col-sm-12">
            <div class="Block">

                <input type="hidden" name="app_type" value="roku" />
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4><span class=" bold red">Roku App is a paid add-on.</span><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/roku/1');"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="channel_stores" class="col-md-4 control-label">Choose Channel Stores: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <?php
                            $stores = json_decode($app_info->channel_store);
                            ?>
                            <div class="">
                                <select name="channel_store[]" class="left_store form-control input-sm" multiple="multiple" size="6">
                                    <option value="United States" <?php if (in_array('United States', $stores)) {
                                echo 'selected';
                            } ?>>United States</option>
                                    <option value="Canada" <?php if (in_array('Canada', $stores)) {
                                echo 'selected';
                            } ?>>Canada</option>
                                    <option value="United Kingdom" <?php if (in_array('United Kingdom', $stores)) {
                                echo 'selected';
                            } ?>>United Kingdom</option>
                                    <option value="Ireland" <?php if (in_array('Ireland', $stores)) {
                                echo 'selected';
                            } ?>>Ireland</option>
                                    <option value="Medico" <?php if (in_array('Medico', $stores)) {
                                echo 'selected';
                            } ?>>Medico</option>
                                    <option value="France" <?php if (in_array('France', $stores)) {
                                echo 'selected';
                            } ?>>France</option>
                                    <option value="Rest of World" <?php if (in_array('Rest of World', $stores)) {
                                echo 'selected';
                            } ?>>Rest of World</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="languages" class="col-md-4 control-label">Languages: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
<?php
$languages = json_decode($app_info->language);
?>
                            <div class="">
                                <select name="lanuages[]" class="left_store form-control input-sm" multiple="multiple" size="4">
                                    <option value="English" <?php if (in_array('English', $languages)) {
    echo 'selected';
} ?>>English</option>
                                    <option value="Spench" <?php if (in_array('Spench', $languages)) {
    echo 'selected';
} ?>>Spench</option>
                                    <option value="German" <?php if (in_array('German', $languages)) {
    echo 'selected';
} ?>>German</option>
                                    <option value="Spanish" <?php if (in_array('Spanish', $languages)) {
    echo 'selected';
} ?>>Spanish</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="classification" class="col-md-4 control-label">Classifications: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select name="classifications" class="left_store form-control input-sm" >
                                    <option value="Video" <?php if ($app_info->classification == 'Video') {
    echo 'selected';
} ?>>Video</option>
                                    <option value="Audio" <?php if ($app_info->classification == 'Audio') {
    echo 'selected';
} ?>>Audio</option>
                                    <option value="Game" <?php if ($app_info->classification == 'Game') {
    echo 'selected';
} ?>>Game</option>
                                    <option value="App/Utility" <?php if ($app_info->classification == 'App/Utility') {
    echo 'selected';
} ?>>App/Utility</option>
                                    <option value="Theme" <?php if ($app_info->classification == 'Theme') {
    echo 'selected';
} ?>>Theme</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="requirement" class="col-md-4 control-label">Additional Requirements: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select name="additional_requirements" class="left_store form-control input-sm">
                                    <option value="None" <?php if ($app_info->additional_requirments == 'None') {
    echo 'selected';
} ?>>None</option>
                                    <option value="Additional  Fees May Apply" <?php if ($app_info->additional_requirments == 'Additional  Fees May Apply') {
    echo 'selected';
} ?>>Additional  Fees May Apply</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="parental" class="col-md-4 control-label">Parental Hint: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select name="parental_hint" class="left_store form-control input-sm">
                                    <option value="All Ages" <?php if ($app_info->parental_hint == 'All Ages') {
    echo 'selected';
} ?>>All Ages</option>
                                    <option value="Content Rated" <?php if ($app_info->parental_hint == 'Content Rated') {
    echo 'selected';
} ?>>Content Rated</option>
                                    <option value="Content Not Rated" <?php if ($app_info->parental_hint == 'Content Not Rated') {
    echo 'selected';
} ?>>Content Not Rated</option>
                                    <option value="Adult" <?php if ($app_info->parental_hint == 'Adult') {
    echo 'selected';
} ?>>Adult</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="form-group">
                    <label for="Channel" class="col-md-4 control-label">Channel Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" name="channel_name" value="<?php echo @$app_info->channel_name ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="subtitle" class="col-md-4 control-label">Channel Subtitle:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" name="channel_subtitle" value="<?php echo @$app_info->channel_subtitle ?>" />
                            
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="description" class="form-control input-sm" rows="5"><?php echo @$app_info->description ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="category" class="col-md-4 control-label">Category: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select name="category" class="left_store form-control input-sm">
                                    <option value="Apps" <?php if ($app_info->category == 'Apps') {
    echo 'selected';
} ?>>Apps</option>
                                    <option value="Comedy" <?php if ($app_info->category == 'Comedy') {
    echo 'selected';
} ?>>Comedy</option>
                                    <option value="Educational" <?php if ($app_info->category == 'Educational') {
    echo 'selected';
} ?>>Educational</option>
                                    <option value="Fitness" <?php if ($app_info->category == 'Fitness') {
    echo 'selected';
} ?>>Fitness</option>
                                    <option value="Food" <?php if ($app_info->category == 'Food') {
    echo 'selected';
} ?>>Food</option>
                                    <option value="Games" <?php if ($app_info->category == 'Games') {
    echo 'selected';
} ?>>Games</option>
                                    <option value="International" <?php if ($app_info->category == 'International') {
    echo 'selected';
} ?>>International</option>
                                    <option value="Kids & Family" <?php if ($app_info->category == 'Kids & Family') {
    echo 'selected';
} ?>>Kids & Family</option>
                                    <option value="LifeStyle" <?php if ($app_info->category == 'LifeStyle') {
    echo 'selected';
} ?>>LifeStyle</option>
                                    <option value="Movies & TV" <?php if ($app_info->category == 'Movies & TV') {
                        echo 'selected';
                    } ?>>Movies & TV</option>
                                    <option value="Music" <?php if ($app_info->category == 'Music') {
                        echo 'selected';
                    } ?>>Music</option>
                                    <option value="News & Weather" <?php if ($app_info->category == 'News & Weather') {
                        echo 'selected';
                    } ?>>News & Weather</option>
                                    <option value="Personal  Media" <?php if ($app_info->category == 'Personal  Media') {
                        echo 'selected';
                    } ?>>Personal  Media</option>
                                    <option value="Photo Apps" <?php if ($app_info->category == 'Photo Apps') {
                        echo 'selected';
                    } ?>>Photo Apps</option>
                                    <option value="Religious" <?php if ($app_info->category == 'Religious') {
                        echo 'selected';
                    } ?>>Religious</option>
                                    <option value="Sci & Tech" <?php if ($app_info->category == 'Sci & Tech') {
                        echo 'selected';
                    } ?>>Sci & Tech</option>
                                    <option value="Screensavers" <?php if ($app_info->category == 'Screensavers') {
                        echo 'selected';
                    } ?>>Screensavers</option>
                                    <option value="Shopping" <?php if ($app_info->category == 'Shopping') {
                        echo 'selected';
                    } ?>>Shopping</option>
                                    <option value="Special  Interest" <?php if ($app_info->category == 'Special  Interest') {
                        echo 'selected';
                    } ?>>Special  Interest</option>
                                    <option value="Sports" <?php if ($app_info->category == 'Sports') {
                        echo 'selected';
                    } ?>>Sports</option>
                                    <option value="Themes" <?php if ($app_info->category == 'Themes') {
                        echo 'selected';
                    } ?>>Themes</option>
                                    <option value="Travel" <?php if ($app_info->category == 'Travel') {
                        echo 'selected';
                    } ?>>Travel</option>
                                    <option value="Web Video" <?php if ($app_info->category == 'Web Video') {
                        echo 'selected';
                    } ?>>Web Video</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="keyword" class="col-md-4 control-label">Keywords: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="keywords" class="form-control input-sm" rows="5"><?php echo @$app_info->keywords ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="web" class="col-md-4 control-label">Web Description: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="web_description" class="form-control input-sm" rows="5"><?php echo @$app_info->web_description ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="feature" class="col-md-4 control-label ">Feature Information URL: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="feature_info_url" class="form-control input-sm" value="<?php echo @$app_info->feature_info_url ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="support" class="col-md-4 control-label">Support Contact: </label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="support_contact" class="form-control input-sm" value="<?php echo @$app_info->support_contact ?>">
                        </div>
                    </div>
                </div>




                <div class="form-group">
<?php
$admin_contact = json_decode(@$app_info->admin_contact);
?>
                    <label for="contact" class="col-md-4 control-label">Administrative Contact: </label>
                    <div class="col-md-8">
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Name:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="admin_contact[name]" value="<?php echo @$admin_contact->name ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telephone" class="col-md-4 control-label">Telephone: </label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="admin_contact[telephone]" value="<?php echo @$admin_contact->telephone ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-4 control-label">Email: </label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="admin_contact[email]" value="<?php echo @$admin_contact->email ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
<?php
$tech_contact = json_decode(@$app_info->tech_contact);
?>
                    <label for="technical" class="col-md-4 control-label">Technical Contact: </label>
                    <div class="col-md-8">
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="Name" class="col-md-4 control-label">Name:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="tech_contact[name]" value="<?php echo @$tech_contact->name ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Telephone:" class="col-md-4 control-label">Telephone: </label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="tech_contact[telephone]" value="<?php echo @$tech_contact->telephone ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Email" class="col-md-4 control-label">Email </label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="tech_contact[email]" value="<?php echo @$tech_contact->email ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
<?php
$roku_account = json_decode(@$app_info->roku_account);
?>
                    <label for="inputPassword3" class="col-md-4 control-label">Roku Account (If wants to publish the app using own roku account): </label>
                    <div class="col-md-8">

                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="user_id" class="col-md-4 control-label">Roku User ID:</label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" name="roku_account[user_id]" value="<?php echo @$roku_account->user_id ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Password" class="col-md-4 control-label">Password: </label>
                                    <div class="col-md-8">
                                        <div class="fg-line">
                                            <input type="password" class="form-control input-sm" name="roku_account[password]" value="<?php echo @$roku_account->password ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
<?php
$monetize = json_decode(@$app_info->monetize_plan);
?>
                    <label for="technical" class="col-md-4 control-label">Do you plan to monetize by any of the following: </label>
                    <div class="col-md-8">
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                                <div class="form-group">

                                    <!--                                <div class="checkbox">
                                                                        <label for="Name" class="col-md-4 control-label">Customer will pay before installing my chanel</label>
                                                                        <input type="checkbox" class="content-status" name="monetize_plan[monetize_1]" <?php if (@$monetize->monetize_1 != "") { ?>checked<?php } ?> value="plan_1" /><i class="input-helper"></i> 
                                                                    </div>-->
                                    <div class="col-sm-12">
                                        <div class="checkbox m-b-15">
                                            <label>
                                                <input type="checkbox" class="content-status" name="monetize_plan[monetize_1]" <?php if (@$monetize->monetize_1 != "") { ?>checked<?php } ?> value="plan_1" /><i class="input-helper"></i> 
                                                <i class="input-helper"></i>
                                                Customer will pay before installing my channel
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="checkbox m-b-15">
                                            <label>
                                                <input type="checkbox" class="content-status" name="monetize_plan[monetize_2]" <?php if (@$monetize->monetize_2 != "") { ?>checked<?php } ?> value="plan_2" /><i class="input-helper"></i> 
                                                <i class="input-helper"></i>
                                                Channel contains in channel subscription
                                            </label>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">
                                        <div class="checkbox m-b-15">
                                            <label>
                                                <input type="checkbox" class="content-status" name="monetize_plan[monetize_1]" <?php if (@$monetize->monetize_1 != "") { ?>checked<?php } ?> value="plan_1" /><i class="input-helper"></i> 
                                                <i class="input-helper"></i>
                                                Channel contains in channel one time purchase
                                            </label>
                                        </div>
                                    </div>


                                </div>
                                
                                <p id="mony-info" style="display:none"> <strong>You need to enable Monetization in your Roku account!</strong></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--SPLASH SCREEN-->
                <div class="form-group">

                    <label class="col-md-4 control-label">Upload Splash screen:</label>

                    <div class="col-md-8">

                        <div class="Collapse-Block">
                            <div class="Block-Header has-right-icon">
                                <div class="icon-OuterArea--rectangular">
                                    <a href="javascript:void(0);"><em class="icon-arrow-down icon left-icon icon-arrow-up"></em></a>
                                </div>
                                <h5 class="lead m-b-0">Splash Screen</h5>
                                <p class="grey font-12">Choose a 1920 x 1080 JPEG or PNG file. This image will display in the Roku Channel Store</p>
                                <div class="fg-line m-t-10">
                                    <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-btn">Upload Splash Screen</button>

                                </div>

                            </div>

                            <div class="Collapse-Content m-t-20" >      

                                <div class="fixedWidth--Preview">
<?php
if (@$app_info->splash_screen != '' || @$app_info->splash_screen != NULL) {
    $sscreen = $app_info->splash_screen;
}
?>
                                    <img src="<?php echo $sscreen; ?>" alt="" id="splash-poster-img" class="img-responsive">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!--SPLASH SCREEN-->



                <div class="form-group">

                    <label class="col-md-4 control-label">Upload Posters:</label>

                    <div class="col-md-8">

                        <div class="Collapse-Block">
                            <div class="Block-Header has-right-icon">
                                <div class="icon-OuterArea--rectangular">
                                    <a href="javascript:void(0);"><em class="icon-arrow-down icon left-icon icon-arrow-up"></em></a>
                                </div>



                                <h5 class="lead m-b-0">Full HD Poster</h5>
                                <p class="grey font-12">Choose a 540 x 405 JPEG or PNG file. This image will display in the Roku Channel Store when in Full HD mode.</p>
                                <div class="fg-line m-t-10">
                                    <button type="button" class="btn btn-default-with-bg btn-sm" id="fhd-btn">Upload Full HD Poster</button>

                                </div>

                            </div>

                            <div class="Collapse-Content m-t-20" >      

                                <div class="fixedWidth--Preview">
<?php
if (@$app_info->fhd_poster != '' || @$app_info->fhd_poster != NULL) {
    $fhd = $app_info->fhd_poster;
}
?>
                                    <img src="<?php echo $fhd; ?>" alt="" id="fhd-poster-img" class="img-responsive">

                                </div>
                            </div>

                        </div>

                        <div class="Collapse-Block">
                            <div class="Block-Header has-right-icon">
                                <div class="icon-OuterArea--rectangular">
                                    <a href="javascript:void(0);"><em class="icon-arrow-down icon left-icon icon-arrow-up"></em></a>
                                </div>



                                <h5 class="lead m-b-0">HD Poster</h5>
                                <p class="grey font-12">Choose a 290 x 218 JPEG or PNG file. This image will display in the Roku Channel Store when in HD mode.</p>
                                <div class="fg-line m-t-10">
                                    <button type="button" class="btn btn-default-with-bg btn-sm" id="hd-btn">Upload HD Poster</button>

                                </div>

                            </div>

                            <div class="Collapse-Content m-t-20">      

                                <div class="fixedWidth--Preview Middle-Width-Block">
<?php
if (@$app_info->hd_poster != '' || @$app_info->hd_poster != NULL) {
    $hd = $app_info->hd_poster;
}
?>
                                    <img src="<?php echo $hd; ?>" id="hd-poster-img" alt="" class="img-responsive">

                                </div>
                            </div>

                        </div>


                        <div class="Collapse-Block">
                            <div class="Block-Header has-right-icon">
                                <div class="icon-OuterArea--rectangular">
                                    <a href="javascript:void(0);"><em class="icon-arrow-down icon left-icon icon-arrow-up"></em></a>
                                </div>



                                <h5 class="lead m-b-0">SD Poster</h5>
                                <p class="grey font-12">Choose a 214 x 144 JPEG or PNG file. This image will display in the Roku Channel Store when in SD mode..</p>
                                <div class="fg-line m-t-10">
                                    <button type="button" class="btn btn-default-with-bg btn-sm" id="sd-btn">Upload SD Poster</button>  

                                </div>

                            </div>

                            <div class="Collapse-Content m-t-20">      

                                <div class="fixedWidth--Preview Small-Width-Block">
<?php
if (@$app_info->sd_poster != '' || @$app_info->sd_poster != NULL) {
    $sd = $app_info->sd_poster;
}
?>
                                    <img src="<?php echo $sd; ?>" id="sd-poster-img" alt="" class="img-responsive">

                                </div>
                            </div>

                        </div>

                    </div>
                </div>






                <div class="form-group">
                    <input type="hidden" value="<?php echo $sscreen; ?>" id="splash-image-path" name="splash_poster" />
                    <input type="hidden" value="<?php echo $fhd; ?>" id="fhd-image-path" name="fhd_poster" />
                    <input type="hidden" value="<?php echo $hd; ?>" id="hd-image-path" name="hd_poster" />
                    <input type="hidden" value="<?php echo $sd; ?>" id="sd-image-path" name="sd_poster" />
                    <input type="hidden" name="app_id" value="<?php echo $app_info->id ?>" />
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" class="btn btn-primary btn-sm" id="data-from-btn" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled<?php } ?>> &nbsp;&nbsp;Save&nbsp;&nbsp;  </button>
                    </div>
                </div>


            </div>
        </div>
    </div>  
</form>

<!--NEW SPLASH MODAL START-->
<div class="modal fade is-Large-Modal" id="splash-image-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/rokuImageUpload" method="POST" enctype="multipart/form-data" id="splash-from">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput_splash')">Browse </button>

                            <p class="help-block">Upload image size of 1920x1080</p>
                            <input class="avatar-input" id="avatarInput_splash" name="splash_image" type="file" onchange="fileSelectHandlerSplash();" style="display:none">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url) && $app_icon_url != '') {
    $posterImg = $app_icon_url;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="fhd_div">
                                    <div class="m-b-10" id="avatar_preview_div_splash" >
                                        <img class="jcrop-preview"  src="<?php echo $sscreen; ?>" style="width:500px; height: 300px;" id="preview_content_img_splash" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="img_type" value="rokusplash" />
                            <input type="hidden" id="x14" name="jcrop_splash[x14]" />
                            <input type="hidden" id="y14" name="jcrop_splash[y14]" />
                            <input type="hidden" id="x24" name="jcrop_splash[x24]" />
                            <input type="hidden" id="y24" name="jcrop_splash[y24]" />
                            <input type="hidden" id="w4" name="jcrop_splash[w4]">
                            <input type="hidden" id="h4" name="jcrop_splash[h4]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="splash-upload-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--NEW SPLASH MODAL END-->

<div class="modal fade is-Large-Modal" id="fhd-image-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/rokuImageUpload" method="POST" enctype="multipart/form-data" id="fhd-from">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput')">Browse </button>

                            <p class="help-block">Upload image size of 540x405</p>
                            <input class="avatar-input" id="avatarInput" name="fhd_image" type="file" onchange="fileSelectHandler();" style="display:none">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url) && $app_icon_url != '') {
    $posterImg = $app_icon_url;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="fhd_div">
                                    <div class="m-b-10" id="avatar_preview_div" >
                                        <img class="jcrop-preview"  src="<?php echo $fhd; ?>" id="preview_content_img" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="img_type" value="fhd" />
                            <input type="hidden" id="x1" name="jcrop_fhd[x1]" />
                            <input type="hidden" id="y1" name="jcrop_fhd[y1]" />
                            <input type="hidden" id="x2" name="jcrop_fhd[x2]" />
                            <input type="hidden" id="y2" name="jcrop_fhd[y2]" />
                            <input type="hidden" id="w" name="jcrop_fhd[w]">
                            <input type="hidden" id="h" name="jcrop_fhd[h]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="fhd-upload-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade is-Large-Modal" id="hd-image-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/rokuImageUpload" method="POST" enctype="multipart/form-data" id="hd-from">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput-2')">Browse </button>
                            <input class="avatar-input" id="avatarInput-2" name="hd_image" type="file" onchange="fileSelectHandler2();" style="display:none">

                            <p class="help-block">Upload image size of 290x218</p>

                            <span id="file_error_2" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url) && $app_icon_url != '') {
    $posterImg = $app_icon_url;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="hd_div">
                                    <div class="m-b-10" id="avatar_preview_div_2">
                                        <img class="jcrop-preview" src="<?php echo $hd; ?>" id="preview_content_img_2" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="img_type" value="hd" />
                            <input type="hidden" id="x11" name="jcrop_hd[x11]" />
                            <input type="hidden" id="y11" name="jcrop_hd[y11]" />
                            <input type="hidden" id="x21" name="jcrop_hd[x21]" />
                            <input type="hidden" id="y21" name="jcrop_hd[y21]" />
                            <input type="hidden" id="w1" name="jcrop_hd[w1]">
                            <input type="hidden" id="h1" name="jcrop_hd[h1]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="hd-upload-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade is-Large-Modal" id="sd-image-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/rokuImageUpload" method="POST" enctype="multipart/form-data" id="sd-from">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                </div>
                <div class="modal-body">
                    <div class="row is-Scrollable">
                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput-3')">Browse </button>
                            <input class="avatar-input" id="avatarInput-3" name="sd_image" type="file" onchange="fileSelectHandler3();" style="display:none">

                            <p class="help-block">Upload image size of 214x144</p>

                            <span id="file_error_3" class="error red" for="subdomain" style="display: block;"></span>

                        </div>
                        <div class="col-xs-12">
<?php
if (isset($app_icon_url) && $app_icon_url != '') {
    $posterImg = $app_icon_url;
}
?>
                            <div class="Preview-Block">
                                <div class="thumbnail m-b-0 jcrop-thumb" id="sd_div">
                                    <div class="m-b-10" id="avatar_preview_div_3">
                                        <img class="jcrop-preview" src="<?php echo $sd; ?>" id="preview_content_img_3" rel="tooltip" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="img_type" value="sd" />
                            <input type="hidden" id="x12" name="jcrop_sd[x12]" />
                            <input type="hidden" id="y12" name="jcrop_sd[y12]" />
                            <input type="hidden" id="x22" name="jcrop_sd[x22]" />
                            <input type="hidden" id="y22" name="jcrop_sd[y22]" />
                            <input type="hidden" id="w2" name="jcrop_sd[w2]">
                            <input type="hidden" id="h2" name="jcrop_sd[h2]">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="sd-upload-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>




<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script>
                                $('#data-from-btn').click(function () {
                                    $(this).html('Wait...');
                                    $(this).attr('disabled', 'disabled');
                                    $('#data-from').submit();
                                });

                                //new splash image 
                                $('#splash-btn').click(function () {
                                    $("#splash-image-modal-box").modal('show');
                                });

                                $('#splash-upload-btn').click(function () {
                                    $('#loader-1').show();
                                    $(this).html('Wait...');
                                    $(this).attr('disabled', 'disabled');
                                    var that = $(this);
                                    var data_from = $('#splash-from').serialize();
                                    var file_data = $('#avatarInput_splash').prop('files')[0];
                                    var form_data = new FormData();
                                    form_data.append('file', file_data);
                                    form_data.append('data', data_from);
                                    $.ajax({
                                        url: HTTP_ROOT + "/template/rokuImageUpload",
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function (res) {
                                            that.html('Upload');
                                            $('#loader-1').hide();
                                            that.removeAttr('disabled');
                                            $('#splash-image-path').val(res);
                                            $('#splash-poster-img').attr('src', res);
                                            $("#splash-image-modal-box").modal('hide');
                                        }
                                    });
                                });

                                //new splash image end
                                $('#fhd-btn').click(function () {
                                    $("#fhd-image-modal-box").modal('show');
                                });

                                $('#fhd-upload-btn').click(function () {
                                    $('#loader-1').show();
                                    $(this).html('Wait...');
                                    $(this).attr('disabled', 'disabled');
                                    var that = $(this);
                                    var data_from = $('#fhd-from').serialize();
                                    var file_data = $('#avatarInput').prop('files')[0];
                                    var form_data = new FormData();
                                    form_data.append('file', file_data);
                                    form_data.append('data', data_from);
                                    $.ajax({
                                        url: HTTP_ROOT + "/template/rokuImageUpload",
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function (res) {
                                            that.html('Upload');
                                            $('#loader-1').hide();
                                            that.removeAttr('disabled');
                                            $('#fhd-image-path').val(res);
                                            $('#fhd-poster-img').attr('src', res);
                                            $("#fhd-image-modal-box").modal('hide');
                                        }
                                    });
                                });

                                $('#hd-btn').click(function () {
                                    $("#hd-image-modal-box").modal('show');
                                });

                                $('#hd-upload-btn').click(function () {
                                    $('#loader-2').show();
                                    $(this).html('Wait...');
                                    $(this).attr('disabled', 'disabled');
                                    var that = $(this);
                                    var data_from = $('#hd-from').serialize();
                                    var file_data = $('#avatarInput-2').prop('files')[0];
                                    var form_data = new FormData();
                                    form_data.append('file', file_data);
                                    form_data.append('data', data_from);
                                    $.ajax({
                                        url: HTTP_ROOT + "/template/rokuImageUpload",
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function (res) {
                                            that.html('Upload');
                                            $('#loader-2').hide();
                                            that.removeAttr('disabled');
                                            $('#hd-image-path').val(res);
                                            $('#hd-poster-img').attr('src', res);
                                            $("#hd-image-modal-box").modal('hide');
                                        }
                                    });
                                });

                                $('#sd-btn').click(function () {
                                    $("#sd-image-modal-box").modal('show');
                                });

                                $('#sd-upload-btn').click(function () {
                                    $('#loader-3').show();
                                    $(this).html('Wait...');
                                    $(this).attr('disabled', 'disabled');
                                    var that = $(this);
                                    var data_from = $('#sd-from').serialize();
                                    var file_data = $('#avatarInput-3').prop('files')[0];
                                    var form_data = new FormData();
                                    form_data.append('file', file_data);
                                    form_data.append('data', data_from);
                                    $.ajax({
                                        url: HTTP_ROOT + "/template/rokuImageUpload",
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function (res) {
                                            that.html('Upload');
                                            $('#loader-3').hide();
                                            that.removeAttr('disabled');
                                            $('#sd-image-path').val(res);
                                            $('#sd-poster-img').attr('src', res);
                                            $("#sd-image-modal-box").modal('hide');
                                        }
                                    });
                                });
</script>

<!--NEW SPLASH SCREEN HANDLER START-->
<script>
    var jcrop_api;
    function fileSelectHandlerSplash() {
        var reqwidth = 1920;
        var reqheight = 1080;
        var aspectRatio = reqwidth / reqheight;
        clearInfoRokuSplash();
        var oFile = $('#avatarInput_splash')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash").html('');
            $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
            $('#splash-upload-btn').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview_content_img_splash');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash").html('');
                    $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
                    $('#splash-upload-btn').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash').width(oImage.naturalWidth);
                $('#preview_content_img_splash').height(oImage.naturalHeight);
                $('#splash-upload-btn').removeAttr('disabled');
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    // maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoRokuSplash,
                    onSelect: updateInfoRokuSplash,
                    onRelease: clearInfoRokuSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<!--NEW SPLASH SCREEN HANDLER END-->


<script>
    var jcrop_api;
    function fileSelectHandler() {
        var reqwidth = 540;
        var reqheight = 405;
        var aspectRatio = reqwidth / reqheight;
        clearInfo();
        var oFile = $('#avatarInput')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div").html('');
            $("#avatar_preview_div").html('<img id="preview_content_img"/>');
            $('#fhd-upload-btn').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview_content_img');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div").html('');
                    $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                    $('#fhd-upload-btn').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img').width(oImage.naturalWidth);
                $('#preview_content_img').height(oImage.naturalHeight);
                $('#fhd-upload-btn').removeAttr('disabled');
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    // maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<script>
    var jcrop_api;
    function fileSelectHandler2() {
        var reqwidth = 290;
        var reqheight = 218;
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        var oFile = $('#avatarInput-2')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_2").html('');
            $("#avatar_preview_div_2").html('<img id="preview_content_img_2"/>');
            $('#hd-upload-btn').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview_content_img_2');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error_2').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_2").html('');
                    $("#avatar_preview_div_2").html('<img id="preview_content_img_2"/>');
                    $('#hd-upload-btn').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_2').width(oImage.naturalWidth);
                $('#preview_content_img_2').height(oImage.naturalHeight);
                $('#hd-upload-btn').removeAttr('disabled');
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_2').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplash,
                    onSelect: updateInfoSplash,
                    onRelease: clearInfoSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<script>
    var jcrop_api;
    function fileSelectHandler3() {
        var reqwidth = 214;
        var reqheight = 144;
        var aspectRatio = reqwidth / reqheight;
        clearInfoFeatureGraphic();
        var oFile = $('#avatarInput-3')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_3").html('');
            $("#avatar_preview_div_3").html('<img id="preview_content_img_3"/>');
            $('#sd-upload-btn').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview_content_img_3');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error_3').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_3").html('');
                    $("#avatar_preview_div_3").html('<img id="preview_content_img_3"/>');
                    $('#sd-upload-btn').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_3').width(oImage.naturalWidth);
                $('#preview_content_img_3').height(oImage.naturalHeight);
                $('#sd-upload-btn').removeAttr('disabled');
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_3').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoFeatureGraphic,
                    onSelect: updateInfoFeatureGraphic,
                    onRelease: clearInfoFeatureGraphic
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
    function openinmodal(url) {
        //$('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            //$('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    function click_browse(modal_file) {
        $("#" + modal_file).click();
    }
    (function () {
        $('body').on('click', '.left-icon', function (e) {
            // e.preventDefault();
            $(this).toggleClass('icon-arrow-up');
            $(this).parent().parent().parent().next().slideToggle(200);
        })
    })();
</script>
<script>
    $(document).ready(function () {

        $('input[type = checkbox]').click(function () {
            var chklen = $('input[type="checkbox"]:checked').length;
           
            if (chklen > 0)
            {
                $("#mony-info").show();
            }
            else {
                $("#mony-info").hide();
            }
        });
    });
</script>
<div  class="loaderDiv">
    <div class="preloader pls-blue">
        <svg viewBox="25 25 50 50" class="pl-circular">
        <circle r="20" cy="50" cx="50" class="plc-path"/>
        </svg>
    </div>
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:10%;display: none;}
    .box.box-primary{border:none !important;}
    /*button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }*/
</style>