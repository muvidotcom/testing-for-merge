<?php 
//print'<pre>'; print_r($appdata); exit;
?>
<?php
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$posterImg1 = POSTER_URL . '/no-image-a.png';
$posterImg2 = POSTER_URL . '/no-image-a.png';
$posterImg3 = POSTER_URL . '/no-image-a.png';
$posterImg4 = POSTER_URL . '/no-image-a.png';
?>
<style>
    #developer_id{
        display: none;
    }
    .help-block{
        display: none;
    }
    .help-block-pop{
        display: block;
    }
    .has-error .help-block {
        display: block;
    }
</style>


<div class="row m-t-40 m-b-40">
    <div class="col-md-8 col-sm-12">
        <div class="Block">
            <form class="form-horizontal" method="POST" id="data-from" autocomplete="off">
                <input type="hidden" name="app_type" value="fireos" />
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4><span class=" bold red">Fire OS App is a paid add-on.</span><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/android/1');"><span class="bold blue"> Purchase subscription</span></a><span class="bold red"> to enable it.</span></h4>
                        </div>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <label for="appname" class="col-md-4 control-label">App Name:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                                <input type="text"  name="app_name" id="app_name" placeholder="App Name  (max limit 30 characters)" class="form-control input-sm" value="<?php if (isset($appdata) && ($appdata['app_name'] != '' || $appdata['app_name'] != null)) {
                    echo $appdata['app_name'];
                                } ?>" 
                                <?php if(isset($appdata['app_name']) && trim($appdata['app_name'])){echo 'readonly="true" style="cursor: not-allowed;" ';}?> required />
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Short Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" name="short_description" id="short_description" placeholder="Short Description   (max limit 1200 characters)" class="form-control input-sm" value="<?php if (isset($appdata)) {
                    echo $appdata['short_description'];
                } ?>" <?php if(isset($appdata['short_description']) && trim($appdata['short_description'])){echo 'readonly="true" style="cursor: not-allowed;"';}?> />
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="desc" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea name="description" id="description" placeholder="Description  (max limit 4000 characters)" class="form-control input-sm" rows="5" <?php if(isset($appdata['description']) && trim($appdata['description'])) { ?> readonly="readonly" style="cursor: not-allowed;" <?php }?>><?php if (isset($appdata)) {
                    echo $appdata['description'];
                } ?></textarea>
                        </div>
                        <small class="help-block"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">General Icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="gen-icon-modal-btn">Upload General Icon</button>
                        <?php
                        $gen_icon_url = $appdata['gen_icon'];
                        $splash_screen_url = $appdata['splash_screen'];
                        
                        ?>
                        <?php if (isset($gen_icon_url) && $gen_icon_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $gen_icon_url; ?>"   rel="tooltip" />
                            </div>
                        <?php } ?>
                        <small class="help-block">Please upload the General icon.</small>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">App Icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="app-icon-modal-btn">Upload App Icon</button>
                        <?php
                        $app_icon_url = $appdata['app_icon'];
                        $splash_screen_url = $appdata['splash_screen'];
                        
                        ?>
                        <?php if (isset($app_icon_url) && $app_icon_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $app_icon_url; ?>"   rel="tooltip" />
                            </div>
                        <?php } ?>
                        <small class="help-block">Please upload the App icon.</small>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="screen" class="col-md-4 control-label">Splash Screen:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg btn-sm" id="splash-screen-modal-btn">Upload Splash Screen</button>

                        <?php if (isset($splash_screen_url) && $splash_screen_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $splash_screen_url; ?>"  rel="tooltip" />
                            </div>    
                        <?php } ?>
                        <small class="help-block">Please upload the Splash Screen.</small>                        
                    </div>
                </div>

                <div class="form-group">
                    <label for="icon" class="col-md-4 control-label">Transparent App icon:</label>
                    <div class="col-md-8">
                        <button type="button" class="btn btn-default-with-bg waves-effect btn-sm" id="transparent-icon-modal-btn">Upload Transparent App Icon</button>
                        <?php
                        $transparent_icon_url = $appdata['transparent_app_icon'];
                        ?>
                        <?php if (isset($transparent_icon_url) && $transparent_icon_url != '') { ?>
                            <div class="fixedWidth--Preview m-t-20">
                                <img src="<?php echo $transparent_icon_url; ?>"   rel="tooltip" />
                            </div>
                        <?php } ?>
                        <small class="help-block">Please upload the transparent app icon.</small>
                    </div>
                    
                </div>  

                <div class="form-group">
                    <label for="distribution" class="col-md-4 control-label">Distribution Geography:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="">
                                <input type="hidden" value="<?php if (isset($appdata)) {
                                    echo $appdata['distribution_geography'];
                                } ?>" id="geography" />
                                <select class="form-control input-sm" name="distribution_geography[]" style="height: 150px;" id="distribution_geography" multiple>
                                    <option value="All" selected>All Countries</option>
                                    <option value="Africa ">Africa </option>
                                    <option value="Antarctica">Antarctica</option>
                                    <option value="Asia ">Asia </option>
                                    <option value="Europe">Europe</option>
                                    <option value="North America">North America</option>
                                    <option value="Oceania">Oceania</option>
                                    <option value="South America">South America</option>                                     
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Language:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                            <input type="hidden" value="<?php if (isset($appdata)) { echo $appdata['language'];} ?>" id="language" />
                                <select class="form-control input-sm" name="language" id="language-dd" <?php if(isset($appdata['language']) && trim($appdata['language'])) { echo "disabled= disabled";}?>>
                                    <option value="Arabic – ar">Arabic – ar</option>
                                    <option value="Chinese (Simplified) – zh-CN">Chinese - CN</option>
                                    <option value="Cornish – hr">Cornish – cr</option>
                                    <option value="Czech – cs-CZ">Czech – cs-CZ</option>
                                    <option value="Dutch – nl-NL">Dutch – nl-NL</option>
                                    <option value="English – en-AU" selected="">English – en</option>
                                    <option value="French – fr-FR">French – fr-FR</option>
                                    <option value="French (Canada) – fr-CA">French (Canada) – fr-CA</option>
                                    <option value="German – de-DE">German – de-DE</option>
                                    <option value="Greek – el-GR">Greek – el-GR</option>
                                    <option value="Hebrew – iw-IL">Hebrew – iw-IL</option>
                                    <option value="Hindi – hi-IN">Hindi – hi-IN</option>
                                    <option value="Italian – it-IT">Italian – it-IT</option>
                                    <option value="Japanese – ja-JP">Japanese – ja-JP</option>
                                    <option value="Kyrgyz – ky-KG">Kazakh – kz-KZ</option>
                                    <option value="Korean (South Korea) – ko-KR">Korean (South Korea) – ko-KR</option>
                                    <option value="Polish – pl-PL">Polish – pl-PL</option>
                                    <option value="Portuguese (Portugal) – pt-PT">Portuguese (Portugal) – pt-PT</option>
                                    <option value="Russian – ru-RU">Russian – ru-RU</option>
                                    <option value="Spanish (Spain) – es-ES">Spanish (Spain) – es-ES</option>
                                    <option value="Zulu – zu">Tagalog</option>
                                    <option value="Vietnamese – vi">Vietnamese – vi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>        
                
                <div class="form-group">
                    <label for="rating" class="col-md-4 control-label">Rating:</label>
                    <div class="col-md-8">
                        <input type="hidden" value="<?php if(isset($appdata)){echo $appdata['rating'];}?>" id="rating" />
                        <div class="row form-group-cancel-magin">
                            <div class="col-xs-12">
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Cartoon or Fantasy Violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-1" name="rating[Cartoon or Fantasy Violence]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Realistic Violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-2" name="rating[Realistic Violence]" >                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Sexual Content or Nudity:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-3" name="rating[Sexual Content or Nudity]"  >                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Profanity or Crude Humor:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-4" name="rating[Profanity or Crude Humor]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Alcohol, Tobacco, or Drug Use or References:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-5" name="rating[Alcohol, Tobacco, or Drug Use or References]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Mature/Suggestive Themes:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-6" name="rating[Mature/Suggestive Themes]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Simulated Gambling:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-7" name="rating[Simulated Gambling]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Horror/Fear Themes:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-8" name="rating[Horror/Fear Themes]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label">Prolonged graphic or sadistic realistic violence:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-9" name="rating[Prolonged graphic or sadistic realistic violence]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-md-4 control-label"> Graphic Sexual Content and Nudity:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control rating-dd input-sm" id="rating-dd-10" name="rating[Graphic Sexual Content and Nudity]">                                    
                                                <option value="None">None</option>
                                                <option value="Infrequent/Mild">Infrequent/Mild</option>
                                                <option value="Frequent/Intense">Infrequent/Intense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>                
                
                <div class="form-group">
                <label for="keyword" class="col-md-4 control-label">Product Feature Bullets: </label>
                <div class="col-md-8">
                    <div class="fg-line">
                    <textarea name="pf_bullet" id="pf_bullet" class="form-control input-sm" rows="5" <?php if(isset($appdata['product_feature_bullets']) && trim($appdata['product_feature_bullets'])) { ?> readonly="readonly" style="cursor: not-allowed;" <?php }?>><?php echo $appdata['product_feature_bullets'];?></textarea>
                    </div>
                    <small class="help-block">Please add the Product Feature Bullets.</small>
                </div>
                </div>                
                
                <div class="form-group">
                <?php
                    $support_data = json_decode($appdata['support_contact']);
                    //var_dump($support_data);
                ?>
                <label for="contact" class="col-md-4 control-label">Support Contact: </label>
                <div class="col-md-8">
                    <div class="row form-group-cancel-magin">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email: </label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="adminContact" name="admin_contact[email]" value="<?php echo @$support_data->email;?>"
                                   <?php if(isset($support_data->email) && trim($support_data->email)) { echo 'readonly="true" style="cursor: not-allowed;" '; }?> >
                                </div>
                                <small class="help-block">Please provide Customer Support Email Address.</small> 
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="telephone" class="col-md-4 control-label">Contact: </label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="admin_contact[telephone]" value="<?php echo @$support_data->telephone;?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Website:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" name="admin_contact[name]" value="<?php echo @$support_data->name;?>" >
                                </div>
                            </div>
                        </div>                        
                     </div>
                    </div>
                </div>
             </div>
                
                <div class="form-group">
                    <label for="category" class="col-md-4 control-label">Category:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <input type="hidden" value="<?php if (isset($appdata)) { echo $appdata['category'];} ?>" id="category" />
                               
                                <?php if(isset($appdata['category']) && trim($appdata['category'])) { ?>
                                 <input type="text" class="form-control input-sm" name="category" id="category-dd" value="<?php echo $appdata['category'] ?>" readonly="true" style="cursor: not-allowed;" > <?php }  else { ?>

                                
                                <select class="form-control input-sm" name="category" id="category-dd" >
                                    <option value="">Select</option>
                                    <option value="Books and References">Books and References</option>
                                    <option value="Business">Business</option>
                                    <option value="Comics">Comics</option>
                                    <option value="Communication">Communication</option>
                                    <option value="Education">Education</option>
                                    <option value="Entertainment">Entertainment</option>
                                    <option value="Finance">Finance</option>
                                    <option value="Health and Fitness">Health and Fitness</option>
                                    <option value="Libraries and Demo">Libraries and Demo</option>
                                    <option value="Lifestyles">Lifestyles</option>
                                    <option value="Live wallpaper">Live wallpaper</option>
                                    <option value="Media and Video">Media and Video</option>
                                    <option value="Medical">Medical</option>
                                    <option value="Music and Audio">Music and Audio</option>
                                    <option value="News and Magazines">News and Magazines</option>
                                    <option value="Personalization">Personalization</option>
                                    <option value="Photography">Photography</option>
                                    <option value="Productivity">Productivity</option>
                                    <option value="Shopping">Shopping</option>
                                    <option value="Social">Social</option>
                                    <option value="Sports">Sports</option>
                                    <option value="Tools">Tools</option>
                                    <option value="Transportation">Transportation</option>
                                    <option value="Travel and Local">Travel and Local</option>
                                    <option value="Weather">Weather</option>
                                    <option value="Widgets">Widgets</option>
                                </select>
                                 <?php } ?>
                            </div>
                        </div>
                        <small class="help-block">Please select a category.</small>
                    </div>
                </div>                
                
                
                <div class="form-group">
                    <label for="inputPassword3" class="col-md-4 control-label">Privacy Policy URL:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" placeholder="Privacy Policy Url" id="privacy_policy" name="privacy_policy_url" class="form-control input-sm" value="<?php if (isset($appdata)) {
    echo $appdata['privacy_policy_url']; } ?>" <?php if(isset($appdata['privacy_policy_url']) && trim($appdata['privacy_policy_url'])) { echo 'readonly="true" style="cursor: not-allowed;" '; }?> />
                        </div>
                        <small class="help-block">Please provide the privacy policy url.</small>
                    </div>
                </div>                

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div class="checkbox">
                            <label>
                        <?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                                    <input type="checkbox" id="developer_id_checkbox" name="developer_check" value="1" checked />
<?php } else { ?>
                                    <input type="checkbox" id="developer_id_checkbox"  name="developer_check" value="1" />
<?php } ?>
                                <i class="input-helper"></i> Use my Fire OS Developers ID
                            </label>
                        </div>
                    </div>
                </div>   
               
<?php if (isset($appdata) && $appdata['developer_username'] != '' && $appdata['developer_password'] != '') { ?>    
                    <div id="developer_id" style="display:block">
<?php } else { ?>
                        <div id="developer_id">
<?php } ?>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">User Name:</label>
                            <div class="col-md-8">
                                <div class="fg-line">
                                    <input type="text" placeholder="User Name" name="developer_username" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_username'];
} ?>" <?php if(isset($appdata['developer_username']) && trim($appdata['developer_username'])){echo 'readonly="true" style="cursor: not-allowed;" ';}?> />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password:</label>
                            <div class="col-md-8">
                                <input type="password" placeholder="Password" name="developer_password" class="form-control input-sm" autocomplete="off" value="<?php if (isset($appdata)) {
    echo $appdata['developer_password'];
} ?>" <?php if(isset($appdata['developer_password']) && trim($appdata['developer_password'])){echo 'readonly="true" style="cursor: not-allowed;" ';}?>/>
                            </div>
                        </div>
                    </div>

                    </div></form>
            <form <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?> action="javascript:void(0)"<?php } else { ?>action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/fireOsApp" method="POST" id="data-from-save" <?php } ?> class="form-horizontal">
                <input type="hidden" name="app_type" value="fireos" />
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="button" class="btn btn-primary waves-effect btn-sm m-t-30" id="data-from-btn" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled<?php } ?>> &nbsp;&nbsp;Save&nbsp;&nbsp;  </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-lg-2"></label>
                    <div class="col-md-10">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<div class="modal fade is-Large-Modal" id="gen-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/generalAppIconFireOs" method="POST" enctype="multipart/form-data" id="gen-icon-from">
			 <input type="hidden" name="app_type" value="fireos" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInputGen')">Browse </button>
							
							<p class="help-block-pop">Upload image size of 512x512</p> 
							 <input class="avatar-input" id="avatarInputGen" name="lunchicon" type="file" style="display:none;" onchange="fileSelectHandlerGen();"> 
							<span id="file_error" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($gen_icon_url) && $gen_icon_url != '') {
									$posterImg4 = $gen_icon_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
											<div class=" m-b-10" id="avatar_preview_div_gen" >
												<img class="jcrop-preview"  src="<?php echo $posterImg4; ?>" id="preview_content_img_gen" rel="tooltip" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x116" name="jcrop_lunchicon[x116]" />
									<input type="hidden" id="y116" name="jcrop_lunchicon[y116]" />
									<input type="hidden" id="x217" name="jcrop_lunchicon[x217]" />
									<input type="hidden" id="y217" name="jcrop_lunchicon[y217]" />
									<input type="hidden" id="w9" name="jcrop_lunchicon[w9]">
									<input type="hidden" id="h9" name="jcrop_lunchicon[h9]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="gen-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
</div>


<div class="modal fade is-Large-Modal" id="app-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/appIconFireOs" method="POST" enctype="multipart/form-data" id="app-icon-from">
			 <input type="hidden" name="app_type" value="fireos" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInput')">Browse </button>
							
							<p class="help-block-pop">Upload image size of 1280x720</p>
							 <input class="avatar-input" id="avatarInput" name="lunchicon" type="file" style="display:none;" onchange="fileSelectHandler();"> 
							<span id="file_error" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($app_icon_url) && $app_icon_url != '') {
									$posterImg = $app_icon_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
											<div class=" m-b-10" id="avatar_preview_div" >
												<img class="jcrop-preview"  src="<?php echo $posterImg; ?>" id="preview_content_img" rel="tooltip" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x1" name="jcrop_lunchicon[x1]" />
									<input type="hidden" id="y1" name="jcrop_lunchicon[y1]" />
									<input type="hidden" id="x2" name="jcrop_lunchicon[x2]" />
									<input type="hidden" id="y2" name="jcrop_lunchicon[y2]" />
									<input type="hidden" id="w" name="jcrop_lunchicon[w]">
									<input type="hidden" id="h" name="jcrop_lunchicon[h]">
									
								</div>
							</div>
					</div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="app-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
</div>

<!--  Transparent app icon start   -->
 <div class="modal fade is-Large-Modal" id="transparent-icon-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/FireOsTransparentAppIcon" method="POST" enctype="multipart/form-data" id="transparent-icon-from">
                <input type="hidden" name="app_type" value="fireos" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
                                    </div>
                    <div class="modal-body">
                    <div class="row is-Scrollable">
                            <div class="col-xs-12 m-t-40 m-b-20 text-center">
                            <button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarInputTransparent')">Browse </button>

                            <p class="help-block-pop">Upload image size of 512x512</p>
                             <input class="avatar-input" id="avatarInputTransparent" name="lunchicon" type="file" style="display:none;" onchange="TransparentfileSelectHandler();">
                            <span id="file_error" class="error red" for="subdomain" style="display: block;"></span>

                            </div>
                            <div class="col-xs-12">
                            <?php
                            if (isset($transparent_icon_url) && $transparent_icon_url != '') {
                                    $posterImg1 = $transparent_icon_url; 
                            }
                            ?>
                                    <div class="Preview-Block">
                                            <div class="thumbnail m-b-0 jcrop-thumb" id="lunchicon_div">
                                                    <div class=" m-b-10" id="avatar_preview_div_transparent" >
                                                            <img class="jcrop-preview" src="<?php echo $posterImg1; ?>" id="preview_content_img_transparent" rel="tooltip" />
                                                    </div>
                                            </div>
                                    </div>
                                    <input type="hidden" id="x101" name="jcrop_lunchicon[x101]" />
                                    <input type="hidden" id="y101" name="jcrop_lunchicon[y101]" />
                                    <input type="hidden" id="x201" name="jcrop_lunchicon[x201]" />
                                    <input type="hidden" id="y201" name="jcrop_lunchicon[y201]" />
                                    <input type="hidden" id="w5" name="jcrop_lunchicon[w5]">
                                    <input type="hidden" id="h5" name="jcrop_lunchicon[h5]">

                            </div>
                    </div>
                </div>
					
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="transparent-icon-btn" data-dismiss="modal">Save</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>

 <div class="modal fade is-Large-Modal" id="splash-screen-modal-box"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
			 <form action="<?php echo Yii::app()->getbaseUrl(true) ?>/template/splashScreenFireOs" method="POST" enctype="multipart/form-data" id="splash-screen-from">
            <input type="hidden" name="app_type" value="fireos" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Image </h4>
				</div>
                <div class="modal-body">
					<div class="row is-Scrollable">
                                                        <!--PORTRAIT-->
							<div class="col-xs-12 m-t-40 m-b-20 text-center">
							<button type="button" class="btn btn-default-with-bg btn-sm" onclick="click_browse('avatarSplashInput')">Browse </button>
         
							<p class="help-block-pop">Upload image size of 1920x1080</p>
							 <input class="avatar-input" id="avatarSplashInput" name="splashicon" type="file" style="display:none;" onchange="splashFileSelectHandler();">
							<span id="file_error_2" class="error red" for="subdomain" style="display: block;"></span>
							
							</div>
								<div class="col-xs-12">
								<?php
								if (isset($splash_screen_url) && $splash_screen_url != '') {
									$posterImg2 = $splash_screen_url;
								}
								?>
									<div class="Preview-Block">
										<div class="thumbnail m-b-0 jcrop-thumb" id="splashicon_div">
											<div class="m-b-10" id="avatar_preview_div_splash">
                                                                                            <img class="jcrop-preview"  src="<?php echo $posterImg2; ?>" id="preview_content_img_splash" rel="tooltip" style="height:400px; width: 600px;" />
											</div>
										</div>
									</div>
									<input type="hidden" id="x11" name="jcrop_splashicon[x11]" />
									<input type="hidden" id="y11" name="jcrop_splashicon[y11]" />
									<input type="hidden" id="x21" name="jcrop_splashicon[x21]" />
									<input type="hidden" id="y21" name="jcrop_splashicon[y21]" />
									<input type="hidden" id="w1" name="jcrop_splashicon[w1]">
									<input type="hidden" id="h1" name="jcrop_splashicon[h1]">
									
								</div>
                                                                
                                                                <!--PORTRAIT END-->
                                                                
                                                                
							</div>
                                        
                                        
					</div>
					
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="splash-screen-btn" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
				</form>
            </div>
        </div>
    </div>


<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<script>
    $('input').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });
    $('textarea').on('keyup', function(){
        if($(this).val().length >= 2) {
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().parent().removeClass('has-error');
        }
    });     
    
    var rating = "<?php if(isset($appdata['rating']) && trim($appdata['rating'])){echo $appdata['rating'];}?>";
    var category = "<?php if(isset($appdata['category'])&& trim($appdata['category'])){echo $appdata['category'];}?>";
    var category_dd = "<?php if(isset($appdata['category-dd'])&& trim($appdata['category-dd'])){echo $appdata['category-dd'];}?>";
    var app_name = "<?php if(isset($appdata['app_name']) && trim($appdata['app_name'])){echo $appdata['app_name'];}?>"; 
    var short_desc = "<?php if(isset($appdata['short_description']) && trim($appdata['short_description'])){echo $appdata['short_description'];}?>";
    var dist_geo = "<?php if(isset($appdata['distribution_geography']) && trim($appdata['distribution_geography'])){echo $appdata['distribution_geography'];}?>";
    var language = "<?php if(isset($appdata['language'])&& trim($appdata['language'])){echo $appdata['language'];}?>";
    var privacy_policy = "<?php if(isset($appdata['privacy_policy_url'])&& trim($appdata['privacy_policy_url'])){echo $appdata['privacy_policy_url'];}?>";
    var developer_username = "<?php if(isset($appdata['developer_username'])&& trim($appdata['developer_username'])){echo $appdata['developer_username'];}?>";
    var developer_password = "<?php if(isset($appdata['developer_password'])&& trim($appdata['developer_password'])){echo $appdata['developer_password'];}?>";
        
       function appFormValidation() {  
        var name = $("#app_name").val();
        var short_description = $("#short_description").val();
        var description = $("#description").val();
        var email = $("#email").val();

        function isProperMailId(string) {
        if(!string) return false;
        var iChars = "=+*|,\":<>[]{}~`\';()&$#%!^\\\/?-";
        for (var i = 0; i < string.length; i++){
           if(iChars.indexOf(string.charAt(i)) != -1)
                return false;
        }
        return true;
        }
        //var phone = $("#phone").val();
        var category_dd = $("#category-dd").val();
        var pp_url = $("#privacy_policy").val(); 
        var pf_bullet = $("#pf_bullet").val();
        var adminContact = $("#adminContact").val();
        if(!$.trim(name)){
         $("#app_name").parent().siblings().text('Please give a name for your app.');
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(name).length > 30 ){
         $("#app_name").parent().siblings().text('max limit 30 characters');   
         $("#app_name").parent().parent().parent().addClass('has-error');  
         return false;
        } 
        if(!$.trim(short_description)){
         $("#short_description").parent().siblings().text('Please give a short description for your app.');              
         $("#short_description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(short_description).length > 1200){
         $("#short_description").parent().siblings().text('max limit 1200 characters');  
         $("#short_description").parent().parent().parent().addClass('has-error');  
         return false;
        }        
        if(!$.trim(description)){
         $("#description").parent().siblings().text('Please give a description for your app.');                 
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }
        if($.trim(description).length > 4000){
         $("#description").parent().siblings().text('max limit 4000 characters');     
         $("#description").parent().parent().parent().addClass('has-error');  
         return false;
        }   
       
        if(!$('#gen-icon-modal-btn').siblings().children('img').length){
            $("#gen-icon-modal-btn").parent().parent().addClass('has-error');  
        return false;
        }         
        if(!$('#app-icon-modal-btn').siblings().children('img').length){
            $("#app-icon-modal-btn").parent().parent().addClass('has-error');  
        return false;
        } 
        if(!$('#splash-screen-modal-btn').siblings().children('img').length){
         $("#splash-screen-modal-btn").parent().parent().addClass('has-error');  
         return false;
        } 
        if(!$('#transparent-icon-modal-btn').siblings().children('img').length){
            $("#transparent-icon-modal-btn").parent().parent().addClass('has-error');  
        return false;
        }  
        if(!$.trim(pf_bullet)){
         $("#pf_bullet").parent().parent().parent().addClass('has-error');   
         return false;
        }        
        if(!$.trim(adminContact)){
         $("#adminContact").parent().parent().parent().addClass('has-error');  
         return false;
        }  

        if(category_dd==''){
         $("#category-dd").parent().parent().parent().parent().addClass('has-error');  
         return false;
        }else{
          $("#category-dd").parent().parent().parent().parent().removeClass('has-error');    
        }
         if(!$.trim(pp_url)){
         $("#privacy_policy").parent().parent().parent().addClass('has-error');  
         return false;
        }           
               
        return true;

    }   
    
    $('#app-icon-modal-btn').click(function () {
        $("#app-icon-modal-box").modal('show');
    });
    $('#gen-icon-modal-btn').click(function () {
        $("#gen-icon-modal-box").modal('show');
    });
    $('#splash-screen-modal-btn').click(function () {
        $("#splash-screen-modal-box").modal('show');
    });
    $('#transparent-icon-modal-btn').click(function () {
        $("#transparent-icon-modal-box").modal('show');
    });
    
    $('#developer_id_checkbox').click(function () {
        if ($(this).is(":checked")) {
            $('#developer_id').show();
        } else {
            $('#developer_id').hide();
        }
    });


    $('#app-icon-btn').click(function () {
        var oFile = $('#avatarInput')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }        
        
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 1;
        <?php }?>        
        $.post(HTTP_ROOT + "/template/updateFireOsApp", {'is_ajax': 1, 'is_image': is_image, 'data_from': data_from}, function (res) {
            if (res) {
                $('#app-icon-from').submit();
            }
        });
    });
    $('#gen-icon-btn').click(function () {
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 1;
        <?php }?>        
        $.post(HTTP_ROOT + "/template/updateFireOsApp", {'is_ajax': 1, 'is_image': is_image, 'data_from': data_from}, function (res) {
            if (res) {
                $('#gen-icon-from').submit();
            }
        });
    });

    $('#splash-screen-btn').click(function () {
        var data_from = $('#data-from').serialize();
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
            var is_image = 6;
        <?php }else{?>
            var is_image = 2;
        <?php }?>       
        
        $.post(HTTP_ROOT + "/template/updateFireOsApp", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#splash-screen-from').submit();
            }
        });
    });

    $('#transparent-icon-btn').click(function () {
        var oFile = $('#avatarInputTransparent')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if(typeof(oFile) == "undefined"){
            swal('Please select a valid image file (jpg and png are allowed)');
            return false;
        }      
        var data_from = $('#data-from').serialize();
        if($('#transparent-icon-modal-btn').siblings().children('img').length){
            
        }
        
        <?php
        if(isset($appdata) && intval($appdata['is_update']) == 5){ ?>
                var is_image = 6;
        <?php }else{?>
            var is_image = 1;
        <?php }?>            
            
        $.post(HTTP_ROOT + "/template/updateFireOsApp", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {
            if (res) {
                $('#transparent-icon-from').submit();
            }
        });
    }); 
 
    $('#data-from-btn').click(function () {
        var is_valid = appFormValidation();
        if(is_valid){
            <?php if (isset($appdata) && !empty($appdata)){ ?>
                 if($.trim(app_name)){
                $('#app_name').val(app_name).removeAttr("disabled");
                }       
                if($.trim(short_desc)){
                $('#short_description').val(short_desc).removeAttr("disabled");
                }
                
                <?php if(isset($appdata) && intval($appdata['is_update']) > 5){?>
                if($.trim(dist_geo)){
                    $("#distribution_geography").removeAttr("name");
                } 
                <?php }?>
                if($.trim(rating) && rating !== 'None-None-None-None-None-None-None-None-None-None'){
                    for(var i=1;i<=10;i++){
                        $("#rating-dd-"+i).removeAttr("name");
                    }
                } 
                if($.trim(language)){
                $('#language').val(language).removeAttr("disabled");
                }
                
                $('#category').val(category);
                $('#category-dd').removeAttr("disabled");

                if($.trim(privacy_policy)){
                $('#privacy_policy').val(privacy_policy).removeAttr("disabled");
                }
                
                if($.trim(developer_username) && $.trim(developer_password)){
                    $('#developer_id_checkbox').removeAttr("disabled");
                    $('#dev_uname').val(developer_username).removeAttr("disabled");
                    $('#dev_password').val(developer_password).removeAttr("disabled");
                }

            <?php }
            ?>
            var data_from = $('#data-from').serialize();
            var is_image = 5;
            $.post(HTTP_ROOT + "/template/updateFireOsApp", {'is_ajax': 1,'is_image': is_image , 'data_from': data_from}, function (res) {      
                if (res) {
                    $('#data-from-btn').html('Wait...');
                    $('#data-from-btn').attr('disabled', 'disabled');
                    $('#data-from-save').submit();
                }
            });
        }
    });

    $(document).ready(function () {
        var category = $('#category').val();
        $('#category-dd option[value="' + category + '"]').attr('selected', true);
        var rating_string = $('#rating').val();
        var k=1;
        $.each(rating_string.split('-'), function(i,e){
            $("#rating-dd-"+k+" option[value='" + e + "']").prop("selected", true);
            k++;
        });
        if($.trim(rating) && rating !== 'None-None-None-None-None-None-None-None-None-None'){
            for(var i=1;i<=10;i++){
                $("#rating-dd-"+i).attr('disabled', 'disabled');
            }
        }   

        var language = $('#language').val();
        $('#language-dd option[value="' + language + '"]').attr('selected', true);
        
        var geography = "<?php if(isset($appdata['distribution_geography'])){echo $appdata['distribution_geography'];}?>";
        if (geography !== '') {
            $("#distribution_geography").attr('disabled',"disabled");
            $("#distribution_geography").children('option').each(function () {
                $(this).removeAttr('selected');
            });
        }

        $.each(geography.split(","), function (i, e) {
            $("#distribution_geography option[value='" + e + "']").prop("selected", true);
        });

    });
    function openinmodal(url) {
        //$('.loaderDiv').show();   
        $.get(url, {"modalflag": 1}, function (data) {
            //$('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
</script>

<script>
    function click_browse(modal_file) {
        $("#"+modal_file).click();
    }
                    var jcrop_api;
                    function fileSelectHandler() {
                        var reqwidth = 1280;
                        var reqheight = 720;
                        var aspectRatio = reqwidth / reqheight;
                        clearInfo();
                        //$("#avataredit_preview").hide();
                        //$("#avatar_preview_div").removeClass("hide");
                        // get selected file
                        var oFile = $('#avatarInput')[0].files[0];
                        // hide all errors
                        // check for image type (jpg and png are allowed)
                        var rFilter = /^(image\/jpeg|image\/png)$/i;
                        if (!rFilter.test(oFile.type)) {
                            swal('Please select a valid image file (jpg and png are allowed)');
                            $("#avatar_preview_div").html('');
                            $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                            $('button[type="submit"]').attr('disabled', 'disabled');
                            return;
                        }

                        // preview element
                        var oImage = document.getElementById('preview_content_img');

                        // prepare HTML5 FileReader
                        var oReader = new FileReader();
                        oReader.onload = function (e) {
                            $('#file_error').hide();
                            // e.target.result contains the DataURL which we can use as a source of the image
                            oImage.src = e.target.result;
                            //$('#contentImg').attr('src',e.target.result);
                            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

                            oImage.onload = function () { // onload event handler
                                var w = oImage.naturalWidth;
                                var h = oImage.naturalHeight;
                                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                                console.log("Width=" + w + " Height=" + h);
                                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                                    $("#avatar_preview_div").html('');
                                    $("#avatar_preview_div").html('<img id="preview_content_img"/>');
                                    $('button[type="submit"]').attr('disabled', 'disabled');
                                    return;
                                }

                                // destroy Jcrop if it is existed
                                if (typeof jcrop_api != 'undefined') {
                                    jcrop_api.destroy();
                                    jcrop_api = null;
                                }
                                $('#preview_content_img').width(oImage.naturalWidth);
                                $('#preview_content_img').height(oImage.naturalHeight);
                                //setTimeout(function(){
                                // initialize Jcrop
                                $('#preview_content_img').Jcrop({
                                    minSize: [reqwidth, reqheight], // min crop size,
                                    //maxSize: [reqwidth, reqheight],
                                    boxWidth: 300,
                                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                                    bgFade: true, // use fade effect
                                    bgOpacity: .3, // fade opacity
                                    onChange: updateInfo,
                                    onSelect: updateInfo,
                                    onRelease: clearInfo
                                }, function () {

                                    // use the Jcrop API to get the real image size
                                    bounds = this.getBounds();
                                    boundx = bounds[0];
                                    boundy = bounds[1];

                                    // Store the Jcrop API in the jcrop_api variable
                                    jcrop_api = this;
                                    //jcrop_api.animateTo([10, 10, 290, 410]);
                                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                                });
                                //},100);

                            };
                        };

                        // read selected file as DataURL
                        oReader.readAsDataURL(oFile);
                    }
</script>

<script>
    var jcrop_api;
    function fileSelectHandlerGen() { 
        var reqwidth = 512;
        var reqheight = 512;
        var aspectRatio = reqwidth / reqheight;
        clearInfoGen();
        var oFile = $('#avatarInputGen')[0].files[0];
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_gen").html('');
            $("#avatar_preview_div_gen").html('<img id="preview_content_img_gen"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_gen');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_gen").html('');
                    $("#avatar_preview_div_gen").html('<img id="preview_content_img_gen"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_gen').width(oImage.naturalWidth);
                $('#preview_content_img_gen').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_gen').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoGen,
                    onSelect: updateInfoGen,
                    onRelease: clearInfoGen
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<script>
    var jcrop_api;  
    function splashFileSelectHandler() {
        var reqwidth = 1920; //before 1242
        var reqheight = 1080; //before 2208
        var aspectRatio = reqwidth / reqheight;
        clearInfoSplash();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarSplashInput')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_splash").html('');
            $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_splash');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_splash").html('');
                    $("#avatar_preview_div_splash").html('<img class="jcrop-preview" id="preview_content_img_splash"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_splash').width(oImage.naturalWidth);
                $('#preview_content_img_splash').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_splash').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                   // maxSize: [reqwidth, reqheight],
                   
                    boxWidth: 300,
                    
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoSplash,
                    onSelect: updateInfoSplash,
                    onRelease: clearInfoSplash
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>

<script>
    var jcrop_api;
    function TransparentfileSelectHandler() {
        var reqwidth = 512;
        var reqheight = 512;
        var aspectRatio = reqwidth / reqheight;
        clearInfoTransparent();
        //$("#avataredit_preview").hide();
        //$("#avatar_preview_div").removeClass("hide");
        // get selected file
        var oFile = $('#avatarInputTransparent')[0].files[0];
        // hide all errors
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            swal('Please select a valid image file (jpg and png are allowed)');
            $("#avatar_preview_div_transparent").html('');
            $("#avatar_preview_div_transparent").html('<img id="preview_content_img_transparent"/>');
            $('button[type="submit"]').attr('disabled', 'disabled');
            return;
        }

        // preview element
        var oImage = document.getElementById('preview_content_img_transparent');

        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('#file_error').hide();
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            //$('#contentImg').attr('src',e.target.result);
            //$('#contentImgDiv').css({'width':reqwidth,'height':reqheight,'overflow':'hidden'});

            oImage.onload = function () { // onload event handler
                var w = oImage.naturalWidth;
                var h = oImage.naturalHeight;
                //$('#contentImgDiv').css({'width':w,'height':h,'overflow':'hidden'});
                console.log("Width=" + w + " Height=" + h);
                if (parseInt(w) < reqwidth || parseInt(h) < reqheight) {
                    swal('Please upload a image of Minimum dimension ' + reqwidth + ' X ' + reqheight);
                    $("#avatar_preview_div_transparent").html('');
                    $("#avatar_preview_div_transparent").html('<img id="preview_content_img_transparent"/>');
                    $('button[type="submit"]').attr('disabled', 'disabled');
                    return;
                }

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                }
                $('#preview_content_img_transparent').width(oImage.naturalWidth);
                $('#preview_content_img_transparent').height(oImage.naturalHeight);
                //setTimeout(function(){
                // initialize Jcrop
                $('#preview_content_img_transparent').Jcrop({
                    minSize: [reqwidth, reqheight], // min crop size,
                    //maxSize: [reqwidth, reqheight],
                    boxWidth: 300,
                    aspectRatio: aspectRatio, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfoTransparent,
                    onSelect: updateInfoTransparent,
                    onRelease: clearInfoTransparent
                }, function () {

                    // use the Jcrop API to get the real image size
                    bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    // Store the Jcrop API in the jcrop_api variable
                    jcrop_api = this;
                    //jcrop_api.animateTo([10, 10, 290, 410]);
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
                //},100);

            };
        };

        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
</script>
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:20%;display: none;}
    .box.box-primary{border:none !important;}
    /*button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }*/
</style>