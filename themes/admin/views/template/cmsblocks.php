<div class="row m-b-40">
    <div class="col-xs-12">
        <a class="btn btn-primary m-t-10" href="<?php echo Yii::app()->getBaseUrl(true) ?>/template/cmsblock">Add New Block</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="Block">

            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-docs icon left-icon "></em>
                </div>
                <h4>Widgets</h4>
            </div>
            <hr>
            <div class="Block-Body">
                <?php
                if(count($blocks) > 0){
                ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>S/L#</th>
                            <th>Widget Title</th>
                            <th>Widget Code</th>
                            <th>Content</th>
                            <th class="width">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($blocks as $block) {
                            if($block['parent_id'] > 0){
                                $block['id'] = $block['parent_id'];
                            }
                            //$html_removed_text = Yii::app()->common->strip_blog_tags($block['content']);
                            $html_removed_text = $block['content'];
                            $html_removed_text = Yii::app()->general->formattedWords($html_removed_text, 200); 
                            if (strlen($html_removed_text) > 200)
                                $html_removed_text.= ' &hellip;';                              
                            ?> 
                            <tr class="" id="<?php echo $block['id']; ?>">
                                <td><?php echo $i++ ?></td>
                                <td><?php echo $block['title']; ?></td>
                                <td><?php echo $block['code']; ?></td>
                                <td><?php echo $html_removed_text;?></td>
                                <td>
                                    <h5><a href="<?php echo Yii::app()->getBaseUrl(true) ?>/template/cmsblock/<?php echo $block['id'] ?>"><em class="icon-pencil"></em> &nbsp;&nbsp;Edit</a></h5>                                    
                                    <?php if ($language_id == 20) { ?>
                                    <h5><a href="#" onclick="openDeleteBlockpopup('<?php echo $block['id'] ?>'); return false;"><em class="icon-trash"></em>&nbsp;&nbsp; Remove</a></h5>
                                    <?php }?>
                                </td>
                            </tr>        

                            <?php
                        }
                        ?>

                    </tbody>
                </table>
                <?php
                }else{
                ?>
                <p class="error">No records found.</p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>


<!--Delete Block-->
<div id="BlockDeletePopup" class="modal fade form-horizontal" role="dialog" aria-hidden="true" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog" style="">
        <form action="<?php echo $this->createUrl('template/cmsblock'); ?>" method="post" id="delete_block">
            <input type="hidden" id="id" name="id" value="" />
            <input type="hidden" id="option" name="option" value="delete" />
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="block_title">Delete Footerlinks?</h4>
                </div>
                <div class="modal-body" id="popup_block_content">              
                    Do you really want to delete this block?
                </div>              

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Yes</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>	
    </div>
</div>



<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>

<!-- Add New Widget -->
<script type="text/javascript">
function openDeleteBlockpopup(block) {
    swal({
        title: "Delete Block?",
        text: "Do you really want to delete this block?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        $('#id').val(block);
        $('#delete_block').submit();
    });
}

</script>   
