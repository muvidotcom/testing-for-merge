<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/application.css?v=45">
<div class="row m-t-40 m-b-40">   
    <div class="col-md-12">
        <div class="Block">      
            <div class="holderjs" id="holder1"></div>  
            <!--div class="row">
                <div class="col-md-12">
                    <form name="frmCAT" id="frmCAT" method="post" action="<?php echo Yii::app()->getBaseUrl(); ?>/template/menu">
                        <input type="hidden" name="action" value="cat_action" />
                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="add_cat_auto" name="add_cat_auto" id="add_cat_auto" <?php echo ($topmenu->auto_categories == '1') ? ' checked="checked"' : ''; ?> />
                                <i class="input-helper"></i>Create Menu from Content Categories
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" id="auto_category" type="submit">Save</button>
                        </div>
                    </form>
                </div>   
            </div-->
            <div class="row">
                <div class="col-md-4">
                    <h3 class="f-300 m-t-0 m-b-20">Set the Main Menu</h3>
                    <div class="panel-group" id="res-left" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_category">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_category" aria-expanded="false" aria-controls="collapse_category">Content Categories</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_category" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_category">
                                <div class="panel-body">
                                    <form name="FRM_MN" method="post" id="FRM_MN">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="0" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <?php
                                        if (count($contentCategories) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($contentCategories as $category) {
                                                    if(@IS_LANGUAGE == 1){
                                                    ?>    
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="category-<?php echo $category['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $category['id']; ?>" name="category-values[]" id="category-<?php echo $category['id']; ?>" <?php echo $category['language_id']==$this->language_id && $category['parent_id'] == 0?"":"disabled"; ?> />
                                                                <i class="input-helper"></i><?php echo $category['category_name']; ?>
                                                            </label>
                                                            <input type="hidden" name="language_id" value="<?php echo $category['language_id'] ?>" />
                                                        </div>
                                                    </li>
                                                    <?php
                                                    }else{ ?>
                                                        <li>
                                                            <div class="checkbox">
                                                                <label for="category-<?php echo $category['id']; ?>">
                                                                    <input type="checkbox" value="<?php echo $category['id']; ?>" name="category-values[]" id="category-<?php echo $category['id']; ?>" />
                                                                    <i class="input-helper"></i><?php echo $category['category_name']; ?>
                                                                </label>
                                                            </div>
                                                        </li>
                                                   <?php } 
                                                }
                                                ?>
                                                    <li class="button-controls">
                                                        <span class="add-to-menu">
                                                            <input type="button" id="add-category" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                            <span class="spinner"></span>
                                                        </span>
                                                    </li>       
                                            </ul>
                                        <?php } ?>                                    
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--Start CMS pages for menu-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_page">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_page" aria-expanded="false" aria-controls="collapse_page">Static Pages</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_page" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_page">
                                <div class="panel-body">
                                    <form id="frmpg" name="frmpg" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="1" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />    
                                        <?php
                                        if (count($pages) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($pages as $page) {
                                                    if(@IS_LANGUAGE == 1){
                                                    ?>    
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="pages-<?php echo $page['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $page['id']; ?>" name="pages[]" id="pages-<?php echo $page['id']; ?>" <?php echo $page['language_id']==$this->language_id && $page['parent_id'] == 0?"":"disabled"; ?> />
                                                                <i class="input-helper"></i><?php echo $page['title']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <input type="hidden" name="language_id" value="<?php echo $page['language_id'] ?>" />
                                                    <?php
                                                    }else{ ?>
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="pages-<?php echo $page['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $page['id']; ?>" name="pages[]" id="pages-<?php echo $page['id']; ?>" /> 
                                                                <i class="input-helper"></i><?php echo $page['title']; ?>
                                                            </label>
                                                        </div>
                                                    </li>

                                                 <?php  }
                                                }
                                                ?>
                                                    <li class="button-controls">
                                                        <span class="add-to-menu">
                                                            <input type="button" id="add-page" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                            <span class="spinner"></span>
                                                        </span>
                                                    </li>
                                            </ul>
                                        <?php } ?>                                          
                                    </form>
                                </div>
                            </div>
                        </div>                      

                        <!--Start Extensions/Apps-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_apps">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_apps" aria-expanded="false" aria-controls="collapse_apps">Apps</a>
                                </h4>
                            </div>                                
                            <div id="collapse_apps" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_apps">
                                <div class="panel-body">
                                    <form id="frm_APPS" name="frm_APPS" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="3" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />

                                        <?php
                                        if (count($extensions) > 0) {
                                            ?>
                                            <ul>
                                                <?php
                                                foreach ($extensions as $extension) {
                                                    ?>    
                                                    <li>
                                                        <div class="checkbox">
                                                            <label for="extensions-<?php echo $extension['id']; ?>">
                                                                <input type="checkbox" value="<?php echo $extension['id']; ?>" name="extensions[]" id="extensions-<?php echo $extension['id']; ?>" /> 
                                                                <i class="input-helper"></i><?php echo $extension['name']; ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                                    <li class="button-controls">
                                                        <span class="add-to-menu">
                                                            <input type="button" id="add-app" class="btn btn-primary" name="add-post-type-menu-item" value="Add to Menu" />
                                                            <span class="spinner"></span>
                                                        </span>
                                                    </li> 
                                            </ul>
                                        <?php } ?>                                         
                                    </form>
                                </div>
                            </div>
                        </div>                          

                        <!--Start External Links-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_external">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#res-left" href="#collapse_external" aria-expanded="false" aria-controls="collapse_external">External Links</a>                                
                                </h4>
                            </div>                                
                            <div id="collapse_external" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_external">
                                <div class="panel-body">
                                    <form id="frm_EXT" name="frm_EXT" method="post">
                                        <input type="hidden" name="option" value="add_menu_item" />
                                        <input type="hidden" name="item_type" value="2" />
                                        <input type="hidden" name="menu_id" value="<?php echo $menu_id ?>" />
                                        <div class="form-group">
                                            <label class="control-label">Link Text</label>
                                            <div class="fg-line"><input type="text" name="menu_title" class="form-control input-sm" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">URL</label>
                                            <div class="fg-line"><input type="text" name="menu_permalink" class="form-control input-sm" /></div>
                                        </div>     
                                        <input type="submit" class="btn btn-primary" name="sbt" value="Submit" />
                                    </form>
                                </div>
                            </div>
                        </div>                    

                    </div>                        

                </div>
                <div class="col-md-offset-4 col-md-4" id="serialization">
                    <h3 class="f-300 m-t-0 m-b-20">Menu Structure</h3>
                    <ol class="serialization vertical p-l-0" role="tablist" id="res-accordion" aria-multiselectable="true">
                        <?php
                        if (count($topmenuitems) > 0) {

                            foreach ($topmenuitems as $menuitem) {
                                $parent_menu_item_id = $menuitem['id'];
                                $item_type = $menuitem['link_type'];
                                $value = $menuitem['value'];
                                $menu_title = $menuitem['title'];
                                $menu_permalink = $menuitem['permalink'];
                                $menu_language = $menuitem['language_id'];
                                $menu_parent_language = $menuitem['language_parent_id'];
                                if($menu_parent_language > 0){
                                    $parent_menu_item_id = $menu_parent_language;
                                }
                                echo $menuform = Yii::app()->general->menuItemForm($this->studio->id, $item_type, $menu_id, $parent_menu_item_id, $value, $menu_title, $menu_permalink, $menu_parent_language,$menu_language,$this->language_id);
                                        $topmenus = MenuItem::model()->findAll(array(
                                    'condition' => 'studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND (language_id=:language_id OR language_parent_id=0 AND id NOT IN (SELECT language_parent_id FROM menu_items WHERE studio_id=:studio_id AND menu_id=:menu_id AND parent_id=:parent_id AND language_id=:language_id))',
                                    'params' => array(':studio_id' => $this->studio->id, ':menu_id' => $menu_id, ':parent_id' => $parent_menu_item_id,':language_id' =>$this->language_id),
                                            'order' => 'id_seq ASC'
                                        ));
                                 echo '<ol>';
                                if (count($topmenus) > 0)
                                {
                                    foreach ($topmenus as $topmenuitem) {
                                        $menu_item_id = $topmenuitem['id'];
                                        $item_type = $topmenuitem['link_type'];
                                        $value = $topmenuitem['value'];
                                        $menu_title = $topmenuitem['title'];
                                        $menu_permalink = $topmenuitem['permalink'];
                                            $menu_language = $topmenuitem['language_id'];
                                            $menu_parent_language = $topmenuitem['language_parent_id'];
                                        if($menu_parent_language > 0){
                                            $menu_item_id = $menu_parent_language;
                                        }
                                            echo $menuform = Yii::app()->general->menuItemForm($this->studio->id, $item_type, $menu_id, $menu_item_id, $value, $menu_title, $menu_permalink, $menu_parent_language,$menu_language,$this->language_id);
                                         echo '</li>';
                                    }
                                }
                               echo '</ol></li>';
                            }
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery.validator.addMethod("external_url", function(value, element) {
        var reg = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
        return value == '#' || reg.test( value );
    }, "Please enter correct url");  
    $(document).ready(function () {        
        $("#frm_EXT").validate({
            rules: {
                menu_title: {
                    required: true,
                    minlength: 2
                },
                menu_permalink: {
                    required: true,
                    external_url: true
                }
            },
            messages: {
                menu_title: {
                    required: "Please enter menu text",
                    minlength: "Menu text should be more than 1"
                },
                menu_permalink: {
                    required: "Please enter menu link",
                    external_url: "Please enter a valid url"
                }
            },
            submitHandler: function (form) {
                var url = "<?php echo Yii::app()->getBaseUrl(); ?>/template/menuitem";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#frm_EXT").serialize(),
                    dataType: "json"
                }).done(function (result) {
                    if (result.action == "success") {
                        window.location.reload();
                    } else {
                        $("#err_message").html(result.message)
                    }
                });
            }
        });
        $(".menu-item-save").click(function () {  
            var frm = $(this).closest("form");
            $(frm).validate({
                rules: {
                    menu_title: {
                        required: true,
                        minlength: 2
                    },
                    menu_permalink: {
                        required: true,
                        external_url: true
                    }
                },
                messages: {
                    menu_title: {
                        required: "Please enter menu text",
                        minlength: "Menu text should be more than 1"
                    },
                    menu_permalink: {
                        required: "Please enter a valid url",
                        external_url: "Please enter a valid url"
                    }
                },
                submitHandler: function (form) {
                    var url = "<?php echo Yii::app()->getBaseUrl(); ?>/template/menuitem";
                    $.ajax({
                        url: url,
                        data: frm.serialize(),
                        dataType: "json",
                        method: "post",
                        success: function (result) {
                            if (result.action == "success") {
                                window.location.reload();
                            } else {
                                $("#err_message").html(result.message)
                            }
                        }
                    });
                }
            });            
                                  
        });
        $(".menu-item-remove").click(function () {
            var item_id = $(this).attr("data-id");
            swal({
                title: "Delete Menu Item",
                text: "Do you really want to delete?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html:true
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/menuitem",
                        data: {"item_id": item_id, "option": "delete_item"},
                        dataType: "json",
                        method: "post",
                        success: function (result) {
                            if (result.action == "success") {
                                window.location.reload();
                            } else {
                                $("#err_message").html(result.message)
                            }
                        }
                    });                 
                }
            });
        });
        $('#add-category').click(function () {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/menuitem",
                data: $('#FRM_MN').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });
        $('#add-page').click(function () {
            console.log($('#frmpg').serialize());
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/menuitem",
                data: $('#frmpg').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });
        $('#add-app').click(function () {
            console.log($('#frm_APPS').serialize());
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/menuitem",
                data: $('#frm_APPS').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    if (result.action == 'success') {
                        window.location.reload();
                    } else {
                        $('#err_message').html(result.message)
                    }
                }
            });
        });

        $('#ext_submit').click(function () {
            $("#frm_EXT").validate({
                rules: {
                    ex_menu_title: {
                        required: true,
                        minlength: 2
                    },
                    ex_menu_permalink: {
                        required: true,
                        url: true
                    }
                },
                messages: {
                    ex_menu_title: {
                        required: "Please enter menu text",
                        minlength: "Menu text should be more than 1"
                    },
                    ex_menu_permalink: {
                        required: "Please enter menu link",
                        url: "Please enter a valid url"
                    }
                },
            });
        });
    });
</script> 
<style>
    ol{list-style: none;}
    ol#res-accordion{padding-left: 0px;}
    .panel-body ul{list-style: none;}
    .panel{padding:0px;}
    #res-accordion li{margin-bottom: 5px;}
    h4{position:relative;
    }
    h4 a:first-child{ 
        display:block;
         padding-right: 20px;
         text-overflow:ellipsis;
         overflow:hidden;
         max-width: 222px;
    }
    h4 a.pull-right{
        position: absolute;
        right:0;
        bottom:0;
    }
</style>    

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-sortable.js"></script>
<script type="text/javascript">
    $(function() {
    var group = $("ol.serialization").sortable({
        group: 'serialization',
        delay: 500,
        onDrop: function($item, container, _super) {
            var data = group.sortable("serialize").get();

            var jsonString = JSON.stringify(data, null, ' ');
            //console.log(jsonString);
           // $('#serialize_output2').text(jsonString);
            _super($item, container);
            var data = {jsonString:jsonString};
             $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/template/sortmenu/",
                data: data,
                dataType: 'json',
                async: false,
                method: 'post',
                success: function (result) {
                    console.log(result);
                }
            });            
            
        }
    });
});


</script>
