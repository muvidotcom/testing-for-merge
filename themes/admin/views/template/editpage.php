<div class="row m-t-40">
    <div class="col-md-12">
        <form action="<?php echo $this->createUrl('template/addpage'); ?>" class="form-horizontal" method="post" id="add_page">
            <input type="hidden" name="page_id" id="page_id" value="<?php echo $page['id']; ?>" />
            <input type="hidden" name="permalink" value="<?php echo $permalink ?>" />
            <input type="hidden" name="language_id" value="<?php echo $page['language_id']; ?>" />
            <div class="form-group">
                <label class="control-label col-sm-2">Page Name:</label>                    
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input required type="text" id="page_name" class="form-control input-sm" name="page_name" placeholder="Enter a Page Name" value="<?php echo Yii::app()->common->htmlchars_encode_to_html($page['title']); ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="minimal" value="external" name="ltype" id="ltype" <?php echo ($page['link_type'] == 'external') ? 'checked' : '' ?>  /><i class="input-helper"></i> External Link
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group" id="internal">
                <label for="page_name" class="control-label col-sm-2">Content:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <textarea id="page_content" class="form-control input-sm" name="page_content" aria-hidden="true"><?php echo (isset($page['content'])) ? $page['content'] : ''; ?></textarea>
                    </div>
                </div>
            </div>  
            <div class="form-group" id="external">
                <label for="page_name" class="control-label col-sm-2">Page Link:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="external_url" id="external_url" placeholder="Enter external page link" value="<?php echo html_entity_decode($page['external_url']); ?>" />
                    </div>
                </div>
            </div>                     

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10 m-t-30">
                    <button type="submit" class="btn btn-primary btn-sm">Save Page</button>
                    <button type="button" onclick="window.location = '/template/footerlinks'" class="btn btn-default btn-sm">Cancel</button>
                </div>

            </div>

        </form>	
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
<?php
if ($page['link_type'] == 'external') {
    ?>
            $('#internal').hide();
<?php } else { ?>
            $('#external').hide();
<?php } ?>
    });

    $('#ltype').change(function() {
        if ($(this).is(":checked"))
        {
            $('#internal').hide();
            $('#external').show();
        } else {
            $('#external').hide();
            $('#internal').show();
        }
    });
</script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        tinymce.init({
            selector: "#page_content",
            formats: {
                bold: {inline: 'b'},  
            },
            valid_elements : '*[*]',
            force_br_newlines : true,
            force_p_newlines : false,
            forced_root_block : false,
            menubar: false,
            element_format: 'html',
            extended_valid_elements: 'div[*], style[*]',
            valid_children: "+body[style]",
            height: 300,
            plugins: [
                'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'save table contextmenu directionality emoticons template paste textcolor'
            ],
            setup: function(editor) {
                editor.addButton('newmedia', {
                    text: 'Add Image',
                    title: 'Add image',
                    icon: 'image',
                    onclick: function() {
                        $("#MediaModal").modal("show");
                        $('#glry_preview').removeAttr('src width height');
                        $('#imgtolib').removeAttr('src width height');
                        $('#choose_img_name').val("");
                        $("#InsertPhoto").text("Insert Image");
                        $("#InsertPhoto").removeAttr("disabled");
                        $("#cancelPhoto").removeAttr("disabled");
                        $('.overlay').removeAttr('style');
                        $("#browsefiledetails").text("No file selected");
                        $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true) ?>/template/tinyGallery");
                    }});
            },
            content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | media code",
        });
    });
</script>
<script type="text/javascript">
    $('#frm_loader').hide();
    $('#view_loader').hide();

    $(function() {
        var validator = $("#add_page").submit(function() {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            ignore: "",
            rules: {
                page_content: {
                    required: function(element) {
                        if ($('#ltype').is(":checked"))
                            return false;
                        else
                            return true;
                    }
                },
                external_url: {
                    required: function(element) {
                        if ($('#ltype').is(":checked"))
                            return true;
                        else
                            return false;
                    },
                    url: function(element) {
                        if ($('#ltype').is(":checked"))
                            return true;
                        else
                            return false;
                    }
                },
            },
            messages: {
                page_content: {
                    required: 'Please enter page content'
                },
                external_url: {
                    required: 'Please enter external page Link'
                },
            },
            errorPlacement: function(label, element) {
                // position error label after generated textarea 
                label.addClass('red');
                label.insertAfter(element.parent());

            },
        });
        validator.focusInvalid = function() {

            // put focus on tinymce on submit validation
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    console.log("Called" + toFocus.is("textarea"));
                    if (toFocus.is("textarea")) {
                        console.log("This is field");
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
    });
</script>