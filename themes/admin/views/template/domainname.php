<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<div class="row m-t-40">
    <div class="col-md-8 col-sm-12">
                    <div class="Block">
    
            <form class="form-horizontal domainsave" <?php if ((isset($studio->is_subscribed) && $studio->is_subscribed == 1) || ($studio->is_default == 1)) { ?>method="post" name="domainsave" id="domainsave"<?php } ?>>
                <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>
                    
                    <div class="form-group">
                        <div class="col-md-8 col-sm-8 red">
                            A paid subscription is required to set domain name. Please <a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/domain/1');">Purchase subscription</a> to proceed.
                        </div>
                    </div>
                <?php } ?>
                <div class="block-body">
                    <div class="form-group">
                                <label for="inputPassword3" class="col-md-4 control-label">Your domain name:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="domain" id="domain" value="<?php echo $this->studio->domain ?>" placeholder="yourdomain.com" <?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled="disabled" style=""<?php } ?> />
                                    </div>
                                </div>
                            </div>
                  
                    <div class="form-group m-t-30">
                                <div class="col-md-offset-4 col-md-8">
                                   
                                 <button type="submit" class="btn btn-primary waves-effect btn-sm"<?php if (isset($studio->is_subscribed) && $studio->is_subscribed == 0 && $studio->is_default == 0) { ?>disabled="disabled"<?php } ?>>Update</button>
                                </div>
                            </div>
                    
                    <div class="form-group m-t-40">
                        <h5>Note*: Change DNS setting of your domain name to point to IP address <?= HOST_IP;?></h5>
                    </div>
                    <div class="form-group">
                        <div id="domain_error" class="error"></div>
                    </div>
                </div>
            </form>
            <script type="text/javascript">
                $(document).ready(function () {
                    jQuery.validator.addMethod("domn", function (value, element, param) {
                        value = value.replace('http://', '');
                        value = value.replace('www.', '');
                        if (this.optional(element) || /^(w{3})?[a-z0-9]+([\-\.]{1}[a-z0-9]+){0,3}$/i.test(value)) {
                            var urls = value.split(/[\s.]+/);
                            console.log(urls + ' and ' + urls.length);
                            if (urls.length > 1) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }, jQuery.validator.messages.url);
                });
                //form validation rules
                $("#domainsave").validate({
                    rules: {
                        domain: {
                            required: true,
                            minlength: 3,
                            domn: true,
                        }
                    },
                    messages: {
                        domain: {
                            required: "Please provide a domain name",
                            minlength: "Your domain name must be at least 3 characters long",
                            domn: 'Please enter a valid domain url',
                        }
                    },
                    submitHandler: function (form) {
                        var url = "<?php echo Yii::app()->getBaseUrl(); ?>/template/savedomain";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $('#domainsave').serialize(),
                            dataType: 'json'
                        }).done(function (data) {
                            if (data.err == 1)
                            {
                                $('#domain_error').html(data.msg);
                                return false;
                            }
                            else
                            {
                                window.location = '<?php echo Yii::app()->getBaseUrl(true) ?>/template/domainname';
                            }
                        });
                    }
                });
                function openinmodal(url) {
                    //$('.loaderDiv').show();   
                    $.get(url, {"modalflag": 1}, function (data) {
                        //$('.loaderDiv').hide();
                        $('#mymodaldiv').html(data);
                        $("#mymodal").modal('show');
                    });
                }
            </script>  
        </div>
    </div>
</div>
<!-- Modal Starts Here -->
<div  class="loaderDiv">
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.gif" />
</div>
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    canvas{margin:0px !important;}
    .loaderDiv{position: absolute;left: 45%;top:30%;display: none;}
    .form-horizontal .form-group{margin-left:0px; margin-right: 0px;}
    .box.box-primary{border:none !important;}
    form .domainsave .form-group label{padding:0px;}
    button.close{
        border-radius: 15px 15px 15px 15px;
        -moz-border-radius: 15px 15px 15px 15px;
        -webkit-border-radius: 15px 15px 15px 15px;
        width: 25px;
        height: 25px;
        margin: 5px;
        border: 2px solid #000;
    }
</style>  

<!--Start Coming Soon-->
<form class="form-horizontal" id="coming_form" method="post" action="javascript:void(0);" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12">
            <div class="block">
                <div class="block-header">
                    <h3>Coming Soon Page</h3>
                </div>
                <div class="block-body">                
                    <label class="checkbox checkbox-inline m-r-20">
                        <input type="checkbox" value="1" name="enable_comingsoon" <?php echo ($enable_comingsoon == true) ? 'checked="checked"' : ''; ?>>
                        <i class="input-helper"></i>
                        Enable Coming Soon Page
                    </label>
                    <div id="coming_block">
                        <div class="col-xs-12">
                            <div class="radio m-b-20">
                                <label>
                                    <input type="radio" value="1" name="coming_option" <?php echo ($coming_option == 1) ? 'checked="checked"' : ''; ?>>
                                    <i class="input-helper"></i>
                                    Coming Soon Page (<i>Announce to the world that you will be launching soon and collect email addresses</i>)
                                </label>
                            </div>   
                            <div class="radio m-b-20">
                                <label>
                                    <input type="radio" value="2" name="coming_option" <?php echo ($coming_option == 2) ? 'checked="checked"' : ''; ?>>
                                    <i class="input-helper"></i>
                                    Passcode Page (<i>Have a passcode for users to enter before they see your website</i>)
                                </label>
                            </div>                         
                        </div>
                        <div class="all_form_area">
                            <div id="comingsoon_section">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3">Message</label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Website releasing soon. Please add your email to get notified." class="form-control input-sm" name="soon_message" value="<?php echo (isset($settings))?$settings->comingsoon_text:'';?>">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div id="passcode_section">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail3">Passcode</label>
                                    <div class="col-sm-10">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Unique Passcode" class="form-control input-sm" name="passcode" value="<?php echo (isset($settings))?$settings->passcode:'';?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail3">Logo</label>
                                <div class="col-sm-10">
                                    <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm" data-name="Logo" data-width="<?php echo $logo_width;?>" data-height="<?php echo $logo_height;?>" onclick="openImageModal(this)">                                    
                                    <br />Recommended size is <?php echo $logo_width;?>x<?php echo $logo_height;?> pixels
                                    <div class="poster-cls  avatar-view jcrop-thumb">
                                        <div id="avatar_preview_div">
                                            <?php if($logo_path != ''){?>
                                            <img class="img-responsive" src="<?php echo $logo_path;?>" />
                                            <?php }else{?>
                                            <img class="img-responsive" data-src="holder.js/<?php echo $logo_width;?>x<?php echo $logo_height;?>" alt="<?php echo $logo_width;?>x<?php echo $logo_height;?>" />                                            
                                            <?php }?>                                         
                                        </div>
                                    </div>
                                    <canvas id="previewcanvas" style="overflow:hidden;display: none;margin-left: 10px;"></canvas>
                                </div>
                            </div>                         
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail3">Background</label>
                                <div class="col-sm-10">
                                    <input type="button" value="Upload File" class="btn btn-default-with-bg btn-file btn-sm" data-name="Background" data-width="<?php echo $bg_width;?>" data-height="<?php echo $bg_height;?>" onclick="bg_openImageModal(this)">                                    
                                    <br />Recommended size is <?php echo $bg_width;?>x<?php echo $bg_height;?> pixels
                                    <div class="poster-cls avatar-view jcrop-thumb">
                                        <div id="bg_avatar_preview_div">
                                            <?php if($background_path != ''){?>
                                            <img class="img-responsive" src="<?php echo $background_path;?>" />
                                            <?php }else{?>
                                            <img class="img-responsive" data-src="holder.js/<?php echo $bg_width;?>x<?php echo $bg_height;?>" alt="<?php echo $bg_width;?>x<?php echo $bg_height;?>" />                                            
                                            <?php }?>                                         
                                        </div>
                                    </div>
                                    <canvas id="bg_previewcanvas" style="overflow:hidden;display: none;margin-left: 10px; max-width: 800px;"></canvas>
                                </div>
                            </div> 
                        </div>                    
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Update" class="btn btn-primary btn-default m-t-10" />
                    </div>                    

                </div>
            </div>
        </div>
    </div>

    <div class="modal is-Large-Modal fade" id="LogoModal" tabindex="-1" role="dialog" aria-labelledby="LogoModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="section_id1" id="section_id1" value="" />
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" onclick="hide_file()">
                                <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                            </li>
                            <li role="presentation" onclick="hide_gallery()"> 
                                <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                        <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('coming_logo_file')">
                                        <input id="coming_logo_file" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                        <p class="help-block"></p>
                                    </div>
                                    <input type="hidden" class="x1" name="fileimage[x1]" />
                                    <input type="hidden" class="y1" name="fileimage[y1]" />
                                    <input type="hidden" class="x2" name="fileimage[x2]" />
                                    <input type="hidden" class="y2" name="fileimage[y2]" />
                                    <input type="hidden" class="w" name="fileimage[w]"/>
                                    <input type="hidden" class="h" name="fileimage[h]"/>
                                    <div class="col-xs-12">
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="upload_preview">
                                                <img id="preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                                <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                                <input type="hidden" name="g_original_image" id="g_original_image" />
                                <input type="hidden" class="x1" name="jcrop_allimage[x13]" />
                                <input type="hidden" class="y1" name="jcrop_allimage[y13]" />
                                <input type="hidden" class="x2" name="jcrop_allimage[x23]" />
                                <input type="hidden" class="y2" name="jcrop_allimage[y23]" />
                                <input type="hidden" class="w" name="jcrop_allimage[w3]" />
                                <input type="hidden" class="h" name="jcrop_allimage[h3]" />
                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">

                                    </div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                        <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                            <div class="preloader pls-blue  ">
                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="gallery_preview">
                                                <img id="glry_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="seepreview(this);">Next</button>
                </div>
            </div>
        </div>
    </div>    

    <div class="modal is-Large-Modal fade" id="bg_Modal" tabindex="-1" role="dialog" aria-labelledby="bg_Modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="bg_ModalLabel">Upload <span class="upload_detail">Image</span></h4>
                </div>
                <div class="modal-body">                    
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active" onclick="bg_hide_file()">
                                <a href="#bg_upload_by_browse" aria-controls="bg_upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                            </li>
                            <li role="presentation" onclick="bg_hide_gallery()"> 
                                <a href="#bg_upload_from_gallery" aria-controls="bg_Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="bg_upload_by_browse">
                                <div class="row is-Scrollable">
                                    <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                        <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="bg_click_browse('bg_coming_logo_file')">
                                        <input id="bg_coming_logo_file" name="bg_Filedata" type="file" onchange="bg_fileSelectHandler()" style="display:none;" />
                                        <p class="help-block"></p>
                                    </div>
                                    <input type="hidden" class="x1" name="bg_fileimage[x1]" />
                                    <input type="hidden" class="y1" name="bg_fileimage[y1]" />
                                    <input type="hidden" class="x2" name="bg_fileimage[x2]" />
                                    <input type="hidden" class="y2" name="bg_fileimage[y2]" />
                                    <input type="hidden" class="w" name="bg_fileimage[w]"/>
                                    <input type="hidden" class="h" name="bg_fileimage[h]"/>
                                    <div class="col-xs-12">
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="bg_upload_preview">
                                                <img id="bg_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="bg_upload_from_gallery">
                                <input type="hidden" name="bg_g_image_file_name" id="bg_g_image_file_name" />
                                <input type="hidden" name="bg_g_original_image" id="bg_g_original_image" />
                                <input type="hidden" class="x1" name="bg_jcrop_allimage[x13]" />
                                <input type="hidden" class="y1" name="bg_jcrop_allimage[y13]" />
                                <input type="hidden" class="x2" name="bg_jcrop_allimage[x23]" />
                                <input type="hidden" class="y2" name="bg_jcrop_allimage[y23]" />
                                <input type="hidden" class="w" name="bg_jcrop_allimage[w3]" />
                                <input type="hidden" class="h" name="bg_jcrop_allimage[h3]" />
                                <div class="row  Gallery-Row">
                                    <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="bg_all_img_glry"></div>
                                    <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                        <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                            <div class="preloader pls-blue  ">
                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="Preview-Block row">
                                            <div class="col-md-12 text-center" id="bg_gallery_preview">
                                                <img id="bg_glry_preview" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="bg_seepreview(this);">Next</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="bg_img_width" name="bg_img_width" value="<?php echo $bg_width;?>">
    <input type="hidden" id="bg_img_height" name="bg_img_height" value="<?php echo $bg_height;?>">     
    <input type="hidden" id="img_width" name="img_width" value="<?php echo $logo_width;?>">
    <input type="hidden" id="img_height" name="img_height" value="<?php echo $logo_height;?>">     
</form>

<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/common/js/placeholder/holder.js"></script>
<script type="text/javascript">
                    $(document).ready(function() {
<?php if ($coming_option == 1) { ?>
                            $('#comingsoon_section').show();
                            $('#passcode_section').hide();
<?php } else if ($coming_option == 2) { ?>
                            $('#comingsoon_section').hide();
                            $('#passcode_section').show();
<?php } if($enable_comingsoon == 0){?>
    $('#coming_block').hide(); 
<?php }?>
                        $('input[name="enable_comingsoon"]').change(function() {
                            if ($('input[name="enable_comingsoon"]').is(":checked")) {                                
                                $('#coming_block').show();                                  
                                if($('input[name="coming_option"]:checked').val() == undefined){
                                    document.getElementsByName("coming_option")[1].checked = true;
                                    $('#comingsoon_section').hide();
                                    $('#passcode_section').show();                                    
                                }
                            } else {
                                $('#coming_block').hide();
                            }
                        });
                        $('input[name="coming_option"]').change(function() {
                            if ($('input[name="coming_option"]:checked').val() == 1) {
                                $('#comingsoon_section').show();
                                $('#passcode_section').hide();
                            } else {
                                $('#comingsoon_section').hide();
                                $('#passcode_section').show();
                            }
                        });
                    });

$(document).ready(function() {
    $("#coming_form").validate({
        rules: {
            soon_message: {required: function(element) {
                    return ($('input[name="coming_option"]:checked').val() == 1);
                }
            },
            passcode: {required: function(element) {
                    return ($('input[name="coming_option"]:checked').val() == 2);
                }
            },
        },
        messages: {
            soon_message: "Please enter your message for Coming Soon page",
            passcode: "Please enter the Passcode for Coming Soon page",
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },
        // set this class to error-labels to indicate valid fields
        submitHandler: function(frm) {
            var path = '<?php echo Yii::app()->getBaseUrl(true) ?>/template/savecomingsoon';
            $("#coming_form").attr('action', path);
            $("#coming_form").submit();
        }
    });
});
function bg_openImageModal(obj) {
    var name = $(obj).attr('data-name');
    var width = $(obj).attr('data-width');
    var height = $(obj).attr('data-height');
    $(".upload_detail").html(name);
    $(".help-block").html("Upload a transparent image of size " + width + " x " + height+'px');
    $("#bg_img_width").val(width);
    $("#bg_img_height").val(height);
    var action = HTTP_ROOT + "/template/SaveComingLogo";

    $("#bg_upload_image_form").attr('action', action);
    $("#bg_all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#bg_Modal").modal('show');
}
function bg_click_browse(modal_file) {
    $("#" + modal_file).click();
}
function bg_hide_file(){
    $('#bg_glry_preview').css("display", "none");
    $('#bg_upload_preview').css("display", "none");
    $('#bg_preview').css("display", "none");
    document.getElementById('bg_coming_logo_file').value = null;
}
function bg_hide_gallery(){
    $('#bg_preview').css("display", "none");
    $("#bg_glry_preview").css("display", "block");
    $('#bg_gallery_preview').css("display", "none");
    $("#bg_g_image_file_name").val("");
    $("#bg_g_original_image").val("");
}
function bg_fileSelectHandler() {
    document.getElementById("bg_g_original_image").value = "";
    document.getElementById("bg_g_image_file_name").value = "";
    var img_width = $("#bg_img_width").val();
    var img_height = $("#bg_img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#bg_celeb_preview").removeClass("hide");
    $('#bg_uplad_buton').removeAttr('disabled');
    var oFile = $('#bg_coming_logo_file')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg and png are allowed)');
        $("#bg_celeb_preview").addClass("hide");
        document.getElementById("bg_coming_logo_file").value = "";
        $('#bg_uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function() {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#bg_celeb_preview").addClass("hide");
            document.getElementById("bg_coming_logo_file").value = "";
            $('#bg_uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = $('#bg_preview');
        var oReader = new FileReader();
        oReader.onload = function(e) {
            $('.error').hide();
            oImage.attr('src', e.target.result);
            oImage.load(function() { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#bg_preview').width(oImage.naturalWidth);
                    $('#bg_preview').height(oImage.naturalHeight);
                    $('#bg_glry_preview').width("450");
                    $('#bg_glry_preview').height("250");
                }
                $('#bg_preview').css("display", "block");
                $('#bg_celeb_preview').css("display", "block");
                $('#bg_preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    allowResize: false,
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: function(e){updateCropImageInfo(e, 'bg_upload_by_browse');},
                    onSelect: function(e){updateCropImageInfo(e, 'bg_upload_by_browse');},
                    onRelease: clearCropImageInfo('bg_upload_by_browse')
                }, function() {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            });
        };
        oReader.readAsDataURL(oFile);
    };
}
function bg_showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image){    
    if(($('#upload_from_gallery').hasClass('active') || $('#upload_by_browse').hasClass('active')) && ($('#LogoModal').hasClass('in'))){
        $('#glry_preview').css("display", "block");
        document.getElementById("coming_logo_file").value = "";
        showLoader();
        var img_width = $("#img_width").val();
        var img_height = $("#img_height").val();
        var aspectratio = img_width / img_height;
        var image_file_name = name_of_image;
        var image_src = img_src;
        clearInfo();
        $("#g_image_file_name").val(image_file_name);
        $("#g_original_image").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        var img = new Image();
        img.src = img_src;
        var width = 0;
        var height = 0;
        img.onload = function() {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < img_width || height < img_height) {
                showLoader(1);
                swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
                $("#celeb_preview").addClass("hide");
                $("#glry_preview").addClass("hide");
                $("#g_image_file_name").val("");
                $("#g_original_image").val("");
                $('#uplad_buton').attr('disabled', 'disabled');
                return;
            }
            var oImage = document.getElementById('glry_preview');
            showLoader(1)
            oImage.src = img_src;
            oImage.onload = function() {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview').width(oImage.naturalWidth);
                    $('#glry_preview').height(oImage.naturalHeight);
                    $('#preview').width("450");
                    $('#preview').height("250");
                }
                $("#glry_preview").css("display", "block");
                $('#gallery_preview').css("display", "block");
                $('#glry_preview').Jcrop({
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    minSize: [img_width, img_height], // min crop size
                    maxSize: [img_width, img_height], // min crop size
                    allowResize: false,
                    allowMove: true,                     
                    aspectRatio: aspectratio, // keep aspect ratio 1:1 
                    onChange: function(e){updateCropImageInfo(e, 'upload_from_gallery');},
                    onSelect: function(e){updateCropImageInfo(e, 'upload_from_gallery');},
                    onRelease: clearCropImageInfo('upload_from_gallery')
                }, function() {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            };
        };        
    }    
    if(($('#bg_upload_from_gallery').hasClass('active') || $('#bg_upload_by_browse').hasClass('active')) && ($('#bg_Modal').hasClass('in'))){
        console.log("Hello");
        $('#bg_glry_preview').css("display", "block");
        document.getElementById("bg_coming_logo_file").value = "";
        bg_showLoader();
        var img_width = $("#bg_img_width").val();
        var img_height = $("#bg_img_height").val();
        var aspectratio = img_width / img_height;
        var image_file_name = name_of_image;
        clearCropImageInfo('bg_upload_by_browse');
        $("#bg_g_image_file_name").val(image_file_name);
        $("#bg_g_original_image").val(img_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        var img = new Image();
        img.src = img_src;
        var width = 0;
        var height = 0;
        img.onload = function() {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < img_width || height < img_height) {
                bg_showLoader(1);
                swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
                $("#bg_celeb_preview").addClass("hide");
                $("#bg_glry_preview").addClass("hide");
                $("#bg_g_image_file_name").val("");
                $("#bg_g_original_image").val("");
                $('#bg_uplad_buton').attr('disabled', 'disabled');
                return;
            }
            var oImage = document.getElementById('bg_glry_preview');
            bg_showLoader(1)
            oImage.src = img_src;
            oImage.onload = function() {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#bg_glry_preview').width(oImage.naturalWidth);
                    $('#bg_glry_preview').height(oImage.naturalHeight);
                    $('#bg_preview').width("450");
                    $('#bg_preview').height("250");
                }
                $("#bg_glry_preview").css("display", "block");
                $('#bg_gallery_preview').css("display", "block");
                $('#bg_glry_preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    allowResize: false,
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: function(e){updateCropImageInfo(e, 'bg_upload_from_gallery');},
                    onSelect: function(e){updateCropImageInfo(e, 'bg_upload_from_gallery');},
                    onRelease: clearCropImageInfo('bg_upload_from_gallery')
                }, function() {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            };
        };        
    }
}
function bg_seepreview(obj) {    
    if ($('#bg_g_image_file_name').val() == '')
        curSel = 'bg_upload_by_browse';
    else
        curSel = 'bg_upload_from_gallery';
    console.log($('#'+curSel).find(".x13").val());
    if ($('#'+curSel).find(".x13").val() != "") {
        $(obj).html("Please Wait");
        $('#bg_Modal').modal({backdrop: 'static', keyboard: false});
        bg_posterpreview(obj, curSel);
    } else {
        if ($("#bg_celeb_preview").hasClass("hide")) {
            $('#bg_Modal').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#bg_Modal').modal({backdrop: 'static', keyboard: false});
            if ($('#'+curSel).find(".x13").val() != "") {
                bg_posterpreview(obj, curSel);
            } else if ($('#'+curSel).find('.x1').val() != "") {
                bg_posterpreview(obj, curSel);
            } else {
                $('#bg_Modal').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function bg_posterpreview(obj, curSel) {
    $("#bg_previewcanvas").show();
    var canvaswidth = $("#bg_img_width").val();
    var canvasheight = $("#bg_img_height").val();
    if ($('#bg_g_image_file_name').val() == '') {
        var x1 = $('#'+curSel).find('.x1').val();
        var y1 = $('#'+curSel).find('.y1').val();
        var width = $('#'+curSel).find('.w').val();
        var height = $('#'+curSel).find('.h').val();
    } else {
        var x1 = $('#'+curSel).find('.x1').val();
        var y1 = $('#'+curSel).find('.y1').val();
        var width = $('#'+curSel).find('.w').val();
        var height = $('#'+curSel).find('.h').val();
    }
    var canvas = $("#bg_previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function() {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        //$('#imgCropped').val(canvas.toDataURL());
    };
    $("#bg_avatar_preview_div").hide();
    if ($('#bg_g_image_file_name').val() == '') {
        img.src = $('#bg_preview').attr("src");
    } else {
        img.src = $('#bg_glry_preview').attr("src");
    }
    $('#bg_Modal').modal('hide');
    $(obj).html("Next");
}

//For Logo Upload
function openImageModal(obj) {
    var name = $(obj).attr('data-name');
    var width = $(obj).attr('data-width');
    var height = $(obj).attr('data-height');
    $(".upload_detail").html(name);
    $(".help-block").html("Upload a transparent image of size " + width + " x " + height+'px');
    $("#img_width").val(width);
    $("#img_height").val(height);
    var action = HTTP_ROOT + "/template/SaveComingLogo";

    $("#upload_image_form").attr('action', action);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#LogoModal").modal('show');
}
function click_browse(modal_file) {
    $("#" + modal_file).click();
}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#upload_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('coming_logo_file').value = null;
}
function hide_gallery()
{
    $('#preview').css("display", "none");
    $("#glry_preview").css("display", "block");
    $('#gallery_preview').css("display", "none");
    $("#g_image_file_name").val("");
    $("#g_original_image").val("");
}
function fileSelectHandler() {
    document.getElementById("g_original_image").value = "";
    document.getElementById("g_image_file_name").value = "";
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#celeb_preview").removeClass("hide");
    $('#uplad_buton').removeAttr('disabled');
    var oFile = $('#coming_logo_file')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg and png are allowed)');
        $("#celeb_preview").addClass("hide");
        document.getElementById("coming_logo_file").value = "";
        $('#uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function() {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#celeb_preview").addClass("hide");
            document.getElementById("coming_logo_file").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = $('#preview');
        var oReader = new FileReader();
        oReader.onload = function(e) {
            $('.error').hide();
            oImage.attr('src', e.target.result);
            oImage.load(function() { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').css("display", "block");
                $('#celeb_preview').css("display", "block");
                $('#preview').Jcrop({
					minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: function(e){updateCropImageInfo(e, 'upload_by_browse');},
                    onSelect: function(e){updateCropImageInfo(e, 'upload_by_browse');},
                    onRelease: clearCropImageInfo('upload_by_browse'),
					setSelect:   [ 0, 0, img_width, img_height ]
                }, function() {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            });
        };
        oReader.readAsDataURL(oFile);
    };
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function seepreview(obj) {
    if ($('#g_image_file_name').val() == '')
        curSel = 'upload_by_browse';
    else
        curSel = 'upload_from_gallery';
    if ($('#'+curSel).find(".x13").val() != "") {
        $(obj).html("Please Wait");
        $('#LogoModal').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj, curSel);
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#LogoModal').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#LogoModal').modal({backdrop: 'static', keyboard: false});
            if ($('#'+curSel).find(".x13").val() != "") {
                posterpreview(obj, curSel);
            } else if ($('#'+curSel).find('.x1').val() != "") {
                posterpreview(obj, curSel);
            } else {
                $('#LogoModal').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function posterpreview(obj, curSel) {
    $("#previewcanvas").show();
    var canvaswidth = $("#img_width").val();
    var canvasheight = $("#img_height").val();
    if ($('#bg_g_image_file_name').val() == '') {
        var x1 = $('#'+curSel).find('.x1').val();
        var y1 = $('#'+curSel).find('.y1').val();
        var width = $('#'+curSel).find('.w').val();
        var height = $('#'+curSel).find('.h').val();
    } else {
        var x1 = $('#'+curSel).find('.x1').val();
        var y1 = $('#'+curSel).find('.y1').val();
        var width = $('#'+curSel).find('.w').val();
        var height = $('#'+curSel).find('.h').val();
    }
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function() {
        canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
        //$('#imgCropped').val(canvas.toDataURL());
    };
    $("#avatar_preview_div").hide();
    if ($('#g_image_file_name').val() == '') {
        img.src = $('#preview').attr("src");
    } else {
        img.src = $('#glry_preview').attr("src");
    }
    $('#LogoModal').modal('hide');
    $(obj).html("Next");
}
</script>