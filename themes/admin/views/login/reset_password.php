<style>
    .login-dv{margin: 125px auto;width: 500px;background-color: #FFF;padding-left: 30px;}
@media(max-width:550px){
    .login-dv{width: 100%;}
}

</style>
<?php if (SUB_DOMAIN == 'partners'){
    $redirecturl = Yii::app()->getBaseUrl(true);
    $reseturl = Yii::app()->getBaseUrl(true)."/login/setreseller_password";
}else{
    $redirecturl = Yii::app()->getBaseUrl(true)."/partners";
    $reseturl = Yii::app()->getBaseUrl(true)."/login/setpartner_password";
}?>
<aside class="right-side">
    <section>
        <div id="flashMessage_js" style="display: none;"></div>
        <!-- Main row -->
        <div class="row">
            <section class="col-lg-12">
                <div class="login-dv">
                    <h3 class="m-b-40">Reset Password</h3>
                    <div class="form">
                        <div id="login_loading" class="loading"></div>
                        <div id="login_errors" class="row error"></div>
                        
                        <form class="form-horizontal" name="reset_pass" id="reset_pass" action="javascript:void(0);" method="post">
                            <div class="form-group">
                                <label class="col-sm-4 control-label f-400" for="New password">New password</label>                
                                <div class="col-sm-8">
                                    <div class="fg-line">
                                        <input type="password" name="new_password" id="new_password" class="form-control input-sm"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label f-400" for="Confirm password">Confirm password</label>                
                                <div class="col-sm-8">
                                    <div class="fg-line">
                                        <input type="password" name="confirm_password" id="confirm_password" class="form-control input-sm"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">&nbsp;</label>
                                <?php 
                                $userid  = (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))?$_REQUEST['user_id']:$data['id'];
                                $auth = (isset($_REQUEST['auth']) && !empty($_REQUEST['auth']))?$_REQUEST['auth']:$data['reset_password_token'];
                                ?>
                                <input type="hidden" name="user_id" value="<?php echo $userid;?>" />
                                <input type="hidden" name="reset_token" value="<?php echo $auth;?>" />
                                <div class="col-sm-8">
                                <input name="submit" class="btn btn-primary col-sm-5" type="submit" value="Save Password" class="btn btn-primary" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>				
        </div>
    </section>
</aside>
<script type="text/javascript">
    
$(document).ready(function(){    
    jQuery.validator.addMethod("alphaNumeric", function (value, element) {
        return this.optional(element) || /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value);
    }, "password must contain atleast one number and one character");
    $("#reset_pass").validate({
        rules: {    
            new_password: {
                required: true,
                minlength:8,
                alphaNumeric: true
            },
            confirm_password: {
                minlength:8,
                equalTo : "#new_password"
            }
        },
        messages: {             
            new_password: {
                required: "Please enter your password",
                minlength: "Password must contain atleast 8 characters",
                alphaNumeric: "password must contain atleast one number and one character"
            },
            confirm_password: {
                minlength: "Password must contain atleast 8 characters",
                equalTo: "Please enter the same value again"
            }
        },
        errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        },             
        submitHandler: function(form) {

            console.log($('#reset_pass').serialize());
            $.ajax({
                url: "<?php echo $reseturl; ?>",
                data: $('#reset_pass').serialize(),
                type:'POST',
                beforeSend:function(){
                    //$('#reset-loading').show();
                },
                success:function(data){
                    if(data == "success"){
                        $("#successPopup").modal('show');
                         setTimeout(function () {
                             window.location = "<?php echo $redirecturl; ?>";
                             return false;
                         }, 3000);
                   }else{
                       alert("Your password can not be reset");
                   }
                }
            });                  
        }
    });  
});
</script>
<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Your password has been reset. Please login using new password.</h4>
            </div>
        </div>
    </div>
</div>