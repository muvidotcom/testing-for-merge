<?php
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getImageGalleryCloudFrontPath($studio_id);

$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
?>
 <div id="crop-avatar">
<form id="bannerText" method="post" name="bannerText" action="<?php echo Yii::app()->getBaseUrl(true) ?>/drafttemplate/saveBannerText">
    <div class="row m-t-40">
        <div class="col-xs-12 BannerCarouselSec">
           
            <?php
            $studio_id = Yii::app()->common->getStudioId();
            $total_sections = count($sections);
            if ($total_sections > 0) {
                ?>    

                <input type="hidden" name="banner_id" id="banner_id" />  
                <input type="hidden" name="section_id"  id="section_id" />
                <input type="hidden" name="update_text" value="0" id="update_text" />                
                <?php
                $p = 0;
                foreach ($sections as $section) {
                    $p++;
                    $section_id = $section->id;

                    $resp = Yii::app()->common->getSectionBannerDimension($section_id);

                    $max_allowed = $section->max_allowed;
                    $max_width = $resp['width'];
                    $max_height = $resp['height'];
                    $placeholder = 'Add text that goes on top of banner here';
                    $btn_txt = 'JOIN NOW';
                    $url_text='';

                    $ban = new StudioBanner;
                    $data = $ban->findAllByAttributes(array('banner_section' => $section_id, 'studio_id' => $studio_id), array('order' => 'id_seq ASC'));
                    $total_bnrs = count($data);
                    if ($total_bnrs > 0) {
                        ?>             

                        <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide relative" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner">
                                <?php
                                if ($total_bnrs > 0) {
                                    $j = 0;
                                    $bannUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                    foreach ($data as $banner) {
                                        $banner_src = $banner->image_name;
                                        $banner_title = $banner->title;
                                        $banner_id = $banner->id;
                                        $banner_full_url = $bannUrl . "/system/studio_banner/" . $studio_id . "/original/" . $banner_src;
                                        $bnr_txt = ($banner->banner_text != '') ? $banner->banner_text : 'ADD TEXT THAT GOES ON TOP OF BANNER HERE';
                                        $join_btn_txt = 'JOIN NOW';
                                        $url_text=$banner->url_text;
                                        $theme = $this->studio->preview_parent_theme;
                                        ?>
                                        <div class="item <?php echo ($j == 0) ? 'active' : '' ?>">
                                            <div class="relative displayInline">
                                            <img class="<?php echo $j ?>-slide img-responsive" src="<?php echo $banner_full_url ?>" alt="<?php echo $banner_title; ?>"  />
                                            
                                                <?php if (@IS_LANGUAGE == 1) {
                                                        if ($this->language_id == $banner_text['language_id'] && $banner_text['parent_id'] == 0) { ?>
                                                        <div class="overlay-bottom overlay-white">
                                                            <div class="overlay-Text">
                                                                <div>
                                                                    <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>">
                                                                            <em class="icon-plus" title="Add Banner"></em>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if ($total_bnrs > 1) {
                                                                        if ($j > 0) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                            </a>   
                                                                   <?php }
                                                                        if($j == 0 || $j < ($total_bnrs - 1) ) {
                                                                            ?>
                                                                            <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                                <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                            </a>                                                                
                                                                    <?php } 
                                                                    }?>                                                                      
                                                                    <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id;?> )" class="btn btn-danger icon-with-fixed-width">
                                                                        <em class="icon-trash" title="Remove Banner"></em>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php }
                                                    } 
                                                    else { ?>
                                                    <div class="overlay-bottom overlay-white">
                                                        <div class="overlay-Text">
                                                            <div>
                                                                <?php if ($max_allowed < 1 || $total_bnrs < $max_allowed) {
                                                                    ?>
                                                                    <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view">
                                                                        <em class="icon-plus" title="Add Banner"></em>
                                                                    </a>
                                                                <?php } ?>
                                                                <?php
                                                                if ($total_bnrs > 1) {
                                                                    if ($j > 0) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'previous')" class="btn btn-danger icon-with-fixed-width">
                                                                            <em class="icon-arrow-left-circle" title="Move Left"></em>
                                                                        </a>   
                                                               <?php }
                                                                    if($j == 0 || $j < ($total_bnrs - 1) ) {
                                                                        ?>
                                                                        <a href="javascript:void(0);" onclick="SortBanner(<?php echo $banner_id; ?>, <?php echo $section->id ?>, 'next')" class="btn btn-danger icon-with-fixed-width">
                                                                            <em class="icon-arrow-right-circle" title="Move Right"></em>
                                                                        </a>                                                                
                                                                <?php } 
                                                                }?>                                                                
                                                                <a href="javascript:void(0);" onclick="deleteBanner(<?php echo $banner_id;?> )" class="btn btn-danger icon-with-fixed-width">
                                                                    <em class="icon-trash" title="Remove Banner"></em>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } if($theme == "traditional"){
                                                ?>
                                                </div>
                                                <?php } ?>
                                            <div class="absolute full-width url-position">
                                                <div class="row">                  
                                                    <div class="col-md-8">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <input type="hidden" value="<?php echo $banner_id; ?>" name="banner_id[]">
                                                                <div class="col-md-4">
                                                                    <div class="urltext">
                                                                        <label>
                                                                            <i class="input-helper"></i> Enter the URL for Banner:
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="fg-line">
                                                                        <input type="url" class="form-control input-sm" id="url_text" name="url_text[]" placeholder="Enter the URL like:https://www.google.co.in" value="<?php echo $url_text; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        <?php if($theme != "traditional"){ ?>    
                                        </div>

                                        <?php
                                        }
                                        $j++;
                                    }
                                } else {
                                    ?>
                                    <div class="item active">                        
                                        <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt=""  />              
                                        <div class="overlay-bottom overlay-white">
                                            <div class="overlay-Text">
                                                <div>
                                                    <a href="#" class="btn btn-success icon-with-fixed-width avatar-view" id="<?php echo $section->id ?>"  data-toggle="modal" data-target="#avatar-modal">
                                                        <em class="icon-plus" title="Add Banner"></em>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

            <?php } ?>
                            </div>
                                <?php if ($max_allowed != 1) { ?>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>
                            <?php } ?>
                        </div>
                        <?php } else { ?>

                        <div id="myCarousel_<?php echo $section->id ?>" class="carousel slide m-t-20" data-ride="carousel" data-interval="false">
                            <!-- Indicators -->
                            <div class="carousel-inner" role="listbox" >

                                <div class="item active"> 
                                    <?php
                                     if($total_sections > 1){
                                         $style="width:". $max_width."px; height:".$max_height."px;";
                                     }else{
                                         $style="width:100%";
                                     }
                                    ?>
                                    <img class="0-slide img-responssive" src="<?php echo Yii::app()->getbaseUrl(true) ?>/images/dummy_banner.jpg" alt="" style="<?php echo $style; ?>" />              

                                    <div class="overlay-bottom overlay-white">
                                        <div class="overlay-Text">
                                            <div>
                                                <a href="javascript:void(0);" data-name="Banner" onclick="openImageModal(this)" id="<?php echo $section->id ?>" class="btn btn-success icon-with-fixed-width avatar-view">
                                                    <em class="icon-plus" title="Add Banner"></em>
                                                </a>
                                            </div>
                                        </div>
                                    </div>                                                                    
                                </div>                             
                            </div>
            <?php if ($max_allowed != 1) { ?>
                                <a class="left carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="prev"><span class="icon-arrow-left"></span></a>
                                <a class="right carousel-control" href="#myCarousel_<?php echo $section->id ?>" data-slide="next"><span class="icon-arrow-right"></span></a>
                            <?php } ?>
                        </div> 

                        <?php
                        }
                    }

                    $bnr_txt = '';
                    $show_join_btn = 0;
                    $join_btn_txt = 'JOIN NOW';
                    if (!empty($banner_text)) {
                        $bnr_txt = $banner_text['bannercontent'];
                        $show_join_btn = $banner_text['show_join_btn'];
                        $join_btn_txt = $banner_text['join_btn_txt'];
                        
                        if (@IS_LANGUAGE == 1) {
                            if ($this->language_id != $banner_text['language_id']) {
                                $bnr_txt = '';
                                $join_btn_txt = '';
                            }
                        }
                    }
                }
                ?>        

        </div>
       
    </div>


    <div class="row m-b-40 ">
        <div class="col-md-8">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-4 control-label">Text on Banner:</label>
                    <div class="col-md-8">
                        <div class="fg-line"> 
                            <input type="text" class="form-control input-sm" id="bnr_txt" name="bnr_txt" placeholder="<?php echo $placeholder ?>" value="<?php echo html_entity_decode($bnr_txt); ?>">
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="rmvdisable" value="1" name="show_join_btn" <?php echo ($show_join_btn > 0) ? ' checked="checked"' : ''; ?> id="show_join_btn" <?php if (@IS_LANGUAGE == 1) { if ($banner_text['parent_id'] > 0) {?>disabled="disabled" <?php }} ?>>
                                <i class="input-helper"></i> Show button to registration page
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" name="join_btn_txt" id="join_btn_txt" value="<?php echo $join_btn_txt; ?>">
                        </div>

                    </div>
                </div>




                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <div id="bnr_txt_error" class="error m-b-20"></div>
                        <input type="button" class="btn btn-primary btn-sm" id="savetxt" value="Update" onclick="return removeDisable();" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
 </div>
