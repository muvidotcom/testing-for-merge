<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" type="text/css" >
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/cropsetup.js"></script>
<input type="hidden" id="img_width" name="img_width">
<input type="hidden" id="img_height" name="img_height">
<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <?php if (count($theme) > 0 && $theme->has_banner == 1) { ?>
        <div class="row m-t-20" id="banner_section">
            <div class="col-sm-12">
                <div class="Block">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-docs icon left-icon "></em>
                        </div>
                        <h4>Banner</h4>                      
                    </div>
                    <hr>
                    <div class="Block-Body">
                        <?php echo $this->renderPartial('banners',array('sections' => $sections, 'banner_text' => $banner_text, 'all_images' => $all_images, 'draft' => $this->draft)) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if (count($theme) > 0 && $theme->has_video_banner == 1) { ?>
        <div class="row m-t-20" id="banner_section">
            <div class="col-sm-12">
                <div class="Block">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-docs icon left-icon "></em>
                        </div>
                        <h4>Video Banner</h4>                      
                    </div>
                    <hr>
                    <div class="Block-Body">
                        <?php //echo $this->renderPartial('videobanner',array('studio' => $studio,'studio_id' => $studio_id,'banners'=>$video_data,'section' => $video_section)) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row m-t-20" id="featured_sections">
            <div class="col-sm-12">
                <div class="Block">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-docs icon left-icon "></em>
                        </div>
                        <h4>Featured Sections </h4>                      
                    </div>
                    <hr>
                    <div class="Block-Body">
                        <?php echo $this->renderPartial('featuredsections', array('data' => $data,'theme'=>$theme, 'language_id' => $language_id)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal is-Large-Modal fade" id="homePageModal" tabindex="-1" role="dialog" aria-labelledby="homePageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/template/Savelogo" id="upload_image_form" method="post" enctype="multipart/form-data">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="HomePageModalLabel">Upload <span class="upload_detail">Image</span></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="section_id1" id="section_id1" value="" />
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" onclick="hide_file()">
                            <a href="#upload_by_browse" aria-controls="upload_by_browse" role="tab" data-toggle="tab">Upload Image</a>
                        </li>
                        <li role="presentation" onclick="hide_gallery()"> 
                            <a href="#upload_from_gallery" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Gallery</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="upload_by_browse">
                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                    <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('celeb_pic')">
                                    <input id="celeb_pic" name="Filedata" type="file" onchange="fileSelectHandler()" style="display:none;" />
                                    <p class="help-block"></p>
                                </div>
                                <input type="hidden" id="x1" name="fileimage[x1]" />
                                <input type="hidden" id="y1" name="fileimage[y1]" />
                                <input type="hidden" id="x2" name="fileimage[x2]" />
                                <input type="hidden" id="y2" name="fileimage[y2]" />
                                <input type="hidden" id="w" name="fileimage[w]"/>
                                <input type="hidden" id="h" name="fileimage[h]"/>
                                <div class="col-xs-12">
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="celeb_preview">
                                            <img id="preview" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="upload_from_gallery">
                            <input type="hidden" name="g_image_file_name" id="g_image_file_name" />
                            <input type="hidden" name="g_original_image" id="g_original_image" />
                            <input type="hidden" id="x13" name="jcrop_allimage[x13]" />
                            <input type="hidden" id="y13" name="jcrop_allimage[y13]" />
                            <input type="hidden" id="x23" name="jcrop_allimage[x23]" />
                            <input type="hidden" id="y23" name="jcrop_allimage[y23]" />
                            <input type="hidden" id="w3" name="jcrop_allimage[w3]" />
                            <input type="hidden" id="h3" name="jcrop_allimage[h3]" />
                            <div class="row  Gallery-Row">
                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40" id="all_img_glry">
                                  
                                </div>
                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue  ">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="gallery_preview">
                                            <img id="glry_preview" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="uplad_buton" disabled="disabled">Upload</button>
                <button type="button" class="btn btn-default" id="cancl_upload" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/homepage_draft.js"></script>
