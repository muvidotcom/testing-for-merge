<style type="text/css">
    .error{font-weight: normal;}
</style>
<div class="row m-t-40 m-b-40">
    <div class="col-sm-12">
        <div class="Block">

            <?php if (isset($studio_languages) && !empty($studio_languages)) { ?>
               
                <table class="table table-hover">
                    <tbody>
                        <?php 
                            foreach ($studio_languages as $key => $value) {
                            if(trim($value['translated_name']) !=""){
                                  $value['name'] = $value['translated_name'];
                            }
                        ?>
                        <tr <?php if ($value['default_language'] != $value['code'] && $value['code'] != 'en' && $value['status'] != 1) { ?>style="color: #aaa;"<?php } ?>>
                            <td>
                                <div class="section_settings form-horizontal">
                                    <div class="editlangsection hide form-group">
                                        <div class="col-sm-4">
                                            <div class="fg-line">
                                                <input type="text" class="langname form-control" value="<?php echo $value['name']; ?>" id="langname_<?php echo $value['languageid']; ?>" name="langname_<?php echo $value['languageid']; ?>" />
                                                <input type="hidden" class="langcode form-control" value="<?php echo $value['code']; ?>" id="langcode_<?php echo $value['languageid']; ?>" name="langcode_<?php echo $value['languageid']; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0);"  onclick="updateLanguageName(<?php echo $value['languageid']; ?>);return false;" title="Update"><i class="h3 green icon-check"></i></a>
                                        </div>
                                    </div> 
                                </div>
                                <div class="<?php if (($value['status'] == 1) || ($value['code'] == 'en')): ?>language_name <?php endif; ?>"><?php echo $value['name']; ?></div>
                            </td>
                                <td>
                                    <?php if ($value['default_language'] == $value['code']) { ?>
                                        
                                    <?php } else if ($value['code'] == 'en') { ?>
                                        <a href="javascript:void(0);" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="make primary" onclick="showConfirmPopup(this);">
                                            Make Primary
                                        </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="javascript:void(0);" class="" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>"  onclick="frontendShowStatus(this);">
                                            <?php echo (!isset($value['frontend_show_status']) || $value['frontend_show_status'] == '1') ? 'Hide' : 'Show'; ?>
                                        </a>
                                    <?php } else { ?>
                                        <a href="javascript:void(0);" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="make primary" onclick="showConfirmPopup(this);">
                                            Make Primary
                                        </a>
                                        <?php if ($value['status'] == 1) { ?>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="disable" onclick="showConfirmPopup(this);">
                                                Disable
                                            </a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" class="" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="make primary" onclick="frontendShowStatus(this);">
                                                 <?php echo $value['frontend_show_status'] == '1' ? 'Hide' : 'Show'; ?>
                                            </a>
                                        <?php } else { ?>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" data-language_id="<?php echo $value['languageid'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="enable" onclick="showConfirmPopup(this);">
                                                Enable
                                            </a>
                                    
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" data-language_id="<?php echo $value['language_id'];?>" data-code="<?php echo $value['code'];?>" data-language="<?php echo $value['name']; ?>" data-type="delete" onclick="showConfirmPopup(this);">
                                                Delete
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
               
            <?php } ?>
            <div class="row">
                <div class="col-sm-12">
                     <h4>Add Language</h4>
                </div>
               
            </div>
            <div class="row m-t-20">
                <div class="col-sm-8">
                    <form class="form-horizontal" method="post" name="languageForm" id="languageForm" action="javascript:void(0);">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Language</label>                    
                            <div class="col-sm-8">
                                <input type="hidden" value="" name="language_id" id="language_id"  />
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm" id="language" name="language" placeholder="Enter Language" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8 m-t-30">
                                <button type="button" onclick="return validateForm();" class="btn btn-primary btn-sm" id="nextbtn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            
        </div>
    </div>
</div>

<div class="modal fade" id="languageModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="langmodal" id="langmodal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_language" name="id_language" value="" />
                            <input type="hidden" id="code" name="code" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="languagebtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#language").keyup(function(e){
            if ($.trim($(this).val())) {
                
            } else {
                $("#language_id").val('');
            }
        });
        $('.language_name').click(function () {
            $(this).parent().find('.editlangsection').removeClass('hide');
            $(this).parent().find('.editlangsection').addClass('show');
            $(this).addClass('hide');
        });
        $('#language').autocomplete({
            source : function (request, response) {
                $.ajax({
                    url: HTTP_ROOT+'/language/getLanguages?term='+$.trim($('#language').val()),
                    dataType: "json",
                    success: function( data ) {
                        response( $.map( data, function( item ) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }

                });
            },
            select: function( event, ui ) {
                event.preventDefault();
                if (parseInt(ui.item.value)) {
                    $("#language").val(ui.item.label);
                } else {
                    $("#language_id").val('');
                }
            },
            focus: function (event, ui) {
                $("#language").val(ui.item.label);
                if (parseInt(ui.item.value)) {
                    $("#language_id").val(ui.item.value);
                } else {
                    $("#language").val('');
                }
                event.preventDefault();
            }
        });
    });
    
    jQuery.validator.addMethod("isSelect", function(value, element) {
        if ($.trim(value)) {
            if ($.trim($('#language_id').val())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }, "Please select language");
    
    function validateForm() {
        var validate = $("#languageForm").validate({
            rules: {
                "language": {
                    required: true,
                    isSelect: true
                }
            },
            messages: {
                "language": {
                    required: "Please enter language",
                    isSelect: "Please select language"
                }
            },errorPlacement: function(error, element) {
                    error.addClass('red');
                    error.insertAfter(element.parent());
            },
        });
        
        var x = validate.form();
        if (x) {
            var action ="<?php echo Yii::app()->baseUrl; ?>/language/saveLanguage";
            
            $('#languageForm').attr("action", action);
            document.languageForm.submit();
        }
    }
    
    function showConfirmPopup(obj) {
    //$("#languageModal").modal('show');
        var type = $(obj).attr('data-type');
        var language = $(obj).attr('data-language');
        
        $("#headermodal").text(type.charAt(0).toUpperCase() + type.slice(1)+" language?");
        $("#bodymodal").text("Are you sure you want to "+type+" '"+language+"' language?");
        
        $("#languagebtn").attr('data-language_id', $(obj).attr('data-language_id'));
        $("#languagebtn").attr('data-code', $(obj).attr('data-code'));
        //var onclick = type+'Language(this)';
        
        //$("#languagebtn").attr('data-type', type);
        //$("#languagebtn").attr('onclick',onclick).bind('click');
        swal({
            title: type.charAt(0).toUpperCase() + type.slice(1)+" "+language.slice(0)+" Language?",
            text: "Are you sure you want to "+type+" '"+language+"' Language?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        },
        function(){
            if(type == "enable"){
                enableLanguage(obj);
            }else if(type == "disable"){
               disableLanguage(obj); 
            }else if(type == "delete"){
                deleteLanguage(obj);
            }else{
                makeprimaryLanguage(obj);
            }
        });
        
    }
   function enableLanguage(obj) {
        $("#id_language").val($(obj).attr('data-language_id'));
        var type = $(obj).attr('data-type');
        var index = type.search(" ");
        if(index !== -1) {
            type = type.replace(' ','');
        }
        var action ="<?php echo Yii::app()->baseUrl; ?>/language/"+type+"Language";
            
        $('#langmodal').attr("action", action);
        document.langmodal.submit();
    }
    
    function disableLanguage(obj) {
        $("#id_language").val($(obj).attr('data-language_id'));
        $("#code").val($(obj).attr('data-code'));
        var type = $(obj).attr('data-type');
        var index = type.search(" ");
        if(index !== -1) {
            type = type.replace(' ','');
        }
        var action ="<?php echo Yii::app()->baseUrl; ?>/language/"+type+"Language";
            
        $('#langmodal').attr("action", action);
        document.langmodal.submit();
    }
    
    function makeprimaryLanguage(obj) {
        $("#id_language").val($(obj).attr('data-language_id'));
        $("#code").val($(obj).attr('data-code'));
        var type = $(obj).attr('data-type');
        var index = type.search(" ");
        if(index !== -1) {
            type = type.replace(' ','');
        }
        var action ="<?php echo Yii::app()->baseUrl; ?>/language/"+type+"Language";
            
        $('#langmodal').attr("action", action);
        document.langmodal.submit();
    }
    
    function deleteLanguage(obj) {
        $("#id_language").val($(obj).attr('data-language_id'));
        $("#code").val($(obj).attr('data-code'));
        var type = $(obj).attr('data-type');
        var index = type.search(" ");
        if(index !== -1) {
            type = type.replace(' ','');
        }
        var language_code = $(obj).attr('data-code');
        var deleteurl = "<?php echo Yii::app()->baseUrl; ?>/language/deletelangugefile";
        $.post(deleteurl,{language_code: language_code}, function(data){
            console.log(data); 
            var action ="<?php echo Yii::app()->baseUrl; ?>/language/"+type+"Language";
            $('#langmodal').attr("action", action);
            document.langmodal.submit();
        });
        
    }
    function updateLanguageName(lang_id){
        var update_url = HTTP_ROOT+"/language/updateLanguageName/";
        var translangname = $('#langname_'+lang_id).val();
        var translangcode = $('#langcode_'+lang_id).val();
        $.post(update_url,{language_id: lang_id,translated_name:translangname,lang_code:translangcode}, function(data){
            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Language name updated successfully.</div>'
            $('.pace').prepend(sucmsg);
            window.location.href = window.location.href;
        });
    }
    function frontendShowStatus(obj){
        var language = $(obj).attr('data-language');
        swal({
            title: "Change Show Status?",
            text: "Are you sure you want to change show status of '"+language+"' Language?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        },
        function(){
            $("#id_language").val($(obj).attr('data-language_id'));
            $("#code").val($(obj).attr('data-code'));
            var action ="<?php echo Yii::app()->baseUrl; ?>/language/frontendShowStatusOfLanguage";
            $('#langmodal').attr("action", action);
            document.langmodal.submit();
        });
    }
</script>