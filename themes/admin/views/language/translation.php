<div class="translation-wrap">
    <div class="translation-top relative">
        <div class="text-center" id="trans_loader">
            <div class="text-center preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                  <circle class="plc-path" cx="50" cy="50" r="20"/>
                </svg>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 message red">* Please do not change system variables such as $action, $registered, $movie_name, $date.</div>
            <div class="col-sm-5 pull-right">
                <a class="btn btn-primary btn-default m-b-10" data-toggle="modal" data-target="#NewKeyword">Add Keyword</a>&nbsp;
                <a class="btn btn-primary btn-default m-b-10 importlang" data-target="#langFileModal" data-toggle="modal">Import</a>&nbsp;
                <a class="btn btn-primary btn-default m-b-10 exportlang">Export</a>
                <a class="btn btn-primary m-b-10" id="save_translated_words">Save</a>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-11">
                <div class="row m-t-20">
                    <div class="col-xs-4">&nbsp;<b>UNIQUE KEY</b></div> 
                    <div class="col-xs-4"><b>ORIGINAL TEXT</b>
                        <button class="btn btn-default-with-bg" type="button" onclick="Copytoall();">
                            <i class="fa fa-copy"></i>&nbsp;&nbsp;
                            Copy all&nbsp;&nbsp;
                            <i class="fa fa-long-arrow-right"></i>
                        </button>
                    </div>
                    <div class="col-xs-4"><b>TRANSLATED TEXT (<?php echo $select_lang; ?>)</b></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20 m-b-20 translation-body">
        <div class="col-sm-11">
            <form class="form-horizontal" method="post" name="translate" id="translate">
                <?php 
                $i = $count = 1;
                foreach($all_msgs as $cat => $cat_msgs): 
                ?>
                <div class="panel-group" id="accordion<?php echo $i; ?>">
                    <div class="panel panel-default translate_panel">
                        <div class="f-200"><?php echo $cat; ?></div>
                        <?php 
                        foreach($cat_msgs as $subcat => $msgs): 
                        ?>
                        <div class="panel-heading title" data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>" href="#collapse<?php echo $count; ?>">
                            <?php echo $subcat; ?>
                        </div>
                        <div id="collapse<?php echo $count; ?>" class="panel-collapse subcat collapse <?php echo $count == 1 ? 'in' : ''; ?>">
                            <div class="panel-body">
                                <?php 
                                foreach($msgs as $msg): 
                                    $translated_value = $new_key = "";
                                    if(array_key_exists($msg->trans_key, $trans_val)){
                                        $translated_value = $trans_val[$msg->trans_key];
                                    }
                                    if(($msg->studio_id == 0) && (trim($translated_value) == "") && (trim($msg->date_added) > $last_update_date) ){
                                        $new_key = '<span class="red">*(New)</span>';
                                    }
                                    if ($msg->trans_type == '0') {
                                        $keyname = "static_message_key[]";
                                        $valuename = "static_message_value[]";
                                    } elseif ($msg->trans_type == '1') {
                                        $keyname = "js_message_key[]";
                                        $valuename = "js_message_value[]";
                                    } elseif ($msg->trans_type == '2') {
                                        $keyname = "server_message_key[]";
                                        $valuename = "server_message_value[]";
                                    }
                                ?>
                                <div class="row">
                                    <div class="col-xs-4"><?php echo $msg->trans_key; ?></div> 
                                    <div class="col-xs-4"><span class="original_word"><?php echo $msg->trans_value; ?></span> <?php echo $new_key; ?></div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="fg-line">
                                                    <input type="hidden" name="<?php echo $keyname; ?>" value="<?php echo $msg->trans_key; ?>" />
                                                    <input name="<?php echo $valuename; ?>" value="<?php echo $translated_value; ?>" class="form-control input-sm tranlate_input" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php $count++; endforeach; ?>
                    </div>
                </div>    
                <?php $i++; endforeach; ?>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="NewKeyword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h4 class="modal-title" id="myModalLabel">Add New Keyword</h4> 
            </div>
            <form name="new_keyword" id="new_keyword" class="form-horizontal" method="post" action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Language Key:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" name="translate_key" id="translate_key" class="form-control" placeholder="Language Key"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Original(English):</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" name="translate_word" id="translate_word" class="form-control" placeholder="Original(English)"/>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary add_new_word_to_translate">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="langFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content relative">

            <div class="centerLoader-InDiv" id="loader" style="display:none;"> 
                <div class="preloader pls-blue">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                    </svg>
                </div>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Translation File</h4>
            </div>
            <div class="modal-body">
                <p>Note: Please make sure worksheet name is unchanged</p>
                <form name="lang_upload" id="lang_upload" method="post">  
                    <div id="message"></div>
                    <button type="button" class="btn btn-default-with-bg" onclick="click_browse('upload_lang_file')">Browse</button>
                    <span id="filename"> No file selected</span>
                    <input type="file" name="upload_lang_file" id="upload_lang_file" style="display:none;" />   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div> 
<style type="text/css">
    .panel.translate_panel{padding:10px;}
    .translation-body._fixed{padding-top: 100px;}
    .translation-top{
        padding: 10px 0;
    }
    .translation-top._fixed{
        position: fixed;
        z-index: 2;
        left: 210px;
        right: 0px;
        background: #FFF;
        top: 60px;
        border-bottom: 1px solid rgba(0,0,0,0.1);
        box-shadow: 0 0 5px rgba(0,0,0,0.07);
        padding: 10px 15px;
    }
    .title{
        cursor: pointer;
        margin-top: 5px;
    }
    .translation-wrap{
        position: relative;
    }
    .translation-wrap .preloader{
        display: none;
        position: absolute;
        left: 0;
        right: 0;
        top: 0px; 
        bottom: 20px;
        background: rgba(255,255,255,0.5);
    }
    .translation-wrap._show_loader .preloader{
        display: block;
    } 
    .translation-wrap._show_loader  .translation-body{
        opacity: 0.5;
        pointer-events: none;
    }
</style>
<script type="text/javascript">
    $(window).on('scroll', function(){
        if($(window).scrollTop() > 70){
            $('.translation-top, .translation-body').addClass("_fixed");
        }else{
            $('.translation-top, .translation-body').removeClass("_fixed");
        }
    });
    function Copytoall() {
        swal({
            title: "Copy All",
            text: "Are you sure to copy all the content of Original(English) to <?php echo $select_lang; ?> ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        },
        function () {
            $('.original_word').each(function () {
                var original_word = $(this).text();
                $(this).parent().next().find('.tranlate_input').val(original_word);
            });
        });
    }
    function click_browse(modal_file) {
        $('#' + modal_file).click();
    }
    function hasExtension(fileName, exts) {
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }
    $(document).ready(function(){
        $('#save_translated_words').click(function () {
            swal({
                title: "Save translated Content",
                text: "Are you sure to save the translated content?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $('.translation-wrap').addClass('_show_loader');
                    $(".alert-dismissable").remove();
                    $.post(HTTP_ROOT + '/language/makeTranslation', $('#translate').serialize(), function (result) {
                        $('.translation-wrap').removeClass('_show_loader');
                        var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Language Content Updated Successfully.</div>'
                        $('.alert').remove();
                        $('.pace').prepend(sucmsg);
                        $('html, body').animate({scrollTop : 0},800);
                    return false; 
                    });
                } else {
                    return false;
                }
            });
        }); 
        $('a.exportlang').on('click',function(){
            window.location = HTTP_ROOT+"/language/getLanguageList";
        });
        $('a.importlang').on('click',function(){
            $("#message").empty();
            $("#filename").empty();
        });
        $("#upload_lang_file").change(function(e) {
            e.preventDefault();
            $("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var filename = file.name;
            $("#filename").html(filename);
            var imagefile = file.type;
            var match = ["application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
            var aftermatch = $.inArray(imagefile, match);
            aftermatch = hasExtension(filename, ['.xls','.xlsx']);
            if (aftermatch === false){
                $("#message").html("<span class='error red'>Please select a valid xls file.</p>");
                return false;
            }
            else{
                $('#loader').show();
                var form_data = new FormData();
                form_data.append('lang_file', file);
                $.ajax({
                    url: '<?php echo $this->createUrl('language/uploadLangfile/'); ?>',
                    dataType: 'json',
                    cache: false,
                    async: true,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(res) {
                        $('#loader').hide();
                        if (res.action == 'success') {
                            $('#langFileModal').modal('hide');
                            $(".alert-dismissable").remove();
                            var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;'+res.message+'</div>'
                            $('.pace').prepend(sucmsg);
                            window.location.reload();
                        } else {
                            $('#message').html("<span class='error red'>" + res.message + "</span>");
                            $("#upload_lang_file").val("");
                        }
                    }
                });
                e.stopImmediatePropagation();
                return false;
            }
        });
        $.validator.addMethod("noSpace", function(value, element) { 
          return value.indexOf(" ") < 0 && value != ""; 
        }, "No space allowed");
        $("#new_keyword").validate({ 
            rules: {    
                translate_key: {
                    required: true,
                    noSpace: true
                },
                translate_word: {
                    required: true,
                }
            },errorPlacement: function(label, element) {
                label.addClass('red');
                label.insertAfter(element.parent());
            },submitHandler: function (form) {
                $.post(HTTP_ROOT+"/language/checkUniqueKey", {key: $('#translate_key').val()}, function(result){
                    if(result.trim() == "unique"){
                        $.post(HTTP_ROOT+"/language/newKeyword", $('#new_keyword').serialize(), function(result){
                            if(result.trim() == "success"){
                                $('#NewKeyword').modal('hide');
                                var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;New message added successfully.</div>'
                                $('.pace').prepend(sucmsg);
                                window.location.href=window.location.href;
                            }
                        }); 
                    }else{
                        $('<label class="error red">Please enter unique key</label>').insertAfter($('#translate_key').parent());
                        return false;
                    }
                }); 
            }         
        });
    });
</script>


