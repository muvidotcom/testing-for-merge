<?php $v = RELEASE; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />    
        <?php if(!isset($_REQUEST['isApp'])){ 
             $this->renderPartial('share_meta',array('page_title'=>CHtml::encode($this->pageTitle),'studio_name'=>$this->studio->name,'page_desc'=>$this->pageDescription,'item_poster'=>$item_poster));             
        } ?> 
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type='text/javascript'>
            if (!window.jQuery) {
                document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
            }
            // video js error message
            var you_aborted_the_media_playback ="<?php echo $this->Language["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $this->Language["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $this->Language["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $this->Language["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $this->Language["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $this->Language["no_compatible_source_was_found"];?>";
           
            var muviWaterMark = "";
            var totalBandwidth = 0;
            var stream_id = "<?php echo $stream_id?>";
            var feed_method = "<?php echo $feed_method?>";
            var studio_id ="<?php echo $studio_id?>";
            var HTTP_ROOT = "<?php echo Yii::app()->getbaseUrl(true)?>";
<?php if (isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != '' && $embedWaterMArk == 1) { ?>
                muviWaterMark = "<?php echo strip_tags($_REQUEST['waterMarkOnPlayer']); ?>";
<?php } else if ($waterMarkOnPlayer != '') { ?>
                muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
<?php } ?>
        </script>        
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getbaseUrl(true) ?>/common/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video-js.min.css">
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/video.js/dist/video.min.js"></script>
        <!--<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/node_modules/hls.js/dist/hls.min.js"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls.js-master/src/videojs5.hlsjs.js"></script>-->
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.9.0/videojs-contrib-hls.js"></script>-->

        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/src/videojs-contrib-hls5.js"></script>
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .RDVideoHelper{display: none;}
            video::-webkit-media-controls{display:none!important}
            video::-webkit-media-controls-panel{display:none!important}
            body {
                background-color: #1E1E1E;;
                color: #fff;
                font: 12px Roboto,Arial,sans-serif;
                height: 100%;
                margin: 0;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 100%;
            }
            html {
                overflow: hidden;
            }        
            .vjs-big-play-button{display: none !important;visibility: hidden;};
            #video_block .vjs-icon-fullscreen-enter:before, 
            #video_block .video-js .vjs-fullscreen-control:before {
                content: "\f108"; }

            #video_block._fullscreen .vjs-icon-fullscreen-exit:before, 
            #video_block._fullscreen .vjs-fullscreen-control:before {
                content: "\f109"; }
            .video-js .vjs-control {
                float:left;
            }
            .video-js .vjs-control.vjs-fullscreen-control{
                float:right;
            }
            .play-btn{
                background: url("<?php echo Yii::app()->theme->baseUrl; ?>/img/black_play_icon.png") no-repeat scroll 0 0;
                bottom: 0;
                height: 60px;
                left: 0;
                margin: auto;
                position: absolute;
                right: 0;
                top: 0;
                width: 60px;
                cursor: pointer;
            }
        </style>   
    </head>
    <body>
    <center id="videoDivContent">
        <?php if (@$_REQUEST["isApp"] == 1) { ?>
            <noscript>
            <style type="text/css">
                #video_block {display:none; content:none;}
            </style>
            <div style="font-size: 22px; position:absolute; left:0; right:0; top:0; bottom:0; margin:auto; width:100%; height:10%;">
                You don't have javascript enabled.
            </div>
            </noscript>
        <?php } ?>
        <div class="demo-video"  style="position: relative;">
            <div class="videocontent" style="overflow:hidden;">                
                <video id="video_block" class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9" autobuffer='true' webkit-playsinline  fluid = 'true' controls="true">
                    <source src="<?php echo @$livestream->feed_url; ?>"  type="application/x-mpegURL" data-title="Live stream">
                </video>                
            </div>            
        </div>
        <div id='play-btn' class="play-btn" <?php if (isset($_REQUEST['isApp'])) { ?> style='display:none;' <?php } ?> ></div>    
    </center>
    <script>
                var full_screen = false;
                var is_mobile = "";
                var player = "";
                var autoplay = true;
                var videoOnPause = 1;
                var videoSource = "";
                $(function () {
                    is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
                    var item_poster = "<?php echo isset($item_poster) ? $item_poster : '' ?>";
                    //Added for iframe embeded
                <?php if (!isset($_REQUEST['isApp'])) { ?>
                        autoplay = false;
                        $('#video_block,.play-btn').hover(function () {
                            $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl; ?>/img/red_play_icon.jpg")');
                        }, function () {
                            $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl; ?>/img/black_play_icon.png")');
                        });
                <?php } ?>
                    player = videojs('video_block', {autoplay: autoplay});
                    player.ready(function () {
                        player = this;
                        //Added for embeded player
                        <?php if (!isset($_REQUEST['isApp'])) { ?>
                            $("#video_block").attr('poster', item_poster);
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $(".vjs-control-bar").hide();
                        <?php } else { ?>
                            $(".vjs-control-bar").show();
                        <?php } ?>
                        <?php if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 2)) { ?>
                            $("#video_block").attr('poster', item_poster);
                            $("#video_block_html5_api").attr('poster', item_poster);
                            $('.vjs-loading-spinner').hide();
                        <?php if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)) { ?>
                                $('.vjs-loading-spinner').attr('style', '-webkit-animation: spin 1.5s infinite linear;-moz-animation: spin 1.5s infinite linear; -o-animation: spin 1.5s infinite linear;animation: spin 1.5s infinite linear');
                                $('.vjs-loading-spinner').show();
                                <?php
                            }
                        }
                        ?>
                        $('#video_block').bind('contextmenu', function () {
                            return false;
                        });
                        $(".vjs-fullscreen-control").click(function () {
                            $(this).blur();
                        });
                        //Added for play btn
                        <?php if (!isset($_REQUEST['isApp'])) { ?>
                            $('#play-btn').on('click', function () {
                                player.play();
                                $('#play-btn').remove();
                                //$('#video_block').removeClass('vjs-controls-disabled');
                                $(".vjs-control-bar").show();
                            });
                        <?php } ?>
                        player.on("play", function () {
                            //$('.play-btn').remove();
                            if (is_mobile !== 0) {
                                $(".vjs-control-bar").show();
                            }
                            videoOnPause = 0;
                        });
                        player.on("pause", function () {
                            videoOnPause = 1;
                        });

                        <?php if ((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)) { ?>
                            player.on('mousemove', function () {
                                player.userActive(true);
                            });
                        <?php } ?>
                        <?php if ((isset($_REQUEST['isApp']) && ($_REQUEST['isApp'] == 2))) { ?>
                            $(".vjs-fullscreen-control").on('touchend', function (event) {
                                full_screen = player.isFullscreen();
                                //alert(full_screen);
                                if (full_screen === false) {
                                    Android.webViewFullscreenExit();
                                    $('#video_block').removeClass('_fullscreen');
                                } else {
                                    Android.webViewFullscreen();
                                    $('#video_block').addClass('_fullscreen');
                                }
                            });
                            var thisIframesHeight = $(window).height();
                            $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px');
                            $(window).on("resize", function (event) {
                                var thisIframesHeight = $(window).height();
                                setTimeout(function () {
                                    $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px');
                                }, 0);
                            });
                    <?php } else { ?>
                            if ($(document).height() != null) {
                                var thisIframesHeight = $(window).height();//$(document).height();                           
                                $(".video-js").attr('style', 'padding-top:' + thisIframesHeight + 'px');
                            } else {
                                $(".video-js").attr('style', 'padding-top:47%; height:50%;');
                            }
                    <?php } ?>
                    <?php if ((isset($_REQUEST['isApp'])) && (($_REQUEST['isApp'] == 1) || ($_REQUEST['isApp'] == 2))) { ?>
                            $('#play-btn').remove();
                            // $(".vjs-res-button").hide();
                            $(".vjs-volume-control").hide();
                            $(".vjs-mute-control").hide();
                            <?php if ($_REQUEST['isApp'] == 1) { ?>
                                $(".vjs-fullscreen-control").hide();
                            <?php } else if ($_REQUEST['isApp'] == 2) { ?>
                                $(".vjs-fullscreen-control").click(function () {
                                    full_screen = player.isFullscreen();
                                    if (full_screen === false) {
                                        $("#video_block").removeClass("vjs-fullscreen");
                                    } else {
                                        $("#video_block").addClass("vjs-fullscreen");
                                        full_screen = true;
                                    }
                                });
                        <?php } ?>
                    <?php } ?>
                    player.on('error', function () {
                        var errordetails = this.player().error()
                        if(errordetails.code !='' && videoSource == ''){
                            $(".vjs-modal-dialog-content").html("<div>Live stream has been disconnected</div>");
                        }else if($('.vjs-modal-dialog-content').html()!==''){
                          $(".vjs-modal-dialog-content").html("<div>"+no_compatible_source_was_found+"</div>");
                        } else {
                            if(document.getElementsByTagName("video")[0].error != null){
                                var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                                if (videoErrorCode === 2) {
                                    $(".vjs-error-display").html("<div>"+a_network_error_caused_the+"</div>");
                                } else if (videoErrorCode === 3) {
                                    $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                                } else if (videoErrorCode === 1) {
                                     $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                                } else if (videoErrorCode === 4) {
                                      $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                                } else if (videoErrorCode === 5) {
                                    $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                                }
                            }
                        }
                    });
                    <?php if($livestream->feed_type == 2) { ?>
                        checkLiveStreamOffline();
                        setInterval(function(){
                            checkLiveStreamOffline();
                        }, 10000);
                    <?php } ?>
                });
                
            });
                function handelAndroidAppPlay() {
                    if (player.paused()) {
                        player.play();
                    } else {
                        player.pause();
                    }
                }
                function handelAppUserActive() {
                    player.userActive(true);
                }
                function pausePlayer() {
                    player.pause();
                }
                function screenRotationForWebView(androidonRotation) {
                    if (androidonRotation === 0) {
                        $('#video_block').removeClass('_fullscreen');
                    } else {
                        $('#video_block').addClass('_fullscreen');
                    }
                }
            function checkLiveStreamOffline(){
                <?php if(@$feedWithIp){ ?>
                    var checkStreamurl = '<?php echo @$livestream->feed_url; ?>';
                    $.ajax({
                        type: 'HEAD',
                        url: '<?php echo @$feedWithIp; ?>',
                        success: function(){
                            if ($('#checkLiveStream').length){
                                $("#play-btn").show();
                                $('#checkLiveStream').remove();
                                if(videoOnPause){
                                    player.src(checkStreamurl);
                                    player.pause();
                                } else{
                                    player.src(checkStreamurl).play();
                                }
                            }
                            videoSource = checkStreamurl;
                        },
                        error: function() {
                            if (!$('#checkLiveStream').length){
                                $("#video_block").append("<span id='checkLiveStream'><img src='/images/streamoffline.png' width='20' height='20' title='stream offline'/>&nbsp;&nbsp;Live stream offline</span>");
                                player.src("");
                            }
                            videoSource = '';
                        }
                    });
                <?php } ?>
            }
    </script> 
        <script src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/livestreamBuffer-log.js"></script>
    <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => @$movie['stream_id'], 'movie_id' => @$movie['id'],'studio_id' => @$movie['studio_id'], 'is_live' => 1)); ?>
</body>
</html>