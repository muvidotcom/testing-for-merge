<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
    <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
      .restrict_div{
          margin-top:15%; 
      }
  </style>
</head>

<body>
    <div class="restrict_div">
        <input type="hidden" id="backbtnlink" value="<?php echo @Yii::app()->session['backbtnlink'];?>" />
        <div class="container">
         <div class="alert alert-danger alert-dismissable" onclick="_divcloseclick()">
           <a href="#" id="alertcloseid" class="close" data-dismiss="alert" aria-label="close">×</a>
           <?php  
                //echo $device_content_type."==========".$ispad;exit();
                if($device_content_type=='drm' && $ispad == 1){
                    echo $this->Language["player_watchable_message"];
                }
           ?>
         </div>
        </div>
    </div>
    <script>
        var historyURL = "<?php echo @Yii::app()->session['backbtnlink'];?>";
         $(".restrict_div").on('click',function(){
           if($('#backbtnlink').val() !='' || historyURL.length>0){
               window.location.href = $('#backbtnlink').val()? historyURL : "";
               //var xxx = $('#backbtnlink').val()? historyURL : "";
               //console.log(xxx)
               $('#alertcloseid').removeAttr('class');
               $('#alertcloseid').removeAttr('data-dismiss');
               $('#alertcloseid').hide();
           }
        });
        
        $(".restrict_div").on('touchstart',function(){
           if($('#backbtnlink').val() !='' || historyURL.length>0){
               window.location.href = $('#backbtnlink').val()? historyURL : "";
               //var xxx = $('#backbtnlink').val()? historyURL : "";
               //console.log(xxx)
               $('#alertcloseid').removeAttr('class');
               $('#alertcloseid').removeAttr('data-dismiss');
               $('#alertcloseid').hide();
           }
       });
    </script>
    </body>
</html>
<?php
    exit();
?>

