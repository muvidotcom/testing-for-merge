<?php
$v = RELEASE;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Playing Video</title>
        <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />-->
        <script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/themes/admin/js/video.dev.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/vjs.youtube.js"></script>
        <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true); ?>/js/videojs.watermark.js"></script>
		
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/flat-ui.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/themes/admin/css/video_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/small_style.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->getbaseUrl(true); ?>/common/css/videojs.watermark.css?v=<?php echo $v?>" rel="stylesheet" type="text/css" />
       
        <script data-cfasync="false" type="text/javascript">
            videojs.options.flash.swf = "<?php echo Yii::app()->baseUrl; ?>/js/video-js.swf";
        </script>
		<style type="text/css">
		
		</style>
    </head>
    <body>
        <center>
            <div class="demo-video video-js-responsive-container vjs-hd"  style="position: relative;">
                <video id = "video_block" class="video-js" controls='false' preload="none" data-setup='{"techOrder": ["youtube", "html5","flash"]}' poster="<?php echo $item_poster; ?>" >
                    <source src="" type="video/mp4">
                </video>
				<div class="error-div" style="color: #fff;position: absolute;z-index: 999999;left:0"><span>An error occurred. Please try again later.<br><a target="_blank" href="<?php echo Yii::app()->getBaseUrl(TRUE);?>">Learn More</a>
</span></div>
				<div id='play-btn' class="play-btn" ></div>
				<div id='movie-name' class="movie-name"><a href="<?php echo 'http://'.$studio->domain.'/'.$movieData['content_permalink'].'/'.$movieData['permalink'];?>" target="_blank"><?php echo $movieData['name'];?></a></div>
            </div> 			
        </center>
<script>
var myPlayer, full_screen = false, request_sent = false, first_time = true, plus_content = "", btn_html = "", show_control = false;
var movie_id = "";
var full_movie = "<?php echo $fullmovie_path ?>";
var can_see = "1";
var back_url = "";
var is_mobile =  "";
var movieName = '';

$('#play-btn').on('click',function(){
	var myPlayer = videojs("video_block");
	myPlayer.play();
});

$(document).ready(function () {
	/*$('#video_block,.play-btn').hover(function(){
		$('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/red_play_icon.jpg")');
		$('.play-btn').html('Error in ');
	},function(){
		$('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png")');
	});*/
	$("#video_block .vjs-control-bar").hide();
	if (full_movie != "") {
		videojs("video_block").ready(function () {
			var is_restrict = "";
			if (is_restrict == "1") {
				alert("This Movie is not allowed to watch in your country.");
				return;
			} else {
				var video_id = videojs("video_block");
				$("#video_block .vjs-control-bar").hide();
				play_video(full_movie, movie_id);
                           
           }
		});
    } else {
        //show_more_info();
    }
});

function resize_player() {
	$("#video_block .vjs-control-bar").show();
}


function show_warning_video() {
	alert('Seems like you have already watched 3 movies this month. Please come back next month to watch again.');
}
function play_video(video_link, movie_id) {
	//$("#vid_more_info").hide();
	//$("#episode_block").html("");
	//$("#episode_block").hide();
	var main_video = videojs("video_block");
	//main_video.src(video_link);

	main_video.src({type: "video/mp4", src: video_link});


	//main_video.src({type: "video/mp4", src: video_link });
	//main_video.play();
	main_video.on("play", function () {
	   // $("#episode_block").hide();
	   //Show and Hide control panel on Mouse hover 
	   $('#video_block').hover(function(){
		   $("#video_block .vjs-control-bar").show();
	   },function(){
		   
	   });
	   $('#play-btn').hide();
	   var video_id = videojs("video_block");
		video_id.watermark({
			file: "<?php echo $v_logo;?>",
			xpos: 80,
			ypos: 74,
			xrepeat: 0,
			opacity:0.75
		});
		$(".vjs-watermark").show();
		videojs("video_block").on("fullscreenchange", resize_player);
	});
	main_video.on("loadeddata", function () {
		$(".vjs-loading-spinner").remove();
	});

}


            function get_infoimg_height(percent) {
                var info_height = $("#vid_more_info").css("height");
                var img_px = parseInt(info_height) * (percent / 100);
                return img_px + "px";
            }

            function get_infoimg_width(percent) {
                var info_width = $("#vid_more_info").css("width");
                var img_px = parseInt(info_width) * (percent / 100);
                return img_px + "px";
            }
            function backto_info() {
                var trailer = videojs("trailer_block");
                trailer.pause();
                $("#trailer_div").hide();
                $("#vid_more_info").show();
            }

            function close_info() {
                $("#vid_more_info").hide();
                $("#wannasee_block").hide();
                //$(".random_msg").hide();
            }
        </script>
    </body>
</html>
