<?php $v = RELEASE;?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $page_title;?> | <?php echo $this->studio->name;?></title>
        <meta name="description" content="<?php echo CHtml::encode($page_desc); ?>" />
        <?php if(!isset($_REQUEST['isApp'])){ 
             $this->renderPartial('share_meta',array('page_title'=>$page_title,'studio_name'=>$this->studio->name,'page_desc'=>$page_desc,'item_poster'=>$item_poster,'og_video' =>$og_video));             
        } ?> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,300italic,300,600italic,600,400italic,800,800italic' rel='stylesheet' type="text/css" />
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" data-cfasync="false" type="text/javascript"></script>
        <script>if (!window.jQuery) {
			document.write('<script src="<?php echo Yii::app()->getbaseUrl(true); ?>/common/js/jquery-2.1.3.min.js"><\/script>');
		}</script>
        <script type='text/javascript'>
         var is_mobile = <?php echo Yii::app()->common->isMobile(); ?>;
        // video js error message
            var you_aborted_the_media_playback ="<?php echo $translateLanguage["you_aborted_the_media_playback"]; ?>";
            var a_network_error_caused_the ="<?php echo $translateLanguage["a_network_error_caused_the"]; ?>";
            var the_media_playback_was_aborted ="<?php echo $translateLanguage["the_media_playback_was_aborted"]; ?>";
            var the_media_could_not_be_loaded ="<?php echo $translateLanguage["the_media_could_not_be_loaded"]; ?>";
            var the_media_is_encrypted ="<?php echo $translateLanguage["the_media_is_encrypted"]; ?>";
            var no_compatible_source_was_found ="<?php echo $translateLanguage["no_compatible_source_was_found"];?>";
            var only_watchable_on_pc_and_laptops ="<?php echo $translateLanguage["player_watchable_message"];?>";
            var restrict_platform_ios ="<?php echo $this->Language['restrict_platform_ios'];?>";
            var restrict_platform_Android ="<?php echo $this->Language["restrict_platform_Android"];?>";
            //subtitles off
            var off = "<?php echo $translateLanguage['off']; ?>";
            var movie_id = "<?php echo @$video_log_data['movie_id']; ?>";
            var stream_id = "<?php echo @$video_log_data['stream_id']; ?>";
            var log_url = "<?php echo Yii::app()->baseUrl?>";
            var totalDRMBandWidth=0;
         //watermark on player  
         var studio_id =  "<?php echo $studio_id; ?>";
         var muviWaterMark = "";
            <?php  if (isset($_REQUEST['waterMarkOnPlayer']) && $_REQUEST['waterMarkOnPlayer'] != '') { ?>
                        var muviWaterMark = "<?php echo $_REQUEST['waterMarkOnPlayer'] ?>";
            <?php } else if ($waterMarkOnPlayer != '') { ?>
                        var muviWaterMark = "<?php echo $waterMarkOnPlayer; ?>";
            <?php } ?>
        </script>
       	   <script data-cfasync="false" type="text/javascript" src="https://vjs.zencdn.net/5.10.8/video.js"></script> 

        <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js" data-cfasync="false" type="text/javascript"></script>
	<link href="https://vjs.zencdn.net/5.10.8/video-js.css" rel="stylesheet" data-cfasync="false" >
	<link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/newdrm.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
	<script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/js/videojs.watermark.js?v=<?php echo $v ?>"></script>
        <link href="<?php echo Yii::app()->getbaseUrl(); ?>/common/css/videojs.watermark.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" /> 
    <!--videojserror-->
    <link href="<?php echo Yii::app()->getbaseUrl(); ?>/vast/css/videojs_error_css.css?v=<?php echo $v ?>" rel="stylesheet" type="text/css" />
    <script data-cfasync="false" type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(); ?>/vast/js/videojs_error_js.js?v=<?php echo $v ?>"></script>
    <!--videojserror-->

	   <style type="text/css">
		.vjs-fullscreen {padding-top: 0px}  
		.RDVideoHelper{display: none;}
		video::-webkit-media-controls {
			display:none !important;
		}
		body {
			background-color: #1E1E1E;;
			color: #fff;
			font: 12px Roboto,Arial,sans-serif;
			height: 100%;
			margin: 0;
			overflow: hidden;
			padding: 0;
			position: absolute;
			width: 100%;
		}
		html {
			overflow: hidden;
		}
		.vjs-fluid{
			width: 100%;
			height: 0;
			padding-top: 56.25%;
		}
		.temp-bg{
			position: absolute;
			z-index: 90;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: #000;
		}
                 #play{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                  -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }


            @-webkit-keyframes fadeinout {
              0%,100% { opacity: 0; }
              50% { opacity: 1; }
              from { transform: 5%; }
              50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
              0%,100% { opacity: 0; }
              50% { opacity: 1; }
              from { transform:  5%; }
              50% { transform: scale(1.4); }
            }


            #pause{
                display: none;
                margin: auto;
                left:0;
                right: 0;
                bottom: 0;
                top: 0;
                position: absolute;
                height: 64px;
                width: 64px;
                -webkit-animation: fadeinout .5s linear forwards;
                animation: fadeinout .5s linear forwards;
            }
            @-webkit-keyframes fadeinout {
              0%,100% { opacity: 0; }
              50% { opacity: 1; }
              from { transform: 5%; }
              50% { transform: scale(1.4); }
            }

            @keyframes fadeinout {
              0%,100% { opacity: 0; }
              50% { opacity: 1; }
              from { transform:  5%; }
              50% { transform: scale(1.4); }
            }
                .vjs-big-play-button{display: none !important;visibility: hidden;};
                
        </style>
    </head>
    <body>
        <center>
            <div class="wrapper">
            <input type='hidden' value="<?php echo Yii::app(true)->baseUrl?>" id="base_url_id" />
            <div class="demo-video video-js-responsive-container vjs-hd"  style="position: relative;">
                <input type="hidden" id='prevSeekingTime' value="0" />
                <div class="videocontent" style="overflow:hidden;">
                    <video id="video_block" class="video-js vjs-default-skin vjs-16-9 " controls preload="auto" autobuffer style="width: 100%;height: 100%;" crossorigin="anonymous" data-setup='{"plugins": {"errors": {}}}'>
                        <?php
                            if((!isset($_REQUEST['isApp']) || ($_REQUEST['isApp'] != 1))){
                                foreach($subtitleFiles as $subtitleFilesKey => $subtitleFilesval)
                                {
                                    if($subtitleFilesKey == 1)
                                    {
                                        echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" default="true" ></track>';
                                    } 
                                    else
                                    {
                                        echo '<track kind="subtitles" src="'.$subtitleFilesval['url'].'" srclang="'.$subtitleFilesval['code'].'" label="'.$subtitleFilesval['language'].'" ></track>';
                                    }
                                }
                            }
                        ?>
                    </video>
                </div> 
                <div id='play-btn' class="play-btn" <?php if((isset($_REQUEST['isApp'])) && ($_REQUEST['isApp'] == 1)){ ?> style='display:none;' <?php } ?> ></div>
            </div> 			
           </div>
        </center>
        <style>
            .play-btn{
                background: url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png") no-repeat scroll 0 0;
                bottom: 0;
                height: 60px;
                left: 0;
                margin: auto;
                position: absolute;
                right: 0;
                top: 0;
                width: 60px;
                cursor: pointer;
            }
			.movie-name{position: absolute;top:0;left: 0;right: 0;}
			.movie-name a{color: #fff;}
        </style>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl();?>/common/js/dash.all.debug.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-dash/2.4.0/videojs-dash.js" data-cfasync="false" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->getbaseUrl() ?>/js/videojs.fairplay.min.js"></script>
        <link href="<?php echo Yii::app()->getbaseUrl() ?>/common/videojs-airplay-master/dist/videojs.airplay.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->getbaseUrl() ?>/common/videojs-airplay-master/dist/videojs.airplay.js" type="text/javascript"></script>
        <script>
			
            var player = videojs('video_block', {
				html5: {
                    dash: {
                      limitBitrateByPortal: true
                    },
                    nativeTextTracks: false
                },
                plugins: { airplayButton: {} }
            });
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var verOffset;

    // In Opera 15+, the true version is after "OPR/" 
    if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
        browserName = "Opera";
    }
    // In older Opera, the true version is after "Opera" or after "Version"
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        browserName = "Chrome";
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
    }
                          <?php 
        //echo $v_logo;
        if($v_logo != ''){ ?>
		player.watermark({
			file: "<?php echo $v_logo; ?>",
			xrepeat: 0,
			opacity: 0.75
		});
		$(".vjs-watermark").attr("style", "bottom:46px;right:1%;width:7%;cursor:pointer;");
		$(".vjs-watermark").click(function () {
			window.history.back();
		});
	<?php } ?>
           
                $('#video_block,.play-btn').hover(function(){
                    $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/red_play_icon.jpg")');
                },function(){
                    $('.play-btn').css('background', 'url("<?php echo Yii::app()->theme->baseUrl;?>/img/black_play_icon.png")');
                });
				player.ready(function(){
                        $('#video_block').append('<img src="/images/pause.png" id="pause"/>'); 
                        $('#video_block').append('<img src="/images/play-button.png" id="play"/>');
                        if(browserName == 'Safari'){
                            player.src({
                                    src: '<?= $fullmovie_path_hls;?>',
                                    type: 'application/x-mpegURL',

                                    protection: {
                                    keySystem: 'com.apple.fps.1_0',

                                    certificateUrl: '/fairplay.cer',
                                    licenseUrl: '<?= $Authtoken['fairPlayToken'];?>',
                                    },
                            });
                        } else{
                            player.src({
                                src: '<?= $fullmovie_path;?>',
                                type: 'application/dash+xml',
                                keySystemOptions: [
                                    {
                                      name: 'com.widevine.alpha',
                                      options: {
                                        licenseUrl:"<?= $Authtoken['wvToken'];?>"
                                      }
                                    },
                                    {
                                      name: 'com.microsoft.playready',
                                      options: {
                                        'serverURL': "<?= $Authtoken['playReadyToken'];?>"
                                      }
                                    }
                                  ]
                            });
                        }
                        $('#video_block_html5_api').on('click',function(e){ 
                            e.preventDefault();
                            if (player.paused()){
                                $('#pause').hide(); 
                                $('#play').show();
                            } else{
                                $('#play').hide(); 
                                $('#pause').show(); 
                            }
                        });
                      // use of spacebar for pause the video during play.                    
                    if ((browserName === 'Safari') || (browserName === 'Firefox')) {
                            $('.vjs-control.vjs-button').click(function (e) {
                                $(this).blur();
                                e.preventDefault();
                            }); 
                        $(document).bind('keydown', function(e) {
                        if (e.keyCode === 32) { 
                            if(player.paused()){
                                player.play();
                            } else{
                                 player.pause();
                            }
                        }
                      });
                    } else if(browserName === 'Chrome'){
                            $('.vjs-control.vjs-button').click(function (e) {
                                $(this).blur();
                                e.preventDefault();
                            }); 
                        $(document).bind('keypress', function(e) {
                            //$(document).focus();
                        if (e.keyCode === 32) {
                                if(player.paused()){
                                    player.play();
                                } else {
                                    player.pause();
                                }
                            }
                         });
                    } else if(browserName === "Microsoft Internet Explorer"){
                        $('.vjs-control.vjs-button').click(function (e) {
                            $(this).blur();
                            e.preventDefault();
                        });
                        $('#video_block_html5_api').on('click',function(e){ 
                            $(this).blur();
                            e.preventDefault(); 
                        });
                        $(document).bind('keyup', function(e) {
                         if (e.keyCode === 32) { 
                                if(player.paused()){
                                    player.play();
                                } else {
                                    player.pause();
                                }
                            }
                        });
                    }
                    player.on('error', function () {
                        var errordetails = this.player().error();
                        if (errordetails.code !=="" && is_mobile === 0){ 
                            if(document.getElementsByTagName("video")[0].error != null){
                                var videoErrorCode = document.getElementsByTagName("video")[0].error.code;
                                if (videoErrorCode === 2) {
                                    $(".vjs-error-display").html("<div>"+a_network_error_caused_the+"</div>");
                                } else if (videoErrorCode === 3) {
                                    $(".vjs-error-display").html("<div>"+the_media_playback_was_aborted+"</div>");
                                } else if (videoErrorCode === 1) {
                                     $(".vjs-error-display").html("<div>"+you_aborted_the_media_playback+"</div>");
                                } else if (videoErrorCode === 4) {
                                      $(".vjs-error-display").html("<div>"+the_media_could_not_be_loaded+"</div>");
                                } else if (videoErrorCode === 5) {
                                    $(".vjs-error-display").html("<div>"+the_media_is_encrypted+"</div>");
                                }
                            }
                        }
                    });
                    if($("div.vjs-subtitles-button").length) {
                        var divtagg = $(".vjs-subtitles-button .vjs-menu .vjs-menu-content li:first-child");
                        var divtaggVal = divtagg.text();
                        if(divtaggVal.trim() == 'subtitles off'){
                            divtagg.text("subtitles "+off);
                        }
                    }
                });
			$('#play-btn').on('click',function(){
				player.play();
			});
			player.on("play", function () {
				$('.play-btn').remove();
			});
			$(document).ready(function(){
				$(".vjs-big-play-button").hide();
				 if($(document).height() != null){
                                var thisIframesHeight = $(window).height();//$(document).height();
					$( ".video-js" ).attr('style','padding-top:'+ thisIframesHeight +'px');
				} else{
					$( ".video-js" ).attr('style','padding-top:47%; height:50%;');
				}
			});
			
			videojs.Html5DashJS.beforeInitialize = function(player, mediaPlayer) {
				/* Log MediaPlayer messages through video.js*/
				if (videojs && videojs.log) {
				  mediaPlayer.getDebug().setLogToBrowserConsole(false);
				  mediaPlayer.on('log', function(event) {
				  /*console.log(event);*/
				  });
				  mediaPlayer.on('error', function(event) {
					console.log(event);
					if(event.type=='error' && event.error=='key_session' && error_count ==1){
					}
				  });
				}
			  };
            function resize_player() {
                if (full_screen === false) {
                    full_screen = true;
                    var large_screen = setTimeout(function () {
                        if (full_screen === true) {
                        }
                    }, 5000);
                } else {
                    //clearTimeout(large_screen);
                    full_screen = false;
                }
            }
            function get_infoimg_height(percent) {
                var info_height = $("#vid_more_info").css("height");
                var img_px = parseInt(info_height) * (percent / 100);
                return img_px + "px";
            }

            function get_infoimg_width(percent) {
                var info_width = $("#vid_more_info").css("width");
                var img_px = parseInt(info_width) * (percent / 100);
                return img_px + "px";
            }
            function backto_info() {
                var trailer = videojs("trailer_block");
                trailer.pause();
                $("#trailer_div").hide();
                $("#vid_more_info").show();
            }

            function close_info() {
                $("#vid_more_info").hide();
                $("#wannasee_block").hide();
                //$(".random_msg").hide();
            }
            function handelAndroidAppPlay(){
                if(player.paused()){
                    player.play();
                } else{
                    player.pause();
                }
            }
            function handelAndroidAppForward(forward){
                var currentTime = player.currentTime();
                if (forward === undefined) {
                    currentTime = currentTime -10;
                    if(currentTime > 0){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(0);
                    }
                } else{
                    currentTime = currentTime +10;
                    var playerFullDuration  = player.duration();
                    if(currentTime < playerFullDuration){
                        player.currentTime(currentTime);
                    }else{
                        player.currentTime(playerFullDuration);
                    }
                }
            }
            function handelAppUserActive(){
                player.userActive(true);
            }
            function pausePlayer(){
                player.pause();
            }
        </script>
        <script type="text/javascript" src="<?php echo Yii::app()->getbaseUrl(true)?>/js/DRMbuffer-log.js"></script>
        <?php $this->renderPartial('//layouts/video_view_log', array('stream_id' => @$movieData['stream_id'], 'movie_id' => @$movieData['id'],'studio_id' => @$movieData['studio_id'])); ?>
  
    </body>
</html>