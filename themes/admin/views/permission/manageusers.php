<div class="row">
    <div class="col-xs-12">
        <button type="button" class="btn btn-primary m-t-10" onclick="newuser();">New Administrator</button>
    </div>
</div>
<div class="row m-t-40 m-b-40">
    <div class="col-xs-12">
        <div class="Block">
            <table class="table table-hover">
                <thead>
                    <?php if (isset($users) && !empty($users)) { ?>
                        <tr>
                            <th class="min-width">Name</th>
                            <th class="min-width">Email Address</th>
                            <th data-hide="phone">Permission</th>
                            <th class="min-width" data-hide="phone">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $key => $value) { ?>
                            <tr>
                                <td class="edit_name" id='<?php echo ucwords($value->id); ?>'>
                                    <span class='mainspn mainspn<?php echo $value->id; ?>'>
                                        <span class="edit_nm_spn" style="color:#41C0FF;cursor:pointer;" ><?php echo ucwords($value->first_name) . ' ' . ucwords($value->last_name); ?></span>
                                        <span class="edit_nm_link" style="display: none;">
                                            <a href="javascript:void(0);" onclick="editName(this);" title="Edit Name">
                                                <?php echo ucwords($value->first_name) . ' ' . ucwords($value->last_name); ?>
                                            </a>
                                        </span>
                                    </span>

                                    <div class="edit_nm_input" id="edit_nm_input<?php echo $value->id; ?>" style="display: none;" >
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div id="partner_<?php echo $value->id . $value->studio_id; ?>-error" class="error" for="name" style="display: none;">Please enter name</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-7">
                                                <div class="fg-line">
                                                    <input type="text"  id="userfirst_<?php echo $value->id . $value->studio_id; ?>" class="form-control input-sm" value="<?php echo ucwords($value->first_name); ?>" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="fg-line">
                                                    <input type="text" id="userlast_<?php echo $value->id . $value->studio_id; ?>" class="form-control input-sm" value="<?php echo ucwords($value->last_name); ?>" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-offset-6 col-md-6 text-right m-t-20">
                                                <a href="javascript:void(0);" class="btn btn-default btn-sm"  onclick="cancelName('edit_nm_input<?php echo $value->id; ?>', event);" data-id="<?php echo $value->id; ?>" data-studio_id="<?php echo $value->studio_id; ?>">
                                                    Cancel
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm" onclick="cancelName('edit_nm_input<?php echo $value->id; ?>', event);" data-id="<?php echo $value->id; ?>" data-studio_id="<?php echo $value->studio_id; ?>" >
                                                    Save
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </td>
                                <td><?php echo $value->email; ?></td>
                                <td>
                                    <div class="form-group">
                                        <?php
                                        foreach ($permissions as $key2 => $value2) {
                                            $permission_id_all = explode(',', $value->permission_id);
                                            $permit_id1 = $value2->id;
                                            ?>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <label class="label-new" >
                                                        <input type="checkbox" class="<?php echo ($value2->module !='viewonly')?'selectall':'viewonly'?>" permission_id ="<?php echo $value2->id; ?>" parent_id ="<?php echo $value2->id_parent; ?>" cont_data-id =<?php echo $value->id; ?> data-studio_id="<?php echo $value->studio_id; ?>" value="Manage_checkbox" 
                                                               module_id="<?php echo $value2->module; ?>" <?php if (in_array($permit_id1, $permission_id_all) || $value->permission_id == '' && $value2->module !='viewonly') { ?> checked="checked" <?php } ?> ><i class="input-helper"></i>
                                                               <?php
                                                               $module = $value2->module;
                                                               switch ($module) {
                                                                   case "content":
                                                                       echo "Manage Content";
                                                                       break;
                                                                   case "report":
                                                                       echo "View Reports";
                                                                       break;
                                                                   case "all":
                                                                       echo "Everything";
                                                                       break;
                                                                   case "viewonly":
                                                                       echo "View Only";
                                                                       break;
                                                                    case "support":
                                                                       echo "Support";
                                                                       break;
                                                                   default:
                                                                       echo "";
                                                               }
                                                               ?> 
                                                    </label>
                                                </div>

                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                </td>
                                <td>
                                    <?php if($value->role_id ==1) {
                                        echo "";
                                    } else { ?>
                                    <h5> <a href="javascript:void(0);"  data_id="<?php echo $value->id; ?>"  onclick="showConfirmPopup(this);" data-studio_id="<?php echo $value->studio_id; ?>" > <i class="icon-trash"></i>&nbsp;Remove</a></h5>
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php } ?>
                    </tbody>
                <?php } else { ?>
                    <tbody>
                        <tr>
                            <td colspan="4" class="error">No members found</td>
                        </tr>
                    </tbody>
                <?php } ?>

            </table>
        </div>
    </div>
</div>

<div id="newuserPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <form action="javascript:void(0);" class="form-horizontal" method="post" name="new_user_form" id="new_user_form" data-toggle="validator">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Administrator</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="errors" class="col-lg-12 error red" style="padding-bottom:15px;"></div>
                    </div>

                    <div class="form-group" >
                        <label class="col-sm-3 control-label" for="First Name" > Name:</label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" placeholder="Name" name="data[Admin][first_name]" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Email">Email:</label>
                        <div class="col-sm-9">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" placeholder="Email"  name="data[Admin][email]" id="email" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Email">Permissions:</label>
                        <div class="col-sm-9">
                             <?php
                                        foreach ($permissions as $key3 => $value3) {
                                            $permit_id1 = $value3->id;
                                            ?>
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <label class="label-new" >
                                                        <input type="checkbox" name="data[default_permission][]" class="<?php echo ($value3->module !='viewonly')?'default_selectall':'default_viewonly'?>" permission_id ="<?php echo $value3->id; ?>" parent_id ="<?php echo $value3->id_parent; ?>"  value="<?php echo $value3->id; ?>" 
                                                               module_id="<?php echo $value3->module; ?>" <?php if (in_array($permit_id1, $default_permissions)) { ?> checked="checked"<?php } ?> ><i class="input-helper"></i>
                                                               <?php
                                                               $module = $value3->module;
                                                               switch ($module) {
                                                                   case "content":
                                                                       echo "Manage Content";
                                                                       break;
                                                                   case "report":
                                                                       echo "View Reports";
                                                                       break;
                                                                   case "all":
                                                                       echo "Everything";
                                                                       break;
                                                                   case "viewonly":
                                                                       echo "View Only";
                                                                       break;
                                                                   case "support":
                                                                       echo "Support";
                                                                       break;
                                                                   default:
                                                                       echo "";
                                                               }
                                                               ?> 
                                                    </label>
                                                </div>

                                            </div>
                                        <?php }
                                        ?>
                        </div>
                    </div>
                    <input type="hidden" name="data[Admin][role_id]" value="3">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="return validateNewUser();" id="newmembership">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>	
    </div>
</div>


<div class="modal fade" id="deleteusermodal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete User?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span>Are you sure you want to <b>remove User ?</b> </span> 
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" id="delete_user" class="btn btn-default studio_user"  onclick="deleteUser(this);">Yes</a>
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>
<script type="text/javascript">
    function cancelName(id, obj) {
        $("#" + id).hide();
        obj.stopPropagation();
        $(".edit_nm_spn" + id).show();
        $(".edit_name").not(document.getElementById(id)).find(".mainspn").css("display", "block");
    }

    $('.edit_name').click(function () {
        var td_id = $(this).attr('id');
        if (!$(this).find('span.edit_nm_input').is(':visible')) {
            $(this).find(".mainspn").css("display", "none");
            $(this).find(".edit_nm_input").css("display", "block");
        }

    });
    function editName(obj) {
        $(".edit_nm_input").hide();
        $(".edit_nm_spn").show();
        $(obj).parent('span.edit_nm_link').hide();
        $(obj).parent('span.edit_nm_link').parent('td.edit_name').find('span.edit_nm_spn').hide();
        $(obj).parent('span.edit_nm_link').parent('td.edit_name').find('span.edit_nm_input').show();
    }

    function saveName(obj) {
        var user_id = $(obj).attr('data-id');
        var studio_id = $(obj).attr('data-studio_id');
        var first_name = $('#userfirst_' + user_id + studio_id).val();
        if ($.trim(first_name) !== '') {
            var last_name = $('#userlast_' + user_id + studio_id).val();
            var url = "<?php echo Yii::app()->baseUrl; ?>/permission/saveUserName";
            $.post(url, {"user_id": user_id, "studio_id": studio_id, "first_name": first_name, "last_name": last_name}, function (res) {
                if (res) {
                    location.reload();
                    return false;
                }
            });
        } else {
            $(".error").hide();
            $('#user_' + user_id + studio_id + '-error').show();
            $('#userfirst_' + user_id + studio_id).focus();
        }
    }
    $('select').each(function () {
        //Store old value
        $(this).data('lastValue', $(this).val());
    });
    function saveRole(obj) {
        var lastRole = $(obj).data('lastValue');
        var newRole = $(obj).val();
        if (newRole.trim() != "Select") {
            swal({
                title: "Change permission?",
                text: "Are you sure, You want to change the permission?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            var permission_id = $(obj).attr('permission_id');
                            var user_id = $(obj).attr('data-id');
                            var studio_id = $(obj).attr('data-studio_id');
                            var role_id = $(obj).val();
                            var url = "<?php echo Yii::app()->baseUrl; ?>/permission/saveRole";
                            $.post(url, {"user_id": user_id, "studio_id": studio_id, "role_id": role_id, "permission_id": permission_id}, function (res) {
                                if (res) {
                                    location.reload();
                                    return false;
                                }
                            });
                        } else {
                            $(obj).val(lastRole);
                        }
                    });
        }
    }

    function newuser() {
        $("#newuserPopup").modal('show');
        $('label.error').remove();
        $('#new_user_form')[0].reset();
        $('#email').on('change', function () {
            checkEmailExists(0);
        });
    }

    function validateNewUser() {
        $("#newmembership").html('Saving...');
        $("#newmembership").attr("disabled", true);
        var validate = $("#new_user_form").validate({
            rules: {
                'data[Admin][first_name]': "required",
                'data[Admin][email]': {
                    required: true,
                    mail: true
                }
            },
            messages: {
                'data[Admin][first_name]': "Please enter first name",
                'data[Admin][email]': {
                    required: "Please enter email address",
                    //$('#errors').html(msg).show();
                    mail: "Please enter a valid email address"
                }
            },
            errorPlacement: function (error, element) {
                //$(".error-box").show();
                // $(".error-inner-box").html('credit card information is incorrect');
                // $(".error-box").delay(4000).fadeOut(1500);
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
        var x = validate.form();

        if (x) {
            checkEmailExists(1);
        } else {
            $("#newmembership").html('Submit');
            $("#newmembership").removeAttr("disabled");
        }
    }

    function checkEmailExists(arg) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/permission/checkEmail";
        var email = $.trim($('#email').val());
        $.post(url, {'email': email}, function (res) {
            if (parseInt(res.isExists) === 1) {
                var msg = "Email is already registered with Muvi.";
                $('#errors').html(msg).show();
                $("#newmembership").html('Submit');
                $("#newmembership").removeAttr("disabled");
                return false;
            } else {
                $('#errors').html('').hide();
                if (parseInt(arg)) {
                    var url = "<?php echo Yii::app()->baseUrl; ?>/permission/newuser";
                    document.new_user_form.action = url;
                    document.new_user_form.submit();
                    return false;
                }
            }
        }, 'json');
    }

    function showConfirmPopup(obj) {
        //$("#deleteusermodal").modal('show');
        //$("#delete_user").attr('data_id', $(obj).attr('data_id'));
        //$(".studio_user").attr('studio_id', $(obj).attr('data-studio_id'));
        swal({
            title: "Delete User?",
            text: "Are you sure you want to <b>remove User ?</b>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true,
        },
                function () {
                    deleteUser(obj);
                });
    }

    function deleteUser(obj) {
        var user_id = $(obj).attr('data_id');
        var studio_id = $(obj).attr('data-studio_id');
        var url = "<?php echo Yii::app()->baseUrl; ?>/permission/deleteUser/studio/" + studio_id + "/user/" + user_id;
        window.location.href = url;
    }
   $('.default_selectall').on('click', function(){
       var module = $(this).attr('module_id');
       //alert(module);
       var count = 0;
            $('.default_selectall').each(function (i) {
                if ($(this).is(':checked')) {
                    count++;
                }
            });
        if (module === 'all') {
            if ($(this).is(':checked')) {
                $('.default_selectall').each(function (i) {
                    $(this).prop('checked', "checked");
                });
            } else {
                $('.default_selectall').each(function (i) {
                    if (parseInt(i)) {
                        $(this).removeAttr('checked');
                    } else {
                        $(this).prop('checked', "checked");
                    }
                });
            }
          }
        if(count === 0) {
             $(this).prop('checked', 'checked');
             swal("Atleast one permission needs to be checked");
        }
   });
    $(".selectall").on('click', function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/permission/changePermission";
        var rootobj = this;
        var manage_cont_id = $(this).attr('cont_data-id');
        var permission_id = '';
        var module = $(this).attr('module_id');
        var selectall_id = $(this).parents('tr').find('.selectall');
        var viewonly_id = $(this).parents('tr').find('.viewonly');
        if (module == 'all') {
            if ($(this).is(':checked')) {
                $(selectall_id).each(function (i) {
                    $(this).prop('checked', "checked");
                });
            } else {
                $(selectall_id).each(function (i) {
                    if (parseInt(i)) {
                        $(this).removeAttr('checked');
                    } else {
                        $(this).prop('checked', "checked");
                    }
                });
            }
        } else {
            var count = 0;
            $(selectall_id).each(function (i) {
                /* if (parseInt(i) == (parseInt(selectall_id.length) - 1)) {
                    $(this).removeAttr('checked');
                } */
                if ($(this).is(':checked')) {
                    count++;
                }
            });
            if (!count) {
                $(rootobj).prop('checked', 'checked');
                //bootbox.alert('Atleast one permission needs to be checked');
                swal("Atleast one permission needs to be checked");
            }
        }
        $(selectall_id).each(function () {
            if ($(this).is(":checked")) {
                permission_id = permission_id + ',' + $(this).attr('permission_id');
            }
        });
        $(viewonly_id).each(function () {
            if ($(this).is(":checked")) {
                permission_id = permission_id + ',' + $(this).attr('permission_id');
            }
        }); 
        $.post(url, {"manage_cont_id": manage_cont_id, "permission_id": permission_id});
    });
    $(".viewonly").on('click', function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/permission/changePermission";
        var rootobj = this;
        var manage_cont_id = $(this).attr('cont_data-id');
        var permission_id = '';
        var module = $(this).attr('module_id');
        var selectall_id = $(this).parents('tr').find('.selectall');
        var viewonly_id = $(this).parents('tr').find('.viewonly');
        $(selectall_id).each(function () {
            if ($(this).is(":checked")) {
                permission_id = permission_id + ',' + $(this).attr('permission_id');
            }
        });
        $(viewonly_id).each(function () {
            if ($(this).is(":checked")) {
                permission_id = permission_id + ',' + $(this).attr('permission_id');
            }
        }); 
        $.post(url, {"manage_cont_id": manage_cont_id, "permission_id": permission_id});
    });
    
</script>