                        <input type="hidden" class="data-count" value="<?php echo $count; ?>" />
<input type="hidden" id="monetization" class="" value="<?php echo $monetization['menu']?>" />
<?php
    
    $isDate=0;
    if(isset($dateRange) && !empty($dateRange)){
        $isDate = 1;
    }
?>
<thead>
    <th>Name</th>
    <th>Type</th>
    <?php if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){ ?>
    <th>Transactions</th> 
     <th>Price</th>
    <th>Amount</th>
    <?php } ?>
    <?php if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){ ?>
    <th>Revenue</th>
    <?php } ?>
    <th>Duration</th>
    <th>Total Views</th>
    <th>Unique Views</th>
     <?php if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){ ?>
    <th>Trailer Views</th>
     <?php } ?>
  
    
    <th>Bandwidth</th>
   <!-- <th>Top Device</th>
    <th>Top Geography</th>-->
</thead>
<tbody class="list">
    <?php
   
        if(isset($viewsDetails) && !empty($viewsDetails)){
            foreach ($viewsDetails as $views){
                    $country = $bandwidth_data[$views['movie_id']]['country'];
                    $bandwidth_consumed = $this->formatKBytes($views['bandwidthdata']*1024,2);
                    $device_type = 'Web';
                    switch ($views['device_type']){
                        case 1:
                            $device_type = 'Web';
                            break;
                        case 2:
                            $device_type = 'Android';
                            break;
                        case 3:
                            $device_type = 'iOS';
                            break;
                        case 4:
                            $device_type = 'Roku';
                            break;
                        Default:
                            $device_type = 'Web';
                            break;
                    }
                    
                    $subscribe=trim($views['subscribe_price'])? Yii::app()->common->formatPrice($views['subscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);
                    $nonsubscribe=trim($views['nonsubscribe_price'])? Yii::app()->common->formatPrice($views['nonsubscribe_price'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id);

                ?>
                <tr>
                    <td class="content">
                        <?php if(isset($views['movie_id']) && $views['movie_id']){?>
<!--                            <a href="<?php echo 'videoDetails/id/'.$views['movie_id'].'/video_id/'.$views['video_id']?><?php echo $isDate ? '/?start='.$dateRange->start.'&end='.$dateRange->end:'';?>">-->
                            <?php 
                            if(trim($views['name'])){
                                if($views['content_type']==2){
                                  $trailer='(Trailer)';  
                                }
                                else{
                                   $trailer=''; 
                                }
                                
                                if(@$views['content_types_id']==3){
                                   echo $views['name']."-> Season ".$views['series_number']." -> ". ($views['episode_title']?$views['episode_title']:'Episode#-'.$views['episode_number']).$trailer;
                                }else{
                                        echo wordwrap($views['name']).$trailer;
                                }
                                
                            }else{
                                echo "Content is removed.";
                            }
                            ?>
                        <?php }else{?>
                            Content is removed.
                        <?php }?>
                    </td>
                     <?php if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){ ?>
                    <td><?php  if($views['content_type']==2){ echo 'N/A'; }else{ if(isset($views['is_advance_purchase']) && ($views['is_advance_purchase'])==0){ echo 'PPV';}elseif(isset($views['is_advance_purchase']) &&($views['is_advance_purchase'])==1){ echo 'PO';}else { echo 'N/A';} } ?></td>
                    <td><?php  if($views['content_type']==2){ echo 0; }else{ echo $views['transactions']? $views['transactions'] : 0; }//echo $transaction_data[$views['movie_id']]['transactioncount'] ? $transaction_data[$views['movie_id']]['transactioncount'] : 0?></td>
                    <td><?php if($views['content_type']==2){ echo 'N/A'; }else{ if(isset($views['subscribe_price']) && !empty($views['subscribe_price'])){ echo  $subscribe.'/'.$nonsubscribe; }else { echo 'N/A';} }?></td>
                     <td><?php if($views['content_type']==2){ echo 'N/A'; }else{ echo $views['total_amount'] ;}  ?></td>
                    <td><?php  if($views['content_type']==2){ echo Yii::app()->common->formatPrice(0,$currency_id); }else{ echo (($views['revenue'])?$views['revenue'] : Yii::app()->common->formatPrice(0,$currency_id)); } //echo trim($transaction_data[$views['movie_id']]['amount'])? Yii::app()->common->formatPrice($transaction_data[$views['movie_id']]['amount'],$currency_id) : Yii::app()->common->formatPrice(0,$currency_id)?></td>
                     <?php } ?>
                      <td><?php echo $this->timeFormat($views['duration']);?></td>
                    <td><?php echo $views['viewcount'] ? $views['viewcount'] : 0;?></td>
                    <td><?php echo $views['u_viewcount'] ? $views['u_viewcount'] : 0;?></td>
                    <?php if((isset($monetization) && (($monetization['menu'] & 2) || ($monetization['menu'] & 16)))){ ?> <td><?php echo $views['trailercount'] ? $views['trailercount'] : 0?></td>
                    
                    <?php } ?>
                    
                    <td><?php echo $bandwidth_consumed;?></td>
                   <?php /* ?> <td><?php //echo $device_type;?></td>
                    <td><?php //echo $country;?></td><?php */ ?>
                </tr>
    <?php   }
        }else{
            echo '<tr><td colspan="4">No Record found!!!</td></tr>';
        }
    ?>
</tbody>