<style type="text/css">
.skin-blue .logo {
    background-color: #3c8dbc;
    color: #f9f9f9;
}
input[type="text"], input[type="password"] {height: 35px;width: 93%;}
.login-dv{border: 1px solid #CCC;margin: 125px auto;width: 400px;background-color: #FFF;padding-left: 30px;}
@media(max-width:550px){
    .login-dv{width: 100%;}
}
.login-h1{border-bottom: 1px solid #3c8dbc;color: #3C8DBC;margin: 5px 15px 15px -20px;padding-bottom: 10px;}

div.form label {
    display: block;
    font-size: 0.9em;
    font-weight: bold;
}
label {
    display: inline-block;
    font-weight: 700;
    margin-bottom: 5px;
    max-width: 100%;
}
div.form input {
    margin: 0.2em 0 0.5em;
}
textarea, input[type="text"], input[type="password"]{
    background-color: #ffffff;
    height: 35px;
    border: 1px solid #cccccc;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
}
input[type="checkbox"] {
    opacity: 1 !important;
    box-sizing: border-box;
    padding: 0;
    cursor: pointer;
    line-height: normal;
    margin: 4px 0 0;
}
.required{color: #F00;}
#ForgotModal .modal-header{background: #42b7da; border-radius: 5px 5px 0px 0px; color: #FFF;}
#ForgotModal .modal-header h3{text-align: center; font-weight: 400; font-size: 32px; text-transform: uppercase; padding-top: 10px; padding-bottom: 10px;}
label.error{
    color: #f55753 !important;
}
#login_errors{
    color: #f55753 !important;
}
</style>

<aside class="right-side">
    <section>
        <div id="flashMessage_js" style="display: none;"></div>
        <!-- Main row -->
        <div class="row">
            <section class="col-lg-12">
                <div class="login-dv">
                    <h1 class="login-h1">Login</h1>
                    <div class="form">
                        <div id="login_loading" class="loading"></div>
                        <div id="login_errors" class="row error"></div>
                        
                        <form class="form-horizontal" name="login_form" id="login_form" action="javascript:void(0);" method="post">        
                            <div class="row form-group">
                                <label for="email">Email <span class="required">*</span></label>
                                <input name="LoginForm[email]" id="email" type="text" required="true" />
                            </div>
                            <div class="row form-group">
                                <label for="password">Password <span class="required">*</span></label>
                                <input name="LoginForm[password]" id="password" type="password" required="true" />
                            </div>

                            <div class="row form-group">
                                <div class="pull-left" style="padding-right: 5px;">
                                    <input class="remeber" name="LoginForm[rememberMe]" id="rememberMe" value="0" type="checkbox" />
                                </div>
                                <div class="pull-left">
                                    <label for="rememberMe">Remember me</label>
                                </div>
                                <div class="clearfix"></div>
                                
                            </div>
                            <div class="row form-group">
                                <div class="pull-left">
                                    <a href="#" onclick="forgot_click();">Forgot Password</a>
                                </div>
                                <div class="clearfix"></div>
                                
                            </div>

                            <div class="row form-group">
                                <button type="submit" class="btn btn-primary" id="login" name="login">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>				
        </div>
    </section>
</aside>
<div class="modal fade" id="ForgotModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div id="myModalLabel" style="position: relative;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>FORGOT PASSWORD</h3>              
                </div>  
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">     
                        <form class="form-horizontal" name="forgotform" id="forgotform" action="javascript:void(0);" method="post">
                        <div class="form-group">
                            <div id="forgot_loading1" class="hide"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif"></div>
                        </div>
                        <div class="form-group"><label>Email :</label>
                            <input name="email" id="forgot_email" type="email" class="form-control" style="width: 95%;">
                        </div>
                        <div class="form-group">
                                <label class="error" id="emailerror"></label>
                        </div>
                        <div class="form-group">
                                <button class="btn btn-blue" onclick="send_instruction(this);">Send reset password instruction</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $('#login_errors').html('').hide();
    $('#login_form').validate({
        rules: {
                "LoginForm[email]": {
                    required: true,
                    mail: true
                },
                "LoginForm[password]": {
                    required: true,
                },
        },
        message:{
            "LoginForm[email]": {
                required: "Please enter email address",
                mail: "Please enter a valid email address"
            },
            "LoginForm[password]": {
                required: "Please enter your password",
            },               
        },
        submitHandler: function (form) {
            $('#login_errors').html('').hide();
            $.ajax({
                url: "<?php echo Yii::app()->getbaseUrl(true)?>/login/partnerLogin",
                data: $('#login_form').serialize(),
                type:'POST',
                dataType: "json",
                beforeSend:function(){
                    $('#login_loading').show();
                },
                success: function (data) {
                    $('#login_loading').hide();
                    if (data.login === 'success') {
                        window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>/partners/video";
                    } else {
                        $('#login_errors').html("You have entered incorrect email or password.").show();
                    }
                },                       
                complete:function(){
                    $('#login_loading').hide();
                }
            });    
        }
    });
    
    $('#forgotform').validate({
        rules: {
            email: {
                required: true,
                mail: true
            }
        },
        message:{
            email: {
                required: "Please enter email address",
                mail: "Please enter a valid email address"
            }              
        }
});
});

function forgot_click(){
    $('#ForgotModal').modal('show');
}

function send_instruction(obj){
        var email = $("#forgot_email").val();
        if(email != ""){
            $.ajax({
                url: "<?php echo Yii::app()->getbaseUrl(true); ?>/login/forgotPartners",
                data: {email:email},
                type:'POST',
                dataType: "json",
                beforeSend:function(){
                    $(obj).html('Sending... please wait!');
                    //$('#forgot_loading').show();
                    //$('#forgot_content').hide();
                },
                success: function (data) {  
                    $(obj).html('Send reset password instruction');
                    if(data.status == 'success')
                    {
                        $('#ForgotModal').modal('hide');
                        $('#login_errors').html(data.message);
                        $('#login_errors').show();
                    }
                    else
                    {
                        $('#emailerror').html(data.message);
                        $('#emailerror').show();
                        return false;
                    }
                }
            });
        }else{
            $("#emailerror").show();
        }
    }
</script>