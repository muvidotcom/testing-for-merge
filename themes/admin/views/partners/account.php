<div class="row m-t-20 m-b-20"></div>
<div class="Block">
    <div class="Block-Header">
        <div class="icon-OuterArea--rectangular">
            <em class="icon-info icon left-icon "></em>
        </div>
        <h4>Security</h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">

            <form class="form-horizontal" method="post" role="form" id="security" name="security" action="<?php echo Yii::app()->baseUrl; ?>/partners/savepassword">
                <div class="loading" id="security-loading"></div>  
                <div class="form-group">
                    <label class="col-md-4 control-label">Old Password:</label>  
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="password" id="password" name="password" class="form-control input-sm" autocomplete="false" placeholder="Please enter your old password" />
                        </div>
                    </div>              
                </div>     
                <div class="form-group">
                    <label class="col-md-4 control-label">New Password:</label>  
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="password" id="new_password" name="new_password" class="form-control input-sm" autocomplete="false" placeholder="Enter new password" />
                        </div>
                    </div>              
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Confirm Password:</label>  
                    <div class="col-md-8">
                        <div class="fg-line">
                            <input type="password" id="conf_password" name="conf_password" class="form-control input-sm" autocomplete="false" placeholder="Re-enter new password" />
                        </div>
                    </div>              
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-primary btn-sm m-t-30">Update</button>
                    </div>
                </div>            

            </form>    
        </div>       
    </div>  
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#security").validate({
            rules: {
                password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    minlength: 6,
                },
                conf_password: {
                    equalTo: '#new_password',
                },
            },
            messages: {
                password: {
                    required: 'Please enter your current password',
                },
                new_password: {
                    required: 'Please enter new password',
                },
                conf_password: {
                    equalTo: 'Please enter the same password again',
                },
            },
            errorPlacement: function (error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            }
        });
    });
</script>