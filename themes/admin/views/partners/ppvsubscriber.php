<input type="hidden" class="data-count" value="<?php echo $userdata['count']?>" />
<thead>
    <th>User</th>
    <th>Amount</th>
    <th data-hide="phone">Date & Time</th>
    <th data-hide="phone">Content</th>
</thead>
<tbody class="list">
    <?php
    
        if(isset($page) && $page){
            $cnt = ($page- 1) * $page_size + 1;
        }else{
            $cnt = 1;
        }
        if(isset($ppv_subscription) && count($ppv_subscription)){
            foreach ($ppv_subscription as $key=>$value){
                $date = new DateTime($value['start_date']);
                $date->setTimezone(new DateTimeZone('GMT'));
    ?>
    <tr>
        <td class="email_ppv"><a style="cursor:pointer;color: #3c8dbc;" onclick="userDetails('<?php echo $value['user_id'];?>');"><?php echo $value['email']?$value['email']:$value['dname'];?></a></td>
        <td class="amount"><?php echo $value['dollar_amount']?$value['dollar_amount']:'0';?></td>
        <td class="date_time"><?php echo $date->format('F j, Y, g:ia'). ' GMT';?></td>
        <td class="content"><?php echo $value['name']?$value['name']:'';?></td>
    </tr>
    <?php }}else{?>
    <tr>
        <td colspan="4">No Record found!!!</td>
    </tr>
    <?php }?>
</tbody>