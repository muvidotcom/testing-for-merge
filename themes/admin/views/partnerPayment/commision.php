<div class="col-sm-12">
    <p>Commission earned  : $<?php echo ($total_earned > 0)?$total_earned:0.00; ?></p>
    <p>Commission received : $<?php echo ($amount_paid > 0)?$amount_paid:0.00; ?></p>
</div>
<div class="clearfix"></div>
<div class="table-responsive col-sm-10">
                        <table class="table table-hover">
                            <tbody>
                                <tr>                               
                                    <th class="width">Business Name</th>
                                    <th class="width">Muvi Subscriber</th>
                                    <th>Joined Muvi Since</th>
                                    <th style="text-align: right;">Commision Earned</th>   
                                    
                                </tr>	
                                <?php
                                if ($linked_studio) {
                                    //$first = $cnt = (($pages->getCurrentPage()) * RECORDS_PER_PAGE) + 1;
                                    foreach ($linked_studio as $key => $value) {
                                        $cnt++;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['name']; ?>
                                            </td>
                                            <td>
                                                <?php echo ($value['is_subscribed'] == 1)?'Yes':'N/A'; ?>
                                            </td>
                                            <td>
                                                <?php echo date("M d, Y", strtotime($value['created_dt'])); ?>
                                            </td>
                                            <td style="text-align: right;">
                                               <?php echo (abs($value['commision_earned']) < 0.01) ?'$0.00' : '$'.$value['commision_earned']; ?>
                                            </td>                                          
                                        </tr>
                                        <?php
                                    }
                                    if ($items_count > $page_size) {
                                        ?>
                                        <tr>
                                            <td colspan="7" style="text-align: right;">
                                                <span style="font-size: 10;">Showing <?php echo $first . " to " . --$cnt; ?> of <?php echo $items_count; ?></span>
                                                <?php
                                                $this->widget('CLinkPager', array(
                                                    'currentPage' => $pages->getCurrentPage(),
                                                    'itemCount' => $items_count,
                                                    'pageSize' => $page_size,
                                                    'maxButtonCount' => 6,
                                                    'nextPageLabel' => 'Next &gt;',
                                                    'header' => '',
                                                ));
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="7">No Record found!!!</td>
                                    </tr>	
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>