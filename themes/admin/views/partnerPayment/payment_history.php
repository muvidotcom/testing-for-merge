<input type="hidden" class="data-count" value="<?php echo $data['count'];?>" />
<table class="table table-hover billing-table">
    <thead>
            <tr>
                <th>Bill</th>
                <th data-hide="phone">Description</th>
                <th data-hide="phone">Date</th>
                <th data-hide="phone">Amount</th>
                <th data-hide="phone"></th>
            </tr>
    </thead>
    <tbody>
        <?php 
        if(isset($data) && !empty($data)){ 
           foreach($data as $key => $val){
         ?>
        <tr>
            <td>Receipt #<?php echo $val['billing_info_id'];?></td>
            <td>
                Payment for billing  period <?php echo Date('M d, Y',strtotime($subscription_arr['start_date']));?> - <?php echo Date('M d, Y',strtotime($subscription_arr['end_date']));?> <br/>
                Monthly platform fee: <?php echo $val['base_price_with_format']; ?>
            </td>
            <td><?php echo Date('M d, Y',strtotime($val['created_date']));?></td>
            <td><?php echo Yii::app()->common->formatPrice($val['billing_amount'],$curency_id); ?></td>
            <td>
                <div>
                    <a href="javascript:void(0);" data-uniqid="<?php echo $val['billing_info_id'];?>" onclick="printReceipt('<?php echo $val['billing_info_id'];?>','<?php echo $val['portal_user_id'];?>');"><i class="fa fa-print"></i> View Invoice</a>
                </div>
            </td>
        </tr>
           <?php }}else{?>
        <tr>
            <td colspan="4">No Record found!!!</td>
        </tr>
        <?php }?>
    </tbody>
</table>
<script type="text/javascript">
    function printReceipt(billing_id,user_id){
        var uniqid = billing_id;
        var url = '/partnerPayment/printReceipt/bill/'+uniqid;
        window.open(url,'_blank');
    }
</script>