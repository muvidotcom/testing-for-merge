<?php
$studio_id = Yii::app()->user->studio_id;
if($count_searched == 0) { ?>
<style>
    #loadmore{display: none;}
 </style>
<?php } ?>  

<?php
if ($count_searched > 0) {
    foreach ($list as $key => $item) {
        ?>
        <tr> 
            <td class="center"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/ticket/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php echo $item['id']; ?></a></td>
            <td style="word-break: inherit;"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/ticket/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php
                    if ($item['title'] != '') {
                        print strlen($item['title']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['title']))) ;
                    } else {
                        print strlen($item['description']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['description']))) ;
                    }
                    ?>
                    <p id="sort_grey"> <?php
                        $note = $this->ticketNotes($item['id'], 'LIMIT 1');

                        $pos = strpos($note[0]['note'], 'quoted-printable');
                        if ($pos) {
                            $notes_explode = explode('quoted-printable', $note[0]['note']);

                            $final_note = trim($notes_explode[1]);
                        } else {

                            $pos1 = strpos($note[0]['note'], 'UTF-8');
                            if ($pos1) {
                                $notes_explode1 = explode('UTF-8', $note[0]['note']);
                                $final_note = trim($notes_explode1[1]);
                            } else {
                                $final_note = trim($note[0]['note']);
                            }
                        }

                        print !empty($note) ? strlen($final_note) > 60 ? substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) . "..." : substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) : '';
                        ?></p>
                </a></td>
            <td class='td_priority'><?php echo $item['priority']; ?></td>
            <td class="center"><?php echo $item['status']; ?></td>            
            <td><?php echo Yii::app()->common->getMuviApps($item['app']); ?></td>
            <td class = 'td_last_updated_date '><?php echo date('M d, h:ia', strtotime($item['last_updated_date'])); ?></td>

        </tr>  
        <?php
    }
}
?>
  
