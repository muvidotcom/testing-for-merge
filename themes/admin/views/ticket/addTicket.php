<style>
    .autogrow{      
    min-height: 50px !important;    
    }
</style>
<?php if (Yii::app()->user->hasFlash('addTicket')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('addTicket'); ?>
    </div>
<?php else: ?>
    <div class="row m-t-40 m-b-40">
        <div class="col-md-8">
            <div class="form ticketlist Block">
               

                <form id="ticket-form" name="ticket-form" action="<?php echo Yii::app()->request->baseUrl; ?>/ticket/insertTicket" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return checkDescription(event);">  
               
                  
                 <div class="form-group add-ticket demo">
                   
                         <label class="col-md-4 control-label" id="title">Title</label>
                   
                     <div class="col-md-8">
                        <span class="hint"></span>
                        <div class="fg-line">
                            <input type="text"  class="checkSpace form-control input-sm" name="TicketForm[title]"  id="txt1" required value="<?php echo stripslashes($_SESSION['description']); ?>">
                        </div>
                     </div>
                </div>
                <div class="form-group add-ticket demo">
                   
                         <label class="col-md-4 control-label" id="description">Description</label>
                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea  class="checkSpace form-control input-sm auto-size" name="TicketForm[description]" placeholder="Describe your support request"  id="txt1" required><?php echo stripslashes($_SESSION['description']); ?></textarea>
                        </div>
                    <span class="error"><?php if ($_GET['error']) echo "Special characters not allowed"; ?></span>
                    </div>
                </div>
                <div class="form-group">
                     
                         <label class="col-md-4 control-label" >Priority</label>
                 
                    
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                
                            <select class="form-control" id="priority" name="TicketForm[priority]">
                             
                              <option value="critical">Critical</option>
                              <option value="high">High</option>
                              <option value="medium" selected="selected">Medium</option>
                              <option value="low">Low</option>
                              
                            </select>
                              </div>
                        </div>
                    
                     </div>
                </div>
                
                <div class="form-group">
		  
                         <label class="col-md-4 control-label" >Type</label>
                   
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select class="form-control" id="type"  name="TicketForm[type]">
                             
                              <option value="Issue">Bug</option>
                              <option value="New Feature">New Feature</option>
                              <option value="Support" selected="selected">How To</option>
                              <option value="SetupMigration">Setup/Migration</option>
                              
                                </select>                
                                </div>
                        </div>
		
		    </div>
                </div>
                <div class="form-group">
                <label class="control-label col-md-4">App:</label>		   
                <div class="col-md-8">
                   <div class="fg-line">
                    <div class="select">
                    <select class="form-control" id="type"  name="TicketForm[app]">
                      <option value="0" selected="selected">All</option>
                      <option value="1">Website</option>
                      <option value="2">iOS App</option>
                      <option value="3">Android App</option>
                      <option value="4">Roku App</option>
                      <option value="5">Android TV App</option>
                      <option value="6">Fire TV App</option>
                      <option value="7">Apple TV App</option>
                      <option value="8">MUVI Server</option>                     
                    </select>
                    </div>
                   </div>          
                </div>
                </div>
                <div class="form-group add-ticket demo">                   
                    <label class="col-md-4 control-label" id="title">CC</label>                   
                     <div class="col-md-8">
                        <span class="hint"></span>
                        <div class="fg-line">
                            <textarea class="form-control" name="TicketForm[ticket_email_cc]"><?php echo stripslashes($_SESSION['ticket_email_cc']); ?></textarea>
                        </div>
                        <small>Add emails with separated by comma ( , ) or semicolon ( ; )</small>
                     </div>
                </div>
                <div class="form-group">
	       	
                    <label class="control-label col-md-4">Add Attachment:</label>
		   
                   <div class="col-md-8">
                       <button class="btn btn-default-with-bg" id="upload_file_button1" type="button" onclick="click_browse('upload_file1')">Browse</button>
                       <input type="file" class="upload" name="upload_file1" id="upload_file1"  onchange="preview1(this, '1');" style="display:none;"/>
                  
                        
                        <div id="preview1" class="m-b-10 fixedWidth--Preview relative Preview-Block"></div>
                        <div id="moreImageUpload" class=""></div>
                        
                        <div id="moreImageUploadLink" style="display:none;margin-left: 10px;">
                            <a href="javascript:void(0);" id="attachMore">Attach another file</a>
                        </div>
                     </div> 
                </div>
                <div class="form-group">
                <div class=" col-md-offset-4 col-md-8">
                    <button id="submit" name="TicketForm[Submit]" class="action-btn btn btn-primary m-t-30">Submit</button>
                </div>
                </div>
            </form>
            </div><!-- form -->
        </div></div>
<?php endif; ?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/auto-size.js"></script>
<script type="text/javascript">
    $(document).ready( function() {
    if ($('.auto-size')[0]) {      
        autosize($('.auto-size'));
    }
    });
   function click_browse(upload){
        $('#'+upload).click();
    }
    
</script>