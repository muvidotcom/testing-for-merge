<?php 
    $url = Yii::app()->request->url;
    $url_arr = explode('?', $url);
    if(count($url_arr) > 1){
        $url_export = '?'.$url_arr[1];
    }
?>
<div class="row m-b-40">
    <div class="col-sm-12">
        <ul class="list-inline m-l-0">
            <li>
                <a href="<?php //echo Yii::app()->getBaseUrl(true);    ?>/ticket/addTicket" class="btn btn-primary m-t-10">Add Ticket</a>
                <button class="btn btn-primary primary report-dd m-t-10" style="width:100px" value="export">Export</button> 
            </li><br><br>
            <li>
                    <span>DevHours:</span>
                    <b>
                        <span  id="purchased_amount">
                            <?php echo $total_dev_hours; ?>
                        </span>
                    </b>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="cursor:pointer" data-toggle="modal" data-target="#show_purchasedevhours_div">Purchase</a> 
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="notification"></div>
      <form name="search" id="searchform">
       <div class="row m-b-20">
            <div class="col-sm-6">
                <div class="form-group input-group">
                    <span class="input-group-addon p-l-0"><i class="icon-magnifier"></i></span>
                    <div class="fg-line">
                        <input class="search form-control input-sm" name="searchtext" id="searchtext" value="<?php echo $_REQUEST['searchtext']; ?>" placeholder="Search for Keyword"/></div>
                </div>
            </div>
           
            <div class="col-sm-6">
                <div class="fg-line">
                    <input  type="text" class="form-control input-sm" id="ticketnumber" name="ticket" value="<?php echo $_REQUEST['ticket']; ?>" placeholder="Jump to Ticket Number" autocomplete="false" />
                </div>
            </div>
       </div>
        <div class="row m-b-20">
            
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="reporter" id="reporter" onchange="this.form.submit()">
                        <option value="All" <?php if($_REQUEST['reporter'] == 'All') {?> selected <?php } ?>>All Reporters</option>
                        <?php foreach($reporters as $reporter) { ?>
                            <option value="<?php echo $reporter['id']; ?>" <?php if($_REQUEST['reporter'] === $reporter['id']) {?> selected <?php } ?>><?php echo $reporter['first_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                 
            </div>
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="status" id="status" onchange="this.form.submit()">
                        <option value="Open" <?php if($_REQUEST['status'] == 'Open') {?> selected <?php } ?>>All Open</option>
                        <option value="Re-opened" <?php if($_REQUEST['status'] == 'Re-opened') {?> selected <?php } ?>>Re-opened</option>
                        <option value="Closed" <?php if($_REQUEST['status'] == 'Closed') {?> selected <?php } ?>>Closed</option>
                        <option value="All" <?php if($_REQUEST['status'] == 'All') {?> selected <?php } ?>>All</option>
                    </select>
                </div>                
            </div>             
            <div class="col-sm-2"> 
                <div class="select">
                    <select class="form-control input-sm" name="priority" id="priority" onchange="this.form.submit()">
                        <option value="All" <?php if($_REQUEST['priority'] == 'All') {?> selected <?php } ?>>All Priorities</option>
                        <option value="critical" <?php if($_REQUEST['priority'] == 'critical') {?> selected <?php } ?>>Critical</option>
                        <option value="high" <?php if($_REQUEST['priority'] == 'high') {?> selected <?php } ?>>High</option>
                        <option value="medium" <?php if($_REQUEST['priority'] == 'medium') {?> selected <?php } ?>>Medium</option>
                        <option value="low" <?php if($_REQUEST['priority'] == 'low') {?> selected <?php } ?>>Low</option>
                    </select>
                </div>
            </div> 
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="app" id="app" onchange="this.form.submit()" >
                    <?php foreach($muvi_apps as $muvi_app) { ?>
                        <option value="<?php echo $muvi_app->app_id; ?>" <?php if($_REQUEST['app'] === $muvi_app->app_id) {?> selected <?php } ?>><?php echo $muvi_app->app_name; ?></option>
                    <?php } ?>
                    </select>
                </div>                
            </div>  
            <div class="col-sm-2">          
                <div class="select">
                    <select class="form-control input-sm" name="type" id="type" onchange="this.form.submit()" >
                        <option value="All" <?php if($_REQUEST['type'] == 'All') {?> selected <?php } ?>>All Types</option>
                        <option value="Issue" <?php if($_REQUEST['type'] == 'Issue') {?> selected <?php } ?>>Bug</option>
                        <option value="NewFeature" <?php if($_REQUEST['type'] == 'NewFeature') {?> selected <?php } ?>>New Feature</option>
                        <option value="Support" <?php if($_REQUEST['type'] == 'Support') {?> selected <?php } ?>>How To</option>
                        <option value="SetupMigration" <?php if($_REQUEST['type'] == 'SetupMigration') {?> selected <?php } ?>>Setup/Migration</option>
                    </select>
                </div>                
            </div> 
              <div class="col-sm-2">          
                  <a href="<?php echo Yii::app()->baseUrl; ?>/ticket/ticketList" class="btn btn-primary">Reset Filter</a>
            </div> 
                
           </div>
        </form>
        
        <div id="loaderDiv" class="text-center m-b-20 loaderDiv"  style="display:none;">
            <div class="preloader pls-blue text-center " >
                <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                </svg>
            </div> 
        </div>
        <div class="row m-b-40">
            <?php if(count($tickets) > 0 ) { ?>
            <table id="ticketlist" class="table" >
                <thead>
                    <tr>
                        <th>Ticket#</th>
                        <th style="width: 250px;">Title</th>
                        <th data-hide="phone" id='priority'  class="sortcolumn">
                            Priority
                        </th>
                        <th data-hide="phone" >Status</th>
                         <th>
                            App
                        </th>
                        <th><?php
                            $sort = "fa fa-sort-desc";
                            if($_REQUEST['sortby']=='asc')
                            {
                              $sort = "fa fa-sort-asc";  
                            }
                            ?>
                            <i id="last_updated_date_i" class="<?php echo $sort ?>"></i> 
                            <label for="sortby" style="cursor: pointer;">Last Update </label><input id="sortby" type="checkbox" name="sortby" value="<?php echo $_REQUEST['sortby']=='asc'?'desc': 'asc' ;?>" form="searchform" onchange="this.form.submit()" style="display: none" />
                        </th> 
                    </tr>	
                </thead>
                <tbody id="autosearchdata"></tbody>
                <tbody id="pageloadmore">
<?php
    foreach ($tickets as $key => $item) {
        ?>
        <tr> 
            <td class="center"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/ticket/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php echo $item['id']; ?></a></td>
            <td style="word-break: inherit;"><a href="<?php echo Yii::app()->getBaseUrl(true) . "/ticket/viewTicket{$uri}/id/{$item['id']}"; ?>"><?php
                    if ($item['title'] != '') {
                        print strlen($item['title']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['title']))) ;
                    } else {
                        print strlen($item['description']) > 50 ? substr(stripslashes(nl2br(htmlentities($item['title']))), 0, 50) . "..." : stripslashes(nl2br(htmlentities($item['description']))) ;
                    }
                    ?>
                    <p id="sort_grey"> <?php
                        $note = $this->ticketNotes($item['id'], 'LIMIT 1');

                        $pos = strpos($note[0]['note'], 'quoted-printable');
                        if ($pos) {
                            $notes_explode = explode('quoted-printable', $note[0]['note']);

                            $final_note = trim($notes_explode[1]);
                        } else {

                            $pos1 = strpos($note[0]['note'], 'UTF-8');
                            if ($pos1) {
                                $notes_explode1 = explode('UTF-8', $note[0]['note']);
                                $final_note = trim($notes_explode1[1]);
                            } else {
                                $final_note = trim($note[0]['note']);
                            }
                        }

                        print !empty($note) ? strlen($final_note) > 60 ? substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) . "..." : substr(str_replace("\xC2\xA0", " ", wordwrap(htmlspecialchars_decode(stripslashes(nl2br(trim($final_note)))))), 0, 60) : '';
                        ?></p>
                </a></td>
            <td class='td_priority'><?php echo $item['priority']; ?></td>
            <td class="center"><?php echo $item['status']; ?></td>            
            <td><?php echo Yii::app()->common->getMuviApps($item['app']); ?></td>
            <td class = 'td_last_updated_date '><?php echo date('M d, h:ia', strtotime($item['last_updated_date'])); ?></td>

        </tr>  
        <?php } ?>
                </tbody>
            </table>
            <?php } else { ?> <div class="text-center">No Record Found </div> <?php } ?>
            </div>
         <?php if(count($tickets) >= 20 ) { ?>
        <input type="button" class="pull-right" id="loadmore"  value="Load More" />
         <?php } ?>

        <div id="show_purchasedevhours_div" style="display:none;" class="modal fade bs-example-modal-lg"  role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Purchase DevHours<span class="" id="pop_movie_name"></span> </h4>
                    </div>
                    <form class="form-horizontal Purchase-Subscription" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
                        <div class="modal-body">

                            <div id="card-info-error" class="error red"></div>

                            <div id="card-info-success" class="success"></div>

                            <p class="text-left">DevHours can be used for any custom design or developement service</p>
                            <?php
                            $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                            ?>

                            <div class="form-group">
                                <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken']; ?>" />

                                <label class="col-sm-12  m-b-10">Amount Details</label>
                                <input type="hidden" id="amount_charged" name="amount_charged" value="" />
                                <input type="hidden" id="dev_hours" name="dev_hours" value="" />
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="first" name="first_radio" value="500"  onchange="change_amount(this.value);" />
                                            <i class="input-helper"></i>10 DevHours $500
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="second" name="first_radio" value="4500" onchange="change_amount(this.value);" />
                                            <i class="input-helper"></i>100 DevHours $4500
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" id="third" name="first_radio"  value="40000" onchange="change_amount(this.value);"/>
                                            <i class="input-helper"></i>1000 DevHours $40,000
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                $months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
                                ?>
                                <input type="hidden" name="payment_type" id="payment_type" value="" /> 
                                <label class="col-sm-12  m-b-10">Payment Information</label>
                                <div class="col-sm-12">
                                    <div class="row">

                                        <?php if (isset($card) && !empty($card)) { ?>
                                            <div class="col-sm-6">

                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="payment_info" id="used_card" onclick="paymentInfo(this);" value="saved" /><i class="input-helper"></i>Use the saved card 
                                                    </label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="fg-line">
                                                        <div class="select">
                                                            <select name="creditcard" id="creditcard" class="form-control input-sm">
                                                                <?php foreach ($card as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id; ?>" <?php if (isset($value->is_cancelled) && $value->is_cancelled == 0) { ?>selected="selected"<?php } ?>><?php echo $value->card_last_fourdigit . " " . $value->card_type; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="payment_info" id="new_card" onclick="paymentInfo(this);" value="new" /><i class="input-helper"></i>Or, add a new card
                                                </label>
                                            </div>
                                            <label id="payment_info-error" class="error red" for="payment_info" style="display: none;"></label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="new_card_dv" class="row m-t-30" style="display: none;"></div>     
                            <div id="terms-div" class="m-t-10"  style="display: none;">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="terms_of_use" id="terms_of_use"><i class="input-helper"></i>                                  
                                                I agree with the <a href="<?php echo Yii::app()->baseUrl; ?>/signup/signupTerms" target="_blank">terms of use</a> of Muvi
                                            </label>
                                            <div><label id="terms_of_use-error" class="error red" for="terms_of_use" style="display: none;" disabled ></label></div>
                                        </div>
                                    </div>                     
                                </div>
                            </div>



                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-sm" id="purchase">Purchase DevHours</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="new_card_info" class="row"  style="display: none;">
                <div id="card-info-error" class="error red" style="display: none;"></div>
                <div class="col-sm-6">

                    <div class="form-group">
                        <label class="control-label col-sm-4">Name on Card:</label>                    
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="card_name" name="card_name" placeholder="Enter Name" />
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Card Number:</label>                    
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" id="card_number" name="card_number" placeholder="Enter Card Number" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Expiry Date:</label>                    
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="exp_month" id="exp_month" class="form-control input-sm">
                                        <option value="">Expiry Month</option>	
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <div class="select">
                                    <select name="exp_year" id="exp_year" class="form-control input-sm" onchange="getMonthList();">
                                        <option value="">Expiry Year</option>
                                        <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-4">Security Code:</label>                    
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="password" id="" name="" style="display: none;" />
                                <input type="password" class="form-control input-sm" id="security_code" name="security_code" placeholder="Enter security code" />
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Billing Address:</label>                    
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="Address 1" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Address 2" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm" id="zipcode" name="zipcode" placeholder="Zipcode" />
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>



        </div>



    </div>
</div>

<!---  purchase Dev hours Pop up--->


<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Your payment is in progress.</h4>
            </div>
        </div>
    </div>
</div>


<!---End purchase Dev hours popup --->


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>

<div class="dialog-email hide" title="Send Reply Mail">
    <span id='success' style='color:red;'></span>
    <table>
        <tr><td>Reply To:</td></tr>
        <tr><td><input type="text" id="email_to"  /></td></tr>
        <tr><td>Subject:</td></tr>
        <tr><td><input type="text" id="subject"   /></td></tr>
        <tr><td>Mail Content:</td></tr>
        <tr><td><textarea id="textarea_content" rows="14" cols="60"></textarea></td></tr>
    </table>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/js/common.js" type="text/javascript"></script>
<script type="text/javascript" src="/themes/admin/js/bootstrap-typeahead.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootbox.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/jquery.bootpag.min.js"></script>

<script type="text/javascript">
    $('.report-dd').click(function(){
            window.location = '<?php echo Yii::app()->baseUrl."/ticket/ExportTickets".$url_export?>';       
    });

    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var pricingall = '';
    var packagesall = '';
    var planall = '';
    var custom_codes = '';
    var num = 2;
    $(document).ready(function(){
    $("#ticketnumber").keyup(function(event){
        if(event.keyCode == 13){
            $("#searchform").submit();
        }
     });
     $("#searchtext").keyup(function(event){
        if(event.keyCode == 13){
            $("#searchform").submit();
        }
     });
     
    });

    function validateForm() {
        $('#card-info-error').hide();
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                first_radio: "required",
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                terms_of_use: "required",
                payment_info: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                first_radio: "Please select the amount",
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                terms_of_use: "Please accept Terms and Conditions",
                payment_info: "Please select payment information",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            },
            errorPlacement: function (error, element) {
                error.addClass('red');
                switch (element.attr("name")) {
                    case 'exp_month':
                        error.insertAfter(element.parent().parent());
                        break;
                    case 'first_radio':
                        error.insertBefore(element.parent().parent());
                        break;
                    case 'payment_info':
                        error.insertBefore(element.parent().parent());
                        break;
                    case 'exp_year':
                        error.insertAfter(element.parent().parent());
                        break;
                    default:
                        error.insertAfter(element.parent());
                }
            }

        });
        var x = validate.form();
        if (x) {
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled', 'disabled');
            $("#loadingPopup").modal('show');
            var url = "<?php echo Yii::app()->baseUrl; ?>/payment/verifyandsavecard";
            var used_creditcard = $("#creditcard").val();
            var amount = $("#amount_charged").val();
            var payment_type = $("#payment_type").val();
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();
            var dev_hours = $("#dev_hours").val();
            var i_agree = $("#terms_of_use").attr("checked");
            var csrfToken = $('#csrfToken').val();
            $.post(url, {'used_creditcard': used_creditcard, 'payment_type': payment_type, 'amount': amount, 'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'dev_hours': dev_hours, 'csrfToken': csrfToken}, function (data) {
                $("#loadingPopup").modal('hide');
                if (parseInt(data.isSuccess) === 1) {
                    $("#successPopup").modal('show');
                    setTimeout(function () {
                        location.reload();
                        $('#card-info-success').show().html('Payment of an amount $' + amount + ' completed successfully</br>');

                        return false;
                    }, 5000);
                } else {
                    $('#nextbtn').html('Purchase DevHours');
                    $('#nextbtn').removeAttr('disabled');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
                }
            }, 'json');
        }
    }


    function change_amount(a)
    {

        $("#amount_charged").val(a);

        if (a == 500)
        {
            $("#dev_hours").val("10");
        } else if (a == 4500)
        {
            $("#dev_hours").val("100");
        } else
        {
            $("#dev_hours").val("1000");
        }
    }
    function paymentInfo(obj) {
        $('#terms_of_use').attr('disabled', false);
        var res = pricingall.split("");

        // $('#paymentMethod')[0].reset();
        $(obj).prop('checked', true);

        //Reset prices
        $("#packagetext").val(packagesall);
        $("#pricingtext").val(pricingall);
        $("#plantext").val(planall);
        $("#custom_codes").val(custom_codes);

        $('.price_chkbox').each(function () {
            var priceid = $(this).attr('data-id');
            var index = res.indexOf(priceid);
            if (index !== -1) {
                $(this).prop('checked', true);
            }
        });

        if ($("#new_card:checked").length) {
            $("#payment_type").val("1");
            $("#new_card_dv").html($("#new_card_info").html());
            $("#new_card_dv").slideDown("slow");
        } else {
            $("#payment_type").val("0");
            $("#new_card_dv").html('');
            $("#new_card_dv").slideUp("slow");
        }
        $("#terms-div").show();
        $("#terms_of_use-error").hide();
    }


    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;

        if (curyr === selyr) {
            startindex = curmonth;
        }

        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" ' + selected + '>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }

    function confirm_pay(ticket_id, dev_hour, studio_id) {
        if (dev_hour != 0) {
            var url = '<?php echo Yii::app()->baseUrl; ?>/ticket/useDevhour';
            swal({
                title: "Purchase DevHour Confirmation?",
                text: dev_hour + " DevHours will be deducted from your account to implement this Change/Feature. Do you approve?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            },
                    function () {
                        $('#pay_btn' + ticket_id).after("<img src='<?php echo Yii::app()->baseUrl; ?>/images/loader.gif' height='20px' class='ajax-loader'/>");
                        $('.ajax-loader').show();
                        show_alert(ticket_id, url, dev_hour, studio_id);
                    });
        } else {
            swal("Please wait for the dev hours to be assigned !!!");
        }
    }

    function show_alert(ticket_id, url, dev_hour, studio_id)
    {

        jQuery.post(url, {'ticket_id': ticket_id, 'dev_hour': dev_hour, 'is_ajax': 1, 'studio_id': studio_id}, function (res) {
            var existing_amount = $("#purchased_amount").html();
            // alert(existing_amount);
            var remaining_amount = existing_amount - dev_hour;
            //  alert(remaining_amount);
            var result = jQuery.parseJSON(res);
            if (result.status == 1) {
                $('.ajax-loader').hide();
                $("#pay_btn" + ticket_id).html("Paid");
                $("#pay_btn" + ticket_id).attr("disabled", "disabled");
                $("#purchased_amount").html(remaining_amount);
                $("#pay_btn" + ticket_id).prop('onclick', null).off('click');
            } else {
                $('.ajax-loader').hide();
                swal("Not enough DevHours. Please purchase");
            }
        });

    }
    function getAutoSearchTicket()
    {
        var reporter = $("#reporter :selected").val();
        var status = $("#status :selected").val();
        var priority = $("#priority :selected").val(); 
        var app = $("#app :selected").val();
        var type = $("#type :selected").val();
         $('#loaderDiv').show();
        $.post('/ticket/AutoSearchticket', {'reporter':reporter, 'status': status, 'priority':priority, 'app':app, 'type':type,}, function (res) {
            $('#autosearchdata').html(res);
            $('#loaderDiv').hide();
         });
    }
$(document).ready(function(){
    /// the below interval for autorefresh tickets
    setInterval(function(){ 
       getAutoSearchTicket();
    }, 60000);
        
        $('#loadmore').click(function(){
        $('#loaderDiv').hide();
        $('#loadmore').val("Loading...");
        var searchText = $.trim($("#searchtext").val());
        var reporter = $("#reporter :selected").val();
        var status = $("#status :selected").val();
        var priority = $("#priority :selected").val(); 
        var app = $("#app :selected").val();
        var type = $("#type :selected").val();
        var sortby = '<?php echo $_REQUEST['sortby'] == 'asc'?'asc':'desc' ?>';
        $.post('/ticket/searchTicket', {'searchText': searchText, 'reporter':reporter, 'status': status, 'priority':priority, 'app':app, 'type':type, 'sortby': sortby, 'page': num,}, function (res) {           
            $('#pageloadmore').append(res);
            //console.log(res);
            $('#loadmore').val("Load More");
            num++;
        });
        
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
        });
    });
</script>
