<!--Add downloadable content-->
<?php if($IsDownloadable){?>
<div class="form-group">
	<label for="itemtype" class="col-md-4 control-label">Content option: </label>
	<div class="col-md-8">
		<div class="control-label row">
			<div class="col-sm-12">
				<label class="checkbox checkbox-inline m-r-20">
					<input value="1" <?php echo (in_array(@$movieDetails[0]['is_downloadable'], array(1,2)))?'checked=checked':'';?> name="download" type="checkbox" <?php if(@$movieDetails[0]['full_movie']!=''){echo 'disabled';}?>>
					<i class="input-helper"></i> Download
				</label>
				<label class="checkbox checkbox-inline m-r-20">
					<input value="1" <?php echo (in_array(@$movieDetails[0]['is_downloadable'], array(0,2)))?'checked=checked':'';?> name="stream" type="checkbox" <?php if(@$movieDetails[0]['full_movie']!=''){echo 'disabled';}?>>
					<i class="input-helper"></i> Stream
				</label>
			</div>
		</div>
	</div>
</div>
<?php }?>
<input type="hidden" id="is_check_custom" value="<?php if (@$customData){echo 1;}else{echo 0;}?>">
<?php
$disable = "";
$enable_lang = "en";
if(isset($movieDetails[0]['id'])){
    $enable_lang = $this->language_code;
    if ($_COOKIE['Language']) {
        $enable_lang = $_COOKIE['Language'];
    }
    $disable = 'disabled="disabled"';
    if(@$movieDetails[0]['language_id'] == $this->language_id && @$movieDetails[0]['parent_id']==0){
        $disable = "";
    }
    $stream_id = $movieDetails[0]['movie_stream_id'];
    if(array_key_exists($stream_id, @$langcontent['episode'])){
        $movieDetails[0] = Yii::app()->Helper->getAdminEpisodeLanguageCustomValue($movieDetails[0],@$langcontent['episode'][$stream_id]);
        $movieDetails[0]['episode_title'] = @$langcontent['episode'][$stream_id]->episode_title;
        $movieDetails[0]['episode_story'] = @$langcontent['episode'][$stream_id]->episode_story;
    }
}
if(@$customData){
	$defaultFields = array('content_name','title','story','series_number','episode_number','episode_date');
	$relationalFields = Yii::app()->Helper->getRelationalEpisodeField(Yii::app()->user->studio_id);  
	$formData = $customData['formData'];
	unset($customData['formData']);
	foreach ($customData AS $ckey=>$cval){
		if(!in_array($cval['f_name'],$defaultFields)){
			if($relationalFields && array_key_exists($cval['f_name'],$relationalFields)){
				$cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
			}
		}
                if($cval['f_name']=='title'){
                    $cvalue = 'episode_title';
                }elseif($cval['f_name']=='story'){
                    $cvalue = 'episode_story';
                }else{
                    $cvalue = str_replace('custom', 'cs', $cval['f_name']);
                }
		?>
<div class="form-group">
			<label for="<?= $cval['f_display_name'];?>" class="col-md-4 control-label"><?= $cval['f_display_name'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
			<?php if(!$cval['f_type']){?>
				<div class="col-md-8">
				<div class="fg-line">
				<?php if($cval['f_id']=='episode_date'){?>
					 <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="episode_date" name="episode[episode_date]" value="<?php
						if (@$movieDetails[0]['episode_date']) {
							echo date('m/d/Y', strtotime($movieDetails[0]['episode_date']));
						}
						?>" class="form-control input-sm checkInput" >
				<?php }else{?>
				<input type='text' placeholder="" id="<?= $cval['f_id'];?>" name="episode[<?= $cval['f_name'];?>]" value="<?php echo @$movieDetails[0][$cvalue]; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> <?php if(@$movieDetails[0]['id']=='' && $cval['f_name']=='name'){?> onblur="checkperma(this.value)"<?php } ?>>
				<?php } ?>
				</div>
				</div>	
			<?php }elseif($cval['f_type']==1){?>
				<div class="col-md-8">
				<textarea class="form-control input-sm checkInput" rows="5" placeholder="" name="episode[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$movieDetails[0][$cvalue]; ?></textarea>
				</div>
			<?php }elseif($cval['f_type']==2){
                            if($cval['f_id']=='content_name'){?>
                                   <div class="col-md-8">
                                       <div class="fg-line">
                                            <div class="select">
                                    <select class="form-control input-sm checkInput" <?php echo $disable; ?>  id="content_name" name='episode[content_name]' <?php if (@$editflag) { ?><!--disabled="true"--><?php } ?> required  >
                                            <option value="">Select</option>
                                                <?php
                                                foreach ($shows AS $key => $val) {
                                                    if(array_key_exists($key, $langcontent['film'])){
                                                        $val = $langcontent['film'][$key]->name;
                                                    }
                                                    $selected = '';
                                                    $selContentId = $movieDetails[0]['id'] ?$movieDetails[0]['id']:@$content_id; 
                                                    if ($key == $selContentId) {
                                                        $selected = "selected='selected'";
                                                    }
                                                    echo '<option value="' . $key . '" ' . $selected . ' >' . $val . '</option>';
                                                }
                                                ?>
                                    </select>
                                    </div> 
                                        </div>
                                    </div>
                                <?php }else{ ?>                           
				<div class="col-md-8">
				<select name="episode[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
					<?php
						echo "<option value=''>-Select-</option>";
						$opData = json_decode($cval['f_value'],true);
                        $opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
						foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
							if (@$movieDetails[0]['id'] && @$movieDetails[0][$cvalue]==$opvalue) {
								$selectedDd = 'selected ="selected"';
							}
							echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
						}
						?>
					</select>
				</div>
                                <?php }}elseif($cval['f_type']==3){?>				
					<div class="col-md-8">
					<select name="episode[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput">
					<?php
						$opData = json_decode($cval['f_value'],true);
                        $opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
						foreach($opData AS $opkey=>$opvalue){
                            $selectedDd = '';
                            if (@$movieDetails[0]['id'] && in_array($opvalue, json_decode(@$movieDetails[0][$cvalue],true))) {
                                $selectedDd = 'selected ="selected"';
                            }
							echo "<option value='".$opvalue."'".$selectedDd.">" . $opvalue . "</option>";
						}
						?>
					</select>
					</div>			
			<?php }elseif($cval['f_type']==4){?>
				<div class="col-md-8">
				<textarea class="form-control input-sm checkInput RichText" rows="5" placeholder="" name="episode[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$movieDetails[0][$cvalue]; ?></textarea>
				</div>
			<?php }?>
		</div>
		<?php if($cval['f_id']=='mname'){?>
		<div id="plink"></div>
		<?php }?>

<?php }
}else{?>
<div class="form-group">
    <label for="movie_name" class="col-md-4 control-label">Content Name<span class="red"><b>*</b></span>:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm checkInput" <?php echo $disable; ?>  id="content_name" name='episode[content_name]' <?php if (@$editflag) { ?><!--disabled="true"--><?php } ?> required  >
                        <option value="">Select</option>
                            <?php
                            foreach ($shows AS $key => $val) {
                                if(array_key_exists($key, $langcontent['film'])){
                                    $val = $langcontent['film'][$key]->name;
                                }
                                $selected = '';
								$selContentId = $movieDetails[0]['id'] ?$movieDetails[0]['id']:@$content_id; 
                                if ($key == $selContentId) {
                                    $selected = "selected='selected'";
                                }
                                echo '<option value="' . $key . '" ' . $selected . ' >' . $val . '</option>';
                            }
                            ?>
                </select>
            </div>
        </div>
        <span class="help-block with-errors"></span>
    </div>
</div>
<?php 
if($this->studio->id < 3710){$old = 1;}else{$old=0;}
?>
<?php if($old){?>
<div class="form-group">
    <label for="movie_name" class="col-md-4 control-label">Season#:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <div class="select">
                <select class="form-control input-sm checkInput"  id="sereies_number" <?php echo $disable; ?> name='episode[series_number]'  <?php if (@$editflag) { ?><!--disabled="true"--><?php } ?> >
                        <option value="">Select</option>
                            <?php
                            for ($i = 1; $i < 100; $i++) {
                                $selc = '';
                                if (isset($movieDetails[0]['series_number']) && $movieDetails[0]['series_number'] && ($movieDetails[0]['series_number'] == $i)) {
                                    $selc = "selected='selected'";
                                }
                                echo '<option value="' . $i . '" ' . $selc . ' >' . $i . '</option>';
                            }
                            ?>
                </select>
            </div>
        </div>
    </div>
</div>


<div class="form-group">
    <label for="episode#" class="col-md-4 control-label">Episode#:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <input type="number" placeholder="Enter episode number..." id="episode_number" name='episode[episode_number]' class="checkInput form-control input-sm" value="<?php echo @$movieDetails[0]['episode_number']; ?>" onkeypress="return isNumber(event);" <?php echo $disable; ?>>
        </div>
    </div>
</div>
<?php }?>
<div class="form-group">
    <label for="title" class="col-md-4 control-label">Title<span class="red"><b>*</b></span>:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <input type="text" placeholder="Enter episode title..." id="title" name='episode[title]' class="checkInput form-control input-sm" value="<?php echo @$movieDetails[0]['episode_title']; ?>" required="" >
        </div>
        <span class="help-block with-errors"></span>
    </div>
</div>
<div class="form-group">
    <label for="story" class="col-md-4 control-label">Story:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <textarea placeholder="Enter episode story..." rows="3" class="form-control input-sm checkInput" name="episode[story]"  id="story1"><?php echo @$movieDetails[0]['episode_story']; ?></textarea>
            <span class="countdown1"></span>
        </div>
    </div>
</div>
<?php if($old){?>
<div class="form-group">
    <label for="date" class="col-md-4 control-label">Episode Date:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="episode_date" name="episode[episode_date]" value="<?php echo isset($movieDetails[0]['episode_date']) ? date('m/d/Y', strtotime($movieDetails[0]['episode_date'])) : ''; ?>" class="form-control input-sm checkInput" <?php echo $disable; ?>>
        </div>
    </div>
</div>
<?php }}?>
  
<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="content_publish_date" id="content_publish_date" <?php if (@$movieDetails[0]['content_publish_date']) { ?>checked="checked"<?php } ?> <?php echo $disable; ?>/>
                <i class="input-helper"></i> Make it Live at Specific time
            </label>
        </div>
        <div class="row m-t-10 <?php if (@$movieDetails[0]['content_publish_date']) {} else { ?>content_publish_date_div<?php } ?>" id="content_publish_date_div" <?php if (@$movieDetails[0]['content_publish_date']) { ?>style=""<?php }else{ ?> style="display:none;"<?php } ?>>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="publish_date" value="<?php
if (@$movieDetails[0]['content_publish_date']) {
    echo date('m/d/Y', strtotime(@$movieDetails[0]['content_publish_date']));
}
?>" <?php echo $disable; ?>>
                    </div>
                    <span class="input-group-addon"><i class="icon-calendar"></i></span>

                </div>



            </div>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="publish_time" value="<?php
if (@$movieDetails[0]['content_publish_date']) {
    echo date('H:i', strtotime(@$movieDetails[0]['content_publish_date']));
}
?>" type="text" <?php echo $disable; ?>>
                    </div>
                    <span class="input-group-addon"><em class="icon-clock"></em></span>

                </div>



            </div>
            <div class="col-sm-2">
                <label for="content_publish_date">UTC</label>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" value="<?php echo @$editflag; ?>" name="editflag" id="editflag"/>
<div class="form-group m-t-30">
    <div class="col-md-offset-4 col-md-8">
        <button class="btn btn-primary movie_save_btn waves-effect btn-sm" id="save-btn" type="submit" value="1" name="submit_btn"><?php if (isset($editflag) && $editflag) { ?>Update<?php } else { ?>Save<?php } ?></button>&nbsp;<a href="<?php echo $this->createUrl('admin/managecontent'); ?>" class="btn btn-default">Cancel</a>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        var publish_date  = $("#publish_date").val();
        $('#content_publish_date').click(function(){
            0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show();
             $('#release_date').datepicker({minDate: '0'});
            $('#publish_date').datepicker({minDate: '0'}),

            $('#publish_time').flatpickr({
                enableTime: true,
                noCalendar: true,
                enableSeconds: false, // disabled by default
                time_24hr: true, // AM/PM time picker is used by default
                // default format
                dateFormat: "H:i",
                // initial values for time. don't use these to preload a date
                defaultHour: 12,
                defaultMinute: 0
            });
        });
	});
</script>
