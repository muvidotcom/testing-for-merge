<!--Add downloadable content-->
<?php if($IsDownloadable){?>
<div class="form-group">
	<label for="itemtype" class="col-md-4 control-label">Content option: </label>
	<div class="col-md-8">
		<div class="control-label row">
			<div class="col-sm-12">
				<label class="checkbox checkbox-inline m-r-20">
					<input value="1" <?php echo (in_array(@$data[0]['is_downloadable'], array(1,2)))?'checked=checked':'';?> name="download" type="checkbox" <?php if(@$data[0]['full_movie']!=''){echo 'disabled';}?>>
					<i class="input-helper"></i> Download
				</label>
				<label class="checkbox checkbox-inline m-r-20">
					<input value="1" <?php echo (in_array(@$data[0]['is_downloadable'], array(0,2)))?'checked=checked':'';?> name="stream" type="checkbox" <?php if(@$data[0]['full_movie']!=''){echo 'disabled';}?>>
					<i class="input-helper"></i> Stream
				</label>
			</div>
		</div>
	</div>
</div>
<?php }?>
<input type="hidden" id="is_check_custom" value="<?php if (@$customData){echo 1;}else{echo 0;}?>">
<?php 
$disable = "";
$enable_lang = 'en';
if(isset($data[0]['id'])){
    $enable_lang = $this->language_code;
	if ($_COOKIE['Language']) {
		$enable_lang = $_COOKIE['Language'];
	}
    $disable = 'disabled="disabled"';
    if(@$data[0]['language_id'] == $this->language_id && @$data[0]['parent_id']==0){
        $disable = "";
    }
}
$movie_id = @$data[0]['id'];
if(array_key_exists($movie_id, @$langcontent['film'])){
	@$data[0] = Yii::app()->Helper->getLanuageCustomValue(@$data[0],@$langcontent['film'][$movie_id]);
    @$data[0]['name'] = @$langcontent['film'][$movie_id]->name;
    @$data[0]['story'] = @$langcontent['film'][$movie_id]->story;
    @$data[0]['genre'] = @$langcontent['film'][$movie_id]->genre;
    @$data[0]['censor_rating'] = @$langcontent['film'][$movie_id]->censor_rating;
    @$data[0]['language'] = @$langcontent['film'][$movie_id]->language;
}
if(@$customData){
	$defaultFields = array('name','release_date','story');//'genre','censor_rating','language',
	$relationalFields = Yii::app()->Helper->getRelationalField(Yii::app()->user->studio_id); 
	$formData = $customData['formData'];
	unset($customData['formData']);
	foreach ($customData AS $ckey=>$cval){
		if(!in_array($cval['f_name'],$defaultFields)){
			if($relationalFields && array_key_exists($cval['f_name'],$relationalFields)){
				$cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
			}
		}
                $cvalue = ($cval['f_id']=='censor_rating')?'censor_rating':$cval['f_name'];
                if($cval['f_id']=='censor_rating'){$cval['f_name']='censer_rating';}
                if($cval['f_id']=='title'){$cval['f_is_required']=0;}
                if(in_array($cval['f_id'],array('censor_rating','genre','language'))){
                    $data[0][$cvalue] = (is_array(json_decode(@$data[0][$cvalue],true)) && !$cval['f_type'])?implode(',', json_decode(@$data[0][$cvalue],true)):@$data[0][$cvalue];
                }
		?>
<div class="form-group">
			<label for="<?= $cval['f_display_name'];?>" class="col-md-4 control-label"><?= $cval['f_display_name'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
			<?php if(!$cval['f_type']){?>
				<div class="col-md-8">
				<div class="fg-line">
				<?php if($cval['f_id']=='release_date'){?>
					 <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php
						if (@$data[0]['release_date']) {
							echo date('m/d/Y', strtotime($data[0]['release_date']));
						}
						?>" class="form-control input-sm checkInput" >
				<?php }else{?>
				<input type='text' placeholder="" id="<?= $cval['f_id'];?>" name="movie[<?= $cval['f_name'];?>]" value="<?php echo @$data[0][$cvalue]; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> <?php if(@$data[0]['id']=='' && $cval['f_name']=='name'){?> onblur="checkperma(this.value)"<?php } ?>>
				<?php } ?>
				</div>
				</div>	
			<?php }elseif($cval['f_type']==1){?>
				<div class="col-md-8">
				<textarea class="form-control input-sm checkInput" rows="5" placeholder="" name="movie[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$data[0][$cvalue]; ?></textarea>
				</div>
			<?php }elseif($cval['f_type']==2){?>
				<div class="col-md-8">
				<select name="movie[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
					<?php
						echo "<option value=''>-Select-</option>";
						$opData = json_decode($cval['f_value'],true);
						$opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
						foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
							if (@$data[0]['id'] && @$data[0][$cvalue]==$opvalue) {
								$selectedDd = 'selected ="selected"';
							}
							echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
						}
						?>
					</select>
				</div>
			<?php }elseif($cval['f_type']==3){?>				
                                    <div class="col-md-8">
                                    <select name="movie[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput">
                                    <?php
                                            $opData = json_decode($cval['f_value'],true);
						$opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
                                            foreach($opData AS $opkey=>$opvalue){
                                                $selectedDd = '';
                                                if (@$data[0]['id'] && in_array($opvalue, json_decode(@$data[0][$cvalue],true))) {
                                                        $selectedDd = 'selected ="selected"';
                                                }
                                                    echo "<option value='".$opvalue."'".$selectedDd.">" . $opvalue . "</option>";
                                            }
                                            ?>
                                    </select>
                                    </div>
			<?php }elseif($cval['f_type']==4){?>
				<div class="col-md-8">
				<textarea class="form-control input-sm checkInput RichText" rows="5" placeholder="" name="movie[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$data[0][$cvalue]; ?></textarea>
				</div>
			<?php }?>
		</div>
		<?php if($cval['f_id']=='mname'){?>
		<div id="plink"></div>
		<?php }?>

<?php }
}else{?>
<div class="form-group">
    <label for="movieName" class="col-md-4 control-label">Content Name<span class="red"><b>*</b></span>:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <input type='text' placeholder="Enter content name.." id="mname" name="movie[name]" value="<?php echo @$data[0]['name']; ?>" class="form-control input-sm checkInput" required <?php if(@$data[0]['id']==''){?> onblur="checkperma(this.value)"<?php } ?>>            
        </div>
        <div id="plink"></div>        
    </div>
</div>
<?php if ($content_types_id){ ?>
<div class="form-group">
    <label for="releaseDate" class="col-md-4 control-label">Release/Recorded Date:</label>
    <div class="col-md-8">
        <div class="fg-line">
            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php
            if (@$data[0]['release_date']) {
                echo date('m/d/Y', strtotime($data[0]['release_date']));
            }
            ?>" class="form-control input-sm checkInput" <?php echo $disable; ?>>
        </div>
    </div>
</div>
<?php }?>
<div class="form-group">
    <label for="genre" class="col-md-4 control-label">Story/Description: </label>
    <div class="col-md-8">
        <div class="fg-line">
            <textarea class="form-control input-sm checkInput" rows="5" placeholder="Enter story..." name="movie[story]" id="story1" ><?php echo @$data[0]['story']; ?></textarea>
            <span class="countdown1"></span>
        </div>
    </div>
</div>
<?php } ?>
<div class="form-group">
	<label for="category" class="col-md-4 control-label">Content Category<span class="red"><b>*</b></span>: </label>
	<div class="col-md-8">
        <div class="fg-line">
			<select  name="movie[content_category_value][]" id="content_category_value" multiple required="true" class="form-control input-sm checkInput" <?php echo $disable; ?> <?php if($issubCat_enabled){?>onchange="getSubCategoryList();"<?php }?>>
				<?php 
					foreach($contentCategories AS $k =>$v){
						if(@$data && in_array($k, explode(',', $data[0]['content_category_value']))){//($data[0]['content_category_value'] & $k)
							$selected = "selected='selected'";
						}else{
							$selected ='';
						}
						echo '<option value="'.$k.'" '.$selected.' >'.$v."</option>";
				}?>
			</select>
		</div>
            <span class="help-block with-errors"></span>
	</div>
</div>
<?php if($issubCat_enabled){
if(@$data && ($data[0]['content_category_value'])){
	@$contentSubCategories = Yii::app()->Helper->getSubcategoryList(@$data[0]['content_category_value'],Yii::app()->user->studio_id);
}?>
<div class="form-group">
		<label for="category" class="col-md-4 control-label">Sub Category: </label>
		<div class="col-md-8">
			<div class="loading_div_subcategory" style="display:none;">
				<div class="preloader pls-blue preloader-sm">
					<svg viewBox="25 25 50 50" class="pl-circular">
					<circle r="20" cy="50" cx="50" class="plc-path"/>
					</svg>
				</div>
			</div>
			<div class="fg-line" id="content_subcategory">
				<select  name="movie[content_subcategory_value][]" id="content_subcategory_value" multiple  class="form-control input-sm checkInput">
					<option value="">-Select-</option>
					<?php 
						foreach(@$contentSubCategories AS $k =>$v){
							if(@$data && ($data[0]['content_subcategory_value'] & (int)$v->subcat_binary_value)){
								$selected = "selected='selected'";
							}else{
								$selected ='';
							}
							echo '<option value="'.$v->subcat_binary_value.'" '.$selected.' >'.$v->subcat_name."</option>";
					}?>
				</select>
			</div>
			<span class="help-block with-errors"></span>
		</div>
	</div>
<?php }?>	
<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="content_publish_date" id="content_publish_date" <?php if (@$data[0]['content_publish_date']) { ?>checked="checked"<?php } ?> <?php echo $disable; ?>/>
                <i class="input-helper"></i> Make it Live at Specific time
            </label>
        </div>
        <div class="row m-t-10 <?php if (@$data[0]['content_publish_date']) {} else { ?>content_publish_date_div<?php } ?>" id="content_publish_date_div" <?php if (@$data[0]['content_publish_date']) { ?>style=""<?php }else{ ?> style="display:none;"<?php } ?>>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="publish_date" value="<?php
if (@$data[0]['content_publish_date']) {
    echo date('m/d/Y', strtotime($data[0]['content_publish_date']));
}
?>" <?php echo $disable; ?>>
                    </div>
                    <span class="input-group-addon"><i class="icon-calendar"></i></span>

                </div>



            </div>
            <div class="col-sm-5">
                <div class="input-group">
                    <div class="fg-line">
                        <input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="publish_time" value="<?php
if (@$data[0]['content_publish_date']) {
    echo date('H:i', strtotime($data[0]['content_publish_date']));
}
?>" type="text" <?php echo $disable; ?>>
                    </div>
                    <span class="input-group-addon"><em class="icon-clock"></em></span>

                </div>



            </div>
            <div class="col-sm-2">
                <label for="content_publish_date">UTC</label>
            </div>
        </div>
    </div>
</div>

<div class="form-group m-t-30">
    <div class="col-md-offset-4 col-md-8">
        <button type="submit" class="btn btn-primary btn-sm" id="save-btn"><?php if (@$data[0]['id']) { ?>Update Content<?php
            } elseif ($content_types_id == 2) {
                echo 'Save';
            } else {
                ?>Save & Continue<?php } ?>
		</button>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#content_publish_date').click(function(){
            0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show();

            $('#publish_time').flatpickr({
            enableTime: true,
            noCalendar: true,
            enableSeconds: false, // disabled by default
            time_24hr: true, // AM/PM time picker is used by default
            // default format
            dateFormat: "H:i",
            // initial values for time. don't use these to preload a date
            defaultHour: 12,
            defaultMinute: 0
        });
        });
    });
</script>