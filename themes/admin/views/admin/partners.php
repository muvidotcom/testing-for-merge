<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.tokenize.js"></script>
<div class="row m-b-40">
    <div class="col-sm-12">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="data[status]"  id="partners_status" data-manage="<?php echo $studio->id; ?>" data-name="Partner Portal" data-partners_type="1" <?php if (isset($studio->is_partners) && intval($studio->is_partners)) { ?>checked="checked" value="1" data-type="disable" <?php } else { ?> data-type="enable"<?php } ?> onclick="showConfirmPopup(this);" />
                <i class="input-helper"></i>Enable Content Partner
            </label>
        </div>
    </div>
    <?php if (isset($studio->is_partners) && intval($studio->is_partners)) { ?>
        <div class="col-sm-12">
            It will be available on <?php echo HTTP. $studio->domain . '/partners'; ?>
        </div>
    <?php } ?>
</div>

<div class="row m-b-40">
    <div class="col-sm-12">
        <div class="Block">
            
                <div id="partners_content" <?php if (isset($studio->is_partners) && intval($studio->is_partners)) { ?>style="display: block;"<?php } else { ?>style="display: none;" <?php } ?>>
                   <div class="row m-b-40">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary m-t-10" onclick="newpartner();">New Partner</button>
                        </div>
                    </div>


                    <table class="table">
                        
                            <?php if (isset($partners) && !empty($partners)) { 
                                ?>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email Address</th>
                                        <th data-hide="phone">Access to Content</th>
                                        <th data-hide="phone">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($partners as $key => $value) { ?>
                                    <tr>
                                        <td class="edit_name" id='<?php echo ucwords($value->id); ?>'>
                                            <span class='mainspn mainspn<?php echo $value->id; ?>'>
                                                <span class="edit_nm_spn" style="color:#41C0FF;cursor:pointer;" ><?php echo ucwords($value->first_name) . ' ' . ucwords($value->last_name); ?></span>
                                                <span class="edit_nm_link" style="display: none;">
                                                    <a href="javascript:void(0);" onclick="editName(this);" title="Edit Name">
                                                        <?php echo ucwords($value->first_name) . ' ' . ucwords($value->last_name); ?>
                                                    </a>
                                                </span>
                                            </span>
                                            <div class="edit_nm_input" id="edit_nm_input<?php echo $value->id; ?>" style="display: none;" >
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div id="partner_<?php echo $value->id . $value->studio_id; ?>-error" class="error" for="name" style="display: none;">Please enter name</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-7">
                                                        <div class="fg-line">
                                                            <input type="text"  id="partnerfirst_<?php echo $value->id . $value->studio_id; ?>" class="form-control input-sm" placeholder="First Name" value="<?php echo ucwords($value->first_name); ?>" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="fg-line">
                                                            <input type="text" id="partnerlast_<?php echo $value->id . $value->studio_id; ?>" class="form-control input-sm" placeholder="Last Name" value="<?php echo ucwords($value->last_name); ?>" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-md-offset-6 col-md-6 text-right m-t-20">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-sm"  onclick="cancelName('edit_nm_input<?php echo $value->id; ?>', event);" data-id="<?php echo $value->id; ?>" data-studio_id="<?php echo $value->studio_id; ?>" class="pull-right cancel-but" style="margin-right:10px;" >
                                                            Cancel
                                                        </a>
                                                        <a href="javascript:void(0);" class="btn btn-primary btn-sm" onclick="saveName(this);" data-id="<?php echo $value->id; ?>" data-studio_id="<?php echo $value->studio_id; ?>" class="pull-right">
                                                            Save
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </td>
                                        <td><?php echo $value->email; ?></td>
                                        <td>
                                            <?php if (isset($partners_content[$value->id]['contents']) && !empty($partners_content[$value->id]['contents'])) { ?>
                                            <?php 
                                            $content = '';
                                            foreach ($partners_content[$value->id]['contents'] as $key1 => $value1) { 
                                                $content.=', '. $value1['name'];
                                            }
                                            echo ltrim($content, ',');
                                            if (isset($partners_content[$value->id]['total']) && intval($partners_content[$value->id]['total']) > 2) {
                                                $cnt = $partners_content[$value->id]['total'] - 2;
                                                echo ' and '.$cnt.' others';
                                            }
                                            ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <h5><a href="javascript:void(0);" data-id="<?php echo $value->id; ?>" onclick="partnersContent(this);"><em class="icon-pencil"></em>&nbsp; Edit</a></h5>
                                            
                                            <h5><a href="javascript:void(0);" data-manage="<?php echo $value->id; ?>" data-name="partner '<?php echo ucwords($value->first_name); ?>'" data-type="delete" data-partners_type="0" onclick="showConfirmPopup(this);"><i class="icon-trash"></i>&nbsp; Remove</a></h5>
                                        </td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                            <?php } else { ?>
                                <tbody>
                                    <tr>
                                        <td colspan="4" class="error">No partners found</td>
                                    </tr>
                                </tbody>
                            <?php } ?>
                        
                    </table>
                </div>
            
        </div>
    </div>
</div>

<div id="newpartnerPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <form action="javascript:void(0);" class="form-horizontal" method="post" name="new_partner_form" id="new_partner_form" data-toggle="validator">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Company/Organization name for Partner</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="errors" class="col-sm-12 error red"></div>
                    </div>
                    
                    <div class="form-group" >
                        <label class="col-sm-4 control-label toper" for="First Name" > Name:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                            <input type="text" class="form-control input-sm" placeholder="Name" name="data[Admin][first_name]" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 toper" for="Email">Email:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                            <input type="text" class="form-control input-sm" placeholder="Email" name="data[Admin][email]" id="email" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label toper" for="Percentage" > Revenue Share:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" placeholder="Percentage Share" name="data[Admin][percentage_share]" id="percentage_share" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                    <label class="col-sm-4 control-label toper" for="" > Authorized for Content:</label>
                    <div class="col-sm-8">
                        
                                <select id="movie_id_new" name="data[movie_id]" class="form-control tokenize-sample" multiple="multiple">
                                   
                                </select>
                            
                        <input type="hidden" class="form-control" name="data[movie_ids]" id="movie_idsn" value="<?php echo $movie_ids;?>">
                    </div>
                </div>
                      
                    <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" value="1" name="data[p_partner_login]" id="p_partner_login" onclick="chk_partner_portal()" checked>
                      <i class="input-helper"></i>
                      Provide login access to Content Partner
                    </label>
                </div>
                
                   <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" value="1" name="data[p_all]" id="p_all" checked>
                      <i class="input-helper"></i>
                      Allow to add Content
                    </label>
                </div>
                      
                  
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="return validatePartner();" id="newpartner">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>	
    </div>
</div>

<!-- modal start here -->
<div class="modal fade" id="partnersModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" ><span id="headermodal"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal  partners_modal" name="partners_modal" id="partners_modal" method="post">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <span id="bodymodal"></span>
                            <input type="hidden" id="id_partners" name="id_partners" value="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:void(0);" id="partnersbtn" class="btn btn-default">Yes</a>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end here -->

<div class="modal fade" id="partnerspopup" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;"></div>

<script type="text/javascript" src="<?php echo ASSETS_URL;?>js/bootbox.js"></script>
<script type="text/javascript">
    function chk_partner_portal()
    {
    var chkpartner=document.getElementById('p_partner_login').checked;
    if(chkpartner==false)
    {
    $("#p_all").attr("checked", false);
    $("#p_all").attr("disabled", true);
    }
    else
    {
      document.getElementById('p_all').checked= true;
    $("#p_all").attr("disabled", false);   
    }
    }
    
      $(function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/movie_autocomplete";
        $('#movie_id_new').tokenize({
            datas: url,
            placeholder: 'Search contents',
            nbDropdownElements: 10,
            onAddToken:function(value, text, e){
                if (parseInt(value)) {
                    var video_ids = $("#movie_idsn").val();
                    video_ids = video_ids.split(",");
                    video_ids.push(value);
                    video_ids.toString();
                    $("#movie_idsn").val(video_ids);
                }
            },
            onRemoveToken:function(value, e){
                var video_ids = $("#movie_idsn").val();
                video_ids = video_ids.split(",");
                
                var index = video_ids.indexOf(value);
                if(index !== -1) {
                    video_ids.splice(index,1);
                }
                video_ids.toString();
               $("#movie_idsn").val(video_ids);
            }
        });
    });
    
    
    
    $(function () {        
        $("#partners_status").change(function() {
            if (parseInt($("#partners_status").val())) {
                $("#partners_status").prop( "checked", true);
            } else {
                $("#partners_status").prop( "checked", false);
            }
        });
    });
    
    function showConfirmPopup(obj) {
       // $("#partnersModal").modal('show');
        var type = $(obj).attr('data-type');
        var name = " "+$(obj).attr('data-name');
        var partners_type = " "+$(obj).attr('data-partners_type');
        
       // $("#headermodal").text(type.charAt(0).toUpperCase() + type.slice(1)+ name+"?");
        //$("#bodymodal").text("Are you sure you want to "+type+name+"?");
        
       // $("#partnersbtn").attr('data-manage', $(obj).attr('data-manage'));
        var manage_id = $(obj).attr('data-manage');
         swal({
                    title: type.charAt(0).toUpperCase() + type.slice(1)+ name+"?",
                    text: "Are you sure you want to "+type+name+"?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true,
                    html:true
                }, function() {
                    if (parseInt(partners_type) === 1) {
                        if(type=="enable"){
                            enablePartnersPortal(obj);
                        }else if(type=="disable"){
                            disablePartnersPortal(obj);
                        }
                   
                    
                } else {
                    deletePartner(obj);
                    
                }
                });
        
        
       /* var onclick =  "";
        if (parseInt(partners_type) === 1) {
            onclick = type+'PartnersPortal(this)';
            $("#partnersbtn").attr('data-type', type+"PartnersPortal");
        } else {
            onclick = type+'Partner(this)';
            $("#partnersbtn").attr('data-type', type);
        }
        
        $("#partnersbtn").attr('onclick',onclick).bind('click');*/
    }
    
    function disablePartnersPortal(obj) {
        $("#id_partners").val($(obj).attr('data-manage'));
        var action ="<?php echo Yii::app()->baseUrl; ?>/admin/disablePartnersPortal";
        $('#partners_modal').attr("action", action);
        document.partners_modal.submit();
    }
    
    function enablePartnersPortal(obj) {
        $("#id_partners").val($(obj).attr('data-manage'));
        var action ="<?php echo Yii::app()->baseUrl; ?>/admin/enablePartnersPortal";
        $('#partners_modal').attr("action", action);
        document.partners_modal.submit();
    }
    
    function deletePartner(obj) {
        $("#id_partners").val($(obj).attr('data-manage'));
        var action ="<?php echo Yii::app()->baseUrl; ?>/admin/deletePartner";
        $('#partners_modal').attr("action", action);
        document.partners_modal.submit();
    }
    
    function newpartner() {
        $("#newpartnerPopup").modal('show');
        $('label.error').remove();
        $('#new_partner_form')[0].reset();
        $('#email').on('change', function () {
            checkEmailExists(0);
        });
    }
    
    function validatePartner() {
        //$("#newpartner").html('Saving...');
        //$("#newpartner").attr("disabled", true);
        var validate = $("#new_partner_form").validate({
            rules: {
                'data[Admin][first_name]': "required",
                'data[Admin][email]': {
                    required: true,
                 
                },
                'data[Admin][email]': {
                    required: true,
                  
                }
            },
            messages: {
                'data[Admin][first_name]': "Please enter first name",
                'data[Admin][email]': {
                    required: "Please enter email address",
                    mail: "Please enter a valid email address"
                }
            },errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertAfter(element.parent());
            } 
        });
        var x = validate.form();

        if (x) {
            checkEmailExists(1);
        } else {
            $("#newpartner").html('Submit');
            $("#newpartner").removeAttr("disabled");
        }
    }
    
    function checkEmailExists(arg) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/checkEmailPartner";
        var email = $.trim($('#email').val());
        $.post(url, {'email': email}, function (res) {
            if (parseInt(res.isExists) === 1) {
                var msg = "Email is already registered.";
                $('#errors').html(msg).show();
                $("#newpartner").html('Submit');
                $("#newpartner").removeAttr("disabled");
                return false;
            } else {
                $('#errors').html('').hide();
                if (parseInt(arg)) {
                    
                 if (!$.trim($("#movie_id_new").val())) {
                swal("Oops! The given content doesn't exists in your studio");
                preventDefault();
                return false;
            }
                    var url = "<?php echo Yii::app()->baseUrl; ?>/admin/newPartner";
                    document.new_partner_form.action = url;
                    document.new_partner_form.submit();
                    return false;
                }
            }
        }, 'json');
    }
    
    function cancelName(id, obj) {
        $("#" + id).hide();
        obj.stopPropagation();
        $(".edit_nm_spn" + id).show();
        $(".edit_name").not(document.getElementById(id)).find(".mainspn").css("display", "block");
    }

    $('.edit_name').click(function () {
        if (!$(this).find('span.edit_nm_input').is(':visible')) {
            $(this).find(".mainspn").css("display", "none");
            $(this).find(".edit_nm_input").css("display", "block");
        }

    });
    
    function editName(obj) {
        $(".edit_nm_input").hide();
        $(".edit_nm_spn").show();
        $(obj).parent('span.edit_nm_link').hide();
        $(obj).parent('span.edit_nm_link').parent('td.edit_name').find('span.edit_nm_spn').hide();
        $(obj).parent('span.edit_nm_link').parent('td.edit_name').find('span.edit_nm_input').show();
    }

    function saveName(obj) {
        var partner_id = $(obj).attr('data-id');
        var studio_id = $(obj).attr('data-studio_id');
        var first_name = $('#partnerfirst_' + partner_id + studio_id).val();
        if ($.trim(first_name) !== '') {
            var last_name = $('#partnerlast_' + partner_id + studio_id).val();
            var url = "<?php echo Yii::app()->baseUrl; ?>/admin/savePartnerName";
            $.post(url, {"partner_id": partner_id, "studio_id": studio_id, "first_name": first_name, "last_name": last_name}, function (res) {
                if (res) {
                    location.reload();
                    return false;
                }
            });
        } else {
            $(".error").hide();
            $('#partner_' + partner_id + studio_id + '-error').show();
            $('#partnerfirst_' + partner_id + studio_id).focus();
        }
    }
    
    function partnersContent(obj) {
        var partner_id = $(obj).attr('data-id');
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/partnersContent";
        $.post(url, {"partner_id": partner_id}, function (res) {
            $("#partnerspopup").html(res).modal('show');
        });
    }
    
    function newpartner() {
        $("#newpartnerPopup").modal('show');
        $('label.error').remove();
        $('#new_partner_form')[0].reset();
        $('#email').on('change', function () {
            checkEmailExists(0);
        });
    }
    
    
</script>