
 <div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/admin/contenttype">
            <button type="submit" class="btn btn-primary btn-default m-t-10">
                    Add New
            </button>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
    <table class="table" id="example">
        <thead>
            <tr>
                <th>Content</th>
                <th>Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <?php
    if ($data) {
        $totalContent = count($data);
        ?>
        <tbody>
            <?php foreach ($data AS $key => $details) { ?>		
            <tr class="" id="tr_<?php echo $details['id']; ?>" >
                <?php
                $id = $details['id'];
                $content_id = $details['content_types_id'];
                ?>
                <td><?php echo $details['display_name']; ?> </td>
                <td>
                    <span><?php echo $details['name']; ?> </span><br/>
                </td>
                <td>
                    <p> <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/admin/contenttype?option=edit&id=<?php echo $details['id']; ?>&ctype_id=<?php echo $details['content_types_id']; ?>"><em class="icon-pencil"></em>&nbsp;&nbsp; Edit</a></p>
                    <?php if ($totalContent > 1) { ?>            
                    <p><a href="javascript:void(0);"  content_id="<?php echo $details['id']; ?>"  onclick="showConfirmPopup(this);" > <em class="icon-trash"></em>&nbsp;&nbsp; Remove</a></p>
                    <?php } ?>
                </td>
            </tr>

             <?php } ?>
        </tbody>
        <?php } else { ?>
            <tbody>
                <tr><td colspan="3">No content type found!</td></tr>
            <tbody>
       <?php } ?>
    </table>
    </div>
</div>

<div class="modal fade" id="contenttypemodal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:22px;" >Delete Content?</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <span>Are you sure you want to <b>remove the content</b> from Studio?</span> 
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0);" id="content_list" class="btn btn-default " content_id=""  onclick="removeContent(this);">Yes</a>
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.tablednd_0_5.js" type="text/javascript"></script>
<?php if ($data) { ?>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/jsnew/datatable_custom_paging.js"></script>
<script>
$(document).ready(function() {
oTable =  $('#example').DataTable( {
        "bInfo" : false,
        "bLengthChange": false,
         "iDisplayLength": 10,
         "bSort": true,
         <?php if($totalContent< 10){ ?>
         "paging":false,  
         <?php } ?>
        //"aLengthMenu": [[5, 10, -1], [5, 10, "All"]],
        createdRow: function ( row ) {
            $('td', row).attr('tabindex', 0);
        }
    } );
    
    $('#myInputTextField').keyup(function(){
      oTable.search($(this).val()).draw() ;
    });
});

    </script>
<?php } ?>
<!--Ordering of content Types...-->
<script type="text/javascript">
                        $(function () {
                            $(".tbl_repeat tbody,.dragdrop-sortable").tableDnD({
                                onDragClass: "myDragClass",
                                onDrop: function (table, row) {
                                    var key = 1;
                                    var orders = $.tableDnD.serialize();
                                    /*$('table tbody tr').each(function() {
                                     $(this).children('td:first-child').html(key++)
                                     });*/
                                    $.post('<?php echo $this->createUrl('admin/reorderStudioContentTypes'); ?>', {'orders': orders});
                                }
                            });
                        });
 
                        function showConfirmPopup(obj) {
                            swal({
                            title: "Delete Content?",
                            text: "Are you sure you want to <b>remove the content</b> from Studio?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                            confirmButtonText: "Yes",
                            closeOnConfirm: true,
                            html:true
                          },
                          function(){
                            
                            removeContent(obj);
                          });
                           // $("#contenttypemodal").modal('show');
                            //$("#content_list").attr('content_id', $(obj).attr('content_id'));
                        }

                        function removeContent(obj) {
                            var content_id = $(obj).attr('content_id');
                            if (parseInt(content_id)) {
                                var url = HTTP_ROOT + "/admin/removeContent/content_id/" + content_id;
                                window.location.href = url;

                            } else {
                                return false;
                            }
                        }

</script>