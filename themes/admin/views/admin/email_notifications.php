<link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/typeahead.bundle.js"></script>
<?php 
$selsupportdata = '';
if (isset($supportdata) && !empty($supportdata)) {
    $selsupportdata = json_encode($supportdata);
}
?>
<div class="row m-t-40">
    <div class="col-md-8">
        <form class="form-horizontal" role="form" id="email_settings" name="email_settings" method="post" action="<?php echo Yii::app()->baseUrl; ?>/admin/saveEmailNotifications">
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="welcome_email" name="email[welcome_email]"  value="0">
                            <input type="checkbox" id="welcome_email" class="email_settings_cls" name="email[welcome_email]" <?php if (isset($email_settings['welcome_email']) && intval($email_settings['welcome_email'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Welcome on Registration
                        </label>
                    </div>
                </div>
            </div>
            <?php if (Yii::app()->common->isPaymentGatwayAndPlanExists($this->studio->id)){?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="welcome_email_with_subscription" name="email[welcome_email_with_subscription]"  value="0">
                            <input type="checkbox" id="welcome_email_with_subscription" class="email_settings_cls" name="email[welcome_email_with_subscription]" <?php if (isset($email_settings['welcome_email_with_subscription']) && intval($email_settings['welcome_email_with_subscription'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Registration & Purchase Subscription
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="membership_subscription" name="email[membership_subscription]"  value="0">
                            <input type="checkbox" id="membership_subscription" class="email_settings_cls" name="email[membership_subscription]" <?php if (isset($email_settings['membership_subscription']) && intval($email_settings['membership_subscription'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Purchase subscription
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="subscription_cancellation" name="email[subscription_cancellation]"  value="0">
                            <input type="checkbox" id="subscription_cancellation" class="email_settings_cls" name="email[subscription_cancellation]" <?php if (isset($email_settings['subscription_cancellation']) && intval($email_settings['subscription_cancellation'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Cancel subscription
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="subscription_reactivate" name="email[subscription_reactivate]"  value="0">
                            <input type="checkbox" id="subscription_reactivate" class="email_settings_cls" name="email[subscription_reactivate]" <?php if (isset($email_settings['subscription_reactivate']) && intval($email_settings['subscription_reactivate'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Reactive subscription
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="subscription_renew" name="email[subscription_renew]"  value="0">
                            <input type="checkbox" id="subscription_renew" class="email_settings_cls" name="email[subscription_renew]" <?php if (isset($email_settings['subscription_renew']) && intval($email_settings['subscription_renew'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Recurring Billing
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="payment_failed" name="email[payment_failed]"  value="0">
                            <input type="checkbox" id="payment_failed" class="email_settings_cls" name="email[payment_failed]" <?php if (isset($email_settings['payment_failed']) && intval($email_settings['payment_failed'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Failure to pay
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="cancellation_card_fail" name="email[cancellation_card_fail]"  value="0">
                            <input type="checkbox" id="cancellation_card_fail" class="email_settings_cls" name="email[cancellation_card_fail]" <?php if (isset($email_settings['cancellation_card_fail']) && intval($email_settings['cancellation_card_fail'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription - Cancellation for card failure
                        </label>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php  if(Yii::app()->common->isPaymentGatwayAndPPVPlanExists($this->studio->id,0)){ ?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="ppv_subscription" name="email[ppv_subscription]"  value="0">
                            <input type="checkbox" id="ppv_subscription" class="email_settings_cls" name="email[ppv_subscription]" <?php if (isset($email_settings['ppv_subscription']) && intval($email_settings['ppv_subscription'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Pay Per View purchase
                        </label>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php  if(Yii::app()->common->isPaymentGatwayExists($this->studio->id)){ ?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="advance_purchase" name="email[advance_purchase]"  value="0">
                            <input type="checkbox" id="advance_purchase" class="email_settings_cls" name="email[advance_purchase]" <?php if (isset($email_settings['advance_purchase']) && intval($email_settings['advance_purchase'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Pre-Order
                        </label>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="video_upload_status" name="email[video_upload_status]"  value="0">
                            <input type="checkbox" id="video_upload_status" class="email_settings_cls" name="email[video_upload_status]" <?php if (isset($email_settings['video_upload_status']) && intval($email_settings['video_upload_status'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Video upload status
                        </label>
                    </div>
                </div>
            </div>
            <?php 
                $pgconfig = Yii::app()->general->getStoreLink();
            if ($pgconfig) {
                ?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="muvi_kart" name="email[muvi_kart_admin_notifications]"  value="0">
                            <input type="checkbox" id="muvi_kart" class="email_settings_cls" name="email[muvi_kart_admin_notifications]" <?php if (isset($email_settings['muvi_kart_admin_notifications']) && intval($email_settings['muvi_kart_admin_notifications'])==1) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Muvi kart Admin Notifications
                        </label>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if(Yii::app()->common->voucherAvailable($this->studio->id)){?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="voucher" name="email[voucher]"  value="0">
                            <input type="checkbox" id="voucher" class="email_settings_cls" name="email[voucher]" <?php if (isset($email_settings['voucher']) && intval($email_settings['voucher'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Voucher Redemption
                        </label>
                    </div>
                </div>
            </div>
            <?php }?>
           <?php  if(Yii::app()->common->isSubscriptionBundlesExists($this->studio->id)){ ?>
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="subscription_bundles" name="email[subscription_bundles]"  value="0">
                            <input type="checkbox" id="subscription_bundles" class="email_settings_cls" name="email[subscription_bundles]" <?php if (isset($email_settings['subscription_bundles']) && intval($email_settings['subscription_bundles'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Subscription Bundles
                        </label>
                    </div>
                </div>
            </div>
            <?php } ?>  
                
            <!--<div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="Support" name="email[Support]"  value="0">
                            <input type="checkbox" id="Support" class="email_settings_cls" name="email[Support]" <?php if (isset($email_settings['Support']) && intval($email_settings['Support'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Support
                        </label>
                    </div>
                </div>
            </div>-->
            <!--<div class="form-group">
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden" id="device_management" name="email[device_management]"  value="0">
                            <input type="checkbox" id="device_management" class="email_settings_cls" name="email[device_management]" <?php if (isset($email_settings['device_management']) && intval($email_settings['device_management'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>Device Management - Remove devices
                        </label>
                    </div>
                </div>
            </div>-->
            <div class="form-group">     
                <div class="col-md-12">
                    <div class="checkbox">
                        <label>
                            <input type="hidden"  name="email[translate_newkey_notification]"  value="0">
                            <input type="checkbox" id="translate_newkey_notification" class="email_settings_cls" name="email[translate_newkey_notification]" <?php if (isset($email_settings['translate_newkey_notification']) && intval($email_settings['translate_newkey_notification'])) { ?> checked="checked" value="1" <?php } else { ?> value="0"<?php } ?> /> <i class="input-helper"></i>New Translation Key Added
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class=" col-md-8 m-t-30">
                    <button type="submit" class="btn btn-primary btn-sm" >Save</button>
                </div>
            </div> 
        </form>        
    </div>
</div>
<div class="row m-t-20">
    <div class="col-md-12">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-docs icon left-icon "></em>
                </div>
                <h4>Email Setting</h4>     
                <hr/>
            </div>
            <div class="Block-Body">
                <form class="form-horizontal" role="form" id="email_settings" name="email_settings" method="post" action="<?php echo Yii::app()->baseUrl; ?>/admin/SaveEmailNotificationsOtherEmails" onsubmit="return checkEmailValid();">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Email Notifications Sent From:</h3>
                            <div class="form-group">
                                <label for="sent_from" class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="fg-line">
                                                <input type="email" name="notification_from_email" id="notification_from_email" class="form-control" placeholder="Enter Email Addess"  value="<?php echo $notification_from_email; ?>"/>
                                            </div>
                                            <label id="email-error" class="error red" for="email"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3 class="m-t-20">End User Notifications:</h3>
                            <div class="form-group">
                                <label for="end_user_action" class="col-sm-2 control-label">End User Actions</label>
                                <div class="col-sm-10">
                                  <div class="fg-line">
                    <select  data-role="tagsinput"  name="end_user_action[]" placeholder="Type to add email,press enter to select multiple email" id="end_user_action" multiple >
                                          <?php  
                                          if (isset($enduserdata) && !empty($enduserdata)) {            
                                              foreach ($enduserdata AS $val) {
                                                  echo "<option value='" . $val . "' selected='selected'>" . $val . "</option>";
                                              }
                                          }
                                          ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_us" class="col-sm-2 control-label">Contact Us</label>
                                <div class="col-sm-10">

                <div class="fg-line">
                    <select  data-role="tagsinput"  name="contact_us[]" placeholder="Type to add email,press enter to select multiple email" id="genre" multiple >
                        <?php
                        if (isset($contdata) && !empty($contdata)) {            
                            foreach ($contdata AS $k => $v) {
                                echo "<option value='" . $v . "' selected='selected'>" . $v . "</option>";
                            }
                        }
                        ?>
                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="support_tickets" class="col-sm-2 control-label">Support Tickets</label>
                                <div class="col-sm-10">
									<div class="fg-line">
<!--                                        <select  data-role="tagsinput" name="support_tickets[]" placeholder="Type to add email, press enter to select multiple email" id="support_tickets" multiple>
                                        </select>-->
                                        <select  data-role="tagsinput"  name="support_tickets[]" placeholder="Type to add email,press enter to select multiple email" id="support_tickets" multiple >
                                            <?php
                                            if (isset($supportdata) && !empty($supportdata)) {
                                                foreach ($supportdata AS $k => $v) {
                                                    echo "<option value='" . $v . "' selected='selected'>" . $v . "</option>";
                                                }
                                            }
                                            ?>
										</select>
									</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="othemail" value="othemail" class="btn btn-primary m-t-30">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".email_settings_cls").click(function(){
            if ($(this).is(':checked')) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
        });
    });
    function checkEmailValid(){
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var email_check = pattern.test($("#notification_from_email").val());
        if(email_check){
           $("#email_settings").submit(); 
        }else{
            $("#email-error").text("Please enter a valid email address.");
            return false;
        }
    }
</script>
<script type="text/javascript">

   var genre = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/GetAdminUsersEmail',
            filter: function (list) {
                return $.map(list, function (genre) {
                    return {name: genre};
                });
            }
        }
    });
    genre.clearPrefetchCache();
    genre.initialize();

    $('#genre').tagsinput({
        typeaheadjs: {
            name: 'genre',
            displayKey: 'name',
            valueKey: 'name',
            source: genre.ttAdapter()
    }
    });

   var end_user_action = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: HTTP_ROOT + '/admin/GetAdminUsersEmail',
            filter: function (list) {
                return $.map(list, function (end_user_action) {
                    return {name: end_user_action};
                });
            }
        }
    });
    end_user_action.clearPrefetchCache();
    end_user_action.initialize();

    $('#end_user_action').tagsinput({
        typeaheadjs: {
            name: 'end_user_action',
            displayKey: 'name',
            valueKey: 'name',
            source: end_user_action.ttAdapter()
    }
    });    
    
    var content = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"), queryTokenizer: Bloodhound.tokenizers.whitespace, prefetch: {url: HTTP_ROOT + "/admin/getAdminUsers", filter: function (e) {
        return e;
    }}});
    content.clearPrefetchCache(),
    content.initialize(),
    $("#end_user_action").tagsinput();

    /*var selsupportdata = '<?php echo $selsupportdata; ?>';*/
    $("#support_tickets").tagsinput({
        typeaheadjs: {name: "support_tickets", displayKey: "name", source: content.ttAdapter()}
    });

//    var contnts = $.trim(selsupportdata) ? jQuery.parseJSON(selsupportdata) : ''; console.log(contnts);
//    if (contnts.length) {
//        for (var i in contnts) {
//            var content_name = $.trim(contnts[i].name);
//            if (content_name) {
//                content_name = content_name.replace("u0027", "'");
//                $("#support_tickets").tagsinput('add', {"content_id": contnts[i].content_id, "name": content_name});
//            }
//        }
//    }
    </script>