<?php
$studio = $this->studio;
?>
<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="/admin/newtracking"><button class="btn btn-primary btn-default m-t-10">New Code</button></a>
    </div> 
</div>
<div class="row m-t-40">
    <div class="col-md-9">
        
            <form method="post" role="form" id="confFRM" class="form-horizontal" name="confFRM" action="<?php echo Yii::app()->getbaseUrl(admin); ?>/admin/advancedSetting" enctype="multipart/form-data">
                <div class="loading" id="subsc-loading"></div>
                <?php if ($data) {?>
                <h3 class="text-capitalize f-300">Tracking Codes</h3>
                    <table class="table mob-compress" id="movie_list_tbl">
                        <thead>
                            <tr>
                                <th class="min-width">NAME</th>
                                <th class="width">ACTIONS</th>
                                    </tr>
                        </thead>
                   <?php     

                    foreach ($data AS $key => $trackcode) {
                        $track_id = $trackcode['id'];
                        $track_name  = $trackcode['name'];
                        //Poster 
                        //$img_path = isset($poster_id)?POSTER_URL.'/system/posters/'.$poster_id."/thumb/".$details['poster']['poster_file_name']:$default_img;
                        ?>


                    <tr>
                        <td><?php echo $trackcode['name']; ?></td>
                        <td><a href="<?php echo $this->createAbsoluteUrl('admin/newtracking/',array('option' => 'edit', 'track_id' =>$track_id)); ?>"  data-toggle="tooltip" data-original-title="Edit" data-placement="top"><em class="icon-pencil"></em>&nbsp;Edit Code</a> </td>
                        <td><a href="javascript:void(0);" data-href="<?php echo $this->createAbsoluteUrl('admin/newtracking/',array('option' => 'delete', 'track_id' =>$track_id)); ?>" data-msg = 'Are you sure, you want to delete this tracking code?' class="delete_track_code"  data-toggle="tooltip" data-original-title="Edit" data-placement="top"><em class="icon-trash"></em>&nbsp;Delete Code</a> </td>
                    </tr>
                    <?php }?>
                    </table>
                <?php }?>
                <div class="form-group">
                    <label class="col-md-4 control-label">
                        Google Analytics Tracking ID: 
                        <h5 class="m-t-0"><a href="https://support.google.com/analytics/answer/1032385?hl=en" target="_blank" style=" font-size: 11px;">
                            Help locating your tracking id
                            </a></h5>
                    </label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 m-b-20">
                                <div class="fg-line">
                                <input type="text" name="google_analytics" id="google_analytics_text" class="ga-input form-control input-sm" placeholder="Enter tracking ID such as UA-11111111-1" value="<?php echo html_entity_decode($studio->google_analytics)?>" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="http://www.google.com/analytics/" target="_blank" id="analytic-btn" class="btn btn-default-with-bg btn-sm">View Analytics</a>
                                <div id="error"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Google Webmaster Verification Code:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                        <input type="text" name="google_webmaster" class="ga-input gw form-control input-sm" id="google_verification_code" placeholder="Enter verification code such as 99EzSUeJp3-Kwt26Jdeyf-R4CKBGnYXMl7KMLvQf3oY" value="<?php if($studio->additional_meta_contents !=''){echo html_entity_decode($studio->additional_meta_contents);}?>" />
                        </div>
                        <div id="error1"></div>
                    </div>
                </div> 
                 <div class="form-group">
                    <label class="col-md-4 control-label">Sitemap</label>
                    <div class="col-md-8">
                        <a href="http://<?php echo $studio->domain;?>/sitemap.xml" target="_blank" >http://<?php echo $studio->domain;?>/sitemap.xml</a>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Robots.txt</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                        <textarea class="form-control input-sm" name="robots_txt" id="robots-text" <?php echo ($studio->is_subscribed == 0) ? "readonly": ""; ?> placeholder="Content is updated to http://<?php echo $studio->domain;?>/robots.txt" row="5"><?php echo $studio->robots_txt;?></textarea>
                        </div> 
                        <?php if($studio->is_subscribed==0){ ?>
                        <div class="red">
                            *Robots.txt file update is not permitted during Free Trial
                        </div>
                        <?php } ?>
                        <input type="hidden" id="updaterobot" value="<?php if($studio->is_subscribed==0){echo 1;}else{echo 0;}?>">
                        <div id="robot_error" class="red"></div>
                    </div>
                </div>
                
                 <div class="form-group">
                     <div class="col-md-offset-4 col-md-8"><button type="button" id="update_btn" class="btn btn-primary btn-sm m-t-30">Update</button></div>
                </div>
                
            </form>
       
    </div>
</div>

<!--style>
    .ga-input{
        width: 80%;
    }
    #robots-text{
        width: 80%;
        height: 150px;
        margin-bottom: 10px;
    }
    .label-div{
        font-size: 13pt;
        font-weight: 500;
    }
    
    .m-top-btn{
            margin-top: 30px;
    }
    #analytic-btn,#analytic-btn:hover{
      color: #fff;
    }
</style-->
<script>
    
    function ContainsAny(str, items){
        for(var i in items){
            var item = items[i];
            if (str.indexOf(item) >= 0){
                return true;
            }
        }
        return false;
    }
    
    function validateGA(el) {
        var regexp = /(UA|YT|MO)-\d+-\d+/i;
        return regexp.test(el);
    }
    $(document).ready(function(){
        $('.delete_track_code').click(function(){
            var msg = $(this).attr('data-msg');
            var deleteurl = $(this).attr('data-href');
            swal({
                title: "Delete tracking code?",
                text: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html:true
            }, function() {
                window.location.href = deleteurl;
            });
        });
    
        $('#update_btn').click(function(){            
            /*if($('#updaterobot').val()==1){
                if($.trim($('#robots-text').val())!=""){
                    $('#robot_error').css('color','red');
                    $('#robot_error').html('Robots.txt file update is not permitted during Free Trial');
                    return false;
                }
            }*/
            var google_verification_code = $('#google_verification_code').val();
            var ga_id = $('#google_analytics_text').val();
            if($.trim(ga_id) && !(validateGA(ga_id))){
                $('#error').css('color','red');
                $('#error').html('Please enter ONLY the code such as UA-11111111-1');
            }else if(ContainsAny(google_verification_code, ["google", "site", "verification","code","=",":","webmaster"])){
                $('#error1').css('color','red');
                $('#error1').html('Please enter ONLY the code such as fsfZQXy915O2jQ1tXlJ1jwLQTl2sf0OaSA5UeI5qRDk');
                return false;
            }else{
                $('#error').html('');
                $('#error1').html('');
                $('#confFRM').submit();
            }
        });
    });
</script>