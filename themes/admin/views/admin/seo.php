<?php
$studio = $this->studio;
function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}
?>
<div class="row m-t-40">
    <div class="col-xs-12">
        <form id="seo-data" class="form-horizontal">
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-docs icon left-icon "></em>
                    </div>
                    <h4>Dynamic Pages</h4>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="f-400 m-b-20 grey">Content Pages</h4>
                        <input type="hidden" name="data[pagename][]" value="content" />
                        <div class="m-b-40">
            <?php if(isset($seocontentinfo)){ ?>
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[title][]" class="form-control input-sm" value="<?php if($seocontentinfo['title'] !=''){ echo $seocontentinfo['title'];}else{ echo 'Watch $ContentName Online';}?>" />
                                    </div>
                                </div>
                            </div>        
                            <div class="form-group">
                                <label for="desc" class="col-md-4 control-label">Description:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <textarea name="data[description][]" class="form-control input-sm" rows="5" ><?php if($seocontentinfo['description'] !=''){ echo $seocontentinfo['description'];}else{ echo 'Watch $ContentName Online Now';}?></textarea>
                                    </div>
                                </div>
                            </div>           
                            <div class="form-group">
                                <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[keywords][]" class="form-control input-sm" value="<?php if($seocontentinfo['keywords'] !=''){ echo $seocontentinfo['keywords'];}else{ echo '$ContentName';}?>" />
                                    </div>
                                </div>    
                            </div>          
            <?php }?>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                    <?php 
                    if(isset($seoinfo) && count($seoinfo)){
                        $a = 0;
                        foreach ($seoinfo AS $info){
                            if($info['page'] == 'content-page'){
                                $a++;                                          
                            }     
                        } 
                    }
                        if(isset($a) && $a > 0){
                    ?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-content-page" checked="checked" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" id="dynamic-content-page-modal" href="javascript:void(0)">Set meta tags</a>
                                    </div>
                        <?php }else{?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-content-page" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" id="dynamic-content-page-modal" style="display:none" href="javascript:void(0)">Set meta tags</a>
                                    </div>
                        <?php }?>
                                </div>
                            </div> 
                        </div>
                            
                        <h4 class="f-400 m-b-20 grey">Content Listing Pages</h4>
                            
                        <input type="hidden" name="data[pagename][]" value="contentlisting" />
                        <div class="m-b-40">
               <?php if(isset($seocontentinfo)){ ?>
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[title][]" class="form-control input-sm" value="<?php if($seocontentlistinginfo['title'] !=''){ echo $seocontentlistinginfo['title'];}else{ echo 'Watch $ContentType Online';}?>" />
                                    </div>
                                </div>
                            </div>        
                            <div class="form-group">
                                <label for="desc" class="col-md-4 control-label">Description:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <textarea name="data[description][]" class="form-control input-sm" rows="5" ><?php if($seocontentlistinginfo['description'] !=''){ echo $seocontentlistinginfo['description'];}else{ echo 'Watch $ContentType Online Now';}?></textarea>
                                    </div>
                                </div>
                            </div>           
                            <div class="form-group">
                                <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[keywords][]" class="form-control input-sm" value="<?php if($seocontentlistinginfo['keywords'] !=''){ echo $seocontentlistinginfo['keywords'];}else{ echo '$ContentType';}?>" />
                                    </div>
                                </div>
                            </div>  
            <?php }?>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                   <?php 
                        if(isset($seoinfo) && count($seoinfo)){
                            $b = 0;
                            foreach ($seoinfo AS $info){
                                if($info['page'] == 'content-listing-page'){
                                    $b++;                                          
                                }     
                            } 
                        }
                     ?>
                    <?php 
                        if(isset($b) && $b > 0){
                            
                    ?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-content-listing-page" checked="checked" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" id="dynamic-content-listing-page-modal" href="javascript:void(0);" >Set meta tags</a>
                                    </div>
                        <?php }else{?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-content-listing-page" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" href="javascript:void(0);" id="dynamic-content-listing-page-modal" style="display:none">Set meta tags</a>
                                    </div>
                        <?php }?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <?php if(Yii::app()->general->getStoreLink()){ ?> 
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="f-400 m-b-20 grey">Product Details Pages</h4>
                        <input type="hidden" name="data[pagename][]" value="store" />
                        <div class="m-b-40">
                            <?php if(isset($seostoreinfo)){ ?>
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[title][]" class="form-control input-sm" value="<?php if($seostoreinfo['title'] !=''){ echo $seostoreinfo['title'];}else{ echo 'Buy $ContentName Online';}?>" />
                                    </div> 
                                </div>
                            </div>        
                            <div class="form-group">
                                <label for="desc" class="col-md-4 control-label">Description:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <textarea name="data[description][]" class="form-control input-sm" rows="5" ><?php if($seostoreinfo['description'] !=''){ echo $seostoreinfo['description'];}else{ echo 'Buy $ContentName Online Now';}?></textarea>
                                    </div>
                                </div>
                            </div>           
                            <div class="form-group">
                                <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[keywords][]" class="form-control input-sm" value="<?php if($seostoreinfo['keywords'] !=''){ echo $seostoreinfo['keywords'];}else{ echo '$ContentName';}?>" />
                                    </div>
                                </div>    
                            </div> 
                            <?php } ?>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <?php 
                                    if(isset($seoinfo) && count($seoinfo)){
                                        $c = 0;
                                        foreach ($seoinfo AS $info){
                                            if($info['page'] == 'store-page'){
                                                $c++;                                          
                                            }     
                                        } 
                                    }
                                    if(isset($c) && $c > 0){
                                    ?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-store-page" checked="checked" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" id="dynamic-store-page-modal" href="javascript:void(0)">Set meta tags</a>
                                    </div>
                                     <?php }else{?>
                                    <div class="checkbox">
                                        <label>    
                                            <input type="checkbox" id="dynamic-store-page" /> 
                                            <i class="input-helper"></i> Set meta tags specific to each page
                                        </label>
                                    </div>
                                    <div class="m-t-10">
                                        <a class="non-a-tag" id="dynamic-store-page-modal" style="display:none" href="javascript:void(0)">Set meta tags</a>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div> 
             <?php if(isset($seostaticinfo)){
        $home_title = '$BusinessName: watch awesome videos';
        $home_description = 'Visit $BusinessName to watch premium videos';
        $home_keywords = '$BusinessName';
     foreach ($seostaticinfo AS $static){
         if($static['page'] == 'homepage'){
            $home_title = $static['title'];
            $home_description = $static['description'];
            $home_keywords = $static['keywords'];
         }
     }
    }
?>
            <div class="Block">
                <div class="Block-Header">
                    <div class="icon-OuterArea--rectangular">
                        <em class="icon-docs icon left-icon "></em>
                    </div>
                    <h4>Static Page</h4>
                        
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="f-400 m-b-20 grey">Home Pages</h4>
                        <input type="hidden" name="data[pagename][]" value="homepage" />
                        <div class="m-b-40">
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[title][]" class="form-control input-sm" value="<?php echo $home_title;?>" />
                                    </div>
                                </div>    
                            </div>        
                                
                            <div class="form-group">
                                <label for="description" class="col-md-4 control-label">Description:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <textarea name="data[description][]" class="form-control input-sm" rows="5" ><?php echo $home_description;?></textarea>
                                    </div>
                                </div>    
                            </div>           
                                
                            <div class="form-group">
                                <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="data[keywords][]" class="form-control input-sm" value="<?php echo $home_keywords;?>" />
                                    </div>
                                </div>
                            </div>  
                        </div>                
                  <?php
                      
            if(count($pages)){
                for($i = 0;$i < count($pages);$i++){ ?>
                    
                    
                        <h4 class="f-400 m-b-20 grey"><?php echo ucfirst($pages[$i]['pagetitle']);?> Pages</h4>
                        <input type="hidden" name="pagename[]" value="<?php echo seoUrl($pages[$i]['pagetitle'])?>" />
                        <input type="hidden" name="pageid[]" value="<?php echo seoUrl($pages[$i]['pageid'])?>" />
                        <div class="m-b-40">
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Title:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="title_footer_page[]" class="form-control input-sm" value="<?php if($pages[$i]['title'] != ''){echo $pages[$i]['title'];}else{echo '$PageName';}?>" />
                                    </div>
                                </div>    
                            </div>        
                            <div class="form-group">
                                <label for="desc" class="col-md-4 control-label">Description:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <textarea name="description_footer_page[]" class="form-control input-sm" rows="5" ><?php if($pages[$i]['description'] != ''){echo $pages[$i]['description'];}else{echo '$PageName';}?></textarea>
                                    </div>
                                </div>    
                            </div> 
                            <div class="form-group">
                                <label for="keyword" class="col-md-4 control-label">Keywords:</label>
                                <div class="col-md-8">
                                    <div class="fg-line">
                                        <input type="text" name="keywords_footer_page[]" class="form-control input-sm" value="<?php if($pages[$i]['keywords'] != ''){echo $pages[$i]['keywords'];}else{echo '$PageName';}?>" />
                                    </div>
                                </div>    
                            </div>            
                        </div>  
                            
                            
        <?php   }
            }
        ?>
                    </div>
                </div>
            </div>
        </form>    
        <div class="row">
            <div class="col-md-8">
                <form class="form-horizontal" action="<?php echo Yii::app()->getbaseUrl(true)?>/admin/seo" method="POST" id="seo-submit">
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            
                            
                            <button type="button" class="btn btn-primary btn-sm" id="seo-submit-btn"> &nbsp;&nbsp;Update&nbsp;&nbsp;  </button>
                                
                                
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
<div class="h-40"></div>
    
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/colorbox.css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/themes/admin/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
    jQuery.validator.addMethod("mail", function(value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
    }, "Please enter a correct email address");    
    $(document).ready(function(){
        $('#subsc-loading').hide();    
        
        $("#confFRM").validate({ 
            rules: {    
                contact_email: {
                    mail: true
                },          
            },         
            submitHandler: function(form) {
                $("#confFRM").submit();
            }                
        });
        
        $('.help-text').tooltip({
            position: {
                at: "center bottom-35",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                            .addClass( "help-text-box" )
                            .appendTo( this );
                }
            }
        });
    });
</script>
<script>
    $('#dynamic-content-page').click(function(){
        var check = $(this).is(":checked");
        if(check){
            $(this).parent().parent().next().children().show();
        }else{
            $(this).parent().parent().next().children().hide();
        }
    });
    $('#dynamic-store-page').click(function(){
        var check = $(this).is(":checked");
        if(check){
            $(this).parent().parent().next().children().show();
        }else{
            $(this).parent().parent().next().children().hide();
        }
    });
    $('#dynamic-content-page-modal').click(function(){
        $("#content-modal").modal('show');
    });
    $('#dynamic-store-page-modal').click(function(){
        $("#store-modal").modal('show');
    });
    
    $('#dynamic-content-listing-page').click(function(){
        var check = $(this).is(":checked");
        if(check){
            $(this).parent().parent().next().children().show();
        }else{
            $(this).parent().parent().next().children().hide();
        }
    });
    $('#dynamic-content-listing-page-modal').click(function(){
        $("#content-listing-modal").modal('show');
    });
</script>
<?php 
    if(count($contentTypes)){
        $contentTypeDD = '';
        for($i=0;$i<count($contentTypes);$i++){
            $id = $contentTypes[$i]["id"];
            $content = strtoupper($contentTypes[$i]['title']);
            $contentTypeDD .= '<option value="'.$id.'">'.addslashes($content).'</option>';
        }
    }
?>
    
<?php 
    if(count($allContent)){
        $contentDDs = '';
        for($i=0;$i<count($allContent);$i++){
            $id = $allContent[$i]["id"];
            $content = strtoupper($allContent[$i]['name']);
            $content=str_replace("\n","",$content);
            $contentDDs .= '<option value="'.$id.'">'.addslashes($content).'</option>';
        }
    }
    if(count($allProduct)){
        $productDDs = '';
        for($i=0;$i<count($allProduct);$i++){
            $id = $allProduct[$i]["id"];
            $product = strtoupper($allProduct[$i]['name']);
            $product=str_replace("\n","",$product);
            $productDDs .= '<option value="'.$id.'">'.addslashes($product).'</option>';
        }
    }
        
?>
    
<div class="modal fade"  id="content-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true)?>/admin/seoContentModal" method="POST" id="content-id"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Meta tags for each content page </h4>
                </div>
                <div class="modal-body" >
                    <div id="content-modal-body">
                        <div class="row">
			<?php if(isset($seoinfo) && count($seoinfo)){
                 $cc = 0;
     foreach ($seoinfo AS $info){
            if($info['page'] == 'content-page'){
                $cc++;
                 ?>			 
                            <div class="form-horizontal"> 
                                
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Content:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="content[]">
                                   <?php 
                                        if(count($allContent)){
                                            $contentDD = '';
                                            for($i=0;$i<count($allContent);$i++){
                                                $id = $allContent[$i]["id"];
                                                $content = strtoupper($allContent[$i]['name']);
                                                if($info['movie_id'] == $id){
                                                    $contentDD .= '<option value="'.$id.'" selected>'.addslashes($content).'</option>';
                                                }else{
                                                    $contentDD .= '<option value="'.$id.'">'.addslashes($content).'</option>';
                                                }
                                            }
                                        }
                                        echo $contentDD;
                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Title</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="title[]" class="form-control input-sm" value="<?php echo $info['title'];?>" />
                                            </div>
                                        </div>
                                    </div>        
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Description:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <textarea name="description[]" class="form-control input-sm" rows="5" ><?php echo $info['description'];?></textarea>
                                            </div>
                                        </div>
                                    </div>           
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Keywords:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="keywords[]" class="form-control input-sm" value="<?php echo $info['keywords'];?>" />
                                            </div>
                                        </div>
                                    </div>          
                                        
                                </div>
                                <div class="col-sm-12 text-right">
                                    <a href="javascript:void(0);" id="remove-more-content">
                                        <em class="icon-trash"></em>&nbsp;
                                        Remove
                                    </a>			 
                                        
                                        
                                </div>
                            </div>
                        <?php }}} ?>
                            
                        </div>	
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" id="count-content" value="<?php echo count($allContent)?>" />
                            <input type="hidden" id="content-count-click" value="<?php if($cc){ echo $cc;}else{ echo 0;}?>" />
                                
                            <a href="javascript:void(0);" id="add-more-content">
                                <em class="icon-plus"></em>
                                Add more Content
                            </a>
                        </div>					
                    </div>							
                </div>
                    
                    
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="sub-btn" data-dismiss="modal">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
            
    </div>
</div>
    
    
<div class="modal fade"  id="content-listing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true)?>/admin/seoContentListingModal" method="POST" id="content-listing-id"> 
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Meta tags for each content listing page </h4>
                </div>
                <div class="modal-body" >
                    <div id="content-type-modal-body">
                        <div class="row">
						  <?php if(isset($seoinfo) && count($seoinfo)){
                 $cl = 0;
     foreach ($seoinfo AS $info){
            if($info['page'] == 'content-listing-page'){
                $cl++;
                 ?>   
                            <div class="form-horizontal"> 
                                
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Content:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="content[]">
                                   <?php 
                                        if(count($contentTypes)){
                                            $contentTypeDD = '';
                                            for($i=0;$i<count($contentTypes);$i++){
                                                $id = $contentTypes[$i]["id"];
                                                $content = strtoupper($contentTypes[$i]['title']);
                                                if($info['content_types_id'] == $id){
                                                    $contentTypeDD .= '<option value="'.$id.'" selected>'.addslashes($content).'</option>';
                                                }else{
                                                    $contentTypeDD .= '<option value="'.$id.'">'.addslashes($content).'</option>';
                                                }
                                            }
                                        }
                                        echo $contentTypeDD;
                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Title</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="title[]" class="form-control input-sm" value="<?php echo $info['title'];?>" />
                                            </div>
                                        </div>
                                    </div>        
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Description:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <textarea name="description[]" class="form-control input-sm" rows="5" ><?php echo $info['description'];?></textarea>
                                            </div>
                                        </div>
                                    </div>           
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Keywords:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="keywords[]" class="form-control input-sm" value="<?php echo $info['keywords'];?>" />
                                            </div>
                                        </div>
                                    </div>          
                                        
                                </div>
                                <div class="col-sm-12 text-right">
                                    
                                    
                                    <a href="javascript:void(0);" id="remove-more-content-type">
                                        <em class="icon-trash"></em>&nbsp;
                                        Remove
                                    </a>
                                </div>
                            </div>
               <?php }}}?>	
                        </div>	
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" id="count-content-type" value="<?php echo count($contentTypes)?>" />
                            <input type="hidden" id="listing-count-click" value="<?php if($cl){ echo $cl;}else{ echo 0;}?>" />
                                
                            <a href="javascript:void(0);" id="add-more-content-type">
                                <em class="icon-plus"></em>
                                Add more Content Type
                            </a>
                        </div>					
                    </div>							  	
                        
                </div>
                    
                    
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="sub-btn-2" data-dismiss="modal">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
            
    </div>
</div>
<div class="modal fade"  id="store-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<?php echo Yii::app()->getbaseUrl(true)?>/admin/seoStoreModal" method="POST" id="store-id"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Meta tags for each store page </h4>
                </div>
                <div class="modal-body" >
                    <div id="product-modal-body">
                        <div class="row">
			<?php if(isset($seoinfo) && count($seoinfo)){
                 $dd = 0;
     foreach ($seoinfo AS $info){
            if($info['page'] == 'store-page'){
                $dd++;
                 ?>			 
                            <div class="form-horizontal"> 
    
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Products:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select class="form-control input-sm" name="store[]">
                                   <?php 
                                        if(count($allProduct)){
                                            $productDD = '';
                                            for($i=0;$i<count($allProduct);$i++){
                                                $id = $allProduct[$i]["id"];
                                                $product = strtoupper($allProduct[$i]['name']);
                                                if($info['movie_id'] == $id){
                                                    $productDD .= '<option value="'.$id.'" selected>'.addslashes($product).'</option>';
                                                }else{
                                                    $productDD .= '<option value="'.$id.'">'.addslashes($product).'</option>';
                                                }
                                            }
                                        }
                                        echo $productDD;
                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
    
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Title</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="title[]" class="form-control input-sm" value="<?php echo $info['title'];?>" />
                                            </div>
                                        </div>
                                    </div>        
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Description:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <textarea name="description[]" class="form-control input-sm" rows="5" ><?php echo $info['description'];?></textarea>
                                            </div>
                                        </div>
                                    </div>           
                                        
                                    <div class="form-group">
                                        <label for="inputPassword3" class="control-label col-sm-3">Keywords:</label>
                                        <div class="col-sm-9">
                                            <div class="fg-line">
                                                <input type="text" name="keywords[]" class="form-control input-sm" value="<?php echo $info['keywords'];?>" />
                                            </div>
                                        </div>
                                    </div>          
                                        
                                </div>
                                <div class="col-sm-12 text-right">
                                    <a href="javascript:void(0);" id="remove-more-product">
                                        <em class="icon-trash"></em>&nbsp;
                                        Remove
                                    </a>			 
                                        
                                        
                                </div>
                            </div>
                        <?php }}} ?>
                            
                        </div>	
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" id="count-product" value="<?php echo count($allProduct)?>" />
                            <input type="hidden" id="product-count-click" value="<?php if($dd){ echo $dd;}else{ echo 0;}?>" />
                                
                            <a href="javascript:void(0);" id="add-more-product">
                                <em class="icon-plus"></em>
                                Add more Product
                            </a>
                        </div>					
                    </div>							
                </div>
                    
                    
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="sub-btn-3" data-dismiss="modal">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
            
    </div>
</div>    
    
    
<script>
    // Content modal 
    $('#add-more-content').click(function(){
        var html_text = '';
        var click_count = $('#content-count-click').val();
        var content_count = $('#count-content').val();
        if(parseInt(click_count) <= parseInt(content_count)){
            html_text += '<div class="form-horizontal"><div class="row"> <div class="col-sm-12"> <div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Content Type</label> <div class="col-sm-9"><div class="fg-line"><div class="select"> <select class="form-control input-sm" name="content[]"> <?php echo $contentDDs;?> </select></div></div> </div></div><div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Title:</label> <div class="col-sm-9"><div class="fg-line"> <input type="text" name="title[]" class="form-control input-sm" placeholder="Title"/> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Description:</label> <div class="col-sm-9"><div class="fg-line"> <textarea name="description[]" class="form-control input-sm" placeholder="Description" rows="5" ></textarea> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Keywords:</label> <div class="col-sm-9"> <div class="fg-line"><input type="text" name="keywords[]" class="form-control input-sm" placeholder="Keywords"/> </div></div></div></div><div class="col-sm-12 text-right m-b-20"> <a href="javascript:void(0);" id="remove-more-content"> <i class="icon-trash" title="Remove"></i>&nbsp;Remove</a></div></div></div>';
            $('#content-modal-body').append(html_text);
            var count = parseInt(click_count)+1;
            $('#content-count-click').val(count);
        }else{
            swal('You have reached maximum number of Content you have added.');
        }
        
    });
    $('#add-more-product').click(function(){
        var html_text = '';
        var click_count = $('#product-count-click').val();
        var content_count = $('#count-product').val();
        if(parseInt(click_count) < parseInt(content_count)){
            html_text += '<div class="form-horizontal"><div class="row"> <div class="col-sm-12"> <div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Product Name</label> <div class="col-sm-9"><div class="fg-line"><div class="select"> <select class="form-control input-sm" name="store[]"> <?php echo $productDDs;?> </select></div></div> </div></div><div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Title:</label> <div class="col-sm-9"><div class="fg-line"> <input type="text" name="title[]" class="form-control input-sm" placeholder="Title"/> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Description:</label> <div class="col-sm-9"><div class="fg-line"> <textarea name="description[]" class="form-control input-sm" placeholder="Description" rows="5" ></textarea> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Keywords:</label> <div class="col-sm-9"> <div class="fg-line"><input type="text" name="keywords[]" class="form-control input-sm" placeholder="Keywords"/> </div></div></div></div><div class="col-sm-12 text-right m-b-20"> <a href="javascript:void(0);" id="remove-more-product"> <i class="icon-trash" title="Remove"></i>&nbsp;Remove</a></div></div></div>';
            $('#product-modal-body').append(html_text);
            var count = parseInt(click_count)+1;
            $('#product-count-click').val(count);
        }else{
            swal('You have reached maximum number of Product you have added.');
        }
        
    });
    $(document.body).on('click','#remove-more-product',function(){
        var click_count = $('#product-count-click').val();
        $(this).parent().parent().remove();
        var count = parseInt(click_count)-1;
        $('#product-count-click').val(count);
    });
    $(document.body).on('click','#remove-more-content',function(){
        var click_count = $('#content-count-click').val();
        $(this).parent().parent().remove();
        var count = parseInt(click_count)-1;
        $('#content-count-click').val(count);
    });
    
    // Content listing modal
    $('#add-more-content-type').click(function(){
        var html_text = '';
        var click_count = $('#listing-count-click').val();
        var content_count = $('#count-content-type').val();
        if(parseInt(click_count) <= parseInt(content_count)){
            html_text += '<div class="form-horizontal"><div class="row"> <div class="col-sm-12"> <div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Content Type</label> <div class="col-sm-9"><div class="fg-line"><div class="select"> <select class="form-control input-sm" name="content[]"> <?php echo $contentTypeDD;?> </select></div></div> </div></div><div class="form-group"> <label for="inputPassword3" class="control-label col-sm-3">Title:</label> <div class="col-sm-9"><div class="fg-line"> <input type="text" name="title[]" class="form-control input-sm" placeholder="Title"/> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Description:</label> <div class="col-sm-9"><div class="fg-line"> <textarea name="description[]" class="form-control input-sm" placeholder="Description" rows="5" ></textarea> </div></div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Keywords:</label> <div class="col-sm-9"> <div class="fg-line"><input type="text" name="keywords[]" class="form-control input-sm" placeholder="Keywords"/> </div></div></div></div><div class="col-sm-12 text-right m-b-20"> <a href="javascript:void(0);" id="remove-more-content"> <i class="icon-trash" title="Remove"></i>&nbsp;Remove</a></div></div></div>';
            $('#content-type-modal-body').append(html_text);
            var count = parseInt(click_count)+1;
            $('#listing-count-click').val(count);
        }else{
            swal('You have reached maximum number of Content Types you have added.');
        }
        
    });
    $(document.body).on('click','#remove-more-content-type',function(){
        var click_count = $('#listing-count-click').val();
        $(this).parent().parent().remove();
        var count = parseInt(click_count)-1;
        $('#listing-count-click').val(count);
    });
    
    $('#sub-btn').click(function(){
        var data_from = $('#seo-data').serialize();
        $.post(HTTP_ROOT+"/admin/updateSeoInfo",{'is_ajax':1,'data_from':data_from},function(res){
            if(res){
                $('#content-id').submit();
            }
        });
    });
    $('#sub-btn-2').click(function(){
        var data_from = $('#seo-data').serialize();
        $.post(HTTP_ROOT+"/admin/updateSeoInfo",{'is_ajax':1,'data_from':data_from},function(res){
            if(res){
                $('#content-listing-id').submit();
            }
        });
    });
    $('#sub-btn-3').click(function(){
        var data_from = $('#seo-data').serialize();
        var page = "store";
        $.post(HTTP_ROOT+"/admin/updateSeoInfo",{'is_ajax':1,'data_from':data_from,page_name:page},function(res){
            if(res){
                $('#store-id').submit();
            }
        });
    });
    $('#seo-submit-btn').click(function(){
        var data_from = $('#seo-data').serialize();
        $.post(HTTP_ROOT+"/admin/updateSeoInfo",{'is_ajax':1,'data_from':data_from},function(res){
            if(res){
                swal({
                    title: "Seo details updated successfully",
                    confirmButtonColor:"#2cb7f6"
                });
               // $('#seo-submit').submit();
            }
        });
    });
</script>