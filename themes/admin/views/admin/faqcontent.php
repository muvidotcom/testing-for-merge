<div class="row m-t-40 m-b-40">
    <div class="col-md-12">
        <form class="form-horizontal" method="post" name="Faq" id="Faq">
            <div class="form-group">
                <label class="control-label col-sm-2">Title:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <input type="text" name="title" class="form-control input-sm" value="<?php echo @$faq['title']; ?>" placeholder="Title" />
                    </div>
                </div>
            </div>
            <div class="form-group m-t-30">
                <label class="control-label col-sm-2">Content:</label>
                <div class="col-sm-10">
                    <div class="fg-line">
                        <textarea type="text" name="content" id="content" class="form-control input-sm" placeholder="Add Content Here" ><?php echo @$faq['content']; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group m-t-30">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-primary faqbtn" id="newFaq" name="newFaq" value="Submit"/>
                    <a href="javascript:void(0);" onclick="javascript: history.back();" id="back" class="btn btn-default faqbtn">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- TinyMce Editor Script Start -->
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true)?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        tinymce.init({
        selector: "#content",
        formats: {
            bold: {inline: 'b'},  
        },
        valid_elements : '*[*]',
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : false,
        menubar: false,
        height: 300,
        plugins: [
        'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'save table contextmenu directionality emoticons template paste textcolor'
        ],
        setup: function (editor) {
        editor.addButton('newmedia', {
         text: 'Add Image',
         title: 'Add image',
         icon: 'image',
         onclick: function() {
            $("#MediaModal").modal("show");
            $('#glry_preview').removeAttr('src width height');
            $('#imgtolib').removeAttr('src width height');
            $('#choose_img_name').val("");
            $("#InsertPhoto").text("Insert Image");
            $("#InsertPhoto").removeAttr("disabled");
            $("#cancelPhoto").removeAttr("disabled");
            $('.overlay').removeAttr('style');
            $("#browsefiledetails").text("No file selected");
            $("#tinygallery").load("<?php echo Yii::app()->getBaseUrl(true)?>/template/tinyGallery");
        } });
         },
        content_css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link newmedia | code",
        });
    });
 </script>
<!--- Validation start -->
<script type="text/javascript">
    $(function() {
        var validator = $("#Faq").submit(function() {
            tinyMCE.triggerSave();
            $('.faqbtn').attr('disabled','disabled');
        }).validate({
            ignore: "",
            rules: {    
                title: "required",
                content:"required"
            }, 
            messages:{
                title:'Please enter title',
                content: 'Please enter Content'
            },
            errorPlacement: function(label, element) {
                label.addClass('red');
                label.insertAfter(element.parent());
                $('.faqbtn').removeAttr('disabled');
            },           
        });
        validator.focusInvalid = function() {
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    console.log("Called" + toFocus.is("textarea"));
                    if (toFocus.is("textarea")) {
                        console.log("This is field");
                            tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                            toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                        // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
    });   
</script>    
