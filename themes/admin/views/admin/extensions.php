<div class="row m-t-40">
    <div class="col-xs-12">
        <div class="row m-b-40">
             <?php
                $c = 0;
                $cart_ext_name = 'Muvi Kart';
                foreach($data as $ext)
                {
                    $status = (in_array($ext->id, $active_extensions) == 1)?'Deactivate':'Activate';
                    $confirm_title = $status.' '.$ext->name.'?';
                    if($ext->name == $cart_ext_name && $status == 'Activate'){
                        $confirm_message = 'Pricing of '.$ext->name.' is 0.5% per transaction, i.e., 0.5% of all physical transactions (order placed). \nAre you sure to '.$status.'.';
                    }
                    else{
                        $confirm_message = 'Are you sure to '.$status.'?';
                        if($ext->name == $cart_ext_name){
                            $confirm_message .= " Be sure to remove Muvi Kart menu from menu section.";
                        }
                    }
                ?> 
            <div class="col-md-6">
                <div class="text-center padding-40 border-dotted m-b-20" style="min-height:200px;">                     
                     <form class="extfrm" id="frmPST_<?php echo $ext->id;?>" name="frmPST" method="post" action="<?php echo Yii::app()->getBaseUrl(true)?>/admin/marketplace">
                        <input type="hidden" name="option" value="status_change" />
                        <input type="hidden" name="extension_id" id="extension_id" value="<?php echo $ext->id;?>" />

                        <p class="lead m-b-0"><?php echo $ext->name;?></p>
                        <p class="grey"><?php echo $ext->ext_desc;?></p>
                        <?php if($ext->name != 'DRM'){?>
                         <p> 
							<a href="javascript:void(0);" class="btn btn-primary m-t-10" id="ext_btn_<?php echo $ext->id;?>">
							<?php echo $status; ?>
							</a>
                         </p>                               
                        <?php }?>           
                    </form>
                    
                 </div>
            </div>
               <script type="text/javascript">
                        $(document).ready(function(){
                            $('#ext_btn_<?php echo $ext->id;?>').click(function(){
                                swal({
                                    title: "<?php echo $confirm_title;?>",
                                    text: "<?php echo $confirm_message;?>",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: true
                                }, function() {
                                    
                                    $.ajax({
                                        url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/updatemarketplace",
                                        data: $('#frmPST_<?php echo $ext->id;?>').serialize(),
                                        method: 'post',
                                        dataType: 'json',
                                        success: function (result) {
                                            window.location.reload();
                                        }
                                    });  
                                });
                               
                            });
                        });                        
                    </script>
                <?php
                }                                    
                ?>
        </div>
    </div>
</div>
<p class="lead">More Apps coming soon!</p>