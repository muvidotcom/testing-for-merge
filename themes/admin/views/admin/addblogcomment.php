<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-body">
                <form role="form" id="frmPST" name="frmPST" method="post" action="<?php echo Yii::app()->getBaseUrl(true)?>/admin/addpost">
                    <input type="hidden" name="option" id="option" value="<?php echo (isset($comment->id) && $comment->id > 0)?'update':'add'?>" />
                    <input type="hidden" name="id" id="id" value="<?php echo (isset($comment->id))?$comment->id:0?>" />
                    <div class="form-group">
                        <label>Comment</label>
                        <textarea placeholder="Enter Comment" id="comment" name="comment" rows="3" class="form-control"><?php echo (isset($comment->comment))?$comment->comment:'';?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <div><label>Action</label></div>
                        <div class="col-md-1">
                            <div class="radio">
                                <label>
                                    <input type="radio" <?php echo (isset($comment->status) && $comment->status == 1)?'checked="checked"':'';?> value="save" id="save" name="status">
                                    Enable
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" value="save_publish" id="save_publish" name="status" <?php echo (isset($comment->status) && $comment->status == 1)?'':'checked="checked"';?>>
                                    Disable
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group"><button class="btn btn-primary" type="button" onclick="javascript: history.back();">Back</button> &nbsp; <button class="btn btn-primary" type="submit">Submit</button></div>
                </form>                
            </div>           
        </div>
    </div>
</div>
 
<script type="text/javascript">
$(document).ready(function(){
    
    $("#frmPST").validate({ 
        rules: {    
            comment: {
                required: true,
                minlength: 2
            },             
        },         
        submitHandler: function(form) {
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/manageblogcomment",
                data: $('#frmPST').serialize(),
                dataType: 'json',
                method: 'post',
                success: function (result) {
                    history.back(-1);
                }
            });  
        }                
    }); 
});
</script>  
