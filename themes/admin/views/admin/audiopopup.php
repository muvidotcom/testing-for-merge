<?php $base_cloud_url = Yii::app()->common->getAudioGalleryCloudFrontPath($studio_id);?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Upload Audio - <span class="" id="pop_movie_name"></span></h4>
        </div>
        <div class="modal-body">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a id="profile-tab"  href="#profile" aria-controls="Choose-From-Library" role="tab" data-toggle="tab">Choose from Library</a>
                    </li>
<!--                    <li role="presentation">
                        <a id="home-tab" href="#home"  aria-controls="Upload-Video" role="tab" data-toggle="tab">Embed from 3rd Party</a>
                    </li>-->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content"> 
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        <!--------------Upload Video-------------->
                        <div id="scroll_container" class="row is-Scrollable" style="height:500px;overflow-x:hidden;" >
                            <div class="_data">
                            <div class="col-xs-12 m-t-40 m-b-20">
                                <div class="form-horizontal">	
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label for="uploadVideo" class="control-label">Upload Method &nbsp;</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="hidden" value="?" name="utf8">
                                            <input type="hidden" name="movie_stream_id" id="movie_stream_id" value=""/>
                                            <input type="hidden" name="movie_id" id="movie_id" value=""/>
                                            <input type="hidden" name="movie_name" id="movie_name" value=""/>
                                            <input type="hidden" name="content_type" id="content_type" value=""/>
                                            <div class="fg-line">
                                                <div class="select">
                                                    <select name="upload_type" class="filter_dropdown form-control" id="filetype">
                                                        <option value="browes">From Computer</option>
                                                        <!--<option value="server">Server to Server Transfer</option>
                                                        <option value="dropbox">From Dropbox</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix m-b-20"></div>
                                        <div class="savefile" id="browes_div">
                                            <div class="col-md-2">&nbsp;</div>
                                            <div class="col-md-4">
                                                <input type="button" value="Browse File" onclick="click_browse();" class="btn btn-default-with-bg btn-sm" >
                                                <input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize();" >
                                            </div>
                                        </div>
                                        <div class="clear-fix"></div>
                                        <div class="savefile" id="server_div" style="display: none;">
                                            <div class="col-md-2">&nbsp;</div>
                                            <div class="col-md-4">
                                                <div class="fg-line">
                                                    <input type="url" pattern="" value="" class="form-control input-sm" name="server_url" id="server_url" placeholder="Path to the file on your server" required>
                                                </div>
                                                <span class="error red help-block" id="server_url_error"></span>
                                                <div style="width: 48%;float: left;">
                                                    <input type="text" name="username"  id="ftpusername" class="form-control input-sm" placeholder='Username if any'>
                                                    <label id="ftpusername-error" class="error" for="ftpusername" style="display: inline-block;"></label>
                                                </div>
                                                <div style="width: 48%;float: right;">
                                                    <input type="password" name="password" id="ftppassword" class="form-control input-sm" placeholder='Password if any'>
                                                    <label id="ftppassword-error" class="error" for="ftppassword"></label>
                                                </div>
                                                <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('');">Submit</button>
                                            </div>
                                        </div>
                                        <div class="savefile" id="dropbox_div" style="display: none;">
                                            <div class="col-md-2">&nbsp;</div>
                                            <div class="col-md-4">
                                                <div class="fg-line">
                                                    <input type="text" name="dropbox" id="dropbox" class="form-control" placeholder="Path to the file on dropbox">
                                                </div>
                                                <span class="error red help-block" id="dropbox_url_error"></span>
                                                <button class="btn btn-primary btn-sm" type="button" onclick="validateURL('dropbox_url_error');">Submit</button>
                                            </div>
                                        </div>
                                        <div class="clear-fix"></div>                                            
                                    </div>
<!--                                    <div class="form-group m-t-20">
                                        <div class="col-md-12">
                                            <div class="red">We recommends S3 Sync and FTP for faster upload</div>
                                            <ul style="padding-left: 12px;">
                                                <li>
                                                    <span style="font-weight: bold;">S3 Sync : </span>
                                                    Install a tool we provide in your server. Videos in the server will be automatically synced with Muvi's Video Library
                                                </li>
                                                <li>
                                                    <span style="font-weight: bold;">FTP : </span>
                                                    Use a traditional FTP client to upload to Muvi's Video Library
                                                </li>
                                            </ul>
											See <a target="_blank" href="https://www.muvi.com/help/uploading-videos#Muvi_Recommended">help article</a> for more details. <?php if(isset(Yii::app()->user->is_partner) && (Yii::app()->user->is_partner==1)){?> <?php }else{?> Please add a <a href="<?php echo Yii::app()->getbaseUrl(true) . "/ticket/ticketList"; ?>">support ticket</a> to use one of the above tools. <?php }?>
                                        </div>
                                    </div>-->
                                </div>		
                            </div>
                            <!----------------------------->
                            <div class="row m-b-20 m-t-20">
                                <div class="col-sm-2">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                        <div class="fg-line">
                                            <input type="text" id="search_video1" class="form-control fg-input input-sm" placeholder="Search" onkeyup="searchvideo('audio');">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group input-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="video_duration" id="video_duration" onchange="audio_filtered_list();">
                                                    <option value="0">Audio Duration</option>
                                                    <option  value="1">Less than 5 minutes</option>
                                                    <option  value="2">Less than 30 minutes</option>
                                                    <option  value="3">Less than 120 minutes</option>
                                                    <option  value="4">More than 120 minutes</option>                          
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group input-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="file_size" id="file_size" onchange="audio_filtered_list();">
                                                    <option value="0">File Size</option>
                                                    <option value="1">Less than 100MB</option>
                                                    <option value="2">Less than 1GB</option>
                                                    <option value="3">More than 1GB</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group input-group">
                                        <div class="fg-line">
                                            <div class="select" >
                                                <select class="form-control" name="is_encoded" id="is_encoded" onchange="audio_filtered_list();">
                                                    <option value="0"> Mapped to Content</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-2">
                                    <div class="form-group input-group">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="uploaded_in" id="uploaded_in" onchange="audio_filtered_list();">
                                                    <option value="0">Uploaded In</option>
                                                    <option value="1">This Week</option>
                                                    <option value="2">This Month</option>
                                                    <option value="3">This Year</option>
                                                    <option value="4">Before This Year</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-1">
                                    <div class="form-group input-group">
                                        <div class="fg-line">
                                            <a href="javascript:void(0);" class="btn btn-default" onclick="reset_filter()"> Reset Filter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                    <div class="preloader pls-blue text-center " >
                                        <svg class="pl-circular" viewBox="25 25 50 50">
                                        <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="row  Gallery-Row">
                                <div class="col-md-12 p-t-40 p-b-40" id="video_content_div">
                                    <ul class="list-inline text-left" id="all_gallery_audio">
                                        <?php
                                        foreach ($all_audios as $key => $val) {
                                            if ($val['audio_name'] == '') {
                                                $video_thumb_path = "/img/No-Image-Horizontal.png";
                                            } else {
                                                if ($val['thumb_image_name'] == '') {
                                                    $video_thumb_path = "/img/No-Image-Horizontal.png";
                                                } else {
                                                    $video_thumb_path = $base_cloud_url . "videogallery-image/" . $val['thumb_image_name'];
                                                }
                                                $orig_video_path = $base_cloud_url . $val['audio_name'];
                                            }
                                            ?> 
                                            <li>
                                                <div class="Preview-Block video_thumb">
                                                    <div class="thumbnail m-b-0">
                                                        <div class="relative m-b-10">
                                                            <input type="hidden" name="original_video<?php echo $val['id']; ?>" id=original_video<?php echo $val['id']; ?>" value="<?php echo $orig_video_path; ?>">
                                                            <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['audio_name']; ?>"    />
                                                            <img class="img" src="<?php echo $video_thumb_path; ?>"  alt="<?php echo "All_Image"; ?>" >
                                                            <div class="caption overlay overlay-white">
                                                                <div class="overlay-Text">
                                                                    <div>
                                                                        <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="addaudioFromAudioGallery('<?php echo $orig_video_path; ?>', '<?php echo $val['id']; ?>','<?php echo $val['audio_name']; ?>');" title="Select Video">
                                                                            <span class="btn btn-primary btn-sm">
                                                                                <em class="icon-check"></em>&nbsp; Select Audio
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <p style="font-size:12px !important;"  title="<?php echo $val['audio_name']; ?>"><?php echo strlen($val['audio_name']) > 40 ? substr($val['audio_name'], 0, 40) . ".." : $val['audio_name']; ?></p>
                                                    </div>


                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------Embed from 3rd Party------->
                    <div role="tabpanel" class="tab-pane" id="home">
                        <div class="row is-Scrollable m-b-20 m-t-20">
                            <div class="col-sm-12 m-t-40 m-b-20">
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label for="uploadVideo" class="control-label col-md-3">Video from 3rd Party Platform &nbsp;</label>
                                        <div class="col-md-4">
                                            <input type="hidden" name="movie_stream_id" id="movie_stream_id" value=""/>
                                            <input type="hidden" name="movie_id" id="movie_id" value=""/>
                                            <input type="hidden" name="movie_name" id="movie_name" value=""/>
                                            <div class="fg-line">
                                                <!--
                                                <input type="text" id="embed_url" class="form-control  input-sm" placeholder="Link from YouTube, Vimeo or another OVP" onkeyup="embedThirdPartyPlatform();">
                                                -->
                                                <input type="text" id="embed_url" class="form-control  input-sm" name="embedurl" required="" placeholder="Link from YouTube, Vimeo or another OVP"  onkeyup="embedThirdPartyPlatform();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <!--
                                            <input type="button" value="Save" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm">
                                            -->
                                            <input type="button" value="Save" id="save-btn" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm" >
                                        </div>
                                    </div>
                                    <span class="error red help-block" id="embed_url_error"></span>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <!-------End of Embed from 3rd party------>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
   
    var processing=false;
    var count=1;
    var totalvideo=<?php echo $totalcount; ?>;
    var maxscrollcount=16;
    var tempScrollTop, currentScrollTop = 0;
    
    $("#scroll_container").scroll(function(){
    console.log('inside');
    var video_duration =$("#video_duration").val();
    var file_size =$("#file_size").val();
    var is_encoded =$("#is_encoded").val();
    var uploaded_in =$("#uploaded_in").val();
    
        currentScrollTop = $("#scroll_container").scrollTop(); 
         
        if (tempScrollTop < currentScrollTop ){
            console.log('firststep');
            if(!processing && parseInt($("#scroll_container").scrollTop()) >= parseInt($("#scroll_container div._data").height())-parseInt($("#scroll_container").innerHeight())){         
              console.log('secondstep');
               processing =true;
               var search_text=$("#search_video1").val();
            var video_duration =$("#video_duration").val();
            var file_size =$("#file_size").val();
            var is_encoded =$("#is_encoded").val();
            var uploaded_in =$("#uploaded_in").val();
            $.ajax({
                type: "POST",
                url: HTTP_ROOT + "/admin/loadGalleryonScroll",
                data: {search_text:search_text,type: 'audio', page: count,video_duration: video_duration,file_size:file_size,is_encoded: is_encoded,uploaded_in: uploaded_in},
                cache: false,
                success: function(data){
                  console.log((parseInt(count)*parseInt(maxscrollcount))+"********"+parseInt(totalvideo));
                 if(parseInt(count)*parseInt(maxscrollcount)<parseInt(totalvideo))
                 {
                  console.log('thirdstep');
                  count=parseInt(count)+1;
                  $('#all_gallery_audio').append(data); 
                  processing =false;
                 }else 
                 {
                    
                   processing =true;
                 }
                 }
            });
        }           
      }
        tempScrollTop = currentScrollTop; 
    });
    
</script>