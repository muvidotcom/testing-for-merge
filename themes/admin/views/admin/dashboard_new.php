<style>
    .white-box{
        min-height:180px;
    }
    .verification_pop_up{
        min-height:50px !important;
        background-color:#FE642E;
    }    
</style>
<script src="<?php echo ASSETS_URL; ?>js/highcharts.js"></script>
<script src="<?php echo ASSETS_URL; ?>js/highcharts-more.js"></script>
<script src="<?php echo ASSETS_URL; ?>js/map.js"></script>
<script src="<?php echo ASSETS_URL; ?>js/data.js"></script>
<script src="<?php echo ASSETS_URL; ?>js/world.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true) ?>/css/jquery.jqplot.css" />
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jqplot.meterGaugeRenderer.js"></script>
<div class="Hero_Section padding m-b-10 m-t-10">
    <div class="row">
        <div class="col-xs-12">
            <?php
            if ($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) {
                $style = "";
            } else {
                $style = 'style="margin-top:85px"';
            }
            ?>
         
            <h1 class="text-center c-white text-capitalize m-t-30 text-wrap f-300" <?php echo $style; ?>>
                <span>Good <span id="timenow"></span>,</span>
                <br class="visible-xxs">
                <span> <?php echo Yii::app()->user->first_name ?>!</span>
            </h1>
            <h5 class="text-center c-white m-b-30">
                Welcome to <?php if(isset($this->master_name)){echo $this->master_name;}else{echo "Muvi";} ?>
            </h5>
            <div class="h-50 hidden-xxs"></div>
            <?php if ($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) { 
            if(Yii::app()->custom->isBlackoutDay() == 1){
            ?>
            <div id="blackday-ad" class="text-center"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/<?php echo Yii::app()->theme->baseUrl; ?>/img/black-friday-hor.jpg" /></div>
            <style>
                .Hero_Section{height: auto;}
            </style>
            <?php }} ?>
        </div>
    </div>
</div>
<div class="To_Do_Walkover m-b-40">

    <div class="row">
        <div class="col-xs-12">
            <div id="owl-Walkover" class="owl-carousel">
                <div class="col">
                    <div class="Walkover-Header p-l-40 m-b-40">
                        <div class="circle">
                            <p class="m-b-0">
                                1
                            </p>
                        </div>
                        <p class="m-b-0 f-700">
                            Configure Content Type
                        </p>
                    </div>
                    <div class="Walkover-Body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4  pull-right">
                                <img src="/img/Configure-Content-Type.png" alt="Configure Content Type">
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <p>
                                    Configure the
                                    <br> Content Category
                                    <br class="hidden-xs"> you will have 
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php if (Yii::app()->common->hasPermission('website', 'view')) { ?>
                        <div class="Walkover-Footer  m-t-40">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/category/manageCategory" class="btn btn-primary ">Configure Content Category</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col">
                    <div class="Walkover-Header p-l-40 m-b-40">
                        <div class="circle">
                            <p class="m-b-0">
                                2
                            </p>
                        </div>
                        <p class="m-b-0 f-700">
                            Add Content
                        </p>
                    </div>
                    <div class="Walkover-Body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4  pull-right">
                                <img src="/img/Add-Content.png" alt="Configure Content Type">
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <p>
                                    Add content
                                    <br> and see it in your
                                    <br class="hidden-xs"> Preview Website
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php if (Yii::app()->common->hasPermission('content', 'view')) { ?>
                        <div class="Walkover-Footer  m-t-40">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class="btn btn-primary" href="<?php echo Yii::app()->getBaseUrl(true) ?>/admin/managecontent">Add Content</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col">
                    <div class="Walkover-Header p-l-40 m-b-40">
                        <div class="circle">
                            <p class="m-b-0">
                                3
                            </p>
                        </div>
                        <p class="m-b-0 f-700">
                            Select Template
                        </p>
                    </div>
                    <div class="Walkover-Body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4  pull-right">
                                <img src="/img/Select-Template.png" alt="Configure Content Type">
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <p>
                                    Select your Template
                                    <br> And add your Logo
                                    <br class="hidden-xs"> and Banner
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php if (Yii::app()->common->hasPermission('website', 'view')) { ?>                
                        <div class="Walkover-Footer  m-t-40">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class="btn btn-primary" href="<?php echo Yii::app()->getBaseUrl(true) ?>/template">Select Template</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row pull-right">
    <a href="javascript:void(0);" title="Reordering Section" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/dashboard/divreorder');"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/setting.png"></a>
</div>
<?php
foreach ($ordering_section as $k => $v) {
    if ($v == 1) {
        ?>
        <div class="Hosting_Block">
            <div class="row">
                <div class="col-xs-12">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-cloud-upload icon left-icon "></em>
                        </div>
                        <h4>Hosting</h4>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div id="serverNstorage" class="col-lg-8">
                    
                </div>
                <div class="col-md-6 col-lg-4 m-b-40">
                    <div class="indicator m-t-20 m-b-40">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cdn.jpg" alt="map" class="img-responsive">
                    </div>
                    <div class="indicator-Desc" style="padding-left: 20px;">
                        <h5 class="text-capitalise f-500">Content Delivery Network (CDN)</h5>
                        <div class="info-block">
                            <ul style="padding-left: 20px;">
                                <li class=green>
                                    <span class="h5 green">Activated</span>
                                </li>
                                <li>
                                    CDN Used: Amazon Cloudfront
                                </li>
                                <li>
                                    Bandwidth Used:<span id="BandwidthUsed"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    if ($v == 2) {
        ?>

        <div class="Video_Block">
            <div class="row">
                <div class="col-xs-12">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-control-play icon left-icon"></em>
                        </div>
                        <h4>Video</h4>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-6 m-b-40" id="videoperdaydiv">

                </div>
                <div class="col-md-6 m-b-40" id="videowatchedhour">

                </div>
            </div>
        </div>

        <?php
    }
    if ($v == 3) {
        ?>

        <div class="Website_Block">
            <div class="row">
                <div class="col-xs-12">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-screen-desktop icon left-icon"></em>

                        </div>
                        <h4>Website</h4>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 m-b-40" id="uniquevisitordiv">

                </div>
                <div class="col-md-6 m-b-40" id="trafficgeo">

                </div>
            </div>
        </div>
        <?php
    }
    if ($v == 4) {
        ?>

        <div class="Revenue_Block">
            <div class="row">
                <div class="col-xs-12">
                    <div class="Block-Header">
                        <div class="icon-OuterArea--rectangular">
                            <em class="icon-rocket icon left-icon"></em>

                        </div>
                        <h4>Revenue</h4>
                    </div>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 m-b-40" id="totalrevenue">

                </div>
                <div class="col-md-6 m-b-40" id="totalregmeb">

                </div>
            </div>
        </div>
        <?php
    }
}
?>
<script type="text/javascript">
    $(document).ready(function () {

        var visitortime = new Date();
        var curtime = visitortime.getHours() + '.' + visitortime.getMinutes();

        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/admin/getdaytime",
            type: 'POST',
            data: {'timenow': curtime},
            dataType: 'json',
            success: function (data) {
                $('#timenow').html(data.msg);
            }
        });
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/BandwidthUsed", function (data) {
            $("#BandwidthUsed").html(data);
        });
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/TrafficGeography", function (data) {
            $("#trafficgeo").html(data);
        });
        /*$.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/StorageUsed", function (data) {
            $("#storageused").html(data);
            $('#storageloc').html('('+$('#storageaddr').html()+')');
        });
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/getserverloc", function (data) {
            $("#getserverloc").html(data);
            $('#storageloc').html('('+$('#storageaddr').html()+')');
        });*/
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/GetserverNstorage", function (data) {
            $("#serverNstorage").html(data);
        });
        /*** uniquevisitor  ***/
        $("#uniquevisitordiv").html('<div style="text-align:center;">' + $('.loaderDiv').html() + '</div>');
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/UniqueVisitor", function (data) {
            $("#uniquevisitordiv").html(data);
        });
        /*Total View video */
        $("#videowatchedhour").html('<div style="text-align:center;">' + $('.loaderDiv').html() + '</div>');
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/VideoViews", function (data) {
            $("#videoperdaydiv").html(data);
        });
        /*Total Watching duration*/
        $("#videowatchedhour").html('<div style="text-align:center;">' + $('.loaderDiv').html() + '</div>');
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/VideoDuration", function (data) {
            $("#videowatchedhour").html(data);
        });
        /*Total Revenue*/
        $("#totalrevenue").html('<div style="text-align:center;">' + $('.loaderDiv').html() + '</div>');
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/TotalRevenue", function (data) {
            $("#totalrevenue").html(data);
        });
        /*Total Registered Member*/
        $("#totalregmeb").html('<div style="text-align:center;">' + $('.loaderDiv').html() + '</div>');
        $.post("<?php echo Yii::app()->getBaseUrl(true) ?>/dashboard/TotalRegmember", function (data) {
            $("#totalregmeb").html(data);
        });
    });
</script>
<?php if ($this->subscription_popup == 0) { ?>
    <div  class="loaderDiv">
        <div class="block-body">
            <div class="preloader pls-blue">
                <svg viewBox="25 25 50 50" class="pl-circular">
                <circle r="20" cy="50" cx="50" class="plc-path"/>
                </svg>
            </div>
        </div>
    </div>
    <!-- Modal Starts Here -->
    <div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

                </div>
            </div>
        </div>
    </div>
    <!-- Modal End  -->
    <style type="text/css">
        .loaderDiv{position: absolute;left: 50%;top:25%;display: none;}
        .box.box-primary{border:none !important;}

        #example td{border:none;}
        #example th{border:none;}
    </style>
    <?php
}?>
     <script>
         $(document).ready(function () {
            var is_popoup='<?php echo Yii::app()->session['is_popup'];?>'; 
            if(parseInt(is_popoup)==1)
            {
               openinmodal('/payment/subscription/purchase'); 
            }
         });
         <?php Yii::app()->session['is_popup'] = 0;?>
    </script>
