<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


?>


<?php
$studio = $this->studio;
$id = $studio->s3bucket_id;
$lat = $allocated_bucket_details['0']['latitude'];
$lng = $allocated_bucket_details['0']['longitude'];
?>
<input type="hidden" id="old-server" value="<?php echo $id;?>" />
<div class="">
    <div class="box box-primary"><br />
        
        <form method="post" role="form" id="confFRM" name="confFRM" action="<?php echo Yii::app()->getbaseUrl(admin); ?>/admin/updateServerLocation">
            <div class="loading" id="subsc-loading"></div>
            <div class="row form-group">
                
                
                <div class="col-lg-12">
                     <div class="col-lg-3">
                    <label>Uploads from</label> 
                    </div>
                    <div class="col-lg-3"><label>
                     <?php 
                                for($i = 0; $i < count($all_avalilable_buckets); $i++){
                                    if($all_avalilable_buckets[$i]['id'] == $id){ ?>
                                        <div id="s3bucket" data-lat="<?php echo $all_avalilable_buckets[$i]['latitude'];?>" data-lng="<?php echo $all_avalilable_buckets[$i]['longitude'];?>"><?php echo $all_avalilable_buckets[$i]['region_name'];?></div>
                                <?php }
                                }?>
                    
                    
<!--                        <select class="filter_dropdown" name="s3bucket_id" id="s3bucket">
                            <?php 
                                for($i = 0; $i < count($all_avalilable_buckets); $i++){
                                    if($all_avalilable_buckets[$i]['id'] == $id){ ?>
                                        <option data-lat="<?php echo $all_avalilable_buckets[$i]['latitude'];?>" data-lng="<?php echo $all_avalilable_buckets[$i]['longitude'];?>" selected="true" value="<?php echo $all_avalilable_buckets[$i]['id'];?>"><?php echo $all_avalilable_buckets[$i]['region_name'];?></option>
                                <?php }else{?>
                                        <option data-lat="<?php echo $all_avalilable_buckets[$i]['latitude'];?>" data-lng="<?php echo $all_avalilable_buckets[$i]['longitude'];?>" value="<?php echo $all_avalilable_buckets[$i]['id'];?>"><?php echo $all_avalilable_buckets[$i]['region_name'];?></option>
                                <?php }
                                }?>
                        </select>
                    <br><br>
                    <label>Select the location from where most of your media uploads will be.<br> This is an optional step.</label> 
                    <br><br>
                    <div class="pull-left">
                        <button type="button" class="btn btn-primary" onclick="return saveServerLocation()">Save</button>
                    </div>-->
                   </label> 
                    </div>
                    <div class="col-lg-6">
                        <div id="map" style="height:300px;width: 100%;"></div>
                        
                    </div>
                </div>
<!--                
                
                
                <br><br><br><br>
                <div class="col-lg-10 col-lg-offset-1">
                    
                    <div class="col-lg-3">&nbsp;</div>
                    <div class="col-lg-4">
                        
                    </div>
                    <div class="col-lg-3">&nbsp;</div>
                    
                </div>-->
            </div>                 
            <br /><br />
        </form>         
    </div>        
</div>
<style>
.toggle-editor, #address_switch {
    display: none;
}
</style>

<script>
    function saveServerLocation(){
       
        var s3bucket = $('#s3bucket').val();
        var oldserver = $('#old-server').val();

//        if(s3bucket == oldserver){
//            return false;
//        }
//        if(s3bucket != oldserver){
            var answer = confirm("Are you sure? All your videos may get deleted !");
            if(answer){
                $("#confFRM").submit();
                
            }
        //}
    }
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
    google.maps.event.addDomListener(window, 'load', function() {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      center: new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lng;?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker1 = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lng;?>)
    });

    google.maps.event.addListener(marker1, 'click', onMarkerClick);
  });
  
  function bucketRegion(lat,lng){
  var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      center: new google.maps.LatLng(lat,lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker1 = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(lat, lng)
    });

    google.maps.event.addListener(marker1, 'click', onMarkerClick);
  }
  
  $('.filter_dropdown').change(function(){
     var lat = $('option:selected', this).attr('data-lat');
     var lng = $('option:selected', this).attr('data-lng');
     bucketRegion(lat,lng);
  });
</script>
