<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.tokenize.js"></script>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="font-size:22px;" >Permissions for Partner <?php echo ucwords($user->first_name);?></h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" name="partners_conent_modal" id="partners_conent_modal" method="post" onsubmit="validatePartnerForm();">
                <div class="form-group" >
                        <label class="col-sm-4 control-label toper" for="First Name" > Name:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                            <input type="text" class="form-control input-sm" placeholder="Name" name="data[Admin][first_name]" value="<?php echo $user->first_name;?>" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>
                
                 <div class="form-group">
                        <label class="col-sm-4 toper" for="Email">Email:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm" placeholder="Email" name="data[Admin][email]" value="<?php echo $user->email;?>" id="email" disabled="" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>
                <input type="hidden" name="data[user_id]" value="<?php echo $user->id;?>" />
                  <div class="form-group" >
                        <label class="col-sm-4 control-label toper" for="First Name" > Revenue Share:</label>
                        <div class="col-sm-8">
                            <div class="fg-line">
                            <input type="text" class="form-control input-sm" placeholder="Percentage Share" name="data[Admin][percentage_share]"  value="<?php echo $movies[0]['percentage_revenue_share'];?>" autocomplete="off" required="true"/>
                            </div>
                        </div>
                    </div>  
                <div class="form-group">
                    <label class="col-sm-4 control-label toper" for="" > Authorized for Content:</label>
                    <div class="col-sm-8">
                        
                                <select id="movie_id" name="data[movie_id]" class="form-control tokenize-sample" multiple="multiple">
                                    <?php 
                                    $movie_ids = '';
                                    if(isset($movies) && !empty($movies)) {
                                        foreach ($movies as $key => $value) {
                                            $movie_ids.=','.$value['value'];
                                            ?>
                                            <option value="<?php echo $value['value'];?>" selected="selected"><?php echo $value['text'];?></option>
                                        <?php } ?>
                                    <?php }
                                    $movie_ids = ltrim($movie_ids, ',')?>
                                </select>
                            
                        <input type="hidden" class="form-control" name="data[movie_ids]" id="movie_ids" value="<?php echo $movie_ids;?>">
                    </div>
                </div>
              
                
                    <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" value="1" name="data[ep_partner_login]" id="ep_partner_login" onclick="echk_partner_portal()" <?php if($user->is_login_allowed==1){?> checked="checked" <?php } ?>>
                      <i class="input-helper"></i>
                      Provide login access to Content Partner
                    </label>
                </div>
                
                 <div class="checkbox m-b-15">
                    <label>
                        <input type="checkbox" value="1" name="data[ep_all]" id="ep_all" <?php if($user->permission_id==1){?>checked<?php }?>>
                      <i class="input-helper"></i>
                      Allow to add Content
                    </label>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Ok</button>
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
     function echk_partner_portal()
    {
    var chkpartner=$('#ep_partner_login').is(':checked');  
    if(chkpartner==false)
    {
    $("#ep_all").attr("checked", false);
    $("#ep_all").attr("disabled", true);
    }
    else
    {
      document.getElementById('ep_all').checked= true;
    $("#ep_all").attr("disabled", false);   
    }
    }
    $(function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/movie_autocomplete";
        $('#movie_id').tokenize({
            datas: url,
            placeholder: 'Search contents',
            nbDropdownElements: 10,
            onAddToken:function(value, text, e){
                if (parseInt(value)) {
                    var video_ids = $("#movie_ids").val();
                    video_ids = video_ids.split(",");
                    video_ids.push(value);
                    video_ids.toString();
                    $("#movie_ids").val(video_ids);
                }
            },
            onRemoveToken:function(value, e){
                
                var video_ids = $("#movie_ids").val();
               
                video_ids = video_ids.split(",");
                var index = video_ids.indexOf(value);
                if(index !== -1) {
                    video_ids.splice(index,1);
                }
                video_ids.toString();
                $("#movie_ids").val(video_ids);
            }
        });
    });
    
    function validatePartnerForm() {
        var validate = $("#partners_conent_modal").validate({
            rules: {
                'data[movie_id]': "required"
            },
            messages: {
                'data[movie_id]': "Please enter content"
            }
        });
        var x = validate.form();
        var hjj=$.trim($("#movie_ids").val());
             if (hjj=='') {
                swal("Oops! The given content doesn't exists in your studio");
                preventDefault();
                return false;
            }
        if (x) {
            var url = "<?php echo Yii::app()->baseUrl; ?>/admin/partnersContent";
            document.partners_conent_modal.action = url;
            document.partners_conent_modal.submit();
        }
    }
</script>