<?php
$studio = $this->studio;
?>
<div class="">
    <div class="box box-primary"><br />
        
        <form method="post" role="form" id="confFRM" name="confFRM" action="<?php echo Yii::app()->getbaseUrl(admin); ?>/admin/saveconf">
            <div class="loading" id="subsc-loading"></div>  
            <div class="row form-group">
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">Google Analytics Code</label>
                    <div class="col-lg-8"><textarea rows="8" class="form-control" name="google_analytics" id="google_analytics"><?php echo html_entity_decode($studio->google_analytics)?></textarea></div>                      
                </div><div class="clearfix"></div><br />
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">Studio Address<br /><code>Complete address</code></label>
                    <div class="col-lg-8">
                    <?php 
                    $this->widget('application.extensions.tinymce.ETinyMce', 
                        array(
                            'name'=>'address', 
                            'id' => 'address',
                            'height' => '100px',
                            'value' => html_entity_decode($studio->address),
                            'options' => array(
                                'theme' => 'advanced',
                                'skin' => 'default',
                                'plugins' => "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist", 
                                'theme_advanced_toolbar_location' => 'top',
                                'theme_advanced_toolbar_align' => 'left',
                                'theme_advanced_statusbar_location' => 'bottom',
                                'theme_advanced_resizing' => 'true',
                                
                             ),
                        )); 
                    ?>                         
                    </div>                      
                </div><div class="clearfix"></div><br />
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">Contact Email<br /><code>You will receive contact emails</code></label>
                    <div class="col-lg-8"><input class="form-control" type="text" name="contact_email" id="contact_email" value="<?php echo $studio->contact_us_email?>" required /></div>                      
                </div><div class="clearfix"></div><br />
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">Contact Phone</label>
                    <div class="col-lg-8"><input class="form-control" type="text" name="contact_phone" id="contact_phone" value="<?php echo $studio->phone?>" /></div>                      
                </div><div class="clearfix"></div><br />
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">Fax</label>
                    <div class="col-lg-8"><input class="form-control" type="text" name="contact_fax" id="contact_fax" value="<?php echo $studio->fax?>" /></div>                      
                </div><div class="clearfix"></div><br />        
                <div class="col-lg-10">
                    <label class="control-label col-lg-2">&nbsp;</label>
                    <div class="col-lg-8 pull-left"><button type="submit" class="btn btn-primary">Save Details</button></div>                 
                </div>                
                
            </div>                 
            <br /><br />
        </form>         
    </div>        
</div>
<style>
.toggle-editor, #address_switch {
    display: none;
}    
</style>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
jQuery.validator.addMethod("mail", function(value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
}, "Please enter a correct email address");    
$(document).ready(function(){
    $('#subsc-loading').hide();    
    
    $("#confFRM").validate({ 
        rules: {    
            contact_email: {
                mail: true
            },          
        },         
        submitHandler: function(form) {
            $("#confFRM").submit();
        }                
    });    
});
</script>