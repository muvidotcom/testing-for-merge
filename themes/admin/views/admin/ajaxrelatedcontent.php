<style>
    /* css :It's Re defined' */
.ui-widget-content{
    z-index: 1050 !important;
    height: 200px !important;
    overflow-y: scroll !important;
}
.ui-widget-content li:hover{
    background-color: #0081c2;
    background-image: linear-gradient(to bottom, #0088cc, #0077b3);
    background-repeat: repeat-x;
    color:white;
    text-decoration: none;
}
.ui-menu .ui-menu-item{
    padding: 5px;
}
.overlay-top-right{
    background-color: #fff;
    border-radius: 50%;
    height: 25px;
    position: absolute;
    right: 5px;
    top: -10px;
    width: 25px;
}
.overlay-top-right > a{
    font-size: 20px;
    opacity: 0.7;
}
</style>
<div class="modal-dialog modal-lg">
    <form action="<?php echo $this->createUrl('admin/AddRelatedContent'); ?>" method="post" id="add_content" class="form-horizontal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Manage Related Content</h4>
            </div>
            <div class="modal-body">
                <div class="form-group p-t-20" id="relatedcontentlist">
                    <?php foreach ($movies as $key => $value) {?>
                        <div class=' col-sm-2 text-center relative'>
                            <img class='img-responsive' src='<?=$value['image'];?>' />
                            <div class="overlay-top-right">                            
                                <a href="#" onclick="deleterelated('<?=$value['id'];?>',this,1,'')">
                                    <em class="icon-close"></em>
                                </a>                                   
                            </div>
                            <?=$value['name'];?>
                        </div>
                    <?php }?>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Name:</label>
                    <div class="col-sm-9">
                        <div class="fg-line">
                            <input type="text" id="title" class="form-control input-sm" name="title" placeholder="Start Typing content name...">
                        </div>
                        <div id="pcontent_poster relative">
                            <div id="pcontent_poster_loader" class="preloader pls-blue text-center" style="display: none;">
                                <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                </svg>                                   
                            </div>
                            <span class="help-block" id="preview_poster"></span>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right" style="display: none;" id="addtorelatedlist">
                        <button type="button" class="btn btn-primary" onclick="addtorelatedlist()" id="fcontent_add">Add</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="fcontent_save" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <input type="hidden" value="<?=$content_id?>" id="mastercontentid" name="mastercontentid" />
        <input type="hidden" value="<?=$content_stream_id?>" id="mastercontentstreamid" name="mastercontentstreamid" />
        <input type="hidden" value="" id="movie_id" name="content_id" />
        <input type="hidden" value="" id="is_episode" name="content_type" />
        <input type="hidden" value="" id="movieids" name="movieids" />
        <input type="hidden" value="" id="contenttypes" name="contenttypes" />
        <input type="hidden" value="<?=@$type?>" id="type" name="type" />
    </form>	
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#title').autocomplete({            
            source: function (request, response) {
                $.ajax({
                    url: HTTP_ROOT + '/admin/RelatedAutocomplete/movie_name?term=' + $('#title').val(),
                    dataType: "json",
                    method:"post",
                    data: {"movie_id":$('#mastercontentid').val(),"movieids":$('#movieids').val(),"movie_stream_id":$('#mastercontentstreamid').val()},
                    beforeSend: function (xhr) {
                        $('#fcontent_save').prop('disabled', true);
                        $('#fcontent_add').prop('disabled', true);
                        $('#pcontent_poster_loader').show();
                    },
                    success: function (data) {
                        if (data.length == 0) {
                            $('#movie_id').val('');
                            $('#preview_poster').html('');
                        } else {
                            response($.map(data.movies, function (item) {
                                return {
                                    label: item.movie_name,
                                    value: item.movie_id,
                                    id: item.is_episode
                                }
                            }));
                        }
                        $('#pcontent_poster_loader').hide();
                    }
                });
            },
            select: function (event, ui) {
                event.preventDefault();
                $("#title").val(ui.item.label);
                $.ajax({
                    url: HTTP_ROOT + "/admin/getPoster",
                    data: {'movie_id': ui.item.value, 'title': ui.item.label, 'is_episode': ui.item.id},
                    beforeSend: function (xhr) {
                        $('#fcontent_save').prop('disabled', true);
                        $('#fcontent_add').prop('disabled', true);
                        $('#pcontent_poster_loader').show();
                    },
                    success: function (res) {
                        $('#addtorelatedlist').show();
                        if (res.toLowerCase().indexOf("no-image.png") >= 0) {
                            $('#preview_poster').html("<img class='img-responsive' src='" + res + "' style='width:100px;' />")
                        } else {
                            $('#preview_poster').html("<img class='img-responsive' src='" + res + "'  style='width:100px;' /><br/><h6>If the above poster is correct, please click 'Save' to add this content to Related Content or You can Click 'add' for adding to list first before Save</h6>")
                        }
                        $('#pcontent_poster_loader').hide();
                        $('#fcontent_save').prop('disabled', false);
                        $('#fcontent_add').prop('disabled', false);
                    }
                });
            },
            focus: function (event, ui) {
                var lb = ui.item.label;
                $("#title").val(ui.item.label);
                $("#movie_id").val(ui.item.value);
                $("#is_episode").val(ui.item.id);
                event.preventDefault(); // Prevent the default focus behavior.
            }
        });
    });
    function addtorelatedlist(){
        var list = $('#preview_poster img').attr("src");
        var spn = "<div class='col-sm-2 text-center relative'><img class='img-responsive' src='" + list + "' /><div class='overlay-top-right'><a href='#' onclick='deleterelated("+$('#movie_id').val()+",this,0,"+$('#is_episode').val()+")'><em class='icon-close'></em></a></div>"+$("#title").val()+"</div>";
        var movieids = $('#movie_id').val()+","+$("#movieids").val();
        $('#movieids').val(movieids);
        var contenttypes = $('#is_episode').val()+","+$("#contenttypes").val();
        $('#contenttypes').val(contenttypes);
        $('#relatedcontentlist').append(spn);
        $('#addtorelatedlist').hide();
        $("#title").val('');
        $('#movie_id').val('');
        $('#preview_poster').html('');
    }
    function deleterelated(id,obj,flag,episode){
        swal({
            title: "Delete content?",
            text: "Are you want to delete content",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html:true
        }, function() {
            if(flag){
                var movie_id = $('#mastercontentid').val();
                var movie_stream_id = $('#mastercontentstreamid').val();
                var url = HTTP_ROOT + "/admin/DeleteRelated";
                $.post(url, {'id': id, 'movie_id':movie_id, 'movie_stream_id':movie_stream_id}, function (data) {
                    if(data){
                        var str = $('#movieids').val();
                        var res = str.replace(id+",", "");
                        var strc = $('#contenttypes').val();   
                        $('#movieids').val(res);
                        var res1 = strc.replace(episode+",", "");                          
                        $('#contenttypes').val(res1);                        
                        $(obj).parent().parent().remove();
                    }                
                }); 
            }else{
                var str = $('#movieids').val();            
                var strc = $('#contenttypes').val();            
                var res = str.replace(id+",", "");           
                $('#movieids').val(res);
                var res1 = strc.replace(episode+",", "");  
                $('#contenttypes').val(res1);
                $(obj).parent().parent().remove();
            } 
        });    
    }
</script>
