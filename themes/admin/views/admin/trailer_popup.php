<!-- Trailer Modal -->
<div class="modal fade" id="myTrailer" >
	<div class="modal-dialog" style="width: 650px;">
		<div class="modal-content">
			<div class="modal-header">
				<div aria-hidden="true" class="pull-right" data-dismiss="modal">
				  <img src="<?php echo Yii::app()->baseUrl; ?>/images/popup_close.png"/>
				</div>
				<div id="myModalLabel" style="color:#494949;font-size:20px;font-weight:bold;">
				  <?php  echo @$movieName." Trailer";?>
				</div>
			</div>
			<div class="modal-body" style="height: auto;">
			  <a href="#" id="video_player" style="display:block;height:300px;width:610px;"></a>
			</div>
			<div class="modal-footer">
			  <button aria-hidden="true" class="btn btn-primary bold_class" data-dismiss="modal">
				Done
			  </button>
			</div>
		</div>
	</div>
</div>
<!-- Trailer Modal End -->