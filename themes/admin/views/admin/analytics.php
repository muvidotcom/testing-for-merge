<?php
$studio = $this->studio;
?>
<div class="row m-t-40">
    <div class="col-sm-8">
        
        <form method="post" role="form" class="form-horizontal" id="confFRM" name="confFRM" action="<?php echo Yii::app()->getbaseUrl(admin); ?>/admin/saveganalytics">
            <div class="loading" id="subsc-loading"></div>
            
            <div class="form-group">
                <div class="col-sm-12">
                    <label>
                        Please insert your Google Analytics Tracking ID starting with UA in the text box provided below.<br>
                        <a href="https://support.google.com/analytics/answer/1032385?hl=en" target="_blank" >
                            Help locating your tracking id
                        </a>
                    </label>
                </div>
            </div>   
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="fg-line">
                            <input type="text" class="form-control input-sm" name="google_analytics" id="google_analytics" value="<?php echo html_entity_decode($studio->google_analytics)?>" />
                        </div>
                    </div>
                    <label class="col-sm-12 red" id="error-box"></label>
                </div>
               <div class="form-group">
                    <div class="col-sm-3">
                        e.g. - UA-11111111-1
                    </div>
               </div> 
                <div class="form-group">
                    <div  style="display: none" id="ga-box">
                        <label class="control-label col-sm-4">Preview of your Google Analytics Code</label>
                        <div class="col-sm-8" id="ga-code"></div>
                    </div>
                </div>
               
                <div class="form-group">
                    
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success m-t-20" id="gen-ga">Generate Google Analytics Code</button>
                        <button type="submit" class="btn btn-primary m-t-20">Save</button>
                        <button type="button" class="btn btn-warning m-t-20">
                            <a href="http://www.google.com/analytics/" target="_blank" id="analytic-btn">View Analytics</a>
                        </button>
                    </div>
                    
                </div>                
                
                             
            
        </form>         
    </div>        
</div>
<style>
.toggle-editor, #address_switch {
    display: none;
}
#error-box{
    color: red;
    display: none;
}
#analytic-btn{
      color: #fff;
}
#analytic-btn:hover{
      color: #fff;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/admin/css/colorbox.css" />
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/themes/admin/js/jquery.colorbox.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/js/jquery.validate.min.js"></script>
<script type="text/javascript"> 
jQuery.validator.addMethod("mail", function(value, element) {
    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
}, "Please enter a correct email address");    
$(document).ready(function(){
    $('#subsc-loading').hide();    
    
    $("#confFRM").validate({ 
        rules: {    
            contact_email: {
                mail: true
            },          
        },         
        submitHandler: function(form) {
            $("#confFRM").submit();
        }                
    });    
});
</script>
<script>
    $('#gen-ga').click(function(){
        var ga = '';
        var uaid = $('#google_analytics').val();
        if(uaid=='' || uaid==null){
            $('#error-box').show().html('Give a valid UA ID');
        }else{
            $('#error-box').hide();
            ga += "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
            ga += "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
            ga += "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
            ga += "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";

            ga += "ga('create', '"+uaid+"', 'auto');";
            ga += "ga('send', 'pageview');";
            $('#ga-box').show();
            $('#ga-code').html(ga);
        }  
    });
    $(document).ready(function(){
        $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
    });
</script>
