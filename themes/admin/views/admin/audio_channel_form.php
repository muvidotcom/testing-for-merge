<?php
if($this->studio->id < 3710){$old = 1;}else{$old=0;}
$disable = "";
$enable_lang = "en";
if(@$data){
    $enable_lang = $this->language_code;
    if ($_COOKIE['Language']) {
        $enable_lang = $_COOKIE['Language'];
    }
    $disable = 'disabled="disabled"';
    if(@$data['language_id'] == $this->language_id && $data['parent_id']==0){
        $disable = "";
    }
}
$movie_id = @$data['id'];
if(array_key_exists($movie_id, @$langcontent['film'])){
	@$data = Yii::app()->Helper->getLanuageCustomValue(@$data,@$langcontent['film'][$movie_id]);
    @$data['name'] = @$langcontent['film'][$movie_id]->name;
    @$data['story'] = @$langcontent['film'][$movie_id]->story;
    @$data['genre'] = @$langcontent['film'][$movie_id]->genre;
    @$data['censor_rating'] = @$langcontent['film'][$movie_id]->censor_rating;
    @$data['language'] = @$langcontent['film'][$movie_id]->language;
}
?>
<div class="form-group">
	<label class="control-label col-sm-4">Method:</label>
	<div class="col-sm-8">
		<div class="row">
                        <input type="hidden" name="method_type_val" value="<?php echo @$data['feed_method'];?>"/>
			<div class="col-sm-5">
				<label class="radio radio-inline m-r-20">
					<input type="radio" id="method_type_pull"  value="pull" name="method_type" checked="checked"  onchange="checkFeedType();" <?php if(@$data['feed_method']!=''){?> disabled <?php }?> <?php echo $disable; ?>>
					<i class="input-helper"></i>  Feed (<small style="font-weight: normal;">You already have an existing feed</small>)
				</label>
			</div>			
        </div>
    </div>
</div>
<input type="hidden" id="is_check_custom" value="<?php if (@$customData){echo 1;}else{echo 0;}?>">
<input type="hidden" name="movie[content_types_id]" value="8">
<?php
if (@$customData) {
    $defaultFields = array('name', 'release_date', 'genre','story');
    $relationalFields = Yii::app()->Helper->getRelationalField(Yii::app()->user->studio_id);
    $formData = $customData['formData'];
    unset($customData['formData']);
    foreach ($customData AS $ckey => $cval) {
        if (!in_array($cval['f_name'], $defaultFields)) {
            if ($relationalFields && array_key_exists($cval['f_name'], $relationalFields)) {
                $cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
            }
        }
        $cvalue = ($cval['f_id']=='censor_rating')?'censor_rating':$cval['f_name'];
        if($cval['f_id']=='censor_rating'){$cval['f_name']='censer_rating';}
        if($cval['f_id']=='title'){$cval['f_is_required']=0;}
        ?>
<div class="form-group">
            <label for="<?= $cval['f_display_name']; ?>" class="col-md-4 control-label"><?= $cval['f_display_name']; ?><?php if ($cval['f_is_required']) { ?><span class="red"><b>*</b></span><?php } ?>:</label>
        <?php if (!$cval['f_type']) { ?>
                <div class="col-md-8">
                    <div class="fg-line">
                        <?php if ($cval['f_id'] == 'release_date') { ?>
                            <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php
                            if (@$data['release_date']) {
                                echo date('m/d/Y', strtotime($data['release_date']));
                            }
                            ?>" class="form-control input-sm checkInput" >
                        <?php } else { ?>
                            <input type='text' placeholder="" id="<?= $cval['f_id']; ?>" name="movie[<?= $cval['f_name']; ?>]" value="<?php echo @$data[$cvalue]; ?>" class="form-control input-sm checkInput" <?php if ($cval['f_is_required']) { ?> required  <?php } ?> <?php if (@$data['id'] == '' && $cval['f_name'] == 'name') { ?> onblur="checkperma(this.value)"<?php } ?>>
            <?php } ?>
                    </div>
                </div>	
        <?php } elseif ($cval['f_type'] == 1) { ?>
                <div class="col-md-8">
                    <textarea class="form-control input-sm checkInput" rows="5" placeholder="" name="movie[<?= $cval['f_name']; ?>]" id="<?= $cval['f_id']; ?>" ><?php echo @$data[$cvalue]; ?></textarea>
                </div>
        <?php } elseif ($cval['f_type'] == 2) { ?>
                <div class="col-md-8">
                    <select name="movie[<?= $cval['f_name']; ?>]" placeholder="" id="<?= $cval['f_id']; ?>" <?php if ($cval['f_is_required']) { ?> required  <?php } ?> class="form-control input-sm checkInput" >
                        <?php
                        echo "<option value=''>-Select-</option>";
                        $opData = json_decode($cval['f_value'], true);
						$opData_new = $opData;
						$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
						$opData = empty($opData)?$opData_new:$opData;
                        foreach ($opData AS $opkey => $opvalue) {
                            $selectedDd = '';
                            if (@$data['id'] && @$data[$cvalue] == $opvalue) {
                                $selectedDd = 'selected ="selected"';
                            }
                            echo "<option value='" . $opvalue . "' " . $selectedDd . " >" . $opvalue . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <?php } elseif ($cval['f_type'] == 3) {?>                    
                    <div class="col-md-8">
                        <select name="movie[<?= $cval['f_name']; ?>][]" placeholder="" id="<?= $cval['f_id']; ?>" multiple <?php if ($cval['f_is_required']) { ?> required  <?php } ?> class="form-control input-sm checkInput">
                            <?php
                            $opData = json_decode($cval['f_value'], true);
							$opData_new = $opData;
							$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
							$opData = empty($opData)?$opData_new:$opData;
                            foreach ($opData AS $opkey => $opvalue) {
                                $selectedDd = '';
                                if (@$data['id'] && in_array($opvalue, json_decode(@$data[$cvalue], true))) {
                                    $selectedDd = 'selected ="selected"';
                                }
                                echo "<option value='" . $opvalue . "'" . $selectedDd . ">" . $opvalue . "</option>";
                            }
                            ?>
                        </select>
                    </div>
            <?php } ?>
        </div>
        <?php if ($cval['f_id'] == 'mname') { ?>
        <div class="col-md-4"></div><div class="col-md-8"><div id="plink"></div></div>
        <?php } ?>

    <?php }
} else {?>
<div class="form-group">
	<label for="movieName" class="col-md-4 control-label">Content Name<span class="red"><b>*</b></span>:</label>
	<div class="col-md-8">
        <div class="fg-line">
			<input type='text' placeholder="Enter content name.." id="mname" name="movie[name]" value="<?php echo @$data['name'];?>" class="form-control input-sm checkInput" required >
		</div>
	</div>
</div>

<div class="form-group">
	<label for="releaseDate" class="col-md-4 control-label" >Release/Recorded Date:</label>
	<div class="col-md-8">
        <div class="fg-line">
			<input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php if(@$data['release_date']){echo date('m/d/Y',strtotime($data['release_date']));}?>" class="form-control checkInput" <?php echo $disable; ?>>
		</div>
	</div>
</div>
<?php if($old){?>
<div class="form-group">
	<label for="genre" class="col-md-4 control-label">Genre:</label><br/>
	
	<select  data-role="tagsinput"  name="movie[genre][]" placeholder="Use Enter or Comma to add new" id="genre" multiple >
		<?php if(@$data['id'] && @$data['genre']){
			$pregenre = json_decode($data['genre']);
			foreach($pregenre AS $k =>$v){
				echo "<option value='".$v."' selected='selected'>".$v."</option>";
			}
		}?>
	</select>&nbsp;&nbsp;
		
</div>
<?php }?>
<div class="form-group">
	<label for="genre" class="col-md-4 control-label">Story/Description: </label>
	<div class="col-md-8">
        <div class="fg-line">
			<textarea placeholder="Enter story..." rows="5" class="form-control textarea checkInput" name="movie[story]" id="story1"   ><?php echo @$data['story'];?></textarea>
			<span class="countdown1"></span>
		</div>
	</div>
</div>
<?php }?>
<div class="form-group">
    <label for="pull" class="control-label col-sm-4" id="pull_feeds" <?php if(@$data['feed_method']=='push'){?>style="display: none;"<?php }?>>Feed<span class="red"><b>*</b></span>:</label>
    <div class="col-sm-8">
         <div class="fg-line">
            <input  placeholder="http://muvi.com/manifests/vM7nH0Kl.m3u8" <?php if( @$data['feed_method']=='push'){?>style="display: none" type="hidden" <?php } else{ ?> required type="text"  onblur="cros_check(this)" <?php }?> id="feed_url" value="<?php echo @$data['feed_url'];?>" class="form-control input-sm" name="feed_url"  <?php if(@$data['feed_type']==2){?>pattern="rtmp?://.+"<?php }else{?>pattern="https?://.+"<?php }?> <?php echo $disable; ?> >
        </div>
    </div>
</div>
<div class="form-group">
    <label for="start_time" class="col-md-4 control-label">Start Time (In GMT): </label>
    <div class="col-md-8">
        <div class="fg-line">
            <div class="input-group bootstrap-timepicker timepicker">
                <input autocomplete="off" id="start_time" name="movie[start_time]" 
                value="<?php echo @$data['start_time']; ?>" type="text" class="form-control">
                <span class="input-group-addon">
                    <a class="input-button" id="clearDatetime" title="cleardatetime">
                        <i class="icon-close"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>
<!-- Duration -->
<div class="form-group">
    <label for="duration" class="col-md-4 control-label">Duration (In Hours): </label>
    <div class="col-md-8">
        <div class="fg-line">
            <div class="input-group bootstrap-timepicker timepicker">
                <input autocomplete="off" id="duration" name="movie[duration]" value="<?php if(isset($data['duration'])){echo gmdate('H:i:s',strtotime(@$data['duration']));}?>" type="text" class="form-control">
                <span class="input-group-addon">
                    <a class="input-button" id="clearTime" title="cleartime">
                        <i class="icon-close"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
	<label for="category" class="col-md-4 control-label">Content Category<span class="red"><b>*</b></span>: </label>
	<div class="col-md-8">
        <div class="fg-line">
			<select  name="movie[content_category_value][]" id="content_category_value" multiple required="true" class="form-control input-sm checkInput" <?php echo $disable; ?>>
				<?php 
					foreach($contentCategories AS $k =>$v){
						if(@$data && in_array($k, explode(',', $data['content_category_value']))){//($data['content_category_value'] & $k)
							$selected = "selected='selected'";
						}else{
							$selected ='';
						}
						echo '<option value="'.$k.'" '.$selected.' >'.$v."</option>";
				}?>
			</select>
		</div>
	</div>
</div>
        
<div class="form-group" <?php if(@$data['feed_method']!='push'){?>style="display: none;"<?php }?> id="recordingDiv" >
    <div class="col-md-offset-4 col-md-8">
        <input type="hidden" name="is_recording_val" value="<?php echo @$data['is_recording'];?>"/>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="is_recording" id="is_recording" <?php if (@$data['is_recording']) { ?>checked="checked"<?php } if(isset($data['is_recording'])){ ?> disabled <?php }  echo $disable; ?>/>
                <i class="input-helper"></i> Enable Recording
            </label>
        </div>
        <div class="row m-t-10" id="recordingDiv_date_div"  style="display:none;">
            
            <div class="col-sm-3 col-md-3">
                <div class="input-group">
                    <div class="fg-line">
                        Delete in  
                    </div>
                </div>
            </div>
            <div class="col-sm-7 col-md-9">
                <div class="row">
                    <div class="col-md-12 m-b-10">
                        <div class="row">
                            <div class="col-sm-1">
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" onchange="checkIsDeleteContent();" value="1" name="delete_content" id="delete_content" <?php if (@$data['delete_no']) { ?>checked="checked"<?php } ?> <?php echo $disable; ?>>
                                    <i class="input-helper"></i>
                                    
                                  </label>
                            </div>
                            <div class="col-sm-3">
                                <div style="margin-top:-7px">
                                    <div class="fg-line">
                                      <input type="text" class="form-control input-sm" type="number" onkeypress="return isNumber(event)" <?php if(@$data['delete_no']){ echo "required"; }?>  id="delete_no" name="delete_no" value="<?php if(@$data['delete_no']){ echo @$data['delete_no']; } ?> <?php echo $disable; ?>">
                                    </div>
                                  </div>
                            </div>
                            <div class="col-sm-6" >
                               <div style="margin-top:-11px">
                                <div class="fg-line">
                                  <div class="select">
                                    <select class="form-control" name="delete_span" id="delete_span" <?php echo $disable; ?>>
                                        <option value="Days" <?php if (@$data['delete_span'] == 'Days') { ?>selected="selected"<?php } ?>>Days</option>
                                        <option value="Months" <?php if (@$data['delete_span'] == 'Months') { ?>selected="selected"<?php } ?>>Months</option>
                                        <option value="Years" <?php if (@$data['delete_span'] == 'Years') { ?>selected="selected"<?php } ?>>Years</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                              <div class="col-sm-1">
                                <label class="radio radio-inline m-r-20">
                                    <input type="radio" onchange="checkIsDeleteContent();" value="0" name="delete_content" id="delete_content" <?php if (@$data['delete_no']== 0) { ?>checked="checked"<?php } ?> <?php echo $disable; ?>>
                                    <i class="input-helper"></i>
                                   
                                  </label>
                            </div>
                      <div class="col-sm-3">
                              
                                   Never
                                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
        
        
<input type="hidden" value="<?php echo @$data['livestream_id']; ?>" name="livestream_id" id="livestream_id"  />
<div class="form-group m-t-30">
    <div class="col-md-offset-4 col-md-8">
        <button type="submit" class="btn btn-primary btn-sm" id="save-btn"><?php if(@$data['id']){?>Update Content<?php }elseif($content_types_id==2){echo 'Save';}else{?>Save & Continue<?php }?></button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var start_time  = $("#start_time").val();
        $('#start_time').flatpickr({
            enableTime: true,
            enableSeconds: true,
            time_24hr: true,
            minDate: "today",
            dateFormat: "Y-m-d H:i:S",
            noCalendar: false,
            defaultDate: start_time
        });
        $('#duration').flatpickr({
            enableTime: true,
            noCalendar: true,
            enableSeconds: false, // disabled by default
            time_24hr: true, // AM/PM time picker is used by default
            // default format
            dateFormat: "H:i",
            // initial values for time. don't use these to preload a date
            defaultHour: 12,
            defaultMinute: 0
        });
        $('#clearDatetime').on('click',function(){
            $('#start_time').val('');
        });
        $('#clearTime').on('click',function(){
            $('#duration').val('');
        })
    });
</script>
