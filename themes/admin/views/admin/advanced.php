<?php $studio_id = Yii::app()->user->studio_id; ?>
<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/themes/admin/js/tinymce/tinymce.min.js"></script>
<div class="row m-t-40">
    <div class="col-sm-12">
        <div class="row m-b-20">
            <div class="col-sm-12">
                <?php 
                $a = 1;
                if ($this->studio->is_default == 0 && $this->studio->status == 1 && $this->studio->is_subscribed == 0 && $this->studio->is_deleted == 0) { 
                ?>
                    <label><a href="javascript:void(0);" onclick="openinmodal('<?php echo Yii::app()->getBaseUrl(); ?>/payment/subscription/purchase');" style="text-decoration: none;">Purchase Subscription</a> to get the API Key.</label>
                <?php } else { ?>
                    <label>API Authorization Key:  <?= $apikey; ?> </label><br/>
                    <a href="<?= Yii::app()->getBaseUrl(true) . '/api-new.html'; ?>" target="_blank">Muvi API documentation </a>
                <?php } ?>
               
            </div>
        </div>	
        
        <form id="manageCookie" name="manageCookie" method="post" class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="control-group">
                        <input type="hidden" name="id" value="<?php echo $stdMngCookieId; ?>"/>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="is_enable" name="is_enable" value="1" <?php if ($is_enable == 1) { ?>checked="checked"<?php } ?>/>
                                <i class="input-helper"></i>Show Cookie message
                            </label>
                        </div>
                        <p>
                            Your website uses some cookies. European law requires to display a message for the cookie usage. You can learn more at <a href="http://ec.europa.eu/ipg/basics/legal/cookies" target="_blank">http://ec.europa.eu/ipg/basics/legal/cookies</a>
                        </p>
                        <p>
                            It's recommended to select this checkbox if your primary market is Europe, otherwise unselect.
                        </p>
                    </div>
                    <div class="control-group m-t-10">                        
                        <textarea name="cookieMsg" id="cookieMsg" rows="10" placeholder="Message for cookie" class="form-control input-sm" required><?php echo $message; ?></textarea>
                    </div>        
                    <div class="control-group m-t-30">
                        <button type="submit" class="btn btn-primary btn-sm" onclick="return validateCookie();">SAVE</button>                        
                    </div>
                </div> 
            </div>
        </form>

    </div>        
</div>
<hr/>
<div class="row m-t-20 m-b-40">
    <div class="col-sm-12">
        <div class="Block">
            <form class="form-horizontal" class="update_content_type" id="update_content_type" method="post">
                <h4 class="m-t-20">Platforms(Apps)</h4>
                <?php $app = Yii::app()->general->apps_count($studio_id); ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 1))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="1" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Website
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 2))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="2" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> iOS App (iPhone, iPad)
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 4))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="4" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Android App
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 8))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="8" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Roku App 
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 128))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="128" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Android TV App
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 16))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="16" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Fire TV App
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 32))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="32" <?php echo $checked; ?> name="apps[]" class="apps">
                            <i class="input-helper"></i> Apple TV App
                        </label>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php 
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 64))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="64" <?php echo $checked; ?> name="apps[]" class="content">
                            <i class="input-helper"></i>  MUVI Server (Build the apps yourself) 
                        </label>
						<div class="clear-fix"></div>
                        <label class="checkbox checkbox-inline m-r-20">
                            <?php                              
                            if(empty($app) || (isset($app) && ($app['apps_menu'] & 256))){
                                $checked = "checked";
                            }else{
                               $checked = ""; 
                            }
                            ?>
                            <input type="checkbox" value="256" <?php echo $checked; ?> name="apps[]" class="content">
                            <i class="input-helper"></i>  Fire OS App
                        </label> 
                        </div>
                    </div>
                </div>
                <div class="form-group m-t-30">
                    <div class="col-sm-12">
                        <button type="button" id="content_apps" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>           
        </div>
    </div>
</div>
<script type="text/javascript">
    tinymce.init({
        selector: "#cookieMsg",
        formats: {
            bold: {inline: 'b'},  
        },
        valid_elements : '*[*]',
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : false,
        menubar: false,
        plugins: [
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
        setup: function (editor) {
            editor.on('init', function () {
                if (tinymce.editors.length) {
                    $("#content_ifr").removeAttr('title');
                }
            });
        }
    });
    function validateCookie() {
        $("#manageCookie").validate({
            rules: {
                "cookieMsg": {
                    required: {
                        depends: function (element) {
                            if ($('#is_enable').is(':checked')) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            messages: {
                "cookieMsg": {
                    required: "Please write some message content for cookie."
                }
            },
            errorPlacement: function(error, element) {
            error.addClass('red');
            error.insertAfter(element.parent());
        }   
        });
    }
    function openinmodal(url) {
        $('.loaderDiv').show();
        $.get(url, {"modalflag": 1}, function (data) {
            $('.loaderDiv').hide();
            $('#mymodaldiv').html(data);
            $("#mymodal").modal('show');
        });
    }
    $(document).ready(function(){
       $('#content_apps').click(function(){
        var validate = $("#update_content_type").validate({
            rules: {
               'content[]': {required: true},
               'apps[]': {required: true},
            },
            messages: {
                "content[]": "Please select atleast one from this section",
                "apps[]": "Please select atleast one from this section",
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertBefore(element.parent().parent());
            }
        });
        var x = validate.form();
        if (x) {
            $('#update_content_type').submit();
        }
       }) ;
    });
</script>

<div class="loaderDiv">
    <div class="preloader pls-blue">
        <svg viewBox="25 25 50 50" class="pl-circular">
          <circle r="20" cy="50" cx="50" class="plc-path"/>
        </svg>
    </div>
</div>
<!-- Modal Starts Here -->
<div id="mymodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs" id="mymodaldiv">

            </div>
        </div>
    </div>
</div>
<!-- Modal End  -->
<style type="text/css">
    .loaderDiv{position: absolute;left: 45%;top:10%;display: none;}
    
</style>
