<?php
$studio_ad = ($studio_ads[0])?$studio_ads[0]:'';
$ad_id = 0;
$ad_network_id = 0;
$channel_id = '';
$website_id = '';
if($studio_ad != '')
{
    $ad_id = $studio_ad->id;
    $ad_network_id = $studio_ad->ad_network_id;
    if($ad_network_id == 1)
        $channel_id = $studio_ad->channel_id;
    else if($ad_network_id == 2)
        $website_id = $studio_ad->channel_id;
    else
        $ad_tag = $studio_ad->channel_id;    
}
?>
<div class="row m-t-40">
    <!-- right column -->
    <div class="col-sm-12">
        <!-- Horizontal Form -->
       
                <p class="m-b-20">Select your ad network. Contact your Muvi representative if you don't see your Ad network listed here.</p>            
                <!-- form start -->
                <form action="<?php echo Yii::app()->baseUrl; ?>/admin/savevideoad"  method="post" class="form-horizontal">
                    <input type="hidden" value="<?php echo $ad_network_id?>" id="mvad_network_id" name="mvad_network_id" />
                    <input type="hidden" value="<?php echo $ad_id?>" id="mvad_id" name="mvad_id" />
                    <div class="row m-b-20">
                        
                        <?php
                        foreach ($adnetworks as $value) {
                        ?>
                        <div class="col-md-3 col-sm-6 col-xs-12 m-b-20">
                            <div class="adlogos">
                                <div data-id="<?php echo $value->id;?>" <?php if ($value->id != $ad_network_id){ ?> style="display: block;" class="default_logo adlogo" <?php } else {?> style="display: none;" class="unselected default_logo adlogo" <?php } ?>>
                                    <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->getbaseUrl(true).'/img/icons/adnetworks/'.$value->code.'-gray.png'; ?>">
                                </div>
                                <div data-id="<?php echo $value->id;?>" <?php if ($value->id == $ad_network_id){ ?> style="display: block;" class="original_logo adlogo selected" <?php } else{?>style="display: none;" class="original_logo adlogo"<?php } ?>>
                                    <img alt="<?php echo $value->name;?>" src="<?php echo Yii::app()->getbaseUrl(true).'/img/icons/adnetworks/'.$value->code.'.png'; ?>">
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                        <?php
                        if(count($adnetworks) > 0)
                        {
                            $network_name = '';
                        foreach ($adnetworks as $value) {
                            if($ad_network_id == $value->id)
                                $network_name = $value->name;
                        ?>     
                        <div id="mvad_<?php echo $value->id;?>" class="mvad_frms <?php echo ($ad_network_id == $value->id)?'show':'hide'?>">
                            <?php if($value->code == 'spotxchange'){?>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="channel">Channel ID:</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Enter your SpotExchange channel ID" name="channel_id_<?php echo $value->id?>" id="channel_id_<?php echo $value->id?>" class="form-control input-sm" value="<?php echo ($channel_id != 0)?$channel_id:'';?>" />
                                        </div>
                                    </div>
                                </div>                        
                            <?php }?>
                            <?php if($value->code == 'yume'){?>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="channel">Website ID:</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Enter your YuMe Website ID" name="channel_id_<?php echo $value->id?>" id="channel_id_<?php echo $value->id?>" class="form-control input-sm" value="<?php echo ($website_id != 0)?$website_id:'';?>" />
                                        </div>
                                    </div>
                                    </div>
                            <?php }?> 
                            <?php if($value->code == 'doubleclick'){?>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="channel">Ad Tag</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" placeholder="Enter your Ad Tag" name="channel_id_<?php echo $value->id?>" id="channel_id_<?php echo $value->id?>" class="form-control input-sm" value="<?php echo ($ad_tag != '')?$ad_tag:'';?>" />
                                        </div>
                                    </div>
                                </div>
                            <?php }?> 
                        </div>
                        <?php 
                        }
                        if($ad_network_id > 0){
                        ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Test Page</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-md-10">
                                         <a href="http://<?php echo $this->studio->domain; ?>/ads/play" target="_blank">http://<?php echo $this->studio->domain; ?>/ads/play</a>                    
                                    </div>                       
                                <div class="col-md-2">
                                    <div style="display:none;" class="animate-div" id="embed_completed">Copied!</div>
                                    <button class="btn btn-success  copyToClipboard"  data-clipboard-text="http://<?php echo $this->studio->domain; ?>/ads/play" type="button" onclick="CopytoClipeboard('embed_completed');">Copy!</button>
                                </div>
                                </div>
                                <p>Send link of the test page to <span id="ad_nt_name"><?php echo $network_name;?></span> for verification.</p>
                            </div>
                        </div>        
                        <?php
                        }
                        }
                        ?> 
                        <div class="form-group" id="sbmt_btn">
                            <div class="col-sm-offset-4 col-sm-8 m-t-30">
                                <button type="submit" class="btn btn-primary btn-sm"><?php if (isset($ad_id) && !empty($ad_id)) { ?>Update<?php } else { ?>Add<?php } ?></button>
                            </div>
                        </div>
                       
                    </div>
                </form>
           
    </div><!--/.col (right) -->
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
clientTarget = new ZeroClipboard($('.copyToClipboard'), {
    moviePath: "<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.swf",
    debug: false
});    
function CopytoClipeboard(id) {
    $('#' + id).css('display', 'block');
    $('#' + id).animate({
        opacity: 0,
        top: '-=75',
    }, {
        easing: 'swing',
        duration: 500,
        complete: function () {
            $('#' + id).css({'display': 'none', 'opacity': 1, top: '+=75'});
        }
    });
}
$(document).ready(function(){
<?php
if($ad_id > 0)
{
?>
    $('#sbmt_btn').show();
<?php
}
else
{
?>
    $('#sbmt_btn').hide();
<?php        
}
?>  
});
 
    $(function () {
        $('.adlogos').hover(function () {
            $(this).find('.default_logo').hide();
            $(this).find('.original_logo').show();
            $(this).find('.selected').show();
            $(this).find('.unselected').hide();
        }, function () {
            $(this).find('.original_logo').hide();
            $(this).find('.default_logo').show();
            $(this).find('.selected').show();
            $(this).find('.unselected').hide();
        });
                
        $('.adlogo').click(function(){
            $('#sbmt_btn').show();
            $('.adlogos').find('.original_logo').hide();
            $('.adlogos').find('.default_logo').show();
            
            $(this).find('.original_logo').show();
            $(this).find('.default_logo').hide();
            
            var id = $(this).attr('data-id'); 
            $('#mvad_network_id').val(id);
            $(this).parent().find('.original_logo').show().addClass('selected');
            $(this).parent().find('.default_logo').hide().addClass('unselected');
            $('.adlogo').find('.selected').removeClass('selected');
            $('.adlogo').find('.unselected').removeClass('unselected');
            $('.mvad_frms').addClass('hide');
            $('#mvad_'+id).removeClass('hide');
            $('#mvad_'+id).addClass('show');
            
            if(id == 1){
                $('#ad_nt_name').html('SpotXchange');
            }else if(id == 2){
                $('#ad_nt_name').html('YuMe');
            }else{
                $('#ad_nt_name').html('Google Doubleclick');
            }            
        });
    });
    
    function validatefrm()
    {
        $('#mvad_form').submit();    
    }
    


    $("#mvad_form").validate({
        rules: {
            mvad_network_id: "required",
        },
        messages: {
            mvad_network_id: "Please choose your ad Network",
        },
        submitHandler: function(form) {
            var url = "<?php echo Yii::app()->baseUrl; ?>/admin/videoad";
            document.mvad_form.action = url;
            document.mvad_form.submit();
        }            
    });

</script>    