<script>
    var userinternetSpeed = "<?php echo  @$_SESSION['internetSpeed']?>"; 
    var video_upload_type = 'physical';
</script>
<?php
if (isset(Yii::app()->user->created_at)) {
    $expairy_date = strtotime(Yii::app()->user->created_at) + 13 * 24 * 60 * 60;
} else {
    $expairy_date = time() + 13 * 24 * 60 * 60;
}
$v = RELEASE;
$delete_multi_msg  = 'Deleting a multi-part parent content deletes all episodes</b> under it and <b>the videos as well.</b> Are you sure?';
$delete_single_msg = 'Deleting content deletes all meta data of the content <b>and the video as well.</b> Are you sure?';
$studio_id = Yii::app()->user->studio_id;
$base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
$payment_gateway = Yii::app()->common->isPaymentGatwayExists($this->studio->id);
$pgconfig = Yii::app()->general->getStoreLink();
if (isset(Yii::app()->user->id) && (Yii::app()->user->id > 0)) {
    $is_subscribed = Yii::app()->common->isSubscribed(Yii::app()->user->id);
}
$studio = $this->studio;
$posterImg = POSTER_URL . '/no-image-a.png';
$isPPVEnabled = $data_enable;
$template_name=$studio->parent_theme;
$content_type =  Yii::app()->general->content_count($this->studio->id);
$video=$audio=$physical=0;
if(empty($content_type) || (isset($content_type) && ($content_type['content_count'] & 1)) || (isset($content) && ($content['content_count'] & 2))){
	$video = 1;
}
if((isset($content_type) && ($content_type['content_count'] & 4))){
	$audio = 1;
}
?>
<style>
    #addvideo_popup .form-group {width:100% !important;}
    .panel-heading span {margin-top: -20px;font-size: 15px;}
    .panel-title{color: #000;}
    .clickable{cursor: pointer;color: #000;}
	.panel-heading {cursor: pointer;}
    .multiselect.dropdown-toggle.btn.btn-default-bg{width: 220px !important;}
    .input-group-addon{color: #000;}
</style>
<?php if (isset(Yii::app()->user->status) && Yii::app()->user->status == 3) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="Block">
                <div class="Block-Header">
                    <h3 class=""><strong><?php if ($expairy_date >= strtotime(date('Y-m-d'))) { ?>Your Trial Period expires on <?php echo date('F d, Y', $expairy_date);
    } else { ?>Site Status<?php } ?></strong></h3>
                    <div class="col-sm-12 pull-right">
                        <button data-widget="collapse" class="btn btn-primary btn-xs"><i class="fa fa-minus"></i></button>
                        <button data-widget="remove" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="Block-Body">
    <?php if ($expairy_date >= strtotime(date('Y-m-d'))) { ?>
                        <p>
                            Cancel anytime. Your card will not be charged if account is cancelled before <?php echo date('F d, Y', $expairy_date); ?>. Unless cancelled, Your card will be charged for the first month on <?php echo date('F d, Y', ($expairy_date + 24 * 60 * 60)); ?>
                        </p>
                        <div><strong>Status</strong></div>
    <?php } ?>
                    <table class="table status-tbl" border="1" >
                        <tbody>
                            <tr>
                                <td>Admin Panel</td>
                                <td>Add Content</td>
                                <td>Payment Gateway</td>
                                <td>Go Live</td>
                            </tr>
                            <tr>
                                <td class="greenBackground status-ready">Ready</td>
                                <td><a href="<?php echo $this->createUrl('admin/newContents'); ?>" >Add Content</a> </td>
                                <td>In Progress</td>
                                <td>In Progress</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
<?php } ?>
    <div class="row m-b-40">
        <div class="col-xs-8">
            <?php if ($allow_new) { ?>
                <a href="<?php echo $this->createUrl('admin/newContents'); ?>"><button class="btn btn-primary btn-default m-t-10">Add Content</button></a>
            <a href="javascript:void(0);"><button class="btn btn-primary btn-default m-t-10" data-toggle="modal" data-target="#importontent">Import</button></a>            
            <?php } else { ?>
                <a class="btn btn-primary btn-default m-t-10" data-toggle="modal" data-target="#Maxcontent">Add Content</a>
            <?php } ?>
        </div>
					</div>					
    <?php if($_GET['searchForm']){$showfilter = 1;}else{$showfilter = 0;}?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-success m-b-10">
                <div class="panel-heading  <?php if(!@$showfilter){echo "panel-collapsed";}?>">
                    <h3 class="panel-title">FILTERS</h3>
                    <span class="pull-right clickable"><i class="fa <?php if(!@$showfilter){echo "fa-angle-down";}else{echo "fa-angle-up";}?>" style="font-size: 23px;" aria-hidden="true"></i></span>
                </div>
                <div class="panel-body" <?php if(!@$showfilter){ echo 'style="display: none;"';}?>>
    <div class="filter-div">
    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo $this->createUrl('admin/managecontent'); ?>" name="searchForm" id="search_content" method="get">
                <div class="row">
                    <div class="col-sm-3 p-l-0">
                        <div class="form-group col-sm-12">
							<h4>Filter By : </h4>
                            <div class="fg-line">
                                <div class="select">
                                    <select class="form-control" name="searchForm[filterby]" id="filterby">
                                        <?php if (count($contentList) >= 1) { ?>
                                            <option value="0" <?php if (empty($searchData) || ($searchData['filterby'] == 0)) {
                                            echo "Selected";
                                        } ?>>&nbsp;Content Category</option>
                                        <?php } ?>
                                        <option value="1" <?php if ($searchData['filterby'] == 1) {
                                            echo "Selected";
                                        } ?>>&nbsp;Content Form</option>
                                    </select>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 p-l-0">
                        <div class="form-group">
                            <h4>Show : </h4>
                                    <?php if (count($contentList) >= 1) { ?>
                                <div id="show_category" style="display: none;">
                                    <select class="form-control input-sm showcatform" name="searchForm[content_category_value][]" id="filter_dropdown" multiple="multiple">
                                        <?php
                                        foreach ($contentList AS $row => $value) {
                                            $selected = (in_array($row, @$content_category_value)) ? 'selected' : '';
                                            echo '<option value="' . $row . '" ' . $selected . ' >' . $value . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                    <?php } ?>
                            <div id="show_form" style="display: none;">
                                <select class="form-control input-sm showcatform" name="searchForm[custom_metadata_form_id][]" id="form_dropdown" multiple="multiple">
                                    <?php
                                    foreach ($form as $value) {
                                        $selected = (in_array($value['id'], $searchData['custom_metadata_form_id'])) ? 'selected' : '';
                                        echo '<option value=' . $value['id'] . ' ' . $selected . '>' . $value['name'] . '</option>';
                                    }
                                    ?>
                                </select>	
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <h4>Sort By : </h4>
                            <div class="fg-line">
                                <div class="select">
                                    <select class="form-control" name="searchForm[sortby]" id="sortby">
                                        <option value="0" <?php if (empty($searchData) || ($searchData['sortby'] == 0)) {
                                        echo "Selected";
                                    } ?>>&nbsp;Last uploads</option>
                                    <?php /*?><option value="1" <?php if ($searchData['sortby'] == 1) {
                                        echo "Selected";
                                    } ?>>&nbsp;Most Sold</option><?php */?>
                                        <option value="2" <?php if ($searchData['sortby'] == 2) {
                                        echo "Selected";
                                    } ?>>&nbsp;A-Z</option>
                                        <option value="3" <?php if ($searchData['sortby'] == 3) {
                                        echo "Selected";
                                    } ?>>&nbsp;Z-A</option>
                                    </select>	
                                </div>					
                            </div>
                        </div>
                    </div>
					<div class="col-sm-2">
                        <div class="form-group">
                            <h4>Content Type : </h4>
							<div class="fg-line">
								<div class="select">
									<select name="searchForm[contenttype]" class="form-control" id="contenttype">
										<?php if($video){?>
										<option value="0" <?php if (($searchData['contenttype'] == 0)) { echo "Selected";} ?>>Video</option>
										<?php }if($audio){?>
										<option value="1" <?php if (($searchData['contenttype'] == 1)) { echo "Selected";} ?>>Audio</option>
										<?php }if($pgconfig){?>
										<option value="2" <?php if (empty($searchData) || ($searchData['contenttype'] == 2)) { echo "Selected";} ?>>Physical</option>
										<?php }?>
									</select>
								</div>					
							</div>
                        </div>
                    </div>
                    <div class="col-sm-2">
						<h4>Update Date : </h4>
                        <div class="input-group form-group">
                            <div class="fg-line">
                                <div class="select">
                                    <input class="form-control" type="text" id="update_date" name="searchForm[update_date]" value='<?php echo @$dateRange; ?>' />
                                </div>
                            </div>
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group  input-group">
                            <div class="fg-line">
                                <input type="text" class="filter_input form-control fg-input" name="searchForm[search_text]" value="<?php echo @$searchText; ?>"  id="search_content" onkeypress="handle(event);" placeholder="What are you searching for?"/>
                            </div>
                            <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table mob-compress" id="movie_list_tbl">
<!--        <thead>
            <tr>
            <th class="min-width">Content</th>
            <th class="width">Video</th>
            <th class="width">Type</th>
            <th class="width">Category</th>
            <th data-hide="phone" style="width:100px;">Views</th>
            <th data-hide="phone" class="width width--deviceBased">Action</th>
            </tr>
        </thead>    -->
        <tbody>
            <?php
            $cnt = 1;
            if ($data) {
                if ($template_name =='traditional-byod'){
                    $default_img = '/img/No-Image-Default-Traditional.jpg';
                    $default_episode_img = '/img/No-Image-Default-Traditional.jpg';
                }else{
                    $default_img = '/img/No-Image-Vertical-Thumb.png';
                    $default_episode_img = '/img/No-Image-Horizontal.png';
                }
                $postUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
                $cnt = (($pages->getCurrentPage()) * 20) + 1;
                foreach ($data AS $key => $details) {
                    $movie_id = $details['id'];
                    $content_types_id = $details['content_types_id'];
                    $cont_id = $movie_id;
                        $contentName = $details['name'];
                    $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
                    
                    
                      
                    ?>
            <tr>
                <td>
                    <a href="<?php echo 'http://' . Yii::app()->user->siteUrl . '/' .  @$plink; ?>" target='_blank' >
                    <div class="Box-Container-width-Modified">
                        <div class="Box-Container">
                             <div class="thumbnail">
                                <img src="<?php echo $details['pgposter']; ?>" alt="<?php echo $details['name']; ?>" style="max-width:100px;">
                             </div>
                         </div>   
                    </div>
						<div class="caption" style="max-width: 255px;">
                            <?php echo $contentName; ?>
                         </div>
                    </a>
                </td>
                <td>
                    <?php 
                        $icon = '<i class="fa fa-dropbox fa-2x" aria-hidden="true"></i>';
                        echo $icon."<br/>";
                    ?>
                    <b> Forms </b><br/>
                    <?=$details['formname'];?>             
                    <?php if($details['content_category_value']){?>
                        <br/><br/><b>Category</b><br/>
                        <?= Yii::app()->Helper->categoryNameFromCommavalues($details['content_category_value'],$contentList);
                    }?>                    
                </td>
                <td>
                    <b>SKU</b><br/>
                    <?php echo $details['sku']; ?>
                    <br>
                    <b>Price</b><br/>
                    <?php echo Yii::app()->common->formatPrice($details['sale_price'], $details['currency_id']);?>
                    <br>
                    <b>Sales</b><br/>
                    <?php echo $details['totquantity']?$details['totquantity']:0; ?>
                </td>
                <td>
                <?php                   
                    if ((count($grestcat) > 0) && $movie_id) {
                        $country = Yii::app()->general->getGeoblockPGName($movie_id);
                        if($country['category_name'] != ''){
                            echo "<b>Geo Block</b><br/>".$country['category_name']."<br>";
                        } 
                    }
                    if (count($method) > 0) {
                        $shipping = implode(',', $method);
                        echo "<b>Shipping</b><br/>";
                        if(strlen($shipping)>25){
                            echo '<span title="'.$shipping.'">'.substr($shipping,0,25).'..</span>';
                        } else {
                            echo $shipping;
                        }
                    } ?>
                </td>
                <td>
                    <h5><a href="<?php echo $this->createUrl('store/EditItem/', array('uid' => $details['uniqid'])); ?>" ><em class="icon-pencil"></em>&nbsp;&nbsp; Edit Item</a></h5>
                    <span id="<?php echo $details['id']; ?>_title" style="display:none"><?php echo $details['name']; ?></span>
                    <span id="<?php echo $details['id']; ?>_link" style="display:none"><?php echo $details['thirdparty_url']; ?></span>
                    <span id="trailerbtn<?php echo $details['id']; ?>">
                    <?php                     
                    if(@$view_trailer){
                        if($details['is_converted']==0 && $details['is_converted']!=''){
                            ?>
                            <h5><a href="javascript:void(0);" class="upload-video"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a></h5>
                            <?php
                        }else {
                            if($details['trailer_file_name']!='' || $details['thirdparty_url']!=''){
                            ?>
                            <h5><a href="javascript:void(0);" onclick="openUploadpopup('<?php echo $details['id'];?>',1);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Change Trailer</a></h5>
                            <?php
                            }else{
                            ?>
                            <h5><a href="javascript:void(0);" onclick="openUploadpopup('<?php echo $details['id'];?>',0);" class="upload-video"><em class="fa fa-upload"></em>&nbsp;&nbsp; Upload Trailer</a></h5>
                            <?php    
                            }                        
                        }                        
                    }
                    ?> 
                    </span>                    
                    
                    
                    <h5><a href="<?php echo $this->createAbsoluteUrl('store/deleteItem/', array('id' => $details['id'])); ?>" class="confirm" data-msg ="Are you sure?"><em class="icon-trash"></em>&nbsp;&nbsp; Delete Item</a></h5>
                    <?php if (count($grestcat) > 0) {?>
                    <h5><a href="javascript:void(0);" onclick="openGeoBlock(this);" data-pg_product_id="<?php echo $details['id'];?>"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp; Geo-block</a></h5>
                    <?php } ?>                
                    <h5><a href="javascript:void(0);" onclick="openRelatedContent(this);" data-content_id="<?=$details['id']; ?>" data-content_stream_id="<?= $details['id']; ?>" ><em class="fa fa-shopping-cart"></em>&nbsp;&nbsp; Manage Related Content</a></h5>
                </td>
            </tr>	
            <?php
            $cnt++;
             }} else {?>
            <tr>
                <td colspan="5">
                    <?php if ($contentType) { ?>
                        <p class="text-red">Oops! You don't have any content under this category.</p>
                    <?php } else { ?>
                        <p class="text-red">Oops! You don't have any content in your Studio.</p>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<div class="pull-right">
        <?php
        if ($data) {
            $opts = array('class' => 'pagination m-t-0');
            $this->widget('CLinkPager', array(
                'currentPage' => $pages->getCurrentPage(),
                'itemCount' => $item_count,
                'pageSize' => $page_size,
                'maxButtonCount' => 6,
                "htmlOptions" => $opts,
                'selectedPageCssClass' => 'active',
                'nextPageLabel' => '&raquo;',
                'prevPageLabel'=>'&laquo;',
                'lastPageLabel'=>'',
                'firstPageLabel'=>'',
                'header' => '',
            ));
        }
        ?>
    </div>
    
<div class="h-40"></div>

<!-- Upload Popup  start -->
<div style="position: fixed;background: rgb(255, 255, 255) none repeat scroll 0% 0%;left: initial;top: initial;bottom: 20px;right: 20px;border-radius: 0px;border: 1px solid rgb(230, 230, 230);width: 400px !important;height:auto !important;display: none;z-index:999999;" id="dprogress_bar">
  <div style="height: 40px;padding: 10px;border-radius: 0px;color: rgb(255, 255, 255);width: 100% !important;background-color: rgb(77, 77, 77);" id="status_header">
	<div style="float:left;font-weight:bold;">File Upload Status</div>
	<div onclick="manage_progressbar();" class="pull-right" style="cursor:pointer;"><i class="fa fa-minus"></i> &nbsp;&nbsp;&nbsp;</div>
  </div>
  <div style="padding:10px 20px 20px;background-color: rgb(255, 255, 255);border: 1px solid rgb(230, 230, 230);" id="all_progress_bar"></div>
</div>
<!-- Upload Popup  end -->
<div class="modal fade is-Large-Modal bs-example-modal-lg" id="addvideo_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 
</div>
<div class="modal fade is-Large-Modal bs-example-modal-lg" id="addvideo_popup_physical"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addvideo_popupLabel">Upload Video</h4>
                </div>
                <div class="modal-body">
                        <div role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                                <a href="#Upload_preview" aria-controls="Upload_preview" role="tab" data-toggle="tab">Upload Video</a>
                                        </li>
                                        <li role="presentation">
                                                <a href="#Choose_from_library" aria-controls="Choose_from_library" role="tab" data-toggle="tab">Choose from Gallery</a>
                                        </li>
                                        <li role="presentation">
                                                <a href="#Embed_from_3rd_party" aria-controls="Embed_from_3rd_party" role="tab" data-toggle="tab">Embed from 3rd party</a>
                                        </li> 
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Upload_preview">
                                                <div class="row is-Scrollable">
                                                        <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                                                <input type="hidden" value="?" name="utf8">
                                                                <input type="hidden" name="movie_id" id="movie_id" value="<?php echo $details['id']; ?>"/>
                                                                <input type="button" value="Upload File" class="btn btn-default-with-bg btn-sm"  onclick="click_browse('videofile');">
                                                                <input type="file" name="file" style="display:none;" id="videofile" required onchange="checkfileSize('physical');" >
                                                        </div>
                                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Choose_from_library">
                                                <div class="row m-b-20 m-t-20">
                                                        <div class="col-xs-6">
                                                                <div class="form-group input-group">
                                                                        <span class="input-group-addon"><i class="icon-magnifier"></i></span>
                                                                        <div class="fg-line">
                                                                                <input type="text" id="search_video1" placeholder="What are you searching for?" class="form-control fg-input input-sm" onkeyup="searchvideo();">

                                                                        </div>

                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="text-center row m-b-20 loaderDiv" style="display: none;">
                                                        <div class="preloader pls-blue text-center " >
                                                                <svg class="pl-circular" viewBox="25 25 50 50">
                                                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                                                </svg>
                                                        </div>
                                                </div>

                                                <div class="row  Gallery-Row">
                                                        <div class="col-md-12 is-Scrollable-has-search p-t-40 p-b-40" id="video_content_div">

                                                        </div>

                                                </div>
                                        </div>
                                     <!------Embed from 3rd Party------->
                                     <div role="tabpanel" class="tab-pane" id="Embed_from_3rd_party">
                                         <div role="tabpanel" class="tab-pane" id="home">
                                             <div class="row is-Scrollable m-b-20 m-t-20">
                                                 <div class="col-sm-12 m-t-40 m-b-20">
                                                    <div class="form-group">
                                                        <label for="uploadVideo" class="control-label col-md-3">Enter Youtube url &nbsp;</label>
                                                        <div class="col-md-4">
                                                            <input type="hidden" name="product_id" id="product_id" value=""/>                                                
                                                            <input type="hidden" name="movie_name" id="movie_name" value=""/>
                                                            <div class="fg-line">
                                                                <!--
                                                                <input type="text" id="embed_url" class="form-control  input-sm" placeholder="Link from YouTube, Vimeo or another OVP" onkeyup="embedThirdPartyPlatform();">
                                                                -->
                                                                <input type="text" id="embed_url" class="form-control  input-sm" name="embedurl" required="" placeholder="Link from YouTube">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-10">
                                                            <!--
                                                            <input type="button" value="Save" onclick="embedFromThirdPartyPlatform();" class="btn btn-primary btn-sm">
                                                            -->
                                                            <br />
                                                            <input type="button" value="Save" id="save-btn" onclick="physicalembedThirdPartyPlatform();" class="btn btn-primary btn-sm" >
                                                        </div>
                                                    </div>
                                                    <span class="error red help-block" id="embed_url_error"></span>
                                                    <div id="embedcode"></div>
                                                 </div>  
                                             </div>
                                         </div>
                                     </div>
                                     <!-------End of Embed from 3rd party------>
                                </div>
                        </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>    
</div>

<!-- Manage add items Modal-->
<div class="modal fade" id="items_popup" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y:hidden !important;"></div>
<!--Start PPV Modal-->
<div id="ppvPopup" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="Maxcontent"></div>
<!--End PPV Modal-->

<style type="text/css">
.comiseo-daterangepicker-triggerbutton {
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #d3d3d3;
    border-top:0px !important;
    border-left:0px !important; 
    border-right:0px !important;
    border-radius: 0px !important;
    color: #333;
    font-weight: normal;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default:hover {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #d3d3d3;
    color: #333;
    font-weight: normal;
}
</style>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/daterangepicker/jquery.comiseo.daterangepicker.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/jquery.comiseo.daterangepicker.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/daterangepicker/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/jsnew/footable.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/custom_upload.js?v=<?php echo $v ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/s3_multi.js?v=1"></script>	
<script type="text/javascript" src="<?php echo ASSETS_URL; ?>js/bootbox.js"></script>	
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-multiselect.js"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
                        var bootboxConfirm = 0;
                        $(function () {
                            $("a.confirm").bind('click', function (e) {
                                e.preventDefault();
                                var location = $(this).attr('href');
                                var msg = $(this).attr('data-msg');
                                if ($('#dprogress_bar').is(":visible")) {
                                    var msg = 'Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.';
                                    var location = $(this).attr('href');
                                    bootbox.hideAll();
                                    bootbox.confirm({
                                        title: "Upload In progress",
                                        message: msg,
                                        html:true,
                                        buttons: {
                                            'confirm': {
                                                label: 'Stop Upload',
                                                className: 'cnfrm-btn cnfrm-succ btn-default pull-right'
                                            },
                                            'cancel': {
                                                label: 'Ok',
                                                className: 'cnfrm-btn cnfrm-cancel btn-default pull-right'
                                            }
                                        }, callback: function (confirmed) {
                                            if (confirmed) {
                                                bootboxConfirm = 1;
                                                confirmDelete(location, msg);
                                            }
                                        }
                                    });
                                } else {
                                    confirmDelete(location, msg);
                                }
                            });

                        });
                       
// Validate and save the Server path for video
                        function validateURL(divid) {
                            showLoader();
                            var action_url = '<?php echo $this->createUrl('admin/validateVideo'); ?>';
                            if(divid){
                                $('#dropbox').html('');
                                var url = $('#dropbox').val();
                            }else{
                                $('#server_url_error').html('');
                                var url = $('#server_url').val();
                            }
                            var movie_id = $('#movie_id').val();
                            var movie_stream_id = $('#movie_stream_id').val();
                            if(divid){
                                var ftpusername = '';
                                var ftppassword = '';
                            }else{
                                var ftpusername = $('#ftpusername').val();
                                var ftppassword = $('#ftppassword').val();
                            }  
                            if (url && isUrlValid(url)) {
                                $.post(action_url, {'url': url, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id,'username':ftpusername,'password':ftppassword}, function (res) {
                                    showLoader(1);
                                    if (res.error) {
                                        if(divid){
                                            $('#dropbox_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                                        }else{
                                                                $('#server_url_error').html('There seems to be something wrong with the path. Please contact your Muvi representative. ');
                                        }
                                    } else {
                                        if (res.is_video) {
                                            $("#addvideo_popup").modal('hide');
                                        if(divid){
                                            $('#dropbox').val('');
                                            $('#dropbox_url_error').val('');
                                        }else{
                                            $('#server_url').val('');
                                            $('#server_url_error').val('');
                                        }                    
                                        var text = '<div><a href="javascript:void(0);" data-toggle="tooltip" title="Video download in progress. It will be available within an hour"><span class="glyphicon glyphicon-cloud-download glyphicon-2x"></span> Download in progress</a></div>';
                                        $('#' + movie_stream_id).html(text);
                                        $('#video_upload_' + movie_stream_id).empty();
                                        } else {
                                            if(divid){
                                                $('#dropbox_url_error').html('Please provide the path of a valid video file');
                                            }else{
                                                $('#server_url_error').html('Please provide the path of a valid video file');
                                            }
                                        }
                                    }
                                }, 'json');
                                console.log('valid url');
                            } else {
                                showLoader(1);
                                if(divid){
                                    $('#dropbox_url_error').html('Please enter a valid url.');
                                }else{
                                    $('#server_url_error').html('Please enter a valid url.');
                                }
                            }
                        }
                        function isUrlValid(url) {
                            return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
                        }
                        function showLoader(isShow) {
                            if (typeof isShow == 'undefined') {
                                $('.loaderDiv').show();
                                $('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
                                $('#profile button').attr('disabled', 'disabled');

                            } else {
                                $('.loaderDiv').hide();
                                $('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
                                $('#profile button').removeAttr('disabled');
                            }
                        }
                        function confirmDelete(location, msg) {
                            swal({
                                  title: "Delete Content?",
                                  text: msg,
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                                  confirmButtonText: "Yes",
                                  closeOnConfirm: true,
                                  html:true
                              }, function() {
                                  window.location.replace(location);
                              });
                        }

                        function validateHHMMSS(data)
                        {
                            var re = /^\d{2}:\d{2}:\d{2}$/;
                            var ret = data.match(re);
                            if (ret == null)
                                return false;
                            else
                            {
                                console.log(ret);
                                var parts = ret[0].split(':');
                                if (parts[1] < 0 && $parts[1] > 59 && parts[2] < 0 && $parts[2] > 59)
                                    return false
                                else
                                    return true;
                            }
                        }
                        function click_browse() {
                            $("#videofile").click();
                        }
                        function handle(e) {
                            if (e.keyCode === 13) {
                                $('#search_content').submit();
                            }
                            return false;
                        }
                        function openUploadpopup(product_id,flag) { 
                            var name = $("#" +product_id + "_title").html();
                            $('input[type=file]').val('');
                            $('#movie_name').val(name);
                            $('#product_id').val(product_id);
                            $('#movie_id').val(product_id);
                            $('#pop_movie_name').html(name);        
                            if(flag){
                                $('#embed_url').val($("#" +product_id + "_link").html());
                            }else{
                                $('#embed_url').val('');
                            }
                            $("#addvideo_popup_physical").modal('show');
                            $('#save-btn').removeAttr('disabled');
                            $('.error').html('');
                            searchvideo(1);
                        }
                        function openPPVpopup(obj) {
                            var content_types_id = $(obj).attr('data-content_types_id');
                            var movie_id = $(obj).attr('data-movie_id');

                            var url = '<?php echo Yii::app()->baseUrl; ?>/monetization/setPPVCategory';
                            $.post(url, {'content_types_id': content_types_id, 'movie_id': movie_id}, function (res) {
                                var name = $(obj).attr('data-name');
                                $("#ppvPopup").html(res).modal('show');
                                $('#ppv_movie_name').html(name);
                            });
                        }
                        // remove Video
                        function deleteVideo(movie_id) {
                            if (bootbox.confirm('Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.')) {
                                $.post(HTTP_ROOT + "/admin/removeVideo", {'is_ajax': 1, 'movie_id': movie_id}, function (res) {
                                    if (res.error == 1) {
                                        alert('Error in removing movie');
                                    } else {
                                        $('#fmovie_name').html("<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>");
                                        alert('Your movie video removed successfully');
                                    }
                                }, 'json');
                            }
                        }
                        function manage_progressbar() {
                            $("#all_progress_bar").toggle('slow');
                        }
                        clientTarget = new ZeroClipboard($('.copyToClipboard'), {
                            moviePath: "<?php echo Yii::app()->theme->baseUrl; ?>/js/zeroclipboard/ZeroClipboard.swf",
                            debug: false
                        });
                        function CopytoClipeboard(id) {
                            $('#' + id).css('display', 'block');
                            $('#' + id).animate({
                                opacity: 0,
                                top: '-=75',
                            }, {
                                easing: 'swing',
                                duration: 500,
                                complete: function () {
                                    $('#' + id).css({'display': 'none', 'opacity': 1, top: '+=75'});
                                }
                            });
                        }
                        function openEmbedBox(embedid) {
                            if ($('#' + embedid).is(':visible')) {
                                $('#' + embedid).fadeOut(1000)
                            } else {
                                $('#' + embedid).fadeIn(1000);
                            }
                        }

                        window.addEventListener("beforeunload", function (e) {
                            //
                            if ($('#dprogress_bar').is(":visible") && !bootboxConfirm) {
                                //var confirmationMessage = "\o/";
                                var confirmationMessage = "Performing any action while upload is in progress will interrupt the upload. Please wait for upload to complete or log into Muvi on a new tab to continue with other actions.";
                                //console.log(confirmationMessage);
                                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                                return confirmationMessage;                            //Webkit, Safari, Chrome
                            }
                        });

                        $(document).ready(function () {
                            $("[rel='tooltip']").tooltip();

                            $('.thumbnail').hover(
                                    function () {
                                        $(this).find('.caption').slideDown(250); //.fadeIn(250)
                                    },
                                    function () {
                                        $(this).find('.caption').slideUp(250); //.fadeOut(205)
                                    }
                            );
                            $('#filter_dropdown').change(function () {
                                $('#search_content').submit();
                            });
                            $("#filetype").change(function () {
                                $('.error').html('')
                                $('.savefile').hide();
                                $('#' + $(this).val() + '_div').show();
                            });
                            $('#filterby').change(function () {
                                showcategoryorform($(this).val(),1);                              
                        });
                            $('#form_dropdown').change(function () {
                                $('#search_content').submit();
                            });
                            $('#sortby').change(function () {
                                $('#search_content').submit();
                            });
							$('#contenttype').change(function () {
                                $('#search_content').submit();
                            });
                            showcategoryorform($('#filterby').val(),0);
                        });
                        function showcategoryorform(v,flag){
                            if(v == 1){
                                $('#show_category').hide();
                                $('#show_form').show();
                                if (flag && $('#form_dropdown').val()) {
                                    $('#search_content').submit();
                                }
                            }else{
                                $('#show_form').hide();
                                $('#show_category').show();
                                if (flag && $('#filter_dropdown').val()) {
                                    $('#search_content').submit();
                                }
                            }
                        }


    function addMore() {               
        var txt = $("#roll_after_section_hidden").html();        
        $("#roll_timing").append(txt);
    }
    
    function removeBox(obj) {
        $(obj).parent().parent().remove();
    }
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('#midroll').click(function(){
           if($('#midroll').is(':checked')){
               $('#roll_timing').show();
           } else{
               $('#roll_timing').hide();
           }
        });
        $('.showcatform').multiselect({
            //enableFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 300,
            dropUp: true,
            selectAllText:'All',
            nonSelectedText: 'No content selected',
            nSelectedText: 'content selected',
            allSelectedText: 'All contents selected',
            numberDisplayed:1,
    });
    });
    function openGeoBlock(obj){
        //var dv = '<div class="modal-dialog modal-lg"><div class="modal-content" style="height: 180px;overflow: hidden;    padding-top: 20px;text-align: center;"><h3>Loading...</h3></div></div>';
        var pg_product_id = $(obj).attr('data-pg_product_id');
        var url = '<?php echo Yii::app()->baseUrl; ?>/store/OpenGeoCategory';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url,{'pg_product_id':pg_product_id}, function (res) {
            $("#ppvPopup").html(res).modal('show');
        });
    }
                                                                    
function reset_filter()
{
 $("#video_duration").val(0); 
 $("#file_size").val(0);
 $("#is_encoded").val(0);
 $("#uploaded_in").val(0);  
 view_filtered_list();
}



function view_filtered_list(){ alert('pop');
       showLoader();
       var video_duration =$("#video_duration").val();
       var file_size =$("#file_size").val();
       var is_encoded =$("#is_encoded").val();
       var uploaded_in =$("#uploaded_in").val();
     
    var url = "<?php echo Yii::app()->baseUrl; ?>/admin/ajaxFiltervideo/video_duration/" + video_duration + "/file_size/" + file_size +"/is_encoded/" + is_encoded+"/uploaded_in/" + uploaded_in;   
     //window.location.href = url;
       $.post(url, {'video_duration': video_duration,'file_size':file_size, 'is_encoded': is_encoded,'uploaded_in': uploaded_in}, function (res) {
            if (res) {
                $('#video_content_div').html();
                 showLoader(1);
                 
          $('#video_content_div').html(res);      
           }
       });
       }

    function physicalembedThirdPartyPlatform(){
           var str =$("#embed_url").val();
            var videoUrl1 = "youtube.com";
            var videoUrl2 = "vimeo.com"; 
            var videoUrl3 = 'm3u8';
            var videoUrl4 = 'iframe';
            var videoUrl5 = "dailymotion.com";
          if(str.length ==0){
            $('#save-btn').attr('disabled', 'disabled');
			var movie_id = $('#product_id').val();
				var remove_url = HTTP_ROOT + "/admin/removeContentTrailer";
				$.post(remove_url, {'movie_id': movie_id, 'is_ajax': 1,'is_physical' : 1}, function (res) {
						window.location.href = window.location.href;
				});
           }else if(str.length){
            // console.log((str.indexOf(videoUrl4))+'.......'+(str.indexOf(videoUrl3))+'.....'+(str.indexOf(videoUrl2))+'.....'+(str.indexOf(videoUrl1)));
                 if ((str.indexOf(videoUrl3) != -1) || 
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl1) != -1)  || 
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl2) != -1) ||
                    (str.indexOf(videoUrl4) != -1 && str.indexOf(videoUrl5) != -1))
                 {                         
                     showLoader();
                     var embed_text =$("#embed_url").val();
                     var action_url = '<?php echo $this->createUrl('admin/embedThirdPartyPlatformForTrailer'); ?>'
                     $('#embed_url_error').html('');
                     var movie_id = $('#product_id').val();
                     $.post(action_url, {'third_party_url': embed_text,'movie_id': movie_id,'type': 'physical'}, function (res) {
                     showLoader(1);
                     window.location.href = window.location.href;
                     });
                 }else{
                     swal("Oops! Provide a valid m3u8 or iframe embed url.");
                     return false;
             }
         }        
    }
    function searchvideo()
    { 
        var key_word = $("#search_video1").val();
        var key_word = key_word.trim();
        if(key_word.length>=3 || key_word.length==0){
            $('.loaderDiv').show();                                            
            $('#profile button').attr('disabled', 'disabled');
            $.ajax({
                url: "<?php echo Yii::app()->getBaseUrl(true); ?>/admin/ajaxSearchVideo",
                data: {'search_word': key_word},
                contentType: false,
                cache: false,
                success: function (result)
                {
                    $('.loaderDiv').hide(); 
                    $('#profile button').removeAttr('disabled');
                    $("#video_content_div").html(result);
                }
            });
        }
        else
        {
        return;

        }
    }

    function addvideoFromVideoGallery(videoUrl, galleryId) {
        showLoader();
        var action_url = HTTP_ROOT + '/admin/addVideoFromVideoGalleryToTrailer';
        $('#server_url_error').html('');
        var movie_id = $('#product_id').val();
        $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id,'type': 'physical'}, function (res) {
            showLoader(1);
            if (res.error) {
                $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
            } else {
                if (res.is_video === 'uploaded') {
                    window.location.href = window.location.href;
                } else if (res.msg === 'Trailer updated successfully') {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Wow! Preview for the content uploaded successfully. It will be available after completion of encoding.</div>'
                    $("#addvideo_popup_physical").modal('hide');
                    $('.pace').prepend(sucmsg);
                    $('#trailer_btn').remove();
                    $('#videoCorrupt').hide();
                    $('#trailerbtn'+movie_id).html('<a href="#" class="f-500"><em class="fa fa-refresh fa-spin blue"></em>&nbsp;&nbsp; Encoding in progress</a>');
                } else {
                    $('#trailerLable').html('There seems to be something wrong with the video');
                }
            }
        }, 'json');
    }























    function openRelatedContent(obj) {
        var content_id = $(obj).attr('data-content_id');
        var content_stream_id = $(obj).attr('data-content_stream_id');
        var url = '<?php echo Yii::app()->baseUrl; ?>/admin/RelatedContent';
        //$("#ppvPopup").html(dv).modal('show');
        $.post(url, {'content_id': content_id,'content_stream_id':content_stream_id,type:1}, function (res) {
            $("#items_popup").html(res).modal('show');
        });
    }
    function showLoader1(isShow) {
      
        if (typeof isShow == 'undefined') {
            
            $('#loaderDiv').show();
            $('#import_all').attr('disabled', 'disabled');
            $('#importontent button').attr('disabled', 'disabled');

        } else {
            
            $('#loaderDiv').hide();
            $('#import_all').removeAttr('disabled');
            $('#importontent button').removeAttr('disabled');
        }
    }
    function show_hint(ele)
    {
      $("#hintmetadata").modal("show"); 
    }
    function download_sample(ele)
    {
    var url="<?= Yii::app()->getBaseUrl(TRUE); ?>/admin/downloadsample";
    window.location =url;
    
    }
    function show_preview()
    {
        $('#uploadexcelForm')[0].submit();
    }
    
     function import_allfile(oInput)
     {
    var _validFileExtensions = [".xls", ".csv",".xlsx"];    
    $("#importfile_name").html($("#import_all").val());
    if (oInput.type == "file") {
         var sFileName = oInput.value;
          if (sFileName.length > 0) {
             var blnValid = false;
             for (var j = 0; j < _validFileExtensions.length; j++) {
                 var sCurExtension = _validFileExtensions[j];
                 if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                     blnValid = true;
                  $('#import_preview').removeAttr('disabled');
                 }
             }

             if (!blnValid) {
                 swal("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                 oInput.value = "";
                 $('#import_preview').attr("disabled","disabled");
                 $("#importfile_name").html("");
                 return false;
             }
         }
         
     }
     return true;
      
     }
     /*
        * modified :   Arvind 
        * email    :   aravind@muvi.com
        * reason   :   Audio Gallery :  
        * functionality : addaudioFromAudioGallery
        * date     :   06-02-2017
        */
     function addaudioFromAudioGallery(videoUrl, galleryId,audioName) {
        showLoader();
        var action_url = '<?php echo $this->createUrl('admin/addAudioFromAudioGallery'); ?>'
        $('#server_url_error').html('');
        var movie_id = $('#movie_id').val();
        var movie_stream_id = $('#movie_stream_id').val();
        $.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id,'audioname':audioName}, function (res) {
            showLoader(1);
            if (res.error) {
                $('#server_url_error').html('There seems to be something wrong with the video. Please contact your Muvi representative. ');
            } else {
                if(res.is_audio === 'uploaded') {
                    window.location.href = window.location.href;
                } else if (res.is_audio) {
                    $("#addvideo_popup").modal('hide');
                    $('#server_url').val('');
                    $('#server_url_error').val('');
                    var text = '<h5><a href="javascript:void(0);" data-toggle="tooltip" title="Video being encoded. It will be available within an hour"><em class="fa fa-refresh fa-spin"></em>&nbsp;&nbsp; Encoding in progress</a></h5>';
                    $('#' + movie_stream_id).html(text);
                } else {
                    $('#server_url_error').html('There seems to be something wrong with the video');
                }
            }
        }, 'json');
        console.log('valid url');
    }
    $(document).on('click', '.panel-heading', function(e){
        var $this = $(this);
            if(!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel-success').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            } else {
                $this.parents('.panel-success').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
    })
    function testFFmpeg(val){
        var action_url1 = '<?php echo $this->createUrl('admin/GetAudioDuration'); ?>';
        $.post(action_url1, {'filename': val},function (res){
            alert(res)
        });
    }
    function testFFmpegBitRate(val){ 
        var action_url1 = '<?php echo $this->createUrl('admin/GetAudioBitRate'); ?>';
        $.post(action_url1, {'filename': val},function (res){
            alert(res) 
        });
    }
    function getInformations3(videoUrl, galleryId) {
      var action_url1 = '<?php echo $this->createUrl('admin/getAudioFromAudioGallery'); ?>';
      var movie_id = $('#movie_id').val();
      var movie_stream_id = $('#movie_stream_id').val();
      //alert("movie_id ::: "+ movie_id + "===================== "+ "movie_stream_id :::" + movie_stream_id);
      $.post(action_url1, {'audio_id': movie_id},function (res){
          console.log(res)
          var resdata = new Array();
          resdata = JSON.parse(res)
          alert(resdata['data']['is_episode'])
          alert(resdata['data']['url'])
      });
  }
  function contentisphysical(){      
      $("input[name='searchForm[is_physical]']").val($('#contentisphysical').val());
      $('#search_content').submit();
  }
  $(function () {
	$("#update_date").daterangepicker({
		initialText: 'Update Date',
		onChange: function () {
			$('#search_content').submit();
		}
	});
	$('.ui-priority-secondary').click(function () {
		if ($(this).text() == 'Clear') {
			if (window.location.search != '') {
				$('#search_content').submit();
			}
		}
	})
});
</script>
