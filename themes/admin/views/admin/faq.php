<div class="row m-b-40">
    <div class="col-xs-12">
        <a href="<?php echo $this->createUrl('admin/addfaq');?>" class="btn btn-primary m-t-10">Add New Help</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="Block">
            <div class="Block-Header">
                <div class="icon-OuterArea--rectangular">
                    <em class="icon-note icon left-icon "></em>
                </div>
                <h4>Latest Contents</h4>
            </div>
            <hr>
            <?php if(!empty($data)): ?>
            <table class="table tbl_repeat">
                <thead>
                    <tr>
                        <th class="width">S/L#</th>
                        <th class="width">Title <a class="pull-right" href="javascript: void(0);" onclick="changeSort('title', '<?php echo ($sortby == 'title' && $order == 'asc')?'desc':'asc';?>')"><em class="fa fa-sort-<?php echo ($sortby == 'title' && $order == 'asc')?'asc':'desc';?>"></em></a></th>
                        <th>Content</th>
                        <th data-hide="phone" class="width">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; foreach($data as $faqs): ?>
                    <tr id="<?php echo $faqs['id']; ?>">
                        <td><?php echo $i++ ?></td>
                        <td><?php echo Yii::app()->common->htmlchars_encode_to_html($faqs['title']); ?></td>
                        <td><?php echo Yii::app()->common->strip_html_tags($faqs['content']); ?></td>
                        <td>
                            <h5><a href="<?php echo $this->createAbsoluteUrl('admin/editfaq/', array('faq_id' => $faqs['id'])); ?>"><em class="icon-pencil"></em>&nbsp;Edit</a></h5>
                            <h5><a href="<?php echo $this->createAbsoluteUrl('admin/faq/', array('option'=>'delete','faq_id' => $faqs['id'])); ?>" class="confirm"><em class="icon-trash"></em>&nbsp;Delete</a></h5>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
            <p class="error">No contents added yet.</p>
            <?php endif; ?>
        </div>
        <div class="pull-right">
            <?php
            /*if($data){
                $opts = array('class' => 'pagination m-t-0 m-b-40');
                $this->widget('CLinkPager', array(
                    'currentPage'=>$pages->getCurrentPage(),
                    'itemCount'=>$item_count,
                    'pageSize'=>$page_size,
                    'maxButtonCount'=>6,
                    'htmlOptions' => $opts,
                    'selectedPageCssClass' => 'active',
                    'nextPageLabel' => '&raquo;',
                    'prevPageLabel'=>'&laquo;',
                    'lastPageLabel'=>'',
                    'firstPageLabel'=>'',
                    'header' => '',
                ));
            }*/
            ?>                            
        </div>
    </div>
</div>  
<script type="text/javascript">
    $(document).ready(function(){
        $("a.confirm").bind('click',function(e) {
            e.preventDefault();
            var location = $(this).attr('href');
            deletefaq(location);
        }); 
    });
    function changeSort(attrib, order){        
        var cur = "<?php echo $_SERVER["REQUEST_URI"];?>";
        $.ajax({
            url: "<?php echo Yii::app()->getBaseUrl(true) ?>/site/formatcurrenturl",
            data: {'pageurl': cur, 'type' : 'sort', 'attrib' : attrib, 'order' : order},
            dataType: 'json',
            success: function (result) {
                console.log(result);
                window.location = result.newurl;
            }
        });        
    }
    function deletefaq(location){
        swal({
           title: "Delete Content?",
           text: "Are you sure you want to delete this content?",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
           confirmButtonText: "Yes",
           closeOnConfirm: true,
           html:true
        }, function() {
          window.location.replace(location);
        });
    }
</script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.tablednd_0_5.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#sortable").sortable();
        $("#sortable").disableSelection();

        $(".tbl_repeat tbody").tableDnD({
            onDrop: function(table, row) {
                var key = 1;
                $('table tbody tr').each(function() {
                    $(this).children('td:first-child').html(key++)
                });
                var orders = $.tableDnD.serialize();
                $.post('<?php echo $this->createUrl('admin/reorderHelpCenterontent'); ?>', {'orders': orders}, function(res) {
                    console.log(res);
                });
            }
        });
    });

</script>   
