<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.form.js"></script>
<?php $jsondata = json_decode($data['data'],TRUE);
//echo "<pre>";print_r($jsondata);exit;
?>
<div class="box-body table-responsive no-padding">
	<div class="col-lg-12">
		<div class="col-lg-3 box-body poster-cls thumbnail" style="min-height:300px;" >
			<img src="<?php echo $jsondata[0]['original_image'];?>" rel="tooltip" title="<?php echo $jsondata[0]['name'];?>" />
			<div class="edit-poster" onclick="change_poster();">Change Poster</div>
		</div>
		<div class="col-md-9 movie-details">
			<div class="box box-primary">
<!--				<div class="box-header">
					<h3 class="box-title">Quick Edit</h3>
				</div>-->
					<form role="form" action="<?php echo $this->createUrl('admin/updateMovieDetails');?>" method="post" enctype="multipart/form-data" name="movie" id='movie' data-toggle="validator">
					<div class="box-body">
						<div class="form-group">
							<label for="movieName">Movie Name:</label>
							<input type='text' placeholder="Enter movie name.." id="mname" name="movie[name]" value="<?php echo $jsondata[0]['name'];?>" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="releaseDate">Release Date:</label>
							<input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="movie[release_date]" value="<?php if(@$movieData['release_date']){echo date('m/d/Y',strtotime($movieData['release_date']));}?>" class="form-control">
						</div>
						<div class="form-group">
							<label for="genre">Genre:</label>
							<select class="form-control" multiple="" name="movie[genre][]">
								<option <?php if(strstr($movieData['genre'],'romance')){echo "selected='selected'";} ?> value="romance">Romance</option>
								<option <?php if(strstr($movieData['genre'],'comedy')){echo "selected='selected'";} ?>  value="comedy">Comedy</option>
								<option  <?php if(strstr($movieData['genre'],'action')){echo "selected='selected'";} ?> value="action">Action</option>
								<option	 <?php if(strstr($movieData['genre'],'thriller')){echo "selected='selected'";} ?>  value="thriller">Thriller</option>
								<option	 <?php if(strstr($movieData['genre'],'horror')){echo "selected='selected'";} ?>  value="horror">Horror</option>
							</select>
						</div>
						<div class="form-group">
							<label for="language">Language: </label>
							<select class="form-control"  name="movie[language]">
								<option <?php if($movieData['language']=='Hindi'){echo "selected='selected'";} ?> value="Hindi">Hindi</option>
								<option <?php if($movieData['language']=='English'){echo "selected='selected'";} ?>  value="English">English</option>
								<option  <?php if($movieData['language']=='Telugu'){echo "selected='selected'";} ?> value="Telugu">Telugu</option>
								<option	 <?php if($movieData['language']=='Tamil'){echo "selected='selected'";} ?>  value="Tamil">Tamil</option>
								<option <?php if($movieData['language']=='Kannada'){echo "selected='selected'";} ?> value="Kannada">Kannada</option>
								<option <?php if($movieData['language']=='Marathi'){echo "selected='selected'";} ?> value="Marathi">Marathi</option>
								<option <?php if($movieData['language']=='Gujarati'){echo "selected='selected'";} ?> value="Gujarati">Gujarati</option>
								<option <?php if($movieData['language']=='Malayalam'){echo "selected='selected'";} ?> value="Malayalam">Malayalam</option>
								<option <?php if($movieData['language']=='Bhojpuri'){echo "selected='selected'";} ?> value="Bhojpuri">Bhojpuri</option>
								<option <?php if($movieData['language']=='Punjabi'){echo "selected='selected'";} ?> value="Punjabi">Punjabi</option>
								<option <?php if($movieData['language']=='Bengali'){echo "selected='selected'";} ?> value="Bengali">Bengali</option>
							</select>
						</div>
						<div class="form-group">
							<label for="genre">Story: </label>
							<textarea placeholder="Enter story..." rows="3" class="form-control" name="movie[story]"><?php if(isset($movieData['story']) && $movieData['story'] ){echo $movieData['story'];}?></textarea>
						</div>
<!--						<div class="form-group">
							<label for="movieName">Add Trailer:</label>
							<input type='url' placeholder="Enter Trailer url.." id="mname" name="movie[trailer]" value="<?php echo @$jsondata[0]['trailer'];?>" class="form-control"/>
						</div>-->
						<div class="form-group">
							<label for="additional_info">Additional Info: </label>
							<textarea placeholder="Enter other information related to movie..." rows="3" class="form-control" name="movie[additional_info]"><?php if(isset($movieData['additional_info']) && $movieData['additional_info']){echo $movieData['additional_info'];}?></textarea>
						</div>
						<div class="form-group">
							<!--<label for="MovieFile">Movie File: &nbsp;<span id="fmovie_name"><?php if($data['full_movie']){echo $data['full_movie']."&nbsp;&nbsp;<a href='javascript:void(0);' onclick='openUploadpopup();'>Change Video</a>&nbsp;<small><em>or</em></small>&nbsp;<a href='javascript:void(0);' onclick='deleteVideo(\"".$movieData['id']."\");'>Remove Video</a> ";}else{echo "<small><em>Not Available</em></small>&nbsp <a href='javascript:void(0);' onclick='openUploadpopup();'>Upload Video</a>";}?></span></label>-->
							<label for="MovieFile"> Movie Format: </label>
								
							<div id="fmovie_name" style="margin-left: 110px;margin-top:-36px;">
								<?php if($data['full_movie']){?>
								<strong>mp4</strong> is available &nbsp;&nbsp
								<?php if($data['full_movie_webm']){?>
										<br/><strong>webm</strong> is available &nbsp;&nbsp
								<?php }
									echo "<a href='javascript:void(0);' onclick='deleteVideo(\"".$movieData['id']."\");'>Remove Video</a> ";
								 }else{
									echo "<small><em>Not Available</em></small>";
								}?>
							</div>
							
						</div>
						<input type="hidden" value="<?php echo $movieData['id'];?>" name="movie_id" id="movie_id"  />
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
						
					</div>
				</form>	
			</div>
		</div>
	</div>
</div>
<div style="clear: both;"></div>
<div class="col-lg-12 box box-primary">
	<div id="castcrew_body">
		<div class="box-header">
			<div style=""><h3 class="box-title">CAST & CREW</h3></div>
			<div style="padding-right: 20px;" class="addcast-cls pull-right"><h4><a href="javascript:void(0);" onclick="addCastCrewpopup();">ADD NEW CAST</a></h4></div>
		</div>
		<div style="display:none" id="add_cast_crew" class="box-body table-responsive no-padding">
			<form class="form-inline" role="form" name="castncrewform" id="castncrewform">
				<table class="table table-hover" >
					<tr>
						<td> 
							<label class="" for="castCrew">Cast/Crew</label>
							<input type="text" class="form-control" id="castname" name="cast_name" placeholder="Enter Name..">
							<input type="hidden" name="cast_id" value="" id="cast_id"/>
						</td>
						<td>
							<div>
								<label class="" for="castType">Type</label>
							</div>
							<div>
								<select class="form-control" multiple=""  name="cast_type[]"id="cast_type" style="width:100%">
							  <option selected="selected" value="actor">Actor</option>
							  <option value="producer">Producer</option>
							  <option value="director">Director</option>
							  <option value="musics">Musics</option>
							  <option value="writer">Writer</option>
							  <option value="cinematographer">Cinematographer</option>
							  <option value="distributor">Distributor</option>
							  <option value="editor">Editor</option>
							  <option value="special effects supervisor">Special Effects Supervisor</option>
							  <option value="art director">Art Director</option>
							  <option value="stunt director">Stunt Director</option>
							  <option value="casting director">Casting Director</option>
							  <option value="location manager">Location Manager</option>
							  <option value="dialogue director">Dialogue Director</option>
							  <option value="sound designer">Sound Designer</option>
							  <option value="makeup artist">Makeup Artist</option>
							  <option value="costume designer">Costume Designer</option>
							  <option value="music director">Music Director</option>
							  <option value="lyricist">Lyricist</option>
							  <option value="choreographer">Choreographer</option>
							  <option value="singer">Singer</option>
							</select>
							</div>
						</td>
						<td>
							<label class="" for="castChar">Character</label>
							<input type="text" class="form-control" id="castchar" name="cast_char" placeholder="Enter cast character...">
						</td>
						<td>
							<div class="pull-10" style="padding-top: 27px;">
								<button type="button" class="btn btn-primary" onclick="addCast();">Add</button>
								<button type="button" class="btn btn-default" onclick="addCastCrewpopup();">Cancel</button>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<?php if(isset($jsondata[0]['cast_detail']) && !empty($jsondata[0]['cast_detail'])){
			foreach($jsondata[0]['cast_detail'] AS $key=>$val){?>
				<div class="col-lg-3" >
					<div class="box box-info">
						<div class="box-body">
							<div>
							<div style="float:left;" class=""><a href="http://muvi.com/stars/<?php echo str_replace(' ',"-",$val['celeb_name']); ?>"><img style="width:40px; height:40px;" src="<?php echo $val['celeb_image'];?>" alt="<?php echo $val['celeb_name'];?>"></a></div>
							<div style="float:left;padding-left: 20px;" class="">
								<?php echo $val['celeb_name'];?><br/>
								<em class="disabled"><small><?php echo ucfirst($val['cast_type']);?></small></em>
							</div>
							<div style="float:right;" class="pull-right">
								<button class="btn btn-default btn-sm" onclick="removeCast(<?php echo $val['celeb_id'];?>,this);"><i class="fa fa-times"></i></button>
							</div>
							<div style="clear: both;"></div>
							</div>
						</div>
					</div>
				</div>	
	<?php   } 
		}else{
			echo "<p class='text-red' id='nocast_crew'>Opps! you have not added any cast and crew for this movie.</p>";
		}
?>
	</div>
</div>
<!-- Change Poster Popup-->
<?php
	$movie_id = @$jsondata[0]['id'];
	$movieName = @$jsondata[0]['name'];
	$banner_image = @$jsondata[0]['thumb_image'];
	require_once 'change_poster.php';
?>
<!-- Change Poster Popup end-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript">
	var timeoutint ='';
	//For 1mb it takes 12sec
	var avgtimefor1mb = 8;
	var totalTimeneeded = 0;
	//Interval duration taken 15sec
	var intervalDuration =1;
	var perintval = 0;
	var completedPer = 0 ;
	var filesize=0; 
	function openUploadpopup(){
		 $("#addvideo_popup").modal('show');
	}
	function change_poster(){
		 $("#addposter_popup").modal('show');
	}
	(function() {
			$("[data-mask]").inputmask();
			//Autocomplet on cast name field
			
			$('#castname').autocomplete({
				source : function (request, response) {
					$.ajax({
						url: HTTP_ROOT+'/admin/celebAutocomp?term='+$('#castname').val(),
						dataType: "json",
						success: function( data ) {
							response( $.map( data.celeb, function( item ) {
								return {
									label: item.celeb_name,
									value: item.celeb_id
								}
							}));
						}

					});
				},
				select: function( event, ui ) {
					event.preventDefault();
					$("#castname").val(ui.item.label);

				},
				focus: function (event, ui) {
					$("#cast_name").val(ui.item.label);
					$("#cast_id").val(ui.item.value);
					event.preventDefault(); // Prevent the default focus behavior.
				}
			});
			//end 
			
			$('.poster-cls').hover(function(res){
				$('.edit-poster').show();
			},function(){
				$('.edit-poster').hide();
			});

		  var bar = $('#pstatus');
		  var movie_name = $('#video_id').val();
		  $('#addvideo_form').ajaxForm({
			  beforeSend: function() {
				$('#progressbar-popup').show();
				// status.empty();
				 totalTimeneeded=0;
				 perintval=0;
				 completedPer=0;
				 filesize=0;
				 var percentVal = '0%';
				 GetFileSize('video_id');
				 bar.width(percentVal)
				 //percent.html(percentVal);
				 movie_name = $('#video_id').val();
			  },
			  uploadProgress: function(event, position, total, percentComplete) {
				//check_fileupload_size($('#video_id').val());
				$('#addvideo_popup').modal('hide');
				if(percentComplete>90){
					percentComplete = 90;
				}
			  },
			  success: function() {
				  $('#addvideo_popup').modal('hide');
				  var percentVal = '100%';
				  bar.width(percentVal);
				  $('#fmovie_name').html(movie_name+'&nbsp;<a href="javascript:void(0);" onclick="openUploadpopup()">Change Video</a>');
				  clearTimeout(timeoutint);
			  },
			  complete: function(xhr) {
				   clearTimeout(timeoutint);
				   var percentVal = '100%';
				   bar.width(percentVal)
				   $('#progressbar-popup').hide();
				   $('#addvideo_popup').modal('hide');
				   if(xhr.responseText=='size_error'){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
				   }else if(xhr.responseText=='extension_error'){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
				   }else if(xhr.responseText=='error'){
						$("#video_id").val('');
						alert('Sorry, We are having some problem in uploading the file. \n Please try again after sometime');
				   }else{
					   $("#video_id").val('');
					   $('#fmovie_name').html(movie_name+'&nbsp;<a href="javascript:void(0);" onclick="openUploadpopup()">Change Video</a>');
				   }
				   
				   
				   
				   console.log(xhr.responseText);
				   //status.html(xhr.responseText);
			  }
		  });
	  })();
	  var settout ='';
	  function addCastCrewpopup(){
		$('#add_cast_crew').toggle('slow');
		settout = setTimeout('scrollwindow()','1000');
	  }
	  function scrollwindow(){
		  window.scrollTo(0,document.body.scrollHeight);
		  clearTimeout(settout);
	  }
	  function addCast(){
		  var castid = $('#cast_id').val();
		  var castname = $('#castname').val();
		  var castchar = $('#castchar').val();
		  var casttype = $('#cast_type').val();
		  var movie_id = $('#movie_id').val();
		  //console.log(castid+"======"+castname+"----"+castchar+"========"+casttype+"----"+movie_id);
		  var url = HTTP_ROOT+"/admin/addCastCrew";
		  var imgsrc = '';
		  $.post(url,{'castid':castid,'castname':castname,'castchar':castchar,'casttype':casttype,'movie_id':movie_id},function(data){
				if(data.success){
					imgsrc = data.img;
					var newcasthtm ='<div class="col-lg-3" ><div class="box box-info"><div class="box-body"><div><span class=""><a href="http://muvi.com/stars/'+castname.replace(/ /gi,"+")+'"><img style="width:35px; height:35px;" src="'+imgsrc+'" alt="'+castname+'"></a></span><span class="">'+castname+'<br><small><em class="disable">'+casttype+'</em></small></span><span class="pull-right"><button data-widget="remove" class="btn btn-default btn-sm" onclick="removeCast('+castid+',\''+casttype+'\',this);"><i class="fa fa-times"></i></button></span></div></div></div></div>';
					$('#castcrew_body').append(newcasthtm);
					$('#cast_id').val('');$('#castname').val('');$('#castchar').val('');$('#cast_type').val('');
					$('#add_cast_crew').toggle('slow');
					$('#nocast_crew').hide();
				}else if(data.error){
					alert(data.msg);
				}
		  },'json');
			
			

	  }
	  function removeCast(cast_id,obj){
		if(confirm('Are you sure you want to remove this actor from the movie?')){
		  var url = HTTP_ROOT+"/admin/removeCastCrew";
		  var movie_id = $('#movie_id').val();
		  $.post(url,{'castid':cast_id,'movie_id':movie_id},function(res){
				$(obj).parents('div .col-lg-3').remove();
		  });
		}
	  }
	  
	  // remove Video
	  function deleteVideo(movie_id){
		  if(confirm('Are you sure you want to remove this video from the movie? \n This video will parmanetly deleted from the movie.')){
			  $.post(HTTP_ROOT+"/admin/removeVideo",{'is_ajax':1,'movie_id':movie_id},function(res){
				  if(res.error==1){
					  alert('Error in removing movie');
				  }else {
					  $('#fmovie_name').html("<small><em>Not Available</em></small>");
					  alert('Your movie video removed successfully');
				  }
			  },'json');
		  }
	  }
	  
	  function checkextension(){
		// get the file name, possibly with path (depends on browser)
		var filename = $("#video_id").val();
		// Use a regular expression to trim everything before final dot
		var extension = filename.replace(/^.*\./, '');
		// Iff there is no dot anywhere in filename, we would have extension == filename,
		// so we account for this possibility now
		if (extension == filename) {
			extension = '';
		} else {
			// if there is an extension, we convert to lower case
			// (N.B. this conversion will not effect the value of the extension
			// on the file upload.)
			extension = extension.toLowerCase();
		}
		switch (extension) {
			case 'mp4':
				// Checking size of the file
				//for IE
				if ($.browser.msie) {
					//before making an object of ActiveXObject,
					//please make sure ActiveX is enabled in your IE browser
					var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
					var objFile = objFSO.getFile(filePath);
					var fileSize = objFile.size; //size in kb
					fileSize = (fileSize / 1048576).toFixed(2); //size in mb
					if(parseFloat(fileSize)>5120){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
					}
				}
				//for FF, Safari, Opeara and Others
				else {
					fileSize = $("#video_id")[0].files[0].size //size in kb
					fileSize = (fileSize / 1048576).toFixed(2); //size in mb
					if(parseFloat(fileSize)>5120){
						$("#video_id").val('');
						alert('Sorry, the file you are trying to upload is too big \n Please upload a file size less then 5GB');
					}
				}
				break;
			default:
				// Cancel the form submission
				alert('Sorry! This video format is not supported. \n Only MP4 format video are allowed to upload');
				$("#video_id").val('');
				break;
		}

	  }
	  function GetFileSize(fileid) {
		try {
		//for IE
		if ($.browser.msie) {
			//before making an object of ActiveXObject,
			//please make sure ActiveX is enabled in your IE browser
			var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
			var objFile = objFSO.getFile(filePath);
			var fileSize = objFile.size; //size in kb
			fileSize = (fileSize / 1048576).toFixed(2); //size in mb
		}
		//for FF, Safari, Opeara and Others
		else {
			fileSize = $("#" + fileid)[0].files[0].size //size in kb
			fileSize = (fileSize / 1048576).toFixed(2); //size in mb
		}
		console.log("Uploaded File Size is" + fileSize + "MB");
		uploadprogressbar(fileSize);
		}
		catch (e) {
		alert("Error is :" + e);
		}
	}

	function uploadprogressbar(filesize){
		if(!totalTimeneeded){
			totalTimeneeded = (parseFloat(avgtimefor1mb)*parseFloat(filesize)).toFixed(2);
			console.log(totalTimeneeded);
			perintval = ((100/parseFloat(totalTimeneeded))*parseInt(intervalDuration)).toFixed(2);
			console.log(perintval);
		}else{
			completedPer = (parseFloat(completedPer) + parseFloat(perintval));
		}
		if(parseFloat(completedPer)>100){
			completedPer = 100;
		}
		var percentVal = completedPer+'%';
		console.log(percentVal);
		$('#pstatus').width(percentVal);
		timeoutint = setTimeout("uploadprogressbar("+filesize+")",'1000');
	}
</script>