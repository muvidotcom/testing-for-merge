<div class="row">
    <div class="col-sm-12 m-t-20">
        <div class="form-horizontal">
            <div class="col-sm-4 text-left">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Method:</label>
                    <div class="col-sm-9 control-label">
                        <label class="radio radio-inline m-r-20">
                            <input type="radio" value="0" name="method" <?php echo (empty($cos) || @$cos['method'] == 0) ? 'checked=checked' : ''; ?>>
                            <i class="input-helper"></i>Manual
                        </label>
                        <label class="radio radio-inline m-r-20">
                            <input type="radio" value="1" name="method" <?php echo (@$cos['method'] == 1) ? 'checked=checked' : ''; ?>>
                            <i class="input-helper"></i>Automatic
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     
<!--- Content listing   -->
<div class="col-sm-12"> 
    <div class="row" id="content_list_div">
        <div class="col-sm-12 m-b-20" style="color: #3c8dbc;font-style: italic;font-weight: bold;">
            Please Drag & Drop in order to arrange content manually
        </div>
    </div>
    <div class="row" id="dropdown_div" style="display: none;">
        <div class="col-sm-4 p-l-0 p-r-0 m-b-20">
            <div class="form-horizontal">
                <label class="col-sm-3 control-label">Order by:</label>
                <div class="col-sm-9">
                    <div class="fg-line">  
                        <div class="select">
                            <select class="form-control input-sm" name="orderby_flag">
                               <option value="1" <?php if(@$cos['orderby_flag']==1){echo "selected=selected";}?>>Views</option>
                               <option value="2" <?php if(@$cos['orderby_flag']==2){echo "selected=selected";}?>>Alphabetic A-Z</option>
                               <option value="3" <?php if(@$cos['orderby_flag']==3){echo "selected=selected";}?>>Alphabetic Z-A</option>
                               <?php /*<option value="4" <?php if(@$cos['orderby_flag']==4){echo "selected=selected";}?>>Ratings</option>*/?>
                            </select>                  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div id="loaderDiv" class="text-center m-b-20 loaderDiv"  style="display:none;">
        <div class="preloader pls-blue text-center " >
            <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
            </svg>
        </div>
    </div>
    <ul id="sortable" class="list-inline text-center"> 
<?php               
           $studio_id=Yii::app()->user->studio_id;
            // $cnt = 1;
            if($data){
            $default_img = '/img/No-Image-Vertical-Thumb.png';
            $default_episode_img = '/img/No-Image-Horizontal.png';
            $postUrl = Yii::app()->common->getPosterCloudFrontPath($studio_id);
          //  $cnt = (($pages->getCurrentPage()) * 20) + 1;
            foreach ($data AS $key => $details) {
                $movie_id         = $details['id'];
                $stream_id        = $details['stream_id'];
                $content_types_id = $details['content_types_id'];
                if(array_key_exists($stream_id, $langcontent['episode'])){
                    $details['episode_title'] = $langcontent['episode'][$stream_id]->episode_title;
                }
                if(array_key_exists($movie_id, $langcontent['film'])){
                    $details['name'] = $langcontent['film'][$movie_id]->name;
                }
                $arg['studio_id']  = $studio_id;
                $arg['movie_id']   = $movie_id;
                $arg['season_id']  = $details['series_number'];
                $arg['episode_id'] = $stream_id;
                $arg['content_types_id'] = $details['content_types_id'];
                $isConverted = $details['is_converted'];
                $isEpisode   = $details['is_episode'];
                $isPPv = 0;
                $isAdvance = 0;
                $isFreeContent = Yii::app()->common->isFreeContent($arg);
                
                //View Count 
                $views = 0;
                if ($details['is_episode']) {
                    $mapped_stream_id= $details['mapped_stream_id'];
                    $views = isset($episodeViewcount[$stream_id]) ? $episodeViewcount[$stream_id] : 0;
                if (isset($episodePosters[$stream_id]) && $episodePosters[$stream_id]['poster_file_name'] != '') {
                    $img_path = $postUrl . '/system/posters/' . $episodePosters[$stream_id]['id'] . '/episode/' . $episodePosters[$stream_id]['poster_file_name'];
                } else {
                    $img_path = $default_episode_img;
                }
                    $contentName = $details['name'] . ' - ' . $details['episode_title'];
                } else {
                    $mapped_id= $details['mapped_id']; 
                    $views = isset($viewcount[$movie_id]) ? $viewcount[$movie_id] : 0;
                    if ($details['content_types_id'] == 2 || $details['content_types_id'] == 4) {
                        $size = 'episode';
                    } else {
                        $size = 'thumb';
                    }

                if (isset($posters[$movie_id]) && $posters[$movie_id]['poster_file_name'] != '') {
                    $img_path = $postUrl . '/system/posters/' . $posters[$movie_id]['id'] . '/' . $size . '/' . $posters[$movie_id]['poster_file_name'];
                } else {
                    $img_path = $default_img;
                }
                    $contentName = $details['name'];
                }
                $plink = (isset($details['permalink']) && $details['permalink']) ? $details['permalink'] : str_replace(' ', "-", strtolower($details['name']));
                if (false === file_get_contents($img_path)) {
                    if ($details['is_episode'] || $details['content_types_id'] == 2 || $details['content_types_id'] == 4  ) {
                        $img_path = $default_episode_img;
                    }else{
                        $img_path = $default_img;
                    }
                }				
				if(@$mapped_stream_id){
					$img_path = $episodePosters[$mapped_stream_id]['poster_url'] ;
				}
				if(@$mapped_id){
					$img_path = $posters[$mapped_id]['poster_url'] ;
				}
                
                 ?>
             <li class="poster_listng ui-state-default" data-streamid="<?php echo $stream_id ?>" data-movieid="<?php echo $movie_id ?>"> 
                 <div class="Preview-Block"> 
                     <div class="thumbnail m-b-0"> 
                         <div class="relative">
                             <!--Place your Image Preview Here-->                              
                             <img src="<?php echo $img_path; ?>" alt="">
                             
                         </div> 
                     </div> 
                 </div> 
                 
             </li>
            <?php } } ?>
         </ul>
</div>

             
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12/themes/resources/demos/style.css">
  <style>
 
  #sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; width: 100px;  font-size: 4em; text-align: center; }
  </style>  
<!--  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
 <script type="text/javascript">
  $(function(){
    $("#sortable" ).sortable({
     start: function(event, ui) {
        ui.item.startPos = ui.item.index();
    },
    stop: function(event, ui) {
        console.log("Start position: " + ui.item.startPos);
        console.log("New position: " + ui.item.index());    
    }
    });
    $("#sortable" ).disableSelection();
    $('input[name="method"]').click(function(){
        if ( $(this).is(':checked') ) {
            if ($(this).val() == '1' ) {
                $('#content_list_div').hide();
                $('#dropdown_div').show();
            } else {
                $('#dropdown_div').hide();
                $('#content_list_div').show();
            }
        }
    });
    var method = $('input[name="method"]:checked').val();
    if (method == '1' ) {
        $('#content_list_div').hide();
        $('#dropdown_div').show();
    }
});
</script>
