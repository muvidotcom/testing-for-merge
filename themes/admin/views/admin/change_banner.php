<?php
$content_banner = Yii::app()->general->getContentBannerSize(Yii::app()->user->studio_id);
$banner_width = $content_banner['banner_width'];
$banner_height = $content_banner['banner_height'];
?>
<!-- Change Poster Popup-->
<div id="bannerImagePopup" class="modal fade">
    <div class="modal-dialog">
		<form action="<?php echo $this->createUrl('admin/addBannerImage');?>" method="post" enctype="multipart/form-data" id="addbanner_form" data-toggle="validator">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Banner Image</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
					<label for="movie_name" class="required">Current Banner: <img height="60px" width="100px" src="<?php echo @$banner_image;?>" rel="tooltip" title="<?php echo $movieName;?>" /></label>
				</div>
                <div class="form-group">
					<label for="uploadposter">Upload Banner</label>
					<input type="file" id="banner_id" name="Filedata" required>
					<p class="help-block">Upload image size of <?php echo $poster_width;?> x <?php echo $poster_height;?>.</p>
				</div>
				<div id="responseUploadify"></div>
            </div>
			<input type="hidden" value="<?php echo @$movie_id;?>" name="movie_id" id="movie_id"  />
			<input type="hidden" value="<?php echo @$movieName;?>" name="movie_name" id="movie_id"  />
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
		</form>	
    </div>
</div>
<!-- Change Poster Popup end-->