<style>
    Preview-Block p{
        width:300px;
        overflow: hidden;

    }

</style>

<ul class="list-inline text-center" id="all_gallery_video">

	<?php
	$studio_id = Yii::app()->user->studio_id;
	$base_cloud_url = Yii::app()->common->getFileGalleryCloudFrontPath($studio_id);

	$studio = $this->studio;
	$posterImg = POSTER_URL . '/no-image-a.png';
//print_r($all_videos);
//echo $base_cloud_url;
	if (count($all_videos) > 0) {
		$video_thumb_path = "/img/No-Image-Horizontal.png";
		foreach ($all_videos as $key => $val) {
				$orig_video_path = $base_cloud_url . $val['file_name'];
			?> 
			<li>
				<div class="Preview-Block video_thumb">
					<div class="thumbnail m-b-0">
						<div class="relative m-b-10">

							<input type="hidden" name="original_video<?php echo $val['id']; ?>" id=original_video<?php echo $val['id']; ?>" value="<?php echo $orig_video_path; ?>">
							<input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['file_name']; ?>"    />
							<img class="img" src="<?php echo $video_thumb_path; ?>"  alt="<?php echo "All_Image"; ?>">

							<div class="caption overlay overlay-white">
								<div class="overlay-Text">
									<div>
										<a href="javascript:void(0);" onclick="addFileFromFileGallery('<?php echo $orig_video_path; ?>', '<?php echo $val['id']; ?>');">
											<span class="btn btn-primary btn-sm">
												<em class="icon-check"></em> &nbsp; Select Video
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<p style="font-size:12px !important;" title="<?php echo $val['file_name']; ?>"><?php echo strlen($val['file_name']) > 40 ? substr($val['file_name'], 0, 40) : $val['file_name']; ?></p>
					</div>
				</div>
			</li>
		<?php
		}
	} else {
		?>
	    <li><p class="lead"> No File found !!!</p></li>
			<?php
		}
		?>

</ul>
<script>
	$(document).ready(function () {

		$("[rel='tooltip']").tooltip();

		$('.thumbnail').hover(
				function () {
					$(this).find('.caption').slideDown(250); //.fadeIn(250)
				},
				function () {
					$(this).find('.caption').slideUp(250); //.fadeOut(205)
				}
		);
		$('#filter_dropdown').change(function () {
			$('#search_content').submit();
		});
	});

</script>
