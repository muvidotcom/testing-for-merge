<div id="firetvimagediv" style="display: <?php if($parent_content_type_id==5){echo "none;";}?>">
<input type="hidden" value="<?php echo $cropDimesion['width']; ?>" name="reqwidthfire" id="reqwidthfire" />
<input type="hidden" value="<?php echo $cropDimesion['height']; ?>" name="reqheightfire" id="reqheightfire" /> 
<div>
    <label class="control-label m-t-20">Poster for FireTV / AppleTV</label>
    <div class="text-center m-t-10">
        <input type="button" class="btn btn-default-with-bg" data-toggle="modal" data-target="#firetvmodal" value="Browse" <?= $disable;?>>
        <h5 class="grey m-t-10 m-b-20">Upload image size of <span class="reqimgsize">800X600</span></h5>
    </div>
    <div class="text-center">
        <div class="m-b-10 displayInline fixedWidth--Preview">
            <?php
            $no_image_array = '/img/No-Image-Vertical.png';
            if (!in_array($TvAppPosterImg, $no_image_array)) {
                ?>
                <div class="poster-cls  avatar-view jcrop-thumb">
                    <div id="avatar_preview_div_fire">                                                
                        <?php if (strpos($TvAppPosterImg, 'No-Image') > -1) { ?>
                            <img data-src="holder.js/<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" alt="<?php echo $cropDimesion['width']; ?>x<?php echo $cropDimesion['height']; ?>" style="max-width: 100% !important;max-height: 220px !important;"/>
                        <?php } else { ?>
                            <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $TvAppPosterImg; ?>" style="max-width: 100% !important;max-height: 220px !important;">
                        <?php } ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="poster-cls  avatar-view jcrop-thumb">
                    <div id="avatar_preview_div_fire">
                        <img title="poster-img" rel="tooltip" id="preview_content_img" src="<?php echo $TvAppPosterImg; ?>" style="max-width: 100% !important;max-height: 220px !important;">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<canvas id="previewcanvasfire" style="overflow:hidden;display: none;margin-left: 10px;width: 100%;"></canvas>
<?php if (@$edit_id && !(strpos($TvAppPosterImg, 'No-Image') > -1)) { if($disable == ""){?>
    <div class="caption text-center">
        <a id="remove-tvposter-text" class="btn remove_btn" href="javascript:void(0);" onclick="return removeapptvposter(<?php echo $edit_id; ?>, <?php echo $obj_type; ?>, <?php echo $movie_stream_id; ?>);" ><em class="icon-close small-icon"></em>&nbsp;&nbsp;Remove Poster</a>
    </div>
<?php } }?>
<div class="modal is-Large-Modal fade" id="firetvmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-left">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload</h4>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active" onclick="hide_file_fire()">
                            <a href="#Upload-firevideo" aria-controls="Upload-firevideo" role="tab" data-toggle="tab">Upload Poster</a>
                        </li>
                        <li role="presentation" onclick="hide_gallery_fire()"> 
                            <a href="#Choose-From-firelibrary" aria-controls="Choose-From-firelibrary" role="tab" data-toggle="tab">Choose from Gallery</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="Upload-firevideo">
                            <div class="row is-Scrollable">
                                <div class="col-xs-12 m-t-40 m-b-20 text-center">
                                    <input type="button" class="btn btn-default-with-bg btn-sm" value="Upload File" onclick="click_browse('celeb_pic_fire')">
                                    <input id="celeb_pic_fire" name="Filedata_fire" type="file" onchange="fileSelectHandlerfire()" style="display:none;" />
                                    <p class="help-block"><?php echo "Upload a transparent image of size <span id='upload-sizes'>" . $cropDimesion['width'] . "x" . $cropDimesion['height'] . '</span>'; ?></p>
                                </div>
                                <input type="hidden" id="fx1" name="fileimage_fire[x1]" />
                                <input type="hidden" id="fy1" name="fileimage_fire[y1]" />
                                <input type="hidden" id="fx2" name="fileimage_fire[x2]" />
                                <input type="hidden" id="fy2" name="fileimage_fire[y2]" />
                                <input type="hidden" id="fw" name="fileimage_fire[w]"/>
                                <input type="hidden" id="fh" name="fileimage_fire[h]"/>
                                <div class="col-xs-12">
                                    <div class="Preview-Block">
                                        <div class="thumbnail m-b-0">
                                            <div id="celeb_preview_fire" class=" col-md-12 margin-topdiv">
                                                <img id="preview_fire" />
                                            </div>
                                        </div>
                                        <?php if ($celeb['poster']['poster_file_name']) { ?>
                                            <div class="thumbnail m-b-0" id="editceleb_preview_fire">
                                                <div class="relative m-b-10">
                                                    <?php
                                                    $postUrl = Yii::app()->common->getPosterCloudFrontPath(Yii::app()->user->studio_id);
                                                    ?>
                                                    <img src="<?php echo $postUrl . '/system/posters/' . $celeb['poster']['id'] . "/medium/" . $celeb['poster']['poster_file_name'] ?>" />
                                                </div>
                                            </div>
                                        <?php } ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="Choose-From-firelibrary">
                            <input type="hidden" name="g_image_file_name_fire" id="g_image_file_name_fire" />
                            <input type="hidden" name="g_original_image_fire" id="g_original_image_fire" />
                            <input type="hidden" id="fx13" name="jcrop_allimage_fire[x13]" />
                            <input type="hidden" id="fy13" name="jcrop_allimage_fire[y13]" />
                            <input type="hidden" id="fx23" name="jcrop_allimage_fire[x23]" />
                            <input type="hidden" id="fy23" name="jcrop_allimage_fire[y23]" />
                            <input type="hidden" id="fw3" name="jcrop_allimage_fire[w3]" />
                            <input type="hidden" id="fh3" name="jcrop_allimage_fire[h3]" />
                            <div class="row  Gallery-Row">
                                <div class="col-md-6 is-Scrollable p-t-40 p-b-40">
                                    <ul class="list-inline text-left">
                                        <?php
                                        foreach ($all_images as $key => $val) {
                                            if ($val['image_name'] == '') {
                                                $img_path = $_SERVER['DOCUMENT_ROOT'] . '/' . SUB_FOLDER . 'images/public/system/no-image-h.png';
                                            } else {
                                                $img_path = $base_cloud_url . $val['s3_thumb_name'];
                                                $orig_img_path = $base_cloud_url . $val['s3_original_name'];
                                            }
                                            ?> 
                                            <li>
                                                <div class="Preview-Block">
                                                    <div class="thumbnail m-b-0">
                                                        <div class="relative m-b-10">
                                                            <input type="hidden" name="original_image<?php echo $val['id']; ?>" id=original_image<?php echo $val['id']; ?>" value="<?php echo $orig_img_path; ?>">
                                                            <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['image_name']; ?>"    />
                                                            <img class="img" src="<?php echo $img_path; ?>"  alt="<?php echo "All_Image"; ?>"  >

                                                            <div class="caption overlay overlay-white">
                                                                <div class="overlay-Text">
                                                                    <div>
                                                                        <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="toggle_preview_fire(<?php echo $val['id']; ?>, '<?php echo $orig_img_path; ?>', '<?php echo $val['image_name']; ?>')">
                                                                            <span class="btn btn-primary icon-with-fixed-width">
                                                                                <em class="icon-check"></em>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>    
                                    </ul>
                                </div>
                                <div class="col-md-6  is-Scrollable p-t-40 p-b-40">
                                    <div class="text-center m-b-20 loaderDiv"  style="display: none;">
                                        <div class="preloader pls-blue  ">
                                            <svg class="pl-circular" viewBox="25 25 50 50">
                                            <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="Preview-Block row">
                                        <div class="col-md-12 text-center" id="gallery_preview_fire">
                                            <img id="glry_preview_fire" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="seepreviewfire(this);">Next</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeapptvposter(movie_id, obj_type, movie_stream_id) {
        swal({
                title: "Remove Poster?",
                text: "Are you sure to remove this poster?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
                confirmButtonText: "Yes",
                closeOnConfirm: true,
                html: true
            }, function () {
            if (movie_id) {
                var url = HTTP_ROOT + "/admin/RemoveAppTvPoster";
                $('#remove-tvposter-text').text('Removing...');
                $('#remove-tvposter-text').attr('disabled', 'disabled');
                $.post(url, {'movie_id': movie_id, 'obj_type': obj_type, 'movie_stream_id': movie_stream_id, 'is_ajax': 1}, function (res) {
                    if (res.err) {
                        $('#remove-tvposter-text').removeAttr('disabled');
                        var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting poster.</div>'
                        $('.pace').prepend(sucmsg);
                        $('#remove-tvposter-text').text('Remove');
                    } else {
                        var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Poster removed successfully.</div>'
                        $('.pace').prepend(sucmsg);
                        $('#remove-tvposter-text').text('');
                        $('#avatar_preview_div_fire').children('img').remove();
                        //$('#avatar_preview_div').html('<img src="' + defaultposter + '" rel="tooltip" title="Upload your poster" />');
                        window.location.reload(1);
                    }
                }, 'json');
            } else {
                return false;
            }
        });
    }
    function hide_file_fire() {
        $('#glry_preview_fire').css("display", "none");
        $('#celeb_preview_fire').css("display", "none");
        $('#preview_fire').css("display", "none");
        document.getElementById('celeb_pic_fire').value = null;
    }
    function hide_gallery_fire() {
        $('#preview_fire').css("display", "none");
        $("#glry_preview_fire").css("display", "block");
        $('#gallery_preview_fire').css("display", "none");
        $("#g_image_file_name_fire").val("");
        $("#g_original_image_fire").val("");
    }
    function posterpreviewfire(obj) {
        $("#previewcanvasfire").show();
        var canvaswidth = $("#reqwidthfire").val();
        var canvasheight = $("#reqheightfire").val();
        if ($('#g_image_file_name_fire').val() == '') {
            var x1 = $('#fx1').val();
            var y1 = $('#fy1').val();
            var width = $('#fw').val();
            var height = $('#fh').val();
        } else {
            var x1 = $('#fx13').val();
            var y1 = $('#fy13').val();
            var width = $('#fw3').val();
            var height = $('#fh3').val();
        }
        var canvas = $("#previewcanvasfire")[0];
        var context = canvas.getContext('2d');
        var img = new Image();
        img.onload = function () {
            canvas.height = canvasheight;
            canvas.width = canvaswidth;
            context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
            //$('#imgCropped').val(canvas.toDataURL());
        };
        $("#avatar_preview_div_fire").hide();
        if ($('#g_image_file_name_fire').val() == '') {
            img.src = $('#preview_fire').attr("src");
        } else {
            img.src = $('#glry_preview_fire').attr("src");
        }
        $('#firetvmodal').modal('hide');
        $(obj).html("Next");
    }
    function seepreviewfire(obj) {
        if ($("#fx13").val() != "") {
            $(obj).html("Please Wait");
            $('#firetvmodal').modal({backdrop: 'static', keyboard: false});
            posterpreviewfire(obj);
        } else {
            if ($("#celeb_preview_fire").hasClass("hide")) {
                $('#firetvmodal').modal('hide');
                $(obj).html("Next");
            } else {
                $(obj).html("Please Wait");
                $('#firetvmodal').modal({backdrop: 'static', keyboard: false});
                if ($("#fx13").val() != "") {
                    posterpreviewfire(obj);
                } else if ($('#fx1').val() != "") {
                    posterpreviewfire(obj);
                } else {
                    $('#firetvmodal').modal('hide');
                    $(obj).html("Next");
                }
            }
        }
    }
    function fileSelectHandlerfire() {
        $('#celeb_preview_fire').css("display", "block");
        $("#g_image_file_name_fire").val('');
        $("#g_original_image_fire").val('');
        $('#glry_preview_fire').css("display", "none");
        clearInfofire();
        $(".jcrop-keymgr").css("display", "none");
        $("#editceleb_preview_fire").hide();
        $("#celeb_preview_fire").removeClass("hide");
        var reqwidth = parseInt($('#reqwidthfire').val());
        var reqheight = parseInt($('#reqheightfire').val());
        var aspectRatio = reqwidth / reqheight;
        var oFile = $('#celeb_pic_fire')[0].files[0];
        var ext = oFile.name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#celeb_preview_fire").addClass("hide");
            document.getElementById("celeb_pic_fire").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        if (oFile.name.match(/['|"|-|,]/)) {
            $("#celeb_preview_fire").addClass("hide");
            document.getElementById("celeb_pic_fire").value = "";
            $('#topbanner_submit_btn').attr('disabled', 'disabled');
            swal('File names with symbols such as \' , - are not supported');
            return;
        }
        var rFilter = /^(image\/jpeg|image\/png)$/i;
        if (!rFilter.test(oFile.type)) {
            $("#celeb_preview_fire").addClass("hide");
            document.getElementById("celeb_pic_fire").value = "";

            swal('Please select a valid image file (jpg and png are allowed)');
            return;
        }
        var img = new Image();
        img.src = window.URL.createObjectURL(oFile);
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);

            if (width < reqwidth || height < reqheight) {
                $("#celeb_preview_fire").addClass("hide");
                document.getElementById("celeb_pic_fire").value = "";
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' x ' + reqheight);

                return;
            }
            var oImage = document.getElementById('preview_fire');
            var oReader = new FileReader();
            oReader.onload = function (e) {
                $('.error').hide();
                oImage.src = e.target.result;
                oImage.onload = function () {
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview_fire').width(oImage.naturalWidth);
                        $('#preview_fire').height(oImage.naturalHeight);
                        $('#glry_preview_fire').width("450");
                        $('#glry_preview_fire').height("250");
                    }
                    $('#preview_fire').Jcrop({
                        minSize: [reqwidth, reqheight],
                        aspectRatio: aspectRatio,
                        boxWidth: 400,
                        boxHeight: 150,
                        bgFade: true,
                        bgOpacity: .3,
                        onChange: updateInfofire,
                        onSelect: updateInfofire,
                        onRelease: clearInfofire
                    }, function () {
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        jcrop_api = this;
                        jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                    });
                };
            };
            oReader.readAsDataURL(oFile);
        }
    }
    function toggle_preview_fire(id, img_src, name_of_image) {

        $('#gallery_preview_fire').css("display", "block");
        clearInfofire();
        $('#gallery_preview_fire').removeClass("hide");
        document.getElementById("celeb_pic_fire").value = "";
        var reqwidth = parseInt($('#reqwidthfire').val());
        var reqheight = parseInt($('#reqheightfire').val());
        var aspectRatio = reqwidth / reqheight;
        showLoader();
        var image_file_name = name_of_image;
        var image_src = img_src;
        $("#g_image_file_name_fire").val(image_file_name);
        $("#g_original_image_fire").val(image_src);
        var res = image_file_name.split(".");
        var image_type = res[1];
        var img = new Image();
        img.src = img_src;
        var width = 0;
        var height = 0;
        img.onload = function () {
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            window.URL.revokeObjectURL(img.src);
            if (width < reqwidth || height < reqheight) {
                showLoader(1);
                $("#gallery_preview_fire").addClass("hide");
                $("#g_image_file_name_fire").val("");
                $("#g_original_image_fire").val("");
                swal('You have selected small file, please select one bigger image file more than ' + reqwidth + ' X ' + reqheight);
                return;
            }
            var oImage = document.getElementById('glry_preview_fire');
            showLoader(1)
            oImage.src = img_src;
            oImage.onload = function () {
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#glry_preview_fire').width(oImage.naturalWidth);
                    $('#glry_preview_fire').height(oImage.naturalHeight);
                    $('#preview_fire').width("800");
                    $('#preview_fire').height("300");
                }
                $('#glry_preview_fire').Jcrop({
                    minSize: [reqwidth, reqheight],
                    aspectRatio: aspectRatio,
                    boxWidth: 400,
                    boxHeight: 150,
                    bgFade: true,
                    bgOpacity: .3,
                    onChange: updateInfoallImagefire,
                    onSelect: updateInfoallImagefire,
                    onRelease: clearInfoallImagefire
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, reqwidth, reqheight]);
                });
            };
        };
    }
    function updateInfoallImagefire(e)
    {
        $('#fx13').val(e.x);
        $('#fy13').val(e.y);
        $('#fx23').val(e.x2);
        $('#fy23').val(e.y2);
        $('#fw3').val(e.w);
        $('#fh3').val(e.h);
    }
    function clearInfoallImagefire() {
        $('#fx13').val('');
        $('#fy13').val('');
        $('#fx23').val('');
        $('#fy23').val('');
        $('#fw3').val('');
        $('#fh3').val('');
        $('button[type="submit"]').removeAttr('disabled');
    }
    function clearInfofire() {
        $('#fx1').val('');
        $('#fy1').val('');
        $('#fx2').val('');
        $('#fy2').val('');
        $('#fw').val('');
        $('#fh').val('');
        $('button[type="submit"]').removeAttr('disabled');
    }
    function showLoader(isShow) {
        if (typeof isShow == 'undefined') {
            $('.loaderDiv').show();
            $('#firetvmodal button,input[type="text"]').attr('disabled', 'disabled');
            $('#profile button').attr('disabled', 'disabled');

        } else {
            $('.loaderDiv').hide();
            $('#firetvmodal button,input[type="text"]').removeAttr('disabled');
            $('#profile button').removeAttr('disabled');
        }
    }
    function updateInfofire(e) {
        $('#fx1').val(e.x);
        $('#fy1').val(e.y);
        $('#fx2').val(e.x2);
        $('#fy2').val(e.y2);
        $('#fw').val(e.w);
        $('#fh').val(e.h);
    }
    function clearInfofire() {
        $('#fx1').val('');
        $('#fy1').val('');
        $('#fx2').val('');
        $('#fy2').val('');
        $('#fw').val('');
        $('#fh').val('');
        $('button[type="submit"]').removeAttr('disabled');
    }
</script>
</div>