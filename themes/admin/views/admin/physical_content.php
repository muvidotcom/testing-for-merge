<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/physical.js?v=<?php echo $vers; ?>"></script>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.tokenize.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.tokenize.js"></script>
<?php $currency_code = $pg['currency_code']; ?>
<input type="hidden" id="is_check_custom" value="<?php if (@$customData) {echo 1;} else {echo 0;} ?>">
<?php $disable = "";
$enable_lang = 'en';
if (isset($data[0]['id'])) {
    $enable_lang = $this->language_code;
	if ($_COOKIE['Language']) {
		$enable_lang = $_COOKIE['Language'];
	}
    $disable = 'disabled="disabled"';
    if (@$data[0]['language_id'] == $this->language_id && @$data[0]['parent_id'] == 0) {
        $disable = "";
    }
}
$movie_id = @$data[0]['id'];
if (array_key_exists($movie_id, @$langcontent['film'])) {
	@$data[0] = Yii::app()->Helper->getLanuageCustomValue(@$data[0],@$langcontent['film'][$movie_id]);
    @$data[0]['name'] = @$langcontent['film'][$movie_id]->name;
    @$data[0]['story'] = @$langcontent['film'][$movie_id]->story;
    @$data[0]['genre'] = @$langcontent['film'][$movie_id]->genre;
    @$data[0]['censor_rating'] = @$langcontent['film'][$movie_id]->censor_rating;
    @$data[0]['language'] = @$langcontent['film'][$movie_id]->language;
}
?>
<div id="formContents">

    <?php
    if(@$customData){
        $defaultFields = array('name','description','sale_price','sku','product_type');
        $relationalFields = Yii::app()->Helper->getRelationalPGField(Yii::app()->user->studio_id); 
        $formData = $customData['formData'];
        unset($customData['formData']);
        foreach ($customData AS $ckey=>$cval){
            if(!in_array($cval['f_name'],$defaultFields)){
                if($relationalFields && array_key_exists($cval['f_name'],$relationalFields)){
                    $cval['f_name'] = $relationalFields[$cval['f_name']]['field_name'];
                }
            }
            $active_fields[] .= $cval['f_name'];
            ?>
        <div class="form-group">
            <label for="<?= $cval['f_display_name'];?>" class="col-md-4 control-label"><?= $cval['f_display_name'];?><?php if($cval['f_is_required']){?><span class="red"><b>*</b></span><?php }?>:</label>
            <?php if(!$cval['f_type']){?>
                <div class="col-md-8">
                    <div class="fg-line">
                        <?php if($cval['f_name']=='sale_price'){?>
                        <div class="moreCurrencyDiv">
                            <?php
                            if(count($currency_code) > 1){
                                $pgcurrenyname = "pg[multi_currency_id][]";
                                $pgsalepricename = "pg[multi_sale_price][]";
                            }else{
                                $pgcurrenyname = "pg[currency_id]";
                                $pgsalepricename = "pg[sale_price]";                                                    
                            }
                            if((count($currency_code) > 1) && !empty($pgmulti)){
                                $counter = 1;
                                foreach ($pgmulti as $mkey => $multivalue) {?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                            <?php 
                                                foreach ($currency_code as $key => $value) {
                                                        if(@$mkey==$value["id"]){
                                                            echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                        }else{
                                                            echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                        }                                                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="fg-line">
                                    <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01" required>
                                    <span class="countdown1"></span>
                                </div>
                            </div>                                                            
                            <div class="col-md-4">                                                    
                                <div class="fg-line">
                                    <h5>
                                        <?php if($counter==1){?>
                                        <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if(count($pgmulti) < count($currency_code)){echo 'block';}else{echo 'none';}?>;">
                                            &nbsp; Add more currency
                                        </a>
                                        <?php }else{?>
                                        <a onclick="removecurrency('<?php echo count($currency_code);?>',this);" href="javascript:void(0);">
                                            &nbsp; Remove currency
                                        </a>
                                        <?php }?>
                                    </h5>
                                </div>                                                                                                       
                            </div>                                                            
                        </div>
                        <?php   $counter++;}
                            }else{
                        ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control input-sm" name="<?= $pgcurrenyname;?>" required="true">
                                            <?php 
                                                foreach ($currency_code as $key => $value) {
                                                    if(@$data[0]['currency_id']==$key){
                                                        echo '<option value='.$value["id"].' selected="selected">'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }else{
                                                        echo '<option value='.$value["id"].'>'.$value["code"] .'('. $value["symbol"] .')'.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="fg-line">
                                    <input type='number' min="1" placeholder="<?= $cval['f_value'];?>" name="<?= $pgsalepricename;?>" value="<?php echo @$data[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01"  required>
                                    <span class="countdown1"></span>
                                </div>
                            </div>
                            <?php if(count($currency_code) > 1){?>
                            <div class="col-md-4">                                                    
                                <div class="fg-line">
                                    <h5>
                                        <a onclick="addmorecurrency('<?php echo count($currency_code);?>');" href="javascript:void(0);" id="addmorelink">
                                            &nbsp; Add more currency
                                        </a>
                                    </h5>
                        </div>
                            </div>
                            <?php }?>
                        </div>
                        <?php }?>
                    </div>
                    <?php }else{?>
                            <input type='text' placeholder="<?= $cval['f_value'];?>" id="<?= $cval['f_id'];?>" name="pg[<?= $cval['f_name'];?>]" value="<?php echo @$data[0][$cval['f_name']]; ?>" class="form-control input-sm checkInput" <?php if($cval['f_is_required']){?> required  <?php }?> >
                    <?php }?>
                    <?php if($cval['f_id']=='skuno'){?>
                            <span class="red" id="email-error" style="display: none;"></span>
                    <?php }?>        
                    </div>
                </div>	
            <?php }elseif($cval['f_type']==1){?>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <textarea class="form-control input-sm checkInput" rows="5" placeholder="<?= $cval['f_value'];?>" name="pg[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$data[0][$cval['f_name']]; ?></textarea>
                        </div>
                    </div>
            <?php }elseif($cval['f_type']==2){?>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                            <?php if($cval['f_name']=='size'){?>
                            <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                <?php
                                    $opData = $pg['shipping_size'];
                                    foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                        if (@$data[0]['id'] && @$data[0][$cval['f_name']]==$opkey) {
                                            $selectedDd = 'selected ="selected"';
                                        }
                                        echo "<option value='".$opkey."' ".$selectedDd." >" . $opvalue . "</option>";
                                    }
                                ?>
                            </select>
                            <?php }else{?>
                            <select name="pg[<?= $cval['f_name'];?>]" placeholder="" id="<?= $cval['f_id'];?>" <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                                <?php
                                    echo "<option value=''>-Select-</option>";
                                    $opData = json_decode($cval['f_value'],true);
									$opData_new = $opData;
									$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
									$opData = empty($opData)?$opData_new:$opData;
                                    foreach($opData AS $opkey=>$opvalue){ $selectedDd = '';
                                        if (@$data[0]['id'] && @$data[0][$cval['f_name']]==$opvalue) {
                                            $selectedDd = 'selected ="selected"';
                                        }
                                        echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                    }
                                ?>
                            </select>
                            <?php }?>
                        </div>
                    </div>
                    </div>
            <?php }elseif($cval['f_type']==3){
                    if($cval['f_id']=='genre' || $cval['f_id']=='language' || $cval['f_id']=='censor_rating'){?>
                    <select  data-role="tagsinput"  name="pg[<?= $cval['f_name'];?>][]" placeholder="Use Enter or Comma to add new" id="<?= $cval['f_id'];?>" multiple >
                        <?php
                        if (@$data[0]['id'] && @$data[0][$cval['f_name']]) {
                            $pregenre = json_decode($data[0][$cval['f_name']]);
                            foreach ($pregenre AS $k => $v) {
                                echo "<option value='" . $v . "' selected='selected'>" . $v . "</option>";
                            }
                        }
                        ?>
                    </select>
                    <?php }else{ ?>
                        <div class="col-md-8">
                            <select name="pg[<?= $cval['f_name'];?>][]" placeholder="" id="<?= $cval['f_id'];?>" multiple <?php if($cval['f_is_required']){?> required  <?php }?> class="form-control input-sm checkInput" >
                            <?php
                                $opData = json_decode($cval['f_value'],true);
								$opData_new = $opData;
								$opData = (array_key_exists($enable_lang, $opData))?$opData[$enable_lang]:$opData['en'];
								$opData = empty($opData)?$opData_new:$opData;
                                foreach($opData AS $opkey=>$opvalue){
                                    $selectedDd = '';
                                    if (@$data[0]['id'] && in_array($opvalue, json_decode($data[0][$cval['f_name']]))) {
                                        $selectedDd = 'selected ="selected"';
                                }
                                    echo "<option value='".$opvalue."' ".$selectedDd." >" . $opvalue . "</option>";
                                }
                                ?>
                            </select>
                        </div>
            <?php	}?>
            <?php }elseif($cval['f_type']==4){
            if($cval['f_name']=='product_type'){?>
                <div class="col-md-8">
                    <div class="control-label row">
                        <div class="col-sm-12">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" value="0" name="pg[<?= $cval['f_name'];?>]" <?php echo (@$data[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$data[0]['product_type'] == 0)?'checked=checked':'';?>>
                                <i class="input-helper"></i>Standalone
                            </label>
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" value="1" name="pg[<?= $cval['f_name'];?>]"<?php echo (@$data[0]['product_type'] == 1)?'checked=checked':'';?>>
                                <i class="input-helper"></i>Link to content
                            </label>
                        </div>
                        <div id="showcntent" <?php echo (@$data[0]['id'] != '' && @$data[0]['product_type'] != 0)?'':'style="display: none;"';?> class="fg-line col-sm-12">
                            <select id="movieid_pgdiv" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple">
                                <?php 
                                $movie_ids = '';
                                if(isset($movies) && !empty($movies)) {
                                    foreach ($movies as $key => $value) {
                                ?>
                                        <option value="<?php echo $value['movie_id'];?>" <?php echo in_array($value['movie_id'],$pgcontent)?'selected=selected':'';?> ><?php echo $value['movie_name'];?></option>
                                    <?php 
                                        if(in_array($value['movie_id'],$pgcontent)){
                                            $movie_ids = ltrim($value['movie_id'], ',');
                                        }
									}	
                                    } ?>
                            </select>                                    
                        </div>
                        <span id="error_movie_id" class="red col-sm-12"></span>
                        <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $movie_ids;?>">
                        <span class="countdown1"></span>
                    </div>
                </div>
			<?php }else{?>
				<div class="col-md-8">
					<textarea class="form-control input-sm checkInput RichText" rows="5" placeholder="<?= $cval['f_value'];?>" name="pg[<?= $cval['f_name'];?>]" id="<?= $cval['f_id'];?>" ><?php echo @$data[0][$cval['f_name']]; ?></textarea>
				</div>
            <?php } }?>
    </div>
    <?php }
                //show default size field if not present
                if(!in_array('size',$active_fields)){
    ?>
                <div class="form-group">
                <label for="size" class="col-md-4 control-label">Shipping Size:</label>
                    <div class="col-md-8">
                        <div class="fg-line">
                            <div class="select">
                                <select id="size" name="pg[size]" class="form-control input-sm">
                                <?php
                                foreach($pg['shipping_size'] AS $key=>$val){
                                ?>
                                    <option value="<?php echo $key;?>" <?php echo (@$fieldval[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
    <?php    
                }
    }else{?>
        <div class="form-group">
            <label for="movieName" class="col-md-4 control-label">Item Name<span class="red"><b>*</b></span>:</label>
            <div class="col-md-8">
                <div class="fg-line">
                    <input type='text' placeholder="Enter item name here." id="mname" name="pg[name]" value="<?php echo htmlspecialchars(@$data[0]['name']); ?>" class="form-control input-sm checkInput" required >
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-md-4 control-label">Description: </label>
            <div class="col-md-8">
                <div class="fg-line">
                    <textarea class="form-control input-sm checkInput" rows="5" placeholder="Enter item description here" name="pg[description]" id="mdescription"><?php echo @$data[0]['description']; ?></textarea>
                    <span class="countdown1"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="itemtype" class="col-md-4 control-label">Item Type: </label>
            <div class="col-md-8">
                <div class="control-label row">
                    <div class="col-sm-12">
                        <label class="radio radio-inline m-r-20">
                            <input type="radio" value="0" name="pg[product_type]" <?php echo (@$data[0]['id'] != '') ? '' : 'checked=checked'; ?> <?php echo (@$data[0]['product_type'] == 0) ? 'checked=checked' : ''; ?>>
                            <i class="input-helper"></i>Standalone
                        </label>
                        <label class="radio radio-inline m-r-20">
                            <input type="radio" value="1" name="pg[product_type]"<?php echo (@$data[0]['product_type'] == 1) ? 'checked=checked' : ''; ?>>
                            <i class="input-helper"></i>Link to content
                        </label>
                    </div>
                    <div id="showcntent" <?php echo (@$data[0]['id'] != '' && @$data[0]['product_type'] != 0) ? '' : 'style="display: none;"'; ?> class="fg-line col-sm-12">
                        <select id="movieid_pgdiv" name="pg[contentid][]" class="form-control tokenize-sample" multiple="multiple">
                        <?php
                        $movie_ids = '';
                        if (isset($movies) && !empty($movies)) {
                            foreach ($movies as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['movie_id']; ?>" <?php echo in_array($value['movie_id'], $pgcontent) ? 'selected=selected' : ''; ?> ><?php echo $value['movie_name']; ?></option>
                            <?php
                            if (in_array($value['movie_id'], $pgcontent)) {
                                $movie_ids = ltrim($value['movie_id'], ',');
                            }
                        }
                        ?>
                    <?php }
                    ?>
                        </select>                                    
                    </div>
                    <span id="error_movie_id" class="red col-sm-12"></span>
                    <input type="hidden" class="form-control" name="pg[contentidtxt]" id="movie_ids" value="<?php echo $movie_ids; ?>">
                    <span class="countdown1"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-md-4 control-label">Price<span class="red"><b>*</b></span>: </label>
            <div class="col-md-8">
            <div class="moreCurrencyDiv">
            <?php
            if (count($currency_code) > 1) {
                $pgcurrenyname = "pg[multi_currency_id][]";
                $pgsalepricename = "pg[multi_sale_price][]";
            } else {
                $pgcurrenyname = "pg[currency_id]";
                $pgsalepricename = "pg[sale_price]";
            }
            if ((count($currency_code) > 1) && !empty($pgmulti)) {
                $counter = 1;
                foreach ($pgmulti as $mkey => $multivalue) {
                    ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm" name="<?= $pgcurrenyname; ?>" required="true">
                            <?php
                            foreach ($currency_code as $key => $value) {
                                if (@$mkey == $value["id"]) {
                                    echo '<option value=' . $value["id"] . ' selected="selected">' . $value["code"] . '(' . $value["symbol"] . ')' . '</option>';
                                } else {
                                    echo '<option value=' . $value["id"] . '>' . $value["code"] . '(' . $value["symbol"] . ')' . '</option>';
                                }
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="fg-line">
                        <input type='number' min="1" placeholder="<?= $cval['f_value']; ?>" name="<?= $pgsalepricename; ?>" value="<?php echo @$multivalue; ?>" class="form-control input-sm cost" step="0.01"  required>
                        <span class="countdown1"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="fg-line">
                        <h5>
                                <?php if ($counter == 1) { ?>
                                <a onclick="addmorecurrency('<?php echo count($currency_code); ?>');" href="javascript:void(0);" id="addmorelink" style="display: <?php if (count($pgmulti) < count($currency_code)) {
                                    echo 'block';
                                } else {
                                    echo 'none';
                                } ?>;">
                                    &nbsp; Add more currency
                                </a>
                                <?php } else { ?>
                                <a onclick="removecurrency('<?php echo count($currency_code); ?>', this);" href="javascript:void(0);">
                                    &nbsp; Remove currency
                                </a>
                                <?php } ?>
                        </h5>
                    </div>                                                                                                       
                </div>                                                            
            </div>
            <?php
            $counter++;
        }
    } else {
        ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="fg-line">
                        <div class="select">
                            <select class="form-control input-sm" name="<?= $pgcurrenyname; ?>" required="true">
                            <?php
                            foreach ($currency_code as $key => $value) {
                                if (@$data[0]['currency_id'] == $key) {
                                    echo '<option value=' . $value["id"] . ' selected="selected">' . $value["code"] . '(' . $value["symbol"] . ')' . '</option>';
                                } else {
                                    echo '<option value=' . $value["id"] . '>' . $value["code"] . '(' . $value["symbol"] . ')' . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="fg-line">
                    <input type='number' min="1" placeholder="<?= $cval['f_value']; ?>" name="<?= $pgsalepricename; ?>" value="<?php echo @$data[0]['sale_price']; ?>" class="form-control input-sm cost" step="0.01"  required>
                    <span class="countdown1"></span>
                </div>
            </div>
            <?php if (count($currency_code) > 1) { ?>
                <div class="col-md-4">                                                    
                    <div class="fg-line">
                        <h5>
                            <a onclick="addmorecurrency('<?php echo count($currency_code); ?>');" href="javascript:void(0);" id="addmorelink">
                                &nbsp; Add more currency
                            </a>
                        </h5>
                    </div>
                </div>
               <?php } ?>
        </div>
    <?php } ?>
                </div>
            </div>
        </div>
        <div id="sku">
            <div class="form-group">
                <label for="sku" class="col-md-4 control-label">SKU Number<span class="red"><b>*</b></span>:</label>
                <div class="col-md-8">
                    <div class="fg-line">
                        <input type='text' placeholder="Enter SKU number." id="skuno" name="pg[sku]" value="<?php echo @$data[0]['sku']; ?>" class="form-control input-sm checkInput" >
                        <span class="red" id="email-error" style="display: none;"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="size">
            <div class="form-group">
                <label for="size" class="col-md-4 control-label">Size:</label>
                <div class="col-md-8">
                    <div class="select">
                        <select id="product_status" name="pg[size]" class="form-control input-sm">
                        <?php
                        foreach($pg['shipping_size'] AS $key=>$val){
                        ?>
                            <option value="<?php echo $key;?>" <?php echo (@$data[0]['size'] == $key) ? 'selected=selected' : ''; ?> ><?php echo $val;?></option>
                        <?php
                        }
                        ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
<!--    <div class="form-group">
        <label for="releaseDate" class="col-md-4 control-label">Release/Recorded Date:</label>
        <div class="col-md-8">
            <div class="fg-line">
                <input type='text' data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'"  id="release_date" name="pg[release_date]" value="<?php if (@$data[0]['release_date']) {echo date('m/d/Y', strtotime(@$data[0]['release_date']));}?>" class="form-control input-sm checkInput" >
            </div>
        </div>
    </div>-->
    <div id="product_status">
        <div class="form-group">
            <label for="status" class="col-md-4 control-label">Status<span class="red"><b>*</b></span>:</label>
            <div class="col-md-8">
                <div class="select">
                    <select id="product_status" name="pg[status]" class="form-control input-sm">
                        <option value="1" <?php echo (@$data[0]['status'] == 1) ? 'selected=selected' : ''; ?> >Active</option>
                        <option value="2" <?php echo (@$data[0]['status'] == 2) ? 'selected=selected' : ''; ?>  >Inactive</option>
                        <option value="3" <?php echo (@$data[0]['status'] == 3) ? 'selected=selected' : ''; ?>  >Out of Stock</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php if (@$pg['enable_category'] == 1) { ?>
        <div class="form-group">
            <label for="category" class="col-md-4 control-label">Content Category<span class="red"><b>*</b></span>: </label>
            <div class="col-md-8">
                <div class="fg-line">
                    <select  name="pg[content_category_value][]" id="content_category_value" required="true" class="form-control input-sm checkInput" <?php echo $disable; ?>>
                        <option value="">-Select-</option>
                        <?php
                        foreach ($contentCategories AS $k => $v) {
                            if(@$data && in_array($k, explode(',', $data[0]['content_category_value']))){//($data[0]['content_category_value'] & $k)
                                $selected = "selected='selected'";
                            } else {
                                $selected = '';
                            }
                            echo '<option value="' . $k . '" ' . $selected . ' >' . $v . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <span class="help-block with-errors"></span>
            </div>
        </div>
    <?php } ?>
    <div class="form-group">
        <div class="col-md-offset-4 col-md-8">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="1" name="pg[content_publish_date]" id="content_publish_date" <?php if (@$data[0]['publish_date']) { ?>checked="checked"<?php } ?>/>
                    <i class="input-helper"></i> Make it Live at Specific time
                </label>
            </div>
            <div class="row m-t-10 <?php if (@$data[0]['content_publish_date']) {} else { ?>content_publish_date_div<?php } ?>" id="content_publish_date_div" <?php if (@$data[0]['publish_date']) { ?>style=""<?php } else { ?> style="display:none;"<?php } ?>>
                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="fg-line">
                            <input class="form-control input-sm publish_date_cls" placeholder="Publish Date" type="text" data-mask="" data-inputmask="'alias': 'mm/dd/yyyy'" placeholder="mm/dd/yyy"  id="publish_date" name="pg[publish_date]" value="<?php echo (@$data[0]['publish_date'] != '0000-00-00' && @$data[0]['publish_date'] != '') ? date('m/d/Y', strtotime($data[0]['publish_date'])) : ''; ?>">
                        </div>
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="input-group">
                        <div class="fg-line">
                            <input class="form-control input-sm cpublish_time_cls" placeholder="Publish Time" data-mask="" data-inputmask="'alias': 'hh:mm'"  placeholder="hh:mm" id="publish_time" name="pg[publish_time]" value="<?php echo (@$data[0]['publish_date'] != '0000-00-00' && @$data[0]['publish_date'] != '') ? date('H:i', strtotime($data[0]['publish_date'])) : ''; ?>" type="text">
                        </div>
                        <span class="input-group-addon"><em class="icon-clock"></em></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="content_publish_date">UTC</label>
                </div>
            </div>
        </div>
    </div>    
    <?php if($productize){?>
	<div class="form-group">
		<label for="itemtype" class="col-md-4 control-label">Personalization: </label>
		<div class="col-md-8">
			<div class="control-label row">
				<div class="col-sm-12">
					<label class="radio radio-inline m-r-20">
						<input type="radio" value="0" name="pg[productize_flag]" <?php echo (@$data[0]['id'] != '')?'':'checked=checked';?> <?php echo (@$fieldval[0]['productize_flag'] == 0)?'checked=checked':'';?>>
						<i class="input-helper"></i>Disabled
					</label>
					<label class="radio radio-inline m-r-20">
						<input type="radio" value="1" name="pg[productize_flag]"<?php echo (@$data[0]['productize_flag'] == 1)?'checked=checked':'';?>>
						<i class="input-helper"></i>Enabled
					</label>
                    <?php if($data[0]['id'] != ''){$cid = $fieldval[0]['id']; ?>
                    <a href="javascript:void(0)" onclick="addCmsCustomizationRecord(<?php echo $cid;?>)">Add/Edit Personalization</a>
                    <?php }?>
                   <a href="javascript:void(0)" class="hide custo" onclick="addCmsCustomizationRecord(<?php echo $cid;?>)">Add/Edit Personalization</a>

				</div>
			</div>
		</div>
        <?php $authToken   = OuathRegistration::model()->findByAttributes(array('studio_id' => $this->studio->id), array('select'=>'oauth_token'))->oauth_token; ?>
	</div>
<?php }?>
    <div class="form-group m-t-30">
        <div class="col-md-offset-4 col-md-8">
            <button type="submit" class="btn btn-primary btn-sm" id="save-btn">
            <?php echo (@$data[0]['id'] != '') ? 'Update' : 'Save'; ?>
            </button>
        </div>
    </div>
</div>
<div style="display: none;" id="defaultcurrencydiv">
	<?php 
		if(count($pg['currency_code']) > 1){
			$pgcurrenyname = "pg[multi_currency_id][]";
			$pgsalepricename = "pg[multi_sale_price][]";
            echo $this->renderPartial('//store/addmorecurrency', array('currency_code'=>$pg['currency_code'],'pgcurrenyname'=>$pgcurrenyname,'pgsalepricename'=>$pgsalepricename), true);
		}else{
			$pgcurrenyname = "pg[currency_id]";
			$pgsalepricename = "pg[sale_price]";                                                    
		}
	?>
</div>
<input type="hidden" id="authToken" value="<?php echo $authToken; ?>">
<input type="hidden" id="prod_id" val="">
<script type="text/javascript">
   var authToken = $('#authToken').val();
   sessionStorage.setItem('HTTP_ROOT',HTTP_ROOT);
   sessionStorage.setItem('STORE_AUTH_TOKEN',authToken);
     
    Array.prototype.getUnique = function () {
        var u = {}, a = [];
        for (var i = 0, l = this.length; i < l; ++i) {
            if (u.hasOwnProperty(this[i])) {
                continue;
            }
            a.push(this[i]);
            u[this[i]] = 1;
        }
        return a;
    }
    $(function () {
        var url = "<?php echo Yii::app()->baseUrl; ?>/admin/movie_autocomplete";
        $('#movieid_pgdiv').tokenize({
            datas: url,
            placeholder: 'Search contents',
            nbDropdownElements: 10,
            onAddToken:function(value, text, e){
                if (parseInt(value)) {
                    var video_ids = $("#movie_ids").val();
                    video_ids = video_ids.split(",");
                    video_ids.push(value);
                    video_ids.toString();
                    $("#movie_ids").val(video_ids);
                }
            },
            onRemoveToken:function(value, e){
                var video_ids = $("#movie_ids").val();
                video_ids = video_ids.split(",");
                
                var index = video_ids.indexOf(value);
                if(index !== -1) {
                    video_ids.splice(index,1);
                }
                video_ids.toString();
                $("#movie_ids").val(video_ids);
            }
        });
    });
    function addmorecurrency(cnt){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv < cnt){
            var morecurreny = $('#defaultcurrencydiv').html();
            $('.moreCurrencyDiv').append(morecurreny);
            var childdiv1 = childdiv+1;
            if(childdiv1==cnt){$('#addmorelink').hide();}
            $('.cost').on("keypress",function(event) {
                return decimalsonly(event);
            });
            $('.cost').keyup(function() {
                if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
            });
        }else{
            $('#addmorelink').hide();
        }
    }
    function removecurrency(cnt,obj){
        cnt = parseInt(cnt);
        var childdiv = $('.moreCurrencyDiv').children('div').length;
        //alert(cnt +' '+ childdiv);
        if(childdiv <= cnt){
            $(obj).parent().parent().parent().parent().remove();
            $('#addmorelink').show();
        }else{
            $('#addmorelink').show();
        }
    }
    function decimalsonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if ((unicode !== 8) && (unicode !== 9)) {
            if ((unicode === 46) || (unicode >= 48 && unicode <= 57)){                
                return true;
            }else
                return false;
        }
    }
    $(document).ready(function(){
        $('.cost').on("keypress",function(event) {
            return decimalsonly(event);
        });
        $('.cost').keyup(function() {
            if($(this).val()!=''){$(this).parent().parent().find('.multierrorprice').remove();}
        });	
        $('input[name="pg[product_type]"]').click(function(){
            if ( $(this).is(':checked') ) {
                if ($(this).val() == '1' ) {
                    $('#showcntent').show();
                } else {
                     $('#showcntent').hide();
                     $('#error_movie_id').html('');
                }
            }
        });
    });    
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#content_publish_date').click(function(){
            0 == this.checked ? $("#content_publish_date_div").hide() : $("#content_publish_date_div").show();

            $('#publish_date').datepicker(),

            $('#publish_time').flatpickr({
				enableTime: true,
				noCalendar: true,
				enableSeconds: false, // disabled by default
				time_24hr: true, // AM/PM time picker is used by default
				// default format
				dateFormat: "H:i",
				// initial values for time. don't use these to preload a date
				defaultHour: 12,
				defaultMinute: 0
			});
		});
    });
    
    function addCmsCustomizationRecord(prodId){ 
      
       var url = HTTP_ROOT+'/shop/AddCmsCustomizationRecord';
         prodId = $('#movie_id').val();
       $.post(url, {'product_id':prodId}, function(res){            
          window.location.href = HTTP_ROOT+'/cmscustomize';
       })  
   console.log(prodId);
   sessionStorage.setItem('CUSTO_PROD_ID',prodId);
   
    } 
     
</script>