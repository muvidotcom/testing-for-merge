<?php
 $base_cloud_url = Yii::app()->common->getVideoGalleryCloudFrontPath($studio_id);
if (count($all_audios)) {
    foreach ($all_audios as $key => $val) {
        if ($val['audio_name'] == '') {
            $video_thumb_path = "/img/No-Image-Horizontal.png";
        } else {
            if ($val['thumb_image_name'] == '') {
                $video_thumb_path = "/img/No-Image-Horizontal.png";
            } else {
                $video_thumb_path = $base_cloud_url . "videogallery-image/" . $val['thumb_image_name'];
            }
            $orig_video_path = $base_cloud_url . $val['audio_name'];
        }
        ?> 
        <li>
            <div class="Preview-Block video_thumb">
                <div class="thumbnail m-b-0">
                    <div class="relative m-b-10">
                        <input type="hidden" name="original_video<?php echo $val['id']; ?>" id=original_video<?php echo $val['id']; ?>" value="<?php echo $orig_video_path; ?>">
                        <input type="hidden" name="file_name<?php echo $val['id']; ?>" id=file_name<?php echo $val['id']; ?>" value="<?php echo $val['audio_name']; ?>"    />
                        <img class="img" src="<?php echo $video_thumb_path; ?>"  alt="<?php echo "All_Image"; ?>" >
                        <div class="caption overlay overlay-white">
                            <div class="overlay-Text">
                                <div>
                                    <a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="addaudioFromAudioGallery('<?php echo $orig_video_path; ?>', '<?php echo $val['id']; ?>');" title="Select Video">
                                    <!--<a href="javascript:void(0);" id="thumb_<?php echo $val['id']; ?>" onclick="getInformations3('<?php echo $orig_video_path; ?>', '<?php echo $val['id']; ?>');" title="Select Video">-->                                                                            
                                        <span class="btn btn-primary btn-sm">
                                            <em class="icon-check"></em>&nbsp; Select Audio
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <p style="font-size:12px !important;"  title="<?php echo $val['audio_name']; ?>"><?php echo strlen($val['audio_name']) > 40 ? substr($val['audio_name'], 0, 40) . ".." : $val['audio_name']; ?></p>
                </div>


            </div>
        </li>
    <?php }
} ?>