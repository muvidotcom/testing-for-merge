<hr>
<div class="row">
    <div class="col-sm-12 m-b-20">
        <?php if(in_array('category', $content)){
            $url="javascript:void(0);";
            $disableCategory = 'style="cursor:not-allowed" title="already synced"';
        }else{
            $url = $this->createUrl('/multistudio/CategorySync');
            $disableCategory = '';
        }?>
        <a href="<?php echo $url; ?>" <?=$disableCategory;?>>Category Sync</a>
    </div>
    <div class="col-sm-12 m-b-20">
        <?php if(in_array('menu', $content)){
            $url="javascript:void(0);";
            $disableMenu = 'style="cursor:not-allowed" title="already synced"';
        }else{
            $url = $this->createUrl('/multistudio/MenusSync');
            $disableMenu = '';
        }?>
        <a href="<?php echo $url; ?>" <?=$disableMenu;?>>Menus Sync</a>
    </div>
    <div class="col-sm-12 m-b-20">
        <?php if(in_array('content', $content)){
            $url="javascript:void(0);";
            $disableContent = 'style="cursor:not-allowed" title="already synced"';
        }else{
            $url = $this->createUrl('/multistudio/ContentSync');
            $disableContent = '';
        }?>
        <a href="<?php echo $url; ?>" <?=$disableContent;?>>Content Sync</a>
    </div>    
    <div class="col-sm-12 m-b-20">
        <?php if(in_array('feature', $content)){
            $url="javascript:void(0);";
            $disableFeatured = 'style="cursor:not-allowed" title="already synced"';
        }else{
            $url = $this->createUrl('/multistudio/FeaturedContentSync');
            $disableFeatured = '';
        }?>
        <a href="<?php echo $url; ?>" <?=$disableFeatured;?>>Featured Content Sync</a>
    </div>
    <div class="col-sm-12 m-b-20">
        <?php 
        $url="javascript:void(0);";
        $disableFeatured = 'style="cursor:not-allowed" title="already synced"';
        ?>
        <a href="<?php echo $url; ?>" <?=$disableFeatured;?>>Cast & crew Sync</a>
    </div>
</div>

