if ($('.auto-size')[0]) {
    autosize($('.auto-size'));
}

function auto_grow(element) {
    element.style.height = "65px";
    element.style.height = (element.scrollHeight) + "px";
}

/*
 * IE 9 Placeholder
 */
//$('input, textarea').placeholder();

/*Toggle*/
function menuToggle(){
    $('body').on('click', '.menu-wrap li>a', function (e) {
        if(parseInt($(this).attr('data-id'))){
            var click_count = parseInt($('#monetization-menu-count').val());
            click_count += 1;
            $('#monetization-menu-count').val(click_count);
            var that = $(this);
            $(that).next().slideToggle(200);
            $(that).find('.icon-arrow-right').toggleClass('rotate270');
            $(that).parent().toggleClass('toggled');
            $.post('/monetization/checkSettings',function(res){
                //var data = $.parseJSON(res);
                try {
                    $.parseJSON(res);
                    //e.preventDefault();
                } catch(e) {
                    if((click_count%2 != 0) && ($(that).find('.icon-arrow-right').hasClass("rotate270"))){
                        $('#monetization-menu-settings-body').html(res);
                        $('#monetization-menu-settings').modal('show');
                    }
                    return false;
                }
            });
        }else{
            //e.preventDefault();
            $(this).next().slideToggle(200);
            $(this).find('.icon-arrow-right').toggleClass('rotate270');
            $(this).parent().toggleClass('toggled');
        }
    });
}

if (($(window).width() <= 767)) {
        menuToggle();
}
(function () {
    $('body').on('click', '.action', function (e) {
        e.preventDefault();
        $('.left-pos-and-initial-Show').toggleClass('sidebar-toggled');
    })
})();
(function () {
    $('body').on('click', '.action', function (e) {
        e.preventDefault();
        $('.sidebar').toggleClass('sidebar-toggled');
    })
})();
/*
$(function(){
    if($('.left-pos-and-initial-Show').hasScrollBar()){
       $('.left-pos-and-initial-Show').addClass('control-SubMenus-ifScrolls');
        menuToggle();
    }
    else{
        $('.left-pos-and-initial-Show').addClass('control-SubMenus');
    }
});
*/
$(function(){
    $('.left-pos-and-initial-Show').addClass('control-SubMenus');
    //menuToggle();
});
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);
/*Owl Carousel*/
$(document).ready(function() {
    $("#owl-Walkover").owlCarousel({
        items : 3,
        itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
        itemsTablet: [767,2],
        itemsMobile:	[560,1],
        autoPlay : true,
        stopOnHover : true,
        //Pagination
        pagination : false,
    });
    $('.modal').on('show.bs.modal', function () {
        if($(this).hasClass('is-Large-Modal')){
            var param = "large";
        }else{
            var param = "small";
        }
       $(this).show();
       setModalMaxHeight(this,param);
   }); 
   $("#MediaModal").on("click",".Preview-Block",function(){
        var postimageid = $(this).children().find('.img').attr('data-src');
        $('#InsertPhoto').attr('data-id', postimageid);
    });
    $('#InsertPhoto').click(function () {
        var dataImage = $("#InsertPhoto").attr('data-image');
        if(dataImage == 0 || dataImage == ""){
            var select_image = $('#choose_img_name').val();
            if(select_image !=""){
                var insmedia = $("#InsertPhoto").attr('data-id');
                if (insmedia != "") {
                tinymce.activeEditor.execCommand('mceInsertContent', false, '<img src="' + insmedia + '">'); }
                $('#MediaModal').modal("hide");
            }else{
                swal("Please select an image");
            }
        }else{
            $("#InsertPhoto").text("Waiting...");
            $("#InsertPhoto").attr("disabled","disabled");
            $("#cancelPhoto").attr("disabled","disabled");
            var fd = new FormData(document.getElementById("image_upload_to_lib"));
            $.ajax({
              url: HTTP_ROOT+"/management/UploadImageToLibrary",
              type: "POST",
              data: fd,
              dataType: 'json',
              processData: false,  // tell jQuery not to process the data
              contentType: false   // tell jQuery not to set contentType
            }).done(function( data ) {
                if(data.success){
                    $("#InsertPhoto").attr('data-image',0);
                    console.log(data.msg);
                    var insmedia = data.img_src;
                    if (insmedia != "") {
                    tinymce.activeEditor.execCommand('mceInsertContent', false, '<img src="' + insmedia + '">'); }
                    $('#MediaModal').modal("hide");
                }else{
                    swal(data.msg);
                }
            });
        }
    });
    $('#MediaModal').on('show.bs.modal', function (event) {
        $("#MediaModal img").removeClass("selected");
        $("#InsertPhoto").attr('data-id','');
    });
});
function setModalMaxHeight(element,param) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var navHeight = this.$element.find('.nav-tabs').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);
    var maxHeightafterNav = contentHeight - (headerHeight + footerHeight + navHeight);
    var seachBoxHeight = 85;
    var maxHeightafetrSearch = contentHeight - (headerHeight + footerHeight + seachBoxHeight + navHeight);
    this.$content.css({
        'overflow': 'hidden'
    });
    if(param == "large"){
    this.$element
        .find('.modal-body').css({
            'height': maxHeight,
            'overflow-y': 'hidden'
        });
    this.$element
        .find('.is-Scrollable').css({
            'height': maxHeightafterNav,
            'overflow-y': 'auto'
        });
    this.$element
        .find('.is-Scrollable-has-search').css({
            'height': maxHeightafetrSearch,
            'overflow-y': 'auto'
        });
    }else{
        this.$element
        .find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto',
            'overflow-x':'hidden'
        });
    }
}

/*Responsive Table*/
$(function () {
    $('table').footable({
        calculateWidthOverride: function () {
            return {
                width: $(window).width()
            };
        }
    });
    
});
function show_img_preview(id, img_src, name_of_image)
{   
    $('#glry_preview').removeAttr('src');
    $('#choose_img_name').val("");
    $("#InsertPhoto").attr('data-image',0);
    $(".loaderDiv").show();
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        var maxWidth = 500; // Max width for the image
        var maxHeight = 300;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        // Check if the current width is larger than the max
        if(width > maxWidth){
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
            height = height * ratio;    // Reset height to match scaled image
            width = width * ratio;    // Reset width to match scaled image
        }

        // Check if current height is larger than max
        if(height > maxHeight){
            ratio = maxHeight / height; // get ratio for scaling image
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);    // Scale width based on ratio
            width = width * ratio;    // Reset width to match scaled image
            height = height * ratio;    // Reset height to match scaled image
        }
        if(width == maxWidth && height == maxHeight){
            width = maxWidth;
            height = maxHeight;
        }
        $('#glry_preview').attr('src',img_src);
        $('#choose_img_name').val(img_src);
        $('#glry_preview').attr({
            width:width,
            height:height
        });
        $(".loaderDiv").hide();
    };
}
function browse_from_pc(){
    $('#image_to_lib').click();
}
function UploadToLibrary(){
    $('#choose_img_name').val("");
    $("#InsertPhoto").attr('data-image',0);
    var oFile = $('#image_to_lib')[0].files[0];
    var ext = oFile.name.split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        document.getElementById("imgtolib").value = "";
        swal('Please select a valid image file (jpg, png and gif are allowed)');
        return;
    }
    if (oFile.name.match(/['|"|-|,]/)) {
        document.getElementById("imgtolib").value = "";
        swal('File names with symbols such as \' , - are not supported');
        return;
    }
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var img_src = oFile.name;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        var oReader = new FileReader();
        oReader.onload = function (e) {
            var img_src = e.target.result;
            var maxWidth = 500; // Max width for the image
            var maxHeight = 300;    // Max height for the image
            var ratio = 0;  // Used for aspect ratio
            // Check if the current width is larger than the max
            if(width > maxWidth){
                ratio = maxWidth / width;   // get ratio for scaling image
                $(this).css("width", maxWidth); // Set new width
                $(this).css("height", height * ratio);  // Scale height based on ratio
                height = height * ratio;    // Reset height to match scaled image
                width = width * ratio;    // Reset width to match scaled image
            }

            // Check if current height is larger than max
            if(height > maxHeight){
                ratio = maxHeight / height; // get ratio for scaling image
                $(this).css("height", maxHeight);   // Set new height
                $(this).css("width", width * ratio);    // Scale width based on ratio
                width = width * ratio;    // Reset width to match scaled image
                height = height * ratio;    // Reset height to match scaled image
            }
            if(width == maxWidth && height == maxHeight){
                width = maxWidth;
                height = maxHeight;
            }
            $("#browsefiledetails").text(oFile.name);
            $('#imgtolib').attr('src',img_src);
            $('#imgtolib').attr({
                width:width,
                height:height
            });
            $("#InsertPhoto").attr('data-image',1);
        };
        oReader.readAsDataURL(oFile);
    };
}
function removeGallerydetails(){
    $('#glry_preview').removeAttr('src width height');
    $("#InsertPhoto").attr('data-id','');
    $(".overlay").removeAttr("style");
    $("#choose_img_name").val("");
}
function removeimagedetails(){
    $("#imgtolib").removeAttr('src width height');
    $("#browsefiledetails").text("No file selected");
    $("#InsertPhoto").attr('data-image','');
}
/*Menu Functionality */
$(document).ready(function(){
    var menu_Height = $('.control-SubMenus').outerHeight();
    var li_Item_Height = $('.menu-wrap > li').outerHeight();
    
    var no_of_lis = $('.menu-wrap > li').length;

    $( ".menu-wrap > li" )
    .mouseenter(function() {
        var li_index =  $(this).index();
        //console.log("Current li"+ li_index);
        var li_Heights_upto_this = li_index*li_Item_Height;
       // console.log("Total li height" + li_Heights_upto_this);
        var total_Height_Remains = menu_Height - li_Heights_upto_this;
        //console.log("Height remains"+total_Height_Remains);

        if($(this).children('.Sub-Menu-Level--1').length > 0)
        {
           var li_subMenu_Height = $(this).children('.Sub-Menu-Level--1').outerHeight();
           //console.log("Sub menu height"+li_subMenu_Height); 
           if(total_Height_Remains < li_subMenu_Height){
              $(this).find(".Sub-Menu-Level--1").addClass("posBottom");
           }
        } else
        {
           //console.log('no');
        }
        
    })
    .mouseleave(function() {
        
    });
   // console.log(menu_Height);
    //console.log(li_Item_Height);
    //console.log(no_of_lis);
});

function storeGlobalConstants(authToken){
    localStorage.setItem("AUTH_TOKEN", authToken);
    localStorage.setItem("HTTP_ROOT", HTTP_ROOT);
}