function fileSelectHandler() {
    document.getElementById("g_original_image").value = "";
    document.getElementById("g_image_file_name").value = "";
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#celeb_preview").removeClass("hide");
    $('#uplad_buton').removeAttr('disabled');
    var oFile = $('#celeb_pic')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg and png are allowed)');
        $("#celeb_preview").addClass("hide");
        document.getElementById("celeb_pic").value = "";
        $('#uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#celeb_preview").addClass("hide");
            document.getElementById("celeb_pic").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').css("display", "block");
                $('#celeb_preview').css("display", "block");
                $('#preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    };
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image)
{
    $('#glry_preview').css("display", "block");
    document.getElementById("celeb_pic").value = "";
    showLoader();
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    var aspectratio = img_width / img_height;
    var image_file_name = name_of_image;
    var image_src = img_src;
    clearInfo();
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            showLoader(1);
            swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
            $("#celeb_preview").addClass("hide");
            $("glry_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("450");
                $('#preview').height("250");
            }
            $("#glry_preview").css("display", "block");
            $('#gallery_preview').css("display", "block");
            $('#glry_preview').Jcrop({
                minSize: [img_width, img_height], // min crop size
                aspectRatio: aspectratio, // keep aspect ratio 1:1
                boxWidth: 450,
                boxHeight: 250,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, img_width, img_height]);
            });
        };
    };

}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#celeb_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('celeb_pic').value = null;
}
function hide_gallery()
{
    $('#preview').css("display", "none");
    $("#glry_preview").css("display", "block");
    $('#gallery_preview').css("display", "none");
    $("#g_image_file_name").val("");
    $("#g_original_image").val("");
}
function openImageModal(obj) {
    var name = $(obj).attr('data-name');
    if (name == "Banner") {
        var action = HTTP_ROOT + "/drafttemplate/AddBannerImage";
        var section = $(obj).attr('id');
        $('#section_id').val(section);
        $('#section_id1').val(section);
        $.ajax({
            url: HTTP_ROOT + "/drafttemplate/sectiondimention",
            data: {'section_id': section},
            dataType: 'json',
            success: function (result) {
                $(".upload_detail").html(name);
                $('#img_width').val(result.width);
                $('#img_height').val(result.height);
                $('.help-block').html('Upload a transparent image of size ' + result.width + 'x' + result.height);
            }
        });
    } else {
        var width = $(obj).attr('data-width');
        var height = $(obj).attr('data-height');
        $(".upload_detail").html(name);
        $(".help-block").html("Upload a transparent image of size " + width + " x " + height);
        $("#img_width").val(width);
        $("#img_height").val(height);
        var action = HTTP_ROOT + "/drafttemplate/Save" + name;
    }
    $("#upload_image_form").attr('action', action);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#homePageModal").modal('show');
}
function click_browse(modal_file) {
    $("#" + modal_file).click();
}
function removeDisable() {
    $('.rmvdisable').removeAttr('disabled');
    return true;
}

function editBnr(bannerID)
{
    $('#bnr_frm_' + bannerID).show();
    $('#bnr_edit_' + bannerID).hide();
}
function saveBanner(bannerID)
{
    $('#banner_id').val(bannerID);
    $('#banner_text').val($('#bannertext_' + bannerID).val());
    $('#bannerText').submit();
    return true;
}
function cancelBanner(bannerID)
{
    $('#bnr_frm_' + bannerID).hide();
    $('#bnr_edit_' + bannerID).show();
}
$(document).ready(function () {
    $('#savetxt').click(function () {
        if ($('#show_join_btn:checked').length > 0 && $('#join_btn_txt').val() == '')
        {
            $('#bnr_txt_error').html("Please enter the sign up button text.");
            return false;
        }
        else
        {
            $('#update_text').val(1);
            $('#bannerText').submit();
            return true;
        }
    });
});
function SortBanner(bannerid, sectionid, direction) {
    $.ajax({
        url: HTTP_ROOT+"/drafttemplate/sortbanner",
        data: {'section_id': sectionid, 'banner_id': bannerid, 'direction': direction},
        dataType: 'json',
        success: function(result) {
            window.location.href = HTTP_ROOT+"/drafttemplate/homepage";
        }
    });
}   
function deleteBanner(banner_id) {
    swal({
        title: "Remove Banner?",
        text: "Are you sure you want to remove the banner?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        window.location.href = HTTP_ROOT+"/drafttemplate/removeBanner?banner_id=" + banner_id;
    });
}
function deleteSection(section) {
    swal({
        title: "Delete Featured Section?",
        text: "Are you sure you want to <b>remove the Featured section</b> on Homepage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        removeSection(section);
    });
}
function deleteFeatured(section, featured_id) {
    swal({
        title: "Delete Featured Content?",
        text: "Are you sure you want to <b>remove the Featured Content</b> on Homepage?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        removeFeatured(section, featured_id)
    });
}
function removeSection(section) {

    $.post(HTTP_ROOT + "/drafttemplate/Removefeaturedsection", {'section': section}, function (data) {
        if (data.err == 1)
        {
            alert("Error in deleting featured section.");
            return false;
        }
        else
        {
            window.location.href = HTTP_ROOT+"/drafttemplate/homepage";
        }
    });

}
function removeFeatured(section, featured_id)
{
    $.post(HTTP_ROOT + "/drafttemplate/Removefeatured", {'section': section, 'featured_id': featured_id}, function (data) {
        if (data.err == 1)
        {
            swal("Error in deleting featured content.");
            return false;
        }
        else
        {
            window.location.href = HTTP_ROOT+"/drafttemplate/homepage";
        }
    });
}
function updateSection(section) {
    var title = $.trim($('#sectionname_' + section).val());
    if (title) {
        $.post(HTTP_ROOT + "/drafttemplate/Updatesection", {'section': section, 'sectionname': title}, function (data) {
            if (data.err === 1) {
                alert("Error in updating featured section.");
                return false;
            } else {
                window.location.href = HTTP_ROOT+"/drafttemplate/homepage";
            }
        });
    } else {
        alert("Section Name can't be blank.");
        return false;
    }
}
function addHomepageSection()
{
    $('#addHomepageSection').modal('show');
}
function openEditSectionpopup(section) {
    $('#addHomepageSection').modal('show');
    $('#frm_loader').show();
    $.ajax({
        type: "POST",
        url: HTTP_ROOT + 'drafttemplate/getfeaturedsection',
        data: {'section': section},
        dataType: 'json'
    }).done(function (data) {
        $('#section_id').val(section);
        $('#section_name').val(data.title);
        $('#frm_loader').hide();
    });

    $("#PagePopup").modal('show');
    return false;
}
function addPopularMovie(section)
{
    $('#addPopularcontnet').modal('show');
    $('#featured_section').val(section);
    $('#title').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: HTTP_ROOT + '/drafttemplate/autocomplete/movie_name?term=' + $('#title').val() + '&section=' + section,
                dataType: "json",
                data: {"featured_section": section},
                init: function (data) {
                },
                success: function (data) {
                    if (data.length == 0) {
                        $('#movie_id').val('');
                        $('#preview_poster').html('');
                    } else {
                        response($.map(data.movies, function (item) {
                            return {
                                label: item.movie_name,
                                value: item.movie_id,
                            }
                        }));
                    }
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $("#title").val(ui.item.label);
            $.post(HTTP_ROOT + "/admin/getPoster", {'movie_id': ui.item.value, 'title': ui.item.label}, function (res) {
                if (res.toLowerCase().indexOf("no-image.png") >= 0) {
                    $('#preview_poster').html("<img src='" + res + "' />")
                } else {
                    $('#preview_poster').html("<img src='" + res + "' /><br/><h6>If the above poster is correct, please click 'Save' to add this content to your popular Movie</h6>")
                }
            });
        },
        focus: function (event, ui) {
            var lb = ui.item.label;
            $("#title").val(ui.item.label);
            $("#movie_id").val(ui.item.value);
            if (lb.indexOf("Episode") > -1) {
                $("#is_episode").val(1);
            } else {
                $("#is_episode").val(0);
            }
            event.preventDefault(); // Prevent the default focus behavior.
        }
    });
}
$(document).ready(function () {
    $('.hidesection').click(function () {
        $(this).addClass('showsection');
        $(this).removeClass('hidesection');
    });
    $('.icon.left-icon').click(function () {
        if ($(this).hasClass('icon-arrow-up')) {
            $(this).removeClass('icon-arrow-up');
            $(this).addClass('icon-arrow-down');
        } else {
            $(this).removeClass('icon-arrow-down');
            $(this).addClass('icon-arrow-up');
        }
    });
    $("#uplad_buton").click(function(){
       $(this).attr('disabled','disabled');
       $('#cancl_upload').attr('disabled','disabled');
       $("#upload_image_form").submit();
    });
});