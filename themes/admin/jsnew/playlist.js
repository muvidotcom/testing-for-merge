$('input.ind_image').on('change', function () {
    $('input.ind_image').not(this).prop('checked', false);
});
$(document).ready(function () {
    $('body').on('click', '.plus_icon', function (e) {
        e.preventDefault();
        $(this).toggleClass('fa-minus');
        $('.fa-plus').removeAttr('title');
        if (!$(this).hasClass("fa-minus")) {
            $(this).attr('title', 'Open');
        }
    $('.fa-minus').attr('title', 'Close');
        $(this).parent().parent().parent().next().slideToggle(200);
    });
    $('body').on('click', '#delete_playlist_btn', function () {
        var playlist_name = $(this).attr('data-name');
        var playlist_id = $(this).attr('data-id');
        var user_id = 0;
        swal({
            title: "Remove Playlist?",
            text: "Are you sure you want to remove the playlist?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        },function (isConfirm) {
            if (isConfirm){
                deletePlaylist(user_id, playlist_name, playlist_id);
            }
        });
    });
    $('body').on('click', '#delete_content_btn', function () {
        var content_id = $(this).attr('data-stream_id');
        var playlist_id = $(this).attr('data-list_id');
        var user_id = 0;
        swal({
        title: "Remove Content?",
            text: "Are you sure you want to remove the content?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#E6E6E6", customClass: "confirmButtonTextColor",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            html: true
        },function (isConfirm) {
            if (isConfirm){
                deleteContent(user_id, playlist_id, content_id);
            }
        });
    });
    $("#new_playlist").keyup(function() {
        $('.new_playlist_error').html('');
    });
    $("#content_category_value").change(function(){
        $('.playlist_category_error').html('');
    });
});
function deletePlaylist(user_id, playlist_name, playlist_id){
    var url =  HTTP_ROOT + '/management/deletePlaylist';
    $.post(url, {'user_id':user_id, 'playlist_id': playlist_id, 'playlist_name': playlist_name}, function (data) {
        var res = JSON.parse(data);
        if (res.status == "Success") {
            swal(res.msg, '', "success");
            location.reload();
        } else {
            swal(res.msg, '', "error");
             location.reload();
        }
    });
}
function deleteContent(user_id, playlist_id, content_id){
    var url =  HTTP_ROOT + '/management/deleteContent';
    $.post(url, {'user_id': user_id, 'playlist_id': playlist_id, 'content_id': content_id}, function (data) {
        var res = JSON.parse(data);
        if (res.status == "Success") {
            swal(res.msg, '', "success");
            location.reload();
        } else {
            swal(res.msg, '', "error");
            location.reload();
        }
    });
}
function validate_form() {
    var playlist_name = $("#new_playlist").val();
    var playlist_category = $('#content_category_value').val();
    var regex = /^[a-zA-Z ]*$/;
    if (playlist_name == "") {
        $(".new_playlist_error").html("Playlist Name Cannot be blank");
        return false;
    }
    if (playlist_name.trim() === ""){
        $(".new_playlist_error").html("Playlist Name Cannot be blank Space!");
        return false;
    }else if (playlist_category == null) {
        $(".playlist_category_error").html("Playlist category cannot be blank");
        return false;
    }else if (playlist_name == "" && playlist_category == '') {
        $(".new_playlist_error").html("Name Can not be blank!");
        $(".playlist_category_error").html("Please fill the details");
        return false;
    } else if (playlist_name != ""){
        $(".new_playlist_error").html("");
    } else if (playlist_category){
        $(".playlist_category_error").html("");
    }
}
function AddNewPlaylist() {
    $('#new_playlist').val('');
    $('#content_category_value option').removeAttr('selected');
    $('.img-playlist-edit').attr('src', '');
    $('.canvas-image').addClass('hide');
    $('#addPlaylist').modal('show');
}
function editPlaylist(list_name, list_id, category_value, img_playlist) {
    $('#text_playlist').html('Edit Playlist');
    if (img_playlist != ''){
        $('.img-playlist-edit').attr('src', '' + img_playlist);
    }
    var category_ids = category_value.split(",");
    $('#content_category_value option').each(function () {
        var chkVal = category_ids.indexOf($(this).attr('value'));
        if (parseInt(chkVal) != - 1) {
            $(this).attr('selected', true);
        } else {
            $(this).removeAttr('selected');
        }
    });
    $('#playlist_form').attr('action', HTTP_ROOT + '/management/editPlaylist');
    $('#new_playlist').val(list_name);
    $('#list_id').val(list_id);
    $('#addPlaylist').modal('show');
}
function openImageModal(obj) {
    var width = $(obj).attr('data-width');
    var height = $(obj).attr('data-height');
    $(".help-block").html("Upload a transparent image of size " + width + " x " + height + 'px');
    $("#img_width").val(width);
    $("#img_height").val(height);
    $("#all_img_glry").load(HTTP_ROOT + "/template/imageGallery");
    $("#category_img").modal('show');
}
function click_browse(modal_file) {
    $("#" + modal_file).click();
}
function fileSelectHandler() {
    document.getElementById("g_original_image").value = "";
    document.getElementById("g_image_file_name").value = "";
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    $(".jcrop-keymgr").css("display", "none");
    $("#celeb_preview").removeClass("hide");
    $('#uplad_buton').removeAttr('disabled');
    var oFile = $('#browse_cat_img')[0].files[0];
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
    if (!rFilter.test(oFile.type)) {
        swal('Please select a valid image file (jpg and png are allowed)');
        $("#celeb_preview").addClass("hide");
        document.getElementById("browse_cat_img").value = "";
        $('#uplad_buton').attr('disabled', 'disabled');
        return;
    }
    var aspectratio = img_width / img_height;
    var img = new Image();
    img.src = window.URL.createObjectURL(oFile);
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            swal('You have selected small file, please select one bigger image file');
            $("#celeb_preview").addClass("hide");
            document.getElementById("browse_cat_img").value = "";
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('preview');
        var oReader = new FileReader();
        oReader.onload = function (e) {
            $('.error').hide();
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                    $('#glry_preview').width("450");
                    $('#glry_preview').height("250");
                }
                $('#preview').css("display", "block");
                $('#celeb_preview').css("display", "block");
                $('#preview').Jcrop({
                    minSize: [img_width, img_height], // min crop size
                    aspectRatio: aspectratio, // keep aspect ratio 1:1
                    boxWidth: 450,
                    boxHeight: 250,
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    onChange: updateInfo,
                    onSelect: updateInfo,
                    onRelease: clearInfo
                }, function () {
                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];
                    jcrop_api = this;
                    jcrop_api.setSelect([10, 10, img_width, img_height]);
                });
            };
        };
        oReader.readAsDataURL(oFile);
    };
}
function showLoader(isShow) {
    if (typeof isShow == 'undefined') {
        $('.loaderDiv').show();
        $('button').attr('disabled', 'disabled');
    } else {
        $('.loaderDiv').hide();
        $('button').removeAttr('disabled');
    }
}
function toggle_preview(id, img_src, name_of_image){
    $('#glry_preview').css("display", "block");
    document.getElementById("browse_cat_img").value = "";
    showLoader();
    var img_width = $("#img_width").val();
    var img_height = $("#img_height").val();
    var aspectratio = img_width / img_height;
    var image_file_name = name_of_image;
    var image_src = img_src;
    clearInfo();
    $("#g_image_file_name").val(image_file_name);
    $("#g_original_image").val(image_src);
    var res = image_file_name.split(".");
    var image_type = res[1];
    var img = new Image();
    img.src = img_src;
    var width = 0;
    var height = 0;
    img.onload = function () {
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        window.URL.revokeObjectURL(img.src);
        if (width < img_width || height < img_height) {
            showLoader(1);
            swal('You have selected small file, please select one bigger image file more than ' + img_width + ' X ' + img_height);
            $("#celeb_preview").addClass("hide");
            $("glry_preview").addClass("hide");
            $("#g_image_file_name").val("");
            $("#g_original_image").val("");
            $('#uplad_buton').attr('disabled', 'disabled');
            return;
        }
        var oImage = document.getElementById('glry_preview');
        showLoader(1)
        oImage.src = img_src;
        oImage.onload = function () {
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
                $('#glry_preview').width(oImage.naturalWidth);
                $('#glry_preview').height(oImage.naturalHeight);
                $('#preview').width("450");
                $('#preview').height("250");
            }
            $("#glry_preview").css("display", "block");
            $('#gallery_preview').css("display", "block");
            $('#glry_preview').Jcrop({
                minSize: [img_width, img_height], // min crop size
                aspectRatio: aspectratio, // keep aspect ratio 1:1
                boxWidth: 450,
                boxHeight: 250,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfoallImage,
                onSelect: updateInfoallImage,
                onRelease: clearInfoallImage
            }, function () {
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                jcrop_api = this;
                jcrop_api.setSelect([10, 10, img_width, img_height]);
            });
        };
    };
}
function hide_file()
{
    $('#glry_preview').css("display", "none");
    $('#celeb_preview').css("display", "none");
    $('#preview').css("display", "none");
    document.getElementById('browse_cat_img').value = null;
}
function hide_gallery()
{
$('#preview').css("display", "none");
        $("#glry_preview").css("display", "block");
        $('#gallery_preview').css("display", "none");
        $("#g_image_file_name").val("");
        $("#g_original_image").val("");
}
function seepreview(obj) {
    if ($('#g_image_file_name').val() == '')
        curSel = 'upload_by_browse';
    else
        curSel = 'upload_from_gallery';
    if ($('#' + curSel).find(".x13").val() != "") {
        $(obj).html("Please Wait");
        $('#category_img').modal({backdrop: 'static', keyboard: false});
        posterpreview(obj, curSel);
        $('#category_img').modal('hide');
    } else {
        if ($("#celeb_preview").hasClass("hide")) {
            $('#category_img').modal('hide');
            $(obj).html("Next");
        } else {
            $(obj).html("Please Wait");
            $('#category_img').modal({backdrop: 'static', keyboard: false});
            if ($('#' + curSel).find(".x13").val() != "") {
                posterpreview(obj, curSel);
            } else if ($('#' + curSel).find('.x1').val() != "") {
                posterpreview(obj, curSel);
            } else {
                $('#category_img').modal('hide');
                $(obj).html("Next");
            }
        }
    }
}
function posterpreview(obj, curSel) {
    $('.canvas-image').show();
    $("#previewcanvas").show();
    var canvaswidth = $("#img_width").val();
    var canvasheight = $("#img_height").val();
    var x1 = $('#' + curSel).find('.x1').eq(0).val();
    var y1 = $('#' + curSel).find('.y1').eq(0).val();
    var width = $('#' + curSel).find('.w').val();
    var height = $('#' + curSel).find('.h').val();
    var canvas = $("#previewcanvas")[0];
    var context = canvas.getContext('2d');
    var img = new Image();
    img.onload = function () {
    canvas.height = canvasheight;
        canvas.width = canvaswidth;
        context.drawImage(img, x1, y1, width, height, 0, 0, canvaswidth, canvasheight);
    };
    $("#avatar_preview_div").hide();
    $('.canvas-image').removeClass('hide');
    if ($('#g_image_file_name').val() == '') {
        img.src = $('#preview').attr("src");
    } else {
        img.src = $('#glry_preview').attr("src");
    }
}
function removePlaylistPoster(playlist_id){
    swal({
        title: "Remove Poster?",
        text: "Are you sure to remove this poster?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E6E6E6",customClass: "confirmButtonTextColor",
        confirmButtonText: "Yes",
        closeOnConfirm: true,
        html: true
    }, function () {
        if (playlist_id) {
            var url = HTTP_ROOT + "/management/removeplaylistposter";
            $('#remove-poster-text').text('Removing...');
            $('#remove-poster-text').attr('disabled', 'disabled');
            $.post(url, {'playlist_id': playlist_id, 'is_ajax': 1}, function (res) {
                if (res.err) {
                    $('#remove-poster-text').removeAttr('disabled');
                    var sucmsg = '<div class="alert alert-danger alert-dismissable flash-msg m-t-20"><i class="icon-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Error in deleting poster.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('Remove');
                } else {
                    var sucmsg = '<div class="alert alert-success alert-dismissable flash-msg m-t-20"><i class="icon-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&nbsp;Poster removed successfully.</div>'
                    $('.pace').prepend(sucmsg);
                    $('#remove-poster-text').text('');
                    $('#avatar_preview_div').children('img').remove();
                    window.location.reload(1);
                }
            }, 'json');
        } else {
            return false;
        }
    });
}
function ajaxshowType(){
    var url = HTTP_ROOT + "/Adminceleb/ajaxshowtype";
    $.post(url,function(postresult){    
    $("#ajaxshowtype").html(postresult);    
    });
}