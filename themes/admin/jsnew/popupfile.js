var filexhr = '';
function addFileFromFileGallery(videoUrl, galleryId) {
	showLoaderfile();
	var action_url = HTTP_ROOT + "/admin/addFileFromFileGallery";
			$('#server_url_error').html('');
	var movie_id = $('#movie_id').val();
	var movie_stream_id = $('#movie_stream_id').val();
	$.post(action_url, {'url': videoUrl, 'galleryId': galleryId, 'movie_id': movie_id, 'movie_stream_id': movie_stream_id}, function (res) {
		showLoaderfile(1);
		if (res.error) {
			$('#server_url_error').html('There seems to be something wrong with the file. Please contact your Muvi representative. ');
		} else {
			if (res.is_video === 'uploaded') {
				window.location.href = window.location.href;
			} else if (res.is_video) {
				$("#addvideo_popup").modal('hide');
				$('#server_url').val('');
				$('#server_url_error').val('');
				var text = '<h5>Encoding : Queued &nbsp;&nbsp;<em class="fa fa-refresh " style="color:#EF6C00;"></em></br><span class="encodingTimeRemaning">Time remaining: NA</span></h5>';
				$('#' + movie_stream_id).html(text);
				$('#video_upload_' + movie_stream_id).empty();
				if ($('#encodingStreamId').is(":empty")) {
					$('#encodingStreamId').html(movie_stream_id);
				} else {
					$('#encodingStreamId').html($('#encodingStreamId').html() + "," + movie_stream_id);
				}
			} else {
				$('#server_url_error').html('There seems to be something wrong with the file');
			}
		}
	}, 'json');
	console.log('valid url');
}
function searchvideo_file() {
	var key_word = $("#search_file").val();
	var key_word = key_word.trim();
	if (key_word.length >= 3 || key_word.length == 0) {
		$('.loaderDiv').show();
		$('#profile button').attr('disabled', 'disabled');
		if(filexhr && filexhr.readyState != 4){
			filexhr.abort();
		}
		filexhr = $.ajax({
			url: HTTP_ROOT+"/admin/ajaxSearchVideo",
			data: {'search_word': key_word, 'type': 'file'},
			contentType: false,
			cache: false,
			success: function (result)
			{
				$('.loaderDiv').hide();
				$('#profile button').removeAttr('disabled');
				$("#video_content_div_file").html(result);
			}
		});
	}
	else
	{
		return;

	}
}
function reset_filter_file() {
	$("#is_encoded_file").val(0);
	$("#uploaded_in_file").val(0);
	view_filtered_list_file();
}

function view_filtered_list_file() {
	processing = false;
	count = 1;
	maxscrollcount = 16;
	tempScrollTop = 0;
	currentScrollTop = 0;
	showLoaderfile();
	var is_encoded = $("#is_encoded_file").val();
	var uploaded_in = $("#uploaded_in_file").val();

	var url = HTTP_ROOT+"/admin/ajaxFilterFile/is_encoded/" + is_encoded + "/uploaded_in/" + uploaded_in;
	//window.location.href = url;
	$.post(url, {'is_encoded': is_encoded, 'uploaded_in': uploaded_in}, function (res) {
		if (res) {
			$('#video_content_div_file').html();
			showLoaderfile(1);

			$('#video_content_div_file').html(res);
		}
	});
}

function showLoaderfile(isShow) {
	if (typeof isShow == 'undefined') {
		$('.loaderDiv').show();
		$('#addvideo_popup button,input[type="text"]').attr('disabled', 'disabled');
		$('#profile button').attr('disabled', 'disabled');

	} else {
		$('.loaderDiv').hide();
		$('#addvideo_popup button,input[type="text"]').removeAttr('disabled');
		$('#profile button').removeAttr('disabled');
	}
}