   var pagelength = 5;
$(function() {
    
  
 $('#myTable').dataTable({
       // "bPaginate":false,
      // "aaSorting": [],
        "rowReorder": true,
        "bInfo": false,
         "pageLength": pagelength
    });
   
    $('#postnew').click(function(){
        
        var title   = $("#post_title").val();
        var content = $("#post_content").val();
       if(title == "" && content == "" ){
            $('#post_title').next('.help-block').html('Please input your post name');
            $('#post_content').next('.help-block').html('Please input your post name');
            return false;
        }
        else if(title == ""){
            $('#post_title').next('.help-block').html('Please input your post name');
            return false;
        }
        else if(content == ""){
            $('#post_content').next('.help-block').html('Please input your post name');
            return false;
        }else{
        var data = {title : title, content : content};
         $.ajax({
            url : "http://devstudio.biswajit.com/admin/addblogpost",
            type: "POST",
            data : data,
            success: function(data, textStatus, jqXHR)
            {
                $('#myModal').modal('hide');
                $('#post_title').val('');
                $('#post_content').val('');
                var total_page = parseInt(data) / pagelength;
                var page_total = Math.ceil(total_page)- 1;
                //alert(page_total);
                $('#loadtable').load("http://devstudio.biswajit.com/admin/reloadtable");
                //alert(page_total);
              //$('span a.paginate_button').eq(page_total).trigger("click");
            }
   
        });
    }
        
    });

});

function deleterow(id){
     if(confirm('Are you sure to delete this row')){
        var post = id.split("_");
        var post_id = post[1];
        var cur_page = $('a.paginate_button.current').html();
        var inputhidden = $('#cur_page').val(cur_page);
        var tot_page = $('span a.paginate_button').length;
        var inputhidden2 = $('#tot_page').val(tot_page);
        var data = {id : post_id};
         $.ajax({
            url : "http://devstudio.biswajit.com/admin/delTabrow/",
            type: "POST",
            data : data,
            success: function(data, textStatus, jqXHR)
            {
              
                 $('#loadtable').load("http://devstudio.biswajit.com/admin/reloadtableafterdelete");
                  //$('span a.paginate_button').eq(cur_page).trigger("click");
            }
   
        });
  
    }
}