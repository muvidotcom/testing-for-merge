/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
var left_side_width = 220;
$(function() {
    "use strict";
// From app.js 

    $( "#search_movie" ).catcomplete({
        delay: 0,
        //source: HTTP_ROOT+'/admin/autocomplete'
		source : function (request, response) {
            $.ajax({
                url: HTTP_ROOT+'/admin/autocomplete?term='+$('#search_movie').val(),
                dataType: "json",
                success: function( data ) {
                    response( $.map( data.movies, function( item ) {
                        return {
                            label: item.movie_name,
                            item_id: item.movie_id,
                            uniq_id: item.uniq_id
                        }
                    }));
                },
			select: function( event, ui ) {
				event.preventDefault();
				//alert(ui.item.label);
				$("#search_movie").val(ui.item.label);
			}
                
            });
        }
    });
// App.js codes are merged with this 

    "use strict";

    //Enable sidebar toggle
    $("[data-toggle='offcanvas']").click(function(e) {
        e.preventDefault();

        //If window is small enough, enable sidebar push menu
        if ($(window).width() <= 992) {
            $('.row-offcanvas').toggleClass('active');
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
            $('.row-offcanvas').toggleClass("relative");
        } else {
            //Else, enable content streching
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        }
});

    //Add hover support for touch devices
    $('.btn').bind('touchstart', function() {
        $(this).addClass('hover');
    }).bind('touchend', function() {
        $(this).removeClass('hover');
    });

    //Activate tooltips
    $("[data-toggle='tooltip']").tooltip();

    /*     
     * Add collapse and remove events to boxes
     */
    $("[data-widget='collapse']").click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!box.hasClass("collapsed-box")) {
            box.addClass("collapsed-box");
            bf.slideUp();
        } else {
            box.removeClass("collapsed-box");
            bf.slideDown();
        }
    });

    /*
     * INITIALIZE BUTTON TOGGLE
     * ------------------------
     */
    $('.btn-group[data-toggle="btn-toggle"]').each(function() {
        var group = $(this);
        $(this).find(".btn").click(function(e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });

    $("[data-widget='remove']").click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        box.slideUp();
    });

    
    /* 
     * Make sure that the sidebar is streched full height
     * ---------------------------------------------
     * We are gonna assign a min-height value every time the
     * wrapper gets resized and upon page load. We will use
     * Ben Alman's method for detecting the resize event.
     * 
     **/
    function _fix() {
        //Get window height and the wrapper height
        var height = $(window).height() - $("body > .header").height();
        $(".wrapper").css("min-height", height + "px");
        var content = $(".wrapper").height();
        //If the wrapper height is greater than the window
        if (content > height)
            //then set sidebar height to the wrapper
            $(".left-side, html, body").css("min-height", content + "px");
        else {
            //Otherwise, set the sidebar to the height of the window
            $(".left-side, html, body").css("min-height", height + "px");
        }
    }
    //Fire upon load
    _fix();
    //Fire when wrapper is resized
    $(".wrapper").resize(function() {
        _fix();
        fix_sidebar();
    });

    //Fix the fixed layout sidebar scroll bug
    fix_sidebar();

});

$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
          
        if (item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>Movie</li>" );
          currentCategory = item.category;
        }
        that._renderItemData(ul,item)
       // that._renderItem(ul,item)
      });
    }
});
_renderItemData= function (e, t) {
    return this._renderItem(e, t).data("ui-autocomplete-item", t)
}
$.ui.autocomplete.prototype._renderItem = function (ul, item) {
	var movie_id = item.item_id;
        var uniq_id = item.uniq_id;
	//this.item = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
	var moviename = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
	//item.value = item.label;
	if(is_sdk){
		return $("<li></li>")
		.data("item.autocomplete", item)
		.append("<a href='"+HTTP_ROOT+"/admin/editmovie?movie_id="+movie_id+"&uniq_id="+uniq_id+"' >" +moviename+ "</a>")
		.appendTo(ul);
	}else{
		return $("<li></li>")
		.data("item.autocomplete", item)
		.append("<a href='"+HTTP_ROOT+"/admin/editmovie?movie_id="+movie_id+"&uniq_id="+uniq_id+"' >" +moviename+ "</a>")
		.appendTo(ul);
	}
};
function addMovie(studio_id){
    $("#addmovie_popup").modal('show');
    $('#movie_name').autocomplete({
        source : function (request, response) {
            $.ajax({
                url: HTTP_ROOT+'/admin/autocomplete/movie_name?term='+$('#movie_name').val(),
                dataType: "json",
                success: function( data ) {
                    response( $.map( data.movies, function( item ) {
                        return {
                            label: item.movie_name,
                            value: item.movie_id
                        }
                    }));
                }
                
            });
        },
        select: function( event, ui ) {
            event.preventDefault();
            $("#movie_name").val(ui.item.label);
            $("#movie_id").val(ui.item.value);
        }
    });
}
 
function fix_sidebar() {
    //Make sure the body tag has the .fixed class
    if (!$("body").hasClass("fixed")) {
        return;
}

    //Add slimscroll
    $(".sidebar").slimscroll({
        height: ($(window).height() - $(".header").height()) + "px",
        color: "rgba(0,0,0,0.2)"
    });
}
