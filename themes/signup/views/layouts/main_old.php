<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />   
        <link href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png" type="image/png" rel="icon" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>" />
        <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />

        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' type='text/css' media='all' />
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true) ?><?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true) ?><?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" />    
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true) ?><?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.css" />    
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true) ?><?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css?v=<?php echo RELEASE ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/common/font-awesome/css/font-awesome.min.css" />
        <script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true); ?>/common/js/jquery.validate.min.js"></script>
        <!-- Start of Leadin Embed -->
        <script type="text/javascript" src="//js.leadin.com/js/v1/2602223.js" id="LeadinEmbed-2602223" crossorigin="use-credentials" async defer></script>
        <!-- End of Leadin Embed -->
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod("mail", function(value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
                }, "Please enter a correct email address");
                jQuery.validator.addMethod("phone", function(value, element) {
                    return this.optional(element) || /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{3,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/.test(value);
                }, "Please enter correct phone number");
            });
        </script> 
        <style>.async-hide { opacity: 0 !important} </style>
        <script>
            (function(a, s, y, n, c, h, i, d, e) {
                s.className += ' ' + y;
                h.start = 1 * new Date;
                h.end = i = function() {
                    s.className = s.className.replace(RegExp(' ?' + y), '')
                };
                (a[n] = a[n] || []).hide = h;
                setTimeout(function() {
                    i();
                    h.end = null
                }, c);
                h.timeout = c;
            })(window, document.documentElement, 'async-hide', 'dataLayer', 4000, {'GTM-T85M3G4': true});
        </script>   
    <?php if(strpos($_SERVER['HTTP_HOST'], 'muvi.in')){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script>
    <?php  } else if(strpos($_SERVER['HTTP_HOST'], 'muvi.com')){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script>  
    <?php  } else if($_SERVER['HTTP_HOST'] != 'localhost'){ ?>
        <script>
            (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                      m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                ga('create', 'UA-57762605-1', 'auto');
                                ga('require', 'GTM-NG7VCM2');
                                ga('send', 'pageview');
       </script>
    <?php } if (Yii::app()->controller->action->id == 'TypeofContent') { ?>
            <img height="1" width="1" style="display:none" src="https://www.quora.com/_/ad/e93e17883f52473fb84c931eaa8a1329/pixel" />
            <?php } ?>     
        <script type='text/javascript'>
            window.__lo_site_id = 78655;
            (function() {
                var wa = document.createElement('script');
                wa.type = 'text/javascript';
                wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wa, s);
            })();
        </script>  
        <!-- Google Tag Manager -->
        <script>(function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MG9TSS7');</script>
        <!-- End Google Tag Manager -->     
    </head>
<?php $cur_action = Yii::app()->controller->action->id; ?>    
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG9TSS7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->           
        <div id="wrap">	

            <div id="content">
                <div class="bluebg pad50">
<?php require_once 'header_old.php'; ?>
                </div>
                <div class="wrapper">                
                    <div class="container">
                        <div class="clear2"></div>
                        <div class="clear2"></div>
                        <div class="clear2"></div>
                        <div class="row-fluid">
                                <?php if ($cur_action != 'signupTerms') { ?>
                                <div class="<?php if ($cur_action == 'purchaseSubscription') { ?> span12<?php } else if ($cur_action == 'index' && Yii::app()->custom->isBlackoutDay() == 1) { ?> span8<?php } else { ?>span12<?php } ?> pull-left">
                                    <?php require_once 'mobilenav.php'; ?>
                                <?php } ?>
                                <?php echo $content; ?>
                            <?php if ($cur_action != 'signupTerms') { ?>
                                </div>
                                    <?php if ($cur_action == 'index' && Yii::app()->custom->isBlackoutDay() == 1) { ?>
                                    <div class="span4 pull-right">
                                    <?php require_once 'right_menu.php'; ?>
                                    </div>
                                <?php } ?>
<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
<?php require_once 'footer_old.php'; ?>
        </div>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3Ck44EbYwaLXpRDAitMdvTyD4LH7YeoO";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<script type="text/javascript">

    var intervalVariable;
intervalVariable = setInterval(zopim30sec, 10000);
$zopim(function () { 
    
    $zopim.livechat.setOnConnected(function () {
       
        $zopim.livechat.departments.clearVisitorDepartment();
        var zemail = $zopim.livechat.getEmail();
        var zname = $zopim.livechat.getName();
		//alert(CHECKSTUDIOLOGIN);
	<?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
		<?php } else { ?>
        
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
		<?php } ?>
        
        //zopim redirection to sales and support department as per the login details
        if (zemail != '') {
            if (getCookie("zopimemail") != zemail) {
                $.post("/contact/SendFromZopim", {"email": zemail, "name": zname}, function () {
                    document.cookie = "zopimemail=" + zemail;
                });
            }
        } else {
            //intervalVariable = setInterval(zopim30sec, 20000);
        }
    });
    
     
   });  
$zopim(function() {
$zopim.livechat.setOnChatEnd(end);
});
function end() {
$zopim.livechat.clearAll();
}
function zopim30sec() {
    
    $zopim.livechat.departments.clearVisitorDepartment();
    
    var zemail1 = $zopim.livechat.getEmail();
    var zname1 = $zopim.livechat.getName();
    //zopim redirection to sales and support department as per the login details
     //alert(CHECKSTUDIOLOGIN);
    <?php if(isset(Yii::app()->user->id) && (Yii::app()->user->id) > 0){?>
        $zopim.livechat.departments.setVisitorDepartment('Support');
        <?php } else { ?>
        $zopim.livechat.departments.setVisitorDepartment('Sales'); 
       <?php } ?>
	
        
    //zopim redirection to sales and support department as per the login details
    if (zemail1 != '') {
        if (getCookie("zopimemail") != zemail1) {
            $.post("/contact/SendFromZopim", {"email": zemail1, "name": zname1}, function () {
                document.cookie = "zopimemail=" + zemail1;
                //clearInterval(intervalVariable);
            });
        }
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
        </script>
    </body>
</html>
