<div class="span12 terms-div" >
    <p>
        By registering on Muvi (<a href="<?php echo Yii::app()->baseUrl; ?>">www.muvi.com</a>), you accept the terms of this Service Level 
        Agreement (“Agreement”). In this Agreement, Muvi LLC is termed as “Service Provider” and you are 
        termed as “Content Owner”. The Service Provider and the Content Owner are individually also referred to as a “Party” and collectively as the “Parties”.<br/>
        You may <a href="<?php echo Yii::app()->baseUrl; ?>/contact">contact us</a> if you have any questions about this agreement.
    </p>
    <p>
    <h4>WHEREAS:</h4>
    <ol style="list-style: upper-alpha;">
        <li> The Service Provider is, inter alia, engaged in the business of offering technical solutions to the 
            entertainment industry that enables the entertainment industry to connect to its audience more 
            effectively;
        </li>
        <li>The Content Owner is, inter alia, engaged in the business of production and/or distribution of 
            such video content which may include films, tele-serials, plays, songs, documentaries etc.
        </li>
        <li> The Content Owner is keen to leverage the technical expertise and resources provided in the 
            form of a software development kit licensed on a non-exclusive basis by the Service Provider. 
            The software development kit shall allow the Content Owner to create its own Streaming Platform 
            (as defined hereinafter) which shall be maintained by the Service Provider.
        </li>
        <li> The Service Provider is willing and desirous of licensing the Services (as defined hereinafter) to 
            the Content Owner for the Term (as defined hereinafter) of this Agreement as an independent 
            contractor, on the terms and conditions as contained herein.
        </li>
        <li> The Parties now enter into this Agreement to record the terms and conditions agreed to between 
            them in respect of the Services and certain rights and obligations interest the Parties.
        </li>
    </ol>	
</p><br/>
<p>
<h4>THE PARTIES HERETO AGREE AS FOLLOWS:</h4>
<ol>
    <li>
        <h5>Definitions.</h5>
        <ol style="list-style:none;">
            <li><span>1.1</span> 
                All the defined and captioned terms in this Agreement shall have the meaning as assigned to them here below:<br/>
                <p>
                    <b>“Business Day”</b> or <b>“Business Days”</b> shall mean any day other than a Sunday or a day on which 
                    scheduled commercial banks in New York, United States of America are authorized or required 
                    by law to be closed.<br/>
                </p>
                <p>
                    <b>“Content”</b>, for the purpose of this Agreement includes text, photos, sounds, music, videos, 
                    audiovisual combinations, interactive features and other materials provided to the Service <br/>
                    Provided by the Content Owner.
                </p>
                <p>
                    <b>“Government”</b> or <b>“Governmental Authority”</b>, for the purpose of this Agreement means any statutory authority, government department, agency, commission, board, tribunal, court or other entity in United States of America or abroad as applicable, authorized to make Laws.
                </p>
                <p>
                    <b>“Hybrid Payment Model”</b> means a combination of the Pay per View Payment Model and User	Subscription Payment Model.
                </p>
                <p>
                    <b>“Intellectual Property Rights”</b> or <b>“IPR”</b> includes all rights in and in relation to any patents, patent applications, know-how, information, data points, trademarks, trade mark applications, trade names, designs, copyrights, domain names or other similar intellectual, industrial or commercial rights, registrations, extensions and renewals thereof in any part of the world. 
                </p>
                <p>
                    <b>“Law”</b> or <b>“Laws”</b> includes all applicable statutes, enactments, acts of legislature or parliament, 
                    laws, ordinances, rules, by-laws, regulations, notifications, guidelines, policies, directions, 
                    directives, and orders of any Governmental Authority, tribunal, board or court, in United States of 
                    America or abroad, as applicable.
                </p>
                <p>
                    <b>“Pay-per-View Payment Model”</b> means a payment model wherein a User pays an amount per view of the Content on the Content Owner’s Streaming Platform.
                </p>	
                <p>
                    <b>“Person”</b> shall mean any natural person, limited or unlimited liability company, corporation, 
                    partnership (whether limited or unlimited), proprietorship, Hindu undivided family, trust, union, 
                    association, society, foundation, Government or any agency or political subdivision thereof or any 
                    other entity that may be treated as a person under applicable Law.
                </p>
                <p>
                    <b>“Revenue”</b> shall mean the sum total of money, exclusive of a certain transaction fees charged 
                    by the payment gateway, generated by screening of Content by Users on the Content Owner’s 
                    Streaming Platform through User Subscription Payment Model (as defined hereinafter) and/
                    or Pay-per-View Payment Model and/or Hybrid Payment Model and/or advertisements and/or 
                    through any other means mutually decided by the Parties from time to time.
                </p>
                <p>
                    <b>“Service”</b> or <b>“Services”</b> shall mean such services as specified in Clause 3 and <a href="#annexture-a"><b>Annexure A</b></a> to this Agreement.
                </p>
                <p>
                    <b>“Streaming Platform”</b>, for the purpose of this Agreement includes streaming website, mobile 
                    app, TV app or such other platform(s) offered by the Service Provider to the Content Owner from 
                    time to time.
                </p>
                <p>
                    <b>“Tax”</b> means all taxes, duties, levies, imposts and social security charges including corporate 
                    tax, income tax, wage withholding tax, value added tax, customs and excise duties, service 
                    tax, capital tax and other legal transaction taxes, stamp duty, dividend withholding tax, real estate 
                    taxes, other municipal taxes and duties, environmental taxes and duties and any other type of 
                    taxes or duties in any relevant jurisdiction together with any interest, penalties, surcharges or 
                    fines relating thereto due, payable, levied, imposed upon or claimed to be owed in any relevant 
                    jurisdiction.
                </p>
                <p>
                    <b>“User”</b> or <b>“Users”</b> means third parties to this Agreement and shall mean such individuals who pay 
                    to screen the Content provided by the Content Owner on its Streaming Platform maintained by 
                    the Service Provider.
                </p>
                <p>
                    <b>“User Subscription Payment Model”</b> means a payment model wherein a User pays a daily 
                    or weekly or monthly or annual amount (as the case may be) on a recurring basis to view the 
                    Content on the Content Owner’s Streaming Platform.
                </p>
            </li>
        </ol>
    </li>
    <li>
        <h5>Interpretation.</h5>
        <ol style="list-style: none;">
            <li><span>2.1</span>
                Any reference to the singular includes a reference to the plural and vice versa, unless explicitly provided for otherwise and any reference to the masculine includes a reference to the feminine and vice versa.
            </li>
            <li><span>2.2</span>
                Headings and captions are used for convenience only and shall not affect the interpretation of this Agreement.
            </li>
            <li><span>2.3</span>
                Any reference to a natural person shall, unless repugnant to the context, include his heirs, executors and permitted assignees.
            </li>
        </ol>
    </li>
    <li>
        <h5>Provision of Services.</h5>
        <ol style="list-style: none;">
            <li><span>3.1</span>
                The Service Provider shall provide and maintain access to the Content Owner’s Streaming Platform by providing Services as provided in <a href="#annexture-a"><b>Annexure A</b></a> to this Agreement. 
            </li>
            <li><span>3.2</span>
                The Parties hereby agree and acknowledge that during the Term of this Agreement, either Party may suggest that changes be made to the scope, nature or time schedule of the Services being rendered, by a written notice to this effect to the other Party. In the event that the proposed change will, in the sole opinion of the Service Provider, require an excessive time delay in delivery or would result in additional expense being incurred by the Service Provider, then the Parties shall confer and may mutually elect either to withdraw the proposed change(s) or require the Service Provider to make the proposed change, subject to agreeing to the quantum of the delay and/or additional expense being reimbursed to the Service Provider by the Content Owner.
            </li>
            <li><span>3.3</span>
                For the avoidance of any doubt, nothing contained in this Agreement shall prohibit the Service Provider from providing services similar to the Services provided to the Content Owner under this Agreement to third parties.
            </li>
        </ol>
    </li>
    <li>
        <h5>Obligations of the Content Owner.</h5>
        <ol style="list-style: none;">
            <li><span>4.1</span>
                The Content Owner hereby agrees that it shall bear the entire obligation of registering the domain name of the Streaming Platform with the relevant authorities in United States of America and/or abroad. The ownership of the domain name of the Streaming Platform shall vest solely with the Content Owner.
            </li>
            <li><span>4.2</span>
                The Content Owner hereby agrees to fully indemnify the Service Provider and keep it clear of any claim(s) regarding any defect in the ownership of the domain name of the Streaming Platform.
            </li>
            <li><span>4.3</span>
                The Content Owner hereby agrees and fully acknowledges that either the legal ownership of the Content vests with the Content Owner or it has the necessary licenses, rights, consents and/or permissions to publish the Content on the Streaming Platform. The Content Owner also has the sole right to add/remove the posted Content on the Content Owner’s Streaming Platform.
            </li>
            <li><span>4.4</span>
                The Content Owner undertakes, acknowledges and agrees that:
                <ol style="list-style: none;">
                    <li><span>(a) </span>
                        The Service Provider shall not be liable in any circumstances in any manner whatsoever for any Content, including but not limited to, for any errors or omissions, or for any loss or damage of any kind incurred as a result of the use of any Content accessed via the Services.
                    </li>
                    <li><span>(b) </span>
                        It must evaluate and bear all risks associated with the use of any Content
                    </li>
                    <li><span>(c) </span>
                        the Service Provider is not responsible for User’s conduct and communications while accessing the Content Owner’s Streaming Platform and for any consequences thereof and
                    </li>
                    <li><span>(d) </span>
                        when using the Service, the Content Owner shall not:
                        <ol style="list-style: lower-roman">
                            <li>defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of any Person</li>
                            <li>post any inappropriate, defamatory, infringing, obscene, illegal or unlawful Content</li>
                            <li>download anything posted by any other user of the Services that the Content Owner knows or reasonably should know cannot be legally distributed in such manner</li>
                            <li>restrict or inhibit any other user of the Services provided by the Service Provider from using and enjoying the Services so provided</li>
                            <li>use the Service for any illegal, unlawful or unauthorized purpose under extant Laws</li>
                            <li>interfere with or disrupt the Service or servers or networks connected to the Service or disobey any requirements, procedures, policies or regulations of networks connected to the Service</li>
                            <li>use any robot, spider, site search/retrieval application or other device to retrieve or index any portion of the Service or collect information about Users in an unauthorised manner for any purpose whatsoever</li>
                            <li>promote or provide instructional information about illegal activities or promote physical harm or injury against the Service Provider or any Person; and/or</li>
                            <li>copy, modify, distribute, sell or lease any part of the software development kit or the Services, nor may it reverse engineer or attempt to extract the source code of the software in the software development kit unless the Content Owner has Service Provider’s written permission</li>
                        </ol>
                    </li>
                </ol>
            </li>
        </ol>
    </li>
    <li>
        <h5>Rights and Obligations of the Service Provider.</h5>
        <ol style="list-style: none;">
            <li><span>5.1</span>
                The Service Provider agrees to comply with its own local rules regarding online and offline conduct and acceptable content including Laws regulating the use, storage and export of data.
            </li>
            <li><span>5.2</span>
                The Service Provider shall make reasonable endeavour that the Services are performed in a professional and competent manner, consistent with industry standards reasonably applicable to such services.
            </li>
            <li><span>5.3</span>
                Nothing in this Agreement shall be construed as a guarantee regarding the results or the definitive outcome of the Services rendered by the Service Provider to the Content Owner. The Service Provider makes no such promises or guarantees in relation to the scope of work or any other incidental or ancillary matters and the Content Owner agrees that the outcome of the Services rendered by the Service Provider pursuant to the scope of work is contingent on a large number of extraneous factors outside the reasonable control of the Service Provider
            </li>
            <li><span>5.4</span>
                <ol style="list-style: none;">
                    <li><span>(a) </span>
                        The Service Provider reserves the right to monitor Content present on the Streaming Platform for the purposes of ensuring adherence to the restrictions and limitations as and when required; and
                    </li>
                    <li><span>(b) </span>
                        The Service Provider is not responsible for monitoring the Content for violation of any Law or restrictions as described in Clause 4.4 above.
                    </li>
                </ol>
            </li>
            <li><span>5.5</span>
                The Service Provider shall provide the Content Owner web-based access to reports and analytics related to Content Owner’s Content, Revenue and Streaming Platform.
            </li>
        </ol>
    </li>
    <li>
        <h5>Revenue.</h5>
        <ol style="list-style: none;">
            <li><span>6.1</span>
                The Parties hereby agree that the Content Owner retains the sole right to collect the Revenue on the Streaming Platform through its payment gateway.
            </li>
            <li><span>6.1</span>
                The Content Owner shall have the sole right to decide the price to be paid by the User for viewing the Content on the Streaming Platform under any/all of the payment models (i.e. Hybrid Payment Model, Pay per View Payment Model and/or User Subscription Payment Model). The Content Owner shall also have the sole right to decide the price to be paid by the advertisers.
            </li>
        </ol>
    </li>
    <li>
        <h5>Pricing.</h5>
        <ol style="list-style: none;">
            <li>
                <p>  Muvi&rsquo;s pricing is mentioned on the page <a href="<?php echo Yii::app()->getBaseUrl(true);?>/pricing.html" target="_blank"><?php echo Yii::app()->getBaseUrl(true);?>/pricing.html.</a> </p>
                <p>FAIR USAGE POLICY: Muvi does not charge for storage as long as storage used in a month is less or equal to bandwidth used in a month. Otherwise, storage is charged at $0.04 per GB. This is to prevent cases where a customer can store a lot of data but never use it.</p>
            </li>
        </ol>
    </li>
    <li>
        <h5>Cancellation and Refund Policy.</h5>
        <ol style="list-style: none;">
            <li>
                You can cancel your subscription to Muvi at any time from the website itself. In this case, you will not be charged for the next month (billing cycle). No refunds are offered.
            </li>
        </ol>
    </li>
    <li>
        <h5>Representations and Warranties.</h5>
        <ol style="list-style: none;">
            <li><span>9.1</span>
                Each Party represents to the other Party hereto that: 
                <ol style="list-style: none;">
                    <li><span>(a)</span>
                        such Party has the full power and authority to enter into, execute and deliver this Agreement and to perform the obligations contemplated herein
                    </li>
                    <li><span>(b)</span>
                        the execution and delivery of this Agreement by any Party and the performance of actions or omissions by such Party of the obligations contemplated hereunder have been duly authorized by all necessary corporate, statutory, contractual or any other authority responsible for authorizing such execution, delivery, action or omission
                    </li>
                    <li><span>(c)</span>
                        assuming the due authorization, execution and delivery hereof by the other Party, this  Agreement constitutes the legal, valid and binding obligation of such Party, enforceable against such Party in accordance with its terms except as such enforceability may be limited by applicable bankruptcy, insolvency, reorganization, moratorium or similar Laws affecting creditors' rights generally; and
                    </li>
                    <li><span>(d)</span>
                        the execution, delivery, and performance of this Agreement by such Party and the consummation of the obligations contemplated herein will not: 
                        <ol style="list-style: lower-roman;">
                            <li>require such Party to obtain any consent, approval or action of any Governmental Authority or any other Person in United States of America or any other Person pursuant to any instrument, contract or other agreement to which such Party is a party or by which such Party is bound.</li>
                            <li>conflict with or result in any material breach or violation of any of the terms and conditions of, or constitute (or with notice or lapse of time or both constitute) a default under, any instrument, contract or other agreement to which such Party is a party or by which such Party is bound</li>
                            <li>violate any order, judgment or decree against, or binding upon, such Party or upon its respective securities, properties or businesses; or </li>
                            <li>violate any Law or regulation of United States of America or any other country in which it maintains its principal office. </li>
                        </ol>	
                    </li>
                </ol>
            </li>
            <li><span>9.2</span>
                The Content Owner represents to the Service Provider that:
                <ol style="list-style: none;">
                    <li><span>(a)</span>
                        the execution, delivery and performance of this Agreement by the Content Owner and the consummation of the obligations contemplated herein shall not violate any provision of the organizational or governance documents of the Content Owner; and
                    </li>
                    <li><span>(b)</span>
                        it has not relied on any oral or written information or advice whether given by the Service Provider, its employees or agents and has used its independent business judgment in order to enter into this Agreement
                    </li>
                </ol>
            </li>
            <li><span>9.3</span>
                The Content Owner agrees to protect any Intellectual Property Rights owned or sought to be protected by the Service Provider or its subcontractors or affiliates. This includes the copyrighted and protected materials and the Service Provider’s logo and trademarks.
            </li>
        </ol>
    </li>
    <li>
        <h5>Relationship.</h5>
        <ol style="list-style: none;">
            <li><span>10.1</span>
                Nothing in this Agreement shall constitute or be deemed to constitute a partnership, joint venture, agency or the like between the Parties hereto or confer on any Party any authority to bind the other Party or to contract in the name of the other Party or to incur any liability or obligation on behalf of the other Party, except as specifically contained herein. 
            </li>
            <li><span>10.2</span>
                Any and all employees of the Service Provider, while engaged in the performance of Services under this Agreement, shall be considered employees of the Service Provider only and not of the Content Owner. 
            </li>
            <li><span>10.3</span>
                During the Term of this Agreement and for a period of 1(one) year thereafter, the Content Owner shall not directly or indirectly, acting alone or in conjunction with others induce or attempt to influence any employee of the Service Provider to terminate his/her employment or to enter into any employment or other business relationship with any other Person (including the Content Owner), firm or corporation.
            </li>
        </ol>
    </li>
    <li>
        <h5>Assignment.</h5>
        <ol style="list-style: none;">
            <li><span>11.1</span>
                This Agreement and the rights and liabilities hereunder shall bind and inure to the benefit of the respective successors of the Parties hereto
            </li>
            <li><span>11.2</span>
                The Parties shall not be entitled to assign this Agreement or any part thereof to any third person under any circumstances, without the prior written consent of the other Party.
            </li>
        </ol>
    </li>
    <li>
        <h5>Limitation of Liability and Indemnity.</h5>
        <ol style="list-style: none;">
            <li><span>12.1</span>
                Each of the Parties (the <b>“Indemnifying Party”</b>) shall specifically and expressly defend, indemnify and hold the other Party (the <b>“Indemnified Party”</b>), its affiliates, employees and its consultants harmless against any direct or indirect losses, costs or expenses the Indemnified Party may sustain or incur as a result of any claim, suit or proceeding made, brought or threatened against the Indemnified Party, its affiliates, agents, employees and its consultants by virtue of the Agreement or any assertions made on behalf of the  Indemnifying Party by the Indemnified Party, its employees and its consultants or any costs incurred by Indemnified Party, its employees and its consultants pursuant to this Agreement.
            </li>
            <li><span>12.2</span>
                Neither Party shall be liable to the other or any third party under this Agreement for any indirect, special, incidental, remote punitive, exemplary or consequential damages arising out of or resulting from this Agreement.
            </li>
        </ol>
    </li>
    <li>
        <h5>Confidentiality.</h5>
        <ol style="list-style: none;">
            <li><span>13.1</span>
                Each of the Parties agree to keep confidential any information provided by a Party (<b>“Disclosing Party”</b>) to the other Party (<b>“Recipient Party”</b>) either directly by communication or inadvertently delivered to the Recipient Party in pursuance to this Agreement, with the exception of the following information:
                <ol style="list-style: none;">
                    <li><span>(a)</span>
                        information which is or was already known to the Recipient Party prior to the Execution Date, other than by virtue of such information having been given to the Recipient Party by the Disclosing Party.
                    </li>
                    <li><span>(b)</span>
                        information which is received by the Recipient Party from a third party, which is not subject to similar confidentiality restrictions and without breach of this Agreement.
                    </li>
                    <li><span>(c)</span>
                        information which is independently developed by the Recipient Party; and
                    </li>
                    <li><span>(d)</span>
                        information which is required to be disclosed by Law. 
                    </li>
                </ol>	
            </li>
        </ol>
    </li>
    <li>
        <h5>Tax.</h5>
        <ol style="list-style: none;">
            <li><span>14.1</span>
                Each Party hereby undertakes to pay to the other promptly on demand an amount equal to such proportion of Tax payable or suffered:
                <ol style="list-style: none;">
                    <li><span>(a)</span>
                        in respect of or arising from any transaction effected or deemed in Laws to have been  effected in relation to this Agreement and
                    </li>
                    <li><span>(b)</span>
                        any and all Tax arising out of this Agreement which is attributable to and payable by either Party under the applicable Laws.
                    </li>
                </ol>	
            </li>
            <li><span>14.2</span>
                Unless otherwise stated in this Agreement, all Tax arising out of this Agreement shall be paid by the Content Owner to Muvi.
            </li>
        </ol>
    </li>
    <li>
        <h5>Intellectual Property Rights.</h5>
        <ol style="list-style: none;">
            <li><span>15.1</span>
                All the Intellectual Property Rights arising out of the Content provided by the Content Owner to the Service Provider shall be solely owned by the Content Owner. Similarly, all the Intellectual Property Rights arising out of the domain name of the Streaming Platform of the Content Owner shall be solely owned by the Content Owner. Any claim by a third party regarding the ownership of the Content provided by the Content Owner and the domain name of the Streaming Platform shall not be the responsibility of the Service Provider. The Content Owner hereby agrees to indemnify the Service Provider with respect to any claims regarding the ownership of the Content as per Clause 11 of this Agreement.
            </li>
            <li><span>15.2</span>
                All the Intellectual Property Rights arising out of the software development kit of the Service Provider shall be solely owned by the Service Provider.
            </li>
            <li><span>15.3</span>
                The Parties agree that all the data that will be generated during the course of rendition of Services, including but not limited to, phone number, e-mail address and any other personal information of Users, will be co-owned by the Service Provider and the Content Owner. Each of the Service Provider and Content Owner shall have the right to access any such data at any given point during the course of Service.
            </li>
            <li><span>15.4</span>
                Any data generated, excluding personal information of the Users, pursuant to the Services can be made available by the Service Provider to any third parties for any legitimate purposes, including but not limited to, for commercial purposes, at the sole discretion of the Service Provider.
            </li>
            <li><span>15.5</span>
                The Parties agree that the Service Provider shall have a right to display and publicize its association with the Content Owner in its collateral, branding, customer listings published on the Service Provider’s website (<a href="http://www.muvi.com">www.muvi.com</a>), sales presentations, promotional materials, business plans or news releases.
            </li>
            <li><span>15.6</span>
                The Parties agree that the Service Provider shall also have the right to display its logo and name on the Streaming Platform’s footer in the form of ‘Powered by Muvi.com’ or such other form as may be decided by the Service Provider from time to time.
            </li>
        </ol>
    </li>
    <li>
        <h5>Notices.</h5>
        <ol style="list-style: none;">
            <li><span>16.1</span>
                All notices given pursuant to this Agreement, shall be in writing and shall be deemed to be served as follows: (i) in the case of any notice delivered by hand, when so delivered; (ii) if sent by pre-paid post or courier, on the fifth business day after the date of posting; (iii) in the case of any notice sent by facsimile, upon the receipt of a confirmation copy at the sender’s facsimile machine; and (iv) if sent by e-mail, 24 (twenty four) hours after the e-mail is sent.
            </li>
            <li><span>16.2</span>
                Any notice to be given by any Party shall be deemed to be duly served if delivered by prepaid registered post, through a delivery service/courier, by hand delivery, by fax or by email to the address mentioned on the signature page of this Agreement. 
            </li>
            <li><span>16.3</span>
                Any change in the address of either Party shall be notified to the other Party in the same manner mentioned hereinabove.
            </li>
        </ol>
    </li>
    <li>
        <h5>Force Majeure.</h5>
        <ol style="list-style: none;">
            <li><span>17.1</span>
                Except in respect of payment liabilities, neither Party shall be liable for any failure or delay in its performance under this Agreement due to reasons beyond its reasonable control, including acts of war, acts of God, riots, embargo, sabotage, governmental act or failure of the internet, provided the delayed Party gives the other Party prompt notice with the reasons for such failure or delay.
            </li>
        </ol>
    </li>
    <li>
        <h5>Miscellaneous.</h5>
        <ol style="list-style: none;">
            <li><span>18.1</span>
            <u>Entire Understanding</u>. This Agreement contains the complete and integrated understanding and agreement between the Parties hereto and supersedes any understanding, agreement or negotiation, whether oral or written, as set forth herein or in written amendments hereto duly executed by both Parties.
    </li>
    <li><span>18.2</span>
    <u>Amendment</u>. No change, alteration, modification, amendment or addition to this Agreement shall be effective unless it is in writing and properly signed by both Parties.
    </li>
    <li><span>18.3</span>
    <u>Governing Law and Jurisdiction</u>. This Agreement shall, in all respects, be governed by and construed in all respects in accordance with the laws of United States of America without giving effect to conflict of laws principles and shall be subject to the exclusive jurisdiction of the courts in New York, United States of America.
    </li>
    <li><span>18.4</span>
    <u>Attorneys’ Fees and Costs</u>. If court proceedings are required to enforce any provision or to remedy any breach of this Agreement, the prevailing Party shall be entitled to an award of reasonable and necessary expenses of litigation, including reasonable attorneys’ fees and costs.
    </li>
    <li><span>18.5</span>
    <u>Severability</u>. If any provision of this Agreement is void, or is so declared, such provision shall be severed from this Agreement. The Agreement shall otherwise remain in full force and effect.
    </li>
    <li><span>18.6</span>
    <u>Remedies</u>. Each of the Parties acknowledge, understand and agree that should a Party breach any of its obligations contained in this Agreement, the other Party shall have the right to fully enforce this Agreement and the other Party shall be irreparably harmed and entitled to specific performance, including without limitation, an immediate issuance of a temporary restraining order or preliminary injunction (without posting a bond) enforcing this Agreement, in addition to a judgment for damages caused by any such breach, and to any other remedies provided for by applicable Laws.
    </li>
    <li><span>18.7</span>
    <u>Counterparts</u>. This Agreement may be signed upon any number of counterparts, whether by original signature or by scan, email or facsimile, with the same effect as if the signature to any counterpart was an original signature upon the same instrument.
    </li>
    <li><span>18.8</span>
    <u>Waiver</u>. Waiver by one Party hereto of breach of any provision of this Agreement by the other shall not operate or be construed as a continuing waiver.
    </li>
    <li><span>18.9</span>
    <u>Mutual Obligations</u>. Each Party agrees to not knowingly do any act or knowingly make any statement, oral or written, which would injure the other Party’s business, its interest, or its reputation, unless required to do so in a legal proceeding by a competent court with proper jurisdiction.
    </li>
    <li><span>18.10</span>
    <u>Good Faith</u>. Each Party will act in good faith in the performance of its respective duties and responsibilities and will not unreasonably delay or withhold the giving of consent or approval required for the other Party under this Agreement. Each Party will provide an acceptable standard of care in its dealings with the other Party and its employees.
    </li>
    <li><span>18.11</span>
    <u>Survival</u>. Notwithstanding the foregoing, the provisions set forth in Clauses 11 (Limitation of Liability and Indemnity), 12 (Confidentiality), 15 (Notices), 17.3 (Governing Law and Jurisdiction), 17.4 (Attorneys’ Fees and Costs), 17.5 (Severability), 17.6 (Remedies) and 17.11 (Survival) shall survive any termination of this Agreement.
    </li>
    <li><span>18.12</span>
    <u>Contact</u>. Contact Muvi LLC any time via email at <a href="mailto:studio@muvi.com">studio@muvi.com</a>, via phone at 860-896-5151 or via physical address at 103 Patroon Dr, Suite 8, Guilderland, NY 12084.
    </li>
</ol>
</li>
</ol>

<br />
<h4 id="annexture-a">Annexure A</h4>
<br />
<p>Muvi is the core product offering of Muvi LLC. Following is a description of different editions of Muvi.</p>
<ul>
    <li>
        <h5>Muvi.</h5>
        <p>Muvi is the comprehensive, all-in-one solution for launching a VOD or live streaming application, it includes everything including white labeled website or/and mobile apps, hosting, CDN, DRM, player, analytics and support. Following are the key features –</p>
        <ol>
            <li>
                <h5>YourBrand.com</h5>
                Use any domain name as you may have registered.
            </li>
            <li>
                <h5>Flexible business models</h5>
                Supports SVOD (subscription), TVOD (pay-per-view), AVOD (advertising) or a hybrid (a content being of different rate for subscribers and non-subscribers). Set any price for pay-per-view and subscription as you like.
            </li>
            <li>
                <h5>Cloud hosting</h5>
                Muvi uses cloud hosting, is therefore infinitely scalable in storage and bandwidth.
            </li>
            <li>
                <h5>Encode to multiple formats</h5>
                Upload video in any format. Muvi automatically converts it to different formats appropriate for different screen sizes (laptop, mobile and TV) and Internet speeds.
            </li>
            <li>
                <h5>Customize the look and feel</h5>
                Completely customize the look and feel of your VOD platform or/and page.
            </li>
            <li>
                <h5>Comprehensive analytics and reports</h5>
                View different kinds of reports including revenue, subscribers, content views and search analytics.
            </li>            
        </ol>
    </li>
    <li>
        <h5>Muvi On-premise</h5>
        <p>Same as Muvi, but videos may be hosted on your server or/and delivered using your CDN.</p>
    </li>
    <li>
        <h5>Muvi Server</h5>
        <p>It does not include the front-end application (website or mobile apps). Muvi Server offers the video CMS (content management system), does all video processing (upload, encoding, storage, and player) and offers an API for you to build your custom VOD application.</p>
    </li>    
</ul>

<br/><br/>

</div>
