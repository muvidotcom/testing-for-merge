<?php
$studio_id = Yii::app()->user->studio_id;
$purchasetype=0;
if((isset($_GET['purchasetype'])=='buynow') && trim($_GET['purchasetype']=='buynow')) {
    $purchasetype=1;
}?>
<style>
    .subli{ display: none;}
    .platform_chkbox.error {
    display: inline;
}
</style>
<?php if($purchasetype==1) {} else { ?>
<div class="col-md-12 frt_form hideloader" id="signup_laststep">
   <div class="setting_heading">Please wait!.... we are building your platform</div>
    <div class="progress">
     <div id="myProgress">
      <div id="myBar" class="progress-bar  progress-bar-striped active"></div>
    </div>
    </div>

   <div class="col-md-12">
       <p class="percentage" id="percentage">100%</p>
  </div>
<div class="col-md-12 col-sm-12 signup_laststep_padd">
     <div class="col-md-5 col-sm-5 rotate">
           <div class="rotatebox">
               
           </div>
         <div class="clearfix"></div>
     </div>
       <div class="col-md-7 col-sm-7 signup_laststep_list">
           <div class="subli">Contacting Amazon Web Services</div>
           <div class="subli">Deploying Cloud Servers, Storage, Transcoding & Database Servers</div>
           <div class="subli">Deploying Global CDN</div>
           <div class="subli">Deploying Firewall & Enabling Security Measures</div>
           <div class="subli">Deploying the CMS & Admin Module</div>
           <div class="subli">Deploying Website, Mobile & TV Apps framework</div>
           <div class="subli">Finishing up all the modules</div>
           <div class="subli">Preparing for launch</div>
     </div>

    <div class="clearfix"></div>
   </div>
       <div class="clearfix"></div>
   </div><?php } ?>
<div class="col-md-6 col-sm-6 frt_form frt_rht_box">
    <?php if($purchasetype==1) {?>
        <h1>Start your Subscription Now!</h1>
        <p class="frt_sub_heading">Credit Card Required</p>
        <div class="frt_steps_box111">
            <div class="frt_steps_box11 frt_steps_box21_color"></div>
            <div class="frt_steps_box21 frt_steps_box11_color"></div>
            <div class="frt_steps_box31 frt_steps_box31_color"></div>
</div>
    <?php }else{ ?>
    <h1>Start your 14 Days Free Trial!</h1>
    <p class="frt_sub_heading">No Credit Card Required</p>
    <div class="frt_steps_box">
        <div class="frt_steps_box1 frt_steps_box2_color"></div>
        <div class="frt_steps_box2 frt_steps_box1_color"></div>
    </div>
    <?php }?>
    <p class="frt_sub_heading2">Please click to select your platform options</p>
   <div class="hintlbl3">You can change these settings later from the CMS as well</div>
      <form  name="templateForm" method="post" id="updateTemplate">
             <div class="linebg"></div>
        <div class="row content1">
            <div class="col-md-12 platform_title">Select the Content Type used on the platform &nbsp;&nbsp; <label id="content[]-error" class="error" for="content[]"></label></div>
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform platform_selected">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Video-on-demand_blue.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Video-on-demand.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Video-on-demand_blue.png"    />
                                    &nbsp;Video
                                    <input type="checkbox" class="platform_chkbox" name="content[]" checked="" value="1" />
                                    <input type="checkbox" class="platform_chkbox" name="content[]" checked="" value="2" />
                            </div>       
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                                
                                    <img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Audio-streming.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Audio-streming.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Content-Audio-streming_blue.png" />
                                    &nbsp;Audio
                                    <input type="checkbox" class="platform_chkbox" name="content[]" value="4" />
                               
                            </div>       
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                          
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/physical_goods_icon.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/physical_goods_icon.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/physical_goods_icon_blue.png"/>
                                    &nbsp;Physical
                                <input type="checkbox" class="platform_chkbox" id="content_physical" name="content_physical[]" value="1" />
                       
                            </div>       
                        </div>                           
        </div>
         
              <div class="linebg"></div>
        <div class="row apps">
            <div class="col-md-12 platform_title">Select the platforms you wish to launch on &nbsp;&nbsp; 
                <label id="apps[]-error" class="error" for="apps[]"></label>
            </div>
    
            <div class="col-md-12 ">
                <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform platform_selected">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_website_blue.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_website.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_website_blue.png"    />
                                    &nbsp;Website
                                    <input type="checkbox" class="platform_chkbox" name="apps[]" checked="" value="1" />
                            </div>       
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                         
                                    <img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_ios.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_ios.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_ios_blue.png" />
                                    &nbsp;iOS App
                                    <input type="checkbox" class="platform_chkbox" name="apps[]" value="2" />
                        
                            </div>       
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                    
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android_blue.png"/>
                                    &nbsp;Android App
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="4" />
                             
                            </div>       
                        </div> 
                          <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                   
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Roku.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Roku.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Roku_blue.png"/>
                                    &nbsp;Roku App
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="8" />
                      
                            </div>       
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                             
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Fire_TV.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Fire_TV.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_Fire_TV_blue.png"/>
                                    &nbsp;Fire TV App
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="16" />
                              
                            </div>       
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                              
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_apple_tv.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_apple_tv.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_apple_tv_blue.png"/>
                                    &nbsp;Apple TV App
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="32" />
                          
                            </div>       
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android_tv.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android_tv.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Platform_android_tv_blue.png"/>
                                    &nbsp;Android TV App
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="128" />
                              
                            </div>       
                        </div>
                         <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                              
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/muvi_server.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/muvi_server.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/muvi_server_blue.png"/>
                                    &nbsp;Muvi Server
                                <input type="checkbox" class="platform_chkbox" name="apps[]" value="64" />
                            
                            </div>       
                        </div>
              
            </div>
                </div>
        </div>
          
              <div class="linebg"></div>
        <div class="row content1">
            <div class="col-md-12 platform_title">Select Monetization options you wish to enable &nbsp;&nbsp;<label id="monetization[]-error" class="error" for="monetization[]"></label></div>
               <?php
                $data = Yii::app()->general->monetizationMenuSetting($studio_id);
                ?>
            <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform platform_selected">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_subscription_blue.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_subscription.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_subscription_blue.png"    />
                                    &nbsp;Subscription
                                    <input type="checkbox" class="platform_chkbox" checked="" name="monetization[]" value="1" /> 
                            </div>       
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                               
                                    <img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_pay_per_view.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_pay_per_view.png" changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_pay_per_view_blue.png" />
                                    &nbsp;Pay Per View
                                    <input type="checkbox" class="platform_chkbox" name="monetization[]" value="2" />
                           
                            </div>       
                        </div> 
                        <div class="col-md-4 col-sm-6 col-xs-6 platformbox">
                            <div class="platform">
                      
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_Video_ads.png" alt="" defaultimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_Video_ads.png"  changeimg="<?php echo Yii::app()->theme->baseUrl; ?>/images/Monetization_Video_ads_blue.png"/>
                                    &nbsp;Advertising
                                <input type="checkbox" class="platform_chkbox" name="monetization[]" value="4" />
                             
                            </div>       
                        </div>                           
        </div>
           
           
<input type="hidden" value="<?php echo $template->code; ?>" id="template_name" name="template_name">
<input type="hidden" value="<?php echo $_GET['purchasetype']; ?>" id="<?php echo $_GET['purchasetype']; ?>" name="purchasetype">
        <div class="form-group fg-float">
            <button type="button" id="nextbtn" class="btn btn-primary frt_btn" >NEXT</button>
        </div>

    </form> 

</div>
<script type="text/javascript" id="pap_x2s6df8d" src="https://muvi.postaffiliatepro.com/scripts/salejs.php"></script>
<script type="text/javascript">
	function showConfirmPopup(obj) {
		$("#contenttypemodal").modal('show');
		$("#content_list").attr('category_id', $(obj).attr('category_id'));
	}
        function removeContent(obj) {
		var categroy_id = $(obj).attr('category_id');
		if ( confirm('Are you sure to delete the content type?') &&  parseInt(categroy_id) ) {
			var url = "<?= Yii::app()->getBaseUrl(TRUE);?>/signup/removeContentCategory/category_id/" + categroy_id;
			window.location.href = url;
		} else {
			return false;
		}
	}
	function addContent(){
            var url ="<?= Yii::app()->getBaseUrl(TRUE);?>/category/ajaxContent";
            $.post(url,{},function(res){
				$("#addContentType").html(res).modal('show');		
            });
	}
	
	function editContentType(obj){
            var content_id = $(obj).attr('data-id');
            var url ="<?= Yii::app()->getBaseUrl(TRUE);?>/category/ajaxContent";
            $.post(url,{'content_id': content_id},function(res){
				$("#addContentType").html(res).modal('show');		
            });
	}
	
	function addContentType(){
            $('#cerror').hide();
		 //form validation rules
            var validate =  $("#addContentForm").validate({
                rules: {
                    displayname: "required"
                },
                messages: {
                    displayname: "Please enter a valid category name"
                }
            });
            var x = validate.form();
            if(x){
                $('#add_btn').html('loading...');
                $('#add_btn').attr('disabled','disabled');
                var is_first = "<?php echo count($data); ?>";
                
                var displayname = $.trim($('#displayname').val());
                var category_id = $('#category_id').val();
                var parent_id = $.trim($('#parent_id').val());
                var url ='';
                
                if (category_id && parent_id !== '') {
                    url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/updateContentCategory";
                } else {
                    url ="<?= Yii::app()->getBaseUrl(TRUE); ?>/category/addContentCategory";
                }
                
                $.post(url,{'displayname':displayname, 'category_id':category_id,'parent_id':parent_id},function(res){
                    if(res){	
                        $('#addContentType').modal('hide');
                        window.location.reload();
                    }else{
                        $('#add_btn').removeAttr('disabled');
                        $('#add_btn').html('submit');
                        $('#cerror').show();
                        if(res.msg){
                            $('#cerror').html(res.msg);
                        }else{
                            $('#cerror').html('Oops! Sorry you have already added this contnet type!');
                        }
                    }
                },'json');
            }
	}
 function progresbar() {
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 90);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      document.getElementById('percentage').innerHTML = width * 1  + '%';
    }
  }
}
  function fade(ele) {
    ele.fadeIn(900, function() { // set fade animation fothe emlement
      var nxt = ele.next(); // get sibling next to current eleemnt
        $(this).addClass("signup_laststep_list1").removeClass("signup_laststep_list2");
      if (nxt.length) // if element exist
        fade(nxt); // call the function for the next element
    });
    ele.addClass("signup_laststep_list2");   
  }
$(document).ready(function(){
$('#nextbtn').click(function(){
        var validate = $("#updateTemplate").validate({
            rules: {
               'content[]': {required: function(element){
                           if($("#content_physical").is(':checked')){
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                },
               'apps[]': {required: true},
               'monetization[]': {required: true},
               
            },
            messages: {
                "content[]": "Please select atleast one from this section",
                "apps[]": "Please select atleast one from this section",
                "monetization[]": "Please select atleast one from this section",
            },
            errorPlacement: function(error, element) {
                error.addClass('red');
                error.insertBefore(element.parent().parent());
            }
        });
        var x = validate.form();
        if (x) {
                $("#nextbtn").text("Wait..");
                $("#nextbtn").attr("disabled","disabled");
                var purchasetype="<?php echo $_GET['purchasetype']; ?>";
             if($.trim(purchasetype) === 'buynow') {}
             else{ 
                $(window).scrollTop(0);
                $('#signup_laststep').removeClass('hideloader').addClass('showloader');
                progresbar();
                fade($('.signup_laststep_list .subli').eq(0)); 
                $('.frt_lft_box').hide();
                $('.frt_rht_box').hide();
                $('.frt_additional_txt').hide();
             }  
           $.ajax({
            url: "<?php echo Yii::app()->baseUrl; ?>/signup/updateTemplate",
            type : "POST",
            data : $("#updateTemplate").serialize(),
            success: function(data){
            setTimeout(function () {          
                var purchasetype="<?php echo $_GET['purchasetype']; ?>";
             
             if($.trim(purchasetype) === 'buynow') 
             {
                window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>/signup/buynow";
             }else {
                /*if(data!=" "){
                    PostAffTracker.setAccountId('default1');
                    var action = PostAffTracker.createAction('registration'); 
                    action.setOrderID('FREETRIAL_'+data);
                    PostAffTracker.register();
                }*/
                window.location.href = "<?php echo Yii::app()->getbaseUrl(true); ?>/admin/dashboard";
            }
               return false;
               }, 5000);
            }
    });
        }
    });

});
</script>