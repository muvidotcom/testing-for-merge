<form class="form-horizontal" method="post" name="domainForm" id="domainForm" onsubmit="return validateDomain();" action="javascript:void(0);">
  <div class="form-group">
    <div class="span10">
		<input type="radio" class="form-control" id="domainName" name="domainName" required="true" checked="true" style="margin: 0px;">
		Have a domain name that i would like to use
    </div>
  </div>
  <div class="form-group">
	<div class="span2"></div>  
    <div class="span10">
		<div class="span2">Domain Name </div>
		<input type="text" class="form-control domain-input" id="subdomain" name="subdomain" placeholder="Enter domain" required="true" onkeyup="getdomainUrl();" onblur="getdomainUrl();" >
		<label id="subdomain-error" class="error" for="subdomain" style="display: none;"></label><br/>
		<span class="prev-text">Your preview website will be <span class="">http://www.<span class="disabled" id="subText">xxx</span>.<?php echo $domain;?></span></span>
    </div>
  </div>
  <div class="form-group disabled ">
	<div class="span12 new-domain">
		<input type="radio" class="form-control" id="domainName" name="domainName" disabled="true" style="margin: 0px;">
		Would like to purchase a new domain name(coming soon)
    </div>
  </div>
  <div class="form-group">
	 <label for="" class="span2 control-label">&nbsp;</label> 
	 <div class="span10"></div>
  </div>
	<div class="form-group">
	  <label for="" class="span2 control-label">&nbsp;</label> 
		<div class="span10">
			<button type="button" class="btn btn-grey" id="prvbtn" disabled="true">Back</button> &nbsp;&nbsp;
			<button type="submit" class="btn btn-blue" id="nextbtn">Next</button>
		</div>
  </div>
	<input type="hidden" value="0" id="is_submit" name="is_submit"/>
</form>
<script type="text/javascript">
	function getdomainUrl(){
		var subdomain = $.trim($('#subdomain').val()).toLocaleLowerCase();
		subdomain = subdomain.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
		if(subdomain && subdomain.length>=3){
			$('#subdomain').val(subdomain);
			$('#subText').html(subdomain);
			$('#subText').removeClass('disabled');
		}else{
			$('#subText').addClass('disabled');
			$('#subText').html('xxx');
		}
	}
	
	function validateDomain(){
		$('#subdomain-error').hide();
		 //form validation rules
          var validate =  $("#domainForm").validate({
                rules: {
                    domainName: "required",
                    subdomain: {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
					domainName: "Please select a domain",
                    subdomain: {
                        required: "Please provide a subdomain",
                        minlength: "Your subdomain must be at least 3 characters long"
                    }
                }
            });
			var x = validate.form();
			if(x){
				var url ="<?php echo Yii::app()->baseUrl;?>/signup/checkSubdomain";
				$.post(url,{'subdomain':$('#subdomain').val()},function(res){
					if(res.succ){	
						$('#nextbtn').html('wait!...');
						$('#is_submit').val(1);
						document.domainForm.action = '<?php echo Yii::app()->baseUrl;?>/signup/domainName';
						document.domainForm.submit();
					}else{
						$('#subText').addClass('disabled');
						$('#subText').html('xxx');
						$('#subdomain-error').show();
						$('#subdomain-error').html('Oops! Sorry this domain is not available.');
					}
				},'json');
			}
	}
</script>