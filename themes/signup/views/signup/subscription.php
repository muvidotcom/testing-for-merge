<?php
$months = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
$applications = json_encode($packages['appilication']);
$app_price = Yii::app()->general->getAppPrice($packages['package'][0]['code']);
?>
<style type="text/css">
    .form-horizontal .control-label {
        text-align: left;
    }
    h3, .h3 {
        font-size: 22px;
        text-align: center;

    }
    .btn-left    {
        margin-left: -15px;
    }
    .control-label-font
    {
        font-size: 16px;
    }
    .checkbox-left{
        margin-left: 18px;
    }
    .error{font-weight: normal;}
    label{cursor:initial; display:inline}
</style>
<div class="col-lg-12 box box-primary">
    <div class="col-lg-12">
        <form class="form-horizontal" method="post" name="paymentMethod" id="paymentMethod" onsubmit="return validateForm();" action="javascript:void(0);">
            <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>" />
            <div class="row form-group">
                <div class="col-lg-11">
                    <h3>Please purchase subscription to continue</h3>
                </div>              
            </div>
            
            <div class="row form-group" id="packagediv">
                <div class="col-lg-11">
                    <div class="pull-left">
                        <h3>Select Package</h3>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div style="color: #ccc;margin: 0px;">
                        <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                            <select id="packages" class="form-control" style="width: auto;margin-left: 5%; cursor: initial" onchange="getAppilication(this);">
                                <?php 
                                $charging_now = 0;
                                $yearly_discount = 0;
                                if (isset($packages['package']) && !empty($packages['package'])) {
                                    $cnt = 0;
                                    foreach ($packages['package'] as $key => $value) {
                                        if ($cnt == 0) {
                                            $charging_now = $value['base_price'];
                                            $yearly_discount = $value['yearly_discount'];
                                        }
                                        $cnt++;?>
                                    <option value="<?php echo $value['id'];?>" data-code="<?php echo $value['code'];?>"  data-price="<?php echo $value['base_price'];?>" data-yearly_discount="<?php echo $value['yearly_discount'];?>" data-is_bandwidth="<?php echo $value['is_bandwidth'];?>"><?php echo $value['name'];?></option>
                                <?php }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div id="appilication_div">
                        <?php if (isset($packages['appilication'][$packages['package'][0]['id']]) && !empty($packages['appilication'][$packages['package'][0]['id']])) { ?>
                        <div class="pull-left">
                            <h3>Select Applications</h3>
                        </div>
                        <div class="clearfix"></div>

                            <?php foreach ($packages['appilication'][$packages['package'][0]['id']] as $key => $value) {?>
                                <div class="pull-left col-xs-1" style="width: 20%;">
                                    <label style="cursor: pointer;">
                                        <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                            <input type="checkbox" class="price_chkbox" data-id="<?php echo $value['id'];?>" data-code="<?php echo $value['code'];?>" data-price="<?php echo $value['price'];?>" <?php if ($value['code'] == 'monthly_charge') { ?>checked="checked"<?php } ?> onclick="selectApplication();" />
                                        </div>
                                        <?php echo $value['name'];?>
                                   </label>
                                </div>
                            <?php } ?>
                            <div class="col-lg-9 pull-left" style="color: #929292;">
                                First application is charged at $<?php echo $charging_now;?> per month, others are $<?php echo $app_price;?> per month per app
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div id="cloud_hosting-div" style="display: none;">
                        <div class="pull-left col-lg-9" style="margin-top: 15px;">
                            <label style="font-weight: bold;">
                                <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                    <input type="checkbox" onclick="cloudHosting(this);"/>
                                </div>
                                Add Cloud hosting for your application (powered by Amazon Web Services)
                            </label>
                            <div>
                                <label id="cloud_hosting-error" class="error" for="cloud_hosting" style="display: none;font-weight: normal;"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div id="custom_code-div" style="display: none;">
                        <div class="pull-left col-lg-9" style="margin-top: 25px;">
                            <div class="pull-left fnt-bold">
                                Custom Code
                            </div>
                            <div class="pull-left col-xs-10">
                                <input type="text" class="form-control" id="custom_code" placeholder="Code given to you by Muvi representive" autocomplete="false" />
                                <div>
                                    <label id="custom_code-error" class="error" for="custom_code" style="margin-top: 5px;display: none;font-weight: normal;"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="pull-left">
                        <h3>Select Plan</h3>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="pull-left col-lg-3">
                        <label style="cursor: pointer">
                            <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                <input type="radio" class="all_plans" value="Month" name="plans" checked="checked" onclick="showPrice();" />
                            </div>
                            Monthly
                       </label>
                    </div>
                    <div class="pull-left" style="color: #929292;">
                        Pay month by month, cancel anytime
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="pull-left col-lg-3">
                        <label>
                            <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                <input type="radio" class="all_plans" value="Year" name="plans" onclick="showPlan(this);" />
                            </div>
                            Yearly <?php if (intval($yearly_discount)) { ?>(save <?php echo $yearly_discount;?>%)<?php } ?>
                       </label>
                    </div>
                    <div class="pull-left" style="color: #929292;">
                        <?php if (intval($yearly_discount)) { ?>Save <?php echo $yearly_discount;?>% by paying for a year in advance.<?php } ?> No refund if canceled during the year
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="pull-left">
                        <h3>Projected Pricing</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row form-group">
                        <div class="col-lg-11">
                            <div>
                                Charging to your card now: <label class="control-label-font">
                                    $<span class="charge_now"><?php echo $charging_now; ?></span>
                                </label>
                            </div>
                            
                            <div class="monthly_payment">
                                <div>
                                    Monthly payment: 
                                    <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                                </div>
                                <div>
                                    Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                                </div>
                            </div>
                            
                            <div class="yearly_payment" style="display: none;">
                                <div>
                                    Yearly payment: 
                                    <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                                </div>
                                <div>
                                    Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                                </div>
                            </div>
                        </div>              
                    </div>

                    <div>
                        See <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">pricing</a> for more detail. Contact your Muvi representative if you are not sure about the package and add-ons you need.
                    </div>

                    <div class="col-lg-9">
                        <br /><button type="button" class="btn btn-blue btn-left" id="nextbutn" onclick="showCards();">Continue to Authorize Payment</button>
                    </div>
                    
                </div>
            </div>
            
            <input type="hidden" name="plan" id="plantext" value="" />
            <input type="hidden" name="packages" id="packagetext" value="" />
            <input type="hidden" name="pricing" id="pricingtext" value="" />
            <input type="hidden" name="cloud_hosting" id="cloud_hosting" value="" />
            <input type="hidden" name="custom_code" id="custom_codes" value="" />
            
            <div id="carddetaildiv" style="display: none;">
                <div class="pull-left">
                    <h3>Projected Pricing</h3>
                </div>
                <div class="clearfix"></div>
                <div class="row form-group">
                    <div class="col-lg-11">
                        <div>
                            Charging to your card now: <label class="control-label-font">
                                $<span class="charge_now"><?php echo $charging_now; ?></span>
                                </label>
                        </div>
                        <div class="monthly_payment">
                            <div>
                                Monthly payment: 
                                <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per month <span class="bandwidth">+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                            </div>
                            <div>
                                Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Months")); ?></label>
                            </div>
                        </div>
                        <div class="yearly_payment" style="display: none;">
                            <div>
                                Yearly payment: 
                                <label class="control-label-font">$<span class="new_charge"><?php echo $charging_now; ?></span></label> per year <span class="bandwidth">+ <a href="<?php echo Yii::app()->baseUrl; ?>/pricing.html" target="_blank">bandwidth</a></span>
                            </div>
                            <div>
                                Next Billing date: <label class="control-label-font"><?php echo Date('F d, Y', strtotime("+1 Years")); ?></label>
                            </div>
                        </div>
                    </div>              
                </div>

                <div id="card-info-error" class="error" style="display: none;margin: 0 0 15px 150px;"></div>
                <div class="row form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">Name on Card</label>                    
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="card_name" name="card_name" placeholder="Enter Name" />
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">Card Number</label>                    
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter Card Number" />
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">Expiry Date</label>                    
                        <div class="col-lg-9">
                        <div style="float: left;width: 49%;margin-right: 2%;">
                            <select name="exp_month" id="exp_month" class="form-control">
                                <option value="">Expiry Month</option>	
                                <?php for ($i = 1; $i <= 12; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $months[$i - 1]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="float: left;width: 49%;">
                            <select name="exp_year" id="exp_year" class="form-control" onchange="getMonthList();">
                                <option value="">Expiry Year</option>
                                <?php for ($i = date('Y'); $i <= date('Y') + 20; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>                     
                </div>
                <div class="row form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">Security Code</label>                    
                        <div class="col-lg-9">
                            <input type="password" id="" name="" style="display: none;" />
                            <input type="password" class="form-control" id="security_code" name="security_code" placeholder="Enter security code" />
                        </div>
                    </div>                     
                </div>        
                <div class="row form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">Billing Address</label>                    
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" ><br/>
                            <input type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" ><br/>
                            <input type="text" class="form-control" id="city" name="city" placeholder="City"><br/>
                            <div style="float: left;width: 49%;margin-right: 2%;">
                                <input type="text" class="form-control" id="state" name="state" placeholder="State" />
                            </div>
                            <div style="float: left;width: 49%;">
                                <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" />
                            </div>
                            <div style="clear: both"></div>
                        </div>
                    </div>                     
                </div>

                <div id="terms-div">
                    <div class="row form-group">
                        <div class="col-lg-10">
                            <div class="col-lg-9">
                                <label style="font-weight: normal;">
                                    <div class="pull-left" style="margin-right: 5px;margin-top: -2px;">
                                        <input type="checkbox" name="terms_of_use" id="terms_of_use">
                                    </div>
                                    I agree with the <a href="<?php echo Yii::app()->baseUrl; ?>/signup/signupTerms" target="_blank">terms of use</a> of Muvi
                               </label>
                               <div><label id="terms_of_use-error" class="error" for="terms_of_use" style="display: none;font-weight: normal;"></label></div>
                            </div>
                        </div>                     
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10">
                        <label class="control-label col-lg-3">&nbsp;</label>
                        <div class="col-lg-5">&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-blue btn-left" id="nextbtn">Purchase Subscription</button></div>
                    </div>
                </div>
            </div>
            <br /><br />
        </form>        
    </div>
</div>

<div id="loadingPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border: none;">
                <div class="modal-title auth-msg">Just a moment... we're authenticating your card.<br/>Please don't refresh or close this page.</div>
                <div><img src="<?php echo Yii::app()->baseUrl; ?>/images/payment_loading.gif" style="padding:5px;"/></div>
            </div>
        </div>
    </div>
</div>

<div id="successPopup" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="color: #42B970">Thank you, Your subscription has been activated successfully.<br/>Please wait we are redirecting you...</h4>
            </div>
        </div>
    </div>
</div>

<div id="studio_login" style="display: none;">
    <form id="studioLoginForm" name="studioLoginForm" method="post" action="javascripti:void(0);">
        <input type="hidden" id="studio_id" name="studio_id" value="" />
        <input type="hidden" id="studio_name" name="studio_name" value="" />
        <input type="hidden" id="studio_email" name="studio_email" value="" />
    </form>
</div>

<input type="hidden" name="price_per_app" id="price_per_app" value="<?php echo $app_price; ?>" />
<script type="text/javascript">
    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'N0V', 'DEC');
    var pricingall = '';
    var packagesall = '';
    var planall = '';
    var custom_codes = '';
    
    $(document).ready(function () {
        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'false');
    
        if($('#custom_code-div').length) {
            $('#custom_code').on('change', function () {
                if ($.trim($("#custom_code").val())) {
                    getPackageCustomeCodePrice(0);
                } else {
                    custom_codes = '';
                    $('option:selected', '#packages').attr('data-price', 0);
                    showPrice();
                }
            });
        }
    });
    
    function showCards() {
        $('#packagediv').hide();
        showPrice();
        $('#carddetaildiv').show();
    }
    
    function validateCustomCode() {
        if ($.trim($("#custom_code").val())) {
            getPackageCustomeCodePrice(1);
        } else {
            $("#custom_code").focus();
            $('#custom_code-error').show().html('Enter custom code');
            return false;
        }
    }
    
    function getPackageCustomeCodePrice(arg) {
        var url = "<?php echo Yii::app()->baseUrl; ?>/payment/validatePackageCustomCode";
        var package_code = $('option:selected', '#packages').attr('data-code')
        var custom_code = $.trim($('#custom_code').val());

        $.post(url, {'package_code': package_code, 'custom_code': custom_code}, function (res) {
            if (parseInt(res.isExists)) {
                $('#custom_code-error').html('').hide();
                
                custom_codes = custom_code;
                $('option:selected', '#packages').attr('data-price', res.price);
                showPrice();
                
                if (parseInt(arg)) {
                    $("#custom_codes").val(custom_codes);
                    showCards();
                }
            } else {
                custom_codes = '';
                $('option:selected', '#packages').attr('data-price', 0);
                showPrice();
                $('#custom_code-error').show().html('Incorrect custom code. Please ask your Muvi representative');
                return false;
            }
        }, 'json');
    }
    
    function getMonthList() {
        var d = new Date();
        var curyr = d.getFullYear();
        var selyr = parseInt($('#exp_year').val());
        var curmonth = d.getMonth() + 1;
        var sel_month = $.trim($("#exp_month").val());
        var startindex = 1;
        
        if (curyr === selyr) {
            startindex = curmonth;
        }
        
        var month_opt = '<option value="">Expiry Month</option>';
        for (var i = startindex; i <= 12; i++) {
            var selected = '';
            if (i === parseInt(sel_month)) {
                selected = 'selected="selected"';
            }
            month_opt += '<option value="' + i + '" '+selected+'>' + months[i - 1] + '</option>';
        }
        $('#exp_month').html(month_opt);
    }
    
    function validateForm() {
        $('#card-info-error').hide();
        
        //form validation rules
        var validate = $("#paymentMethod").validate({
            rules: {
                card_name: "required",
                exp_month: "required",
                exp_year: "required",
                security_code: "required",
                address1: "required",
                city: "required",
                state: "required",
                zipcode: "required",
                terms_of_use: "required",
                card_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                card_name: "Please enter a valid name",
                exp_month: "Please select the expiry month",
                exp_year: "Please select the expiry year",
                security_code: "Please enter your security code",
                address1: "Please enter your address",
                city: "Please enter your city",
                state: "Please enter your State",
                zipcode: "Please enter your Zipcode",
                terms_of_use: "Please select terms of use",
                card_number: {
                    required: "Please enter a valid card number",
                    number: "Please enter a valid card number"
                }
            }
        });

        var x = validate.form();
        
        if (x) {
            $("#packagetext").val(packagesall);
            $("#pricingtext").val(pricingall);
            $("#plantext").val(planall);
            $("#custom_codes").val(custom_codes);
            
            $('#nextbtn').html('wait!...');
            $('#nextbtn').attr('disabled','disabled');
            $("#loadingPopup").modal('show');
            var url ="<?php echo Yii::app()->baseUrl; ?>/signup/purchase";
            var card_name = $('#card_name').val();
            var card_number = $('#card_number').val();
            var exp_month = $('#exp_month').val();
            var exp_year = $('#exp_year').val();
            var cvv = $('#security_code').val();
            var address1 = $('#address1').val();
            var address2 = $('#address2').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var zip = $('#zipcode').val();
            var uniqid ="<?php echo $studio->uniqid; ?>";
            var pricing = pricingall;
            var packages = packagesall;
            var plan = planall;
            var cloud_hosting = $.trim($("#cloud_hosting").val());
            var custom_code = $.trim($("#custom_codes").val());
            var csrfToken = $('#csrfToken').val();
            
            $.post(url, {'card_name': card_name, 'card_number': card_number, 'exp_month': exp_month, 'exp_year': exp_year, 'cvv': cvv, 'address1': address1, 'address2': address2, 'city': city, 'state': state, 'zip': zip, 'packages': packages, 'pricing': pricing, 'plan': plan, 'cloud_hosting': cloud_hosting, 'custom_code': custom_code, 'uniqid': uniqid, 'csrfToken': csrfToken}, function (data) {
                $("#loadingPopup").modal('hide');
                if (parseInt(data.isSuccess) === 1) {
                    $("#successPopup").modal('show');
                    setTimeout(function() {
                        $('#is_submit').val(1);
                        studioLogin(data);
                    }, 5000);
               } else {
                    $('#nextbtn').html('Purchase Subscription');
                    $('#nextbtn').removeAttr('disabled');
                    $('#is_submit').val('');
                    if ($.trim(data.Message)) {
                        $('#card-info-error').show().html(data.Message);
                    } else {
                        $('#card-info-error').show().html('We are not able to process your credit card. Please try another card.');
                    }
               }
            }, 'json');
        }
    }
    
    function getAppilication(obj) {
        var parent = $(obj).val();
        var code = $.trim($('option:selected', obj).attr('data-code'));
        
        //if (code === 'muvi_studio_enterprise') {
        //    $('option:selected', obj).attr('data-price', 0);
        //}
        
        var base_price = parseFloat($('option:selected', obj).attr('data-price'));
        var is_bandwidth = $('option:selected', obj).attr('data-is_bandwidth');
        
        if (parseInt(is_bandwidth)) {
            $(".bandwidth").show();
        } else {
            $(".bandwidth").hide();
        }
        
        var applications = jQuery.parseJSON('<?php echo $applications;?>');
        var str = '';
        
        if (applications[parent]) {
            str = str +'<div class="pull-left">';
            str = str +'<h3>Select Applications</h3>';
            str = str +'</div>';
            str = str +'<div class="clearfix"></div>';
            
            for (var i in applications[parent]) {
                str = str +'<div class="pull-left col-xs-1" style="width: 20%;">';
                str = str +'<label style="cursor:pointer">';
                str = str +'<div class="pull-left" style="margin-right: 5px;margin-top: -2px;">';
                var checked = '';
                if(applications[parent][i].code === "monthly_charge") {
                    checked = 'checked="checked"';
                }
                str = str +'<input type="checkbox" class="price_chkbox" data-id="'+applications[parent][i].id+'" data-code="'+applications[parent][i].code+'" data-price="'+applications[parent][i].price+'" '+checked+' onclick="selectApplication();" />';
                str = str +'</div>';
                str = str +applications[parent][i].name;
                str = str +'</label>';
                str = str +'</div>';
            }
            
            var APP_PRICE = 0;
            $.ajax({
              url: "<?php echo Yii::app()->baseUrl; ?>/site/packageappprice",
              method: "POST",
              data: { 'code': code },
              dataType: "json",
              async: false
            }).done(function( res ) {                                
                APP_PRICE = res.app_price;     
                $('#price_per_app').val(APP_PRICE);
            });

            str = str +'<div class="col-lg-9 pull-left" style="color: #929292;">';
            str = str +'First application iscdfd charged at $'+base_price+' per month, others are $'+APP_PRICE+' per month per app';
            str = str +'</div>'; 
        }
        
        $("#cloud_hosting").val(0);
        if (code === 'muvi_studio_server') {
            $("#cloud_hosting-div").show();
        } else {
            $("#cloud_hosting-div").hide();
        }
        
        $("#custom_code").val('');
        //if (code === 'muvi_studio_enterprise') {
        //    $("#custom_code-div").show();
        //    $("#nextbutn").attr("onclick","validateCustomCode()").bind('click');
        //} else {
            $("#custom_code-div").hide();
            $("#nextbutn").attr("onclick","showCards()");
        //}
        
        $("#appilication_div").html(str);
        showPrice();
    }
    
    function selectApplication() {
        var check = $("input:checkbox.price_chkbox:checked").length;
        if (parseInt(check) === 0) {
            $("input:checkbox.price_chkbox[data-code='monthly_charge']").prop('checked', true);
        }
        
        showPrice();
    }
    
    function cloudHosting(obj) {
        if ($(obj).is(":checked")) {
            $("#cloud_hosting").val(1);
        } else {
            $("#cloud_hosting").val(0);
        }
    }
    
    function showPlan(obj) {
        var plan = $(obj).val();
        
        if (plan === 'Year') {
            $(".monthly_payment").hide();
            $(".yearly_payment").show();
        } else {
            $(".yearly_payment").hide();
            $(".monthly_payment").show();
        }
        
        showPrice();
    }
    
    function showPrice() {
        var base_price = parseFloat($('option:selected', '#packages').attr('data-price'));
        var code = parseFloat($('option:selected', '#packages').attr('data-code'));
        var plan = $('.all_plans:checked').val();
        var price_total = base_price;
        var priceids = new Array();
        
        if ($('.price_chkbox').length) {
            var cnt = 0;
            
            $('.price_chkbox').each(function() {
                if($(this).is(":checked")){
                    var priceid = $(this).attr('data-id');
                    priceids.push(priceid);
                    cnt++;
                }
            });
            var APP_PRICE = $('#price_per_app').val();         
            if (cnt > 1) {
                price_total = price_total + ((cnt -1) * APP_PRICE);
            }
        }
        
        if (plan === 'Year') {
            var yearly_discount = parseInt($('option:selected', '#packages').attr('data-yearly_discount'));
            price_total = ((price_total * 12) - (((price_total * 12) * yearly_discount)/100));
        }
        
        $("#pricingtext").val(priceids.toString());
        pricingall = $("#pricingtext").val();
        
        $('#packagetext').val($('option:selected', '#packages').val());
        packagesall = $('#packagetext').val();
        
        $("#plantext").val(plan);
        planall = plan;
        
        price_total = price_total.toFixed(2);
    
        $(".charge_now").html(price_total);
        $(".new_charge").html(price_total);
    }
    
    function studioLogin(data) {
        $("#studio_login").show();
        $("#studio_id").val(data.studio_id);
        $("#studio_name").val(data.studio_name);
        $("#studio_email").val(data.studio_email);
        document.studioLoginForm.action = '<?php echo Yii::app()->baseUrl; ?>/login/studioLogin';
        document.studioLoginForm.submit();
        $("#studio_login").hide();
    }
</script>