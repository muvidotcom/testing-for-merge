<div class="wrapper">
    <div class="container home-page-customers home-page-studio">
        <p class="center">
			<a href="<?php echo Yii::app()->getBaseUrl(TRUE);?>"> <img src="<?php echo $this->siteLogo?>" alt="" /></a>
        </p>
        <!--<h3 style="color: #000;">Testing content</h3>-->
		<table width="100%" align="center">
			<tbody>
				<tr>
					<td align="center">
						<table width="520px" cellspacing="8" cellpadding="8" align="center" style="border:1px solid #999999;color:#000000">
							<tbody>
							<tr>
								<td align="left" style="border-bottom:1px solid #999999">
									<div style="float:left"><img src="<?php echo ASSETS_URL;?>img/error.png"></div>
									<div style="float:left;padding-left:10px;font-size:18px;">The page you requested was not found</div>
								</td>
							</tr>
							<tr>
								<td align="left" style="padding-top:10px">
									<font style="color:#000000;font-size:14px;">You may have clicked an expired link or mistyped the address.<br>Or something may went wrong.</font>
									<br>
									<br>
									<a class="link" style="color:#0000FF;" href="<?php echo Yii::app()->getBaseUrl(TRUE);?>">Return</a>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
    </div>        
</div> 


