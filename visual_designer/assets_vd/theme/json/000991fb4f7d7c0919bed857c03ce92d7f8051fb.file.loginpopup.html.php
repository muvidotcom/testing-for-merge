<?php /* Smarty version Smarty-3.1-DEV, created on 2017-10-12 10:20:58
         compiled from "/var/www/edocent/themes/player/views/layouts/loginpopup.html" */ ?>
<?php /*%%SmartyHeaderCode:44018512259df420a8bb288-26926581%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '000991fb4f7d7c0919bed857c03ce92d7f8051fb' => 
    array (
      0 => '/var/www/edocent/themes/player/views/layouts/loginpopup.html',
      1 => 1507799259,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44018512259df420a8bb288-26926581',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
    'socialAuth' => 0,
    'Yii' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_59df420a985849_61579785',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59df420a985849_61579785')) {function content_59df420a985849_61579785($_smarty_tpl) {?><!-- Login Modal -->
<div id="loginModal" class="modal fade login-popu" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>


            <div class="modal-body popup_bottom">
                <div class="row">
                    <input type="hidden" id="movie_id" value="" />
                    <input type="hidden" id="stream_id" value="" />
                    <input type="hidden" id="season_id" value="" />
                    <input type="hidden" id="isppv" value="" />
                    <input type="hidden" id="videotype" value="" />
                    <input type="hidden" id="content-permalink" value="" />
                    <input type="hidden" id="content-type-permalink" value="" />
                    <input type="hidden" id="is_ppv_bundle" value="" />
                    <input type="hidden" id="isadv" value="" />
                    <input type="hidden" value="" id="play_url" />
                    <input type="hidden" value="" id="to_play" />
					<input type="hidden" id="isDownload" value="" />
                    <div  id="login_form_div">
                        <div class="col-md-12">
                            <ul class="list-inline top10p">
                                <li>
                                    <h5 class="text-uppercase"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['login'];?>
</h5></li>
                                <li class="meta-info"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['dont_have_account'];?>
  <a href="javascript:void(0);" onclick="showRegister();" class="text-uppercase"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['btn_signup'];?>
</a></li>
                            </ul>
                        </div>
                        <?php if ($_smarty_tpl->tpl_vars['this']->value->studio->social_logins){?>
                        <?php if (isset($_SESSION['social'])){?> 
                        <?php $_smarty_tpl->tpl_vars['socialAuth'] = new Smarty_variable($_SESSION['social'], null, 0);?>
                        <?php }else{ ?>
                        <?php $_smarty_tpl->tpl_vars['socialAuth'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->getSocialUrls(), null, 0);?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['socialAuth']->value){?>
                        <div class="col-md-12">
                            <?php if (($_smarty_tpl->tpl_vars['this']->value->studio->social_logins&1)&&$_smarty_tpl->tpl_vars['socialAuth']->value['fb_url']){?>
                            <a  href="javascript:void(0);" data-url="<?php echo $_smarty_tpl->tpl_vars['socialAuth']->value['fb_url'];?>
" data-login="1" class="login-fb btn as-btn-lg btn-block facebook_login text-uppercase"><img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->siteurl;?>
<?php echo $_smarty_tpl->tpl_vars['Yii']->value->theme->baseUrl;?>
/images/Icons/Facebook-Icon.png"> <?php echo $_smarty_tpl->tpl_vars['this']->value->Language['login_facebook'];?>
</a>
                            <?php }?>
                        </div>
                        <?php }?>
                        
                        <div class="col-md-12">
                            <div class="gray text-center text-uppercase">-<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['or'];?>
-</div>
                        </div>
<?php }?>
                        <div class="col-md-12">
                                <div id="login_errors" class="error"></div>

                            <form name="login_form" id="login_form" method="post">
                                <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>
" />
                            <div class="loader" id="login_loading"></div>

                                <div class="form-group bottom10p">
                                    <input type="email" name="LoginForm[email]" id="username" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['text_email_placeholder'];?>
" autocomplete="off" class="form-control" value="" />

                                </div>
                                <div class="form-group bottom45p">
                                    <input type="password" name="LoginForm[password]" id="password" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['text_password_placeholder'];?>
" autocomplete="off" class="form-control" />
                                </div>

                                <button type="submit" class="btn as-btn-accent as-btn-md center-block top10p" id="login-btn"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['btn_login'];?>
</button>
                                <ul class="list-inline  text-center top15p">
                                    <li class="meta-info"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['forgot_password'];?>
<a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->siteurl;?>
/user/forgot"> <?php echo $_smarty_tpl->tpl_vars['this']->value->Language['btn_reset'];?>
</a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    <div  id="register_form_div" style="display: none;">
                        <ul class="list-inline top10p">
                            <li>
                                <h5 class="text-uppercase"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['register'];?>
</h5></li>
                            <li class="meta-info"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['already_registered'];?>
 <a href="javascript:void(0);"   onclick="showLogin();" class="text-uppercase"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['login'];?>
</a></li>
                        </ul>
                   
                    <?php if ($_smarty_tpl->tpl_vars['this']->value->studio->social_logins){?>
                    <?php if (isset($_SESSION['social'])){?> 
                    <?php $_smarty_tpl->tpl_vars['socialAuth'] = new Smarty_variable($_SESSION['social'], null, 0);?>
                    <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars['socialAuth'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->getSocialUrls(), null, 0);?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['socialAuth']->value){?>
                    <div class="col-md-12">
                        <?php if (($_smarty_tpl->tpl_vars['this']->value->studio->social_logins&1)&&$_smarty_tpl->tpl_vars['socialAuth']->value['fb_url']){?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['socialAuth']->value['fb_url'];?>
" class="login-fb btn as-btn-lg btn-block text-uppercase"><img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->siteurl;?>
<?php echo $_smarty_tpl->tpl_vars['Yii']->value->theme->baseUrl;?>
/images/Icons/Facebook-Icon.png"> <?php echo $_smarty_tpl->tpl_vars['this']->value->Language['register_facebook'];?>
</a>
                        <?php }?>
                    </div>
                    <?php }?>
               
                    <div class="col-md-12">
                        <div class="gray text-center text-uppercase">-<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['or'];?>
-</div>
                    </div>
     <?php }?>
                    <div class="col-md-12">

                        <form id="register_form" name="register_form" method="POST" >
                            <input type="hidden" name="csrfToken" id="csrfToken" value="<?php echo $_SESSION['csrfToken'];?>
" />
                            <div id="register_errors" class="error text-center"></div> 
                            <div id="loader_register" class="loader"></div>
                            <div class="row">
                                <div class="form-group col-md-6 bottom10p">
                                    <input type="text" required name="data[name]" id="fullname" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['text_name_placeholder'];?>
" autocomplete="off" class="form-control" />
                                </div>
                                <div class="form-group col-md-6 bottom10p">
                                    <input type="email" required name="data[email]" id="email" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['text_email_placeholder'];?>
" autocomplete="off" class="form-control" />
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6 bottom10p">
                                    <input type="password" required name="data[password]" id="join_password" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['text_password_placeholder'];?>
" autocomplete="off" class="form-control join_password" />
                                </div>
                                <div class="form-group col-md-6 bottom45p">
                                    <input type="password" required="" value="" class="form-control" autocomplete="off" placeholder="<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['confirm_password'];?>
" id="confirm-passwords" name="data[confirm_password]">

                                </div>
                                <ul class="list-inline  text-center bottom15p">
                                    <li class="meta-info"> 
                                        <input name="data[subscribe_newsletter]" value="1" type="checkbox" checked="checked"> &nbsp;<?php echo $_smarty_tpl->tpl_vars['this']->value->Language['subscribe_to_announcement'];?>
<br />
                                    </li>                                    
                                    <li class="meta-info"> <?php echo $_smarty_tpl->tpl_vars['this']->value->Language['chk_over_18'];?>
  <a  href="<?php echo $_smarty_tpl->tpl_vars['this']->value->siteurl;?>
/page/terms-privacy-policy" target="_blank"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['terms'];?>
</a></li>
                                </ul>
                                <button type="submit" class="btn as-btn-accent as-btn-md center-block" name="register_submit"><?php echo $_smarty_tpl->tpl_vars['this']->value->Language['btn_register'];?>
</button>

                            </div>
                        </form>
                    </div>

                </div>  
            </div>
        </div>
    </div>
</div>
</div>

<div id="ppvModal" class="modal fade login-popu" data-backdrop="static" data-keyboard="false"></div>
<?php }} ?>