$(function(){
    $(window).scroll(function(){
        if($(window).scrollTop() > $('header').height()){
            $('header').addClass('_scrolled');
        }else{
            $('header').removeClass('_scrolled');
            setTimeout(function(){
                $('.template-wrap').css({'paddingTop': $('header').outerHeight()});
            }, 350)
        }
    })
});